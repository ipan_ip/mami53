<?php

namespace App\Exports;

use App\Test\MamiKosTestCase;

class AreaCityCleanUpCommandExportTest extends MamiKosTestCase
{

    public function testArray()
    {
        $data = [1,2,3];
        $exportClass = new AreaCityCleanUpCommandExport($data);

        $this->assertEquals($data,$exportClass->array());
    }
}
