<?php

namespace App\Exports;

use App\Test\MamiKosTestCase;

class UpdateConsultantCommandExportTest extends MamiKosTestCase
{

    public function testArray()
    {
        $data = [1,2,3];
        $exportClass = new UpdateConsultantCommandExport($data);

        $this->assertEquals($data,$exportClass->array());
    }
}
