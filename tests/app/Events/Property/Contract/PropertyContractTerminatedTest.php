<?php

namespace App\Events\Property\Contract;

use App\Entities\Property\PropertyContract;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Event;

class PropertyContractTerminatedTest extends MamiKosTestCase
{
    protected $withEvents = true;

    public function testEventSuccessTriggered()
    {
        Event::fake(PropertyContractTerminated::class);
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_INACTIVE
        ]);
        $contract->status = PropertyContract::STATUS_TERMINATED;
        $contract->save();
        Event::assertDispatched(PropertyContractTerminated::class);
    }
}
