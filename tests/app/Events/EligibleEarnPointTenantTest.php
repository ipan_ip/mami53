<?php

namespace App\Events;

use App\Entities\Level\KostLevel;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;
use App\User;

class EligibleEarnPointTenantTest extends MamiKosTestCase
{
    public function testConstruct_AttributesSaved()
    {
        $activity = factory(PointActivity::class)->create(['is_active' => 1]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];
        $level = factory(KostLevel::class)->create();

        $event = new EligibleEarnPointTenant($user, $activity->key, $notes, $level, true, 1000);
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertEquals($activity->id, $this->getNonPublicAttributeFromClass('activity', $event)->id);
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
        $this->assertEquals($level->id, $this->getNonPublicAttributeFromClass('level', $event)->id);
        $this->assertEquals(true, $this->getNonPublicAttributeFromClass('isMamirooms', $event));
        $this->assertEquals(1000, $this->getNonPublicAttributeFromClass('invoiceAmount', $event));
    }

    public function testConstruct_WhenActivityInactive_AttributesSaved()
    {
        $activity = factory(PointActivity::class)->create(['is_active' => 0]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];
        $level = factory(KostLevel::class)->create();

        $event = new EligibleEarnPointTenant($user, $activity->key, $notes, $level, true, 1000);
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertNull($this->getNonPublicAttributeFromClass('activity', $event));
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
        $this->assertEquals($level->id, $this->getNonPublicAttributeFromClass('level', $event)->id);
        $this->assertEquals(true, $this->getNonPublicAttributeFromClass('isMamirooms', $event));
        $this->assertEquals(1000, $this->getNonPublicAttributeFromClass('invoiceAmount', $event));
    }
}
