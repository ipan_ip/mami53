<?php

namespace App\Events\Forum;

use App\Entities\Forum\ForumThread;
use App\Test\MamiKosTestCase;

class ThreadDeactivatedTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $thread = $this->createMock(ForumThread::class);
        $event = new ThreadDeactivated($thread);
        $this->assertEquals($thread, $event->thread);
    }
}
