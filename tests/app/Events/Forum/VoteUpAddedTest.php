<?php

namespace App\Events\Forum;

use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;
use App\Test\MamiKosTestCase;

class VoteUpAddedTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $answer = $this->createMock(ForumAnswer::class);
        $vote = $this->createMock(ForumVote::class);
        $event = new VoteUpAdded($answer, $vote);
        $this->assertEquals($answer, $event->answer);
        $this->assertEquals($vote, $event->vote);
    }
}
