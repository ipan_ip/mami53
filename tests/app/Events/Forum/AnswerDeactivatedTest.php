<?php

namespace App\Events\Forum;

use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumThread;
use App\Test\MamiKosTestCase;

class AnswerDeactivatedTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $thread = $this->createMock(ForumThread::class);
        $answer = $this->createMock(ForumAnswer::class);
        $event = new AnswerDeactivated($thread, $answer);
        $this->assertEquals($thread, $event->thread);
        $this->assertEquals($answer, $event->answer);
    }
}
