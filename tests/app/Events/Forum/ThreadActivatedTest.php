<?php

namespace App\Events\Forum;

use App\Entities\Forum\ForumThread;
use App\Test\MamiKosTestCase;

class ThreadActivatedTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $thread = $this->createMock(ForumThread::class);
        $event = new ThreadActivated($thread);
        $this->assertEquals($thread, $event->thread);
    }
}
