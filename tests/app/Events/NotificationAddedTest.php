<?php

namespace App\Events;

use App\Test\MamiKosTestCase;
use Illuminate\Broadcasting\PrivateChannel;

class NotificationAddedTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $data = ['userid' => 123];
        $event = new NotificationAdded($data);
        $this->assertEquals($data, $event->data);
    }

    public function testBroadcastOn_Success()
    {
        $data = ['userid' => 123];
        $event = new NotificationAdded($data);
        $this->assertInstanceOf(PrivateChannel::class, $event->broadcastOn());
    }
}
