<?php

namespace App\Events;

use App\Test\MamiKosTestCase;
use App\User;

class NotificationSentTest extends MamiKosTestCase
{
    public function testConstruct_Success()
    {
        $user = $this->createMock(User::class);
        $type = 'type';
        $event = new NotificationSent($user, $type);
        $this->assertEquals($user, $event->user);
        $this->assertEquals($type, $event->type);
    }
}
