<?php

namespace App\Events;

use App\Entities\Level\KostLevel;
use App\Entities\Point\PointActivity;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class EligibleEarnPointTest extends MamiKosTestCase
{
    
    public function testConstruct_AttributesSaved()
    {
        $activityKey = 'key';
        $activity = factory(PointActivity::class)->create(['key' => $activityKey, 'is_active' => 1]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];

        $event = new EligibleEarnPoint($user, $activityKey, $notes);
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertEquals($activity->id, $this->getNonPublicAttributeFromClass('activity', $event)->id);
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
    }

    public function testConstruct_WhenActivityInactive_AttributesSaved()
    {
        $activityKey = 'key';
        $activity = factory(PointActivity::class)->create(['key' => $activityKey, 'is_active' => 0]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];

        $event = new EligibleEarnPoint($user, $activityKey, $notes);
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertNull($this->getNonPublicAttributeFromClass('activity', $event));
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
    }
    
    public function testConstruct_WithAdjustment_AttributesSaved()
    {
        $activityKey = 'key';
        $activity = factory(PointActivity::class)->create(['key' => $activityKey, 'is_active' => 1]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];

        $event = new EligibleEarnPoint($user, $activityKey, $notes, 'admin_adjustment_topup');
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertEquals($activity->id, $this->getNonPublicAttributeFromClass('activity', $event)->id);
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
        $this->assertEquals('admin_adjustment_topup', $this->getNonPublicAttributeFromClass('adjustment', $event));
    }

    public function testConstruct_WithLevel_AttributesSaved()
    {
        $activityKey = 'key';
        $activity = factory(PointActivity::class)->create(['key' => $activityKey, 'is_active' => 1]);
        $user = $this->createMock(User::class);
        $notes = ['notes1', 'notes2'];
        $level = factory(KostLevel::class)->create();
        $room = factory(Room::class)->create([
            'user_id' => $user->id,
        ]);
        $room->level()->attach($level->id);

        $event = new EligibleEarnPoint($user, $activityKey, $notes, null, $room->level->first());
        $this->assertEquals($user, $this->getNonPublicAttributeFromClass('user', $event));
        $this->assertEquals($activity->id, $this->getNonPublicAttributeFromClass('activity', $event)->id);
        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $event));
        $this->assertEquals($level->id, $this->getNonPublicAttributeFromClass('level', $event)->id);
    }
}
