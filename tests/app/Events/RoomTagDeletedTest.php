<?php
namespace App\Events;

use \App\Entities\Room\Element\RoomTag;
use \App\Events\RoomTagDeleted;

use \App\Test\MamiKosTestCase;

class RoomTagDeletedTest extends MamiKosTestCase
{
    public function testAttributes() {
        $roomTag = factory(RoomTag::class)->make(['id' => 1]);
        $event = new RoomTagDeleted($roomTag);

        $this->assertEquals($roomTag, $event->roomTag);
    }
}