<?php

namespace App\Exceptions;

use App\Entities\Room\Room;
use App\Exceptions\Owner\ResourceNotFoundException;
use App\Test\MamiKosTestCase;

class ResourceNotFoundExceptionTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */ 
    public function testRender()
    {
        $execption = new ResourceNotFoundException('Not Found', 404);
        $execption->setModel(Room::class);
        $result = $execption->render()->getData();
        $this->assertFalse($result->status);
        $this->assertEquals($result->meta->response_code, 404);
        $this->assertEquals($result->meta->code, 404);
        $this->assertEquals($result->meta->severity, 'NOT FOUND');
        $this->assertEquals($result->meta->message, "No query results for model [App\Entities\Room\Room].");
    }
}
