<?php

namespace App\Exceptions;

use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Test\MamiKosTestCase;

class ResourceNotOwnedExceptionTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */ 
    public function testRender()
    {
        $execption = new ResourceNotOwnedException('Unauthorize', 401);
        $result = $execption->render()->getData();
        $this->assertFalse($result->status);
        $this->assertEquals($result->meta->response_code, 401);
        $this->assertEquals($result->meta->code, 401);
        $this->assertEquals($result->meta->severity, 'UNAUTHORIZE');
        $this->assertEquals($result->meta->message, 'Unauthorize');
    }
}
