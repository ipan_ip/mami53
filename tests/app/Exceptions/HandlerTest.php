<?php

namespace App\Exceptions;

use App\Test\MamiKosTestCase;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Request;

use function App\Channel\ABTest\config;

class HandlerTest extends MamiKosTestCase
{
    private $handler;

    protected function setUp()
    {
        parent::setUp();
        $this->handler = app()->make(Handler::class);
    }

    public function testRenderTokenMismatch()
    {
        $response = $this->handler->render(
            Request::create(
                '/',
                'GET'
            ),
            new TokenMismatchException("mismatch")
        );

        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testRenderTokenMismatchAjax()
    {
        $request = Request::create(
            '/',
            'GET'
        );
        $request->headers->set('Accept', 'application/json');
        $response = $this->handler->render(
            $request,
            new TokenMismatchException("mismatch")
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testRenderUnauthenticated()
    {
        $response = $this->handler->render(
            Request::create(
                '/',
                'GET'
            ),
            new AuthenticationException("unauthenticated")
        );
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(route('web.login'), $response->getTargetUrl());
    }

    public function testRenderUnauthenticatedAjax()
    {
        $request = Request::create(
            '/',
            'GET'
        );
        $request->headers->set('Accept', 'application/json');
        $response = $this->handler->render(
            $request,
            new AuthenticationException("unauthenticated")
        );
        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testRenderUnauthenticatedAdmin()
    {
        $request = Request::create(
            '/admin/owner',
            'GET'
        );
        $response = $this->handler->render(
            $request,
            new AuthenticationException("unauthenticated")
        );
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(route('admin.login'), $response->getTargetUrl());
    }

    public function testRenderWithDebug()
    {
        $request = Request::create(
            '/landing',
            'GET'
        );
        $response = $this->handler->render(
            $request,
            new Exception("unauthenticated")
        );

        $this->assertEquals(500, $response->getStatusCode());
    }

    public function testRenderNoDebug()
    {
        app('config')->set(['app.debug' => false]);
        $request = Request::create(
            '/landing',
            'GET'
        );
        $response = $this->handler->render(
            $request,
            new Exception("unauthenticated")
        );

        $this->assertEquals(500, $response->getStatusCode());
    }

    public function testRenderNoDebugApi()
    {
        app('config')->set(['app.debug' => false]);
        $request = Request::create(
            '/api/amazing-resource',
            'GET'
        );
        $response = $this->handler->render(
            $request,
            new Exception("unauthenticated")
        );

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRenderNoDebugGaruda()
    {
        app('config')->set(['app.debug' => false]);
        $request = Request::create(
            '/garuda/amazing-resource',
            'GET'
        );
        $response = $this->handler->render(
            $request,
            new Exception("unauthenticated")
        );
        $this->assertEquals(500, $response->getStatusCode());
    }
}
