<?php
namespace App\Abstractor;

use App\Test\MamiKosTestCase;
use Bugsnag\Client;

class ErrorNotifierTest extends MamiKosTestCase
{
    protected $client;
    protected $notifier;

    protected function setUp()
    {
        $this->client = \Mockery::mock(Client::class);
        $this->notifier = new ErrorNotifier($this->client);
    }

    public function testNotifyException()
    {
        $err = new \Exception("An exception occurs");
        $this->client->shouldReceive('notifyException')->once()->with($err);
        $this->notifier->notifyException($err);
    }

    public function testOtherMethodShouldBeDirectedToClient()
    {
        $str = "Some string";
        $this->client->shouldReceive('someMethod')->once()->with($str);
        $this->notifier->someMethod($str);
    }
}