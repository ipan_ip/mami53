<?php

namespace App\Abstractor;

use App\Test\MamiKosTestCase;
use Cviebrock\LaravelElasticsearch\Manager;

class ElasticSearchClientTest extends MamiKosTestCase
{
    protected $es;
    protected $client;

    protected function setUp(): void
    {
        $this->es = \Mockery::mock(Manager::class);
        $this->client = new ElasticSearchClient($this->es);
    }

    public function testIndex(): void
    {
        $arr = ['some' => 'data', 'to' => 'test'];
        $this->es->shouldReceive('index')->once()->with($arr);
        $this->client->index($arr);
    }

    public function testSearch(): void
    {
        $arr = ['some' => 'data', 'to' => 'test'];
        $this->es->shouldReceive('search')->once()->with($arr);
        $this->client->search($arr);
    }

    public function testDelete(): void
    {
        $arr = ['some' => 'data', 'to' => 'test'];
        $this->es->shouldReceive('delete')->once()->with($arr);
        $this->client->delete($arr);
    }
}
