<?php

namespace App\Libraries;

use App\Test\MamiKosTestCase;

class NumberGeneratorTest extends MamiKosTestCase
{
    private $numbersPool = [];
    private $numbersLength = 0;
    private $numbersToGenerate = 0;

    protected function setUp() : void
    {
        parent::setUp();
        $this->numbersPool = range(1, 9);
        $this->numbersLength = 3;
        $this->numbersToGenerate = 10;
    }

    public function testConstruct()
    {
        $event = new NumberGenerator($this->numbersPool, $this->numbersLength, $this->numbersToGenerate);

        $this->assertEquals($this->numbersPool, $event->numbersPool);
        $this->assertEquals($this->numbersLength, $event->numbersLength);
        $this->assertEquals($this->numbersToGenerate, $event->numbersToGenerate);
    }

    public function testGenerate()
    {
        $generator = new NumberGenerator($this->numbersPool, $this->numbersLength, $this->numbersToGenerate);

        $this->assertTrue(!empty($generator->generate()));
    }
}
