<?php
declare(strict_types=1);

namespace App\Libraries;

use InvalidArgumentException;
use TypeError;
use Illuminate\Http\Request;

use App\Test\MamiKosTestCase;
use App\Libraries\RequestHelper;

final class RequestHelperTest extends MamiKosTestCase
{
    protected $withDbTransaction = false;

    public function testGetClientIpRegardingCF(): void
    {
    }

    public function testGetIsPrivateIpShouldReturnTrueForPrivateIps(): void
    {
        $privateIps = [
            '127.0.0.1', // localhost
            '172.24.0.1', // docker ip
            '192.168.0.1', // general private ip from private network
        ];

        foreach ($privateIps as $privateIp)
        {
            $this->assertTrue(RequestHelper::getIsPrivateIp($privateIp));
        }
    }

    public function testGetIsPrivateIpShouldReturnFalseForPublicIps(): void
    {
        $publicIps = [
            '123.231.165.26', // palagan office
            '3.1.17.228', // mamiVPN
            '172.217.22.14', // google.com
        ];
        
        foreach ($publicIps as $publicIp)
        {
            $this->assertFalse(RequestHelper::getIsPrivateIp($publicIp));
        }
    }

    public function testGetIsPrivateIpShouldReturnExceptionWithEmptyIp(): void
    {
        $this->expectException(InvalidArgumentException::class);
        RequestHelper::getIsPrivateIp('');
    }

    public function testGetIsPrivateIpShouldReturnExceptionWithNullIp(): void
    {
        $this->expectException(TypeError::class);
        RequestHelper::getIsPrivateIp(null);
    }

    public function testOptimizeLocationShouldSucceedForNormalLocationData(): void
    {
        $request = new Request([
            'location' => [
                [
                    106.7830409918847,
                    -6.21377170754858
                ],
                [
                    106.82374800811516,
                    -6.173302292451408
                ]
            ]
        ]);
        RequestHelper::optimizeLocation($request);

        $this->assertEquals(106.783041, $request['location'][0][0]);
        $this->assertEquals(-6.213772, $request['location'][0][1]);
        $this->assertEquals(106.823748, $request['location'][1][0]);
        $this->assertEquals(-6.173302, $request['location'][1][1]);
    }

    public function testOptimizeLocationShouldNoProblemForNoLocationData(): void
    {
        $request = new Request([]);
        RequestHelper::optimizeLocation($request);
    }

    public function testOptimizeLocationShouldNoProblemForMalformedData(): void
    {
        $request = new Request([
            'location' => [
                [106.7830409918847],
                [-6.21377170754858]
            ]
        ]);
        RequestHelper::optimizeLocation($request);
    }
}