<?php

namespace App\Libraries;

use App\Test\MamiKosTestCase;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Room\Room;
use App\Entities\Room\Element\RoomUnit;
use App\Libraries\KostLevelImporter;

class KostLevelImporterTest extends MamiKosTestCase
{
    
    private $regularLevel;
    private $standardLevel;
    private $executiveLevel;

    private $regularRoomLevel;
    private $standardRoomLevel;
    private $executiveRoomLevel;

    protected function setUp()
    {
        parent::setUp();

        $this->regularLevel = factory(KostLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'is_hidden' => false,
            'order' => 1,
        ]);

        $this->standardLevel = factory(KostLevel::class)->create([
            'name' => 'Stardard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $this->executiveLevel = factory(KostLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 3,
        ]);

        $this->regularRoomLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'is_hidden' => false,
            'order' => 1,
        ]);

        $this->standardRoomLevel = factory(RoomLevel::class)->create([
            'name' => 'Stardard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $this->executiveRoomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 3,
        ]);
    }

    public function testImportSingleKostRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->regularLevel);
    }

    public function testImportSingleKostRecord_WithSingleRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->regularLevel, false, array($this->regularRoomLevel));
    }

    public function testImportSingleKostRecord_WithMultiRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room, 3);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->regularLevel, false, array($this->regularRoomLevel, $this->standardRoomLevel, $this->executiveRoomLevel));
    }

    public function testImportUpdatedSingleKostRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
            ],
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->executiveLevel, true);
    }

    public function testImportUpdatedSingleKostRecord_WithSingleRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->executiveLevel, true, array($this->standardRoomLevel));
    }

    public function testImportUpdatedSingleKostRecord_WithMultiRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $roomUnit = $this->createRoomUnit($room, 3);

        $data = array(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[1]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit[2]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room, $roomUnit, $this->executiveLevel, true, array($this->standardRoomLevel, $this->executiveRoomLevel, $this->regularRoomLevel));
    }

    public function testImportMultiKostRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0]);
        $roomUnit2 = $this->createRoomUnit($room[1]);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->regularLevel, false);
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->standardLevel, true);
    }

    public function testImportMultiKostRecord_WithSingleRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0]);
        $roomUnit2 = $this->createRoomUnit($room[1]);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->regularLevel, false, array($this->regularRoomLevel));
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->standardLevel, true, array($this->regularRoomLevel));
    }

    public function testImportMultiKostRecord_WithMultiRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0], 3);
        $roomUnit2 = $this->createRoomUnit($room[1], 3);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->regularLevel, false, array($this->regularRoomLevel, $this->standardRoomLevel, $this->executiveRoomLevel));
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->standardLevel, true, array($this->regularRoomLevel, $this->standardRoomLevel, $this->executiveRoomLevel));
    }

    public function testImportUpdatedMultiKostRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0]);
        $roomUnit2 = $this->createRoomUnit($room[1]);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
            ],
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => false,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->executiveLevel, true);
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->executiveLevel, false);
    }

    public function testImportUpdatedMultiKostRecord_WithSingleRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0]);
        $roomUnit2 = $this->createRoomUnit($room[1]);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->executiveLevel, true, array($this->standardRoomLevel));
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->executiveLevel, false, array($this->standardRoomLevel));
    }

    public function testImportUpdatedMultiKostRecord_WithMultiRoomRecord_ShouldSuccess()
    {
        $room = factory(Room::class, 2)->create(['is_active' => true]);
        $roomUnit1 = $this->createRoomUnit($room[0], 3);
        $roomUnit2 = $this->createRoomUnit($room[1], 3);

        $data = array(
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->regularLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->standardLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[1]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[2]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => $room[0]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => true,
                'room_id' => $roomUnit1[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[1]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit1[2]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
            [
                'kost_id' => $room[1]->song_id,
                'level_id' => $this->executiveLevel->id,
                'is_charge_by_room' => false,
                'room_id' => $roomUnit2[0]->id,
                'room_level_id' => $this->standardRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[1]->id,
                'room_level_id' => $this->executiveRoomLevel->id,
            ],
            [
                'kost_id' => null,
                'level_id' => null,
                'is_charge_by_room' => null,
                'room_id' => $roomUnit2[2]->id,
                'room_level_id' => $this->regularRoomLevel->id,
            ],
        );

        (new KostLevelImporter)->array($data);

        $this->assertKostAndRoomLevel($room[0], $roomUnit1, $this->executiveLevel, true, array($this->standardRoomLevel, $this->executiveRoomLevel, $this->regularRoomLevel));
        $this->assertKostAndRoomLevel($room[1], $roomUnit2, $this->executiveLevel, false, array($this->standardRoomLevel, $this->executiveRoomLevel, $this->regularRoomLevel));
    }

    private function createRoomUnit(Room $room, int $count = 1)
    {
        $result = factory(RoomUnit::class, $count)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        return $result;
    }

    private function assertKostAndRoomLevel(Room $room, $roomUnits, KostLevel $kostLevel = null, bool $isChargeByRoom = false, array $roomLevels = null)
    {
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        foreach ($roomUnits as $index => $roomUnit) { 
            $roomUnitResult = $result->room_unit()->where('id', $roomUnit->id)->first();
            $this->assertNotNull($roomUnitResult);
            $this->assertEquals($roomUnit->id, $roomUnitResult->id);
            $this->assertEquals($isChargeByRoom, (bool) $roomUnitResult->is_charge_by_room);

            if (!is_null($roomLevels)) {
                $roomLevel = $roomLevels[$index];
                
                $this->assertEquals($roomLevel->id, $roomUnitResult->room_level_id);
                
                $levelHistory = $roomUnitResult->level_histories()->orderBy('id', 'DESC')->first();
                $this->assertEquals($roomLevel->id, $levelHistory->room_level_id);
            }
        }

        $levelResult = $result->level()->where('level_id', $kostLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($kostLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories()->where('kost_id', $room->song_id)->latest()->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($kostLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
    }
}
