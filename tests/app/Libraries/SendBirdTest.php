<?php

namespace App\Entities\SendBird\Helper;
namespace App\Libraries;

use App\Libraries\SendBird\HttpClient;
use App\Libraries\SendBird\ParamsError;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use PHPUnit\Framework\MockObject\MockObject;

class SendBirdTest extends MamiKosTestCase
{
    /** @var SendBirdClient|MockObject */
    private $client;
    private $response;

    protected function setUp() : void
    {
        parent::setUp();
        $this->response = $this->createMock(ResponseInterface::class);
        $this->client = $this->createMock(HttpClient::class);
    }

    public function testCreateUser_Success()
    {
        $userId = 1;
        $nickname = 'name';
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId, 'nickname' => $nickname]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->createUser($userId, $nickname);
        $this->assertNotNull($response);
    }

    public function testCreateUserShouldFailWhenUserId0()
    {
        $userId = 0;
        $nickname = 'name';
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId, 'nickname' => $nickname]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->createUser($userId, $nickname);
        $this->assertNull($response);
    }

    public function testCreateUser_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->createUser(0, '');
        $this->assertNull($response);
    }

    public function testCreateUser_ExceptionThrown()
    {
        $userId = 1;
        $nickname = 'name';
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->createUser($userId, $nickname);
        $this->assertNull($response);
    }

    public function testCreateGroupChannelWithBadParams()
    {
        $helper = $this->createHelper();
        $this->expectException(ParamsError::class);
        $helper->createGroupChannel([], []);
    }

    public function testCreateGroupChannelSuccess()
    {
        $this->response->method('getBody')->willReturn(json_encode(['channel_url' => 'some_url']));
        $this->client->method('post')->willReturn($this->response);
        $helper = $this->createHelper();

        $userIds = [1, 2, 3];
        $response = $helper->createGroupChannel($userIds, []);
        $this->assertEquals('some_url', $response['channel_url']);
    }

    public function testCreateUserWithAccessToken_Success()
    {
        $userId = 1;
        $nickname = 'name';
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId, 'nickname' => $nickname]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->createUserWithAccessToken($userId, $nickname);
        $this->assertNotNull($response);
    }

    public function testUpdateUserMetadata_Success()
    {
        $userId = 1;
        $metadata = ['role' => 'admin'];
        $this->response->method('getBody')->willReturn(json_encode($metadata));
        $this->client->method('put')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->updateUserMetadata($userId, $metadata);
        $this->assertNotNull($response);
    }

    public function testUpdateUserMetadata_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->updateUserMetadata(0, []);
        $this->assertNull($response);
    }

    public function testUpdateUserMetadata_ExceptionThrown()
    {
        $userId = 1;
        $metadata = ['role' => 'admin'];
        $exception = new \Exception;
        $this->client->method('put')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->updateUserMetadata($userId, $metadata);
        $this->assertNull($response);
    }

    public function testGetUserDetail_Success()
    {
        $userId = 1;
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserDetail($userId);
        $this->assertNotNull($response);
    }

    public function testGetUserDetail_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->getUserDetail(0);
        $this->assertNull($response);
    }

    public function testGetUserDetail_ExceptionThrown()
    {
        $userId = 1;
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getUserDetail($userId);
        $this->assertNull($response);
    }

    public function testGetUserDetailWithUnreadCount_Success()
    {
        $userId = 1;
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserDetailWithUnreadCount($userId);
        $this->assertNotNull($response);
    }

    public function testGetUserDetailWithUnreadCount_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->getUserDetailWithUnreadCount(0);
        $this->assertNull($response);
    }

    public function testGetUserDetailWithUnreadCount_ExceptionThrown()
    {
        $userId = 1;
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getUserDetailWithUnreadCount($userId);
        $this->assertNull($response);
    }

    public function testGetUserList_Success()
    {
        $this->response->method('getBody')->willReturn(json_encode([['user_id' => 1], ['user_id' => 2]]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserList();
        $this->assertNotNull($response);
    }

    public function testGetUserList_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getUserList();
        $this->assertNull($response);
    }

    public function testUpdateUser_Success()
    {
        $userId = 1;
        $nickname = 'name2';
        $profileUrl = 'http://url.com';
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId, 'nickname' => $nickname, 'profile_url' => $profileUrl]));
        $this->client->method('put')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->updateUser($userId, $nickname, $profileUrl);
        $this->assertNotNull($response);
    }

    public function testUpdateUser_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->updateUser(0);
        $this->assertNull($response);
    }

    public function testUpdateUser_ExceptionThrown()
    {
        $userId = 1;
        $nickname = 'name2';
        $exception = new \Exception;
        $this->client->method('put')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->updateUser($userId, $nickname);
        $this->assertNull($response);
    }

    public function testRevokeAccessToken_Success()
    {
        $userId = 1;
        $this->response->method('getBody')->willReturn(json_encode(['user_id' => $userId, 'access_token' => 'qwerty']));
        $this->client->method('put')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->revokeAccessToken($userId);
        $this->assertNotNull($response);
    }

    public function testRevokeAccessToken_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->revokeAccessToken(0);
        $this->assertNull($response);
    }

    public function testRevokeAccessToken_ExceptionThrown()
    {
        $userId = 1;
        $exception = new \Exception;
        $this->client->method('put')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->revokeAccessToken($userId);
        $this->assertNull($response);
    }

    public function testDeleteUser_Success()
    {
        $userId = 1;
        $this->response->method('getBody')->willReturn(json_encode([]));
        $this->client->method('delete')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->deleteUser($userId);
        $this->assertNotNull($response);
    }

    public function testDeleteUser_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->deleteUser(0);
        $this->assertNull($response);
    }

    public function testDeleteUser_ExceptionThrown()
    {
        $userId = 1;
        $exception = new \Exception;
        $this->client->method('delete')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->deleteUser($userId);
        $this->assertNull($response);
    }

    public function testCreateGroupChannelOrGetExisting_Success()
    {
        $channelUrl = 'url';
        $channelName = 'name';
        $userIds = [1, 2];
        $this->response->method('getBody')->willReturn(json_encode(['channel_url' => $channelUrl]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->createGroupChannelOrGetExisting($channelUrl, $channelName, $userIds);
        $this->assertNotNull($response);
    }

    public function testCreateGroupChannelOrGetExisting_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->createGroupChannelOrGetExisting('', '', []);
        $this->assertNull($response);
    }

    public function testCreateGroupChannelOrGetExisting_ExceptionThrown()
    {
        $channelUrl = 'url';
        $channelName = 'name';
        $userIds = [1, 2];
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->createGroupChannelOrGetExisting($channelUrl, $channelName, $userIds);
        $this->assertNull($response);
    }

    public function testCreateGroupChannelAlwaysNew_Success()
    {
        $channelUrl = 'url';
        $channelName = 'name';
        $userIds = [1, 2];
        $this->response->method('getBody')->willReturn(json_encode(['channel_url' => $channelUrl]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->createGroupChannelAlwaysNew($channelUrl, $channelName, $userIds);
        $this->assertNotNull($response);
    }

    public function testGetGroupChannelDetail_Success()
    {
        $channelUrl = 'url';
        $this->response->method('getBody')->willReturn(json_encode(['channel_url' => $channelUrl]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getGroupChannelDetail($channelUrl);
        $this->assertNotNull($response);
    }

    public function testGetGroupChannelDetail_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->getGroupChannelDetail('');
        $this->assertNull($response);
    }

    public function testGetGroupChannelDetail_ExceptionThrown()
    {
        $channelUrl = 'url';
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getGroupChannelDetail($channelUrl);
        $this->assertNull($response);
    }


    public function testGetUserGroupChannelsWithEmptyUserId()
    {
        $helper = $this->createHelper();
        $this->expectException(\Exception::class);
        $helper->getUserGroupChannels(0);
    }

    public function testGetUserGroupChannels_Success()
    {
        $userId = rand(1, 1000);
        $this->response->method('getBody')->willReturn(json_encode([
            'channels' => [['channel_url' => 'url1'], ['channel_url' => 'url2']]
        ]));
        $this->client->method('get')->willReturn($this->response);
        $helper = $this->createHelper();

        $response = $helper->getUserGroupChannels($userId);
        $this->assertEquals(2, count($response['channels']));
    }

    public function testGetUsersGroupChannelList_Success()
    {
        $userId = 1;
        $this->response->method('getBody')->willReturn(json_encode([['channel_url' => 'url'], ['channel_url' => 'url2']]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUsersGroupChannelList($userId);
        $this->assertNotNull($response);
    }

    public function testGetUsersGroupChannelList_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->getUsersGroupChannelList(0);
        $this->assertNull($response);
    }

    public function testGetUsersGroupChannelList_ExceptionThrown()
    {
        $userId = 1;
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getUsersGroupChannelList($userId);
        $this->assertNull($response);
    }

    public function testAddUserToGroupChannel_Success()
    {
        $userId = 1;
        $channelUrl = 'url';
        $this->response->method('getBody')->willReturn(json_encode(['channel_url' => $channelUrl, 'members' => [['user_id' => $userId]]]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->addUserToGroupChannel($userId, $channelUrl);
        $this->assertNotNull($response);
    }

    public function testAddUserToGroupChannel_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->addUserToGroupChannel(0, '');
        $this->assertNull($response);
    }

    public function testAddUserToGroupChannel_ExceptionThrown()
    {
        $userId = 1;
        $channelUrl = 'url';
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->addUserToGroupChannel($userId, $channelUrl);
        $this->assertNull($response);
    }

    public function testGetGroupChannelMessages_Success()
    {
        $channelUrl = 'url';
        $this->response->method('getBody')->willReturn(json_encode([['message' => 'msg'], ['message' => 'msg']]));
        $this->client->method('get')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getGroupChannelMessages($channelUrl);
        $this->assertNotNull($response);
    }

    public function testGetGroupChannelMessages_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->getGroupChannelMessages('');
        $this->assertNull($response);
    }

    public function testGetGroupChannelMessages_ExceptionThrown()
    {
        $channelUrl = 'url';
        $exception = new \Exception;
        $this->client->method('get')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->getGroupChannelMessages($channelUrl);
        $this->assertNull($response);
    }

    public function testSendMessageToGroupChannel_Success()
    {
        $channelUrl = 'url';
        $userId = 1;
        $message = 'message';
        $this->response->method('getBody')->willReturn(json_encode(['message' => $message]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->sendMessageToGroupChannel($channelUrl, $userId, $message);
        $this->assertNotNull($response);
    }

    public function testSendMessageToGroupChannel_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->sendMessageToGroupChannel('', 0, '');
        $this->assertNull($response);
    }

    public function testSendMessageToGroupChannel_ClientExceptionThrown()
    {
        $channelUrl = 'url';
        $userId = 1;
        $message = 'message';
        $request = $this->createMock(RequestInterface::class);
        $clientException = new ClientException('errormessage', $request);
        $this->client->method('post')->willThrowException($clientException);
        Bugsnag::shouldReceive('notifyException')->once();

        $helper = $this->createHelper();
        $response = $helper->sendMessageToGroupChannel($channelUrl, $userId, $message);
        $this->assertNull($response);
    }

    public function testSendMessageToGroupChannel_ExceptionThrown()
    {
        $channelUrl = 'url';
        $userId = 1;
        $message = 'message';
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->sendMessageToGroupChannel($channelUrl, $userId, $message);
        $this->assertNull($response);
    }

    public function testSendAdminMessageToGroupChannel_Success()
    {
        $channelUrl = 'url';
        $message = 'message';
        $this->response->method('getBody')->willReturn(json_encode(['message' => $message]));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->sendAdminMessageToGroupChannel($channelUrl, $message);
        $this->assertNotNull($response);
    }

    public function testSendAdminMessageToGroupChannel_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->sendAdminMessageToGroupChannel('', '');
        $this->assertNull($response);
    }

    public function testSendAdminMessageToGroupChannel_ExceptionThrown()
    {
        $channelUrl = 'url';
        $message = 'message';
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->sendAdminMessageToGroupChannel($channelUrl, $message);
        $this->assertNull($response);
    }

    public function testSendFileUrlToGroupChannel_Success()
    {
        $channelUrl = 'url';
        $userId = 1;
        $fileUrl = 'http://file.url';
        $this->response->method('getBody')->willReturn(json_encode(['message' => '']));
        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->sendFileUrlToGroupChannel($channelUrl, $userId, $fileUrl);
        $this->assertNotNull($response);
    }

    public function testSendFileUrlToGroupChannel_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->sendFileUrlToGroupChannel('', 0, '');
        $this->assertNull($response);
    }

    public function testSendFileUrlToGroupChannel_ExceptionThrown()
    {
        $channelUrl = 'url';
        $userId = 1;
        $fileUrl = 'http://file.url';
        $exception = new \Exception;
        $this->client->method('post')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->sendFileUrlToGroupChannel($channelUrl, $userId, $fileUrl);
        $this->assertNull($response);
    }

    public function testLeaveChannel_Success()
    {
        $channelUrl = 'url';
        $userIds = [1];
        $this->response->method('getBody')->willReturn(json_encode([]));
        $this->client->method('put')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->leaveChannel($channelUrl, $userIds);
        $this->assertNotNull($response);
    }

    public function testLeaveChannel_EmptyParam()
    {
        $helper = $this->createHelper();
        $response = $helper->leaveChannel('', []);
        $this->assertNull($response);
    }

    public function testLeaveChannel_ExceptionThrown()
    {
        $channelUrl = 'url';
        $userIds = [1];
        $exception = new \Exception;
        $this->client->method('put')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $helper = $this->createHelper();
        $response = $helper->leaveChannel($channelUrl, $userIds);
        $this->assertNull($response);
    }

    private function createHelper()
    {
        return new SendBird($this->client);
    }

    public function testIsValidApplozicChannelUrl_Return_True_For_Valid_Channel_Name()
    {
        $this->assertTrue(SendBird::isValidApplozicGroupChannelUrl('12345'));
    }

    public function testIsValidApplozicChannelUrl_Return_False_For_Invalid_Channel_Name()
    {
        $this->assertFalse(SendBird::isValidApplozicGroupChannelUrl(null));
        $this->assertFalse(SendBird::isValidApplozicGroupChannelUrl(''));
        $this->assertFalse(SendBird::isValidApplozicGroupChannelUrl('abcd'));
        $this->assertFalse(SendBird::isValidApplozicGroupChannelUrl('1234 '));
        $this->assertFalse(SendBird::isValidApplozicGroupChannelUrl(' 1234'));
    }

    public function testIsValidSendBirdDefaultGroupChannelUrl_Return_True_For_Valid_Channel_Name()
    {
        $this->assertTrue(SendBird::isValidSendBirdDefaultGroupChannelUrl('sendbird_group_channel_abcd'));
        $this->assertTrue(SendBird::isValidSendBirdDefaultGroupChannelUrl('sendbird_group_channel_1234'));
        $this->assertTrue(SendBird::isValidSendBirdDefaultGroupChannelUrl('sendbird_group_channel_183150021_40c7dab9c983d4fe041a6ccd882aabed20eee3db'));
    }

    public function testIsValidSendBirdDefaultGroupChannelUrl_Return_False_For_Invalid_Channel_Name()
    {
        $this->assertFalse(SendBird::isValidSendBirdDefaultGroupChannelUrl(null));
        $this->assertFalse(SendBird::isValidSendBirdDefaultGroupChannelUrl(''));
        $this->assertFalse(SendBird::isValidSendBirdDefaultGroupChannelUrl('1234'));
        $this->assertFalse(SendBird::isValidSendBirdDefaultGroupChannelUrl('Kost '));
    }

    public function testIsValidChannelUrl_Return_True_For_Valid_Channel_Name()
    {
        $this->assertTrue(SendBird::isValidChannelUrl('12345'));
        $this->assertTrue(SendBird::isValidChannelUrl('sendbird_group_channel_abcd'));
        $this->assertTrue(SendBird::isValidChannelUrl('sendbird_group_channel_1234'));
        $this->assertTrue(SendBird::isValidChannelUrl('sendbird_group_channel_183150021_40c7dab9c983d4fe041a6ccd882aabed20eee3db'));
    }

    public function testIsValidChannelUrl_Return_False_For_Invalid_Channel_Name()
    {
        $this->assertFalse(SendBird::isValidChannelUrl(null));
        $this->assertFalse(SendBird::isValidChannelUrl(''));
        $this->assertFalse(SendBird::isValidChannelUrl('abcd'));
        $this->assertFalse(SendBird::isValidChannelUrl('1234 '));
        $this->assertFalse(SendBird::isValidChannelUrl(' 1234'));
        $this->assertFalse(SendBird::isValidChannelUrl(' 1234 '));
        $this->assertFalse(SendBird::isValidChannelUrl('Kost '));
        $this->assertFalse(SendBird::isValidChannelUrl('kost '));
    }

    /**
     * @group UG-4487
     */
    public function testUpdateGroupChannel_ShouldOnlyAllowWhitelistedParams()
    {
        $channelUrl = str_random();
        $customType = str_random();
        $params = [
            'custom_type' => $customType,
            'not_on_whitelist' => str_random(),
        ];

        $this->client->method('setParams')->with([
            'custom_type' => $customType,
        ]);
        $this->response->method('getBody')->willReturn(json_encode([
            'custom_type' => $customType,
            'channel_url' => $channelUrl,
        ]));
        $this->client->method('put')->willReturn($this->response);

        $result = $this->createHelper()->updateGroupChannel($channelUrl, $params);

        $this->assertNotNull($result);
    }

    /**
     * @group UG-4487
     */
    public function testUpdateGroupChannel_ShouldOnlyAllowNonEmptyChannel()
    {
        $this->expectException(ParamsError::class);
        $channelUrl = str_repeat(' ', mt_rand(0, 2));
        $params = [];

        $result = $this->createHelper()->updateGroupChannel($channelUrl, $params);

        $this->assertTrue(false);
    }
}

function sleep($sec)
{
    // 
}
