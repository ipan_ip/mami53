<?php
declare(strict_types=1);

namespace App\Libraries;

use App\Test\MamiKosTestCase;
use App\Libraries\StringHelper;

final class StringHelperTest extends MamikosTestCase
{
    protected $withDbTransaction = false;

    ///////////////////////////////////////////////////////////////////
    // StringHelper::startsWith
    ///////////////////////////////////////////////////////////////////
    public function testStartsWithShouldReturnTrueWhenParamsMatchedWithCaseNonSensitive(): void
    {
        $this->assertTrue(StringHelper::startsWith('People love Mamikos', 'People'));
        $this->assertTrue(StringHelper::startsWith('People love Mamikos', 'people'));
        $this->assertTrue(StringHelper::startsWith('People love Mamikos', ''));
        $this->assertTrue(StringHelper::startsWith(null, ''));
        $this->assertTrue(StringHelper::startsWith('', ''));
    }

    public function testStartsWithShouldReturnTrueWhenParamsMatchedWithCaseSensitive(): void
    {
        $this->assertTrue(StringHelper::startsWith('People love Mamikos', 'People', true));
        $this->assertTrue(StringHelper::startsWith('People love Mamikos', '', true));
        $this->assertTrue(StringHelper::startsWith(null, '', true));
        $this->assertTrue(StringHelper::startsWith('', '', true));
    }

    public function testStartsWithShouldReturnFalseWhenParamsUnmatchedWithCaseNonSensitive(): void
    {
        $this->assertFalse(StringHelper::startsWith('People love Mamikos', 'a'));
    }

    public function testStartsWithShouldReturnFalseWhenParamsUnmatchedWithCaseSensitive(): void
    {
        $this->assertFalse(StringHelper::startsWith('People love Mamikos', 'people', true));
        $this->assertFalse(StringHelper::startsWith('People love Mamikos', 'a', true));
    }
}