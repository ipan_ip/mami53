<?php

namespace App\Libraries\SendBird;

use App\Test\MamiKosTestCase;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class HttpClientTest extends MamiKosTestCase
{
    private $client;

    protected function setUp() : void
    {
        parent::setUp();
        $response = $this->createMock(ResponseInterface::class);
        $this->client = $this->createMock(Client::class);
        $this->client->method('__call')->with($this->isType('string'), $this->isType('array'))->willReturn($response);
        $this->client->method('request')->with($this->isType('string'), $this->isType('string'), $this->isType('array'))->willReturn($response);
    }

    public function testAsync()
    {
        $sendbird = $this->createSendBird();
        $sendbird->async();
        $this->assertSame(true, $this->getNonPublicAttributeFromClass('requestAsync', $sendbird));
    }

    public function testCallback()
    {
        $callback = function() {
            return true;
        };
        $sendbird = $this->createSendBird();
        $sendbird->callback($callback);
        $this->assertSame($callback, $this->getNonPublicAttributeFromClass('requestCallback', $sendbird));
    }

    public function testSetParams()
    {
        $params = ['user_id' => 123];
        $sendbird = $this->createSendBird();
        $sendbird->setParams($params);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $sendbird));
    }

    public function testAddParam()
    {
        $key = 'user_id';
        $value = 123;
        $params = [$key => $value];
        $sendbird = $this->createSendBird();
        $sendbird->addParam($key, $value);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $sendbird));
    }

    public function testClearParams()
    {
        $params = ['user_id' => 123];
        $sendbird = $this->createSendBird();
        $sendbird->setParams($params);
        $sendbird->clearParams();
        $this->assertSame([], $this->getNonPublicAttributeFromClass('additionalParams', $sendbird));
    }

    public function testGet()
    {
        $sendbird = $this->createSendBird();
        $result = $sendbird->get('/');
        $headers = ['Content-Type' => 'application/json', 'Api-Token' => 'sendbird.api_token'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $sendbird));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPost_NotAsync()
    {
        $params = ['user_id' => 123];
        $sendbird = $this->createSendBird();
        $result = $sendbird->post('/', $params);
        $headers = ['Content-Type' => 'application/json', 'Api-Token' => 'sendbird.api_token'];
        $body = json_encode($params);
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $sendbird));
        $this->assertContains($body, $this->getNonPublicAttributeFromClass('args', $sendbird));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPost_Async()
    {
        $sendbird = $this->createSendBird();
        $sendbird->async();
        $result = $sendbird->post('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPut_NotAsync()
    {
        $sendbird = $this->createSendBird();
        $result = $sendbird->put('/');
        $headers = ['Content-Type' => 'application/json', 'Api-Token' => 'sendbird.api_token'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $sendbird));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPut_Async()
    {
        $sendbird = $this->createSendBird();
        $sendbird->async();
        $result = $sendbird->put('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testDelete_NotAsync()
    {
        $sendbird = $this->createSendBird();
        $result = $sendbird->delete('/');
        $headers = ['Content-Type' => 'application/json', 'Api-Token' => 'sendbird.api_token'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $sendbird));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testDelete_Async()
    {
        $sendbird = $this->createSendBird();
        $sendbird->async();
        $result = $sendbird->delete('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    private function createSendBird()
    {
        return new HttpClient($this->client);
    }
}

function config($key)
{
    return $key;
}
