<?php
declare(strict_types=1);

namespace App\Libraries;

use App\Test\MamiKosTestCase;

final class YouTubeLibraryTest extends MamikosTestCase
{
    ///////////////////////////////////////////////////////////////////
    // SMSLibrary::validateDomesticIndonesianMobileNumber
    ///////////////////////////////////////////////////////////////////
    
    public function testExtractVideoIdFromReturnsVideoIdIfUrlIsValid(): void
    {
        $this->assertEquals('o599gAG5tQs', YouTubeLibrary::extractVideoIdFrom('https://www.youtube.com/watch?v=o599gAG5tQs'), "extractVideoIdFrom('https://www.youtube.com/watch?v=o599gAG5tQs') did not extract well");
        $this->assertEquals('QIdB4sxhsrA', YouTubeLibrary::extractVideoIdFrom('https://www.youtube.com/watch?v=QIdB4sxhsrA&t=275s'), "extractVideoIdFrom('https://www.youtube.com/watch?v=QIdB4sxhsrA&t=275s') did not extract well");
        $this->assertEquals('K28Pw3IlCWY', YouTubeLibrary::extractVideoIdFrom('https://youtu.be/K28Pw3IlCWY'), "extractVideoIdFrom('https://youtu.be/K28Pw3IlCWY') did not extract well");
    }

    public function testExtractVideoIdFromReturnsNullIfUrlIsNotValid(): void
    {
        $this->assertNull(YouTubeLibrary::extractVideoIdFrom(NULL), "extractVideoIdFrom(NULL) didn't return null");
        $this->assertNull(YouTubeLibrary::extractVideoIdFrom(''), "extractVideoIdFrom('') didn't return null");
        $this->assertNull(YouTubeLibrary::extractVideoIdFrom('https://matterport.com/K28Pw3IlCWY'), "extractVideoIdFrom('https://matterport.com/K28Pw3IlCWY') didn't return null");
    }
}