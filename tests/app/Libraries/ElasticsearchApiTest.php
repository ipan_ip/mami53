<?php

namespace App\Libraries;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class ElasticsearchApiTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $elasticsearch;
    protected $sut;
    protected $roomIndex;

    protected function setUp(): void
    {
        parent::setUp();

        $this->elasticsearch = $this->mock('alias:Elasticsearch');
        $this->sut = new ElasticsearchApi();
        $this->roomIndex = Config::get('elasticsearch.index.room', 'mamisearch-room');
    }

    public function testGet(): void
    {
        $this->elasticsearch
            ->shouldReceive('get')
            ->with([
                'index' => 'mamisearch-room',
                'id' => 5
            ])
            ->once();

        $this->sut->get('room', 5);
    }

    public function testAdd(): void
    {
        $kost = factory(Room::class)->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => null,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testAddWithUniqueCode(): void
    {
        $kost = factory(Room::class)->state('with-unique-code')->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => $kost->unique_code->code,
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => null,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testAddWithOwner(): void
    {
        $kost = factory(Room::class)->state('with-owner')->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => $kost->owners->first()->status,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testAddWithMamipayOwner(): void
    {
        $kost = factory(Room::class)->state('with-mamipay-owner')->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => true,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => $kost->owners->first()->status,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testAddWithKostLevel(): void
    {
        $kost = factory(Room::class)->state('with-kost-level')->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => null,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => $kost->level->first()->id,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testAddWithConsultant(): void
    {
        $kost = factory(Room::class)->state('with-consultant')->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => null,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => [$kost->consultant_rooms->first()->consultant_id],
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->add('room', $kost);
    }

    public function testUpdate(): void
    {
        $kost = factory(Room::class)->create(['apartment_project_id' => null, 'gender' => 0]);

        $params = [
            'index' => $this->roomIndex,
            'id' => $kost->id,
            'body' => [
                'room_id' => $kost->id,
                'apartment_id' => null,
                'title' => ucwords($kost->name),
                'slug' => $kost->slug,
                'array_gender' => $kost->gender,
                'gender' => 'putri',
                'type' => 'kos',
                'code' => '',
                'song_id' => $kost->song_id,
                'is_mamipay_active' => false,
                'room_available' => $kost->room_available,
                'room_count' => $kost->room_count,
                'owner_status' => null,
                'is_booking' => ($kost->is_booking === 1),
                'is_promoted' => ($kost->is_promoted === 'true'),
                'kost_level_id' => null,
                'consultant_ids' => null,
                'updated_at' => $kost->updated_at->toDateTimeString()
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->once();

        $this->sut->update('room', $kost);
    }

    public function testDelete(): void
    {
        $kost = factory(Room::class)->create();

        $this->elasticsearch
            ->shouldReceive('delete')
            ->with([
                'index' => $this->roomIndex,
                'id' => $kost->id
            ])
            ->once();

        $this->sut->delete('room', $kost->id);
    }

    public function testSearch(): void
    {
        $this->elasticsearch
            ->shouldReceive('search')
            ->with([
                'index' => $this->roomIndex,
                'body' => [
                    'query' => [
                        'match_all' => []
                    ]
                ]
            ])
            ->once();

        $this->sut->search('room', '{ "query": {"match_all": {}} }');
    }
}
