<?php
declare(strict_types=1);

namespace App\Libraries;

use App\Test\MamiKosTestCase;

final class SMSLibraryTest extends MamikosTestCase
{
    ///////////////////////////////////////////////////////////////////
    // SMSLibrary::validateDomesticIndonesianMobileNumber
    ///////////////////////////////////////////////////////////////////
    
    public function testValidateDomesticIndonesianMobileNumberReturnsTrueIfMobileNumberIsValid(): void
    {
        // 10 digit
        $this->assertTrue(SMSLibrary::validateDomesticIndonesianMobileNumber('0834567890'), "validateDomesticIndonesianMobileNumber('0834567890') did not return true");

        // 11 digit
        $this->assertTrue(SMSLibrary::validateDomesticIndonesianMobileNumber('08345678901'), "validateDomesticIndonesianMobileNumber('08345678901') did not return true");

        // 12 digit
        $this->assertTrue(SMSLibrary::validateDomesticIndonesianMobileNumber('083456789012'), "validateDomesticIndonesianMobileNumber('083456789012') did not return true");
    
        // 13 digit
        $this->assertTrue(SMSLibrary::validateDomesticIndonesianMobileNumber('0834567890123'), "validateDomesticIndonesianMobileNumber('0834567890123') did not return true");
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberIsNullOrEmpty(): void
    {
        // null
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber(null), "validateDomesticIndonesianMobileNumber(null) did not return false");

        // empty string
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber(''), "validateDomesticIndonesianMobileNumber('') did not return false");        
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberIsInvalid(): void
    {
        // 10 digit but invalid (just starts with 8)
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('8234567890'), "validateDomesticIndonesianMobileNumber('8234567890') did not return false");

        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('1234567890'), "validateDomesticIndonesianMobileNumber('1234567890') did not return false");

        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('0234567890'), "validateDomesticIndonesianMobileNumber('0234567890') did not return false");
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooShort(): void
    {
        $fullMobileNumber = "0834567890";

        // from 1 to 9 digit
        for ($x = 1; $x < 10; $x++) {
            $temp = substr($fullMobileNumber, 0, $x);
            $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber($temp), "validateDomesticIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooLong(): void
    {
        $seedMobileNumber = "0834567890123";
        $temp = $seedMobileNumber;

        // add 4 to 9 more number
        for ($x = 4; $x < 10; $x++) {
            $temp .= $x;
            $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber($temp), "validateDomesticIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberIsInternationalFormat(): void
    {
        // Indonesian Mobile Number with International Format
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('+628134567890'), "validateDomesticIndonesianMobileNumber('+628134567890') did not return false");

        // Korean Mobile Number with International Format
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('+828134567890'), "validateDomesticIndonesianMobileNumber('+828134567890') did not return false");
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberStartsWith08ButIncludeAlphabet(): void
    {
        // Starts with '08' but include alphabets
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('08abcdefgh'), "validateDomesticIndonesianMobileNumber('08abcdefgh') did not return false");
    }

    ///////////////////////////////////////////////////////////////////
    // SMSLibrary::validateIndonesianMobileNumber
    ///////////////////////////////////////////////////////////////////

    public function testValidateIndonesianMobileNumberReturnsTrueIfMobileNumberIsValid(): void
    {
        // 10 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('0834567890'), "validateIndonesianMobileNumber('0834567890') did not return true");

        // 11 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('08345678901'), "validateIndonesianMobileNumber('08345678901') did not return true");

        // 12 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('083456789012'), "validateIndonesianMobileNumber('083456789012') did not return true");
    
        // 13 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('0834567890123'), "validateIndonesianMobileNumber('0834567890123') did not return true");
    }

    public function testValidateIndonesianMobileNumberReturnsTrueIfMobileNumberIsValidInternationalFormat(): void
    {
        // 10 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('+62834567890'), "validateIndonesianMobileNumber('+62834567890') did not return true");

        // 11 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('+628345678901'), "validateIndonesianMobileNumber('+628345678901') did not return true");

        // 12 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('+6283456789012'), "validateIndonesianMobileNumber('+6283456789012') did not return true");
    
        // 13 digit
        $this->assertTrue(SMSLibrary::validateIndonesianMobileNumber('+62834567890123'), "validateIndonesianMobileNumber('+62834567890123') did not return true");
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsNullOrEmpty(): void
    {
        // null
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber(null), "validateIndonesianMobileNumber(null) did not return false");

        // empty string
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber(''), "validateIndonesianMobileNumber('') did not return false");        
    }

    public function testValidateDomesticIndonesianMobileNumberReturnsFalseIfMobileNumberHasHyphens(): void
    {
        // 10 digits with format of 08*-***-**** 
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('081-456-7890'), "validateDomesticIndonesianMobileNumber('081-456-7890') did not return false");       

        // 11 digits with format of 08**-***-**** 
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('0812-456-7890'), "validateDomesticIndonesianMobileNumber('0812-456-7890') did not return false");       

        // 12 digits with format of 08***-***-**** 
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('08123-456-7890'), "validateDomesticIndonesianMobileNumber('08123-456-7890') did not return false");       

        // 13 digits with format of 08****-***-**** 
        $this->assertFalse(SMSLibrary::validateDomesticIndonesianMobileNumber('081234-456-7890'), "validateDomesticIndonesianMobileNumber('081234-456-7890') did not return false");       
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsInvalid(): void
    {
        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('1234567890'), "validateIndonesianMobileNumber('1234567890') did not return false");

        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('0234567890'), "validateIndonesianMobileNumber('0234567890') did not return false");
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsInvalidInternationalFormat(): void
    {
        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('+621234567890'), "validateIndonesianMobileNumber('+621234567890') did not return false");

        // 10 digit but invalid (not starts with 08)
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('+620234567890'), "validateIndonesianMobileNumber('+620234567890') did not return false");
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooShort(): void
    {
        $fullMobileNumber = "0834567890";

        // from 1 to 9 digit
        for ($x = 1; $x < 10; $x++) {
            $temp = substr($fullMobileNumber, 0, $x);
            $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber($temp), "validateIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooShortInternationalFormat(): void
    {
        $fullMobileNumber = "0834567890";

        // from 1 to 8 digit
        for ($x = 1; $x < 9; $x++) {
            $temp = "+62" . substr($fullMobileNumber, 1, $x);
            $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber($temp), "validateIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooLong(): void
    {
        $seedMobileNumber = "0834567890123";
        $temp = $seedMobileNumber;

        // add 4 to 9 more number
        for ($x = 4; $x < 10; $x++) {
            $temp .= $x;
            $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber($temp), "validateIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsTooLongInternationalFormat(): void
    {
        // already full length
        $seedMobileNumber = "+62834567890123";
        $temp = $seedMobileNumber;

        // add 4 to 9 more number
        for ($x = 4; $x < 10; $x++) {
            $temp .= $x;
            $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber($temp), "validateIndonesianMobileNumber('" . $temp . "') did not return false");
        }
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberIsNotIndonesianInternationalFormat(): void
    {
        // Korean Mobile Number with International Format
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('+828134567890'), "validateIndonesianMobileNumber('+828134567890') did not return false");
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberStartsWith08ButIncludeAlphabet(): void
    {
        // Starts with '08' but include alphabets
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('08abcdefgh'), "validateIndonesianMobileNumber('08abcdefgh') did not return false");
    }

    public function testValidateIndonesianMobileNumberReturnsFalseIfMobileNumberStartsWith628ButIncludeAlphabet(): void
    {
        // Starts with '+628' but include alphabets
        $this->assertFalse(SMSLibrary::validateIndonesianMobileNumber('+628abcdefgh'), "validateIndonesianMobileNumber('+628abcdefgh') did not return false");
    }

    ///////////////////////////////////////////////////////////////////
    // SMSLibrary::containsCriticalRejectedStatus
    ///////////////////////////////////////////////////////////////////
    
    public function testContainsCriticalRejectedStatusReturnsTrueIfStatusIdIsCritical(): void
    {
        $this->assertTrue(SMSLibrary::containsCriticalRejectedStatus(SMSLibrary::INFOBIP_STATUS_ID_REJECTED_PREFIX_MISSING));
        $this->assertTrue(SMSLibrary::containsCriticalRejectedStatus(SMSLibrary::INFOBIP_STATUS_ID_REJECTED_SENDER));
        $this->assertTrue(SMSLibrary::containsCriticalRejectedStatus(SMSLibrary::INFOBIP_STATUS_ID_REJECTED_DESTINATION));
    }

    public function testContainsCriticalRejectedStatusReturnsFalseIfStatusIdIsNotCritical(): void
    {
        $this->assertFalse(SMSLibrary::containsCriticalRejectedStatus(SMSLibrary::INFOBIP_STATUS_ID_REJECTED_NETWORK));
    }
}
