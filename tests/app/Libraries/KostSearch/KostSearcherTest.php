<?php

namespace App\Libraries\KostSearch;

use Illuminate\Http\Request;
use Config;

use App\Test\MamiKosTestCase;
use App\Libraries\KostSearch\KostSearchSorting;
use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\User;

class KostSearcherTest extends MamiKosTestCase
{
    const WEBAPI_PATH_LISTING_KOST = '/garuda/stories/list';
    const APPAPI_PATH_LISTING_KOST = '/api/v2/stories/list';

    public function testCreateSortingFromRequestWhenSortByRecommendation()
    {
        $request = new Request();
        $postBody = [ 
            'sorting' => [
                'field' => 'price',
                'direction' => '-'
            ]
        ];
        $request->replace($postBody);
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByRecommendation));
    }

    public function testCreateSortingFromRequestWhenSortByPriceAscending()
    {
        $request = new Request();
        $postBody = [ 
            'sorting' => [
                'field' => 'price',
                'direction' => 'asc'
            ]
        ];
        $request->replace($postBody);
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByPriceAscending));
    }

    public function testCreateSortingFromRequestWhenSortByPriceDescending()
    {
        $request = new Request();
        $postBody = [ 
            'sorting' => [
                'field' => 'price',
                'direction' => 'desc'
            ]
        ];
        $request->replace($postBody);
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByPriceDescending));
    }

    public function testCreateSortingFromRequestShouldCreateSortByRecommendationAsFallback()
    {
        // no sorting data
        $request = new Request();
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByRecommendation));

        // null
        $request = new Request();
        $postBody = [ 
            'sorting' => null
        ];
        $request->replace($postBody);
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByRecommendation));

        // empty array
        $request = new Request();
        $postBody = [ 
            'sorting' => []
        ];
        $request->replace($postBody);
        $sorting = KostSearcher::createSortingFromRequest($request);
        
        $this->assertTrue($sorting->is(KostSearchSorting::SortByRecommendation));
    }

    public function testGetOffsetFromRequest()
    {
        $request = new Request();
        $postBody = [ 
            'offset' => 12
        ];
        $request->replace($postBody);
        $offset = KostSearcher::getOffsetFromRequest($request);
        $this->assertEquals(12, $offset);
    }

    public function testGetOffsetFromRequestShouldReturnDefaultValueAsFallback()
    {
        $request = new Request();
        $offset = KostSearcher::getOffsetFromRequest($request);
        $this->assertEquals(KostSearcher::DEFAULT_OFFSET, $offset);

        $request = new Request();
        $postBody = [ 
            'offset' => null
        ];
        $request->replace($postBody);
        $offset = KostSearcher::getOffsetFromRequest($request);
        $this->assertEquals(KostSearcher::DEFAULT_OFFSET, $offset);
    }

    public function testGetOffsetFromRequestShouldReturnMinValueWhenNegative()
    {

        $request = new Request();
        $postBody = [ 
            'offset' => -1
        ];
        $request->replace($postBody);
        $offset = KostSearcher::getOffsetFromRequest($request);
        $this->assertEquals(KostSearcher::MIN_OFFSET, $offset);
    }

    public function testGetLimitFromRequest()
    {
        $request = new Request();
        $postBody = [ 
            'limit' => 15
        ];
        $request->replace($postBody);
        $limit = KostSearcher::getLimitFromRequest($request);
        $this->assertEquals(15, $limit);
    }

    public function testGetLimitFromRequestShouldReturnDefaultValueAsFallback()
    {
        $request = new Request();
        $limit = KostSearcher::getLimitFromRequest($request);
        $this->assertEquals(KostSearcher::DEFAULT_LIMIT, $limit);

        $request = new Request();
        $postBody = [ 
            'limit' => null
        ];
        $request->replace($postBody);
        $limit = KostSearcher::getLimitFromRequest($request);
        $this->assertEquals(KostSearcher::DEFAULT_LIMIT, $limit);
    }

    public function testGetLimitFromRequestShouldReturnMinValueWhenNegative()
    {
        $request = new Request();
        $postBody = [ 
            'limit' => -1
        ];
        $request->replace($postBody);
        $limit = KostSearcher::getLimitFromRequest($request);
        $this->assertEquals(KostSearcher::MIN_LIMIT, $limit);
    }

    public function testCreateFilterFromRequestWithPriceFilter()
    {
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'rent_type' => 1, // weekly
                'price_range' => [
                    10000,
                    20000000
                ]
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getPriceFilterType()->is(PropertyPricePeriod::Weekly));
        $this->assertEquals(10000, $filter->getPriceRangeMin());
        $this->assertEquals(20000000, $filter->getPriceRangeMax());
    }

    public function testCreateFilterFromRequestWithGenders()
    {
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'gender' => [0, 2]
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $genders = $filter->getGenders();
        $this->assertContains(PropertyGender::Mixed, $genders);
        $this->assertContains(PropertyGender::Female, $genders);
    }

    public function testCreateFilterFromRequestWithLocation()
    {
        $left = 106.81581974029542;
        $right = 106.84019565582277;
        $top = -6.17430498224146;
        $bottom = -6.186678095092927;

        $request = new Request();
        $postBody = [ 
            'location' => [
                [
                    $left,
                    $bottom
                ],
                [
                    $right,
                    $top
                ]
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $searchRectangle = $filter->getSearchRectangle();
        $this->assertEquals($left, $searchRectangle->left);
        $this->assertEquals($top, $searchRectangle->top);
        $this->assertEquals($right, $searchRectangle->right);
        $this->assertEquals($bottom, $searchRectangle->bottom);
    }

    public function testCreateFilterFromRequestWithOnlyCanBooking()
    {
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'booking' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyCanBooking());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'booking' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyCanBooking());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_can_booking' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyCanBooking());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_can_booking' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyCanBooking());

        // only_mamichecked (true) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'booking' => false,
                'only_can_booking' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyCanBooking());

        // only_mamichecked (false) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'booking' => true,
                'only_can_booking' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyCanBooking());
    }

    public function testCreateFilterFromRequestWithOnlyMamichecked()
    {
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamichecker' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamichecked());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamichecker' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamichecked());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_mamichecked' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamichecked());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_mamichecked' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamichecked());

        // only_mamichecked (true) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamichecker' => false,
                'only_mamichecked' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamichecked());

        // only_mamichecked (false) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamichecker' => true,
                'only_mamichecked' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamichecked());
    }

    public function testCreateFilterFromRequestWithOnlyMamiroom()
    {
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamirooms' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamiroom());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamirooms' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamiroom());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_mamiroom' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamiroom());

        $request = new Request();
        $postBody = [ 
            'filters' => [
                'only_mamiroom' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamiroom());

        // only_mamiroom (true) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamirooms' => false,
                'only_mamiroom' => true
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertTrue($filter->getOnlyMamiroom());

        // only_mamiroom (false) has the higher priority 
        $request = new Request();
        $postBody = [ 
            'filters' => [
                'mamirooms' => true,
                'only_mamiroom' => false
            ]
        ];
        $request->replace($postBody);
        $filter = KostSearcher::createFilterFromRequest($request);

        $this->assertFalse($filter->getOnlyMamiroom());
    }

    public function testShouldRerouteShouldReturnFalseWhenNotEnabled()
    {
        $users = [];
        for ($i = 0; $i < 10; $i++)
        {
            $users[] = Factory(User::class)->create([ 'id' => 1000 + $i]);
        }
            
        Config::set('kostsearcher.beta.reroute_enabled', false);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[]');
        Config::set('kostsearcher.beta.allow_tester_only', false);
        
        foreach ($users as $user)
        {
            $this->assertFalse(KostSearcher::shouldReroute($user));
        }

        Config::set('kostsearcher.beta.reroute_user_id_endings', '[1]');

        foreach ($users as $user)
        {
            $this->assertFalse(KostSearcher::shouldReroute($user));
        }

        Config::set('kostsearcher.beta.reroute_user_id_endings', null);
        
        foreach ($users as $user)
        {
            $this->assertFalse(KostSearcher::shouldReroute($user));
        }
    }

    public function testShouldRerouteShouldReturnTrueWhenEnabled()
    {
        $users = [];
        for ($i = 0; $i < 10; $i++)
        {
            $users[] = Factory(User::class)->create([ 'id' => 1000 + $i]);
        }

        Config::set('kostsearcher.beta.reroute_enabled', true);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[]');
        Config::set('kostsearcher.beta.allow_tester_only', false);
        
        foreach ($users as $user)
        {
            $this->assertTrue(KostSearcher::shouldReroute($user));
        }

        // it should return true selectively
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[3,7]');
        foreach ($users as $user)
        {
            $rest = $user->id % 10;
            if ($rest === 3 || $rest === 7)
            {
                $this->assertTrue(KostSearcher::shouldReroute($user));
            }
            else
            {
                $this->assertFalse(KostSearcher::shouldReroute($user));
            }
        }
    }

    public function testShouldRerouteForTesters()
    {
        Config::set('kostsearcher.beta.reroute_enabled', true);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[]');
        Config::set('kostsearcher.beta.allow_tester_only', true);

        $tester = Factory(User::class)->create([ 'is_tester' => true]);
        $this->assertTrue(KostSearcher::shouldReroute($tester));

        Config::set('kostsearcher.beta.reroute_enabled', false);
        $this->assertFalse(KostSearcher::shouldReroute($tester));
    }

    public function testShouldRerouteForAnonymous()
    {
        Config::set('kostsearcher.beta.reroute_enabled', true);
        Config::set('kostsearcher.beta.allow_anonymous', true);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[1]');
        Config::set('kostsearcher.beta.allow_tester_only', false);

        $user = null; // in case of anonymous user
        $this->assertTrue(KostSearcher::shouldReroute($user));

        Config::set('kostsearcher.beta.reroute_enabled', false);
        $this->assertFalse(KostSearcher::shouldReroute($user));
    }

    public function testShouldRerouteShouldIgnoreUserIdEndingsWhenAnonymousAllowed()
    {
        Config::set('kostsearcher.beta.reroute_enabled', true);
        Config::set('kostsearcher.beta.allow_anonymous', true);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[1]');
        Config::set('kostsearcher.beta.allow_tester_only', false);

        $user = Factory(User::class)->create([ 'id' => 11, 'is_tester' => false]);
        $this->assertTrue(KostSearcher::shouldReroute($user));

        $user = Factory(User::class)->create([ 'id' => 21, 'is_tester' => true]);
        $this->assertTrue(KostSearcher::shouldReroute($user));
    }

    public function testShouldRerouteShouldIgnoreTesterOnlyWhenAnonymousAllowed()
    {
        Config::set('kostsearcher.beta.reroute_enabled', true);
        Config::set('kostsearcher.beta.allow_anonymous', true);
        Config::set('kostsearcher.beta.reroute_user_id_endings', '[]'); // allow all
        Config::set('kostsearcher.beta.allow_tester_only', true);

        $user = Factory(User::class)->create([ 'id' => 11, 'is_tester' => false]);
        $this->assertTrue(KostSearcher::shouldReroute($user));
    }

    public function testCanSupportShouldReturnTrueWhenPathIsWebApi()
    {
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPathIsNonWebApi()
    {
        $request = Request::create(self::APPAPI_PATH_LISTING_KOST);
        $this->assertFalse(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenFlashSaleIsTrue()
    {
        $requestBody = [
            'filters' => [
                'flash_sale' => true
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'flash_sale' => false
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenLandingsNotEmpty()
    {
        $requestBody = [
            'filters' => [
                'landings' => [
                    "promo-ngebut-abepura",
                    "promo-ngebut-abepura"
                ]
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'landings' => []
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'landings' => null
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPlaceNotEmpty()
    {
        $requestBody = [
            'filters' => [
                'place' => [
                    "Jakarta Pusat"
                ]
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'place' => []
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'place' => null
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPromotedOnly()
    {
        $requestBody = [
            'include_promoted' => true
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'include_promoted' => false
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPinnedOnly()
    {
        $requestBody = [
            'include_pinned' => true
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'include_pinned' => false
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPromotionActivatedOnly()
    {
        $requestBody = [
            'filters' => [
                'promotion' => true
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'promotion' => false
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenPropertyTypeIsApartment()
    {
        $requestBody = [
            'filters' => [
                'property_type' => 'apartment'
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'property_type' => 'kost'
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'property_type' => 'all'
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));

        $requestBody = [
            'filters' => [
                'property_type' => null
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }

    public function testCanSupportShouldReturnFalseWhenItHasPoint()
    {
        $requestBody = [
            'point' => [
                'latitude' => '-6.1960149285714',
                'longitude' => '106.8214845'
            ]
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertFalse(KostSearcher::canSupport($request));

        $requestBody = [
            'point' => null
        ];
        $request = Request::create(self::WEBAPI_PATH_LISTING_KOST, 'POST', $requestBody);
        $this->assertTrue(KostSearcher::canSupport($request));
    }
}