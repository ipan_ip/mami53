<?php

namespace App\Libraries\KostSearch;

use OutOfBoundsException;
use OutOfRangeException;
use Exception;

use App\Test\MamiKosTestCase;
use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\Geospatial\GeoRectangle;

class KostSearchFilterTest extends MamiKosTestCase
{
    protected $withDbTransaction = false;

    public function testSetGetOnlyMamiroomShouldSucceed()
    {
        $filter = new KostSearchFilter();

        $filter->setOnlyMamiroom(false);
        $this->assertEquals(false, $filter->getOnlyMamiroom());

        $filter->setOnlyMamiroom(true);
        $this->assertEquals(true, $filter->getOnlyMamiroom());
    }

    public function testSetGetOnlyGoldplusShouldSucceed()
    {
        $filter = new KostSearchFilter();
        
        $filter->setOnlyGoldplus(false);
        $this->assertEquals(false, $filter->getOnlyGoldplus());

        $filter->setOnlyGoldplus(true);
        $this->assertEquals(true, $filter->getOnlyGoldplus());
    }

    public function testSetGetOnlyCanBookingShouldSucceed()
    {
        $filter = new KostSearchFilter();
        
        $filter->setOnlyCanBooking(false);
        $this->assertEquals(false, $filter->getOnlyCanBooking());

        $filter->setOnlyCanBooking(true);
        $this->assertEquals(true, $filter->getOnlyCanBooking());
    }

    public function testSetGetOnlyMamicheckedShouldSucceed()
    {
        $filter = new KostSearchFilter();
        
        $filter->setOnlyMamichecked(false);
        $this->assertEquals(false, $filter->getOnlyMamichecked());

        $filter->setOnlyMamichecked(true);
        $this->assertEquals(true, $filter->getOnlyMamichecked());
    }

    public function testSetGetExcludeTestingShouldSucceed()
    {
        $filter = new KostSearchFilter();
        
        $filter->setExcludingTesting(false);
        $this->assertEquals(false, $filter->getExcludingTesting());

        $filter->setExcludingTesting(true);
        $this->assertEquals(true, $filter->getExcludingTesting());
    }

    public function testSetGetPriceFilterShouldSucceed()
    {
        $filter = new KostSearchFilter();
        
        $filter->setPriceFilter(PropertyPricePeriod::None(), 1, 2);
        $this->assertEquals(PropertyPricePeriod::None(), $filter->getPriceFilterType());
        $this->assertEquals(0, $filter->getPriceRangeMin());
        $this->assertEquals(0, $filter->getPriceRangeMax());

        $filter->setPriceFilter(PropertyPricePeriod::Daily(), 1, 2);
        $this->assertEquals(PropertyPricePeriod::Daily(), $filter->getPriceFilterType());
        $this->assertEquals(1, $filter->getPriceRangeMin());
        $this->assertEquals(2, $filter->getPriceRangeMax());

        $filter->setPriceFilter(PropertyPricePeriod::Monthly(), 3, 4);
        $this->assertEquals(PropertyPricePeriod::Monthly(), $filter->getPriceFilterType());
        $this->assertEquals(3, $filter->getPriceRangeMin());
        $this->assertEquals(4, $filter->getPriceRangeMax());
    }

    public function testSetPriceFilterWithNegativePriceForPriceRangeMinShouldThrowException()
    {
        $filter = new KostSearchFilter();

        $this->expectException(OutOfBoundsException::class);
        $filter->setPriceFilter(PropertyPricePeriod::Monthly(), -1, 1);
    }

    public function testSetPriceFilterWithNegativePriceForPriceRangeMaxShouldThrowException()
    {
        $filter = new KostSearchFilter();

        $this->expectException(OutOfBoundsException::class);
        $filter->setPriceFilter(PropertyPricePeriod::Monthly(), 1, -1);
    }

    public function testSetPriceFilterWithReversedPricesForPriceRangeBetweenMinAndMaxShouldThrowException()
    {
        $filter = new KostSearchFilter();

        $this->expectException(Exception::class);
        $filter->setPriceFilter(PropertyPricePeriod::Monthly(), 10, 5);
    }

    public function testSetPriceFilterWithPriceTypeNoneShouldNotThrowAnyException()
    {
        $filter = new KostSearchFilter();
        
        // negative price for min
        $filter->setPriceFilter(PropertyPricePeriod::None(), -1, 1);

        // negative price for max
        $filter->setPriceFilter(PropertyPricePeriod::None(), 1, -1);

        // reversed prices
        $filter->setPriceFilter(PropertyPricePeriod::None(), 10, 5);
    }

    public function testSetGetGendersShouldSucceed()
    {
        $filter = new KostSearchFilter();
        $filter->setGenders([]);
        $this->assertEquals([], $filter->getGenders());

        // all gender means there is no gender filtering
        $filter->setGenders([0, 1, 2]);
        $this->assertEquals([], $filter->getGenders());

        $filter->setGenders([0]);
        $this->assertEquals([PropertyGender::Mixed], $filter->getGenders());

        $filter->setGenders([1]);
        $this->assertEquals([PropertyGender::Male], $filter->getGenders());

        $filter->setGenders([2]);
        $this->assertEquals([PropertyGender::Female], $filter->getGenders());

        $filter->setGenders([0, 1]);
        $this->assertEquals([PropertyGender::Mixed, PropertyGender::Male], $filter->getGenders());

        $filter->setGenders([1, 2]);
        $this->assertEquals([PropertyGender::Male, PropertyGender::Female], $filter->getGenders());

        $filter->setGenders([0, 2]);
        $this->assertEquals([PropertyGender::Mixed, PropertyGender::Female], $filter->getGenders());
    }

    public function testSetGetGendersShouldSucceedEvenIfNotSorted()
    {
        $filter = new KostSearchFilter();

        // all gender means there is no gender filtering
        $filter->setGenders([1, 2, 0]);
        $this->assertEquals([], $filter->getGenders());

        $filter->setGenders([1, 0]);
        $this->assertEquals([PropertyGender::Mixed, PropertyGender::Male], $filter->getGenders());

        $filter->setGenders([2, 1]);
        $this->assertEquals([PropertyGender::Male, PropertyGender::Female], $filter->getGenders());

        $filter->setGenders([2, 0]);
        $this->assertEquals([PropertyGender::Mixed, PropertyGender::Female], $filter->getGenders());
    }

    public function testSetGendersShouldThrowExceptionWhenInputInvalidValue()
    {
        $filter = new KostSearchFilter();

        $this->expectException(OutOfRangeException::class);
        $filter->setGenders([0, 3]);
    }

    public function testSetGetTagsShouldSucceed()
    {
        $filter = new KostSearchFilter();
        $filter->setTags([]);
        $this->assertEquals([], $filter->getTags());

        $filter->setTags([1]);
        $this->assertEquals([1], $filter->getTags());

        $filter->setTags([2, 1, 10, 12]);
        $this->assertEquals([1, 2, 10 ,12], $filter->getTags());
    }

    public function testSetTagsShouldMergeDuplicatedTags()
    {
        $filter = new KostSearchFilter();

        $filter->setTags([1, 1]);
        $this->assertEquals([1], $filter->getTags());

        $filter->setTags([1, 1, 1]);
        $this->assertEquals([1], $filter->getTags());

        $filter->setTags([1, 2, 2, 10, 11, 100, 100, 101]);
        $this->assertEquals([1, 2, 10, 11, 100, 101], $filter->getTags());
    }

    public function testSetGetSearchRectangleShouldSucceed()
    {
        $filter = new KostSearchFilter();

        // set null
        $filter->setSearchRectangle(null);
        $this->assertNull($filter->getSearchRectangle());

        // set valid data
        $rect = new GeoRectangle(106.80934539999998, -6.2003444, 106.85934539999998, -6.2503444);
        $filter->setSearchRectangle($rect);
        $result = $filter->getSearchRectangle();
        $this->assertEquals($rect, $result);
    }

    public function testSetGetKostIdsShouldSucceed()
    {
        $filter = new KostSearchFilter();
        $ids = [1,2,3];
        $filter->setKostIds($ids);
        $this->assertEquals($ids, $filter->getKostIds());
    }

}