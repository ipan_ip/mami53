<?php

namespace App\Libraries\KostSearch;

use Google\Cloud\Language\V1beta2\PartOfSpeech\Proper;

use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Libraries\KostSearch\KostSearchSorting;
use App\Geospatial\GeoRectangle;
use Carbon\Carbon;
use Mockery;

class KostSearchElasticsearchClientTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->indexName = 'fake-kost-search-index';
        $this->elasticsearch = Mockery::mock('mockElasticsearch');
        $this->esClient = new KostSearchElasticsearchClient($this->indexName, $this->elasticsearch);
        Carbon::setTestNow(now());
    }

    public function testIndex(): void
    {
        $room = factory(Room::class)->create();
        factory(PriceFilter::class)->create([
            'designer_id' => $room
        ]);
        $params = [
            'index' => $this->indexName,
            'id' => $room->id,
            'body' => $room->elasticsearchData()
        ];
        $this->elasticsearch
            ->shouldReceive('index')
            ->with($params)
            ->andReturn(
                [
                    "_index" => "active-kosts",
                    "_type" => "_doc",
                    "_id" => "10",
                    "_version" => 6,
                    "result" => "updated",
                    "_shards" => [
                        "total" => 2,
                        "successful" => 1,
                        "failed" => 0,
                    ],
                    "_seq_no" => 41,
                    "_primary_term" => 1,
                ]
            );
        $result = $this->esClient->index($room);
        $this->assertTrue($result->success);
    }

    public function testBulkIndex(): void
    {
        $rooms = factory(Room::class, 3)
            ->create()
            ->each(function ($room) {
                factory(PriceFilter::class)->create([
                    'designer_id' => $room
                ]);
            });
        $params = ['body' => []];
        foreach ($rooms as $room) {
            $params['body'][] = [
                'index' => [
                    '_index' => $this->indexName,
                    '_id'    => $room->id
                ]
            ];

            $params['body'][] = $room->elasticsearchData();
        }
        $this->elasticsearch
            ->shouldReceive('bulk')
            ->with($params)
            ->andReturn(
                [
                    "took" => 7,
                    "errors" => false,
                    "items" => [
                        [
                            "index" => [
                                "_index" => "active-kosts",
                                "_type" => "_doc",
                                "_id" => "1",
                                "_version" => 5,
                                "result" => "updated",
                                "_shards" => [
                                    "total" => 2,
                                    "successful" => 1,
                                    "failed" => 0,
                                ],
                                "_seq_no" => 42,
                                "_primary_term" => 1,
                                "status" => 200,
                            ],
                        ],
                        [
                            "index" => [
                                "_index" => "active-kosts",
                                "_type" => "_doc",
                                "_id" => "2",
                                "_version" => 5,
                                "result" => "updated",
                                "_shards" => [
                                    "total" => 2,
                                    "successful" => 1,
                                    "failed" => 0,
                                ],
                                "_seq_no" => 43,
                                "_primary_term" => 1,
                                "status" => 200,
                            ],
                        ],
                        [
                            "index" => [
                                "_index" => "active-kosts",
                                "_type" => "_doc",
                                "_id" => "3",
                                "_version" => 5,
                                "result" => "updated",
                                "_shards" => [
                                    "total" => 2,
                                    "successful" => 1,
                                    "failed" => 0,
                                ],
                                "_seq_no" => 44,
                                "_primary_term" => 1,
                                "status" => 200,
                            ],
                        ],
                    ],
                ]
            );
        $result = $this->esClient->bulkIndex($rooms);
        $this->assertTrue($result[1]->success);
        $this->assertTrue($result[2]->success);
        $this->assertTrue($result[3]->success);
    }

    public function testDelete(): void
    {
        $room = factory(Room::class)->create();
        $params = [
            'index' => $this->indexName,
            'id' => $room->id,
        ];
        $this->elasticsearch
            ->shouldReceive('delete')
            ->with($params)
            ->andReturn(
                [
                    "_index" => "active-kosts",
                    "_type" => "_doc",
                    "_id" => "1",
                    "_version" => 6,
                    "result" => "deleted",
                    "_shards" => [
                        "total" => 2,
                        "successful" => 1,
                        "failed" => 0,
                    ],
                    "_seq_no" => 52,
                    "_primary_term" => 1,
                ]
            );
        $result = $this->esClient->delete($room->id);
        $this->assertTrue($result->success);
    }

    public function testSearchByKeyword(): void
    {
        $this->elasticsearch
            ->shouldReceive('search')
            ->andReturn(
                [
                    "took" => 2,
                    "timed_out" => false,
                    "_shards" => [
                        "total" => 1,
                        "successful" => 1,
                        "skipped" => 0,
                        "failed" => 0,
                    ],
                    "hits" => [
                        "total" => [
                            "value" => 7,
                            "relation" => "eq",
                        ],
                        "max_score" => 4.333619,
                        "hits" => [
                            [
                                "_index" => "active-kosts",
                                "_type" => "_doc",
                                "_id" => "9",
                                "_score" => 4.333619,
                                "_source" => [
                                    "indexed_at" => "2021-01-22 17:01:42",
                                    "kost_id" => 9,
                                    "apartment_id" => null,
                                    "title" => "Kos Owner Survey",
                                    "slug" => "kost-jakarta-selatan-kost-campur-eksklusif-kos-owner-survey-1",
                                    "gender" => 0,
                                    "type" => "kos",
                                    "song_id" => 12459949,
                                    "available_room_count" => 11,
                                    "max_room_count" => 19,
                                    "can_booking" => false,          
                                    "is_mamichecked" => false,
                                    "is_mamiroom" => false,
                                    "is_promoted" => true,
                                    "is_testing" => false,
                                    "kost_updated_at" => "2021-01-19 13:55:42",
                                    "sort_score" => 1110100000,
                                    "location" => [
                                        "lat" => -7.75837,
                                        "lon" => 110.377781
                                    ],
                                    "can_rent_daily" => false,
                                    "can_rent_weekly" => false,
                                    "can_rent_monthly" => true,
                                    "can_rent_quarterly" => false,
                                    "can_rent_semiannually" => false,
                                    "can_rent_yearly" => true,
                                    "final_price_daily" => 0,
                                    "final_price_weekly" => 0,
                                    "final_price_monthly" => 2000000,
                                    "final_price_quarterly" => 0,
                                    "final_price_semiannually" => 0,
                                    "final_price_yearly" => 24000000,
                                    "tags" => [
                                        2,
                                        3,
                                        4,
                                        8,
                                        10,
                                        11,
                                        12,
                                        13
                                    ],
                                ],
                            ],
                            [
                                "_index" => "active-kosts",
                                "_type" => "_doc",
                                "_id" => "3",
                                "_score" => 3.9294567,
                                "_source" => [
                                    "indexed_at" => "2021-01-22 17:01:42",
                                    "kost_id" => 3,
                                    "apartment_id" => null,
                                    "title" => "Kos Ngitung Budget Owner",
                                    "slug" => "kost-jakarta-selatan-kost-campur-eksklusif-kos-ngitung-budget-owner-1",
                                    "gender" => 0,
                                    "type" => "kos",
                                    "song_id" => 61579850,
                                    "available_room_count" => 11,
                                    "max_room_count" => 15,
                                    "can_booking" => false,
                                    "is_mamichecked" => false,
                                    "is_mamiroom" => false,
                                    "is_promoted" => true,
                                    "is_testing" => false,
                                    "kost_updated_at" => "2021-01-19 13:55:42",
                                    "sort_score" => 0,
                                    "location" => [
                                        "lat" => -7.758377,
                                        "lon" => 110.377878
                                    ],
                                    "can_rent_daily" => false,
                                    "can_rent_weekly" => false,
                                    "can_rent_monthly" => true,
                                    "can_rent_quarterly" => false,
                                    "can_rent_semiannually" => false,
                                    "can_rent_yearly" => true,
                                    "final_price_daily" => 0,
                                    "final_price_weekly" => 0,
                                    "final_price_monthly" => 1600000,
                                    "final_price_quarterly" => 0,
                                    "final_price_semiannually" => 0,
                                    "final_price_yearly" => 19200000,
                                    "tags" => [
                                        1,
                                        3,
                                        4,
                                        8,
                                        10,
                                        11,
                                        12,
                                        13,
                                        15
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]

            );
        $result = $this->esClient->searchByKeyword('kos');
        $expected = [9, 3];
        $this->assertEquals($expected, $result);
    }

    public function testCreateIndex(): void
    {
        $params = [
            'index' => $this->indexName,
            'body' => [
                'settings' => [
                    'index' => [
                        'sort.field' => [ 'sort_score', 'kost_updated_at' ],
                        'sort.order' => [ 'desc', 'desc' ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        'sort_score' => [
                            'type' => 'long',
                        ],
                        'kost_updated_at' => [
                            'type' => 'date',
                        ],
                    ]
                ]
            ]
        ];
        $this->elasticsearch
            ->shouldReceive('indices')
            ->andReturn($this->elasticsearch);
        $this->elasticsearch->shouldReceive('create')
            ->with($params)
            ->andReturn([
                'acknowledged' => true
            ]);
        $result = $this->esClient->createIndex();
        $this->assertTrue($result);
    }

    public function testUpdateMapping(): void
    {
        $params = [
            'index' => $this->indexName,
            'body' => [
                'properties' => [
                    'location' => [
                        'type' => 'geo_point',
                    ],
                ]
            ]
        ];
        $this->elasticsearch
            ->shouldReceive('indices')
            ->andReturn($this->elasticsearch);
        $this->elasticsearch->shouldReceive('putMapping')
            ->with($params)
            ->andReturn([
                'acknowledged' => true
            ]);
        $result = $this->esClient->updateMapping();
        $this->assertTrue($result);
    }

    public function testUpdateRefreshInterval(): void
    {
        $inputSeconds = 5;
        $params = [
            'index' => $this->indexName,
            'body' => [
                'refresh_interval' => $inputSeconds . 's'
            ]
        ];
        $this->elasticsearch
            ->shouldReceive('indices')
            ->andReturn($this->elasticsearch);
        $this->elasticsearch->shouldReceive('putSettings')
            ->with($params)
            ->andReturn([
                'acknowledged' => true
            ]);
        $result = $this->esClient->updateRefreshInterval($inputSeconds);
        $this->assertTrue($result);
    }


    public function testGetKostIds()
    {
        $this->elasticsearch
            ->shouldReceive('search')
            ->andReturn(
                [
                    "took" => 4,
                    "timed_out" => false,
                    "_shards" => [
                        "total" => 1,
                        "successful" => 1,
                        "skipped" => 0,
                        "failed" => 0,
                    ],
                    "hits" => [
                        "total" => [
                            "value" => 29,
                            "relation" => "eq",
                        ],
                        "max_score" => null,
                        "hits" => [
                            [
                                "_id" => "205080",
                                "_source" => [
                                    "kost_id" => 205080
                                ],
                            ],
                            [
                                "_id" => "183886",
                                "_source" => [
                                    "kost_id" => 183886
                                ],
                            ]
                        ]
                    ]
                ]);

        $filter = new KostSearchFilter();

        $filter->setGenders([PropertyGender::Mixed, PropertyGender::Female]);

        $filter->setOnlyCanBooking(true);
        $filter->setOnlyGoldplus(true);
        $filter->setOnlyMamichecked(true);
        $filter->setOnlyMamiroom(true);
        $filter->setExcludingTesting(true);

        $filter->setTags([8, 12, 13, 15]);

        $filter->setPriceFilter(PropertyPricePeriod::Weekly(), 10000, 20000000);

        $filter->setSearchRectangle(new GeoRectangle(
            106.80286260141636, -6.1723343145242024, 
            106.84119701548268, -6.210698979549181)
        );
       
        $result = $this->esClient->getKostIds($filter, KostSearchSorting::SortByRecommendation());

        $this->assertEquals(29, $result->total);
        $this->assertEquals([205080, 183886], $result->items->toArray());
    }
}
