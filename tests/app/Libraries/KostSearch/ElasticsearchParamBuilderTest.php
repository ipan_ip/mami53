<?php

namespace App\Libraries\KostSearch;

use App\Test\MamiKosTestCase;
use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\Libraries\KostSearch\KostSearchSorting;
use App\Geospatial\GeoRectangle;

class ElasticsearchParamBuilderTest extends MamiKosTestCase
{
    public function testGetSortParamForSortByRecommendation()
    {
        $param = ElasticsearchParamBuilder::getSortParam(KostSearchSorting::SortByRecommendation(), PropertyPricePeriod::Yearly());
        $expected = [
            [ 'sort_score' => [ 'order' => 'desc' ] ],
            [ 'kost_updated_at' => [ 'order' => 'desc' ] ]
        ];

        $this->assertEquals($expected, $param);
    }

    public function testGetSortParamForSortByPriceAscending()
    {
        $param = ElasticsearchParamBuilder::getSortParam(KostSearchSorting::SortByPriceAscending(), PropertyPricePeriod::Quarterly());
        $expected = [
            [ 'final_price_quarterly' => [ 'order' => 'asc' ]],
            [ 'sort_score' => [ 'order' => 'desc' ] ],
            [ 'kost_updated_at' => [ 'order' => 'desc' ] ]
        ];

        $this->assertEquals($expected, $param);
    }

    public function testGetSortParamForSortByPriceDescending()
    {
        $param = ElasticsearchParamBuilder::getSortParam(KostSearchSorting::SortByPriceDescending(), PropertyPricePeriod::Semiannually());
        $expected = [
            [ 'final_price_semiannually' => [ 'order' => 'desc' ]],
            [ 'sort_score' => [ 'order' => 'desc' ] ],
            [ 'kost_updated_at' => [ 'order' => 'desc' ] ]
        ];

        $this->assertEquals($expected, $param);
    }

    public function testGetTermParam()
    {
        $param = ElasticsearchParamBuilder::getTermParam('my_key', true);
        $expected = [
            'term' => [
                'my_key' => true
            ]
        ];
        $this->assertEquals($expected, $param);

        $param = ElasticsearchParamBuilder::getTermParam('my_key', false);
        $expected = [
            'term' => [
                'my_key' => false
            ]
        ];
        $this->assertEquals($expected, $param);
    }

    public function testGetTermsParam()
    {
        $param = ElasticsearchParamBuilder::getTermsParam('my_key', []);
        $this->assertNull($param);

        $param = ElasticsearchParamBuilder::getTermsParam('my_key', [1]);
        $expected = [
            'terms' => [
                'my_key' => [1]
            ]
        ];
        $this->assertEquals($expected, $param);

        $param = ElasticsearchParamBuilder::getTermsParam('my_key', [1, 2]);
        $expected = [
            'terms' => [
                'my_key' => [1, 2]
            ]
        ];
        $this->assertEquals($expected, $param);
    }

    public function testGetTermsOrTermParam()
    {
        $param = ElasticsearchParamBuilder::getTermsOrTermParam('my_key', []);
        $this->assertNull($param);

        $param = ElasticsearchParamBuilder::getTermsOrTermParam('my_key', [1]);
        $expected = [
            'term' => [
                'my_key' => 1
            ]
        ];
        $this->assertEquals($expected, $param);

        $param = ElasticsearchParamBuilder::getTermsOrTermParam('my_key', [1, 2]);
        $expected = [
            'terms' => [
                'my_key' => [1, 2]
            ]
        ];
        $this->assertEquals($expected, $param);
    }

    public function testGetRangeParam()
    {
        $param = ElasticsearchParamBuilder::getRangeParamBothIncluded('my_key', 1000, 20000);
        $expected = [
            'range' => [
                'my_key' => [
                    'gte' => 1000,
                    'lte' => 20000
                ]
            ]
        ];
        $this->assertEquals($expected, $param);
    }

    public function testGetGeoBoundingBoxParam()
    {
        $param = ElasticsearchParamBuilder::getGeoBoundingBoxParam(new GeoRectangle(10, 40, 30, 20));
        $expected = [
            'geo_bounding_box' => [
                'location' => [
                    'top_left' => [
                        'lat' => 40,
                        'lon' => 10
                    ],
                    'bottom_right'=> [
                        'lat' => 20,
                        'lon' => 30
                    ]
                ]
            ]
        ];
        $this->assertEquals($expected, $param);
    }

    public function testGetSearchParam()
    {
        $filter = new KostSearchFilter();

        $filter->setGenders([PropertyGender::Mixed, PropertyGender::Female]);

        $filter->setOnlyCanBooking(true);
        $filter->setOnlyGoldplus(true);
        $filter->setOnlyMamichecked(true);
        $filter->setOnlyMamiroom(true);
        $filter->setExcludingTesting(true);

        $tags = [8, 12];
        $filter->setTags($tags);

        $filter->setPriceFilter(PropertyPricePeriod::Weekly(), 10000, 20000000);

        $filter->setSearchRectangle(new GeoRectangle(
            106.80286260141636, -6.1723343145242024, 
            106.84119701548268, -6.210698979549181)
        );

        $param = ElasticsearchParamBuilder::getSearchParam(
            'my-index', 
            $filter, 
            KostSearchSorting::SortByRecommendation(), 
            30, 
            15
        );

        // Check basic info
        $this->assertEquals('my-index', $param['index']);
        $this->assertEquals(30, $param['body']['from']);
        $this->assertEquals(15, $param['body']['size']);

        // Check sort param
        $expected = [
            [ 'sort_score' => [ 'order' => 'desc' ] ],
            [ 'kost_updated_at' => [ 'order' => 'desc' ] ]
        ];
        $this->assertEquals($expected, $param['body']['sort']);

        $must = $param['body']['query']['bool']['must'];

        // Check can booking
        $expected = [ 'term' => [ 'can_booking' => true ] ];
        $this->assertContains($expected, $must);

        // Check Goldplus
        $expected = [ 'term' => [ 'is_goldplus' => true ] ];
        $this->assertContains($expected, $must);

        // Check Mamichecked
        $expected = [ 'term' => [ 'is_mamichecked' => true ] ];
        $this->assertContains($expected, $must);

        // Check Mamiroom
        $expected = [ 'term' => [ 'is_mamiroom' => true ] ];
        $this->assertContains($expected, $must);

        // Check Excluding Testing
        $expected = [ 'term' => [ 'is_testing' => false ] ];
        $this->assertContains($expected, $must);

        // Check Genders
        $expected = [ 'terms' => [ 'gender' => [PropertyGender::Mixed, PropertyGender::Female] ] ];
        $this->assertContains($expected, $must);

        // Check Tags
        foreach ($tags as $tag)
        {
            $expected = [ 'term' => [ 'tags' => $tag ] ];
            $this->assertContains($expected, $must);
        }

        // Check PriceFilter
        $expected = [ 'term' => [ 'can_rent_weekly' => true ] ];
        $this->assertContains($expected, $must);

        $expected = [ 
            'range' => [ 
                'final_price_weekly' => [
                    'gte' => 10000,
                    'lte' => 20000000
                ] 
            ]
        ];
        $this->assertContains($expected, $must);

        // Check Search Rectangle
        $expected = [
            'geo_bounding_box' => [
                'location' => [
                    'top_left' => [
                        'lat' => -6.1723343145242024,
                        'lon' => 106.80286260141636
                    ],
                    'bottom_right'=> [
                        'lat' => -6.210698979549181,
                        'lon' => 106.84119701548268
                    ]
                ]
            ]
        ];
        $this->assertContains($expected, $must);
    }
}