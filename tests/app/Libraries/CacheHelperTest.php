<?php
declare(strict_types=1);

namespace App\Libraries;

use App\Test\MamiKosTestCase;
use Illuminate\Http\Request;
use TypeError;

final class CacheHelperTest extends MamiKosTestCase
{
    ///////////////////////////////////////////////////////////////////
    // CacheHelper::removeRandomness
    ///////////////////////////////////////////////////////////////////
    public function testRemoveRandomnessReturnsProperResultWithoutRandomnessProperties(): void
    {
        $requestBody = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":790,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $expected = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $this->assertEquals($expected, CacheHelper::removeRandomness($requestBody), 'removeRandomness($original) failed');

        // One which doesn't have any randomness properties
        $this->assertEquals($expected, CacheHelper::removeRandomness($expected), 'removeRandomness($expected) failed');
    }

    public function testRemoveRandomnessReturnsEmptyStringIfNullOrEmpty(): void
    {
        // ''
        $this->assertEquals('', CacheHelper::removeRandomness(''), 'removeRandomness(\'\') failed');
    }

    public function testRemoveRandomnessWillOccurExceptionIfNull(): void
    {
        $this->expectException(TypeError::class);

        // null
        CacheHelper::removeRandomness(null);
    }

    ///////////////////////////////////////////////////////////////////
    // CacheHelper::getKeyFrom
    ///////////////////////////////////////////////////////////////////
    public function testGetKeyFromReturnsProperResult(): void
    {
        // POST /garuda/stories/list
        $requestBodyForList = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":790,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $this->assertEquals('c3f98619a6779a411b53de100c2cef8e0a52f5bc', CacheHelper::getKeyFrom('/garuda/stories/list', 'POST', $requestBodyForList), 'getKeyFrom(\'/garuda/stories/list\', \'POST\', $requestBodyForList) failed');
        
        // POST /garuda/stories/list having another random_seeds value
        $requestBodyForList2 = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":119,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $this->assertEquals('c3f98619a6779a411b53de100c2cef8e0a52f5bc', CacheHelper::getKeyFrom('/garuda/stories/list', 'POST', $requestBodyForList2), 'getKeyFrom(\'/garuda/stories/list\', \'POST\', $requestBodyForList2) failed');
        
        // POST /garuda/stories/cluster (different route)
        $requestBodyForCluster = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":790,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $this->assertEquals('ff4cf058503cb5b8878ee4505e911845415ba93e', CacheHelper::getKeyFrom('/garuda/stories/cluster', 'POST', $requestBodyForCluster), 'getKeyFrom(\'/garuda/stories/cluster\', \'POST\', $requestBodyForCluster) failed');
    
        // GET /garuda/stories/cluster (different method)
        $this->assertEquals('670c4a95550e0c4eb8104ce4fedfb5b0a00c916c', CacheHelper::getKeyFrom('/garuda/stories/list', 'GET', $requestBodyForList), 'getKeyFrom(\'/garuda/stories/list\', \'GET\', $requestBodyForList) failed');
    }

    ///////////////////////////////////////////////////////////////////
    // CacheHelper::getKeyFromRequest
    ///////////////////////////////////////////////////////////////////
    public function testGetKeyFromRequestReturnsProperResult(): void
    {
        // POST /garuda/stories/list
        $requestBodyForList = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":790,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $request = Request::create('/garuda/stories/list', 'POST', [], [], [], [], $requestBodyForList);
        $this->assertEquals('c3f98619a6779a411b53de100c2cef8e0a52f5bc', CacheHelper::getKeyFromRequest($request), 'getKeyFromRequest($request) failed');
    
        // POST /garuda/stories/list having another random_seeds value should have same cache key
        $requestBodyForList2 = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":119,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $request2 = Request::create('/garuda/stories/list', 'POST', [], [], [], [], $requestBodyForList2);
        $this->assertEquals('c3f98619a6779a411b53de100c2cef8e0a52f5bc', CacheHelper::getKeyFromRequest($request2), 'getKeyFromRequest($request) failed');

        // POST /garuda/stories/cluster (different route)
        $requestBodyForCluster = '{"filters":{"gender":[0,1,2],"price_range":[0,15000000],"tag_ids":[],"level_info":[],"rent_type":2,"property_type":"all","random_seeds":790,"booking":0},"sorting":{"fields":"price","direction":"-"},"include_promoted":false,"limit":20,"offset":0,"location":[[106.81084156036378,-6.191029948722027],[106.8451738357544,-6.169910324287827]]}';
        $request3 = Request::create('/garuda/stories/cluster', 'POST', [], [], [], [], $requestBodyForCluster);
        $this->assertEquals('ff4cf058503cb5b8878ee4505e911845415ba93e', CacheHelper::getKeyFromRequest($request3), 'getKeyFromRequest($request) failed');
        
        // GET /garuda/stories/list (different method)
        $request4 = Request::create('/garuda/stories/list', 'GET', [], [], [], [], $requestBodyForList);
        $this->assertEquals('670c4a95550e0c4eb8104ce4fedfb5b0a00c916c', CacheHelper::getKeyFromRequest($request4), 'getKeyFromRequest($request) failed');
    }

    public function testGetKeyFromQueryString()
    {
        $url = '/sanjunipero/list?gender=0&price_range=10000,2000000&tags_ids=9,7,10'.
            '&level_info=9&rent_type=2&property_type=all'.'&random_seeds=230&booking=0&flash_sale=false'.
            '&mamirooms=false&include_promoted=false&mamichecker=false'.
            '&virtual_tour=false&sorting_field=price&sorting_direction=-&limit=20&offset=0&slug=kos/oyo/jakarta-utara';
        $request = Request::create($url, 'POST', [], [], [], [], []);
        $this->assertSame('369ae723f71b055b7ea3c01520b7798a3dc264c2', CacheHelper::getKeyFromQueryString($request));
    }

    public function testGetKeyFromRequestWillOccurExceptionIfNull(): void
    {
        $this->expectException(TypeError::class);

        // null
        CacheHelper::getKeyFromRequest(null);
    }
}

