<?php

namespace App\Libraries;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutEvents;

class UniqueCodeGeneratorTest extends MamiKosTestCase
{
    use WithoutEvents;

    public const TEST_PREFIX = "AB";
    public const TEST_ROOM_NAME = "Kos Exclusive Palagan Pelajar 10A";

    public function testGenerateWithEmptyPrefixAndEmptyRoomName()
    {
        $room = factory(Room::class)->create(
            [
                'is_booking' => 1
            ]
        );

        $generatedCode = UniqueCodeGenerator::generate('', $room);

        $this->assertEquals(8, strlen($generatedCode));
        $this->assertIsString($generatedCode);
    }

    public function testGenerateWithEmptyPrefixAndValidRoomName()
    {
        $room = factory(Room::class)->create(
            [
                'is_booking' => 1,
                'name' => self::TEST_ROOM_NAME
            ]
        );

        $generatedCode = UniqueCodeGenerator::generate('', $room);

        $this->assertEquals(8, strlen($generatedCode));
        $this->assertTrue(strpos($generatedCode, 'EP') !== false);
        $this->assertIsString($generatedCode);
    }

    public function testGenerateWithValidPrefixAndEmptyRoomName()
    {
        $room = factory(Room::class)->create(
            [
                'is_booking' => 1
            ]
        );

        $generatedCode = UniqueCodeGenerator::generate(self::TEST_PREFIX, $room);

        $this->assertEquals(10, strlen($generatedCode));
        $this->assertTrue(strpos($generatedCode, self::TEST_PREFIX) !== false);
        $this->assertIsString($generatedCode);
    }

    public function testGenerateWithValidPrefixAndValidRoomName()
    {
        $room = factory(Room::class)->create(
            [
                'is_booking' => 1,
                'name' => self::TEST_ROOM_NAME
            ]
        );

        $generatedCode = UniqueCodeGenerator::generate(self::TEST_PREFIX, $room);

        $this->assertEquals(10, strlen($generatedCode));
        $this->assertTrue(strpos($generatedCode, self::TEST_PREFIX) !== false);
        $this->assertTrue(strpos($generatedCode, 'EP') !== false);
        $this->assertIsString($generatedCode);
    }

    public function testGetTokenWithInvalidLength()
    {
        $token = UniqueCodeGenerator::getToken(0, UniqueCodeGenerator::ALPHA_POOL);

        $this->assertEmpty($token);
        $this->assertIsString($token);
    }

    public function testGetTokenWithValidLength()
    {
        $token = UniqueCodeGenerator::getToken(1, UniqueCodeGenerator::ALPHA_POOL);
        $this->assertEquals(1, strlen($token));
        $this->assertIsString($token);

        $token = UniqueCodeGenerator::getToken(5, UniqueCodeGenerator::ALPHA_POOL);
        $this->assertEquals(5, strlen($token));
        $this->assertIsString($token);

        $token = UniqueCodeGenerator::getToken(10, UniqueCodeGenerator::ALPHA_POOL);
        $this->assertEquals(10, strlen($token));
        $this->assertIsString($token);
    }

    public function testGetAbbreviationWithEmptyString()
    {
        $response = UniqueCodeGenerator::getAbbreviation('', null, []);

        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));
    }

    public function testGetAbbreviationWithInvalidLength()
    {
        $this->assertEquals(2, strlen(UniqueCodeGenerator::getAbbreviation('Kos Unit', null, [])));
    }

    public function testGetAbbreviationWithNonNumericLength()
    {
        $this->assertEquals(2, strlen(UniqueCodeGenerator::getAbbreviation('Kos Unit', '2', [])));
    }

    public function testGetAbbreviationWithSpecifiedLength()
    {
        $this->assertEquals(2, strlen(UniqueCodeGenerator::getAbbreviation('Kos Unit', 1, [])));
        $this->assertEquals(2, strlen(UniqueCodeGenerator::getAbbreviation('Kos Unit', 2, [])));
        $this->assertEquals(2, strlen(UniqueCodeGenerator::getAbbreviation('Kos Unit', 5, [])));
    }

    public function testGetAbbreviationWithSingleLetter()
    {
        $response = UniqueCodeGenerator::getAbbreviation('A', 2, []);
        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));

        $response = UniqueCodeGenerator::getAbbreviation('z', 2, []);
        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));
    }

    public function testGetAbbreviationWithNonAlphaString()
    {
        $response = UniqueCodeGenerator::getAbbreviation('0', 2, []);
        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));

        $response = UniqueCodeGenerator::getAbbreviation('123', 2, []);
        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));

        $response = UniqueCodeGenerator::getAbbreviation('&#$@', 2, []);
        $this->assertNotEmpty($response);
        $this->assertEquals(2, strlen($response));
    }
}
