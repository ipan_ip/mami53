<?php

namespace App\Libraries\Activity\Infobip;

use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Enums\Activity\Provider\Infobip\InfobipErrorCode;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCode;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCodeGroup;
use App\Libraries\Activity\Infobip\InfobipHttpClient;
use App\Libraries\Activity\Infobip\InfobipLibraryImplImpl;
use App\Test\MamiKosTestCase;
use Exception;
use Mockery;
use Psr\Http\Message\MessageInterface;
use stdClass;

/**
 * @property \Mockery\MockInterface|\Mockery\LegacyMockInterface|InfobipClient $client
 * @property InfobipLibraryImpl $infobipLibrary
 */
class InfobipLibraryImplTest extends MamiKosTestCase
{
    private $client;
    private $infobipLibrary;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = $this->mock(InfobipHttpClient::class);
        $this->infobipLibrary = $this->app->make(InfobipLibraryImpl::class);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    private function mockGetDeliveryReport($result, Exception $exception = null)
    {
        if (!is_null($exception)) {
            $this->client->shouldReceive('get')->andThrow($exception);
        } else {
            $this->client->shouldReceive('get')->andReturn($result);
        }
    }

    private function mockPostLookupNumber($result, Exception $exception = null)
    {
        $expectation = $this->client->shouldReceive('post')->withArgs([
            InfobipLibraryImpl::INFOBIP_URL_LOOKUP_NUMBER,
            Mockery::any()
        ]);
        if (!is_null($exception)) {
            $expectation->andThrow($exception);
        } else {
            $expectation->andReturn($result);
        }
    }

    private function mockPostSendSms($result, Exception $exception = null)
    {
        $expectation = $this->client->shouldReceive('post')->withArgs([
            InfobipLibraryImpl::INFOBIP_URL_SEND_SMS,
            Mockery::any()
        ]);
        if (!is_null($exception)) {
            $expectation->andThrow($exception);
        } else {
            $expectation->andReturn($result);
        }
    }

    private function mockPostSendSmsAdvanced($result, Exception $exception = null)
    {
        $expectation = $this->client->shouldReceive('post')->withArgs([
            InfobipLibraryImpl::INFOBIP_URL_SEND_SMS_ADVANCED,
            Mockery::any()
        ]);
        if (!is_null($exception)) {
            $expectation->andThrow($exception);
        } else {
            $expectation->andReturn($result);
        }
    }
    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testFetchDeliveryReportInfobipWithDeliveredResponse()
    {
        $json = [
            "results" => [
                [
                    "bulkId" => "BULK-ID-123-xyz",
                    "messageId" => "MESSAGE-ID-123-xyz",
                    "to" => "41793026727",
                    "sentAt" => "2019-11-09T16:00:00.000+0000",
                    "doneAt" => "2019-11-09T16:00:00.000+0000",
                    "smsCount" => 1,
                    "price" => [
                        "pricePerMessage" => 0.01,
                        "currency" => "EUR",
                    ],
                    "status" => [
                        "groupId" => 3,
                        "groupName" => "DELIVERED",
                        "id" => 5,
                        "name" => "DELIVERED_TO_HANDSET",
                        "description" => "Message delivered to handset",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "Ok",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockGetDeliveryReport($this->getMockInfobipResponse($json));
        $result = $this->infobipLibrary->fetchDeliveryReport(str_random());
        $this->assertNotNull($result);
        $this->assertEquals(ActivationCodeDeliveryReportStatus::DELIVERED, $result->status);
        $this->assertTrue($result->success);
        $this->assertNull($result->reason);
        $this->assertNotNull($result->sendAt);
        $this->assertNotNull($result->doneAt);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testFetchDeliveryReportInfobipWithFailedResponse()
    {
        $json = [
            "results" => [
                [
                    "bulkId" => "BULK-ID-123-xyz",
                    "messageId" => "MESSAGE-ID-123-xyz",
                    "to" => "41793026727",
                    "sentAt" => "2019-11-09T16:00:00.000+0000",
                    "doneAt" => "2019-11-09T16:00:00.000+0000",
                    "smsCount" => 1,
                    "price" => [
                        "pricePerMessage" => 0.01,
                        "currency" => "EUR",
                    ],
                    "status" => [
                        "groupId" => 2,
                        "groupName" => "UNDELIVERABLE",
                        "id" => 4,
                        "name" => "UNDELIVERABLE_REJECTED_OPERATOR",
                        "description" => "Message has been sent to the operator, whereas the request was rejected.",
                    ],
                    "error" => [
                        "groupId" => 1,
                        "groupName" => "HANDSET_ERRORS",
                        "id" => 1,
                        "name" => "EC_UNKNOWN_SUBSCRIBER",
                        "description" => "The number does not exist or it has not been assigned to any active subscriber in the operator’s user database.",
                        "permanent" => true,
                    ],
                ],
            ],
        ];
        $this->mockGetDeliveryReport($this->getMockInfobipResponse($json));
        $result = $this->infobipLibrary->fetchDeliveryReport(str_random());
        $this->assertNotNull($result);
        $this->assertEquals(ActivationCodeDeliveryReportStatus::FAILED, $result->status);
        $this->assertTrue($result->success);
        $this->assertNotNull($result->reason);
        $this->assertNull($result->sendAt);
        $this->assertNull($result->doneAt);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testFetchDeliveryReportInfobipWithEmptyResponse()
    {
        $json = [
            "results" => [],
        ];
        $this->mockGetDeliveryReport($this->getMockInfobipResponse($json));
        $result = $this->infobipLibrary->fetchDeliveryReport(str_random());
        $this->assertNotNull($result);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testFetchDeliveryReportInfobipWithEmptyStringResponse()
    {
        $this->expectException(Exception::class);
        $this->mockGetDeliveryReport('');
        $result = $this->infobipLibrary->fetchDeliveryReport(str_random());
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testFetchDeliveryReportInfobipWithException()
    {
        $this->expectException(Exception::class);
        $this->mockGetDeliveryReport(null, new Exception("Mock exception"));
        $result = $this->infobipLibrary->fetchDeliveryReport(str_random());
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsEmptyLookupResults()
    {
        $lookupJson = [
            "results" => [],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsEmptyStringLookupResults()
    {
        $this->expectException(Exception::class);
        $this->mockPostLookupNumber('');
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsPendingSendSms()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,

                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $sendSmsJson = [
            "messages" => [
                [
                    "to" => "628980385441",
                    "status" => [
                        "groupId" => 1,
                        "groupName" => "PENDING",
                        "id" => 7,
                        "name" => "PENDING_ENROUTE",
                        "description" => "Message sent to next instance",
                    ],
                    "messageId" => "29341033332007156296",
                    "smsCount" => 1,
                ],
            ],
        ];
        $this->mockPostSendSmsAdvanced($this->getMockInfobipResponse($sendSmsJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNull($result->message);
        $this->assertTrue($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsPendingSendSmsWithoutNotificationUrl()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $sendSmsJson = [
            "messages" => [
                [
                    "to" => "628980385441",
                    "status" => [
                        "groupId" => 1,
                        "groupName" => "PENDING",
                        "id" => 7,
                        "name" => "PENDING_ENROUTE",
                        "description" => "Message sent to next instance",
                    ],
                    "messageId" => "29341033332007156296",
                    "smsCount" => 1,
                ],
            ],
        ];
        $this->mockPostSendSms($this->getMockInfobipResponse($sendSmsJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), null);
        $this->assertNull($result->message);
        $this->assertTrue($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsRejectedSendSms()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $sendSmsJson = [
            "messages" => [
                [
                    "to" => "628980385441",
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "messageId" => "29341033332007156296",
                    "smsCount" => 1,
                ],
            ],
        ];
        $this->mockPostSendSmsAdvanced($this->getMockInfobipResponse($sendSmsJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsRejectedSendSmsWithErrorObj()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $sendSmsJson = [
            "messages" => [
                [
                    "to" => "628980385441",
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                    "messageId" => "29341033332007156296",
                    "smsCount" => 1,
                ],
            ],
        ];
        $this->mockPostSendSmsAdvanced($this->getMockInfobipResponse($sendSmsJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedCriticalLookupResultsPendingSendSms()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "1",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 8,
                        "name" => "REJECTED_PREFIX_MISSING",
                        "description" => "Number prefix missing",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsUndeliverableLookupResultsPendingSendSms()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "1",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 2,
                        "groupName" => "UNDELIVERABLE",
                        "id" => 4,
                        "name" => "UNDELIVERABLE_REJECTED_OPERATOR",
                        "description" => "Undeliverable",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsExpiredLookupResultsPendingSendSms()
    {
        $lookupJson = [
            "results" => [
                [
                    "to" => "1",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 4,
                        "groupName" => "EXPIRED",
                        "id" => 15,
                        "name" => "EXPIRED_EXPIRED",
                        "description" => "Expired",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertNotEmpty($result->message);
        $this->assertFalse($result->success);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsEmptyStringSendSmsResult()
    {
        $this->expectException(Exception::class);
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $this->mockPostSendSmsAdvanced('');
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), str_random());
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testSendSmsRejectedNonCriticalLookupResultsEmptyStringSendSmsResultWithoutNotificationUrl()
    {
        $this->expectException(Exception::class);
        $lookupJson = [
            "results" => [
                [
                    "to" => "628980385441",
                    "originalNetwork" => [],
                    "status" => [
                        "groupId" => 5,
                        "groupName" => "REJECTED",
                        "id" => 6,
                        "name" => "REJECTED_NETWORK",
                        "description" => "Network is forbidden",
                        "action" => "Contact account manager",
                    ],
                    "error" => [
                        "groupId" => 0,
                        "groupName" => "OK",
                        "id" => 0,
                        "name" => "NO_ERROR",
                        "description" => "No Error",
                        "permanent" => false,
                    ],
                ],
            ],
        ];
        $this->mockPostLookupNumber($this->getMockInfobipResponse($lookupJson));
        $this->mockPostSendSms('');
        $result = $this->infobipLibrary->sendActivationCode(str_random(), str_random(), null);
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testConvertSendSmsStatusCodeNotAcceptedGroupCode()
    {
        $result = $this->executeConvertSendSmsStatusCode(InfobipStatusCodeGroup::REJECTED);
        $this->assertEquals(ActivationCodeDeliveryReportStatus::FAILED, $result);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testConvertSendSmsStatusCodePendingGroupCode()
    {
        $result = $this->executeConvertSendSmsStatusCode(InfobipStatusCodeGroup::PENDING);
        $this->assertEquals(ActivationCodeDeliveryReportStatus::PENDING, $result);
    }

    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryImpl
     */
    public function testConvertSendSmsStatusCodeOkGroupCode()
    {
        $result = $this->executeConvertSendSmsStatusCode(InfobipStatusCodeGroup::OK);
        $this->assertEquals(ActivationCodeDeliveryReportStatus::DELIVERED, $result);
    }

    private function executeConvertSendSmsStatusCode(int $groupCode)
    {
        return $this->getNonPublicMethodFromClass(InfobipLibraryImpl::class, 'convertSendSmsStatusCode')
            ->invokeArgs($this->infobipLibrary, [$groupCode]);
    }

    private function getMockInfobipResponse(array $mockResult)
    {
        $mockMessageInterface = Mockery::mock(MessageInterface::class);
        $mockMessageInterface->shouldReceive('getBody')->andReturn($mockMessageInterface);
        $mockMessageInterface->shouldReceive('getContents')->andReturn(json_encode($mockResult));
        return $mockMessageInterface;
    }
}
