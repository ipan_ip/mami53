<?php

namespace App\Libraries\Activity\Infobip;

use App\Test\MamiKosTestCase;
use App\Libraries\Activity\Infobip\InfobipConverter;
use stdClass;

class InfobipConverterTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-2054
     * @group App\Libraries\Activity\Infobip\InfobipConverter
     */
    public function testConvertToTranslatedSuccessStatusMessageNonRegisteredId()
    {
        $status = new stdClass();
        $status->id = mt_rand(1111, 9999);
        $status->name = str_random(10);
        $message = InfobipConverter::convertToTranslatedSuccessStatusMessage($status);
        $this->assertEquals($status->name, $message);
    }

    /**
     * @group UG
     * @group UG-2054
     * @group App\Libraries\Activity\Infobip\InfobipConverter
     */
    public function testConvertToTranslatedErrorStatusMessageNonRegisteredId()
    {
        $error = new stdClass();
        $error->id = mt_rand(1111, 9999);
        $error->name = str_random(10);
        $message = InfobipConverter::convertToTranslatedErrorStatusMessage($error);
        $this->assertEquals($error->name, $message);
    }
}
