<?php

namespace App\Libraries\Activity\Infobip;

use Mockery;
use App\Test\MamiKosTestCase;
use App\Libraries\Activity\Infobip\InfobipHttpClient;

class InfobipHttpClientTest extends MamiKosTestCase
{
    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App/Libraries/Activity/Infobip/InfobipHttpClient
     */
    public function testHttpClientShouldBeConstructable()
    {
        $this->assertNotNull($this->app->make(InfobipHttpClient::class));
    }
}
