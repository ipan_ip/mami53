<?php

namespace App\Libraries\Activity\Infobip;

use App\Libraries\Activity\Infobip\InfobipLibraryFaker;
use App\Test\MamiKosTestCase;

class InfobipLibraryFakerTest extends MamiKosTestCase
{
    /** @var InfobipLibraryFaker $infobipLibraryFaker */
    private $infobipLibraryFaker;

    protected function setUp(): void
    {
        parent::setup();
        $this->infobipLibraryFaker = $this->app->make(InfobipLibraryFaker::class);
    }
    
    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryFaker
     *
     * @return void
     */
    public function testFetchDeliveryReportShouldNotNull()
    {
        $this->assertNotNull($this->infobipLibraryFaker->fetchDeliveryReport(str_random(10)));
    }
    
    /**
     * @group UG
     * @group UG-1283
     * @group App/Libraries/Activity/Infobip/InfobipLibraryFaker
     *
     * @return void
     */
    public function testSendActivationCodeNotNull()
    {
        $this->assertNotNull($this->infobipLibraryFaker->sendActivationCode(str_random(10), str_random(10), str_random(10)));
    }
    
}
