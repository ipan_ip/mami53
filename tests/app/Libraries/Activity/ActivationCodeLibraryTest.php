<?php

namespace App\Libraries\Activity;

use App\Exceptions\ProviderNotSupportedException;
use App\Libraries\Activity\ActivationCodeLibrary;
use App\Libraries\Activity\Infobip\InfobipLibrary;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use App\Test\MamiKosTestCase;
use Mockery;

/**
 * @property ActivationCodeLibrary $activationCodeLibrary
 * @property \Mockery\MockInterface|\Mockery\LegacyMockInterface $infobipClient
 */
class ActivationCodeLibraryTest extends MamiKosTestCase
{

    private $activationCodeLibrary;
    private $infobipClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->infobipClient = $this->mock(InfobipLibrary::class);
        $this->activationCodeLibrary = app()->make(ActivationCodeLibrary::class);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Libraries\Activity\ActivationCodeLibrary
     */
    public function testFetchDeliveryReportWithNotRegisteredProviderShouldThrowException()
    {
        $this->expectException(ProviderNotSupportedException::class);
        $result = $this->activationCodeLibrary->fetchDeliveryReport(str_random(), 'Random Provider');
        $this->assertNotNull($result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Libraries\Activity\ActivationCodeLibrary
     */
    public function testFetchDeliveryReportInfobipWithDeliveredResponse()
    {
        $this->mockFetchDeliveryReportInfobipResponse(Mockery::mock(ProviderActivationCodeDeliveryReport::class));
        $result = $this->activationCodeLibrary->fetchDeliveryReport(str_random(), ActivationCodeLibrary::PROVIDER_INFOBIP);
        $this->assertNotNull($result);
        $this->assertInstanceOf(ProviderActivationCodeDeliveryReport::class, $result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Libraries\Activity\ActivationCodeLibrary
     */
    public function testSendSmsActivationCodeWithNotRegisteredProviderShouldThrowException()
    {
        $this->expectException(ProviderNotSupportedException::class);
        $result = $this->activationCodeLibrary->sendSmsActivationCode(str_random(), str_random(), 'Random Provider', str_random());
        $this->assertNotNull($result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Libraries\Activity\ActivationCodeLibrary
     */
    public function testSendSmsActivationCodeInfobipWithDeliveredResponse()
    {
        $this->mockSendSmsActivationCodeInfobipResponse(Mockery::mock(ProviderSendActivationCodeResult::class));
        $result = $this->activationCodeLibrary->sendSmsActivationCode(str_random(), str_random(), ActivationCodeLibrary::PROVIDER_INFOBIP, null);
        $this->assertNotNull($result);
        $this->assertInstanceOf(ProviderSendActivationCodeResult::class, $result);
    }

    private function mockFetchDeliveryReportInfobipResponse(?ProviderActivationCodeDeliveryReport $providerActivationCodeDeliveryReport): void
    {
        $this->infobipClient->expects('fetchDeliveryReport')->andReturn($providerActivationCodeDeliveryReport);
    }

    private function mockSendSmsActivationCodeInfobipResponse(?ProviderSendActivationCodeResult $providerActivationCodeDeliveryReport): void
    {
        $this->infobipClient->expects('sendActivationCode')->andReturn($providerActivationCodeDeliveryReport);
    }
}
