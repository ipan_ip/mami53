<?php

namespace App\Libraries;

use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

class PackageHelperTest extends MamiKosTestCase
{
    private $mockFileContent;
    private $mockGlob;
    private $mockFileExist;
    private $mockIsDir;

    protected function setUp(): void
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['build_version' => '1.8.8.0']);
                    }
                    return 'ref: refs/heads/master';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('glob')
            ->setFunction(
                function ($folder) {
                    return ['tag/v1.8.6', 'tag/v1.8.7', 'tag/v1.8.7.1', 'tag/v1.8.8', 'tag/v1.8.7.2'];
                }
            );
        $this->mockGlob = $builder->build();
        $this->mockGlob->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($file) {
                    return true;
                }
            );
        $this->mockFileExist = $builder->build();
        $this->mockFileExist->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('is_dir')
            ->setFunction(
                function ($folder) {
                    return true;
                }
            );
        $this->mockIsDir = $builder->build();
        $this->mockIsDir->enable();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->mockFileContent->disable();
        $this->mockGlob->disable();
        $this->mockFileExist->disable();
        $this->mockIsDir->disable();
    }

    public function testGetApplicationVersion_Success()
    {
        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_NoBuildVersionKey_GitTagVersionReturned()
    {
        $this->mockFileContent->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['not_build_version' => '1.8.8.0']);
                    }
                    return 'ref: refs/heads/master';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion(false));
    }

    public function testGetApplicationVersion_GitHeadFileNotExist_PackageJsonVersionReturned()
    {
        $this->mockFileExist->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($file) {
                    return false;
                }
            );
        $this->mockFileExist = $builder->build();
        $this->mockFileExist->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitHeadFileEmpty_PackageJsonVersionReturned()
    {
        $this->mockFileContent->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['build_version' => '1.8.8.0']);
                    }
                    return '';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitTagFolderNotExist_PackageJsonVersionReturned()
    {
        $this->mockIsDir->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('is_dir')
            ->setFunction(
                function ($folder) {
                    return false;
                }
            );
        $this->mockIsDir = $builder->build();
        $this->mockIsDir->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitTagFolderEmpty_PackageJsonVersionReturned()
    {
        $this->mockGlob->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('glob')
            ->setFunction(
                function ($folder) {
                    return [];
                }
            );
        $this->mockGlob = $builder->build();
        $this->mockGlob->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_NoProperGitTag_PackageJsonVersionReturned()
    {
        $this->mockGlob->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('glob')
            ->setFunction(
                function ($folder) {
                    return ['not_tag_1', 'not_tag_2'];
                }
            );
        $this->mockGlob = $builder->build();
        $this->mockGlob->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitFolderExist_FeatureBranch_AddedMinorVersionReturned()
    {
        $this->mockFileContent->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['build_version' => '1.8.8.0']);
                    }
                    return 'ref: refs/heads/feature/asd';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $this->assertEquals('v1.8.9.0', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitFolderExist_HotfixBranch_AddedPatchVersionReturned()
    {
        $this->mockFileContent->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['build_version' => '1.8.8.0']);
                    }
                    return 'ref: refs/heads/hotfix/asd';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $this->assertEquals('v1.8.8.1', PackageHelper::getApplicationVersion());
    }

    public function testGetApplicationVersion_GitFolderExist_NoRefBranch_VersionReturnedAsIs()
    {
        $this->mockFileContent->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_get_contents')
            ->setFunction(
                function ($file) {
                    if ($file === base_path('package.json')) {
                        return json_encode(['build_version' => '1.8.8.0']);
                    }
                    return 'asd';
                }
            );
        $this->mockFileContent = $builder->build();
        $this->mockFileContent->enable();

        $this->assertEquals('v1.8.8.0', PackageHelper::getApplicationVersion());
    }
}
