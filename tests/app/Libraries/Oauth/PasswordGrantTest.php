<?php

namespace App\Libraries\Oauth;

use App\Test\MamiKosTestCase;
use App\Test\Traits\UsePassport;
use App\Test\Stubs\AccessTokenEntity;
use App\Test\Stubs\ClientEntity;
use App\Test\Stubs\RefreshTokenEntity;
use App\Test\Stubs\ScopeEntity;
use App\Test\Stubs\ResponseType;
use App\User;
use Laravel\Passport\Bridge\ClientRepository;
use Laravel\Passport\Bridge\UserRepository;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\ClientRepository as PassportClientRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Mockery;
use stdClass;

class PasswordGrantTest extends MamiKosTestCase
{
    use UsePassport;

    const DEFAULT_SCOPE = 'basic';
    private $clientEntity;

    public function testSetClientCredentialNoPasswordGrantClientCreationFail()
    {
        $passportClientMock = $this->mockPartialAlternatively(PassportClientRepository::class)->shouldReceive('createPasswordGrantClient')->andReturn(null);
        $grant = new PasswordGrant(
            $this->mockAlternatively(UserRepository::class),
            $this->mockAlternatively(RefreshTokenRepository::class)
        );
        $grant->setClientRepository(app()->make(ClientRepository::class));

        $this->expectExceptionCode(ErrorCode::CLIENT_ERROR);
        $grant->setClientCredential(null, null);
        $passportClientMock->disable();
    }

    public function testSetClientCredentialNoEnvironmentNoExistingSuccess()
    {
        $grant = new PasswordGrant(
            $this->mockAlternatively(UserRepository::class),
            $this->mockAlternatively(RefreshTokenRepository::class)
        );
        $grant->setClientRepository(app()->make(ClientRepository::class));
        $grant->setClientCredential(null, null);
        $client = $grant->getClientEntity();

        $this->assertInstanceOf(ClientEntityInterface::class, $client, "correct Instance");
        $this->cleanPassportClient(Passport::client()->where('id', $client->getIdentifier())->first());
    }

    public function testSetClientCredentialNoEnvironmentInjectedSuccess()
    {
        $existingClientEntity = $this->createClientGrant();

        $grant = new PasswordGrant(
            $this->mockAlternatively(UserRepository::class),
            $this->mockAlternatively(RefreshTokenRepository::class)
        );
        $grant->setClientRepository(app()->make(ClientRepository::class));
        $grant->setClientCredential(null, null);
        $client = $grant->getClientEntity();

        $this->assertInstanceOf(ClientEntityInterface::class, $client, "correct Instance");
        $this->assertSame($client->getIdentifier(), (string) $existingClientEntity->id);
        $this->cleanPassportClient($existingClientEntity);
    }

    public function testSetClientCredentialWithEnvironmentSuccess()
    {
        $existingClientEntity = $this->createClientGrant();

        $grant = new PasswordGrant(
            $this->mockAlternatively(UserRepository::class),
            $this->mockAlternatively(RefreshTokenRepository::class)
        );
        $grant->setClientRepository(app()->make(ClientRepository::class));
        $grant->setClientCredential($existingClientEntity->id, $existingClientEntity->secret);
        $client = $grant->getClientEntity();

        $this->assertInstanceOf(ClientEntityInterface::class, $client, "correct Instance");
        $this->assertSame($client->getIdentifier(), (string) $existingClientEntity->id);
        $this->cleanPassportClient($existingClientEntity);
    }

    public function testIssueTokenForUserSuccess()
    {
        $client = new ClientEntity();
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $userEntity = factory(User::class)->create(['is_verify' => 1]);
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());

        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new PasswordGrant($userRepositoryMock, $refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setDefaultScope(self::DEFAULT_SCOPE);


        $responseType = new ResponseType();
        $grant->issueTokenFor($userEntity, $responseType, new \DateInterval('PT5M'));

        $this->assertInstanceOf(AccessTokenEntityInterface::class, $responseType->getAccessToken());
        $this->assertInstanceOf(RefreshTokenEntityInterface::class, $responseType->getRefreshToken());
    }

    public function testIssueTokenForUserWrongClientEntityFail()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn(new stdClass);

        $userEntity = factory(User::class)->create(['is_verify' => 1]);
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());

        $grant = new PasswordGrant($userRepositoryMock, $refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);

        $this->expectExceptionCode(ErrorCode::CLIENT_ERROR);
        $result = $grant->getClientEntity();
        $this->assertNull($result);
    }

    protected function createClientGrant()
    {
        return $this->registerPassportEnvironment();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }
}
