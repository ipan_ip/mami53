<?php

namespace App\Libraries\Oauth;

use App\Test\MamiKosTestCase;
use App\User;
use App\Libraries\Oauth\AuthorizationServer;
use App\Test\Stubs\AccessTokenEntity;
use App\Test\Stubs\ClientEntity;
use App\Test\Stubs\ScopeEntity;
use App\Test\Stubs\ResponseType;
use App\Test\Stubs\RefreshTokenEntity;
use App\Test\Traits\UsePassport;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Grant\PasswordGrant as BasePasswordgrant;
use Zend\Diactoros\Response;

class AuthorizationServerTest extends MamiKosTestCase
{
    use UsePassport;

    const DEFAULT_SCOPE = 'basic';

    /**
     * @covers App\Libraries\Oauth\AuthorizationServer
     */
    public function testRespondAccessTokenForUser()
    {
        $clientRepository = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepository->method('getClientEntity')->willReturn(new ClientEntity());

        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());

        $userEntity =  factory(User::class)->create(['is_verify' => 1]);
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());

        $grant = new PasswordGrant($userRepositoryMock, $refreshTokenRepositoryMock);

        $server = new AuthorizationServer(
            $clientRepository,
            $accessTokenRepositoryMock,
            $scopeRepositoryMock,
            $this->makeCryptKey('private'),
            base64_encode(random_bytes(36)),
            new ResponseType()
        );

        $server->setDefaultScope(self::DEFAULT_SCOPE);
        $server->enableGrantType($grant, new \DateInterval('PT1M'));

        $response = $server->respondAccessTokenForUser($userEntity, new Response);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRespondAccessTokenForUserGrantTypeNotModifiedNotMatchFailed()
    {
        $clientRepository = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepository->method('getClientEntity')->willReturn(new ClientEntity());

        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());

        $userEntity =  factory(User::class)->create(['is_verify' => 1]);

        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());

        $grant = new BasePasswordgrant($userRepositoryMock, $refreshTokenRepositoryMock);

        $server = new AuthorizationServer(
            $clientRepository,
            $accessTokenRepositoryMock,
            $scopeRepositoryMock,
            $this->makeCryptKey('private'),
            base64_encode(random_bytes(36)),
            new ResponseType()
        );

        $server->setDefaultScope(self::DEFAULT_SCOPE);
        $server->enableGrantType($grant, new \DateInterval('PT1M'));

        $this->expectExceptionCode(ErrorCode::CONFIG_ERROR);
        $response = $server->respondAccessTokenForUser($userEntity, new Response);
        $this->assertEquals(500, $response->getStatusCode());
    }
}
