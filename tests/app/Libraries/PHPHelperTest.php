<?php
namespace App\Libraries;

use App\Test\MamiKosTestCase;
use App\Libraries\PHPHelper;
use App\User;

class PHPHelperTest extends MamiKosTestCase
{
    public function testIsCountableTrue()
    {
        $arr = [6,7,8];
        $this->assertTrue(PHPHelper::isCountable($arr));

        $users = User::all();
        $this->assertTrue(PHPHelper::isCountable($users));
    }

    public function testIsCountableFalse()
    {
        $user = new User();
        $this->assertFalse(PHPHelper::isCountable($user));
    }

    public function testBooleanToString()
    {
        $this->assertEquals('true', PHPHelper::booleanToString(true));
        $this->assertEquals('true', PHPHelper::booleanToString(1));
        $this->assertEquals('false', PHPHelper::booleanToString(false));
        $this->assertEquals('false', PHPHelper::booleanToString(0));
    }

}