<?php
namespace App\Libraries;

use App\Test\MamiKosTestCase;

class PhoneNumberFormatterTest extends MamiKosTestCase
{
    protected $lib;
    protected function setUp(): void
    {
        parent::setUp();
        $this->lib = new PhoneNumberFormatter();
    }

    public function testGetVariationNumberFor()
    {
        $number = "081234567890";

        $res = $this->lib->getVariationNumberFor($number);

        $this->assertGreaterThanOrEqual(4, count($res));
        $this->assertContains("81234567890", $res);
        $this->assertContains("081234567890", $res);
        $this->assertContains("6281234567890", $res);
        $this->assertContains("+6281234567890", $res);
    }

    public function testTrimPhoneNumberWithNoLeading()
    {
        $number = "81234567890";

        $res = $this->lib->trimPhoneNumber($number);

        $this->assertEquals("81234567890", $res);
    }

    public function testTrimPhoneNumberWithZeroLeading()
    {
        $number = "081234567890";

        $res = $this->lib->trimPhoneNumber($number);

        $this->assertEquals("81234567890", $res);
    }

    public function testTrimPhoneNumberWith62Leading()
    {
        $number = "6281234567890";

        $res = $this->lib->trimPhoneNumber($number);

        $this->assertEquals("81234567890", $res);
    }

    public function testTrimPhoneNumberWithPlus62Leading()
    {
        $number = "+6281234567890";

        $res = $this->lib->trimPhoneNumber($number);

        $this->assertEquals("81234567890", $res);
    }
}