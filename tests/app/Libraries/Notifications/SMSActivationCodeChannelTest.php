<?php

namespace App\Libraries\Notifications;

use App\Entities\Activity\ActivationCode;
use App\Libraries\Notifications\SMSActivationCodeChannel;
use App\Notifications\PhoneNumberVerification;
use App\Services\Activity\ActivationCodeService;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;
use Notification;

class SMSActivationCodeChannelTest extends MamiKosTestCase
{
    /** @var ActivationCodeService|Mockery\MockInterface $activationCodeService */
    private $activationCodeService;
    /** @var SMSActivationCodeChannel $smsActivationCodeChannel */
    private $smsActivationCodeChannel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->activationCodeService = $this->mock(ActivationCodeService::class);
        $this->smsActivationCodeChannel = $this->app->make(SMSActivationCodeChannel::class);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }


    /**
     * @group UG
     * @group UG-1270
     * @group App/Libraries/Notifications/SMSActivationCodeChannel
     */
    public function testShouldBeAbleToSendViaNotificationSend()
    {
        $phoneNumberVerification = new PhoneNumberVerification(
            factory(ActivationCode::class)->make()
        );
        $user = factory(User::class)->make();
        Notification::fake();
        Notification::send($user, $phoneNumberVerification);
        Notification::assertSentTo($user, PhoneNumberVerification::class, function ($notifiable, $channels) {
            $this->assertEquals([SMSActivationCodeChannel::class], $channels);
            return true;
        });
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Libraries/Notifications/SMSActivationCodeChannel
     */
    public function testSendWithInvalidIndonesiaNumberShouldNotExecuteTheLibrary()
    {
        $this->activationCodeService->shouldNotReceive('sendSmsActivationCode');
        $phoneNumberVerification = new PhoneNumberVerification(
            factory(ActivationCode::class)->make()
        );
        $user = factory(User::class)->make([
            'phone_number' => '189299384023'
        ]);
        $this->smsActivationCodeChannel->send($user, $phoneNumberVerification);
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Libraries/Notifications/SMSActivationCodeChannel
     */
    public function testSendWithoutAnyPhoneNumberShouldNotExecuteTheLibrary()
    {
        $this->activationCodeService->shouldNotReceive('sendSmsActivationCode');
        $phoneNumberVerification = new PhoneNumberVerification(
            factory(ActivationCode::class)->make()
        );
        $user = factory(User::class)->make([
            'phone_number' => null
        ]);
        $this->smsActivationCodeChannel->send($user, $phoneNumberVerification);
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Libraries/Notifications/SMSActivationCodeChannel
     */
    public function testSendWithValidIndonesiaNumberShouldExecuteTheSendSmsService()
    {
        $this->activationCodeService->shouldReceive('sendSmsActivationCode')->once()->andReturn();
        $phoneNumberVerification = new PhoneNumberVerification(
            factory(ActivationCode::class)->make()
        );
        $user = factory(User::class)->make([
            'phone_number' => '089299384023'
        ]);
        $this->smsActivationCodeChannel->send($user, $phoneNumberVerification);
        $this->assertTrue(true);
    }
}
