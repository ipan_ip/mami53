<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class ManualPaymentPremiumRequestReminderTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new ManualPaymentPremiumRequestReminder('GoldPlus 1');
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class,
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(ManualPaymentPremiumRequestReminder::SUBJECT, $result->title);
        $this->assertEquals(sprintf(ManualPaymentPremiumRequestReminder::BODY, 'GoldPlus 1') . ' ' . ManualPaymentPremiumRequestReminder::BUTTON_TEXT, $result->message);
        $this->assertEquals(ManualPaymentPremiumRequestReminder::BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(ManualPaymentPremiumRequestReminder::BUTTON_URL, $result->url_button_one);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => ManualPaymentPremiumRequestReminder::SUBJECT,
            'message' =>  implode(" ", [sprintf(ManualPaymentPremiumRequestReminder::BODY, 'GoldPlus 1'), ManualPaymentPremiumRequestReminder::BUTTON_TEXT]),
            'scheme' =>  AppSchemes::getOwnerListRoom()
        ], $result);
    }
}
