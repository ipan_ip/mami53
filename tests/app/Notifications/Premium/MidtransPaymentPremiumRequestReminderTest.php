<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class MidtransPaymentPremiumRequestReminderTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new MidtransPaymentPremiumRequestReminder('GP4');
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => MidtransPaymentPremiumRequestReminder::SUBJECT,
            'message'   =>  sprintf(MidtransPaymentPremiumRequestReminder::BODY, 'GP4'),
            'scheme'    =>  AppSchemes::getOwnerListRoom()
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(MidtransPaymentPremiumRequestReminder::SUBJECT, $result->title);
        $this->assertEquals(sprintf(MidtransPaymentPremiumRequestReminder::BODY, 'GP4'), $result->message);
        $this->assertEquals(MidtransPaymentPremiumRequestReminder::BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(MidtransPaymentPremiumRequestReminder::BUTTON_URL, $result->url_button_one);
    }
}