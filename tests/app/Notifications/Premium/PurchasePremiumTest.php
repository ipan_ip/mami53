<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Test\MamiKosTestCase;
use App\User;

class PurchasePremiumTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new PurchasePremium([
            'total' => 1500000,
            'bank_name' => 'Mandiri',
            'bank_rek' => 123456,
            'bank_account' => 345678
        ]);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannelInfoBip::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'Silakan lakukan pembayaran Rp1.500.000 ke rek.Mandiri:123456. Terima kasih'
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(PurchasePremium::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(PurchasePremium::WEBPUSH_BODY, $result->message);
        $this->assertEquals(PurchasePremium::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(PurchasePremium::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}