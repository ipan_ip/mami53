<?php

namespace App\Notifications\Premium;

use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class ReminderBalanceTopupTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new ReminderBalanceTopup('bang.kerupux.com://scheme');
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotifData()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'message' => 'Langsung top up sekarang',
            'name' => 'Saldo Iklan Anda hampir habis',
            'scheme' => 'bang.kerupux.com://scheme',
        ], $result);
    }
}
