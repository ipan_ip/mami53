<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class GpCashbackTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new GpCashback(100000);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class,
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(sprintf(GpCashback::NOTIF_TITLE, number_format(100000,0,".",".")), $result->title);
        $this->assertEquals(GpCashback::NOTIF_BODY, $result->message);
        $this->assertEquals(GpCashback::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(config('owner.dashboard_url'), $result->url_button_one);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => sprintf(GpCashback::NOTIF_TITLE, number_format(100000,0,".",".")),
            'message' => GpCashback::NOTIF_BODY,
            'scheme' => AppSchemes::getOwnerProfile()
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
