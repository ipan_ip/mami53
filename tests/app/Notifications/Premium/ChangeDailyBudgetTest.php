<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class ChangeDailyBudgetTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new ChangeDailyBudget([
            'room_name' => 'Test room name',
            'allocation' => 10000
        ]);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => 'Alokasi harian iklan Test room name telah diubah',
            'message' => 'Jumlah alokasi menjadi 10000',
            'scheme' => AppSchemes::getOwnerListRoom()
        ], $result);
    }
}
