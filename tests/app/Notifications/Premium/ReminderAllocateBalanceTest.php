<?php

namespace App\Notifications\Premium;

use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class ReminderAllocateBalanceTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new ReminderAllocateBalance('bang.kerupux.com://scheme');
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotifData()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => 'Alokasikan Saldo Iklan Anda',
            'message' => 'Yuk, alokasikan saldo iklan Anda agar iklan makin banyak dilihat pencari kos',
            'scheme' => 'bang.kerupux.com://scheme',
        ], $result);
    }
}
