<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class DailyBudgetAllocationTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new DailyBudgetAllocation([
            'title' => 'Test title',
            'body' => 'Test body message',
            'scheme' => 'bang.kerupux.com://scheme'
        ]);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => 'Test title',
            'message' => 'Test body message',
            'scheme' => 'bang.kerupux.com://scheme'
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
