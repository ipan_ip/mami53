<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class AllocationAutomaticallyTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new AllocationAutomatically(['saldo_total' => 100000]);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class,
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(AllocationAutomatically::MESSAGE_TITLE.(100000), $result->title);
        $this->assertEquals(AllocationAutomatically::MESSAGE_BODY, $result->message);
        $this->assertEquals(AllocationAutomatically::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(AllocationAutomatically::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => AllocationAutomatically::MESSAGE_TITLE.(100000),
            'message' => AllocationAutomatically::MESSAGE_BODY,
            'scheme' => AppSchemes::getOwnerListRoom()
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
