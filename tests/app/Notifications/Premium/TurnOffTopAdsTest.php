<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class TurnOffTopAdsTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new TurnOffTopAds();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => TurnOffTopAds::NOTIF_TITLE,
            'message' => 'Iklan Teratas nonaktif untuk semua iklan Anda. Segera isi saldo Premium untuk mengaktifkan kembali.',
            'scheme' => AppSchemes::getTopupBalancePage()
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(TurnOffTopAds::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(TurnOffTopAds::WEBPUSH_BODY, $result->message);
        $this->assertEquals(TurnOffTopAds::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(TurnOffTopAds::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}