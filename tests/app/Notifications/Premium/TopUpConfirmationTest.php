<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Test\MamiKosTestCase;
use App\User;

class TopUpConfirmationTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new TopUpConfirmation([
            'saldo' => 1500000
        ]);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannelInfoBip::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => sprintf(TopUpConfirmation::SMS_MESSAGE, '1.500.000'),
        ], $result);
    }
}