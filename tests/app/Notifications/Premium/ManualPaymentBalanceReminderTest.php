<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class ManualPaymentBalanceReminderTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new ManualPaymentBalanceReminder('GoldPlus 1');
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class,
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(ManualPaymentBalanceReminder::SUBJECT, $result->title);
        $this->assertEquals(sprintf(ManualPaymentBalanceReminder::BODY, 'GoldPlus 1') . ' ' . ManualPaymentBalanceReminder::BUTTON_TEXT, $result->message);
        $this->assertEquals(ManualPaymentBalanceReminder::BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(ManualPaymentBalanceReminder::BUTTON_URL, $result->url_button_one);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        
        $this->assertEquals([
            'name' => ManualPaymentBalanceReminder::SUBJECT,
            'message' =>  implode(" ", [sprintf(ManualPaymentBalanceReminder::BODY, 'GoldPlus 1'), ManualPaymentBalanceReminder::BUTTON_TEXT]),
            'scheme' =>  AppSchemes::getOwnerListRoom()
        ], $result);
    }
}
