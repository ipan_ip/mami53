<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Test\MamiKosTestCase;
use App\User;

class MidtransPaymentTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new MidtransPayment();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannelInfoBip::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'Pembelian premium Sukses! Silakan bayar sesuai instruksi pembayaran (jangan tutup halaman hingga sukses) / cek email untuk detail cara pembayaran. Terima kasih',
        ], $result);
    }
}