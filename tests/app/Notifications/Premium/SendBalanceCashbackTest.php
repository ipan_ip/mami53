<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class SendBalanceCashbackTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new SendBalanceCashback(12000);
    }

    public function testSendCashbackNotificationViaAppChannel()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(sprintf(SendBalanceCashback::NOTIF_TITLE, number_format(12000,0,".",".")), $result->title);
        $this->assertEquals(SendBalanceCashback::NOTIF_BODY, $result->message);
        $this->assertEquals(SendBalanceCashback::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(config('owner.dashboard_url'), $result->url_button_one);
    }

    public function testToApp_ReturnNotifData()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => sprintf(SendBalanceCashback::NOTIF_TITLE, number_format(12000,0,".",".")),
            'message' => SendBalanceCashback::NOTIF_BODY,
            'scheme' => AppSchemes::getOwnerProfile()
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}