<?php

namespace App\Notifications\Premium;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;

class FreeApproveTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new FreeApprove();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            SMSChannel::class
        ], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);
        
        $this->assertEquals([
            'message' => 'MAMIKOS: Selamat paket premium gratis anda sudah di verifikasi oleh admin.'
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
