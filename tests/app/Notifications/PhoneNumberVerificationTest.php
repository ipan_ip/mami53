<?php

namespace App\Notifications;

use App\Entities\Activity\ActivationCode;
use App\Libraries\Notifications\SMSActivationCodeChannel;
use App\Notifications\PhoneNumberVerification;
use App\Test\MamiKosTestCase;
use App\User;

class PhoneNumberVerificationTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-1270
     * @group App/Notifications/PhoneNumberVerification
     */
    public function testPhoneNumberVerificationShouldReturnActivationCode()
    {
        $activationCode = factory(ActivationCode::class)->make();
        $phoneNumberVerification = new PhoneNumberVerification(
            $activationCode
        );
        $user = factory(User::class)->make();
        $this->assertEquals($activationCode, $phoneNumberVerification->activationCode);
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Notifications/PhoneNumberVerification
     */
    public function testPhoneNumberVerificationToArrayShouldReturnEmptyArray()
    {
        $activationCode = factory(ActivationCode::class)->make();
        $phoneNumberVerification = new PhoneNumberVerification(
            $activationCode
        );
        $user = factory(User::class)->make();
        $this->assertEquals([], $phoneNumberVerification->toArray($user));
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Notifications/PhoneNumberVerification
     */
    public function testPhoneNumberVerificationToSmsShouldHasMesssageKey()
    {
        $activationCode = factory(ActivationCode::class)->make();
        $phoneNumberVerification = new PhoneNumberVerification(
            $activationCode
        );
        $user = factory(User::class)->make();
        $this->assertArrayHasKey('message', $phoneNumberVerification->toSms($user));
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App/Notifications/PhoneNumberVerification
     */
    public function testPhoneNumberVerificationViaShouldUseSmsChannelInfobip()
    {
        $activationCode = factory(ActivationCode::class)->make();
        $phoneNumberVerification = new PhoneNumberVerification(
            $activationCode
        );
        $user = factory(User::class)->make();
        $this->assertEquals([SMSActivationCodeChannel::class], $phoneNumberVerification->via($user));
    }
}
