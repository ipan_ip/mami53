<?php

namespace App\Notifications;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;

class BookingNotificationTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new BookingNotification();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        $this->assertEquals([
            AppChannel::class,
            SMSChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);
        $expected = [
            'title'     =>  'Ada Booking baru di kost Anda ',
            'message'   =>  'Konfirmasi sekarang sebelum pesanan kadaluarsa. ',
            'is_mamipay' => true,
            'scheme'    =>  AppSchemes::getBookingDetail()
        ];
        $this->assertEquals($expected, $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);
        $expected = [
            'message'   =>  'Ada Booking baru di kost Anda. Konfirmasi sekarang di aplikasi Mamikos Anda sebelum pesanan kadaluarsa. ',
        ];
        $this->assertEquals($expected, $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        $expected = [];
        $this->assertEquals($expected, $result);
    }
}
