<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;
use App\User;

class PremiumDateLimitTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new PremiumDateLimit(3);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannel::class, AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_Day1_ReturnNotif()
    {
        $notif = new PremiumDateLimit(1);
        $result = $notif->toApp($this->user);

        $this->assertEquals([
            'name' => sprintf(PremiumDateLimit::WEBPUSH_SUBJECT, 1),
            'message' => PremiumDateLimit::WEBPUSH_BODY_2,
            'scheme' => AppSchemes::getPackageList()
        ], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => sprintf(PremiumDateLimit::WEBPUSH_SUBJECT, 3),
            'message' => PremiumDateLimit::WEBPUSH_BODY,
            'scheme' => AppSchemes::getPackageList()
        ], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'MAMIKOS: Membership Anda tinggal 3 hari, untuk memperpanjang silakan ke akun pemiliknya atau WA ke 0877-3922-2850.Terimakasih'
        ], $result);
    }

    public function testGetPushNotifData_Day1_ShouldSuccess()
    {
        $notif = new PremiumDateLimit(1);
        $result = $notif->getPushNotifData($this->user);

        $this->assertEquals(sprintf(PremiumDateLimit::WEBPUSH_SUBJECT, 1), $result->title);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BODY_2, $result->message);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(sprintf(PremiumDateLimit::WEBPUSH_SUBJECT, 3), $result->title);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BODY, $result->message);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(PremiumDateLimit::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}