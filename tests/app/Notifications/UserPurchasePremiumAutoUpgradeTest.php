<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Test\MamiKosTestCase;
use App\User;

class UserPurchasePremiumAutoUpgradeTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new UserPurchasePremiumAutoUpgrade(1500000);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannelInfoBip::class, AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'message' => 'Anda telah melakukan perpanjangan otomatis saat pembelian paket, Silakan lakukan pembayaran Rp 1.500.000',
            'scheme' => AppSchemes::getOwnerProfile()
        ], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'Masa aktif premium Anda segera habis. Lakukan pembayaran Rp 1.500.000 paket perpanjangan otomatis Anda atau cek request paket Anda di akun pemilik.Terima Kasih'
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(UserPurchasePremiumAutoUpgrade::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(UserPurchasePremiumAutoUpgrade::WEBPUSH_BODY . ' Rp 1.500.000', $result->message);
        $this->assertEquals(UserPurchasePremiumAutoUpgrade::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(UserPurchasePremiumAutoUpgrade::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}