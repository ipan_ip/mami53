<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class PromoApprovedTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new PromoApproved(1);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([MoEngageChannel::class, AppChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'message' => PromoApproved::WEBPUSH_BODY,
            'scheme' => AppSchemes::roomDetailId(1)
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(PromoApproved::WEBPUSH_BODY, $result->title);
        $this->assertEquals(PromoApproved::WEBPUSH_BODY, $result->message);
        $this->assertEquals(PromoApproved::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(PromoApproved::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}