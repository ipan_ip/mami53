<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class NearEndTotalBalanceTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new NearEndTotalBalance();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'message' => 'Total saldo iklan di akun Anda akan segera habis',
            'scheme'  => AppSchemes::getOwnerProfile()
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(NearEndTotalBalance::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(NearEndTotalBalance::WEBPUSH_BODY, $result->message);
        $this->assertEquals(NearEndTotalBalance::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(NearEndTotalBalance::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}