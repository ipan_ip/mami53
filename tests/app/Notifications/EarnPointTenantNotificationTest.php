<?php

namespace App\Notifications;

use App\Entities\Point\PointActivity;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class EarnPointTenantNotificationTest extends MamiKosTestCase
{
    private $activity;
    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->activity = $this->createMock(PointActivity::class);
    }

    public function testVia_AppChannelExist()
    {
        $notif = new EarnPointTenantNotification($this->activity, 1);
        
        $result = $notif->via($this->user);
        $this->assertEquals([AppChannel::class], $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_OtherActivity_ReturnEmptyNotif()
    {
        $notif = new EarnPointTenantNotification($this->activity, 1);
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('user-point-history');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $notif->getTitle(),
            'message' => $notif->getMessage($this->user),
            'scheme' => $notif->getScheme()
        ]);

        $result = $notif->toApp($this->user);
        $expected = [
            'title' => '',
            'message' => '',
            'scheme' => 'user-point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_AdjustmentTopupActivity_ReturnAppropiateNotif()
    {
        $notif = new EarnPointTenantNotification(null, 1, "admin_adjustment_topup");

        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('user-point-history');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $notif->getTitle(),
            'message' => $notif->getMessage($this->user),
            'scheme' => $notif->getScheme()
        ]);

        $result = $notif->toApp($this->user);
        $expected = [
            'title' => 'Kamu mendapat 1 poin dari Mamikos',
            'message' => 'Klik untuk cek poin yang sudah terkumpul.',
            'scheme' => 'user-point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_AdjustmentExpiryActivity_ReturnAppropiateNotif()
    {
        $notif = new EarnPointTenantNotification(null, 1, "expiry_adjustment");

        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('user-point-expired');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $notif->getTitle(),
            'message' => $notif->getMessage($this->user),
            'scheme' => $notif->getScheme()
        ]);

        $result = $notif->toApp($this->user);
        $expected = [
            'title' => '1 poin telah kedaluwarsa',
            'message' => 'Silahkan cek poin yang masih terkumpul di sini.',
            'scheme' => 'user-point-expired'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_WithEmptyAdjustmentActivity_ReturnEmptyNotif()
    {
        $notif = new EarnPointTenantNotification(null, 1);
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('user-point-history');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $notif->getTitle(),
            'message' => $notif->getMessage($this->user),
            'scheme' => $notif->getScheme()
        ]);

        $result = $notif->toApp($this->user);
        $expected = [
            'title' => '',
            'message' => '',
            'scheme' => 'user-point-history'
        ];
        $this->assertEquals($expected, $result);
    }
}
