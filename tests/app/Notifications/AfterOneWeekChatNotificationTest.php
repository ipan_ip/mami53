<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Test\MamiKosTestCase;

class AfterOneWeekChatNotificationTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new AfterOneWeekChatNotification();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_BODY, $result->message);
        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_BUTTON_ONE_TEXT, $result->text_button_one);
        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_BUTTON_ONE_URL, $result->url_button_one);
        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_BUTTON_TWO_TEXT, $result->text_button_two);
        $this->assertEquals(AfterOneWeekChatNotification::WEBPUSH_BUTTON_TWO_URL, $result->url_button_two);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
