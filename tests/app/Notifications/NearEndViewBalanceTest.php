<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Room\Room;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class NearEndViewBalanceTest extends MamiKosTestCase
{
    private $user;
    private $room;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->room = $this->createMock(Room::class);
        $this->room->method('__get')->with('song_id')->willReturn(1);

        $this->notif = new NearEndViewBalance($this->room);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name' => NearEndViewBalance::NOTIF_TITLE,
            'message' => 'Ayo, segera isi ulang agar iklan Anda tetap di posisi teratas.',
            'scheme'  => AppSchemes::roomDetail($this->room->song_id)
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(NearEndViewBalance::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(NearEndViewBalance::WEBPUSH_BODY, $result->message);
        $this->assertEquals(NearEndViewBalance::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(NearEndViewBalance::WEBPUSH_BUTTON_URL . $this->room->song_id, $result->url_button_one);
    }
}