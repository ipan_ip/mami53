<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;
use App\User;

class PremiumExpiredTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new PremiumExpired();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([AppChannel::class, SMSChannel::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToApp_ReturnNotif()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'message' => 'Perhatian! Membership Anda sudah berakhir. ',
            'scheme' => AppSchemes::getPackageList()
        ], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'MAMIKOS: Membership Anda sudah berakhir. Silakan perpanjang paket  di akun pemilik atau WA ke 0877-3922-2850. Terimakasih.'
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(PremiumExpired::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(PremiumExpired::WEBPUSH_BODY, $result->message);
        $this->assertEquals(PremiumExpired::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(PremiumExpired::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}