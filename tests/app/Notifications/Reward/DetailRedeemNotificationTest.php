<?php

namespace App\Notifications\Reward;

use App\Entities\Reward\RewardRedeem;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;

class DetailRedeemNotificationTest extends MamiKosTestCase
{
    protected $user;
    protected $redemption;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->redemption = $this->mockPartialAlternatively(RewardRedeem::class);
        $this->redemption->shouldReceive('getAttribute')->with('public_id')->andReturn(123);

        $this->redemption->shouldReceive('getAttribute')->with('reward')->andReturn((object) [
            'name' => 'iPhone'
        ]);
    }

    public function testVia_AppChannelExist()
    {
        $notif = new DetailRedeemNotification(50, $this->redemption);

        $result = $notif->via($this->user);
        $this->assertEquals([AppChannel::class], $result);
    }

    public function testToApp_OnProcessRedemption_ReturnNotif()
    {
        $this->redemption->shouldReceive('getAttribute')->with('status')->andReturn(RewardRedeem::STATUS_ONPROCESS);

        $notif = new DetailRedeemNotification(50, $this->redemption);
        $result = $notif->toApp($this->user);

        $this->assertEquals([
            'title'     => \sprintf("50 poin berhasil ditukarkan"),
            'message'   => \sprintf("Hadiah sedang diproses. Pantau prosesnya di sini >>"),
            'scheme'    => 'detail-redemption?id=123'
        ], $result);
        $this->assertEquals('50 poin berhasil ditukarkan. Hadiah sedang diproses. Cek statusnya di sini >>', $notif->getFullMessage());
        $this->assertEquals('/mamipoin/redeem/123', $notif->getUrl());
    }

    public function testToApp_SuccessRedemption_ReturnNotif()
    {
        $this->redemption->shouldReceive('getAttribute')->with('status')->andReturn(RewardRedeem::STATUS_SUCCESS);

        $notif = new DetailRedeemNotification(50, $this->redemption);
        $result = $notif->toApp($this->user);

        $this->assertEquals([
            'title'     => \sprintf("iPhone berhasil diterima"),
            'message'   => \sprintf("Langsung cek detail penukarannya di sini yuk >>"),
            'scheme'    => 'detail-redemption?id=123'
        ], $result);
        $this->assertEquals('iPhone berhasil diterima. Cek detail penukarannya di sini >>', $notif->getFullMessage());
        $this->assertEquals('/mamipoin/redeem/123', $notif->getUrl());
    }

    public function testToApp_FailedRedemption_ReturnNotif()
    {
        $this->redemption->shouldReceive('getAttribute')->with('status')->andReturn(RewardRedeem::STATUS_FAILED);

        $notif = new DetailRedeemNotification(50, $this->redemption);
        $result = $notif->toApp($this->user);

        $this->assertEquals([
            'title'     => \sprintf("Penukaran untuk iPhone gagal"),
            'message'   => \sprintf("Cek detail penukarannya di sini >>"),
            'scheme'    => 'detail-redemption?id=123'
        ], $result);
        $this->assertEquals('Penukaran untuk iPhone gagal. Cek detail penukarannya di sini >>', $notif->getFullMessage());
        $this->assertEquals('/mamipoin/redeem/123', $notif->getUrl());
    }

    public function testToApp_OtherStatus_ReturnNotif()
    {
        $this->redemption->shouldReceive('getAttribute')->with('status')->andReturn('abcdef');

        $notif = new DetailRedeemNotification(50, $this->redemption);
        $result = $notif->toApp($this->user);

        $this->assertEquals([
            'title'     => '',
            'message'   => '',
            'scheme'    => 'detail-redemption?id=123'
        ], $result);
        $this->assertEquals('', $notif->getFullMessage());
        $this->assertEquals('/mamipoin/redeem/123', $notif->getUrl());
    }
}
