<?php
namespace app\Notifications;

use App\Entities\Activity\AppSchemes;
use App\Notifications\PropertyUnverified;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;

class PropertyUnverifiedTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->room = factory(Room::class)->create([]);
        $this->class = new PropertyUnverified($this->room);
    }

    public function testgetPushNotifData()
    {
        $expectedResult =
            [
                'title' => 'Mohon Maaf, Data Kos Anda Ditolak Admin',
                'message' => $this->room->name . PropertyUnverified::WEBPUSH_BODY,
                'text_button_one' => $this->class::WEBPUSH_BUTTON_TEXT,
                'url_button_one' => config('app.url') . '/ownerpage/kos'
            ];

        $result = $this->class->getPushNotifData(null);
        $this->assertEquals($result->toArray(), $expectedResult);
    }

    public function testVia()
    {
        $expectedResult =
            [
                AppChannel::class,
                MoEngageChannel::class
            ];

        $result = $this->class->via(null);
        $this->assertEquals($result, $expectedResult);
    }

    public function testtoApp()
    {
        $expectedResult =
        [
            'title' => 'Mohon Maaf, Data Kos Anda Ditolak Admin',
            'message' => $this->room->name . ' tidak dapat diverifikasi. Silakan lihat detail dan perbaiki datanya di Properti Saya.',
            'scheme' => AppSchemes::getOwnerListRoom()
        ];

        $result = $this->class->toApp(null);
        $this->assertEquals($result, $expectedResult);
    }

    public function testtoArray()
    {
        $result = $this->class->toArray(null);
        $this->assertEquals($result, []);
    }
}
