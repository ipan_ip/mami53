<?php

namespace App\Notifications;

use App\Entities\Notif\MamiKosPushNotifData;
use App\Test\MamiKosTestCase;
use App\User;

class MamiKosNotificationTest extends MamiKosTestCase
{
    private $pushNotifData;
    private $notifiable;
    private $attrMapping = [];

    protected function setUp() : void
    {
        parent::setUp();

        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->pushNotifData->method('__set')->will($this->returnCallback(function ($key, $value) {
            $this->attrMapping[$key] = $value;
            return null;
        }));
        $this->pushNotifData->method('__get')->will($this->returnCallback(function ($key) {
            return $this->attrMapping[$key];
        }));
        $this->notifiable = $this->createMock(User::class);
    }

    public function testSetPushNotifData()
    {
        $notification = new MamiKosNotification();
        $this->assertSame(null, $this->getNonPublicAttributeFromClass('pushNotifData', $notification));

        $notification->setPushNotifData($this->pushNotifData);
        $this->assertSame($this->pushNotifData, $this->getNonPublicAttributeFromClass('pushNotifData', $notification));
    }

    public function testGetPushNotifData()
    {
        $notification = new MamiKosNotification();
        $notification->getPushNotifData($this->notifiable);
        $this->assertInstanceOf(MamiKosPushNotifData::class, $this->getNonPublicAttributeFromClass('pushNotifData', $notification));
    }

    public function testUpdatePushNotifData_responseSuccess()
    {
        $this->pushNotifData->expects($this->once())->method('save')->willReturn($this->anything());

        $apiResponse = [
            'responseId' => 'xyz',
            'status' => 'Success'
        ];
        $notification = new MamiKosNotification();
        $notification->setPushNotifData($this->pushNotifData);
        $notification->updatePushNotifData($apiResponse);
        $this->assertEquals(MamiKosPushNotifData::STATUS_SENT, $this->pushNotifData->status);
    }

    public function testUpdatePushNotifData_responseNotSuccess()
    {
        $this->pushNotifData->expects($this->once())->method('save')->willReturn($this->anything());

        $apiResponse = [
            'responseId' => 'xyz',
            'status' => 'Failed'
        ];
        $notification = new MamiKosNotification();
        $notification->setPushNotifData($this->pushNotifData);
        $notification->updatePushNotifData($apiResponse);
        $this->assertEquals(MamiKosPushNotifData::STATUS_FAILED, $this->pushNotifData->status);
    }

    public function testToMoEngage()
    {
        $this->notifiable->method('__get')->will($this->returnCallback(function ($key) {
            if ($key === 'id') {
                return 1;
            }
            return $this->anything();
        }));

        $notification = new MamiKosNotification();
        $result = $notification->toMoEngage($this->notifiable);
        $this->assertIsArray($result);
    }
}
