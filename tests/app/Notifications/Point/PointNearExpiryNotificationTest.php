<?php

namespace App\Notifications\Point;

use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Mockery;

class PointNearExpiryNotificationTest extends MamiKosTestCase
{
    private $notif;
    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new PointNearExpiryNotification(50, Carbon::now());
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        $this->assertEquals([AppChannel::class], $result);
    }

    public function testToApp_ForOwner_ReturnNotif()
    {
        $this->user->method('isOwner')->willReturn(true);

        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'title'     => 'Ada poin yang mau kedaluwarsa nih',
            'message'   => sprintf("%d poin Anda akan kedaluwarsa pada %s. Yuk dipakai sebelum hangus.", 50, Carbon::now()->format('d M Y')),
            'scheme'    => 'point-history'
        ], $result);
    }

    public function testToApp_ForTenant_ReturnNotif()
    {
        $this->user->method('isOwner')->willReturn(false);

        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'title'     => 'Ada poin yang mau kedaluwarsa nih',
            'message'   => sprintf("%d poin Kamu akan kedaluwarsa pada %s. Yuk dipakai sebelum hangus.", 50, Carbon::now()->format('d M Y')),
            'scheme'    => 'user-point-expired'
        ], $result);
    }

    public function testGetUrl_ForOwner_ReturnUrl()
    {
        $this->user->method('isOwner')->willReturn(true);

        $result = $this->notif->getUrl($this->user);

        $this->assertEquals('/mamipoin?tab=1', $result);
    }

    public function testGetUrl_ForTenant_ReturnUrl()
    {
        $this->user->method('isOwner')->willReturn(false);

        $result = $this->notif->getUrl($this->user);

        $this->assertEquals('/user/mamipoin/expired', $result);
    }
}