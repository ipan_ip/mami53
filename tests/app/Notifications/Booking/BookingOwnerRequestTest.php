<?php

namespace App\Notifications\Booking;

use App\Entities\Activity\AppSchemes;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;

class BookingOwnerRequestTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new BookingOwnerRequest(['message' => 'Test message', 'message_detail' => 'Test message detail']);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            AppChannel::class
        ], $result);
    }

    public function testToApp_ReturnNotifData()
    {
        $result = $this->notif->toApp($this->user);

        $this->assertEquals([
            'name'      => 'Test message',
            'message'   => 'Test message detail',
            'is_mamipay' => true,
            'scheme'    => AppSchemes::getNotificationPage()
        ], $result);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
