<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Notifications\Booking\RoomNotAvailable;
use App\Test\MamiKosTestCase;

class RoomNotAvailableTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new RoomNotAvailable();
    }

    public function testGetPushNotifData()
    {
        $result = $this->class->getPushNotifData(null);
        $this->assertEquals(RoomNotAvailable::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(RoomNotAvailable::WEBPUSH_BODY, $result->message);
    }

    public function testVia()
    {
        $result = $this->class->via(null);
        $this->assertEquals(MoEngageChannel::class, $result[0]);
    }

    public function testToArray()
    {
        $result = $this->class->toArray(null);
        $this->assertEquals([], $result);
    }
}
