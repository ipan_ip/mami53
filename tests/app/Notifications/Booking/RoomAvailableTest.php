<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Notifications\Booking\RoomAvailable;
use App\Test\MamiKosTestCase;

class RoomAvailableTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new RoomAvailable();
    }

    public function testGetPushNotifData()
    {
        $result = $this->class->getPushNotifData(null);
        $this->assertEquals(RoomAvailable::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(RoomAvailable::WEBPUSH_BODY, $result->message);
    }

    public function testVia()
    {
        $result = $this->class->via(null);
        $this->assertEquals(MoEngageChannel::class, $result[0]);
    }

    public function testToArray()
    {
        $result = $this->class->toArray(null);
        $this->assertEquals([], $result);
    }
}
