<?php

namespace app\Notifications;

use App\Entities\Activity\AppSchemes;
use App\Notifications\Booking\BookingOwnerRequestByAdmin;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class BookingOwnerRequestByAdminTest extends MamiKosTestCase
{
    use WithFaker;
    private $notification;
    private $message;
    private $message_detail;

    protected function setUp() : void
    {
        $this->setUpFaker();

        $this->message = $this->faker->sentence(3);
        $this->message_detail = $this->faker->sentence(6);

        $data = [
            'message' => $this->message,
            'message_detail' => $this->message_detail,
        ];

        $this->notification = new BookingOwnerRequestByAdmin($data);
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testVia()
    {
        $result = $this->notification->via(null);
        $this->assertEquals(AppChannel::class, $result[0]);
    }

    public function testToApp()
    {
        $result = $this->notification->toApp(null);
        $expectedResult = [
            'name' => $this->message,
            'message' => $this->message_detail,
            'scheme' => AppSchemes::getOwnerprofileMamipay()
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToArray()
    {
        $result = $this->notification->toArray(null);
        $expectedResult = [];
        $this->assertEquals($expectedResult, $result);
    }
}
