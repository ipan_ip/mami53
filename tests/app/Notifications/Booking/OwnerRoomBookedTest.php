<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\Booking\OwnerRoomBooked;
use App\Test\MamiKosTestCase;
use App\Entities\Activity\AppSchemes;

class OwnerRoomBookedTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new OwnerRoomBooked();
    }

    public function testGetPushNotifData()
    {
        $result = $this->class->getPushNotifData(null);
        $this->assertEquals(OwnerRoomBooked::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(OwnerRoomBooked::WEBPUSH_BODY, $result->message);
        $this->assertEquals(OwnerRoomBooked::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(OwnerRoomBooked::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }

    public function testVia()
    {
        $result = $this->class->via(null);
        $this->assertEquals(
            [
                AppChannel::class,
                MoEngageChannel::class
            ],
            $result
        );
    }

    public function testToApp()
    {
        $result = $this->class->toApp(null);
        $expectedResult = [
            'name' => 'Ada yang mau booking kos!',
            'message' => 'Konfirmasi sekarang sebelum waktunya habis!',
            'scheme' => AppSchemes::getPackageList()
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToArray()
    {
        $result = $this->class->toArray(null);
        $this->assertEquals([], $result);
    }
}
