<?php

namespace App\Notifications\Booking;

use App\Channel\MoEngage\MoEngageChannel;
use App\Test\MamiKosTestCase;

class BookingVerifiedTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->mockPartialAlternatively(User::class);

        $this->notif = new BookingVerified();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);
        
        $this->assertEquals([
            MoEngageChannel::class
        ], $result);
    }

    public function testGetPushNotifData_ReturnNotifData()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(BookingVerified::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(BookingVerified::WEBPUSH_BODY, $result->message);
    }

    public function testToArray_ReturnNotif()
    {
        $result = $this->notif->toArray($this->user);
        
        $this->assertEquals([], $result);
    }
}
