<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Notifications\Booking\RoomBooked;
use App\Test\MamiKosTestCase;

class RoomBookedTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $this->notification = new RoomBooked();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetPushNotifData()
    {
        $result = $this->notification->getPushNotifData(null);

        $this->assertEquals(RoomBooked::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(RoomBooked::WEBPUSH_BODY, $result->message);
    }

    public function testVia()
    {
        $result = $this->notification->via(null);
        $this->assertEquals([MoEngageChannel::class], $result);
    }

    public function testToArray()
    {
        $result = $this->notification->toArray(null);
        $this->assertEquals([], $result);
    }
}
