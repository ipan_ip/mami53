<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Test\MamiKosTestCase;
use App\User;
use App\Notifications\IncompletedInput;

class IncompletedInputTest extends MamiKosTestCase
{
    private $notifiable;
    private $pushNotifData;

    protected function setUp() : void
    {
        parent::setUp();
        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->notifiable = $this->createMock(User::class);
    }

    public function testToArray()
    {
        $notification = new IncompletedInput();

        $this->assertEmpty($notification->toArray($this->notifiable));
    }

    public function testVia()
    {
        $notification = new IncompletedInput();

        $this->assertContains(MoEngageChannel::class, $notification->via($this->notifiable));
    }

    public function testGetPushNotifData()
    {
        $notification = new IncompletedInput();
        $notification->getPushNotifData($this->notifiable);

        $this->assertInstanceOf(MamiKosPushNotifData::class, $this->getNonPublicAttributeFromClass('pushNotifData', $notification));
    }

}