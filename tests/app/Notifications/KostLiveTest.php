<?php

namespace app\Notifications;

use App\Notifications\KostLive;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;

class KostLiveTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetPushNotifData()
    {
        $this->setupKostLive();

        $title = sprintf(KostLive::WEBPUSH_SUBJECT, $this->propertyType);
        $body = sprintf(KostLive::WEBPUSH_BODY, $this->room->name);

        $result = $this->notification->getPushNotifData(null);
        $this->assertEquals($title, $result->title);
        $this->assertEquals($body, $result->message);
        $this->assertEquals(KostLive::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(config('app.url') . '/ownerpage/kos', $result->url_button_one);
    }

    public function testVia()
    {
        $this->setupKostLive();

        $result = $this->notification->via(null);
        $expectedResult = [
            AppChannel::class,
            MoEngageChannel::class
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToApp()
    {
        $this->setupKostLive();

        $title = sprintf(KostLive::WEBPUSH_SUBJECT, $this->propertyType);
        $body = sprintf(KostLive::WEBPUSH_BODY, $this->room->name);

        $result = $this->notification->toApp(null);
        $expectedResult = [
            'title' => $title,
            'message' => $body,
            'scheme' => AppSchemes::getOwnerListRoom(),
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToArray()
    {
        $this->setupKostLive();

        $result = $this->notification->toArray(null);
        $this->assertEquals($result, []);
    }

    private function setupKostLive()
    {
        $this->room = factory(Room::class)->create();
        $this->notification = new KostLive($this->room);
        $this->propertyType = is_null($this->room->apartment_project_id) ? 'Kos' : 'Apartemen';
    }
}
