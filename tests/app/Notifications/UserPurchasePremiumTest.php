<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Premium\Bank;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Test\MamiKosTestCase;
use App\User;

class UserPurchasePremiumTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        factory(Bank::class)->create([
            "is_active" => 1,
            "name" => "Mandiri",
            "number" => "123456"
        ]);

        $this->notif = new UserPurchasePremium(1500000);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannelInfoBip::class, MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'Silakan lakukan pembayaran Rp1.500.000 ke Rek Mandiri:123456. Terima kasih'
        ], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(UserPurchasePremium::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(UserPurchasePremium::WEBPUSH_BODY, $result->message);
        $this->assertEquals(UserPurchasePremium::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(UserPurchasePremium::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}