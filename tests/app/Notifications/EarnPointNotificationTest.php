<?php

namespace App\Notifications;

use App\Entities\Point\PointActivity;
use App\Libraries\Notifications\AppChannel;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class EarnPointNotificationTest extends MamiKosTestCase
{
    private $activity;
    private $class;
    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->activity = $this->createMock(PointActivity::class);
        $this->class = new EarnPointNotification($this->activity, 1);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->class->via($this->user);
        $this->assertEquals([AppChannel::class], $result);
    }
    
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_JoinBookingActivity_ReturnAppropiateNotif()
    {
        $this->user->method('__get')->with('name')->willReturn('john');
        $this->activity->method('__get')->with('key')->willReturn('owner_join_bbk');
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => $this->class->getScheme()
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => '1 poin diterima dari Booking Langsung',
            'message' => 'john, ayo cek poin yang sudah terkumpul di sini.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_JoinGoldplusActivity_ReturnAppropiateNotif()
    {
        $this->user->method('__get')->with('name')->willReturn('john');
        $this->activity->method('__get')->with('key')->willReturn('owner_join_goldplus');
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => 'point-history'
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => '1 poin diterima dari aktivasi Mamikos GoldPlus',
            'message' => 'john, ayo cek poin yang sudah terkumpul di sini.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_RoomJoinGoldplusActivity_ReturnAppropiateNotif()
    {
        $this->user->method('__get')->with('name')->willReturn('john');
        $this->activity->method('__get')->with('key')->willReturn('owner_room_join_goldplus');
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => 'point-history'
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => '1 poin diterima dari daftar kamar GoldPlus',
            'message' => 'john, ayo cek poin yang sudah terkumpul di sini.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_AdjustmentTopupActivity_ReturnAppropiateNotif()
    {
        $this->class = new EarnPointNotification(null, 1, "admin_adjustment_topup");
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => $this->class->getScheme()
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => 'Anda mendapat 1 poin dari Mamikos',
            'message' => 'Cek poin yang sudah terkumpul di sini.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_AdjustmentTopdownActivity_ReturnAppropiateNotif()
    {
        $this->class = new EarnPointNotification(null, 1, "admin_adjustment_topdown");
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => $this->class->getScheme()
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => 'Terdapat penyesuaian poin dari Mamikos',
            'message' => 'Klik untuk lihat detail poin Anda.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_EmptyAdjustmentActivity_ReturnEmptyNotif()
    {
        $this->class = new EarnPointNotification(null, 1, null);
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => $this->class->getScheme()
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => '',
            'message' => '',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testToApp_AdjustmentExpiry_ReturnAppropiateNotif()
    {
        $this->class = new EarnPointNotification(null, 1, "expiry_adjustment");
        
        $notifData = Mockery::mock('overload:App\Entities\Notif\NotifData');
        $notifData->shouldReceive('buildFromParam')->andReturn($notifData);
        $notifData->shouldReceive('scheme')->andReturn('scheme://test');
        $notifData->shouldReceive('toArray')->andReturn([
            'title' => $this->class->getTitle(),
            'message' => $this->class->getMessage($this->user),
            'scheme' => $this->class->getScheme()
        ]);


        $result = $this->class->toApp($this->user);
        $expected = [
            'title' => '1 poin Anda telah kedaluwarsa',
            'message' => 'Silahkan cek poin yang masih terkumpul di sini.',
            'scheme' => 'point-history'
        ];
        $this->assertEquals($expected, $result);
    }
}
