<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Test\MamiKosTestCase;
use App\User;

class MoEngageSimpleNotificationTest extends MamiKosTestCase
{
    private $notifiable;

    protected function setUp() : void
    {
        parent::setUp();
        $this->notifiable = $this->createMock(User::class);
    }

    public function testGetPushNotifData()
    {
        $notification = new MoEngageSimpleNotification(1, 'title', 'message');
        $this->assertInstanceOf(MamiKosPushNotifData::class, $notification->getPushNotifData($this->notifiable));
    }

    public function testVia()
    {
        $notification = new MoEngageSimpleNotification(1, 'title', 'message');
        $this->assertContains(MoEngageChannel::class, $notification->via($this->notifiable));
    }

    public function testToArray()
    {
        $notification = new MoEngageSimpleNotification(1, 'title', 'message');
        $this->assertIsArray($notification->toArray($this->notifiable));
    }
}
