<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Test\MamiKosTestCase;
use App\User;

class NearEndPremiumDateTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new NearEndPremiumDate();
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testGetPushNotifData_ShouldSuccess()
    {
        $result = $this->notif->getPushNotifData($this->user);

        $this->assertEquals(NearEndPremiumDate::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(NearEndPremiumDate::WEBPUSH_BODY, $result->message);
        $this->assertEquals(NearEndPremiumDate::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(NearEndPremiumDate::WEBPUSH_BUTTON_URL, $result->url_button_one);
    }
}