<?php

namespace App\Notifications;

use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;
use App\User;

class ReminderPremiumExpiredBalanceTest extends MamiKosTestCase
{
    private $user;
    private $notif;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->notif = new ReminderPremiumExpiredBalance(1000);
    }

    public function testVia_AppChannelExist()
    {
        $result = $this->notif->via($this->user);

        $this->assertEquals([SMSChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $result = $this->notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testToSms_ReturnNotif()
    {
        $result = $this->notif->toSms($this->user);

        $this->assertEquals([
            'message' => 'MAMIKOS: Sisa saldo premium/trial anda 1000. Gunakan saldo lagi dgn Paket premium/trial di https://mamikos.com/pp '
        ], $result);
    }
}