<?php

namespace app\Notifications;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Notifications\ThanosNotification;
use App\User;

class ThanosNotificationTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetPushNotifData()
    {
        $this->setupThanos();

        $result = $this->notification->getPushNotifData(null);
        $this->assertEquals(ThanosNotification::WEBPUSH_SUBJECT, $result->title);
        $this->assertEquals(ThanosNotification::WEBPUSH_BODY, $result->message);
        $this->assertEquals(ThanosNotification::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(config('app.url') . '/ownerpage/kos', $result->url_button_one);
    }

    public function testVia()
    {
        $this->setupThanos();

        $result = $this->notification->via(null);
        $expectedResult = [
            AppChannel::class,
            MoEngageChannel::class
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToApp()
    {
        $this->setupThanos();

        $result = $this->notification->toApp(null);
        $expectedResult = [
            'title' => ThanosNotification::WEBPUSH_SUBJECT,
            'message' => ThanosNotification::WEBPUSH_BODY,
            'scheme' => AppSchemes::getOwnerListRoom(),
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testToArray()
    {
        $this->setupThanos();

        $result = $this->notification->toArray(null);
        $this->assertEquals($result, []);
    }

    private function setupThanos()
    {
        $this->user = factory(User::class)->create();
        $this->room = factory(Room::class)->state('active')->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $this->room->id,
            'user_id' => $this->user->id
        ]);
        $this->thanosHidden = factory(ThanosHidden::class)->create([
            'designer_id' => $this->room->id
        ]);

        $this->notification = new ThanosNotification($this->thanosHidden);
    }
}
