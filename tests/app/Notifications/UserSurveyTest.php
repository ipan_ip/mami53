<?php

namespace App\Notifications;

use App\Entities\Room\Room;
use App\Channel\MoEngage\MoEngageChannel;
use App\Test\MamiKosTestCase;
use App\User;

class UserSurveyTest extends MamiKosTestCase
{
    private $user;
    private $room;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);
        $this->room = $this->createMock(Room::class);
        
    }

    public function testVia_AppChannelExist()
    {
        $notif = new UserSurvey($this->room);
        $result = $notif->via($this->user);

        $this->assertEquals([MoEngageChannel::class], $result);
    }

    public function testToArray_ReturnEmpty()
    {
        $notif = new UserSurvey($this->room);
        $result = $notif->toArray($this->user);

        $this->assertEquals([], $result);
    }

    public function testGetPushNotifData_WithNewSurveyType_ShouldSuccess()
    {
        $notif = new UserSurvey($this->room);
        $result = $notif->getPushNotifData($this->user);

        $this->assertEquals(UserSurvey::WEBPUSH_NEW_SURVEY_SUBJECT, $result->title);
        $this->assertEquals(UserSurvey::WEBPUSH_BODY, $result->message);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_URL . $this->room->song_id, $result->url_button_one);
    }

    public function testGetPushNotifData_WithUpdateSurveyType_ShouldSuccess()
    {
        $notif = new UserSurvey($this->room, 'update');
        $result = $notif->getPushNotifData($this->user);

        $this->assertEquals(UserSurvey::WEBPUSH_UPDATE_SURVEY_SUBJECT, $result->title);
        $this->assertEquals(UserSurvey::WEBPUSH_BODY, $result->message);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_URL . $this->room->song_id, $result->url_button_one);
    }

    public function testGetPushNotifData_WithOtherSurveyType_ShouldSuccess()
    {
        $notif = new UserSurvey($this->room, 'abcdef');
        $result = $notif->getPushNotifData($this->user);

        $this->assertEquals('', $result->title);
        $this->assertEquals(UserSurvey::WEBPUSH_BODY, $result->message);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_TEXT, $result->text_button_one);
        $this->assertEquals(UserSurvey::WEBPUSH_BUTTON_URL . $this->room->song_id, $result->url_button_one);
    }
}