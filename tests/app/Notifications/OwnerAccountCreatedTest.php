<?php

namespace app\Notifications;

use App\Libraries\Notifications\SMSChannel;
use App\Test\MamiKosTestCase;
use App\Notifications\OwnerAccountCreated;
use App\User;

class OwnerAccountCreatedTest extends MamiKosTestCase
{
    private $notifiable;

    protected function setUp() : void
    {
        parent::setUp();
        $this->notifiable = $this->createMock(User::class);
    }

    public function testToSMS()
    {
        $ownerAccountCreated = new OwnerAccountCreated('link');

        $this->assertEquals('MAMIKOS: No.Telp anda sudah terdaftar di akun Mamikos. ' .
            'Selanjutnya setting password anda di https://mamikos.com/p/'.'link', $ownerAccountCreated->toSms($this->notifiable)['message']);
    }

    public function testVia()
    {
        $ownerAccountCreated = new OwnerAccountCreated('link');
        $this->assertContains(SMSChannel::class, $ownerAccountCreated->via($this->notifiable));
    }


    public function testToArray()
    {
        $ownerAccountCreated = new OwnerAccountCreated('link');

        $this->assertEmpty($ownerAccountCreated->toArray($this->notifiable));
    }


}