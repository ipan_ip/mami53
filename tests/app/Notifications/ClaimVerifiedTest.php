<?php

namespace app\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Notifications\ClaimVerified;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class ClaimVerifiedTest extends MamiKosTestCase
{
    private $notifiable;
    private $pushNotifData;

    protected function setUp() : void
    {
        parent::setUp();
        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->notifiable = $this->createMock(User::class);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testToArray()
    {
        $notification = new ClaimVerified();

        $this->assertEmpty($notification->toArray($this->notifiable));
    }

    public function testVia()
    {
        $notification = new ClaimVerified();

        $this->assertContains(MoEngageChannel::class, $notification->via($this->notifiable));
    }

    public function testGetPushNotifData()
    {
        $notification = new ClaimVerified();
        $notification->getPushNotifData($this->notifiable);

        $this->assertInstanceOf(MamiKosPushNotifData::class, $this->getNonPublicAttributeFromClass('pushNotifData', $notification));
    }

}