<?php

namespace App\Entities\Squarepants;

use App\Entities\Room\Room;
use App\Entities\Squarepants\AgentData;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SquarepantsTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testRoomRelationship()
    {
        $agentData = factory(AgentData::class)->create();

        $room = factory(Room::class)->create();

        $agentData->setRelation('room', $room);

        $this->assertEquals($room->id, $agentData->room->id);
    }
}