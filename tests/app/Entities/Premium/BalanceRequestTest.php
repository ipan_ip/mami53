<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumCashbackHistory;

class BalanceRequestTest extends MamiKosTestCase
{

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }
 
    public function testGetPackageBalanceEmpty()
    {
        $balancePackage = BalanceRequest::getPackageBalance(1);
        $this->assertNull($balancePackage);
    }

    public function testGetPremiumPackageFromBalanceRequest()
    {
        $package = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::BALANCE_TYPE
        ]);

        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_package_id' => $package->id
        ]);

        $balancePackage = BalanceRequest::getPackageBalance($balanceRequest->id);
        $this->assertInstanceOf(PremiumPackage::class, $balancePackage);
    }

    public function testGetEmptyPriceExistingPayment()
    {
        $price = BalanceRequest::getPriceFromExistingPayment($this->user, null, 1);

        $this->assertEquals(0, $price);
    }

    public function testGetEmptyPriceExistingPaymentScenario2()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'expired_status' => null
        ]);
        $premiumPackage = factory(PremiumPackage::class)->create();

        $price = BalanceRequest::getPriceFromExistingPayment($this->user, $premiumRequest, $premiumPackage);

        $this->assertEquals(0, $price);
    }

    public function testRelationCashbackHistory()
    {
        $balanceRequest = factory(BalanceRequest::class)->create();
        
        factory(PremiumCashbackHistory::class)->create([
            'reference_id' => $balanceRequest->id,
            'type' => PremiumCashbackHistory::CASHBACK_TYPE_TOPUP
        ]);

        $this->assertInstanceOf(PremiumCashbackHistory::class, $balanceRequest->cashback_history);
    }

    public function testGetUnconfirmedStatus()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'expired_status' => null
        ]);

        $ids = BalanceRequest::getUnconfirmedStatus($premiumRequest);

        $this->assertContains($balanceRequest->id, $ids);
    }

    public function testGetUnconfirmedStatusWithTrashed()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'expired_status' => null
        ]);

        $balanceRequest->delete();

        $ids = BalanceRequest::getUnconfirmedStatus($premiumRequest, true);

        $this->assertContains($balanceRequest->id, $ids);
    }
}