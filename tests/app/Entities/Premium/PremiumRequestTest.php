<?php
namespace App\Entities\Premium;

use App\Entities\Room\ThanosHidden;
use Mockery;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Room\Room;
use App\Notifications\NearEndTotalBalance;
use App\Test\MamiKosTestCase;
use Notification;
use App\Entities\User\Notification as NotifEntity;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class PremiumRequestTest extends MamiKosTestCase
{
    
    protected $premiumRequest;

    public function setUp() : void
    {
        parent::setUp();
        $this->premiumRequest = app()->make(PremiumRequest::class);
    }

    public function testRelationToPremiumExecutiveUser()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create(['request_user_id' => $user->id]);

        $this->assertSame($user->id, $premiumRequest->request_user_id);
    }

    public function testGetPremiumRequestByPackage()
    {
        $user = factory(\App\User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id,
            "status" => "1"
        ]);

        $result = PremiumRequest::getPremiumRequestByPackage([
            "user_id" => $user->id,
            "package_id" => $premiumPackage->id
        ]);

        $this->assertEquals($premiumRequest->id, $result->id);
    }

    public function testGetPremiumRequestBeforeLastOneSuccess()
    {
        $user = factory(\App\User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id,
            "status" => "1"
        ]);

        $lastPremiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id,
            "status" => "1"
        ]);

        $result = PremiumRequest::premiumRequestBeforeLastOne($lastPremiumRequest);
        $this->assertEquals($premiumRequest->id, $result->id);
    }

    public function testIncreaseView()
    {
        // setup
        $user = factory(User::class)->create();
        $existingPremiumRequest = factory(PremiumRequest::class)->create([
            'view' => 8000,
            'used' => 5000,
            'allocated' => 7000,
            'user_id' => $user->id
        ]);
        $newAllocation = 1000;
        $newUsed = 2000;
        $premiumRequest = app()->make(PremiumRequest::class);
        // use fake notification
        Notification::fake();

        $premiumRequest->increaseView($existingPremiumRequest->id, $newAllocation, $newUsed);

        Notification::assertSentTo(
            $user, NearEndTotalBalance::class
        );
        $updatedRequest = $premiumRequest->where('id', $existingPremiumRequest->id)->first();
        $this->assertEquals($existingPremiumRequest->used + $newUsed, $updatedRequest->used);
        $this->assertEquals($existingPremiumRequest->allocated - $newAllocation, $updatedRequest->allocated);
    }

    public function testIncreaseViewWhenPremiumIsNull()
    {
        $increaseView = (new PremiumRequest)->increaseView(2, 1000, 2000);

        $this->assertNull($increaseView);
    }

    public function testLastActiveRequest()
    {
        $user = factory(User::class)->create();
        $userPremiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'expired_date' => null,
            'status' => '1'
        ]);
        $lastActiveRequest = $this->premiumRequest::lastActiveRequest($user);
        $this->assertEquals($userPremiumRequest->id, $lastActiveRequest->id);
    }

    public function testLastActiveRequestMidtrans()
    {
        $user = factory(User::class)->create();
        $user = [
            'user_id' => $user->id
        ];
        $userPremiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user['user_id'],
            'expired_date' => null,
            'status' => '1'
        ]);
        $lastActiveRequest = $this->premiumRequest::lastActiveRequest($user, 'midtrans');
        $this->assertEquals($userPremiumRequest->id, $lastActiveRequest->id);
    }

    public function testLastPremiumRequest()
    {
        $user = factory(User::class)->create();
        $existingPremiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'expired_status' => null
        ]);
        $lastActiveRequest = $this->premiumRequest::LastPremiumRequest($user);
        $this->assertEquals($existingPremiumRequest->id, $lastActiveRequest->id);
    }

    public function testLastPremiumRequestNoData()
    {
        $user = factory(User::class)->create();
        $lastActiveRequest = $this->premiumRequest::LastPremiumRequest($user);
        $this->assertEquals(null, $lastActiveRequest);
    }

    public function testExpiredStatusDelete()
    {
        $existingPremiumRequest = factory(PremiumRequest::class)->create();
        $data = [
            'premium_request_id' => $existingPremiumRequest->id
        ];
        $updatedPremiumRequest = $this->premiumRequest::expiredStatusDelete($data);
        $this->assertEquals(null, $updatedPremiumRequest->expired_status);
        $this->assertEquals(null, $updatedPremiumRequest->expired_date);
    }

    public function testDeleteFalseExpired()
    {
        $user = factory(User::class)->create();
        $existingPremiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'expired_status' => 'false',
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING
        ]);
        $this->premiumRequest::deleteFalseExpired($user);
        $updatedPremiumRequest = PremiumRequest::where('id', $existingPremiumRequest->id)->first();
        $this->assertTrue(is_null($updatedPremiumRequest));
    }

    public function testPremiumRequestAllocation1PremiumRequest()
    {
        $user = factory(User::class)->create();
        $newPremiumRequest = factory(PremiumRequest::class)->create([
            'expired_status' => null,
            'user_id' => $user->id,
            'used' => 0,
            'allocated' => 0,
            'daily_allocation' => 1,
        ]);
        
        $updatedPremiumRequest = $this->premiumRequest->premiumRequestAllocation($user->id, $newPremiumRequest);
        $this->assertEquals(0, $updatedPremiumRequest->used);
        $this->assertEquals(0, $updatedPremiumRequest->allocated);
    }

    public function testPremiumRequestAllocation2PremiumRequest()
    {
        $user = factory(User::class)->create();
        // existing
        $existingPremiumRequest = factory(PremiumRequest::class)->create([
            'expired_status' => null,
            'status' => '1',
            'user_id' => $user->id,
            'used' => 1000,
            'allocated' => 1500,
            'view' => 500
        ]);
        $existingViewPromote = factory(ViewPromote::class)->create([
            'premium_request_id' => $existingPremiumRequest->id
        ]);
        $newPremiumRequest = factory(PremiumRequest::class)->create([
            'expired_status' => null,
            'status' => '0',
            'user_id' => $user->id,
            'used' => 0,
            'allocated' => 0,
            'view' => 500
        ]);
        $updatedPremiumRequest = $this->premiumRequest->premiumRequestAllocation($user->id, $newPremiumRequest);
        $this->assertEquals(1000, $updatedPremiumRequest->used);
        $this->assertEquals(1500, $updatedPremiumRequest->allocated);
        $this->assertEquals(1000, $updatedPremiumRequest->view);

        $updatedViewPromote = ViewPromote::where('id', $existingViewPromote->id)->first();
        $this->assertEquals($newPremiumRequest->id, $updatedViewPromote->premium_request_id);

    }

    public function testUpdatePremiumRequest()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $data = [
            'view' => 2000,
            'used' => 1000,
            'allocated' => 1500
        ];
        $result = $this->premiumRequest::updatePremiumRequest($premiumRequest, $data);
        $this->assertEquals(2000, $result->view);
        $this->assertEquals(1000, $result->used);
        $this->assertEquals(1500, $result->allocated);
    }

    public function testStore()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();
        $this->premiumRequest::store($premiumPackage, $user);
        $queriedPremiumRequest = PremiumRequest::where('user_id', $user->id)
            ->where('premium_package_id', $premiumPackage->id)
            ->first();
        $this->assertEquals($user->id, $queriedPremiumRequest->user_id);
        $this->assertEquals($premiumPackage->id, $queriedPremiumRequest->premium_package_id);
    }

    public function testIsEmptyBalance()
    {
        $premiumRequest = factory(PremiumRequest::class)->make([
            'view' => 1000,
            'used' => 1000
        ]);
        $result = $this->premiumRequest->isEmptyBalance();
        $this->assertTrue($result);
    }

    public function testAutoUpgradeAfterExpiredFailed()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => "false",
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id
        ]);

        $result = PremiumRequest::AutoUpgradeAfterExpired([
            "user" => $user,
            "package" => $premiumRequest->premium_package
        ]);
        $this->assertFalse($result);
    }

    public function testAutoUpgradeAfterExpiredSuccess()
    {
        $user = factory(User::class)->create();

        $premiumPackage = factory(PremiumPackage::class)->create([
            "sale_limit_date" => date('Y-m-d', strtotime('+1 days')),
            "total_day" => 30,
            "sale_price" => 0,
            "price" => 1000,
            "view" => 1000
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        $result = PremiumRequest::AutoUpgradeAfterExpired([
            "user" => $user,
            "package" => $premiumRequest->premium_package
        ]);
        $this->assertTrue($result);
    }

    public function testAutoUpgradeAfterExpiredShouldReturnFalse()
    {
        $user = factory(User::class)->create();

        $premiumPackage = factory(PremiumPackage::class)->create([
            "sale_limit_date" => date('Y-m-d', strtotime('-1 days')),
            "total_day" => 30,
            "sale_price" => 0,
            "price" => 1000,
            "view" => 1000
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "user_id" => $user->id,
            "premium_package_id" => $premiumPackage->id
        ]);

        $result = PremiumRequest::AutoUpgradeAfterExpired([
            "user" => $user,
            "package" => $premiumRequest->premium_package
        ]);
        $this->assertFalse($result);
    }

    public function testScopeConfirmedByAdmin()
    {
        $user = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'expired_date' => null,
            'expired_status' => null,
            'user_id' => $user->id,
            'status' => '1',
            'premium_package_id' => factory(PremiumPackage::class)->create([
                'for' => 'trial',
            ]),
            'deleted_at' => null,
            'auto_upgrade' => null,
            'request_user_id' => null
        ]);

        $res = PremiumRequest::where('user_id', $user->id)
            ->confirmedByAdmin()
            ->first()
            ->toArray();

        $this->assertEquals($premiumRequest->toArray(), $res);
    }
    
    public function testsetPopupDailyAllocationState()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id
        ]);
        
        $premiumRequest->setPopupDailyAllocationState();
        Cache::shouldReceive('forever')
            ->once()
            ->with('showPopupDailyAllocation:'.$user->id)
            ->andReturn(true);
        
        $this->assertTrue(Cache::forever('showPopupDailyAllocation:'.$user->id));
    }

    public function testPremiumInsertShouldSuccess()
    {
        $user = factory(\App\User::class)->create();
        $package = factory(PremiumPackage::class)->create();

        $premiumRequest = PremiumRequest::premiumInsertAuto([
            "user" => $user,
            "premium_package_id" => $package->id
        ]);

        $this->assertTrue($premiumRequest);
    }

    public function testPremiumInsertFail()
    {
        $user = factory(\App\User::class)->create();
        $premiumRequest = PremiumRequest::premiumInsertAuto([
            "user" => $user,
            "premium_package_id" => 0
        ]);

        factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "expired_status" => "false"
        ]);
        
        $premiumPackage2 = factory(PremiumPackage::class)->create();
        $premiumRequest2 = PremiumRequest::premiumInsertAuto([
            "user" => $user,
            "premium_package_id" => $premiumPackage2->id // it's randow without having relation to package
        ]);
        
        $this->assertFalse($premiumRequest);
        $this->assertFalse($premiumRequest2);
    }

    public function testCheckPremiumRequestShouldSuccess()
    {
        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);

        $today = Carbon::today();
        $user1 = factory(\App\User::class)->create([
            "date_owner_limit" => $today->copy()->subDays(1)
        ]);
        $user2 = factory(\App\User::class)->create([
            "date_owner_limit" => $today->copy()->subDays(1)
        ]);

        $room = factory(\App\Entities\Room\Room::class)->create();

        $premiumRequest1 = factory(PremiumRequest::class)->create([
            "user_id" => $user1,
            "status" => "1",
            "allocated" => 0
        ]);

        $premiumRequest2 = factory(PremiumRequest::class)->create([
            "user_id" => $user2,
            "status" => "1"
        ]);

        factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest1,
            "total" => 20,
            "history" => 20,
            "used" => 8,
            "designer_id" => $room,
            "is_active" => 1
        ]);

        (new PremiumRequest)->checkPremiumRequest();

        $newPremiumPromote1 = PremiumRequest::where("id", $premiumRequest1->id)->first();
        $newPremiumPromote2 = PremiumRequest::where("id", $premiumRequest2->id)->first();

        $newVp1 = ViewPromote::where("premium_request_id", $premiumRequest1->id)->first();

        $this->assertEquals(0, $newPremiumPromote2->daily_allocation);
        $this->assertEquals(8, $newPremiumPromote1->allocated);
        $this->assertEquals("false", $newVp1->room->is_promoted);
        $this->assertEquals(0, $newVp1->is_active);

    }

    public function testCheckPremiumRequestShouldDeleteViewPromote()
    {
        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);

        $today = Carbon::today();
        $user1 = factory(\App\User::class)->create([
            "date_owner_limit" => $today->copy()->subDays(1)
        ]);

        $premiumRequest1 = factory(PremiumRequest::class)->create([
            "user_id" => $user1,
            "status" => "1",
            "allocated" => 0
        ]);

        factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest1,
            "total" => 20,
            "history" => 20,
            "used" => 8,
            "is_active" => 1
        ]);

        (new PremiumRequest)->checkPremiumRequest();

        $newVp1 = ViewPromote::where("premium_request_id", $premiumRequest1->id)->first();

        $this->assertNull($newVp1);
    }

    public function testGetRelationToPayment()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        
        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id
        ]);

        $this->assertInstanceOf(Payment::class, $premiumRequest->payment->first());
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }
}
