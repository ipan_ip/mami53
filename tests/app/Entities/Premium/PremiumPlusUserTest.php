<?php
namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;

class PremiumPlusUserTest extends MamiKosTestCase
{
    public function testUser()
    {
        $user = (new PremiumPlusUser)->user();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\belongsTo', $user);
        $this->assertInstanceOf('App\User', $user->getRelated());
    }

    public function testConsultant()
    {
        $consultant = (new PremiumPlusUser)->consultant();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\belongsTo', $consultant);
        $this->assertInstanceOf('App\Entities\Consultant\Consultant', $consultant->getRelated());
    }

    public function testRoom()
    {
        $room = (new PremiumPlusUser)->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\belongsTo', $room);
        $this->assertInstanceOf('App\Entities\Room\Room', $room->getRelated());
    }

    public function testInvoices()
    {
        $invoices = (new PremiumPlusUser)->invoices();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\hasMany', $invoices);
        $this->assertInstanceOf('App\Entities\Premium\PremiumPlusInvoice', $invoices->getRelated());
    }

    public function testGetFirstInvoice()
    {
        $userGP4 = factory(PremiumPlusUser::class)->create();
        
        factory(PremiumPlusInvoice::class)->create([
            'name'                  => 'Pembayaran bulan ke-1',
            'premium_plus_user_id'  => $userGP4->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'name'                  => 'Pembayaran bulan ke-2',
            'premium_plus_user_id'  => $userGP4->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'name'                  => 'Pembayaran bulan ke-3',
            'premium_plus_user_id'  => $userGP4->id
        ]);

        $result = $userGP4->getFirstInvoice();

        $this->assertInstanceOf('App\Entities\Premium\PremiumPlusInvoice', $result);
    }

    public function testCheckUserActive()
    {
        $user = factory(PremiumPlusUser::class)->create();
        $userGP4 = factory(PremiumPlusUser::class)->create();
        
        factory(PremiumPlusInvoice::class)->create([
            'name'                  => 'Pembayaran bulan ke-1',
            'premium_plus_user_id'  => $userGP4->id,
            'status'                => PremiumPlusInvoice::INVOICE_STATUS_PAID
        ]);

        $this->assertTrue($userGP4->isActive());
        $this->assertFalse($user->isActive());
    }

    public function testIsInvoiceSendFalse()
    {
        $premiumPlus = factory(PremiumPlusUser::class)->create();
        $result = $premiumPlus->isInvoiceSend();
        $this->assertFalse($result);
    }

    public function testIsInvoiceSendSuccess()
    {
        $premiumPlus = factory(PremiumPlusUser::class)->create();
        $invoices = factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $premiumPlus->id,
            'name' => 'Pembayaran bulan ke-1',
            'notif_status' => PremiumPlusInvoice::INVOICE_NOTIF_STATUS_SEND
        ]);
        $result = $premiumPlus->isInvoiceSend();
        $this->assertTrue($result);
    }
}