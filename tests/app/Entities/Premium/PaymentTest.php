<?php

namespace App\Entities\Premium;

use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumPlusInvoice;
use App\User;
use App\Test\MamiKosTestCase;

class PaymentTest extends MamiKosTestCase
{
    private $payment;
    private $premiumRequest;
    private $premiumPackage;
    // private $midtransMock;

    public function setUp() : void
    {
        parent::setUp();

        // $this->midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $this->premiumPackage = factory(PremiumPackage::class)->create();
        $this->premiumRequest = factory(PremiumRequest::class)->create([
            "status" => "1",
            "premium_package_id" => $this->premiumPackage->id
        ]);

        $user = factory(User::class)->create();

        $this->payment = factory(Payment::class)->create([
            "user_id" => $user->id,
            "order_id" => 456,
            "transaction_id" => "HAKSB71",
            "payment_type" => "bank_transfer",
            "premium_request_id" => $this->premiumRequest->id,
            "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
            "payment_code" => "aKSSJ28",
            "version" => 1 // old data
            
        ]);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetPremiumRequest()
    {
        $premiumReq = $this->payment->premium_request();
        
        $premiumReq2 = factory(PremiumRequest::class)->create([
            "status" => "1"
        ]);
        $payment = factory(Payment::class)->create([
            "version" => Payment::PAYMENT_VERSION,
            "source_id" => $premiumReq2->id,
            "source"    => Payment::PAYMENT_SOURCE_PREMIUM_REQUEST,
            "premium_request_id" => 0
        ]);

        $this->assertContains($this->payment->version, Payment::SUPPORTED_PAYMENT_VERSION);
        $this->assertEquals($this->premiumRequest->id, $premiumReq->first()->id);
        $this->assertEquals($premiumReq2->id, $payment->premium_request()->first()->id);
    }

    public function testStorePaymentSuccess()
    {
        $orderId = 1232222;
        $user = factory(User::class)->create();
        $invoice = factory(PremiumPlusInvoice::class)->create();
        $expiredAt = date("Y-m-d H:i:s", strtotime("+2 hours"));
        
        $data = [
            "user_id" => $user->id,
            "order_id" => $orderId,
            "request_id" => null,
            "with" => 'midtrans',
            "source" => Payment::PAYMENT_SOURCE_GP4,
            "source_id" => $invoice->id,
            "expired_at" => $expiredAt
        ];

        $payment = Payment::store($data);

        $this->assertEquals($orderId, $payment->order_id);
    }

    public function testStorePaymentGp4()
    {
        $orderId = 123;
        $data = [
            "user_id" => 1,
            "order_id" => $orderId,
            "request_id" => 1,
            "with" => 'midtrans'
        ];

        $payment = Payment::store($data);

        $this->assertEquals($orderId, $payment->order_id);
    }

    public function testUpdatePaymentSuccess()
    {
        $updatePayment = Payment::updateToDB($this->paymentData(), "not_midtrans");
        $this->assertEquals($this->payment->id, $updatePayment->id);
    }

    public function testUpdatePaymentMidtransSuccess()
    {
        $updatePayment = Payment::updateToDB($this->paymentData(), "midtrans");
        $this->assertEquals($this->payment->id, $updatePayment->id);
    }

    public function testUpdatePaymentFail()
    {
        $paymentData = [
            "order_id" => 893,
            "user_id" => 0,
            "transaction_id" => $this->payment->transaction_id,
            "payment_type" => $this->payment->payment_type,
            "transaction_status" => 'deny',
            "payment_code" => $this->payment->payment_code,
            "pdf_url" => "aaa.pdf",
            "biller_code" => "JSHAJHSABSAJHSJAHSJAHJS",
            "bill_key" => "728363",
            "gross_amount" => 12000
        ];
        $updatePayment = Payment::updateToDB($paymentData, "midtrans");
        $this->assertEquals(null, $updatePayment);
    }

    public function testUpdatePaymentFromGp4()
    {
        $user = factory(User::class)->create();

        $invoice = factory(PremiumPlusInvoice::class)->create();
        $payment = factory(Payment::class)->create([
            "user_id" => $user->id,
            "order_id" => 456,
            "transaction_id" => "HAKSB71",
            "payment_type" => "bank_transfer",
            "premium_request_id" => 0,
            "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
            "payment_code" => "aKSSJ28",
            "version" => Payment::PAYMENT_VERSION,
            "source" => Payment::PAYMENT_SOURCE_GP4,
            "source_id" => $invoice->id
        ]);

        $data = [
            "order_id" => $payment->order_id,
            "user_id" => $payment->user_id,
            "transaction_id" => $payment->transaction_id,
            "payment_type" => $payment->payment_type,
            "transaction_status" => Payment::TRANSACTION_STATUS_SUCCESS,
            "payment_code" => $payment->payment_code,
            "pdf_url" => "aaa.pdf",
            "biller_code" => "JSHAJHSABSAJHSJAHSJAHJS",
            "bill_key" => "728363",
            "total" => 12000
        ];

        $updatePayment = Payment::updateToDb($data, "midtrans");
        $invoice = PremiumPlusInvoice::find($invoice->id);
        $this->assertEquals(PremiumPlusInvoice::INVOICE_STATUS_PAID, $invoice->status);
    }

    public function testUpdatePaymentBalanceRequest()
    {
        $user = factory(User::class)->create();

        $balanceRequest = factory(BalanceRequest::class)->create();
        $payment = factory(Payment::class)->create([
            "user_id"   => $user->id,
            "order_id" => 45667,
            "transaction_id"    => "8f595442-4243-443a-9d2b-3137d088b242",
            "payment_type"  => "bank_transfer",
            "premium_request_id" => 0,
            "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
            "payment_code" => null,
            "version" => Payment::PAYMENT_VERSION,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id
        ]);

        $data = [
            "order_id" => $payment->order_id,
            "user_id" => $payment->user_id,
            "transaction_id" => $payment->transaction_id,
            "payment_type" => $payment->payment_type,
            "transaction_status" => Payment::TRANSACTION_STATUS_SUCCESS,
            "payment_code" => $payment->payment_code,
            "pdf_url" => "aaa.pdf",
            "biller_code" => "JSHAJHSABSAJHSJAHSJAHJS",
            "bill_key" => "728363",
            "total" => 12000
        ];

        $updatePayment = Payment::updateToDB($data, "midtrans");
        $this->assertEquals($payment->id, $updatePayment->id);
    }

    public function testUpdatePaymentBalanceRequestFail()
    {
        $user = factory(User::class)->create();

        $payment = factory(Payment::class)->create([
            "user_id"   => $user->id,
            "order_id" => 45667,
            "transaction_id"    => "8f595442-4243-443a-9d2b-3137d088b242",
            "payment_type"  => "bank_transfer",
            "premium_request_id" => 0,
            "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
            "payment_code" => null,
            "version" => Payment::PAYMENT_VERSION,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => null
        ]);

        $data = [
            "order_id" => $payment->order_id,
            "user_id" => $payment->user_id,
            "transaction_id" => $payment->transaction_id,
            "payment_type" => $payment->payment_type,
            "transaction_status" => Payment::TRANSACTION_STATUS_SUCCESS,
            "payment_code" => $payment->payment_code,
            "pdf_url" => "aaa.pdf",
            "biller_code" => "JSHAJHSABSAJHSJAHSJAHJS",
            "bill_key" => "728363",
            "total" => 12000
        ];

        $updatePayment = Payment::updateToDB($data, "midtrans");
        $this->assertFalse($updatePayment);
    }

    public function testPaymentCheckSuccess()
    {
        $payment = Payment::paymentCheck($this->paymentData(), false);
        $this->assertEquals($this->payment->id, $payment->id);
    }

    public function testPaymentCheckNotFound()
    {
        $payment = Payment::paymentCheck([
            "order_id" => 992
        ], true);
        
        $this->assertEquals(null, $payment);
    }

    public function testPaymentSuccess()
    {
        $payment = Payment::success($this->payment->order_id);
        $this->assertTrue($payment);
    }

    public function testSuccessOrFailedCheckerPaymentSuccess()
    {
        $payment = Payment::successOrFailedChecker($this->payment->premium_request_id);
        $this->assertEquals(1, $payment);
    }

    public function testCheckStatusExpiredOrNotPaymentSuccess()
    {   
        $payment = Payment::checkStatusExpiredOrNot(38383);
        $this->assertEquals(null, $payment);
    }

    public function testGetPaymentOrderSuccess()
    {
        $payment = Payment::getPaymentOrder($this->payment->order_id);
        $this->assertTrue($payment instanceOf Payment);
    }

    public function testSetPaymentStatusSuccess()
    {
        $data = [
            'transaction_status' => Payment::TRANSACTION_STATUS_PENDING,
            'payment_type' => 'bank_transfer'
        ];
        $payment = Payment::setPaymentStatus($this->payment->order_id, $data);
        $this->assertTrue($payment instanceOf Payment);
    }

    public function testDeleteExistPaymentSuccess()
    {
        $orderId = 500;
        $premiumReq = factory(PremiumRequest::class)->create([
            "status" => "1"
        ]);
        $balanceRequest = factory(BalanceRequest::class)->create();
        $payment = factory(Payment::class)->create([
            "premium_request_id" => $premiumReq->id,
            "order_id" => $orderId
        ]);
        $payment2 = factory(Payment::class)->create([
            "premium_request_id" => 0,
            "order_id" => "aboneceu",
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id
        ]);

        $deletePayment = Payment::deleteExistPayment($payment, $this->payment->order_id);
        $deletePayment2 = Payment::deleteExistPayment($payment2, $this->payment->order_id);
        $this->assertEquals(1, $deletePayment);
        $this->assertEquals(1, $deletePayment2);
    }

    public function testGetDenyPaymentFailed()
    {
        $payment = Payment::deny(742323982938);
        $this->assertFalse($payment);
    }

    public function testGetDenyPaymentSuccess()
    {
        $payment = Payment::deny($this->payment->order_id);
        $this->assertTrue($payment);
    }

    public function testGetDenyPaymentBalanceRequestSuccess()
    {
        $user = factory(User::class)->create();

        $balanceRequest = factory(BalanceRequest::class)->create();
        $payment = factory(Payment::class)->create([
            "user_id"   => $user->id,
            "order_id" => 45667,
            "transaction_id"    => "8f595442-4243-443a-9d2b-3137d088b242",
            "payment_type"  => "bank_transfer",
            "premium_request_id" => 0,
            "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
            "payment_code" => null,
            "version" => Payment::PAYMENT_VERSION,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id
        ]);
        $payment = Payment::denyBalanceRequest($payment->order_id);
        $this->assertTrue($payment);
    }

    public function testDenyPaymentBalanceRequestFail()
    {
        $payment = Payment::denyBalanceRequest("payment");
        $this->assertFalse($payment);
    }

    public function testMidtransConfirmationFailed()
    {
        $payment = Payment::midtransConfirmation(["order_id" => 2837287287382]);
        $this->assertFalse($payment);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetMidtransNotificationSuccess()
    {
        $orderId = 30000;
        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => "success",
                        "payment_type" => "bank_transfer",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);
        
        $paymentStatus = Payment::midtransNotification($orderId);
        $this->assertEquals($orderId, $paymentStatus['order_id']);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransConfirmationSuccess()
    {
        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($this->payment->order_id)
                    ->andReturn((object) [
                        "transaction_status" => "success",
                        "payment_type" => "bank_transfer",
                        "order_id" => $this->payment->order_id,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept"
                    ]);

        $payment = Payment::midtransConfirmation([
                                                "order_id" => $this->payment->order_id,
                                                "selected_payment_type" => "credit_card",
                                                "transaction_status"    => Payment::MIDTRANS_STATUS_CAPTURE
                                                ]);
                                                
        $this->assertTrue($payment instanceOf Payment);
    }

    public function testMidtransConfirmationWhenPremiumRequestDeleted()
    {
        $pay = factory(Payment::class)->create([
            "order_id" => 999,
            "premium_request_id" => null,
        ]);

        $result = Payment::midtransConfirmation(["order_id" => 999]);
        $payment = Payment::find($pay->id);
        $this->assertFalse($result);
        $this->assertEquals(null, $payment);
    }

    public function testGetPremiumPackage()
    {
        $payment = factory(Payment::class)->create([
            "premium_request_id" => null
        ]);
        $this->assertEquals(null, $payment->premiumPackage());
        $this->assertEquals($this->premiumPackage->id, $this->payment->premiumPackage()->id);
    }

    private function paymentData()
    {
        return [
            "order_id" => $this->payment->order_id,
            "user_id" => $this->payment->user_id,
            "transaction_id" => $this->payment->transaction_id,
            "payment_type" => $this->payment->payment_type,
            "transaction_status" => $this->payment->transaction_status,
            "payment_code" => $this->payment->payment_code,
            "pdf_url" => "aaa.pdf",
            "biller_code" => "JSHAJHSABSAJHSJAHSJAHJS",
            "bill_key" => "728363",
            "gross_amount" => 12000
        ];
    }
}