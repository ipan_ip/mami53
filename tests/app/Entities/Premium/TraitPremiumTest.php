<?php
namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Promoted\ViewPromote;

class TraitPremiumTest extends MamiKosTestCase
{
    
    public function testGetOwnerDataSuccess()
    {
        $user = factory(User::class)->create([
            "id" => 1,
            "date_owner_limit" => date('Y-m-d', strtotime('-7 days'))
        ]);

        $expiredDate = date('Y-m-d', strtotime('+1 day'));
        $expectedDate = date('d M Y', strtotime('+1 day'));
        $user2 = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        factory(PremiumRequest::class)->create([
            "id" => 1,
            "user_id" => $user->id,
            "created_at" => date("Y-m-d H:i:s")
        ]);

        factory(PremiumRequest::class)->create([
            'id' => 2,
            'user_id' => $user2->id
        ]);

        $premiumRequest = PremiumRequest::with('user')->where('id', 1)->first();
        $premiumRequest2 = PremiumRequest::with('user')->find(2);
        $ownerData = $premiumRequest->owner_data;
        $ownerData2 = $premiumRequest2->owner_data;

        $this->assertEquals("Expired", $ownerData['premium_finish']);
        $this->assertEquals($expectedDate, $ownerData2['premium_finish']);
    }

    public function testGetPremiumPackageSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "id" => 2,
            "for" => "Package",
            "name" => "Paket apa aja"
        ]);

        factory(PremiumRequest::class)->create([
            "id" => 2,
            "user_id" => 2,
            "premium_package_id" => $premiumPackage->id
        ]);

        $premiumRequest = PremiumRequest::with('premium_package')->where('id', 2)->first();
        $package = $premiumRequest->premium_pack;
        $this->assertEquals($premiumPackage->for, $package['for']);
    }

    public function testGetPremiumPackageShouldEmpty()
    {
        factory(PremiumRequest::class)->create([
            'id' => 34
        ]);

        $premiumRequest = PremiumRequest::with('premium_package')->find(34);
        $package = $premiumRequest->premium_pack;
        $this->assertEquals("", $package['for']);
    }

    public function testGetRequestExpiredStatus()
    {
        factory(PremiumRequest::class)->create([
            "id" => 3,
            "expired_status" => "true"
        ]);

        $premiumRequest = PremiumRequest::where('id', 3)->first();
        $requestStatus = $premiumRequest->status_request;
        $this->assertEquals("Expired", $requestStatus);
    }

    public function testGetRequestWaitingConfirmationStatus()
    {
        factory(PremiumRequest::class)->create([
            "id" => 14,
            "expired_status" => "false"
        ]);

        $premiumRequest = PremiumRequest::where('id', 14)->first();
        $requestStatus = $premiumRequest->status_request;
        $this->assertEquals("Belum Konfirmasi", $requestStatus);
    }

    public function testGetRequestSuccessStatus()
    {
        factory(PremiumRequest::class)->create([
            "id" => 4,
            "expired_status" => null
        ]);

        $premiumRequest = PremiumRequest::where('id', 4)->first();
        $requestStatus = $premiumRequest->status_request;
        $this->assertEquals("Sukses", $requestStatus);
    }

    public function testGetTotalAllocationSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
                "id" => 5,
                "view" => 1000,
                "allocated" => 1000,
                "expired_status" => null
            ]);

        factory(ViewPromote::class)->create([
            "id" => 5,
            "premium_request_id" => $premiumRequest->id,
            "total" => 1000,
            "is_active" => 1
        ]);

        $request = PremiumRequest::with('view_promote')->where('id', $premiumRequest->id)->first();
        $allocation = $request->allocated_now;
        $this->assertEquals($premiumRequest->allocated, $allocation);
    }

    public function testGetZeroTotalAllocation()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 6,
            "view" => 1000,
            "allocated" => 0,
            "expired_status" => null
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $allocation = $request->allocated_now;
        $this->assertEquals($premiumRequest->allocated, $allocation);
    }

    public function testCheckConfirmationStatusFalse()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 17,
            "expired_status" => "false"
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $this->assertFalse($request->check_confirmation);
    }

    public function testCheckConfirmationStatusFalseBalanceRequest()
    {
        $balanceRequest = factory(BalanceRequest::class)->create();
        
        $request = BalanceRequest::with('account_confirmation')->find($balanceRequest->id);
        $this->assertFalse($request->check_confirmation);
    }

    public function testCheckConfirmationStatusTrue()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 7,
            "expired_status" => "false"
        ]);

        factory(AccountConfirmation::class)->create([
            "id" => 7,
            "premium_request_id" => $premiumRequest->id
        ]);
        
        $request = PremiumRequest::with('account_confirmation')->where('id', $premiumRequest->id)->first();
        $this->assertTrue($request->check_confirmation);
    }

    public function testGetPremiumWaitingConfirmationStatus()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 8,
            "status" => "0",
            "expired_date" => date('Y-m-d', strtotime('+7 days')),
            "expired_status" => "false",
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $this->assertEquals("waiting_confirmation", $request->premium_status);
    }

    public function testGetPremiumExpiredStatus()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 9,
            "status" => "0",
            "expired_date" => date('Y-m-d', strtotime('-7 days')),
            "expired_status" => "false",
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $this->assertEquals("expired", $request->premium_status);
    }

    public function testGetPremiumWaitingConfirmationFromAdmin()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 10,
            "status" => "0",
            "expired_date" => null,
            "expired_status" => null,
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $this->assertEquals("admin_verification", $request->premium_status);
    }

    public function testGetPremiumSuccessStatus()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 11,
            "status" => "1",
            "expired_date" => null,
            "expired_status" => null,
        ]);
        
        $request = PremiumRequest::where('id', $premiumRequest->id)->first();
        $this->assertEquals("success", $request->premium_status);
    }

    public function testCheckTopupRequestFalse()
    {
        $topup = factory(BalanceRequest::class)->create([
            "id" => 1,
            "premium_request_id" => null
        ]);

        $request = BalanceRequest::where('id', 1)->first();
        $this->assertFalse($request->balance_owner);
    }

    public function testCheckTopupRequestSuccess()
    {
        $topup = factory(BalanceRequest::class)->create([
            "id" => 1,
            "premium_request_id" => factory(PremiumRequest::class)->create([
                "id" => 100
            ])->id
        ]);

        $request = BalanceRequest::with('premium_request')->where('id', 1)->first();
        $this->assertEquals(100, $request->balance_owner['id']);
    }

    public function testPremiumExecutiveNull()
    {
        $user = factory(User::class)->create([
            'id' => 1991,
            'email' => 'test@mail.com'
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'id' => 12,
            'status' => "1",
            'request_user_id' => $user->id,
            'expired_date' => null,
            'expired_status' => null,
        ]);

        $this->assertTrue($premiumRequest->premium_executive_user instanceof User);
    }

    public function testGetEmptyPremiumStatusAttribute()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => "3",
            "expired_date" => null,
            "expired_status" => null,
        ]);
        
        $request = PremiumRequest::find($premiumRequest->id);
        $this->assertEquals("", $request->premium_status);   
    }

    public function testGetPremiumExecutiveAttributeIsNull()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "expired_date" => null,
            "expired_status" => null,
        ]);
        
        $request = PremiumRequest::find($premiumRequest->id);
        $this->assertNull($request->premium_executive); 
    }

    public function testGetPremiumExecutiveAttributeShouldSuccess()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "expired_date" => null,
            "expired_status" => null,
            "request_user_id" => $user->id
        ]);

        $request = PremiumRequest::with('user')->find($premiumRequest->id);
        $this->assertInstanceOf(User::class, $request->premium_executive);
    }
}