<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumCashbackHistory;
use App\Entities\Mamipay\MamipayPayoutTransaction;

class PremiumCashbackHistoryTest extends MamiKosTestCase 
{

    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testRelationToUser()
    {
        $cashback = factory(PremiumCashbackHistory::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->assertInstanceOf(User::class, $cashback->user);
    }

    public function testRelationToBalanceRequest()
    {
        $balanceRequest = factory(BalanceRequest::class)->create();
        $cashback = factory(PremiumCashbackHistory::class)->create([
            'reference_id' => $balanceRequest->id
        ]);

        $this->assertInstanceOf(BalanceRequest::class, $cashback->balance_request);
    }

    public function testRelationToMamipayPayoutTransaction()
    {
        $payoutTransaction = factory(MamipayPayoutTransaction::class)->create();
        $cashback = factory(PremiumCashbackHistory::class)->create([
            'reference_id' => $payoutTransaction->id
        ]);

        $this->assertInstanceOf(MamipayPayoutTransaction::class, $cashback->payoutTransaction);
    }

}