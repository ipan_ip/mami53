<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use Mockery;
use Carbon\Carbon;
use StatsLib;

class AdsInteractionTrackerTest extends MamiKosTestCase
{

    public function setUp(): void
    {
        parent::setUp();

        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        // Reset test carbon mock
        Carbon::setTestNow();
    }

    public function testInsertTrackerShouldSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $this->assertDatabaseMissing('designer_ads_interaction_tracker', [
            'designer_id' => $room->id, 
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);

        AdsInteractionTracker::insertTracker($user, $room, AdsInteractionTracker::LIKE_TRACKER_TYPE);

        $this->assertDatabaseHas('designer_ads_interaction_tracker', [
            'designer_id' => $room->id, 
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);
    }

    public function testDeleteTrackerShouldSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $this->assertDatabaseMissing('designer_ads_interaction_tracker', [
            'designer_id' => $room->id, 
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);

        AdsInteractionTracker::insertTracker($user, $room, AdsInteractionTracker::LIKE_TRACKER_TYPE);

        $this->assertDatabaseHas('designer_ads_interaction_tracker', [
            'designer_id' => $room->id, 
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);

        AdsInteractionTracker::deleteTracker($user, $room, AdsInteractionTracker::LIKE_TRACKER_TYPE);

        $this->assertDatabaseMissing('designer_ads_interaction_tracker', [
            'designer_id' => $room->id, 
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);
    }

    public function testGetRelation()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $like = factory(AdsInteractionTracker::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $this->assertEquals($room->id, $like->room->id);
        $this->assertEquals($user->id, $like->user->id);
    }

    private function generateLove(Room $room, User $user, $creationTime = null, $promoted = true, $type = 'like', $status = 'true')
    {

        $now = Carbon::now();
        
        factory(AdsInteractionTracker::class)->create([
            'designer_id'   => $room,
            'user_id'       => $user,
            'type'          => $type,
            'created_at'    => is_null($creationTime) ? $now : $creationTime,
            'updated_at'    => is_null($creationTime) ? $now : $creationTime,
        ]);

        return true;
    }

    public function testGetLoveStatisticToday()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        $startDate = $now->copy()->startOfDay();
        $date23PM = $startDate->copy()->subHour();
        $date2AM = $startDate->copy()->addHours(2);
        $date3AM = $startDate->copy()->addHours(3);
        $date345AM = $startDate->copy()->addHours(3)->addMinutes(45);
        $validateDate23PM = $date23PM->format('Y-m-d H');
        $validateDate3AM = $date3AM->format('Y-m-d H');
        $validateDate1AM = $startDate->copy()->addHour()->format('Y-m-d H');
        $currentHour = $now->hour;

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        $this->generateLove($room1, factory(User::class)->create(), $date23PM);

        $this->generateLove($room1, factory(User::class)->create(), $date3AM);
        $this->generateLove($room2, factory(User::class)->create(), $date3AM);
        $this->generateLove($room2, factory(User::class)->create(), $date345AM);

        $this->generateLove($room1, factory(User::class)->create(), $date2AM);
        $this->generateLove($room3, factory(User::class)->create(), $date2AM);
        $this->generateLove($room3, factory(User::class)->create(), $date2AM);

        $stats = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, null);
        $statsOn12AM = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, null, null, $startDate);
        $statsOn1AM = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, null, null, $startDate->copy()->addHour());

        if ($currentHour > 3) {
            $key = array_search($validateDate3AM, array_column($stats, 'date'));
            $this->assertEquals(3, $stats[$key]['value']);
        }

        // it will show until current hour
        if ($currentHour <= 23) {
            $this->assertEquals($currentHour + 1, count($stats));
        }
        // if current hour is 12 AM then stats only show 2 statistics, on 11 PM - 00 AM
        $this->assertEquals(2, count($statsOn12AM));
        $this->assertEquals($validateDate23PM, $statsOn12AM[0]['date']);
        // if current hour is 1 AM then stats only show 2 statistics, on 00 AM - 01 AM
        $this->assertEquals(2, count($statsOn1AM));
        $this->assertEquals($validateDate1AM, $statsOn1AM[1]['date']);

    }

    public function testGetLoveStatisticYesterday()
    {
        $now = Carbon::now();
        $yesterday = $now->copy()->subDay();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $yesterday = $yesterday->copy()->startOfDay()->addHours(9);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateLove($room, factory(User::class)->create(), $yesterday);
        $this->generateLove($room, factory(User::class)->create(), $yesterday);

        $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, StatsLib::RangeYesterday);

        $key = array_search($yesterday->format('Y-m-d H'), array_column($result, 'date'));

        $this->assertEquals(2, $result[$key]['value']);
    }

    public function testGetLoveStatistic7Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $twoDaysBehind = $now->copy()->subDays(2);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);

        $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, StatsLib::Range7Days);

        $this->assertEquals(2, $result[4]['value']);

    }

    public function testGetLoveStatistic30Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $twoDaysBehind = $now->copy()->subDays(12);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);

        $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, StatsLib::Range30Days);

        $this->assertEquals(2, $result[17]['value']);
    }

    public function testGetLoveStatisticShouldReturnEmpty()
    {
        $now = Carbon::now();
        
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $twoDaysBehind = $now->copy()->subDays(12);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "unverified"
        ]);

        $this->generateLove(factory(Room::class)->create(), factory(User::class)->create(), $twoDaysBehind);

        $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, StatsLib::Range30Days);

        $this->assertEquals(0, count($result));
    }

    public function testGetSummaryLoveStatisticShouldSuccess()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $twoDaysBehind = $now->subDays(12);
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove($room, factory(User::class)->create(), $twoDaysBehind);

        $result = AdsInteractionTracker::getLoveStatisticSummaryFromAdsRooms($user, StatsLib::Range30Days);

        $this->assertEquals(3, $result['value']);
    }

    public function testGetSummaryLoveStatisticShouldEmpty()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $twoDaysBehind = $now->subDays(12);

        $this->generateLove(factory(Room::class)->create(), factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove(factory(Room::class)->create(), factory(User::class)->create(), $twoDaysBehind);
        $this->generateLove(factory(Room::class)->create(), factory(User::class)->create(), $twoDaysBehind);

        $result = AdsInteractionTracker::getLoveStatisticSummaryFromAdsRooms($user, StatsLib::Range30Days);

        $this->assertEquals(0, $result['value']);
    }
}