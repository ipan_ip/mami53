<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumStopReason;
use App\User;

class PremiumStopReasonTest extends MamiKosTestCase
{
    public function testGetUserRelation()
    {
        $user = factory(User::class)->create();
        $reason = factory(PremiumStopReason::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $reason->user);
    }
}