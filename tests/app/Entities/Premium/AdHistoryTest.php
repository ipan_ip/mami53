<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Premium\AdHistory;
use Carbon\Carbon;

class AdHistoryTest extends MamiKosTestCase
{

    public function setUp() : void
    {
        parent::setUp();

        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    private function generateAdHistory(Room $room, $createdTime = null, $click = 1, $price = null)
    {
        $now = Carbon::now();
        $historyData = [
            'designer_id'   => $room->id,
            'type'          => AdHistory::TYPE_CLICK,
            'total_click'   => $click,
            'created_at'    => (is_null($createdTime)) ? $now : $createdTime,
            'updated_at'    => (is_null($createdTime)) ? $now : $createdTime,
        ];

        if (!is_null($price)) {
            $historyData['total'] = $price;
        }

        factory(AdHistory::class)->create($historyData);

        return true;
    }

    public function testGetClickAdHistoryWithRange()
    {
        $now = Carbon::now();
        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();

        $this->generateAdHistory($room1, $now->copy()->subDays(3)->subHour(6), 1, 900);
        $this->generateAdHistory($room1, $now->copy()->subDays(4)->subHour(2), 1, 900);

        $stats = AdHistory::GetClickAdHistoryWithRange($room1->id);
        $this->assertEquals(2, $stats);
    }

    public function testGetRelationRoomShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $history = factory(AdHistory::class)->create([
            'designer_id' => $room->id
        ]);

        $this->assertInstanceOf(Room::class, $history->room);
    }
    
}