<?php

namespace App\Entities\Premium;

use App\Entities\Premium\PremiumPackage;
use App\Test\MamiKosTestCase;

class PremiumPackageTest extends MamiKosTestCase
{
    
    public function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    /** @test */
    public function testDiscount()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 100000,
            'sale_price' => 50000
        ]);
        $discount = $premiumPackage->discount();
        $this->assertEquals(50, $discount);
    }

    /** @test */
    public function testDiscountNoPrice()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 0,
        ]);
        $discount = $premiumPackage->discount();
        $this->assertEquals(0, $discount);
    }

    /** @test */
    public function testPriceString()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 100000,
        ]);
        $priceString = $premiumPackage->priceString();
        $this->assertEquals('Rp 100.000', $priceString);
    }

    /** @test */
    public function testSalePriceStringWithoutSpecialPrice()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 100000,
            'sale_price' => 50000,
            'special_price' => 0
        ]);
        $salePriceString = $premiumPackage->salePriceString();
        $this->assertEquals('Rp 50.000', $salePriceString);
    }

     /** @test */
     public function testSalePriceStringWithSpecialPrice()
     {
         $premiumPackage = factory(PremiumPackage::class)->make([
             'price' => 100000,
             'sale_price' => 50000,
             'special_price' => 50000
         ]);
         $salePriceString = $premiumPackage->salePriceString();
         $this->assertEquals('Rp 50.000 + Rp 50.000', $salePriceString);
     }

    /** @test */
    public function testSaleLimitDateString()
    {
        $dateTomorrow = date('Y-m-d', strtotime('+1 day'));
        $premiumPackage = factory(PremiumPackage::class)->make([
            'sale_limit_date' => $dateTomorrow
        ]);
        $saleLimitDateString = $premiumPackage->saleLimitDateString();
        $expectedDateFormatted =  date('d M Y', strtotime($dateTomorrow));
        $this->assertEquals("( Diskon s/d " . $expectedDateFormatted . ")", $saleLimitDateString);
    }

    /** @test */
    public function testActiveCountStringDay0()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'total_day' => 0,
        ]);
        $activeCountString = $premiumPackage->activeCountString();
        $this->assertEquals("", $activeCountString);
    }

    /** @test */
    public function testActiveCountStringDay15()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'total_day' => 15,
        ]);
        $activeCountString = $premiumPackage->activeCountString();
        $this->assertEquals("Masa aktif 15 hari", $activeCountString);
    }

    /** @test */
    public function testActiveCountStringDay60()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'total_day' => 60,
        ]);
        $activeCountString = $premiumPackage->activeCountString();
        $this->assertEquals("Masa aktif 2 bulan", $activeCountString);
    }

    /** @test */
    public function testFeatureArray()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'feature' => 'Fitur 1.Fitur 2.Fitur 3'
        ]);
        $featureArray = $premiumPackage->featureArray();
        $expected = [
            'Fitur 1',
            'Fitur 2',
            'Fitur 3'
        ];
        $this->assertEquals($expected, $featureArray);
    }

    /** @test */
    public function testFeatureArrayNoFeature()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'feature' => null
        ]);
        $featureArray = $premiumPackage->featureArray();
        $this->assertEquals([], $featureArray);
    }

    /** @test */
    public function testDiscountString()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 100000,
            'sale_price' => 50000
        ]);
        $discountString = $premiumPackage->discountString();
        $this->assertEquals("Diskon 50%", $discountString);
    }

    /** @test */
    public function testDiscountStringNoDiscount()
    {
        $premiumPackage = factory(PremiumPackage::class)->make([
            'price' => 0
        ]);
        $discountString = $premiumPackage->discountString();
        $this->assertEquals(null, $discountString);
    }

    public function testGetPremiumPackageByIdSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'id' => 1000000
        ]);

        $result = PremiumPackage::getPremiumPackageById($premiumPackage->id);
        $this->assertEquals($premiumPackage->id, $result->id);
    }

    public function testPriceTotalPackageWhenSalePriceNull()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'id' => 1000001,
            'sale_price' => 0,
            'price' => 1200,
            'special_price' => 0
        ]);

        $expected = 1200;
        $this->assertEquals($expected, $premiumPackage->totalPrice());
    }

    public function testPriceTotalPackageWhenSalePriceNotNull()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'id' => 1000001,
            'sale_price' => 500,
            'price' => 1200,
            'special_price' => 200
        ]);
        
        $expected = 700;
        $this->assertEquals($expected, $premiumPackage->totalPrice());
    }

    public function testGetCompiledNameShouldSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'sale_price' => 500,
            'price' => 1200,
            'special_price' => 200,
            'view' => 1000,
            'name' => 'Paket Premium',
            'for' => PremiumPackage::PACKAGE_TYPE
        ]);
        $balancePackage = factory(PremiumPackage::class)->create([
            'sale_price' => 500,
            'price' => 1200,
            'special_price' => 200,
            'view' => 1000,
            'for' => PremiumPackage::BALANCE_TYPE
        ]);

        $this->assertEquals('Saldo 1.000', $balancePackage->compiledName());
        $this->assertEquals('Paket Premium', $premiumPackage->compiledName());
    }

    public function testGetPromoPackageWhenEmptySalePrice()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'sale_price' => 0,
            'price' => 1200,
            'special_price' => 200,
            'view' => 1000,
            'start_date' => date('Y-m-d'),
            'sale_limit_date' => date('Y-m-d', strtotime('+1 day'))
        ]);

        $promoPackage = [
            'diskon' => 'Diskon 0%',
            'paket' => $premiumPackage->name
        ];

        $promoDetail = (new PremiumPackage)->getPromoPackage();
        $this->assertEquals($promoPackage, $promoDetail);
    }

    public function testGetRelationToPremiumRequestShouldSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::PACKAGE_TYPE
        ]);

        factory(PremiumRequest::class)->create([
            'premium_package_id' => $premiumPackage
        ]);

        $this->assertEquals(1, $premiumPackage->premium_request->count());
    }

    public function testGetRelationToBalanceRequestShouldSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::BALANCE_TYPE
        ]);

        factory(BalanceRequest::class)->create([
            'premium_package_id' => $premiumPackage
        ]);

        $this->assertEquals(1, $premiumPackage->balance_request->count());
    }

}
