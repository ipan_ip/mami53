<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPlusInvoice;

class PremiumPlusInvoiceTest extends MamiKosTestCase
{

    public function testPremiumPlusUser()
    {
        $premiumPlusUser = (new PremiumPlusInvoice)->premium_plus_user();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\belongsTo', $premiumPlusUser);
        $this->assertInstanceOf('App\Entities\Premium\PremiumPlusUser', $premiumPlusUser->getRelated());
    }

    public function testUpdateStatus()
    {
        $result = PremiumPlusInvoice::updateStatus(['source_id' => 0]);

        $this->assertNull($result);

        $invoice = factory(PremiumPlusInvoice::class)->create();

        $data = [
            'source_id' => $invoice->id,
            'transaction_status' => 'success',
            'total' => 9000
        ];

        $result = PremiumPlusInvoice::updateStatus($data);
        $this->assertTrue($result);
    }

    public function testGetDeepLinkInvoice()
    {
        $invoice = factory(PremiumPlusInvoice::class)->create();

        $this->assertEquals('/s/gp4-invoice/' . $invoice->shortlink, $invoice->getDeepLinkInvoice());
    }

}