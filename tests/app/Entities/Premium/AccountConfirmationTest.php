<?php

namespace App\Entities\Premium;

use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\Payment;
use App\Entities\Premium\Bank;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use App\Test\MamiKosTestCase;

class AccountConfirmationTest extends MamiKosTestCase
{
    
    public function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetConfirmationFromRequestReturnData()
    {
        $user = factory(User::class)->create();
        $data = [
            'user' => $user
        ];
        factory(AccountConfirmation::class)->create([
            'user_id' => $user->id
        ]);
        $accountConfirmation = AccountConfirmation::getConfirmationFromRequest($data);
        $this->assertEquals($accountConfirmation->user_id, $user->id);
    }

    public function testRequestConfirmation()
    {
        $dummyAccountConfirmation = factory(AccountConfirmation::class)->create();
        $accountConfirmation = AccountConfirmation::requestConfirmation($dummyAccountConfirmation->premium_request_id);
        $this->assertContains($dummyAccountConfirmation->id, $accountConfirmation->pluck('id')->toArray());
    }

    public function testStoreInsertNew()
    {
        $data =[
            'user_id' => 1,
            'premium_request_id' => 1,
            'bank' => 'bca',
            'name' => 'James',
            'total' => 100123,
            'transfer_date' => date('Y-m-d'),
        ];
        $accountConfirmation = AccountConfirmation::store($data, 'desktop');
        $this->assertInstanceOf(AccountConfirmation::class, $accountConfirmation);
        $this->assertEquals($accountConfirmation->total, $data['total']);
    }

    public function testStoreWithExistConfirmation()
    {
        $dummyAccountConfirmation = factory(AccountConfirmation::class)->create();
        $data =[
            'user_id' => $dummyAccountConfirmation->user_id,
            'premium_request_id' => $dummyAccountConfirmation->premium_request_id,
            'bank' => 'bca',
            'name' => 'James',
            'total' => 100123,
            'transfer_date' => date('Y-m-d'),
        ];
        $accountConfirmation = AccountConfirmation::store($data, 'desktop');
        $this->assertInstanceOf(AccountConfirmation::class, $accountConfirmation);
        $this->assertEquals($accountConfirmation->total, $data['total']);
        // assert not creating another row
        $accountConfirmations = AccountConfirmation::where('user_id', $data['user_id'])
            ->where('premium_request_id', $data['premium_request_id'])->get();
        $this->assertEquals(1, $accountConfirmations->count());
    }

    public function testAutoConfirm()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id
        ]);
        $accountConfirmation = AccountConfirmation::AutoConfirm($user, $premiumRequest);
        $this->assertInstanceOf(AccountConfirmation::class, $accountConfirmation);
        $this->assertEquals($accountConfirmation->user_id, $user->id);
        $this->assertEquals($accountConfirmation->premium_request_id, $premiumRequest->id);
        $this->assertEquals($accountConfirmation->total, 0);
    }

    public function testAutoVerifFromAdminWithNoAccountConfirmation()
    {
        // accountConfirmation that isn't persisted in db
        $accountConfirmation = factory(AccountConfirmation::class)->make([
            'id' => 3
        ]);
        $result = AccountConfirmation::autoVerifFromAdmin($accountConfirmation);
        $this->assertFalse($result);
    }

    public function testAutoVerifFromAdminWithUserHasNoRoom()
    {
        $user = factory(User::class)->create();
        $accountConfirmation = factory(AccountConfirmation::class)->create([
            'user_id' => $user->id
        ]);
        $result = AccountConfirmation::autoVerifFromAdmin($accountConfirmation);
        $this->assertFalse($result);
    }

    public function testAutoVerifFromAdminWithUserHasRoomUnverified()
    {
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'status' => 'unverified'
        ]);
        $accountConfirmation = factory(AccountConfirmation::class)->create([
            'user_id' => $user->id
        ]);
        $result = AccountConfirmation::autoVerifFromAdmin($accountConfirmation);
        $this->assertFalse($result);
    }

    public function testAutoVerifFromAdminWithUserHasRoomMoreThan1()
    {
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'status' => 'verified'
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'status' => 'verified'
        ]);
        $accountConfirmation = factory(AccountConfirmation::class)->create([
            'user_id' => $user->id
        ]);
        $result = AccountConfirmation::autoVerifFromAdmin($accountConfirmation);
        $this->assertFalse($result);
    }

    public function testAutoVerifFromAdmin()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);
        $premiumPackage = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $premiumPackage->id,
            'user_id' => $user->id
        ]);
        $accountConfirmation = factory(AccountConfirmation::class)->create([
            'user_id' => $user->id,
            'premium_request_id' => $premiumRequest->id
        ]);
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        $result = AccountConfirmation::autoVerifFromAdmin($accountConfirmation);
        $this->assertTrue($result);
    }

    public function testGetBankAccountConfirmation()
    {
        $bank = factory(Bank::class)->create();
        $confirmation = factory(AccountConfirmation::class)->create([
            'bank_account_id' => $bank->id
        ]);

        $this->assertInstanceOf(Bank::class, $confirmation->bank_account);
        $this->assertInstanceof(AccountConfirmation::class, $bank->account_confirmation->first());
    }

    public function testDeleteNotConfirmedStatusBalanceRequest()
    {
        $balanceRequest = factory(BalanceRequest::class)->create([
            'status' => 1
        ]);

        factory(AccountConfirmation::class)->create([
            'view_balance_request_id' => $balanceRequest->id,
            'premium_request_id' => 0,
            'is_confirm' => '1'
        ]);

        factory(AccountConfirmation::class)->create([
            'view_balance_request_id' => $balanceRequest->id,
            'premium_request_id' => 0,
            'is_confirm' => '0'
        ]);

        $payment = factory(Payment::class)->create([
            'version' => 3,
            'source_id' => $balanceRequest->id
        ]);

        $getPayment = Payment::with('premium_balance_request.account_confirmation')
                            ->where('source_id', $balanceRequest->id)
                            ->first();
        
        AccountConfirmation::deleteNotConfirmedStatus($balanceRequest);
        $getNotConfirmedStatus = AccountConfirmation::where('view_balance_request_id', $balanceRequest->id)
                                        ->where('is_confirm', '0')
                                        ->count();

        $this->assertEquals($getNotConfirmedStatus, 0);
    }

    public function testDeleteNotConfirmedStatusPackageRequest()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => 1
        ]);

        factory(AccountConfirmation::class)->create([
            'view_balance_request_id' => 0,
            'premium_request_id' => $premiumRequest->id,
            'is_confirm' => '1'
        ]);

        factory(AccountConfirmation::class)->create([
            'view_balance_request_id' => 0,
            'premium_request_id' => $premiumRequest->id,
            'is_confirm' => '0'
        ]);

        $payment = factory(Payment::class)->create([
            'version' => 1,
            'source_id' => $premiumRequest->id
        ]);

        $getPayment = Payment::with('premium_request.account_confirmation')
                            ->where('source_id', $premiumRequest->id)
                            ->first();
        
        AccountConfirmation::deleteNotConfirmedStatus($premiumRequest);
        $getNotConfirmedStatus = AccountConfirmation::where('premium_request_id', $premiumRequest->id)
                                        ->where('is_confirm', '0')
                                        ->count();

        $this->assertEquals($getNotConfirmedStatus, 0);
    }
}
