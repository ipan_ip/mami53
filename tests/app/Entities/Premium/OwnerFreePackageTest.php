<?php

namespace App\Entities\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPackage;
use App\User;

class OwnerFreePackageTest extends MamiKosTestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testSubmitDataShouldReturnFalse()
    {
        $data = [
            'user' => $this->user,
            'ids' => 2,
            'type' => OwnerFreePackage::TYPE[0]
        ];

        factory(OwnerFreePackage::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->assertFalse(OwnerFreePackage::insertOrUpdateToDB($data));
    }

    public function testSubmitDataShouldReturnTrue()
    {
        $package = factory(PremiumPackage::class)->create();
        $data = [
            'user' => $this->user,
            'ids' => $package->id,
            'type' => OwnerFreePackage::TYPE[0]
        ];

        $this->assertTrue(OwnerFreePackage::insertOrUpdateToDB($data));
    }

    public function testCheckSubmitDataShouldReturnFalse()
    {
        $user = factory(User::class)->create();

        $this->assertFalse(OwnerFreePackage::checkSubmitData($user));
    }

    public function testCheckSubmitDataShouldSuccess()
    {
        $package = factory(PremiumPackage::class)->create();
        $data = [
            'user' => $this->user,
            'ids' => $package->id,
            'type' => OwnerFreePackage::TYPE[0]
        ];

        OwnerFreePackage::insertOrUpdateToDB($data);

        $check = OwnerFreePackage::checkSubmitData($this->user);

        $this->assertInstanceOf(OwnerFreePackage::class, $check);
    }
}