<?php

namespace App\Entities\Lpl;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CriteriaTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testTrackChanges()
    {
        $factory = factory(Criteria::class)->create();

        $criteria = Criteria::first();
        $newScore = 256;
        $criteria->trackChanges($newScore);

        $history = History::where('lpl_criteria_id', $criteria->id)->first();
        $this->assertEquals($newScore, $history->original_score);
        $this->assertEquals($factory->score, $history->modified_score);
    }

    public function testTrackChangesReturnEmpty()
    {
        factory(Criteria::class)->create();

        $criteria = Criteria::first();
        $newScore = 512;
        $criteria->trackChanges($newScore);

        $history = History::where('lpl_criteria_id', $criteria->id)->first();
        $this->assertNull($history);
    }

    public function testHistories()
    {
        $rows = mt_rand(5, 10);
        $criteria = factory(Criteria::class)->create();
        factory(History::class, $rows)->create(
            [
                'lpl_criteria_id' => $criteria->id
            ]
        );

        $this->assertInstanceOf(Collection::class, $criteria->histories);
        $this->assertEquals($rows, $criteria->histories->count());
    }

    public function testGetCalculatedScore()
    {
        factory(Criteria::class)->create(
            [
                'order' => 9
            ]
        );

        $criteria = Criteria::first();
        $response = $criteria->getCalculatedScore();

        $this->assertEquals(256, $response);
    }

    public function testCalculateScore()
    {
        $this->markTestSkipped('Temporary skip this test');
        factory(Criteria::class)->create();

        $criteria = Criteria::first();
        $criteria->calculateScore();
    }

    public function testCalculateScoreReturnFalse()
    {
        $this->markTestSkipped('Temporary skip this test');
        factory(Criteria::class)->create();

        $mock = $this->getMockBuilder(Criteria::class)
            ->setMethods(array('getCalculatedScore'))
            ->getMock();

        $mock->expects($this->any())
            ->method('getCalculatedScore')
            ->will($this->returnValue('123'));

        $criteria = Criteria::first();
        $response = $criteria->calculateScore();

        $this->assertTrue($response);
    }
}
