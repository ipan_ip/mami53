<?php

namespace App\Entities\Lpl;

use App\Test\MamiKosTestCase;
use App\User;

class HistoryTest extends MamiKosTestCase
{
    private $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['role' => 'administrator']);
        $this->actingAs($this->user);
    }

    public function testAdmin()
    {
        factory(History::class)
            ->connection('mysql_log')
            ->create(
                [
                    'admin_id' => $this->user->id
                ]
            );

        $history = History::with(['admin'])
            ->orderBy('id', 'desc')
            ->first();

        $this->assertInstanceOf(User::class, $history->admin);
    }

    public function testCriteria()
    {
        $criteria = factory(Criteria::class)->create();

        factory(History::class)
            ->connection('mysql_log')
            ->create(
            [
                'lpl_criteria_id' => $criteria->id
            ]
        );

        $history = History::with(['criteria'])
            ->orderBy('id', 'desc')
            ->first();

        $this->assertInstanceOf(Criteria::class, $history->criteria);
    }
}
