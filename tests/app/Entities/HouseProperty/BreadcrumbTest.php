<?php
namespace App\Entities\HouseProperty;

use App\Test\MamiKosTestCase;
use App\Entities\HouseProperty\Breadcrumb;
use App\Entities\Landing\LandingHouseProperty;

class BreadcrumbTest extends MamiKosTestCase
{
    public function testGenerateBreadcrumbNull()
    {
        $response = (new Breadcrumb())->generate();
        $breadcrumb = $response->getList()[0];
        $this->assertEquals($breadcrumb['name'], 'Home');
    }

    public function testGenerateBreadcrumHouseProperty()
    {
        $houseProperty = factory(HouseProperty::class)->create();
        $response = (new Breadcrumb())->generate($houseProperty);
        $breadcrumb = $response->getList()[0];
        $this->assertEquals($breadcrumb['name'], 'Home');
    }

    public function testGenerateBreadcrumHousePropertyLanding()
    {
        $housePropertyLanding = factory(LandingHouseProperty::class)->create();
        $response = (new Breadcrumb())->generate($housePropertyLanding);
        $breadcrumb = $response->getList()[0];
        $this->assertEquals($breadcrumb['name'], 'Home');
    }

    public function testHouseBreadcrumb()
    {
        $housePropertyLanding = factory(LandingHouseProperty::class)->create([
            'area_city' => 'Jakarta',
            'type' => 'villa',
            'area_subdistrict' => 'Jaktim'
        ]);

        $houseProperty = factory(HouseProperty::class)->create([
            'area_city' => $housePropertyLanding->area_city,
            'type' => $housePropertyLanding->type,
            'slug' => "slug"
        ]);

        $response = (new Breadcrumb())->houseBreadcrumb($houseProperty);
        $breadcrumb = $response->getList();
        $this->assertEquals(count((array) $breadcrumb), 3);
    }

    public function testGetList()
    {
        $response = (new Breadcrumb())->getList();
        $this->assertEquals(count((array) $response), 0);
    }
}