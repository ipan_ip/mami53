<?php

namespace App\Entities\Sanjunipero;

use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DynamicLandingParentTest extends MamiKosTestCase
{
    public function testSetSlugAttribute()
    {
        $res = (new DynamicLandingParent)->setSlugAttribute('lala');
        $this->assertNull($res);
    }

    public function testScopeActive()
    {
        $res = (new DynamicLandingParent)->active();
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testRelationChildren()
    {
        $res = (new DynamicLandingParent)->children();
        $this->assertInstanceOf(HasMany::class, $res);
    }

    public function testRelationDesktopHeaderPhoto()
    {
        $res = (new DynamicLandingParent)->desktop_header_photo();
        $this->assertInstanceOf(BelongsTo::class, $res);
    }

    public function testRelationMobileHeaderPhoto()
    {
        $res = (new DynamicLandingParent)->mobile_header_photo();
        $this->assertInstanceOf(BelongsTo::class, $res);
    }

    public function testGetUrlLandingAttribute()
    {
        $parent = factory(DynamicLandingParent::class)->make();
        $this->assertIsString($parent->url_landing);
    }

    public function testSetHeaderImage_Filled()
    {
        $desktopHeaderImage = factory(Media::class)->create();
        $mobileHeaderImage = factory(Media::class)->create();
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create([
            'type_kost' => DynamicLandingParent::GOLDPLUS_1,
            'desktop_header_media_id' => $desktopHeaderImage->id,
            'mobile_header_media_id' => $mobileHeaderImage->id,
        ]);

        $dynamicLandingParent->setHeaderImage();

        $this->assertIsArray($dynamicLandingParent->desktop_header_image);
        $this->assertIsArray($dynamicLandingParent->mobile_header_image);
    }
}