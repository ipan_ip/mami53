<?php

namespace App\Entities\Sanjunipero;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DynamicLandingChildTest extends MamiKosTestCase
{
    public function testScopeActive()
    {
        $res = (new DynamicLandingChild)->active();
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testRelationParent()
    {
        $res = (new DynamicLandingChild)->parent();
        $this->assertInstanceOf(BelongsTo::class, $res);
    }

    public function testGetUrlLandingAttribute()
    {
        $parent = factory(DynamicLandingParent::class)->make();
        $child = factory(DynamicLandingChild::class)->make([
            'parent_id' => $parent->id
        ]);
        $child->parent = $parent;
        $this->assertIsString($child->url_landing);
    }
}