<?php

namespace App\Entities\MoEngage\Helper;

use App\Test\MamiKosTestCase;

class MoEngagePayloadHelperTest extends MamiKosTestCase
{
    public function testForAndroid()
    {
        $helper = new MoEngagePayloadHelper();
        $helper->forAndroid();
        $this->assertSame([MoEngagePayloadHelper::WEB_PLATFORM, MoEngagePayloadHelper::ANDROID_PLATFORM], $this->getNonPublicAttributeFromClass('platforms', $helper));
    }

    public function testForIos()
    {
        $helper = new MoEngagePayloadHelper();
        $helper->forIos();
        $this->assertSame([MoEngagePayloadHelper::WEB_PLATFORM, MoEngagePayloadHelper::IOS_PLATFORM], $this->getNonPublicAttributeFromClass('platforms', $helper));
    }

    public function testForWindows()
    {
        $helper = new MoEngagePayloadHelper();
        $helper->forWindows();
        $this->assertSame([MoEngagePayloadHelper::WEB_PLATFORM, MoEngagePayloadHelper::WINDOWS_PLATFORM], $this->getNonPublicAttributeFromClass('platforms', $helper));
    }
}
