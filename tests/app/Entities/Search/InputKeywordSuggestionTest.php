<?php

namespace App\Entities\Search;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Area\AreaGeolocationMapping;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class InputKeywordSuggestionTest extends MamiKosTestCase
{
    public function testGeolocationRelation()
    {
        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        factory(AreaGeolocationMapping::class)->create(
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        $this->assertInstanceOf(Collection::class, $suggestion->geolocation);
    }

    public function testUpdateBySuggestionAndArea()
    {
        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        $request = new Request();
        $request->title = $suggestion->suggestion;
        $request->subtitle = $suggestion->area;
        $request->latitude = 0.1;
        $request->longitude = 0.1;
        $request->place_id = 'ChIJ6Tdt1_5Vei4R2K26P4Ugl-c';
        $request->administrative_type = 'administrative_area_level_3';

        InputKeywordSuggestion::updateBySuggestionAndArea($request);

        $this->assertDatabaseHas('search_input_keyword_suggestion',
            [
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'place_id' => $request->place_id,
                'administrative_type' => $request->administrative_type
            ]
        );
    }
}
