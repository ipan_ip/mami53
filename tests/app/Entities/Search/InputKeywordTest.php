<?php

namespace App\Entities\Search;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\HasMany;

class InputKeywordTest extends MamiKosTestCase
{
    protected $suggestionCount = 5;

    public function testHasSuggestionRelationship()
    {
        $instance = factory(InputKeyword::class)->make();
        $relation = $instance->suggestions();

        $this->assertInstanceOf(HasMany::class, $relation);
        $this->assertInstanceOf(InputKeywordSuggestion::class, $relation->getRelated());
        $this->assertEquals('input_id', $relation->getForeignKeyName());
    }

    public function testGetSuggestions()
    {
        $this->setupInputKeywordData(true);

        $instance = InputKeyword::with('suggestions')->first();
        $this->assertEquals(0, $instance->total);

        $response = $instance->getSuggestions();

        $this->assertEquals(1, InputKeyword::first()->total);
        $this->assertEquals($instance->keyword, $response['keyword']);
        $this->assertCount($this->suggestionCount, $response['suggestions']);
    }

    public function testGetSuggestionsReturnEmptySuggestions()
    {
        $this->setupInputKeywordData();

        $instance = InputKeyword::with('suggestions')->first();
        $this->assertEquals(0, $instance->total);

        $response = $instance->getSuggestions();

        $this->assertEquals(1, InputKeyword::first()->total);
        $this->assertNull($response);
    }

    private function setupInputKeywordData(bool $withSuggestions = false)
    {
        if (InputKeyword::query()->count()) {
            InputKeyword::query()->delete();
        }

        $instance = factory(InputKeyword::class)
            ->state('without-hit')
            ->create();

        if ($withSuggestions) {
            if (InputKeywordSuggestion::query()->count()) {
                InputKeywordSuggestion::query()->delete();
            }

            $instance->suggestions()->saveMany(
                factory(InputKeywordSuggestion::class, $this->suggestionCount)->make()
            );
        }
    }
}
