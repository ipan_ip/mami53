<?php

namespace App\Entities\Search;

use App\Test\MamiKosTestCase;

class GeneralSearchTest extends MamiKosTestCase
{
    public function testGetFiltersAttribute()
    {
        $generalSearch = $this->generateGeneralSearch();

        $filters = $generalSearch->filters;

        $this->assertEquals($generalSearch->price_min, $filters['price_range'][0]);
        $this->assertEquals($generalSearch->price_max, $filters['price_range'][1]);
    }

    public function testExpandLocation()
    {
        $generalSearch = $this->generateGeneralSearch();

        $response = $generalSearch->expandLocation(GeneralSearch::DEFAULT_EXPAND_RANGE, "A");
        $this->assertIsArray($response);
    }

    private function generateGeneralSearch()
    {
        return factory(GeneralSearch::class)->create([
            'slug' => 'malang-kota-malang-jawa-timur-indonesia/all/bulanan/0-10000000',
            'keywords' => 'Malang, Kota Malang, Jawa Timur, Indonesia',
            'gender' => null,
            'rent_type' => 2,
            'price_min' => 0,
            'price_max' => 10000000,
            'longitude' => 112.633,
            'latitude' => -7.96662
        ]);
    }
}
