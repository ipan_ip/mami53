<?php

namespace App\Entities\Voucher;

use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipient;
use App\Test\MamiKosTestCase;

class VoucherRecipientTest extends MamiKosTestCase
{
    public function testVoucherRecipientRelation()
    {
        $program = factory(VoucherProgram::class)->create();
        $recipient = factory(VoucherRecipient::class)->create([
            'voucher_program_id' => $program->id,
        ]);
        
        $this->assertEquals($program->id, $recipient->voucher_program->id);
    }
}