<?php

namespace App\Entities\Voucher;

use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipient;
use App\Test\MamiKosTestCase;

class VoucherProgramTest extends MamiKosTestCase
{
    public function testVoucherProgramRelation()
    {
        $program = factory(VoucherProgram::class)->create();
        $recipient = factory(VoucherRecipient::class)->create([
            'voucher_program_id' => $program->id,
        ]);
        
        $this->assertEquals($recipient->id, $program->recipients->first()->id);
    }
}