<?php

namespace App\Entities\Voucher;

use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipientFile;
use App\Test\MamiKosTestCase;

class VoucherRecipientFileTest extends MamiKosTestCase
{
    public function testVoucherProgramRelation()
    {
        $program = factory(VoucherProgram::class)->create();
        $recipientFile = factory(VoucherRecipientFile::class)->create([
            'voucher_program_id' => $program->id,
        ]);
        
        $this->assertTrue($recipientFile->voucher_program->is($program));
        $this->assertTrue(!empty($recipientFile->stripExtension()));
    }

    public function testGetCachePath()
    {
        $recipientFile = factory(VoucherRecipientFile::class)->create();

        $folderData  = \Config::get('api.media.folder_data');
        $folderCache = \Config::get('api.media.folder_cache');
        
        $this->assertEquals(str_replace($folderData, $folderCache, $recipientFile->file_path), $recipientFile->getCachePath());
    }
}