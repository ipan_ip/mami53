<?php

namespace App\Entities\LandingContent;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\HasMany;

class LandingContentTest extends MamiKosTestCase
{
    public function testFiles()
    {
        $this->assertInstanceOf(HasMany::class, (new LandingContent)->files());
    }

    public function testGetShareUrlAttributeKonten()
    {
        $landingContent = new LandingContent;
        $landingContent->type = 'konten';

        $this->assertSame('/konten/', $landingContent->share_url);
    }

    public function testGetShareUrlAttributeLoker()
    {
        $landingContent = new LandingContent;
        $landingContent->type = 'loker';

        $this->assertSame('/loker/1/post/', $landingContent->share_url);
    }

    public function testGetShareUrlAttributeApartment()
    {
        $landingContent = new LandingContent;
        $landingContent->type = 'apartment';

        $this->assertSame('/apartemen/post/', $landingContent->share_url);
    }
}