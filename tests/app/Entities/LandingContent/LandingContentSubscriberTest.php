<?php

namespace App\Entities\LandingContent;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LandingContentSubscriberTest extends MamiKosTestCase
{
    public function testLandingContent()
    {
        $this->assertInstanceOf(
            BelongsTo::class,
            (new LandingContentSubscriber())->landing_content()
        );
    }
}