<?php

namespace App\Entities\LandingContent;

use App\Test\MamiKosTestCase;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Mockery;
use phpmock\MockBuilder;

class LandingContentFileTest extends MamiKosTestCase
{
    public function testLandingContent()
    {
        $this->assertInstanceOf(BelongsTo::class, (new LandingContentFile)->landing_content());
    }

    public function testUploadStatusFalse()
    {
        $mockMediaHandler = Mockery::mock('alias:App\Entities\Media\MediaHandler');
        $mockMediaHandler->shouldReceive('getFileExtension');
        $mockMediaHandler->shouldReceive('generateFileName');
        $mockMediaHandler->shouldReceive('getOriginalFileDirectory');
        $mockMediaHandler->shouldReceive('getCacheFileDiretory');
        $mockMediaHandler->shouldReceive('storeFile')->andThrow(new Exception());

        $res = (new LandingContentFile)->upload('');

        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testUploadStatusTrue()
    {
        $mockMediaHandler = Mockery::mock('alias:App\Entities\Media\MediaHandler');
        $mockMediaHandler->shouldReceive('getFileExtension');
        $mockMediaHandler->shouldReceive('generateFileName');
        $mockMediaHandler->shouldReceive('getOriginalFileDirectory');
        $mockMediaHandler->shouldReceive('getCacheFileDiretory');
        $mockMediaHandler->shouldReceive('storeFile');

        $res = (new LandingContentFile)->upload('');

        $this->assertIsArray($res);
    }

    public function testGetCacheUrlUpload()
    {
        $this->assertIsString((new LandingContentFile)->getCacheUrl());
    }

    public function testStripExtension()
    {
        $landingContentFile = new LandingContentFile;
        $landingContentFile->file_name = 'testing-file-name.jpg';
        $res = $landingContentFile->stripExtension();

        $this->assertIsString($res);
    }
}