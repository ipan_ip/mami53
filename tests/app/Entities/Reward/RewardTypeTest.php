<?php

namespace App\Entities\Reward;

use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardType;
use App\Test\MamiKosTestCase;

class RewardTypeTest extends MamiKosTestCase
{
    public function testRewardTypeRelation()
    {
        $type = factory(RewardType::class)->create();
        $reward = factory(Reward::class)->create([
            'type_id' => $type->id,
        ]);
        
        $this->assertEquals($reward->id, $type->reward->first()->id);
    }

    public function testRewardRedeem_GetAsDropdown()
    {
        $type = factory(RewardType::class)->create();
        $reward = factory(Reward::class)->create([
            'type_id' => $type->id,
        ]);
        
        $this->assertEquals(2, count($type->getAsDropdown()));
    }
}