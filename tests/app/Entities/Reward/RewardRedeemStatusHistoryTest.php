<?php

namespace App\Entities\Reward;

use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use App\User;
use App\Test\MamiKosTestCase;

class RewardRedeemStatusHistoryTest extends MamiKosTestCase
{
    public function testRewardRedeemStatusHistoryRelation()
    {
        $redeem = factory(RewardRedeem::class)->create();
        $user = factory(User::class)->create();
        $history = factory(RewardRedeemStatusHistory::class)->create([
            'redeem_id' => $redeem->id,
            'user_id' => $user->id,
        ]);
        
        $this->assertEquals($redeem->id, $history->redeem->id);
        $this->assertEquals($user->id, $history->user->id);
    }
}