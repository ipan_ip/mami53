<?php

namespace App\Entities\Reward;

use App\Entities\Level\KostLevel;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardTargetConfig;
use App\Test\MamiKosTestCase;

class RewardTargetConfigTest extends MamiKosTestCase
{
    public function testRewardTargetConfigRelation()
    {
        $reward = factory(Reward::class)->create();
        $kostLevels = factory(KostLevel::class, 3)->create();
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_GOLDPLUS_1,
            'value' => $kostLevels[0]->id,
        ]));

        $config = $reward->target_configs()->first();
        
        $this->assertEquals($reward->id, $config->reward->id);
    }

    public function testRewardTargetConfig_GetOwnerSegments()
    {
        $reward = factory(Reward::class)->create();
        $kostLevels = factory(KostLevel::class, 3)->create();
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_GOLDPLUS_1,
            'value' => $kostLevels[0]->id,
        ]));

        $config = $reward->target_configs()->first();

        $this->assertEquals(7, count($config->getOwnerSegments()));
    }
}