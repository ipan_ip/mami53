<?php

namespace App\Entities\Reward;

use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardRedeem;
use App\User;
use App\Test\MamiKosTestCase;

class RewardRedeemTest extends MamiKosTestCase
{
    public function testRewardRedeemRelation()
    {
        $reward = factory(Reward::class)->create();
        $user = factory(User::class)->create();
        $redeem = factory(RewardRedeem::class)->create([
            'reward_id' => $reward->id,
            'user_id' => $user->id,
        ]);
        $history = factory(RewardRedeemStatusHistory::class)->create([
            'redeem_id' => $redeem->id,
            'user_id' => $user->id,
        ]);
        
        $this->assertEquals($reward->id, $redeem->reward->id);
        $this->assertEquals($user->id, $redeem->user->id);
        $this->assertEquals($history->id, $redeem->status_history->first()->id);
    }

    public function testRewardRedeem_GetStatusList()
    {
        $reward = factory(Reward::class)->create();
        $user = factory(User::class)->create();
        $redeem = factory(RewardRedeem::class)->create([
            'reward_id' => $reward->id,
            'user_id' => $user->id,
        ]);

        $this->assertEquals(4, count($redeem->getStatusList()));
    }

    public function testRewardRedeem_GetStatusDropdown()
    {
        $reward = factory(Reward::class)->create();
        $user = factory(User::class)->create();
        $redeem = factory(RewardRedeem::class)->create([
            'reward_id' => $reward->id,
            'user_id' => $user->id,
        ]);
        
        $this->assertEquals(5, count($redeem->getStatusDropdown()));
    }
}