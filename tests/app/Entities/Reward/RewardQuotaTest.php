<?php

namespace App\Entities\Reward;

use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Test\MamiKosTestCase;

class RewardQuotaTest extends MamiKosTestCase
{
    public function testRewardQuotaRelation()
    {
        $reward = factory(Reward::class)->create();
        $quota = factory(RewardQuota::class)->create([
            'reward_id' => $reward->id,
        ]);
        
        $this->assertEquals($reward->id, $quota->reward->id);
    }
}