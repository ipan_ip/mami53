<?php

namespace App\Entities\Agent;
use App\Test\MamiKosTestCase;

class AgentTest extends MamiKosTestCase
{
    public function testRoom()
    {
        $relation = (new Agent)->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKeyName());
    }

    public function testDummyRoom()
    {
        $relation = (new Agent)->dummy_room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Agent\DummyDesigner', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKeyName());
    }

    public function testAssign()
    {
        $relation = (new Agent)->assign();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Agent\ApartmentAssign', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKeyName());
    }

    public function testDummyReview()
    {
        $relation = (new Agent)->dummy_review();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Agent\DummyReview', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKeyName());
    }
}