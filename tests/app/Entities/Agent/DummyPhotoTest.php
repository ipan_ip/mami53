<?php
namespace App\Entities\Agent;

use App\Test\MamiKosTestCase;

class DummyPhotoTest extends MamiKosTestCase
{
    public function testRoom()
    {
        $relation = (new DummyPhoto)->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('designer_id', $relation->getForeignKey());
    }
}