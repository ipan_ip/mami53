<?php
namespace App\Entities\Agent;

use App\Test\MamiKosTestCase;

class AgentRegistrationTest extends MamiKosTestCase
{
    public function testUser()
    {
        $relation = (new AgentRegistration)->user();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\User', $relation->getRelated());
        $this->assertEquals('user_id', $relation->getForeignKey());
    }
}