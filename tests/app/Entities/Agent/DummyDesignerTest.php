<?php
namespace App\Entities\Agent;

use App\Test\MamiKosTestCase;

class DummyDesignerTest extends MamiKosTestCase
{
    public function testRoom()
    {
        $relation = (new DummyDesigner)->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('designer_id', $relation->getForeignKey());
    }

    public function testAgent()
    {
        $relation = (new DummyDesigner)->agent();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Agent\Agent', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKey());
    }
}