<?php
namespace App\Entities\Agent;

use App\Test\MamiKosTestCase;

class ApartmentAssignTest extends MamiKosTestCase
{
    public function testAgent()
    {
        $relation = (new ApartmentAssign)->agent();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Agent\Agent', $relation->getRelated());
        $this->assertEquals('agent_id', $relation->getForeignKey());
    }
}