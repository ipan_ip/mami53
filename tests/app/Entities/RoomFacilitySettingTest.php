<?php

namespace App\Entities;

use App\Entities\Room\Room;
use App\Entities\Room\RoomFacilitySetting;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomFacilitySettingTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testRoomRelationship()
    {
        $roomFacilitySetting = factory(RoomFacilitySetting::class)->create();

        $room = factory(Room::class)->create();

        $roomFacilitySetting->setRelation('room', $room);

        $this->assertEquals($room->id, $roomFacilitySetting->room->id);
    }
}