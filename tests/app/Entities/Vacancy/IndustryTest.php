<?php

namespace App\Entities\Vacancy;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\HasMany;

class IndustryTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $this->assertInstanceOf(HasMany::class, (new Industry)->vacancy());
    }

    public function testCompanyProfile()
    {
        $this->assertInstanceOf(HasMany::class, (new Industry)->company_profile());
    }

    public function testList()
    {
        $length = 3;
        $industry = factory(Industry::class, $length)->states('active')->create();

        $res = (new Industry)->list();

        $this->assertIsArray($res);
        $this->assertSame($industry->first()->id, $res[0]['key']);
    }
}