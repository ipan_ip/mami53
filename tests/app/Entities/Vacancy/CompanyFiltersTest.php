<?php

namespace App\Entities\Vacancy;

use App\Entities\Vacancy\CompanyProfile;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;

class CompanyFiltersTest extends MamiKosTestCase
{
    use WithFaker;

    public function testDoFilter()
    {
        $industryId = 1;
        $companyProfile = factory(CompanyProfile::class)->states('active')->create([
            'industry_id' => $industryId
        ]);

        $res = (new CompanyFilters([
                'industry' => [$industryId],
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterIndustryNotSet()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterIndustryEmpty()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'industry' => [],
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterAreaNotSet()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterAreaEmpty()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'place' => [],
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterAreaFilled()
    {
        $city = $this->faker->city;
        $companyProfile = factory(CompanyProfile::class)->states('active')->create([
            'city' => $city
        ]);

        $res = (new CompanyFilters([
                'place' => [$city],
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
        $this->assertSame($city, $res->city);
    }

    public function testDoFilterNameEmptyOrNotSet()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterNameFilled()
    {
        $name = $this->faker->state;
        $companyProfile = factory(CompanyProfile::class)->states('active')->create([
            'name' => $name
        ]);

        $res = (new CompanyFilters([
                'name' => $name,
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
        $this->assertSame($name, $res->name);
    }

    public function testDoFilterSortEmpty()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create();

        $res = (new CompanyFilters([
                'sort' => ''
            ]))
            ->doFilter()
            ->first();

        $this->assertInstanceOf(CompanyProfile::class, $res);
        $this->assertSame($companyProfile->id, $res->id);
    }

    public function testDoFilterSortNew()
    {
        $length = 3;
        $companyProfile = factory(CompanyProfile::class, $length)->states('active')->create();
        $companyProfileIds = $companyProfile->pluck('id')->toArray();
        $lastId = end($companyProfileIds);

        $res = (new CompanyFilters([
                'sort' => 'new'
            ]))
            ->doFilter()
            ->get();

        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame($lastId, $res[0]->id);
    }

    public function testDoFilterSortOld()
    {
        $length = 3;
        $companyProfile = factory(CompanyProfile::class, $length)->states('active')->create();
        $companyProfileIds = $companyProfile->pluck('id')->toArray();
        $firstId = reset($companyProfileIds);

        $res = (new CompanyFilters([
                'sort' => 'old'
            ]))
            ->doFilter()
            ->get();

        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame($firstId, $res[0]->id);
    }

    public function testDoFilterSortName()
    {
        $length = 3;
        $companyProfile = factory(CompanyProfile::class, $length)->states('active')->create();

        $res = (new CompanyFilters([
                'sort' => 'name'
            ]))
            ->doFilter()
            ->get();

        $this->assertInstanceOf(Collection::class, $res);
    }
}