<?php

namespace App\Entities\Vacancy;

use App\Libraries\StatisticsLibrary;
use App\Test\MamiKosTestCase;
use App\User;

class VacancyApplyTest extends MamiKosTestCase
{
    public function setUp() : void
    {
        parent::setUp();
        \Config::set('api.media.cdn_url', 'https://static.mamikos.com/');
        \Config::set('api.media.folder_cv', 'uploads/cv');
    }

    public function testGetFileUrlWithEmptyValue()
    {
        $vacancy = factory(VacancyApply::class)->make(['file' => null]);
        $this->assertNull($vacancy->getFileUrl());
    }

    public function testGetFileUrlWithExistingValue()
    {
        $vacancy = factory(VacancyApply::class)->make(['file' => 'somefile.pdf']);
        $this->assertEquals('https://static.mamikos.com/uploads/cv/somefile.pdf', $vacancy->getFileUrl());
    }

    public function testBelongsToVacancy()
    {
        $vacancy = factory(Vacancy::class)->create();
        $vacancyApply = factory(VacancyApply::class)->create([
            'vacancy_id' => $vacancy->id
        ]);
        $this->assertInstanceOf(Vacancy::class, $vacancyApply->vacancy);
    }
    
    public function testBelongsToUser()
    {
        $user = factory(User::class)->create();
        $vacancyApply = factory(VacancyApply::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertInstanceOf(User::class, $vacancyApply->user);
    }

    public function testApplyChecker()
    {
        $user = factory(User::class)->create();
        $vacancy = factory(Vacancy::class)->create();
        factory(VacancyApply::class)->create([
            'vacancy_id' => $vacancy->id,
            'user_id' => $user->id
        ]);
        $applied = VacancyApply::apply_checker($vacancy, $user);
        $this->assertTrue($applied);
    }

    public function testApplyCheckerWithNullUser()
    {
        $vacancy = factory(Vacancy::class)->create();
        factory(VacancyApply::class)->create([
            'vacancy_id' => $vacancy->id
        ]);
        $applied = VacancyApply::apply_checker($vacancy, null);
        $this->assertFalse($applied);
    }

    public function testApplyCheckerWithNoApply()
    {
        $user = factory(User::class)->create();
        $vacancy = factory(Vacancy::class)->create();
        $applied = VacancyApply::apply_checker($vacancy, $user);
        $this->assertFalse($applied);
    }

    public function testGetVacancyApplyWithType()
    {
        $vacancy = factory(Vacancy::class)->create();
        $yesterdayTime = new \DateTime('yesterday 14:00');
        factory(VacancyApply::class, 3)->create([
            'vacancy_id' => $vacancy->id,
            'created_at' =>  $yesterdayTime,
            'updated_at' =>  $yesterdayTime,
        ]);
        $count = VacancyApply::getVacancyApplyWithType($vacancy->id, StatisticsLibrary::RangeYesterday);
        $this->assertEquals(3, $count);
    }
}