<?php

namespace App\Entities\Vacancy;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SpesialisasiTest extends MamiKosTestCase
{
    public function testScopeActive()
    {
        $this->assertInstanceOf(Builder::class, (new Spesialisasi)->ScopeActive());
    }

    public function testVacancy()
    {
        $this->assertInstanceOf(HasMany::class, (new Spesialisasi)->vacancy());
    }

    public function testVacancySpesialisasiType()
    {
        $this->assertInstanceOf(BelongsTo::class, (new Spesialisasi)->vacancy_spesialisasi_type());
    }

    public function testList()
    {
        $length = 3;
        $group = 'general';
        $spesialisasi = factory(Spesialisasi::class, $length)->states($group, 'active')->create();

        $res = (new Spesialisasi)->list($group);

        $this->assertIsArray($res);
        $this->assertSame($res[0]['value'], $spesialisasi->first()->name);
    }
}