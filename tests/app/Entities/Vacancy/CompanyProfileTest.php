<?php

namespace App\Entities\Vacancy;

use App\Entities\Vacancy\CompanyProfile;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CompanyProfileTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $this->assertInstanceOf(HasMany::class, (new CompanyProfile)->vacancy());
    }

    public function testIndustry()
    {
        $this->assertInstanceOf(BelongsTo::class, (new CompanyProfile)->industry());
    }

    public function testPhoto()
    {
        $this->assertInstanceOf(HasOne::class, (new CompanyProfile)->photo());
    }

    public function testSluggable()
    {
        $companyProfile = factory(CompanyProfile::class)->states('active')->create([
            'slug' => ''
        ]);
        $this->assertIsArray($companyProfile->sluggable());
    }
}