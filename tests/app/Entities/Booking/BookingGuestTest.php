<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingGuest;
use App\Entities\Booking\BookingUser;
use App\Test\MamiKosTestCase;

class BookingGuestTest extends MamiKosTestCase
{
    public function testRelationBookingGuestExistSuccess(): void
    {
        // create data booking user
        $bookingUser = factory(BookingUser::class)->create();

        // create data booking guest
        $bookingGuest = factory(BookingGuest::class)->create([
            'booking_user_id' => $bookingUser->id,
        ]);

        // check relationship booking User belongs to booking guest
        $this->assertSame($bookingUser->id, $bookingGuest->booking_user_id);
        $this->assertInstanceOf(BookingUser::class, $bookingGuest->booking_user);
    }

    public function testRelationBookingGuestNotExistSuccess() : void
    {
        // create data booking guest
        $bookingGuest = factory(BookingGuest::class)->create();

        // check relation between booking User not belongs to booking guest
        $this->assertFalse($bookingGuest->booking_user()->exists());
    }
}
