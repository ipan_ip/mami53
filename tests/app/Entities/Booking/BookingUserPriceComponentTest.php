<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingUserPriceComponent;
use App\Entities\Booking\BookingUser;
use App\Test\MamiKosTestCase;

class BookingUserPriceComponentTest extends MamiKosTestCase
{
    public function testRelationBookingUserPriceComponentExistSuccess(): void
    {
        // create data booking user
        $bookingUser = factory(BookingUser::class)->create();

        // create data booking user price component
        $bookingUserPriceComponent = factory(BookingUserPriceComponent::class)->create([
            'booking_user_id' => $bookingUser->id,
        ]);

        // check relationship booking User belongs to booking user price component
        $this->assertSame($bookingUser->id, $bookingUserPriceComponent->booking_user_id);
        $this->assertInstanceOf(BookingUser::class, $bookingUserPriceComponent->booking_user);
    }

    public function testRelationBookingUserPriceComponentNotExistSuccess() : void
    {
        // create data booking user price component
        $bookingUserPriceComponent = factory(BookingUserPriceComponent::class)->create();

        // check relation between booking User not belongs to booking user price component
        $this->assertFalse($bookingUserPriceComponent->booking_user()->exists());
    }
}
