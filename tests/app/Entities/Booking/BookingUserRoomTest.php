<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingDesignerRoom;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Booking\BookingUser;
use App\Test\MamiKosTestCase;

class BookingUserRoomTest extends MamiKosTestCase
{
    public function testRelationBookingUserRoomExistSuccess(): void
    {
        // create data booking user
        $bookingUser = factory(BookingUser::class)->create();

        // create data booking designer room
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create();

        // create data booking user room
        $bookingUserRoom = factory(BookingUserRoom::class)->create([
            'booking_user_id' => $bookingUser->id,
            'booking_designer_room_id' => $bookingDesignerRoom->id,
        ]);

        // check relationship booking User room belongs to booking user and booking designer room
        $this->assertSame($bookingUser->id, $bookingUserRoom->booking_user_id);
        $this->assertInstanceOf(BookingUser::class, $bookingUserRoom->booking_user);

        $this->assertSame($bookingDesignerRoom->id, $bookingUserRoom->booking_designer_room_id);
        $this->assertInstanceOf(BookingDesignerRoom::class, $bookingUserRoom->booking_designer_room);
    }

    public function testRelationBookingUserRoomNotExistSuccess() : void
    {
        // create data booking user room
        $bookingUserRoom = factory(BookingUserRoom::class)->create();

        // check relation between booking User not belongs to booking user and booking designer room
        $this->assertFalse($bookingUserRoom->booking_user()->exists());
        $this->assertFalse($bookingUserRoom->booking_designer_room()->exists());
    }
}
