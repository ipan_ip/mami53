<?php

namespace App\Entities\Booking;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Entities\Booking\BookingAcceptanceRate;

class BookingAcceptanceRateTest extends MamiKosTestCase
{

    public function testRoom()
    {
        $room = factory(Room::class)->create();
        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $result = $bar->room;

        $this->assertSame($room->song_id, $result->song_id);
        $this->assertInstanceOf(Room::class, $result);
    }
}
