<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerRoom;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;

class BookingDesignerRoomTest extends MamiKosTestCase
{
    public function testBookingDesigner(): void
    {
        $expected = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create(['booking_designer_id' => $expected->id]);
        $result = $bookingDesignerRoom->booking_designer;

        $this->assertTrue($result->is($expected));
    }

    public function testScopeActive()
    {
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create();

        $result = $bookingDesignerRoom->active();
        $this->assertNotEmpty($result);
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testActivate()
    {
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create([
            'is_active' => 0
        ]);

        $result = $bookingDesignerRoom->activate();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->is_active);
        $this->assertInstanceOf(BookingDesignerRoom::class, $result);
    }

    public function testDeactivate()
    {
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create([
            'is_active' => 1
        ]);

        $result = $bookingDesignerRoom->deactivate();

        $this->assertNotEmpty($result);
        $this->assertEquals(0, $result->is_active);
        $this->assertInstanceOf(BookingDesignerRoom::class, $result);
    }
}