<?php

namespace App\Entities\Booking;

use App\Entities\Activity\Call;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class BookingDesignerTest extends MamiKosTestCase
{
    
    public function testEntityRelation()
    {
        [
            $userEntity,
            $roomEntity,
            $callEntity,
            $bookingDesignerEntity,
            $bookingDesignerRoomEntity,
            $bookingDesignerPriceComponentEntity,
            $bookingUserEntity
        ] = $this->createEntitiesSet();

        // getting relationship
        $userRetrive                = $bookingDesignerEntity->booking_user()->first()->user;
        $roomRetrive                = $bookingDesignerEntity->room;
        $activeRoomsRetrive         = $bookingDesignerEntity->active_rooms()->first();
        $priceComponentsRetrive     = $bookingDesignerEntity->price_components()->first();
        $bookingUserRetrive         = $bookingDesignerEntity->booking_user()->first();
        $chatRetrive                = $bookingDesignerEntity->chat;
        $basePriceComponentRetrive  = $bookingDesignerEntity->base_price_component()->first();
        $bookingDesignerRoomRetrive = $bookingDesignerEntity->booking_designer_room()->first();

        // run test
        $this->assertInstanceOf(User::class, $userRetrive);
        $this->assertSame($userEntity->id, $userRetrive->id, 'Same User');

        $this->assertInstanceOf(Room::class, $roomRetrive);
        $this->assertSame($roomEntity->id, $roomRetrive->id, 'Same Room');

        $this->assertInstanceOf(BookingDesignerPriceComponent::class, $priceComponentsRetrive);
        $this->assertSame($bookingDesignerPriceComponentEntity->id, $priceComponentsRetrive->id, 'Same Price Component');

        $this->assertInstanceOf(BookingDesignerPriceComponent::class, $basePriceComponentRetrive);
        $this->assertSame($bookingDesignerPriceComponentEntity->id, $basePriceComponentRetrive->id, 'Same Base Price Component');

        $this->assertInstanceOf(BookingUser::class, $bookingUserRetrive);
        $this->assertSame($bookingUserEntity->id, $bookingUserRetrive->id, 'Same Booking User');

        $this->assertInstanceOf(Call::class, $chatRetrive);
        $this->assertSame($callEntity->id, $chatRetrive->id, 'Same Chat');

        $this->assertInstanceOf(BookingDesignerRoom::class, $activeRoomsRetrive);
        $this->assertSame($bookingDesignerRoomEntity->id, $activeRoomsRetrive->id, 'Same Active Room');

        $this->assertInstanceOf(BookingDesignerRoom::class, $bookingDesignerRoomRetrive);
        $this->assertSame($bookingDesignerRoomEntity->id, $bookingDesignerRoomRetrive->id, 'Same Booking Designer Room');
    }

    public function testScopeActiveEmptyData()
    {
        $this->createBookingDesignerEntity([
            'is_active' => 0
        ]);

        $bookingDesigner = BookingDesigner::active()->get();

        // run test
        $this->assertInstanceOf(Collection::class, $bookingDesigner);
        $this->assertEmpty($bookingDesigner);
    }

    public function testScopeActiveSuccess()
    {
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'is_active' => 1
        ]);

        $bookingDesignerRetrive = BookingDesigner::active()->first();

        // run test
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerRetrive);
        $this->assertEquals($bookingDesignerEntity->id, $bookingDesignerRetrive->id);
        $this->assertEquals(1, $bookingDesignerRetrive->is_active);
    }

    public function testActiveSuccess()
    {
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'is_active' => 0
        ]);

        // run test
        $this->assertEquals(0, $bookingDesignerEntity->is_active);

        // change value to is_active
        $bookingDesignerEntity->activate();

        // run test
        $this->assertEquals(1, $bookingDesignerEntity->is_active);
    }

    public function testDeactiveSuccess()
    {
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'is_active' => 1
        ]);

        // run test
        $this->assertEquals(1, $bookingDesignerEntity->is_active);

        // change value to is_active
        $bookingDesignerEntity->deactivate();

        // run test
        $this->assertEquals(0, $bookingDesignerEntity->is_active);
    }

    public function testInsertDataBookingNullPriceMonthly()
    {
        $roomEntity = $this->createRoomEntity(['price_monthly' => 1000000]);
        $bookingDesignerEntity = BookingDesigner::insertData(null, $roomEntity);

        // run test
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerEntity);
        $this->assertEquals(BookingDesigner::BOOKING_TYPE_MONTHLY, $bookingDesignerEntity->type);
        $this->assertEquals(1000000, $bookingDesignerEntity->price);
        $this->assertEquals(1, $bookingDesignerEntity->is_active);
        $this->assertEquals($roomEntity->name, $bookingDesignerEntity->name);
        $this->assertEquals($roomEntity->id, $bookingDesignerEntity->designer_id);
    }

    public function testInsertDataBookingNullPriceDaily()
    {
        $roomEntity = $this->createRoomEntity(['price_daily' => 500000]);
        $bookingDesignerEntity = BookingDesigner::insertData(null, $roomEntity);

        // run test
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerEntity);
        $this->assertEquals(BookingDesigner::BOOKING_TYPE_DAILY, $bookingDesignerEntity->type);
        $this->assertEquals(500000, $bookingDesignerEntity->price);
        $this->assertEquals(1, $bookingDesignerEntity->is_active);
        $this->assertEquals($roomEntity->name, $bookingDesignerEntity->name);
        $this->assertEquals($roomEntity->id, $bookingDesignerEntity->designer_id);
    }

    public function testInsertDataBookingIsExist()
    {
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id,
            'is_active' => 0
        ]);

        // run test
        $this->assertEquals(0, $bookingDesignerEntity->is_active);

        $bookingDesignerEntity = BookingDesigner::insertData(null, $roomEntity);

        // run test
        $this->assertEquals(1, $bookingDesignerEntity->is_active);
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerEntity);
    }

    public function testBookingRoomActiveNull()
    {
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id,
            'is_active' => 0
        ]);

        // run test
        $this->assertEquals(0, $bookingDesignerEntity->is_active);

        $bookingDesignerEntity = BookingDesigner::bookingRoomActive($roomEntity->id, $bookingDesignerEntity->id);

        // run test
        $this->assertNull($bookingDesignerEntity);
    }

    public function testBookingRoomActiveSuccess()
    {
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id,
            'is_active' => 1
        ]);

        // run test
        $this->assertEquals(1, $bookingDesignerEntity->is_active);

        $bookingDesignerEntity = BookingDesigner::bookingRoomActive($roomEntity->id, $bookingDesignerEntity->id);

        // run test
        $this->assertEquals($roomEntity->id, $bookingDesignerEntity->designer_id);
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerEntity);
    }

    public function testUpdatePriceNull()
    {
        $bookingDesignerEntity = BookingDesigner::updatePrice(100000, 1);
        $this->assertNull($bookingDesignerEntity);
    }

    public function testUpdatePriceSuccess()
    {
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id,
            'type' => BookingDesigner::BOOKING_TYPE_MONTHLY
        ]);
        $bookingDesignerRetrive = BookingDesigner::updatePrice(100000, $roomEntity->id);

        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerRetrive);
        $this->assertEquals($bookingDesignerEntity->id, $bookingDesignerRetrive->id);
        $this->assertEquals(100000, $bookingDesignerRetrive->price);
    }

    public function testGetRoomIncludingDeletedSuccess()
    {
        $roomEntity = $this->createRoomEntity(['deleted_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id,
            'type' => BookingDesigner::BOOKING_TYPE_MONTHLY
        ]);

        $roomRetrive = $bookingDesignerEntity->getRoomIncludingDeleted();

        // run test
        $this->assertEquals($roomEntity->id, $roomRetrive->id);
        $this->assertEquals($roomEntity->name, $roomRetrive->name);
        $this->assertNotNull($roomRetrive->deleted_at);
    }

    private function createEntitiesSet(): array
    {
        $userEntity = $this->createUserEntity();
        $roomEntity = $this->createRoomEntity();
        $callEntity = $this->createCallEntity([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id
        ]);
        // booking designer
        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingDesignerRoomEntity = $this->createBookingDesignerRoomEntity([
            'booking_designer_id' => $bookingDesignerEntity->id,
            'is_active' => 1,
            'name' => 1
        ]);

        $bookingDesignerPriceComponentEntity = $this->createBookingDesignerPriceComponentEntity([
            'booking_designer_id' => $bookingDesignerEntity->id,
            'price_name' => 'base',
            'price_label' => 'Harga Perbulan',
            'regular_price' => 1000000,
            'price' => 1000000
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesignerEntity->id,
            'user_id' => $userEntity->id
        ]);

        return [
            $userEntity,
            $roomEntity,
            $callEntity,
            $bookingDesignerEntity,
            $bookingDesignerRoomEntity,
            $bookingDesignerPriceComponentEntity,
            $bookingUserEntity
        ];
    }

    private function createBookingDesignerEntity(array $overrideData = []): BookingDesigner
    {
        return factory(BookingDesigner::class)->create($overrideData);
    }

    private function createUserEntity(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }

    private function createRoomEntity(array $overrideData = []): Room
    {
        return factory(Room::class)->create($overrideData);
    }

    private function createBookingDesignerRoomEntity(array $overrideData = []): BookingDesignerRoom
    {
        return factory(BookingDesignerRoom::class)->create($overrideData);
    }

    private function createBookingDesignerPriceComponentEntity(array $overrideData = []): BookingDesignerPriceComponent
    {
        return factory(BookingDesignerPriceComponent::class)->create($overrideData);
    }

    private function createBookingUserEntity(array $overrideData = []): BookingUser
    {
        return factory(BookingUser::class)->create($overrideData);
    }

    private function createCallEntity(array $overrideData = []): Call
    {
        return factory(Call::class)->create($overrideData);
    }
}