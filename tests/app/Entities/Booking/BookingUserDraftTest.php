<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class BookingUserDraftTest extends MamiKosTestCase
{
    public function testRelationBookingUserDraftExistSuccess(): void
    {
        // create data room
        $room = factory(Room::class)->create();

        // create data booking guest
        $bookingUserDraft = factory(BookingUserDraft::class)->create([
            'designer_id' => $room->id,
        ]);

        // check relationship booking User Draft belongs to room
        $this->assertSame($room->id, $bookingUserDraft->designer_id);
        $this->assertInstanceOf(Room::class, $bookingUserDraft->room);
    }

    public function testRelationBookingUserDraftNotExistSuccess() : void
    {
        // create data booking guest
        $bookingUserDraft = factory(BookingUserDraft::class)->create();

        // check relation between booking User not belongs to booking guest
        $this->assertFalse($bookingUserDraft->room()->exists());
    }
}
