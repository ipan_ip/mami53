<?php

namespace App\Entities\Booking;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;

class BookingRejectTest extends MamiKosTestCase
{
    private const REJECT_BOOKING = 'admin/booking/reject-reason/';

    public function testEntityIsExists()
    {
        $bookingRejectReason = factory(BookingReject::class)->create();

        $this->assertNotNull($bookingRejectReason);
    }

    public function testScopeSearchByDescription()
    {
        $bookingRejectReason = factory(BookingReject::class)->create([
            'description' => 'vento deco'
        ]);

        $description = "v";
        

        $result = $bookingRejectReason->searchByDescription($description);
        $this->assertInstanceOf(Builder::class, $result);
    }
    
    public function testScopeByType()
    {
        $bookingRejectReason = factory(BookingReject::class)->create([
            'type' => 'cancel'
        ]);

        $type = 'cancel';

        $result = $bookingRejectReason->searchByType($type);

        $this->assertInstanceOf(Builder::class, $result);
    }

}


