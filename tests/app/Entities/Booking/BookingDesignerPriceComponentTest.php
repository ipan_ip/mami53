<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Entities\Booking\BookingDesigner;
use App\Test\MamiKosTestCase;

class BookingDesignerPriceComponentTest extends MamiKosTestCase
{
    public function testRelationBookingDesignerPriceComponentExistSuccess(): void
    {
        // create data booking designer
        $bookingDesigner = factory(BookingDesigner::class)->create();

        // create data booking designer price component
        $bookingDesignerPriceComponent = factory(BookingDesignerPriceComponent::class)->create([
            'booking_designer_id' => $bookingDesigner->id,
        ]);

        // check relationship booking designer belongs to booking designer price component
        $this->assertSame($bookingDesigner->id, $bookingDesignerPriceComponent->booking_designer_id);
        $this->assertInstanceOf(BookingDesigner::class, $bookingDesignerPriceComponent->booking_designer);
    }

    public function testRelationBookingDesignerPriceComponentNotExistSuccess(): void
    {
        // create data booking designer price component
        $bookingDesignerPriceComponent = factory(BookingDesignerPriceComponent::class)->create();

        // check relation between booking designer not belongs to booking designer price component
        $this->assertFalse($bookingDesignerPriceComponent->booking_designer()->exists());
    }
}
