<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingStatus;
use App\Entities\Booking\BookingUser;
use App\Test\MamiKosTestCase;

class BookingStatusTest extends MamiKosTestCase
{
    public function testRelationBookingUserExistSuccess() : void
    {
        // create data booking user
        $bookingUser = factory(BookingUser::class)->create();

        // create data booking status
        $bookingStatus = factory(BookingStatus::class)->create(['booking_user_id' => $bookingUser->id]);

        // check relationship booking status belongs to booking user
        $this->assertInstanceOf(BookingUser::class, $bookingStatus->booking_user);
    }

    public function testRelationBookingUserNotExistSuccess() : void
    {
        // create data booking status
        $bookingStatus = factory(BookingStatus::class)->create();

        // check relation between booking status not belongs to booking user
        $this->assertFalse($bookingStatus->booking_user()->exists());
    }
}
