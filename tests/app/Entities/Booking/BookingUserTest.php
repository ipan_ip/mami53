<?php

namespace App\Entities\Booking;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Level\KostLevel;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\SLA\SLAConfiguration;
use App\Entities\SLA\SLAVersion;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Builder;

class BookingUserTest extends MamiKosTestCase
{
    
    public function testEntityRelation()
    {
        // create data
        [
            $user,
            $room,
            $bookingDesigner,
            $mamipayContract,
            $bookingUser,
            $bookingUserRoom,
            $bookingUserPriceComponent,
            $bookingGuest,
            $bookingStatus,
            $consultantNote,
            $flashSale,
            $bookingRejectReason,
        ] = $this->createEntitiesSet();

        // getting relationship date from booking user
        $userRetrive                        = $bookingUser->user()->first();
        $bookingDesignerRetrive             = $bookingUser->booking_designer()->first();
        $mamipayContractRetrive             = $bookingUser->contract()->first();
        $bookingUserRoomRetrive             = $bookingUser->booking_user_rooms()->first();
        $bookingUserPriceComponentRetrive   = $bookingUser->price_components()->first();
        $bookingGuestRetrive                = $bookingUser->guests()->first();
        $bookingStatusRetrive               = $bookingUser->statuses()->first();
        $bookingStatusChaneRetrive          = $bookingUser->booking_status_changes()->first();
        $flashSaleRetrive                   = $bookingUser->booking_user_flash_sale()->first();
        $bookingRejectReasonRetrive         = $bookingUser->booking_reject_reason()->first();

        // run test
        $this->assertSame($user->name, $userRetrive->name, 'Same User');
        $this->assertSame($bookingDesigner->id, $bookingDesignerRetrive->id, 'Same Booking Designer');
        $this->assertSame($mamipayContract->id, $mamipayContractRetrive->id, 'Same Mamipay Contract');
        $this->assertSame($bookingUserRoom->id, $bookingUserRoomRetrive->id, 'Same Booking User Room');
        $this->assertSame($bookingUserPriceComponent->id, $bookingUserPriceComponentRetrive->id, 'Same Booking User Price Component');
        $this->assertSame($bookingGuest->id, $bookingGuestRetrive->id, 'Same Booking Guest');
        $this->assertSame($bookingStatus->id, $bookingStatusRetrive->id, 'Same Booking Status');
        $this->assertSame($bookingStatus->id, $bookingStatusChaneRetrive->id, 'Same Booking Status Changed');
        $this->assertSame($flashSale->booking_user_id, $flashSaleRetrive->booking_user_id, 'Same Booking Flash Sale');
        $this->assertSame($bookingRejectReason->id, $bookingRejectReasonRetrive->id, 'Same Booking Reject');
    }

    public function testGenerateBookingCodeSuccess()
    {
        // create booking user
        $bookingUserEntity = $this->createBookingUserEntity([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'checkin_date' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        // update booking code
        $bookingUserEntity->generateBookingCode();
        // run test
        $this->assertEquals($this->generateBookingCode($bookingUserEntity), $bookingUserEntity->booking_code);
    }

    public function testBookingUserActiveEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $bookingUser = new BookingUser();
        $bookingUser->bookingUserActive();
    }

    public function testBookingUserActiveEmptyData()
    {
        $bookingUser = new BookingUser();
        $this->assertEmpty($bookingUser->bookingUserActive([]));
    }

    public function testBookingUserActiveSuccess()
    {
        $userEntity = $this->createUserEntity();
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'checkout_date' => Carbon::today()->addMonths(3)
        ]);
        $bookingActive = $bookingUserEntity->bookingUserActive($userEntity);

        $this->assertEquals($bookingUserEntity->id, $bookingUserEntity->id);
        $this->assertEquals($userEntity->id, $bookingActive->first()->user_id);
    }

    public function testExistingRoomBookedEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $bookingUser = new BookingUser();
        $bookingUser->existingRoomBooked();
    }

    public function testExistingRoomBookedEmptyData()
    {
        $userEntity = $this->createUserEntity();
        $roomEntity = $this->createRoomEntity();

        $bookingUser = new BookingUser();
        $this->assertEmpty($bookingUser->existingRoomBooked($userEntity, $roomEntity));
    }

    public function testExistingRoomBookedSuccess()
    {
        $userEntity = $this->createUserEntity();

        $roomEntity = $this->createRoomEntity();
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);
        $bookingUserEntity->existingRoomBooked($userEntity, $roomEntity);
        $bookingUserEntity = $bookingUserEntity->first();

        $this->assertEquals($bookingUserEntity->id, $bookingUserEntity->id);
        $this->assertEquals($userEntity->id, $bookingUserEntity->user_id);
        $this->assertEquals($roomEntity->song_id, $bookingUserEntity->designer_id);
    }

    public function testGetByStatusBookedEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $bookingUser = new BookingUser();
        $bookingUser->getByStatusBooked();
    }

    public function testGetByStatusBookedEmptyData()
    {
        $bookingUser = new BookingUser();
        $this->assertEmpty($bookingUser->getByStatusBooked(1));
    }

    public function testGetByStatusBookedSuccess()
    {
        $userEntity = $this->createUserEntity();

        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);
        $bookingUserEntity->getByStatusBooked($userEntity->id);

        $this->assertEquals($bookingUserEntity->id, $bookingUserEntity->id);
        $this->assertEquals($userEntity->id, $bookingUserEntity->user_id);
    }

    public function testGetByBookingCodeEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $bookingUser = new BookingUser();
        $bookingUser->getByBookingCode();
    }

    public function testGetByBookingCodeEmptyData()
    {
        $bookingUser = new BookingUser();
        $this->assertEmpty($bookingUser->getByBookingCode(1, 1));
    }

    public function testGetByBookingCodeSuccess()
    {
        $userEntity = $this->createUserEntity();
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id
        ]);
        $bookingUserEntity->generateBookingCode();
        $bookingUserEntity->getByBookingCode($bookingUserEntity->booking_code, $userEntity->id);

        $this->assertEquals($bookingUserEntity->id, $bookingUserEntity->id);
        $this->assertEquals($userEntity->id, $bookingUserEntity->user_id);
    }

    public function testBookingUserActiveRoomsEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $bookingUser = new BookingUser();
        $bookingUser->bookingUserActiveRooms();
    }

    public function testBookingUserActiveRoomsEmptyData()
    {
        $bookingUser = new BookingUser();
        $this->assertEmpty($bookingUser->bookingUserActiveRooms([], [1]));
    }

    public function testBookingUserActiveRoomsSuccess()
    {
        $userEntity = $this->createUserEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity();
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'booking_designer_id' => $bookingDesignerEntity->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);
        $bookingUserEntity->bookingUserActiveRooms($userEntity, [$bookingDesignerEntity->id]);

        $this->assertEquals($bookingUserEntity->id, $bookingUserEntity->id);
        $this->assertEquals($userEntity->id, $bookingUserEntity->user_id);
        $this->assertEquals($bookingDesignerEntity->id, $bookingUserEntity->booking_designer_id);
        $this->assertEquals(BookingUser::BOOKING_STATUS_BOOKED, $bookingUserEntity->status);
    }

    public function testGetRoomPriceEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);
        BookingUser::getRoomPrice();
    }

    public function testGetRoomPriceSuccess()
    {
        $roomEntity = $this->createRoomEntity([
            'price_yearly' => 20000000,
            'price_monthly' => 2000000,
            'price_weekly' => 1000000,
        ]);
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        $this->assertEquals(20000000, BookingUser::getRoomPrice($roomEntity, BookingUser::YEARLY_TYPE));
        $this->assertEquals(2000000, BookingUser::getRoomPrice($roomEntity, BookingUser::MONTHLY_TYPE));
        $this->assertEquals(1000000, BookingUser::getRoomPrice($roomEntity, BookingUser::WEEKLY_TYPE));
    }

    public function testGetListingPriceAttributeSuccess()
    {
        $roomEntity = $this->createRoomEntity([
            'price_monthly' => 2000000,
        ]);
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        $bookingDesigner = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id,
            'rent_count_type' => BookingUser::MONTHLY_TYPE 
        ]);

        $this->assertEquals(2000000, $bookingUserEntity->getListingPriceAttribute());
    }

    public function testGetOriginalPriceAttributeSuccess()
    {
        $roomEntity = $this->createRoomEntity([
            'price_monthly' => 2000000,
        ]);
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        $bookingDesigner = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id,
            'rent_count_type' => BookingUser::MONTHLY_TYPE
        ]);

        $this->assertEquals(2000000, $bookingUserEntity->getOriginalPriceAttribute());
    }

    public function testIsRoomDeletedFalseSuccess()
    {
        $roomEntity = $this->createRoomEntity([
            'price_monthly' => 2000000,
        ]);

        $bookingDesigner = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $this->assertFalse($bookingUserEntity->isRoomDeleted());
    }

    public function testIsRoomDeletedTrueSuccess()
    {
        $bookingDesigner = $this->createBookingDesignerEntity();
        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $this->assertTrue($bookingUserEntity->isRoomDeleted());
    }

    public function testScopeFunctionSuccess()
    {
        $bookingUserEntity = $this->createBookingUserEntity([
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'contact_phone' => '082234167880',
            'contact_name' => 'Syifandi Mulyanto',
            'is_expired' => 0
        ]);

        $this->createBookingStatusEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'changed_by_role' => 'admin'
        ]);

        $bookingUserQuery = BookingUser::filterByStatus(BookingUser::BOOKING_STATUS_VERIFIED)
            ->searchByPhoneNumber('082234167880')
            ->searchByContactName('fandi')
            ->filterByStatusChangedBy('admin', BookingUser::BOOKING_STATUS_VERIFIED)
            ->filterByMultipleStatus(BookingUser::BOOKING_STATUS_VERIFIED)
            ->filterByPaymentStatus('paid')
            ->sort('first_requested')
            ->notExpired()
            ->active()
            ->paid()
            ->first();

        $this->assertEquals($bookingUserEntity->id, $bookingUserQuery->id);
        $this->assertEquals($bookingUserEntity->contact_phone, $bookingUserQuery->contact_phone);
        $this->assertEquals($bookingUserEntity->contact_name, $bookingUserQuery->contact_name);
        $this->assertEquals($bookingUserEntity->status, $bookingUserQuery->status);
        $this->assertEquals($bookingUserEntity->is_expired, $bookingUserQuery->is_expired);
    }

    public function testUpdateStatusWhenRoomDeletedEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);
        BookingUser::updateStatusWhenRoomDeleted();
    }

    public function testUpdateStatusWhenRoomDeletedSuccess()
    {
        $roomEntity = $this->createRoomEntity();
        $this->createBookingUserEntity([
            'designer_id' => $roomEntity->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $this->assertTrue(BookingUser::updateStatusWhenRoomDeleted($roomEntity->id));
    }

    public function testRoomFacilitiesSuccess()
    {
        $roomEntity = $this->createRoomEntity();
        $bookingDesigner = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $this->createBookingUserRoomEntity([
            'booking_user_id' => $bookingUserEntity->id
        ]);
        $facilities = $bookingUserEntity->roomFacilities();
        $this->assertArrayHasKey('top_facilities', $facilities);
        $this->assertArrayHasKey('fac_room', $facilities);
        $this->assertArrayHasKey('fac_share', $facilities);
        $this->assertArrayHasKey('fac_bath', $facilities);
        $this->assertArrayHasKey('fac_near', $facilities);
        $this->assertArrayHasKey('fac_park', $facilities);
        $this->assertArrayHasKey('fac_price', $facilities);
    }

    public function testRoomPricesSuccess()
    {
        $roomEntity = $this->createRoomEntity([
            'price_yearly' => 20000000,
            'price_monthly' => 2000000,
            'price_weekly' => 1000000,
            'price_daily' => 150000,
        ]);

        $bookingDesigner = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);

        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $roomPrice = $bookingUserEntity->roomPrices();

        $this->assertIsArray($roomPrice);
        $this->assertEquals(150000, $roomPrice['daily']);
        $this->assertEquals(1000000, $roomPrice['weekly']);
        $this->assertEquals(2000000, $roomPrice['monthly']);
        $this->assertEquals(0, $roomPrice['quarterly']);
        $this->assertEquals(0, $roomPrice['semiannualy']);
        $this->assertEquals(20000000, $roomPrice['yearly']);
    }

    public function testGetBookingDetailFromMamipaySuccess()
    {
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => 1
        ]);
        $this->assertNull($bookingUserEntity->getBookingDetailFromMamipay());
    }

    public function testTotalOfPreebookSuccess()
    {
        $roomEntity = $this->createRoomEntity();
        // update song id
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        // create booking data
        $this->createBookingUserEntity([
            'designer_id' => $roomEntity->song_id,
            'checkin_date' => Carbon::today()->addMonth(6)->format('Y-m-d')
        ]);

        // tun test
        $totalOfPrebook = BookingUser::totalOfPreebook($roomEntity, config('booking.prebook_checkin_normal_month'));
        $this->assertEquals(1, $totalOfPrebook);
    }

    public function testScopeSearchByKostLevel(): void
    {
        $roomEntity = $this->createRoomEntity();

        // level
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);

        // add level to kost / room
        $roomEntity->changeLevel($level->id);

        // create booking
        $bookingUserDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);
        $bookingUserEntity = $this->createBookingUserEntity([
            'booking_designer_id' => $bookingUserDesignerEntity->id
        ]);
        $arr = [];
        array_push($arr, $level->id);
        // run test
        $result = $bookingUserEntity->searchByKostLevel($arr);
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testScopeSearchByBookingCode()
    {
        $bookingUserEntity = $this->createBookingUserEntity(
            [
                'booking_code' => 'ABC123'
            ]
        );

        $result = $bookingUserEntity->searchByBookingCode('ABC123');
        $this->assertInstanceOf(Builder::class, $result);
    }

    /**
     * This function is replicate from BookingUser->generateBookingCode
     */
    private function generateBookingCode(BookingUser $bookingUser): string
    {
        $bookingCodePrefix = 'MAMI';
        $bookingCheckinDate = $bookingUser->created_at->format('m') . Carbon::parse($bookingUser->checkin_date)->format('m');
        $bookingCodeSequence = str_pad($bookingUser->id, 6, '0', STR_PAD_LEFT);

        return $bookingCodePrefix . $bookingCheckinDate  . $bookingCodeSequence;
    }

    private function createBookingDesignerEntity(array $overrideData = []): BookingDesigner
    {
        return factory(BookingDesigner::class)->create($overrideData);
    }

    private function createUserEntity(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }

    private function createBookingUserRoomEntity(array $overrideData = []): BookingUserRoom
    {
        return factory(BookingUserRoom::class)->create($overrideData);
    }

    private function createBookingUserPriceComponentEntity(array $overrideData = []): BookingUserPriceComponent
    {
        return factory(BookingUserPriceComponent::class)->create($overrideData);
    }

    private function createBookingGuestEntity(array $overrideData = []): BookingGuest
    {
        return factory(BookingGuest::class)->create($overrideData);
    }

    private function createBookingStatusEntity(array $overrideData = []): BookingStatus
    {
        return factory(BookingStatus::class)->create($overrideData);
    }

    private function createMamipayContractEntity(array $overrideData = []): MamipayContract
    {
        return factory(MamipayContract::class)->create($overrideData);
    }

    private function createRoomEntity(array $overrideData = []): Room
    {
        return factory(Room::class)->create($overrideData);
    }

    private function createConsultanNoteEntity(array $overrideData = []): ConsultantNote
    {
        return factory(ConsultantNote::class)->create($overrideData);
    }

    private function createBookingUserEntity(array $overrideData = []): BookingUser
    {
        return factory(BookingUser::class)->create($overrideData);
    }

    private function createBookingUserFlashSaleEntity(array $overrideData = []): BookingUserFlashSale
    {
        return factory(BookingUserFlashSale::class)->create($overrideData);
    }

    private function createFlashSaleEntity(array $overrideData = []): FlashSale
    {
        return factory(FlashSale::class)->create($overrideData);
    }

    private function createBookingRejectReasonEntity(array $overrideData = []): BookingReject
    {
        return factory(BookingReject::class)->create($overrideData);
    }

    private function createEntitiesSet(): array
    {
        // before main entity
        $userEntity = $this->createUserEntity();
        $roomEntity = $this->createRoomEntity();
        $roomEntity->song_id = $roomEntity->id;
        $roomEntity->save();

        $bookingDesignerEntity = $this->createBookingDesignerEntity([
            'designer_id' => $roomEntity->id
        ]);
        $mamipayContractEntity = $this->createMamipayContractEntity();

        $rejectReasonEntity = $this->createBookingRejectReasonEntity();

        // main entity
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'booking_designer_id' => $bookingDesignerEntity->id,
            'contract_id' => $mamipayContractEntity->id,
            'booking_reject_reason_id' => $rejectReasonEntity->id,
        ]);

        // after main entity
        $bookingUserRoomsEntity = $this->createBookingUserRoomEntity([
            'booking_user_id' => $bookingUserEntity->id
        ]);

        $bookingUserPriceComponentEntity = $this->createBookingUserPriceComponentEntity([
            'booking_user_id' => $bookingUserEntity->id
        ]);

        $bookingGuestEntity = $this->createBookingGuestEntity([
            'booking_user_id' => $bookingUserEntity->id
        ]);

        $bookingStatusEntity = $this->createBookingStatusEntity([
            'booking_user_id' => $bookingUserEntity->id
        ]);

        $consultantNoteEntity = $this->createConsultanNoteEntity([
            'noteable_id' => $bookingUserEntity->id
        ]);

        $flashSaleEntity  = $this->createFlashSaleEntity();

        $bookingUserFlashSaleEntity = $this->createBookingUserFlashSaleEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'flash_sale_id' => $flashSaleEntity->id
        ]);

        return [
            $userEntity,
            $roomEntity,
            $bookingDesignerEntity,
            $mamipayContractEntity,
            $bookingUserEntity,
            $bookingUserRoomsEntity,
            $bookingUserPriceComponentEntity,
            $bookingGuestEntity,
            $bookingStatusEntity,
            $consultantNoteEntity,
            $bookingUserFlashSaleEntity,
            $rejectReasonEntity,
        ];
    }

    public function testGetExpiredDateTenantNotPaid()
    {
        $userEntity = $this->createUserEntity();

        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
            'expired_date' => Carbon::today()
        ]);

        $this->createBookingStatusEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
            'created_at' => Carbon::today()
        ]);

        $getExpiredDateBooking = $bookingUserEntity->getExpiredDateTenantNotPaid();
        $this->assertNotNull($getExpiredDateBooking);
        $this->assertEquals(Carbon::today(), $getExpiredDateBooking);
    }

    public function testGetExpiredDateTenantNotPaidIsNull()
    {
        $userEntity = $this->createUserEntity();

        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
        ]);

        $this->createBookingStatusEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
            'created_at' => Carbon::today()
        ]);

        $getExpiredDateBooking = $bookingUserEntity->getExpiredDateTenantNotPaid();
        $this->assertNull($getExpiredDateBooking);
    }

    public function testGetExpiredDateBookingNotConfirmed()
    {
        $userEntity = $this->createUserEntity();

        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $userEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER
        ]);

        $this->createBookingStatusEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER,
            'created_at' => Carbon::today()
        ]);

        $getExpiredDateBooking = $bookingUserEntity->getExpiredDateBookingNotConfirmed();
        $this->assertNotNull($getExpiredDateBooking);
        $this->assertEquals(Carbon::today(), $getExpiredDateBooking);
    }

    public function testRoom(): void
    {
        $room = factory(Room::class)->create();
        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $this->assertTrue(
            $booking->room->is($room)
        );
    }

    public function testBookingUserRoom(): void
    {
        $booking = factory(BookingUser::class)->create();
        $room = factory(BookingUserRoom::class)->create(['booking_user_id' => $booking->id]);

        $this->assertTrue(
            $booking->booking_user_room->is($room)
        );
    }

    public function testGetConsultantStatusFormatWithBookedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_BOOKED, null);
        $expected = 'Butuh Konfirmasi';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithConfirmedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_CONFIRMED, null);
        $expected = 'Tunggu Pembayaran';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithVerifiedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_VERIFIED, null);
        $expected = 'Terbayar';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithRejectedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_REJECTED, null);
        $expected = 'Ditolak Pemilik';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormetWithRejectedByConsultantStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_REJECTED, null, 'consultant');
        $expected = 'Ditolak Admin';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithRejectedByAdminStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_REJECTED, null, 'admin');
        $expected = 'Ditolak Admin';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithRejectedByOwnerStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_REJECTED, null, 'owner');
        $expected = 'Ditolak Pemilik';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithRejectedBySystemStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_REJECTED, null, 'system');
        $expected = 'Kadaluwarsa oleh Pemilik';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithCancelByAdminStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN, null);
        $expected = 'Ditolak Admin';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithCancelledStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_CANCELLED, null);
        $expected = 'Dibatalkan';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithVerifiedAndAllowDisburseStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_VERIFIED, BookingUser::ALLOW_DISBURSE);
        $expected = 'Terbayar';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithVerifiedAndDisbursedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_VERIFIED, BookingUser::DISBURSED);
        $expected = 'Pembayaran Diterima';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithExpiredByOwnerStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER, null);
        $expected = 'Kadaluwarsa oleh Pemilik';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithExpiredStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_EXPIRED, null);
        $expected = 'Kadaluwarsa oleh Penyewa';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithFinishedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_FINISHED, null);
        $expected = 'Sewa Berakhir';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatWithTerminatedStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_TERMINATED, null);
        $expected = 'Sewa Berakhir';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusWithCheckedInStatus(): void
    {
        $actual = BookingUser::getConsultantStatusFormat(BookingUser::BOOKING_STATUS_CHECKED_IN, null);
        $expected = 'Terbayar';

        $this->assertEquals($expected, $actual);
    }

    public function testGetConsultantStatusFormatShouldReturnNull(): void
    {
        $actual = BookingUser::getConsultantStatusFormat('test', null);

        $this->assertNull($actual);
    }

    public function testGetDueDateAttributeShouldUseVersionTwo(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-17 17:00:00'
        ]);

        factory(SLAConfiguration::class)->create([
            'case' => SLAConfiguration::CASE_EQUAL,
            'sla_version_id' => $sla->id,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'checkin_date' => '2020-08-19 18:00:00',
            'created_at' => '2020-08-19 18:00:00'
        ]);

        $this->assertEquals(
            '2020-08-19 19:00:00',
            $booking->due_date->toDateTimeString()
        );
    }

    public function testGetDueDateAttributeShouldUseVersionThree(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => '2020-08-17 17:00:00'
        ]);

        factory(SLAConfiguration::class)->create([
            'case' => SLAConfiguration::CASE_EQUAL,
            'sla_version_id' => $sla->id,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'checkin_date' => '2020-08-19 18:00:00',
            'created_at' => '2020-08-19 18:00:00',
            'updated_at' => '2020-08-19 18:00:00'
        ]);

        $this->assertEquals(
            '2020-08-19 19:00:00',
            $booking->due_date->toDateTimeString()
        );
    }

    public function testGetDueDateAttributeWithOtherStatusShouldReturnNull(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => '2020-08-17 17:00:00'
        ]);

        factory(SLAConfiguration::class)->create([
            'case' => SLAConfiguration::CASE_EQUAL,
            'sla_version_id' => $sla->id,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_EXPIRED_LIMIT,
            'checkin_date' => '2020-08-19 18:00:00',
            'created_at' => '2020-08-19 18:00:00',
            'updated_at' => '2020-08-19 18:00:00'
        ]);

        $this->assertNull(
            $booking->due_date
        );
    }

    public function testDeletedNotes(): void
    {
        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_VERIFIED
        ]);

        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $bookingUser->id,
            'deleted_at' => Carbon::now()
        ]);

        factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $bookingUser->id
        ]);

        $this->assertTrue(
            $bookingUser->deletedNotes->first()->is($note)
        );

        $this->assertCount(1, $bookingUser->deletedNotes);
    }

    protected function createBookingWithInstantBookingOwner(): BookingUser
    {
        $kost = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $booking = factory(BookingUser::class)->create(['booking_designer_id' => $bookingDesigner->id]);
        $owner = factory(User::class)->create();
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => $owner->id]);

        return $booking;
    }

    protected function createBookingWithNonInstantBookingOwner(): BookingUser
    {
        $kost = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $booking = factory(BookingUser::class)->create(['booking_designer_id' => $bookingDesigner->id]);
        $owner = factory(User::class)->create();
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => false]);
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => $owner->id]);

        return $booking;
    }

    public function testScopeFilterByInstantBookingWithTrue(): void
    {
        $expected = $this->createBookingWithInstantBookingOwner();
        $notExpected = $this->createBookingWithNonInstantBookingOwner();

        $actual = BookingUser::filterByInstantBooking(true)->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testScopeFilterByInstantBookingWithFalse(): void
    {
        $notExpected = $this->createBookingWithInstantBookingOwner();
        $expected = $this->createBookingWithNonInstantBookingOwner();

        $actual = BookingUser::filterByInstantBooking(false)->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testFilterByTimespanWithTodayShouldFind(): void
    {
        $expected = factory(BookingUser::class)->create(['created_at' => Carbon::now()]);

        $actual = BookingUser::filterByTimespan('today')->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testFilterByTimespanWithTodayShouldNotFind(): void
    {
        factory(BookingUser::class)->create(['created_at' => Carbon::now()->addDay()]);

        $actual = BookingUser::filterByTimespan('today')->first();

        $this->assertNull($actual);
    }

    public function testFilterByTimespanWithYesterdayShouldFind(): void
    {
        $expected = factory(BookingUser::class)->create(['created_at' => Carbon::now()->subDay()]);

        $actual = BookingUser::filterByTimespan('yesterday')->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testFilterByTimespanWithYesterdayShouldNotFind(): void
    {
        factory(BookingUser::class)->create(['created_at' => Carbon::now()]);

        $actual = BookingUser::filterByTimespan('yesterday')->first();

        $this->assertNull($actual);
    }

    public function testFilterByTimespanWithLast7DaysShouldFind(): void
    {
        $expected = factory(BookingUser::class)->create(['created_at' => Carbon::now()->subDays(3)]);

        $actual = BookingUser::filterByTimespan('last_7_days')->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testFilterByTimespanWithLast7DaysShouldNotFind(): void
    {
        factory(BookingUser::class)->create(['created_at' => Carbon::now()->subDays(7)]);

        $actual = BookingUser::filterByTimespan('last_7_days')->first();

        $this->assertNull($actual);
    }

    public function testPaymentsRelation(): void
    {
        $booking = factory(BookingUser::class)->create();
        $expected = factory(BookingPayment::class)->create(['booking_user_id' => $booking->id]);

        $result = $booking->payments()->first();

        $this->assertTrue($result->is($expected));
    }

    public function testScopeSearchByPhoneNumberWithEmpty(): void
    {
        $expected = factory(BookingUser::class)->create();

        $result = BookingUser::searchByPhoneNUmber('')->first();

        $this->assertTrue($result->is($expected));
    }

    public function testScopeSearcByContactNameWithEmpty(): void
    {
        $expected = factory(BookingUser::class)->create();

        $result = BookingUser::searchByContactName('')->first();

        $this->assertTrue($result->is($expected));
    }

    public function testScopeSearchByKostNameWithEmpty(): void
    {
        $expected = factory(BookingUser::class)->create();

        $result = BookingUser::searchByKostName('')->first();

        $this->assertTrue($result->is($expected));
    }

    public function testScopeSearchByKostName(): void
    {
        $kost = factory(Room::class)->create(['name' => 'Search by kost name']);
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $result = BookingUser::searchByKostName('Search by kost name')->first();

        $this->assertTrue($result->is($expected));
    }
    public function testFilterByTimespanWithoutEmptyColumnParamShouldFind(): void
    {
        factory(BookingUser::class)->create(['checkin_date' => Carbon::now()->subDays(7)]);

        $actual = BookingUser::filterByTimespan('last_7_days','checkin_date')->first();

        $this->assertNull($actual);
    }

    public function testFilterColumnByDateRangeShouldShowMatchedFrom(): void
    {
        $expected = factory(BookingUser::class)->create(['checkin_date' => Carbon::now()->startOfDay()]);
        $actual = BookingUser::filterColumnByDateRange('checkin_date', Carbon::now()->format('d-m-Y'), null)
            ->first();

        $this->assertTrue($expected->is($actual));
    }

    public function testFilterColumnByDateRangeShouldNotShowNonMatchedFrom(): void
    {
        $notExpected = factory(BookingUser::class)->create(['checkin_date' => Carbon::now()->subDay()->startOfDay()]);
        $actual = BookingUser::filterColumnByDateRange('checkin_date', Carbon::now()->format('d-m-Y'), null)
            ->first();

        $this->assertFalse($notExpected->is($actual));
    }

    public function testFilterColumnByDateRangeShouldShowMatchedTo(): void
    {
        $expected = factory(BookingUser::class)->create(['checkin_date' => Carbon::now()->startOfDay()->setTime(23, 59, 59)]);
        $actual = BookingUser::filterColumnByDateRange('checkin_date', null, Carbon::now()->addDay()->format('d-m-Y'))
            ->first();

        $this->assertTrue($expected->is($actual));
    }

    public function testFilterColumnByDateRangeShouldNotShowMatchedTo(): void
    {
        $notExpected = factory(BookingUser::class)->create(['checkin_date' => Carbon::now()->addDay()->startOfDay()]);
        $actual = BookingUser::filterColumnByDateRange('checkin_date', null, Carbon::now()->format('d-m-Y'))
            ->first();

        $this->assertFalse($notExpected->is($actual));
    }

    public function testGetTotalPaidBookingLastXDay()
    {
        $room = factory(Room::class)->create();

        factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_FINISHED,
            'created_at' => Carbon::now()->subDays(7),
        ]);

        $result = BookingUser::getTotalPaidBookingLastXDay(14, $room->song_id);
        $this->assertEquals(1, $result);
    }

    public function testGetTotalPaidBookingLastXDayReturnZero()
    {
        $room = factory(Room::class)->create();

        factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_FINISHED,
            'created_at' => Carbon::now()->subDays(20),
        ]);

        $result = BookingUser::getTotalPaidBookingLastXDay(14, $room->song_id);
        $this->assertEquals(0, $result);
    }

    public function testGetTotalPaidBookingLastXDayReturnOne()
    {
        $room = factory(Room::class)->create();

        factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'tenant_checkin_time' => Carbon::now()->subDays(4),
            'status' => BookingUser::BOOKING_STATUS_TERMINATED,
            'created_at' => Carbon::now()->subDays(4),
        ]);

        $result = BookingUser::getTotalPaidBookingLastXDay(14, $room->song_id);
        $this->assertEquals(1, $result);
    }

    public function testFilterByKostTypeWithAll()
    {
        $room = factory(Room::class)->create();
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('all')->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testFilterByKostTypeWithNull()
    {
        $room = factory(Room::class)->create(['is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType(null)->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testFilterByKostTypeWithNonTestingMamirooms()
    {
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => false])->song_id
        ]);
        factory(BookingUser::class)->create(['designer_id' => factory(Room::class)->create([
            'is_mamirooms' => true, 'is_testing' => true])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('mamirooms_non_testing')->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testFilterByKostTypeWithNonTestingNonMamirooms()
    {
        factory(BookingUser::class)->create(['designer_id' => factory(Room::class)->create([
            'is_mamirooms' => true, 'is_testing' => false])->song_id
        ]);
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => true])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('non_mamirooms_non_testing')->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testFilterByKostTypeWithTestingMamirooms()
    {
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => true])->song_id
        ]);
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => false])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => true]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('mamirooms_testing')->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testFilterByKostTypeWithTestingNonMamirooms()
    {
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => true])->song_id
        ]);
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => false])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => true]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('non_mamirooms_testing')->get();

        $this->assertCount(1, $actual);
        $this->assertTrue($actual->first()->is($expected));
    }

    public function testFilterByKostTypeWithAllNonTesting()
    {
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => false])->song_id
        ]);
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => false])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => true]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('all_non_testing')->get();

        $this->assertCount(2, $actual);
    }

    public function testFilterByKostTypeWithAllTesting()
    {
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => true, 'is_testing' => true])->song_id
        ]);
        factory(BookingUser::class)->create([
            'designer_id' => factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => true])->song_id
        ]);
        $room = factory(Room::class)->create(['is_mamirooms' => false, 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $actual = BookingUser::filterByKostType('all_testing')->get();

        $this->assertCount(2, $actual);
    }

    public function testScopeSearchByAreaCityWithEmpty()
    {
        $kost = factory(Room::class)->create();
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $this->assertTrue(
            BookingUser::searchByAreaCity([])->first()->is($expected)
        );
    }

    public function testScopeSearchByOwnerPhoneWithEmpty()
    {
        $kost = factory(Room::class)->create();
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $this->assertTrue(
            BookingUser::searchByOwnerPhone('')->first()->is($expected)
        );
    }

    public function testScopeFilterByStatusWithEmpty()
    {
        $expected = factory(BookingUser::class)->create();

        $this->assertTrue(
            BookingUser::filterByStatus('')->first()->is($expected)
        );
    }

    public function testScopeFilterByStatusChangedByWithEmpty()
    {
        $expected = factory(BookingUser::class)->create();

        $this->assertTrue(
            BookingUser::filterByStatusChangedBy('', null)->first()->is($expected)
        );
    }

    public function testScopeFilterByMultipleStatusWithCancelled()
    {
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_CANCELLED]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED]);

        $this->assertCount(2, BookingUser::filterByMultipleStatus(BookingUser::BOOKING_STATUS_CANCELLED)->get());
    }

    public function testScopeFilterByMultipleStatusWithExpired()
    {
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_EXPIRED]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_EXPIRED_DATE]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED]);

        $this->assertCount(3, BookingUser::filterByMultipleStatus(BookingUser::BOOKING_STATUS_EXPIRED)->get());
    }

    public function testScopeFilterByMultipleStatusWithDefault()
    {
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER]);
        factory(BookingUser::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED]);

        $this->assertCount(1, BookingUser::filterByMultipleStatus(BookingUser::BOOKING_STATUS_BOOKED)->get());
    }
}
