<?php

namespace App\Entities\Booking;

use App\Test\MamiKosTestCase;

class BookingDiscountTest extends MamiKosTestCase
{
    
    const REAL_PRICE = 1500000;

    public function testGetOriginalPrice()
    {
        $discount = factory(BookingDiscount::class)->create(
            [
                'price' => self::REAL_PRICE
            ]
        );

        $this->assertEquals(self::REAL_PRICE, $discount->getOriginalPrice());
    }
}
