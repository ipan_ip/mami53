<?php

namespace app\Entities\Booking;

use App\Entities\Booking\BookingPayment;
use App\Entities\Booking\BookingUser;
use App\Entities\Premium\Bank;
use App\Test\MamiKosTestCase;

class BookingPaymentTest extends MamiKosTestCase
{
    public function testRelationBookingPaymentExistSuccess(): void
    {
        // create data booking user
        $bookingUser = factory(BookingUser::class)->create();

        // create data bank
        $premiumBank = factory(Bank::class)->create();

        // create data Booking Payment
        $BookingPayment = factory(BookingPayment::class)->create([
            'booking_user_id' => $bookingUser->id,
            'bank_account_id' => $premiumBank->id,
        ]);

        // check relationship booking payment belongs to booking user and premium bank
        $this->assertSame($bookingUser->id, $BookingPayment->booking_user_id);
        $this->assertSame($premiumBank->id, $BookingPayment->bank_account_id);
        $this->assertInstanceOf(BookingUser::class, $BookingPayment->booking_user);
        $this->assertInstanceOf(Bank::class, $BookingPayment->bank);
    }

    public function testRelationBookingPaymentNotExistSuccess() : void
    {
        // create data booking guest
        $BookingPayment = factory(BookingPayment::class)->create();

        // check relation between booking payment not belongs to booking user and premium bank
        $this->assertFalse($BookingPayment->booking_user()->exists());
        $this->assertFalse($BookingPayment->bank()->exists());
    }
}
