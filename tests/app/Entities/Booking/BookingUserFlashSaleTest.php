<?php

namespace App\Entities\Booking;

use App\Entities\FlashSale\FlashSale;
use App\Test\MamiKosTestCase;

class BookingUserFlashSaleTest extends MamiKosTestCase
{
    
    public function testEntityRelation()
    {
        // create data
        [
            $bookingUser,
            $bookingUserFlashSale,
        ] = $this->createEntitiesSet();

        // getting relationship date from booking user
        $bookingUserFlashSaleRetrive = $bookingUser->booking_user_flash_sale()->first();

        // run test
        $this->assertSame($bookingUserFlashSale->booking_user_id, $bookingUserFlashSaleRetrive->booking_user_id, 'Same Booking User ID');
        $this->assertSame($bookingUserFlashSale->flash_sale_id, $bookingUserFlashSaleRetrive->flash_sale_id, 'Same Flash Sale ID');
    }

    private function createBookingUserEntity(array $overrideData = []): BookingUser
    {
        return factory(BookingUser::class)->create($overrideData);
    }

    private function createBookingUserFlashSaleEntity(array $overrideData = []): BookingUserFlashSale
    {
        return factory(BookingUserFlashSale::class)->create($overrideData);
    }

    private function createFlashSaleEntity(array $overrideData = []): FlashSale
    {
        return factory(FlashSale::class)->create($overrideData);
    }

    private function createEntitiesSet(): array
    {
        // main entity
        $bookingUserEntity = $this->createBookingUserEntity();

        $flashSaleEntity  = $this->createFlashSaleEntity();

        $bookingUserFlashSaleEntity = $this->createBookingUserFlashSaleEntity([
            'booking_user_id' => $bookingUserEntity->id,
            'flash_sale_id' => $flashSaleEntity->id
        ]);

        return [
            $bookingUserEntity,
            $bookingUserFlashSaleEntity
        ];
    }
}