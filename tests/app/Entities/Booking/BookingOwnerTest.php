<?php

namespace Test\App\Entities\Booking;

use App\Entities\Booking\BookingOwner;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;

class BookingOwnerTest extends MamiKosTestCase
{
    use WithoutEvents;

    public function testIsBookingAllRoomWithNoVerifiedRoomOwner(): void
    {
        $user = $this->mock(User::class);
        $user->shouldReceive('getAttribute')
            ->with('room_owner_verified')
            ->andReturn(null);

        $this->assertFalse(BookingOwner::isBookingAllRoom($user));
    }

    public function testInsertOrUpdate(): void
    {
        $user = factory(User::class)->create();

        $bookingOwner = BookingOwner::insertOrUpdate($user);

        $this->assertEquals($user->id, $bookingOwner->owner_id);
        $this->assertEquals(1, $bookingOwner->is_active);
    }

    public function testIsBookingAllRoomFalse(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = BookingOwner::isBookingAllRoom($userEntity);
        $this->assertFalse($data);
    }

    public function testIsBookingAllRoomTrue(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create([
            'is_booking' => true
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);
        // run test
        $data = BookingOwner::isBookingAllRoom($userEntity);
        $this->assertTrue($data);
    }
}