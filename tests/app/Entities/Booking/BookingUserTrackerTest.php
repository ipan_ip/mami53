<?php

namespace App\Entities\Booking;

use App\Test\MamiKosTestCase;

class BookingUserTrackerTest extends MamiKosTestCase
{   
    public function testEntityIsExists()
    {
        $bookingUserTracker = factory(BookingUserTracker::class)->create();

        $this->assertNotNull($bookingUserTracker);
    }
}
