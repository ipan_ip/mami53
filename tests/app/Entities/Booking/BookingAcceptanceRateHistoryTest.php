<?php

namespace App\Entities\Booking;

use App\Test\MamiKosTestCase;

class BookingAcceptanceRateHistoryTest extends MamiKosTestCase
{   
    public function testEntityIsExists()
    {
        $bookingAcceptanceRate = factory(BookingAcceptanceRate::class)->create();

        $this->assertNotNull($bookingAcceptanceRate);
    }

    public function testEntityRelation()
    {
        $bookingAcceptanceRate        = factory(BookingAcceptanceRate::class)->create();
        $bookingAcceptanceRateHistory = factory(BookingAcceptanceRateHistory::class)->create([
            'booking_acceptance_rate_id' => $bookingAcceptanceRate->id,
        ]);

        $this->assertSame($bookingAcceptanceRate->id, $bookingAcceptanceRateHistory->booking_acceptance_rate_id);
    }
}
