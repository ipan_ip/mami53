<?php
namespace App\Entities\Chat;

use App\Entities\Activity\Question;
use App\Test\MamiKosTestCase;
use Illuminate\Http\Request;

class ChatConditionTest extends MamiKosTestCase
{

    public function testQuestionRelation()
    {
        $question = factory(Question::class)->create();
        $condition = factory(ChatCondition::class)->create(
            [
                'chat_question_id' => $question->id
            ]
        );

        $response = $condition->question;
        $this->assertInstanceOf(Question::class, $response);
    }

    public function testGetCriteriaIdsAttribute()
    {
        $condition = factory(ChatCondition::class)->create(
            [
                'chat_criteria_ids' => '2,4'
            ]
        );

        $response = $condition->getCriteriaIdsAttribute();

        $this->assertIsArray($response);
        $this->assertEquals(2, $response[0]);
        $this->assertEquals(4, $response[1]);
    }

    public function testGetViewCriteriaNameAttribute()
    {
        factory(Criteria::class)->create(
            [
                'id' => 1,
                'name' => 'gp1',
                'attribute' => 'is_gp1',
                'description' => 'Kos is Mamikos Goldplus 1',
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 3,
                'name' => 'Mamirooms',
                'attribute' => 'is_mamirooms',
                'description' => 'Kos is a Mamiroom'
            ]
        );

        $condition = factory(ChatCondition::class)->create(
            [
                'chat_criteria_ids' => '1,3'
            ]
        );

        $response = $condition->getViewCriteriaNameAttribute();

        $this->assertEquals('gp1, Mamirooms', $response);
    }

    public function testUpdateChatCondition()
    {
        $condition = factory(ChatCondition::class)->create();
        $request = new Request();
        $request->reply = 'Success update!';
        $request->criteria_ids = [1,2];

        ChatCondition::updateChatCondition($request, $condition);

        $this->assertDatabaseHas(
            'chat_condition',
            [
                'reply' => 'Success update!',
                'chat_criteria_ids' => '1,2'
            ]
        );
    }

    public function testCreateChatCondition()
    {
        $question = factory(Question::class)->create();

        $request = new Request();
        $request->reply = 'Condition Created!';
        $request->chat_question_id = $question->id;
        $request->criteria_ids = [1,3];

        ChatCondition::createChatCondition($request, $question);

        $this->assertDatabaseHas(
            'chat_condition',
            [
                'reply' => 'Condition Created!',
                'chat_criteria_ids' => '1,3',
                'chat_question_id' => $question->id,
                'order' => 1
            ]
        );
    }

    public function testSpecifyTheOrderWithNullExistingCondition()
    {
        $question = factory(Question::class)->create();
        $this->assertEquals(1, ChatCondition::specifyTheOrder($question));
    }

    public function testSpecifyTheOrderWithConditionExist()
    {
        $question = factory(Question::class)->create();

        factory(ChatCondition::class)->create(
            [
                'chat_question_id' => $question->id,
                'order' => 1
            ]
        );

        $this->assertEquals(2, ChatCondition::specifyTheOrder($question));
    }

    public function testSaveSortOrderWithValidIdShouldReturnTrue()
    {
        $condition1 = factory(ChatCondition::class)->create([
            'id' => 1,
            'order' => 1
        ]);

        $condition2 = factory(ChatCondition::class)->create([
            'id' => 2,
            'order' => 2
        ]);

        $this->assertDatabaseHas('chat_condition', $condition1->toArray());
        $this->assertDatabaseHas('chat_condition', $condition2->toArray());

        $orderData = [];
        $orderData[] = [
            'id' => $condition1->id,
            'order' => '2'
        ];
        $orderData[] = [
            'id' => $condition2->id,
            'order' => '1'
        ];

        $IsSuccess = ChatCondition::saveSortOrder($orderData);

        $this->assertDatabaseHas('chat_condition', $orderData[0]);
        $this->assertDatabaseHas('chat_condition', $orderData[1]);
        $this->assertTrue($IsSuccess);
    }

    public function testSaveSortOrderWithInvalidIdShouldReturnFalse()
    {
        $condition1 = factory(ChatCondition::class)->create([
            'id' => 1,
            'order' => 1
        ]);

        $condition2 = factory(ChatCondition::class)->create([
            'id' => 2,
            'order' => 2
        ]);

        $this->assertDatabaseHas('chat_condition', $condition1->toArray());
        $this->assertDatabaseHas('chat_condition', $condition2->toArray());

        $orderData = [];
        $orderData[] = [
            'id' => 3,
            'order' => '2'
        ];
        $orderData[] = [
            'id' => 4,
            'order' => '1'
        ];

        $isSuccess = ChatCondition::saveSortOrder($orderData);

        $this->assertFalse($isSuccess);
    }

    public function testRemoveChatCondition()
    {
        $question = factory(Question::class)->create();

        $condition = factory(ChatCondition::class)->create([
            'id' => 2,
            'chat_question_id' => $question->id,
            'order' => 2
        ]);

        factory(ChatCondition::class)->create([
            'id' => 3,
            'chat_question_id' => $question->id,
            'order' => 3
        ]);

        ChatCondition::removeChatCondition($condition);

        $this->assertNotNull($condition->deleted_at);

        $this->assertDatabaseHas('chat_condition',
            [
                'id' => 3,
                'order' => 2
            ]
        );
    }

    public function testFixTheOrderAfterRemove()
    {
        $question = factory(Question::class)->create();

        factory(ChatCondition::class)->create([
            'id' => 2,
            'chat_question_id' => $question->id,
            'order' => 2
        ]);

        factory(ChatCondition::class)->create([
            'id' => 3,
            'chat_question_id' => $question->id,
            'order' => 3
        ]);

        ChatCondition::fixTheOrderAfterRemove($question, 1);

        $this->assertDatabaseMissing('chat_condition', ['order' => 3]);
        $this->assertDatabaseHas(
            'chat_condition',
            [
                'id' => 2,
                'order' => 1
            ]
        );
        $this->assertDatabaseHas(
            'chat_condition',
            [
                'id' => 3,
                'order' => 2
            ]
        );
    }
}