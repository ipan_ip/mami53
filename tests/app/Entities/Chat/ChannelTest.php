<?php
namespace App\Entities\Chat;

use App\Test\MamiKosTestCase;

class ChannelTest extends MamiKosTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->channel = factory(Channel::class)->make();
    }
}