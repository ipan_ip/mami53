<?php
namespace Test\App\Entities\Classes;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\Payment;
use App\Entities\Classes\MidtransFinish;

class MidtransFinishTest extends MamiKosTestCase
{
    private $user;
    private $payment;
    private $premiumRequest;
    private $midtransData;

    public function setUp() : void
    {
        parent::setUp();
        $this->user = factory(\App\User::class)->create();
        
        $this->premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 2,
            "view" => 1000,
            "used" => 0,
            "allocated" => 0
        ]);

        $this->payment = factory(Payment::class)->create([
            "id" => 1,
            "user_id" => $this->user->id,
            "order_id" => 123,
            "transaction_status" => "pending",
            "premium_request_id" => $this->premiumRequest->id
        ]);

        $this->midtransData = [
            "user_id" => $this->user->id,
            "order_id" => $this->payment->order_id,
            "transaction_id" => "kjdkaj8981921jkq",
            "payment_type" => "bank_transfer",
            "transaction_status" => "pending",
            "payment_code" => "jdsj22",
            "pdf_url" => "",
            "biller_code" => "73287232",
            "bill_key" => "273827382783",
            "gross_amount" => 10000
        ];

    }

    public function testMidtransFinishFromMobileAppSuccess()
    {
        $result = (new MidtransFinish($this->midtransData, $this->user, true))->finish();
        
        unset($this->midtransData['gross_amount']);
        $this->midtransData['total'] = 10000;

        $resultIOS = (new MidtransFinish($this->midtransData, $this->user, true))->finish();
        $this->assertTrue($result instanceOf Payment);
        $this->assertTrue($resultIOS instanceOf $resultIOS);
    }

    public function testMidtransFinishFromWebSuccess()
    {
        $result = (new MidtransFinish((object) $this->midtransData, $this->user, false))->finish();
        $this->assertTrue($result instanceOf Payment);
    }
}