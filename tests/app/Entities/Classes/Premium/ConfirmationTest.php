<?php
namespace Test\App\Entities\Classes\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\AccountConfirmation;
use App\User;
use App\Entities\Classes\Premium\Confirmation;
use App\Entities\Premium\BalanceRequest;
use Notification;

class ConfirmationTest extends MamiKosTestCase
{
    private $paymentData;
    private $premiumPackage;
    private $premiumRequest;
    private $user;
    private $paymentConfirmation;
    private $payment;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            "id" => 1,
            "date_owner_limit" => null
        ]);

        $this->premiumPackage = factory(PremiumPackage::class)->create([
            "id" => 1,
            "for" => "package",
            "name" => "Paket test",
            "view" => 10000,
            "total_day" => 30
        ]);

        $this->premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 1,
            "premium_package_id" => $this->premiumPackage->id,
            "view" => $this->premiumPackage->view,
            "used" => 0,
            "status" => "0",
            "allocated" => 0,
            "user_id" => $this->user->id,
        ]);

        $this->paymentConfirmation = factory(AccountConfirmation::class)->create([
            "id" => 1,
            "premium_request_id" => $this->premiumRequest->id,
            "user_id" => $this->user->id,
            "is_confirm" => "0"
        ]);

        $this->payment = factory(Payment::class)->create([
            "id" => 1,
            "user_id" => $this->user->id,
            "premium_request_id" => $this->premiumRequest->id,
            "order_id" => 12345,
            "payment_type" => "bank_transfer",
            "transaction_status" => Payment::MIDTRANS_STATUS_PENDING,
            "total" => 10000,
            "bill_key" => "HSKDHAB",
            "biller_code" => "POSIDDDGGDG"
        ]);

        $this->paymentData = [
            "order_id" => $this->payment->order_id,
            "payment_type" => $this->payment->payment_type,
            "selected_payment_type" => $this->payment->payment_type,
            "transaction_status" => $this->payment->transaction_status,
            "fraud" => $this->payment->fraud,
            "gross_amount" => $this->payment->total,
            "bill_key" => $this->payment->bill_key,
            "biller_code" => $this->payment->biller_code
        ];
    }

    public function testPaymentSettlementSuccess()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $settlement = (new Confirmation($this->paymentData))->settlement();
        $this->assertTrue($settlement);
    }

    public function testPaymentSettlementFail()
    {
        $settlement = (new Confirmation(['order_id' => 'this is just another dummy']))->settlement();
        $this->assertNull($settlement);
    }

    public function testPaymentBalanceSettlementSuccess()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        Notification::shouldReceive('send');
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "expired_date" => null,
            "view" => 1000
        ]);
        $balanceRequest = factory(BalanceRequest::class)->create([
            "status" => "0",
            "premium_request_id" => $premiumRequest->id,
            "view" => 50
        ]);
        
        $payment = factory(Payment::class)->create([
            "premium_request_id" => 0,
            "user_id" => $user->id,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id,
            "order_id" => "5fbf449ad0f49",
            "transaction_status" => Payment::MIDTRANS_STATUS_PENDING
        ]);

        factory(AccountConfirmation::class)->create([
            "premium_request_id" => 0,
            "view_balance_request_id" => $balanceRequest->id,
            "user_id" => $user->id,
            "is_confirm" => "0"
        ]);

        $paymentData = [
            "order_id" => $payment->order_id,
            "payment_type" => $payment->payment_type,
            "selected_payment_type" => $payment->payment_type,
            "transaction_status" => $payment->transaction_status,
            "fraud" => $payment->fraud,
            "gross_amount" => $payment->total,
            "bill_key" => $payment->bill_key,
            "biller_code" => $payment->biller_code
        ];

        $settlement = (new Confirmation($paymentData))->settlement();
        $premium = PremiumRequest::lastActiveRequest($user);
        $this->assertTrue($settlement);
        $this->assertEquals(1050, $premium->view);
    }

    public function testPaymentBalanceSettlementFail()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "expired_date" => null,
            "view" => 1000
        ]);
        $balanceRequest = factory(BalanceRequest::class)->create([
            "status" => "0",
            "premium_request_id" => $premiumRequest->id,
            "view" => 50
        ]);
        
        $payment = factory(Payment::class)->create([
            "premium_request_id" => 0,
            "user_id" => $user->id,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id,
            "order_id" => "5fbf449ad0f49",
            "transaction_status" => Payment::MIDTRANS_STATUS_PENDING
        ]);

        $paymentData = [
            "order_id" => $payment->order_id,
            "payment_type" => $payment->payment_type,
            "selected_payment_type" => $payment->payment_type,
            "transaction_status" => $payment->transaction_status,
            "fraud" => $payment->fraud,
            "gross_amount" => $payment->total,
            "bill_key" => $payment->bill_key,
            "biller_code" => $payment->biller_code
        ];

        $settlement = (new Confirmation($paymentData))->settlement();
        $this->assertFalse($settlement);
    }

    public function testPaymentBalanceSettlementFailUpdateSaldo()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        Notification::shouldReceive('send');
        $user = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "expired_date" => null,
            "view" => 2000
        ]);

        $balanceRequest = factory(BalanceRequest::class)->create([
            "status" => "0",
            "premium_request_id" => $premiumRequest->id,
            "view" => 50
        ]);
        
        $payment = factory(Payment::class)->create([
            "premium_request_id" => 0,
            "user_id" => $user->id,
            "source" => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            "source_id" => $balanceRequest->id,
            "order_id" => "5fbf449ad0f49",
            "transaction_status" => Payment::MIDTRANS_STATUS_SETTLEMENT,
            "payment_type" => "gopay"
        ]);

        $paymentData = [
            "order_id" => $payment->order_id,
            "payment_type" => $payment->payment_type,
            "selected_payment_type" => $payment->payment_type,
            "transaction_status" => $payment->transaction_status,
            "fraud" => $payment->fraud,
            "gross_amount" => $payment->total,
            "bill_key" => $payment->bill_key,
            "biller_code" => $payment->biller_code
        ];

        $settlement = (new Confirmation($paymentData))->settlement();
        $premium = PremiumRequest::lastActiveRequest($user);
        $this->assertNull($settlement);
    }

    public function testPaymentDenySuccess()
    {
        $deny = (new Confirmation($this->paymentData))->deny();
        $this->assertTrue($deny);
    }

    public function testPaymentPendingFailed()
    {
        $data = $this->paymentData;
        $data['order_id'] = 17832738273827;
        $pendingFailed = (new Confirmation($data))->pending();
        $this->assertFalse($pendingFailed);
    }

    public function testPaymentPendingSuccess()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        
        $pendingSuccess = (new Confirmation($this->paymentData))->pending();
        $this->assertTrue($pendingSuccess);
    }

    public function testPaymentConfirmationSuccess()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        
        $payment = (new Confirmation($this->paymentData))->paymentConfirmation($this->payment);
        $this->assertTrue($payment);
    }

    public function testPremiumConfirmationSuccess()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        
        $premiumConfirmation = (new Confirmation($this->paymentData))->premiumConfirmation($this->premiumRequest);
        $this->assertTrue($premiumConfirmation instanceof AccountConfirmation);
    }

    public function testPremiumConfirmationSettlement()
    {
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        
        $premiumRequest = factory(PremiumRequest::class)->create([
                "id" => 2,
                "premium_package_id" => $this->premiumPackage->id,
                "view" => $this->premiumPackage->view,
                "used" => 0,
                "status" => "0",
                "allocated" => 0,
                "user_id" => $this->user->id,
        ]);

        $data = $this->paymentData;
        $data['transaction_status'] = 'capture';
        $data['premium_request_id'] = $premiumRequest->id;
        $premiumConfirmation = (new Confirmation($data))->premiumConfirmation($premiumRequest);
        $this->assertTrue($premiumConfirmation instanceof AccountConfirmation);
    }

    public function testUpdateExpiredPremiumUserSuccess()
    {
        $expiredDate = date('Y-m-d', strtotime(date("Y-m-d") . "+" .$this->premiumPackage->total_day. " days")); 
        $user = (new Confirmation($this->paymentData))->userDateUpdate($this->user, $this->premiumPackage);
        $this->assertEquals($expiredDate, $user->date_owner_limit);
    }

    public function testUpdateExpiredPremiumUserFromPremiumExpired()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('+1 day'))
        ]);

        $expiredDate = date('Y-m-d', strtotime($user->date_owner_limit . "+" .$this->premiumPackage->total_day. " days")); 

        $userUpdated = (new Confirmation($this->paymentData))->userDateUpdate($user, $this->premiumPackage);

        $this->assertEquals($expiredDate, $userUpdated->date_owner_limit);
    }

    public function testUpdatePremiumRequestSuccess()
    {
        $premiumRequest = (new Confirmation($this->paymentData))->updatePremiumPackage($this->premiumRequest, null);
        $this->assertEquals("1", $premiumRequest->status);
    }

    public function testUpdatePremiumRequestFromLatestRequestSuccess()
    {
        $lastPremiumRequest = factory(PremiumRequest::class)->create();
        $premiumRequest = (new Confirmation($this->paymentData))->updatePremiumPackage($this->premiumRequest, $lastPremiumRequest);
        $this->assertEquals("1", $premiumRequest->status);
    }

    public function testPaymentConfirmationFail()
    {
        $confirm = (new Confirmation([]))->paymentConfirmation(null);

        $this->assertFalse($confirm);
    }

    public function testPaymentBalanceConfirmationFail()
    {
        $confirm = (new Confirmation([]))->paymentBalanceConfirmation(null);

        $this->assertFalse($confirm);
    }

    public function testPaymentConfirmationThatDoesNotConfirmed()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "premium_package_id" => $this->premiumPackage->id,
            "view" => $this->premiumPackage->view,
            "used" => 0,
            "status" => "0",
            "allocated" => 0,
            "user_id" => $this->user->id,
        ]);

        $payment = factory(Payment::class)->create([
            "user_id" => $this->user->id,
            "premium_request_id" => $premiumRequest->id,
            "order_id" => 12345,
            "payment_type" => "gopay",
            "transaction_status" => Payment::MIDTRANS_STATUS_SETTLEMENT,
            "total" => 10000,
            "bill_key" => "HSKDHAB",
            "biller_code" => "POSIDDDGGDG"
        ]);

        $paymentData = [
            "order_id" => $this->payment->order_id,
            "payment_type" => $this->payment->payment_type,
            "selected_payment_type" => $this->payment->payment_type,
            "transaction_status" => $this->payment->transaction_status,
            "fraud" => $this->payment->fraud,
            "gross_amount" => $this->payment->total,
            "bill_key" => $this->payment->bill_key,
            "biller_code" => $this->payment->biller_code
        ];

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $confirm = (new Confirmation($paymentData))->paymentConfirmation($payment);

        $this->assertFalse($confirm);

    }

    public function testPaymentConfirmationHasLastPremiumRequest()
    {
        factory(PremiumRequest::class)->create([
            "user_id" => $this->user->id,
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS
        ]);
        
        $premiumRequest = factory(PremiumRequest::class)->create([
            "premium_package_id" => $this->premiumPackage->id,
            "view" => $this->premiumPackage->view,
            "used" => 0,
            "status" => "0",
            "allocated" => 0,
            "user_id" => $this->user->id,
        ]);

        $payment = factory(Payment::class)->create([
            "user_id" => $this->user->id,
            "premium_request_id" => $premiumRequest->id,
            "order_id" => 12345,
            "payment_type" => "gopay",
            "transaction_status" => Payment::MIDTRANS_STATUS_SETTLEMENT,
            "total" => 10000,
            "bill_key" => "HSKDHAB",
            "biller_code" => "POSIDDDGGDG"
        ]);

        $paymentData = [
            "order_id" => $payment->order_id,
            "payment_type" => $payment->payment_type,
            "selected_payment_type" => $payment->payment_type,
            "transaction_status" => $payment->transaction_status,
            "fraud" => $payment->fraud,
            "gross_amount" => $payment->total,
            "bill_key" => $payment->bill_key,
            "biller_code" => $payment->biller_code
        ];

        factory(AccountConfirmation::class)->create([
            "premium_request_id" => $premiumRequest->id,
            "user_id" => $this->user->id,
            "is_confirm" => "0"
        ]);

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $confirm = (new Confirmation($paymentData))->paymentConfirmation($payment);

        $this->assertTrue($confirm);
    }
}