<?php

namespace App\Entities\Classes\Premium;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;

class RequestProcessTest extends MamiKosTestCase
{

    private $user;
    private $premiumPackage;
    private $balancePackage;
    private $premiumRequest;
    private $balanceRequest;
    private $premiumRequestData;
    private $balanceRequestData;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime("+ 2 day"))
        ]);

        $this->premiumPackage = factory(PremiumPackage::class)->create([
            "for" => PremiumPackage::PACKAGE_TYPE,
            "sale_price" => 0
        ]);

        $this->balancePackage = factory(PremiumPackage::class)->create([
            "for" => PremiumPackage::BALANCE_TYPE,
            "sale_price" => 10
        ]);

        $this->premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $this->user->id,
            "status" => 0,
            "premium_package_id" => $this->premiumPackage->id,
            "expired_status" => "false"
        ]);

        $this->premiumRequestData = [
            "package_id" => $this->premiumPackage->id,
            "request_type" => PremiumPackage::PACKAGE_TYPE,
            "price" => 5543
        ];

        $this->balanceRequestData = [
            "package_id" => $this->balancePackage->id,
            "request_type" => PremiumPackage::BALANCE_TYPE,
            "price" => 40543
        ];
    }

    public function testGetPackage()
    {
        $package = (new RequestProcess($this->user, $this->premiumRequestData))->get_package();

        $this->assertEquals($package->id, $this->premiumPackage->id);
    }

    public function testOwnerCheckerRequest()
    {
        $request = (new RequestProcess($this->user, $this->premiumRequestData))->owner_checker_request();

        $this->assertEquals($request->id, $this->premiumRequest->id);
    }

    public function testRequestPremiumRequestShouldSuccess()
    {
        $user = factory(User::class)->create();
        
        factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => 1,
            "premium_package_id" => $this->premiumPackage->id
        ]);

        $data = [
            "package_id" => $this->premiumPackage->id,
            "request_type" => PremiumPackage::PACKAGE_TYPE,
            "price" => 9000
        ];
            
        $request = (new RequestProcess($this->user, $this->premiumRequestData, $this->premiumPackage, 'midtrans'))->perform();
        $request2 = (new RequestProcess($user, $data, $this->premiumPackage, 'midtrans'))->perform();

        $this->assertTrue($request['status']);
        $this->assertTrue($request2['status']);
    }

    public function testRequestPremiumRequestFail()
    {
        $user = factory(User::class)->create();
        
        factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => 0,
            "premium_package_id" => $this->premiumPackage->id,
            "expired_status" => null
        ]);

        $data = [
            "package_id" => $this->premiumPackage->id,
            "request_type" => PremiumPackage::PACKAGE_TYPE,
            "price" => 9000
        ];
            
        $request = (new RequestProcess($user, $data, $this->premiumPackage, 'midtrans'))->perform();

        $this->assertFalse($request['status']);
    }

}