<?php

namespace Test\App\Entities\Classes;

use App\Entities\Classes\UpdateRoom;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class UpdateRoomTest extends MamiKosTestCase
{
    public function testMinPayment()
    {
        $updateRoom = new UpdateRoom(null, null);

        $expectedResult = [65, 67, 68, 69, 66, 64, 70, 71, 72, 73, 74];
        $result = $updateRoom->min_payment();
        $this->assertEquals($expectedResult, $result);
    }

    public function testRoomUpdate()
    {
        $room = factory(Room::class)->create();
        $data = factory(Room::class)->make()->toArray();

        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->room_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer', $data);
    }

    public function testTagsUpdateInsideBathroomWithExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 1
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 4
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'inside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateInsideBathroomWithoutExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 1
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 4
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'inside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateOutsideBathroomWithExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 4
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 1
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'outside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateOutsideBathroomWithoutExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 4
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 1
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'outside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateMinMonth()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 65
        ]);

        $id = 66 ;
        $tags = factory(Tag::class, 5)->make([
            'id' => 4
        ])->each(function ($tag) use (&$id, $room) {
            $tag->id = $id++;
            $tag->name = $tag->name;
            $tag->save();

            factory(RoomTag::class)->create([
                'designer_id' => $room->id,
                'tag_id' => $tag->id,
            ]);
        })->toArray();

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);
        foreach ($tags as $tag) {
            $this->assertDatabaseHas('designer_tag', [
                'designer_id' => $room->id,
                'tag_id' => $tag['id'],
            ]);
        }

        $data = [
            'min_month' => $newTag->id
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        foreach ($tags as $tag) {
            $this->assertDatabaseMissing('designer_tag', [
                'designer_id' => $room->id,
                'tag_id' => $tag['id'],
            ]);
        }
    }

    public function testTagsUpdateWithListrikWithExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 84
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 144
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'with_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateWithListrikWithoutExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 84
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 144
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'with_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateWithoutListrikWithExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 144
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 84
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'without_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testTagsUpdateWithoutListrikWithoutExistingRoomTag()
    {
        $room = factory(Room::class)->create();
        $newTag = factory(Tag::class)->create([
            'id' => 144
        ]);
        $oldTag = factory(Tag::class)->create([
            'id' => 84
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);

        $data = [
            'without_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, []);

        $result = $updateRoom->tags_update($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $newTag->id,
        ]);

        $this->assertDatabaseMissing('designer_tag', [
            'designer_id' => $room->id,
            'tag_id' => $oldTag->id,
        ]);
    }

    public function testCheckingDataSimpleInsideBathroom()
    {
        $room = factory(Room::class)->create();
        $data = [
            'inside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->checking_data_simple();
        $this->assertTrue($result['inside_bathroom']);
        $this->assertFalse($result['outside_bathroom']);
        $this->assertEmpty($result['min_month']);
        $this->assertFalse($result['with_listrik']);
        $this->assertFalse($result['without_listrik']);
    }

    public function testCheckingDataSimpleOutsideBathroom()
    {
        $room = factory(Room::class)->create();
        $data = [
            'outside_bathroom' => true
        ];
        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->checking_data_simple();
        $this->assertTrue($result['outside_bathroom']);
        $this->assertFalse($result['inside_bathroom']);
        $this->assertEmpty($result['min_month']);
        $this->assertFalse($result['with_listrik']);
        $this->assertFalse($result['without_listrik']);
    }

    public function testCheckingDataSimpleWithListrik()
    {
        $room = factory(Room::class)->create();
        $data = [
            'with_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->checking_data_simple();
        $this->assertTrue($result['with_listrik']);
        $this->assertFalse($result['outside_bathroom']);
        $this->assertFalse($result['inside_bathroom']);
        $this->assertEmpty($result['min_month']);
        $this->assertFalse($result['without_listrik']);
    }

    public function testCheckingDataSimpleWithoutListrik()
    {
        $room = factory(Room::class)->create();
        $data = [
            'without_listrik' => true
        ];
        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->checking_data_simple();
        $this->assertFalse($result['with_listrik']);
        $this->assertFalse($result['outside_bathroom']);
        $this->assertFalse($result['inside_bathroom']);
        $this->assertEmpty($result['min_month']);
        $this->assertTrue($result['without_listrik']);
    }

    public function testCheckingDataSimpleMinMonth()
    {
        $room = factory(Room::class)->create();
        $data = [
            'min_month' => 65
        ];
        $updateRoom = new UpdateRoom($room, $data);
        $result = $updateRoom->checking_data_simple();
        $this->assertFalse($result['with_listrik']);
        $this->assertFalse($result['outside_bathroom']);
        $this->assertFalse($result['inside_bathroom']);
        $this->assertEquals($data['min_month'], $result['min_month']);
        $this->assertFalse($result['without_listrik']);
    }
}
