<?php
namespace Test\App\Entities\Classes;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Classes\MidtransToken;

class MidtransTokenTest extends MamiKosTestCase
{
    private $mockForAppDevice;
    private $postParams;
    private $user;
    private $package;

    const EMAIL = 'myemail@mail.com';
    const PHONE_NUMBER = '087812345';
    const DEFAULT_CUSTOMER_NAME = 'Ahmad Sururi';

    public function setUp() : void
    {
        parent::setUp();
        
        $this->user = factory(\App\User::class)->create([
            'email' => self::EMAIL,
            'phone_number' => self::PHONE_NUMBER
        ]);

        $this->package = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::PACKAGE_TYPE,
            'name' => 'package name',
            'price' => 10000,
            'sale_price' => 10000,
            'special_price' => 0,
            'start_date' => date('Y-m-d'),
            'sale_limit_date' => date('Y-m-d H:i:s'),
            'total_day' => 30,
            'view' => 10000,
            'is_active' => "1"
        ]);

        $this->balance = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::BALANCE_TYPE,
            'is_active' => '1'
        ]);
        
        $this->postParams = [
            "package_id" => $this->package->id,
            "request_type" => PremiumPackage::PACKAGE_TYPE
        ];
    }

    public function testGetTransactionDetailSuccess()
    {
        $actualPrice = 10342;
        $result = (new MidtransToken($this->user, $this->postParams))->transactionDetail();
        $token = (new MidtransToken($this->user, $this->postParams));
        $token->price = $actualPrice;
        $result2 = $token->transactionDetail();

        $this->assertEquals(10000, $result['package']['total']);
        $this->assertEquals(342, $result2['package']['unique_code']);
    }

    public function testGetCustomerDetailSuccess()
    {
        $result = (new MidtransToken($this->user, $this->postParams))->customerDetail();
        $result2 = (new MidtransToken(null, $this->postParams))->customerDetail();

        $this->assertEquals(self::EMAIL, $result['email']);
        $this->assertEquals(self::PHONE_NUMBER, $result['phone']);
        $this->assertEquals(self::DEFAULT_CUSTOMER_NAME, $result2['name']);
    }

    public function testGetExpiredTimeSuccess()
    {
        $result = (new MidtransToken($this->user, $this->postParams))->expiredTime();
        
        $this->assertEquals(2, $result['duration']);
        $this->assertEquals('hour', $result['unit']);
    }

    public function testItemsSuccess()
    {
        $result = (new MidtransToken($this->user, $this->postParams))->items([
            "package" => $this->package,
            "total" => 1000,
            "unique_code" => 123
        ]);
        
        $this->assertEquals(1, $result[0]['quantity']);
        $this->assertEquals(1000, $result[0]['price']);
    }

    public function testGetPackagePriceSuccess()
    {
        $actualPrice = 98232;
        $premiumPackage = factory(PremiumPackage::class)->create([
            "sale_price" => 0,
            "price" => $actualPrice
        ]);
        $user = factory(User::class)->create();

        $result = (new MidtransToken($this->user, $this->postParams))->PackagePrice($this->package);
        $result2 = (new MidtransToken($user, [
            'package_id' => $premiumPackage->id, 
            'request_type' => PremiumPackage::PACKAGE_TYPE
        ]))->PackagePrice($premiumPackage);

        $this->assertEquals(10000, $result);
        $this->assertEquals($actualPrice, $result2);
    }

    public function testGetPackageSuccess()
    {
        $result = (new MidtransToken($this->user, $this->postParams))->PremiumPackageOne();
        $this->assertTrue($result instanceOf PremiumPackage);
    }

    public function testGetTokenWasNull()
    {
        $data = [
            'package_id' => 0,
            'request_type' => PremiumPackage::BALANCE_TYPE
        ];
        $getToken = (new MidtransToken($this->user, $data))->perform();
        $this->assertNull($getToken);
    }

    public function testGetTokenWithPriceFromPreviousPayment()
    {
        $actualPrice = 10923;
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        factory(PremiumRequest::class)->create([
            "expired_status" => "false",
            "user_id" => $user->id,
            "total" => $actualPrice,
            "premium_package_id" => $this->package->id
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "user_id" => $user2->id
        ]);

        factory(BalanceRequest::class)->create([
            "expired_status" => "false",
            "premium_request_id" => $premiumRequest->id,
            "premium_package_id" => $this->balance->id,
            "price" => $actualPrice
        ]);

        $getToken = (new MidtransToken($user, $this->postParams));
        $getToken->getPriceFromExistingPayment();

        $getToken2 = (new MidtransToken($user2, [
            "package_id" => $this->balance->id,
            "request_type" => PremiumPackage::BALANCE_TYPE
        ]));
        $getToken2->getPriceFromExistingPayment();

        $this->assertEquals($getToken->price, $actualPrice);
        $this->assertEquals($getToken2->price, $actualPrice);
    }

    public function testGetPremiumRequestWhenGetToken()
    {
        $userPackage = factory(User::class)->create();
        $userPackage2 = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id" => $userPackage->id
        ]);
        
        $premiumRequest2 = factory(PremiumRequest::class)->create([
            "user_id" => $userPackage2->id,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "expired_status" => null
        ]);

        $userPremiumRequestNotNull = (new MidtransToken($userPackage, [
            'package_id' => $this->package->id,
            'request_type' => PremiumPackage::PACKAGE_TYPE,
            'request_id' => $premiumRequest->id
        ]))->premiumRequest();
        $userPremiumRequestNull = (new MidtransToken($userPackage2, [
            'package_id' => $this->package->id,
            'request_type' => PremiumPackage::PACKAGE_TYPE,
            'request_id' => $premiumRequest->id
        ]))->premiumRequest();

        $this->assertEquals($userPremiumRequestNotNull->id, $premiumRequest->id);
        $this->assertEquals($userPremiumRequestNull->id, $premiumRequest2->id);
    }

}