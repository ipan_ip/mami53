<?php
namespace Test\App\Entities\Classes;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Classes\PremiumAction;
use Notification;

class PremiumActionTest extends MamiKosTestCase
{
    public function setUp() : void
    {
        parent::setUp();   
    }

    public function testAutoApproveTrialFailed()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id
        ]);

        $result = (new PremiumAction([
            'user' => $user,
            'request' => []
        ]))->autoApproveTrial();

        $this->assertFalse($result);
    }

    public function testAutoApproveTrialSuccess()
    {
        Notification::shouldReceive('send');
        $user = factory(User::class)->create();
        factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::TRIAL_TYPE,
            'start_date' => date('Y-m-d'),
            'sale_limit_date' => date('Y-m-d', strtotime('+1 day')),
            'is_active' => '1'
        ]);

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');
        
        $result = (new PremiumAction([
            'user' => $user,
            'request' => []
        ]))->autoApproveTrial();
        
        $this->assertTrue($result);
    }

    public function testCanTrialFailed()
    {
        $user = factory(User::class)->create();
        factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
        ]);

        $result = (new PremiumAction([
            'user' => $user,
            'request' => []
        ]))->canTrial();

        $this->assertFalse($result);
    }

    public function testCanTrialSuccess()
    {
        $user = factory(User::class)->create();

        $result = (new PremiumAction([
            'user' => $user,
            'request' => []
        ]))->canTrial();

        $this->assertTrue($result);
    }
}