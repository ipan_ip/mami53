<?php
namespace App\Entities\Classes;

use App\Entities\Promoted\HistoryPromote;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\Room;

use App\Test\MamiKosTestCase;

class AdminDeactivateTest extends MamiKosTestCase
{
    // the code itself is buggy. and im not sure how it should behave correctly.
    // public function testPerformOnRoomNull()
    // {
    //     $id = rand(100000, 10000000);
    //     // make sure room doesnt exists
    //     while (Room::find($id)) {
    //         $id = rand(100000, 10000000);
    //     }
    //     $res = (new AdminDeactivate($id))->perform();
        
    //     $this->assertArrayHasKey('redirect');
    //     $this->assertArrayHasKey('message');
    //     $this->assertEquals('admin/room/promotion/'.$id, $res['redirect']);
    // }

    public function testPerformOnRoomNotPromoted()
    {
        $room = factory(Room::class)->create(['is_promoted' => 'false']);
        $res = (new AdminDeactivate($room->id))->perform();
        
        $this->assertArrayHasKey('redirect', $res);
        $this->assertArrayHasKey('message', $res);
        $this->assertEquals('admin/room/promotion/'.$room->id, $res['redirect']);
    }

    public function testPerformOnViewPromoteNotActive()
    {
        $room = factory(Room::class)->create(['is_promoted' => 'true']);
        factory(ViewPromote::class)->create([
            'designer_id' => $room->id,
            'is_active' => false
            ]);

        $res = (new AdminDeactivate($room->id))->perform();
        
        $this->assertArrayHasKey('redirect', $res);
        $this->assertArrayHasKey('message', $res);
        $this->assertEquals('admin/room/promotion/'.$room->id, $res['redirect']);
    }

    public function testPerformOnViewPromoteActive()
    {
        $room = factory(Room::class)->create(['is_promoted' => 'true']);
        $pr = factory(PremiumRequest::class)->create();
        $vp = factory(ViewPromote::class)->create([
            'designer_id' => $room->id,
            'is_active' => true,
            'premium_request_id' => $pr->id
            ]);
        factory(HistoryPromote::class)->create([
            'view_promote_id' => $vp->id,
            'session_id' => $vp->session_id]);
        
        $res = (new AdminDeactivate($room->id))->perform();
        
        $this->assertArrayHasKey('redirect', $res);
        $this->assertArrayHasKey('message', $res);
        $this->assertEquals('admin/room/promotion/'.$room->id, $res['redirect']);
        $this->assertStringContainsString('berhasil', strtolower($res['message']));
    }
}