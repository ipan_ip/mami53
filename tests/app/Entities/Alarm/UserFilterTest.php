<?php

namespace App\Entities\Alarm;

use App\Test\MamiKosTestCase;
use App\User;

class UserFilterTest extends MamiKosTestCase
{
    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $filter = factory(UserFilter::class)->create(['user_id' => $user->id]);

        $this->assertTrue($filter->user->is($user));
    }
}
