<?php

namespace App\Entities\Tracker;

use Mockery;
use Illuminate\Foundation\Testing\WithFaker;

use App\Test\MamiKosTestCase;
use App\Libraries\MockHelper;
use App\Entities\DailyDocumentModel;
use App\Entities\Tracker\SearchKeyword;

class SearchKeywordTest extends MamiKosTestCase
{
    use WithFaker;

    public function testGetClientByRoute(): void
    {
        // web
        $this->assertEquals(SearchKeyword::CLIENT_WEB, SearchKeyword::getClientTypeByRoute('/garuda/suggest'));

        // app
        $this->assertEquals(SearchKeyword::CLIENT_APP, SearchKeyword::getClientTypeByRoute('/api/v1/suggest'));
        $this->assertEquals(SearchKeyword::CLIENT_APP, SearchKeyword::getClientTypeByRoute('/api/v2/suggest'));

        // unknown
        $this->assertEquals(SearchKeyword::CLIENT_UNKNOWN, SearchKeyword::getClientTypeByRoute('/this/is/unknown'));
        $this->assertEquals(SearchKeyword::CLIENT_UNKNOWN, SearchKeyword::getClientTypeByRoute(''));
    }

    public function testGetAllowDailyCollectionNameShouldReturnTrue(): void
    {
        $mock = Mockery::mock(SearchKeyword::class)
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();
        $this->assertTrue($mock->getAllowDailyCollectionName());
    }

    public function testLog(): void
    {
        $keyword = $this->faker()->city;
        $userId = $this->faker()->randomDigit;
        $route = SearchKeyword::ROUTE_FOR_APP_TESTING;
        $os = 'android';
        $version = '1.0.3';
        $clientType = SearchKeyword::getClientTypeByRoute($route);

        // Mock getAllowDailyCollectionName returns false
        // so it will use collection name without date
        $mock = Mockery::mock(SearchKeyword::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();
        $this->app->instance(SearchKeyword::class, $mock);
        $mock->shouldReceive('getAllowDailyCollectionName')->andReturn(false);
        
        // test call
        $logged = SearchKeyword::log($keyword, $route, $os, $version, $userId);

        // approvement
        $found = SearchKeyword::find($logged->id);
        $this->assertNotNull($found);
        $this->assertEquals($keyword, $found->keyword);
        $this->assertEquals($userId, $found->userId);
        $this->assertEquals($route, $found->route);
        $this->assertEquals($os, $found->os);
        $this->assertEquals($version, $found->version);
        $this->assertEquals(strtolower($keyword), $found->lowerKeyword);
        $this->assertEquals($clientType, $found->clientType);

        if (is_null($found) === false)
        {
            $found->delete();
        }
    }
}