<?php

namespace App\Entities\ABTest\Helper;

use App\Channel\ABTest\ABTestClient;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ABTestRequestHelperTest extends MamiKosTestCase
{

    private $client;
    private $response;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = $this->createMock(ABTestClient::class);
        $this->response = $this->createMock(ResponseInterface::class);
    }

    private function createHelper()
    {
        return new ABTestRequestHelper($this->client);
    }

    public function testGetUserDataShouldSuccess()
    {
        $user = factory(User::class)->create();
        $experimentId = 6;

        $this->response->method('getBody')
            ->willReturn(
                json_encode([
                    'status' => 'success',
                    'data' => [
                        "user_id" => $user->id,
                        "device_id" => "839c77c0-8c4e-492f-bf65-e5db9ef9b412",
                        "session_id" => "681fcea9-9d84-45df-a27c-157d56f0ca4e",
                        "experiment_id" => 1,
                        "experiment_value" => "control",
                        "is_active" => true
                    ]
                ])
            );

        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserData($user, ['experiment_id' => $experimentId]);

        $this->assertNotNull($response);
    }

    public function testGetUserDataShouldReturnEmpty()
    {
        $user = factory(User::class)->create();
        $experimentId = 6;

        $this->response->method('getBody')->willReturn([]);

        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserData($user, []);

        $this->assertNotNull($response);
    }

    public function testGetUserDataWithEmptyExperimentId()
    {
        $user = factory(User::class)->create();
        $experimentId = 6;

        $this->response->method('getBody')->willReturn([]);

        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserData($user, ['device_id' => 'device_id']);

        $this->assertNotNull($response);
    }

    public function testGetUserDataGotExceptionThrow()
    {
        $user = factory(User::class)->create();
        $experimentId = 6;
        $exception = new \Exception;
        $this->response->method('getBody')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $this->client->method('post')->willReturn($this->response);

        $helper = $this->createHelper();
        $response = $helper->getUserData($user, ['experiment_id' => $experimentId]);

        $this->assertEquals($response, array());
    }
}