<?php

namespace App\Entities\Landing;

use App\Entities\Geocode\GeocodeCache;
use App\Test\MamiKosTestCase;

class LandingListTest extends MamiKosTestCase
{
    
    public function testItShouldReturnValid()
    {
        $landing = factory(LandingList::class)->create([
            'parent_id' => factory(LandingList::class)->create()->id,
        ]);

        $this->assertInstanceOf('App\Entities\Landing\LandingList', $landing->landing_parent);
    }

    public function testGetAllBookingLandingShouldCorrect()
    {
        $landing = factory(LandingList::class)->create([
            'type' => 'booking',
            'parent_id' => 0,
        ]);

        $this->assertInstanceOf(
            '\Illuminate\Database\Eloquent\Builder', 
            $landing->getAllBookingLanding()
        );
    }

    public function testGetIndexBookingLandingShouldCorrect()
    {
        $landing = factory(LandingList::class)->create([
            'type' => 'booking',
            'parent_id' => 0,
        ]);

        $this->assertInstanceOf(
            '\Illuminate\Database\Eloquent\Builder', 
            $landing->getIndexBookingLanding()
        );
    }

    public function testGetParentBookingLandingShouldCorrect()
    {
        $landing = factory(LandingList::class)->create([
            'type' => 'booking',
            'slug' => '',
        ]);

        $response = LandingList::getParentBookingLanding();
        $this->assertEquals($landing->id, $response->id);
        $this->assertEquals($landing->is_active, $response->is_active);
    }

    public function testGetDefaultBookingLandingShouldCorrect()
    {
        $landing = factory(LandingList::class)->create([
            'type' => 'booking',
            'slug' => 'kost-murah',
            'is_active' => 1,
            'is_default' => 0,
        ]);

        $response = LandingList::getDefaultBookingLanding('kost-murah');
        $this->assertEquals($landing->id, $response->id);
        $this->assertEquals($landing->slug, $response->slug);
    }

    public function testNormalizeCityNameShouldCorrect()
    {
        $landingList = new LandingList;
        $method = new \ReflectionMethod(LandingList::class, 'normalizeCityName');
        $method->setAccessible(true);

        $this->assertEquals('Malang kota', $method->invoke($landingList, 'malang-kota'));
        $this->assertEquals('Balikpapan', $method->invoke($landingList, 'balikpapan'));
        $this->assertEquals('Ujung pandang', $method->invoke($landingList, 'ujung-pandang'));
    }

    public function testGetGeocodeByCityNameShouldCorrect()
    {
        $dummyGeolocation = factory(\App\Entities\Geocode\GeocodeCache::class)->create([
            'subdistrict' => '',
            'city' => 'surabaya',
            'status' => 'processed',
        ]);

        $result = LandingList::getGeocodeByCityName('surabaya');
        $response = $result->toArray();

        $this->assertEquals($dummyGeolocation->id, $result->id);
        $this->assertEquals('surabaya', $result->city);

        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('city', $response);
        $this->assertArrayHasKey('latitude', $response);
        $this->assertArrayHasKey('longitude', $response);
    }

    public function testGetCitiesAjaxShouldCorrect()
    {
        factory(\App\Entities\Room\Room::class)->create([
            'name' => 'Nurdiono House',
            'area_city' => 'depok sleman'
        ]);

        factory(\App\Entities\Room\Room::class)->create([
            'name' => 'Nurdiono House',
            'area_city' => 'malioboro'
        ]);

        $response = LandingList::getCitiesAjax('name', 'Nurdiono House');
        $this->assertEquals([
            ['id' => 'depok-sleman', 'text' => 'depok sleman'],
            ['id' => 'malioboro', 'text' => 'malioboro'],
        ], $response);
    }

    public function testListParentLandingShouldCorrect()
    {
        $landingParent = factory(LandingList::class)->create([
            'slug' => 'kost-jogja',
            'is_active' => 1,
            'parent_id' => 0,
        ]);

        factory(LandingList::class)->create([
            'slug' => 'nurdiono-house',
            'is_active' => 1,
            'parent_id' => $landingParent->id,
        ]);

        factory(LandingList::class)->create([
            'slug' => 'residence',
            'is_active' => 1,
            'parent_id' => $landingParent->id,
        ]);

        $response = LandingList::listParentLanding($landingParent);
        $this->assertEquals(2, count($response));
    }

    public function testGetAvailableBookingCitiesShouldCorrect()
    {
        $landing = factory(LandingList::class)->create([
            'slug' => 'halmahera-utara',
            'type' => 'booking',
            'is_active' => 1,
            'is_default' => 1,
            'sort' => 1,
            'option' => '{
                "title": "test",
                "article": "Test Article",
                "location":[
                    ["107.41710831343764","-6.866098369173987"],
                    ["107.32646731511218","-6.945512219508057"]
                ]
            }'
        ]);

        $response = LandingList::getAvailableBookingCities();
        $this->assertArrayHasKey('halmahera-utara', $response);
        $this->assertEquals('Halmahera utara', $response['halmahera-utara']['label']);
        $this->assertEquals('halmahera-utara', $response['halmahera-utara']['slug']);
    }

    public function testSaveSortOrderShouldCorrectReturnTrue()
    {
        $landing1 = factory(LandingList::class)->create([
            'slug' => 'nurdiono-house',
            'is_active' => 1,
            'sort' => 3,
        ]);

        $landing2 = factory(LandingList::class)->create([
            'slug' => 'residence',
            'is_active' => 1,
            'sort' => 4,
        ]);

        $payload = [
            ['id' => $landing1->id, 'sort' => 2],
            ['id' => $landing2->id, 'sort' => 1],
        ];

        $response = LandingList::saveSortOrder($payload);

        $resultLanding1 = LandingList::find($landing1->id);
        $resultLanding2 = LandingList::find($landing2->id);

        $this->assertTrue($response);
        $this->assertEquals('2', $resultLanding1->sort);
        $this->assertEquals('1', $resultLanding2->sort);
    }

    public function testSaveSortOrderShouldCorrectReturnFalse()
    {
        $payload = [
            ['id' => 99, 'sort' => 2],
        ];

        $response = LandingList::saveSortOrder($payload);
        $this->assertFalse($response);
    }

    public function testStore()
    {
        $landing = factory(LandingList::class)->make([
            'city' => 'depok',
            'keyword' => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'description' => 'description content',
        ]);

        $response = LandingList::store('kost', $landing);

        $this->assertNotEmpty($response->id);
        $this->assertEquals('kost', $response->type);
        $this->assertEquals(
            'Rukost Murah - Rumah Kost Mulai Rp 53.250', 
            $response->keyword
        );
    }

    public function testStoreShouldCorrectWhenTypeBooking()
    {
        $landing = factory(LandingList::class)->make([
            'city'          => 'depok',
            'keyword'       => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'title'         => 'title content',
            'article'       => 'title article',
            'description'   => 'description content',
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
        ]);

        $response = LandingList::store('booking', $landing);

        $this->assertNotEmpty($response->id);
        $this->assertEquals('booking', $response->type);
        $this->assertEquals(
            'Rukost Murah - Rumah Kost Mulai Rp 53.250', 
            $response->keyword
        );
    }

    public function testStoreShouldCorrectWhenTypeBookingSpecific()
    {
        $landing = factory(LandingList::class)->make([
            'city'          => 'depok',
            'keyword'       => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'title'         => 'title content',
            'article'       => 'title article',
            'parent_id'     => 1,
            'price_min'     => 1000000,
            'price_max'     => 100000000,
            'rent_type'     => 'monthly',
            'gender'        => ['male'],
            'concern_ids'   => ['1'],
            'description'   => 'description content',
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
            'area'          => 'jogjakarta'
        ]);

        $response = LandingList::store('booking-specific', $landing);

        $this->assertNotEmpty($response->id);
        $this->assertEquals('booking-specific', $response->type);
        $this->assertEquals(
            'Rukost Murah - Rumah Kost Mulai Rp 53.250', 
            $response->keyword
        );
    }

    public function testUpdateDataShouldCorrect()
    {
        // insert first to make it more easy
        $landing = factory(LandingList::class)->make([
            'city'          => 'depok',
            'keyword'       => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'title'         => 'title content',
            'article'       => 'title article',
            'description'   => 'description content',
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
        ]);

        $response = LandingList::store('booking', $landing);
        
        $newLanding = factory(LandingList::class)->make([
            'config-keyword'    => 'new keyword',
            'config-description'=> 'new description content',
            'config-title'      => 'new title content',
            'config-article'    => 'new article content',
        ]);

        // then update
        LandingList::updateData($response->id, 'booking-parent', $newLanding);

        // check from db
        $responseAfterUpdate = LandingList::find($response->id);
        
        $this->assertEquals('new keyword', $responseAfterUpdate->keyword);
        $this->assertEquals('new description content', $responseAfterUpdate->description);
    }

    public function testUpdateDataShouldCorrectWithBookingSpecific()
    {
        // insert first to make it more easy
        $landing = factory(LandingList::class)->make([
            'city'          => 'depok',
            'keyword'       => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'title'         => 'title content',
            'article'       => 'title article',
            'description'   => 'description content',
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
        ]);

        $response = LandingList::store('booking', $landing);
        
        $newLanding = factory(LandingList::class)->make([
            'city'          => 'new-depok',
            'keyword'       => 'new keyword',
            'title'         => 'title content',
            'article'       => 'title article',
            'description'   => 'new description content',
            'parent_id'     => 2,
            'price_min'     => 1000000,
            'price_max'     => 100000000,
            'rent_type'     => 'monthly',
            'gender'        => ['male'],
            'concern_ids'   => ['1'],
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
            'area'          => 'new jogjakarta'
        ]);

        // then update
        LandingList::updateData($response->id, 'booking-specific', $newLanding);

        // check from db
        $responseAfterUpdate = LandingList::find($response->id);
        
        $this->assertEquals('new-depok', $responseAfterUpdate->slug);
        $this->assertEquals('new keyword', $responseAfterUpdate->keyword);
        $this->assertEquals('new description content', $responseAfterUpdate->description);
        $this->assertEquals(2, $responseAfterUpdate->parent_id);
        $this->assertEquals('new jogjakarta', $responseAfterUpdate->area);
    }

    public function testStoreShouldCorrectWhenTypeBookingSpecificWithIsIndexed()
    {
        $landing = factory(LandingList::class)->make([
            'city'          => 'depok',
            'keyword'       => 'Rukost Murah - Rumah Kost Mulai Rp 53.250',
            'title'         => 'title content',
            'article'       => 'title article',
            'parent_id'     => 1,
            'price_min'     => 1000000,
            'price_max'     => 100000000,
            'rent_type'     => 'monthly',
            'gender'        => ['male'],
            'concern_ids'   => ['1'],
            'description'   => 'description content',
            'longitude_2'   => '-6.185270134775632',
            'latitude_2'    => '106.86951076765001',
            'longitude_1'   => '-6.185270134775632',
            'latitude_1'    => '106.86951076765001',
            'area'          => 'jogjakarta',
            'indexed'       => 1
        ]);

        $response = LandingList::store('booking-specific', $landing);

        $this->assertNotEmpty($response->id);
        $this->assertEquals('booking-specific', $response->type);
        $this->assertEquals(
            'Rukost Murah - Rumah Kost Mulai Rp 53.250', 
            $response->keyword
        );
    }

    public function testGetOptionValueAttribute()
    {
        $landing = new LandingList();
        $landing->option = '{
            "title": "test",
            "article": "Test Article",
            "location":[
                ["107.41710831343764","-6.866098369173987"],
                ["107.32646731511218","-6.945512219508057"]
            ]
        }';
        $res = $landing->getOptionValueAttribute();
        
        $this->assertIsObject($res);
    }
}