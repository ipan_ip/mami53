<?php

namespace App\Entities\Landing;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class LandingApartmentTest extends MamiKosTestCase
{
    
    public function testRelationTags_Success()
    {
        $landingApartment = factory(LandingApartment::class)->create();

        $result = $landingApartment->tags();

        $this->assertInstanceOf(BelongsToMany::class, $result);
    }

    public function testRelationParent_Success()
    {
        $landingApartment = factory(LandingApartment::class)->create();
        $result = $landingApartment->parent();

        $this->assertInstanceOf(BelongsTo::class, $result);
    }

    public function testRelationPhoto_Success()
    {
        $landingApartment = factory(LandingApartment::class)->create();
        $result = $landingApartment->photo();

        $this->assertInstanceOf(HasOne::class, $result);
    }

    public function testGetLocationAttribute_Array()
    {
        $landingApartment = factory(LandingApartment::class)->make();

        $coordinate = array(
            array((float) $landingApartment->longitude_1, (float) $landingApartment->latitude_1),
            array((float) $landingApartment->longitude_2, (float) $landingApartment->latitude_2)
        );

        $result = $landingApartment->getLocationAttribute();

        $this->assertEquals($coordinate, $result);
        $this->assertIsArray($result);
    }

    public function testGetFiltersAttribute_Array()
    {
        $landingApartment = factory(LandingApartment::class)->make();

        $result = $landingApartment->getFiltersAttribute();

        $this->assertIsArray($result);
        $this->assertSame($result['property_type'], 'apartment');
        $this->assertSame($result['place'][0], $landingApartment->area_city);
        $this->assertSame($result['place'][1], $landingApartment->area_subdistrict);
        $this->assertArrayHasKey('price_range', $result, "Array doesn't contains 'price_range' as key"); 
        $this->assertArrayHasKey('rent_type', $result, "Array doesn't contains 'rent_type' as key");
        $this->assertArrayHasKey('tag_ids', $result, "Array doesn't contains 'tag_ids' as key");
        $this->assertArrayHasKey('place', $result, "Array doesn't contains 'place' as key");
        $this->assertArrayHasKey('furnished', $result, "Array doesn't contains 'furnished' as key");
        $this->assertArrayHasKey('property_type', $result, "Array doesn't contains 'property_type' as key");
        $this->assertArrayHasKey('unit_type', $result, "Array doesn't contains 'unit_type' as key");
    }

    public function testCleanNoFollowLinks_NoFollowLinks()
    {
        $landingApartment = factory(LandingApartment::class)->make([
            'description_1' => '<a href="https://ahrefs1.com" rel="nofollow">blue text 1</a>',
            'description_2' => '<a href="https://ahrefs2.com" rel="nofollow">blue text 2</a>',
            'description_3' => '<a href="https://ahrefs3.com" rel="nofollow">blue text 3</a>',
        ]);
        
        $landingApartment->cleanNoFollowLinks();

        $this->assertSame(strpos($landingApartment->description_1, 'rel="nofollow"'), false);
        $this->assertSame(strpos($landingApartment->description_2, 'rel="nofollow"'), false);
        $this->assertSame(strpos($landingApartment->description_3, 'rel="nofollow"'), false);
    }

    public function testGetLandingChildByArea_Condition1()
    {
        $expected = [
            'child' => null,
            'parent' => null
        ];
        $landingApartment = factory(LandingApartment::class)->make();
        $result = $landingApartment->getLandingChildByArea('', '');

        $this->assertIsArray($result);
        $this->assertSame($expected, $result);
        $this->assertSame(count($expected), count($result));
    }

    public function testGetLandingChildByArea_Condition2()
    {
        $expected = [
            'child' => null,
            'parent' => null
        ];
        $expectedCity = 'cities';
        $expectedSubdistrict = '';

        $landingApartment = factory(LandingApartment::class)->create([
            'area_city' => 'city',
            'area_subdistrict' => ''
        ]);
        $result = $landingApartment->getLandingChildByArea($expectedCity, $expectedSubdistrict);

        $this->assertIsArray($result);
        $this->assertSame($expected, $result);
        $this->assertSame(count($expected), count($result));
    }

    public function testGetLandingChildByArea_Condition3()
    {
        $unexpected = [
            'child' => null,
            'parent' => null
        ];
        $expectedCity = 'city';
        $expectedSubdistrict = 'subdistrict';
        $childCountExpected = 2;

        $parentLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => $expectedCity,
            'area_subdistrict' => $expectedSubdistrict
        ]);
        factory(LandingApartment::class, $childCountExpected)->create([
            'parent_id' => $parentLandingApartment->id
        ]);
        $result = $parentLandingApartment->getLandingChildByArea($expectedCity, $expectedSubdistrict);

        $this->assertIsArray($result);
        $this->assertNotEquals($unexpected, $result);
        $this->assertSame($childCountExpected, $result['child']->count());
        $this->assertInstanceOf(LandingApartment::class, $result['parent']);
        $this->assertInstanceOf(Collection::class, $result['child']);
    }

    public function testGetLandingChildByKostLanding_Condition1()
    {
        $expected = [
            'child' => null,
            'parent' => null
        ];
        $expectedCity = '';

        $parentLanding = factory(Landing::class)->create([
            'area_city' => $expectedCity,
        ]);
        $parentLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => $expectedCity,
        ]);

        $result = $parentLandingApartment->getLandingChildByKostLanding($parentLanding);

        $this->assertIsArray($result);
        $this->assertSame($expected, $result);
        $this->assertSame(count($expected), count($result));
    }

    public function testGetLandingChildByKostLanding_Condition2()
    {
        $expected = [
            'child' => null,
            'parent' => null
        ];

        $parentLanding = factory(Landing::class)->create([
            'area_city' => 'city',
        ]);
        $parentLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => 'cities',
        ]);

        $result = $parentLandingApartment->getLandingChildByKostLanding($parentLanding);

        $this->assertIsArray($result);
        $this->assertSame($expected, $result);
        $this->assertSame(count($expected), count($result));
    }

    public function testGetLandingChildByKostLanding_Condition3()
    {
        $unexpected = [
            'child' => null,
            'parent' => null
        ];
        $expectedCity = 'city';
        $expectedSubdistrict = 'subdistrict';
        $childCountExpected = 2;

        $parentLanding = factory(Landing::class)->create([
            'area_city' => $expectedCity,
            'area_subdistrict' => $expectedSubdistrict 
        ]);        
        $parentLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => $expectedCity,
            'area_subdistrict' => $expectedSubdistrict
        ]);
        factory(LandingApartment::class, $childCountExpected)->create([
            'parent_id' => $parentLandingApartment->id
        ]);

        $result = $parentLandingApartment->getLandingChildByKostLanding($parentLanding);

        $this->assertIsArray($result);
        $this->assertNotEquals($unexpected, $result);
        $this->assertSame($childCountExpected, $result['child']->count());
        $this->assertInstanceOf(LandingApartment::class, $result['parent']);
        $this->assertInstanceOf(Collection::class, $result['child']);
    }

    public function testGetLandingChildByParent_ParentIdNotNull()
    {
        $grandParentLandingApartment = factory(LandingApartment::class)->create([
            'redirect_id' => null
        ]);
        $parentLandingApartment = factory(LandingApartment::class)->create([
            'parent_id' => $grandParentLandingApartment->id,
            'redirect_id' => null
        ]);

        $result = $parentLandingApartment->getLandingChildByParent($parentLandingApartment);
        
        $this->assertSame(1, $result->count());
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testGetLandingChildByParent_ParentIdNull()
    {
        $childCountExpected = 5;

        $parentLandingApartment = factory(LandingApartment::class)->create([
            'redirect_id' => null
        ]);
        factory(LandingApartment::class, $childCountExpected)->create([
            'parent_id' => $parentLandingApartment->id,
            'redirect_id' => null
        ]);

        $result = $parentLandingApartment->getLandingChildByParent($parentLandingApartment);
        
        $this->assertSame(($childCountExpected+1), $result->count());
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testGetParentSlugLanding()
    {
        $landingApartment = factory(LandingApartment::class)->create();
        $parentLanding = factory(Landing::class)->create([
            'slug' => $landingApartment->slug,
            'redirect_id' => null
        ]);
        factory(Landing::class)->create([
            'redirect_id' => null,
            'parent_id' => $parentLanding->id
        ]);

        $res = (new LandingApartment)->getParentSlugLanding($landingApartment->slug);
        
        $this->assertInstanceOf(Collection::class, $res);
    }
}
