<?php

namespace App\Entities\Landing\Traits;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class BeforeSavingTest extends MamiKosTestCase
{
    use WithFaker, BeforeSaving;

    public function testStripHtmlTags()
    {
        $sentence = $this->faker->randomHtml(1,1);
        $res = $this->stripHtmlTags($sentence);

        $this->assertNotSame($res, $sentence);
        $this->assertIsString($res);
    }
}