<?php

namespace App\Entities\Landing;

use App\Test\MamiKosTestCase;

use InvalidArgumentException;
use App\Entities\Room\Element\Tag;
use App\Entities\Component\BreadcrumbList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

use function factory;

class LandingTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    //////////////////////////////////////////////////
    //// Landing::normalizeGender
    //////////////////////////////////////////////////

    /**
     * @covers \App\Entities\Landing\Landing::normalizeGender
     */
    public function testNormalizeGenderReturnNullIfArgIsNull()
    {
        $this->assertSame(null, Landing::normalizeGender(null));
    }

    /**
     * @covers       \App\Entities\Landing\Landing::normalizeGender
     * @dataProvider validGenderProviderForNormalizeGender
     */
    public function testNormalizeGenderReturnIntArrayIfItIsValid($genders, $expected)
    {
        $this->assertSame($expected, Landing::normalizeGender($genders));
    }

    public function validGenderProviderForNormalizeGender()
    {
        return [
            'int array - campur, pria, wanita' => [[0, 1, 2], [0, 1, 2]],
            'int array - campur, pria' => [[0, 1], [0, 1]],
            'int array - campur' => [[0], [0]],
            'string array - campur, pria, wanita' => [["0", "1", "2"], [0, 1, 2]],
            'string array - campur, pria' => [["0", "1"], [0, 1]],
            'string array - campur' => [["0"], [0]],
        ];
    }

    /**
     * @covers \App\Entities\Landing\Landing::normalizeGender
     */
    public function testNormalizeGenderThrowInvalidArgumentExceptionIfArgIsNumberNotArray()
    {
        $this->expectException(InvalidArgumentException::class);

        Landing::normalizeGender(1);
    }

    /**
     * @covers \App\Entities\Landing\Landing::normalizeGender
     */
    public function testNormalizeGenderThrowInvalidArgumentExceptionIfArgIsStringNotArray()
    {
        $this->expectException(InvalidArgumentException::class);

        Landing::normalizeGender("0");
    }

    //////////////////////////////////////////////////
    //// Landing::convertGenderRequestToGender
    //////////////////////////////////////////////////
    /**
     * @covers \App\Entities\Landing\Landing::convertGenderRequestToGender
     */
    public function testConvertGenderRequestToGenderReturnNullIfArgIsNull()
    {
        $this->assertSame(null, Landing::convertGenderRequestToGender(null));
    }

    /**
     * @covers       \App\Entities\Landing\Landing::convertGenderRequestToGender
     * @dataProvider validGenderProviderForconvertGenderRequestToGender
     */
    public function testConvertGenderRequestToGenderReturnIntArrayIfItIsValid($genders, $expected)
    {
        $this->assertSame($expected, Landing::convertGenderRequestToGender($genders));
    }

    public function validGenderProviderForConvertGenderRequestToGender()
    {
        return [
            'string array - campur, pria, wanita' => [["0", "1", "2"], "[0,1,2]"],
            'string array - campur, pria' => [["0", "1"], "[0,1]"],
            'string array - campur' => [["0"], "[0]"],
        ];
    }

    /**
     * @covers \App\Entities\Landing\Landing::convertGenderRequestToGender
     */
    public function testConvertGenderRequestToGenderThrowInvalidArgumentExceptionIfArgIsNumberNotArray()
    {
        $this->expectException(InvalidArgumentException::class);

        Landing::convertGenderRequestToGender(1);
    }

    /**
     * @covers \App\Entities\Landing\Landing::convertGenderRequestToGender
     */
    public function testConvertGenderRequestToGenderThrowInvalidArgumentExceptionIfArgIsStringNotArray()
    {
        $this->expectException(InvalidArgumentException::class);

        Landing::convertGenderRequestToGender("0");
    }

    //////////////////////////////////////////////////
    //// Landing::renderGenderForDataForm
    //////////////////////////////////////////////////
    /**
     * @covers \App\Entities\Landing\Landing::renderGenderForDataForm
     */
    public function testRenderGenderForDataFormReturnNullIfArgIsNull()
    {
        $this->assertSame(null, Landing::renderGenderForDataForm(null));
    }

    /**
     * @covers       \App\Entities\Landing\Landing::renderGenderForDataForm
     * @dataProvider validGenderProviderForRenderGenderForDataForm
     */
    public function testRenderGenderForDataFormReturnIntArrayIfItIsValid($genders, $expected)
    {
        $this->assertSame($expected, Landing::renderGenderForDataForm($genders));
    }

    public function validGenderProviderForRenderGenderForDataForm()
    {
        return [
            'JSON string - campur, pria, wanita' => ["[0,1,2]", [0, 1, 2]],
            'JSON string - campur, pria' => ["[0,1]", [0, 1]],
            'JSON string - campur' => ["[0]", [0]],
        ];
    }


    public function testLandingBelongsToParentLanding()
    {
        $landing1 = factory(Landing::class)->create(['parent_id' => null]);

        $landing2 = factory(Landing::class)->create(['parent_id' => $landing1->id]);

        $this->assertInstanceOf(Landing::class, $landing2->parent);
    }

    public function testLandingHasOneImage()
    {
        $image = factory(\App\Entities\Media\Media::class)->create();

        $landing = factory(Landing::class)->create(['image' => $image->id]);

        $this->assertInstanceOf(\App\Entities\Media\Media::class, $landing->images);
    }

    public function testGetLocationAttribute()
    {
        $landing = factory(Landing::class)->create();

        $cordinate = array(
            array((float)$landing->longitude_1, (float)$landing->latitude_1),
            array((float)$landing->longitude_2, (float)$landing->latitude_2)
        );

        $response = $landing->getLocationAttribute();

        $this->assertEquals($cordinate, $response);
    }

    public function testGetFiltersAttribute()
    {
        $landing = factory(Landing::class)->create();

        $place = [
            $landing->area_city,
            $landing->area_subdistrict
        ];

        $data = array(
            'price_range' => array($landing->price_min, $landing->price_max),
            'rent_type' => is_null($landing->rent_type) ? 2 : $landing->rent_type,
            'gender' => [0, 1, 2],
            'tag_ids' => null,
            'place' => $place
        );

        $response = $landing->getFiltersAttribute();

        $this->assertEquals($data['price_range'], $response['price_range']);
        $this->assertEquals($data['rent_type'], $response['rent_type']);
        $this->assertEquals($data['gender'], $response['gender']);
        $this->assertEquals($data['tag_ids'], $response['tag_ids']);
        $this->assertEquals($data['place'], $response['place']);
    }

    public function testGetFiltersAttributePlaceShouldReturnEmptyArrayIfAreaNull()
    {
        $landing = factory(Landing::class)->create(
            [
                'area_city' => null,
                'area_subdistrict' => null
            ]
        );

        $place = [
            $landing->area_city,
            $landing->area_subdistrict
        ];

        $data = array(
            'price_range' => array($landing->price_min, $landing->price_max),
            'rent_type' => is_null($landing->rent_type) ? 2 : $landing->rent_type,
            'gender' => [0, 1, 2],
            'tag_ids' => null,
            'place' => $place
        );

        $response = $landing->getFiltersAttribute();

        $this->assertNotEquals($data['place'], $response['place']);
        $this->assertEquals([], $response['place']);
    }

    public function testGetFiltersAttributePlaceShouldReturnEmptyArrayIfAreaHasBlankString()
    {
        $landing = factory(Landing::class)->create(
            [
                'area_city' => '',
                'area_subdistrict' => ''
            ]
        );

        $place = [
            $landing->area_city,
            $landing->area_subdistrict
        ];

        $data = array(
            'price_range' => array($landing->price_min, $landing->price_max),
            'rent_type' => is_null($landing->rent_type) ? 2 : $landing->rent_type,
            'gender' => [0, 1, 2],
            'tag_ids' => null,
            'place' => $place
        );

        $response = $landing->getFiltersAttribute();

        $this->assertNotEquals($data['place'], $response['place']);
        $this->assertEquals([], $response['place']);
    }

    public function testGetBreadCrumb()
    {
        $landing = factory(Landing::class)->create();

        $breadCrumb = BreadcrumbList::generate($landing)->getList();

        $response = $landing->getBreadCrumb();

        $this->assertEquals($breadCrumb, $response);
    }

    public function testCleanNoFollowLinks()
    {
        $landing = factory(Landing::class)->create(
            [
                'description_1' => 'testingrel="nofollow"',
                'description_2' => 'roll rel="nofollow"',
                'description_3' => 'up rel="nofollow"down',
            ]
        );

        $landing->cleanNoFollowLinks();

        $this->assertEquals('testing', $landing['description_1']);
        $this->assertEquals('roll ', $landing['description_2']);
        $this->assertEquals('up down', $landing['description_3']);
    }

    public function testGetLandingWithoutRedirectionGiveDataResponse()
    {
        $landing = factory(Landing::class)->create(
            [
                'redirect_id' => null
            ]
        );

        $response = Landing::getLandingWithoutRedirection();

        $this->assertCount(1, $response);
        $this->assertEquals($landing['id'], $response[0]['id']);
        $this->assertEquals($landing['heading_1'], $response[0]['heading_1']);
    }

    public function testGetLandingWithoutRedirectionGiveNoResponseIfRedirectIdIsNotNull()
    {
        $landing = factory(Landing::class)->create(
            [
                'redirect_id' => 5
            ]
        );

        $response = Landing::getLandingWithoutRedirection();

        $this->assertCount(0, $response);
    }

    public function testGetLandingWithoutRedirectionGiveOnlyDataWithNullRedirectId()
    {
        $landing1 = factory(Landing::class)->create(
            [
                'redirect_id' => 5
            ]
        );

        $landing2 = factory(Landing::class)->create(
            [
                'redirect_id' => null
            ]
        );

        $response = Landing::getLandingWithoutRedirection();

        $this->assertCount(1, $response);
    }

    public function testGetChildFromSlug()
    {
        $landingParent = factory(Landing::class)->create(
            [
                'slug' => 'testing-slug',
            ]
        );

        $landingChild1 = factory(Landing::class)->create(
            [
                'parent_id' => $landingParent->id
            ]
        );

        $landingChild2 = factory(Landing::class)->create(
            [
                'parent_id' => $landingParent->id
            ]
        );

        $response = Landing::getChildFromSlug('testing');

        $this->assertCount(2, $response);
        $this->assertEquals($landingChild1['slug'], $response[0]);
        $this->assertEquals($landingChild2['slug'], $response[1]);
    }

    public function testGetChildFromSlugGiveNullResponseIfParentIdIsNotNull()
    {
        $landingParent = factory(Landing::class)->create(
            [
                'slug' => 'testing-slug',
            ]
        );

        factory(Landing::class, 2)->create();

        $response = Landing::getChildFromSlug('testing');

        $this->assertCount(0, $response);
    }

    public function testScopeCampus()
    {
        $res = (new Landing)->campus();
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testScopeChosenUniv_EmptyArray()
    {
        $res = (new Landing)->chosenUniv([]);
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testScopeChosenUniv_NonEmptyArray()
    {
        $res = (new Landing)->chosenUniv(['lala']);
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testFirstPhoto()
    {
        $this->assertInstanceOf(BelongsTo::class, (new Landing)->first_photo());
    }

    public function testSecondPhoto()
    {
        $this->assertInstanceOf(BelongsTo::class, (new Landing)->second_photo());
    }

    public function testSuggestionBlocks()
    {
        $this->assertInstanceOf(MorphToMany::class, (new Landing)->suggestion_blocks());
    }

    public function testGetChildFromSlugParentIdNotNull()
    {
        $slugChild = 'testing-child-slug';
        $landingParent = factory(Landing::class)->create(
            [
                'slug' => 'testing-parent-slug',
            ]
        );

        factory(Landing::class)->create(
            [
                'parent_id' => $landingParent->id,
                'slug' => $slugChild
            ]
        );

        $res = Landing::getChildFromSlug($slugChild);

        $this->assertNull($res);
    }
}