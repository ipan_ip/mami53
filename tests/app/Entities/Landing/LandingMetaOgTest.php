<?php

namespace App\Entities\Landing;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;

class LandingMetaOgTest extends MamiKosTestCase
{
    public function testActive_Success()
    {
        $meta = (new LandingMetaOg)->active();

        $this->assertInstanceOf(Builder::class, $meta);
    }
}