<?php

namespace App\Entities\SLA;

use App\Entities\Booking\BookingStatus;
use App\Entities\Booking\BookingUser;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class SLAVersionTest extends MamiKosTestCase
{
    public function testGetConfigRuleByDayLength()
    {
        $slaVersion = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE
        ]);
        $expected = factory(SLAConfiguration::class)->create([
            'sla_version_id' => $slaVersion->id,
            'case' => SLAConfiguration::CASE_EQUAL
        ]);

        $result = $slaVersion->getConfigRuleByDayLength('sla_3', 0);
        
        $this->assertTrue(
            $result->is($expected)
        );
    }

    public function testGetConfigRuleByDayLengthWithEqualAndSLA2(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00'
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_TWO]);
        $expected = factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL]);
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());
        $actual = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $daysToCheckIn);

        $this->assertTrue(
            $expected->is($actual)
        );
    }

    public function testGetConfigRuleByDayLengthWithEqualAndSLA3(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00'
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE]);
        $expected = factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL]);
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());
        $actual = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_THREE, $daysToCheckIn);

        $this->assertTrue(
            $expected->is($actual)
        );
    }

    public function testGetConfigRuleByDayLengthWithLessEqualTenAndSLA2(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-27',
            'created_at' => '2020-08-17 07:00:00'
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_TWO]);
        $expected = factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN]);
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());
        $actual = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $daysToCheckIn);

        $this->assertTrue(
            $expected->is($actual)
        );
    }

    public function testGetConfigRuleByDayLengthWithMoreTenAndSLA2(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-28',
            'created_at' => '2020-08-17 07:00:00'
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_TWO]);
        $expected = factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_MORE_TEN]);
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());
        $actual = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $daysToCheckIn);

        $this->assertTrue(
            $expected->is($actual)
        );
    }

    public function testGetConfigRuleByDayLengthWithMoreEqualOneAndSLA3(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-18',
            'created_at' => '2020-08-17 07:00:00'
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE]);
        $expected = factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_MORE_EQUAL_ONE]);
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());
        $actual = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_THREE, $daysToCheckIn);

        $this->assertTrue(
            $expected->is($actual)
        );
    }

    public function testGetDueDateWithBookedStatusShouldReturnDueDate(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00',
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_TWO]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->assertEquals('2020-08-17 08:00:00', $sla->getDueDate($booking));
    }

    public function testGetDueDateWithConfirmedStatusAndBookingStatus(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED
        ]);

        factory(BookingStatus::class)->create([
            'booking_user_id' => $booking->id,
            'created_at' => '2020-08-17 09:00:00',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->assertEquals('2020-08-17 10:00:00', $sla->getDueDate($booking));
    }

    public function testGetDueDateWithConfirmedStatusAndNoBookingStatus(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00',
            'updated_at' => '2020-08-17 09:00:00',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->assertEquals('2020-08-17 10:00:00', $sla->getDueDate($booking));
    }

    public function testGetDueDateWithOtherStatus(): void
    {
        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'created_at' => '2020-08-17 07:00:00',
            'updated_at' => '2020-08-17 09:00:00',
            'status' => BookingUser::BOOKING_STATUS_CANCELLED
        ]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->assertNull($sla->getDueDate($booking));
    }
}