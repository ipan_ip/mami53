<?php

namespace App\Entities\Feeds;

use App\Entities\Level\KostLevel;
use App\Test\MamiKosTestCase;
use Mockery;

class FeedsTest extends MamiKosTestCase
{
    /**
     * @runTestsInSeparateProcesses
     * @preserveGlobalState disabled
     */
    public function testGetAllGoldplusKeysDBSuccess()
    {
        $expected = ['goldplusKey' => 'goldplusId'];
        $mockKostLevel = Mockery::mock('alias:App\Entities\Level\KostLevel');
        $mockKostLevel->shouldReceive('getGpLevelKeyIds')->andReturn($expected);

        $res = Feeds::getAllGoldplusKeys();

        $this->assertSame(array_keys($expected), $res);
    }

    /**
     * @runTestsInSeparateProcesses
     * @preserveGlobalState disabled
     */
    public function testGetAllGoldplusKeysDBFailed()
    {
        $expected = [
            'goldplus1',
            'goldplus2',
            'goldplus3',
            'goldplus4',
            'goldplus1_promo',
            'goldplus2_promo',
            'goldplus3_promo'
        ];

        $mockKostLevel = Mockery::mock('alias:App\Entities\Level\KostLevel');
        $mockKostLevel->shouldReceive('getGpLevelKeyIds')->andReturn([]);

        $res = Feeds::getAllGoldplusKeys();

        $this->assertSame($expected, $res);
    }
}