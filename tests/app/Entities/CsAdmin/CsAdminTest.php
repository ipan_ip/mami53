<?php

namespace App\Entities\CsAdmin;

use Cache;
use Bugsnag;
use Mockery;

use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\SoftDeletes;

class CsAdminTest extends MamiKosTestCase
{
    public function testUserRelationship()
    {
        $csAdmin = factory(CsAdmin::class)->create(['user_id' => factory(User::class)]);

        $this->assertInstanceOf(User::class, $csAdmin->user);
    }

    public function testSoftDeletion(): void
    {
        $csAdmin = factory(CsAdmin::class)->create();
        $csAdmin->delete();

        $this->assertSoftDeleted('cs_admin', [
            'id' => $csAdmin->id,
        ]);
    }

    public function testRegister()
    {
        $params = factory(CsAdmin::class)->make()->toArray();

        $csAdmin = CsAdmin::register($params);

        $this->assertInstanceOf(CsAdmin::class, $csAdmin);
    }

    public function testRegisterShouldHandleException()
    {
        $params = [];

        Mockery::mock('alias:Bugsnag\BugsnagLaravel\Facades\Bugsnag')
            ->shouldReceive('notifyException')
            ->once();

        CsAdmin::register($params);
    }

    public function testGetRoomChatUrlShouldReturnCorrectValue()
    {
        $user = factory(User::class)->create([
            'id' => 1111,
            'chat_access_token' => 'abcdEFGH1234'
        ]);

        $csAdmin = factory(CsAdmin::class)->create(['user_id' => $user->id]);

        $actual = $csAdmin->getRoomChatUrl();
        $expected = config('sendbird.chat_room_url') 
            . '?userid=1111&access_token=abcdEFGH1234';

        $this->assertEquals($expected, $actual);
    }

    public function testGetRoomChatUrlWithNullChatAccessTokenShouldReturnNull()
    {
        $user = factory(User::class)->create([
            'id' => 1111,
            'chat_access_token' => null
        ]);

        $csAdmin = factory(CsAdmin::class)->create(['user_id' => $user->id]);

        $roomChatUrl = $csAdmin->getRoomChatUrl();

        $this->assertNull($roomChatUrl);
    }

    public function testGetRoomChatUrlWithoutUserShouldReturnNull()
    {
        $csAdmin = factory(CsAdmin::class)->create();

        $roomChatUrl = $csAdmin->getRoomChatUrl();

        $this->assertNull($roomChatUrl);
    }
}