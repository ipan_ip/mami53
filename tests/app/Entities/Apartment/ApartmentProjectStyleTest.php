<?php

namespace App\Entities\Apartment;

use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;

class ApartmentProjectStyleTest extends MamiKosTestCase
{
    public function testApartmentProject(): void
    {
        $project = factory(ApartmentProject::class)->create();
        $style = factory(ApartmentProjectStyle::class)->create(['apartment_project_id' => $project->id]);

        $this->assertTrue($style->apartment_project->is($project));
    }

    public function testPhoto(): void
    {
        $photo = factory(Media::class)->create();
        $style = factory(ApartmentProjectStyle::class)->create(['photo_id' => $photo->id]);

        $this->assertTrue($style->photo->is($photo));
    }

    public function testGetPhotoUrlAttribute(): void
    {
        $photo = factory(Media::class)->create();
        $style = factory(ApartmentProjectStyle::class)->create(['photo_id' => $photo->id]);

        $this->assertEquals($photo->getMediaUrl(), $style->getPhotoUrlAttribute());
    }

    public function testListPhotoInsideForeach()
    {
        $photo = factory(Media::class)->create();
        $project = factory(ApartmentProject::class)->create();
        factory(ApartmentProjectStyle::class, 2)
            ->create([
                'apartment_project_id' => $project->id,
                'photo_id' => $photo->id,
                'title' => 'Apartment cover'
            ]);

        $this->assertIsArray((new ApartmentProjectStyle)->listPhoto($project));
    }

    public function testGetPhotoUrlAttributeNull()
    {
        $project = factory(ApartmentProject::class)->create();
        factory(ApartmentProjectStyle::class, 2)
            ->create([
                'apartment_project_id' => $project->id,
                'title' => 'Apartment cover'
            ]);
        $res = (new ApartmentProjectStyle)->listPhoto($project);

        $this->assertIsArray($res);
        $this->assertEmpty(reset($res)['photo_url']['real']);
    }
}
