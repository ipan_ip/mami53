<?php
namespace App\Entities\Apartment;

use App\Test\MamiKosTestCase;

class ApartmentProjectClusterTest extends MamiKosTestCase
{
    public function testGetRangeSingleCluster()
    {
        $res = ApartmentProjectCluster::getRangeSingleCluster([
            [-6.94, 100.44],
            [-6.88, 100.344]
        ],
        ['latitude' => -7.797068, 'longitude' => 110.370529],
        10);

        $this->assertEquals(110.366, round($res[0][0], 3));
        $this->assertEquals(-7.79, round($res[0][1], 3));
        $this->assertEquals(110.372, round($res[1][0], 3));
        $this->assertEquals(-7.8, round($res[1][1], 3));
    }
}