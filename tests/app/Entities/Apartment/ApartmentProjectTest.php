<?php
namespace App\Entities\Apartment;

use App\Test\MamiKosTestCase;

class ApartmentProjectTest extends MamiKosTestCase
{     
    protected $aptProject;

    public function setUp() : void
    {
        parent::setUp();
        $this->aptProject = factory(ApartmentProject::class)->make();
    }

    public function testConstants()
    {
        $this->assertGreaterThanOrEqual(5, count(ApartmentProject::UNIT_TYPE));
        $this->assertGreaterThanOrEqual(4, count(ApartmentProject::UNIT_TYPE_ROOMS));
        foreach(ApartmentProject::UNIT_TYPE_ROOMS as $roomType) {
            $this->assertArrayHasKey('bedroom', $roomType);
            $this->assertArrayHasKey('bathroom', $roomType);
        }
        $this->assertGreaterThanOrEqual(3, count(ApartmentProject::UNIT_CONDITION));
    }

    public function testTags()
    {
        $relation = $this->aptProject->tags();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsToMany', $relation);
        $this->assertInstanceOf('App\Entities\Room\Element\Tag', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignPivotKeyName());
    }

    public function testRooms()
    {
        $relation = $this->aptProject->rooms();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKeyName());
    }

    public function testStyles()
    {
        $relation = $this->aptProject->styles();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProjectStyle', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKeyName());
    }

    public function testTowers()
    {
        $relation = $this->aptProject->towers();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProjectTower', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKeyName());
    }

    public function testTypes()
    {
        $relation = $this->aptProject->types();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProjectType', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKeyName());
    }

    public function testSlugHistory()
    {
        $relation = $this->aptProject->slug_history();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProjectSlug', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKeyName());
    }

    public function testRedirect()
    {
        $relation = $this->aptProject->redirect();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProject', $relation->getRelated());
        $this->assertEquals('redirect_id', $relation->getForeignKey());
    }

    public function testGenerateApartmentCode()
    {
        $aptProject = factory(ApartmentProject::class)->make([
            'name' => 'Menara Kebaikan',
            'project_code' => null
            ]);
        $aptProject->generateApartmentCode();
        $this->assertEquals('MK', substr($aptProject->project_code, 0, 2));
    }

    public function testSluggable()
    {
        $slugable = $this->aptProject->sluggable();
        $this->assertArrayHasKey('slug', $slugable);
        $slug = $slugable['slug'];
        $this->assertArrayHasKey('source', $slug);
        $this->assertArrayHasKey('method', $slug);
        $this->assertEquals('name_slug', $slug['source']);
        $this->assertInstanceOf('Closure', $slug['method']);
    }

    public function testGetNameSlugAttribute()
    {
        $this->aptProject->name = "Menara *kebaikan* 19;)";
        $this->assertEquals("menara kebaikan 19", $this->aptProject->getNameSlugAttribute());
    }

    public function testGetShareUrlAttribute()
    {
        $this->aptProject->area_city = "Garis Hidup 19";
        $this->assertEquals("garis-hidup-", $this->aptProject->getCitySlugAttribute());
    }
}