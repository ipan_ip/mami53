<?php
namespace App\Entities\Apartment;

use App\Test\MamiKosTestCase;

class ApartmentProjectSlugTest extends MamiKosTestCase
{
    public function testApartmentProject()
    {
        $relation = (new ApartmentProjectSlug)->apartment_project();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Apartment\ApartmentProject', $relation->getRelated());
        $this->assertEquals('apartment_project_id', $relation->getForeignKey());
    }
}