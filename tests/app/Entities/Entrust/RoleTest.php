<?php

namespace App\Entities\Entrust;

use App\Test\MamiKosTestCase;
use App\User;

class RoleTest extends MamiKosTestCase
{
    public function testGetUserCountAttribute(): void
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create();

        $user->attachRole($role);

        $this->assertEquals(1, $role->user_count);
    }

    public function testGetPermissionCountAttribute(): void
    {
        $role = factory(Role::class)->create();
        $permission = factory(Permission::class)->create();

        $role->attachPermission($permission);

        $this->assertEquals(1, $role->permission_count);
    }

    public function testSearchByUsernameOrEmail(): void
    {
        $user = factory(User::class)->create(['name' => 'John Doe']);
        $role = factory(Role::class)->create();

        $user->attachRole($role);

        $actual = Role::searchByUsernameOrEmail('John Doe')->first();

        $this->assertTrue(
            $actual->is($role)
        );
    }

    public function testSearchByUsernameOrEmailWithEmptyParams(): void
    {
        $role = factory(Role::class)->create();

        $actual = Role::searchByUsernameOrEmail(null)->first();

        $this->assertTrue(
            $actual->is($role)
        );
    }

    public function testSearchByPermission(): void
    {
        $role = factory(Role::class)->create();
        $permission = factory(Permission::class)->create();

        $role->attachPermission($permission);

        $actual = Role::searchByPermission($permission->name)->first();

        $this->assertTrue(
            $actual->is($role)
        );
    }

    public function testSearchByPermissionWithEmptyParam(): void
    {
        $role = factory(Role::class)->create();

        $actual = Role::searchByPermission(null)->first();

        $this->assertTrue(
            $actual->is($role)
        );
    }
}
