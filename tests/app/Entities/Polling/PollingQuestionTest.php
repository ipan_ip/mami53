<?php

namespace App\Entities\Polling;

use App\Test\MamiKosTestCase;

class PollingQuestionTest extends MamiKosTestCase
{
    public function testGetAsDropdown()
    {
        factory(PollingQuestion::class, 3)->create();
        $result = PollingQuestion::getAsDropdown();

        $this->assertEquals(3, count($result));
    }
}
