<?php
namespace App\Entities\Event;
use App\Entities\Media\Media;
use App\Entities\Notif\Category;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class EventModelTest extends MamiKosTestCase
{
    public function setUp(): void 
    {
        parent::setUp();

        $mockDate = Carbon::create(2020, 1, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    public function testGetEventCount()
    {
        factory(EventModel::class, 2)->create();
        $nEvents = EventModel::getEventCount();

        $this->assertEquals(2, $nEvents);
    }

    public function testPhotoRelation()
    {
        $media = factory(Media::class)->create();
        $banner = factory(EventModel::class)->create([
            'photo_id' => $media->id,
        ]);

        $this->assertInstanceOf(Media::class, $banner->photo);
        $this->assertEquals($media->id, $banner->photo->id);
    }

    public function testCategoryRelation()
    {
        $category = factory(Category::class)->create([
            'name' => 'Event Category'
        ]);

        $banner = factory(EventModel::class)->create([
            'category_id' => $category->id
        ]);

        $this->assertInstanceOf(Category::class, $banner->category);
        $this->assertEquals($category->id, $banner->category->id);
    }

    public function testGetEventBanner()
    {
        factory(EventModel::class, 2)->create([
            'is_owner' => '1',
            'is_published' => '1',
        ]);

        $attributes = [
            'is_owner' => '1',
            'is_published' => '1',
        ];

        $result = EventModel::getEventBanner(10, $attributes);
        $this->assertEquals(2, $result->count());
    }

    public function testGetEventBannerWithQuery()
    {
        $banner = factory(EventModel::class)->create([
            'title' => 'mamikos promo',
            'is_owner' => '1',
            'is_published' => '1',
        ]);

        factory(EventModel::class)->create([
            'title' => 'random title',
            'is_owner' => '1',
            'is_published' => '1',
        ]);

        $attributes = [
            'title' => 'mamikos promo',
            'is_owner' => '1',
            'is_published' => '1',
        ];

        $result = EventModel::getEventBanner(10, $attributes);

        $this->assertEquals(1, $result->count());
        $this->assertEquals($banner->id, $result[0]->id);
    }

    public function testShowPhotos()
    {
        $media = factory(Media::class)->create();
        $banner = factory(EventModel::class)->create([
            'photo_id' => $media->id,
        ]);

        $result = EventModel::showPhotos($banner);

        $this->assertEquals($media->id, $result['id']);
        $this->assertNotEmpty($result['photo_url']);
    }

}