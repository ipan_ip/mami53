<?php

namespace App\Entities\Document;

use App\Test\MamiKosTestCase;

class DocumentTest extends MamiKosTestCase
{
    public function testStore(): void
    {
        Document::store([
            'user_id' => 5,
            'file_name' => 'file_name',
            'type' => 'type',
            'format' => 'format'
        ]);

        $this->assertDatabaseHas(
            'document',
            [
                'user_id' => 5,
                'file_name' => 'file_name',
                'file_path' => Document::FILE_PATH,
                'type' => 'type',
                'format' => 'format'
            ]
        );
    }
}
