<?php
namespace App\Entities\Media;

use Illuminate\Http\UploadedFile;

use App\Entities\Media\MediaHandler;
use App\Test\MamiKosTestCase;

use Mockery;
use Config;

use function array_slice;

class MediaHandlerTest extends MamiKosTestCase
{
    protected $pngFile;
    protected $jpegFile;
    protected $jpgFile;
    protected $gifFile;

    public const RANDOM_UPLOAD_TYPE = 'scrap';

    protected function setUp() : void
    {
        $this->pngFile  = UploadedFile::fake()->image('testFile.png', '1024', '768');
        $this->jpegFile = UploadedFile::fake()->image('testFile.jpeg', '1024', '768');
        $this->jpgFile  = UploadedFile::fake()->image('testFile.jpg', '1024', '768');
        $this->gifFile  = UploadedFile::fake()->image('testFile.gif', '60', '30');

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testGetFileExtension_UploadImage_ResultEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile);
        $this->assertEquals('png', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile);
        $this->assertEquals('jpeg', $jpegFileExt);

        $jpgFileExt = MediaHandler::getFileExtension($this->jpgFile);
        $this->assertEquals('jpg', $jpgFileExt);
    }

    public function testGetFileExtension_UploadImage_ResultNotEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile);
        $this->assertNotEquals('jpg', $pngFileExt);
        $this->assertNotEquals('jpeg', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile);
        $this->assertNotEquals('jpg', $jpegFileExt);
        $this->assertNotEquals('png', $jpegFileExt);

        $jpgFileExt = MediaHandler::getFileExtension($this->jpgFile);
        $this->assertNotEquals('png', $jpgFileExt);
        $this->assertNotEquals('jpeg', $jpgFileExt);
    }

    public function testGetFileExtension_UploadImage_UploadTypeUrl_ResultEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile, 'url');
        $this->assertEquals('jpg', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile, 'url');
        $this->assertEquals('jpg', $jpegFileExt);

        $jpgFileExt = MediaHandler::getFileExtension($this->jpgFile, 'url');
        $this->assertEquals('jpg', $jpgFileExt);

        $gifFileExt = MediaHandler::getFileExtension($this->gifFile, 'url');
        $this->assertEquals('jpg', $gifFileExt);
    }

    public function testGetFileExtension_UploadImage_UploadTypeUrl_ResultNotEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile, 'url');
        $this->assertNotEquals('png', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile, 'url');
        $this->assertNotEquals('jpeg', $jpegFileExt);

        $gifFileExt = MediaHandler::getFileExtension($this->gifFile, 'url');
        $this->assertNotEquals('gif', $gifFileExt);
    }

    public function testGetFileExtension_UploadImage_UploadTypeRandom_ResultEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertEquals('jpg', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertEquals('jpg', $jpegFileExt);

        $jpgFileExt = MediaHandler::getFileExtension($this->jpgFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertEquals('jpg', $jpgFileExt);

        $gifFileExt = MediaHandler::getFileExtension($this->gifFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertEquals('jpg', $gifFileExt);
    }

    public function testGetFileExtension_UploadImage_UploadTypeRandom_ResultNotEqual()
    {
        $pngFileExt = MediaHandler::getFileExtension($this->pngFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertNotEquals('gif', $pngFileExt);

        $jpegFileExt = MediaHandler::getFileExtension($this->jpegFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertNotEquals('gif', $jpegFileExt);

        $jpgFileExt = MediaHandler::getFileExtension($this->jpgFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertNotEquals('gif', $jpgFileExt);

        $gifFileExt = MediaHandler::getFileExtension($this->gifFile, self::RANDOM_UPLOAD_TYPE);
        $this->assertNotEquals('gif', $gifFileExt);
    }

    public function testGetUrlMimeType_Jpg()
    {
        //random image url for jpg type from our server to reduce risk error/exception
        $url = Config::get('api.media.cdn_url').'/uploads/cache/data/event/2020-03-16/jVkzBcx8-540x720.jpg';

        //mocking static method
        $mediaHandlerMock = Mockery::mock('alias:MediaHandler');
        $mediaHandlerMock->shouldReceive('GetUrlMimeType')
            ->once()
            ->with($url)
            ->andReturn('jpg');

        $this->assertStringContainsString(
            MediaHandler::getFileExtension($url, 'url'), 
            $mediaHandlerMock->GetUrlMimeType($url)
        );
    }

    public function testGetUrlMimeType_Svg()
    {
        //random image url for svg type from our server to reduce risk error/exception
        $url = 'https://mamikos.com/general/img/pictures/placeholder/placeholder_loading.svg';

        //mocking static method
        $mediaHandlerMock = Mockery::mock('alias:MediaHandler');
        $mediaHandlerMock->shouldReceive('GetUrlMimeType')
            ->once()
            ->with($url)
            ->andReturn('svg+xml');

        $this->assertStringContainsString(
            MediaHandler::getFileExtension($url, 'url'), 
            $mediaHandlerMock->GetUrlMimeType($url)
        );
    }

    public function testGetUrlMimeType_RequestException()
    {
        //random broken url to test Exception
        $url = 'ht//upload.wikimedia.org/wikipedia/commons/2/2c/Rotating_earth_%28large%29.gif';

        $this->assertStringContainsString(
            MediaHandler::getFileExtension($url, 'url'),
            'jpg'
        );
    }

    public function testGetFilenamePartWithNameAsPart()
    {
       $n = 1;
       $pieces = explode('-', 'vBFPc4M0.jpg');

       $part1 = implode('-', array_slice($pieces, 0, $n));

       $response = MediaHandler::getFilenamePart('vBFPc4M0.jpg', 'name');

       $this->assertEquals($part1. '-re', $response);
    }

    public function testGetFilenamePartWithNotNameAsPartAndPieceOnlyHasNoIndex1()
    {
        $n = 1;
        $pieces = explode('-', 'vBFPc4M0.jpg');

        $part1 = implode('-', array_slice($pieces, 0, $n));

        $response = MediaHandler::getFilenamePart('vBFPc4M0.jpg', 'test');

        $this->assertNotEquals($part1. '-re', $response);
        $this->assertEquals('-', $response);
    }

    public function testGetFilenamePartWithNotNameAsPartAndPieceHasIndex1()
    {
        $n = 1;
        $pieces = explode('-', 'vBFPc4M0.jpg-re');

        $part1 = implode('-', array_slice($pieces, 0, $n));

        $response = MediaHandler::getFilenamePart('vBFPc4M0.jpg-re', 'test');

        $this->assertNotEquals($part1. '-re', $response);
        $this->assertEquals('-'. $pieces[$n], $response);
    }

    public function testStripExtension()
    {
        $jpg = str_replace('.jpg', '', $this->jpgFile);
        $response = MediaHandler::stripExtension($jpg);
        $this->assertEquals($jpg, $response);

        $png = str_replace('.png', '', $this->pngFile);
        $response = MediaHandler::stripExtension($png);
        $this->assertEquals($png, $response);

        $jpeg = str_replace('.jpeg', '', $this->jpegFile);
        $response = MediaHandler::stripExtension($jpeg);
        $this->assertEquals($jpeg, $response);

        $gif = str_replace('.gif', '', $this->gifFile);
        $response = MediaHandler::stripExtension($gif);
        $this->assertEquals($gif, $response);

    }

    public function testGetTagIconDirectory()
    {
        $this->assertEquals(Config::get('api.media.folder_tag_icon'),
                            MediaHandler::getTagIconDirectory());
    }

    public function testGetOriginalFileDirectoryForMediaTypeUser()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/user/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::USER_PHOTO_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeReview()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/review/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::REVIEW_PHOTO_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeLanding()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/landing/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::CONTENT_DOWNLOAD_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeVoucherRecipient()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/voucher_recipient/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::VOUCHER_RECIPIENT_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForPhotobooth()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/photobooth/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::PHOTOBOOTH_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeCompany()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/company_logo/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::COMPANY_PHOTO_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeEvent()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/event/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::EVENT_PHOTO_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeSlide()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/slide/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::SLIDE_PHOTO_TYPE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaSanjuniperoHeaderDesktop()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/sanjunipero_header_desktop/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::SANJUNIPERO_HEADER_DESKTOP);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaSanjuniperoHeaderMobile()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/sanjunipero_header_mobile/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::SANJUNIPERO_HEADER_MOBILE);

        $this->assertEquals($directory, $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeDefault()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/style/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory('testing');

        $this->assertEquals($directory, $response);
    }

    public function testGetCacheFileDirectory()
    {
        $folderDataConfig  = Config::get('api.media.folder_data');
        $folderCacheConfig = Config::get('api.media.folder_cache');

        $cacheDir = str_replace($folderDataConfig, $folderCacheConfig, 'uploads/data/style/2020-04-15');

        $response = MediaHandler::getCacheFileDiretory('uploads/data/style/2020-04-15');

        $this->assertEquals($cacheDir, $response);
    }

    public function testGenerateFileName()
    {
        $nameLength = Config::get('api.media.random_name_length');

        $fileName = sprintf("%s.%s", str_random($nameLength), 'jpg');

        $response = MediaHandler::generateFilename('jpg');

        $this->assertStringEndsWith('jpg', $fileName);
        $this->assertStringEndsWith('jpg', $response);
        $this->assertStringMatchesFormat('%s.%s', $fileName);
        $this->assertStringMatchesFormat('%s.%s', $response);
    }

    public function testGetOriginalFileDirectoryForMediaTypeSinggahsiniRegistration()
    {
        $base = Config::get('api.media.folder_data');

        $folderDate = date('Y-m-d');

        $directory = "{$base}/".Media::SINGGAHSINI_REGISTRATION."/{$folderDate}";

        $response = MediaHandler::getOriginalFileDirectory(Media::SINGGAHSINI_REGISTRATION);

        $this->assertEquals($directory, $response);
    }

}
