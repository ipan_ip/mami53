<?php

namespace Test\App\Entities\Media;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Media\Media;
use Config;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage as FacadesStorage;
use Schema;
use Storage;

class MediaTest extends MamiKosTestCase
{
    
    protected $user;
    protected $model;
    protected $postOptions;

    public const TEST_IMAGE = 'testImage.jpg';
    public const PARAM_UPLOAD_TYPE = 'upload';
    public const PARAM_UPLOAD_IDENTIFIER = '127.0.0.1';
    public const PARAM_UPLOAD_IDENTIFIER_TYPE = 'admin_ip_address';
    public const PARAM_MEDIA_IDENTIFIER = null;
    public const PARAM_MEDIA_TYPE = 'jpg';
    public const PARAM_WATERMARKING = false;
    public const PARAM_EFFECTS = null;
    public const PARAM_SAVE_ORIGINAL_FILE = true;

    protected function setUp() : void
    {
        parent::setUp();

        Storage::fake('testing');

        $this->fileName = 'mediatest-'.time().'-'.rand(1,1000);
        $this->model = factory(Media::class)->create([
            'file_name' => $this->fileName.'.jpg',
            'file_path' => 'uploads/data/style/2020-06-25'
        ]);

        $this->postOptions = [
            'original_extension' => 'jpg',
            'custom_file_name' => 'testImage',
            'large_photo_size' => 'large',
            'watermarking_type' => 'premium',
            'small_watermark' => true,
            'keep_png' => false,
            'from' => 'agent',
            'room_type' => 'kos',
        ];

        // setup config
        Config::set('api.media.cdn_url', 'https://d1pohsgm7db6t7.cloudfront.net/');
        Config::set('api.media.size.user', array(
          'small'  => '240x320',
          'medium' => '360x480',
          'large'  => '540x720',
        ));
        Storage::fake('s3', ['url' => 'https://my-s3-bucket.s3.ap-southeast-1.amazonaws.com/']);
    }

    public function testModelHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns(
                'media',
                [
                    'id',
                    'upload_identifier',
                    'upload_identifier_type',
                    'file_name',
                    'file_path',
                    'type',
                    'server_location',
                    'media_format',
                ]
            ),
            1
        );
    }

    public function testModelHasUserRelationship()
    {
        $this->user = factory(User::class)->create(
            [
                'photo_id' => $this->model->id
            ]
        );

        $this->assertInstanceOf(User::class, $this->model->user);
    }

    /**
    * @link http://docs.mockery.io/en/latest/cookbook/mocking_hard_dependencies.html
    *
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testPostMediaWithoutOptions()
    {
        $this->setupMediaHandlerMock($this->getMediaHandlerDefMockValues());
        $file = UploadedFile::fake()->image('kos-image.png');
        $media = Media::postMedia($file);
        foreach(['id', 'url', 'file_name', 'file_path', 'server_location'] as $key) {
            $this->assertArrayHasKey($key, $media);
        }
    }

    /**
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testPostMediaFailGenerateThumbnail()
    {
        $methods = $this->getMediaHandlerDefMockValues();
        $methods['generateThumbnail'] = false;
        $this->setupMediaHandlerMock($methods);

        $file = UploadedFile::fake()->image('kos-image.png');
        $media = Media::postMedia($file);
        $this->assertEquals($media['id'], 0);
    }

    public function testRegenerateThumbnail()
    {
        // TODO: This method needs refactoring before this test case
        self::assertTrue(true);
    }

    public function testRemoveWaterMark()
    {
        // TODO: This method needs refactoring before this test case
        self::assertTrue(true);
    }

    public function testGetCachePath()
    {
        $result = Media::getCachePath($this->model, 640, 480);

        $this->assertEquals('uploads/cache/data/style/2020-06-25/'.$this->fileName.'-640x480.jpg', $result);
    }

    public function testGetCachePathWithMakedirOption()
    {
        $result = Media::getCachePath($this->model, 640, 480, true);

        $this->assertEquals('uploads/cache/data/style/2020-06-25/'.$this->fileName.'-640x480.jpg', $result);
    }

    public function testGetCacheUrl()
    {
        $result = Media::getCacheUrl($this->model, 640, 480);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-640x480.jpg', $result);
    }

    public function testGetCacheUrlOnS3()
    {
        // to make sure that it returns the right url when config of s3_cdn_url is set to string.
        $defConf = Config::get('storage.s3_cdn_url');
        Config::set('api.media.cdn_url', 'https://my-cdn.mami.kos/');

        $result = Media::getCacheUrl($this->model);
        // assert with hard coded string to test if it really get the value from api.media.cdn_url.
        $this->assertEquals('https://my-cdn.mami.kos/uploads/cache/data/style/2020-06-25/'.$this->fileName.'.jpg', $result);

        Config::set('storage.s3_cdn_url', $defConf);
    }

    public function testGetCacheUrlWithDimensionOnS3()
    {
        $result = Media::getCacheUrl($this->model, 540, 720);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-540x720.jpg', $result);

    }

    public function testStripExtension()
    {
        $expected = 'testImage';

        $jpgFileName = self::TEST_IMAGE;
        $this->assertEquals($expected, Media::stripExtension($jpgFileName));

        $jpegFileName = 'testImage.jpeg';
        $this->assertEquals($expected, Media::stripExtension($jpegFileName));

        $pngFileName = 'testImage.png';
        $this->assertEquals($expected, Media::stripExtension($pngFileName));

        $gifFileName = 'testImage.gif';
        $this->assertEquals($expected, Media::stripExtension($gifFileName));
    }

    public function testGetMediaUrl()
    {
        $response = $this->model->getMediaUrl();

        $this->assertArrayHasKey('real', $response);
        $this->assertArrayHasKey('small', $response);
        $this->assertArrayHasKey('medium', $response);
        $this->assertArrayHasKey('large', $response);

        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/data/style/2020-06-25/'.$this->fileName.'.jpg', $response['real']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-240x320.jpg', $response['small']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-360x480.jpg', $response['medium']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-540x720.jpg', $response['large']);
    }

    public function testGetMediaUrlOnS3()
    {

        $response = $this->model->getMediaUrl();

        $this->assertArrayHasKey('real', $response);
        $this->assertArrayHasKey('small', $response);
        $this->assertArrayHasKey('medium', $response);
        $this->assertArrayHasKey('large', $response);

        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/data/style/2020-06-25/'.$this->fileName.'.jpg', $response['real']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-240x320.jpg', $response['small']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-360x480.jpg', $response['medium']);
        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/cache/data/style/2020-06-25/'.$this->fileName.'-540x720.jpg', $response['large']);

    }

    public function testGetUrl()
    {
        $result = Media::getUrl($this->model);

        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/data/style/2020-06-25/'.$this->fileName.'.jpg', $result);
    }

    public function testGetUrlOnS3()
    {

        $result = Media::getUrl($this->model);

        $this->assertEquals(Config::get('api.media.cdn_url').'uploads/data/style/2020-06-25/'.$this->fileName.'.jpg', $result);

    }

    public function testSizeByType()
    {
        $this->assertEquals(
            Config::get('api.media.size.user'),
            Media::sizeByType(Media::USER_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.style'),
            Media::sizeByType(Media::STYLE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.round_style'),
            Media::sizeByType(Media::ROUND_STYLE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.review'),
            Media::sizeByType(Media::REVIEW_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.photobooth'),
            Media::sizeByType(Media::PHOTOBOOTH_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.company'),
            Media::sizeByType(Media::COMPANY_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.event'),
            Media::sizeByType(Media::EVENT_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.flash_sale'),
            Media::sizeByType(Media::FLASH_SALE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.sanjunipero_header_desktop'),
            Media::sizeByType(Media::SANJUNIPERO_HEADER_DESKTOP)
        );

        $this->assertEquals(
            Config::get('api.media.size.sanjunipero_header_mobile'),
            Media::sizeByType(Media::SANJUNIPERO_HEADER_MOBILE)
        );

        $this->assertEquals(
            Config::get('api.media.size.style'),
            Media::sizeByType('default')
        );
    }

    public function testRealSizeByType()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.user'),
            Media::realSizeByType(Media::USER_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.style'),
            Media::realSizeByType(Media::STYLE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.round_style'),
            Media::realSizeByType(Media::ROUND_STYLE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.review'),
            Media::realSizeByType(Media::REVIEW_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.photobooth'),
            Media::realSizeByType(Media::PHOTOBOOTH_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.company'),
            Media::realSizeByType(Media::COMPANY_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.event'),
            Media::realSizeByType(Media::EVENT_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.flash_sale'),
            Media::sizeByType(Media::FLASH_SALE_PHOTO_TYPE)
        );

        $this->assertEquals(
            Config::get('api.media.size.sanjunipero_header_desktop'),
            Media::realSizeByType(Media::SANJUNIPERO_HEADER_DESKTOP)
        );

        $this->assertEquals(
            Config::get('api.media.size.sanjunipero_header_mobile'),
            Media::realSizeByType(Media::SANJUNIPERO_HEADER_MOBILE)
        );

        $this->assertEquals(
            Config::get('api.media.real_size.style'),
            Media::realSizeByType('default')
        );
    }

    public function testDeleteFileShouldDeleteFromStorage(): void
    {
        $file = UploadedFile::fake()->image('test.jpg');

        $base = Config::get('api.media.folder_data');
        $folderType = 'consultant';
        $folderDate = date('Y-m-d');
        $location = "{$base}/{$folderType}/{$folderDate}";

        $file->store($location);
        $media = factory(Media::class)->create([
            'file_path' => $location,
            'file_name' => $file->hashName()
        ]);

        $media->deleteFile();
        FacadesStorage::assertMissing($location . '/' . $file->hashName());
    }

    /**
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testPostTagIcon()
    {
        $this->setupMediaHandlerMock($this->getMediaHandlerDefMockValues());
        $file = UploadedFile::fake()->image('testImage.jpg', 640, 480)->size(50);
        $result = Media::postTagIcon($file);

        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('url', $result);
        $this->assertArrayHasKey('file_name', $result);
        $this->assertArrayHasKey('file_path', $result);
        $this->assertArrayHasKey('server_location', $result);
    }

    private function getMediaHandlerDefMockValues()
    {
        return [
            'generateThumbnail' => true,
            'getFileExtension' => 'png',
            'generateFileName' => 'my-awesome-filename',
            'getOriginalFileDirectory' => 'style',
            'getCacheFileDiretory' => '/uploads/cache/data',
            'stripExtension' => 'my-awesome-filename',
            'storeFile' => true,
            'getTagIconDirectory' => 'uploads/tags'
        ];
    }

    private function setupMediaHandlerMock($methods)
    {
        // setup
        $mock = \Mockery::mock('alias:App\Entities\Media\MediaHandler');
        foreach($methods as $key => $value) {
            $mock->shouldReceive($key)->andReturn($value);
        }

        $this->mock = $mock;
    }
}
