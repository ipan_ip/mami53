<?php

namespace App\Entities\Fake;


use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Fake\Faker;
use App\Entities\Fake\FakeInfo;
use App\Entities\Room\Room;

class FakerTest extends MamiKosTestCase
{
    
    public function testGetIsFaker_True()
    {
        $user = factory(User::class)->create();

        factory(Faker::class)->create([
            'user_id' => $user->id,
            'comment' => "{$user->name} ({$user->email})",
        ]);

        $this->assertTrue(Faker::GetIsFaker($user->id));
    }

    public function testGetIsFaker_False()
    {
        $user = factory(User::class)->create();
        $this->assertFalse(Faker::GetIsFaker($user->id));
    }

    public function testTryGetFakeOwnerPhoneNumber_Success()
    {
        $user = factory(User::class)->create();
        $designer = factory(Room::class)->create();

        // register user as faker
        factory(Faker::class)->create([
            'user_id' => $user->id,
            'comment' => "{$user->name} ({$user->email})",
        ]);

        for ($i = 0; $i < 3; $i++)
        {
            factory(FakeInfo::class)->create();
        }

        $phoneNumber = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);

        $this->assertNotEmpty($phoneNumber);

        $found = FakeInfo::where('info', $phoneNumber);

        $this->assertNotNull($found);
    }

    public function testTryGetFakeOwnerPhoneNumber_Should_Return_Cached_Result()
    {
        $user = factory(User::class)->create();
        $designer = factory(Room::class)->create();

        // register user as faker
        factory(Faker::class)->create([
            'user_id' => $user->id,
            'comment' => "{$user->name} ({$user->email})",
        ]);

        for ($i = 0; $i < 100; $i++)
        {
            factory(FakeInfo::class)->create();
        }

        $phoneNumber = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);
        for ($i = 0; $i < 10; $i++)
        {
            $reference = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);
            $this->assertEquals($phoneNumber, $reference);
            $result[] = $reference;
        }
    }

    public function testTryGetFakeOwnerPhoneNumber_Fail_When_No_FakeInfo()
    {
        $user = factory(User::class)->create();
        $designer = factory(Room::class)->create();

        // register user as faker
        factory(Faker::class)->create([
            'user_id' => $user->id,
            'comment' => "{$user->name} ({$user->email})",
        ]);

        $phoneNumber = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);

        $this->assertEmpty($phoneNumber);
    }

    public function testTryGetFakeOwnerPhoneNumber_Fail_When_No_Faker()
    {
        $user = factory(User::class)->create();
        $designer = factory(Room::class)->create();

        for ($i = 0; $i < 3; $i++)
        {
            factory(FakeInfo::class)->create();
        }

        $phoneNumber = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);

        $this->assertEmpty($phoneNumber);
    }

    public function testTryGetFakeOwnerPhoneNumber_Fail_When_User_Is_Not_Faker()
    {
        $user = factory(User::class)->create();
        $designer = factory(Room::class)->create();

        // register user as faker
        factory(Faker::class)->create([
            'user_id' => $user->id + 1,
            'comment' => "{$user->name} ({$user->email})",
        ]);

        $phoneNumber = Faker::TryGetFakeOwnerPhoneNumber($user->id, $designer->id);

        $this->assertEmpty($phoneNumber);
    }
}