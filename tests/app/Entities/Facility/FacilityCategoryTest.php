<?php

namespace App\Entities\Facility;

use App\Entities\Room\Element\Tag;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomTypeFacility;
use App\Test\MamiKosTestCase;
use App\User;

class FacilityCategoryTest extends MamiKosTestCase
{
    public function testTypes(): void
    {
        $category = factory(FacilityCategory::class)->create();
        $type = factory(FacilityType::class)->create([
            'facility_category_id' => $category->id
        ]);

        $this->assertTrue(
            $category->types[0]->is($type)
        );
    }

    public function testCreator(): void
    {
        $user = factory(User::class)->create();
        $category = factory(FacilityCategory::class)->create(['creator_id' => $user->id]);

        $this->assertTrue($category->creator->is($user));
    }

    public function testTags(): void
    {
        $category = factory(FacilityCategory::class)->create();
        $type = factory(FacilityType::class)->create([
            'facility_category_id' => $category->id
        ]);
        $tag = factory(Tag::class)->create(['facility_type_id' => $type->id]);

        $this->assertTrue($category->tags[0]->is($tag));
    }

    public function testRoomFacilities(): void
    {
        $category = factory(FacilityCategory::class)->create();
        $type = factory(FacilityType::class)->create([
            'facility_category_id' => $category->id
        ]);
        $tag = factory(Tag::class)->create(['facility_type_id' => $type->id]);
        $facility = factory(RoomFacility::class)->create(['tag_id' => $tag->id]);

        $this->assertTrue($category->room_facilities[0]->is($facility));
    }

    public function testRoomTypeFacilities(): void
    {
        $category = factory(FacilityCategory::class)->create();
        $type = factory(FacilityType::class)->create([
            'facility_category_id' => $category->id
        ]);
        $tag = factory(Tag::class)->create(['facility_type_id' => $type->id]);
        $facility = factory(RoomTypeFacility::class)->create(['tag_id' => $tag->id]);

        $this->assertTrue($category->room_type_facilities[0]->is($facility));
    }
}
