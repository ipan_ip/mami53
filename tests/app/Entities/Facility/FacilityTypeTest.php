<?php

namespace App\Entities\Facility;

use App\Entities\Room\Element\Tag;
use App\Test\MamiKosTestCase;
use App\User;

class FacilityTypeTest extends MamiKosTestCase
{
    public function testFacilityCategory(): void
    {
        $category = factory(FacilityCategory::class)->create();
        $type = factory(FacilityType::class)->create(['facility_category_id' => $category->id]);

        $this->assertTrue($type->facility_category->is($category));
    }

    public function testTags(): void
    {
        $type = factory(FacilityType::class)->create();
        $tag = factory(Tag::class)->create(['facility_type_id' => $type->id]);

        $this->assertTrue($type->tags[0]->is($tag));
    }

    public function testCreator(): void
    {
        $user = factory(User::class)->create();
        $type = factory(FacilityType::class)->create(['creator_id' => $user->id]);

        $this->assertTrue($type->creator->is($user));
    }
}
