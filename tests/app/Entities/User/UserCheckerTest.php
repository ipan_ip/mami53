<?php

namespace App\Entities\User;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\User\UserChecker;
use App\Entities\User\UserCheckerTracker;

class UserCheckerTest extends MamiKosTestCase
{
    
    public function testBelongsToUser()
    {
        $user = factory(User::class)->create();
        $userChecker = factory(UserChecker::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertInstanceOf(User::class, $userChecker->user);
    }

    public function testHasManyTrackings()
    {
        $userChecker = factory(UserChecker::class)->create();
        $trackings = factory(UserCheckerTracker::class, 3)->create([
            'user_checker_id' => $userChecker->id
        ]);
        
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $userChecker->trackings);
        foreach ($userChecker->trackings as $key => $tracking) {
            $this->assertInstanceOf(UserCheckerTracker::class, $tracking);
        }
        $this->assertEquals(3, $userChecker->trackings->count());
    }

    public function testRegister()
    {
        $user = factory(User::class)->create();
        $userChecker = UserChecker::register($user, false);
        # assert return object
        $this->assertInstanceOf(UserChecker::class, $userChecker);
        # assert persisted object
        $queriedUserChecker = UserChecker::where('user_id', $user->id)->first();
        $this->assertInstanceOf(UserChecker::class, $queriedUserChecker);
    }

    public function testRemove()
    {
        $userChecker = factory(UserChecker::class)->create();
        $deletedUserChecker = UserChecker::remove($userChecker->id);
        $this->assertTrue(isset($deletedUserChecker->deleted_at));
    }
}
