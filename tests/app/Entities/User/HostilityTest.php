<?php

namespace App\Entities\User;

use App\Test\MamiKosTestCase;
use App\User;

class HostilityTest extends MamiKosTestCase
{
    public function testUser(): void
    {
        $user = factory(User::class)->create(['hostility' => 100]);
        $reason = factory(Hostility::class)->create(['user_id' => $user->id]);

        $this->assertTrue($reason->user->is($user));
    }

    public function testCreator(): void
    {
        $user = factory(User::class)->create(['hostility' => 100]);
        $reason = factory(Hostility::class)->create(['created_by' => $user->id]);

        $this->assertTrue($reason->creator->is($user));
    }

    public function testUpdater(): void
    {
        $user = factory(User::class)->create(['hostility' => 100]);
        $reason = factory(Hostility::class)->create(['updated_by' => $user->id]);

        $this->assertTrue($reason->updater->is($user));
    }
}
