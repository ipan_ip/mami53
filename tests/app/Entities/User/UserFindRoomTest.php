<?php

namespace App\Entities\User;

use App\Entities\Landing\Landing;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class UserFindRoomTest extends MamiKosTestCase
{
    use WithFaker;

    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $userFindRoom = factory(UserFindRoom::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertEquals($user->id, $userFindRoom->user->id);

        $this->setUpFaker();
    }

    public function testLandingRelation()
    {
        $landing = factory(Landing::class)->create();
        $userFindRoom = factory(UserFindRoom::class)->create([
            'landing_id' => $landing->id
        ]);
        $this->assertEquals($landing->id, $userFindRoom->landing->id);
    }

    public function testSaveFromSubscribe()
    {
        $userFindRoom = factory(UserFindRoom::class)->make()->toArray();
        $user = factory(User::class)->create();
        $userDetail = [
            'landing' => $userFindRoom['landing_id'],
            'name' => $userFindRoom['name'],
            'email' => $userFindRoom['email'],
            'phone' => $userFindRoom['phone_number'],
            'notes' => $userFindRoom['note'],
            'start_date' => isset($userFindRoom['start_date']) ? $userFindRoom['start_date'] : null,
            'duration' => isset($userFindRoom['duration_month']) ? $userFindRoom['duration_month'] : null,
            'city' => isset($userFindRoom['current_city']) ? $userFindRoom['current_city'] : null,
            'destination' => isset($userFindRoom['destination_city']) ? $userFindRoom['destination_city'] : null,
            'university' => isset($userFindRoom['university']) ? $userFindRoom['university'] : null,
        ];

        $result = UserFindRoom::saveFromSubscribe($user, $userDetail);
        $expectedResult = [
            'user_id' => $user->id,
            'landing_id' => $userDetail['landing'],
            'name' => $userDetail['name'],
            'email' => $userDetail['email'],
            'phone_number' => $userDetail['phone'],
            'note' => $userDetail['notes'],
            'start_date' => isset($userDetail['start_date']) ? $userDetail['start_date'] : null,
            'duration_month' => isset($userDetail['duration']) ? $userDetail['duration'] : null,
            'current_city' => isset($userDetail['city']) ? $userDetail['city'] : null,
            'destination_city' => isset($userDetail['destination']) ? $userDetail['destination'] : null,
            'university' => isset($userDetail['university']) ? $userDetail['university'] : null,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(), $keys);
        $this->assertEquals($expectedResult, $result);
    }
}
