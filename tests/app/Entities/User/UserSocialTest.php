<?php
namespace App\Entities\User;

use App\Test\MamiKosTestCase;
use App\Entities\User\UserSosial;
use App\User;
use App\Entities\User\SocialAccountType;

class UserSocialTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-1648
     * @group App/Entities/User
     */
    public function testBelongsToUser()
    {
        $user = factory(User::class)->create();
        $userChecker = factory(UserChecker::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertInstanceOf(User::class, $userChecker->user);
    }

    /**
     * @group UG
     * @group UG-1648
     * @group App/Entities/User
     */
    public function testValueOfTypeShouldReturnValidValue()
    {
        $userSocial = factory(UserSocial::class)->create([
            'type' => SocialAccountType::APPLE,
        ]);
        
        $expectedTypeResult = SocialAccountType::APPLE;
        $this->assertEquals($userSocial->type, $expectedTypeResult);
    }
}
