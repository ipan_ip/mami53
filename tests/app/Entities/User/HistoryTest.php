<?php

namespace App\Entities\User;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class HistoryTest extends MamiKosTestCase
{
    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $history = factory(History::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $history->user->is($user)
        );
    }

    public function testRoom(): void
    {
        $kost = factory(Room::class)->create();
        $history = factory(History::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $history->room->is($kost)
        );
    }

    public function testAutoCheckIn(): void
    {
        $user = factory(User::class)->create();
        $history = factory(History::class)->create(['user_id' => $user->id, 'is_active' => false]);
        $kost = factory(Room::class)->create();

        $actual = $history->autoCheckIn($kost, $user);

        $this->assertDatabaseHas(
            'user_designer_history',
            [
                'id' => $actual->id,
                'user_id' => $user->id,
                'designer_id' => $kost->id,
                'time' => Date('Y-m-d'),
                'is_active' => 1
            ]
        );

        $this->assertDatabaseHas(
            'user_designer_history',
            [
                'id' => $history->id,
                'user_id' => $user->id,
                'is_active' => 0
            ]
        );
    }
}
