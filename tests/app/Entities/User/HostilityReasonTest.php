<?php

namespace App\Entities\User;

use App\Test\MamiKosTestCase;

class HostilityReasonTest extends MamiKosTestCase
{
    public function testFraud(): void
    {
        $this->assertEquals('fraud', HostilityReason::FRAUD);
    }

    public function testCompetitor(): void
    {
        $this->assertEquals('competitor', HostilityReason::COMPETITOR);
    }

    public function testNotPayingGP(): void
    {
        $this->assertEquals('not_paying_gp', HostilityReason::NOT_PAYING_GP);
    }
}
