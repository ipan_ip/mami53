<?php

namespace App\Entities\User;

use Faker\Generator as Faker;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\User\Notification;
use App\Test\MamiKosTestCase;

class NotificationTest extends MamiKosTestCase
{
    
    protected $premiumPackage;
    protected $fakePremiumPackageId;

    public function setUp() : void
    {
        parent::setUp();
        $this->premiumPackage = factory(PremiumPackage::class)->create([
            'id' => 25
        ]);
        $this->fakePremiumPackageId = 99;
    }

    public function testGetPremiumPackageBalanceRequestWithPackage()
    {
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_package_id' => $this->premiumPackage->id
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $balanceRequest->id,
            'type' => 'balance_request_reminder',
            'identifier_type' => 'Balance_Request'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage->id, $this->premiumPackage->id);
    }

    public function testGetPremiumPackageBalanceRequestNoPackage()
    {
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_package_id' => $this->fakePremiumPackageId
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $balanceRequest->id,
            'type' => 'balance_request_reminder',
            'identifier_type' => 'Balance_Request'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage, null);
    }

    public function testGetPremiumPackagePremiumRequestWithPackage()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $this->premiumPackage->id
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $premiumRequest->id,
            'type' => 'premium_request_reminder',
            'identifier_type' => 'Premium_Request'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage->id, $this->premiumPackage->id);
    }

    public function testGetPremiumPackagePremiumRequestNoPackage()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $this->fakePremiumPackageId
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $premiumRequest->id,
            'type' => 'premium_request_reminder',
            'identifier_type' => 'Premium_Request'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage, null);
    }

    public function testGetPremiumPackagePremiumPaymenttWithPackage()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $this->premiumPackage->id
        ]);
        $premiumPayment = factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $premiumPayment->id,
            'type' => 'premium_payment_reminder',
            'identifier_type' => 'Premium_Payment'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage->id, $this->premiumPackage->id);
    }

    public function testGetPremiumPackagePremiumPaymentNoPackage()
    {
        $premiumPayment = factory(Payment::class)->create([
            'premium_request_id' => $this->fakePremiumPackageId
        ]);
        $notification = factory(Notification::class)->make([
            'identifier' => $premiumPayment->id,
            'type' => 'premium_payment_reminder',
            'identifier_type' => 'Premium_Payment'
        ]);
        $testPremiumPackage = $notification->getPremiumPackage();
        $this->assertEquals($testPremiumPackage, null);
    }
}
