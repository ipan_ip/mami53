<?php

namespace App\Entities\User;

use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class UserVerificationAccountTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-1395
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testResetIdentityCardStatusAlreadyVerifiedShouldReset()
    {
        $user = factory(User::class)->create();
        $this->createIdentityCardUserMedias($user);
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
        ]);

        $result = UserVerificationAccount::resetIdentityCardStatus($user);

        $userVerificationAccount->refresh();
        
        $this->assertTrue($result);
        $this->assertEquals(UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING, $userVerificationAccount->identity_card);
    }

    /**
     * @group UG
     * @group UG-1395
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testResetIdentityCardStatusNoUserMedias()
    {
        $user = factory(User::class)->create();

        $result = UserVerificationAccount::resetIdentityCardStatus($user);

        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1395
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testResetIdentityCardStatusNoUserId()
    {
        $user = factory(User::class)->make([
            'id' => null,
        ]);

        $result = UserVerificationAccount::resetIdentityCardStatus($user);

        $this->assertFalse($result);
    }

    public function testSetVerifyPhone()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'is_verify_phone_number' => true
        ]);
        $userVerificationAccount->setVerifyPhone(false);
        $this->assertEquals(false, $userVerificationAccount->fresh()->is_verify_phone_number);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testUserRelationship()
    {
        $user = factory(User::class)->create();
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertInstanceOf(User::class, $userVerificationAccount->user);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testUserMediaRelationship()
    {
        $user = factory(User::class)->create();
        $this->createIdentityCardUserMedias($user);
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertInstanceOf(UserMedia::class, $userVerificationAccount->user_medias->first());
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testAddOrUpdateWithUserVerifyShouldUpdate()
    {
        $user = factory(User::class)->create();
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user->id,
            'is_verify_email' => false
        ]);
        $request = ['email' => true];
        
        $addOrUpdate = UserVerificationAccount::addOrUpdate($request, $user);

        $this->assertInstanceOf(UserVerificationAccount::class, $addOrUpdate);
        $this->assertEquals(true, $userVerificationAccount->refresh()->is_verify_email);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testAddOrUpdateWithoutUserVerify()
    {
        $user = factory(User::class)->create();
        $addOrUpdate = UserVerificationAccount::addOrUpdate([], $user);

        $this->assertInstanceOf(UserVerificationAccount::class, $addOrUpdate);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testAddUserVerify()
    {
        $user = factory(User::class)->create();
        $request = [
            'phone_number' => true,
            'email' => true,
        ];
        $userVerificationAccount = UserVerificationAccount::add($request, $user);

        $this->assertInstanceOf(UserVerificationAccount::class, $userVerificationAccount);
        $this->assertEquals(true, $userVerificationAccount->refresh()->is_verify_phone_number);
        $this->assertEquals(true, $userVerificationAccount->refresh()->is_verify_email);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testAddUserVerifyWithEmptyRequest()
    {
        $user = factory(User::class)->create();
        $addOrUpdate = UserVerificationAccount::add([],$user);

        $this->assertInstanceOf(UserVerificationAccount::class, $addOrUpdate);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testValidateIdentityCardStatusWithoutIdentityCard()
    {
        $user = factory(User::class)->create();
        $validateIdentity = UserVerificationAccount::validateIdentityCardStatus($user);

        $this->assertTrue($validateIdentity);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testValidateIdentityCardStatusWithIdentityCardShouldCreateUserVerificationAccount()
    {
        $user = factory(User::class)->create();
        $this->createIdentityCardUserMedias($user);
        $validateIdentity = UserVerificationAccount::validateIdentityCardStatus($user);

        $this->assertTrue($validateIdentity);
        $result = UserVerificationAccount::where('user_id', $user->id)->first();
        $this->assertNotNull($result);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testValidateIdentityCardStatusShouldCatchExeption()
    {
        $exception = new Exception();
        $user = factory(User::class)->create();
        $this->createIdentityCardUserMedias($user);
        
        $this->mock(UserVerificationAccount::class)->shouldReceive('addOrUpdate')
            ->andThrow($exception);
        
        Bugsnag::shouldReceive('notifyException')
            ->once();
        
        $validateIdentity = UserVerificationAccount::validateIdentityCardStatus($user);
        $this->assertFalse($validateIdentity);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Entities\User\UserVerificationAccount
     */
    public function testResetIdentityCardStatusShouldCatchExeption()
    {
        $exception = new Exception();
        $user = factory(User::class)->create();
        $this->createIdentityCardUserMedias($user);
        
        $this->mock(UserVerificationAccount::class)->shouldReceive('addOrUpdate')
            ->andThrow($exception);
        
        Bugsnag::shouldReceive('notifyException')
            ->once();
        
        $resetIdentity = UserVerificationAccount::resetIdentityCardStatus($user);
        $this->assertFalse($resetIdentity);
    }

    /**
     * Will create user media for selfie and id card
     *
     * @param User $user
     * @return void
     */
    private function createIdentityCardUserMedias(User $user): void
    {
        factory(UserMedia::class)->create([
            'user_id' => $user,
            'description' => UserMedia::PHOTO_IDENTITY_CARD,
        ]);
        factory(UserMedia::class)->create([
            'user_id' => $user,
            'description' => UserMedia::SELFIE_IDENTITY_CARD,
        ]);
    }
}
