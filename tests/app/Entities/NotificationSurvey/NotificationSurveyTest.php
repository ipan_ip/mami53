<?php

namespace App\Entities\NotificationSurvey;

use App\Entities\NotificationSurvey\NotificationSurvey;
use App\Entities\NotificationSurvey\NotificationSurveyRecipient;
use App\Entities\Room\Room;
use App\Entities\Room\RoomPriceType;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationSurveyTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testRecipientsRelationship()
    {
        $notificationSurvey = factory(NotificationSurvey::class)->create();

        $recipients = factory(NotificationSurveyRecipient::class)->create();

        $notificationSurvey->setRelation('recipients', $recipients);

        $this->assertEquals($recipients->id, $notificationSurvey->recipients->id);
    }
}