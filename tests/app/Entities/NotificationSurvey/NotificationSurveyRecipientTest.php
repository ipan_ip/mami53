<?php

namespace App\Entities\NotificationSurvey;

use App\Entities\NotificationSurvey\NotificationSurvey;
use App\Entities\NotificationSurvey\NotificationSurveyRecipient;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationSurveyRecipientTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testNotificationSurveyRelationship()
    {
        $notificationSurvey = factory(NotificationSurvey::class)->create();

        $recipients = factory(NotificationSurveyRecipient::class)->create();

        $recipients->setRelation('notification_survey', $notificationSurvey);

        $this->assertEquals($notificationSurvey->id, $recipients->notification_survey->id);
    }

    public function testUserRelationship()
    {
        $recipients = factory(NotificationSurveyRecipient::class)->create();

        $user = factory(User::class)->create();

        $recipients->setRelation('user', $user);

        $this->assertEquals($user->id, $recipients->user->id);
    }

}