<?php

namespace App\Entities\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Test\MamiKosTestCase;
use App\User;

class PotentialOwnerFollowupHistoryTest extends MamiKosTestCase
{
    public function testPotentialOwnerRelation(): void
    {
        $expected = factory(PotentialOwner::class)->create();
        $history = factory(PotentialOwnerFollowupHistory::class)->create(['potential_owner_id' => $expected->id]);

        $actual = $history->potential_owner;

        $this->assertTrue($actual->is($expected));
    }

    public function testOwnerRelation(): void
    {
        $expected = factory(User::class)->create();
        $history = factory(PotentialOwnerFollowupHistory::class)->create(['user_id' => $expected->id]);

        $actual = $history->owner;

        $this->assertTrue($actual->is($expected));
    }
}
