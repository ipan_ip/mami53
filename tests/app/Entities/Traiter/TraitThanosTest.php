<?php

namespace App\Entities\Traiter;

use App\Entities\Room\Room;
use App\User;
use App\Entities\Room\ThanosHidden;
use App\Test\MamiKosTestCase;

class TraitThanosTest extends MamiKosTestCase
{
    public function setUp() : void
    {
        parent::setUp();
        $this->room = new Room;
    }

    public function testRevertThanos()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $this->actingAs($user);

        factory(ThanosHidden::class)->create([
            'designer_id' => $room->id,
            'snapped' => true,
            'log' => []
        ]);

        $expectedResult = [
            'designer_id' => $room->id,
            'snapped' => 0,
        ];

        $reflection = new \ReflectionClass(Room::class);
        $method = $reflection->getMethod('revertThanos');
        $method->setAccessible(true);
        $method->invokeArgs($this->room, [$room, []]);

        $this->assertDatabaseHas('designer_hidden_thanos', $expectedResult);
    }
}
