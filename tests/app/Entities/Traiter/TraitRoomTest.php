<?php

namespace App\Entities\Traiter;

use App\Entities\Activity\Love;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Review;
use App\Entities\Room\RoomInputProgress;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomPriceType;
use App\Entities\Room\RoomProgress;
use App\Entities\Room\Survey;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Room\Element\Price;

class TraitRoomTest extends MamiKosTestCase
{
    
    public function testCheckingLoveWithNoRelationShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        
        
        $love = $room->checking_love;
        $this->assertFalse($love);
    }

    public function testCheckingLoveWithNoUserShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        app()->user = null; 

        $user = factory(User::class)->create([]);

        factory(Love::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('love')->where('id', 2)->first();
        $love = $room->checking_love;
        $this->assertFalse($love);
    }

    public function testCheckingLoveWithValidUserAndLoveShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(Love::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('love')->where('id', 2)->first();
        $love = $room->checking_love;
        $this->assertTrue($love);
    }

    public function testCheckingLoveWithInvalidUserShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create(['id'=>10]);
        
        app()->user = $user; 

        factory(Love::class)->create([
            "user_id" => 5,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('love')->where('id', 2)->first();
        $love = $room->checking_love;
        $this->assertFalse($love);
    }

    public function testCheckingSurveyWithNoRelationShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        
        
        $survey = $room->checking_survey;
        $this->assertFalse($survey);
    }

    public function testCheckingSurveyWithNoUserShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        app()->user = null; 

        $user = factory(User::class)->create([]);

        factory(Survey::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('survey')->where('id', 2)->first();
        $survey = $room->checking_survey;
        $this->assertFalse($survey);
    }

    public function testCheckingSurveyWithInvalidUserShouldReturFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(Survey::class)->create([
            "user_id" => 3,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('survey')->where('id', 2)->first();
        $survey = $room->checking_survey;
        $this->assertFalse($survey);
    }

    public function testCheckingSurveyWithValidUserAndSurveyShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(Survey::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('survey')->where('id', 2)->first();
        $survey = $room->checking_survey;
        $this->assertTrue($survey);
    }

    public function testCheckingReviewWithNoRelationShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        
        
        $review = $room->checking_review;
        $this->assertTrue($review);
    }

    public function testCheckingReviewWithNoUserShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        app()->user = null; 

        $user = factory(User::class)->create([]);

        factory(Review::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('review')->where('id', 2)->first();
        $review = $room->checking_review;
        $this->assertTrue($review);
    }

    public function testCheckingReviewWithInvalidUserShouldReturTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(Review::class)->create([
            "user_id" => 3,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('review')->where('id', 2)->first();
        $review = $room->checking_review;
        $this->assertTrue($review);
    }

    public function testCheckingReviewWithValidUserAndReviewShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(Review::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('review')->where('id', 2)->first();
        $review = $room->checking_review;
        $this->assertFalse($review);
    }

    public function testCheckingOwnerWithNoRelationShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        
        
        $owner = $room->checking_owner;
        $this->assertFalse($owner);
    }

    public function testCheckingOwnerWithNoUserShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        app()->user = null; 

        $user = factory(User::class)->create([]);

        factory(RoomOwner::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('owners')->where('id', 2)->first();
        $owner = $room->checking_owner;
        $this->assertFalse($owner);
    }

    public function testCheckingOwnerWithInvalidUserShouldReturFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(RoomOwner::class)->create([
            "user_id" => 3,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('owners')->where('id', 2)->first();
        $owner = $room->checking_owner;
        $this->assertFalse($owner);
    }

    public function testCheckingOwnerWithValidUserAndOwnerShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $user = factory(User::class)->create([]);
        
        app()->user = $user; 

        factory(RoomOwner::class)->create([
            "user_id" => $user->id,
            "designer_id" => $room->id
        ]);
      
        $room = Room::with('owners')->where('id', 2)->first();
        $owner = $room->checking_owner;
        $this->assertTrue($owner);
    }

    public function testCheckingPriceTyperWithNoRelationShouldReturnIdr()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $room = Room::with('room_price_type')->where('id', 2)->first();
        $owner = $room->price_typer;

        $expectedResult = 'idr';
        
        $this->assertEquals($owner,$expectedResult);
    }

    public function testCheckingPriceTyperWithValidRelationShouldReturnUsd()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
            ]);
        $room = Room::with('room_price_type')->where('id', 2)->first();
        $owner = $room->price_typer;

        $expectedResult = 'usd';
        
        $this->assertEquals($owner,$expectedResult);
    }

    public function testCheckingPriceTypesWithNoRelationShouldReturnIdr()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $room = Room::with('room_price_type')->where('id', 2)->first();
        $owner = $room->price_types;

        $expectedResult = 'idr';
        
        $this->assertEquals($owner,$expectedResult);
    }

    public function testCheckingPriceTypesWithValidRelationShouldReturnUsd()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
            ]);
        $room = Room::with('room_price_type')->where('id', 2)->first();
        $owner = $room->price_types;

        $expectedResult = 'usd';
        
        $this->assertEquals($owner,$expectedResult);
    }

    public function testInputProgressWithNoRelationShouldReturnNull()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        
        
        $input_progress = $room->input_progress;
        $this->assertNull($input_progress);
    }

    // public function testInputProgressWithIscompleteIsTrueShouldReturnNull()
    // {
    //     $room = factory(Room::class)->create([
    //         "id" => 2,
    //     ]);
        
    //     $inputProgress = factory(RoomInputProgress::class)->create([
    //         "designer_id" => $room->id,
    //     ]);
        
    //     $inputProgress->is_complete = 1;

    //     $room = Room::with('room_input_progress')->where('id', 2)->first();
    //     $owner = $room->inputProgress;
    //     $this->assertNull($owner);
    // }

    public function testInputProgressWithRelationTypes()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        $inputProgress = factory(RoomInputProgress::class)->create([
            "designer_id" => $room->id,
            "is_completed" => 1,
            "on_step" => 1
        ]);

        $roomType = factory(Type::class)->create([
            "designer_id" => $room->id,
        ]);
      
        $room = Room::with(['room_input_progress','types'])->where('id', 2)->first();
        $result = $room->input_progress;
      
        $expectedResult = [
			"on_step" => $inputProgress->on_step,
			"step_total" => RoomInputProgress::INPUT_PROGRESS_TOTAL,
			"room_type_id" => $roomType->id,
		];
        $this->assertEquals($result,$expectedResult);
    }

    public function testApartmentProperties()
    {
        $aptProject = factory(ApartmentProject::class)->create([]);

        $room = factory(Room::class)->create([
            "id" => 2,
            "furnished" => 1,
            "unit_type" => "unit_type",
            "floor" => "2",
            "size" => "25",
            "apartment_project_id" => $aptProject->id
        ]);

        
        $result = $room->apartment_properties;
        
        $expectedResult = [
		    [
		        'name' => 'condition',
		        'label' => 'Kondisi',
		        'value' => $room->apartment_project::UNIT_CONDITION[$room->furnished],
		        'icon' => ''
		    ],
		    [
		        'name' => 'unit_type',
		        'label' => 'Tipe Unit',
		        'value' => $room->unit_type,
		        'icon' => url('general/img/icons/ic_apartment_building.png'),
		    ],
		    [
		        'name' => 'floor',
		        'label' => 'Lantai',
		        'value' => (string) $room->floor,
		        'icon' => url('general/img/icons/ic_villa_floor.png'),
		    ],
		    [
		        'name' => 'size',
		        'label' => 'Luas',
		        'value' => $room->size . ' m²',
		        'icon' => url('general/img/icons/ic_villa_building.png'),
		    ]
        ];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testIsEditableWithNoRelationShouldReturnTrue()
    {
        $room = factory(Room::class)->create();
        $result = $room->is_editable;
        $this->assertTrue($result);
    }

    public function testIsEditableWithValidRelationShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);
        
        factory(RoomInputProgress::class)->create([
            "designer_id" => $room->id,
        ]);

        $room = Room::with('room_input_progress')->where('id', 2)->first();
        $result = $room->is_editable;
      
        $this->assertFalse($result);
    }

    public function testIsEditableWithInvalidRelationShouldReturnTrue()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
        ]);        

        $room = Room::with('room_input_progress')->where('id', 2)->first();
        $result = $room->is_editable;
      
        $this->assertTrue($result);
    }

    public function testRealPrice()
    {
        $room = factory(Room::class)->create([]);        

        $result = $room->real_price;
      
        $expectedResult = [
			'price_daily' => $room->price_daily,
			'price_weekly' => $room->price_weekly,
			'price_monthly' => $room->price_monthly,
			'price_quarterly' => $room->price_3_month,
			'price_semiannually' => $room->price_6_month,
			'price_yearly' => $room->price_yearly,
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleWithPriceMonthly()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 200,
        ]);

        $priceTime = Price::MONTHLY_TIME;
        $priceTitle = $room->price_monthly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $result = $room->price_title;
        $priceTitleTimeUsd='';

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleWithPriceYearly()
    {
        $room = factory(Room::class)->create([
            'price_yearly' => 200,
        ]);

        $priceTime = Price::YEARLY_TIME;
        $priceTitle = $room->price_yearly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $result = $room->price_title;
        $priceTitleTimeUsd='';

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleWithPriceWeeky()
    {
        $room = factory(Room::class)->create([
            'price_weekly' => 200,
        ]);

        $priceTime = Price::WEEKLY_TIME;
        $priceTitle = $room->price_weekly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $result = $room->price_title;
        $priceTitleTimeUsd='';

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleWithPriceDaily()
    {
        $room = factory(Room::class)->create([
            'price_daily' => 200,
        ]);

        $priceTime = Price::DAILY_TIME;
        $priceTitle = $room->price_daily;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $result = $room->price_title;
        $priceTitleTimeUsd='';

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleUsdWithPriceMonthly()
    {
        $room = factory(Room::class)->create([
            'price_monthly_usd' => 200,
            'price_monthly' => 1490000,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
        ]);

        $room = Room::with('room_price_type')->where('id', $room->id)->first();

        $priceTime = Price::MONTHLY_TIME;
        $priceTitle = $room->price_monthly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $priceTitleUsd = $room->price_monthly_usd;
        $priceTitleTimeUsd = Price::shorten_v2($priceTitleUsd);
        
        $result = $room->price_title;

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleUsdWithPriceYearly()
    {
        $room = factory(Room::class)->create([
            'price_yearly_usd' => 200,
            'price_yearly' => 1490000,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
        ]);

        $room = Room::with('room_price_type')->where('id', $room->id)->first();

        $priceTime = Price::YEARLY_TIME;
        $priceTitle = $room->price_yearly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $priceTitleUsd = $room->price_yearly_usd;
        $priceTitleTimeUsd = Price::shorten_v2($priceTitleUsd);
        
        $result = $room->price_title;

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleUsdWithPriceWeekly()
    {
        $room = factory(Room::class)->create([
            'price_weekly_usd' => 200,
            'price_weekly' => 1490000,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
        ]);

        $room = Room::with('room_price_type')->where('id', $room->id)->first();

        $priceTime = Price::WEEKLY_TIME;
        $priceTitle = $room->price_weekly;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $priceTitleUsd = $room->price_weekly_usd;
        $priceTitleTimeUsd = Price::shorten_v2($priceTitleUsd);
        
        $result = $room->price_title;

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPriceTitleUsdWithPricDaily()
    {
        $room = factory(Room::class)->create([
            'price_daily_usd' => 200,
            'price_daily' => 1490000,
        ]);
        
        factory(RoomPriceType::class)->create([
            'designer_id' => $room->id,
            'type' => 'usd'
        ]);

        $room = Room::with('room_price_type')->where('id', $room->id)->first();

        $priceTime = Price::DAILY_TIME;
        $priceTitle = $room->price_daily;
        $priceTitleTime = Price::shorten_v2($priceTitle);
        
        $priceTitleUsd = $room->price_daily_usd;
        $priceTitleTimeUsd = Price::shorten_v2($priceTitleUsd);
        
        $result = $room->price_title;

        $expectedResult = [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
        
        $this->assertEquals($result,$expectedResult);
    }

    public function testPromoteStatusFalseWithNoRelationShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            "id" => 1,
        ]);
        
        $promoteStatus = $room->promote_status;
        $this->assertFalse($promoteStatus);
    }

    public function testPromoteStatusFalse()
    {
        factory(Room::class)->create([
            "id" => 1,
            "is_promoted" => "false"
        ]);
        
        $room = Room::with('view_promote')->where('id', 1)->first();
        $promoteStatus = $room->promote_status;
        $this->assertFalse($promoteStatus);
    }

    public function testPromoteStatusTrue()
    {
        factory(Room::class)->create([
            "id" => 2,
            "is_promoted" => "true"
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 1,
            "is_active" => 1,
            "designer_id" => 2
        ]);
      
        $room = Room::with('view_promote')->where('id', 2)->first();
        $promoteStatus = $room->promote_status;
        $this->assertTrue($promoteStatus);
    }

    public function testPromoteStatusWithIspromoteAndIsactiveAreFalseShouldReturnFalse()
    {
        factory(Room::class)->create([
            "id" => 2,
            "is_promoted" => "false"
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 1,
            "is_active" => 0,
            "designer_id" => 2
        ]);

        $room = Room::with('view_promote')->where('id', 2)->first();
        $promoteStatus = $room->promote_status;
        $this->assertFalse($promoteStatus);
    }

    public function testGetSaldoAvailable()
    {
        factory(Room::class)->create([
            "id" => 3,
            "is_promoted" => "true"
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 2,
            "is_active" => 0,
            "designer_id" => 3,
            "total" => 500,
            "history" => 200
        ]);

        $room = Room::with('view_promote')->where('id', 3)->first();
        $saldo = $room->saldo_available;
        $this->assertEquals(0, $saldo);
    }

    public function testGetSaldoAvailableWithNoRelation()
    {
        $room = factory(Room::class)->create([
            "id" => 3,
            "is_promoted" => "true"
        ]);

        $saldo = $room->saldo_available;

        $this->assertEquals($saldo,0);
    }

    public function testGetSaldoAvailableWhenViewPromoteIsActive()
    {
        factory(Room::class)->create([
            "id" => 3,
            "is_promoted" => "true"
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 2,
            "is_active" => 1,
            "designer_id" => 3,
            "total" => 500,
            "history" => 200,
            "used" => 200
        ]);

        $room = Room::with('view_promote')->where('id', 3)->first();
        $saldo = $room->saldo_available;
        $this->assertEquals($saldo,100);
    }

    public function testGetSaldoAvailableWhenTotalSaldoLessThanHistoryAndUsedShouldReturnZero()
    {
        factory(Room::class)->create([
            "id" => 3,
            "is_promoted" => "true"
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 2,
            "is_active" => 1,
            "designer_id" => 3,
            "total" => 500,
            "history" => 200,
            "used" => 300
        ]);

        $room = Room::with('view_promote')->where('id', 3)->first();
        $saldo = $room->saldo_available;
        $this->assertEquals($saldo,0);
    }

    public function testGetRoomPromotionNull()
    {
        $room = factory(Room::class)->create([
            "id" => 721
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertEquals(null, $promo);
    }

    public function testGetRoomPromotionVerified()
    {
        $room = factory(Room::class)->create([
            "id" => 728
        ]);
        
        factory(Promotion::class)->create([
            "id" => 1,
            "verification" => "true",
            "content" => "description",
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertEquals('verified', $promo['status']);
    }

    public function testGetRoomPromotionWaiting()
    {
        $room = factory(Room::class)->create([
            "id" => 721
        ]);
        
        factory(Promotion::class)->create([
            "id" => 2,
            "verification" => "false",
            "content" => "description",
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertEquals('waiting', $promo['status']);
    }

    public function testGetRoomPromotionExpired()
    {
        $room = factory(Room::class)->create([
            "id" => 753
        ]);
        
        factory(Promotion::class)->create([
            "id" => 3,
            "verification" => "true",
            "content" => "description",
            "start_date" => date('Y-m-d', strtotime('-7 days')),
            "finish_date" => date('Y-m-d', strtotime('-1 days')),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertEquals('expired', $promo['status']);
    }

    public function testGetRoomPromotionIgnored()
    {
        $room = factory(Room::class)->create([
            "id" => 893
        ]);
        
        factory(Promotion::class)->create([
            "id" => 3,
            "verification" => Promotion::IGNORE,
            "content" => "description",
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertEquals(Promotion::IGNORE_STATUS, $promo['status']);
    }

    public function testGetRoomVerifiedWhenPromotionIgnored()
    {
        $room = factory(Room::class)->create([
            "id" => 893
        ]);
        
        factory(Promotion::class)->create([
            "id" => 3,
            "verification" => Promotion::IGNORE,
            "content" => "description",
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertFalse($promo['is_verified']);
    }

    public function testGetRoomVerifiedWhenPromotionVerified()
    {
        $room = factory(Room::class)->create([
            "id" => 893
        ]);
        
        factory(Promotion::class)->create([
            "id" => 3,
            "verification" => Promotion::VERIFY,
            "content" => "description",
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "promo title",
            "designer_id" => $room->id,
        ]);
        
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $promo = $room->room_promotion;
        $this->assertTrue($promo['is_verified']);
    }

    public function testGetRoomPromotionAttributeWithNoRelation()
    {
        $room = factory(Room::class)->create([
            "id" => 893
        ]);
        
        $promo = $room->room_promotion;
        $this->assertNull($promo);
    }
}