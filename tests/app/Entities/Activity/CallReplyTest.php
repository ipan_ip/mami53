<?php

namespace App\Entities\Activity;

use App\Entities\Activity\Call;
use App\Entities\Activity\CallReply;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CallReplyTest extends MamikosTestCase
{
    use DatabaseTransactions;

    public function testNotificationRelationship()
    {
        $callReply = factory(CallReply::class)->create();

        $call = factory(Call::class)->create();

        $callReply->setRelation('call', $call);

        $this->assertEquals($callReply->call->id, $call->id);
    }
}
