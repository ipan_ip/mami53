<?php

namespace App\Entities\Activity;

use Illuminate\Support\Facades\Cache;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use Mockery;
use Carbon\Carbon;
use StatsLib;

class LoveTest extends MamiKosTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        // Reset test carbon mock
        Carbon::setTestNow();
    }

    private function generateLove(Room $room, User $user, $creationTime = null, $promoted = true, $type = 'like', $status = 'true')
    {

        $now = Carbon::now();
        
        factory(Love::class)->create([
            'designer_id'   => $room,
            'user_id'       => $user,
            'type'          => $type,
            'status'        => $status,
            'created_at'    => is_null($creationTime) ? $now : $creationTime,
            'updated_at'    => is_null($creationTime) ? $now : $creationTime,
            'is_promoted_room'  => $promoted
        ]);

        return true;
    }

    public function testCheckIsUserLovedARoomShouldReturnTrue()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $this->generateLove($room, $user);

        $result = Love::isUserLovedARoom($room, $user);
        $users = Love::where('designer_id', $room->id)->first();

        $this->assertTrue($result);
        $this->assertEquals($user->id, $users->user->id);
    }

    public function testCheckUserLovedARoomFromCacheShouldReturnTrue()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $this->generateLove($room, $user);

        Cache::shouldReceive('has')
            ->once()
            ->with('user' . $user->id . ':love')
            ->andReturn(true);

        Cache::shouldReceive('get')
            ->once()
            ->with('user' . $user->id . ':love')
            ->andReturn([
                'loves' => [
                    $room->id
                ]
            ]);

        $result = Love::isUserLovedARoom($room, $user);
        $rooms = Love::where('user_id', $user->id)->first();

        $this->assertTrue($result);
        $this->assertEquals($room->id, $rooms->room->id);
    }

    public function testCheckIsUserLovedARoomShouldReturnFalse()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $this->generateLove($room, factory(User::class)->create());

        $result = Love::isUserLovedARoom($room, $user);

        $this->assertFalse($result);
    }

    public function testCheckUserLovedARoomFail()
    {
        $room = factory(Room::class)->create();
        $this->generateLove($room, factory(User::class)->create());
        $this->generateLove($room, factory(User::class)->create());

        $result = Love::isUserLovedARoom($room);

       $this->assertFalse($result);
    }

    public function testAddLovedARoomShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $result = Love::addOrDelete($user, $room);

        $this->assertEquals('love', $result);
    }

    public function testUnlovedARoomShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $this->generateLove($room, $user);

        $result = Love::addOrDelete($user, $room);

        $this->assertEquals('unlove', $result);
    }

    public function testGetLoveCountWithDateRangeSpecified()
    {
        $room = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $this->generateLove($room, $user, null, false);
        $this->generateLove($room2, $user, null, true);
        $this->generateLove($room2, $user, null, true);

        $result = Love::GetLoveCountWithType($room->id);
        $result2 = Love::GetLoveCountWithType($room2->id, 5, true);

        $this->assertEquals(1, $result);
        $this->assertEquals(2, $result2);
    }
    
}