<?php

namespace App\Entities\Activity;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class PhotoByTest extends MamiKosTestCase
{
    public function testRoom(): void
    {
        $room = factory(Room::class)->create();
        $photoBy = factory(PhotoBy::class)->create(['identifier' => $room->id]);

        $this->assertTrue($photoBy->room->is($room));
    }

    public function testUpdateTo(): void
    {
        $room = factory(Room::class)->create();
        $photoBy = factory(PhotoBy::class)->create(['identifier' => 0]);

        PhotoBy::updateTo($photoBy->id, $room->id);

        $this->assertDatabaseHas('media_photo_by', ['id' => $photoBy->id, 'identifier' => $room->id]);
    }
}
