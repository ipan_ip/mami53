<?php

namespace App\Entities\Activity;

use App\Entities\Activity\OwnerChat;
use App\Test\MamiKosTestCase;
use SendBird;

class OwnerChatTest extends MamiKosTestCase
{
    public function testSendMessageWithGoupId()
    {
        $groupId = 123;
        $message = "Untuk bantuan input data , silakan lengkapi data berikut : \n" .
            "1. Nama Kost \n" .
            "2. Alamat Kost \n" .
            "3. Nama Pemilik \n" .
            "4. Nomor telp pemilik \n" .
            "5. Harga Sewa \n";

        //Mock sendBird
        SendBird::shouldReceive("sendMessageToGroupChannel")
            ->andReturn(true);
        $ownerChat = new OwnerChat($groupId);
        $response = $ownerChat->reply();

        $this->assertNotNull($response);
        $this->assertEquals($message, $response["message"]);
    }

    public function testSendMessageResponseSenBirdIsNull()
    {
        $groupId = 123;
        //Mock sendBird
        SendBird::shouldReceive("sendMessageToGroupChannel")
            ->andReturn(null);
        $ownerChat = new OwnerChat($groupId);
        $response = $ownerChat->reply();

        $this->assertNull($response);
    }


}