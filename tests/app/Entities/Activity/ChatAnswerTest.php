<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;

class ChatAnswerTest extends MamiKosTestCase
{
    public function testQuestion(): void
    {
        $question = factory(Question::class)->create();
        $answer = factory(ChatAnswer::class)->create(['chat_question_id' => $question->id]);

        $this->assertTrue($answer->question->is($question));
    }
}
