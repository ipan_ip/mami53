<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class ActivityLogTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $activityLog;

    protected function setUp(): void
    {
        parent::setUp();

        $this->activityLog = Mockery::mock(ActivityLog::class)->makePartial();
    }

    public function testGetUser(): void
    {
        $expected = factory(User::class)->create();

        $this->activityLog->causer_id = $expected->id;

        $actual = $this->activityLog->getUser();

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testLogException(): void
    {
        $exception = new Exception('This is a test exception.');

        ActivityLog::LogException('Kost', $exception);
    }

    public function testLog()
    {
        $activity = 'Unit Test Activity Log';
        $description = 'description of Activity Log';

        ActivityLog::Log($activity, $description);
        $log = ActivityLog::where('log_name', $activity)->first();
        $this->assertEquals($description, $log->description);
    }
    
    public function testLogWithSubject()
    {
        $activity = 'Unit Test Activity Log';
        $description = 'description of Activity Log with subject';
        $user = factory(\App\User::class)->create();

        ActivityLog::Log($activity, $description, $user);
        $log = ActivityLog::where('log_name', $activity)->first();
        $this->assertEquals($description, $log->description);
        $this->assertEquals($user->id, $log->subject_id);
        $this->assertEquals(get_class($user), $log->subject_type);
    }
}
