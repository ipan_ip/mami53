<?php
namespace App\Entities\Activity;

use App\Entities\Activity\Call;
use App\Entities\Consultant\Consultant;
use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use StatsLib;

class FakeConsultant {
    public $user_id = 10;
}

class CallTest extends MamikosTestCase
{
    use DatabaseTransactions;

    public function testGetChatRoom()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();
        $admin = factory(User::class)->create();

        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => null
            ]
        );
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => 'sendbird_channel_id'
            ]
        );

        $result = Call::getChatRoom($room->id, $user);
        $this->assertEquals(1, $result['count']);
        $this->assertEquals('sendbird_channel_id', $result['chat'][0]['group_channel_url']);
    }

    public function testGetChatRoomWhenOwnerClaimedKostLately()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create();
        $roomOwnerRelation = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        $tenant = factory(Room::class)->create();
        $admin = factory(User::class)->create();

        // add 1 call before owner claims kost
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $tenant->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => 'sendbird_channel_id1',
                'created_at' => Carbon::parse($roomOwnerRelation->created_at)->subHours(1)
            ]
        );

        // add 1 soft-deleted call before owner claims kost
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $tenant->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => 'sendbird_channel_id1',
                'created_at' => Carbon::parse($roomOwnerRelation->created_at)->subHours(1),
                'deleted_at' => Carbon::parse($roomOwnerRelation->created_at)->subMinutes(30),
            ]
        );

        // add 1 call after owner claims kost
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $tenant->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => 'sendbird_channel_id2',
                'created_at' => Carbon::parse($roomOwnerRelation->created_at)->addHours(1)
            ]
        );

        // add 1 soft-deleted call after owner claims kost
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $tenant->id,
                'chat_admin_id' => $admin->id,
                'chat_group_id' => 'sendbird_channel_id2',
                'created_at' => Carbon::parse($roomOwnerRelation->created_at)->addHours(1),
                'deleted_at' => Carbon::parse($roomOwnerRelation->created_at)->subMinutes(30),
            ]
        );

        $result = Call::getChatRoom($room->id, $owner, true);
        $this->assertEquals(1, $result['count']);

        $result = Call::getChatRoom($room->id, $owner, false);
        $this->assertEquals(2, $result['count']);

        $result = Call::getChatRoom($room->id, $owner);
        $this->assertEquals(2, $result['count']);
    }

    /**
    * @link http://docs.mockery.io/en/latest/cookbook/mocking_hard_dependencies.html
    *
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testStoreWithConsultantFound()
    {
        $room = factory(Room::class)->make(["id" => 1991]);
        $user = factory(User::class)->make(["id" => 1992]);
        $device = factory(UserDevice::class)->make(["id" => 1993]);

        $trckMock = Mockery::mock('overload:App\Entities\Consultant\Consultant');
        $trckMock->shouldReceive('getAssignedTo')
            ->with($room->id, $room->area_city, $room->is_mamirooms)
            ->andReturn(new FakeConsultant);

        $note = "this is call data note";
        $callData = [
            "note" => $note,
            "group_chat_id" => 10,
            "add_from" => 20,
        ];

        $this->assertTrue(true);

        $call = Call::store($room, $user, $callData, $device);
        $this->assertNotNull($call);
        $this->assertEquals(20, $call->add_from);
        $this->assertEquals($room->id, $call->designer_id);
        $this->assertEquals($user->id, $call->user_id);
        $this->assertEquals($device->id, $call->device_id);
        $this->assertNull($call->phone);
        $this->assertNull($call->email);
        $this->assertNull($call->name);
        $this->assertNull($call->schedule);
        $this->assertEquals($note, $call->note);
        // 10 is from FakeConsultant default user_id
        $this->assertEquals(10, $call->chat_admin_id);
        $this->assertEquals(10, $call->chat_group_id);
    }

    public function testSetReplyStatus()
    {
        $call = new Call;
        $call->setReplyStatus(["status" => "success"]);
        $this->assertEquals('true', $call->reply_status);

        $call = new Call;
        $call->setReplyStatus(["status" => "fail"]);
        $this->assertNull($call->reply_status);
    }

    /** public function testAutoReply()
    * {   
        * to hard to test.
        * This function tries to get Call records by querying into database.
        * Query will try to take all records created in time range of 7 days ago
        * until now. There is no guarantee that the data will not be polluted.
        * (polluted = There will be unexpected data in the table, could be
        * from another test case)
        
        * Maybe it would be easier to test IF this autoReply method is written
        * outside of Call class into it's own Class, say that CallService
        * So then we can mock the Call class to get predictable records
        * and do the test properly.
    * }
    */

    /**
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testGetMessageCountReport()
    {
        $mock = Mockery::mock('overload:StatsLib');
        $mock->shouldReceive("count")
            ->andReturn(10);
        $mock->shouldReceive('ScopeWithRange')
            ->andReturn($mock);

        $count = Call::GetMessageCountReport(10, 'the_type');
        $this->assertEquals(10, $count);
    }

    // Skip another static method for now
    // unfortunately, other methods are static.
    // so maybe I'll continue later, maybe not.

    public function testReplies(): void
    {
        $call = factory(Call::class)->create();
        $reply = factory(CallReply::class)->create(['call_id' => $call->id]);

        $this->assertTrue($call->replies[0]->is($reply));
    }

    public function testNotificationRelationship()
    {
        $call = factory(Call::class)->create();

        $notification = factory(Notification::class)->create();

        $call->setRelation('notification', $notification);

        $this->assertEquals($call->notification->id, $notification->id);
    }
}
