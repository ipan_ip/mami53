<?php

namespace App\Entities\Activity;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class TrackingFeatureTest extends MamiKosTestCase
{
    use WithFaker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->setUpFaker();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testRoom(): void
    {
        $room = factory(Room::class)->create();
        $feature = factory(TrackingFeature::class)->create(['identifier' => $room->id]);

        $this->assertTrue($feature->room->is($room));
    }

    /**
    * @group PMS-495
    */
    public function testInsertToWithTypeNotInTrackingFeatureType()
    {
        $room = factory(Room::class)->create();
        $data = [
            'type' => '-',
            'identifier' => $room->id,
            'device' => 'device',
        ];

        $this->assertFalse(TrackingFeature::insertTo($data));
    }

    /**
    * @group PMS-495
    */
    public function testInsertToWithTypeInTrackingFeatureType()
    {
        $room = factory(Room::class)->create();
        $data = [
            'type' => $this->faker->randomElement(TrackingFeature::TYPE),
            'identifier' => $room->id,
            'device' => 'device',
        ];

        TrackingFeature::insertTo($data);

        $this->assertDatabaseHas('tracking_feature', [
            'for' => $data['type'],
            'identifier' => $data['identifier'],
            'total' => 1,
            'from' => $data['device'],
        ]);
    }
}
