<?php

namespace App\Entities\Activity;

use App\Entities\Config\AppConfig;
use ReflectionClass;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ChatAdminTest extends MamikosTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDefaultAdminIdForBookingIsValid()
    {
        $this->assertTrue(ChatAdmin::getDefaultAdminIdForBooking() > 0);
    }

    public function testgetConsultantOrAdminIdForRoom()
    {
        $rooms = [
            factory(Room::class)->make(['id' => rand(1, 1000), 'is_mamirooms' => false]),
            factory(Room::class)->make(['id' => rand(1, 1000), 'is_mamirooms' => true])
        ];

        foreach ($rooms as $room) {
            $id = ChatAdmin::getConsultantOrAdminIdForRoom($room);
            $this->assertInternalType("int", $id);
        }
        
    }

    public function testGetRandomAdminIdForTenantsWithoutOnlineAdmin(): void
    {
        $adminId = ChatAdmin::getRandomAdminIdForTenants();
        $this->assertTrue($adminId > 0);
    }

    public function testGetRandomAdminIdForTenantsWithOnlineAdmin(): void
    {
        $chatAdmin = new ReflectionClass(ChatAdmin::class);
        $constAdminId = $chatAdmin->getConstants()['DEFAULT_ADMIN_ID_FOR_TENANT'];
        
        factory(AppConfig::class)->create([
            'name' => 'cs_'. $constAdminId .'_on',
            'value' => '1'
        ]);

        $adminId = ChatAdmin::getRandomAdminIdForTenants();
        $this->assertEquals($constAdminId, $adminId);
    }

    public function testGetDefaultAdminIdForMarketplace(): void
    {
        $chatAdmin = new ReflectionClass(ChatAdmin::class);
        $chatAdminConst = $chatAdmin->getConstants();
        $marketplaceAdminId = ChatAdmin::getDefaultAdminIdForMarketplace();
        $this->assertEquals(
            $chatAdminConst['DEFAULT_ADMIN_ID_FOR_MARKETPLACE'],
            $marketplaceAdminId
        );
    }

    public function testGetAdminIdAndDisplayNames(): void
    {
        $adminDisplayName = ChatAdmin::getAdminIdAndDisplayNames();
        $this->assertNotEmpty($adminDisplayName);
    }

    public function testGetAdminIdsForRatingCSPost(): void
    {
        $adminIdsForRatingCSPost = ChatAdmin::getAdminIdsForRatingCSPost();
        $this->assertNotEmpty($adminIdsForRatingCSPost);
    }

    public function testGetAdminIdsForMarketplace(): void
    {
        $adminIdsForMarketplace = ChatAdmin::getAdminIdsForMarketplace();
        $this->assertNotEmpty($adminIdsForMarketplace);
    }
}
