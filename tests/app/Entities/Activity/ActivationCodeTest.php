<?php
namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;
use Mockery;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Cache;

class ActivationCodeTest extends MamikosTestCase
{
        /**
    * @link http://docs.mockery.io/en/latest/cookbook/mocking_hard_dependencies.html
    *
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testGenerateActivationCode()
    {
        $trckMock = Mockery::mock('overload:App\Entities\Activity\Tracking');
        $trckMock->shouldReceive('checkDevice')
            ->andReturn("mobile");

        $apiHlpMock = Mockery::mock('overload:App\Http\Helpers\ApiHelper');
        $apiHlpMock->shouldReceive('random_string')
            ->andReturn("2354");

        $actvCode = ActivationCode::generateActivationCode('6281234567890', "mailtoken");

        $this->assertEquals('6281234567890', $actvCode->phone_number);
        $this->assertEquals('mobile', $actvCode->device);
        $this->assertEquals('2354', $actvCode->code);
        $this->assertEquals('mailtoken', $actvCode->for);

        $minExprAt = Carbon::now()->addHours(5);
        $this->assertTrue(Carbon::parse($actvCode->expired_at)->gte($minExprAt));
    }

    public function testOwnerVerificationSuccess()
    {
        Event::fake();
        // Anw, I tried to mock User clas but somehow it's unseccessful
        // Mockery give error: Could not load mock User\App, class already exists
        $user = factory(\App\User::class)->create(["phone_number" => "6281234567899"]);
        $phnNum = $user->phone_number;

        // cannot mock it since the method calls it's self class.
        factory(ActivationCode::class)->create([
            "phone_number" => $phnNum,
            "for" => "owner_verification",
            "code" => "0164"
        ]);

        $verv = ActivationCode::ownerVerification($user, ['code' => '0164']);
        $this->assertTrue($verv["status"]);
        $this->assertEquals("Success", $verv["message"]);
    }

    public function testOwnerVerificationFail()
    {
        Event::fake();
        $user = factory(\App\User::class)->create(["phone_number" => "6281234567899"]);
        $phnNum = $user->phone_number;

        factory(ActivationCode::class)->create([
            "phone_number" => $phnNum,
            "for" => "owner_verification",
            "code" => "6898"
        ]);

        $verv = ActivationCode::ownerVerification($user, ['code' => '2BMW']);
        $this->assertFalse($verv["status"]);
        $this->assertEquals("Gagal verifikasi", $verv["message"]);
    }

    public function testOwnerVerificationChangePhoneSuccess()
    {
        Event::fake();
        $user = factory(\App\User::class)->create(["phone_number" => "6281234567899"]);
        $phnNum = $user->phone_number;

        factory(ActivationCode::class)->create([
            "phone_number" => $phnNum,
            "for" => "change",
            "code" => "2143"
        ]);

        Cache::shouldReceive('get')
            ->once()
            ->with('changephoneuser:'.$user->id)
            ->andReturn('6281234567899');

        Cache::shouldReceive('getStore')
            ->once()
            ->andReturn(true);

        $verv = ActivationCode::ownerVerification($user, ['code' => '2143', 'change_phone' => "true"]);
        $this->assertTrue($verv["status"]);
        $this->assertEquals("Success", $verv["message"]);
    }

    public function testOwnerVerificationChangePhoneFail()
    {
        Event::fake();
        $user = factory(\App\User::class)->create(["phone_number" => "6281234567899"]);
        $phnNum = $user->phone_number;

        factory(ActivationCode::class)->create([
            "phone_number" => $phnNum,
            "for" => "change",
            "code" => "6543"
        ]);

        Cache::shouldReceive('get')
            ->once()
            ->with('changephoneuser:'.$user->id)
            ->andReturn('6281234567899');

        $verv = ActivationCode::ownerVerification($user, ['code' => '6898', 'change_phone' => "true"]);
        $this->assertFalse($verv["status"]);
        $this->assertEquals("Gagal verifikasi", $verv["message"]);
    }

    public function testDeleteLastCode()
    {
        $phnNum = "6281209876543";
        factory(ActivationCode::class)->create([
            "phone_number" => $phnNum,
            "for" => "change",
            "code" => "6543",
            "created_at" => Carbon::now()->addHours(-1)
        ]);

        $res = ActivationCode::deleteLastCode($phnNum);
        $this->assertTrue($res);
    }
    
    public function testDeleteLastCodeFalse()
    {
        $phnNum = "6281209876542";

        $res = ActivationCode::deleteLastCode($phnNum);
        $this->assertFalse($res);
    }    
}