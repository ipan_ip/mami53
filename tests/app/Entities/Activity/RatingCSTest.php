<?php

namespace App\Entities\Activity;

use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Activity\RatingCS;

class RatingCSTest extends MamiKosTestCase
{
    public function testUserRelationFound()
    {
        $user = factory(User::class)->create();
        $ratingCS = factory(RatingCS::class)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $ratingCS->user);
    }
}