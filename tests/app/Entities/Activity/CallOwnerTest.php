<?php

namespace App\Entities\Activity;

use App\Entities\Notif\SettingNotif;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Libraries\SMSLibrary;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Mockery;
use Monolog\Handler\RotatingFileHandler;
use Illuminate\Support\Facades\Log;

class CallOwnerTest extends MamikosTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->activity = new CallOwner;
        $mockSMSLibrary = $this->mockPartialAlternatively(SMSLibrary::class);
        $mockSMSLibrary->shouldReceive('send');
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testIsAllowedNotificationByUpdateDateWithRoomUpdateTimeGreaterThanAllowedUpdatedDate()
    {
        $room = factory(Room::class)->create([
            'created_at' => now()
        ]);

        $result = $this->activity->isAllowedNotificationByUpdateDate($room);
        $this->assertFalse($result);
    }

    public function testIsAllowedNotificationByUpdateDateWithRoomAailableIs0AndRoomUpdateTimeGreaterThanAllowedUpdateDateFull()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(10),
            'room_available' => 0
        ]);

        $result = $this->activity->isAllowedNotificationByUpdateDate($room);
        $this->assertFalse($result);
    }

    public function testIsAllowedNotificationShouldRoomAvailableIsNot0AndRoomUpdateTimeGreaterThanEquals30Days()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'room_available' => 1
        ]);

        $result = $this->activity->isAllowedNotificationByUpdateDate($room);
        $this->assertTrue($result);
    }

    public function testMarkCallAsOwnerSent()
    {
        $room = factory(Room::class)->create();
        $calls = factory(Call::class,5)->create([
            'designer_id' => $room->id,
            'owner_sent' => 'false',
        ]);
        $this->activity->markCallAsOwnerSent($room->id);

        $calls = Call::where('designer_id', $room->id)->get();
        foreach ($calls as $call) {
            $this->assertEquals($call->owner_sent, 'true');
        }
    }
}
