<?php
namespace App\Entities\Activity;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Event;
use SendBird;

class AutoChatTest extends MamikosTestCase
{
    public function testReplyWithAdminId(){
        // Admin id Ines from app/Entities/Activity/ChatAdmin.php
        $adminId = 12041;
        $room = factory(Room::class)->make();
        $replyData = [
            'group_id' => 1,
            'is_premium' => false,
            'admin_id' => $adminId
        ];

        // mocking SendBird facade
        SendBird::shouldReceive("sendMessageToGroupChannel")
            ->andReturn(true);

        $autoChat = new AutoChat($room, $replyData);
        $resp = $autoChat->reply();
        $this->assertEquals("Pesan anda telah kami sampaikan kepada pemilik kost. untuk survei bisa langsung datang ke lokasi berdasarkan waktu yang telah anda tentukan. Apabila ada kendala bisa menghubungi kami kembali. Terima kasih", $resp["message"]);
    }

    public function testReplyWithAdminIdNull(){
        // mocking SendBird facade
        SendBird::shouldReceive("sendMessageToGroupChannel")
            ->andReturn(true);

        // Admin id Ines from app/Entities/Activity/ChatAdmin.php
        $inesId = 12041;
        $grpId = 1;
        SendBird::shouldReceive("getGroupChannelDetail")
            ->with($grpId)
            ->andReturn([
                'members' => [
                    ['user_id' => $inesId]
                ]
            ]);

        $room = factory(Room::class)->make();
        $replyData = [
            'group_id' => $grpId,
            'is_premium' => false,
            'admin_id' => null
        ];
        $autoChat = new AutoChat($room, $replyData);
        $resp = $autoChat->reply();
        $this->assertEquals("Pesan anda telah kami sampaikan kepada pemilik kost. untuk survei bisa langsung datang ke lokasi berdasarkan waktu yang telah anda tentukan. Apabila ada kendala bisa menghubungi kami kembali. Terima kasih", $resp["message"]);
    }

    public function testGetGroupInfoChannelExists()
    {
        // Admin id Ines from app/Entities/Activity/ChatAdmin.php
        $adminId = 12041;
        $grpId = 10;
        SendBird::shouldReceive("getGroupChannelDetail")
            ->with($grpId)
            ->andReturn([
                'members' => [
                    ['user_id' => $adminId]
                ]
            ]);
        
        $room = factory(Room::class)->make();
        $replyData = [
            'group_id' => $grpId,
            'is_premium' => false,
            'admin_id' => null
        ];
        $autoChat = new AutoChat($room, $replyData);
        $grpInfo = $autoChat->getGroupInfo($grpId);

        $this->assertNull($grpInfo);
        $this->assertEquals($adminId, $this->getNonPublicAttributeFromClass('adminId', $autoChat));
    }

    public function testGetGroupInfoChannelNotExists()
    {
        $grpId = 10;

        // channel not exists
        SendBird::shouldReceive("getGroupChannelDetail")
            ->with($grpId)
            ->andReturn(null);
        
        $room = factory(Room::class)->make();
        $replyData = [
            'group_id' => $grpId,
            'is_premium' => false,
            'admin_id' => null
        ];
        $autoChat = new AutoChat($room, $replyData);
        $grpInfo = $autoChat->getGroupInfo($grpId);

        $this->assertNull($grpInfo);
        $this->assertEquals(0, $this->getNonPublicAttributeFromClass('adminId', $autoChat));
    }
}