<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;
use App\User;

class TrackingTest extends MamiKosTestCase
{
    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $tracking = factory(Tracking::class)->create(['user_id' => $user->id]);

        $this->assertTrue($tracking->user->is($user));
    }
}
