<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;

class ActivationCodeDeliveryReportTest extends MamiKosTestCase
{
    public function testActivationCode(): void
    {
        $code = factory(ActivationCode::class)->create();
        $report = factory(ActivationCodeDeliveryReport::class)->create(['user_verification_code_id' => $code->id]);

        $this->assertTrue($report->activation_code->is($code));
    }
}
