<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;

class AppSchemesTest extends MamikosTestCase
{
    /** @test */
    public function get_package_list_scheme_valid()
    {
        $this->assertEquals("owner_profile?go_list_package=true", AppSchemes::getPackageList());
    }

    /** @test */
    public function get_room_update_scheme_valid()
    {
        $this->assertEquals("owner_profile?room_update=1", AppSchemes::roomUpdate());
    }

    /** @test */
    public function get_room_detail_scheme_valid()
    {
        $this->assertEquals("owner_profile?kos=123", AppSchemes::roomDetail(123));
    }

    /** @test */
    public function get_room_detail_with_id_scheme_valid()
    {
        $this->assertEquals("room/123", AppSchemes::roomDetailId(123));
    }

    /** @test */
    public function get_homepage_valid()
    {
        $this->assertEquals('home', AppSchemes::getHomepage());
    }

    /** @test */
    public function get_main_search_valid()
    {
        $this->assertEquals('main_search', AppSchemes::getMainSearch());
    }

    /** @test */
    public function get_landing_detail_valid()
    {
        $this->assertEquals('kost/', AppSchemes::getLandingDetail());
    }

    /** @test */
    public function get_setting_page_valid()
    {
        $this->assertEquals('setting', AppSchemes::getSetting());
    }

    /** @test */
    public function get_favorite_page_valid()
    {
        $this->assertEquals('main_fav', AppSchemes::getRoomFavorite());
    }

    /** @test */
    public function get_recommendation_page_valid()
    {
        $this->assertEquals('main_recommendation', AppSchemes::getRoomRecommendation());
    }

    /** @test */
    public function get_info_page_valid()
    {
        $this->assertEquals('main_home', AppSchemes::getInfoPage());
    }

    /** @test */
    public function get_room_detail_slug_valid()
    {
        $this->assertEquals('rooms/slug', AppSchemes::getRoomDetailSlug('slug'));
    }

    /** @test */
    public function get_room_detail_slug_invalid()
    {
        $this->assertNotEquals('rooms/123', AppSchemes::getRoomDetailSlug('slug'));
    }

    /** @test */
    public function get_room_detail_webview_invalid()
    {
        $this->assertNotEquals('room_webview/123', AppSchemes::getRoomDetailWebView('slug'));
    }

    /** @test */
    public function get_room_detail_webview_valid()
    {
        $this->assertEquals('room_webview/slug', AppSchemes::getRoomDetailWebView('slug'));
    }

    /** @test */
    public function get_detail_webview_valid()
    {
        $this->assertEquals('web/', AppSchemes::getWebView());
    }

    /** @test */
    public function get_register_owner_valid()
    {
        $this->assertEquals('register_owner', AppSchemes::getOwnerRegisterPage());
    }

    /** @test */
    public function get_login_owner_valid()
    {
        $this->assertEquals('login_owner', AppSchemes::getOwnerLoginPage());
    }
    
    /** @test */
    public function get_owner_history_survey_valid()
    {
        $this->assertEquals('owner_survey/', AppSchemes::getOwnerSurveyHistory());
    }
    
    /** @test */
    public function get_owner_call_history_valid()
    {
        $this->assertEquals('owner_call/123', AppSchemes::getOwnerCallHistory(123));
    }

    /** @test */
    public function get_owner_chat_history_valid()
    {
        $this->assertEquals('owner_chat/123', AppSchemes::getOwnerChatHistory(123));
    }

    /** @test */
    public function get_owner_review_history_valid()
    {
        $this->assertEquals('owner_review/123', AppSchemes::getOwnerReviewHistory(123));
    }

    /** @test */
    public function get_room_list_valid()
    {
        $this->assertEquals('owner_ads', AppSchemes::getOwnerListRoom());
    }

    /** @test */
    public function get_booking_voucher_valid()
    {
        $this->assertEquals('voucher_booking?booking_code=cDFSK', AppSchemes::getBookingVoucher("cDFSK"));
    }

    /** @test */
    public function get_add_kost_form_valid()
    {
        $this->assertEquals('addkostV2', AppSchemes::getRoomInputPage());
    }

    /** @test */
    public function get_search_kost_valid()
    {
        $this->assertEquals('search_kost', AppSchemes::getRoomSearchPage());
    }

    /** @test */
    public function get_profile_page_valid()
    {
        $this->assertEquals('user', AppSchemes::getUserProfilePage());
    }

    /** @test */
    public function get_vacancy_detail_valid()
    {
        $this->assertEquals('lowongan/123', AppSchemes::getVacancyDetail(123));
    }

    /** @test */
    public function get_apartment_detail_valid()
    {
        $this->assertEquals('apartment_detail/123', AppSchemes::getApartmentDetail(123));
    }

    /** @test */
    public function get_booking_list_valid()
    {
        $this->assertEquals('list_booking', AppSchemes::getBookingListPage());
    }

    /** @test */
    public function get_user_survey_valid()
    {
        $this->assertEquals('survey', AppSchemes::getUserSurveyPage());
    }

    /** @test */
    public function get_company_profile_valid()
    {
        $this->assertEquals('perusahaan/profile', AppSchemes::getCompanyProfilePage());
    }

    /** @test */
    public function get_notification_page_valid()
    {
        $this->assertEquals('notifications', AppSchemes::getNotificationPage());
    }

    /** @test */
    public function get_booking_detail_valid()
    {
        $this->assertEquals('booking_detail/', AppSchemes::getBookingDetail());
    }

    /** @test */
    public function get_owner_profile_mamipay_valid()
    {
        $this->assertEquals('pay.mamikos.com://owner_profile', AppSchemes::getOwnerprofileMamipay());
    }

    /** @test */
    public function get_show_tutorial_in_owner_page()
    {
        $this->assertEquals('owner_profile?is_tutorial=true', AppSchemes::showTutorialInOwnerpage());
    }
}