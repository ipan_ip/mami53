<?php

namespace App\Entities\Activity;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\User;
use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp3;

class ViewTest extends MamiKosTestCase
{
    public function testAddOrUpdateForTheFirstTime()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        View::addOrUpdate($room, $user);

        $result = View::GetViewCountWithType($room->id);
        $this->assertEquals(1, $result);
    }

    public function testAddOrUpdateForTheSecondTime()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        View::addOrUpdate($room, $user);
        View::addOrUpdate($room, $user);

        $result =View::GetViewCountWithType($room->id);
        $this->assertEquals(2, $result);
    }

    public function testGetViewCountFromAds()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        View::addOrUpdate($room, $user);
        View::addOrUpdate($room, $user);
        View::addOrUpdate($room, $user, null, true);

        $result = View::GetViewAdsCountWithType($room->id);
        $this->assertEquals(1, $result);
    }

    public function testGetViewCountFromViews()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        View::addOrUpdate($room, $user);
        View::addOrUpdate($room, $user);
        View::addOrUpdate($room, $user, null, true);

        $result = View::GetViewCountWithType($room->id);
        $this->assertEquals(3, $result);
    }

    public function testRoom(): void
    {
        $room = factory(Room::class)->create();
        $view = factory(View::class)->create(['designer_id' => $room->id]);

        $this->assertTrue($view->room->is($room));
    }
}
