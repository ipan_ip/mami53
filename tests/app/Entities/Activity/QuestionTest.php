<?php

namespace App\Entities\Activity;

use App\Entities\Activity\Question;
use App\Test\MamiKosTestCase;

class QuestionTest extends MamiKosTestCase
{
    public const VALID_STRING = "Tanya alamat ?";
    public const INVALID_STRING = "Berapa Harga Per Bulan ?";
    public const FOR_TYPE = 'kos';

    public function testGetQuestionByString()
    {
        $expectedData = Question::where('question', self::VALID_STRING)
            ->where('for', self::FOR_TYPE)
            ->select(
                [
                    'id',
                    'question',
                ]
            )
            ->first();

        $this->assertEquals(
            $expectedData,
            Question::getQuestionByString(
                [
                    "string" => self::VALID_STRING,
                    "for"    => self::FOR_TYPE,
                ]
            )
        );

        $this->assertNull(
            Question::getQuestionByString(
                [
                    "string" => self::INVALID_STRING,
                    "for"    => self::FOR_TYPE,
                ]
            )
        );
    }

    public function testChatAnswer(): void
    {
        $question = factory(Question::class)->create();
        $answer = factory(ChatAnswer::class)->create(['chat_question_id' => $question->id]);

        $this->assertTrue($question->chat_answer[0]->is($answer));
    }
}
