<?php

namespace app\Entities\Property;

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Test\MamiKosTestCase;
use App\User;

class PropertyContractTest extends MamiKosTestCase
{
    public function testPropertiesRelationship()
    {
        $property = factory(Property::class)->create();
        $propertyContract = factory(PropertyContract::class)->create();
        $propertyContract->properties()->attach($property);

        $this->assertEquals($property->id, $propertyContract->properties()->first()->id);
    }

    public function testPropertiesRelationshipWithManyProperties()
    {
        $this->withExceptionHandling();
        $property = factory(Property::class)->create();
        $otherProperty = factory(Property::class)->create();

        $propertyContract = factory(PropertyContract::class)->create();

        $propertyContract->properties()->attach($property);
        $propertyContract->properties()->attach($otherProperty);

        $this->assertEquals(2, count($propertyContract->properties));
    }

    public function testPropertyLevelRelationship()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id
        ]);
        $this->assertEquals(
            $propertyLevel->id,
            $propertyContract->property_level->id
        );
    }

    public function testAssignedByRelationship()
    {
        $user = factory(User::class)->create();
        $propertyContract = factory(PropertyContract::class)->create([
            'assigned_by' => $user->id
        ]);
        $this->assertEquals(
            $user->id,
            $propertyContract->assigned_by_user->id
        );
    }

    public function testEndedByRelationship()
    {
        $user = factory(User::class)->create();
        $propertyContract = factory(PropertyContract::class)->create([
            'ended_by' => $user->id
        ]);
        $this->assertEquals(
            $user->id,
            $propertyContract->ended_by_user->id
        );
    }

    public function testGetIsRecentlyTerminatedAttribute()
    {
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_INACTIVE
        ]);
        $contract->status = PropertyContract::STATUS_TERMINATED;
        $contract->save();
        $this->assertTrue($contract->is_recently_terminated);
    }

    public function testUpdatedByUser()
    {
        $expected = factory(User::class)->create();
        $contract = factory(PropertyContract::class)->create(['updated_by' => $expected->id]);

        $this->assertTrue($contract->updated_by_user->is($expected));
    }
}
