<?php

namespace App\Entities\Property;

use App\Test\MamiKosTestCase;

class PropertyGenderTest extends MamiKosTestCase
{
    protected $withDbTransaction = false;

    public function testCasting()
    {
        $this->assertEquals(PropertyGender::Mixed(), PropertyGender::fromValue(0));
        $this->assertEquals(PropertyGender::Male(), PropertyGender::fromValue(1));
        $this->assertEquals(PropertyGender::Female(), PropertyGender::fromValue(2));
    }
}