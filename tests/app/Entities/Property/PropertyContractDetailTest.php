<?php

namespace App\Entities\Property;

use App\Test\MamiKosTestCase;

class PropertyContractDetailTest extends MamiKosTestCase
{
    public function testPropertyContract(): void
    {
        $contract = factory(PropertyContract::class)->create();
        $detail = factory(PropertyContractDetail::class)->create(['property_contract_id' => $contract->id]);

        $this->assertTrue(
            $detail->property_contract->is($contract)
        );
    }

    public function testProperty(): void
    {
        $property = factory(Property::class)->create();
        $detail = factory(PropertyContractDetail::class)->create(['property_id' => $property->id]);

        $this->assertTrue(
            $detail->property->is($property)
        );
    }
}
