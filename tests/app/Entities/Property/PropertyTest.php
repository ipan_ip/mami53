<?php

namespace app\Entities\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Revision\Revision;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class PropertyTest extends MamiKosTestCase
{
    public function testRoomsRelationship()
    {
        $property = factory(Property::class)->create();
        $room = factory(Room::class)->create(['property_id' => $property->id]);
        $this->assertEquals($room->id, $property->rooms->first()->id);
    }

    public function testOwnerUserRelationship()
    {
        $user = factory(User::class)->create(['is_owner' => 'true']);
        $property = factory(Property::class)->create([
            'owner_user_id' => $user->id
            ]);
        $this->assertEquals($user->id, $property->owner_user->id);
    }

    public function testPropertyContractsRelationship()
    {
        $property = factory(Property::class)->create();
        $propertyContract = factory(PropertyContract::class)->create(['status' => 'active']);

        $propertyContract->properties()->attach($property);

        $this->assertEquals(
            $propertyContract->id,
            $property->property_contracts->first()->id
        );
    }

    public function testPropertyActiveContractRelationship()
    {
        $property = factory(Property::class)->create();
        $propertyContract = factory(PropertyContract::class)->create(['status' => 'active']);

        $propertyContract->properties()->attach($property);

        $this->assertEquals(
            $propertyContract->id,
            $property->property_active_contract->id
        );
    }

    public function testGetHasActiveContractAttributeWithoutLoadRelation()
    {
        $property = factory(Property::class)->create();
        $this->assertFalse($property->has_active_contract);
    }

    public function testGetHasActiveContractAttributeWithLoadRelation()
    {
        $property = factory(Property::class)->create();
        $propertyContract = factory(PropertyContract::class)->create(['status' => 'active']);
        $propertyContract->properties()->attach($property);

        $this->assertTrue($property->has_active_contract);
    }

    public function testRevisionHistoryRelationship()
    {
        $property = factory(Property::class)->create();
        factory(Revision::class)->create([
            'revisionable_type' => Property::class,
            'revisionable_id' => $property->id
        ]);
        $result = $property->revisionHistory;
        $this->assertInstanceOf(Revision::class, $result->first());
    }
}
