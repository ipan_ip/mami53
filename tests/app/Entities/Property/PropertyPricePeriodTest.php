<?php

namespace App\Entities\Property;

use App\Test\MamiKosTestCase;

class PropertyPricePeriodTest extends MamiKosTestCase
{
    protected $withDbTransaction = false;

    public function testCasting()
    {
        $this->assertEquals(PropertyPricePeriod::None(), PropertyPricePeriod::fromValue(-1));
        $this->assertEquals(PropertyPricePeriod::Daily(), PropertyPricePeriod::fromValue(0));
        $this->assertEquals(PropertyPricePeriod::Weekly(), PropertyPricePeriod::fromValue(1));
        $this->assertEquals(PropertyPricePeriod::Monthly(), PropertyPricePeriod::fromValue(2));
        $this->assertEquals(PropertyPricePeriod::Yearly(), PropertyPricePeriod::fromValue(3));
        $this->assertEquals(PropertyPricePeriod::Quarterly(), PropertyPricePeriod::fromValue(4));
        $this->assertEquals(PropertyPricePeriod::Semiannually(), PropertyPricePeriod::fromValue(5));
    }

    public function testGetLowercaseKeys()
    {
        $this->assertEquals('daily', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Daily()));
        $this->assertEquals('weekly', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Weekly()));
        $this->assertEquals('monthly', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Monthly()));
        $this->assertEquals('yearly', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Yearly()));
        $this->assertEquals('quarterly', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Quarterly()));
        $this->assertEquals('semiannually', PropertyPricePeriod::getLowercaseKey(PropertyPricePeriod::Semiannually()));
    }
}