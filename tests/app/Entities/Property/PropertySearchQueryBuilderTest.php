<?php

namespace app\Entities\Property;

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class PropertySearchQueryBuilderTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->propertyA = factory(Property::class)->create(['name' => 'property A']);
        $this->propertyA = factory(Property::class)->create(['name' => 'property B']);
    }

    public function testGenerateWithoutParams()
    {
        $builder = new PropertySearchQueryBuilder([]);
        $query = $builder->generate();
        $result = $query->get();
        $this->assertNotEmpty($result);
    }

    public function testGenerateWithPropertyName()
    {
        $params = ['property_name' => $this->propertyA->name];
        $builder = new PropertySearchQueryBuilder($params);
        $query = $builder->generate();
        $result = $query->get();
        $this->assertNotEmpty($result);
        $this->assertEquals($this->propertyA->id, $result->first()->id);
        $this->assertCount(1, $result);
    }

    public function testGenerateWithOwner()
    {
        $user = factory(User::class)->create([
            'name' => 'aha',
            'phone_number' => '082132232323'
        ]);
        $this->propertyA->owner_user_id = $user->id;
        $this->propertyA->save();
        $params = [
            'owner_name' => $user->name,
            'owner_phone_number' => $user->phone_number
        ];
        $builder = new PropertySearchQueryBuilder($params);
        $query = $builder->generate();
        $result = $query->get();
        $this->assertNotEmpty($result);
        $this->assertEquals($this->propertyA->id, $result->first()->id);
        $this->assertCount(1, $result);
    }

    public function testGenerateWithLevel()
    {
        $level = factory(PropertyLevel::class)->create();
        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $level->id,
            'status' => PropertyContract::STATUS_ACTIVE
        ]);
        $propertyContract->properties()->attach($this->propertyA);
        $params = ['levels' => [$level->id]];
        $builder = new PropertySearchQueryBuilder($params);
        $query = $builder->generate();
        $result = $query->get();
        $this->assertNotEmpty($result);
        $this->assertEquals($this->propertyA->id, $result->first()->id);
        $this->assertCount(1, $result);
    }

    public function testGenerateWithLevelShouldNotReturnInactiveContract()
    {
        $level = factory(PropertyLevel::class)->create();
        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $level->id,
            'status' => PropertyContract::STATUS_INACTIVE
        ]);
        $propertyContract->properties()->attach($this->propertyA);

        $params = ['levels' => [$level->id]];
        $builder = new PropertySearchQueryBuilder($params);
        $query = $builder->generate();
        $result = $query->get();
        $this->assertEmpty($result);
        $this->assertCount(0, $result);
    }

    public function testWith(): void
    {
        $query = Mockery::mock(PropertySearchQueryBuilder::class)->makePartial();

        $this->assertInstanceOf(PropertySearchQueryBuilder::class, $query->with(['rooms']));
    }

    public function testWithCount(): void
    {
        $query = Mockery::mock(PropertySearchQueryBuilder::class)->makePartial();

        $this->assertInstanceOf(PropertySearchQueryBuilder::class, $query->withCount(['rooms']));
    }
}
