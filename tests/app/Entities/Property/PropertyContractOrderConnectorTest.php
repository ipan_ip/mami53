<?php

namespace app\Entities\Property;

use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractOrderConnector;
use App\Http\Helpers\MamipayApiHelper;
use App\Test\MamiKosTestCase;

class PropertyContractOrderConnectorTest extends MamiKosTestCase
{
    public function testCreateOrderAndSuccess()
    {
        $helperMock = $this->mockAlternatively(MamipayApiHelper::class);
        $helperMock->shouldReceive('makeRequest');
        $contract = factory(PropertyContract::class)->make(['id' => 123]);
        $conn = new PropertyContractOrderConnector($helperMock);
        $conn->createOrder($contract);
        $this->assertTrue(true);
    }
}
