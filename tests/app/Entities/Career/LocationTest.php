<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class LocationTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $relation = (new Location)->vacancy();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsToMany', $relation);
        $this->assertInstanceOf('App\Entities\Career\Vacancy', $relation->getRelated());
        $this->assertEquals('career_location_id', $relation->getForeignPivotKeyName());
    }
}