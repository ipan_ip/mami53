<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class RequirementTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $relation = (new Requirement)->vacancy();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Career\Vacancy', $relation->getRelated());
        $this->assertEquals('id', $relation->getForeignKey());
    }
}