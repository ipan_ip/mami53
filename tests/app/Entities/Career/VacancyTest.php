<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class VacancyTest extends MamiKosTestCase
{
    public function testPosition()
    {
        $relation = (new Vacancy)->position();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Career\Position', $relation->getRelated());
        $this->assertEquals('career_position_id', $relation->getForeignKey());
    }

    public function testLocations()
    {
        $relation = (new Vacancy)->locations();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsToMany', $relation);
        $this->assertInstanceOf('App\Entities\Career\Location', $relation->getRelated());
        $this->assertEquals('career_vacancy_id', $relation->getForeignPivotKeyName());
    }

    public function testRequirements()
    {
        $relation = (new Vacancy)->requirements();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Career\Requirement', $relation->getRelated());
        $this->assertEquals('career_vacancy_id', $relation->getForeignKeyName());
    }

    public function description()
    {
        $relation = (new Vacancy)->description();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Career\Description', $relation->getRelated());
        $this->assertEquals('career_vacancy_id', $relation->getForeignKeyName());
    }
}