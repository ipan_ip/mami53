<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class VacancyLocationTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $relation = (new VacancyLocation)->vacancy();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Career\Vacancy', $relation->getRelated());
        $this->assertEquals('id', $relation->getForeignKey());
    }

    public function testLocation()
    {
        $relation = (new VacancyLocation)->location();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Career\Location', $relation->getRelated());
        $this->assertEquals('id', $relation->getForeignKey());
    }
}