<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class DescriptionTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $relation = (new Description)->vacancy();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Career\Vacancy', $relation->getRelated());
        $this->assertEquals('vacancy_id', $relation->getForeignKey());
    }
}