<?php
namespace App\Entities\Career;

use App\Test\MamiKosTestCase;

class PositionTest extends MamiKosTestCase
{
    public function testVacancy()
    {
        $relation = (new Position)->vacancy();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Career\Vacancy', $relation->getRelated());
        $this->assertEquals('career_position_id', $relation->getForeignKeyName());
    }
}