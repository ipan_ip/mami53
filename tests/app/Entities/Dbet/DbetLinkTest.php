<?php

namespace App\Entities\Dbet;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class DbetLinkTest extends MamiKosTestCase
{

    public function testRoomIsNull(): void
    {
        // prepare data
        $data = factory(DbetLink::class)->create();

        // run test
        $this->assertNull($data->room);
    }

    public function testRoomIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $data = factory(DbetLink::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $this->assertNotNull($data->room);
    }
}