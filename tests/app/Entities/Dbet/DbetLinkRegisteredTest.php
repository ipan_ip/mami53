<?php

namespace App\Entities\Dbet;

use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class DbetLinkRegisteredTest extends MamiKosTestCase
{

    public function testRoomIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        // run test
        $this->assertNull($data->room);
    }

    public function testRoomIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $this->assertNotNull($data->room);
    }

    public function testRoomUnitIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        // run test
        $this->assertNull($data->room_unit);
    }

    public function testRoomUnitIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_room_id' => $roomUnit->id
        ]);

        // run test
        $this->assertNotNull($data->room_unit);
    }

    public function testIdentityIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        // run test
        $this->assertNull($data->identity);
    }

    public function testIdentityIsSuccess(): void
    {
        // prepare data
        $media = factory(MamipayMedia::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'identity_id' => $media->id
        ]);

        // run test
        $this->assertNotNull($data->identity);
    }

    public function testUserIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        // run test
        $this->assertNull($data->user);
    }

    public function testUserIsSuccess(): void
    {
        // prepare data
        $user = factory(User::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'user_id' => $user->id
        ]);

        // run test
        $this->assertNotNull($data->user);
    }
}