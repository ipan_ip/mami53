<?php

namespace App\Entities\Owner;

use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class StatusHistoryTest extends MamiKosTestCase
{
    use WithFaker;

    public function testOwner(): void
    {
        $expected = factory(RoomOwner::class)->create(['status' => $this->faker->randomElement(EditStatus::getValues())]);
        $history = factory(StatusHistory::class)->create(['designer_owner_id' => $expected->id, 'new_status' => $expected->status]);

        $this->assertTrue(
            $history->owner->is($expected)
        );
    }

    public function testCreator(): void
    {
        $expected = factory(User::class)->create();
        $history = factory(StatusHistory::class)->create(['created_by' => $expected->id]);

        $this->assertTrue(
            $history->creator->is($expected)
        );
    }
}
