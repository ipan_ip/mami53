<?php

namespace App\Entities\Owner;

use App\Entities\Owner\TopOwnerHistory;
use App\Test\MamiKosTestCase;
use App\User;

class TopOwnerHistoryTest extends MamiKosTestCase
{
    
    public function testCreateData()
    {
        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->store(1, 'active');

        $saved = TopOwnerHistory::where('user_id', 1)->get();

        $this->assertNotNull($topOwnerHistory);
        $this->assertNotNull($saved);

    }

    public function testCreateDataIdNullThrowsException()
    {
        $this->expectException(\PDOException::class);

        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->store(null, 'active');

    }

    public function testCreateDataStatusNullThrowsException()
    {
        $this->expectException(\PDOException::class);

        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->store(1, null);
    }

    public function testDeactivateWithStatus()
    {
        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->store(1, 'deactivate', 'reason');

        $saved = TopOwnerHistory::where('user_id', 1)->get();
        $this->assertNotNull($topOwnerHistory);
    }

    public function testDeactivateWithoutStatus()
    {
        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->store(1, 'deactivate', null);

        $saved = TopOwnerHistory::where('user_id', 1)->get();
        $this->assertNotNull($topOwnerHistory);
    }

    public function testEntityRelation()
    {
        [$user, $topOwnerEntity] = $this->createEntitySet();
        $userRetrieve = $topOwnerEntity->user()->first();

        $this->assertSame($user->name, $userRetrieve->name);
    }

    private function createEntitySet(array $userData = [], array $topOwnerData = []) :array
    {
        $userEntity = $this->createUserEntity($userData);
        $topOwnerEntity = $this->createTopOwnerEntity($userEntity, $topOwnerData);
        return [$userEntity, $topOwnerEntity];
    }

    private function createTopOwnerEntity(User $userEntity, array $overrideData = []): TopOwnerHistory
    {
        return factory(TopOwnerHistory::class)->create(
            array_merge(
                [
                    'user_id' => $userEntity->id
                ],
                $overrideData
            )
        );
    }

    private function createUserEntity(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }
}
