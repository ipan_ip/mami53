<?php

namespace app\Entities\Owner;

use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Room\Room;
use App\User;
use App\Test\MamiKosTestCase;

class SurveySatisfactionTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testRelation()
    {
        [$room, $user, $survey] = $this->createEntitiesSet();
        $roomRetrieve = $survey->room()->first();
        $userRetrieve = $survey->user()->first();

        $this->assertSame($room->name, $roomRetrieve->name, "Same Room");
        $this->assertSame($user->name, $userRetrieve->name, "Same User");
    }

    private function createEntitiesSet(
        array $roomData = [],
        array $userData = [],
        array $surveyData = []
    ): array {
        $roomEntity = $this->createRoomEntity($roomData);
        $userEntity = $this->createUserEntity($userData);
        $surveySatisfactionEntity = $this->createSurveySatisfactionEntity(
            $roomEntity,
            $userEntity,
            $surveyData
        );

        return [
            $roomEntity,
            $userEntity,
            $surveySatisfactionEntity
        ];
    }

    private function createRoomEntity(array $overrideData = []): Room
    {
        return factory(Room::class)->create($overrideData);
    }

    private function createUserEntity(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }

    private function createSurveySatisfactionEntity(Room $roomEntity, User $userEntity, array $overrideData = []): SurveySatisfaction
    {
        return factory(SurveySatisfaction::class)->create(
            array_merge(
                [
                    'identifier' => $roomEntity->id,
                    'user_id' => $userEntity->id
                ],
                $overrideData
            )
        );
    }


    public function testInsertDataPremiumExpired()
    {
        $data = array(
            "user_id"     => 1,
            "identifier"  => 3333,
            "for"         => 'premium_expired',
            "content"     => 'Kos sudah penuh',
        );
        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }


    public function testInsertDataUpdateRoom()
    {
        $data = array(
            "user_id"     => 1,
            "identifier"  => 3333,
            "for"         => 'update_room',
            "content"     => 'Kos sudah penuh',
        );
        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }


    public function testInsertDataWithoutUserId()
    {
        $this->expectException(\ErrorException::class);
        $data = array(
            "identifier"  => 3333,
            "for"         => 'update_room',
            "content"     => 'Kos sudah penuh',
        );
        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }


    public function testInsertDataWithoutIdentifier()
    {
        $this->expectException(\ErrorException::class);
        $data = array(
            "user_id"     => 1,
            "for"         => 'update_room',
            "content"     => 'Kos sudah penuh',
        );
        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }


    public function testInsertDataWithoutFor()
    {
        $this->expectException(\ErrorException::class);
        $data = array(
            "user_id"     => 1,
            "identifier"  => 3333,
            "content"     => 'Kos sudah penuh',
        );
        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }

    public function testInsertOrUpdateForUpdateRoom()
    {
        $dataUpdate = [
            'user_id'       => 2,
            'identifier'    => 2222,
            'for'           => 'update_room',
            'content'       => null
        ];

        $responseSurvey = SurveySatisfaction::insertOrUpdate($dataUpdate);
        $responseStatus = $responseSurvey['status'];
        $responseShowSurvey = $responseSurvey['show_survey'];

        $this->assertTrue($responseStatus);
        $this->assertTrue($responseShowSurvey);
    }

    public function testInsertOrUpdateForPremiumExpired()
    {
        $dataUpdate = [
            'user_id'       => 2,
            'identifier'    => 2222,
            'for'           => 'premium_expired',
            'content'       => null
        ];

        $responseSurvey = SurveySatisfaction::insertOrUpdate($dataUpdate);
        $responseStatus = $responseSurvey['status'];
        $responseShowSurvey = $responseSurvey['show_survey'];

        $this->assertTrue($responseStatus);
        $this->assertTrue($responseShowSurvey);
    }

    public function testInsertDataWithFullBookedRoom()
    {
        $data = [
            "user_id"     => 1,
            "identifier"  => 3333,
            "for"         => 'update_room',
            "content"     => 'Kamar masih terisi penuh',
            "room_count"  => 13
        ];

        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }

    public function testInsertDataWithDesiredPrice()
    {
        $data = [
            "user_id"     => 1,
            "identifier"  => 3333,
            "for"         => 'update_room',
            "content"     => 'Harga dan keuntungan tidak sesuai',
            "price"       => 'Rp 500.000'
        ];

        $isSaved = SurveySatisfaction::insertData($data);

        $this->assertTrue($isSaved);
    }


}