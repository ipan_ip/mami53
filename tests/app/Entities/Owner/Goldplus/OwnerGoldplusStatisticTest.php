<?php
namespace App\Entities\Owner\Goldplus;

use App\Test\MamiKosTestCase;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;

class OwnerGoldplusStatisticTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    /**
     * @group UG
     * @group UG-1535
     * @group App/Entities/Owner/Goldplus/OwnerGoldplusStatistic
     */
    public function testOwnerGoldplusStatisticShouldReturnSuccess()
    {
        $id = sha1(\uniqid('', true));
        $ownerGoldplusStatistic = factory(OwnerGoldplusStatistic::class)->make([
            'id' => $id
        ]);

        $this->assertEquals($id, $ownerGoldplusStatistic['id']);
    }
}
