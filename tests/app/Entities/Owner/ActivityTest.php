<?php

namespace App\Entities\Owner;

use App\Jobs\Owner\MarkActivity;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class ActivityTest extends MamiKosTestCase
{
    
    public function testCreationSuccess()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        Queue::fake();
        $userEntity->markActivity(ActivityType::TYPE_LOGIN);
        Queue::assertPushedTimes(MarkActivity::class, 1);
    }

    public function testSetLatestMark()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $activity = $userEntity->activity;

        $this->assertNull($activity->logged_in_at);
        $activity->setLatestMark(ActivityType::TYPE_LOGIN);
        $this->assertNotNull($activity->logged_in_at);
        $this->assertNull($activity->updated_property_at);
        $activity->setLatestMark(ActivityType::TYPE_UPDATE_PROPERTY);
        $this->assertNotNull($activity->updated_property_at);
    }

    public function testisShowingActivityPopupTrueForNewUser()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $userEntity->load('activity');
        $this->assertTrue($userEntity->activity->isShowingActivityPopup());
    }

    public function testisShowingActivityPopupFalseForLoggedInUser()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $job = new MarkActivity($userEntity->id, ActivityType::TYPE_LOGIN);
        $job->handle();
        $userEntity->load('activity');
        $this->assertFalse($userEntity->activity->isShowingActivityPopup());
    }

    public function testisShowingActivityPopupTruForOldLoggedInUser()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $job = new MarkActivity($userEntity->id, ActivityType::TYPE_LOGIN);
        $job->handle();
        $userEntity->load('activity');
        $userEntity->activity->logged_in_at = Carbon::now()->subDay(4);
        $this->assertTrue($userEntity->activity->isShowingActivityPopup());
    }

    public function testisShowingActivityPopupFalseForPropertyUpdatingUser()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $job = new MarkActivity($userEntity->id, ActivityType::TYPE_UPDATE_PROPERTY);
        $job->handle();
        $userEntity->load('activity');
        $this->assertFalse($userEntity->activity->isShowingActivityPopup());
    }

    public function testisShowingActivityPopupTrueForOldPropertyUpdatingUser()
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'true', 'is_verify' => 1]);
        $job = new MarkActivity($userEntity->id, ActivityType::TYPE_UPDATE_PROPERTY);
        $job->handle();
        $userEntity->load('activity');
        $userEntity->activity->updated_property_at = Carbon::now()->subDay(4);
        $this->assertTrue($userEntity->activity->isShowingActivityPopup());
    }
}
