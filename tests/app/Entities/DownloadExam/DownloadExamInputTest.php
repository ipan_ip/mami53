<?php

namespace App\Entities\DownloadExam;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DownloadExamInputTest extends MamiKosTestCase
{
    public function testUser()
    {
        $this->assertInstanceOf(BelongsTo::class, (new DownloadExamInput)->user());
    }

    public function testExam()
    {
        $this->assertInstanceOf(BelongsTo::class, (new DownloadExamInput)->exam());
    }
}