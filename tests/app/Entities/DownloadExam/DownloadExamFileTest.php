<?php

namespace App\Entities\DownloadExam;

use App\Test\MamiKosTestCase;
use Exception;
use Mockery;

class DownloadExamFileTest extends MamiKosTestCase
{
    private $mediaHandler;

    public function setUp()
    {
        parent::setUp();
        $this->mediaHandler = Mockery::mock('alias:App\Entities\Media\MediaHandler');
    }

    public function testUploadSuccess()
    {
        $this->mediaHandler->shouldReceive('getFileExtension');
        $this->mediaHandler->shouldReceive('generateFileName');
        $this->mediaHandler->shouldReceive('getOriginalFileDirectory');
        $this->mediaHandler->shouldReceive('getCacheFileDiretory');
        $this->mediaHandler->shouldReceive('storeFile')->twice();

        $res = DownloadExamFile::upload('');
        
        $this->assertIsArray($res);
    }

    public function testUploadExceptionThrown()
    {
        $this->mediaHandler->shouldReceive('getFileExtension');
        $this->mediaHandler->shouldReceive('generateFileName');
        $this->mediaHandler->shouldReceive('getOriginalFileDirectory');
        $this->mediaHandler->shouldReceive('getCacheFileDiretory');
        $this->mediaHandler->shouldReceive('storeFile')->andThrow(new Exception());

        $res = DownloadExamFile::upload('');
        
        $this->assertIsArray($res);
        $this->assertFalse($res['status']);
    }

    
    public function testGetCacheUrlDefault()
    {
        $downloadExamFile = new DownloadExamFile;
        $downloadExamFile->file_name = 'testing-file-name';
        $downloadExamFile->file_path = 'testing-file-path';
        $res = $downloadExamFile->getCacheUrl();

        $this->assertIsString($res);
    }

    public function testStripExtension()
    {
        $downloadExamFile = new DownloadExamFile;
        $downloadExamFile->file_name = 'testing-file-name.jpg';
        $res = $downloadExamFile->stripExtension();

        $this->assertIsString($res);
    }
}