<?php

namespace App\Entities\DownloadExam;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DownloadExamTest extends MamiKosTestCase
{
    public function testFiles()
    {
        $this->assertInstanceOf(HasMany::class, (new DownloadExam)->files());
    }

    public function testInputs()
    {
        $this->assertInstanceOf(HasMany::class, (new DownloadExam)->inputs());
    }

    public function testGetLabels()
    {
        $labelType = 'college';
        $expected = $this->getNonPublicAttributeFromClass('labels', (new DownloadExam));
        $res = (new DownloadExam)->getLabels($labelType);

        $this->assertIsArray($res);
        $this->assertSame($expected[$labelType], $res);        
    }
}