<?php

namespace App\Entities\Point;

use App\Entities\Point\PointActivity;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;

class PointActivityTest extends MamiKosTestCase
{
    public function testPointSettingRelation()
    {
        $activity = factory(PointActivity::class)->create([]);
        $settings = factory(PointSetting::class, 3)->create([
            'activity_id' => $activity->id
        ]);
        
        $this->assertEquals(3, $activity->settings->count());
    }

    public function testPointHistoryRelation()
    {
        $activity = factory(PointActivity::class)->create([]);
        $histories = factory(PointHistory::class, 3)->create([
            'activity_id' => $activity->id
        ]);
        
        $this->assertEquals(3, $activity->histories->count());
    }
}