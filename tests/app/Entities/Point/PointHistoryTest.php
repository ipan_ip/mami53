<?php

namespace App\Entities\Point;

use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Point\PointHistory;
use App\Test\MamiKosTestCase;
use App\User;

class PointHistoryTest extends MamiKosTestCase
{
    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $history = factory(PointHistory::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertEquals($user->id, $history->user->id);
    }

    public function testEarnableRelation()
    {
        $user = factory(User::class)->create([]);
        
        $invoice = factory(MamipayInvoice::class)->create();

        $history = factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'earnable_type' => 'room_contract',
            'earnable_id' => $invoice->id,
            'value' => 2,
        ]);

        $this->assertEquals($invoice->id, $history->earnable()->first()->id);
    }
}