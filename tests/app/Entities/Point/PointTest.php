<?php

namespace App\Entities\Point;

use App\Entities\Point\Point;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;

class PointTest extends MamiKosTestCase
{
    public function testPointSettingRelation()
    {
        $point = factory(Point::class)->create([]);
        $settings = factory(PointSetting::class, 3)->create([
            'point_id' => $point->id
        ]);
        
        $this->assertEquals(3, $point->settings->count());
    }
}