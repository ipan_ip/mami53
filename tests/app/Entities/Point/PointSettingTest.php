<?php

namespace App\Entities\Point;

use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;

class PointSettingTest extends MamiKosTestCase
{
    public function testPointOwnerRoomGroupRelation()
    {
        $ownerRoomGroup = factory(PointOwnerRoomGroup::class)->create([]);
        $setting = factory(PointSetting::class)->create([
            'room_group_id' => $ownerRoomGroup->id
        ]);
        
        $this->assertEquals($ownerRoomGroup->id, $setting->room_group->id);
    }
}