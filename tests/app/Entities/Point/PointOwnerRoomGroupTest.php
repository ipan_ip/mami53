<?php

namespace App\Entities\Point;

use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;

class PointOwnerRoomGroupTest extends MamiKosTestCase
{
    public function testPointSettingRelation()
    {
        $ownerGroup = factory(PointOwnerRoomGroup::class)->create([]);
        $settings = factory(PointSetting::class, 3)->create([
            'room_group_id' => $ownerGroup->id
        ]);
        
        $this->assertEquals(3, $ownerGroup->settings->count());
    }
}