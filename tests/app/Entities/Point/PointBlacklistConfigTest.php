<?php

namespace App\Entities\Point;

use App\Entities\Level\KostLevel;
use App\Entities\Point\PointBlacklistConfig;
use App\Test\MamiKosTestCase;

class PointBlacklistConfigTest extends MamiKosTestCase
{
    public function testKostLevelRelation()
    {
        $level = factory(KostLevel::class)->create([]);
        $config = factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_KOST_LEVEL,
            'value' => $level->id
        ]);
        
        $this->assertEquals($level->id, $config->level->id);
    }

    public function testGetUserTypeList()
    {
        $userTypes = PointBlacklistConfig::getUserTypeList();
        $this->assertEquals(1, count($userTypes));

        $this->assertEquals('Mamirooms', PointBlacklistConfig::getUserTypeList(PointBlacklistConfig::USERTYPE_MAMIROOMS));
    }
}