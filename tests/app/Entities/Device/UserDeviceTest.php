<?php

namespace App\Entities\Room;

use App\Entities\Device\UserDevice;
use App\Test\MamiKosTestCase;
use App\User;

class UserDeviceTest extends MamiKosTestCase
{

    public function test_get_is_rating_five_supported_return_valid_when_access_from_android_equal_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => UserDevice::MIN_RATING_5_APP_VERSION_ANDROID,
            ]);

        $this->assertTrue($device->getIsRatingFiveSupported());
    }

    public function test_get_is_rating_five_supported_return_valid_when_access_from_android_newer_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_RATING_5_APP_VERSION_ANDROID + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsRatingFiveSupported());
    }

    public function test_get_is_rating_five_supported_return_valid_when_access_from_android_older_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_RATING_5_APP_VERSION_ANDROID - 1), // older version
            ]);

        $this->assertFalse($device->getIsRatingFiveSupported());
    }

    public function test_get_is_rating_five_supported_return_valid_when_access_from_ios_older_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_RATING_5_APP_VERSION_IOS - 1), // older version
            ]);

        $this->assertFalse($device->getIsRatingFiveSupported());
    }

    public function test_get_is_rating_five_supported_return_valid_when_access_from_ios_equal_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => UserDevice::MIN_RATING_5_APP_VERSION_IOS,
            ]);

        $this->assertTrue($device->getIsRatingFiveSupported());
    }

    public function test_get_is_rating_five_supported_return_valid_when_access_from_ios_newer_version()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_RATING_5_APP_VERSION_IOS + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsRatingFiveSupported());
    }

    public function testGetIsFlashSaleSupportedWhenAccessFromAndroidOlderVersion()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID - 10 ), // older version
            ]);
        $this->assertFalse($device->getIsFlashSaleSupported());
    }

    public function testGetIsFlashSaleSupportedWhenAccessFromAndroidNewerVersion()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID + 10 ), // newer version
            ]);
        $this->assertTrue($device->getIsFlashSaleSupported());
    }

    public function testGetIsFlashSaleSupportedWhenAccessFromAndroidEqualVersion()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID ), // equals version
            ]);
        $this->assertTrue($device->getIsFlashSaleSupported());
    }

    public function testGetIsFlashSaleSupportedWhenAccessFromIos()
    {
        $device = factory(\App\Entities\Device\UserDevice::class)
            ->make([
                'platform' => 'ios',
            ]);
        $this->assertFalse($device->getIsFlashSaleSupported());
    }

    public function testGetIsSupportKosRuleReturnTrueWithEqualAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID,
            ]);

        $this->assertTrue($device->getIsSupportKosRule());
    }
    public function testGetIsSupportKosRuleReturnTrueWithNewerAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsSupportKosRule());
    }

    public function testGetIsSupportKosRuleReturnTrueWithOlderAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID - 1), // older version
            ]);

        $this->assertFalse($device->getIsSupportKosRule());
    }

    public function testGetIsSupportKosRuleReturnTrueWithEqualIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => UserDevice::MIN_KOS_RULE_API_VERSION_IOS,
            ]);

        $this->assertTrue($device->getIsSupportKosRule());
    }

    public function testGetIsSupportKosRuleReturnTrueWithNewerIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_API_VERSION_IOS + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsSupportKosRule());
    }

    public function testGetIsSupportKosRuleReturnTrueWithOlderIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_API_VERSION_IOS - 1), // older version
            ]);

        $this->assertFalse($device->getIsSupportKosRule());
    }

    public function testGetIsSupportNewRecommendationCityReturnTrueWithEqualAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID,
            ]);

        $this->assertTrue($device->getIsSupportNewRecommendationCity());
    }
    public function testGetIsSupportNewRecommendationCityReturnTrueWithNewerAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsSupportNewRecommendationCity());
    }

    public function testGetIsSupportNewRecommendationCityReturnTrueWithOlderAndroidVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID - 1), // older version
            ]);

        $this->assertFalse($device->getIsSupportNewRecommendationCity());
    }

    public function testGetIsSupportNewRecommendationCityReturnTrueWithEqualIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS,
            ]);

        $this->assertTrue($device->getIsSupportNewRecommendationCity());
    }

    public function testGetIsSupportNewRecommendationCityReturnTrueWithNewerIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS + 10 ), // newer version
            ]);

        $this->assertTrue($device->getIsSupportNewRecommendationCity());
    }

    public function testGetIsSupportNewRecommendationCityReturnTrueWithOlderIosVersion()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'ios',
                'app_version_code' => ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS - 1), // older version
            ]);

        $this->assertFalse($device->getIsSupportNewRecommendationCity());
    }

    public function testIsIosDeviceFromWeb()
    {
        $this->app->device = null;
        $this->assertFalse(UserDevice::isIosDevice());
    }

    public function testIsIosDeviceFromIos()
    {
        $device = factory(UserDevice::class)->create(['platform' => 'ios']);
        $this->app->device = $device;
        $this->assertTrue(UserDevice::isIosDevice());
    }

    public function testIsIosDeviceFromAndroid()
    {
        $device = factory(UserDevice::class)->create(['platform' => 'android']);
        $this->app->device = $device;
        $this->assertFalse(UserDevice::isIosDevice());
    }

    public function testCreateDummy()
    {
        $token = 'randomtoken';
        $createdUserDeviceDummy = UserDevice::createDummy($token);
        $queriedUserDevice = UserDevice::where('device_token', $token)->first();
        $this->assertEquals($queriedUserDevice->id, $createdUserDeviceDummy->id);
        $this->assertEquals($queriedUserDevice->platform, $createdUserDeviceDummy->platform);
        $this->assertEquals($queriedUserDevice->identifier, $createdUserDeviceDummy->identifier);
    }

    /**
     * @group TA
     * @group KOS
     * @group KOS-16472
     * @group App/Entities/Room/UserDevice
     */
    public function testSetLastLoginSuccess(): void 
    {
        $identifier = md5(uniqid('', TRUE));
        $platform   = 'ios';
        $uuid       = '1f982c98-b735-4fef-8b4b-'.md5(uniqid('', TRUE));
        $model      = '1Phone 7s Plus '.mt_rand();
        $appVersionCode     = mt_rand();
        $appInstallCount    = mt_rand(1, 10);
        $appUpdateCount     = mt_rand(1, 10);
        $enableAppPopup     = "true";
        $userId             = mt_rand(1, 999999);

        $userDeviceIdAsLastLoginId = mt_rand(1, 999999);
        $userDevice = factory(UserDevice::class)
            ->create([
                'id'            => $userDeviceIdAsLastLoginId,
                'user_id'       => $userId,
                'platform'      => $platform,
                'identifier'    => $identifier,
                'uuid'          => $uuid,
                'model'         => $model,
                'app_version_code'  => $appVersionCode,
                'app_install_count' => $appInstallCount,
                'app_update_count'  => $appUpdateCount,
                'enable_app_popup'  => $enableAppPopup,
            ]);
        
        $user = factory(User::class)->create([
                'last_login' => $userDeviceIdAsLastLoginId,
                'id'       => $userId,
            ]
        );
        
        $expectedResult = [
            'id'            => $userDeviceIdAsLastLoginId,
            'uuid'          => $uuid,
            'identifier'    => $identifier,
            'platform'      => $platform,
            'model'         =>  $userDevice->model,
            'app_version_code'  => $userDevice->app_version_code,
            'app_install_count' => $userDevice->app_install_count,
            'app_update_count'  => $userDevice->app_update_count,
            'enable_app_popup'  => $enableAppPopup,
        ];

        $this->assertEquals(
            UserDevice::setLastLogin($user->id, $identifier, $platform, $uuid),
            $expectedResult
        );
    }

    /**
     * @group UG
     * @group KOS
     * @group KOS-16472
     * @group App/Entities/Room/UserDevice
     */
    public function testSetLastLoginThrowRuntimeException(): void 
    {
        $identifier = md5(uniqid('', TRUE));
        $platform   = 'ios';
        $uuid       = '1f982c98-b735-4fef-8b4b-'.md5(uniqid('', TRUE));
        $model      = '1Phone 7s Plus '.mt_rand();
        $appVersionCode     = mt_rand();
        $appInstallCount    = mt_rand(1, 10);
        $appUpdateCount     = mt_rand(1, 10);
        $enableAppPopup     = "true";
        $userId             = mt_rand(1, 999999);

        $userDeviceIdAsLastLoginId = mt_rand(1, 999999);
        $userDevice = factory(UserDevice::class)
            ->create([
                'id'            => $userDeviceIdAsLastLoginId,
                'user_id'       => $userId,
                'platform'      => $platform,
                'identifier'    => $identifier,
                'uuid'          => $uuid,
                'model'         => $model,
                'app_version_code'  => $appVersionCode,
                'app_install_count' => $appInstallCount,
                'app_update_count'  => $appUpdateCount,
                'enable_app_popup'  => $enableAppPopup,
            ]);
        
        $user = factory(User::class)->create([
            'last_login'    => $userDeviceIdAsLastLoginId,
            'id'       => $userId,
            ]
        );
        
        $expectedResult = [
            'id'            => $userDeviceIdAsLastLoginId,
            'uuid'          => $uuid,
            'identifier'    => $identifier,
            'platform'      => $platform,
            'model'         =>  $userDevice->model,
            'app_version_code'  => $userDevice->app_version_code,
            'app_install_count' => $userDevice->app_install_count,
            'app_update_count'  => $userDevice->app_update_count,
            'enable_app_popup'  => $enableAppPopup,
        ];

        $this->expectException(\RuntimeException::class, 'User device data accidentaly is null!');
        $userDeviceResult = UserDevice::setLastLogin($user->id, $identifier.mt_rand(), $platform, $uuid);
    }
    
    public function testGetIsSupportKosRuleWithPhotoWithOlderVersionAndroid()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID - 1), // older version
            ]);

        $this->assertFalse($device->getIsSupportKosRuleWithPhoto());
    }

    public function testGetIsSupportKosRuleWithPhotoWithSameVersionAndroid()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID), // same version
            ]);

        $this->assertTrue($device->getIsSupportKosRuleWithPhoto());
    }

    public function testGetIsSupportKosRuleWithPhotoWithNewerVersionAndroid()
    {
        $device = factory(UserDevice::class)
            ->make([
                'platform' => 'android',
                'app_version_code' => ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID + 1), // same version
            ]);

        $this->assertTrue($device->getIsSupportKosRuleWithPhoto());
    }
}
