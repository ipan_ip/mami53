<?php

namespace App\Entities\Component;

use App\Entities\Feeds\ImageFacebook;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Room;
use App\Repositories\Feeds\MoEngage\MoEngageRepository;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Log;
use Mockery;
use phpmock\MockBuilder;

//Mock php native function
function fopen()
{
    return true;
}

function fclose()
{
    return true;
}

function fputcsv()
{
    return true;
}

function file_exists($path)
{
    return MoEngageFeedsTest::$functions->file_exists($path);
}

function mkdir()
{
    return false;
}

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MoEngageFeedsTest extends MamiKosTestCase
{
    use Traits\TraitAttributeFeeds;
    public static $functions;
    private $repo, $faker;

    protected function setUp() : void
    {
        parent::setUp();

        self::$functions = Mockery::mock();
        $this->repo = $this->mockPartialAlternatively(MoEngageRepository::class);
        $this->faker = Faker::create();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('unlink')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        $this->mock->disable();
    }

    public function testWriteHeaderCsv()
    {
        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'writeHeaderCsv');
        $result = $method->invokeArgs(
            $moengageFeeds,
            [
                ''
            ]
        );
        
        $this->assertNull($result);
    }

    public function testWriteHeaderCsvForGoldPlus()
    {
        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'writeHeaderCsvForGoldPlus');
        $result = $method->invokeArgs(
            $moengageFeeds,
            [
                ''
            ]
        );
        
        $this->assertNull($result);
    }

    public function testGetCsvPath()
    {
        $type = 1;
        $expected = MoEngageFeeds::DIR . '/' . MoEngageFeeds::FILENAME . '_' . $type . '.csv';
        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'getCsvPath');
        $result = $method->invokeArgs(
            $moengageFeeds,
            [
                $type
            ]
        );
        
        $this->assertSame($expected, $result);
    }

    public function testCheckDir()
    {
        self::$functions->shouldReceive('file_exists')
            ->with(public_path(MoEngageFeeds::DIR))
            ->andReturn(false);

        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'checkDir');
        $res = $method->invokeArgs(
            $moengageFeeds,
            []
        );

        $this->expectOutputString('backing up data'.PHP_EOL.'end backing up data'.PHP_EOL);
        $this->assertNull($res);
    }

    public function testRepeatUntillLimit()
    {
        $length = 10;

        $rooms = collect(factory(Room::class, 3)->make()->toArray());

        $mockMoengageFeeds = $this->mockPartialAlternatively(MoEngageFeeds::class);
        $mockMoengageFeeds->shouldReceive('writeBodyCsv')
            ->times(($length+1))
            ->with($rooms, '', true)
            ->andReturn(null);

        $result = $mockMoengageFeeds->repeatUntillLimit(
            $length,
            '',
            function() use($rooms) {
                return $rooms;
            },
            true);
        
        $this->assertNull($result);
    }

    public function testGetReguler()
    {
        $length = 5;

        $rooms = factory(Room::class, $length)->create();

        $this->repo->shouldReceive('getReguler')->andReturn($rooms);

        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'getReguler');
        $res = $method->invokeArgs(
            $moengageFeeds,
            [
                $length,
                ''
            ]
        );
        
        $this->assertNull($res);
    }

    public function testGetRegulerPremium()
    {
        $length = 5;

        $rooms = factory(Room::class, $length)->create();

        $this->repo->shouldReceive('getRegulerPremium')->andReturn($rooms);

        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'getRegulerPremium');
        $res = $method->invokeArgs(
            $moengageFeeds,
            [
                $length,
                ''
            ]
        );
        
        $this->assertNull($res);
    }

    public function testGetMamirooms()
    {
        $length = 5;

        $rooms = factory(Room::class, $length)->create();

        $this->repo->shouldReceive('getMamirooms')->andReturn($rooms);

        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'getMamirooms');
        $res = $method->invokeArgs(
            $moengageFeeds,
            [
                $length,
                ''
            ]
        );
        
        $this->assertNull($res);
    }

    public function testGetGoldPlus()
    {
        $length = 5;

        $rooms = factory(Room::class, $length)->create();

        $this->repo->shouldReceive('getGoldPlus')->andReturn($rooms);

        $moengageFeeds = new MoEngageFeeds();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeeds::class, 'getGoldPlus');
        $res = $method->invokeArgs(
            $moengageFeeds,
            [
                $length,
                ''
            ]
        );
        
        $this->assertNull($res);
    }

    public function testGenerate()
    {
        self::$functions->shouldReceive('file_exists')->andReturn(true);

        $expectedString = 'backing up data'.PHP_EOL.'end backing up data'.PHP_EOL;
        for ($x=0; $x<10; $x++) {
            $expectedString .= 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;
        }
            
        $length = 5;
        $rooms = factory(Room::class, $length)->create();

        //MOCK ALL GOLDPLUS METHOD
        $this->repo->shouldReceive('getCountGoldPlus')->andReturn($length);
        $this->repo->shouldReceive('getGoldPlus')->andReturn($rooms);

        //MOCK ALL MAMIROOMS METHOD
        $this->repo->shouldReceive('getCountMamirooms')->andReturn($length);
        $this->repo->shouldReceive('getMamirooms')->andReturn($rooms);

        //MOCK ALL BBK METHOD
        $this->repo->shouldReceive('getCountBisaBooking')->andReturn($length);
        $this->repo->shouldReceive('getBisaBooking')->andReturn($rooms);

        //MOCK ALL REGULER PREMIUM METHOD
        $this->repo->shouldReceive('getCountRegulerPremium')->andReturn($length);
        $this->repo->shouldReceive('getRegulerPremium')->andReturn($rooms);

        //MOCK ALL REGULER METHOD
        $this->repo->shouldReceive('getCountReguler')->andReturn($length);
        $this->repo->shouldReceive('getReguler')->andReturn($rooms);

        //MOCK WRITELOG
        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')->once()->andReturn(true);

        $res = (new MoEngageFeeds())->generate();
        
        $this->expectOutputString($expectedString);
        $this->assertNull($res);
    }

    public function testWriteBodyCsv()
    {
        $length = 5;
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class, $length)->states('active')->create();
        $roomIds = $room->pluck('id')->toArray();
        $room->each(function($item) use($level) {
            factory(KostLevelMap::class)->create([
                'kost_id' => $item->song_id,
                'level_id' => $level->id
            ]);

            $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

            $item->cards = $cards;
        });

        $mockTraitAttributeFeeds = $this->mockPartialAlternatively(MoEngageFeeds::class);
        $mockTraitAttributeFeeds->shouldReceive('writeToCsv')->andReturn(
            (object) [
                'status' => true,
                'ids' => $roomIds
            ]);

        $res = $mockTraitAttributeFeeds->writeBodyCsv(
            $room,
            '',
            true,
            'new'
        );

        $this->expectOutputString('converting to csv...'.PHP_EOL.'count now: '.$length.' csv converted...'.PHP_EOL);
        $this->assertNull($res);
    }
}