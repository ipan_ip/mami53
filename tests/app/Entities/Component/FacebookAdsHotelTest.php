<?php

namespace App\Entities\Component;

use App\Entities\Room\Element\Card;
use App\Entities\Component\FacebookAdsHotel;
use App\Entities\Component\Traits\TraitAttributeFeeds;
use App\Entities\Feeds\Facebook;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Landing\Landing;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Media\Media;

use App\Repositories\Feeds\Facebook\FacebookRepository;
use App\Repositories\Landing\LandingRepository;
use App\Test\MamiKosTestCase;
use App\User;

use Faker\Factory as Faker;
use phpmock\MockBuilder;

use Mockery;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class FacebookAdsHotelTest extends MamiKosTestCase
{
    protected $mock;
    protected $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();
        $this->faker = Faker::create();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
        $this->mock->disable();
    }

    public function testGetImage_Success()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
        ]);

        $room = factory(Room::class, 1)->make();
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $result = $facebookAdsHotel->getImage(
            $room->first(),
            ImageFacebook::TYPE_DALAM_KAMAR
        );

        $this->assertEquals($cards->first(), $result);
    }

    public function testGetImage_Fail()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'
        ]);

        $room = factory(Room::class, 1)->make();
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $result = $facebookAdsHotel->getImage(
            $room->first(),
            ImageFacebook::TYPE_DALAM_KAMAR
        );

        $this->assertEquals(NULL, $result);
    }

    public function testSetImage_Success()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
        ]);
        $cards->first()->photo = factory(Media::class)->make();
        $expected =
            Config::get('api.media.cdn_url')
                .$cards->first()->photo['file_path'].'/'
                .$cards->first()->photo['file_name'].'-540x720.'
                .$cards->first()->photo['media_format'];

        $room = factory(Room::class, 1)->make();
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setImage');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                ImageFacebook::TYPE_DALAM_KAMAR
            ]
        );

        $this->assertIsString($result);
        $this->assertEquals($expected, $result);
    }

    public function testSetTag_Null()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug
        ]);
        $room = factory(Room::class, 1)->make([
            'is_premium_photo' => 1
        ]);
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTag');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                Facebook::KAMAR,
                ImageFacebook::TYPE_DALAM_KAMAR
            ]
        );
        $this->assertIsString($result);
        $this->assertEquals('', $result);
    }

    public function testSetTagCoverFotoBooking_Success()
    {
        $expected = Facebook::KAMAR.' - Cover - Foto Booking';

        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
        ]);
        $room = factory(Room::class, 1)->make([
            'is_premium_photo' => 1
        ]);
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTag');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                Facebook::KAMAR,
                ImageFacebook::TYPE_DALAM_KAMAR
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetTagFotoBooking_Success()
    {
        $expected = Facebook::KAMAR.' - Foto Booking';

        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR
        ]);
        $room = factory(Room::class, 1)->make([
            'is_premium_photo' => 1
        ]);
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTag');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                Facebook::KAMAR,
                ImageFacebook::TYPE_DALAM_KAMAR
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetTagCover_Success()
    {
        $expected = Facebook::KAMAR.' - Cover';

        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
        ]);
        $room = factory(Room::class, 1)->make();
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTag');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                Facebook::KAMAR,
                ImageFacebook::TYPE_DALAM_KAMAR
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetTagDepan_Success()
    {
        $expected = Facebook::DEPAN_RUMAH.' - Cover - Foto Booking';

        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_TAMPAK_DEPAN.'-cover'
        ]);
        $room = factory(Room::class, 1)->make([
            'is_premium_photo' => 1
        ]);
        $room->first()->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTag');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                Facebook::DEPAN_RUMAH,
                ImageFacebook::TYPE_TAMPAK_DEPAN
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetBrand_Apartment()
    {
        $expected = 'Apartment';

        $room = factory(Room::class, 1)->make([
            'apartment_project_id' => $this->faker->numberBetween(1,1000)
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setBrand');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetBrand_Putra()
    {
        $expected = 'Kost Khusus Putra';
        $room = factory(Room::class, 1)->make([
            'gender' => 1
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setBrand');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetBrand_Putri()
    {
        $expected = 'Kost Khusus Putri';
        $room = factory(Room::class, 1)->make([
            'gender' => 2
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setBrand');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetBrand_Campur()
    {
        $expected = 'Kost Campur';
        $room = factory(Room::class, 1)->make([
            'gender' => 0
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setBrand');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testGetGender_Pasutri()
    {
        $expected = 'Kost Pasutri';
        $room = factory(Room::class, 1)->make([
            'gender' => 0,
            'tags' => factory(Tag::class, 1)->make([
                'id' => 60
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getGender');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                true
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testGetGender_PutraPutri()
    {
        $expected = 'Kost Putra/Putri';
        $room = factory(Room::class, 1)->make([
            'gender' => 0,
            'tags' => factory(Tag::class, 1)->make([
                'id' => 10
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getGender');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room->first(),
                true
            ]
        );
        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriority_Old()
    {
        $room = factory(Room::class)->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setPriorityOldRule')->once();

        $res = $mockFacebookAdsHotel->setPriority($room, 'old');

        $this->assertIsString($res);
    }

    public function testSetPriority_New()
    {
        $room = factory(Room::class)->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setPriorityNewRule')->once();

        $res = $mockFacebookAdsHotel->setPriority($room, 'new');

        $this->assertIsString($res);
    }

    public function testSetPriorityOldRule_5()
    {
        $expected = '5';
        $room = factory(Room::class)->make([
            'is_mamirooms' => 1,
            'is_promoted' => 'true',
            'label_promoted' => true,
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriorityOldRule_4()
    {
        $expected = '4';
        $room = factory(Room::class)->make([
            'is_booking' => 1,
            'is_promoted' => 'true',
            'label_promoted' => true,
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriorityOldRule_3()
    {
        $expected = '3';
        $room = factory(Room::class)->make([
            'is_mamirooms' => 1,
            'is_promoted' => 'false',
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriorityOldRule_2()
    {
        $expected = '2';
        $room = factory(Room::class)->make([
            'is_booking' => 1,
            'is_promoted' => 'false',
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriorityOldRule_1()
    {
        $expected = '1';
        $room = factory(Room::class)->make([
            'is_booking' => 0,
            'is_promoted' => 'true',
            'label_promoted' => true
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetPriorityOldRule_0()
    {
        $expected = '0';
        $room = factory(Room::class)->make([
            'is_booking' => 0,
            'is_promoted' => 'false',
            'label_promoted' => false
        ]);

        $result = (new FacebookAdsHotel())->setPriorityOldRule($room);

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetStatusPremiumOwner_True()
    {
        $user = factory(User::class)->states('owner')->make([
            'date_owner_limit' => date("Y-m-d",strtotime("tomorrow"))
        ]);
        $owners = factory(RoomOwner::class, 1)->make([
                'status' => RoomOwner::ROOM_VERIFY_STATUS,
                'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER_OLD
            ])->map(function($owner) use ($user) {
                $owner->user = $user;
                return $owner;
            });

        $result = (new FacebookAdsHotel())->setStatusPremiumOwner($owners);

        $this->assertTrue($result);
    }

    public function testSetStatusPremiumOwner_False()
    {
        $user = factory(User::class)->states('owner')->make([
            'date_owner_limit' => null
        ]);
        $owners = factory(RoomOwner::class, 1)->make([
                'status' => RoomOwner::ROOM_VERIFY_STATUS,
                'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER_OLD
            ])->map(function($owner) use ($user) {
                $owner->user = $user;
                return $owner;
            });

        $result = (new FacebookAdsHotel())->setStatusPremiumOwner($owners);

        $this->assertFalse($result);
    }

    public function testSetStatusIklanKost_True()
    {
        $room = factory(Room::class)->make([
                'id' => mt_rand(1, 999999999),
                'is_promoted' => 'true'
            ]);
        $viewPromote = factory(ViewPromote::class, 3)->make([
            'is_active' => 1,
            'designer_id' => $room->id
        ]);
        $room->view_promote = $viewPromote;

        $facebookAdsHotel = new FacebookAdsHotel();
        $result = $facebookAdsHotel->setStatusIklanKost($room);

        $this->assertTrue($result);
    }

    public function testSetStatusIklanKost_False()
    {
        $room = factory(Room::class)->make([
                'id' => mt_rand(1, 999999999),
                'is_promoted' => 'true'
            ]);
        $viewPromote = factory(ViewPromote::class, 3)->make([
            'is_active' => 0,
            'designer_id' => $room->id
        ]);
        $room->view_promote = $viewPromote;

        $facebookAdsHotel = new FacebookAdsHotel();
        $result = $facebookAdsHotel->setStatusIklanKost($room);

        $this->assertFalse($result);
    }

    public function testSetStatusIklanKost_FalseWhenNull()
    {
        $room = factory(Room::class)->make([
                'id' => mt_rand(1, 999999999),
                'is_promoted' => 'true'
            ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $result = $facebookAdsHotel->setStatusIklanKost($room);

        $this->assertFalse($result);
    }

    public function testSetPriorityNewRule_5()
    {
        $expected = '5';
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class)->create([
                'is_promoted' => 'true'
            ]);
        factory(KostLevelMap::class)->create([
                'kost_id' => $room->song_id,
                'level_id' => $level->id
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusGoldplusOwner')->once()->andReturn(true);

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetPriorityNewRule_4()
    {
        $expected = '4';
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class)->create([
                'is_promoted' => 'false'
            ]);
        factory(KostLevelMap::class)->create([
                'kost_id' => $room->song_id,
                'level_id' => $level->id
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(false);
        $mockFacebookAdsHotel->shouldReceive('setStatusGoldplusOwner')->once()->andReturn(true);

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetPriorityNewRule_3()
    {
        $expected = '3';
        $room = factory(Room::class)->states('mamirooms')->make([
                'is_promoted' => 'true',
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(true);

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetPriorityNewRule_2()
    {
        $expected = '2';
        $room = factory(Room::class)->states('availableBooking')->make([
                'is_promoted' => 'true',
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(true);

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetPriorityNewRule_1()
    {
        $expected = '1';
        $room = factory(Room::class)->states(
                collect('mamirooms', 'availableBooking')->random()
            )
            ->make([
                'is_promoted' => collect(true, false)->random(),
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(
                collect(true, false)->random()
            );

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetPriorityNewRule_0()
    {
        $expected = '0';
        $room = factory(Room::class)->make([
                'is_promoted' => false,
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(false);

        $res = $mockFacebookAdsHotel->setPriorityNewRule($room);

        $this->assertSame($expected, $res);
    }

    public function testSetNeighborhoodOldRuleCardsEmpty_NA()
    {
        $room = factory(Room::class)->make();
        $room->cards = collect([]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_NA, $result);
    }

    public function testSetNeighborhoodOldRule_VerifikasiChecker()
    {
        $cards = factory(Card::class, 3)->make([
            'type' => 'video',
            'video_url' => $this->faker->imageUrl(640, 480)
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_MAMICHECKER, $result);
    }

    public function testSetNeighborhoodOldRuleVideoUrlEmpty_NA()
    {
        $cards = factory(Card::class, 3)->make([
            'type' => 'image',
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_NA, $result);
    }

    public function testSetNeighborhoodNewRuleCardsPremiumCardsEmpty_NA()
    {
        $room = factory(Room::class)->make();
        $room->cards = collect([]);
        $room->premium_cards = collect([]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
                'new'
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_NA, $result);
    }

    public function testSetNeighborhoodNewRuleCardsPremiumCardsNotEmpty_MamicheckerPremiumphoto()
    {
        $cards = factory(Card::class, 3)->make([
            'type' => 'video',
            'video_url' => $this->faker->imageUrl(640, 480)
        ]);
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->cards = $cards;
        $room->premium_cards = $cardPremium;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
                'new'
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_MAMICHECKER_PREMIUM_PHOTO, $result);
    }

    public function testSetNeighborhoodNewRule_VerifikasiChecker()
    {
        $cards = factory(Card::class, 3)->make([
            'type' => 'video',
            'video_url' => $this->faker->imageUrl(640, 480)
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
                'new'
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_MAMICHECKER, $result);
    }

    public function testSetNeighborhoodNewRule_Premiumphoto()
    {
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setNeighborhood');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
                'new'
            ]
        );
        $this->assertIsString($result);
        $this->assertSame(TraitAttributeFeeds::$neighborhood_PREMIUM_PHOTO, $result);
    }

    public function testGetListrikStatus_84()
    {
        $room = factory(Room::class)->make([
            'tags' => factory(Tag::class, 1)->make([
                'id' => 84,
                'name' => 'Sudah termasuk listrik'
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getListrikStatus');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame($room->tags->pluck('name')->first(), $result);
    }

    public function testGetListrikStatus_144()
    {
        $room = factory(Room::class)->make([
            'tags' => factory(Tag::class, 1)->make([
                'id' => 144,
                'name' => 'Tidak Termasuk Biaya Listrik'
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getListrikStatus');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame($room->tags->pluck('name')->first(), $result);
    }

    public function testSearchLanding_EmptyString()
    {
        $this->mockPartialAlternatively(LandingRepository::class)
            ->shouldReceive('getFirstLandingBySlugAreaSubdistrict')
            ->andReturn(null);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'searchLanding');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                ['area_subdistrict' => '']
            ]
        );

        $this->assertIsString($result);
        $this->assertSame("", $result);
    }

    public function testSearchLanding_SlugAvailable()
    {
        $landing = factory(Landing::class)->make();
        $expected = env('APP_URL').'/kost/'.$landing->slug;

        $this->mockPartialAlternatively(LandingRepository::class)
            ->shouldReceive('getFirstLandingBySlugAreaSubdistrict')
            ->andReturn($landing);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'searchLanding');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                ['area_subdistrict' => '']
            ]
        );

        $this->assertIsString($result);
        $this->assertSame($expected, $result);
    }

    public function testSetLoyaltyProgram_GoldPus()
    {
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class)->create([
                'is_promoted' => 'true'
            ]);
        factory(KostLevelMap::class)->create([
                'kost_id' => $room->song_id,
                'level_id' => $level->id
            ]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusGoldplusOwner')->once()->andReturn(true);

        $result = $mockFacebookAdsHotel->setLoyaltyProgram($room);

        $this->assertIsString('string', $result);
        $this->assertSame(Facebook::GOLDPLUS, $result);
    }

    public function testSetLoyaltyProgram_MamiroomsFromIsMamirooms()
    {
        $room = factory(Room::class)->states('mamirooms')->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $result = $mockFacebookAdsHotel->setLoyaltyProgram($room);

        $this->assertIsString($result);
        $this->assertSame(Facebook::MAMIROOMS, $result);
    }

    public function testSetLoyaltyProgram_Booking()
    {
        $room = factory(Room::class)->states('availableBooking')->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $result = $mockFacebookAdsHotel->setLoyaltyProgram($room);


        $this->assertIsString($result);
        $this->assertSame(Facebook::BOOKING, $result);
    }

    public function testSetLoyaltyProgram_Reguler()
    {
        $room = factory(Room::class)->make([
            'is_promoted' => 'false'
        ]);
        $room->view_promote = collect([]);

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $result = $mockFacebookAdsHotel->setLoyaltyProgram($room);

        $this->assertIsString($result);
        $this->assertSame(Facebook::REGULER, $result);
    }

    public function testSetRoomCategory_Apartemen()
    {
        $room = factory(Room::class)->make([
            'apartment_project_id' => 1,
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setRoomCategory');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame(Facebook::APARTEMEN, $result);
    }

    public function testSetRoomCategory_Bulanan()
    {
        $room = factory(Room::class)->make([
            'price_monthly' => 1000000,
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setRoomCategory');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame(Facebook::KOST_BULANAN, $result);
    }

    public function testSetRoomCategory_Tahunan()
    {
        $room = factory(Room::class)->make([
            'price_yearly' => 12000000,
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setRoomCategory');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame(Facebook::KOST_TAHUNAN, $result);
    }

    public function testSetRoomCategory_Mingguan()
    {
        $room = factory(Room::class)->make([
            'price_weekly' => 120000,
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setRoomCategory');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame(Facebook::KOST_MINGGUAN, $result);
    }

    public function testSetRoomCategory_Harian()
    {
        $room = factory(Room::class)->make();

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setRoomCategory');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame(Facebook::KOST_HARIAN, $result);
    }

    public function testGetFacilities_TagMoreThan1()
    {
        $room = factory(Room::class)->make([
            'tags' => collect([
                factory(Tag::class)->make([
                    'id' => 1,
                    'facility_type_id' => 1,
                    'name' => 'Kamar mandi dalam'
                ]),
                factory(Tag::class)->make([
                    'id' => 2,
                    'facility_type_id' => 2,
                    'name' => 'Kloset duduk'
                ])
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getFacilities');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame('Kamar mandi dalam, Kloset duduk', $result);
    }

    public function testGetFacilities_WithoutFacilityId()
    {
        $room = factory(Room::class)->make([
            'tags' => collect([
                factory(Tag::class)->make([
                    'id' => 1,
                    'name' => 'Kamar mandi dalam'
                ]),
                factory(Tag::class)->make([
                    'id' => 2,
                    'name' => 'Kloset duduk'
                ])
            ])
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'getFacilities');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame('', $result);
    }

    public function testSetUrlCityLanding_EmptyString()
    {
        $this->mockPartialAlternatively(LandingRepository::class)
            ->shouldReceive('getFirstLandingBySlugAreaSubdistrict')
            ->andReturn(null);

        $room = factory(Room::class)->make([
            'area_subdistrict' => 'Subdistrict',
            'area_city' => 'City',
            'area_big' => 'Big',
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setUrlCityLanding');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame('', $result);
    }

    public function testSetUrlCityLanding_NotEmptyString()
    {
        $landing = factory(Landing::class)->make();

        $this->mockPartialAlternatively(LandingRepository::class)
            ->shouldReceive('getFirstLandingBySlugAreaSubdistrict')
            ->andReturn($landing);

        $room = factory(Room::class)->make([
            'area_subdistrict' => 'Subdistrict ',
            'area_city' => 'City ',
            'area_big' => 'Big ',
        ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setUrlCityLanding');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $this->assertIsString($result);
        $this->assertSame('Cek kost lainnya di Subdistrict , City , Big  di '.env('APP_URL').'/kost/'.$landing->slug, $result);
    }

    public function testSetDescription()
    {
        $slug = $this->faker->slug;
        $room = factory(Room::class)->make([
            'gender' => 1,
            'tags' => collect([
                factory(Tag::class)->make([
                    'id' => 1,
                    'facility_type_id' => 1,
                    'name' => 'Kamar mandi dalam'
                ]),
                factory(Tag::class)->make([
                    'id' => 2,
                    'facility_type_id' => 2,
                    'name' => 'Kloset duduk'
                ])
            ]),
            'price_monthly' => '1500000',
            'slug' => $slug
        ]);

        $landing = factory(Landing::class)->make();

        $this->mockPartialAlternatively(LandingRepository::class)
            ->shouldReceive('getFirstLandingBySlugAreaSubdistrict')
            ->andReturn($landing);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setDescription');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room
            ]
        );

        $expected = 'Kost di '.$room->area_city.' ini cocok buat khusus putra dilengkapi dengan kamar mandi dalam, kloset duduk.';
        $expected .= ' Harga '.$room->price_monthly.' IDR . Dijamin #CepatCocok';
        $expected .= ' '.$room->share_url.'.';
        $expected .= ' Cek kost lainnya di '.$room->area_subdistrict.', '.$room->area_city.', '.$room->area_big.' ';
        $expected .= 'di '.env('APP_URL').'/kost/'.$landing->slug;

        $this->assertIsString('string', $result);
        $this->assertSame($expected, $result);
    }

    public function testSetPremiumProgram_PremiumOn()
    {
        $room = factory(Room::class)->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(true);

        $result = $mockFacebookAdsHotel->setPremiumProgram($room);

        $this->assertIsString('string', $result);
        $this->assertSame(Facebook::PREMIUM_ON, $result);
    }

    public function testSetPremiumProgram_PremiumOff()
    {
        $room = factory(Room::class)->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(true);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(false);

        $result = $mockFacebookAdsHotel->setPremiumProgram($room);

        $this->assertIsString('string', $result);
        $this->assertSame(Facebook::PREMIUM_OFF, $result);
    }

    public function testSetPremiumProgram_NonPremium()
    {
        $room = factory(Room::class)->make();

        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('setStatusPremiumOwner')->once()->andReturn(false);
        $mockFacebookAdsHotel->shouldReceive('setStatusIklanKost')->once()->andReturn(false);

        $result = $mockFacebookAdsHotel->setPremiumProgram($room);

        $this->assertIsString('string', $result);
        $this->assertSame(Facebook::NON_PREMIUM_PROGRAM, $result);
    }

    public function testGenerate()
    {
        $mockFacebookAdsHotel = $this->mockPartialAlternatively(FacebookAdsHotel::class);

        $mockFacebookAdsHotel->shouldReceive('backupOldCatalog')->once();
        $mockFacebookAdsHotel->shouldReceive('getRoomsData')->once();
        $mockFacebookAdsHotel->generate();
    }

    public function testBackupOldCatalog_SecondThirdFileExistsTrue()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'backing up data'.PHP_EOL.'end backing up data'.PHP_EOL;

        (new FacebookAdsHotel)->backupOldCatalog();

        $this->expectOutputString($expectedString);

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockCopy->disable();
        $mockMkdir->disable();
    }

    public function testBackupOldCatalog_SecondThirdFileExistsFalse()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function (string $file) {
                    if ($file === 'facebookads_hotel') {
                        return true;
                    }
                    return false;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'backing up data'.PHP_EOL.'end backing up data'.PHP_EOL;

        (new FacebookAdsHotel)->backupOldCatalog();

        $this->expectOutputString($expectedString);

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockCopy->disable();
        $mockMkdir->disable();
    }

    public function testBackupOldCatalog_FirstFileExistsFalse()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function (string $file) {
                    if ($file === 'facebookads_hotel') {
                        return false;
                    }
                    return true;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'backing up data'.PHP_EOL.'end backing up data'.PHP_EOL;

        (new FacebookAdsHotel)->backupOldCatalog();

        $this->expectOutputString($expectedString);

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockCopy->disable();
        $mockMkdir->disable();
    }

    public function testGetRoomsData()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fopen')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFopen = $builder->build();
        $mockFopen->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('fclose')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockClose = $builder->build();
        $mockClose->enable();

        $length = 2;
        $rooms = factory(Room::class, $length)->create();
        $repo = $this->mockPartialAlternatively(FacebookRepository::class);
        $repo->shouldReceive('getCount')->andReturn(5);
        $repo->shouldReceive('getDataWithOffsetLimit->get')->andReturn($rooms);

        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $expectedString = 'fetching data'.PHP_EOL.'converting to csv...'.PHP_EOL;
        $expectedString .= 'count now: 0 csv converted...'.PHP_EOL.'end fetching data...'.PHP_EOL;

        (new FacebookAdsHotel)->getRoomsData();

        $this->expectOutputString($expectedString);

        $mockFopen->disable();
        $mockPublicpath->disable();
        $mockFputcsv->disable();
        $mockClose->disable();
    }

    public function testGenerateCatalog()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $length = 2;
        $photo  = factory(Media::class)->create();
        $rooms = factory(Room::class, $length)->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create()
            ->each(function($item) use($photo) {
                factory(Card::class)->create([
                    'designer_id' => $item->id,
                    'photo_id' => $photo->id,
                    'type' => 'image',
                    'description' => 'dalam-kamar'
                ]);
            });

        $this->callNonPublicMethod(FacebookAdsHotel::class, 'generateCatalog', [$rooms, '', 'old']);
        
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: '.$length.' csv converted...'.PHP_EOL;
        $this->expectOutputString($expectedString);

        $mockFputcsv->disable();
    }

    public function testWriteToCsv_ArrayShiftCalled()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $facebookAdsHotel = new FacebookAdsHotel();
        $facebookAdsHotel->ids = [0,1,2,3,4];
        $songId = mt_rand(10,100);
        $roomData = [$songId];

        $res = $facebookAdsHotel->writeToCsv('', $roomData);

        $this->assertTrue($res);

        $mockFputcsv->disable();
    }
}
