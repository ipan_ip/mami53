<?php

namespace App\Entities\Component;

use App\Entities\Room\Element\Card;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SitemapTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockSitemap = $this->mockPartialAlternatively(Sitemap::class);
        $mockSitemap->shouldReceive('sitemapLandingPage')->once();
        $mockSitemap->shouldReceive('sitemapFilterKost')->once();
        $mockSitemap->shouldReceive('sitemapLandingApartment')->once();
        $mockSitemap->shouldReceive('sitemapFilterApartment')->once();
        $mockSitemap->shouldReceive('sitemapRoom')->once();
        $mockSitemap->shouldReceive('sitemapRoomOtherCity')->once();
        $mockSitemap->shouldReceive('sitemapApartmentProject')->once();
        $mockSitemap->shouldReceive('sitemapLandingPageApp')->once();
        $mockSitemap->shouldReceive('sitemapRoomApp')->once();
        $mockSitemap->shouldReceive('sitemapRoomAppOtherCity')->once();
        $mockSitemap->shouldReceive('indexingSitemap')->once();

        $mockSitemap->generate();
    }

    public function testRemoveOldSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('rename')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockRename = $builder->build();
        $mockRename->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'Deleting the old sitemap'.PHP_EOL.PHP_EOL;
        $this->callNonPublicMethod(Sitemap::class, 'removeOldSitemap', []);

        $this->expectOutputString($expectedString);

        $mockRename->disable();
        $mockPublicPath->disable();
        $mockMkdir->disable();
    }

    public function testSitemapLandingPage()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 2;
        $landings = factory(Landing::class, $length)->create([
            'redirect_id' => null
        ]);

        $string = implode('', $landings->map(function($landing){
            return 'Adding generate sitemap for landing : '.$landing->slug.PHP_EOL.PHP_EOL;
        })->toArray());

        $expectedString = 'Prepare generating sitemap for landing page'.PHP_EOL.
            $string.
            'Collecting sitemap done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing'.PHP_EOL;
        (new Sitemap)->sitemapLandingPage();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }

    public function testSitemapFilterKost()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        (new Sitemap)->sitemapFilterKost();

        $mockPublicPath->disable();
    }

    public function testSitemapLandingApartment()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 2;
        $landingApartments = factory(LandingApartment::class, $length)->create([
            'redirect_id' => null
        ]);

        $string = implode('', $landingApartments->map(function($landing){
            return 'Adding generate sitemap for landing : '.$landing->slug.PHP_EOL.PHP_EOL;
        })->toArray());

        $expectedString = 'Prepare generating sitemap for landing page apartment'.PHP_EOL.
            $string.
            'Collecting sitemap done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing Apartment'.PHP_EOL;
        (new Sitemap)->sitemapLandingApartment();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }

    public function testSitemapFilterApartment()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        (new Sitemap)->sitemapFilterApartment();

        $mockPublicPath->disable();
    }

    public function testSitemapLandingPageApp()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 2;
        $landings = factory(Landing::class, $length)->create([
            'redirect_id' => null
        ]);

        $string = implode('', $landings->map(function($landing){
            return 'Adding generate sitemap for landing : '.$landing->slug.PHP_EOL.PHP_EOL;
        })->toArray());

        $expectedString = 'Prepare generating sitemap for landing page App'.PHP_EOL.PHP_EOL.
            $string.
            'Collecting sitemap done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing Page App'.PHP_EOL;

        (new Sitemap)->sitemapLandingPageApp();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }

    public function testSitemapRoom()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $cities = $this->getNonPublicAttributeFromClass('cities', (new Sitemap));

        $media = factory(Media::class)->create();

        array_map(function($city) use($media) {
            $room = factory(Room::class)->states('active')->create([
                'is_indexed' => 1,
                'area_big' => $city,
                'photo_id' => $media->id
            ]);
            factory(Card::class)->create([
                'designer_id' => $room->id,
                'photo_id' => $media->id
            ]);
        }, $cities);

        (new Sitemap)->sitemapRoom();

        $mockPublicPath->disable();
    }

    public function testSitemapRoomPhotoEmpty()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $cities = $this->getNonPublicAttributeFromClass('cities', (new Sitemap));

        array_map(function($city) {
            factory(Room::class)->states('active')->create([
                'is_indexed' => 1,
                'area_big' => $city
            ]);
        }, $cities);

        (new Sitemap)->sitemapRoom();

        $mockPublicPath->disable();
    }

    public function testSitemapRoomOtherCity()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 3;
        $media = factory(Media::class)->create();

        factory(Room::class, $length)->states('active')->create([
            'is_indexed' => 1,
            'photo_id' => $media->id
        ])->each(function($room) use($media) {
            factory(Card::class)->create([
                'designer_id' => $room->id,
                'photo_id' => $media->id
            ]);
        });

        (new Sitemap)->sitemapRoomOtherCity();

        $mockPublicPath->disable();
    }

    public function testSitemapRoomOtherCityPhotoEmpty()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 3;

        factory(Room::class, $length)->states('active')->create([
            'is_indexed' => 1,
        ]);

        (new Sitemap)->sitemapRoomOtherCity();

        $mockPublicPath->disable();
    }

    public function testSitemapApartmentProject()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $media = factory(Media::class)->create();
        $project = factory(ApartmentProject::class)->states('active')->create();
        factory(ApartmentProjectStyle::class)->create([
            'apartment_project_id' => $project,
            'photo_id' => $media->id,
            'type' => 'image'
        ]);

        (new Sitemap)->sitemapApartmentProject();

        $mockPublicPath->disable();
    }

    public function testSitemapRoomApp()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $cities = $this->getNonPublicAttributeFromClass('cities', (new Sitemap));

        $media = factory(Media::class)->create();
        array_map(function($city) use($media) {
            $room = factory(Room::class)->states('active')->create([
                'is_indexed' => 1,
                'area_big' => $city,
                'apartment_project_id' => null,
            ]);

            factory(Card::class)->create([
                'designer_id' => $room->id,
                'photo_id' => $media->id
            ]);
        }, $cities);

        (new Sitemap)->sitemapRoomApp();

        $mockPublicPath->disable();
    }

    public function testSitemapRoomAppOtherCity()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 3;
        $media = factory(Media::class)->create();
        factory(Room::class, $length)->states('active')->create([
            'is_indexed' => 1,
            'apartment_project_id' => null
        ])->each(function($room) use($media) {
            factory(Card::class)->create([
                'designer_id' => $room->id,
                'photo_id' => $media->id
            ]);
        });

        (new Sitemap)->sitemapRoomAppOtherCity();

        $mockPublicPath->disable();
    }

    public function testIndexingSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        (new Sitemap)->indexingSitemap();

        $mockPublicPath->disable();
    }
}