<?php

namespace App\Entities\Component;

use App\Entities\Forum\ForumCategory;
use App\Entities\Forum\ForumThread;
use App\Test\MamiKosTestCase;

class BreadcrumbForumTest extends MamiKosTestCase
{
    public function testGenerate_Null()
    {
        $res = BreadcrumbForum::generate();
        $this->assertInstanceOf(BreadcrumbForum::class, $res);
    }

    public function testGenerate_ForumCategory()
    {
        $forumCategory = factory(ForumCategory::class)->create();
        $res = BreadcrumbForum::generate($forumCategory);
        $this->assertInstanceOf(BreadcrumbForum::class, $res);
    }

    public function testGenerate_ForumThread()
    {
        $forumThread = factory(ForumThread::class)->create();
        $res = BreadcrumbForum::generate($forumThread);
        $this->assertInstanceOf(BreadcrumbForum::class, $res);
    }

    public function testGetList()
    {
        $forumThread = factory(ForumThread::class)->create();
        $breadcrumbForum = new BreadcrumbForum;
        $res = $breadcrumbForum::generate($forumThread)->getList();
        $this->assertIsArray($res);
    }
}