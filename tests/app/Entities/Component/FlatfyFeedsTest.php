<?php

namespace App\Entities\Component;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Libraries\SimpleXMLExtended;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class FlatfyFeedsTest extends MamiKosTestCase
{
    public function testConvertObjectToXML_DescEmptyOwnerPhoneEmptyCardPhotoNotNull()
    {
        $mediaRoom = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'photo_id' => $mediaRoom->id,
            'description' => ''
        ]);
        $mediaCard = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $mediaCard->id,
            'designer_id' => $room->id
        ]);

        $simpleXmlExtended = (new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>"));
        (new FlatfyFeeds)->convertObjectToXML($room, $simpleXmlExtended);
    }

    public function testConvertObjectToXML_DescFilledOwnerPhoneFilledCardPhotoNull()
    {
        $mediaRoom = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'photo_id' => $mediaRoom->id,
            'description' => 'test',
            'owner_phone' => '0123893812'
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id
        ]);

        $simpleXmlExtended = (new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>"));
        (new FlatfyFeeds)->convertObjectToXML($room, $simpleXmlExtended);
    }

    public function testGenerateFeeds()
    {
        $mediaRoom = factory(Media::class)->create();
        $rooms = factory(Room::class, 2)->create([
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'photo_id' => $mediaRoom->id,
            'description' => ''
        ])->each(function($item){
            $mediaCard = factory(Media::class)->create();
            factory(Card::class)->create([
                'photo_id' => $mediaCard->id,
                'designer_id' => $item->id
            ]);
        });

        $mockFlatfyFeeds = $this->mockPartialAlternatively(FlatfyFeeds::class);
        $mockFlatfyFeeds->shouldReceive('convertObjectToXML');
        $mockFlatfyFeeds->shouldReceive('endWritingXML');
        $mockFlatfyFeeds->generateFeeds($rooms);

        $expectedString = 'converting to xml...'.PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testEndWritingXML()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("public_path")
            ->setFunction( function () {
                return true;
            });

        $this->mockPublicpath = $builder->build();
        $this->mockPublicpath->enable();

        $simpleXmlExtended = (new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>"));
        (new FlatfyFeeds)->endWritingXML($simpleXmlExtended);

        $this->expectOutputString('xml converted...'.PHP_EOL);

        $this->mockPublicpath->disable();
    }
}