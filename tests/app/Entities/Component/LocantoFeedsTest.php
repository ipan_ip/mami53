<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Libraries\SimpleXMLExtended;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

class LocantoFeedsTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockLocanto = $this->mockPartialAlternatively(LocantoFeeds::class);
        $mockLocanto->shouldReceive('generateLocantoFeeds')->once();

        $mockLocanto->generate();
    }

    public function testGenerateLocantoFeeds()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $mockLocanto = $this->mockPartialAlternatively(LocantoFeeds::class);
        $mockLocanto->shouldReceive('processRoomFeeds')->once();

        $mockLocanto->generateLocantoFeeds();

        $expectedString = 'converting to xml...'.PHP_EOL.'xml converted...'.PHP_EOL;
        $this->expectOutputString($expectedString);

        $mockPublicpath->disable();
    }

    public function testProcessRoomFeeds()
    {
        $room = factory(Room::class)->states('active', 'availablePriceMonthly')->create();

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><locanto></locanto>");
        (new LocantoFeeds)->processRoomFeeds($xmlInfo);

        $expectedString = 'fetching data'.PHP_EOL.'processing : '.$room->name.PHP_EOL.'end fetching data'.PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testConvertObjectToXMLRoomWithPhotoId()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->states('active', 'availablePriceMonthly')->create([
            'photo_id' => $photo
        ]);

        $idTagKmDalam = 1;
        if (! (Tag::find($idTagKmDalam)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTagKmDalam
            ]);
        }

        $idTagWifi = 15;
        if (! (Tag::find($idTagWifi)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTagWifi
            ]);
        }

        $idTag24Jam = 59;
        if (! (Tag::find($idTag24Jam)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTag24Jam
            ]);
        }

        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTagKmDalam
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTagWifi
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTag24Jam
        ]);

        $room->facility(false)->bath_ids = [1];
        $room->facility(false)->room_ids = [15];
        $room->facility(false)->share_ids = [59];

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><locanto></locanto>");

        $res = (new LocantoFeeds)->convertObjectToXML($room, $xmlInfo);
        
        $this->assertNull($res);
    }

    public function testConvertObjectToXMLRoomWithOutPhotoId()
    {
        $room = factory(Room::class)->states('active', 'availablePriceMonthly')->create();

        $idTagKmDalam = 1;
        if (! (Tag::find($idTagKmDalam)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTagKmDalam
            ]);
        }

        $idTagWifi = 15;
        if (! (Tag::find($idTagWifi)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTagWifi
            ]);
        }

        $idTag24Jam = 59;
        if (! (Tag::find($idTag24Jam)) instanceof Tag) {
            factory(Tag::class)->create([
                'id' => $idTag24Jam
            ]);
        }

        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTagKmDalam
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTagWifi
        ]);
        factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $idTag24Jam
        ]);

        $room->facility(false)->bath_ids = [1];
        $room->facility(false)->room_ids = [15];
        $room->facility(false)->share_ids = [59];

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><locanto></locanto>");

        $res = (new LocantoFeeds)->convertObjectToXML($room, $xmlInfo);
        
        $this->assertNull($res);
    }
}