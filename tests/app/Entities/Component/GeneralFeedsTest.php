<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Config\AppConfig;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Libraries\SimpleXMLExtended;
use App\Repositories\Feeds\Flatfy\FlatfyRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use phpmock\MockBuilder;

class GeneralFeedsTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $rooms = factory(Room::class, 2)->create();
        $mockGeneralFeeds = $this->mockPartialAlternatively(GeneralFeeds::class);
        $mockGeneralFeeds->shouldReceive('getRoomsData')->andReturn($rooms);
        $mockGeneralFeeds->shouldReceive('generateFeeds');

        $mockGeneralFeeds->generate();
    }

    public function testGetRoomsData()
    {
        $filterCountPair = $this->getNonPublicAttributeFromClass(
            'filterCountPair', 
            (new GeneralFeeds(''))
        );
        
        foreach ($filterCountPair as $key => $val) {
            factory(AppConfig::class)->create([
                'name' => $key,
                'value' => mt_rand(1, 10)
            ]);
            factory(AppConfig::class)->create([
                'name' => $val,
                'value' => mt_rand(1, 10)
            ]);
        }

        $length = 1;
        $rooms = factory(Room::class, $length)->create();
        $mockFlatfyrepo = $this->mockPartialAlternatively(FlatfyRepositoryEloquent::class);
        $mockFlatfyrepo->shouldReceive('getPremiumCount')->andReturn($length);
        $mockFlatfyrepo->shouldReceive('getPremium')->andReturn($rooms);
        $mockFlatfyrepo->shouldReceive('getReguler')->andReturn($rooms);

        $res = (new GeneralFeeds(''))->getRoomsData();

        $expectedString = 'fetching data'.PHP_EOL.'end fetching data...'.PHP_EOL;
        $this->expectOutputString($expectedString);
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testBackupOldFeedsFileExistsFalse()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function () {
                    return false;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $res = (new GeneralFeeds(''))->backupOldFeeds();

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockMkdir->disable();
    }

    public function testBackupOldFeedsFileExiststrue()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($key) {
                    if ($key === 'generalfeeds') {
                        return true;
                    }

                    return false;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return false;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        (new GeneralFeeds(''))->backupOldFeeds();

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockMkdir->disable();
        $mockCopy->disable();
    }

    public function testConvertObjectToXML()
    {
        $room = factory(Room::class)
            ->states(
                'active',
                'availablePriceMonthly',
                'notTesting',
                'with-slug'
            )
            ->create([
                'kost_updated_date' => Carbon::now()->toDateTimeString(),
                'description' => '-'
            ]);
            
        $media = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $room->id
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id
        ]);

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");
        $rootTag = $xmlInfo->addChild("ad");
        (new GeneralFeeds(''))->convertObjectToXML($room, $rootTag);
    }

    public function testConvertObjectToXMLDescAvailable()
    {
        $room = factory(Room::class)
            ->states(
                'active',
                'availablePriceMonthly',
                'notTesting',
                'with-slug'
            )
            ->create([
                'kost_updated_date' => Carbon::now()->toDateTimeString(),
                'description' => 'testing'
            ]);
            
        $media = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $room->id
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id
        ]);

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");
        $rootTag = $xmlInfo->addChild("ad");
        (new GeneralFeeds(''))->convertObjectToXML($room, $rootTag);
    }

    public function testGenerateFeeds()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $roomsMerged = collect();
        $rooms1 = factory(Room::class, 2)->create();
        $rooms2 = factory(Room::class, 2)->create();
        $roomsMerged = $roomsMerged->merge([$rooms1]);
        $roomsMerged = $roomsMerged->merge([$rooms2]);

        (new GeneralFeeds(''))->generateFeeds($roomsMerged);

        $expectedString = 'converting to xml...'.PHP_EOL.'xml converted...'.PHP_EOL;
        $this->expectOutputString($expectedString);

        $mockPublicpath->disable();
    }
}