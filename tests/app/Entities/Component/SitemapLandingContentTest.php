<?php

namespace App\Entities\Component;

use App\Entities\LandingContent\LandingContent;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

class SitemapLandingContentTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockLandingContent = $this->mockPartialAlternatively(SitemapLandingContent::class);
        $mockLandingContent->shouldReceive('sitemapLandingPage')->once();
        $mockLandingContent->shouldReceive('indexingSitemap')->once();

        $mockLandingContent->generate();
    }

    public function testRemoveOldSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('rename')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockRename = $builder->build();
        $mockRename->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = "Deleting the old sitemap\n\n";
        $this->callNonPublicMethod(SitemapLandingContent::class, 'removeOldSitemap', []);

        $this->expectOutputString($expectedString);

        $mockRename->disable();
        $mockPublicPath->disable();
        $mockMkdir->disable();
    }

    public function testSitemapLandingPage()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(function () {return 'sitemapLandingContent';});
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $landingContent = factory(LandingContent::class)->create();

        $expectedString = 'Prepare generating sitemap for landing page'.PHP_EOL.
            'Adding generate sitemap for landing : '.$landingContent->slug.PHP_EOL.PHP_EOL.
            'Collecting sitemap done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing'.PHP_EOL;

        $sitemapLandingContent = new SitemapLandingContent;
        $sitemapLandingContent->sitemapLandingPage();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }

    public function testIndexingSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(function () {return 'sitemapLandingContent';});
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $sitemapLandingContent = new SitemapLandingContent;
        $sitemapLandingContent->sitemaps = ['testing-sitemap.xml'];
        $sitemapLandingContent->indexingSitemap();

        $expectedString = 'Generating index sitemap'.PHP_EOL;

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }
}