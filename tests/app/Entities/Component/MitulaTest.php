<?php

namespace App\Entities\Component;

use App\Test\MamiKosTestCase;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Room;
use App\Entities\Component\Mitula;
use App\Entities\Media\Media;
use App\Libraries\SimpleXMLExtended;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use phpmock\MockBuilder;

class MitulaTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        factory(AreaBigMapper::class);
    }

    public function testGenerate_Success()
    {
        $mockMitula = $this->mockPartialAlternatively(Mitula::class);
        $mockMitula->shouldReceive('startWritingXML')->once();
        $mockMitula->shouldReceive('processRooms')->once();
        $mockMitula->shouldReceive('endWritingXML')->once();

        $mockMitula->generate();
    }

    public function testStartWritingXML()
    {
        $expectedString = "Start Writing XML...".PHP_EOL;
        (new Mitula)->startWritingXML();

        $this->expectOutputString($expectedString);
    }

    public function testProcessRooms()
    {
        $expectedString = 'mamirooms called'.PHP_EOL.
            'Start fetching mamirooms data'.PHP_EOL.
            'premium called'.PHP_EOL.
            'Start fetching premium data'.PHP_EOL.
            'available-booking called'.PHP_EOL.
            'Start fetching available-booking data'.PHP_EOL.
            'regular called'.PHP_EOL.
            'Start fetching regular data'.PHP_EOL;

        (new Mitula)->processRooms();
        $this->expectOutputString($expectedString);
    }

    public function testEndWritingXML()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(function () {return 'mitula.xml';});
        $this->mockPublicPath = $builder->build();
        $this->mockPublicPath->enable();

        $mitula = new Mitula;
        $mitula->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");

        $expectedString = "xml converted...".PHP_EOL;

        $mitula->endWritingXML();

        $this->expectOutputString($expectedString);

        $this->mockPublicPath->disable();
    }

    public function testWriteLog()
    {
        $roomsCount = '10';

        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')
            ->once()
            ->withArgs(function ($message) use ($roomsCount) {
                return strpos($message, $roomsCount) !== false;
            })
            ->andReturn(true);

        $mitula = new Mitula;
        $method = $this->getNonPublicMethodFromClass(Mitula::class, 'writeLog');
        $res = $method->invokeArgs(
            $mitula,
            [
                $roomsCount
            ]
        );

        $this->assertNull($res);
    }

    public function testSwitchTypeRoomMamiroom()
    {
        $type = 'mamirooms';
        $this->callNonPublicMethod(Mitula::class, 'switchTypeRoom', [
            new Room,
            $type
        ]);

        $expectedString = "$type called".PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testSwitchTypeRoomPremium()
    {
        $type = 'premium';
        $this->callNonPublicMethod(Mitula::class, 'switchTypeRoom', [
            new Room,
            $type
        ]);

        $expectedString = "$type called".PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testSwitchTypeRoomAvailableBooking()
    {
        $type = 'available-booking';
        $this->callNonPublicMethod(Mitula::class, 'switchTypeRoom', [
            new Room,
            $type
        ]);

        $expectedString = "$type called".PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testSwitchTypeRoomRegular()
    {
        $type = 'regular';
        $this->callNonPublicMethod(Mitula::class, 'switchTypeRoom', [
            new Room,
            $type
        ]);

        $expectedString = "$type called".PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testWriteRoomsToXML()
    {
        $length = 3;
        $rooms = factory(Room::class, $length)->create();

        $mitula = new Mitula;
        $mitula->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");
        $mitula->writeRoomsToXML($rooms);

        $expectedString = "write rooms to XML".PHP_EOL."end write rooms to XML".PHP_EOL;
        $this->expectOutputString($expectedString);
    }

    public function testConvertObjectToXML()
    {
        $room = factory(Room::class)
            ->states(
                'active',
                'availablePriceMonthly',
                'notTesting',
                'with-slug'
            )
            ->create([
                'kost_updated_date' => Carbon::now()->toDateTimeString()
            ]);
            
        $media = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $room->id
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id
        ]);

        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");
        $rootTag = $xmlInfo->addChild("ad");
        (new Mitula)->convertObjectToXML($room, $rootTag);
    }
}
