<?php 

namespace App\Entities\Component\Traits;

use App\Entities\Component\{
    FacebookAdsHotel,
    MoEngageFeeds,
    MoEngageFeedsCrossSelling
};
use App\Entities\Level\KostLevel;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Feeds\Facebook;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Review;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Faker\Factory as Faker;
use phpmock\MockBuilder;

class TraitAttributeFeedsTest extends MamiKosTestCase
{
    use TraitAttributeFeeds;

    protected $faker;

    protected function setUp() : void
    {
        parent::setUp();

        $this->faker = Faker::create();
    }

    public function testSetTestingInfo_True()
    {
        $room = factory(Room::class)->make([
                'is_testing' => 1
            ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTestingInfo');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
            ]
        );

        $this->assertIsString('string', $result);
        $this->assertSame(TraitAttributeFeeds::$testing_kost, $result);
    }

    public function testSetTestingInfo_False()
    {
        $room = factory(Room::class)->make([
                'is_testing' => 0
            ]);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'setTestingInfo');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $room,
            ]
        );

        $this->assertIsString('string', $result);
        $this->assertSame(TraitAttributeFeeds::$non_testing_kost, $result);
    }

    public function testSetImageLink_Success()
    {
        $media = factory(Media::class)->make();
        $room = factory(Room::class)->make();
        $room->photo = $media;

        $expected = 
            Config::get('api.media.cdn_url')
                .$media->file_path.'/'
                .$media->file_name.'-540x720.'
                .$media->media_format;

        $mockMoEngageFeeds = $this->mockPartialAlternatively(MoEngageFeeds::class);
        $mockMoEngageFeeds->shouldReceive('setImage')->once()->andReturn(null);

        $result = $mockMoEngageFeeds->setImageLink($room);

        $this->assertSame($result, $expected);
    }

    public function testSetGoldplusProgram_Success()
    {
        $goldplusCategory = [
            'Mamikos Goldplus 1',
            'Mamikos Goldplus 2',
            'Mamikos Goldplus 3',
            'Mamikos Goldplus 4',
            'Mamikos Goldplus 1 PROMO',
            'Mamikos Goldplus 2 PROMO',
            'Mamikos Goldplus 3 PROMO'
        ];
        $level = factory(KostLevel::class)->states('goldplus')->make();
        $room = factory(Room::class)->make();
        $room->level = $level;

        $res = (new MoEngageFeeds)->setGoldplusProgram($level);

        $this->assertSame((string) (array_search($level->name, $goldplusCategory)+1), $res);
    }

    public function testSetGoldplusProgram_Fail()
    {
        $level = factory(KostLevel::class)->make();
        $room = factory(Room::class)->make();
        $room->level = $level;

        $res = (new MoEngageFeeds)->setGoldplusProgram($level);

        $this->assertSame("0", $res);
    }

    public function testWriteLog()
    {
        $roomsCount = '10';
        $filename = 'test';

        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')
            ->once()
            ->withArgs(function ($message) use ($roomsCount) {
                return strpos($message, $roomsCount) !== false;
            })
            ->andReturn(true);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'writeLog');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $roomsCount,
                $filename
            ]
        );
        $this->assertTrue($result);
    }

    public function testWriteLogForDuplicate()
    {
        $songId = mt_rand(0, 99999);
        $filename = 'test';

        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')
            ->once()
            ->withArgs(function ($message) use ($songId) {
                return strpos($message, (string) $songId) !== false;
            })
            ->andReturn(true);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'writeLogForDuplicate');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $songId,
                $filename
            ]
        );
        $this->assertTrue($result);
    }

    public function testWriteLogForReason()
    {
        $songId = mt_rand(0, 99999);
        $filename = 'test';

        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')
            ->once()
            ->withArgs(function ($message) use ($songId) {
                return strpos($message, (string) $songId) !== false;
            })
            ->andReturn(true);

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'writeLogForReason');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                $songId,
                $filename
            ]
        );
        $this->assertTrue($result);
    }

    public function testGetStatusFacility_Success()
    {
        $room = factory(Room::class)->create();
        $tags = factory(Tag::class, 3)->create();
        $idSearched = [$tags->pluck('id')->first()];
        $room->tags = $tags;

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->getStatusFacility($room, $idSearched);

        $this->assertSame($res, $idSearched);
        $this->assertIsArray($res);
    }

    public function testDistance()
    {
        $roomRecommended = factory(Room::class)->create([
            'longitude' => 110.376729,
            'latitude' => -7.760671
        ]);
        $room = factory(Room::class)->create([
            'longitude' => 110.379443,
            'latitude' => -7.75684
        ]);

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->distance($room, $roomRecommended);
        
        $this->assertIsFloat($res);
    }

    public function testSetAvgRating_Zero()
    {
        $room = factory(Room::class)->states('active')->create();

        $res = $this->setAvgRating($room);

        $this->assertEquals($res, 0);
    }

    public function testSetAvgRating_NotZero()
    {
        $room = factory(Room::class)->states('active')->create();
        $review = factory(Review::class, 3)->create([
            'designer_id' => $room->id,
            'cleanliness' => 5,
            'comfort' => 5,
            'safe' => 5,
            'price' => 5,
            'room_facility' => 5,
            'public_facility' => 5,
            'status' => 'live'
        ]);
        $room->avg_review = $review;

        $res = $this->setAvgRating($room);

        $this->assertTrue((int) $res > 0);
    }

    public function testSetStatusGoldplusOwner_True()
    {
        $length = 2;
        $level = factory(KostLevel::class, $length)->states('goldplus')->create();
        $res = $this->setStatusGoldplusOwner($level);

        $this->assertTrue($res);
    }

    public function testSetStatusGoldplusOwner_False()
    {
        $length = 2;
        $level = factory(KostLevel::class, $length)->states('regular')->create();
        $res = $this->setStatusGoldplusOwner($level);

        $this->assertFalse($res);
    }

    public function testSetPriorityOldRuleMamirooms3_IsIklanIncluded()
    {
        $room = factory(Room::class)->states('active', 'mamirooms')->create();
        $room->label_promoted = false;

        $res = $this->setPriorityOldRule($room);

        $this->assertEquals($res, 3);
    }

    public function testSetPriorityOldRuleBBK2_IsIklanIncluded()
    {
        $room = factory(Room::class)->states('active', 'availableBooking')->create();
        $room->label_promoted = false;

        $res = $this->setPriorityOldRule($room);

        $this->assertEquals($res, 2);
    }

    public function testSetNeighborhoodNewRuleWithoutIf_NA()
    {
        $cards = factory(Card::class, 3)->make([
            'type' => 'image'
        ]);
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = collect([]);
        $room->cards = $cards;
        $room->premium_cards = $cardPremium;

        $res = $this->setNeighborhoodNewRule($room);

        $this->assertEquals($res, self::$neighborhood_NA);
    }

    public function testSetLoyaltyProgram_Moengage()
    {
        $room = factory(Room::class)->states('active', 'mamirooms')->make([
            'id' => mt_rand(1, 99999)
        ]);
        $res = $this->setLoyaltyProgram($room, 'moengage');

        $this->assertSame($res, Facebook::MAMIROOMS);
    }

    public function testSetLoyaltyProgram_Moengage_Goldplus()
    {
        $level = factory(KostLevel::class, 2)->states('goldplus')->create();
        $room = factory(Room::class)->states('active')->create();
        $room->level = $level;
        $res = $this->setLoyaltyProgram_Moengage($room);

        $this->assertSame($res, Facebook::GOLDPLUS);
    }

    public function testSetLoyaltyProgram_Moengage_Mamirooms()
    {
        $room = factory(Room::class)->states('active', 'mamirooms')->make([
            'id' => mt_rand(1, 99999)
        ]);
        $res = $this->setLoyaltyProgram_Moengage($room);

        $this->assertSame($res, Facebook::MAMIROOMS);
    }

    public function testSetLoyaltyProgram_Moengage_Booking()
    {
        $room = factory(Room::class)->states('active', 'availableBooking')->make([
            'id' => mt_rand(1, 99999)
        ]);
        $res = $this->setLoyaltyProgram_Moengage($room);

        $this->assertSame($res, Facebook::BOOKING);
    }

    public function testSetLoyaltyProgram_Moengage_Reguler()
    {
        $room = factory(Room::class)->states('active')->make([
            'id' => mt_rand(1, 99999)
        ]);
        $res = $this->setLoyaltyProgram_Moengage($room);

        $this->assertSame($res, Facebook::REGULER);
    }

    public function testSetTagImageLink_NotCover()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $res = $this->setTagImageLink($room);

        $this->assertEquals(trim($res), Facebook::KAMAR);
    }

    public function testSetTagImageLink_Cover()
    {
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_TAMPAK_DEPAN.'-cover'
        ]);
        $room = factory(Room::class)->make([
            'is_premium_photo' => 1
        ]);
        $room->cards = $cards;

        $res = $this->setTagImageLink($room);

        $this->assertEquals(trim($res), 'Cover');
    }

    public function testSetImageWithPremiumPhoto_CoverPremiumPhoto()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) use($media) {
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $res = $this->setImageWithPremiumPhoto($room);
        
        $this->assertSame($res, $room->premium_cards->first()->photo_url['large']);
    }

    public function testSetImageWithPremiumPhoto_DalamKamarPremiumPhoto()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-dalam-kamar'
        ])->each(function ($card) use($media) {
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $res = $this->setImageWithPremiumPhoto($room);
        
        $this->assertSame($res, $room->premium_cards->first()->photo_url['large']);
    }

    public function testSetImageWithPremiumPhoto_EmptyPremiumPhoto()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $cardPremium = factory(CardPremium::class, 3)->create([
            'designer_id' => $room->id,
            'description' => 'afoto-kamar'
        ])->each(function ($card) use($media) {
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $res = $this->setImageWithPremiumPhoto($room);
        
        $this->assertNull($res);
    }

    public function testSetImageWithPremiumPhoto_Cover()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_TAMPAK_DEPAN.'-cover',
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->make([
            'is_premium_photo' => 1
        ]);
        $room->cards = $cards;

        $res = $this->setImageWithPremiumPhoto($room);

        $this->assertSame($res, $room->cards->first()->photo_url['large']);
    }

    public function testSetImageWithPremiumPhoto_DalamKamarCover()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->create([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->create();
        $room->cards = $cards;

        $res = $this->setImageWithPremiumPhoto($room);

        $this->assertSame($res, $room->cards->first()->photo_url['large']);
    }

    public function testSetImageWithPremiumPhoto_DalamKamarOnly()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->create([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR,
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->create();
        $room->cards = $cards;

        $res = $this->setImageWithPremiumPhoto($room);

        $this->assertSame($res, $room->cards->first()->photo_url['large']);
    }

    public function testSetTagWithPremiumPhoto_PremiumPhoto()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) use($media) {
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $res = $this->setTagWithPremiumPhoto($room);

        $this->assertSame($res, ucfirst(self::$image_cover_type));
    }

    public function testSetTagWithPremiumPhoto_Cover()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_TAMPAK_DEPAN.'-cover',
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->make([
            'is_premium_photo' => 1
        ]);
        $room->cards = $cards;

        $res = $this->setTagWithPremiumPhoto($room);

        $this->assertSame($res, ucfirst(self::$image_cover_type));
    }

    public function testSetTagWithPremiumPhoto_DalamKamar()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR,
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $res = $this->setTagWithPremiumPhoto($room);

        $this->assertSame($res, ucfirst(self::$image_kamar_type));
    }

    public function testSetTagWithPremiumPhoto_Empty()
    {
        $media = factory(Media::class)->create();
        $cards = factory(Card::class, 3)->make([
            'description' => $this->faker->slug.'-testing',
            'photo_id' => $media->id
        ]);
        $room = factory(Room::class)->make();
        $room->cards = $cards;

        $res = $this->setTagWithPremiumPhoto($room);

        $this->assertTrue(empty($res));
    }

    public function testWriteToCsv_WriteLogForDuplicateNotCalled()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $res = $this->writeToCsv('', ['',''], [], '', '');

        $this->assertTrue(is_object($res));
        $this->assertTrue($res->status);

        $mockFputcsv->disable();
    }

    public function testWriteToCsv_WriteLogForDuplicateCalled()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        //MOCK WRITELOG
        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $res = $this->writeToCsv('', [1,''], [1,2], 'lala', '');

        $this->assertFalse($res->status);
        $this->assertTrue(is_object($res));

        $mockFputcsv->disable();
    }

    public function testCheckDir()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function () {
                    return false;
                }
            );
        $mockFileexists = $builder->build();
        $mockFileexists->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $facebookAdsHotel = new FacebookAdsHotel();
        $method = $this->getNonPublicMethodFromClass(FacebookAdsHotel::class, 'checkDir');
        $result = $method->invokeArgs(
            $facebookAdsHotel,
            [
                'test',
            ]
        );

        $this->expectOutputString('checking directory'.PHP_EOL.'end checking directory'.PHP_EOL);
        $this->assertNull($result);

        $mockFileexists->disable();
        $mockMkdir->disable();
    }
}