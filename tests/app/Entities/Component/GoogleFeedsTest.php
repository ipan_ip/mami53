<?php

namespace App\Entities\Component;

use App\Entities\Feeds\Facebook;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Tag;
use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Log;
use phpmock\MockBuilder;

class GoogleFeedsTest extends MamiKosTestCase
{
    protected $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testSetDestinationName_NotEmpty()
    {
        $room = factory(Room::class)->states('active')->make();
        $expected = $room->area_subdistrict.' '.$room->area_city;

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setDestinationName', [$room]);
        
        $this->assertSame($res, $expected);
    }

    public function testSetDestinationName_Empty()
    {
        $room = factory(Room::class)->states('active')->make([
            'area_city' => '',
            'area_subdistrict' => ''
        ]);

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setDestinationName', [$room]);
        
        $this->assertSame($res, '');
    }

    public function testSetContextualKeywords_Empty()
    {
        $room = factory(Room::class)->states('active')->make([
            'area_city' => '',
            'area_subdistrict' => ''
        ]);

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertTrue(empty($res));
    }

    public function testSetContextualKeywords_Gender()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict;

        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'gender' => 0
        ]);

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testSetContextualKeywords_PriceDaily()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict.';';
        $expected .= 'Kost Harian '.$city.';Kost Harian '.$subdistrict;

        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'price_daily' => mt_rand(1, 99999),
            'gender' => 0
        ]);

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testSetContextualKeywords_PriceMonthly()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict.';';
        $expected .= 'Kost Bulanan '.$city.';Kost Bulanan '.$subdistrict;

        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'price_monthly' => mt_rand(1, 99999),
            'gender' => 0
        ]);

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testSetContextualKeywords_Pasutri()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict.';';
        $expected .= 'Kost Pasutri '.$city.';Kost Pasutri '.$subdistrict;

        $tags = factory(Tag::class, 1)->make([
            'id' => 60
        ]);
        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'gender' => 0
        ]);
        $room->tags = $tags;

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testSetContextualKeywords_AksesBebas()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict.';';
        $expected .= 'Kost Bebas '.$city.';Kost Bebas '.$subdistrict;

        $tags = factory(Tag::class, 1)->make([
            'id' => 59
        ]);
        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'gender' => 0
        ]);
        $room->tags = $tags;

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testSetContextualKeywords_Ac()
    {
        $city = $this->faker->city;
        $subdistrict = $this->faker->streetName;

        $expected = 'Kost Campur '.$city.';Kost Campur '.$subdistrict.';';
        $expected .= 'Kost AC '.$city.';Kost AC '.$subdistrict;

        $tags = factory(Tag::class, 1)->make([
            'id' => 13
        ]);
        $room = factory(Room::class)->states('active')->make([
            'area_city' => $city,
            'area_subdistrict' => $subdistrict,
            'gender' => 0
        ]);
        $room->tags = $tags;

        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setContextualKeywords', [$room]);

        $this->assertSame($res, $expected);
    }

    public function testGetCsvPath()
    {
        $number = '1';
        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'getCsvPath', [$number]);
        $this->assertSame(GoogleFeeds::DIR.'/'.GoogleFeeds::FILENAME.'_'.$number.'.csv', $res);
    }

    public function testWriteHeaderCsv()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockFputcsv = $builder->build();
        $this->mockFputcsv->enable();

        $this->callNonPublicMethod(GoogleFeeds::class, 'writeHeaderCsv', ['']);

        $this->mockFputcsv->disable();
    }

    public function testRepeatUntillLimit()
    {
        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $expectedString = 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;
        $rooms = factory(Room::class, 2)->make();
        $this->callNonPublicMethod(GoogleFeeds::class, 'repeatUntillLimit', ['', $rooms, 'mamirooms']);

        $this->expectOutputString($expectedString);
    }

    public function testSetPriceWithCurrency_Filled()
    {
        $price = 500000;
        $rooms = factory(Room::class)->make([
            'price_monthly' => $price
        ]);
        
        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setPriceWithCurrency', [$rooms]);

        $this->assertSame($res, (string) $price.' '.Facebook::CURRENCY);
    }

    public function testSetPriceWithCurrency_Empty()
    {
        $rooms = factory(Room::class)->make([
            'price_monthly' => 0
        ]);
        
        $res = $this->callNonPublicMethod(GoogleFeeds::class, 'setPriceWithCurrency', [$rooms]);

        $this->assertSame($res, (string)'0 '.Facebook::CURRENCY);
    }

    public function testWriteBodyCsv()
    {
        $length = 2;
        $photo  = factory(Media::class)->create();
        $rooms = factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-room-unit', 'mamirooms')
            ->create()
            ->each(function($item) use($photo) {
                factory(Card::class)->create([
                    'designer_id' => $item->id,
                    'photo_id' => $photo->id,
                    'type' => 'image',
                    'description' => 'dalam-kamar'
                ]);
            });
        $roomOwners = factory(RoomOwner::class, $length)->create()->each(function($item, $key) use($rooms) {
                $item->designer_id = $rooms[$key]->id;
            });
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: '.$length.' csv converted...'.PHP_EOL;

        $controller = $this->mockPartialAlternatively(GoogleFeeds::class);
        $controller->shouldReceive('writeToCsv')->andReturn((object) ['status' => true,'ids' => []]);
        $controller->writeBodyCsv('', $roomOwners, 'mamirooms');

        $this->expectOutputString($expectedString);
    }

    public function testGetReguler()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $length = 2;
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ])
            ->each(function($item) use($user) {
                factory(RoomOwner::class)->states('owner')->create([
                    'user_id' => $user->id,
                    'designer_id' => $item->id
                ]);
            });
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;

        (new GoogleFeeds)->getReguler('');

        $this->expectOutputString($expectedString);
    }

    public function testGetBisaBooking()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $length = 2;
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-room-unit', 'availableBooking')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ])
            ->each(function($item) use($user) {
                factory(RoomOwner::class)->states('owner')->create([
                    'user_id' => $user->id,
                    'designer_id' => $item->id
                ]);
            });
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;

        $this->callNonPublicMethod(GoogleFeeds::class, 'getBisaBooking', ['']);

        $this->expectOutputString($expectedString);
    }

    public function testGetMamirooms()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $length = 2;
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-room-unit', 'mamirooms')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_promoted' => 'false'
            ])
            ->each(function($item) use($user) {
                factory(RoomOwner::class)->states('owner')->create([
                    'user_id' => $user->id,
                    'designer_id' => $item->id
                ]);
            });
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;

        (new GoogleFeeds)->getMamirooms('');

        $this->expectOutputString($expectedString);
    }

    public function testGetGoldPlus()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $length = 2;
        $level = factory(KostLevel::class)->states('gp1', 'gp_level_1')->create();
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ])
            ->each(function($item) use($user, $level) {
                factory(RoomOwner::class)->states('owner')->create([
                    'user_id' => $user->id,
                    'designer_id' => $item->id
                ]);

                factory(KostLevelMap::class)->create([
                    'kost_id' => $item->song_id,
                    'level_id' => $level->id
                ]);
            });
        $expectedString = 'converting to csv...'.PHP_EOL.'count now: 0 csv converted...'.PHP_EOL;

        (new GoogleFeeds)->getGoldPlus(
            '',
            [$level->id]
        );

        $this->expectOutputString($expectedString);
    }

    public function testGenerate()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockFputcsv = $builder->build();
        $this->mockFputcsv->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockPublicpath = $builder->build();
        $this->mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('unlink')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockUnlink = $builder->build();
        $this->mockUnlink->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('fopen')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockFopen = $builder->build();
        $this->mockFopen->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('fclose')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $this->mockFclose = $builder->build();
        $this->mockFclose->enable();

        $controller = $this->mockPartialAlternatively(GoogleFeeds::class);
        $controller->shouldReceive('getReguler');
        $controller->shouldReceive('getMamirooms');
        $controller->shouldReceive('getBisaBooking');
        $controller->shouldReceive('getGoldplus');

        $controller->generate();
        
        $expectedString = 'checking directory'.PHP_EOL.'end checking directory'.PHP_EOL;

        $this->expectOutputString($expectedString);

        $this->mockFputcsv->disable();
        $this->mockPublicpath->disable();
        $this->mockUnlink->disable();
        $this->mockFopen->disable();
        $this->mockFclose->disable();
    }

    public function testSetGoogleCategoryString()
    {
        $expectedCategory = 'Goldplus 1 ON';
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $level = factory(KostLevel::class)->states('gp1', 'gp_level_1')->create();
        $room = factory(Room::class)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ]);
        factory(RoomOwner::class)->states('owner')->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $res = $this->callNonPublicMethod(
            GoogleFeeds::class,
            'setGoogleCategory',
            [
                $room,
                $expectedCategory
            ]
        );

        $this->assertIsString($res);
        $this->assertSame($res, $expectedCategory);
    }

    public function testSetGoogleCategoryArray()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $level = factory(KostLevel::class)->states('gp1', 'gp_level_1')->create();
        $room = factory(Room::class)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ]);
        factory(RoomOwner::class)->states('owner')->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $res = $this->callNonPublicMethod(
            GoogleFeeds::class,
            'setGoogleCategory',
            [
                $room,
                [
                    GoogleFeeds::GOLDPLUS_1,
                    GoogleFeeds::GOLDPLUS_2,
                    GoogleFeeds::GOLDPLUS_3
                ]
            ]
        );

        $this->assertIsString($res);
        $this->assertSame($res, GoogleFeeds::GOLDPLUS_1);
    }

    public function testSetGoogleCategoryArrayGoldplusPromo()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $level = factory(KostLevel::class)->create([
            'key' => 'goldplus2_promo',
            'name' => 'Mamikos Goldplus 2'
        ]);
        $room = factory(Room::class)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ]);
        factory(RoomOwner::class)->states('owner')->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $res = $this->callNonPublicMethod(
            GoogleFeeds::class,
            'setGoogleCategory',
            [
                $room,
                [
                    GoogleFeeds::GOLDPLUS_1,
                    GoogleFeeds::GOLDPLUS_2,
                    GoogleFeeds::GOLDPLUS_3
                ]
            ]
        );

        $this->assertIsString($res);
        $this->assertSame($res, GoogleFeeds::GOLDPLUS_2);
    }

    public function testSetGoogleCategoryArrayNotSet()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $level = factory(KostLevel::class)->create([
            'key' => 'goldplus100_promo',
            'name' => 'Mamikos Goldplus 100'
        ]);
        $room = factory(Room::class)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ]);
        factory(RoomOwner::class)->states('owner')->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $res = $this->callNonPublicMethod(
            GoogleFeeds::class,
            'setGoogleCategory',
            [
                $room,
                [
                    GoogleFeeds::GOLDPLUS_1,
                    GoogleFeeds::GOLDPLUS_2,
                    GoogleFeeds::GOLDPLUS_3
                ]
            ]
        );

        $this->assertIsString($res);
        $this->assertEmpty($res);
    }

    public function testSetGoogleCategoryArrayFailed()
    {
        $user = factory(User::class)->states('owner')->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $level = factory(KostLevel::class)->create([
            'key' => 'testing',
            'name' => 'testing'
        ]);
        $room = factory(Room::class)
            ->states('active', 'availablePriceMonthly', 'with-room-unit')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s'),
                'sort_score' => 4000,
                'is_testing' => false,
                'is_booking' => false,
                'is_mamirooms' => false,
                'is_promoted' => 'false'
            ]);
        factory(RoomOwner::class)->states('owner')->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $res = $this->callNonPublicMethod(
            GoogleFeeds::class,
            'setGoogleCategory',
            [
                $room,
                [
                    'testing1',
                    'testing2',
                    'testing3'
                ]
            ]
        );

        $this->assertIsString($res);
        $this->assertEmpty($res);
    }
}