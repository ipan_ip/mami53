<?php

namespace App\Entities\Component;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Test\MamiKosTestCase;
use App\Libraries\SimpleXMLExtended;
use Illuminate\Support\Facades\Log;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class Waa2Test extends MamiKosTestCase
{
    private $mockForAddChild, $mockForAddChildWithCDATA, $mockForAsXml;

    protected function setUp() : void
    {
        parent::setUp();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Libraries\SimpleXMLExtended')
            ->setName("addChild")
            ->setFunction( function ($text) {
                return $text;
            });

        $this->mockForAddChild = $builder->build();
        $this->mockForAddChild->enable();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Libraries\SimpleXMLExtended')
            ->setName("addChildWithCDATA")
            ->setFunction( function ($text) {
                return $text;
            });

        $this->mockForAddChildWithCDATA = $builder->build();
        $this->mockForAddChildWithCDATA->enable();
        
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("public_path")
            ->setFunction( function () {
                return true;
            });

        $this->mockForAsXml = $builder->build();
        $this->mockForAsXml->enable();
    }

    protected function tearDown() : void
    {
        $this->mockForAddChild->disable();
        $this->mockForAddChildWithCDATA->disable();
        $this->mockForAsXml->disable();

        parent::tearDown();
    }

    public function testGenerate()
    {
        $mockWaa2 = $this->mockPartialAlternatively(Waa2::class);
        
        $mockWaa2->shouldReceive('startWritingXML')->once();
        $mockWaa2->shouldReceive('processRooms')->once();
        $mockWaa2->shouldReceive('endWritingXML')->once();

        $mockWaa2->generate();
    }

    public function testStartWritingXML()
    {
        $waa2 = new Waa2;
        $waa2->startWritingXML();

        $this->expectOutputString('Start Writing XML...'.PHP_EOL);
        $this->assertInstanceOf( SimpleXMLExtended::class, $this->getNonPublicAttributeFromClass('xmlInfo', $waa2) );
    }

    public function testProcessRooms()
    {
        $mockWaa2 = $this->mockPartialAlternatively(Waa2::class);

        $mockWaa2->shouldReceive('setPremium')->once();
        $mockWaa2->shouldReceive('setReguler')->once();
        $mockWaa2->shouldReceive('writeLog')->once();

        $mockWaa2->processRooms();

        $this->expectOutputString('fetching premium rooms'.PHP_EOL.
            'end fetching premium rooms'.PHP_EOL.
            'fetching reguler rooms'.PHP_EOL.
            'end fetching reguler rooms'.PHP_EOL
        );
    }

    public function testWriteLog()
    {
        $roomsCount = '10';

        Log::shouldReceive('getLogger->popHandler')->once();
        Log::shouldReceive('getLogger->pushHandler')->once();
        Log::shouldReceive('getLogger->info')
            ->once()
            ->withArgs(function ($message) use ($roomsCount) {
                return strpos($message, $roomsCount) !== false;
            })
            ->andReturn(true);

        $waa2 = new Waa2();
        $method = $this->getNonPublicMethodFromClass(Waa2::class, 'writeLog');
        $res = $method->invokeArgs(
            $waa2,
            [
                $roomsCount
            ]
        );

        $this->assertNull($res);
    }

    public function testSetPremium()
    {
        $length = mt_rand(2, 10);
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'premium', 'notTesting')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s')
            ])
            ->each(function($item){
                $media = factory(Media::class)->create();
                factory(Card::class)->create([
                    'photo_id' => $media->id,
                    'designer_id' => $item->id
                ]);
            });

        $expectedString = 'write rooms to XML'.PHP_EOL.'end write rooms to XML'.PHP_EOL.
            'Rooms processed: '.$length.PHP_EOL;

        $res = (new Waa2)->setPremium();

        $this->assertNull($res);
        $this->expectOutputString($expectedString);
    }

    public function testSetReguler()
    {
        $length = mt_rand(2, 10);
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'notTesting')
            ->create([
                'kost_updated_date' => date('Y-m-d H:i:s')
            ])
            ->each(function($item){
                $media = factory(Media::class)->create();
                factory(Card::class)->create([
                    'photo_id' => $media->id,
                    'designer_id' => $item->id
                ]);
            });

        $res = (new Waa2)->setReguler();

        $expectedString = 'write rooms to XML'.PHP_EOL.'end write rooms to XML'.PHP_EOL.
            'Rooms processed: '.$length.PHP_EOL;

        $this->assertNull($res);
        $this->expectOutputString($expectedString);
    }

    public function testSetChunkedData()
    {
        $length = 3;
        $rooms = factory(Room::class, $length)->create([
            'kost_updated_date' => date('Y-m-d H:i:s')
        ])->each(function($item){
            $media = factory(Media::class)->create();
            factory(Card::class)->create([
                'photo_id' => $media->id,
                'designer_id' => $item->id
            ]);
        });

        $expectedString = 'write rooms to XML'.PHP_EOL.'end write rooms to XML'.PHP_EOL.
            'Rooms processed: '.$length.PHP_EOL;

        (new Waa2)->setChunkedData($rooms);

        $this->expectOutputString($expectedString);
    }

    public function testEndWritingXML()
    {
        (new Waa2)->endWritingXML();

        $this->expectOutputString('xml converted...'.PHP_EOL);
    }

    public function testConvertObjectToXMLDescAvailable()
    {
        $mediaRoom = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'photo_id' => $mediaRoom->id,
            'description' => 'testing description'
        ]);
        $mediaCard = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $mediaCard->id,
            'designer_id' => $room->id,
            'description' => 'testing-dalam-kamar-1'
        ]);

        $simpleXmlExtended = (new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>"));
        (new Waa2)->convertObjectToXML($room, $simpleXmlExtended);
    }

    public function testConvertObjectToXMLDescEmpty()
    {
        $mediaRoom = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'photo_id' => $mediaRoom->id,
            'description' => '-'
        ]);
        $mediaCard = factory(Media::class)->create();
        factory(Card::class)->create([
            'photo_id' => $mediaCard->id,
            'designer_id' => $room->id,
            'description' => 'testing-dalam-kamar-1'
        ]);

        $simpleXmlExtended = (new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>"));
        (new Waa2)->convertObjectToXML($room, $simpleXmlExtended);
    }
}
