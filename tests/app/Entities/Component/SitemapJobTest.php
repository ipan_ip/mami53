<?php

namespace App\Entities\Component;

use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SitemapJobTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockSitemapJob = $this->mockPartialAlternatively(SitemapJob::class);
        $mockSitemapJob->shouldReceive('sitemapLandingPage')->once();
        $mockSitemapJob->shouldReceive('sitemapFilterJob')->once();
        $mockSitemapJob->shouldReceive('sitemapJob')->once();
        $mockSitemapJob->shouldReceive('sitemapJobOther')->once();
        $mockSitemapJob->shouldReceive('indexingSitemap')->once();

        $mockSitemapJob->generate();
    }

    public function testRemoveOldSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('rename')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockRename = $builder->build();
        $mockRename->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'Deleting the old sitemap'.PHP_EOL.PHP_EOL;
        $this->callNonPublicMethod(SitemapJob::class, 'removeOldSitemap', []);

        $this->expectOutputString($expectedString);

        $mockRename->disable();
        $mockPublicPath->disable();
        $mockMkdir->disable();
    }

    public function testSitemapLandingPage()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $length = 3;
        $landings = factory(LandingVacancy::class, $length)->create([
            'redirect_id' => null,
            'is_niche' => 1
        ]);

        $string = implode('', $landings->map(function($landing){
            return 'Adding generate sitemap for landing : '.$landing->slug.PHP_EOL.PHP_EOL;
        })->toArray());
        $expectedString = 'Prepare generating sitemap for landing page'.PHP_EOL.
            $string.
            'Collecting sitemap done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing'.PHP_EOL;

        (new SitemapJob)->sitemapLandingPage();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }

    public function testSitemapFilterJob()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        (new SitemapJob)->sitemapFilterJob();

        $mockPublicPath->disable();
    }

    public function testSitemapJob()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $defaultCities = $this->getNonPublicAttributeFromClass( 'defaultCities', (new SitemapJob));

        array_map(function($city) {
            factory(Vacancy::class)->states('active')->create([
                'is_indexed' => 1,
                'city' => $city,
                'group' => 'niche',
                'vacancy_aggregator_id' => null
            ]);
        }, $defaultCities);

        (new SitemapJob)->sitemapJob();

        $mockPublicPath->disable();
    }

    public function testSitemapJobOther()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        factory(Vacancy::class, 3)->states('active')->create([
            'city' => 'testing',
            'is_indexed' => 1,
            'group' => 'niche',
            'vacancy_aggregator_id' => null
        ]);

        (new SitemapJob)->sitemapJobOther();

        $mockPublicPath->disable();
    }

    public function testIndexingSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        (new SitemapJob)->indexingSitemap();

        $mockPublicPath->disable();
    }
}