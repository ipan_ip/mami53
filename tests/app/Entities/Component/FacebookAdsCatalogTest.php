<?php

namespace App\Entities\Component;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

class FacebookAdsCatalogTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockCatalog = $this->mockPartialAlternatively(FacebookAdsCatalog::class);
        $mockCatalog->shouldReceive('backupOldCatalog');
        $mockCatalog->shouldReceive('getRoomsData');

        $mockCatalog->generate();
    }

    public function testGetRoomsData()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fopen')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFopen = $builder->build();
        $mockFopen->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputCsv = $builder->build();
        $mockFputCsv->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fclose')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFclose = $builder->build();
        $mockFclose->enable();

        $length = 3;
        factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-address')
            ->create();

        $expectedString = 'fetching data'.PHP_EOL.
            'converting to csv...'.PHP_EOL.
            'csv converted...'.PHP_EOL.
            'end fetching data...'.PHP_EOL.
        (new FacebookAdsCatalog())->getRoomsData();

        $this->expectOutputString($expectedString);

        $mockFopen->disable();
        $mockPublicpath->disable();
        $mockFputCsv->disable();
        $mockFclose->disable();
    }

    public function testBackupOldCatalogPublicPathFacebookads()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($key) {
                    if ($key === 'facebookads') {
                        return true;
                    }

                    return false;
                }
            );
        $mockFileexists = $builder->build();
        $mockFileexists->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fclose')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFclose = $builder->build();
        $mockFclose->enable();

        $expectedString = 'backing up data'.PHP_EOL.
            'end backing up data'.PHP_EOL;

        (new FacebookAdsCatalog())->backupOldCatalog();

        $this->expectOutputString($expectedString);

        $mockFileexists->disable();
        $mockPublicpath->disable();
        $mockMkdir->disable();
        $mockCopy->disable();
        $mockFclose->disable();
    }

    public function testGenerateCatalog()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockFputCsv = $builder->build();
        $mockFputCsv->enable();

        $length = 3;
        $photoId = factory(Media::class)->create();
        $rooms = factory(Room::class, $length)
            ->states('active', 'availablePriceMonthly', 'with-address', 'with-slug')
            ->create([
                'photo_id' => $photoId
            ]);

        $expectedString = 'converting to csv...'.PHP_EOL.
            'csv converted...'.PHP_EOL;
        (new FacebookAdsCatalog())->generateCatalog($rooms, '');

        $this->expectOutputString($expectedString);

        $mockFputCsv->disable();
    }
}