<?php

namespace App\Entities\Component;

use App\Entities\Vacancy\CompanyProfile;
use App\Test\MamiKosTestCase;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SitemapCompanyTest extends MamiKosTestCase
{
    public function testGenerate()
    {
        $mockSitemapCompany = $this->mockPartialAlternatively(SitemapCompany::class);
        $mockSitemapCompany->shouldReceive('sitemapCompanyProfile')->once();
        $mockSitemapCompany->generate();
        
    }

    public function testRemoveOldSitemap()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('rename')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockRename = $builder->build();
        $mockRename->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $expectedString = 'Deleting the old sitemap'.PHP_EOL.PHP_EOL;
        $this->callNonPublicMethod(SitemapCompany::class, 'removeOldSitemap', []);

        $this->expectOutputString($expectedString);

        $mockRename->disable();
        $mockPublicPath->disable();
        $mockMkdir->disable();
    }

    public function testsitemapCompanyProfile()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockPublicPath = $builder->build();
        $mockPublicPath->enable();

        factory(CompanyProfile::class)->states('active')->create();

        $expectedString = 'Prepare generating sitemap for Company Profile'.PHP_EOL.
            'Collecting Sitemap Company Profile Done'.PHP_EOL.PHP_EOL.PHP_EOL.
            'Success Generate XML For Landing'.PHP_EOL;
        (new SitemapCompany)->sitemapCompanyProfile();

        $this->expectOutputString($expectedString);

        $mockPublicPath->disable();
    }
}