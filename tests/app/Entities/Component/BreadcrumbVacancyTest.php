<?php

namespace App\Entities\Component;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Landing\LandingVacancy;
use App\Test\MamiKosTestCase;

class BreadcrumbVacancyTest extends MamiKosTestCase
{
    public function testGenerate_Null()
    {
        $res = BreadcrumbVacancy::generate();
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_LandingVacancyIf()
    {
        $parentLandingVacancy = factory(LandingVacancy::class)->create();
        $childLandingVacancy = factory(LandingVacancy::class)->create([
            'parent_id' => $parentLandingVacancy->id
        ]);
        $res = BreadcrumbVacancy::generate($childLandingVacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_LandingVacancyElse()
    {
        $landingVacancy = factory(LandingVacancy::class)->create();
        $res = BreadcrumbVacancy::generate($landingVacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_VacancyBreadcrumbIfSubdistrictNotNull()
    {
        $city ='testingCity';
        $subdistrict = 'testingSubdistrict';
        $vacancy = factory(Vacancy::class)->create([
            'city' => $city,
            'subdistrict' => $subdistrict
        ]);
        factory(LandingVacancy::class)->create([
            'heading_1' => 'title '.$subdistrict
        ]);
        $res = BreadcrumbVacancy::generate($vacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_VacancyBreadcrumbIfCityNotNull()
    {
        $city ='testingCity';
        $vacancy = factory(Vacancy::class)->create([
            'city' => $city,
        ]);
        factory(LandingVacancy::class)->create([
            'heading_1' => 'title '.$city
        ]);
        $res = BreadcrumbVacancy::generate($vacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_LandingNull()
    {
        $city ='testingCity';
        $vacancy = factory(Vacancy::class)->create([
            'city' => $city
        ]);
        $res = BreadcrumbVacancy::generate($vacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGenerate_VacancyBreadcrumbElse()
    {
        $subdistrict = 'testingSubdistrict';
        $vacancy = factory(Vacancy::class)->create([
            'city' => $subdistrict,
            'subdistrict' => $subdistrict
        ]);
        factory(LandingVacancy::class, 2)->create([
            'area_city' => $subdistrict
        ]);
        $res = BreadcrumbVacancy::generate($vacancy);
        $this->assertInstanceOf(BreadcrumbVacancy::class, $res);
    }

    public function testGetList()
    {
        $subdistrict = 'testingSubdistrict';
        $vacancy = factory(Vacancy::class)->create([
            'city' => $subdistrict,
            'subdistrict' => $subdistrict
        ]);
        factory(LandingVacancy::class, 2)->create([
            'area_city' => $subdistrict
        ]);
        $breadcrumbVacancy = new BreadcrumbVacancy;
        $res = $breadcrumbVacancy::generate($vacancy)->getList();
        $this->assertIsArray($res);
    }
}