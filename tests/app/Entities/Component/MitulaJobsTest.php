<?php

namespace App\Entities\Component;

use App\Entities\Vacancy\Vacancy;
use App\Test\MamiKosTestCase;
use App\Libraries\SimpleXMLExtended;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use phpmock\MockBuilder;

class MitulaJobsTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        factory(AreaBigMapper::class);
    }

    public function testGenerate()
    {
        $mockMitula = $this->mockPartialAlternatively(MitulaJobs::class);
        $mockMitula->shouldReceive('startWritingXML')->once();
        $mockMitula->shouldReceive('processJobs')->once();
        $mockMitula->shouldReceive('endWritingXML')->once();

        $mockMitula->generate();
    }

    public function testStartWritingXML()
    {
        $expectedString = "Start Writing XML...".PHP_EOL;
        (new MitulaJobs)->startWritingXML();

        $this->expectOutputString($expectedString);
    }

    public function testEndWritingXML()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(function () {return 'mitula.xml';});
        $this->mockPublicPath = $builder->build();
        $this->mockPublicPath->enable();

        $mitula = new MitulaJobs;
        $mitula->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");

        $expectedString = "xml converted...".PHP_EOL;

        $mitula->endWritingXML();

        $this->expectOutputString($expectedString);

        $this->mockPublicPath->disable();
    }

    public function testbackupOldJobsIfTrue()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($key) {
                    if ($key === 'mitulajobs') {
                        return true;
                    }

                    return false;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $this->callNonPublicMethod(MitulaJobs::class, 'backupOldJobs', []);

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockCopy->disable();
        $mockMkdir->disable();
    }

    public function testbackupOldJobsIfFalse()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function () {
                    return false;
                }
            );
        $mockFilexists = $builder->build();
        $mockFilexists->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('public_path')
            ->setFunction(
                function (string $text) {
                    return $text;
                }
            );
        $mockPublicpath = $builder->build();
        $mockPublicpath->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('copy')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockCopy = $builder->build();
        $mockCopy->enable();

        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mockMkdir = $builder->build();
        $mockMkdir->enable();

        $this->callNonPublicMethod(MitulaJobs::class, 'backupOldJobs', []);

        $mockFilexists->disable();
        $mockPublicpath->disable();
        $mockCopy->disable();
        $mockMkdir->disable();
    }

    public function testProcessJobs()
    {
        $createdAt = Carbon::now()->subDays(5);
        $expiredDate = Carbon::now()->addDays(1);
        $length = 4;
        factory(Vacancy::class, $length)->states('active', 'full-time')->create([
            'created_at' => $createdAt,
            'expired_date' => $expiredDate,
            'vacancy_aggregator_id' => null
        ]);
        factory(Vacancy::class, $length)->states('active', 'part-time')->create([
            'created_at' => $createdAt,
            'expired_date' => $expiredDate,
            'vacancy_aggregator_id' => null
        ]);
        factory(Vacancy::class, $length)->states('active', 'magang')->create([
            'created_at' => $createdAt,
            'expired_date' => $expiredDate,
            'vacancy_aggregator_id' => null
        ]);
        factory(Vacancy::class, $length)->states('active', 'freelance')->create([
            'created_at' => $createdAt,
            'expired_date' => $expiredDate,
            'vacancy_aggregator_id' => null
        ]);

        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $expectedString = 'fetching jobs data'.PHP_EOL.
            'start fetching jobs data'.PHP_EOL.
            'offset : 0'.PHP_EOL.
            'end fetching data...'.PHP_EOL.
            'write jobs to XML'.PHP_EOL.
            'end write jobs to XML'.PHP_EOL.
            'start fetching jobs data'.PHP_EOL.
            'offset : 200'.PHP_EOL.
            'end fetching data...'.PHP_EOL;
        $mitulaJobs = new MitulaJobs;
        $mitulaJobs->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><MitulaJobs></MitulaJobs>");
        $mitulaJobs->processJobs();


        $this->expectOutputString($expectedString);
    }
}