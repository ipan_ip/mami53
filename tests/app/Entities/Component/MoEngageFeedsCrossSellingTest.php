<?php 

namespace App\Entities\Component;

use App\Entities\Component\MoEngageFeedsCrossSelling;
use App\Entities\Feeds\Facebook;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use phpmock\MockBuilder;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MoEngageFeedsCrossSellingTest extends MamiKosTestCase
{
    public static $functions;
    protected $faker, $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->faker = Faker::create();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testSetRecommendedRooms_OneMamiRooms()
    {
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $referenceRoom = factory(Room::class)->make([
            'area_subdistrict' => $subdistrict,
            'area_city' => $city
        ]);
        $recommendedRooms = factory(Room::class, 1)->states('mamirooms')->make();
        
        $repo = $this->mockAlternatively('App\Repositories\Feeds\MoEngage\CrossSellingRepository');
        $repo->shouldReceive('getRecommendedMamiRooms')
            ->with($subdistrict, $city)
            ->once()
            ->andReturn($recommendedRooms);

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->setRecommendedRooms(
            $referenceRoom,
            500000,
            MoEngageFeedsCrossSelling::TAG_ID_NEEDED,
            MoEngageFeedsCrossSelling::MAMIROOMS_TYPE
        );

        $this->assertNotNull($res->recommended);
        $this->assertInstanceOf(Room::class, $res);
    }

    public function testSetRecommendedRooms_OneGoldPlus()
    {
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $referenceRoom = factory(Room::class)->make([
            'area_subdistrict' => $subdistrict,
            'area_city' => $city
        ]);
        $recommendedRooms = factory(Room::class, 1)->make();

        $repo = $this->mockAlternatively('App\Repositories\Feeds\MoEngage\CrossSellingRepository');
        $repo->shouldReceive('getRecommendedGoldPlusRooms')
            ->with($subdistrict, $city)
            ->once()
            ->andReturn($recommendedRooms);

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->setRecommendedRooms(
            $referenceRoom,
            500000,
            MoEngageFeedsCrossSelling::TAG_ID_NEEDED,
            MoEngageFeedsCrossSelling::GOLDPLUS_TYPE
        );

        $this->assertNotNull($res->recommended);
        $this->assertInstanceOf(Room::class, $res);
    }

    public function testSetRecommendedRooms_MoreThanOneMamiRooms()
    {
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $referenceRoom = factory(Room::class)->make([
            'area_subdistrict' => $subdistrict,
            'area_city' => $city
        ]);
        $recommendedRooms = factory(Room::class, 3)->states('mamirooms')->make();

        $repo = $this->mockAlternatively('App\Repositories\Feeds\MoEngage\CrossSellingRepository');
        $repo->shouldReceive('getRecommendedGoldPlusRooms')
            ->with($subdistrict, $city)
            ->once()
            ->andReturn($recommendedRooms);

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->setRecommendedRooms(
            $referenceRoom,
            500000,
            MoEngageFeedsCrossSelling::TAG_ID_NEEDED,
            MoEngageFeedsCrossSelling::GOLDPLUS_TYPE
        );

        $this->assertInstanceOf(Room::class, $res);
    }

    public function testSetCountGoldPlusMamirooms_Mamirooms()
    {
        $length = 1;
        $roomMamirooms = factory(Room::class, $length)->states('active', 'mamirooms')->make();

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->setCountGoldPlusMamirooms($roomMamirooms);
        
        $this->assertIsObject($res);
        $this->assertSame($length, $res->mamirooms);
    }

    public function testSetCountGoldPlusMamirooms_Goldplus()
    {
        $length = 1;
        $roomGoldPlus = factory(Room::class, 1)->states('active')->make();
        $level = factory(KostLevel::class)->states('goldplus')->make();
        $roomGoldPlus->each(function($item) use($level) {
            $item->level = $level;
            return $item;
        });

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->setCountGoldPlusMamirooms($roomGoldPlus);

        $this->assertIsObject($res);
    }

    public function testFilterMamiroomsOrGoldPlus_OneGoldplus()
    {
        $item = factory(Room::class)->states('active')->create();
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->create();
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $recommends->each(function($item, $key) use($level) {
            if ($key%2 === 0) {
                $item->level = $level;
            }

            if ($key%2 === 1) {
                $item->is_mamirooms = 1;
            }
        });

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterMamiroomsOrGoldPlus($recommends, $item, 500000, []);
        
        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame(1, $res->count());
    }

    public function testFilterMamiroomsOrGoldPlus_MultipleGoldplus()
    {
        $item = factory(Room::class)->states('active')->make([
            'price_monthly' => 500000
        ]);
        $length = 3;
        $recommends = factory(Room::class, $length)->states('active')->make();
        $level = factory(KostLevel::class, 1)->states('goldplus')->make();
        $recommends->each(function($item, $key) use($level) {
            if ($key%2 === 0) {
                $item->price_monthly = 1500000;
                if ($key === 0) {
                    $item->price_monthly = 1000000;
                }
                $item->level = $level;
            }

            if ($key%2 === 1) {
                $item->is_mamirooms = 1;
            }
        });
        
        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterMamiroomsOrGoldPlus($recommends, $item, 500000, []);

        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame(1, $res->count());
    }

    public function testFilterByPriceRange_One()
    {
        $item = factory(Room::class)->states('active')->make([
            'price_monthly' => 500000
        ]);
        $length = 1;
        $recommends = factory(Room::class, $length)->states('active')->make([
            'price_monthly' => 1000000
        ]);
        
        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByPriceRange($recommends, $item, 500000, []);

        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame(1, $res->count());
    }

    public function testFilterByPriceRange_Multiple()
    {
        $item = factory(Room::class)->states('active')->make([
            'price_monthly' => 500000
        ]);
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->make([
            'price_monthly' => 1000000
        ]);
        
        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByPriceRange($recommends, $item, 500000, []);

        $this->assertNull($res);
    }

    public function testFilterByTag_One()
    {
        $tag = factory(Tag::class)->create([
            'id' => 15
        ]);
        $item = factory(Room::class)->states('active')->create();
        $item->tags = $tag;
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->create();
        $recommends->first()->tags = $tag;

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByTag($recommends, $item, [15]);

        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testFilterByTag_Multiple()
    {
        $tag = factory(Tag::class)->make([
            'id' => 15
        ]);
        $item = factory(Room::class)->states('active')->make();
        $item->tags = $tag;
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->make();
        $recommends->each(function($item) use($tag) {
            $item->tags = $tag;
        });

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByTag($recommends, $item, [15]);

        $this->assertNull($res);
    }

    public function testFilterByLongLat_One()
    {
        $item = factory(Room::class)->states('active')->make([
            'latitude' => -7.758377,
            'longitude' => 110.377878
        ]);
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->make();
        $recommends->first()->latitude = -7.758377;
        $recommends->first()->longitude = 110.377878;

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByLongLat($recommends, $item);

        $this->assertInstanceOf(Collection::class, $res);
        $this->assertSame(1, $res->count());
    }

    public function testFilterByLongLat_Null()
    {
        $item = factory(Room::class)->states('active')->make();
        $length = 2;
        $recommends = factory(Room::class, $length)->states('active')->make();

        $moEngageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $res = $moEngageFeedsCrossSelling->filterByLongLat($recommends, $item);

        $this->assertNull($res);
    }

    public function testSetLoyaltyProgramRecommendedRoom()
    {
        $recommends = factory(Room::class)->states('active')->make();
        $level = factory(KostLevel::class, 1)->states('goldplus')->make();
        $recommends->level = $level;

        $mockMoEngageCrossSelling = $this->mockPartialAlternatively(MoEngageFeedsCrossSelling::class);

        $mockMoEngageCrossSelling->shouldReceive('setLoyaltyProgram')->once()->andReturn(Facebook::GOLDPLUS);

        $res = $mockMoEngageCrossSelling->setLoyaltyProgramRecommendedRoom($recommends, 'moengage');
        
        $this->assertIsString($res);
        $this->assertNotFalse(strpos($res, 'oldplus'));
    }

    public function testGetCsvPath()
    {
        $mockMoEngageCrossSelling = new MoEngageFeedsCrossSelling();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeedsCrossSelling::class, 'getCsvPath');
        $res = $method->invoke($mockMoEngageCrossSelling);
        
        $this->assertIsString($res);
    }

    public function testWriteHeaderCsv()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('fputcsv')
            ->setFunction(
                function () {
                    return true;
                }
            );
        $mock = $builder->build();
        $mock->enable();

        $moengageFeedsCrossSelling = new MoEngageFeedsCrossSelling();
        $method = $this->getNonPublicMethodFromClass(MoEngageFeedsCrossSelling::class, 'writeHeaderCsv');
        $result = $method->invokeArgs(
            $moengageFeedsCrossSelling,
            [
                ''
            ]
        );

        $this->assertNull($result);

        $mock->disable();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGenerateSinggahsini()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("file_exists")
            ->setFunction( function () { return true; });
        $mockFileExists = $builder->build();
        $mockFileExists->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("unlink")
            ->setFunction( function () { return true; });
        $mockUnlink = $builder->build();
        $mockUnlink->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("fopen")
            ->setFunction( function () { return ''; });
        $mockFopen = $builder->build();
        $mockFopen->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("fputcsv")
            ->setFunction( function () { return true; });
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Entities\Component\Traits')
            ->setName("fputcsv")
            ->setFunction( function () { return true; });
        $mockFputcsvTrait = $builder->build();
        $mockFputcsvTrait->enable();

        $length = 3;
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $media = factory(Media::class)->create();

        //Reference Rooms
        factory(Room::class, $length)
            ->states(
                'active',
                'notTesting',
                'availablePriceMonthly',
                'with-address'
            )
            ->create([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])
            ->each(function($item) use($media) {
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
                    'designer_id' => $item->id,
                    'photo_id' => $media->id
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;
            });

        //Singgahsini Recommended Rooms
        factory(Room::class, $length)
            ->states(
                'active',
                'notTesting',
                'availablePriceMonthly',
                'with-address',
                'mamirooms'
            )
            ->create([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])
            ->each(function($item) use($media) {
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
                    'designer_id' => $item->id,
                    'photo_id' => $media->id
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;
            });

        $res = (new MoEngageFeedsCrossSelling())->generate();

        $this->assertNull($res);

        $mockFileExists->disable();
        $mockUnlink->disable();
        $mockFopen->disable();
        $mockFputcsv->disable();
        $mockFputcsvTrait->disable();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGenerateGoldplus()
    {
        $kostLevel = factory(KostLevel::class)->state('goldplus')->create();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("file_exists")
            ->setFunction( function () { return true; });
        $mockFileExists = $builder->build();
        $mockFileExists->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("unlink")
            ->setFunction( function () { return true; });
        $mockUnlink = $builder->build();
        $mockUnlink->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("fopen")
            ->setFunction( function () { return ''; });
        $mockFopen = $builder->build();
        $mockFopen->enable();

        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName("fputcsv")
            ->setFunction( function () { return true; });
        $mockFputcsv = $builder->build();
        $mockFputcsv->enable();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Entities\Component\Traits')
            ->setName("fputcsv")
            ->setFunction( function () { return true; });
        $mockFputcsvTrait = $builder->build();
        $mockFputcsvTrait->enable();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Repositories\Feeds\MoEngage')
            ->setName("env")
            ->setFunction( function () use($kostLevel) { 
                return $kostLevel->id;
            });
        $mockEnv = $builder->build();
        $mockEnv->enable();

        $length = 3;
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $media = factory(Media::class)->create();

        //Reference Rooms
        factory(Room::class, $length)
            ->states(
                'active',
                'notTesting',
                'availablePriceMonthly',
                'with-address'
            )
            ->create([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])
            ->each(function($item) use($media) {
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
                    'designer_id' => $item->id,
                    'photo_id' => $media->id
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;
            });

        //Singgahsini Recommended Rooms
        factory(Room::class, $length)
            ->states(
                'active',
                'notTesting',
                'availablePriceMonthly',
                'with-address'
            )
            ->create([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])
            ->each(function($item) use($media, $kostLevel) {
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
                    'designer_id' => $item->id,
                    'photo_id' => $media->id
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;

                factory(KostLevelMap::class)->create([
                    'kost_id' => $item->song_id,
                    'level_id' => $kostLevel->id
                ]);
            });

        $res = (new MoEngageFeedsCrossSelling())->generate();

        $this->assertNull($res);

        $mockFileExists->disable();
        $mockUnlink->disable();
        $mockFopen->disable();
        $mockFputcsv->disable();
        $mockFputcsvTrait->disable();
        $mockEnv->disable();
    }

    public function testCatalog()
    {
        $length = 3;
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $recommendedRooms = factory(Room::class, 1)->states('mamirooms')->make([])
            ->each(function($item){
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;
            });
        $rooms = factory(Room::class, $length)->make([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])->each(function($item) use($recommendedRooms) {
                $cards = factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
                ])->each(function($item){
                    $item->photo = factory(Media::class)->create();
                });

                $item->cards = $cards;
                $item->recommended = $recommendedRooms;
            });
        $roomIds = $rooms->pluck('song_id')->toArray();

        //MOCK WRITELOG
        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $mockMoEngageCrossSelling = $this->mockPartialAlternatively(MoEngageFeedsCrossSelling::class);
        $mockMoEngageCrossSelling->shouldReceive('writeToCsv')->andReturn(
            (object) [
                'status' => true,
                'ids' => $roomIds
            ]);

        $res = $mockMoEngageCrossSelling->generateCatalog(
            $rooms,
            ''
        );

        $this->assertNull($res);
    }

    public function testCatalogImageEmpty()
    {
        $length = 3;
        $subdistrict = $this->faker->cityPrefix;
        $city = $this->faker->city;
        $recommendedRooms = factory(Room::class, 1)->states('mamirooms')->make([]);
        $rooms = factory(Room::class, $length)->make([
                'area_subdistrict' => $subdistrict,
                'area_city' => $city
            ])->each(function($item) use($recommendedRooms) {
                $item->recommended = $recommendedRooms;
            });
        $roomIds = $rooms->pluck('song_id')->toArray();

        //MOCK WRITELOG
        Log::shouldReceive('getLogger->popHandler');
        Log::shouldReceive('getLogger->pushHandler');
        Log::shouldReceive('getLogger->info')->andReturn(true);

        $mockMoEngageCrossSelling = $this->mockPartialAlternatively(MoEngageFeedsCrossSelling::class);
        $mockMoEngageCrossSelling->shouldReceive('writeToCsv')->andReturn(
            (object) [
                'status' => true,
                'ids' => $roomIds
            ]);

        $res = $mockMoEngageCrossSelling->generateCatalog(
            $rooms,
            ''
        );

        $this->assertNull($res);
    }
}