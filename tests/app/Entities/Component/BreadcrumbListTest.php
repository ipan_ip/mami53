<?php

namespace App\Entities\Component;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class BreadcrumbListTest extends MamiKosTestCase
{
    public function testGenerate_ApartmentUnitBreadcrumb()
    {
        $apartmentProject = factory(ApartmentProject::class)->states('active')->create();
        $room = factory(Room::class)->states('active')->create([
            'apartment_project_id' => $apartmentProject->id
        ]);

        $res = BreadcrumbList::generate($room);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testGenerate_ApartmentProjectBreadcrumbIfIfAreaNotNull()
    {
        $apartmentProject = factory(ApartmentProject::class)->states('active')->create([
            'area_subdistrict' => 'testingUT'
        ]);
        $landingApartment = factory(LandingApartment::class)->create([
            'heading_1' => 'Temp '.$apartmentProject->area_subdistrict.' temp'
        ]);
        $room = factory(Room::class)->states('active')->create([
            'apartment_project_id' => $apartmentProject->id
        ]);

        $res = BreadcrumbList::generate($apartmentProject);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testGenerate_ApartmentProjectBreadcrumbAreaAreaCity()
    {
        $apartmentProject = factory(ApartmentProject::class)->states('active')->create([
            'area_city' => 'testingUT',
            'area_subdistrict' => ''
        ]);
        $landingApartment = factory(LandingApartment::class)->create([
            'heading_1' => 'Temp '.$apartmentProject->area_city.' temp'
        ]);
        $room = factory(Room::class)->states('active')->create([
            'apartment_project_id' => $apartmentProject->id
        ]);

        $res = BreadcrumbList::generate($apartmentProject);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testGenerate_ApartmentProjectBreadcrumbElseElse()
    {
        $apartmentProject = factory(ApartmentProject::class)->states('active')->create();
        $landingApartment = factory(LandingApartment::class)->create([
            'area_city' => $apartmentProject->area_city,
            'area_subdistrict' => $apartmentProject->area_subdistrict
        ]);
        $room = factory(Room::class)->states('active')->create([
            'apartment_project_id' => $apartmentProject->id
        ]);

        $res = BreadcrumbList::generate($apartmentProject);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testroomBreadcrumb_AreaNull()
    {
        $room = factory(Room::class)->states('active')->create([
            'area_subdistrict' => '',
            'area_city' => 'tempTestingUt'
        ]);

        $res = BreadcrumbList::generate($room);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testroomBreadcrumb_Else()
    {
        $room = factory(Room::class)->states('active')->create([
            'area_subdistrict' => 'tempTestingUtCity',
            'area_city' => 'tempTestingUtCity'
        ]);
        $landing = factory(Landing::class)->create([
            'area_city' => $room->area_city,
            'area_subdistrict' => $room->area_subdistrict,
        ]);

        $res = BreadcrumbList::generate($room);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testLandingBreadcrumb_parentIdAvailable()
    {
        $parentLanding = factory(Landing::class)->create([
            'area_city' => 'parentTestingUtCity',
            'area_subdistrict' => 'parentTestingUtSubdistrict'
        ]);
        $childLanding = factory(Landing::class)->create([
            'area_city' => 'childTestingUtCity',
            'area_subdistrict' => 'childTestingUtSubdistrict',
            'parent_id' => $parentLanding->id
        ]);

        $res = BreadcrumbList::generate($childLanding);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }

    public function testlandingApartmentBreadcrumb_parentIdAvailable()
    {
        $parentLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => 'parentTestingUtCity',
            'area_subdistrict' => 'parentTestingUtSubdistrict'
        ]);
        $childLandingApartment = factory(LandingApartment::class)->create([
            'area_city' => 'childTestingUtCity',
            'area_subdistrict' => 'childTestingUtSubdistrict',
            'parent_id' => $parentLandingApartment->id
        ]);

        $res = BreadcrumbList::generate($childLandingApartment);

        $this->assertInstanceOf(BreadcrumbList::class, $res);
    }
}