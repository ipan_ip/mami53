<?php

namespace App\Entities\Component;

use App\Test\MamiKosTestCase;
use App\Repositories\Landing\LandingRepository;
use App\Entities\Landing\Landing;
use Faker\Factory as Faker;
use phpmock\MockBuilder;

function file_put_contents()
{
    return false;
}

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class ListCampusTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->faker = Faker::create();
        $this->repo = $this->mockAlternatively(LandingRepository::class);
    }

    public function testGenerate()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('file_exists')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $builder->setNamespace(__NAMESPACE__)
            ->setName('mkdir')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();

        $landings = factory(Landing::class, 3)->states('campus')->create([
            'keyword' => 'tester'
        ]);
        $expected = $this->repo->shouldReceive('getCampusType')->andReturn($landings);

        $res = (new ListCampus($this->repo))->generate();
        
        $this->assertNull($res);

        $this->mock->disable();
    }
}