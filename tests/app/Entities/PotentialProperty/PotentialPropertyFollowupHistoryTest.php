<?php

namespace App\Entities\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class PotentialPropertyFollowupHistoryTest extends MamiKosTestCase
{
    public function testPotentialPropertyRelation(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $history = factory(PotentialPropertyFollowupHistory::class)->create(['potential_property_id' => $expected->id]);

        $actual = $history->potential_property;

        $this->assertTrue($actual->is($expected));
    }

    public function testRoomRelation(): void
    {
        $expected = factory(Room::class)->create();
        $history = factory(PotentialPropertyFollowupHistory::class)->create(['designer_id' => $expected->id]);

        $actual = $history->room;

        $this->assertTrue($actual->is($expected));
    }
}
