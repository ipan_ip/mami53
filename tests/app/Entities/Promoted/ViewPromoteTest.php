<?php

namespace App\Entities\Promoted;

use App\Entities\Promoted\ViewPromote;
use App\Entities\Promoted\HistoryPromote;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\Room;
use App\Entities\Premium\ClickPricing;
use App\User;
use App\Test\MamiKosTestCase;

class ViewPromoteTest extends MamiKosTestCase
{
    
    public function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetDailyBudgetAllocation()
    {
        $dailyBudget = 1000;
        $promote = factory(ViewPromote::class)->create([
            "daily_budget" => $dailyBudget,
        ]);
        
        $dailyBudgetAllocation = $promote->dailyBudgetAllocation();
        $this->assertEquals($dailyBudget, $dailyBudgetAllocation);
    }

    public function testGetUsedBalanceWhenActiveAndNotDailyAllocation()
    {
        $promote = factory(ViewPromote::class)->create([
            "is_active" => 1,
            "total" => 1000,
            "used" => 100,
            "daily_budget" => 0,
            "history" => 300
        ]);

        $usedBalance = $promote->usedBalance();
        $this->assertEquals(400, $usedBalance);
    }

    public function testGetUsedBalanceWhenNotActiveAndNotDailyAllocation()
    {
        $promote = factory(ViewPromote::class)->create([
            "is_active" => 0,
            "total" => 1000,
            "used" => 0,
            "daily_budget" => 0,
            "history" => 300
        ]);

        $usedBalance = $promote->usedBalance();
        $this->assertEquals(0, $usedBalance);
    }

    public function testGetUsedBalanceWhenDailyAllocationIsActive()
    {
        $promote = factory(ViewPromote::class)->create([
            "is_active" => 1,
            "total" => 1000,
            "used" => 200,
            "daily_budget" => 1000,
            "history" => 300
        ]);

        $usedBalance = $promote->usedBalance();
        $this->assertEquals(200, $usedBalance);
    }

    public function testUpdateEndHistoryShouldSuccess()
    {
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $vp1 = factory(ViewPromote::class)->create([
            'designer_id' => $room1,
            'is_active' => 1,
            'total' => 1800,
            'used' => 900
        ]);
        $vp2 = factory(ViewPromote::class)->create([
            'designer_id' => $room2,
            'is_active' => 1,
            'total' => 1800,
            'used' => 900
        ]);

        factory(HistoryPromote::class)->create([
            'view_promote_id'   => $vp1->id,
            'session_id'    => $vp1->session_id
        ]);

        $result1 = ViewPromote::updateEndHistory($vp1);
        $result2 = ViewPromote::updateEndHistory($vp2);

        $this->assertTrue($result1);
        $this->assertFalse($result2);
    }

    public function testChangeNewRequestIdShouldSuccess()
    {
        $premiumReqId = factory(PremiumRequest::class)->create()->id;
        factory(ViewPromote::class)->create([
            'premium_request_id'    => $premiumReqId
        ]);
        $pr = factory(PremiumRequest::class)->create();

        $result = ViewPromote::ChangeNewRequestId($premiumReqId, $pr->id);

        $this->assertEquals(1, $result);

    }

    public function testGetDailyUsedShouldSuccess()
    {
        $promote1 = factory(ViewPromote::class)->create([
            'total'     => 0,
            'history'   => 0,
            'used'      => 0
        ]);
        $promote2 = factory(ViewPromote::class)->create([
            'total'     => 200000,
            'history'   => 45700,
            'used'      => 45700
        ]);

        $this->assertEquals(0, $promote1->dailyUsed());
        $this->assertEquals(108600, $promote2->dailyUsed());
    }

    public function testIncreaseView()
    {
        $room = factory(Room::class)->create();
        $room2 = factory(Room::class)->create([
            'area_city' => 'Jogja',
            'price_monthly' => 900000
        ]);
        $user = factory(User::class)->create();
        
        app()->device = null;
        app()->user = $user;

        factory(ClickPricing::class)->create([
            'area_city' => 'Jogja',
            'property_median_price' => 800000,
            'low_price' => 400,
            'high_price'    => 500
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'view'      => 200000,
            'used'      => 45700,
            'allocated' => 45700
        ]);

        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id'   => $room,
            'is_active'     => ViewPromote::PROMOTION_ACTIVE,
            'premium_request_id'    => $premiumRequest,
            'total'     => 200000,
            'history'   => 45700,
            'used'      => 45700
        ]);
        $viewPromote2 = factory(ViewPromote::class)->create([
            'designer_id' => $room2,
            'is_active' => ViewPromote::PROMOTION_ACTIVE,
            'premium_request_id'    => $premiumRequest,
            'total'     => 20000,
            'used'      => 21000,
        ]);

        $result = $viewPromote->increaseView($room,ViewPromote::CLICK);
        $result2 = $viewPromote->increaseView($room, ViewPromote::CLICK);
        
        $this->assertTrue($result);
        $this->assertTrue($result2);
    }

    public function testIncreaseViewShouldStopPremium()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();
        
        app()->device = null;
        app()->user = $user;

        $premiumRequest = factory(PremiumRequest::class)->create([
            'view'      => 20000,
            'used'      => 45700,
            'allocated' => 45700
        ]);

        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id'   => $room,
            'is_active'     => ViewPromote::PROMOTION_ACTIVE,
            'premium_request_id'    => $premiumRequest,
            'total'     => 20000,
            'history'   => 45700,
            'used'      => 45700
        ]);

        $result = $viewPromote->increaseView($room, ViewPromote::CLICK);

        $this->assertFalse($result);
    }

    public function testPremiumStopShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id'   => $room,
            'is_active'     => ViewPromote::PROMOTION_NOT_ACTIVE
        ]);

        $result = $viewPromote->premiumStop($room);

        $this->assertTrue($result);
    }

    public function testStoreShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create();
        $data = [
            'designer_id'   => $room->id,
            'premium_request_id'    => $premiumRequest->id,
            'saldo' => 9000,
            'daily_budget'  => 500
        ];

        $result = ViewPromote::store($data);

        $this->assertInstanceOf('App\Entities\Promoted\ViewPromote', $result);
    }

    public function testGetHistoryPromote()
    {
        $viewPromote = factory(ViewPromote::class)->create();
        $history = factory(HistoryPromote::class)->create([
            'view_promote_id'   => $viewPromote->id
        ]);

        $this->assertEquals($history->id, $viewPromote->history_promote->first()->id);
    }

    public function testUpdateSaldo()
    {
        $viewPromote = factory(ViewPromote::class)->create();
        $data = [
            'total'     => 900,
            'used'      => 80,
            'session_id' => 'session_id',
            'is_active' => ViewPromote::PROMOTION_ACTIVE
        ];

        $result = ViewPromote::updateSaldo($viewPromote, $data);

        $this->assertInstanceOf('App\Entities\Promoted\ViewPromote', $result);
    }

    public function testRoomCanAllocation()
    {
        factory(ClickPricing::class)->create([
            'area_city' => ClickPricing::DEFAULT_AREA_LABEL_CLICK,
            'for'       => ClickPricing::FOR_TYPE_CLICK,
            'low_price' => 500
        ]);

        $result = ViewPromote::roomCanAllocation(5, 5000);

        $this->assertEquals(5, $result['total']);
        $this->assertEquals(1000, $result['saldo']);
    }

    public function testEndAllocatedViews()
    {
       $premiumRequest = factory(PremiumRequest::class)->create([
           'allocated'  => 9000
       ]);

       $result = ViewPromote::endAllocatedViews($premiumRequest, 8000);

       $this->assertTrue($result);
    }

    public function testEndViewPromote()
    {
        $room = factory(Room::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'allocated'  => 9000
        ]);
        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id'   => $room->id,
            'is_active' => ViewPromote::PROMOTION_ACTIVE,
            'premium_request_id' => $premiumRequest->id
        ]);

        $result = ViewPromote::endViewPromote($room->id);

        $this->assertInstanceOf('App\Entities\Promoted\ViewPromote', $result);
    }

}