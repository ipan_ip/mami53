<?php

namespace App\Entities\Promoted;

use App\Test\MamiKosTestCase;
use App\Entities\Promoted\AdsDisplayTrackerTemp;

/**
 * 
 */
class AdsDisplayTrackerTempTest extends MamiKosTestCase 
{
    public function testSetDisplayedLogFromArrayRoomsId()
    {
        $room1 = factory(\App\Entities\Room\Room::class)->create();
        $room2 = factory(\App\Entities\Room\Room::class)->create();
        app()->user = factory(\App\User::class)->create();
        app()->device = null;

        $roomsID = [
            $room1->id,
            $room2->id
        ];

        $response = (new AdsDisplayTrackerTemp)->setDisplayTracker($roomsID);

        $this->assertTrue($response);
    }

    public function testSetDisplayedLogFromUserLogInShouldSuccess()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        app()->user = factory(\App\User::class)->create();
        app()->device = null;
        
        $response = (new AdsDisplayTrackerTemp)->setDisplayTracker($room->id);

        $this->assertTrue($response);
    }

    public function testSetDisplayedLogFromUserDeviceShouldSuccess()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        app()->device = factory(\App\Entities\Device\UserDevice::class)->create();

        $response = (new AdsDisplayTrackerTemp)->setDisplayTracker($room->id);

        $this->assertTrue($response);
    }

    public function testSetDisplayedLogFromCookieShouldSuccess()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        app()->device = null;
        app()->user = null;

        $_COOKIE['adsession'] = 'not null';

        $response = (new AdsDisplayTrackerTemp)->setDisplayTracker($room->id);

        $this->assertTrue($response);
    }

    // temporary comment out
    // public function testSetDisplayedLogFail()
    // {
    //     $room = factory(\App\Entities\Room\Room::class)->create();
    //     app()->device = null;
    //     app()->user = null;
    //     $identifier = [
    //         'identifier' => '',
    //         'identifier_type' => ''
    //     ];

    //     $response = (new AdsDisplayTrackerTemp)->setDisplayTracker($room->id, $identifier);

    //     $this->assertFalse($response);
    // }

}