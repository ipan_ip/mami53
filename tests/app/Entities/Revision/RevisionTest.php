<?php

namespace app\Entities\Revision;

use App\Entities\Revision\Revision;
use App\Test\MamiKosTestCase;
use App\User;

class RevisionTest extends MamiKosTestCase
{
    public function testUserRelationship()
    {
        $user = factory(User::class)->create();
        $revision = factory(Revision::class)->create(['user_id' => $user->id]);
        $this->assertEquals($user->id, $revision->user->id);
    }
}
