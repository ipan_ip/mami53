<?php

namespace App\Entities\Point;

use App\Entities\Level\KostLevel;
use App\Entities\Level\LevelHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class LevelHistoryTest extends MamiKosTestCase
{
    public function testKostLevelRelation()
    {
        $level = factory(KostLevel::class)->create();
        $history = factory(LevelHistory::class)->create([
            'level_id' => $level->id
        ]);
        
        $this->assertEquals($level->id, $history->kost_level->id);
    }

    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $history = factory(LevelHistory::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertEquals($user->id, $history->user->id);
    }

    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $history = factory(LevelHistory::class)->create([
            'kost_id' => $room->song_id
        ]);
        
        $this->assertEquals($room->song_id, $history->room->song_id);
    }

    public function testScopeWhereLevelHistoryBelongsToOwner()
    {
        $level = factory(KostLevel::class)->create();
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = RoomOwner::add([
                'id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => 'Pemilik'
        ]);
        $history = factory(LevelHistory::class)->create([
            'level_id' => $level->id,
            'user_id' => $user->id,
            'kost_id' => $room->song_id,
        ]);
        $levelArray = [
            $level->id,
        ];
        
        $result = LevelHistory::whereLevelHistoryBelongsToOwner($levelArray, $user->id);

        $this->assertEquals($result->count(), 1);
        $this->assertEquals($result->first()->id, $history->id);
    }
}