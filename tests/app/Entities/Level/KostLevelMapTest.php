<?php

namespace App\Entities\Level;

use App\Test\MamiKosTestCase;

class KostLevelMapTest extends MamiKosTestCase
{
    public function testKostLevel(): void
    {
        $level = factory(KostLevel::class)->create();
        $map = factory(KostLevelMap::class)->create(['level_id' => $level->id]);

        $this->assertTrue(
            $map->kost_level->is($level)
        );
    }
}
