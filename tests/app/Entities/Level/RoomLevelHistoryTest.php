<?php

namespace App\Entities\Point;

use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Room;
use App\Entities\Room\Element\RoomUnit;
use App\Test\MamiKosTestCase;

class RoomLevelHistoryTest extends MamiKosTestCase
{
    public function testRoomUnitRelation()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);
        $history = factory(RoomLevelHistory::class)->create([
            'room_id' => $roomUnit->id
        ]);
        
        $this->assertEquals($roomUnit->id, $history->room->id);
    }
}