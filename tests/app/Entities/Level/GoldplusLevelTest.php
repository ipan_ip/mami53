<?php

namespace App\Entities\Level;

use App\Test\MamiKosTestCase;

class GoldplusLevelTest extends MamiKosTestCase
{
    public function testGoldplus1(): void
    {
        $this->assertEquals('Mamikos Goldplus 1', GoldplusLevel::GOLDPLUS_1);
    }

    public function testGoldplus2(): void
    {
        $this->assertEquals('Mamikos Goldplus 2', GoldplusLevel::GOLDPLUS_2);
    }

    public function testGoldplus3(): void
    {
        $this->assertEquals('Mamikos Goldplus 3', GoldplusLevel::GOLDPLUS_3);
    }

    public function testGoldplus1Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 1 PROMO', GoldplusLevel::GOLDPLUS_1_PROMO);
    }

    public function testGoldplus2Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 2 PROMO', GoldplusLevel::GOLDPLUS_2_PROMO);
    }

    public function testGoldplus3Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 3 PROMO', GoldplusLevel::GOLDPLUS_3_PROMO);
    }
}
