<?php

namespace App\Entities\Point;

use App\Entities\Level\KostLevel;
use App\Entities\Level\LevelBenefit;
use App\Test\MamiKosTestCase;

class LevelBenefitTest extends MamiKosTestCase
{
    public function testKostLevelRelation()
    {
        $level = factory(KostLevel::class)->create();
        $benefit = factory(LevelBenefit::class)->create([
            'level_id' => $level->id
        ]);
        
        $this->assertEquals($level->id, $benefit->kost_level->id);
    }

    public function testKostLevelIds()
    {
        $level = factory(KostLevel::class)->create();
        $benefit = factory(LevelBenefit::class)->create([
            'level_id' => $level->id
        ]);
        
        $this->assertEquals(1, count(LevelBenefit::getIdsOfLevel($level)));
    }
}