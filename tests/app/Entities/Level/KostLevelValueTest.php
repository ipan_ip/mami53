<?php

namespace App\Entities\Level;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use DB;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\WithFaker;

class KostLevelValueTest extends MamiKosTestCase
{
    use WithFaker;

    protected $kostLevelValue;

    public function testKostLevelsRelationship()
    {
        $this->setupKostLevelValueData();

        // Creating `KostLevelValue` with pivot
        $valuesCount = $this->faker->numberBetween(2, 5);
        $this->kostLevelValue->kost_levels()->saveMany(
            factory(KostLevel::class, $valuesCount)->create()
        );

        $levels = $this->kostLevelValue->kost_levels();

        $this->assertInstanceOf(BelongsToMany::class, $levels);
        $this->assertEquals($valuesCount, $levels->count());
    }

    public function testRoomsRelationship()
    {
        $this->setupKostLevelValueData();

        // Creating `kost_level_value` with pivot
        $valuesCount = $this->faker->numberBetween(3, 5);
        $this->kostLevelValue->rooms()->saveMany(
            factory(Room::class, $valuesCount)->create()
        );

        $rooms = $this->kostLevelValue->rooms();

        $this->assertInstanceOf(BelongsToMany::class, $rooms);
        $this->assertEquals($valuesCount, $rooms->count());
    }

    /**
     * @group BG-2996
     * @return void
     */
    public function testRelationPhoto(): void
    {
        $media = factory(Media::class)->create();
        $kostValue = factory(KostLevelValue::class)->create([
            'image_id' => $media->id,
        ]);

        $result = $kostValue->photo;

        $this->assertInstanceOf(Media::class, $result);
        $this->assertNotEmpty($result);
    }

    /**
     * @group BG-2996
     * @return void
     */
    public function testRelationPhotoSmall(): void
    {
        $media = factory(Media::class)->create();
        $kostValue = factory(KostLevelValue::class)->create([
            'small_image_id' => $media->id,
        ]);

        $result = $kostValue->photoSmall;

        $this->assertInstanceOf(Media::class, $result);
        $this->assertNotEmpty($result);
    }

    /**
     * @group BG-2996
     * @return void
     */
    public function testRelationCreatedBy()
    {
        $user = factory(\App\User::class)->create();
        $kostValue = factory(KostLevelValue::class)->create([
            'created_by' => $user->id,
        ]);

        $result = $kostValue->createdBy;

        $this->assertInstanceOf(\App\User::class, $result);
        $this->assertNotEmpty($result);
        $this->assertEquals($user->name, $result->name);
    }

    /**
     * @group BG-2996
     * @return void
     */
    public function testGetBenefitWithNoParam(): void
    {
        if (KostLevelValue::query()->count()) {
            DB::table('kost_level_value')->truncate();
        }

        $kostLevel = factory(KostLevelValue::class, 2)->create();
        $result = KostLevelValue::getBenefit(2, []);

        $this->assertEquals(2, $result->count());
    }

    /**
     * @group BG-2996
     * @return void
     */
    public function testGetBenefitWithParam(): void
    {
        if (KostLevelValue::query()->count()) {
            DB::table('kost_level_value')->truncate();
        }

        $kostLevel = factory(KostLevelValue::class)->create([
            'name' => 'mamichecker Benefit'
        ]);
        $result = KostLevelValue::getBenefit(2, [
            'q' => 'mamichecker'
        ]);

        $this->assertEquals(1, $result->count());
        $this->assertEquals($kostLevel->name, $result[0]->name);
    }

    private function setupKostLevelValueData()
    {
        if (KostLevelValue::query()->count()) {
            DB::table('kost_level_value')->truncate();
        }

        $this->kostLevelValue = factory(KostLevelValue::class)->create();
    }
}
