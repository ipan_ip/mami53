<?php

namespace App\Entities\Level;

use App\Test\MamiKosTestCase;

class PropertyLevelTest extends MamiKosTestCase
{
    public function testGoldplus1(): void
    {
        $this->assertEquals('Mamikos Goldplus 1', PropertyLevel::GOLDPLUS_1);
    }

    public function testGoldplus2(): void
    {
        $this->assertEquals('Mamikos Goldplus 2', PropertyLevel::GOLDPLUS_2);
    }

    public function testGoldplus3(): void
    {
        $this->assertEquals('Mamikos Goldplus 3', PropertyLevel::GOLDPLUS_3);
    }

    public function testGoldplus1Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 1 PROMO', PropertyLevel::GOLDPLUS_1_PROMO);
    }

    public function testGoldplus2Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 2 PROMO', PropertyLevel::GOLDPLUS_2_PROMO);
    }

    public function testGoldplus3Promo(): void
    {
        $this->assertEquals('Mamikos Goldplus 3 PROMO', PropertyLevel::GOLDPLUS_3_PROMO);
    }

    public function testDefaultName(): void
    {
        $this->assertEquals('Default Name', PropertyLevel::DEFAULT_NAME);
    }

    public function testKostLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);

        $this->assertTrue($propertyLevel->kost_level->is($kostLevel));
    }

    public function testDeleteShouldRemoveKostLevelRelationToPropertyLevel()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $result = $propertyLevel->delete();

        $this->assertNull($kostLevel->refresh()->property_level_id);
        $this->assertNull($kostLevel->property_level);
        $this->assertTrue($result);
    }
}
