<?php

namespace App\Entities\Level;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\WithFaker;

class KostLevelTest extends MamiKosTestCase
{
    use WithFaker;

    protected $level;

    protected function setUp(): void
    {
        parent::setUp();

        $this->level = factory(KostLevel::class)->create();
    }

    public function testBenefits(): void
    {
        $benefit = factory(LevelBenefit::class)->create(['level_id' => $this->level->id]);

        $this->assertTrue(
            $this->level->benefits->first()->is($benefit)
        );
    }

    public function testCriterias(): void
    {
        $criteria = factory(LevelCriteria::class)->create(['level_id' => $this->level->id]);

        $this->assertTrue(
            $this->level->criterias->first()->is($criteria)
        );
    }

    public function testHistories(): void
    {
        $history = factory(LevelHistory::class)->create(['level_id' => $this->level->id]);

        $this->assertTrue(
            $this->level->histories->first()->is($history)
        );
    }

    public function testRooms(): void
    {
        $kost = factory(Room::class)->create();
        factory(KostLevelMap::class)->create([
            'level_id' => $this->level->id,
            'kost_id' => $kost->song_id
        ]);

        $this->assertTrue(
            $this->level->rooms->first()->is($kost)
        );
    }

    public function testRoomLevel(): void
    {
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $this->level->id]);

        $this->assertTrue($this->level->room_level->is($roomLevel));
    }

    public function testPropertyLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $this->level->property_level_id = $propertyLevel->id;
        $this->level->save();

        $this->assertTrue(
            $this->level->property_level->is($propertyLevel)
        );
    }

    public function testValuesRelationship()
    {
        $level = factory(KostLevel::class)->create();

        // Creating `kost_level_value` with pivot
        $valuesCount = $this->faker->numberBetween(3, 5);
        $level->values()->saveMany(
            factory(KostLevelValue::class, $valuesCount)->create()
        );

        $values = $level->values();

        $this->assertInstanceOf(BelongsToMany::class, $values);
        $this->assertEquals($valuesCount, $values->count());
    }

    public function testRegularLevel(): void
    {
        $this->level->is_regular = true;
        $this->level->save();

        $this->assertTrue(
            KostLevel::regularLevel()->is($this->level)
        );
    }

    public function testCanMakeRegularLevel(): void
    {
        $this->level->is_regular = false;
        $this->level->save();

        $this->assertTrue(
            KostLevel::canMakeRegularLevel()
        );
    }

    public function testPartnerLevel(): void
    {
        $this->level->is_partner = true;
        $this->level->save();

        $this->assertTrue(
            KostLevel::partnerLevel()->is($this->level)
        );
    }

    public function testCanMakePartnerLevel(): void
    {
        $this->level->is_partner = false;
        $this->level->save();

        $this->assertTrue(
            KostLevel::canMakePartnerLevel()
        );
    }

    public function testGetAvailableOrder(): void
    {
        $this->level->order = 100;
        $this->level->save();

        $this->assertEquals(
            range(1, 99),
            KostLevel::getAvailableOrder()
        );
    }

    public function testGoldplusLevelIds(): void
    {
        $this->assertEquals(
            [
                config('kostlevel.id.goldplus1'),
                config('kostlevel.id.goldplus2'),
                config('kostlevel.id.goldplus3'),
                config('kostlevel.id.goldplus4')
            ],
            KostLevel::getGoldplusLevelIds()
        );
    }

    public function testGetGpLevelIds()
    {
        $expected = [
            factory(KostLevel::class)->state('gp_level_1')->create()->id,
            factory(KostLevel::class)->state('gp_level_2')->create()->id,
            factory(KostLevel::class)->state('gp_level_3')->create()->id,
            factory(KostLevel::class)->state('gp_level_4')->create()->id,
        ];

        $gpLevelIds = KostLevel::getGpLevelIds();

        $this->assertEquals($expected, $gpLevelIds);
    }

    public function testGetGpLevelKeyIds()
    {
        $expected = [
            'goldplus1' => factory(KostLevel::class)->state('gp_level_1')->create()->id,
            'goldplus2' => factory(KostLevel::class)->state('gp_level_2')->create()->id,
            'goldplus3' => factory(KostLevel::class)->state('gp_level_3')->create()->id,
            'goldplus4' => factory(KostLevel::class)->state('gp_level_4')->create()->id,
        ];

        $gpLevelIds = KostLevel::getGpLevelKeyIds();

        $this->assertEquals($expected, $gpLevelIds);
    }

    public function testScopeWhereLevelsBelongToOwner(): void
    {
        $kost = factory(Room::class)->create();
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => 5]);
        factory(KostLevelMap::class)->create([
            'level_id' => $this->level->id,
            'kost_id' => $kost->song_id
        ]);

        $actual = KostLevel::whereLevelsBelongToOwner([$this->level->id], 5)->first();

        $this->assertTrue(
            $actual->is($this->level)
        );
    }

    public function testDeleteShouldRemoveRoomLevelRelationToKostLevel(): void
    {
        $kostLevel = factory(KostLevel::class)->create();
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);
        $result = $kostLevel->delete();

        $this->assertNull($roomLevel->refresh()->kost_level_id);
        $this->assertNull($roomLevel->kost_level);
        $this->assertTrue($result);
    }
    
    /**
     * @group UG
     * @group UG-4087
     */
    public function testGetGoldplusLevelIdsByLevel()
    {
        $this->assertEquals([
            9,
            185,
            7,
            183,
            10,
            188,
            101,
            187,
        ], KostLevel::getGoldplusLevelIdsByLevel());
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testGetGoldplusLevelIdsByLevelOne()
    {
        $this->assertEquals([
            9,
            185,
        ], KostLevel::getGoldplusLevelIdsByLevel(1));
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testGetGoldplusLevelIdsByLevelTwo()
    {
        $this->assertEquals([
            7,
            183,
        ], KostLevel::getGoldplusLevelIdsByLevel(2));
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testGetGoldplusLevelIdsByLevelThree()
    {
        $this->assertEquals([
            10,
            188,
        ], KostLevel::getGoldplusLevelIdsByLevel(3));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusLevelIdsByLevelFour()
    {
        $this->assertEquals([
            101,
            187,
        ], KostLevel::getGoldplusLevelIdsByLevel(4));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusLevelIdsByLevelFive()
    {
        $this->assertEquals([], KostLevel::getGoldplusLevelIdsByLevel(5));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusLevelIdsByGroupsEmpty()
    {
        $this->assertEqualsCanonicalizing([
            9,
            185,
            7,
            183,
            10,
            188,
            101,
            187,
        ], KostLevel::getGoldplusLevelIdsByGroups());
    }

    /**
     * @group UG
     */
    public function testGetGoldplusLevelIdsByGroupsOneThree()
    {
        $this->assertEqualsCanonicalizing([
            9,
            185,
            10,
            188,
        ], KostLevel::getGoldplusLevelIdsByGroups(1, 3));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelNonGp()
    {
        $this->assertEquals(0, KostLevel::getGoldplusGroupByLevel(mt_rand(11111, 99999)));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelGpOne()
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups(1) as $gpId) {
            $this->assertEquals(1, KostLevel::getGoldplusGroupByLevel($gpId));
        }
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelGpTwo()
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups(2) as $gpId) {
            $this->assertEquals(2, KostLevel::getGoldplusGroupByLevel($gpId));
        }
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelGpThree()
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups(3) as $gpId) {
            $this->assertEquals(3, KostLevel::getGoldplusGroupByLevel($gpId));
        }
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelGpFour()
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups(4) as $gpId) {
            $this->assertEquals(4, KostLevel::getGoldplusGroupByLevel($gpId));
        }
    }

    /**
     * @group UG
     */
    public function testGetGoldplusNameByLevelIdGpShouldNotNull()
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups() as $gpId) {
            $this->assertNotNull(KostLevel::getGoldplusNameByLevelId($gpId));
        }
    }

    /**
     * @group UG
     */
    public function testGetGoldplusGroupByLevelNull()
    {
        $this->assertEquals(0, KostLevel::getGoldplusGroupByLevel(null));
    }

    /**
     * @group UG
     */
    public function testGetGoldplusNameByLevelIdNonGpShouldNull()
    {
        $this->assertNull(KostLevel::getGoldplusNameByLevelId(mt_rand(111111, 999999)));
    }

    /**
     * @group UG
     */
    public function testExplodeAndCastGoldplusLevelIdsNull()
    {
        $this->assertEquals([], $this->callExplodeAndCastGoldplusLevelIds(null));
    }

    /**
     * @group UG
     */
    public function testExplodeAndCastGoldplusLevelIdsEmptyString()
    {
        $this->assertEquals([], $this->callExplodeAndCastGoldplusLevelIds(''));
    }

    /**
     * @group UG
     */
    public function testExplodeAndCastGoldplusLevelIdsWhiteSpace()
    {
        $this->assertEquals([], $this->callExplodeAndCastGoldplusLevelIds('  ,   '));
    }

    /**
     * @group UG
     */
    public function testExplodeAndCastGoldplusLevelIdsWhiteSpaceAndValidInteger()
    {
        $this->assertEquals([1], $this->callExplodeAndCastGoldplusLevelIds('  ,   1'));
    }

    /**
     * @group BG-3215
     */
    public function testGetGoldPlusGroupByLevelId()
    {
        $this->assertEquals(0, KostLevel::getGoldplusGroupByLevelId(null));
        $this->assertEquals(0, KostLevel::getGoldplusGroupByLevelId(999));

        // goldplus 1
        $this->assertEquals(1, KostLevel::getGoldplusGroupByLevelId(9));
        $this->assertEquals(1, KostLevel::getGoldplusGroupByLevelId(185));

        // goldplus 2
        $this->assertEquals(2, KostLevel::getGoldplusGroupByLevelId(7));
        $this->assertEquals(2, KostLevel::getGoldplusGroupByLevelId(183));

        // goldplus 3
        $this->assertEquals(3, KostLevel::getGoldplusGroupByLevelId(10));
        $this->assertEquals(3, KostLevel::getGoldplusGroupByLevelId(188));

        // goldplus 4
        $this->assertEquals(4, KostLevel::getGoldplusGroupByLevelId(101));
        $this->assertEquals(4, KostLevel::getGoldplusGroupByLevelId(187));
    }

    /**
     * @group BG-3215
     */
    public function testGetLevelIdsByArrayOfGoldPlus()
    {
        $this->assertEquals([], KostLevel::getLevelIdsByArrayOfGoldPlus());
        $this->assertEquals([], KostLevel::getLevelIdsByArrayOfGoldPlus([]));
        
        $this->assertEquals([9,185], KostLevel::getLevelIdsByArrayOfGoldPlus([1]));
        $this->assertEquals([7,183], KostLevel::getLevelIdsByArrayOfGoldPlus([2]));
        $this->assertEquals([10,188], KostLevel::getLevelIdsByArrayOfGoldPlus([3]));
        $this->assertEquals([101,187], KostLevel::getLevelIdsByArrayOfGoldPlus([4]));        

        $this->assertEquals([9,185,7,183], KostLevel::getLevelIdsByArrayOfGoldPlus([1,2]));
        $this->assertEquals([9,185,7,183,10,188], KostLevel::getLevelIdsByArrayOfGoldPlus([1,2,3]));
        $this->assertEquals([9,185,7,183,10,188,101,187], KostLevel::getLevelIdsByArrayOfGoldPlus([1,2,3,4]));
    }

    private function callExplodeAndCastGoldplusLevelIds(?string $string): array
    {
        return $this->callStaticNonPublicMethod(KostLevel::class, 'explodeAndCastGoldplusLevelIds', [$string]);
    }
}
