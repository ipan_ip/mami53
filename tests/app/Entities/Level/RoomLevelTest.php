<?php

namespace App\Entities\Level;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\RoomOwner;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class RoomLevelTest extends MamiKosTestCase
{
    public function testRoomsRelation()
    {
        $roomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);

        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'room_level_id' =>$roomLevel->id
        ]);
        
        $this->assertEquals($roomLevel->rooms()->first()->id, $roomUnit->id);
    }

    public function testHistoriesRelation()
    {
        $roomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);

        $roomLevelHistory = factory(RoomLevelHistory::class)->create([
            'room_level_id' =>$roomLevel->id
        ]);
        
        $this->assertEquals($roomLevel->histories()->first()->id, $roomLevelHistory->id);
    }

    public function testKostLevelRelation()
    {
        $kostLevel = factory(KostLevel::class)->create();
        $roomLevel = factory(RoomLevel::class)->create([
            'kost_level_id' => $kostLevel->id
        ]);
        
        $this->assertEquals($roomLevel->kost_level_id, $kostLevel->id);
    }

    public function testScopeRegularLevel()
    {
        factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);

        factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
        ]);

        factory(RoomLevel::class)->create([
            'name' => 'Standar',
            'is_regular' => true,
        ]);

        $this->assertInstanceOf(Builder::class, RoomLevel::regularLevel());
        $this->assertEquals(2, RoomLevel::regularLevel()->count());
    }

    public function testScopeWhereLevelsBelongToOwner()
    {
        $roomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);

        factory(RoomLevel::class, 3)->create();

        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'room_level_id' =>$roomLevel->id
        ]);
        $user = factory(User::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);

        $levelIds = RoomLevel::all()->pluck('id')->toArray();

        $this->assertInstanceOf(Builder::class, RoomLevel::whereLevelsBelongToOwner($levelIds, $user->id));
        $this->assertEquals(1, RoomLevel::whereLevelsBelongToOwner($levelIds, $user->id)->count());
        $this->assertEquals($roomLevel->id, RoomLevel::whereLevelsBelongToOwner($levelIds, $user->id)->first()->id);
    }

    public function testGetChargingTypeOptions()
    {
        $expected = [
            "percentage" => "Percentage",
            "amount" => "Amount"
        ];

        $actual = RoomLevel::getChargingTypeOptions();

        $this->assertEquals($expected, $actual);
    }

    public function testGetChargeInvoiceTypeOptions()
    {
        $expected = [
            "all" => "All",
            "per_contract" => "Per-Contract",
            "per_tenant" => "Per-Tenant",
        ];

        $actual = RoomLevel::getChargeInvoiceTypeOptions();

        $this->assertEquals($expected, $actual);
    }

    public function testGetGoldplusLevelIds()
    {
        $expected = [
            config('roomlevel.id.goldplus1'),
            config('roomlevel.id.goldplus2'),
            config('roomlevel.id.goldplus3'),
            config('roomlevel.id.goldplus4')
        ];

        $actual = RoomLevel::getGoldplusLevelIds();

        $this->assertEquals($expected, $actual);
    }

    public function testGetGpLevelIds()
    {
        $expected = [
            factory(RoomLevel::class)->state('gp_level_1')->create()->id,
            factory(RoomLevel::class)->state('gp_level_2')->create()->id,
            factory(RoomLevel::class)->state('gp_level_3')->create()->id,
            factory(RoomLevel::class)->state('gp_level_4')->create()->id,
        ];

        $gpLevelIds = RoomLevel::getGpLevelIds();

        $this->assertEquals($expected, $gpLevelIds);
    }

    public function testGetGpLevelKeyIds()
    {
        $expected = [
            'goldplus1' => factory(RoomLevel::class)->state('gp_level_1')->create()->id,
            'goldplus2' => factory(RoomLevel::class)->state('gp_level_2')->create()->id,
            'goldplus3' => factory(RoomLevel::class)->state('gp_level_3')->create()->id,
            'goldplus4' => factory(RoomLevel::class)->state('gp_level_4')->create()->id,
        ];

        $gpLevelIds = RoomLevel::getGpLevelKeyIds();

        $this->assertEquals($expected, $gpLevelIds);
    }
}