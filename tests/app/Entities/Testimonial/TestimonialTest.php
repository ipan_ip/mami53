<?php

namespace App\Entities\Testimonial;

use App\Entities\Testimonial\Testimonial;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestimonialTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testCreatorRelationship()
    {
        $testimonial = factory(Testimonial::class)->create();

        $user = factory(User::class)->create();

        $testimonial->setRelation('creator', $user);

        $this->assertEquals($user->id, $testimonial->creator->id);
    }
}