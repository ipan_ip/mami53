<?php

namespace App\Entities\Singgahsini\Registration;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class SinggahsiniRegistrationTest extends MamiKosTestCase
{
    public function testTagRelation()
    {
        $regis = (new SinggahsiniRegistration)->tags();
        $this->assertInstanceOf(BelongsToMany::class, $regis);
    }

    public function testMediaRelation()
    {
        $regis = (new SinggahsiniRegistration)->media();
        $this->assertInstanceOf(BelongsToMany::class, $regis);
    }
}