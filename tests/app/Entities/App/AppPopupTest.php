<?php

namespace App\Entities\App;

use App\Test\MamiKosTestCase;

class AppPopupTest extends MamiKosTestCase
{
    public function testStore(): void
    {
        $expected = ['method' => 'store'];
        $actual = AppPopup::store();

        $this->assertEquals($expected, $actual);
    }

    public function testShow(): void
    {
        $expected = ['method' => 'show 1'];
        $actual = AppPopup::show(1);

        $this->assertEquals($expected, $actual);
    }

    public function testEdit(): void
    {
        $expected = ['method' => 'update 1'];
        $actual = AppPopup::edit(1);

        $this->assertEquals($expected, $actual);
    }

    public function testDestroy(): void
    {
        $expected = ['method' => 'destroy 1'];
        $actual = AppPopup::destroy(1);

        $this->assertEquals($expected, $actual);
    }
}
