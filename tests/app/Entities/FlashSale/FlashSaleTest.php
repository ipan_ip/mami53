<?php

namespace App\Entities\FlashSale;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Landing\Landing;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Queue;

class FlashSaleTest extends MamiKosTestCase
{
    use WithoutEvents;
    use WithoutMiddleware;

    const ROOM_LATITUDE = -8.014663865095645;
    const ROOM_LONGITUDE = 110.2936449049594;

    const PRICE_DAILY = 100000;
    const PRICE_WEEKLY = 500000;
    const PRICE_MONTHLY = 1500000;
    const PRICE_QUARTERLY = 4500000;
    const PRICE_SEMIANNUALLY = 7000000;
    const PRICE_ANNUALLY = 15000000;

    public function setUp() : void
    {
        parent::setUp();
        // reset memoization
        FlashSale::$LandingsOfCurrentlyRunningData = null;
    }
    
    public function testRelation()
    {
        $banner = factory(Media::class)->create();
        $flashSale = factory(FlashSale::class)->create(['photo_id' => $banner->id]);
        $flashSaleArea = factory(FlashSaleArea::class)->create(['flash_sale_id' => $flashSale->id]);
        $landing = factory(Landing::class)->create();

        $flashSaleAreaLanding = factory(FlashSaleAreaLanding::class)->create(
            [
                'flash_sale_area_id' => $flashSaleArea->id,
                'landing_id' => $landing->id,
            ]
        );

        // assert areas()
        $this->assertInstanceOf(Collection::class, $flashSale->areas);
        $this->assertEquals(1, $flashSale->areas->count());

        // assert banner
        $this->assertInstanceOf(Media::class, $flashSale->banner);

        // landings
        $this->assertTrue($flashSale->landings->contains($flashSaleAreaLanding));
    }

    public function testValidateNameAvailable()
    {
        $this->assertTrue(FlashSale::validateName('flash sale unique name'));
    }

    public function testValidateNameNotAvailable()
    {
        factory(FlashSale::class)->create(['name' => 'flash sale name']);
        $this->assertFalse(FlashSale::validateName('flash sale name'));
    }

    public function testIsCurrentlyRunningShouldReturnTrue()
    {
        $flashSale = factory(FlashSale::class)->create(
            [
                'start_time' => '2018-06-01 19:00:00',
                'end_time' => '2020-06-01 19:00:00',
            ]
        );

        $mockDate = Carbon::create(2019, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $this->assertTrue($flashSale->isCurrentlyRunning());
    }

    public function testIsCurrentlyRunningShouldReturnFalse()
    {
        $flashSale = factory(FlashSale::class)->create(
            [
                'start_time' => '2020-06-01 19:00:00',
                'end_time' => '2020-06-01 19:00:00',
            ]
        );

        $mockDate = Carbon::create(2019, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $this->assertFalse($flashSale->isCurrentlyRunning());
    }

    public function testIsAlreadyFinished()
    {
        $flashSale = factory(FlashSale::class)->make(
            [
                'end_time' => null,
            ]
        );
        $this->assertFalse($flashSale->isAlreadyFinished());

        $flashSale = factory(FlashSale::class)->make(
            [
                'end_time' => '2020-06-01 19:00:00',
            ]
        );
        $this->assertTrue($flashSale->isAlreadyFinished());
    }

    public function testGetRemainingTime()
    {
        $endTime = Carbon::now()->addDays(7);
        $flashSale = factory(FlashSale::class)->make(
            [
                'end_time' => $endTime,
            ]
        );
        $this->assertNotEmpty($flashSale->getRemainingTime());
        $this->assertNotNull($flashSale->getRemainingTime());
    }

    public function testGetUpcomingTime()
    {
        $endTime = Carbon::now()->addDays(7);
        $flashSale = factory(FlashSale::class)->make(
            [
                'start_time' => $endTime,
            ]
        );
        $this->assertNotEmpty($flashSale->getUpcomingTime());
        $this->assertNotNull($flashSale->getUpcomingTime());
    }

    public function testGetAllRoomsUnderRunningData()
    {
        $rows = 10;
        $this->setRoomWithDiscounts($rows);
        $this->setRunningFlashSaleData('2');

        $flashSale = new FlashSale();

        $this->assertEquals($rows, $flashSale->getAllRoomsUnderRunningData()->count());
    }

    public function testGetAllRoomsUnderRunningDataReturnEmpty()
    {
        $rows = mt_rand(5, 10);
        $this->setRoomWithDiscounts($rows);

        $flashSale = new FlashSale();

        $this->assertEquals([], $flashSale->getAllRoomsUnderRunningData());
    }

    /*
     * Private methods
     */

    private function setRoomWithDiscounts($roomCount = 1): void
    {
        factory(Room::class, $roomCount)
            ->create()
            ->each(
                function ($room) {
                    // Geolocation
                    $room->latitude = self::ROOM_LATITUDE;
                    $room->longitude = self::ROOM_LONGITUDE;

                    // Price elements
                    $room->price_daily = self::PRICE_DAILY;
                    $room->price_weekly = self::PRICE_WEEKLY;
                    $room->price_monthly = self::PRICE_MONTHLY;
                    $room->price_yearly = self::PRICE_ANNUALLY;

                    // Room status
                    $room->is_active = 1;
                    $room->is_booking = 1;

                    $room->save();

                    // Monthly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->make(
                            [
                                'price' => 2500000,
                                'markup_type' => 'percentage',
                                'markup_value' => 20,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );
                }
            );
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }
}
