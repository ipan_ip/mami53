<?php

namespace App\Entities\FlashSale;

use App\Test\MamiKosTestCase;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleHistory;
use App\User;
use App\Entities\Media\Media;
use App\Entities\Landing\Landing;

class FlashSaleHistoryTest extends MamiKosTestCase
{
    private $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['role' => 'administrator']);
        $this->actingAs($this->user);
    }

    public function testLogFlashSaleOnCreated()
    {
        $flashSale = factory(FlashSale::class)->create();
        $data      = (object) [
            'id'             => $flashSale->id,
            'original_value' => '{}',
            'modified_value' => '{"start_time":"2020-06-02 14:54:25","end_time":"2020-06-09 14:54:25","is_active":1,"updated_at":"2020-06-01 14:55:19"}',
            'triggered_by'   => 'administrator :: mamiteam', 
        ];

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_CREATED, $data);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
    }

    public function testLogFlashSaleOnDeleted()
    {
        $flashSale = factory(FlashSale::class)->create();
        $data      = (object) [
            'id'             => $flashSale->id,
            'original_value' => '{}',
            'modified_value' => '{"start_time":"2020-06-02 14:54:25","end_time":"2020-06-09 14:54:25","is_active":1,"updated_at":"2020-06-01 14:55:19"}',
            'triggered_by'   => 'administrator :: mamiteam', 
        ];

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_DELETED, $data);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
    }

    public function testLogFlashSaleOnUpdated()
    {
        // TODO: Skip for now
        $this->assertTrue(true);
    }

    public function testLogFlashSaleAreaOnCreated()
    {
        $flashSale = factory(FlashSale::class)->create();
        $data      = (object) [
            'flash_sale_id'  => $flashSale->id,
            'original_value' => '{}',
            'modified_value' => '{"start_time":"2020-06-02 14:54:25","end_time":"2020-06-09 14:54:25","is_active":1,"updated_at":"2020-06-01 14:55:19"}',
            'triggered_by'   => 'administrator :: mamiteam', 
        ];

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_CREATED, $data);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
        $this->assertEquals('{}', $getLatest->original_value);
    }

    public function testLogFlashSaleAreaOnDeleted()
    {
        $flashSale = factory(FlashSale::class)->create();
        $data      = (object) [
            'flash_sale_id'  => $flashSale->id,
            'original_value' => '{}',
            'modified_value' => '{"start_time":"2020-06-02 14:54:25","end_time":"2020-06-09 14:54:25","is_active":1,"updated_at":"2020-06-01 14:55:19"}',
            'triggered_by'   => 'administrator :: mamiteam', 
        ];

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_DELETED, $data);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
        $this->assertEquals('{}', $getLatest->modified_value);
    }

    public function testLogFlashSaleAreaOnUpdated()
    {
        // TODO: Skip for now
        $this->assertTrue(true);
    }

    public function testLogFlashSaleAreaLandingOnCreated()
    {
        $flashSale     = factory(FlashSale::class)->create();
        $flashSaleArea = factory(FlashSaleArea::class)->create(['flash_sale_id' => $flashSale->id]);
        $landing       = factory(Landing::class)->create();

        $flashSaleAreaLanding = factory(FlashSaleAreaLanding::class)->create([
            'flash_sale_area_id' => $flashSaleArea->id,
            'landing_id' => $landing->id,
        ]);

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_LANDING_CREATED, $flashSaleAreaLanding);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSaleAreaLanding->area->flash_sale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
        $this->assertEquals('{}', $getLatest->original_value);
    }

    public function testLogFlashSaleAreaLandingOnDeleted()
    {
        $flashSale     = factory(FlashSale::class)->create();
        $flashSaleArea = factory(FlashSaleArea::class)->create(['flash_sale_id' => $flashSale->id]);
        $landing       = factory(Landing::class)->create();

        $flashSaleAreaLanding = factory(FlashSaleAreaLanding::class)->create([
            'flash_sale_area_id' => $flashSaleArea->id,
            'landing_id' => $landing->id,
        ]);

        $result = FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_LANDING_DELETED, $flashSaleAreaLanding);

        // assert
        $getLatest = FlashSaleHistory::where('flash_sale_id', $flashSaleAreaLanding->area->flash_sale->id)->first();
        $this->assertEquals($this->user->role . ' :: ' . $this->user->name, $getLatest->triggered_by);
        $this->assertEquals('{}', $getLatest->modified_value);
    }

}
