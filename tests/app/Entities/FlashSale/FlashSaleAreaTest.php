<?php

namespace App\Entities\FlashSale;

use App\Test\MamiKosTestCase;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use Illuminate\Database\Eloquent\Collection;

class FlashSaleAreaTest extends MamiKosTestCase
{
    
    public function testRelation()
    {
        $flashSale     = factory(FlashSale::class)->create();
        $flashSaleArea = factory(FlashSaleArea::class)->create([
            'flash_sale_id' => $flashSale->id
        ]);

        $flashSaleAreaLanding = factory(FlashSaleAreaLanding::class, 2)->create([
            'flash_sale_area_id' => $flashSaleArea->id,
            'added_by' => 'administrator :: mamiteam', 
        ]);

        // assert flash_sale()
        $this->assertInstanceOf(FlashSale::class, $flashSaleArea->flash_sale);

        // assert landings()
        $this->assertInstanceOf(Collection::class, $flashSaleArea->landings);
        $this->assertEquals(2, $flashSaleArea->landings->count());
    }

}
