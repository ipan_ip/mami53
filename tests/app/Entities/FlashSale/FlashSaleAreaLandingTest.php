<?php

namespace App\Entities\FlashSale;

use App\Test\MamiKosTestCase;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use Illuminate\Database\Eloquent\Collection;
use App\Entities\Landing\Landing;

class FlashSaleAreaLandingTest extends MamiKosTestCase
{
    
    public function testRelation()
    {
        $flashSaleArea = factory(FlashSaleArea::class)->create();
        $landing       = factory(Landing::class)->create();

        $flashSaleAreaLanding = factory(FlashSaleAreaLanding::class)->create([
            'landing_id'         => $landing->id,
            'flash_sale_area_id' => $flashSaleArea->id,
            'added_by'           => 'administrator :: mamiteam', 
        ]);

        // assert area()
        $this->assertInstanceOf(FlashSaleArea::class, $flashSaleAreaLanding->area);

        // assert landing()
        $this->assertInstanceOf(Landing::class, $flashSaleAreaLanding->landing);
    }
}
