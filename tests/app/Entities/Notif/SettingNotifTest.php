<?php

namespace App\Entities\Notif;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Room\Room;

class SettingNotifTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->settingNotif = new SettingNotif;
    }

    public function testUserRelation()
    {
        $user = factory(User::class)->create([]);
        $settingNotif = factory(SettingNotif::class)->create(['identifier' => $user->id]);
        $result = $settingNotif->user()->first();

        $this->assertEquals($result->id, $user->id);
    }

    public function testRoomRelation()
    {
        $room = factory(Room::class)->create([]);
        $settingNotif = factory(SettingNotif::class)->create(['identifier' => $room->id]);
        $result = $settingNotif->room()->first();

        $this->assertEquals($result->id, $room->id);
    }

    public function testInsertOrUpdateWithEmptyKeyParamShouldReturnFalse()
    {
        $result = $this->settingNotif->insertOrUpdate([]);
        $this->assertFalse($result);
    }

    public function testInsertOrUpdateWithKeyParamNotExistShouldReturnFalse()
    {
        $result = $this->settingNotif->insertOrUpdate(['key' => '']);
        $this->assertFalse($result);
    }

    public function testInsertOrUpdateWithValidKeyParamShouldInsertToDatabase()
    {
        $user = factory(User::class)->create([]);

        $data['key'] = 'app_chat';
        $data['for'] = 'user';
        $data['identifier_id'] = $user->id;
        $data['value'] = 'true';

        $result = $this->settingNotif->insertOrUpdate($data);

        $expectedResult = [
            'key' => $data['key'],
            'for' => $data['for'],
            'identifier' => $data['identifier_id'],
            'value' => $data['value'],
        ];
        $result = array_only($result->toArray(), ['key', 'for', 'identifier', 'value']);
        $this->assertEquals($result, $expectedResult);
        $this->assertDatabaseHas('setting_notification', $expectedResult);
    }

    public function testInsertOrUpdateWithExistingRecordsShouldUpdate()
    {
        $user = factory(User::class)->create([]);
        $settingNotif = factory(SettingNotif::class)->create([
            'key' => 'app_chat',
            'for' => 'user',
            'identifier' => $user->id,
            'value' => 'true',
        ]);

        $data['key'] = $settingNotif['key'];
        $data['for'] = $settingNotif['for'];
        $data['identifier_id'] = $settingNotif['identifier'];
        $data['value'] = 'false';

        $result = $this->settingNotif->insertOrUpdate($data);

        $expectedResult = [
            'key' => $data['key'],
            'for' => $data['for'],
            'identifier' => $data['identifier_id'],
            'value' => $data['value'],
        ];
        $result = array_only($result->toArray(), ['key', 'for', 'identifier', 'value']);
        $this->assertEquals($result, $expectedResult);
        $this->assertDatabaseHas('setting_notification', $expectedResult);
    }

    public function testinsertSettingNotif()
    {
        $user = factory(User::class)->create([]);

        $data['key'] = 'app_chat';
        $data['for'] = 'user';
        $data['identifier'] = $user->id;

        $result = $this->settingNotif->insertSettingNotif($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('setting_notification', $data);
    }

    public function testSettingNotifActionShouldInsertToDatabase()
    {
        $user = factory(User::class)->create([]);

        $data['key'] = 'app_chat';
        $data['for'] = 'user';
        $data['identifier'] = $user->id;

        $result = $this->settingNotif->SettingNotifAction($data);

        $this->assertTrue($result);
        $this->assertDatabaseHas('setting_notification', $data);
    }

    public function testSettingNotifActionShouldUpdateValueToTrue()
    {
        $room = factory(Room::class)->create([]);
        $settingNotif = factory(SettingNotif::class)->create([
            'identifier' => $room->id,
            'key' => 'update_kost',
            'for' => 'designer',
            'value' => 'false'
        ]);

        $data['key'] = $settingNotif['key'];
        $data['for'] = $settingNotif['for'];
        $data['identifier'] = $settingNotif['identifier'];

        $result = $this->settingNotif->SettingNotifAction($data);

        $expectedResult = $data;
        $expectedResult['value'] = 'true';

        $this->assertTrue($result);
        $this->assertDatabaseHas('setting_notification', $expectedResult);
    }

    public function testSettingNotifActionShouldUpdateValueToFalse()
    {
        $room = factory(Room::class)->create([]);
        $settingNotif = factory(SettingNotif::class)->create([
            'identifier' => $room->id,
            'key' => 'update_kost',
            'for' => 'designer',
            'value' => 'true'
        ]);

        $data['key'] = $settingNotif['key'];
        $data['for'] = $settingNotif['for'];
        $data['identifier'] = $settingNotif['identifier'];

        $result = $this->settingNotif->SettingNotifAction($data);

        $expectedResult = $data;
        $expectedResult['value'] = 'false';

        $this->assertTrue($result);
        $this->assertDatabaseHas('setting_notification', $expectedResult);
    }

    public function testgetSingleUserNotificationSettingWithInvalidParamShouldReturnNull()
    {
        $result = $this->settingNotif->getSingleUserNotificationSetting(null, null);
        $this->assertNull($result);
    }

    public function testgetSingleUserNotificationSettingWithKNotifKeyIsUser()
    {
        $user = factory(User::class)->create([]);

        $settingNotif = factory(SettingNotif::class)->create([
            'key' => 'app_chat',
            'for' => 'user',
            'identifier' => $user->id,
            'value' => 'false',
        ]);

        $result = $this->settingNotif->getSingleUserNotificationSetting(
            $user->id,
            $settingNotif['key'],
            false
        );

        $this->assertFalse($result);
    }

    public function testgetSingleUserNotificationSettingWithKNotifKeyIsOwner()
    {
        $user = factory(User::class)->create([]);

        $result = $this->settingNotif->getSingleUserNotificationSetting(
            $user->id,
            'app_chat',
            true
        );

        $this->assertTrue($result);
    }

    public function testgetChangeUserNotificationSettingsShouldInsertToDatabase()
    {
        $user = factory(User::class)->create([]);

        $result = $this->settingNotif->changeUserNotificationSettings(
            [false],
            $user->id,
            true
        );

        $expectedResult=[
            'key' => 'app_chat',
            'value' => true,
            'label' => 'Notifikasi via chat',
        ];

        $this->assertEquals($result[1],$expectedResult);
        // $this->assertTrue($result);
    }

    public function testgetChangeUserNotificationSettingsWithExistingKeyShouldUpdate()
    {
        $user = factory(User::class)->create([]);

        $settingNotif = factory(SettingNotif::class)->create([
            'key' => 'app_chat',
            'for' => 'user',
            'identifier' => $user->id,
            'value' => 'true',
        ]);

        $result = $this->settingNotif->changeUserNotificationSettings(
            [false],
            $user->id,
            false
        );

        $expectedResult=[
            'key' => $settingNotif['key'],
            'value' => false,
            'label' => 'Notifikasi via chat',
        ];

        $this->assertEquals($result[1],$expectedResult);
        // $this->assertTrue($result);
    }

}
