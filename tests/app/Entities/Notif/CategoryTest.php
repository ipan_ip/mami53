<?php

namespace App\Entities\Notif;

use App\Entities\Notif\Category;
use App\Test\MamiKosTestCase;

class CategoryTest extends MamiKosTestCase
{
    
    public function testGetNotificationCategory_GetDesiredCategory()
    {
        $categoryId = 1;
        $categoryName = 'test';
        factory(Category::class)->create(['id' => $categoryId, 'name' => $categoryName]);
        $this->assertEquals($categoryId, Category::getNotificationCategory($categoryName)->id);
    }
}
