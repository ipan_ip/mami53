<?php

namespace App\Entities\Notif;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class PushNotificationTest extends MamiKosTestCase
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(PushNotification::class);
    }

    public function testTransformPayloadAndroidSuccess()
    {
        // prepare data
        $data = [];
        $registrationIds = '';

        // run test
        $result = $this->getPrivateFunction(
            'transformPayloadAndroid',
            [
                $data, $registrationIds
            ]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('to', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('priority', $result);
    }

    public function testTransformPayloadIosSuccess()
    {
        // prepare data
        $data = [];
        $registrationIds = '';

        // run test
        $result = $this->getPrivateFunction(
            'transformPayloadIos',
            [
                $data, $registrationIds
            ]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('to', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('notification', $result);
        $this->assertArrayHasKey('priority', $result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            PushNotification::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}