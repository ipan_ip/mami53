<?php

namespace App\Entities\Notif;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class NotifDataTest extends MamiKosTestCase
{
    use WithFaker;
    protected $notifData;

    protected function setUp(): void
    {
        parent::setUp();
        $this->notifData = new NotifData;
    }

    public function testBuildFromParamWithoutActionSuccess(): void
    {
        // prepare data
        $title      = $this->faker->title;
        $message    = $this->faker->text;
        $scheme     = $this->faker->url;
        $bigFoto    = '';
        $data = NotifData::buildFromParam($title, $message, $scheme, $bigFoto);

        // run test
        $this->assertInstanceOf(NotifData::class, $data);
    }

    public function testBuildFromParamWithActionSuccess(): void
    {
        // prepare data
        $title      = $this->faker->title;
        $message    = $this->faker->text;
        $scheme     = $this->faker->url;
        $bigFoto    = '';
        $actions    = [
            'click_action' => 'listBill',
            'data' => [
                [
                    'id'            => (string) 10,
                    'title'         => 'Lihat Daftar Tagihan',
                    'type'          => PushNotification::TYPE_API_KAY,
                    'url'           => $this->faker->url,
                    'method'        => 'GET',
                    'post_param'    => null,
                    'toast'         => null
                ]
            ]
        ];
        $data = NotifData::buildFromParam($title, $message, $scheme, $bigFoto, $actions);

        // run test
        $this->assertInstanceOf(NotifData::class, $data);
    }

    public function testToFcmFormatIsMamipayFalseSuccess(): void
    {
        // prepare data
        $title      = $this->faker->title;
        $message    = $this->faker->text;
        $scheme     = $this->faker->url;
        $bigFoto    = '';
        $data = NotifData::buildFromParam($title, $message, $scheme, $bigFoto);
        $fcmFormat = $data->toFcmFormat(false);

        // run test
        $this->assertIsArray($fcmFormat);
        $this->assertArrayHasKey('notif_id', $fcmFormat);
        $this->assertArrayHasKey('title', $fcmFormat);
        $this->assertArrayHasKey('body', $fcmFormat);
        $this->assertArrayHasKey('message', $fcmFormat);
        $this->assertArrayHasKey('scheme', $fcmFormat);
        $this->assertArrayHasKey('big_photo', $fcmFormat);
        $this->assertArrayHasKey('sound', $fcmFormat);
        $this->assertArrayHasKey('badge', $fcmFormat);
    }

    public function testToFcmFormatIsMamipayTrueSuccess(): void
    {
        // prepare data
        $title      = $this->faker->title;
        $message    = $this->faker->text;
        $scheme     = $this->faker->url;
        $bigFoto    = '';
        $data = NotifData::buildFromParam($title, $message, $scheme, $bigFoto);
        $fcmFormat = $data->toFcmFormat(true);

        // run test
        $this->assertIsArray($fcmFormat);
        $this->assertArrayHasKey('notif_id', $fcmFormat);
        $this->assertArrayHasKey('title', $fcmFormat);
        $this->assertArrayHasKey('body', $fcmFormat);
        $this->assertArrayHasKey('message', $fcmFormat);
        $this->assertArrayHasKey('scheme', $fcmFormat);
        $this->assertArrayHasKey('big_photo', $fcmFormat);
        $this->assertArrayHasKey('sound', $fcmFormat);
        $this->assertArrayHasKey('badge', $fcmFormat);
    }

    public function testToFcmFormatSuccess(): void
    {
        // prepare data
        $title      = $this->faker->title;
        $message    = $this->faker->text;
        $scheme     = $this->faker->url;
        $bigFoto    = '';
        $actions    = [
            'click_action' => 'listBill',
            'data' => [
                [
                    'id'            => (string) 10,
                    'title'         => 'Lihat Daftar Tagihan',
                    'type'          => PushNotification::TYPE_API_KAY,
                    'url'           => $this->faker->url,
                    'method'        => 'GET',
                    'post_param'    => null,
                    'toast'         => null
                ]
            ]
        ];
        $data = NotifData::buildFromParam($title, $message, $scheme, $bigFoto, $actions);
        $fcmFormat = $data->toFcmFormat(true);

        // run test
        $this->assertIsArray($fcmFormat);
        $this->assertArrayHasKey('click_action', $fcmFormat);
        $this->assertArrayHasKey('actions', $fcmFormat);
    }
}
