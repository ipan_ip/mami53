<?php

namespace App\Entities\Generate;

use App\Test\MamiKosTestCase;

class ListingRejectReasonTest extends MamiKosTestCase
{
    public function testRejectReasonRelation()
    {
        $rejectReason = factory(RejectReason::class)->create();
        $listingRejectReason = factory(ListingRejectReason::class)->create([
            'reject_reason_id' => $rejectReason->id,
        ]);

        $listingRejectReason->load('reject_reason');
        $this->assertSame($listingRejectReason->reject_reason->id, $rejectReason->id);
    }
}
