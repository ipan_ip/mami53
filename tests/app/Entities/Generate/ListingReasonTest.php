<?php

namespace App\Entities\Generate;

use App\Test\MamiKosTestCase;
use App\User;

class ListingReasonTest extends MamiKosTestCase
{
    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $listingReason = factory(ListingReason::class)->create([
            'user_id' => $user->id,
            'from' => 'room',
        ]);

        $listingReason->load('user');
        $this->assertSame($listingReason->user->id, $user->id);
    }

    public function testListingRejectReasonRelation()
    {
        $listingReason = factory(ListingReason::class)->create([
            'from' => 'room',
        ]);
        $listingRejectReason = factory(ListingRejectReason::class)->create([
            'scraping_listing_reason_id' => $listingReason->id,
        ]);

        $listingReason->load('listing_reject_reason');
        $this->assertSame($listingReason->listing_reject_reason->first()->id, $listingRejectReason->id);
    }
}
