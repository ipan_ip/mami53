<?php

namespace App\Entities\Consultant;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class ConsultantRoleTest extends MamiKosTestCase
{
    use WithFaker;

    public function testConsultantRoleAdminShouldHaveCorrectValue(): void
    {
        $this->assertTrue(ConsultantRole::ADMIN === 'admin');
    }

    public function testConsultantRoleDemandShouldHaveCorrectValue(): void
    {
        $this->assertTrue(ConsultantRole::DEMAND === 'demand');
    }

    public function testConsultantRoleSupplyShouldHaveCorrectValue(): void
    {
        $this->assertTrue(ConsultantRole::SUPPLY === 'supply');
    }

    public function testConsultantRelationshipShouldReturnCorrectData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $role = factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id
        ]);
        $isSame = $role->consultant->is($consultant);
        $this->assertTrue($isSame);
    }

    public function testCreateConsultantWithAdminRoleShouldSaveCorrectly(): void
    {
        $consultantId = $this->faker->numberBetween(0, 100000);
        $role = new ConsultantRole();
        $role->consultant_id = $consultantId;
        $role->role = ConsultantRole::ADMIN;
        $role->save();

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultantId,
            'role' => ConsultantRole::ADMIN
        ]);
    }

    public function testCreateConsultantWithDemandRoleShouldSaveCorrectly(): void
    {
        $consultantId = $this->faker->numberBetween(0, 100000);
        $role = new ConsultantRole();
        $role->consultant_id = $consultantId;
        $role->role = ConsultantRole::DEMAND;
        $role->save();

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultantId,
            'role' => ConsultantRole::DEMAND
        ]);
    }

    public function testCreateConsultantWithSupplyRoleShouldSaveCorrectly(): void
    {
        $consultantId = $this->faker->numberBetween(0, 100000);
        $role = new ConsultantRole();
        $role->consultant_id = $consultantId;
        $role->role = ConsultantRole::SUPPLY;
        $role->save();

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultantId,
            'role' => ConsultantRole::SUPPLY
        ]);
    }

    public function testCreateConsultantWithAdminChatRoleShouldSaveCorrectly(): void
    {
        $consultantId = $this->faker->numberBetween(0, 100000);
        $role = new ConsultantRole();
        $role->consultant_id = $consultantId;
        $role->role = ConsultantRole::ADMIN_CHAT;
        $role->save();

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultantId,
            'role' => ConsultantRole::ADMIN_CHAT
        ]);
    }
}
