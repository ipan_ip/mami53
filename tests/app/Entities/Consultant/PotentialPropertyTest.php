<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class PotentialPropertyTest extends MamiKosTestCase
{
    public function testPhotoRelationShouldReturnCorrectData(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create([
            'media_id' => $photo->id
        ]);

        $this->assertTrue($property->photo->is($photo));
    }

    public function testGetPhotoUrlWithoutPhoto(): void
    {
        $kost = factory(PotentialProperty::class)->create(['media_id' => 0]);

        $this->assertEmpty($kost->getPhotoUrl());
    }

    public function testGetKostUrl(): void
    {
        $kost = factory(Room::class)->create();
        $potential = factory(PotentialProperty::class)->create(['designer_id' => $kost->id]);

        $this->assertEquals($kost->getPageUrl(), $potential->getKostUrl());
    }

    public function testConsultantRelationShouldReturnCorrectData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $property = factory(PotentialProperty::class)->create([
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue($property->consultant->is($consultant));
    }

    public function testConsultantNote(): void
    {
        $property = factory(PotentialProperty::class)->create();
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => 'potential_property',
            'noteable_id' => $property->id
        ]);
        $this->assertTrue($property->consultant_note->is($note));
    }

    public function testProgressShouldReturnCorrectData(): void
    {
        $property = factory(PotentialProperty::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => 'potential_property',
            'progressable_id' => $property->id
        ]);

        $this->assertTrue($property->progress()->first()->is($progress));
    }

    public function testGetTaskNameAttribute(): void
    {
        $property = factory(PotentialProperty::class)->create();
        $expected = $property->name;
        $actual = $property->getTaskNameAttribute();

        $this->assertEquals($expected, $actual);
    }

    public function testProductGP(): void
    {
        $this->assertEquals('gp', PotentialProperty::PRODUCT_GP);
    }

    public function testProductBBK(): void
    {
        $this->assertEquals('bbk', PotentialProperty::PRODUCT_BBK);
    }

    public function testPotentialOwner(): void
    {
        $expected = factory(PotentialOwner::class)->create();
        $kost = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => $expected->id]);

        $actual = $kost->potential_owner;

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testFirstCreatedBy(): void
    {
        $expected = factory(User::class)->create();
        $property = factory(PotentialProperty::class)->create(['created_by' => $expected->id]);

        $actual = $property->first_created_by;

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testLastUpdatedBy(): void
    {
        $expected = factory(User::class)->create();
        $property = factory(PotentialProperty::class)->create(['updated_by' => $expected->id]);

        $actual = $property->last_updated_by;

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testRoom(): void
    {
        $relation = factory(PotentialProperty::class)->make(['designer_id' => 1])->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('designer_id', $relation->getForeignKey());
    }

}
