<?php

namespace App\Entities\Consultant;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Test\MamiKosTestCase;

class ConsultantNoteTest extends MamiKosTestCase
{
    
    protected $potentialTenant;
    protected $bookingUser;
    protected $contract;
    protected $invoice;
    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->potentialTenant = factory(PotentialTenant::class)->create();
        $this->bookingUser = factory(BookingUser::class)->create();
        $this->contract = factory(MamipayContract::class)->create();
        $this->invoice = factory(MamipayInvoice::class)->create();
        $this->consultant = factory(Consultant::class)->create();
    }

    public function testNoteBookingUser(): void
    {
        $this->assertEquals('booking_user', ConsultantNote::NOTE_BOOKING_USER);
    }

    public function testNoteCreateContract(): void
    {
        $this->assertEquals('contract_create', ConsultantNote::NOTE_CREATE_CONTRACT);
    }

    public function testNotePotentialProperty(): void
    {
        $this->assertEquals('potential_property', ConsultantNote::NOTE_POTENTIAL_PROPERTY);
    }

    public function testNotePotentialTenant(): void
    {
        $this->assertEquals('potential_tenant', ConsultantNote::NOTE_POTENTIAL_TENANT);
    }

    public function testNoteableMorphToPotentialTenant()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->potentialTenant->id, 'noteable_type' => 'potential_tenant']
        );

        $this->assertInstanceOf(PotentialTenant::class, $note->noteable);
    }

    public function testNoteableMorphToBookingUser()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->bookingUser->id, 'noteable_type' => 'booking_user']
        );

        $this->assertInstanceOf(BookingUser::class, $note->noteable);
    }

    public function testNoteableMorphToContract()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->contract->id, 'noteable_type' => 'mamipay_contract']
        );

        $this->assertInstanceOf(MamipayContract::class, $note->noteable);
    }

    public function testNoteableMorphToInvoice()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->invoice->id, 'noteable_type' => 'mamipay_contract_invoice']
        );

        $this->assertInstanceOf(MamipayInvoice::class, $note->noteable);
    }

    public function testConsultantRelation()
    {
        $note = factory(ConsultantNote::class)->create(['consultant_id' => $this->consultant->id]);

        $this->assertEquals(1, $note->consultant->count());
        $this->assertInstanceOf(Consultant::class, $note->consultant);
    }

    public function testScopeForBookingUser()
    {
        factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->bookingUser->id, 'noteable_type' => 'booking_user']
        );

        $note = ConsultantNote::forBookingUser()->first();

        $this->assertEquals($this->bookingUser->id, $note->noteable_id);
        $this->assertEquals('booking_user', $note->noteable_type);
    }

    public function testIsForBookingUserSuccess()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->bookingUser->id, 'noteable_type' => 'booking_user']
        );

        $this->assertEquals(true, $note->isForBookingUser());
    }

    public function testIsForBookingUserFailed()
    {
        $note = factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->invoice->id, 'noteable_type' => 'mamipay_contract_invoice']
        );

        $this->assertEquals(false, $note->isForBookingUser());
    }

}
