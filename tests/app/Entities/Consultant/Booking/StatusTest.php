<?php

namespace App\Entities\Consultant\Booking;

use App\Test\MamiKosTestCase;

class StatusTest extends MamiKosTestCase
{
    public function testBooked()
    {
        $this->assertEquals(Status::BOOKED, 'Butuh Konfirmasi');
    }

    public function testConfirmed()
    {
        $this->assertEquals(Status::CONFIRMED, 'Tunggu Pembayaran');
    }

    public function testPaid()
    {
        $this->assertEquals(Status::PAID, 'Terbayar');
    }

    public function testRejectedByOwner()
    {
        $this->assertEquals(Status::REJECTED_BY_OWNER, 'Ditolak Pemilik');
    }

    public function testRejectedByAdmin()
    {
        $this->assertEquals(Status::REJECTED_BY_ADMIN, 'Ditolak Admin');
    }

    public function testCancelByAdmin()
    {
        $this->assertEquals(Status::CANCEL_BY_ADMIN, 'Ditolak Admin');
    }

    public function testCancelled()
    {
        $this->assertEquals(Status::CANCELLED, 'Dibatalkan');
    }

    public function testDisbursed()
    {
        $this->assertEquals(Status::DISBURSED, 'Pembayaran Diterima');
    }

    public function testExpiredByOwner()
    {
        $this->assertEquals(Status::EXPIRED_BY_OWNER, 'Kadaluwarsa oleh Pemilik');
    }

    public function testExpired()
    {
        $this->assertEquals(Status::EXPIRED, 'Kadaluwarsa oleh Penyewa');
    }

    public function testFinished()
    {
        $this->assertEquals(Status::FINISHED, 'Sewa Berakhir');
    }
}
