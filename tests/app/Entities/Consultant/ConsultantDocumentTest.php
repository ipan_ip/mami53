<?php

namespace App\Entities\Consultant\Document;

use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Test\MamiKosTestCase;

class ConsultantDocumentTest extends MamiKosTestCase
{
    public function testMorphTypeSalesMotionProgress(): void
    {
        $this->assertEquals(
            'sales_motion_progress',
            ConsultantDocument::MORPH_TYPE_SALES_MOTION_PROGRESS
        );
    }

    public function testDocumentableWithSalesMotionProgress(): void
    {
        $progress = factory(SalesMotionProgress::class)->create();
        $document = factory(ConsultantDocument::class)->create([
            'documentable_type' => ConsultantDocument::MORPH_TYPE_SALES_MOTION_PROGRESS,
            'documentable_id' => $progress->id
        ]);

        $this->assertTrue(
            $document->documentable->is($progress)
        );
    }

    public function testGetFileLocationAttribute(): void
    {
        $progress = factory(SalesMotionProgress::class)->create();
        $document = factory(ConsultantDocument::class)->create([
            'documentable_type' => ConsultantDocument::MORPH_TYPE_SALES_MOTION_PROGRESS,
            'documentable_id' => $progress->id,
            'file_path' => '/var',
            'file_name' => 'mou.pdf'
        ]);

        $this->assertEquals(
            '/var/mou.pdf',
            $document->getFIleLocationAttribute()
        );
    }
}
