<?php

namespace App\Entities\Consultant\Elasticsearch;

use App\Test\MamiKosTestCase;

class RoomIndexQueryTest extends MamiKosTestCase
{
    public function testGetIndexName(): void
    {
        $this->assertEquals(
            'mamisearch-room',
            RoomIndexQuery::getIndexName()
        );
    }
}
