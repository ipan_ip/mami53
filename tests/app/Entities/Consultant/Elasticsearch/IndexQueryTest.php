<?php

namespace App\Entities\Consultant\Elasticsearch;

use App\Test\MamiKosTestCase;
use InvalidArgumentException;
use Mockery;
use stdClass;

class IndexQueryTest extends MamiKosTestCase
{
    protected $query;

    protected function setUp(): void
    {
        $this->query = Mockery::mock(IndexQuery::class)->shouldAllowMockingProtectedMethods()->makePartial();
    }

    public function testGetQueryWithDefaultOffset(): void
    {
        $this->query = $this->query->getQuery();

        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithNullOffset(): void
    {
        $this->query->offset(null);
        $this->query = $this->query->getQuery();

        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithSort(): void
    {
        $this->query->sort();
        $this->query = $this->query->getQuery();

        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithSearch(): void
    {
        $this->query->search([
            ['name' => 'test']
        ]);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['name' => 'test']]
                        ],
                        'minimum_should_match' => 1
                    ]
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithMultipleSearch(): void
    {
        $this->query->search([
            ['name' => 'test'],
            ['phone' => '0812']
        ]);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['name' => 'test']],
                            ['match' => ['phone' => '0812']]
                        ],
                        'minimum_should_match' => 1
                    ]
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithOffset(): void
    {
        $this->query->offset(10);
        $this->query = $this->query->getQuery();
        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 10
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithZeroOffset(): void
    {
        $this->query->offset(0);
        $this->query = $this->query->getQuery();
        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithFilter(): void
    {
        $this->query->filter('name', 'Kost');
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['name' => 'Kost']]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithMultipleFilter(): void
    {
        $this->query->filter('name', 'Kost');
        $this->query->filter('area_city', 'Sleman');
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['name' => 'Kost']],
                                        ['term' => ['area_city' => 'Sleman']]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithNullFilter(): void
    {
        $this->query->filter('name', null);
        $this->query = $this->query->getQuery();
        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithFilters(): void
    {
        $this->query->filters('is_instant_booking', [true, false]);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'bool' => [
                                                'should' => [
                                                    ['term' => ['is_instant_booking' => true]],
                                                    ['term' => ['is_instant_booking' => false]]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithNullFilters(): void
    {
        $this->query->filters('is_instant_booking', null);
        $this->query = $this->query->getQuery();
        $this->assertTrue(isset($this->query['body']['query']['match_all']));

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'query' => [
                    'match_all' => $this->query['body']['query']['match_all']
                ],
                'from' => 0
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithFiltersAndFilter(): void
    {
        $this->query->filter('is_instant_booking', false);
        $this->query->filters('status', ['booked', 'cancelled']);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['is_instant_booking' => false]],
                                        [
                                            'bool' => [
                                                'should' => [
                                                    ['term' => ['status' => 'booked']],
                                                    ['term' => ['status' => 'cancelled']]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithRange(): void
    {
        $this->query->range('updated_at', '2021-12-12 12:12:12', '2022-12-12 12:12:12');
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'range' => [
                                    'updated_at' => [
                                        'gte' => '2021-12-12 12:12:12',
                                        'lte' => '2022-12-12 12:12:12'
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithRangeAndFilters(): void
    {
        $this->query->range('updated_at', '2021-12-12 12:12:12', '2022-12-12 12:12:12');
        $this->query->filters('status', ['booked', 'cancelled']);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'range' => [
                                    'updated_at' => [
                                        'gte' => '2021-12-12 12:12:12',
                                        'lte' => '2022-12-12 12:12:12'
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'bool' => [
                                                'should' => [
                                                    ['term' => ['status' => 'booked']],
                                                    ['term' => ['status' => 'cancelled']]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ];

        $this->assertSame($expected, $this->query);
    }

    public function testGetQueryWithInvalidArgumentForRange(): void
    {
        $this->query->range('updated_at', null, null);
        $this->query = $this->query->getQuery();

        $expected = [
            'body' => [
                'sort' => [
                    [
                        'updated_at' => [
                            'order' => 'desc'
                        ]
                    ]
                ],
                'query' => [
                    'match_all' => new stdClass()
                ],
                'from' => 0
            ]
        ];

        $this->assertEquals($expected, $this->query);
    }

    public function testInitializeUnextendedIndexQuery(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->query = new IndexQuery();
    }
}
