<?php

namespace App\Entities\Consultant\Elasticsearch;

use App\Test\MamiKosTestCase;

class AlwaysUseCustomSortTest extends MamiKosTestCase
{
    public function testGetQuery(): void
    {
        $query = new class extends IndexQuery {
            use AlwaysUseCustomSort;

            protected $index = 'mamisearch-room';
        };

        $query->search([
            ['title' => 'Kost']
        ]);
        $query->filter('area_city', 'Bantul');
        $query->range('updated_at', '2021-12-12 12:12:12', '2022-12-12 12:12:12');

        $expected = [
            'index' => 'mamisearch-room',
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['title' => 'Kost']]
                        ],
                        'minimum_should_match' => 1,
                        'filter' => [
                            [
                                'range' => [
                                    'updated_at' => [
                                        'gte' => '2021-12-12 12:12:12',
                                        'lte' => '2022-12-12 12:12:12'
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['area_city' => 'Bantul']]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'sort' => [
                    ['updated_at' => ['order' => 'desc']]
                ],
                'from' => 0
            ]
        ];

        $this->assertSame($expected, $query->getQuery());
    }
}
