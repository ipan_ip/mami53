<?php

namespace App\Entities\Consultant\Elasticsearch;

use App\Test\MamiKosTestCase;

class BookingUserIndexQueryTest extends MamiKosTestCase
{
    /**
     *  Instance of BookingUserIndexQuery
     *
     *  @var BookingUserIndexQuery
     */
    protected $query;

    protected function setUp(): void
    {
        parent::setUp();
        $this->query = new BookingUserIndexQuery();
    }

    public function testGetIndexName(): void
    {
        $this->assertEquals(
            'mamisearch-booking',
            BookingUserIndexQuery::getIndexName()
        );
    }

    public function testGetQueryWithSearchStillSort(): void
    {
        $this->query->sort('booking_created', 'desc');
        $this->query->search([
            ['name' => 'test']
        ]);
        $this->query = $this->query->getQuery();

        $expected = [
            'index' => BookingUserIndexQuery::getIndexName(),
            'body' => [
                'sort' => [
                    ['booking_created' => ['order' => 'desc']]
                ],
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['name' => 'test']]
                        ],
                        'minimum_should_match' => 1
                    ]
                ],
                'from' => 0
            ],
        ];

        $this->assertEquals($expected, $this->query);
    }

    public function testGetQueryWithMultipleSearch(): void
    {
        $this->query->sort('booking_created', 'desc');
        $this->query->search([
            ['name' => 'test'],
            ['phone' => '0812']
        ]);
        $this->query = $this->query->getQuery();

        $expected = [
            'index' => BookingUserIndexQuery::getIndexName(),
            'body' => [
                'sort' => [
                    ['booking_created' => ['order' => 'desc']]
                ],
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => ['name' => 'test']],
                            ['match' => ['phone' => '0812']]
                        ],
                        'minimum_should_match' => 1
                    ]
                ],
                'from' => 0
            ],
        ];

        $this->assertEquals($expected, $this->query);
    }
}
