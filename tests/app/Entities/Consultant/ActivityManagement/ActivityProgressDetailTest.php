<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;

class ActivityProgressDetailTest extends MamiKosTestCase
{
    
    public function testProgressShouldReturnActivityProgressClass()
    {
        $progress = factory(ActivityProgress::class)->create(['id' => 10]);
        $detail = factory(ActivityProgressDetail::class)->create(['consultant_activity_progress_id' => 10]);

        $this->assertInstanceOf(ActivityProgress::class, $detail->progress()->first());
        $this->assertEquals($progress->id, $detail->progress()->first()->id);
    }

    public function testFormShouldReturnActivityFormClass()
    {
        $form = factory(ActivityForm::class)->create(['id' => 10]);
        $detail = factory(ActivityProgressDetail::class)->create(['consultant_activity_form_id' => 10]);

        $this->assertInstanceOf(ActivityForm::class, $detail->form()->first());
        $this->assertEquals($form->id, $detail->form()->first()->id);
    }
}
