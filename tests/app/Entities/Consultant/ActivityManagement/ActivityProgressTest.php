<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ActivityProgressTest extends MamiKosTestCase
{
    
    public function testMorphTypeDBET(): void
    {
        $this->assertEquals('potential_tenant', ActivityProgress::MORPH_TYPE_DBET);
    }

    public function testMorphTypeContract(): void
    {
        $this->assertEquals('mamipay_contract', ActivityProgress::MORPH_TYPE_CONTRACT);
    }

    public function testMorphTypeProperty(): void
    {
        $this->assertEquals('App\Entities\Room\Room', ActivityProgress::MORPH_TYPE_PROPERTY);
    }

    public function testMorphTypePotentialProperty(): void
    {
        $this->assertEquals('potential_property', ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY);
    }

    public function testMorphTypePotentialOwner(): void
    {
        $this->assertEquals('potential_owner', ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER);
    }

    public function testConsultantShouldRetrieveCorrectRelation(): void
    {
        $consultant = factory(Consultant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_id' => $consultant->id
            ]
        );

        $this->assertTrue($progress->consultant->is($consultant));
    }

    public function testFunnelShouldReturnCorrectRelation(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id
            ]
        );

        $this->assertTrue($progress->funnel->is($funnel));
    }

    public function testDetailsShouldReturnCorrectRelations(): void
    {
        $progress = factory(ActivityProgress::class)->create();
        $detail = factory(ActivityProgressDetail::class, 2)->create(
            [
                'consultant_activity_progress_id' => $progress->id
            ]
        );

        $this->assertTrue($progress->details[0]->is($detail[0]));
        $this->assertTrue($progress->details[1]->is($detail[1]));
    }

    public function testProgressableWithPotentialTenantShouldReturnCorrectRelation(): void
    {
        $potentialTenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $potentialTenant->id
            ]
        );

        $this->assertTrue($progress->progressable->is($potentialTenant));
    }

    public function testProgressableWithMamipayContractShouldReturnCorrectRelation(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id
            ]
        );

        $this->assertTrue($progress->progressable->is($contract));
    }

    public function testProgressableWithRoomShouldReturnCorrectRelation(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id
            ]
        );

        $this->assertTrue($progress->progressable->is($room));
    }

    public function testProgressableWithPotentialPropertyShouldReturnCorrectRelation(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => 'potential_property',
            'progressable_id' => $expected->id
        ]);

        $actual = $task->progressable;

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testProgressableWithPotentialOwnerShouldReturnCorrectRelation(): void
    {
        $expected = factory(PotentialOwner::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => 'potential_owner',
            'progressable_id' => $expected->id
        ]);

        $actual = $task->progressable;
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testCurrentDetailReturnCorrectDetail()
    {
        $progress = factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 2, 'consultant_activity_funnel_id' => 2]
        );
        factory(ActivityForm::class)->create(
            ['id' => 2, 'consultant_activity_funnel_id' => 2, 'stage' => 2, 'form_element' => []]
        );
        $detail = factory(ActivityProgressDetail::class)->create(['consultant_activity_progress_id'=>10,'consultant_activity_form_id'=>2]);
        factory(ActivityProgressDetail::class)->create(['consultant_activity_progress_id'=>10,'consultant_activity_form_id'=>4]);

        $relationDetail = $progress->currentDetail[0];

        $this->assertEquals($detail->id,$relationDetail->id);
        $this->assertEquals($detail->consultant_activity_progress_id,$relationDetail->consultant_activity_progress_id);
        $this->assertEquals($detail->consultant_activity_form_id,$relationDetail->consultant_activity_form_id);
        $this->assertEquals($detail->value,$relationDetail->value);
    }
}
