<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;
use Mockery;

class FormInputTest extends MamiKosTestCase
{
    protected $mock;
    protected $real;

    protected function setUp(): void
    {
        $this->mock = Mockery::mock(FormInput::class);
        $this->real = new FormInput();
    }

    public function testSetTypeWithTime(): void
    {
        $this->mock->allows()
            ->setType(InputType::CLOCK_PICKER())
            ->andSet('type', InputType::CLOCK_PICKER)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithTime(): void
    {
        $this->real->setType(InputType::CLOCK_PICKER());
        $this->assertEquals(InputType::CLOCK_PICKER, $this->real->type);
    }

    public function testSetTypeWithDateTime(): void
    {
        $this->mock->allows()
            ->setType(InputType::DATE_PICKER())
            ->andSet('type', InputType::DATE_PICKER)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithDatePicker(): void
    {
        $this->real->setType(InputType::DATE_PICKER());
        $this->assertEquals(InputType::DATE_PICKER, $this->real->type);
    }

    public function testSetTypeWithText(): void
    {
        $this->mock->allows()
            ->setType(InputType::TEXT())
            ->andSet('type', InputType::TEXT)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithText(): void
    {
        $this->real->setType(InputType::TEXT());
        $this->assertEquals(InputType::TEXT, $this->real->type);
    }

    public function testSetTypeWithNumber(): void
    {
        $this->mock->allows()
            ->setType(InputType::NUMBER())
            ->andSet('type', InputType::NUMBER)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithNumber(): void
    {
        $this->real->setType(InputType::NUMBER());
        $this->assertEquals(InputType::NUMBER, $this->real->type);
    }

    public function testSetTypeWithRadio(): void
    {
        $this->mock->allows()
            ->setType(InputType::RADIO())
            ->andSet('type', InputType::RADIO)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithRadio(): void
    {
        $this->real->setType(InputType::RADIO());
        $this->assertEquals(InputType::RADIO, $this->real->type);
    }

    public function testSetTypeWithSelect(): void
    {
        $this->mock->allows()
            ->setType(InputType::SELECT())
            ->andSet('type', InputType::SELECT)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithSelect(): void
    {
        $this->real->setType(InputType::SELECT());
        $this->assertEquals(InputType::SELECT, $this->real->type);
    }

    public function testSetTypeWithFile(): void
    {
        $this->mock->allows()
            ->setType(InputType::UPLOAD_BUTTON())
            ->andSet('type', InputType::UPLOAD_BUTTON)
            ->andReturn($this->mock);
    }

    public function test__getWithTypeWithFile(): void
    {
        $this->real->setType(InputType::UPLOAD_BUTTON());
        $this->assertEquals(InputType::UPLOAD_BUTTON, $this->real->type);
    }

    public function testSetLabel(): void
    {
        $this->mock->allows()
            ->setLabel('test')
            ->andSet('label', 'test')
            ->andReturn($this->mock);
    }

    public function test__getWithLabel(): void
    {
        $this->real->setLabel('test');
        $this->assertEquals('test', $this->real->label);
    }

    public function testSetPlaceholder(): void
    {
        $this->mock->allows()
            ->setPlaceholder('test')
            ->andSet('placeholder', 'test')
            ->andReturn($this->mock);
    }

    public function test__getWithPlaceholder(): void
    {
        $this->real->setPlaceholder('test');
        $this->assertEquals('test', $this->real->placeholder);
    }

    public function testSetValues(): void
    {
        $values = [
            'test1' => 0,
            'test2' => 1
        ];
        $this->mock->allows()
            ->setValues($values)
            ->andSet('values', $values)
            ->andReturn($this->mock);
    }

    public function test__getWithValues(): void
    {
        $values = [
            'test1' => 0,
            'test2' => 1
        ];
        $this->real->setValues($values);
        $this->assertEquals($values, $this->real->values);
    }

    public function testSetOrder(): void
    {
        $this->mock->allows()
            ->setOrder(1)
            ->andSet('order', 0)
            ->andReturn($this->mock);
    }

    public function test__getWithOrder(): void
    {
        $this->real->setOrder(1);
        $this->assertEquals(1, $this->real->order);
    }

    public function testSetId(): void
    {
        $this->mock->allows()
            ->setId(1)
            ->andSet('id', 1)
            ->andReturn($this->mock);
    }

    public function test__getWithId(): void
    {
        $this->real->setId(1);
        $this->assertEquals(1, $this->real->id);
    }

    public function test__getWithUndefinedProperty(): void
    {
        $this->assertNull($this->real->test);
    }
}
