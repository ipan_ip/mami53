<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;

class BacklogStageTest extends MamiKosTestCase
{
    protected $stage;

    protected function setUp(): void
    {
        $this->stage = new BacklogStage();
    }

    public function testGetName(): void
    {
        $expected = 'Daftar Tugas';
        $actual = $this->stage->getName();
        $this->assertEquals($expected, $actual);
    }

    public function testGetDetail(): void
    {
        $expected = '';
        $actual = $this->stage->getDetail();
        $this->assertEquals($expected, $actual);
    }

    public function testGetFormattedStage(): void
    {
        $expected = 1;
        $actual = $this->stage->getFormattedStage();
        $this->assertEquals($expected, $actual);
    }

    public function testGetStage(): void
    {
        $expected = -1;
        $actual = $this->stage->getStage();
        $this->assertEquals($expected, $actual);
    }

    public function testGetIsCompletedWithCompletedStage(): void
    {
        $actual = $this->stage->getIsCompleted(0);
        $this->assertTrue($actual);
    }

    public function testGetIsCompletedWithIncompleteStage(): void
    {
        $actual = $this->stage->getIsCompleted(-1);
        $this->assertFalse($actual);
    }

    public function testGetId(): void
    {
        $this->assertNull($this->stage->getId());
    }
}
