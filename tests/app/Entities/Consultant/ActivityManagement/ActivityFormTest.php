<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;

class ActivityFormTest extends MamiKosTestCase
{
    
    public function testFunnelShouldReturnCorrectRelation(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);

        $this->assertTrue($form->funnel->is($funnel));
    }

    public function testProgressDetailShouldReturnCorrectRelation(): void
    {
        $form = factory(ActivityForm::class)->create();
        $detail = factory(ActivityProgressDetail::class)->create([
            'consultant_activity_form_id' => $form->id
        ]);

        $this->assertTrue($form->progressDetail[0]->is($detail));
    }

    public function testProgressShouldReturnCorrectRelation(): void
    {
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => 1,
            'stage' => 0,
        ]);
        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'current_stage' => 0,
        ]);

        $this->assertCount(1, $form->progress);
        $this->assertTrue($form->progress[0]->is($progress));
    }

    public function testGetName(): void
    {
        $stage = factory(ActivityForm::class)->create();
        $this->assertEquals(
            $stage->name,
            $stage->getName()
        );
    }

    public function testGetDetail(): void
    {
        $stage = factory(ActivityForm::class)->create();
        $this->assertEquals(
            $stage->detail,
            $stage->getDetail()
        );
    }

    public function testGetFormattedStage(): void
    {
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);
        $this->assertEquals(
            2,
            $stage->getFormattedStage()
        );
    }

    public function testGetStage(): void
    {
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);
        $this->assertEquals(
            1,
            $stage->getStage()
        );
    }

    public function testGetIsCompletedWithEquals(): void
    {
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);
        $this->assertTrue(
            $stage->getIsCompleted(1)
        );
    }

    public function testGetIsCompletedWithBiggerStageNumber(): void
    {
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);
        $this->assertTrue(
            $stage->getIsCompleted(2)
        );
    }

    public function testGetIsCompletedShouldReturnFalse(): void
    {
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);
        $this->assertFalse(
            $stage->getIsCompleted(0)
        );
    }

    public function testGetId(): void
    {
        $stage = factory(ActivityForm::class)->create();
        $this->assertEquals(
            $stage->id,
            $stage->getid()
        );
    }
}
