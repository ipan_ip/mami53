<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;

class ActivityFunnelTest extends MamiKosTestCase
{
    
    public function testTableDBET(): void
    {
        $this->assertEquals('consultant_tools_potential_tenant', ActivityFunnel::TABLE_DBET);
    }

    public function testTableProperty(): void
    {
        $this->assertEquals('designer', ActivityFunnel::TABLE_PROPERTY);
    }

    public function testTableContract(): void
    {
        $this->assertEquals('mamipay_contract', ActivityFunnel::TABLE_CONTRACT);
    }

    public function testTablePotentialProperty(): void
    {
        $this->assertEquals('consultant_potential_property', ActivityFunnel::TABLE_POTENTIAL_PROPERTY);
    }

    public function testTablePotentialOwner(): void
    {
        $this->assertEquals('consultant_potential_owner', ActivityFunnel::TABLE_POTENTIAL_OWNER);
    }

    public function testFormsShouldReturnActivityFormClass()
    {
        $funnel = factory(ActivityFunnel::class)->create(['id' => 10]);
        $form = factory(ActivityForm::class)->create(['consultant_activity_funnel_id' => 10]);

        $this->assertEquals(1, count($funnel->forms()->get()));
        $this->assertInstanceOf(ActivityForm::class, $funnel->forms[0]);
        $this->assertEquals($form->id, $funnel->forms[0]->id);
    }

    public function testProgressShouldReturnActivityProgressClass()
    {
        $funnel = factory(ActivityFunnel::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(['consultant_activity_funnel_id' => 10]);

        $this->assertEquals(1, count($funnel->progress()->get()));
        $this->assertInstanceOf(ActivityProgress::class, $funnel->progress[0]);
        $this->assertEquals($progress->id, $funnel->progress[0]->id);
    }

    public function testGetFunnelableTypeAttributeShouldReturnCorrectValue()
    {
        //DBET
        $dbet = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $this->assertEquals(ActivityFunnel::MAPPING[ActivityFunnel::TABLE_DBET], $dbet->getFunnelableTypeAttribute());

        //Property
        $property = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $this->assertEquals(
            ActivityFunnel::MAPPING[ActivityFunnel::TABLE_PROPERTY],
            $property->getFunnelableTypeAttribute()
        );

        //Mamipay Contract
        $contract = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $this->assertEquals(
            ActivityFunnel::MAPPING[ActivityFunnel::TABLE_CONTRACT],
            $contract->getFunnelableTypeAttribute()
        );

        //Potential Property
        $potentialProperty = factory(ActivityFunnel::class)->create(
            ['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]
        );
        $this->assertEquals(
            ActivityFunnel::MAPPING[ActivityFunnel::TABLE_POTENTIAL_PROPERTY],
            $potentialProperty->getFunnelableTypeAttribute()
        );

        //Potential Owner
        $potentialProperty = factory(ActivityFunnel::class)->create(
            ['related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER]
        );
        $this->assertEquals(
            ActivityFunnel::MAPPING[ActivityFunnel::TABLE_POTENTIAL_OWNER],
            $potentialProperty->getFunnelableTypeAttribute()
        );
    }
}
