<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use CLosure;

class ConsultantTest extends MamiKosTestCase
{
    public function testUserRelationFound()
    {
        $user = factory(User::class)->create();
        $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $consultant->user);
    }

    public function testUserRelationNotFound()
    {
        $consultant = factory(Consultant::class)->create(['user_id'=>0]);
        $this->assertNull($consultant->user);
    }

    public function testMappingRelationFound()
    {
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantMapping::class)->create(['consultant_id' => $consultant->id]);

        $this->assertTrue($consultant->mapping->contains($mapping));
        $this->assertEquals(1, $consultant->mapping->count());
        $this->assertInstanceOf(Collection::class, $consultant->potentialTenant);
    }

    public function testMappingRelationNotFound()
    {
        $consultant = factory(Consultant::class)->create();

        $this->assertEquals(0, $consultant->mapping->count());
        $this->assertInstanceOf(Collection::class, $consultant->mapping);
    }

    public function testPotentialTenantRelationFound()
    {
        $consultant = factory(Consultant::class)->create();
        $potentialTenant = factory(PotentialTenant::class)->create(['consultant_id' => $consultant->id]);

        $this->assertTrue($consultant->potentialTenant->contains($potentialTenant));
        $this->assertEquals(1, $consultant->potentialTenant->count());
        $this->assertInstanceOf(Collection::class, $consultant->potentialTenant);
    }

    public function testPotentialTenantRelationNotFound()
    {
        $consultant = factory(Consultant::class)->create();

        $this->assertEquals(0, $consultant->potentialTenant->count());
        $this->assertInstanceOf(Collection::class, $consultant->potentialTenant);
    }

    public function testConsultantRoomsRelationFound()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(ConsultantRoom::class)->create(['consultant_id' => $consultant->id]);

        $this->assertEquals(1, $consultant->consultant_rooms->count());
        $this->assertInstanceOf(Collection::class, $consultant->consultant_rooms);
    }

    public function testConsultantRoomsRelationNotFound()
    {
        $consultant = factory(Consultant::class)->create();

        $this->assertEquals(0, $consultant->consultant_rooms->count());
        $this->assertInstanceOf(Collection::class, $consultant->consultant_rooms);
    }

    public function testRegister()
    {
        $user = factory(User::class)->create();
        $params = [
            'name' => 'test',
            'email' => 'email@test.com',
            'chat_id' => '20'
        ];
        $consultant = Consultant::register($user, $params);
        $this->assertEquals($consultant->name, $params['name']);
        $this->assertEquals($consultant->email, $params['email']);
        $this->assertEquals($consultant->chat_id, $params['chat_id']);
        $this->assertEquals($consultant->user_id, $user->id);
    }

    public function testGetDeviceKeyFound()
    {
        $consultant = factory(Consultant::class)->create(['user_id' => 10, 'device_key' => 'key']);
        $this->assertNotNull(Consultant::getDeviceKey(10));
        $this->assertEquals(Consultant::getDeviceKey(10), $consultant->device_key);
    }

    public function testGetDeviceKeyNotFound()
    {
        factory(Consultant::class)->make(['user_id' => 10]);

        $this->assertNull(Consultant::getDeviceKey(11));
    }

    public function testGetNameByUserIdFound()
    {
        $user = factory(User::class)->create();
        factory(Consultant::class)->create(['user_id' => $user->id]);
        $this->assertNotNull(Consultant::getNameByUserId($user->id));
        $this->assertEquals(Consultant::getNameByUserId($user->id), $user->name);
    }

    public function testGetNameByUserIdNotFound()
    {
        factory(Consultant::class)->create(['user_id' => 0]);
        $this->assertNull(Consultant::getNameByUserId(0));
    }

    public function testGetRoomChatUrlSuccess()
    {
        $user = factory(User::class)->create();
        $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);

        $this->assertNotNull($consultant->getRoomChatUrl());
    }

    public function testGetRoomChatUrlFailedNullUser()
    {
        $consultant = factory(Consultant::class)->make();

        $this->assertNull($consultant->getRoomChatUrl());
    }

    public function testGetRoomChatUrlFailedEmptyChatAccessToken()
    {
        $user = factory(User::class)->make(['chat_access_token' => '']);
        $consultant = factory(Consultant::class)->make(['user_id' => $user->id]);

        $this->assertNull($consultant->getRoomChatUrl());
    }

    public function testGetConsultantByAreaCityEmptyAreaCity(): void
    {
        $this->assertNull(Consultant::getConsultantByAreaCity('', 1));
    }

    public function testGetConsultantByAreaCityEmptyMapping(): void
    {
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn([]);

        $this->assertNull(Consultant::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetConsultantByAreaCityConsultantFound(): void
    {
        $consultant = factory(Consultant::class)->create();
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Surabaya' => ['mamirooms' => [$consultant->id]]]);

        $this->assertNotNull(Consultant::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetConsultantByAreaCityWithNoMappedConsultant(): void
    {
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Surabaya' => ['mamirooms' => []]]);

        $this->assertNull(Consultant::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetConsultantByAreaCityWithTwoConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Surabaya' => ['mamirooms' => [$consultant->id, $consultant->id]]]);

        $this->assertTrue(Consultant::getConsultantByAreaCity('Surabaya', 1)->is($consultant));
    }

    public function testGetConsultantByAreaCityWithInvalidId(): void
    {
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Surabaya' => ['mamirooms' => [5]]]);

        $this->assertNull(Consultant::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetMapDataWithNoMapping()
    {
        factory(ConsultantMapping::class)->create();

        $this->assertNotNull(Consultant::getMapData());
    }

    public function testRefreshCache()
    {
        Cache::shouldReceive('forget')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(null);

        Cache::shouldReceive('rememberForever')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING, Closure::class)
            ->andReturn('test');

        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn('test');

        $cacheMap = Consultant::refreshCache();

        $this->assertNotNull($cacheMap);
        $this->assertEquals($cacheMap, 'test');
    }

    public function testRoomRelationFound()
    {
        $room = factory(Room::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRoom::class)->create([
            'consultant_id' => $consultant->id,
            'designer_id' => $room->id
        ]);

        $retrievedRoom = $consultant->rooms()->first();
        $this->assertTrue($room->is($retrievedRoom));
    }

    public function testRoleRelationShouldReturnCorrectData()
    {
        $consultant = factory(Consultant::class)->create();
        $role = factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue($consultant->roles()->first()->is($role));
    }

    public function testGetAssignedTo(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN_CHAT
        ]);
        $mapping = factory(ConsultantRoom::class)->create([
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue(Consultant::getAssignedTo($mapping->designer_id)->is($consultant));
    }

    public function testGetAssignedToWithNonAdminChatShouldFallback(): void
    {
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'consultant_id' => $consultant->id
        ]);

        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn([]);

        $this->assertNull(Consultant::getAssignedTo($mapping->designer_id, 'Surabaya', 1));
    }

    public function testGetAssignedToShouldFallback(): void
    {
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'consultant_id' => $consultant->id + 1
        ]);

        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn([]);

        $this->assertNull(Consultant::getAssignedTo($mapping->designer_id, 'Surabaya', 1));
    }

    public function testRegisterShouldSave(): void
    {
        $user = factory(User::class)->create();
        Consultant::register(
            $user,
            [
                'name' => 'John Doe',
                'email' => 'john.doe@example.com',
                'chat_id' => 1,
            ]
        );

        $this->assertDatabaseHas('consultant', [
            'name' => 'John Doe',
            'email' => 'john.doe@example.com',
            'chat_id' => 1,
            'user_id' => $user->id
        ]);
    }

    public function testRegisterWithInvalidParamShouldReturnNull(): void
    {
        $actual = Consultant::register(factory(User::class)->make(), []);
        $this->assertNull($actual);
    }

    public function testRegisterWithEmptyParamsShouldReturnNull(): void
    {
        $user = factory(User::class)->create();
        $response = Consultant::register($user, []);

        $this->assertNull($response);
    }

    public function testGetRoomChatUrl(): void
    {
        $user = factory(User::class)->create([
            'chat_access_token' => 'test'
        ]);
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id
        ]);

        $expected = config('sendbird.chat_room_url') . '?userid=' . $user->id . '&access_token=' . 'test';
        $this->assertEquals($expected, $consultant->getRoomChatUrl());
    }

    public function testGetRoomChatUrlWithoutUser(): void
    {
        $consultant = factory(Consultant::class)->create();

        $this->assertNull($consultant->getRoomChatUrl());
    }

    public function testGetRoomChatUrlWithMissingChatAccessToken(): void
    {
        $user = factory(User::class)->create([
            'chat_access_token' => ''
        ]);
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertNull($consultant->getRoomChatUrl());
    }

    public function testSalesMotionsShouldRetreieveAssignedSalesMotion(): void
    {
        $expected = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $expected->id,
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue(
            $consultant->sales_motions()->first()->is($expected)
        );
    }

    public function testSalesMotionShouldNotRetrieveNonAssignedSalesMotion(): void
    {
        factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();

        $this->assertEmpty(
            $consultant->sales_motions()->get()
        );
    }

    private function createMapData(): array
    {
        $consultant = factory(Consultant::class)->create();

        factory(ConsultantMapping::class)->create([
            'area_city' => 'Sleman',
            'is_mamirooms' => true,
            'consultant_id' => $consultant->id
        ]);

        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN_CHAT
        ]);

        return [
            'Sleman' => [
                Consultant::SUBKEY_MAMIROOMS => [$consultant->id],
                Consultant::SUBKEY_NON_MAMIROOMS => []
            ]
        ];
    }

    public function testGetMapData(): void
    {
        $expected = $this->createMapData();
        $this->assertEquals($expected, Consultant::getMapData());
    }
}
