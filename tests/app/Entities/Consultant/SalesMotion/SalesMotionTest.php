<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;

class SalesMotionTest extends MamiKosTestCase
{
    public function testTypePromotion(): void
    {
        $this->assertEquals('promotion', SalesMotion::TYPE_PROMOTION);
    }

    public function testTypeCampaign(): void
    {
        $this->assertEquals('campaign', SalesMotion::TYPE_CAMPAIGN);
    }

    public function testTypeProduct(): void
    {
        $this->assertEquals('product', SalesMotion::TYPE_PRODUCT);
    }

    public function testTypeFeature(): void
    {
        $this->assertEquals('feature', SalesMotion::TYPE_FEATURE);
    }

    public function testDivisionMarketing(): void
    {
        $this->assertEquals('marketing', SalesMotion::DIVISION_MARKETING);
    }

    public function testDivisionCommercial(): void
    {
        $this->assertEquals('commercial', SalesMotion::DIVISION_COMMERCIAL);
    }

    public function testPhotoRelation(): void
    {
        $photo = factory(Media::class)->create();
        $salesMotion = factory(SalesMotion::class)->create(
            [
                'media_id' => $photo->id,
            ]
        );

        $this->assertTrue($salesMotion->photo->is($photo));
    }

    public function testConsultantsShouldReturnAssignedConsultant(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $expected = factory(Consultant::class)->create();
        factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => $expected->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $this->assertTrue(
            $salesMotion->consultants()->first()->is($expected)
        );
    }

    public function testConsultantsShouldNotReturnNonAssignedConsultant(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        factory(Consultant::class)->create();

        $this->assertEmpty(
            $salesMotion->consultants()->get()
        );
    }

    public function testProgressStatusDealRelationShouldReturnCorrectRelation()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $deal = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_DEAL]
        );
        $interest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_INTERESTED]
        );
        $notInterest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_NOT_INTERESTED]
        );

        $salesMotion = SalesMotion::find($salesMotion->id);

        $this->assertEquals(1, count($salesMotion->progress_status_deal));
        $this->assertEquals($deal->id, $salesMotion->progress_status_deal[0]->id);
    }

    public function testProgressStatusInterestedRelationShouldReturnCorrectRelation()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $deal = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_DEAL]
        );
        $interest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_INTERESTED]
        );
        $notInterest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_NOT_INTERESTED]
        );

        $salesMotion = SalesMotion::find($salesMotion->id);

        $this->assertEquals(1, count($salesMotion->progress_status_interested));
        $this->assertEquals($interest->id, $salesMotion->progress_status_interested[0]->id);
    }

    public function testProgressStatusNotInterestedRelationShouldReturnCorrectRelation()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $deal = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_DEAL]
        );
        $interest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_INTERESTED]
        );
        $notInterest = factory(SalesMotionProgress::class)->create(
            ['consultant_sales_motion_id' => $salesMotion->id, 'status' => SalesMotionProgress::STATUS_NOT_INTERESTED]
        );

        $salesMotion = SalesMotion::find($salesMotion->id);

        $this->assertEquals(1, count($salesMotion->progress_status_not_interested));
        $this->assertEquals($notInterest->id, $salesMotion->progress_status_not_interested[0]->id);
    }

    public function testProgress(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(['consultant_sales_motion_id' => $salesMotion->id]);

        $this->assertTrue(
            $salesMotion->progress->first()->is($progress)
        );
    }
}
