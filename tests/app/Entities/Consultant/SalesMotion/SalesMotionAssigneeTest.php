<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SalesMotionAssigneeTest extends MamiKosTestCase
{
    public function testSalesMotionRelation(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $assignee = factory(SalesMotionAssignee::class)->create(
            [
                'consultant_sales_motion_id' => $salesMotion->id,
            ]
        );
        $this->assertTrue($assignee->sales_motion->is($salesMotion));
    }

    public function testConsultantRelation(): void
    {
        $consultant = factory(Consultant::class)->create();
        $assignee = factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => $consultant->id
            ]
        );

        $this->assertTrue($assignee->consultant->is($consultant));
    }

    public function testRoomRelation()
    {
        $this->assertInstanceOf(BelongsTo::class, (new SalesMotionAssignee)->room());
    }
}
