<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class SalesMotionProgressTest extends MamiKosTestCase
{
    public function testStatusDeal(): void
    {
        $this->assertEquals(SalesMotionProgress::STATUS_DEAL, 'deal');
    }

    public function testStatusInterested(): void
    {
        $this->assertEquals(SalesMotionProgress::STATUS_INTERESTED, 'interested');
    }

    public function testStatusNotInterested(): void
    {
        $this->assertEquals(SalesMotionProgress::STATUS_NOT_INTERESTED, 'not_interested');
    }

    public function testConsultantRelation(): void
    {
        $consultant = factory(Consultant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'consultant_id' => $consultant->id
            ]
        );
        $this->assertTrue($progress->consultant->is($consultant));
    }

    public function testSalesMotionRelation(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        $this->assertTrue($progress->sales_motion->is($salesMotion));
    }

    public function testPhotoRelation(): void
    {
        $photo = factory(Media::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => $photo->id
            ]
        );
        $this->assertTrue($progress->photo->is($photo));
    }

    public function testSoftDeletion(): void
    {
        $progress = factory(SalesMotionProgress::class)->create();
        $progress->delete();

        $this->assertSoftDeleted('consultant_sales_motion_progress', [
            'id' => $progress->id,
        ]);
    }

    public function testMorphTypeProperty(): void
    {
        $this->assertEquals(Room::class, SalesMotionProgress::MORPH_TYPE_PROPERTY);
    }

    public function testMorphTypeContract(): void
    {
        $this->assertEquals('mamipay_contract', SalesMotionProgress::MORPH_TYPE_CONTRACT);
    }

    public function testMorphTypeDBET(): void
    {
        $this->assertEquals('potential_tenant', SalesMotionProgress::MORPH_TYPE_DBET);
    }

    public function testProgressableWithProperty(): void
    {
        $expected = factory(Room::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $expected->id
        ]);

        $actual = $progress->progressable;
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testProgressableWithContract(): void
    {
        $expected = factory(MamipayContract::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $expected->id
        ]);

        $actual = $progress->progressable;
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testProgressableWithDBET(): void
    {
        $expected = factory(PotentialTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
            'progressable_id' => $expected->id
        ]);

        $actual = $progress->progressable;
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testDocumentableRelation()
    {
        $progress = factory(SalesMotionProgress::class)->create();

        $expected = factory(ConsultantDocument::class)->create([
            'documentable_id' => $progress->id
        ]);
        $actual = $progress->consultant_documents;
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $actual);
        $this->assertTrue($actual[0]->is($expected));
    }

    public function testConsultantDocumentsWithMultiDocument()
    {
        $progress = factory(SalesMotionProgress::class)->create();

        $expected = factory(ConsultantDocument::class, 2)->create([
            'documentable_id' => $progress->id
        ]);
        $actual = $progress->consultant_documents;

        $this->assertTrue($actual[0]->is($expected[0]));
        $this->assertTrue($actual[1]->is($expected[1]));
    }

    public function testConsultantDocumentsIsEmpty()
    {
        $progress = factory(SalesMotionProgress::class)->create();
        $this->assertEmpty($progress->consultant_documents);
    }
}
