<?php

namespace App\Entities\Consultant;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ConsultantRoomTest extends MamiKosTestCase
{
    
    public function testConsultantFound(): void
    {
        $room = factory(Room::class)->create();
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue($mapping->consultant()->first()->is($consultant));
    }

    public function testConsultantNotFound(): void
    {
        $room = factory(Room::class)->create();
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => $consultant->id + 1
        ]);

        $this->assertNull($mapping->consultant()->first());
    }

    public function testRoomFound(): void
    {
        $room = factory(Room::class)->create();
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => $consultant->id
        ]);

        $this->assertTrue($mapping->room()->first()->is($room));
    }

    public function testRoomNotFound(): void
    {
        $room = factory(Room::class)->create();
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id + 1,
            'consultant_id' => $consultant->id
        ]);

        $this->assertNull($mapping->room()->first());
    }

    public function testDeleteShouldDeletedRecord()
    {
        $mapping = factory(ConsultantRoom::class)->create();
        ConsultantRoom::where('consultant_id', $mapping->consultant_id)
                ->where('designer_id', $mapping->designer_id)
                ->delete();

        $this->assertDatabaseMissing('consultant_designer', ['designer_id' => $mapping->designer_id]);
    }
}
