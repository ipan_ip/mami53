<?php

namespace App\Entities\Consultant;

use App\Test\MamiKosTestCase;

class ConsultantMappingTest extends MamiKosTestCase
{
    
    public function testConsultantRelationFound()
    {
        $consultantMapping = factory(ConsultantMapping::class)->create();
        factory(Consultant::class)->create(['id' => $consultantMapping->consultant_id]);

        $this->assertEquals(1, $consultantMapping->consultant->count());
        $this->assertInstanceOf(Consultant::class, $consultantMapping->consultant);
    }

    public function testConsultantRelationNotFound()
    {
        $consultantMapping = factory(ConsultantMapping::class)->create();

        $this->assertNull($consultantMapping->consultant);
    }
}
