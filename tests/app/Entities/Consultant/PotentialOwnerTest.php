<?php

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class PotentialOwnerTest extends MamiKosTestCase
{
    
    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $owner = factory(PotentialOwner::class)->create([
            'user_id' => $user->id
        ]);
        $this->assertTrue($owner->user->is($user));
    }

    public function testCreatedBy(): void
    {
        $user = factory(User::class)->create();
        $owner = factory(PotentialOwner::class)->create([
            'created_by' => $user->id
        ]);
        $this->assertTrue($owner->created_by_user->is($user));
    }

    public function testUpdatedBy(): void
    {
        $user = factory(User::class)->create();
        $owner = factory(PotentialOwner::class)->create([
            'updated_by' => $user->id
        ]);
        $this->assertTrue($owner->last_updated_by->is($user));
    }

    public function testProgressable(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $expected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owner->id
        ]);
        $actual = $owner->progress()->first();
        $this->assertTrue($actual->is($expected));
    }

    public function testGetTaskNameAttribute(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $expected = $owner->name;
        $this->assertEquals(
            $expected,
            $owner->getTaskNameAttribute()
        );
    }

    public function testSetFolUpStatToNew()
    {
        $owner = factory(PotentialOwner::class)->make(['followup_status' => 'in-progress']);
        $owner->setFolUpStatToNew();
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_NEW, $owner->followup_status);
    }

    public function testSetFolUpStatToOnGoing()
    {
        $owner = factory(PotentialOwner::class)->make(['followup_status' => 'new']);
        $owner->setFolUpStatToOnGoing();
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_ONGOING, $owner->followup_status);
    }

    public function testSetFolUpStatToDone()
    {
        $owner = factory(PotentialOwner::class)->make(['followup_status' => 'new']);
        $owner->setFolUpStatToDone();
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_DONE, $owner->followup_status);
    }

    public function testProperties(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $expected = factory(Property::class)->create(['owner_user_id' => $potentialOwner->user_id]);

        $this->assertTrue(
            $potentialOwner->properties->first()->is($expected)
        );
    }

    public function testRooms(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $expected = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $potentialOwner->user_id,
            'designer_id' => $expected->id,
            'status' => 'verified'
        ]);

        $this->assertTrue(
            $potentialOwner->rooms->first()->is($expected)
        );
    }

    public function testRoomsShouldNotFindUnverifiedOwner(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $expected = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $potentialOwner->user_id,
            'designer_id' => $expected->id,
        ]);

        $this->assertEmpty($potentialOwner->rooms);
    }
}
