<?php

namespace App\Entities\Consultant;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class DesignerChangesByConsultantTest extends MamiKosTestCase
{
    
    public function testConsultantRelationship()
    {
        $consultant = factory(Consultant::class)->create();
        $changes = factory(DesignerChangesByConsultant::class)->create(['consultant_id' => $consultant->id]);

        $this->assertInstanceOf(Consultant::class, $changes->consultant);
    }

    public function testRoomRelationship()
    {
        $room = factory(Room::class)->create();
        $changes = factory(DesignerChangesByConsultant::class)->create(['designer_id'=>$room->id]);

        $this->assertInstanceOf(Room::class, $changes->room);
    }
}
