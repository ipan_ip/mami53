<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;

class PotentialTenantTest extends MamiKosTestCase
{
    
    protected $potentialTenant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->potentialTenant = factory(PotentialTenant::class)->create(['phone_number' => '081111111']);
    }

    public function testConsultantRelationship()
    {
        factory(Consultant::class)->create(['id' => $this->potentialTenant->consultant_id]);

        $this->assertEquals(1, $this->potentialTenant->consultant->count());
        $this->assertInstanceOf(Consultant::class, $this->potentialTenant->consultant);
    }

    public function testRoomRelationship()
    {
        // temporary solution, skip failed test due to migrate to paratest
        $this->markTestSkipped('Skip test for now');
        factory(Room::class)->create(['id' => $this->potentialTenant->designer_id]);

        $this->assertEquals(1, $this->potentialTenant->room->count());
        $this->assertInstanceOf(Room::class, $this->potentialTenant->room);
    }

    public function testConsultantNoteRelationship()
    {
        factory(ConsultantNote::class)->create(
            ['noteable_id' => $this->potentialTenant->id, 'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT]
        );

        $this->assertInstanceOf(ConsultantNote::class, $this->potentialTenant->consultantNote);
    }

    public function testProgress()
    {
        $potential = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => 'potential_tenant',
            'progressable_id' => $potential->id
        ]);

        $this->assertTrue($potential->progress[0]->is($progress));
    }

    public function testScopeWherePhoneNumber()
    {
        $result = PotentialTenant::wherePhoneNumber('81111111')->first();
        $this->assertEquals($result->phone_number, $this->potentialTenant->phone_number);

        $result = PotentialTenant::wherePhoneNumber('+6281111111')->first();
        $this->assertEquals($result->phone_number, $this->potentialTenant->phone_number);

        $result = PotentialTenant::wherePhoneNumber('081111111')->first();
        $this->assertEquals($result->phone_number, $this->potentialTenant->phone_number);
    }

    public function testIsRegisteredSuccess()
    {
        factory(User::class)->create(
            ['phone_number' => $this->potentialTenant->phone_number, 'role' => UserRole::User, 'is_owner' => 'false']
        );

        $this->assertEquals(true, $this->potentialTenant->isRegistered());
    }

    public function testIsRegisteredFailed()
    {
        $this->assertEquals(false, $this->potentialTenant->isRegistered());
    }

    public function testHasActiveContractSuccess()
    {
        $mamipayTenant = factory(MamipayTenant::class)->create(['phone_number'=>$this->potentialTenant->phone_number]);
        factory(MamipayContract::class)->create(['tenant_id'=>$mamipayTenant,'status'=>MamipayContract::STATUS_ACTIVE]);

        $this->assertEquals(true, $this->potentialTenant->hasActiveContract());
    }

    public function testHasActiveContractFailed()
    {
        $mamipayTenant = factory(MamipayTenant::class)->create(['phone_number'=>$this->potentialTenant->phone_number]);
        factory(MamipayContract::class)->create(['tenant_id'=>$mamipayTenant,'status'=>MamipayContract::STATUS_BOOKED]);

        $this->assertEquals(false, $this->potentialTenant->hasActiveContract());
    }

    public function testGetTaskNameAttribute(): void
    {
        $tenant = factory(PotentialTenant::class)->create();

        $expected = $tenant->name;
        $actual = $tenant->getTaskNameAttribute();

        $this->assertEquals($expected, $actual);
    }

    public function testConsultantRooms()
    {
        $tenant = factory(PotentialTenant::class)->create(['designer_id'=>1]);
        factory(ConsultantRoom::class)->create(['designer_id'=>$tenant->designer_id,'consultant_id'=>1]);
        factory(ConsultantRoom::class)->create(['designer_id'=>$tenant->designer_id,'consultant_id'=>10]);

        $tenant = PotentialTenant::find($tenant->id);

        $this->assertEquals(2, count($tenant->consultant_rooms));
        $this->assertInstanceOf(ConsultantRoom::class,$tenant->consultant_rooms[0]);
    }

    public function testUser(): void
    {
        $tenant = factory(PotentialTenant::class)->create(['phone_number' => '081234']);
        factory(User::class)->create(['phone_number' => '081234', 'is_owner' => 'true', 'role' => UserRole::User]);
        factory(User::class)->create(['phone_number' => '081234', 'is_owner' => 'false', 'role' => UserRole::Administrator]);
        $user = factory(User::class)->create(['phone_number' => '081234', 'is_owner' => 'false', 'role' => UserRole::User]);

        $this->assertTrue(
            $tenant->user->is($user)
        );
    }
}
