<?php
namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;

class MamipayBillingRuleTest extends MamikosTestCase
{
    
    public function testGetFineAmount()
    {
        $result = MamipayBillingRule::getFineAmount(10000, '2020-07-01', 10, 'day');

        $keys = [
            'price_name',
            'price_label',
            'price_total',
            'price_total_string',
            'billing_date',
            'maximum_length',
            'duration_type'
        ];

        foreach($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }

        $this->assertEquals('Fine', $result['price_name']);
        $this->assertEquals('Fine', $result['price_label']);
        $this->assertEquals(10000.0, $result['price_total']);
        $this->assertEquals('Rp10.000', $result['price_total_string']);
        $this->assertEquals('2020-07-01', $result['billing_date']);
        $this->assertEquals(10, $result['maximum_length']);
        $this->assertEquals('hari', $result['duration_type']);

        // tests all 4 type, its ok to redefine the array here.
        // just to make sure when code is change, it is intentionally
        // and the test need to adjust.
        $durType = [
            'day' => 'hari',
            'week' => 'minggu',
            'month' => 'bulan',
            'year' => 'tahun'
        ];

        foreach($durType as $key => $type) {
            $res = MamipayBillingRule::getFineAmount(10000, '2020-07-01', 10, $key);
            $this->assertEquals($type, $res['duration_type']);
        }
    }

    public function testGetDepositAmount()
    {
        $rule = factory(MamipayBillingRule::class)->create([
            "tenant_id" => 1991,
            "room_id" => 101,
            "deposit_amount" => 300000
        ]);

        $result = MamipayBillingRule::getDepositAmount(1991, 101);

        $keys = [
            'price_name',
            'price_label',
            'price_total',
            'price_total_string'
        ];

        foreach($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }

        $this->assertEquals('deposit', $result['price_name']);
        $this->assertEquals('deposit', $result['price_label']);
        $this->assertEquals(300000.0, $result['price_total']);
        $this->assertEquals('Rp300.000', $result['price_total_string']);
    }

    public function testGetDepositAmountShouldReturnNull(): void
    {
        $this->assertNull(MamipayBillingRule::getDepositAmount(5, 1));
    }
}
