<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherPublicCampaign;
use App\Test\MamiKosTestCase;

class MamipayVoucherPublicCampaignTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $publicCampaign = factory(MamipayVoucherPublicCampaign::class)->create();
        $voucher = factory(MamipayVoucher::class)->create([
            'public_campaign_id' => $publicCampaign->id
        ]);

        $this->assertTrue($publicCampaign->vouchers->first()->is($voucher));
    }
}