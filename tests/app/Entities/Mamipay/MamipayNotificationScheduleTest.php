<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;

class MamipayNotificationScheduleTest extends MamiKosTestCase
{
    public function testInvoice(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $notification = factory(MamipayNotificationSchedule::class)->create(['contract_invoice_id' => $invoice->id]);

        $this->assertTrue(
            $notification->invoice->is($invoice)
        );
    }
}
