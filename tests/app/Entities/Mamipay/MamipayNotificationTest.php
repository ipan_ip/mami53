<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;

class MamipayNotificationTest extends MamiKosTestCase
{
    public function testStoreNotifShouldSaveIntoDatabase(): void
    {
        $template = [
            'message' => 'test',
            'scheme' => 'test'
        ];

        MamipayNotification::storeNotif(1, 1, $template);

        $this->assertDatabaseHas(
            'mamipay_notification',
            [
                'recipient_id' => 1,
                'tenant_id' => 1,
                'type' => 'booking_user',
                'notification_text' => 'test',
                'scheme' => 'pay.mamikos.com://test'
            ]
        );
    }
}
