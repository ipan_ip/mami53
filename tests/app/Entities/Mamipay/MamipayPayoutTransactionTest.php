<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayPayoutTransaction;
use App\Entities\Premium\PremiumCashbackHistory;

class MamipayPayoutTransactionTest extends MamiKosTestCase
{

    public function testInvoiceRelation()
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $payout = factory(MamipayPayoutTransaction::class)->create([
            'contract_invoice_id'   => $invoice->id,
        ]);

        $this->assertInstanceOf(MamipayInvoice::class, $payout->invoice);
    }

    public function testCashbackHistoryRelation()
    {
        $payoutTransaction = factory(MamipayPayoutTransaction::class)->create();
        
        factory(PremiumCashbackHistory::class)->create([
            'reference_id' => $payoutTransaction->id,
            'type' => PremiumCashbackHistory::CASHBACK_TYPE_COMMISSION
        ]);

        $this->assertInstanceOf(PremiumCashbackHistory::class, $payoutTransaction->cashback_history);
    }

}