<?php

namespace App\Entities\Mamipay;

use App\Http\Helpers\MamipayApiHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class MamipayInvoicesTest extends MamiKosTestCase
{
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRequestCreateInvoice()
    {
        $data = [
            'code_product' => 'CODE_PRODUCT',
            'order_type' => 'order_type',
            'order_id' => 1,
            'amount' => 10000,
            'expiry_time' => date('Y-m-d H:i:s', strtotime('+2 hours'))
        ];
        $user = factory(User::class)->create();

        $helperMock = Mockery::mock('overload:' . MamipayApiHelper::class);

        $helperMock->shouldReceive('makeRequest')
            ->andReturnUsing(function() {
                $data = new \StdClass;
                $data->id = 1;
                $data->expiry_time = date('Y-m-d H:i:s', strtotime('+2 hours'));

                $resp = new \StdClass;
                $resp->data = $data;

                return $resp;
            });

        $response = MamipayInvoices::requestCreateInvoice($data, $user);

        $this->assertNotNull($response);
    }

}