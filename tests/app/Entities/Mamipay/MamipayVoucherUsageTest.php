<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherUsage;
use App\Test\MamiKosTestCase;

class MamipayVoucherUsageTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $voucher = factory(MamipayVoucher::class)->create();
        $usage = factory(MamipayVoucherUsage::class)->create([
            'voucher_id' => $voucher->id
        ]);

        $this->assertTrue($usage->voucher->is($voucher));
    }
}