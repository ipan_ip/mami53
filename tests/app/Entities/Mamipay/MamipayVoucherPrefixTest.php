<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherPrefix;
use App\Test\MamiKosTestCase;

class MamipayVoucherPrefixTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $voucherPrefix = factory(MamipayVoucherPrefix::class)->create();
        $voucher = factory(MamipayVoucher::class)->create([
            'prefix_id' => $voucherPrefix->id
        ]);

        $this->assertTrue($voucherPrefix->vouchers->first()->is($voucher));
    }
}