<?php

namespace App\Entities\Mamipay;

use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ConsultantNote;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class MamipayContractTest extends MamiKosTestCase
{
    public function testProgress(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $expected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id
        ]);

        $this->assertTrue(
            $contract->progress()->first()->is($expected)
        );
    }

    public function testGetTaskNameAttribute(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);

        $expected = $tenant->name;
        $actual = $contract->getTaskNameAttribute();
        $this->assertEquals($expected, $actual);
    }

    public function testGetDurationFormattedAttributeMonthly(): void
    {
        $contract = factory(MamipayContract::class)->create([
            'duration' => 1,
            'duration_unit' => MamipayContract::UNIT_MONTH
        ]);
        $this->assertEquals('1 bulan', $contract->duration_formatted);
    }

    public function testGetDurationFormattedAttribute3Monthly(): void
    {
        $contract = factory(MamipayContract::class)->create([
            'duration' => 1,
            'duration_unit' => MamipayContract::UNIT_3MONTH
        ]);
        $this->assertEquals('3 bulan', $contract->duration_formatted);
    }

    public function testGetDurationFormattedAttribute6Monthly(): void
    {
        $contract = factory(MamipayContract::class)->create([
            'duration' => 1,
            'duration_unit' => MamipayContract::UNIT_6MONTH
        ]);
        $this->assertEquals('6 bulan', $contract->duration_formatted);
    }

    public function testGetBookingFunnelFromBooking()
    {
        $contract = factory(MamipayContract::class)->create();
        factory(BookingUser::class)->create([
            'contract_id' => $contract->id
        ]);

        $funnelFrom = $contract->getBookingFunnelFrom();

        $this->assertEquals(MamipayContract::FUNNEL_FROM_BOOKING, $funnelFrom);
        $this->assertNotNull($funnelFrom);
        $this->assertIsString($funnelFrom);
    }

    public function testGetBookingFunnelFromConsultant()
    {
        $contract = factory(MamipayContract::class)->create([
            'owner_id' => 1,
            'consultant_id' => 1
        ]);

        $funnelFrom = $contract->getBookingFunnelFrom();

        $this->assertEquals(MamipayContract::FUNNEL_FROM_CONSULTANT, $funnelFrom);
        $this->assertNotNull($funnelFrom);
        $this->assertIsString($funnelFrom);
    }

    public function testGetBookingFunnelFromOwner()
    {
        $contract = factory(MamipayContract::class)->create([
            'owner_id' => 1
        ]);

        $funnelFrom = $contract->getBookingFunnelFrom();

        $this->assertEquals(MamipayContract::FUNNEL_FROM_OWNER, $funnelFrom);
        $this->assertNotNull($funnelFrom);
        $this->assertIsString($funnelFrom);
    }

    public function testGetPaymentStatusAttribute()
    {
        $contract = factory(MamipayContract::class)->create();
        factory(MamipayInvoice::class)->create([
           'contract_id' => $contract->id,
           'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID
        ]);

        $result = $contract->paymentStatus;

        $this->assertEquals(MamipayInvoice::PAYMENT_STATUS_UNPAID, $result);
    }

    public function testGetDurationUnitStringAttribute()
    {
        $durationUnits = [
            MamipayContract::UNIT_WEEK,
            MamipayContract::UNIT_MONTH,
            MamipayContract::UNIT_3MONTH,
            MamipayContract::UNIT_6MONTH,
            MamipayContract::UNIT_YEAR,
            'other'
        ];

        $durationUnitsMap = [
            'weekly',
            'monthly',
            'quarterly',
            'semiannually',
            'yearly',
            null
        ];
        for($i = 0; $i < 5; $i++) {
            $contract = factory(MamipayContract::class)->make(['duration_unit' => $durationUnits[$i]]);
            $this->assertEquals(
                $durationUnitsMap[$i], $contract->duration_unit_string
            );
        }
    }

    public function testConsultantNote(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => 'mamipay_contract',
            'noteable_id' => $contract->id
        ]);

        $this->assertTrue($contract->consultantNote->is($note));
    }

    public function testDetermineScheduledDateWithUnitDays(): void
    {
        $this->assertEquals(
            '2020-10-07 17:17:17',
            MamipayContract::determineScheduledDate('2020-10-06 17:17:17', 2, MamipayContract::UNIT_DAY)
        );
    }

    public function testDetermineScheduledDateWithUnitWeeks(): void
    {
        $this->assertEquals(
            '2020-10-13 17:17:17',
            MamipayContract::determineScheduledDate('2020-10-06 17:17:17', 2, MamipayContract::UNIT_WEEK)
        );
    }

    public function testDetermineScheduledDateWithUnitYears(): void
    {
        $this->assertEquals(
            '2021-10-06 17:17:17',
            MamipayContract::determineScheduledDate('2020-10-06 17:17:17', 2, MamipayContract::UNIT_YEAR)
        );
    }

    public function testDetermineScheduledDateWithUnit3Month(): void
    {
        $this->assertEquals(
            '2021-01-06 17:17:17',
            MamipayContract::determineScheduledDate('2020-10-06 17:17:17', 2, MamipayContract::UNIT_3MONTH)
        );
    }

    public function testDetermineScheduledDateWithUnit6Month(): void
    {
        $this->assertEquals(
            '2021-04-06 17:17:17',
            MamipayContract::determineScheduledDate('2020-10-06 17:17:17', 2, MamipayContract::UNIT_6MONTH)
        );
    }

    public function testGetPaymentStatusAttributeWithPaidStatus(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $this->assertEquals(MamipayInvoice::PAYMENT_STATUS_PAID, $contract->payment_status);
    }

    public function testScopeFilterDownPaymentStatus(): void
    {
        $contract = factory(MamipayContract::class)->create();
        factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'DP/xxxx/xxxx', 'status' => MamipayInvoice::STATUS_PAID]);

        $actual = MamipayContract::filterDownPaymentStatus(MamipayInvoice::STATUS_PAID)->first();

        $this->assertTrue(
            $actual->is($contract)
        );
    }

    public function testScopeFilterSettlementStatus(): void
    {
        $contract = factory(MamipayContract::class)->create();
        factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'ST/xxxx/xxxx', 'status' => MamipayInvoice::STATUS_PAID]);

        $actual = MamipayContract::filterSettlementStatus(MamipayInvoice::STATUS_PAID)->first();

        $this->assertTrue(
            $actual->is($contract)
        );
    }

    public function testTwoMonthsOverWithoutTenant(): void
    {
        $user = factory(User::class)->create();

        $this->assertFalse(
            (new MamipayContract())->twoMonthsOver($user)
        );
    }

    public function testTwoMonthsOverWithoutContract(): void
    {
        $user = factory(User::class)->create();
        factory(MamipayTenant::class)->create(['user_id' => $user->id]);

        $this->assertFalse(
            (new MamipayContract())->twoMonthsOver($user)
        );
    }

    public function testTwoMonthsOverShouldReturnTrue(): void
    {
        $user = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['user_id' => $user->id]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id,
            'end_date' => Carbon::now(),
            'status' => 'active'
        ]);

        $this->assertTrue(
            (new MamipayContract())->twoMonthsOver($user)
        );
    }
}
