<?php

namespace App\Entities\Mamipay;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class MamipayRoomPriceComponentTest extends MamiKosTestCase
{
    public function testPriceComponentAdditionalsRelationIsNull(): void
    {
        // prepare data
        $data = factory(MamipayRoomPriceComponent::class)->create();

        // run test
        $this->assertNull($data->price_component_additionals()->first());
    }

    public function testPriceComponentAdditionalsRelationIsSuccess(): void
    {
        // prepare data
        $data = factory(MamipayRoomPriceComponent::class)->create();
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $data->id
        ]);

        // run test
        $this->assertNotNull($data->price_component_additionals()->first());
    }

    public function testRoomIsNull(): void
    {
        // prepare data
        $data = factory(MamipayRoomPriceComponent::class)->create();

        // run test
        $this->assertNull($data->room);
    }

    public function testRoomIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $data = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $this->assertNotNull($data->room);
    }
}