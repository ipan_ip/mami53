<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;
use App\User;

class MamipayMediaTest extends MamiKosTestCase
{
    protected $media;

    protected function setUp(): void
    {
        parent::setUp();

        $this->media = factory(MamipayMedia::class)->create();
    }

    public function testUser(): void
    {
        $user = factory(User::class)->create();

        $this->media->user_id = $user->id;
        $this->media->save();

        $this->assertTrue(
            $this->media->user->is($user)
        );
    }
}
