<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Test\MamiKosTestCase;

class MamipayVoucherExcludeIdentifierTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $voucher = factory(MamipayVoucher::class)->create();
        $voucherIdentifier = factory(MamipayVoucherExcludeIdentifier::class)->create([
            'voucher_id' => $voucher->id
        ]);

        $this->assertTrue($voucherIdentifier->voucher->is($voucher));
    }
}