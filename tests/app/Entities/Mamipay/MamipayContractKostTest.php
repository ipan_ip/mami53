<?php

declare(strict_types=1);

namespace App\Entities\Mamipay;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MamipayContractKostTest extends MamiKosTestCase
{
    public function testRoom_BelongsTo()
    {
        $response = (new MamipayContractKost)->room();
        $this->assertInstanceOf(BelongsTo::class, $response);
    }

    public function testContractInvoice_HasMany()
    {
        $response = (new MamipayContractKost)->contract_invoice();
        $this->assertInstanceOf(HasMany::class, $response);
    }

    public function testContract(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $contractKost = factory(MamipayContractKost::class)->create(['contract_id' => $contract->id]);

        $this->assertTrue($contractKost->contract->is($contract));
    }

    public function testGetTotalActiveContract(): void
    {
        $room = factory(Room::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE]
        );
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
        ]);

        $result = MamipayContractKost::getTotalActiveContractKosWithin(14, $room->id);
        $this->assertEquals(1, $result);
    }

    public function testGetTotalActiveContractWhereDateExpired(): void
    {
        $room = factory(Room::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE]
        );
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'created_at' => Carbon::now()->subDays(30),
        ]);

        $result = MamipayContractKost::getTotalActiveContractKosWithin(14, $room->id);
        $this->assertEquals(0, $result);
    }

}
