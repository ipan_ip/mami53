<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Test\MamiKosTestCase;

class MamipayVoucherTargetIdentifierTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $voucher = factory(MamipayVoucher::class)->create();
        $voucherIdentifier = factory(MamipayVoucherTargetIdentifier::class)->create([
            'voucher_id' => $voucher->id
        ]);

        $this->assertTrue($voucherIdentifier->voucher->is($voucher));
    }
}