<?php

declare(strict_types=1);

use App\Console\Kernel;
use App\Entities\Contract\ContractInvoice;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\Misc\ManualPayout;
use App\Entities\Mamipay\Payout\PayoutTransaction;
use App\Entities\Mamipay\TransferStatus;
use App\Test\MamiKosTestCase;
use App\Entities\Mamipay\MamipayContract;

class MamipayInvoiceTest extends MamikosTestCase
{
    public function createApplication()
    {
        $app = require __DIR__ . '/../../../../bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();
        return $app;
    }

    public function setUp() : void
    {
        parent::setUp();
    }

    public function testGetIsPaidAttribute()
    {
        $invoiceUnpaid = factory(MamipayInvoice::class)->create(['status'=>MamipayInvoice::STATUS_UNPAID]);
        $invoicePaid = factory(MamipayInvoice::class)->create(['status'=>MamipayInvoice::STATUS_PAID]);
        $this->assertFalse($invoiceUnpaid->is_paid);
        $this->assertTrue($invoicePaid->is_paid);
    }

    public function testGetIsDpAttribute()
    {
        $invoice = factory(MamipayInvoice::class)->make(
            [
                'invoice_number' => 'DP/12345'
            ]
        );
        
        $this->assertTrue($invoice->is_dp);
    }

    public function testGetIsSettlementAttribute()
    {
        $invoice = factory(MamipayInvoice::class)->make(
            [
                'invoice_number' => 'ST/12345'
            ]
        );

        $this->assertTrue($invoice->is_settlement);
    }

    public function testActiveContractRelationship()
    {
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE
        ]);
        $inv = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id
        ]);
        $this->assertEquals($contract->id, $inv->active_contract->id);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetDetailExternal()
    {
        $obj = new stdClass;
        $obj->status = true;

        $mamipayApi = Mockery::mock('overload:App\Libraries\MamipayApi');
        $mamipayApi->shouldReceive('get')->andReturn($obj);

        $result = MamipayInvoice::getDetailExternal(123);

        $this->assertIsObject($result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetPaymentScheduleExternal()
    {
        $obj = new stdClass;
        $obj->status = true;

        $mamipayApi = Mockery::mock('overload:App\Libraries\MamipayApi');
        $mamipayApi->shouldReceive('get')->andReturn($obj);

        $result = MamipayInvoice::getPaymentScheduleExternal(1, 2020);

        $this->assertIsObject($result);
    }

    public function testManualPayout()
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $payout = factory(ManualPayout::class)->create(['invoice_id' => $invoice->id]);

        $this->assertTrue($invoice->manual_payout->first()->is($payout));
    }

    public function testSplitPayoutProcessedTransactions()
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $payout = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);

        $this->assertTrue(
            $invoice->split_payout_processed_transactions->first()->is($payout)
        );
    }
}
