<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;
use App\User;

class MamipayOwnerTest extends MamiKosTestCase
{
    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $owner = factory(MamipayOwner::class)->create(['user_id' => $user->id]);

        $this->assertTrue($owner->user->is($user));
    }
}
