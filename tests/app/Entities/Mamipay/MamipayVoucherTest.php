<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Entities\Mamipay\MamipayVoucherPrefix;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Test\MamiKosTestCase;

class MamipayVoucherTest extends MamikosTestCase
{
    public function testVoucherPrefix()
    {
        $voucherPrefix = factory(MamipayVoucherPrefix::class)->create();
        $voucher = factory(MamipayVoucher::class)->create([
            'prefix_id' => $voucherPrefix->id
        ]);

        $this->assertTrue($voucher->code_prefix->is($voucherPrefix));
    }

    public function testGetIdentifierBy()
    {
        $voucher = factory(MamipayVoucher::class)->create();
        $includeIdentifier = factory(MamipayVoucherTargetIdentifier::class)->create([
            'voucher_id' => $voucher->id
        ]);
        $excludeIdentifier = factory(MamipayVoucherExcludeIdentifier::class)->create([
            'voucher_id' => $voucher->id
        ]);

        $this->assertTrue($voucher->getIncludeIdentifierBy($includeIdentifier->type, $includeIdentifier->identifier)->is($includeIdentifier));
        $this->assertTrue($voucher->getExcludeIdentifierBy($excludeIdentifier->type, $excludeIdentifier->identifier)->is($excludeIdentifier));
    }

    public function testApplicableProfession()
    {
        $voucher = factory(MamipayVoucher::class)->create();
        $includeIdentifier = factory(MamipayVoucherTargetIdentifier::class)->create([
            'voucher_id' => $voucher->id,
            'type' => MamipayVoucher::IDENTIFIER_PROFESSION,
            'identifier' => 'Student'
        ]);
        $excludeIdentifier = factory(MamipayVoucherExcludeIdentifier::class)->create([
            'voucher_id' => $voucher->id,
            'type' => MamipayVoucher::IDENTIFIER_PROFESSION,
            'identifier' => 'Lawyer'
        ]);

        $this->assertEquals('Developer', $voucher->getApplicableProfessionAttribute('Developer'));
        $this->assertEquals($includeIdentifier->identifier, $voucher->applicable_profession);
        $this->assertEquals($excludeIdentifier->identifier, $voucher->applicable_profession_exclude);
    }
}