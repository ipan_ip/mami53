<?php
namespace Test\App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;

class MamipayTenantTest extends MamiKosTestCase
{
    
    public function testTenantContractActiveIsTrue()
    {
        $user = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'user_id' => $user->id
        ]);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id,
            'status' => MamipayContract::STATUS_ACTIVE,
            'end_date' => Carbon::today()->addMonths(3)
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID
        ]);

        $result = MamipayTenant::tenantContractActive($user->id);
        $this->assertNotNull($result);
        $this->assertIsBool($result);
        $this->assertEquals(true, $result);
        $this->assertTrue($result);
    }

    public function testTenantContractActiveIsFalse()
    {
        $user = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'user_id' => $user->id
        ]);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id,
            'status' => MamipayContract::STATUS_ACTIVE,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID
        ]);

        $result = MamipayTenant::tenantContractActive($user->id);
        $this->assertNotNull($result);
        $this->assertIsBool($result);
        $this->assertEquals(false, $result);
        $this->assertFalse($result);
    }

    public function testHasActiveContractAttributeIsFalse(): void
    {
        // create tenant data
        $tenantEntity = factory(MamipayTenant::class)->create();

        // run test
        $this->assertFalse($tenantEntity->hasActiveContractAttribute());
    }

    public function testHasActiveContractAttributeIsTrue(): void
    {
        // create tenant & contract data
        $tenantEntity = factory(MamipayTenant::class)->create();
        $contractEntity = factory(MamipayContract::class)->create([
            'tenant_id' => $tenantEntity->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);

        // run test
        $this->assertTrue($tenantEntity->hasActiveContractAttribute());
    }

    public function testCleanPhoneNumber()
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            MamipayTenant::class,
            'cleanPhoneNumber'
        );
        $entity = $this->app->make(MamipayTenant::class);

        // run test
        $this->assertEquals('82234178993', $method->invokeArgs($entity, ['082234178993']));
        $this->assertEquals('82234178993', $method->invokeArgs($entity, ['6282234178993']));
        $this->assertEquals('82234178993', $method->invokeArgs($entity, ['+6282234178993']));
    }

    public function testScopeWherePhoneNumberV2()
    {
        $phoneNumber = '8987'.mt_rand(111111, 999999);
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'phone_number' => '+62' . $phoneNumber,
        ]);
        /** @var MamipayTenant */
        $result = MamipayTenant::wherePhoneNumberV2('0' . $phoneNumber)->first();
        $this->assertEquals($mamipayTenant->id, $result->id);
    }

    public function testScopeWhereRoomInAreaCity(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id, 'status' => MamipayContract::STATUS_ACTIVE]);

        $kost = factory(Room::class)->create(['area_city' => 'Sleman']);
        factory(MamipayContractKost::class)->create(['contract_id' => $contract->id, 'designer_id' => $kost->id]);

        $actual = MamipayTenant::whereRoomInAreaCity(['Sleman'])->first();

        $this->assertTrue(
            $actual->is($tenant)
        );
    }

    public function testScopeWhereRoomIdInArray(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id, 'status' => MamipayContract::STATUS_ACTIVE]);

        $kost = factory(Room::class)->create();
        factory(MamipayContractKost::class)->create(['contract_id' => $contract->id, 'designer_id' => $kost->id]);

        $actual = MamipayTenant::whereRoomIdInArray([$kost->id])->first();

        $this->assertTrue(
            $actual->is($tenant)
        );
    }
}
