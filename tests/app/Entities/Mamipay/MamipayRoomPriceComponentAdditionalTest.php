<?php

namespace App\Entities\Mamipay;

use App\Test\MamiKosTestCase;

class MamipayRoomPriceComponentAdditionalTest extends MamiKosTestCase
{
    public function testPriceComponentRelationIsNull(): void
    {
        // prepare data
        $data = factory(MamipayRoomPriceComponentAdditional::class)->create();

        // run test
        $this->assertNull($data->price_component);
    }

    public function testPriceComponentRelationSuccess(): void
    {
        // prepare data
        $priceComponent = factory(MamipayRoomPriceComponent::class)->create();
        $data = factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $priceComponent->id
        ]);

        // run test
        $this->assertNotNull($data->price_component);
    }
}