<?php

namespace App\Entities\Config;

use App\Test\MamiKosTestCase;
use App\Entities\Config;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AppConfigTest extends MamiKosTestCase
{
    const EMAIL_BLACKLISTED_CONFIG_NAME = 'mamipay_blacklisted_mail';

    public function testGetConfigBlacklistedEmailsIsValid()
    {
        factory(AppConfig::class)->create([
            'name' => self::EMAIL_BLACKLISTED_CONFIG_NAME,
            'value' => 'tenant@mamikos.com,dummy@mail.dummy'
        ]);

        $result = AppConfig::getConfigBlacklistedEmails();
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertEquals('tenant@mamikos.com', $result[0]);
    }

    public function testGetConfigBlacklistedEmailsIsNotValid()
    {
        $result = AppConfig::getConfigBlacklistedEmails();
        $this->assertNull($result);
    }
}