<?php

namespace App\Entities\GoldPlus;

use App\Test\MamiKosTestCase;
use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\User;

class SubmissionTest extends MamiKosTestCase
{

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetListingIdsArray()
    {
        $submission = factory(Submission::class)->make([
            'listing_ids' => '1,2',
        ]);
        $this->assertEquals([1, 2], $submission->listing_ids_array);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetListingIdsArrayNullShouldReturnEmpty()
    {
        $submission = factory(Submission::class)->make([
            'listing_ids' => null,
        ]);
        $this->assertEquals([], $submission->listing_ids_array);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetListingIdsArrayEmptyStringShouldReturnEmpty()
    {
        $submission = factory(Submission::class)->make([
            'listing_ids' => '',
        ]);
        $this->assertEquals([], $submission->listing_ids_array);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetListingIdsArrayOnlySpacesShouldReturnEmpty()
    {
        $submission = factory(Submission::class)->make([
            'listing_ids' => '  ',
        ]);
        $this->assertEquals([], $submission->listing_ids_array);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetIsAllListingsProcessed()
    {
        /** @var Submission */
        $submission = factory(Submission::class)->make([
            'listing_ids' => '1,2,3',
            'status' => SubmissionStatus::DONE,
        ]);
        $this->assertTrue($submission->is_all_listings_processed);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testGetIsAllListingsNoDoneAt()
    {
        /** @var Submission */
        $submission = factory(Submission::class)->make([
            'listing_ids' => '1,2,3',
        ]);
        $this->assertFalse($submission->is_all_listings_processed);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Entities\GoldPlus\Submission
     */
    public function testPackageRelation()
    {
        $package = factory(Package::class)->create();
        $submission = factory(Submission::class)->create([
            'package_id' => $package,
        ]);
        $this->assertEquals($package->id, $submission->package->id);
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Entities\GoldPlus\Submission
     */
    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $submission = factory(Submission::class)->create([
            'user_id' => $user->id,
        ]);

        $this->assertEquals($user->id, $submission->user->id);
    }
}
