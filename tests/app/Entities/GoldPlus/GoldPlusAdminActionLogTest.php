<?php

namespace App\Entities\GoldPlus;

use App\Enums\GoldPlus\AdminActionCode;
use App\Test\MamiKosTestCase;

class GoldPlusAdminActionLogTest extends MamiKosTestCase
{

    /**
     * @group UG
     * @group UG-4072
     */
    public function testNew()
    {
        $log = GoldPlusAdminActionLog::new(
            mt_rand(),
            mt_rand(),
            AdminActionCode::getRandomValue()
        );
        $this->assertNotNull($log);
    }
}
