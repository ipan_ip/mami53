<?php 
namespace App\Entities\Log;

use App\Test\MamiKosTestCase;
use App\Entities\Log\ThanosBookingRequestLog;

class ThanosBookingRequestLogTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    /**
     * @group UG
     * @group UG-1151
     * @group App/Entities/Log/ThanosBookingRequestLog
     */
    public function testGetThanosBookingRequestLogDocumentSuccess()
    {
        $bookingOwnerRequestId = mt_rand(1111, 9999);
        $bookingRequestLog = factory(ThanosBookingRequestLog::class)
            ->make(['booking_request.id' => $bookingOwnerRequestId]);
        
        $this->assertEquals($bookingRequestLog['booking_request']['id'], $bookingOwnerRequestId);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group App/Entities/Log/ThanosBookingRequestLog
     */
    public function testGetThanosBookingRequestLogDocumentFailed()
    {
        $bookingOwnerRequestId = mt_rand(1111, 9999);
        $bookingRequestLog = factory(ThanosBookingRequestLog::class)
            ->make(['booking_request.id' => $bookingOwnerRequestId.mt_rand(1, 10)]);
        
        $this->assertNotEquals($bookingRequestLog['booking_request']['id'], $bookingOwnerRequestId);
    }
}
