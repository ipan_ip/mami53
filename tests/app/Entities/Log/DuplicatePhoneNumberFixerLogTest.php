<?php

namespace App\Entities\Log;

use App\Test\MamiKosTestCase;
use App\Entities\Log\DuplicatePhoneNumberFixerLog;

class DuplicatePhoneNumberFixerLogTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-4450
     * @group App/Entities/Log/DuplicatePhoneNumberFixerLog
     */
    public function testGetThanosBookingRequestLogDocumentSuccess()
    {
        $userId = mt_rand(1111, 9999);
        $duplicatePhoneNumberFixerLog = factory(DuplicatePhoneNumberFixerLog::class)
            ->make(['user_id' => $userId]);
        
        $this->assertEquals($duplicatePhoneNumberFixerLog['user_id'], $userId);
    }
}