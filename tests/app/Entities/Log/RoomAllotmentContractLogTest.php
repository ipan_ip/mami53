<?php
namespace App\Entities\Log;

use Carbon\Carbon;
use App\Test\MamiKosTestCase;
use App\Entities\Log\RoomAllotmentContractLog;

class RoomAllotmentContractLogTest extends MamiKosTestCase
{
    
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testCreateDocumentSuccess(): void
    {
        // prepare data
        $logEntity = factory(RoomAllotmentContractLog::class)->make([
            'designer_room_id' => 1
        ]);

        // run test
        $this->assertEquals(1, $logEntity['designer_room_id']);
        $this->assertNull($logEntity['deleted_at']);
    }

    public function testDeleteDocumentSuccess(): void
    {
        // prepare data
        $logEntity = factory(RoomAllotmentContractLog::class)->make([
            'designer_room_id' => 1,
            'deleted_at' => Carbon::now()
        ]);

        // run test
        $this->assertNotNull($logEntity['deleted_at']);
    }
}