<?php
namespace App\Entities\Area;

use App\Test\MamiKosTestCase;

class AreaTest extends MamiKosTestCase
{
    public function testRemovePrefix()
    {
        $cities = [
            "City Jakarta",
            "Kota Jakarta",
            "Kabupaten Jakarta",
            "Kecamatan Jakarta",
            "Town Jakarta"
        ];
        
        foreach ($cities as $city) {
            $this->assertEquals("Jakarta", Area::removePrefix($city));
        }

        // dont remove non-city words
        $this->assertEquals("Dermaga Jakarta", Area::removePrefix("Dermaga Jakarta"));
    }

    public function testConvertToBigCity()
    {
        // test Jogja
        $jogjaCities = ['Sleman', 'Bantul','Kulonprogo','Yogyakarta'];
        foreach ($jogjaCities as $city) {
            $this->assertEquals('Jogja', Area::convertToBigCity($city));
        }

        $jakartaCities = ['Bekasi','Tangerang','Bogor','Tangerang Selatan'];
        foreach ($jakartaCities as $city) {
            $this->assertEquals('Jakarta', Area::convertToBigCity($city));
        }

        $bandungCities = ['Sumedang'];
        foreach ($bandungCities as $city) {
            $this->assertEquals('Bandung', Area::convertToBigCity($city));
        }

        // other values should stay the same
        $this->assertEquals('Medan', Area::convertToBigCity('Medan'));
    }

    public function testCityKeyword()
    {
        $jakartas = [
            'Kota Jakarta',
            'Tangerang City',
            'Kota Bekasi'
        ];

        foreach ($jakartas as $city) {
            $this->assertEquals('Jakarta', Area::cityKeyword($city));
        }
    }
}