<?php

namespace App\Entities\Area;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Schema;

class CityTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */
    public function testCityEntity()
    {
        Schema::disableForeignKeyConstraints();
        $city = factory(City::class)->create(['id' => 5]);
        $id = 2;
        factory(Subdistrict::class, 5)->make()->each(function ($subdistrict) use ($city, &$id) {
            $subdistrict->id = $id++;
            $subdistrict->area_city_id = $city->id;
            $subdistrict->save();
        });

        $city->load('subdistricts');
        Schema::enableForeignKeyConstraints();
        $this->assertCount(5, $city->subdistricts);
    }
}
