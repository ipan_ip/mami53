<?php
namespace App\Entities\Area;

use App\Test\MamiKosTestCase;
use DB;

class AreaGeolocationTest extends MamiKosTestCase
{
    /**
     * @var areaGeolocation|mixed
     */
    private $areaGeolocation;

    protected function setUp(): void
    {
        parent::setUp();
        $this->areaGeolocation = app()->make(AreaGeolocation::class);
    }

    public function testScopeExclude()
    {
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        $response = AreaGeolocation::exclude(['geolocation'])->first();
        $this->assertObjectNotHasAttribute('geolocation', $response);
    }

    public function testGetWithPaginationProvinceLevel()
    {
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        DB::commit();

        $request = [
            'name' => 'Jawa Barat',
            'level' => 'province'
        ];

        $response = $this->areaGeolocation->getWithPagination(20, $request);
        $this->assertCount(1, $response);

        // because we are using DB::commit(), we need to delete the record manually
        DB::table('area_geolocation')->delete();
    }

    public function testGetWithPaginationCityLevel()
    {
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        DB::commit();

        $request = [
            'name' => 'Bandung',
            'level' => 'city'
        ];

        $response = $this->areaGeolocation->getWithPagination(20, $request);
        $this->assertCount(1, $response);

        // because we are using DB::commit(), we need to delete the record manually
        DB::table('area_geolocation')->delete();
    }

    public function testGetWithPaginationWithoutLevelFilter()
    {
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        DB::commit();

        $request = [
            'name' => 'Bandung',
        ];

        $response = $this->areaGeolocation->getWithPagination(20, $request);
        $this->assertCount(1, $response);

        // because we are using DB::commit(), we need to delete the record manually
        DB::table('area_geolocation')->delete();
    }

    public function testGetWithPaginationUsingEmptyRequest()
    {
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);
        DB::commit();

        $response = $this->areaGeolocation->getWithPagination(20, []);
        $this->assertCount(1, $response);

        // because we are using DB::commit(), we need to delete the record manually
        DB::table('area_geolocation')->delete();
    }
}