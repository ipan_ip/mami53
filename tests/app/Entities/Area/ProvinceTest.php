<?php

namespace App\Entities\Area;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Schema;

class ProvinceTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */
    public function testRelationToCities()
    {
        Schema::disableForeignKeyConstraints();
        $province = factory(Province::class)->create();
        $id = 21;
        $cities = factory(City::class, 5)->make()->each(
            function ($city) use ($province, &$id) {
                $city->area_province_id = $province->id;
                $city->id = $id++;
                $city->save();
            }
        );
        Schema::enableForeignKeyConstraints();

        $this->assertCount(count($cities), $province->cities);
    }
}
