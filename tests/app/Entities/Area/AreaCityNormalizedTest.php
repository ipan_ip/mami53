<?php
namespace App\Entities\Area;

use App\Test\MamiKosTestCase;

class AreaCityNormalizedTest extends MamiKosTestCase
{
    public function testRoom()
    {
        $relation = (new AreaCityNormalized)->room();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $relation);
        $this->assertInstanceOf('App\Entities\Room\Room', $relation->getRelated());
        $this->assertEquals('designer_id', $relation->getForeignKey());
    }

}