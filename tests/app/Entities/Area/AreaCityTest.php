<?php
namespace App\Entities\Area;

use App\Test\MamiKosTestCase;

class AreaCityTest extends MamiKosTestCase
{
    public function testNormalizedCities()
    {
        $relation = (new AreaCity)->normalized_cities();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\HasMany', $relation);
        $this->assertInstanceOf('App\Entities\Area\AreaCityNormalized', $relation->getRelated());
        $this->assertEquals('normalized_city', $relation->getForeignKeyName());
    }
}
