<?php

namespace App\Entities\Area;

use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Landing\LandingVacancy;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class AreaBigMapperTest extends MamiKosTestCase
{
    use WithFaker;
    protected $areaBIgMapper;

    protected function setUp()
    {
        parent::setUp();
        $this->areaBIgMapper = new AreaBigMapper();
        $this->setUpFaker();
    }

    public function testBigCityMapperWithAreaCityParamIsEmpty()
    {
        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $result = $this->areaBIgMapper->bigCityMapper('');

        $areaBigCities = $this->areaBIgMapper->get();

        $expectedResult = [];

        foreach ($areaBigCities as $area) {
            $expectedResult[$area->area_city] = [
                'area_big' => $area->area_big,
                'province' => $area->province
            ];
        }

        $this->assertEquals($expectedResult, $result);
    }

    public function testBigCityMapperWithAreaCityParamIsNotEmptyAndExistingRecord()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $result = $this->areaBIgMapper->bigCityMapper($area->area_city);

        $this->assertEquals($area->area_big, $result);
    }

    public function testBigCityMapperWithAreaCityParamIsNotEmptyAndWithoutExistingRecord()
    {
        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $result = $this->areaBIgMapper->bigCityMapper('-');

        $this->assertNull($result);
    }

    public function testAreaBigMapperWithAreaCityIsEmpty()
    {
        $this->assertNull($this->areaBIgMapper->areaBIgMapper(''));
    }

    public function testAreaBigMapperWithoutExistingRecord()
    {
        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $this->assertNull($this->areaBIgMapper->areaBIgMapper('-'));
    }

    public function testAreaBigMapperWithExistingRecord()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $this->assertEquals($area->area_big, $this->areaBIgMapper->areaBIgMapper($area->area_city));
    }

    public function testGetAreaCampusMatch()
    {
        $campus = $this->faker->randomElement(array_keys(AreaBigMapper::$campusMapper));
        $string = str_replace(' ', '-', $campus);
        $result = AreaBigMapper::getAreaCampusMatch($string);
        $this->assertEquals(AreaBigMapper::$campusMapper[$campus], $result);
    }

    public function testGetAreaCampusMatchWithoutExistidRecord()
    {
        $this->assertEmpty(AreaBigMapper::getAreaCampusMatch(''));
    }

    public function testGetAreaBigMatchWithExistingRecord()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);
        $string = str_replace(' ', '-', $area->area_big);

        $this->assertEquals($area->area_big, $this->areaBIgMapper->getAreaBigMatch($string));
    }

    public function testGetAreaBigMatchWithoutExistingRecord()
    {
        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $this->assertEmpty($this->areaBIgMapper->getAreaBigMatch(''));
    }

    public function testmatchAreaCityWithExistingRecord()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);
        $string = strtolower(str_replace(' ', '-', $area->area_city));

        $result = $this->areaBIgMapper->matchAreaCity($string, strtolower($area->area_big));
        $this->assertEquals(strtolower($area->area_city), $result);
    }

    public function testmatchAreaCityWithExistingRecordWithAreaBigParamIsEmpty()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);
        $string = strtolower(str_replace(' ', '-', $area->area_city));

        $result = $this->areaBIgMapper->matchAreaCity($string, '');
        $this->assertEquals(strtolower($area->area_city), $result);
    }

    public function testmatchAreaCityWithNoMatchingString()
    {
        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);

        $result = $this->areaBIgMapper->matchAreaCity('', '');
        $this->assertEmpty($result);
    }

    public function testMatchAreaBigWithTypeIsCampus()
    {
        $campus = $this->faker->randomElement(array_keys(AreaBigMapper::$campusMapper));
        $string = str_replace(' ', '-', $campus);
        $expectedResult = AreaBigMapper::getAreaCampusMatch($string);
        $result = AreaBigMapper::matchAreaBig($string, 'campus');
        $this->assertEquals($expectedResult, $result);
    }

    public function testMatchAreaBigWithTypeIsArea()
    {
        $area = factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 1',
            'area_city' => 'Area City 1',
            'province' => 'province 1',
        ]);

        factory(AreaBigMapper::class)->create([
            'area_big' => 'Area Big 2',
            'area_city' => 'Area City 2',
            'province' => 'province 2',
        ]);
        $string = str_replace(' ', '-', $area->area_big);

        $expectedResult = $this->areaBIgMapper->getAreaBigMatch($string);
        $result = AreaBigMapper::matchAreaBig($string, 'area');
        $this->assertEquals($expectedResult, $result);
    }

    public function testMatchAreaBigWithoutArea()
    {
        $this->assertEquals('semarang', AreaBigMapper::matchAreaBig('kost-dekat-simpang-lima-murah', 'area'));
        $this->assertEquals('jakarta', AreaBigMapper::matchAreaBig('kost-dekat-plaza-indonesia-murah', 'area'));
    }

    public function testLandingConnectorWithTypeIsKostAndLandingIsNull()
    {
        $result = AreaBigMapper::landingConnector('kost', null);
        $this->assertEquals($result, [
            'landing_apartment' => null,
            'landing_vacancy' => null
        ]);
    }

    public function testLandingConnectorWithTypeIsApartmentAndLandingIsNull()
    {
        $result = AreaBigMapper::landingConnector('apartment', null);
        $this->assertEquals($result, [
            'landing_kost' => null,
            'landing_vacancy' => null
        ]);
    }

    public function testLandingConnectorWithTypeIsJobAndLandingIsNull()
    {
        $result = AreaBigMapper::landingConnector('job', null);
        $this->assertEquals($result, [
            'landing_kost' => null,
            'landing_apartment' => null
        ]);
    }

    public function testLandingConnectorWithTypeIsKost()
    {
        $landing = factory(Landing::class)->create([
            'area_city' => 'Area City 1',
            'area_subdistrict' => 'Subdistrict 1',
        ]);
        $landingApartment = factory(LandingApartment::class)->create([
            'area_city' => $landing->area_city,
            'area_subdistrict' => $landing->area_subdistrict
        ]);
        $landingvacancy = factory(LandingVacancy::class)->create([
            'area_city' => $landing->area_city,
            'area_subdistrict' => $landing->area_subdistrict
        ]);

        $result = AreaBigMapper::landingConnector('kost', $landing);
        $this->assertEquals($landingApartment->id, $result['landing_apartment']->id);
        $this->assertEquals($landingvacancy->id, $result['landing_vacancy']->id);
    }

    public function testLandingConnectorWithTypeIsApartment()
    {
        $landingApartment = factory(LandingApartment::class)->create([
            'area_city' => 'Area City 1',
            'area_subdistrict' => 'Subdistrict 1'
        ]);
        $landing = factory(Landing::class)->create([
            'area_city' => $landingApartment->area_city,
            'area_subdistrict' => $landingApartment->area_subdistrict
        ]);
        $landingvacancy = factory(LandingVacancy::class)->create([
            'area_city' => $landingApartment->area_city,
            'area_subdistrict' => $landingApartment->area_subdistrict
        ]);

        $result = AreaBigMapper::landingConnector('apartment', $landing);
        $this->assertEquals($landing->id, $result['landing_kost']->id);
        $this->assertEquals($landingvacancy->id, $result['landing_vacancy']->id);
    }

    public function testLandingConnectorWithTypeIsJob()
    {
        $landingvacancy = factory(LandingVacancy::class)->create([
            'area_city' => 'Area City 1',
            'area_subdistrict' => 'Subdistrict 1'
        ]);
        $landingApartment = factory(LandingApartment::class)->create([
            'area_city' => $landingvacancy->area_city,
            'area_subdistrict' => $landingvacancy->area_subdistrict
        ]);
        $landing = factory(Landing::class)->create([
            'area_city' => $landingvacancy->area_city,
            'area_subdistrict' => $landingvacancy->area_subdistrict
        ]);


        $result = AreaBigMapper::landingConnector('job', $landingvacancy);
        $this->assertEquals($landing->id, $result['landing_kost']->id);
        $this->assertEquals($landingApartment->id, $result['landing_apartment']->id);
    }
}
