<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DataStageTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    /**
     * @group PMS-495
     */
    public function testRelationToRoom()
    {
        $room = factory(Room::class)->create();
        $dataStage = factory(DataStage::class)->state('complete')->create(['designer_id' => $room->id]);

        $dataStage->load('room');
        $this->assertEquals($room->name, $dataStage->room->name);
    }

    /**
     * @group PMS-495
     */
    public function testRelationFromRoom()
    {
        $room = factory(Room::class)->create();
        $dataStage = factory(DataStage::class)->state('complete')->create(['designer_id' => $room->id]);

        $room->load('data_stage');
        $this->assertEquals($room->data_stage->first()->id, $dataStage->id);
    }

    /**
     * @group PMS-495
     */
    public function testMakeStagesCreationNotSaved()
    {
        $room = factory(Room::class)->create();
        $dataStages = (new DataStage())->makeStages($room->id);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $dataStages->count());
        $this->assertDatabaseMissing('designer_data_stage_status', ['designer_id' => $room->id]);
    }

    /**
     * @group PMS-495
     */
    public function testMakeStagesCreationSaved()
    {
        $room = factory(Room::class)->create();
        $dataStages = (new DataStage())->makeStages($room->id, function (DataStage $dataStage): DataStage {
            $dataStage->save();
            return $dataStage;
        });

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $dataStages->count());
        $this->assertDatabaseHas('designer_data_stage_status', ['designer_id' => $room->id]);
    }
}
