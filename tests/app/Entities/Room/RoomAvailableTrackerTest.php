<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;

class RoomAvailableTrackerTest extends MamiKosTestCase
{
    
    public function testRelationToRoomSuccess()
    {
        $room = factory(Room::class)->create();

        $roomAvailableTracker = factory(RoomAvailableTracker::class)->create(['designer_id' => $room->id]);
        $roomAvailableTracker->load('room');
        $roomFromRelation = $roomAvailableTracker->room;

        $this->assertSame($roomFromRelation->song_id, $room->song_id);
    }
}
