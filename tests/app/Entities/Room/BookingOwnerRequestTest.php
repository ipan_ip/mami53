<?php

namespace app\Entities\Room;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BookingOwnerRequestTest extends MamiKosTestCase
{
    
    public function testStore()
    {
        $user = factory(User::class)->create();

        $result = BookingOwnerRequest::store('1234', $user->id, 'approve');
        $this->assertNotEmpty($result);
        $this->assertInstanceOf(BookingOwnerRequest::class, $result);
    }

    public function testScopeRequestedBefore()
    {
        $date = Carbon::today();
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create();

        $result = $bookingOwnerRequest->requestedBefore($date);

        $this->assertNotEmpty($result);
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testScopeRequestedAfter()
    {
        $date = Carbon::today();
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create();

        $result = $bookingOwnerRequest->requestedAfter($date);

        $this->assertNotEmpty($result);
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testRelationToUser()
    {
        $user = factory(User::class)->create();
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create(['user_id' => $user->id]);

        $userFromDB = $bookingOwnerRequest->user;

        $this->assertSame($user->id, $userFromDB->id);
    }

    public function testRelationToRoom()
    {
        $room = factory(Room::class)->create();
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create(['designer_id' => $room->id]);

        $roomFromDB = $bookingOwnerRequest->room;

        $this->assertSame($room->id, $roomFromDB->id);
    }

    public function testRelationToBookingRoom()
    {
        $room = factory(Room::class)->create();
        $bookingRoom = factory(BookingDesigner::class)->create(['designer_id' => $room->id]);
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create(['designer_id' => $room->id]);

        $bookingRoomFromDB = $bookingOwnerRequest->booking_room;

        $this->assertSame($bookingRoom->id, $bookingRoomFromDB->id);
    }

    public function testRelationToMamipayOwnerProfile()
    {
        $user = factory(User::class)->create();
        $mamipayOwner = factory(MamipayOwner::class)->create(['user_id' => $user->id]);
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create(['user_id' => $user->id]);

        $mamipayOwnerFromDB = $bookingOwnerRequest->mamipay_owner_profile;

        $this->assertSame($mamipayOwner->id, $mamipayOwnerFromDB->id);
    }

    public function testRelationToVerificator()
    {
        $verificator = factory(User::class)->create();
        $bookingOwnerRequest = factory(BookingOwnerRequest::class)->create(['verificator_user_id' => $verificator->id]);

        $verificatorFromDB = $bookingOwnerRequest->verificator;

        $this->assertSame($verificator->id, $verificatorFromDB->id);
    }
}
