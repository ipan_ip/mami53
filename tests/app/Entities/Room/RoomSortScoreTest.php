<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;

class RoomSortScoreTest extends MamiKosTestCase
{
    
    public function testCreateFrom()
    {
        $room = factory(Room::class)->make();
        $response = RoomSortScore::createFrom($room);

        $this->assertNotNull($response);
        $this->assertInstanceOf(RoomSortScore::class, $response);
    }

    public function testGetScore()
    {
        $room = factory(Room::class)->make();
        $sortScore = RoomSortScore::createFrom($room);
        $response = $sortScore->getScore();

        $this->assertEquals(1010000000, $response);
    }

    public function testGetScoreWithUplifting()
    {
        $room = factory(Room::class)->make(
            [
                'slug' => 'kost-jakarta-barat-kost-campur-eksklusif-kost-grogol-petamburan-trans-jakarta-barat-rmz'
            ]
        );

        $sortScore = RoomSortScore::createFrom($room);
        $response = $sortScore->getScore();

        $this->assertEquals(RoomSortScore::FULL_SCORE, $response);
    }

    public function testIsEligibleForUpliftingReturnTrue()
    {
        $room = factory(Room::class)->make(
            [
                'slug' => 'kost-jakarta-barat-kost-campur-eksklusif-kost-grogol-petamburan-trans-jakarta-barat-rmz'
            ]
        );

        $sortScore = RoomSortScore::createFrom($room);
        $response = $sortScore->isEligibleForUplifting();

        $this->assertTrue($response);
    }

    public function testIsEligibleForUpliftingReturnFalse()
    {
        $room = factory(Room::class)->make(
            [
                'slug' => 'kost-jakarta-selatan-kost-campur-eksklusif-kost-tebet-timur-syailendra-tipe-a-jakarta-selatan-rmz'
            ]
        );

        $sortScore = RoomSortScore::createFrom($room);
        $response = $sortScore->isEligibleForUplifting();

        $this->assertFalse($response);
    }

    public function testIsEligibleForUpliftingReturnTrueInArray()
    {
        $upliftedRoom = [
            "kost-jakarta-barat-kost-campur-eksklusif-kost-grogol-petamburan-trans-jakarta-barat-rmz",
            "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-amira-tipe-a-depok-sleman",
            "kost-sleman-kost-putra-eksklusif-kost-mamirooms-ugm-pandega-duta-sleman-yogyakarta-1",
            "kost-sleman-kost-campur-eksklusif-kost-mamirooms-ugm-yang-dji-tipe-b-mlati-sleman",
            "kost-sleman-kost-putri-eksklusif-kost-mamirooms-kataji-tipe-a-mlati-sleman",
            "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-pastika-d2-tipe-a-depok-sleman"
        ];

        foreach ($upliftedRoom as $slug) {
            $room = factory(Room::class)->make(['slug' => $slug]);
            $sortScore = RoomSortScore::createFrom($room);
            $response = $sortScore->isEligibleForUplifting();
            $this->assertTrue($response);
        }
    }
}
