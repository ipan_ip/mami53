<?php

namespace App\Entities\Room;

use App;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Booking\BookingDiscount;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Libraries\SMSLibrary;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class TraitFieldGeneratorTest extends MamiKosTestCase
{
    use WithoutEvents;
    use WithoutMiddleware;
    use WithFaker;

    const ROOM_LATITUDE = -8.014663865095645;
    const ROOM_LONGITUDE = 110.2936449049594;

    const PRICE_DAILY = 100000;
    const PRICE_WEEKLY = 500000;
    const PRICE_MONTHLY = 1500000;
    const PRICE_QUARTERLY = 4500000;
    const PRICE_SEMIANNUALLY = 7000000;
    const PRICE_ANNUALLY = 15000000;

    protected $room;

    protected function setUp() : void
    {
        parent::setUp();

        $this->room = factory(Room::class)->create(
            [
                'song_id' => 0,
                'owner_phone' => '08123456789'
            ]
        );

        $this->mockAlternatively('overload:App\Libraries\ElasticsearchApi');
    }

    protected function tearDown() : void
    {
        Mockery::close();

        // To avoid "General error: 1205 Lock wait timeout exceeded" error
        $config = app('config');
        parent::tearDown();
        app()->instance('config', $config);
    }

    public function testGenerateSongId()
    {
        $this->room->generateSongId();

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'song_id' => $this->room->song_id
            ]
        );
    }

    public function testGenerateSongIdWithExistingData()
    {
        $this->room->song_id = 123456789;
        $this->room->save();

        $this->room->generateSongId();

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'song_id' => 123456789
            ]
        );
    }

    public function testGenerateLocationId()
    {
        $this->room->generateLocationId();

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'location_id' => $this->room->location_id
            ]
        );
    }

    public function testSetRoomPrice()
    {
        $priceData = [
            'price_daily' => 200000,
            'price_weekly' => 1000000,
            'price_monthly' => 2500000,
            'price_yearly' => 20000000
        ];

        $response = $this->room->setRoomPrice($priceData);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'price_daily' => 200000,
                'price_weekly' => 1000000,
                'price_monthly' => 2500000,
                'price_yearly' => 20000000
            ]
        );

        $this->assertInstanceOf(Room::class, $response);
    }

    public function testSetRoomPriceWithExtraPrices()
    {
        $priceData = [
            'price_daily' => 200000,
            'price_weekly' => 1000000,
            'price_monthly' => 2500000,
            'price_yearly' => 20000000,
            'price_3_month' => 7000000,
            'price_6_month' => 10000000
        ];

        $response = $this->room->setRoomPrice($priceData);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'price_daily' => 200000,
                'price_weekly' => 1000000,
                'price_monthly' => 2500000,
                'price_yearly' => 20000000,
                'price_quarterly' => 7000000,
                'price_semiannually' => 10000000
            ]
        );

        $this->assertInstanceOf(Room::class, $response);
    }

    public function testSetRoomPriceWithUsdCurrency()
    {
        $priceData = [
            'price_daily' => 200000,
            'price_weekly' => 1000000,
            'price_monthly' => 2500000,
            'price_yearly' => 20000000,
            'price_daily_usd' => 100,
            'price_weekly_usd' => 500,
            'price_monthly_usd' => 1500,
            'price_yearly_usd' => 15000
        ];

        $response = $this->room->setRoomPrice($priceData);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'price_daily' => 200000,
                'price_weekly' => 1000000,
                'price_monthly' => 2500000,
                'price_yearly' => 20000000,
                'price_daily_usd' => 100,
                'price_weekly_usd' => 500,
                'price_monthly_usd' => 1500,
                'price_yearly_usd' => 15000
            ]
        );

        $this->assertInstanceOf(Room::class, $response);
    }

    public function testSetRoomPriceWithNonNumericPrices()
    {
        $priceData = [
            'price_daily' => 'ABC',
            'price_weekly' => 'DEF',
            'price_monthly' => 'GHI',
            'price_yearly' => 'JKL'
        ];

        $response = $this->room->setRoomPrice($priceData);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'price_daily' => 0,
                'price_weekly' => 0,
                'price_monthly' => 0,
                'price_yearly' => 0
            ]
        );

        $this->assertInstanceOf(Room::class, $response);
    }

    public function testSetRoomPriceWithNonNumericPricesAndUsdCurrency()
    {
        $priceData = [
            'price_daily' => 'ABC',
            'price_weekly' => 'DEF',
            'price_monthly' => 'GHI',
            'price_yearly' => 'JKL',
            'price_daily_usd' => 'MNO',
            'price_weekly_usd' => 'PQR',
            'price_monthly_usd' => 'STU',
            'price_yearly_usd' => 'VWX'
        ];

        $response = $this->room->setRoomPrice($priceData);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'price_daily' => 0,
                'price_weekly' => 0,
                'price_monthly' => 0,
                'price_yearly' => 0,
                'price_daily_usd' => 0,
                'price_weekly_usd' => 0,
                'price_monthly_usd' => 0,
                'price_yearly_usd' => 0
            ]
        );

        $this->assertInstanceOf(Room::class, $response);
    }

    public function testGenerateBreadcrumb()
    {
        $response = $this->room->generateBreadcrumb();

        $this->assertTrue(strpos($response[0]['name'], 'Home') !== false);
        $this->assertTrue(strpos($response[1]['url'], '/room/') !== false);
        $this->assertEquals($this->room->name, $response[1]['name']);
    }

    public function testTogglePhoneActive()
    {
        $this->room->expired_phone = 0;
        $this->room->togglePhoneActive();
        $this->assertEquals(1, $this->room->expired_phone);

        $this->room->expired_phone = 1;
        $this->room->togglePhoneActive();
        $this->assertEquals(0, $this->room->expired_phone);
    }

    public function testSendSmsRegistration()
    {
        $mock = Mockery::mock('overload:\App\Libraries\SMSLibrary');
        $mock->shouldReceive('send')
            ->once()
            ->andReturnTrue();

        App::instance(SMSLibrary::class, $mock);

        $response = $this->room->sendSmsRegistration();

        $this->assertTrue($response);
    }

    public function testGenerateNameSlugUsingKosDataWithAreaCity()
    {
        $kos = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => null,
                    'name' => 'Mamirooms 123',
                    'gender' => 0,
                    'area_city' => 'Kabupaten Sleman',
                    'price_monthly' => 2500000
                ]
            );

        $response = $kos->generateNameSlug();
        $expectedName = preg_replace("/[^a-zA-Z0-9\s]/", "", $kos->name);

        $this->assertEquals(
            ucwords('Kost ' . $kos->area_city . ' Kost Campur Eksklusif ' . $expectedName),
            $response
        );
    }

    public function testGenerateNameSlugUsingKosDataWithoutAreaCity()
    {
        $kos = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => null,
                    'gender' => 0,
                    'area_city' => null,
                    'area_big' => 'Jakarta',
                    'price_monthly' => 2500000
                ]
            );

        $response = $kos->generateNameSlug();
        $expectedName = preg_replace("/[^a-zA-Z0-9\s]/", "", $kos->name);

        $this->assertEquals(
            ucwords('Kost ' . $kos->area_big . ' Kost Campur Eksklusif ' . $expectedName),
            $response
        );
    }

    /**
     * @group PMS-495
     */
    public function testGenerateNameSlugUsingKosDataWithOutGender()
    {
        $kos = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => null,
                    'name' => 'Mamirooms 123',
                    'gender' => null,
                    'area_city' => 'Kabupaten Sleman',
                    'price_monthly' => 2500000
                ]
            );

        $response = $kos->generateNameSlug();
        $expectedName = preg_replace("/[^a-zA-Z0-9\s]/", "", $kos->name);

        $this->assertEquals(
            ucwords('Kost ' . $kos->area_city . ' Kost Campur Eksklusif ' . $expectedName),
            $response
        );
    }

    public function testGenerateNameSlugUsingApartmentData()
    {
        $apartment = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => mt_rand(111, 999),
                    'name' => 'Mamirooms View',
                    'unit_type' => '1-Room Studio'
                ]
            );

        $response = $apartment->generateNameSlug();
        $expectedName = preg_replace("/[^a-zA-Z0-9\s]/", "", $apartment->name);
        $expectedUnitType = preg_replace("/[^a-zA-Z0-9\s]/", "", $apartment->unit_type);

        $this->assertEquals(
            ucwords($expectedName . ' ' . $expectedUnitType),
            $response
        );
    }

    public function testGenerateUniqueCode()
    {
        // Faking user login
        $user = factory(User::class)->state('admin')->create();
        $this->be($user);

        $this->room->generateUniqueCode();

        $this->assertDatabaseHas(
            'designer_unique_code',
            [
                'designer_id' => $this->room->id,
                'trigger' => $user->role . ' :: ' . $user->name
            ]
        );
    }

    public function testRenewLastUpdate()
    {
        $this->room->renewLastUpdate();

        $room = Room::first();
        $updatedDate = Carbon::parse($room->kost_updated_date);

        $this->assertTrue(Carbon::now()->gte($updatedDate));
    }

    public function testNormalizeCity()
    {
        $this->mock->shouldReceive('search')
            ->once()
            ->andReturn(
                [
                    'data' => 'success',
                    'hits' => [
                        'hits' => []
                    ]
                ]
            );

        $response = $this->room->normalizeCity();

        $this->assertStringContainsString('SUCCESS', $response);
    }

    public function testGenerateCityCodeWithTransformableAreaCity()
    {
        $room = factory(Room::class)->create(
            [
                'area_city' => 'Kota Surabaya'
            ]
        );

        $response = $room->generateCityCode();

        $this->assertEquals('surabaya1', $response);
        $this->assertDatabaseHas(
            'designer',
            [
                'city_code' => 'surabaya1'
            ]
        );
    }

    public function testGenerateCityCodeWithTransformableAreaCityAndExistingData()
    {
        // Generate existing rooms
        $rows = mt_rand(10, 20);
        factory(Room::class, $rows)
            ->create(
                [
                    'area_city' => 'Kota Surabaya'
                ]
            )
            ->each(
                function ($room) {
                    $room->generateCityCode();
                }
            );

        $this->assertDatabaseHas(
            'designer',
            [
                'city_code' => 'surabaya' . $rows
            ]
        );
    }

    public function testGenerateCityCodeWithNonTransformableAreaCity()
    {
        $room = factory(Room::class)->create(
            [
                'area_city' => 'Jayapura City'
            ]
        );

        $response = $room->generateCityCode();

        $this->assertEquals('jayapura1', $response);
        $this->assertDatabaseHas(
            'designer',
            [
                'city_code' => 'jayapura1'
            ]
        );
    }

    public function testSetAreaBigAttributeWithUnmatchedAreaCity()
    {
        $this->room->area_city = $this->faker->city;
        $this->room->save();

        $expectedCity = $this->faker->city;
        $this->room->setAreaBigAttribute($expectedCity);
        $this->assertEquals($expectedCity, $this->room->area_big);

        $this->room->setAreaBigAttribute('');
        $this->assertEmpty($this->room->area_big);
    }

    public function testSetAreaBigAttributeWithMatchedAreaButEmptyBigAreaMapper()
    {
        $this->room->area_city = $this->faker->city;
        $this->room->save();

        $this->room->setAreaBigAttribute($this->room->area_city);

        $this->assertNull($this->room->area_big);
    }

    public function testSetAreaBigAttributeWithMatchedAreaCityReturnAreaBig()
    {
        $area = factory(AreaBigMapper::class)->create(
            [
                'area_city' => $this->faker->city,
                'area_big' => $this->faker->citySuffix,
                'province' => $this->faker->state
            ]
        );

        $this->room->area_city = $area->area_city;
        $this->room->save();

        $this->room->setAreaBigAttribute($this->room->area_city);

        $this->assertEquals($area->area_big, $this->room->area_big);
    }

    public function testSetAreaBigAttributeWithMatchedAreaCityReturnProvince()
    {
        $area = factory(AreaBigMapper::class)->create(
            [
                'area_city' => $this->faker->city,
                'area_big' => '',
                'province' => $this->faker->state
            ]
        );

        $this->room->area_city = $area->area_city;
        $this->room->save();

        $this->room->setAreaBigAttribute($this->room->area_city);

        $this->assertEquals($area->province, $this->room->area_big);
    }

    public function testSetAreaBigAttributeWithMatchedAreaCityReturnNull()
    {
        $area = factory(AreaBigMapper::class)->create(
            [
                'area_city' => $this->faker->city,
                'area_big' => '',
                'province' => ''
            ]
        );

        $this->room->area_city = $area->area_city;
        $this->room->save();

        $this->room->setAreaBigAttribute($this->room->area_city);

        $this->assertNull($this->room->area_big);
    }

    public function testGenerateApartmentUnitCodeWithSingleUnit()
    {
        $project = factory(ApartmentProject::class)->create();
        $unit = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => $project->id
                ]
            );

        $unit->generateApartmentUnitCode();

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $unit->id,
                'apartment_project_id' => $project->id,
                'code' => $project->project_code . '-001'
            ]
        );
    }

    public function testGenerateApartmentUnitCodeWithManyUnits()
    {
        $project = factory(ApartmentProject::class)->create();

        factory(Room::class, 5)
            ->create(
                [
                    'apartment_project_id' => $project->id
                ]
            )
            ->each(
                function ($unit) {
                    $unit->generateApartmentUnitCode();
                }
            );

        $unit = factory(Room::class)
            ->create(
                [
                    'apartment_project_id' => $project->id
                ]
            );

        $unit->generateApartmentUnitCode();

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $unit->id,
                'apartment_project_id' => $project->id,
                'code' => $project->project_code . '-006'
            ]
        );
    }

    public function testGenerateApartmentUnitCodeWithNoApartmentProject()
    {
        $unit = factory(Room::class)->create();

        $unit->generateApartmentUnitCode();

        $this->assertNull($unit->code);
    }

    public function testGetCityNameSuggestionWithAddress()
    {
        $this->room->address = $this->faker->address;
        $this->room->save();

        $this->mock->shouldReceive('search')
            ->once()
            ->andReturn(
                [
                    'data' => 'success',
                    'hits' => [
                        'hits' => []
                    ]
                ]
            );

        $response = $this->room->getCityNameSuggestion();

        $this->assertNotNull($response);
        $this->assertArrayHasKey('address', $response);
        $this->assertArrayHasKey('response', $response);
    }

    public function testGetCityNameSuggestionWithoutAddress()
    {
        $this->room->address = null;
        $this->room->save();

        $this->assertNull($this->room->getCityNameSuggestion());
    }

    public function testSetAvailableCountWithEmptyNumber()
    {
        $this->room->setAvailableCount(null);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'status' => 2,
                'room_available' => 0
            ]
        );
    }

    public function testSetAvailableCountWithValidNumber()
    {
        $number = mt_rand(1, 10);
        $this->room->setAvailableCount($number);

        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $this->room->id,
                'status' => 0,
                'room_available' => $number
            ]
        );
    }

    public function testGetIsFlashSale()
    {
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData('2');

        $this->assertTrue($this->room->getIsFlashSale());
        $this->assertTrue($this->room->getIsFlashSale('all'));
        $this->assertFalse($this->room->getIsFlashSale('2'));
        $this->assertTrue($this->room->getIsFlashSale('4'));
    }

    public function testGetIsFlashSaleWithoutRunningFlashSale()
    {
        $this->setRoomWithDiscounts();

        $this->assertFalse($this->room->getIsFlashSale());
        $this->assertFalse($this->room->getIsFlashSale('all'));
        $this->assertFalse($this->room->getIsFlashSale('2'));
        $this->assertFalse($this->room->getIsFlashSale('4'));
    }

    public function testGetIsFlashSaleWithoutMatchingFlashSaleArea()
    {
        $this->setRoomWithDiscounts();

        $this->room->latitude = -9.020135563600139;
        $this->room->longitude = 111.3350067138672;
        $this->room->save();

        $this->setRunningFlashSaleData('2');

        $this->assertFalse($this->room->getIsFlashSale());
        $this->assertFalse($this->room->getIsFlashSale('all'));
        $this->assertFalse($this->room->getIsFlashSale('2'));
        $this->assertFalse($this->room->getIsFlashSale('4'));
    }

    public function testGetFlashSaleRentType()
    {
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData('2');

        $response = $this->room->getFlashSaleRentType();
        $this->assertTrue($response['quarterly']);
    }

    public function testGetFlashSaleRentTypeWithoutRunningFlashSale()
    {
        $this->setRoomWithDiscounts();

        $response = $this->room->getFlashSaleRentType();
        $this->assertFalse($response['quarterly']);
    }

    public function testGetFlashSaleRentTypeWithoutMatchingFlashSaleArea()
    {
        $this->setRoomWithDiscounts();
        $this->room->latitude = -9.020135563600139;
        $this->room->longitude = 111.3350067138672;
        $this->room->save();

        $this->setRunningFlashSaleData('2');

        $response = $this->room->getFlashSaleRentType();
        $this->assertFalse($response['quarterly']);
    }

    public function testGetFlashSaleRentTypeWithoutRoomDiscount()
    {
        $this->setRunningFlashSaleData('2');

        $response = $this->room->getFlashSaleRentType();
        $this->assertFalse($response['quarterly']);
    }

    public function testGenerateGeolocation()
    {
        $this->setGeolocation();
        $this->room->generateGeolocation();

        $room = Room::with('geolocation')->first();

        $this->assertEquals(
            new Point(self::ROOM_LATITUDE, self::ROOM_LONGITUDE),
            $room->geolocation->geolocation
        );
    }

    public function testRemoveGeolocation()
    {
        $this->setGeolocation();
        $this->room->generateGeolocation();
        $this->room->removeGeolocation();

        $room = Room::with('geolocation')->first();

        $this->assertNull($room->geolocation);
    }

    /*
     * Private methods
     */

    private function setRoomWithDiscounts($roomCount = 1): void
    {
        // Geolocation
        $this->setGeolocation();

        // Price elements
        $this->room->price_daily = self::PRICE_DAILY;
        $this->room->price_weekly = self::PRICE_WEEKLY;
        $this->room->price_monthly = self::PRICE_MONTHLY;
        $this->room->price_yearly = self::PRICE_ANNUALLY;

        // Room status
        $this->room->is_active = 1;
        $this->room->is_booking = 1;

        $this->room->save();

        // Quarterly discount
        $this->room->discounts()->save(
            factory(BookingDiscount::class)
                ->state('quarterly')
                ->make(
                    [
                        'price' => 2500000,
                        'markup_type' => 'percentage',
                        'markup_value' => 20,
                        'discount_type' => 'percentage',
                        'discount_value' => 10,
                        'is_active' => 1
                    ]
                )
        );
    }

    private function setGeolocation():void
    {
        $this->room->latitude = self::ROOM_LATITUDE;
        $this->room->longitude = self::ROOM_LONGITUDE;

        $this->room->save();
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }
}
