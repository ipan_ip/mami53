<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;

class ThanosHiddenTest extends MamiKosTestCase
{
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRelationToRoom()
    {
        $roomEntity = factory(Room::class)->create();
        $hiddenRoom = factory(ThanosHidden::class)->create(['designer_id' => $roomEntity->id]);

        $this->assertSame($roomEntity->id, $hiddenRoom->designer_id);
    }

    public function testIsHiddenAlias()
    {
        $hiddenRoom = factory(ThanosHidden::class)->make(['snapped' => true]);
        $this->assertTrue($hiddenRoom->isHidden());
    }

    public function testRoomIsHiddenByThanos()
    {
        $roomEntity = factory(Room::class)->create();
        $hiddenRoom = factory(ThanosHidden::class)->create(['designer_id' => $roomEntity->id]);

        $roomEntity->load('hidden_by_thanos');

        $this->assertSame($hiddenRoom->id, $roomEntity->hidden_by_thanos->id);
    }
    
    public function testRoomHaveLotsOfThanosExes()
    {
        $historyAmmount = rand(2, 5);
        $roomEntity = factory(Room::class)->create();
        factory(ThanosHidden::class, $historyAmmount)->create(['designer_id' => $roomEntity->id, 'snapped' => false]);

        $roomEntity->load('thanos_history');

        $this->assertCount($historyAmmount, $roomEntity->thanos_history);
    }

}
