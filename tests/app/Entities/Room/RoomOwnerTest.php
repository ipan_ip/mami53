<?php

namespace App\Entities\Room;

use App\Entities\Booking\BookingOwner;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Media\Media;
use App\Entities\Owner\EditStatus;
use App\Entities\Owner\StatusHistory;
use App\Entities\Premium\AdHistory;
use App\Entities\Premium\ClickPricing;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;

class RoomOwnerTest extends MamiKosTestCase
{
    use WithFaker;

    public function testEntityRelation()
    {
        [$room, $user, $roomOwner,$premiumAdHistory,$mamipayContractKost] = $this->createEntitiesSet();
        $roomRetrieve = $roomOwner->room()->first();
        $userRetrieve = $roomOwner->user()->first();
        $premiumAdHistoryRetrieve = $roomOwner->premium_ad_history()->first();
        $mamipayContractKostRetrieve = $roomOwner->mamipay_contract_kost()->first();

        $this->assertSame($room->name, $roomRetrieve->name, "Same Room");
        $this->assertSame($user->name, $userRetrieve->name, "Same User");
        $this->assertEquals($premiumAdHistoryRetrieve->id,$premiumAdHistory->id);
        $this->assertEquals($mamipayContractKostRetrieve->id,$mamipayContractKost->id);

    }

    public function testCheckFunctionUserNullReturnFalse()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet();
        $this->assertFalse($roomOwner->check($room, null));
    }

    public function testCheckFunctionUserRelatedReturnTrue()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet();
        $this->assertTrue($roomOwner->check($room, $user));
    }

    public function testCheckFunctionUserUnRelatedReturnFalse()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet();
        $unrelatedUser = $this->createUserEntity();
        $this->assertFalse($roomOwner->check($room, $unrelatedUser));
    }

    public function testAddOwnerWithoutRoomOwnerSuccessfullyCreateUnverifiedRoomOwner()
    {
        $room = $this->createRoomEntity();
        $user = $this->createUserEntity();

        $result = RoomOwner::add(
            [
                'id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => 'Pemilik'
            ]
        );

        $roomOwner = RoomOwner::where('user_id', $user->id)->first();

        $this->assertSame($roomOwner->status, 'unverified');
    }

    public function testAddOwnerOnNonExistRoomKosFailed()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet();

        $result = $roomOwner->add(
            [
                'id' => 89898989,
                'user_id' => $user->id,
                'owner_status' => $roomOwner->owner_status
            ]
        );
        $this->assertSame($result, "Kamar yang di klaim tidak tersedia");
    }

    public function testAddOwnerOnClaimedKosFailed()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet();

        $result = $roomOwner->add(
            [
                'id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => $roomOwner->owner_status
            ]
        );
        $this->assertSame($result, "Kost ini telah diklaim dan terverifikasi");
    }

    public function testAddOwnerOnKosPreviouslyClaimedFailed()
    {
        [$room, $user, $roomOwner] = $this->createEntitiesSet([], [], ['status' => RoomOwner::ROOM_ADD_STATUS]);

        $result = $roomOwner->add(
            [
                'id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => $roomOwner->owner_status
            ]
        );
        $this->assertSame($result, "Klaim anda telah ada sebelumnya");
    }

    public function testAddOwnerOnKosSuccess()
    {
        $room = $this->createRoomEntity();
        $user = $this->createUserEntity();

        $result = RoomOwner::add(
            [
                'id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => 'Pemilik'
            ]
        );
        $this->assertSame($result, "Klaim disimpan");

        $roomOwner = RoomOwner::where('user_id', $user->id)->first();

        $this->assertNotNull($roomOwner, 'RoomOwner Exist');
        $this->assertSame('Pemilik Kos', $roomOwner->owner_status, 'Pemilik changed to Pemilik Kos');
    }

    public function testCheckAvailSaldoEmptyPromote()
    {
        $premiumRequest = factory(PremiumRequest::class)->make();
        $premiumRequest->view_promote = [];
        $this->assertTrue(RoomOwner::checkAvailSaldo($premiumRequest, 100));
    }

    public function testCheckAvailSaldoHavePromoteNotEnoughSaldo()
    {
        $premiumRequest = factory(PremiumRequest::class)->make([
            'view' => 1000
        ]);
        $premiumRequest->view_promote = [
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
        ];
        $this->assertTrue(RoomOwner::checkAvailSaldo($premiumRequest, 100));
    }

    public function testCheckAvailSaldoHavePromoteEnoughSaldo()
    {
        $premiumRequest = factory(PremiumRequest::class)->make([
            'view' => 1000
        ]);
        $premiumRequest->view_promote = [
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
        ];
        $this->assertFalse(RoomOwner::checkAvailSaldo($premiumRequest, 50000));
    }

    public function testOwnerListRoomNoRoomOwned()
    {
        $user = $this->createUserEntity();

        $result = RoomOwner::ownerListRoom([], $user);

        $this->assertArrayHasKey('room', $result, 'has room key result');
        $this->assertEmpty($result['room'], 'has no room child');
    }

    public function testOwnerListRoomNotExpiredPremium()
    {
        $user = $this->createUserEntity([
            'date_owner_limit' => Carbon::now()->addDay(1)
        ]);
        $rooms = factory(Room::class, rand(1, 3))->create([
            'expired_phone' => 0
        ]);
        $rooms->each(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
        });

        $premiumRequest = factory(PremiumRequest::class)->make([
            'view' => 10000,
            'user_id' => $user->id,
            'status' => '1'
        ]);
        $premiumRequest->view_promote = [
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 1,
                'total' => 100
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
            (object) [
                'is_active' => 0,
                'total' => 100,
                'history_promote' => [
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ],
                    (object) [
                        'used_view' => 100
                    ]
                ]
            ],
        ];

        $result = RoomOwner::ownerListRoom([], $user);

        $this->assertArrayHasKey('room', $result, 'has room key result');
        $this->assertCount(count($rooms), $result['room'], 'Room count match');
    }

    public function testOwnerListRoomNoPremium()
    {
        $user = $this->createUserEntity();
        $rooms = factory(Room::class, rand(5, 9))->create([
            'expired_phone' => 0
        ]);
        $rooms->each(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
        });

        $result = RoomOwner::ownerListRoom(
            [
                'limit' => 4,
                'offset' => 0
            ],
            $user
        );

        $this->assertArrayHasKey('room', $result, 'has room key result');
        $this->assertCount(4, $result['room'], 'Room count match');
    }

    public function testOwnerListRoomPremium()
    {
        $now = now();
        $user = $this->createUserEntity(['date_owner_limit'=> date('Y-m-d',strtotime($now. ' + 1 weeks'))]);
        $photo = factory(Media::class) ->create();
        
        $rooms = factory(Room::class,5)->create([
            'expired_phone' => 0,
            // 'incomplete_room' => true,
            'photo_id'=> $photo->id,
        ]);

        foreach($rooms as $room){            
            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => 'draft2'
            ]);
        }

        $rooms[0]->is_active = 'true';
        $rooms[0]->save();

        $rooms[1]->is_active = 'false';
        $rooms[1]->save();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
        ]);

        $viewPromote1 = factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest->id,
            'designer_id'=> $rooms[4]->id,
            'is_active' => true,
            'history' => 1
        ]);

        $viewPromote2= factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest->id,
            'designer_id'=> $rooms[1]->id,
            'is_active' => true,
            'history' => 90,
            'total'=> 100
        ]);

        $viewPromote3= factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest->id,
            'designer_id'=> $rooms[2]->id,
            'is_active' => false,
            'history' => 1
        ]);

        $viewPromote4= factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest->id,
            'designer_id'=> $rooms[3]->id,
            'is_active' => false,
            'history' => 0
        ]);

        $viewPromote4= factory(ViewPromote::class)->create([
            "premium_request_id" => $premiumRequest->id,
            'designer_id'=> $rooms[1]->id,
            'is_active' => true,
            'history' => null
        ]);

        $result = RoomOwner::ownerListRoom(
            [
                'limit' => 4,
                'offset' => 0,
                'v' => true            ],
            $user
        );
        $this->assertArrayHasKey('room', $result, 'has room key result');
        $this->assertCount(4, $result['room'], 'Room count match');
    }

    public function testSuggestedRoomOwnerEmptyOnNonExistUser()
    {
        $result = RoomOwner::suggestedRoomOwner(8989898989);
        $this->assertEmpty($result);
    }

    public function testSuggestedRoomOwnerEmptyOnNullPhoneNumber()
    {
        $user = $this->createUserEntity(['phone_number' => null]);
        $result = RoomOwner::suggestedRoomOwner($user->id);
        $this->assertEmpty($result);
    }

    public function testSuggestedRoomOwnerNoneSuggestedSuccess()
    {
        $user = $this->createUserEntity();

        $roomsWithOwnerPhone = factory(Room::class, rand(3, 9))->create([
            'owner_phone' => $user->phone_number,
            'expired_phone' => 0
        ]);
        $connectedRoomWithOwnerPhone = $roomsWithOwnerPhone->map(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
            return $room;
        });

        $roomsWithManagerPhone = factory(Room::class, rand(3, 9))->create([
            'manager_phone' => $user->phone_number,
            'expired_phone' => 0
        ]);
        $connectedRoomWithManagerPhone = $roomsWithManagerPhone->map(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
            return $room;
        });

        $allRoomsWithPhone = $roomsWithManagerPhone->merge($roomsWithOwnerPhone);
        $allConnectedRoom = $connectedRoomWithManagerPhone->merge($connectedRoomWithOwnerPhone);

        $allRoomId = $allRoomsWithPhone->pluck('song_id')->toArray();
        $allConnectedRoomId = $allConnectedRoom->pluck('song_id')->toArray();

        $allIdNotConnectedRoom = array_diff($allRoomId, $allConnectedRoomId);

        $notConnectedRoomCount = count($allIdNotConnectedRoom);

        $result = RoomOwner::suggestedRoomOwner($user->id);
        $this->assertCount($notConnectedRoomCount, $result, "Counted $notConnectedRoomCount excluding the linked room to owner");

        $shouldBeEmptyDiff = array_diff($allIdNotConnectedRoom, $result);
        $this->assertEmpty($shouldBeEmptyDiff);
    }

    public function testSuggestedRoomOwnerWithDifferencesSuccess()
    {
        $user = $this->createUserEntity();

        $roomsWithOwnerPhone = factory(Room::class, rand(3, 9))->create([
            'owner_phone' => $user->phone_number,
            'expired_phone' => 0
        ]);
        $connectedRoomWithOwnerPhone = $roomsWithOwnerPhone->nth(2, 1)->map(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
            return $room;
        });

        $roomsWithManagerPhone = factory(Room::class, rand(3, 9))->create([
            'manager_phone' => $user->phone_number,
            'expired_phone' => 0
        ]);
        $connectedRoomWithManagerPhone = $roomsWithManagerPhone->nth(2, 1)->map(function ($room) use ($user) {
            $this->createRoomOwnerEntity($room, $user);
            return $room;
        });

        $allRoomsWithPhone = $roomsWithManagerPhone->merge($roomsWithOwnerPhone);
        $allConnectedRoom = $connectedRoomWithManagerPhone->merge($connectedRoomWithOwnerPhone);

        $allRoomId = $allRoomsWithPhone->pluck('song_id')->toArray();
        $allConnectedRoomId = $allConnectedRoom->pluck('song_id')->toArray();

        $allIdNotConnectedRoom = array_diff($allRoomId, $allConnectedRoomId);

        $notConnectedRoomCount = count($allIdNotConnectedRoom);

        $result = RoomOwner::suggestedRoomOwner($user->id);
        $this->assertCount($notConnectedRoomCount, $result, "Counted $notConnectedRoomCount excluding the linked room to owner");

        $shouldBeEmptyDiff = array_diff($allIdNotConnectedRoom, $result);
        $this->assertEmpty($shouldBeEmptyDiff);
    }

    public function testStoreFunction()
    {
        $room = $this->createRoomEntity();
        $user = $this->createUserEntity();

        $this->assertTrue(
            RoomOwner::store([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'owner_status' => 'Pemilik Kos',
                'status' => 'verified'
            ])
        );

        $secondRoom = $this->createRoomEntity();

        $this->assertTrue(
            RoomOwner::store([
                'designer_id' => $secondRoom->id,
                'user_id' => $user->id,
                'status' => 'verified'
            ])
        );

        $roomOwner = RoomOwner::where('designer_id', $secondRoom->id)->first();
        $this->assertNotNull($roomOwner, 'Room Owner Exist');
        $this->assertNotNull($roomOwner->owner_status, 'Owner Status set even when ommited');
    }

    public function testBookingOwner(): void
    {
        $owner = factory(RoomOwner::class)->create();
        $expected = factory(BookingOwner::class)->create(['owner_id' => $owner->user_id]);

        $this->assertTrue(
            $owner->booking_owner->is($expected)
        );
    }

    public function testcheckscopeOwnerTypeOnly() : void
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'owner_status' => RoomOwner::OWNER_TYPES[0]
        ]);
        $result = $this->roomOwner->ownerTypeOnly()->first();
        $this->assertEquals($result->id,$roomOwner->id);
    }

    protected function setUp() : void
    {
        parent::setUp();

        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CHAT)->make()->toArray()
        );

        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CLICK)->make()->toArray()
        );

        $this->roomOwner = new RoomOwner;
    }

    private function createRoomEntity(array $overrideData = []): Room
    {
        return factory(Room::class)->create($overrideData);
    }

    private function createUserEntity(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }
    private function createRoomOwnerEntity(
        Room $roomEntity,
        User $userEntity,
        array $overrideData = []
    ): RoomOwner {
        return factory(RoomOwner::class)->state('owner')->create(
            array_merge(
                [
                    'designer_id' => $roomEntity->id,
                    'user_id' => $userEntity->id
                ],
                $overrideData
            )
        );
    }

    private function createEntitiesSet(
        array $roomData = [],
        array $userData = [],
        array $roomOwnerData = []
    ): array {
        $roomEntity = $this->createRoomEntity($roomData);
        $userEntity = $this->createUserEntity($userData);
        $roomOwnerEntity = $this->createRoomOwnerEntity(
            $roomEntity,
            $userEntity,
            $roomOwnerData
        );
        $premiumAdHistoryEntity = factory(AdHistory::class)->create(['designer_id'=> $roomEntity->id]);
        $mamipayContractKost = factory(MamipayContractKost::class)->create(['designer_id'=> $roomEntity->id]);
        return [
            $roomEntity,
            $userEntity,
            $roomOwnerEntity,
            $premiumAdHistoryEntity,
            $mamipayContractKost
        ];
    }

    public function testStatusHistory(): void
    {
        $owner = factory(RoomOwner::class)->create(['status' => $this->faker->randomElement(EditStatus::getValues())]);
        $expected = factory(StatusHistory::class)->create(['designer_owner_id' => $owner->id, 'new_status' => $owner->status]);

        $this->assertTrue(
            $owner->status_histories()->first()->is($expected)
        );
    }
}
