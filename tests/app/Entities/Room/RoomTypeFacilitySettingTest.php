<?php

namespace App\Entities\Room;

use App\Entities\Room\Element\Type;
use App\Entities\Room\RoomTypeFacilitySetting;
use App\Test\MamiKosTestCase;

class RoomTypeFacilitySettingTest extends MamiKosTestCase
{
    public function testRoomRelation()
    {
        $roomType = factory(Type::class)->create();
        $roomTypeFacilitySetting = factory(RoomTypeFacilitySetting::class)->create([
            'designer_type_id' => $roomType
        ]);

        $this->assertEquals($roomTypeFacilitySetting->room->id, $roomType->id);
    }
}
