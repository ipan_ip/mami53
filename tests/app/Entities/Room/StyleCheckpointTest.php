<?php

namespace App\Entities\Survey;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\StyleCheckpoint;
use App\Test\MamiKosTestCase;
use App\User;

class StyleCheckpointTest extends MamiKosTestCase
{
    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $styleCheckpoint = factory(StyleCheckpoint::class)->create([
            'designer_id' => $room->id
        ]);

        $result = $styleCheckpoint->room;
        $this->assertEquals($result->id, $room->id);
    }

    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $styleCheckpoint = factory(StyleCheckpoint::class)->create([
            'user_id' => $user->id
        ]);

        $result = $styleCheckpoint->user;
        $this->assertEquals($result->id, $user->id);
    }

    public function testPhotoRelation()
    {
        $photo = factory(Media::class)->create();
        $styleCheckpoint = factory(StyleCheckpoint::class)->create([
            'photo_id' => $photo->id
        ]);

        $result = $styleCheckpoint->photo;
        $this->assertEquals($result->id, $photo->id);
    }
}
