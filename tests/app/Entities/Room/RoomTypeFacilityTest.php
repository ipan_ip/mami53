<?php

namespace App\Entities\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Facility\FacilityType;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Type;
use App\Test\MamiKosTestCase;
use App\User;

class RoomTypeFacilityTest extends MamiKosTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->roomTypeFac = new RoomTypeFacility();
    }

    public function testTagRelation()
    {
        $tag = factory(Tag::class)->create();
        $roomTypeFacility = factory(RoomTypeFacility::class)->create([
            'tag_id' => $tag->id
        ]);

        $this->assertEquals($tag->id, $roomTypeFacility->tag->id);
    }

    public function testRoomTypeRelation()
    {
        $type = factory(Type::class)->create();
        $roomTypeFacility = factory(RoomTypeFacility::class)->create([
            'designer_type_id' => $type->id
        ]);

        $this->assertEquals($type->id, $roomTypeFacility->room_type->id);
    }

    public function testRoomPhotoRelation()
    {
        $photo = factory(Media::class)->create();
        $roomTypeFacility = factory(RoomTypeFacility::class)->create([
            'photo_id' => $photo->id
        ]);

        $this->assertEquals($photo->id, $roomTypeFacility->photo->id);
    }

    public function testRoomCreatorRelation()
    {
        $creator = factory(User::class)->create();
        $roomTypeFacility = factory(RoomTypeFacility::class)->create([
            'creator_id' => $creator->id
        ]);

        $this->assertEquals($creator->id, $roomTypeFacility->creator->id);
    }

    public function testGetIdFromKey()
    {
        $string = 'abcd123-123-asdbasd';

        $reflection = new \ReflectionClass(get_class($this->roomTypeFac));
        $method = $reflection->getMethod('getIdFromKey');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->roomTypeFac, [$string]);

        $this->assertIsInt($result);
        $this->assertEquals($result, 123);
    }

    public function testStoreIfPhotoDataIsEmpty()
    {
        $data = ['room_id' => rand(1, 100)];
        $result = RoomTypeFacility::store($data);

        $this->assertFalse($result);
    }

    public function testStore()
    {
        $data = [
            'room_id' => rand(1, 100),
            'test-001-test' => 1,
            'test-002-test' => 2,
            'test-003-test' => 3,
        ];
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $result = RoomTypeFacility::store($data);

        $this->assertTrue($result);

        $this->assertDatabaseHas('designer_type_facility', [
            'designer_type_id' => $data['room_id'],
            'tag_id' => 001,
            'creator_id' => $user->id,
            'photo_id' => 1,
        ]);

        $this->assertDatabaseHas('designer_type_facility', [
            'designer_type_id' => $data['room_id'],
            'tag_id' => 002,
            'creator_id' => $user->id,
            'photo_id' => 2,
        ]);

        $this->assertDatabaseHas('designer_type_facility', [
            'designer_type_id' => $data['room_id'],
            'tag_id' => 003,
            'creator_id' => $user->id,
            'photo_id' => 3,
        ]);
    }

    public function testGetFacilityStructure()
    {
        $faciltyCategory = factory(FacilityCategory::class)->create([
            'name' => RoomTypeFacility::CATEGORY_NAME
        ]);
        $faciltyType = factory(FacilityType::class)->create([
            'facility_category_id' => $faciltyCategory->id
        ]);
        $tags = factory(Tag::class, 5)->create([
            'facility_type_id' => $faciltyType->id
        ])->pluck('id')->toArray();

        $result = $this->roomTypeFac->getFacilityStructure($tags);
        $this->assertIsArray($result);
        $this->assertEquals($faciltyCategory->id, $result[0]['id']);
        $this->assertEquals($faciltyType->id, $result[0]['types'][0]['id']);
        $this->assertEquals($tags[0], $result[0]['types'][0]['tags'][0]['id']);
        $this->assertEquals($tags[1], $result[0]['types'][0]['tags'][1]['id']);
        $this->assertEquals($tags[2], $result[0]['types'][0]['tags'][2]['id']);
        $this->assertEquals($tags[3], $result[0]['types'][0]['tags'][3]['id']);
        $this->assertEquals($tags[4], $result[0]['types'][0]['tags'][4]['id']);
    }

    public function testGetCompiledFacilityStructureWithActiveTagsIsNotEmpty()
    {
        $roomTypeId = rand(1, 100);
        $faciltyCategory = factory(FacilityCategory::class)->create([
            'name' => RoomTypeFacility::CATEGORY_NAME,
        ]);
        $faciltyType = factory(FacilityType::class)->create([
            'facility_category_id' => $faciltyCategory->id
        ]);
        $tags = factory(Tag::class, 2)->create([
            'facility_type_id' => $faciltyType->id
        ])->pluck('id')->toArray();

        $roomTypeFacilitySetting = factory(RoomTypeFacilitySetting::class)->create([
            'designer_type_id' => $roomTypeId,
            'active_tags' => implode(',', $tags)
        ]);

        $photo = factory(Media::class)->create();
        $roomTypeFacility = factory(RoomTypeFacility::class)->create([
            'tag_id' => $tags[0],
            'photo_id' => $photo->id,
            'designer_type_id' => $roomTypeId
        ]);

        $result = $this->roomTypeFac->getCompiledFacilityStructure($roomTypeId);

        $this->assertIsArray($result);
        $this->assertEquals($faciltyCategory->id, $result[0]['id']);
        $this->assertEquals($faciltyType->id, $result[0]['types'][0]['id']);
        $this->assertEquals($tags[0], $result[0]['types'][0]['tags'][0]['id']);
        $this->assertEquals($tags[1], $result[0]['types'][0]['tags'][1]['id']);

        $this->assertEquals(1, $result[0]['types'][0]['tags'][0]['type_facilities_count']);
        $this->assertEquals($roomTypeFacility->id, $result[0]['types'][0]['tags'][0]['type_facilities'][0]['id']);
        $this->assertEquals($photo->id, $result[0]['types'][0]['tags'][0]['type_facilities'][0]['photo']['id']);

        $this->assertEquals(0, $result[0]['types'][0]['tags'][1]['type_facilities_count']);
        $this->assertEquals([], $result[0]['types'][0]['tags'][1]['type_facilities']);
        $this->assertEquals(null, $result[0]['types'][0]['tags'][1]['photo_id']);
    }
}
