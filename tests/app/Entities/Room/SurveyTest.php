<?php

namespace App\Entities\Survey;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Survey;
use App\Entities\User\Notification;
use App\User;
use App\Entities\Room\RoomOwner;

class SurveyTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $this->survey = new Survey();
    }

    public function testRelationshipToRoom()
    {
        $room = factory(Room::class)->create([]);
        $survey = factory(Survey::class)->create(['designer_id' => $room->id]);
        $this->assertEquals($room->id, $survey->room->id);
    }

    public function testRelationshipToUser()
    {
        $user = factory(User::class)->create([]);
        $survey = factory(Survey::class)->create(['user_id' => $user->id]);
        $this->assertEquals($user->id, $survey->user->id);
    }

    // public function testRelationshipToNotification()
    // {
    //     $survey = factory(Survey::class)->create([]);
    //     $notification = factory(Notification::class)->create([
    //         'identifier' => $survey->id,
    //         'type' => 'survey'
    //     ]);

    //     $this->assertEquals($notification->identifier, $survey->notification()->first()->identifier);
    // }

    public function testGetCountReport()
    {
        $room = factory(Room::class)->create([]);
        factory(Survey::class, 5)->create(['designer_id' => $room->id]);

        $result = Survey::GetCountReport($room->id, 'survey');

        $this->assertEquals($result, 5);
    }

    public function testgetIdUserOwner()
    {
        $user1 = factory(User::class)->create([]);
        $room1 = factory(Room::class)->create([]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room1->id,
            'user_id' => $user1->id
        ]);

        $user2 = factory(User::class)->create([]);
        $room2 = factory(Room::class)->create([]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room2->id,
            'user_id' => $user2->id
        ]);

        $room_id = [$room1->id, $room2->id];

        $result = $this->survey->getIdUserOwner($room_id);

        $this->assertEquals($result->count(), 2);
        $this->assertEquals($result[0]->user_id, $user1->id);
        $this->assertEquals($result[1]->user_id, $user2->id);
    }

    public function testCheckShouldReturnTrue()
    {
        $user = factory(User::class)->create([]);
        $room = factory(Room::class)->create([]);

        factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $result = $this->survey->check($room, $user);

        $this->assertTrue($result);
    }

    public function testCheckShouldReturnFalse()
    {
        $relatedUser = factory(User::class)->create([]);
        $ordinaryUser = factory(User::class)->create([]);
        $room = factory(Room::class)->create([]);

        factory(Survey::class)->create([
            'user_id' => $relatedUser->id,
            'designer_id' => $room->id
        ]);

        $this->assertFalse($this->survey->check($room, $ordinaryUser));
    }

    public function testSendNotificationShouldSuccess()
    {
        $currentDate = \Carbon\Carbon::now();
        $lastWeek = $currentDate->subWeek()->format('Y-m-d');

        $user = factory(User::class)->create([]);
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([]);

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id
        ]);

        $survey = factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'notification' => '0',
            'notification_time' => $lastWeek
        ])->toArray();

        $expectedResult = [
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'notification' => '0',
            'notification_time' => $lastWeek
        ];

        $this->assertDatabaseHas('survey', $expectedResult);

        $this->survey->sendNotification();

        $expectedResult['notification'] = 1;

        $this->assertDatabaseHas('survey', $expectedResult);
    }

    public function testAfterSurveyNotificationShouldReturnTrue()
    {
        $currentDate = \Carbon\Carbon::now();
        $lastWeek = $currentDate->subWeek()->format('Y-m-d');

        $room = factory(Room::class)->create([]);

        factory(Survey::class)->create([
            'designer_id' => $room->id,
            'notification_time' => $lastWeek,
            'time' => $lastWeek
        ])->toArray();

        $expectedResult = [
            'designer_id' => $room->id,
            'notification_time' => $lastWeek,
            'time' => $lastWeek
        ];

        $this->assertDatabaseHas('survey', $expectedResult);

        $this->assertTrue($this->survey->AfterSurveyNotification());
    }
}
