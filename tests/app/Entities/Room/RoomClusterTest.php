<?php

namespace App\Entities\Room;

use App\Entities\Booking\BookingDiscount;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class RoomClusterTest extends MamiKosTestCase
{
    
    const ROOM_LATITUDE = -8.014663865095645;
    const ROOM_LONGITUDE = 110.2936449049594;

    const PRICE_DAILY = 100000;
    const PRICE_WEEKLY = 500000;
    const PRICE_MONTHLY = 1500000;
    const PRICE_QUARTERLY = 4500000;
    const PRICE_SEMIANNUALLY = 7000000;
    const PRICE_ANNUALLY = 15000000;

    public function testGetDiscountedRoomTotal()
    {
        $filter = [
            "tag_ids" => [],
            "random_seeds" => 839,
            "property_type" => "apartment",
            "place" => null,
            "landings" => [],
            "location" => [
                106.76872640531816,
                -6.235163163393989
            ],
            [
                106.87687759214604,
                -6.1279556080436155
            ],
            "disable_sorting" => true,
            "rent_type" => '0'
        ];

        $roomCluster = new RoomCluster($filter);

        $this->assertEquals(0, $roomCluster->getDiscountedRoomTotal());
    }

    public function testGetDiscountedRoomTotalWithNoRentType()
    {
        $filter = [
            "tag_ids" => [],
            "random_seeds" => 839,
            "property_type" => "apartment",
            "place" => null,
            "landings" => [],
            "location" => [
                106.27492523193361,
                -6.235163163393989
            ],
            [
                106.3350067138672,
                -6.1279556080436155
            ],
            "disable_sorting" => true
        ];

        $roomCluster = new RoomCluster($filter);

        $this->assertEquals(0, $roomCluster->getDiscountedRoomTotal());
    }

    public function testGetDiscountedRoomTotalWithRunningFlashSaleData()
    {
        $rentTypeIndex = '2';

        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $filter = [
            "tag_ids" => [],
            "random_seeds" => 839,
            "property_type" => "all",
            "place" => null,
            "landings" => [],
            "location" => [
                [
                    110.27492523193361,
                    -8.020135563600139
                ],
                [
                    110.3350067138672,
                    -7.976957752572763
                ]
            ],
            "disable_sorting" => true,
            "rent_type" => $rentTypeIndex,
            "flash_sale" => true
        ];

        $roomCluster = new RoomCluster($filter);

        $this->assertEquals(1, $roomCluster->getDiscountedRoomTotal());
    }

    private function setRoomWithDiscounts(): void
    {
        factory(Room::class)
            ->create(
                [
                    "latitude" => self::ROOM_LATITUDE,
                    "longitude" => self::ROOM_LONGITUDE
                ]
            )
            ->each(
                function ($room) {
                    // Price elements
                    $room->price_daily = self::PRICE_DAILY;
                    $room->price_weekly = self::PRICE_WEEKLY;
                    $room->price_monthly = self::PRICE_MONTHLY;
                    $room->price_yearly = self::PRICE_ANNUALLY;
                    $room->price_quarterly = self::PRICE_QUARTERLY;
                    $room->price_semiannually = self::PRICE_SEMIANNUALLY;
                    $room->save();

                    // Monthly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->make(
                            [
                                'price' => 2500000,
                                'markup_type' => 'percentage',
                                'markup_value' => 20,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Daily discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('daily')->make(
                            [
                                'price' => 100000,
                                'markup_type' => 'percentage',
                                'markup_value' => 50,
                                'discount_type' => 'percentage',
                                'discount_value' => 30,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Weekly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('weekly')->make(
                            [
                                'price' => 750000,
                                'markup_type' => 'percentage',
                                'markup_value' => 25,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Annually discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('annually')->make(
                            [
                                'price' => 15000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 20,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Quarterly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('quarterly')->make(
                            [
                                'price' => 5000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'percentage',
                                'discount_value' => 5,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Semi-annually discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('semiannually')->make(
                            [
                                'price' => 10000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'percentage',
                                'discount_value' => 5,
                                'is_active' => 1
                            ]
                        )
                    );
                }
            );
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }
}
