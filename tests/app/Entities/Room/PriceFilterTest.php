<?php

namespace App\Entities\Room;

use App\Services\Room\PriceFilterService;
use App\Test\MamiKosTestCase;

class PriceFilterTest extends MamiKosTestCase
{
    public function testRelationRoom()
    {
        $room = factory(Room::class)->create();

        $priceFilter = factory(PriceFilter::class)->create(['designer_id' => $room->id]);

        $this->assertInstanceOf(Room::class, $priceFilter->room);
    }

    public function testRentTypeStringForDaily()
    {
        $response = PriceFilter::rentTypeString(PriceFilterService::DAILY_RENT_TYPE);
        $this->assertEquals(PriceFilter::DAILY_PRICE_FILTER, $response);
    }

    public function testRentTypeStringForMonthly()
    {
        $response = PriceFilter::rentTypeString(PriceFilterService::MONTHLY_RENT_TYPE);
        $this->assertEquals(PriceFilter::MONTHLY_PRICE_FILTER, $response);
    }
}