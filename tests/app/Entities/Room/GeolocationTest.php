<?php

namespace App\Entities\Survey;

use App\Entities\Room\Geolocation;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class GeolocationTest extends MamiKosTestCase
{
    public function testRelationRoom()
    {
        $room = factory(Room::class)->create();
        $geolocation = factory(Geolocation::class)->create([
            'designer_id' => $room->id
        ]);

        $this->assertEquals($room->id, $geolocation->room->id);
    }
}
