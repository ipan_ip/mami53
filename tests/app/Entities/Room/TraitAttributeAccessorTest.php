<?php

namespace App\Entities\Room;

use App\Entities\Activity\Call;
use App\Entities\Activity\Love;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Media\Media;
use App\Entities\Notif\SettingNotif;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\UniqueCode;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

class TraitAttributeAccessorTest extends MamiKosTestCase
{
    use WithoutEvents;
    use WithoutMiddleware;

    protected $room;
    protected $cards;
    protected $premiumCards;
    protected $user;
    protected $owner;

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)
            ->state('owner')
            ->create();

        $this->room = factory(Room::class)
            ->state('active')
            ->create(
                [
                    'owner_phone' => $this->user->phone_number,
                    'room_available' => 10
                ]
            );

        $this->cards = factory(Card::class, 5)
            ->create(
                [
                    'designer_id' => $this->room->id
                ]
            );

        $this->premiumCards = factory(CardPremium::class, 5)
            ->create(
                [
                    'designer_id' => $this->room->id
                ]
            );

        $this->owner = factory(RoomOwner::class)
            ->state('owner')
            ->create(
                [
                    'designer_id' => $this->room->id,
                    'user_id' => $this->user->id
                ]
            );

        factory(KostLevel::class)->create(
            [
                'name' => 'regular',
                'is_regular' => 1,
                'is_hidden' => 0
            ]
        );
    }

    protected function tearDown() : void
    {
        Mockery::close();

        parent::tearDown();
    }

    public function testGetAreaCityKeywordAttributeWithNullAreaCityString()
    {
        $this->room->area_city = null;
        $this->room->save();

        $this->assertEquals('', $this->room->area_city_keyword);
    }

    public function testGetAreaCityKeywordAttributeWithEmptyAreaCityString()
    {
        $this->room->area_city = '';
        $this->room->save();

        $this->assertEquals('', $this->room->area_city_keyword);
    }

    public function testGetAreaCityKeywordAttributeWithValidAreaCityString()
    {
        $this->room->area_city = "Kabupaten Sleman";
        $this->room->save();
        $this->assertEquals('Jogja', $this->room->area_city_keyword);

        $this->room->area_city = "Kota Bogor";
        $this->room->save();
        $this->assertEquals('Jakarta', $this->room->area_city_keyword);

        $this->room->area_city = "Kabupaten Sumedang";
        $this->room->save();
        $this->assertEquals('Bandung', $this->room->area_city_keyword);
    }

    public function testGetOwnerPhoneArrayAttribute()
    {
        $this->assertTrue(isset($this->room->owner_phone_array));
    }

    public function testGetOwnerPhoneNullOnDash()
    {
        $room = factory(Room::class)->make([
            'owner_phone' => '-'
        ]);
        $this->assertNull($room->getOwnerPhone());
    }

    public function testGetOwnerPhoneNullOnEmptyString()
    {
        $room = factory(Room::class)->make([
            'owner_phone' => ''
        ]);
        $this->assertNull($room->getOwnerPhone());
    }

    public function testGetListCardAttributeWithPremiumCards()
    {
        $this->assertIsArray($this->room->list_card);
    }

    public function testGetListCardAttributeWithRegularCards()
    {
        // Remove premium cards
        $cards = $this->room->premium_cards();
        foreach ($cards as $card) {
            $card->delete();
        }

        $this->assertIsArray($this->room->list_card);
    }

    public function testGetShareUrlAttributeWithKosData()
    {
        $this->assertTrue(strpos($this->room->share_url, '/room/') !== false);
    }

    public function testGetShareUrlAttributeWithApartmentData()
    {
        $apartmentProject = factory(ApartmentProject::class)->create();
        $this->room->apartment_project_id = $apartmentProject->id;

        $this->assertTrue(strpos($this->room->share_url, '/unit/') !== false);
    }

    public function testGetHasPhotoRoundReturnFalse()
    {
        $this->assertFalse($this->room->has_photo_round);
    }

    public function testGetHasPhotoRoundWithInvalidPhoto()
    {
        $this->room->photo_round_id = mt_rand(11111, 99999);
        $this->room->save();

        $this->assertFalse($this->room->has_photo_round);
    }

    public function testGetHasPhotoRoundWithValidPhoto()
    {
        $media = factory(Media::class)->create();

        $this->room->photo_round_id = $media->id;
        $this->room->save();

        $this->assertTrue($this->room->has_photo_round);
    }

    public function testGetTimeConnectAttribute()
    {
        Carbon::setTestNow(Carbon::parse('08:00:00'));
        $this->room->room_available = 1;
        $this->room->save();

        factory(SettingNotif::class)->state('for_user')->create(
            [
                'identifier' => $this->user->id
            ]
        );

        $this->assertFalse(isset($this->room->time_connect));
    }

    public function testGetBreadcrumbAttribute()
    {
        $this->assertTrue(isset($this->room->breadcrumb));
    }

    public function testGetDetailRatingAttribute()
    {
        $this->assertTrue(isset($this->room->detail_rating));
    }

    public function testGetStatusTitleAttribute()
    {
        $room = factory(Room::class)->make(
            [
                'status' => 0
            ]
        );
        $this->assertEquals('null', $room->status_title);

        $room = factory(Room::class)->make(
            [
                'status' => 1
            ]
        );
        $this->assertEquals('none', $room->status_title);

        $room = factory(Room::class)->make(
            [
                'status' => 2
            ]
        );
        $this->assertEquals('full', $room->status_title);

        $room = factory(Room::class)->make(
            [
                'status' => 5
            ]
        );
        $this->assertEquals('full', $room->status_title);
    }

    public function testGetStatusTitleAttributeWithoutStatusFilter()
    {
        $room = factory(Room::class)->make(
            [
                'status' => null
            ]
        );
        $this->assertEquals('null', $room->status_title);
    }

    public function testGetLoveCountReturnZero()
    {
        $this->assertEquals(0, $this->room->love_count);
    }

    public function testGetLoveCount()
    {
        factory(Room::class)->create()->each(function($room) {
            $room->promotion()->saveMany(factory(Love::class, 5)->make());
        });

        $this->assertEquals(5, $this->room->love_count);
    }

    public function testGetLocationAttribute()
    {
        $this->assertNotEmpty($this->room->location);
    }

    public function testGetLoveCountAttribute()
    {
        $this->assertTrue(isset($this->room->love_count));
    }

    public function testGetLastUpdateAttribute()
    {
        $now = Carbon::now()->format("d-m-Y H:i:s");
        $this->room->kost_updated_date = $now;
        $this->room->save();

        $this->assertEquals($now, $this->room->last_update);
    }

    public function testGetLastUpdateAttributeWithNullKostUpdatedDate()
    {
        $expectedDate = Carbon::parse($this->room->created_at)->format("d-m-Y H:i:s");
        $this->room->kost_updated_date = null;
        $this->room->save();

        $this->assertEquals($expectedDate, $this->room->last_update);
    }

    public function testGetPhoneNumbersAttribute()
    {
        $this->assertTrue(isset($this->room->phone_numbers));
    }

    public function testGetViewCountAttribute()
    {
        // TODO: Remove the method as it's not being used anymore (self-referencing)
        $this->assertTrue(true);
    }

    public function testGetOwnerIdReturnNull()
    {
        $this->owner->status = 'unverified';
        $this->owner->save();

        $this->assertNull($this->room->owner_id);
    }

    public function testGetOwnerId()
    {
        $this->assertEquals((string)$this->user->id, $this->room->owner_id);
    }

    public function testGetLabelPromotedAttribute()
    {
        $this->assertTrue(isset($this->room->label_promoted));
    }

    public function testGetPriceShownAttributeWithInvalidData()
    {
        $this->assertArraySubset(
            [
                'monthly'
            ],
            $this->room->price_shown
        );
    }

    public function testGetPriceShownAttributeWithValidData()
    {
        $this->room->payment_duration = "daily|monthly|yearly";
        $this->room->save();

        $this->assertArraySubset(
            [
                'daily',
                'monthly',
                'yearly'
            ],
            $this->room->price_shown
        );
    }

    public function testGetMessageCountReturnZero()
    {
        $this->assertEquals(0, $this->room->message_count);
    }

    public function testGetMessageCount()
    {
        $userId = mt_rand(11111, 99999);
        $rows = mt_rand(5, 10);

        factory(Room::class)->create()->each(
            function ($room) use ($userId, $rows) {
                return $room->calls()->saveMany(
                    factory(Call::class, $rows)->make(
                        [
                            'user_id' => $userId
                        ]
                    )
                );
            }
        );

        $this->assertEquals($rows, $this->room->message_count);
    }

    public function testGetPhoneNumbers()
    {
        $this->room->phone = '08123456789';
        $this->room->manager_phone = '08129876543';
        $this->room->owner_phone = '08111222333';
        $this->room->save();

        $this->assertEquals(3, count($this->room->phone_numbers));
    }

    public function testGetAllRatingAttribute()
    {
        $this->assertTrue(isset($this->room->all_rating));
    }

    public function testGetHasPhotoRoundAttribute()
    {
        $this->assertTrue(isset($this->room->has_photo_round));
    }

    public function testGetRelatedPromotedAttribute()
    {
        // TODO: Method needs a refactor before this testcase
        $this->assertTrue(true);
    }

    public function testGetPromoTitleAttribute()
    {
        factory(Promotion::class)->create(
            [
                'designer_id' => $this->room->id
            ]
        );

        $this->assertTrue(isset($this->room->promo_title));
    }

    public function testGetPromoTitleAttributeReturnNull()
    {
       factory(Promotion::class)->create(
            [
                'designer_id' => $this->room->id,
                'start_date'  => '2019-06-21',
                'finish_date' => '2019-07-21'
            ]
        );

        $this->assertNull($this->room->promo_title);
    }

    public function testGetPhoto360AttributeWithNull()
    {
        $this->room->photo_round_id = null;
        $this->room->save();

        $this->assertNull($this->room->photo_360);

        $this->room->photo_round_id = 0;
        $this->room->save();

        $this->assertNull($this->room->photo_360);
    }

    public function testGetPhoto360AttributeWithInvalidMedia()
    {
        $this->room->photo_round_id = 123456;
        $this->room->save();

        $this->assertNull($this->room->photo_360);
    }

    public function testGetPhoto360AttributeWithValidMedia()
    {
        $media = factory(Media::class)->create();

        $this->room->photo_round_id = $media->id;
        $this->room->save();

        $this->assertNotNull($this->room->photo_360);
    }

    public function testGetPhotoUrlAttributeWithNoPhoto()
    {
        $this->assertEquals(
            [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => ''
            ],
            $this->room->photo_url
        );
    }

    public function testGetPhotoUrlAttributeWithValidPhoto()
    {
        $photo = factory(Media::class)->create();

        $this->room->photo_id = $photo->id;
        $this->room->save();

        $response = $this->room->photo_url;

        $this->assertArrayHasKey('real', $response);
        $this->assertTrue(strpos($response['small'], '-240x320.jpg') !== false);
        $this->assertTrue(strpos($response['medium'], '-360x480.jpg') !== false);
        $this->assertTrue(strpos($response['large'], '-540x720.jpg') !== false);
    }

    public function testGetPhoneStatusAttribute()
    {
        $this->room->expired_phone = 0;
        $this->assertFalse($this->room->phone_status);

        $this->room->expired_phone = 1;
        $this->assertTrue($this->room->phone_status);
    }

    public function testGetMinMonthAttributeWithNoTag()
    {
        $tag = factory(Tag::class)->create(
            [
                'type' => 'emotion'
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => $tag->id
            ]
        );

        $this->assertNull($this->room->min_month);
    }

    public function testGetMinMonthAttributeWithTag()
    {
        $tag = factory(Tag::class)->create(
            [
                'type' => 'keyword'
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => $tag->id
            ]
        );

        $this->assertEquals($tag->name, $this->room->min_month);
    }

    public function testGetMessageCountAttribute()
    {
        $this->assertTrue(isset($this->room->message_count));
    }

    public function testGetAllPhoneAttribute()
    {
        $this->room->phone = '08123456789';
        $this->room->manager_phone = '08123456789';
        $this->room->owner_phone = '08123456789';
        $this->room->save();

        $this->assertIsArray($this->room->all_phone);
        $this->assertEquals(3, count($this->room->all_phone));
    }

    public function testGetCityAttributeWithNullAreaCityString()
    {
        $this->room->area_city = null;
        $this->room->save();

        $this->assertEmpty($this->room->city);
    }

    public function testGetCityAttributeWithEmptyAreaCityString()
    {
        $this->room->area_city = "";
        $this->room->save();

        $this->assertEmpty($this->room->city);
    }

    public function testGetCityAttributeWithValidAreaCityString()
    {
        $this->room->area_city = "Kabupaten Sleman DIY";
        $this->room->save();
        $this->assertEquals('Sleman DIY', $this->room->city);

        $this->room->area_city = "Kota Surabaya";
        $this->room->save();
        $this->assertEquals('Surabaya', $this->room->city);

        $this->room->area_city = "City of Heroes";
        $this->room->save();
        $this->assertEquals('of Heroes', $this->room->city);
    }

    public function testGetOfficePhoneWithInvalidCityName()
    {
        $this->room->area_city = "Kota Surabaya";
        $this->room->save();

        $this->assertEquals('087839647530', $this->room->office_phone);
    }

    public function testGetOfficePhoneWithValidCityName()
    {
        $this->room->area_city = "Kota Depok";
        $this->room->save();
        $this->assertEquals('087705466177', $this->room->office_phone);

        $this->room->area_city = "Kota Jakarta Selatan";
        $this->room->save();
        $this->assertEquals('087705466166', $this->room->office_phone);

        $this->room->area_city = "Kabupaten Sleman";
        $this->room->save();
        $this->assertEquals('087705466177', $this->room->office_phone);
    }

    public function testGetLandingChildAttribute()
    {
        $this->assertTrue(isset($this->room->landing_child));
    }

    public function testGetListAddressAttribute()
    {
        $this->assertEquals("", $this->room->list_address);
    }

    public function testGetLabelAttributeWithValidTags()
    {
        $tag = factory(Tag::class)->create(
            [
                'id' => 59
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => $tag->id
            ]
        );

        $this->room->price_monthly = 1000000;
        $this->room->save();

        $this->assertArraySubset(
            [
                [
                    "label" => "Bebas 24 Jam",
                    "priority" => 2
                ]
            ],
            $this->room->label
        );
    }

    public function testGetLabelAttributeWithValidTagsAndHigherPrice()
    {
        $tag = factory(Tag::class)->create(
            [
                'id' => 13
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => $tag->id
            ]
        );

        $this->room->price_monthly = 2000000;
        $this->room->save();

        $this->assertNull($this->room->label);
    }

    public function testGetLabelAttributeWithInvalidTags()
    {
        $tag = factory(Tag::class)->create(
            [
                'id' => 100
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => $tag->id
            ]
        );

        $this->assertNull($this->room->label);
    }

    public function testGetCountRatingAttribute()
    {
        $this->assertTrue(isset($this->room->count_rating));
    }

    public function testGetRelatedRoomAttribute()
    {
        // TODO: Method needs a refactor before this testcase
        $this->assertTrue(true);
    }

    public function testGetAvailableRoomAttribute()
    {
        $this->assertTrue(isset($this->room->available_room));
    }

    public function testGetAvailableRoomOnStatusZeroNotNullReturnOne()
    {
        $room = factory(Room::class)->make([
            'status' => 0,
            'room_available' => null
        ]);
        $this->assertSame(0, $room->getAvailableRoom());
    }

    public function testGetOwnerIdAttribute()
    {
        $this->assertTrue(isset($this->room->owner_id));
    }

    public function testGetOfficePhoneAttribute()
    {
        $this->assertTrue(isset($this->room->office_phone));
    }

    public function testGetNameSlugAttribute()
    {
        $this->assertTrue(isset($this->room->name_slug));
    }

    public function testGetListCardsAttributeWithEmptyPhoto()
    {
        $this->assertEmpty($this->room->list_cards);
    }

    public function testGetListCardsAttributeWithNullPhoto()
    {
        factory(Card::class)->create(
            [
                'designer_id' => $this->room->id,
                'photo_id' => null
            ]
        );

        $this->assertEmpty($this->room->list_cards);
    }

    public function testGetListCardsAttributeWithValidPhotos()
    {
        $media = factory(Media::class)->create();
        factory(Card::class)->create(
            [
                'designer_id' => $this->room->id,
                'photo_id' => $media->id
            ]
        );

        $media = factory(Media::class)->create();
        factory(Card::class)->create(
            [
                'designer_id' => $this->room->id,
                'photo_id' => $media->id
            ]
        );

        $this->assertNotEmpty($this->room->list_cards);
    }

    public function testGetPhotoRoundUrlAttributeWithNull()
    {
        $this->room->photo_round_id = null;
        $this->room->save();

        $this->assertNull($this->room->photo_round_url);

        $this->room->photo_round_id = 0;
        $this->room->save();

        $this->assertNull($this->room->photo_round_url);
    }

    public function testGetPhotoRoundUrlAttributeWithInvalidMedia()
    {
        $this->room->photo_round_id = 123456;
        $this->room->save();

        $this->assertNull($this->room->photo_round_url);
    }

    public function testGetPhotoRoundUrlAttributeWithValidMedia()
    {
        $media = factory(Media::class)->create();

        $this->room->photo_round_id = $media->id;
        $this->room->save();

        $this->assertNotNull($this->room->photo_round_url);
    }

    public function testGetCountTelpAttribute()
    {
        $this->assertTrue(isset($this->room->count_telp));
    }

    public function testGetStatusRoomAttribute()
    {
        $this->assertTrue(isset($this->room->status_room));
    }

    public function testGetStatusRoomOnAvailableZeroNullStatusReturn2()
    {
        $room = factory(Room::class)->make([
            'status' => null,
            'room_available' => 0
        ]);
        $this->assertSame(2, $room->getStatusRoom());
    }

    public function testGetStatusRoomOnAvailableZeroStatusNotNullReturnStatus()
    {
        $room = factory(Room::class)->make([
            'status' => 1,
            'room_available' => 0
        ]);
        $this->assertSame(1, $room->getStatusRoom());
    }

    public function testGetVerifiedOwnerAttribute()
    {
        $this->assertTrue(isset($this->room->verified_owner));
    }

    public function testGetStringRatingAttribute()
    {
        $this->assertTrue(isset($this->room->string_rating));
    }

    public function testGetDetailAddressAttribute()
    {
        $this->assertEquals('', $this->room->detail_address);
    }

    public function testGetSingleLabelAttribute()
    {
        factory(RoomTag::class)->create(
            [
                'designer_id' => $this->room->id,
                'tag_id' => factory(Tag::class)->create(
                    [
                        'id' => 59
                    ]
                )
            ]
        );

        $this->room->price_monthly = 1000000;
        $this->room->save();

        $this->assertArraySubset(
            [
                "label" => "Bebas 24 Jam",
                "priority" => 2
            ],
            $this->room->single_label
        );
    }

    public function testStringRatingHaveValidOutput()
    {
        $fakeRoom = factory(Room::class)->create();

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 4,
                    'comfort' => 4,
                    'safe' => 4,
                    'price' => 4,
                    'room_facility' => 4,
                    'public_facility' => 4
                ]
            );

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 2,
                    'comfort' => 2,
                    'safe' => 2,
                    'price' => 2,
                    'room_facility' => 2,
                    'public_facility' => 2
                ]
            );

        $room = Room::with('avg_review')->where('song_id', $fakeRoom->song_id)->first();
        $this->assertEquals(3, $room->string_rating);
    }

    public function testStringRatingHaveNullOutputWhenNoRelationPassed()
    {
        $fakeRoom = factory(Room::class)->create();

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 4,
                    'comfort' => 4,
                    'safe' => 4,
                    'price' => 4,
                    'room_facility' => 4,
                    'public_facility' => 4
                ]
            );

        $room = Room::where('song_id', $fakeRoom->song_id)->first();
        $this->assertEquals(0, $room->string_rating);
    }

    public function testStringRatingHaveNullOutputWhenHasNoRating()
    {
        $fakeRoom = factory(Room::class)->create();
        $room = Room::where('song_id', $fakeRoom->song_id)->first();
        $this->assertEquals(0, $room->string_rating);
    }

    public function testGetDetailRatingHasValidReturn()
    {
        $fakeRoom = factory(Room::class)->create();

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 4,
                    'comfort' => 4,
                    'safe' => 4,
                    'price' => 4,
                    'room_facility' => 4,
                    'public_facility' => 4
                ]
            );

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 3,
                    'comfort' => 3,
                    'safe' => 3,
                    'price' => 3,
                    'room_facility' => 3,
                    'public_facility' => 3
                ]
            );

        $room = Room::with('avg_review')->where('song_id', $fakeRoom->song_id)->first();
        $this->assertEquals(3, $room->detail_rating);
    }

    public function testGetDetailRatingInDoubleHasValidReturn()
    {
        $fakeRoom = factory(Room::class)->create();

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 4,
                    'comfort' => 4,
                    'safe' => 4,
                    'price' => 4,
                    'room_facility' => 4,
                    'public_facility' => 4
                ]
            );

        factory(Review::class)
            ->create(
                [
                    'designer_id' => $fakeRoom->id,
                    'cleanliness' => 3,
                    'comfort' => 3,
                    'safe' => 3,
                    'price' => 3,
                    'room_facility' => 3,
                    'public_facility' => 3
                ]
            );

        $room = Room::with('avg_review')->where('song_id', $fakeRoom->song_id)->first();
        $this->assertEquals(4.38, $room->detail_rating_in_double);
    }

    public function testGetPromoRoomSuccess()
    {
        $room = factory(Room::class)->create();

        $promotion = factory(Promotion::class)->create([
            'designer_id' => $room->id,
            'title' => 'Promo title',
            'id' => 1001,
            'content' => 'Promo description',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d', strtotime('+7 days')),
            'verification' => 'true'
        ]);

        $room = Room::with('promotion')->where('id', $room->id)->first();
        $getPromotion = $room->kost_promotion;
        $this->assertEquals($promotion->title, $getPromotion->title);
    }

    public function testGetPromoRoomNull()
    {
        $room = factory(Room::class)->create();
        $room = Room::with('promotion')->where('id', $room->id)->first();
        $getPromotion = $room->kost_promotion;
        $this->assertEquals(null, $getPromotion);
    }

    public function testGetKostPromotionNull()
    {
        $room = factory(Room::class)->create();

        $this->assertNull($room->getKostPromotion());
    }

    public function testGetPromoRoomExpired()
    {
        $room = factory(Room::class)->create();

        $promotion = factory(Promotion::class)->create([
            'designer_id' => $room->id,
            'title' => 'Promo title',
            'id' => 1001,
            'content' => 'Promo description',
            'start_date' => date('Y-m-d', strtotime('-7 days')),
            'finish_date' => date('Y-m-d', strtotime('-3 days')),
            'verification' => 'true'
        ]);

        $room = Room::with('promotion')->where('id', $room->id)->first();
        $getPromotion = $room->kost_promotion;
        $this->assertEquals(null, $getPromotion);
    }

    public function testGetKosStatusesAttributesReturnEditedOnDraft2()
    {

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->state('owner')->create(
            [
                'designer_id' => $room->id,
                'user_id' => $this->user->id,
                'status' => RoomOwner::ROOM_EDIT_STATUS
            ]
        );

        $room->load('owners');
        $this->assertSame(RoomOwner::LABEL_ROOM_OWNER_EDITED, $room->kos_statuses);
    }

    public function testGetUniqueCode()
    {
        $room = factory(Room::class)->create();
        factory(UniqueCode::class)->create(
            [
                'designer_id' => $room->id
            ]
        );

        $this->assertTrue(!empty($room->getUniqueCode()));
    }

    public function testGetUniqueCodeReturnEmpty()
    {
        $room = factory(Room::class)->create();

        $this->assertTrue(empty($room->getUniqueCode()));
    }

    public function testGetIsTestDataAttribute()
    {
        $room = factory(Room::class)->create(
            [
                'name' => 'Kost Campur Palagan Sleman'
            ]
        );

        $this->assertFalse($room->is_test_data);

        $room = factory(Room::class)->create(
            [
                'name' => 'Kost Test Campur Palagan Sleman'
            ]
        );

        $this->assertTrue($room->is_test_data);
    }

    // test owner get active since

    public function testGetOwnerActiveSince()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => '2018-06-01 19:00:00',
        ]);

        $result = $room->owner_active_since;
        $this->assertEquals('2018-06-01 19:00:00', $result);
    }

    public function testGetOwnerActiveSinceWithNoRelationExist()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $result = $room->owner_active_since;
        $this->assertEquals('', $result);
    }

    public function testGetOwnerActiveSinceStatusNotApprove()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => 'anything',
            'created_at' => '2018-06-01 19:00:00',
        ]);

        $result = $room->owner_active_since;
        $this->assertEquals('', $result);
    }

    public function testGetOwnerActiveSinceWhenCreatedAtNull()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => null,
        ]);

        $result = $room->owner_active_since;
        $this->assertEquals('', $result);
    }

    public function testGetClickAttributeGetFromViewCount()
    {
        $room = factory(Room::class)->create([
            'view_count'    => 90
        ]);

        $result = $room->click_count;

        $this->assertEquals(90, $result);
    }

    public function testGetSpecificGoldPlusLevelAttributeForGp1()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus1'),
                'name' => 'Mamikos Goldplus 1',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals(Room::INT_VALUE_OF_GOLDPLUS_1, $room->specific_gold_plus_level);
    }

    public function testGetSpecificGoldPlusLevelAttributeForGp2()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus2'),
                'name' => 'Mamikos Goldplus 2',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals(Room::INT_VALUE_OF_GOLDPLUS_2, $room->specific_gold_plus_level);
    }

    public function testGetSpecificGoldPlusLevelAttributeForGp3()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus3'),
                'name' => 'Mamikos Goldplus 3',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals(Room::INT_VALUE_OF_GOLDPLUS_3, $room->specific_gold_plus_level);
    }

    public function testGetSpecificGoldPlusLevelAttributeForGp4()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus4'),
                'name' => 'Mamikos Goldplus 4',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals(Room::INT_VALUE_OF_GOLDPLUS_4, $room->specific_gold_plus_level);
    }

    public function testGetSpecificGoldPlusLevelAttributeForNonGp()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => 1,
                'name' => 'OYO',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals(Room::INT_VALUE_OF_NON_GOLDPLUS, $room->specific_gold_plus_level);
    }

    private function prepareGpRoom(int $gpLevelId): Room
    {
        $room = factory(Room::class)->create();
        $room->level()->attach($gpLevelId);
        return $room;
    }
    private function prepareKostLevel(int $levelId): KostLevel
    {
        return factory(KostLevel::class)->create([
            'id' => $levelId,
        ]);
    }
    public function prepareRegularLevel()
    {
        factory(KostLevel::class)->state('regular')->create([
            'id' => 1,
        ]);
    }

    /**
     * @group UG
     * @group UG-4216
     */
    public function testGetGoldplusLevelIdAttributeGp1()
    {
        $this->prepareRegularLevel();

        foreach (KostLevel::getGoldplusLevelIdsByLevel(1) as $gpId) {
            $this->prepareKostLevel($gpId);
            $room = $this->prepareGpRoom($gpId);
            $this->assertNotNull($room->goldplus_level_id);
            $this->assertEquals($gpId, $room->goldplus_level_id);
        }
    }

    /**
     * @group UG
     * @group UG-4216
     */
    public function testGetGoldplusLevelIdAttributeGp2()
    {
        $this->prepareRegularLevel();

        foreach (KostLevel::getGoldplusLevelIdsByLevel(2) as $gpId) {
            $this->prepareKostLevel($gpId);
            $room = $this->prepareGpRoom($gpId);
            $this->assertNotNull($room->goldplus_level_id);
            $this->assertEquals($gpId, $room->goldplus_level_id);
        }
    }

    /**
     * @group UG
     * @group UG-4216
     */
    public function testGetGoldplusLevelIdAttributeGp3()
    {
        $this->prepareRegularLevel();

        foreach (KostLevel::getGoldplusLevelIdsByLevel(3) as $gpId) {
            $this->prepareKostLevel($gpId);
            $room = $this->prepareGpRoom($gpId);
            $this->assertNotNull($room->goldplus_level_id);
            $this->assertEquals($gpId, $room->goldplus_level_id);
        }
    }

    /**
     * @group UG
     * @group UG-4216
     */
    public function testGetGoldplusLevelIdAttributeGp4()
    {
        $this->prepareRegularLevel();

        foreach (KostLevel::getGoldplusLevelIdsByLevel(4) as $gpId) {
            $this->prepareKostLevel($gpId);
            $room = $this->prepareGpRoom($gpId);
            $this->assertNotNull($room->goldplus_level_id);
            $this->assertEquals($gpId, $room->goldplus_level_id);
        }
    }

    public function testGetOwnerSuccessTransaction()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => true]);

        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id,
                'status' => 'verified'
            ]
        );

        $contract = factory(MamipayContract::class)->create(['owner_id' => $owner->id]);

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(1, $room->getOwnerSuccessTransaction());
    }

    public function testGetOwnerSuccessTransactionWithoutValidOwner()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => true]);

        // room owner is not verified
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id,
                'status' => 'unverified'
            ]
        );

        $contract = factory(MamipayContract::class)->create(['owner_id' => $owner->id]);

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(0, $room->getOwnerSuccessTransaction());
    }

    public function testGetOwnerSuccessTransactionWithoutContract()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => true]);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id,
                'status' => 'verified'
            ]
        );

        // contract with wrong owner
        $contract = factory(MamipayContract::class)->create(['owner_id' => $owner->id+1]);

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(0, $room->getOwnerSuccessTransaction());
    }

    public function testGetOwnerSuccessTransactionWithoutPaidInvoice()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => true]);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id,
                'status' => 'verified'
            ]
        );

        $contract = factory(MamipayContract::class)->create(['owner_id' => $owner->id]);

        // unpaid invoice
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_UNPAID
            ]
        );

        $this->assertEquals(0, $room->getOwnerSuccessTransaction());
    }

    public function testGetKosSuccessTransaction()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id,
                'designer_id' => $room->id
            ]
        );

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(1, $room->getKosSuccessTransaction());
    }

    public function testGetKosSuccessTransactionWithoutValidContract()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        // invalid contract_id
        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id+1,
                'designer_id' => $room->id
            ]
        );

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(0, $room->getKosSuccessTransaction());
    }

    public function testGetKosSuccessTransactionWithoutPaidInvoice()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id,
                'designer_id' => $room->id
            ]
        );

        // unpaid invoice
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_UNPAID
            ]
        );

        $this->assertEquals(0, $room->getKosSuccessTransaction());
    }

    public function testGetKosBookingTransaction()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id,
                'designer_id' => $room->id
            ]
        );

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        factory(BookingUser::class)->create(
            [
                'contract_id' => $contract->id,
                'booking_designer_id' => $room->id
            ]
        );

        $this->assertEquals(1, $room->getKosBookingTransaction());
    }

    public function testGetKosBookingTransactionWithoutBookingUser()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id,
                'designer_id' => $room->id
            ]
        );

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        $this->assertEquals(0, $room->getKosBookingTransaction());
    }

    public function testGetOwnerAndKosTransactionAttribute()
    {
        $room = factory(Room::class)->create();

        $transaction = $room->owner_and_kos_transaction;

        $this->assertEquals(0, $transaction['number_success_owner_trx']);
        $this->assertEquals(0, $transaction['number_success_kos_trx']);
    }
}
