<?php

namespace App\Entities\Room;

use App\Entities\Room\HistorySlug;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class HistorySlugTest extends MamiKosTestCase
{
    public function testRoomSimilarSlug_FoundInHistory()
    {
        $expectedSlug = 'expected';
        $room = factory(Room::class)->states(['active', 'availablePriceMonthly'])->create();
        $historySlug = factory(HistorySlug::class)->create([
            'designer_id' => $room->id,
            'slug' => $expectedSlug,
        ]);

        $result = HistorySlug::roomSimilarSlug($expectedSlug . '-1');
        $this->assertEquals($room->id, $result->id);
    }

    public function testRoomSimilarSlug_FoundInRoom()
    {
        $expectedSlug = 'expected';
        $room = factory(Room::class)->states(['active', 'availablePriceMonthly'])->create([
            'slug' => $expectedSlug
        ]);

        $result = HistorySlug::roomSimilarSlug($expectedSlug . '-1');
        $this->assertEquals($room->id, $result->id);
    }

    public function testRoomSimilarSlug_NotFound()
    {
        $expectedSlug = 'expected';
        $room = factory(Room::class)->states(['active', 'availablePriceMonthly'])->create();
        $historySlug = factory(HistorySlug::class)->create([
            'designer_id' => $room->id,
            'slug' => 'expectnot',
        ]);

        $result = HistorySlug::roomSimilarSlug($expectedSlug . '-1');
        $this->assertNull($result);
    }

    public function testRoomSimilarSlug_SlugNotNumeric()
    {
        $expectedSlug = 'expected';
        $room = factory(Room::class)->states(['active', 'availablePriceMonthly'])->create();
        $historySlug = factory(HistorySlug::class)->create([
            'designer_id' => $room->id,
            'slug' => $expectedSlug,
        ]);

        $result = HistorySlug::roomSimilarSlug($expectedSlug . '-a');
        $this->assertNull($result);
    }
}
