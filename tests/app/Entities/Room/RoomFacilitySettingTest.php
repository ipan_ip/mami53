<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;

class RoomFacilitySettingTest extends MamiKosTestCase
{
    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $facilitySetting = factory(RoomFacilitySetting::class)->create([
            'designer_id' => $room->id
        ]);

        $this->assertEquals($room->id, $facilitySetting->room->id);
    }
}
