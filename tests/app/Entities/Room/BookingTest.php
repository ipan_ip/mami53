<?php

namespace App\Entities\Survey;

use App\Entities\Room\Booking;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class BookingTest extends MamiKosTestCase
{
    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $booking = factory(Booking::class)->create([
            'designer_id' => $room->id
        ]);

        $result = $booking->room;
        $this->assertEquals($result->id, $room->id);
    }

    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $booking = factory(Booking::class)->create([
            'user_id' => $user->id
        ]);

        $result = $booking->user;
        $this->assertEquals($result->id, $user->id);
    }
}
