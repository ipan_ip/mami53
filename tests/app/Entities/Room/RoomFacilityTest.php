<?php

namespace App\Entities\Room;

use App\User;
use App\Entities\Room\Element\Tag;
use App\Entities\Media\Media;
use App\Entities\Facility\FacilityCategory;
use App\Entities\Facility\FacilityType;
use Schema;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Test\MamiKosTestCase;

class RoomFacilityTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testRoomFacilityHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('designer_facility',[
                'designer_id',
                'tag_id',
                'creator_id',
                'photo_id',
                'name',
                'note',
        ]), 1);
    }

    public function testEntityRelation()
    {
        $tag    = factory(Tag::class)->create();
        $room   = factory(Room::class)->create();
        $media  = factory(Media::class)->create();
        $user   = factory(User::class)->create();

        $roomFacility = factory(RoomFacility::class)->create([
            'tag_id'        => $tag->id,
            'designer_id'   => $room->id,
            'photo_id'      => $media->id,
            'creator_id'    => $user->id
        ]);

        $this->assertInstanceOf(Tag::class, $roomFacility->tag);
        $this->assertInstanceOf(Room::class, $roomFacility->room);
        $this->assertInstanceOf(Media::class, $roomFacility->photo);
        $this->assertInstanceOf(User::class, $roomFacility->creator);
    }

    public function testRemoveExistingRoomFacility()
    {
        $roomFacility = factory(RoomFacility::class)->create();

        RoomFacility::remove($roomFacility->id);

        $deletedRoomFacility = RoomFacility::where('id', $roomFacility->id)->first();

        $this->assertNull($deletedRoomFacility);
    }

    public function testRemoveNonExistingRoomFacility()
    {
        $response = RoomFacility::remove(1010);

        $this->assertFalse($response);
    }

    public function testRoomFacilityStoreMissingSomeData()
    {
        $room   = factory(Room::class)->create();

        $data   = [
            'room_id' => $room->id
        ];

        $response = RoomFacility::store($data);

        $this->assertFalse($response);
    }

    public function testRoomFacilityStore()
    {
        $tag    = factory(Tag::class)->create();
        $room   = factory(Room::class)->create();
        $media  = factory(Media::class)->create();
        $user   = factory(User::class)->create();
        $this->actingAs($user);

        $data   = [
            'room_id' => $room->id,
            '-'.$tag->id.'-' => $media->id
        ];

        RoomFacility::store($data);
        
        $roomFacility = RoomFacility::first();

        $this->assertEquals($room->id, $roomFacility->designer_id);
        $this->assertEquals($tag->id, $roomFacility->tag_id);
        $this->assertEquals($user->id, $roomFacility->creator_id);
    }

    public function testRoomFacilityGetFacilityStructure()
    {
        $facilityCategory   = factory(FacilityCategory::class)->create([
            'name'                  => 'Fasilitas Kamar'
        ]);
        $facilityType       = factory(FacilityType::class)->create([
            'name'                  => 'Layanan Kamar',
            'facility_category_id'  => $facilityCategory->id
        ]);
        $tag                = factory(Tag::class)->create([
            'facility_type_id'      => $facilityType->id
        ]);

        $facilityStructure = RoomFacility::getFacilityStructure([$facilityCategory->id]);

        $this->assertEquals($facilityCategory->id, $facilityStructure[0]['id']);
        $this->assertEquals($facilityCategory->name, $facilityStructure[0]['name']);
        $this->assertEquals($facilityType->id, $facilityStructure[0]['types'][0]['id']);
        $this->assertEquals($facilityType->name, $facilityStructure[0]['types'][0]['name']);
    }

    public function testRoomFacilityGetCompiledFacilityStructure()
    {
        $room = factory(Room::class)->create();
        $facilityCategory   = factory(FacilityCategory::class)->create([
            'name'                  => 'Fasilitas Kamar'
        ]);
        $facilityType       = factory(FacilityType::class)->create([
            'name'                  => 'Layanan Kamar',
            'facility_category_id'  => $facilityCategory->id
        ]);
        $tag                = factory(Tag::class)->create([
            'facility_type_id'      => $facilityType->id
        ]);
        $facilitySetting    = factory(RoomFacilitySetting::class)->create([
            'designer_id'           => $room->id,
            'active_tags'           => (string)$tag->id
        ]);
        $media              = factory(Media::class)->create();
        $roomFacility       = factory(RoomFacility::class)->create([
            'tag_id'                => $tag->id,
            'photo_id'              => $media->id,
            'designer_id'           => $room->id
        ]);

        $compiledFacilityStructure = RoomFacility::getCompiledFacilityStructure($room->id);

        $this->assertEquals($facilityCategory->id, $compiledFacilityStructure[0]['id']);
        $this->assertEquals($facilityCategory->name, $compiledFacilityStructure[0]['name']);
        $this->assertEquals($facilityType->id, $compiledFacilityStructure[0]['types'][0]['id']);
        $this->assertEquals($facilityType->name, $compiledFacilityStructure[0]['types'][0]['name']);
        $this->assertEquals($tag->id, $compiledFacilityStructure[0]['types'][0]['tags'][0]['id']);
        $this->assertEquals($roomFacility->id, $compiledFacilityStructure[0]['types'][0]['tags'][0]['facilities'][0]['id']);
        $this->assertEquals($media->id, $compiledFacilityStructure[0]['types'][0]['tags'][0]['facilities'][0]['photo_id']);
        $this->assertEquals($media->getMediaUrl()['medium'], $compiledFacilityStructure[0]['types'][0]['tags'][0]['facilities'][0]['photo_url']);
    }
}
