<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;

class RoomSortScoreTrackerTest extends MamiKosTestCase
{
    
    public function testReport()
    {
        $room = factory(Room::class)->create();
        $sortScore = RoomSortScore::createFrom($room);

        $response = RoomSortScoreTracker::report($sortScore, $room->id);

        $this->assertTrue($response);
        $this->assertDatabaseHas(
            'designer_sort_score_tracker',
            [
                'designer_id' => $room->id,
                'score' => 1010000000
            ]
        );
    }

    public function testTrackScoreAndVersion()
    {
        $room = factory(Room::class)->create();
        RoomSortScoreTracker::trackScoreAndVersion(3, 100,  $room->id);
        $this->assertDatabaseHas('designer_sort_score_tracker',[
            'designer_id' => $room->id,
            'score' => 100,
            'score_version' => 3,
        ]);
    }
}
