<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class AddressNoteTest extends MamiKosTestCase
{

    /**
     * @group PMS-495
     */
    public function testRoomRelation()
    {
        $room = factory(Room::class)->states([
            'with-address',
            'attribute-complete-state',
            'with-small-rooms',
        ])->create();
        $addressNote = factory(AddressNote::class)->create([
            'designer_id' => $room->id
        ]);

        $room->load('address_note');
        $addressNote->load('room');
        $this->assertSame($room->id, $addressNote->room->id);
        $this->assertSame($room->address_note->id, $addressNote->id);
    }
}
