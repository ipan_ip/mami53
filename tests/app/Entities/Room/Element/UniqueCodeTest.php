<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Support\Facades\Schema;

class UniqueCodeTest extends MamiKosTestCase
{
    use WithoutEvents;

    protected function setUp() : void
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->actingAs($user);
    }

    public function testModelHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns(
                'designer_unique_code',
                [
                    'designer_id',
                    'code',
                    'trigger',
                ]
            ),
            1
        );
    }

    public function testModelHasRoomRelationship()
    {
        $room = factory(Room::class)->create();
        $model = factory(UniqueCode::class)->create(
            [
                'designer_id' => $room->id
            ]
        );

        $this->assertInstanceOf(Room::class, $model->room);
    }

    public function testUpdateCodeWithExistingRecord()
    {
        $room = factory(Room::class)->create();

        $record = factory(UniqueCode::class)->create(
            [
                'designer_id' => $room->id
            ]
        );

        $response = UniqueCode::updateCode($room, '456CD');

        $this->assertTrue($response);
        $this->assertDatabaseHas(
            'designer_unique_code',
            [
                'id' => $record->id,
                'code' => '456CD'
            ]
        );
    }

    public function testUpdateCodeWithNewRecord()
    {
        $room = factory(Room::class)->create();

        $response = UniqueCode::updateCode($room, '123AB');

        $this->assertTrue($response);
        $this->assertDatabaseHas(
            'designer_unique_code',
            [
                'designer_id' => $room->id,
                'code' => '123AB'
            ]
        );
    }
}
