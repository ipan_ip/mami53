<?php

namespace App\Entities\Room\Element;

use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Room;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use Illuminate\Support\Carbon;

class RoomUnitTest extends MamiKosTestCase
{
    public function testRelationToRoom()
    {
        $roomEntity = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $roomEntity->id]);

        $roomFromDB = $roomUnit->room()->first();

        $this->assertSame($roomEntity->id, $roomFromDB->id);
    }

    public function testRelationToLevel()
    {
        $roomEntity = factory(Room::class)->create();
        $roomLevel = factory(RoomLevel::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $roomEntity->id,
            'room_level_id' => $roomLevel->id
        ]);

        $roomFromDB = $roomUnit->level()->first();

        $this->assertSame($roomLevel->id, $roomFromDB->id);
    }

    public function testRelationToLevelHistory()
    {
        $user = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create(['user_id' => $user->id]);
        $roomLevel = factory(RoomLevel::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $roomEntity->id,
            'room_level_id' => $roomLevel->id,
        ]);
        $roomLevelHistory = RoomLevelHistory::create([
            'room_level_id' => $roomLevel->id,
            'room_id' => $roomUnit->id,
            'user_id' => $user->id,
            'action' => 'upgrade',
        ]);

        $roomFromDB = $roomUnit->level_histories()->first();

        $this->assertSame($roomLevelHistory->id, $roomFromDB->id);
    }

    public function testRelationToActiveContract()
    {
        $room = factory(Room::class)->state('active')->create([]);
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'occupied' => true
        ]);

        $contract = factory(MamipayContract::class)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'designer_room_id' => $roomUnit->id
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $roomUnit->active_contract);
    }

    public function testScopeOccupiedAndEmpty()
    {
        $roomEntity = factory(Room::class)->create();
        $roomUnits = factory(RoomUnit::class, 12)->make(['designer_id' => $roomEntity->id]);
        $occupiedUnites = $roomUnits->nth(2, 1)->each(function (RoomUnit $unit) {
            $unit->occupied = true;
        });

        $roomUnits->each(function (RoomUnit $unit) {
            $unit->save();
        });

        $this->assertEquals(
            $occupiedUnites->count(),
            RoomUnit::where('designer_id', $roomEntity->id)->occupied()->count()
        );
        $this->assertEquals(
            $roomUnits->count() - $occupiedUnites->count(),
            RoomUnit::where('designer_id', $roomEntity->id)->empty()->count()
        );
    }

    public function testIsOccupiedShortcut()
    {
        $roomUnit = factory(RoomUnit::class)->make(['occupied' => true]);

        $this->assertTrue($roomUnit->isOccupied());
    }

    public function testIsEmptyShortcut()
    {
        $roomUnit = factory(RoomUnit::class)->make(['occupied' => false]);

        $this->assertTrue($roomUnit->isEmpty());
    }

    public function testRoomBackRelationToRoomUnit()
    {
        $roomEntity = factory(Room::class)->create();
        $roomUnits = factory(RoomUnit::class, 12)->make(['designer_id' => $roomEntity->id]);
        $occupiedUnites = $roomUnits->nth(2, 1)->each(function (RoomUnit $unit) {
            $unit->occupied = true;
        });

        $roomUnits->each(function (RoomUnit $unit) {
            $unit->save();
        });

        $roomFromDB = Room::where('id', $roomEntity->id)->withCount(['room_unit_occupied', 'room_unit_empty', 'room_unit'])->first();

        $this->assertEquals(
            $occupiedUnites->count(),
            $roomFromDB->room_unit_occupied_count
        );

        $this->assertEquals(
            $roomUnits->count() - $occupiedUnites->count(),
            $roomFromDB->room_unit_empty_count
        );

        $this->assertEquals(
            $roomUnits->count(),
            $roomFromDB->room_unit_count
        );
    }

    // public function testRunSoftDelete()
    // {
    //     $room = factory(Room::class)->create([]);
    //     $roomUnit = factory(RoomUnit::class)->create([
    //         'designer_id' => $room->id
    //     ]);

    //     $reflection = new \ReflectionClass($roomUnit);
    //     $method = $reflection->getMethod('runSoftDelete');
    //     $method->setAccessible(true);

    //     $this->assertDatabaseHas('designer_room', $roomUnit->toArray());
    //     $method->invokeArgs($roomUnit, []);
    //     $this->assertDatabaseMissing('designer_room', $roomUnit->toAray());
    // }
}
