<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class InputTrackerTest extends MamiKosTestCase
{
    use WithFaker;

    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $inputTracker = factory(InputTracker::class)->create([
            'designer_id' => $room->id
        ]);

        $result = $inputTracker->room;
        $this->assertEquals($result->id, $room->id);
    }

    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $inputTracker = factory(InputTracker::class)->create([
            'user_id' => $user->id
        ]);

        $result = $inputTracker->user;
        $this->assertEquals($result->id, $user->id);
    }
}
