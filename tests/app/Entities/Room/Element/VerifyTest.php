<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class VerifyTest extends MamiKosTestCase
{
    private $verify;
    private $carbon;

    protected function setUp()
    {
        parent::setUp();
        $this->verify = app()->make(Verify::class);
        $this->carbon = app()->make(Carbon::class);
    }


    public function testRelationToRoom()
    {
        $room = factory(Room::class)->create();
        $verify = factory(Verify::class)->create(['designer_id' => $room->id]);

        $roomFromDB = $verify->room;

        $this->assertSame($room->id, $roomFromDB->id);
    }

    public function testVerifyNull()
    {
        $this->assertFalse($this->verify->verify());
    }

    public function testVerify()
    {
        $room = factory(Room::class)->create();

        $this->verify::verify($room->id);

        $this->assertDatabaseHas('designer_verify', [
            'designer_id' => $room->id,
            'action' => 'verify'
        ]);
    }

    public function testUnverifyNull()
    {
        $this->assertFalse($this->verify->unverify());
    }

    public function testUnverify()
    {
        $room = factory(Room::class)->create();

        $this->verify->unverify($room->id);

        $this->assertDatabaseHas('designer_verify', [
            'designer_id' => $room->id,
            'action' => 'unverify'
        ]);
    }

    public function testGetLastWeekOfYear()
    {
        $this->assertEquals(52, $this->verify->getLastWeekOfYear(2020));
        $this->assertEquals(53, $this->verify->getLastWeekOfYear(2021));
    }

    public function testGetRangeDateFromWeek()
    {
        $date = $this->carbon->now();
        $date = $date->setISODate(date('Y'), 1);

        $result = $this->verify->getRangeDateFromWeek(1);
        
        $this->assertEquals($date->startOfWeek(), $result['start']);
        $this->assertEquals($date->endOfWeek(), $result['end']);
    }
}
