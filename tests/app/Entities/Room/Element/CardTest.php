<?php

namespace App\Entities\Room\Element;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\Card\GroupType;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use App\Test\MamiKosTestCase;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;

class CardTest extends MamiKosTestCase
{
    use WithFaker;

    public function testShowList()
    {
        $rows = $this->faker->numberBetween(2, 5);
        $photoId = $this->faker->numberBetween(10000, 99999);

        // Cover photo
        $coverCard = factory(Card::class)->create(
            [
                'photo_id' => $photoId,
                'description' => 'This should be a -cover'
            ]
        );

        factory(Room::class)
            ->create()
            ->each(
                function ($room) use ($rows, $photoId) {
                    $cards = factory(Card::class, $rows)->create();
                    $room->cards()->saveMany($cards);
                }
            );

        $room = Room::first();
        $room->cards()->save($coverCard);

        $response = Card::showList($room);

        $this->assertEquals($rows + 1, count($response));
        $this->assertEquals($photoId, $response[0]['photo_id']);
    }

    public function testShowListWithSingleCard()
    {
        $photoId = $this->faker->numberBetween(10000, 99999);

        // Cover photo
        $coverCard = factory(Card::class)->create(
            [
                'photo_id' => $photoId,
                'description' => 'This should be a -cover'
            ]
        );

        $room = factory(Room::class)->create();
        $room->cards()->save($coverCard);

        $response = Card::showList($room);

        // TODO: Revert the expected number from 2 to 1 when iOS fix the issue
        // BG-3540 Quick Fix for iOS: 
        $this->assertEquals(2, count($response));
        $this->assertEquals($photoId, $response[0]['photo_id']);

        $coverCard->load('room');
        $resultRoom = $coverCard->room;
        $this->assertEquals($room->id, $resultRoom->id);
    }

    public function testShowListWithNoCards()
    {
        $room = factory(Room::class)->create();
        $response = Card::showList($room);

        // TODO: Revert the expected number from 2 to 1 when iOS fix the issue
        // BG-3540 Quick Fix for iOS: 
        $this->assertEquals(2, count($response));
        $dummy = ApiHelper::getDummyImage($room->name);
        $this->assertEquals($response[0], $dummy);
    }

    public function testGetAllFunctionSuccess()
    {
        $ammount =  5;
        $room = factory(Room::class)->create();
        factory(Card::class, $ammount)->make([
            'designer_id' => $room->id,
            'description' => 'This should be a -cover'
        ])->each(function ($card) {
            $card->photo_id = $this->faker->numberBetween(10000, 99999);
            $card->save();
        });

        $result = (new Card())->getAll($room);

        $this->assertEquals($ammount, count($result));
    }

    public function testGetCoverPhotoIdSuccess()
    {
        $ammount =  3;
        $room = factory(Room::class)->create();
        factory(Card::class, $ammount)->make([
            'designer_id' => $room->id,
            'description' => 'this-is-foto-kamar-mandi'
        ])->each(function ($card) {
            $card->photo_id = $this->faker->numberBetween(10000, 99999);
            $card->save();
        });
        $photoId = $this->faker->numberBetween(10000, 99999);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photoId,
            'description' => 'this-is-a-cover'
        ]);

        $result = (new Card())->getCoverPhotoId($room);

        $this->assertEquals($result, $photoId);
    }

    public function testGetCoverPhotoIdNoCards()
    {
        $room = factory(Room::class)->create();
        $result = (new Card())->getCoverPhotoId($room);
        $this->assertNull($result);
    }

    public function testGetIsCoverable()
    {
        $this->assertFalse(Card::getIsCoverable('uncoverable kamar mandi'));
        $this->assertTrue(Card::getIsCoverable('uncoverable kamar'));
    }

    public function testSanitizeCoverDescription()
    {
        $description = 'foto-foto-kamar-utama';
        $result = Card::sanitizeCoverDescription($description);
        $this->assertEquals('foto-kamar-utama-cover', $result);
    }

    public function testValidateCoverPhoto()
    {
        $photoId = $this->faker->numberBetween(10000, 99999);
        $room = factory(Room::class)->create([
            'photo_id' => $photoId
        ]);

        $description = 'foto-kamar';

        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photoId,
            'description' => $description
        ]);

        $valid = Card::validateCoverPhoto($room);
        $this->assertTrue($valid);

        $card->refresh();
        $this->assertNotEquals($description, $card->description);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardIdOne()
    {
        $room = factory(Room::class)->create();

        factory(Card::class, 2)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-'. GroupType::COVER
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 2)->make([
            'designer_id' => $room->id,
            'description' => 'foto-' . GroupType::MAIN . '-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 2)->make([
            'designer_id' => $room->id,
            'description' => 'foto-' . GroupType::BEDROOM . '-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 2)->make([
            'designer_id' => $room->id,
            'description' => 'foto-' . GroupType::BATHROOM . 'bak-nya'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $result = Card::categorizeCardWithoutCardIdOne($room);

        $this->assertEquals(4, count($result));
    }

    public function testCategorizeCardWithoutCardIdOneZeroCard()
    {
        $room = factory(Room::class)->create();
        $result = Card::categorizeCardWithoutCardIdOne($room);
        $this->assertEquals(array(), $result);
    }


    public function testRemoveRoomNameDescription()
    {
        $string = 'foto-kamar-as-cover';
        $result = Card::removeRoomNameDescription($string);
        $this->assertEquals($string, $result);
    }

    public function testRemoveAnyDuplicateCoverPhoto()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $media->id
        ]);

        factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $media->id,
            'description' => 'foto-kamar-as-cover'
        ]);

        factory(Card::class, 3)->create([
            'designer_id' => $room->id,
            'photo_id' => $media->id + rand(1, 8),
            'description' => 'foto-kamar-utama-as-cover'
        ]);

        Card::removeAnyDuplicateCoverPhoto($room);
        $room->load('cards');

        $filtered = $room->cards->filter(function ($card) {
            return stripos($card->description, '-cover');
        });

        $this->assertEquals(1, count($filtered));

    }

    public function testRemoveAnyDuplicateCoverPhotoZeroCard()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $media->id
        ]);

        $this->assertNull(
            Card::removeAnyDuplicateCoverPhoto($room)
        );
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardId()
    {
        $room = factory(Room::class)->create();

        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = Card::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardIdZeroCard()
    {
        $room = factory(Room::class)->create();

        $categorized = Card::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardIdWith360()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_round_id' => $media->id
        ]);

        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = Card::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }
    
    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardZeroCard()
    {
        $room = factory(Room::class)->create();

        $categorized = Card::categorizedCards($room, 'edit');

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizedCards()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create();

        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(Card::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = Card::categorizedCards($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testGetGroupFilterByTypeNotDefinedThrowException()
    {
        $this->expectException(Exception::class);
        (new Card())->getGroupFilterByType("definitely-not-exist");
    }

    /**
    * @group PMS-495
    */
    public function testUpdatePhotoGroupWithCardTypeIsNotImage()
    {
        $this->expectException(Exception::class);

        $style = factory(Card::class)->create([
            'type' => $this->faker->randomElement(['video_file', 'video', 'gif', 'text', 'webview_html', 'webview_url', 'link_webview', 'link', 'quiz', 'faisal', 'vote']),
        ]);

        (new Card())->updatePhotoGroup($style->id, Group::BUILDING_FRONT);
    }

    /**
    * @group PMS-495
    */
    public function testUpdatePhotoGroupTypeImageWithIsCoverIsTrueAndGroupIsNotInsideRoom()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $style = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM
        ]);
        $newPhotoGroup = $this->faker->randomElement([
            Group::BUILDING_FRONT,
            Group::INSIDE_BUILDING,
            Group::ROADVIEW_BUILDING,
            Group::ROOM_FRONT,
            Group::BATHROOM,
            Group::OTHER,
        ]);

        $result = (new Card())->updatePhotoGroup($style->id, $newPhotoGroup);
        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_style', [
            'id' => $style->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => false,
            'description' => Card::DESCRIPTION_PREFIX[$newPhotoGroup],
            'group' => $newPhotoGroup,
        ]);
        $this->assertDatabaseMissing('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testPhotoGroupAttributeWithGroupFieldIsNotNull()
    {
        $style = factory(Card::class)->create([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::BATHROOM,
                Group::OTHER,
            ]),
        ]);

        $this->assertEquals($style->group, $style->photo_group);
    }

    /**
    * @group PMS-495
    */    
    public function testPhotoGroupAttributeWithDescriptionisMatchedWithGroupFrontBuilding()
    {
        $string = Card::DESCRIPTION_PREFIX[Group::BUILDING_FRONT].'-1';
        $style = factory(Card::class)->create([
            'type' => 'image',
            'group' => null,
            'description' => $string,
        ]);

        $this->assertEquals(Group::BUILDING_FRONT, $style->photo_group);
    }

    /**
    * @group PMS-495
    */    
    public function testPhotoGroupAttributeWithDescriptionisMatchedWithGroupInsideRoom()
    {
        $string1 = Card::DESCRIPTION_PREFIX['cover'].'-1';
        $string2 = Card::DESCRIPTION_PREFIX[Group::INSIDE_ROOM].'-1';
        $style1 = factory(Card::class)->create([
            'type' => 'image',
            'group' => null,
            'description' => $string1,
        ]);
        $style2 = factory(Card::class)->create([
            'type' => 'image',
            'group' => null,
            'description' => $string2,
        ]);

        $this->assertEquals(Group::INSIDE_ROOM, $style1->photo_group);
        $this->assertEquals(Group::INSIDE_ROOM, $style2->photo_group);
    }

    /**
    * @group PMS-495
    */    
    public function testPhotoGroupAttributeWithDescriptionisMatchedWithGroupBathRoom()
    {
        $string = Card::DESCRIPTION_PREFIX[Group::BATHROOM].'-1';
        $style = factory(Card::class)->create([
            'type' => 'image',
            'group' => null,
            'description' => $string,
        ]);

        $this->assertEquals(Group::BATHROOM, $style->photo_group);
    }

    /**
    * @group PMS-495
    */    
    public function testPhotoGroupAttributeWithDescriptionisMatchedWithGroupOther()
    {
        $string = 'other group';
        $style = factory(Card::class)->create([
            'type' => 'image',
            'group' => null,
            'description' => $string,
        ]);

        $this->assertEquals(Group::OTHER, $style->photo_group);
    }

    /**
    * @group PMS-495
    */    
    public function testRegisterWithTypeImage()
    {
        $room = factory(Room::class)->create();
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::BATHROOM,
                Group::OTHER,
            ]),
            'source' => null,
            'ordering' => null,
            'photo_id' => null,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->register([$card], $room->id);
        $this->assertTrue($result);
        
        $this->assertDatabaseHas('designer_style', [
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'description' => Card::DESCRIPTION_PREFIX[$card['group']],
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithIsCoverIsTrueAndGroupIsNotInsideRoom()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $oldCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM
        ]);
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ]),
            'source' => null,
            'ordering' => null,
            'photo_id' => $photo->id,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id,$oldCard->id);
        $this->assertTrue($result);
        
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'photo_id' => $photo->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'description' => Card::DESCRIPTION_PREFIX[$card['group']],
            'is_cover' => false,
        ]);

        $this->assertDatabaseMissing('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testupdateCardsWithTypeImage()
    {
        $room = factory(Room::class)->create();
        $oldCard = factory(Card::class)->create();
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::BATHROOM,
                Group::OTHER,
            ]),
            'source' => null,
            'ordering' => null,
            'photo_id' => null,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id, $oldCard->id);
        $this->assertTrue($result);
        
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithIsCoverIsTrueAndGroupIsInsideRoom()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $oldCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]); 
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => Group::INSIDE_ROOM,
            'source' => null,
            'ordering' => null,
            'photo_id' => $photo->id,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id, $oldCard->id);
        $this->assertTrue($result);
        
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'photo_id' => $photo->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'is_cover' => true,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithRoomPhotoIdIsEqualsToCardPhotoIdAndGroupIsNotInsideRoom()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $photo->id
        ]);
        $oldCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'group' => Group::INSIDE_ROOM
        ]);
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ]),
            'source' => null,
            'ordering' => null,
            'photo_id' => $photo->id,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id, $oldCard->id);
        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'photo_id' => $photo->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'description' => Card::DESCRIPTION_PREFIX[$card['group']],
            'is_cover' => false,
        ]);

        $this->assertDatabaseMissing('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithRoomPhotoIdIsEqualsToCardPhotoIdAndGroupIsInsideBuilding()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $photo->id
        ]);
        $oldCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]); 
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => Group::INSIDE_ROOM,
            'source' => null,
            'ordering' => null,
            'photo_id' => $photo->id,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id, $oldCard->id);
        $this->assertTrue($result);
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'photo_id' => $photo->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'is_cover' => true,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);
    }

    /**
    * @group PMS-495
    */
    public function testUpdateCardGroupPhotoCoverToGroupWhichIsNotAllowedAsCover()
    {
        $room = factory(Room::class)->create();
        $oldCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type' => 'image',
            'group' => Group::INSIDE_ROOM,
            'is_cover' => true,
            'photo_id' => factory(Media::class)->create()->id,
        ]);
        $expectedCard = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type' => 'image',
            'group' => Group::INSIDE_ROOM,
            'is_cover' => false,
            'photo_id' => factory(Media::class)->create()->id,
        ]);
        $card = factory(Card::class)->make([
            'type' => 'image',
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ]),
            'source' => null,
            'ordering' => null,
            'photo_id' => null,
            'url_ori' => null,
        ])->toArray();

        $result = (new Card())->updateCards([$card], $room->id, $oldCard->id);
        $this->assertTrue($result);
        
        $this->assertDatabaseHas('designer_style', [
            'id' => $oldCard->id,
            'title' => 'cards',
            'type' => $card['type'],
            'designer_id' => $room->id,
            'source' => $card['source'],
            'ordering' => $card['ordering'],
            'group' => $card['group'],
            'is_cover' => false,
        ]);
        $this->assertDatabaseHas('designer_style', [
            'id' => $expectedCard->id,
            'type' => $expectedCard['type'],
            'designer_id' => $room->id,
            'source' => $expectedCard['source'],
            'ordering' => $expectedCard['ordering'],
            'group' => $expectedCard['group'],
            'is_cover' => true,
        ]);
    }
}
