<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Entities\Room\RoomClassHistory;
use App\Test\MamiKosTestCase;

class RoomClassHistoryTest extends MamiKosTestCase
{
    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $roomClassHistory = factory(RoomClassHistory::class)->create([
            'designer_id' => $room->id
        ]);

        $this->assertEquals($roomClassHistory->room->id, $room->id);
    }
}
