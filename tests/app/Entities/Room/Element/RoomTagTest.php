<?php

namespace App\Entities\Room\Element;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;

class RoomTagTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $this->roomTag = new RoomTag();
    }

    public function testRelationshipToRoom()
    {
        $tag = factory(Tag::class)->create([]);
        $room = factory(Room::class)->create([]);
        $roomTag = factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $tag->id
        ]);

        $this->assertDatabaseHas('designer', $room->toArray());
        $this->assertDatabaseHas('tag', $tag->toArray());
        $this->assertDatabaseHas('designer_tag', $roomTag->toArray());

        $this->assertEquals(1, $roomTag->room->count());
        $this->assertInstanceOf(Room::class, $roomTag->room);
    }

    public function testStoreIfRoomTagIsExists()
    {
        $tag = factory(Tag::class)->create([]);
        $room = factory(Room::class)->create([]);
        $roomTag = factory(RoomTag::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $tag->id
        ]);

        $result = $this->roomTag->store($tag->id, $room->id);

        $this->assertEquals($roomTag->id, $result->id);
    }

    public function testStoreIfRoomTagNotExistsShouldStoreToDatabase()
    {
        $tag = factory(Tag::class)->create([]);
        $room = factory(Room::class)->create([]);

        $result = $this->roomTag->store($tag->id, $room->id);

        $expectedResult = [
            'designer_id' => $room->id,
            'tag_id' => $tag->id
        ];

        $this->assertDatabaseHas('designer_tag', $expectedResult);
        $this->assertInstanceOf(RoomTag::class, $result);
    }
}
