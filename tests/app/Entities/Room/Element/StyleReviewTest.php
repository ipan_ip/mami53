<?php

namespace App\Entities\Room\Element;

use App\Entities\Media\Media;
use App\Entities\Room\Review;
use App\Test\MamiKosTestCase;

class StyleReviewTest extends MamiKosTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->class = new StyleReview();
    }

    public function testPhotoByRelation()
    {
        $photo = factory(Media::class)->create();
        $styleReview = factory(StyleReview::class)->create([
            'photo_id' => $photo->id
        ]);

        $this->assertEquals($photo->id, $styleReview->photo->id);
    }

    public function testReviewsRelation()
    {
        $review = factory(Review::class)->create();
        $styleReview = factory(StyleReview::class)->create([
            'review_id' => $review->id
        ]);

        $this->assertEquals($review->id, $styleReview->reviews->id);
    }

    public function testGetListPhoto()
    {
        $review = factory(Review::class)->create();
        $styleReviews = factory(StyleReview::class, 5)->create([
            'review_id' => $review->id
        ]);

        $result = $this->class->getListPhoto($review->id);
        $this->assertEquals($styleReviews->count(), $result->count());

        $styleReviews = $styleReviews->toArray();
        $result = $result->toArray();

        $resultCount = count($result);
        for ($i = 0 ; $i < $resultCount; $i++) {
            $this->assertEquals($styleReviews[$i]['id'], $result[$i]['id']);
        }
    }

    public function testShowList()
    {
        $review = factory(Review::class)->create();
        $styleReviews = factory(StyleReview::class, 5)->create([
            'review_id' => $review->id
        ]);

        $result = $this->class->showList($review);
        $this->assertIsArray($result);
        $this->assertEquals($styleReviews->count(), count($result));

        $styleReviews = $styleReviews->toArray();
        $resultCount = count($result);
        for ($i = 0 ; $i < $resultCount; $i++) {
            $this->assertEquals($styleReviews[$i]['id'], $result[$i]['id']);
            $this->assertEquals('image', $result[$i]['type']);
            $this->assertEquals($styleReviews[$i]['photo_id'], $result[$i]['photo_id']);
            $this->assertArrayHasKey('photo_url', $result[$i]);
        }
    }

    public function testPhotoUrlWithParamIsInstanceOfMedia()
    {
        $photo = factory(Media::class)->create();

        $result = $this->class->photo_url($photo);

        $this->assertNotEmpty($result['real']);
        $this->assertNotEmpty($result['small']);
        $this->assertNotEmpty($result['medium']);
        $this->assertNotEmpty($result['large']);
    }

    public function testPhotoUrlWithParmIsPhotoId()
    {
        $photo = factory(Media::class)->create();

        $result = $this->class->photo_url($photo->id);

        $this->assertNotEmpty($result['real']);
        $this->assertNotEmpty($result['small']);
        $this->assertNotEmpty($result['medium']);
        $this->assertNotEmpty($result['large']);
    }

    public function testPhotoUrlWithInvalidPhoto()
    {
        $photo = rand(1, 100);

        $result = $this->class->photo_url($photo);
        $this->assertEmpty($result['real']);
        $this->assertEmpty($result['small']);
        $this->assertEmpty($result['medium']);
        $this->assertEmpty($result['large']);
    }
}
