<?php

namespace App\Entities\Room\Element;

use App\Test\MamiKosTestCase;
use Illuminate\Http\Request;

class TagTypeTest extends  MamiKosTestCase
{
    protected $tag;

    protected function setUp() : void
    {
        parent::setUp();

        $this->tag = factory(Tag::class)->create();
    }

    public function testTagTypeBelongsToTag()
    {
        $tagType = factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id
            ]
        );

        $this->assertInstanceOf(Tag::class, $tagType->tag);
    }

    public function testCreateTagType()
    {
        $request = new Request();
        $request->label = 'keyword';

        TagType::createTagType($request, $this->tag);
        $this->assertCount(1, TagType::get());
    }

    public function testUpdateTagTypeForFirstType()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->label = 'keyword';

        TagType::updateTagType($request, $this->tag);
        $this->assertEquals($request->label, TagType::first()->name);
    }

    public function testUpdateTagTypeWithoutInstance()
    {
        $request = new Request();
        $request->label = 'keyword';

        TagType::updateTagType($request, $this->tag);
        $this->assertCount(0, TagType::get());
    }

    public function testUpdateTagTypeForExistingType2()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->label = 'keyword';
        $request->label2 = 'fac_room';

        TagType::updateTagType($request, $this->tag);
        $tagType = TagType::where('tag_id', $this->tag->id)->get();

        $this->assertCount(2, $tagType);
        $this->assertEquals($request->label, $tagType[0]->name);
        $this->assertEquals($request->label2, $tagType[1]->name);
    }

    public function testUpdateTagTypeForNewType2()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->label = 'keyword';
        $request->label2 = 'fac_room';

        TagType::updateTagType($request, $this->tag);
        $tagType = TagType::where('tag_id', $this->tag->id)->get();

        $this->assertCount(2, $tagType);
        $this->assertEquals($request->label, $tagType[0]->name);
        $this->assertEquals($request->label2, $tagType[1]->name);
    }

    public function testUpdateTagTypeForType2WithBlankValue()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->label = 'keyword';
        $request->label2 = '';

        TagType::updateTagType($request, $this->tag);
        $tagType = TagType::where('tag_id', $this->tag->id)->get();

        $this->assertCount(1, $tagType);
    }

    public function testDeleteSecondTagTypeReturnTrue()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'kos_rule'
            ]
        );

        $request = new Request();
        $request->id = $this->tag->id;

        $response = TagType::deleteSecondTagType($request);

        $this->assertTrue($response);
        $this->assertCount(1, $this->tag->types);
    }

    public function testDeleteSecondTagTypeWithOnlyOneTypeReturnFalse()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->id = $this->tag->id;

        $response = TagType::deleteSecondTagType($request);

        $this->assertFalse($response);
    }

    public function testDeleteSecondTagTypeWithNullTagReturnFalse()
    {
        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $this->tag->id,
                'name' => 'fac_share'
            ]
        );

        $request = new Request();
        $request->id = $this->tag->id + 1;

        $response = TagType::deleteSecondTagType($request);

        $this->assertFalse($response);
    }
}

