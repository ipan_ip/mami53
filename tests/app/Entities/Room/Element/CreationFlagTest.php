<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class CreationFlagTest extends MamiKosTestCase
{

    public function testRelationToRoom()
    {
        $room = factory(Room::class)->states(['active'])->create();
        $flag = factory(CreationFlag::class)->create([
            'designer_id' => $room->id
        ]);

        $this->assertEquals($room->name, $flag->room->name);
    }
}
