<?php

namespace App\Entities\Room\Element;

use App\Test\MamiKosTestCase;
use Exception;
use App\Entities\Media\Media;
use App\Entities\Facility\FacilityType;
use App\Entities\Room\RoomTypeFacility;
use App\Entities\Room\RoomFacility;

class TagTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $this->tag = new Tag();
    }

    public function testGetOneFac()
    {
        $tag = factory(Tag::class)->create();

        $result = $this->tag->getOneFac($tag->id);

        $expectedResult = [
            'id' => $tag->id,
            'name' => $tag->name,
            'photo_url' => $tag->photo ? $tag->photo->getMediaUrl()['real'] : null,
            'small_photo_url' => $tag->photoSmall ? $tag->photoSmall->getMediaUrl()['real'] : null,
        ];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetOneFacShouldReturnEmptyArray()
    {
        $result = $this->tag->getOneFac(null);

        $expectedResult = [];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetTopFac()
    {
        $tag = factory(Tag::class)->create(['is_top' => 1]);

        $result = $this->tag->getTopFac();

        $this->assertTrue($result->contains($tag));
    }

    public function testEnablePhotoUploadAsTrue()
    {
        $tag = factory(Tag::class)->create(['is_photo_upload_enabled' => false]);

        // Check if tag is_photo_upload_enabled false
        $this->assertFalse($tag->is_photo_upload_enabled);

        $result = $this->tag->enablePhotoUpload($tag->id, true);

        // Check if tag is_photo_upload_enabled true
        $this->assertTrue($result->is_photo_upload_enabled);
    }

    public function testDisablePhotoUploadAsFalse()
    {
        $tag = factory(Tag::class)->create(['is_photo_upload_enabled' => true]);

        // Check if tag is_photo_upload_enabled true
        $this->assertTrue($tag->is_photo_upload_enabled);

        $result = $this->tag->enablePhotoUpload($tag->id, false);

        // Check if tag is_photo_upload_enabled false
        $this->assertFalse($result->is_photo_upload_enabled);
    }

    public function testEnablePhotoUploadWithTagIdNotfoundShouldReturnFalse()
    {
        $result = $this->tag->enablePhotoUpload(rand(1, 10), true);

        $this->assertFalse($result);
    }

    public function testSetAsTopFacilityAsTrue()
    {
        $tag = factory(Tag::class)->create(['is_top' => false]);

        // Check if tag is_top false
        $this->assertFalse($tag->is_top);

        $result = $this->tag->setAsTopFacility($tag->id, true);

        // Check if tag is_top true
        $this->assertTrue($result->is_top);
    }

    public function testSetAsTopFacilityAsFalse()
    {
        $tag = factory(Tag::class)->create(['is_top' => true]);

        // Check if tag is_top true
        $this->assertTrue($tag->is_top);

        $result = $this->tag->setAsTopFacility($tag->id, false);

        // Check if tag is_top false
        $this->assertFalse($result->is_top);
    }

    public function testSetAsTopFacilityTagIdNotfoundShouldReturnFalse()
    {
        $result = $this->tag->setAsTopFacility(rand(1, 10), true);

        $this->assertFalse($result);
    }

    public function testDisplayInListWithTrueBoolean()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'AC',
                'code' => ''
            ]
        );

        $response = $tag->displayInList($tag->id, true);

        $this->assertEquals('list', $response->code);
    }

    public function testDisplayInListWithFalseBoolean()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'AC',
                'code' => 'list'
            ]
        );

        $response = $tag->displayInList($tag->id, false);

        $this->assertNull($response->code);
    }

    public function testDisplayInListWithWrongId()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'AC',
                'code' => 'list'
            ]
        );

        $response = $tag->displayInList(12450, false);

        $this->assertFalse($response);
    }

    public function testSetAsFacilityFilterWithTrueBoolean()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'wifi',
                'is_for_filter' => 0
            ]
        );

        $response = $tag->setAsFacilityFilter($tag->id, true);

        $this->assertEquals(1, $response->is_for_filter);
    }

    public function testSetAsFacilityFilterWithFalseBoolean()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'wifi',
                'is_for_filter' => 1
            ]
        );

        $response = $tag->setAsFacilityFilter($tag->id, false);

        $this->assertEquals(0, $response->is_for_filter);
    }

    public function testSetAsFacilityFilterWithWrongId()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'wifi',
                'is_for_filter' => 1
            ]
        );

        $response = $tag->setAsFacilityFilter(9999, false);

        $this->assertFalse($response);
    }

    public function testRemoveTagWithValidIdShouldReturnTrue()
    {
        $tag = factory(Tag::class)->create();

        $this->assertDatabaseHas('tag', $tag->toArray());

        $expectedResult = true;

        $result = $this->tag->remove($tag->id);
        //$this->assertSoftDeleted('tag', $tag->toArray());
        $this->assertEquals($result, $expectedResult);
        $this->assertNull(Tag::find($tag->id));
    }

    public function testRemoveTagWithWrongValueShouldReturnFalse()
    {
        $expectedResult = false;

        $result = $this->tag->remove(0);

        $this->assertEquals($result, $expectedResult);
    }

    public function testRemoveTagWithInvalidValueShouldReturnFalse()
    {
        $expectedResult = false;

        $result = $this->tag->remove(2);

        $this->assertEquals($result, $expectedResult);
    }

    public function testgetSortedFacShouldReturnValidOrder()
    {
        $tag1 = factory(Tag::class)->create([
            'is_top' => 1,
            'order' => 1
        ]);
        $tag2 = factory(Tag::class)->create([
            'is_top' => 0,
            'order' => 2
        ]);

        $expectedResult = $this->tag->with('photo', 'photoSmall', 'types')
            ->withCount('facilities')
            ->orderBy('is_top', 'desc')
            ->orderBy('order', 'asc');

        $result = $this->tag->getSortedFac();

        $this->assertEquals($result, $expectedResult);
    }

    public function testSaveSortOrderWithValidIdShouldReturnTrue()
    {
        $tag1 = factory(Tag::class)->create([
            'id' => 1,
            'order' => 1
        ]);
        $tag2 = factory(Tag::class)->create([
            'id' => 2,
            'order' => 2
        ]);

        $this->assertDatabaseHas('tag', $tag1->toArray());
        $this->assertDatabaseHas('tag', $tag2->toArray());

        $orderData = [];
        $orderData[] = [
            'id' => $tag1->id,
            'order' => '5'
        ];
        $orderData[] = [
            'id' => $tag2->id,
            'order' => '6'
        ];

        $result = $this->tag->saveSortOrder($orderData);

        $expectedResult = true;

        $this->assertDatabaseHas('tag', $orderData[0]);
        $this->assertDatabaseHas('tag', $orderData[1]);
        $this->assertEquals($result, $expectedResult);
    }

    public function testSaveSortOrderWithInvalidId()
    {
        $this->expectException(Exception::class);

        $tag1 = factory(Tag::class)->create([
            'id' => 1,
            'order' => 1
        ]);
        $tag2 = factory(Tag::class)->create([
            'id' => 2,
            'order' => 2
        ]);

        $this->assertDatabaseHas('tag', $tag1->toArray());
        $this->assertDatabaseHas('tag', $tag2->toArray());

        $orderData = [];
        $orderData[] = [
            'id' => 3,
            'order' => '5'
        ];
        $orderData[] = [
            'id' => 4,
            'order' => '6'
        ];

        $result = $this->tag->saveSortOrder($orderData);

        // $expectedResult = false;

        // $this->assertEquals($result, $expectedResult);
        // $this->assertDatabaseMissing('tag', $orderData[0]);
        // $this->assertDatabaseMissing('tag', $orderData[1]);
    }

    public function testRelationshipToPhoto()
    {
        $media = factory(Media::class)->create([]);
        $tag = factory(Tag::class)->create([
            'photo_id' => $media->id
        ]);

        $this->assertDatabaseHas('media', $media->toArray());
        $this->assertDatabaseHas('tag', $tag->toArray());

        $this->assertEquals(1, $tag->photo->count());
        $this->assertInstanceOf(Media::class, $tag->photo);
    }

    public function testRelationshipToPhotoSmall()
    {
        $media = factory(Media::class)->create(['file_name' => 'photo small']);
        $tag = factory(Tag::class)->create([
            'small_photo_id' => $media->id
        ]);

        $this->assertDatabaseHas('media', $media->toArray());
        $this->assertDatabaseHas('tag', $tag->toArray());

        $this->assertEquals(1, $tag->photoSmall->count());
        $this->assertInstanceOf(Media::class, $tag->photoSmall);
    }

    public function testRelationshipToFacilityType()
    {
        $facility = factory(FacilityType::class)->create();
        $tag = factory(Tag::class)->create([
            'facility_type_id' => $facility->id
        ]);

        $this->assertDatabaseHas('facility_type', $facility->toArray());
        $this->assertDatabaseHas('tag', $tag->toArray());

        $this->assertEquals(1, $tag->facility_type->count());
        $this->assertInstanceOf(FacilityType::class, $tag->facility_type);
    }

    public function testRelationshipToFacilities()
    {
        $tag = factory(Tag::class)->create([]);
        $facility = factory(RoomFacility::class)->create([
            'tag_id' => $tag->id
        ]);

        $this->assertDatabaseHas('tag', $tag->toArray());
        $this->assertDatabaseHas('designer_facility', $facility->toArray());

        $result = $tag->facilities;
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $result);
    }

    public function testRelationshipToMaxRenter()
    {
        $tag = factory(Tag::class)->create([]);
        $maxRenter = factory(TagMaxRenter::class)->create([
            'tag_id' => $tag->id
        ]);

        $this->assertDatabaseHas('tag', $tag->toArray());
        $this->assertDatabaseHas('tag_max_renter', $maxRenter->toArray());

        $this->assertEquals(1, $tag->max_renter->count());
        $this->assertInstanceOf(TagMaxRenter::class, $tag->max_renter);
    }

    public function testRelationshipToTypeFacility()
    {
        $tag = factory(Tag::class)->create([]);
        $roomTypeFac = factory(RoomTypeFacility::class)->create([
            'tag_id' => $tag->id
        ]);

        $this->assertDatabaseHas('tag', $tag->toArray());
        $this->assertDatabaseHas('designer_type_facility', $roomTypeFac->toArray());

        $result = $tag->type_facilities;
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $result);
    }

    public function testRelationshipToType()
    {
        $tag = factory(Tag::class)->create([]);
        $tagType = factory(TagType::class)->create([
            'tag_id' => $tag->id
        ]);

        $this->assertDatabaseHas('tag', $tag->toArray());
        $this->assertDatabaseHas('tag_type', $tagType->toArray());

        $result = $tag->types;
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $result);
    }

    public function testgetTagRentCount()
    {
        $tag = factory(Tag::class)->create([
            'type' => 'keyword',
            'name' => 'name',
        ]);

        $expectedResult = [
            $tag->id => $tag->name
        ];

        $result = $this->tag->getTagRentCount();

        $this->assertEquals($result, $expectedResult);
    }
}
