<?php

namespace App\Entities\Room\Element;

use App\Entities\Document\Document;
use App\Entities\Media\Media;
use App\Entities\Room\RoomTerm;
use App\Test\MamiKosTestCase;

class RoomTermDocumentTest extends MamiKosTestCase
{
    public function testRelationToRoomTerm()
    {
        $roomTerm = factory(RoomTerm::class)->create();
        $roomTermDocument = factory(RoomTermDocument::class)->create(['designer_term_id' => $roomTerm->id]);

        $this->assertSame($roomTerm->id, $roomTermDocument->room_term->id);
    }

    public function testRelationToDocument()
    {
        $document = factory(Document::class)->create();
        $roomTermDocument = factory(RoomTermDocument::class)->create(['document_id' => $document->id]);

        $this->assertSame($document->id, $roomTermDocument->document->id);
    }

    public function testRelationToMedia()
    {
        $media = factory(Media::class)->create();
        $roomTermDocument = factory(RoomTermDocument::class)->create(['media_id' => $media->id]);

        $this->assertSame($media->id, $roomTermDocument->media->id);
    }

    public function testRoomDocument()
    {
        $media = factory(Media::class)->create();
        $document = factory(Document::class)->create();
        $roomTerm = factory(RoomTerm::class)->create();
        factory(RoomTermDocument::class)->create([
            'media_id' => $media->id,
            'designer_term_id' => $roomTerm->id
        ]);
        $roomTermDocument = factory(RoomTermDocument::class)->create([
            'document_id' => $document->id,
            'designer_term_id' => $roomTerm->id
        ]);

        $docs = RoomTermDocument::roomDocument($roomTerm->room_term_document);

        $this->assertContains($docs[0]['file_id'], [$media->id, $document->id]);
        $this->assertContains($docs[1]['file_id'], [$media->id, $document->id]);
    }
}
