<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class ReportTest extends MamiKosTestCase
{
    public function testReportTypeLabel(): void
    {
        $this->assertEquals(
            [
                [
                    'type' => 'photo',
                    'name' => 'Foto tidak sesuai (depan/bangunan/kamar/kamar mandi)'
                ],
                [
                    'type' => 'address',
                    'name' => 'Alamat lengkap tidak sesuai'
                ],
                [
                    'type' => 'phone',
                    'name' => 'Nomor telepon tidak bisa dihubungi'
                ],
                [
                    'type' => 'price',
                    'name' => 'Harga tidak sesuai'
                ],
                [
                    'type' => 'facility',
                    'name' => 'Fasilitas tidak sesuai'
                ],
                [
                    'type' => 'other',
                    'name' => 'Laporkan lainnya'
                ]
            ],
            Report::REPORT_TYPE_LABEL
        );
    }

    public function testReportStatusNew(): void
    {
        $this->assertEquals('new', Report::REPORT_STATUS_NEW);
    }

    public function testReportStatusFollowedUp(): void
    {
        $this->assertEquals('followed_up', Report::REPORT_STATUS_FOLLOWED_UP);
    }

    public function testRoom(): void
    {
        $kost = factory(Room::class)->create();
        $report = factory(Report::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $report->room->is($kost)
        );
    }

    public function testChildren(): void
    {
        $report = factory(Report::class)->create();
        $children = factory(Report::class)->create(['report_parent_id' => $report->id]);

        $this->assertTrue(
            $report->children->first()->is($children)
        );
    }

    public function testUser(): void
    {
        $user = factory(User::class)->create();
        $report = factory(Report::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $report->user->is($user)
        );
    }
}
