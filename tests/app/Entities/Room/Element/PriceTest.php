<?php

namespace App\Entities\Room\Element;

use App\Entities\Booking\BookingDiscount;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class PriceTest extends MamiKosTestCase
{
    
    const ROOM_LATITUDE = -8.014663865095645;
    const ROOM_LONGITUDE = 110.2936449049594;

    const PRICE_DAILY = 100000;
    const PRICE_WEEKLY = 500000;
    const PRICE_MONTHLY = 1500000;
    const PRICE_QUARTERLY = 4500000;
    const PRICE_SEMIANNUALLY = 7000000;
    const PRICE_ANNUALLY = 15000000;

    protected function setUp(): void
    {
        parent::setUp();
        // reset memoization
        FlashSale::$LandingsOfCurrentlyRunningData = null;
    }

    public function testGetOtherDiscountUsingMonthlyRentType()
    {
        $rentTypeIndex = 2;
        $this->setRoomWithDiscounts();
        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingMonthlyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 2;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "30%",
                    "rent_type_unit" => "hari",
                    "order" => 1
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "minggu",
                    "order" => 2
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "3 bulan",
                    "order" => 4
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "6 bulan",
                    "order" => 5
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "tahun",
                    "order" => 6
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetOtherDiscountUsingDailyRentType()
    {
        $rentTypeIndex = 0;
        $this->setRoomWithDiscounts();
        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingDailyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 0;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "10%",
                    "rent_type_unit" => "minggu",
                    "order" => 2
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "bulan",
                    "order" => 3
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "3 bulan",
                    "order" => 4
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "6 bulan",
                    "order" => 5
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "tahun",
                    "order" => 6
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetOtherDiscountUsingWeeklyRentType()
    {
        $rentTypeIndex = 1;
        $this->setRoomWithDiscounts();
        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingWeeklyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 1;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "30%",
                    "rent_type_unit" => "hari",
                    "order" => 1
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "bulan",
                    "order" => 3
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "3 bulan",
                    "order" => 4
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "6 bulan",
                    "order" => 5
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "tahun",
                    "order" => 6
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetOtherDiscountUsingAnnuallyRentType()
    {
        $rentTypeIndex = 3;
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingAnnuallyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 3;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "30%",
                    "rent_type_unit" => "hari",
                    "order" => 1
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "minggu",
                    "order" => 2
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "bulan",
                    "order" => 3
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "3 bulan",
                    "order" => 4
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "6 bulan",
                    "order" => 5
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetOtherDiscountUsingQuarterlyRentType()
    {
        $rentTypeIndex = 4;
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingQuarterlyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 4;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "30%",
                    "rent_type_unit" => "hari",
                    "order" => 1
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "minggu",
                    "order" => 2
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "bulan",
                    "order" => 3
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "6 bulan",
                    "order" => 5
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "tahun",
                    "order" => 6
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetOtherDiscountUsingSemiAnnuallyRentType()
    {
        $rentTypeIndex = 5;
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(0, count($otherDiscounts));
    }

    public function testGetOtherDiscountUsingSemiAnnuallyRentTypeWithRunningFlashSale()
    {
        $rentTypeIndex = 5;
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $otherDiscounts = $price->getOtherDiscounts($rentTypeIndex);

        $this->assertEquals(5, count($otherDiscounts));
        $this->assertEquals(
            [
                [
                    "discount" => "30%",
                    "rent_type_unit" => "hari",
                    "order" => 1
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "minggu",
                    "order" => 2
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "bulan",
                    "order" => 3
                ],
                [
                    "discount" => "5%",
                    "rent_type_unit" => "3 bulan",
                    "order" => 4
                ],
                [
                    "discount" => "10%",
                    "rent_type_unit" => "tahun",
                    "order" => 6
                ]
            ],
            $otherDiscounts
        );
    }

    public function testGetPrice()
    {
        $this->setRoomWithDiscounts();
        $room = Room::first();
        $price = new Price($room);

        $this->assertEquals(self::PRICE_DAILY, $price->getPrice(price::STRING_DAILY));
        $this->assertEquals(self::PRICE_WEEKLY, $price->getPrice(price::STRING_WEEKLY));
        $this->assertEquals(self::PRICE_MONTHLY, $price->getPrice(price::STRING_MONTHLY));
        $this->assertEquals(self::PRICE_QUARTERLY, $price->getPrice(price::STRING_QUARTERLY));
        $this->assertEquals(self::PRICE_SEMIANNUALLY, $price->getPrice(price::STRING_SEMIANNUALLY));
        $this->assertEquals(self::PRICE_ANNUALLY, $price->getPrice(price::STRING_YEARLY));
        $this->assertEquals(self::PRICE_ANNUALLY, $price->getPrice(price::STRING_ANNUALLY));
        $this->assertEquals(0, $price->getPrice(''));
    }

    public function testGetPriceType()
    {
        $this->setRoomWithDiscounts();
        $room = Room::first();
        $price = new Price($room);

        $this->assertEquals('Harian', $price->getPriceType(price::STRING_DAILY));
        $this->assertEquals('Mingguan', $price->getPriceType(price::STRING_WEEKLY));
        $this->assertEquals('Bulanan', $price->getPriceType(price::STRING_MONTHLY));
        $this->assertEquals('3 Bulanan', $price->getPriceType(price::STRING_QUARTERLY));
        $this->assertEquals('6 Bulanan', $price->getPriceType(price::STRING_SEMIANNUALLY));
        $this->assertEquals('Tahunan', $price->getPriceType(price::STRING_YEARLY));
        $this->assertEquals('Tahunan', $price->getPriceType(price::STRING_ANNUALLY));
        $this->assertEquals('', $price->getPriceType(''));
    }

    public function testPriceTitleFormatMonthly()
    {
        $rentTypeIndex = '2';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 2500000,
                "rent_type_unit" => "bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatMonthlyWithFlashSale()
    {
        $rentTypeIndex = '2';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 3000000,
                "discount_price" => 2700000,
                "discount" => 10,
                "rent_type_unit" => "bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatDaily()
    {
        $rentTypeIndex = '0';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 100000,
                "rent_type_unit" => "hari",
            ],
            $response
        );
    }

    public function testPriceTitleFormatDailyWithFlashSale()
    {
        $rentTypeIndex = '0';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 150000,
                "discount_price" => 105000,
                "discount" => 30,
                "rent_type_unit" => "hari",
            ],
            $response
        );
    }

    public function testPriceTitleFormatWeekly()
    {
        $rentTypeIndex = '1';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 750000,
                "rent_type_unit" => "minggu",
            ],
            $response
        );
    }

    public function testPriceTitleFormatWeeklyWithFlashSale()
    {
        $rentTypeIndex = '1';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 937500,
                "discount_price" => 843750,
                "discount" => 10,
                "rent_type_unit" => "minggu",
            ],
            $response
        );
    }

    public function testPriceTitleFormatAnnually()
    {
        $rentTypeIndex = '3';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 15000000,
                "rent_type_unit" => "tahun",
            ],
            $response
        );
    }

    public function testPriceTitleFormatAnnuallyWithFlashSale()
    {
        $rentTypeIndex = '3';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 18000000,
                "discount_price" => 16200000,
                "discount" => 10,
                "rent_type_unit" => "tahun",
            ],
            $response
        );
    }

    public function testPriceTitleFormatQuarterly()
    {
        $rentTypeIndex = '4';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 5000000,
                "rent_type_unit" => "3 bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatQuarterlyWithFlashSale()
    {
        $rentTypeIndex = '4';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 5500000,
                "discount_price" => 5225000,
                "discount" => 5,
                "rent_type_unit" => "3 bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatSemiannually()
    {
        $rentTypeIndex = '5';
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 10000000,
                "rent_type_unit" => "6 bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatSemiannuallyWithFlashSale()
    {
        $rentTypeIndex = '5';
        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData($rentTypeIndex);

        $room = Room::with('discounts')->first();

        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 11000000,
                "discount_price" => 10450000,
                "discount" => 5,
                "rent_type_unit" => "6 bulan",
            ],
            $response
        );
    }

    public function testGetPriceTypeByIndex()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(price::STRING_DAILY, $price::getPriceTypeByIndex(0));
        $this->assertEquals(price::STRING_WEEKLY, $price::getPriceTypeByIndex(1));
        $this->assertEquals(price::STRING_MONTHLY, $price::getPriceTypeByIndex(2));
        $this->assertEquals(price::STRING_ANNUALLY, $price::getPriceTypeByIndex(3));
        $this->assertEquals(price::STRING_QUARTERLY, $price::getPriceTypeByIndex(4));
        $this->assertEquals(price::STRING_SEMIANNUALLY, $price::getPriceTypeByIndex(5));
    }

    public function testGetPriceIndexByType()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(0, $price::getPriceIndexByType(price::STRING_DAILY));
        $this->assertEquals(1, $price::getPriceIndexByType(price::STRING_WEEKLY));
        $this->assertEquals(2, $price::getPriceIndexByType(price::STRING_MONTHLY));
        $this->assertEquals(3, $price::getPriceIndexByType(price::STRING_ANNUALLY));
        $this->assertEquals(4, $price::getPriceIndexByType(price::STRING_QUARTERLY));
        $this->assertEquals(5, $price::getPriceIndexByType(price::STRING_SEMIANNUALLY));
    }

    public function testGetOriginalPriceByDiscount()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(100000, $price->getOriginalPriceByDiscount(0));
        $this->assertEquals(750000, $price->getOriginalPriceByDiscount(1));
        $this->assertEquals(2500000, $price->getOriginalPriceByDiscount(2));
        $this->assertEquals(15000000, $price->getOriginalPriceByDiscount(3));
        $this->assertEquals(5000000, $price->getOriginalPriceByDiscount(4));
        $this->assertEquals(10000000, $price->getOriginalPriceByDiscount(5));
    }

    public function testGetDiscountPercentage()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(30, $price->getDiscountPercentage(Price::STRING_DAILY));
        $this->assertEquals(10, $price->getDiscountPercentage(Price::STRING_WEEKLY));
        $this->assertEquals(10, $price->getDiscountPercentage(Price::STRING_MONTHLY));
        $this->assertEquals(10, $price->getDiscountPercentage(Price::STRING_ANNUALLY));
        $this->assertEquals(5, $price->getDiscountPercentage(Price::STRING_QUARTERLY));
        $this->assertEquals(5, $price->getDiscountPercentage(Price::STRING_SEMIANNUALLY));
    }

    public function testGetDiscountPercentageWithNominalDiscountType()
    {
        factory(Room::class)
            ->create()
            ->each(
                function ($room) {
                    // Monthly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->make(
                            [
                                'price' => 2500000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'nominal',
                                'discount_value' => 250000,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Quarterly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('quarterly')->make(
                            [
                                'price' => 5000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'nominal',
                                'discount_value' => 250000,
                                'is_active' => 1
                            ]
                        )
                    );
                }
            );

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(9, $price->getDiscountPercentage(Price::STRING_MONTHLY));
        $this->assertEquals(5, $price->getDiscountPercentage(Price::STRING_QUARTERLY));
    }

    public function testGetDiscountPercentageWithNoDiscountData()
    {
        $this->setRoomWithoutDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertNull($price->getDiscountPercentage(Price::STRING_DAILY));
        $this->assertNull($price->getDiscountPercentage(Price::STRING_WEEKLY));
        $this->assertNull($price->getDiscountPercentage(Price::STRING_MONTHLY));
        $this->assertNull($price->getDiscountPercentage(Price::STRING_ANNUALLY));
        $this->assertNull($price->getDiscountPercentage(Price::STRING_QUARTERLY));
        $this->assertNull($price->getDiscountPercentage(Price::STRING_SEMIANNUALLY));
    }

    public function testGetListingPriceWithoutDiscount()
    {
        $this->setRoomWithoutDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(self::PRICE_DAILY, $price->getListingPrice(Price::STRING_DAILY));
        $this->assertEquals(self::PRICE_WEEKLY, $price->getListingPrice(Price::STRING_WEEKLY));
        $this->assertEquals(self::PRICE_MONTHLY, $price->getListingPrice(Price::STRING_MONTHLY));
        $this->assertEquals(self::PRICE_ANNUALLY, $price->getListingPrice(Price::STRING_ANNUALLY));
        $this->assertEquals(self::PRICE_QUARTERLY, $price->getListingPrice(Price::STRING_QUARTERLY));
        $this->assertEquals(self::PRICE_SEMIANNUALLY, $price->getListingPrice(Price::STRING_SEMIANNUALLY));
    }

    public function testGetListingPriceWithDiscount()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(105000, $price->getListingPrice(Price::STRING_DAILY));
        $this->assertEquals(843750, $price->getListingPrice(Price::STRING_WEEKLY));
        $this->assertEquals(2700000, $price->getListingPrice(Price::STRING_MONTHLY));
        $this->assertEquals(16200000, $price->getListingPrice(Price::STRING_ANNUALLY));
        $this->assertEquals(5225000, $price->getListingPrice(Price::STRING_QUARTERLY));
        $this->assertEquals(10450000, $price->getListingPrice(Price::STRING_SEMIANNUALLY));
    }

    public function testHasDiscountIsExist()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(1, $price->hasDiscount(PRICE::STRING_DAILY));
        $this->assertEquals(1, $price->hasDiscount(PRICE::STRING_WEEKLY));
        $this->assertEquals(1, $price->hasDiscount(PRICE::STRING_MONTHLY));
    }

    public function testHasDiscountIsNotExist()
    {
        $this->setRoomWithoutDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertEquals(0, $price->hasDiscount(PRICE::STRING_YEARLY));
    }

    public function testGetDiscountIsExist()
    {
        $this->setRoomWithDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $response1 = $price->getDiscount(PRICE::STRING_DAILY);
        $response2 = $price->getDiscount(PRICE::STRING_WEEKLY);
        $response3 = $price->getDiscount(PRICE::STRING_MONTHLY);

        $this->assertInstanceOf(BookingDiscount::class, $response1);
        $this->assertInstanceOf(BookingDiscount::class, $response2);
        $this->assertInstanceOf(BookingDiscount::class, $response3);
    }

    public function testGetDiscountIsNotExist()
    {
        $this->setRoomWithoutDiscounts();

        $room = Room::with('discounts')->first();
        $price = new Price($room);

        $this->assertNull($price->getDiscount(PRICE::STRING_YEARLY));
    }

    public function testGetDiscountByPriceType()
    {
        $room = factory(Room::class)->create(['price_monthly' => self::PRICE_MONTHLY]);
        $room->discounts()->save($this->makeBookingDiscountMonthly());
        
        $room = Room::with('discounts')->find($room->id);
        $price = new Price($room);

        $this->assertEquals(2500000, $price->getDiscountByPriceType(price::STRING_MONTHLY)['price']);
    }

    public function testGetMarkupPrice()
    {
        $room = factory(Room::class)->create(['price_monthly' => self::PRICE_MONTHLY]);
        $room->discounts()->save($this->makeBookingDiscountMonthly());
        $price = new Price($room);

        $this->assertEquals(3000000, $price->getMarkupPrice(price::STRING_MONTHLY));
        $this->assertEquals(null, $price->getMarkupPrice(price::STRING_YEARLY));
    }

    /*
    * Private helper methods
    */

    private function makeBookingDiscountMonthly()
    {
        return factory(BookingDiscount::class)->make([
            'price' => 2500000,
            'markup_type' => 'percentage',
            'markup_value' => 20,
            'discount_type' => 'percentage',
            'discount_value' => 10,
            'is_active' => 1
        ]);
    }

    private function setRoomWithoutDiscounts(): void
    {
        factory(Room::class)
            ->create(
                [
                    "latitude" => self::ROOM_LATITUDE,
                    "longitude" => self::ROOM_LONGITUDE
                ]
            )
            ->each(
                function ($room) {
                    // Price elements
                    $room->price_daily = self::PRICE_DAILY;
                    $room->price_weekly = self::PRICE_WEEKLY;
                    $room->price_monthly = self::PRICE_MONTHLY;
                    $room->price_yearly = self::PRICE_ANNUALLY;
                    $room->price_quarterly = self::PRICE_QUARTERLY;
                    $room->price_semiannually = self::PRICE_SEMIANNUALLY;
                    $room->save();
                }
            );
    }

    private function setRoomWithDiscounts(): void
    {
        factory(Room::class)
            ->create(
                [
                    "latitude" => self::ROOM_LATITUDE,
                    "longitude" => self::ROOM_LONGITUDE
                ]
            )
            ->each(
                function ($room) {
                    // Price elements
                    $room->price_daily = self::PRICE_DAILY;
                    $room->price_weekly = self::PRICE_WEEKLY;
                    $room->price_monthly = self::PRICE_MONTHLY;
                    $room->price_yearly = self::PRICE_ANNUALLY;
                    $room->price_quarterly = self::PRICE_QUARTERLY;
                    $room->price_semiannually = self::PRICE_SEMIANNUALLY;
                    $room->save();

                    // Monthly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->make(
                            [
                                'price' => 2500000,
                                'markup_type' => 'percentage',
                                'markup_value' => 20,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Daily discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('daily')->make(
                            [
                                'price' => 100000,
                                'markup_type' => 'percentage',
                                'markup_value' => 50,
                                'discount_type' => 'percentage',
                                'discount_value' => 30,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Weekly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('weekly')->make(
                            [
                                'price' => 750000,
                                'markup_type' => 'percentage',
                                'markup_value' => 25,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Annually discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('annually')->make(
                            [
                                'price' => 15000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 20,
                                'discount_type' => 'percentage',
                                'discount_value' => 10,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Quarterly discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('quarterly')->make(
                            [
                                'price' => 5000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'percentage',
                                'discount_value' => 5,
                                'is_active' => 1
                            ]
                        )
                    );

                    // Semi-annually discount
                    $room->discounts()->save(
                        factory(BookingDiscount::class)->state('semiannually')->make(
                            [
                                'price' => 10000000,
                                'markup_type' => 'percentage',
                                'markup_value' => 10,
                                'discount_type' => 'percentage',
                                'discount_value' => 5,
                                'is_active' => 1
                            ]
                        )
                    );
                }
            );
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 10.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }

    public function testGetAdditionalPrice()
    {
        $room = factory(Room::class)->states('active')->create();
        $price = new Price($room);

        $this->assertEquals(0, $price->getAdditionalPrice(2));
    }

    public function testGetAdditionalPriceWithData()
    {
        $room = factory(Room::class)->states('active')->create();

        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'price' => 500000,
            'is_active' => 1,
        ]);

        $price = new Price($room);
        $this->assertEquals(500000, $price->getAdditionalPrice(2));
    }

    public function testGetAdditionalPriceWithDataForYearly()
    {
        $room = factory(Room::class)->states('active')->create();

        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'price' => 500000,
            'is_active' => 1,
        ]);

        $price = new Price($room);
        $this->assertEquals(500000 * 12, $price->getAdditionalPrice(3));
    }

    public function testPriceTitleFormatWithAdditionalMonthly()
    {
        // dump data
        $this->setRoomWithDiscounts();
        $room = Room::with('discounts')->first();
        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'price' => 500000,
            'is_active' => 1,
        ]);

        // call method
        $rentTypeIndex = '2'; // monthly
        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        // assert
        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 3000000, // 2.500.000 (self::PRICE_MONTHLY) + 500.000 (additional)
                "rent_type_unit" => "bulan",
            ],
            $response
        );
    }

    public function testPriceTitleFormatWithAdditionalYearly()
    {
        // dump data
        $this->setRoomWithDiscounts();
        $room = Room::with('discounts')->first();
        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'price' => 500000,
            'is_active' => 1,
        ]);

        // call method
        $rentTypeIndex = '3'; // yearly
        $price = new Price($room);
        $response = $price->priceTitleFormats($rentTypeIndex);

        // assert
        self::assertEquals(
            [
                "currency_symbol" => "Rp",
                "price" => 21000000, // 15.000.000 (self::PRICE_ANNUALLY) + (500.000*12)
                "rent_type_unit" => "tahun",
            ],
            $response
        );
    }

    /**
     * @group PMS-495
     */   
    public function testTagPriceMonthly()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 1000000
        ]);

        $price = new Price($room);
        $this->assertEquals($price->tag(),'eksklusif');
    }

    /**
     * @group PMS-495
     */   
    public function testTagPriceYearly()
    {
        $room = factory(Room::class)->create([
            'price_yearly' => 12000000
        ]);

        $price = new Price($room);
        $this->assertEquals($price->tag(),'eksklusif');
    }

    /**
     * @group PMS-495
     */   
    public function testTagPriceWeekly()
    {
        $room = factory(Room::class)->create([
            'price_weekly' => 250000
        ]);

        $price = new Price($room);
        $this->assertEquals($price->tag(),'eksklusif');
    }

    /**
     * @group PMS-495
     */   
    public function testTagPriceMurah()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 1000000-1,
            'price_yearly' => 12000000-1,
            'price_weekly' => 250000-1,
        ]);

        $price = new Price($room);
        $this->assertEquals($price->tag(),'murah');
    }
}
