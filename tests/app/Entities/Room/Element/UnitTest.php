<?php

namespace App\Entities\Room\Element;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class UnitTest extends MamiKosTestCase
{
    use WithFaker;

    public function testTypeRelation()
    {
        $type = factory(Type::class)->create();
        $unit = factory(Unit::class)->create([
            'designer_type_id' => $type->id
        ]);

        $result = $unit->type;
        $this->assertEquals($result->id, $type->id);
    }
}
