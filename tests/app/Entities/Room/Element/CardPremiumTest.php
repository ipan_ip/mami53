<?php

namespace App\Entities\Room\Element;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Card\GroupType;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class CardPremiumTest extends MamiKosTestCase
{
    use WithFaker;

    public function testShowList()
    {
        $rows = $this->faker->numberBetween(2, 5);
        $photoId = $this->faker->numberBetween(10000, 99999);

        // Cover photo
        $coverCard = factory(CardPremium::class)->create(
            [
                'photo_id' => $photoId,
                'description' => 'This should be a -cover'
            ]
        );

        factory(Room::class)
            ->create()
            ->each(
                function ($room) use ($rows, $photoId) {
                    $cards = factory(CardPremium::class, $rows)->create();
                    $room->cards()->saveMany($cards);
                }
            );

        $room = Room::first();
        $room->premium_cards()->save($coverCard);

        $response = CardPremium::showList($room);

        $this->assertEquals($rows + 1, count($response));
        $this->assertEquals($photoId, $response[0]['photo_id']);
    }

    public function testShowListWithSingleCard()
    {
        $photoId = $this->faker->numberBetween(10000, 99999);

        // Cover photo
        $coverCard = factory(CardPremium::class)->create(
            [
                'photo_id' => $photoId,
                'description' => 'This should be a -cover'
            ]
        );

        $room = factory(Room::class)->create();
        $room->premium_cards()->save($coverCard);

        $response = CardPremium::showList($room);

        $this->assertEquals(1, count($response));
        $this->assertEquals($photoId, $response[0]['photo_id']);
    }

    public function testShowListWithNoCards()
    {
        $room = factory(Room::class)->create();
        $response = CardPremium::showList($room);
        $this->assertEquals(1, count($response));
        $dummy = ApiHelper::getDummyImage($room->name);
        $this->assertEquals($response[0], $dummy);
    }

    public function testGetAllFunctionSuccess()
    {
        $ammount =  5;
        $room = factory(Room::class)->create();
        factory(CardPremium::class, $ammount)->make([
            'designer_id' => $room->id,
            'description' => 'This should be a -cover'
        ])->each(function ($card) {
            $card->photo_id = $this->faker->numberBetween(10000, 99999);
            $card->save();
        });

        $result = (new CardPremium())->getAll($room);

        $this->assertEquals($ammount, count($result));
    }

    public function testGetCoverPhotoIdSuccess()
    {
        $ammount =  3;
        $room = factory(Room::class)->create();
        factory(CardPremium::class, $ammount)->make([
            'designer_id' => $room->id,
            'description' => 'this-is-foto-kamar-mandi'
        ])->each(function ($card) {
            $card->photo_id = $this->faker->numberBetween(10000, 99999);
            $card->save();
        });
        $photoId = $this->faker->numberBetween(10000, 99999);
        factory(CardPremium::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photoId,
            'description' => 'this-is-a-cover'
        ]);

        $result = (new CardPremium())->getCoverPhotoId($room);

        $this->assertEquals($result, $photoId);
    }

    public function testGetCoverPhotoIdNoCards()
    {
        $room = factory(Room::class)->create();
        $result = (new CardPremium())->getCoverPhotoId($room);
        $this->assertNull($result);
    }

    public function testGetIsCoverable()
    {
        $this->assertFalse(CardPremium::getIsCoverable('uncoverable kamar mandi'));
        $this->assertTrue(CardPremium::getIsCoverable('uncoverable kamar'));
    }

    public function testSanitizeCoverDescription()
    {
        $description = 'foto-foto-kamar-utama';
        $result = CardPremium::sanitizeCoverDescription($description);
        $this->assertEquals('foto-kamar-utama', $result);
    }

    public function testValidateCoverPhoto()
    {
        $photoId = $this->faker->numberBetween(10000, 99999);
        $room = factory(Room::class)->create([
            'photo_id' => $photoId
        ]);

        $description = 'foto-kamar';

        $card = factory(CardPremium::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photoId,
            'description' => $description
        ]);

        $valid = CardPremium::validateCoverPhoto($room);
        $this->assertTrue($valid);
    }

    public function testCategorizeCardWithoutCardIdOneZeroCard()
    {
        $room = factory(Room::class)->create();
        $result = CardPremium::categorizeCardWithoutCardIdOne($room);
        $this->assertEquals(array(), $result);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardId()
    {
        $room = factory(Room::class)->create();

        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = CardPremium::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardIdZeroCard()
    {
        $room = factory(Room::class)->create();

        $categorized = CardPremium::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardIdWith360()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_round_id' => $media->id
        ]);

        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = CardPremium::categorizeCardWithoutCardId($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }
    

    /**
     * @group PMS-495
     */
    public function testCategorizeCardWithoutCardZeroCard()
    {
        $room = factory(Room::class)->create();

        $categorized = CardPremium::categorizedCards($room, 'edit');

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }

    /**
     * @group PMS-495
     */
    public function testCategorizedCards()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create();

        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-bangunan-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-tidur-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-mandi'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });
        factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-fasilitas-utama'
        ])->each(function ($card) {
            $media = factory(Media::class)->create();
            $card->photo_id = $media->id;
            $card->save();
        });

        $categorized = CardPremium::categorizedCards($room);

        $this->assertArrayHasKey(GroupType::COVER, $categorized);
        $this->assertArrayHasKey(GroupType::MAIN, $categorized);
        $this->assertArrayHasKey(GroupType::BEDROOM, $categorized);
        $this->assertArrayHasKey(GroupType::BATHROOM, $categorized);
        $this->assertArrayHasKey(GroupType::OTHER, $categorized);
    }
}
