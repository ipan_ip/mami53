<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class VerificationTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */
    public function testRelationToRoomSuccess()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->state('owner')->create();
        $verification = factory(Verification::class)->create([
            'designer_id' => $room->id,
            'requested_by_user_id' => $user->id,
            'is_ready' => true
        ]);

        $this->assertEquals($room->id, $verification->room->id);
        $this->assertEquals($verification->id, $room->verification->id);
        $this->assertEquals($user->id, $verification->requester->id);
        $this->assertTrue($verification->isReady());
    }
}
