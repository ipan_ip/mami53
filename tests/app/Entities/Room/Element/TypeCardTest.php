<?php

namespace App\Entities\Room\Element;

use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;

class TypeCardTest extends MamiKosTestCase
{
    protected $tag;

    protected function setUp() : void
    {
        parent::setUp();

        $this->typeCard = factory(TypeCard::class)->create();
    }

    public function testRegister()
    {
        $cards = factory(TypeCard::class, 5)->make()->toArray();

        $designerId = rand(1, 100);
        $result = $this->typeCard->register($cards, $designerId);

        $this->assertTrue($result);

        foreach ($cards as $card) {
            $card = array_only($card, ['type', 'photo_id', 'description', 'source', 'ordering']);
            $this->assertDatabaseHas('designer_type_style', $card);
        }
    }

    public function testPhotoRelation()
    {
        $media = factory(Media::class)->create();
        $typeCard = factory(TypeCard::class)->create([
            'photo_id' => $media->id
        ]);

        $result = $typeCard->photo;
        $this->assertEquals($result->id, $media->id);
    }

    public function testRoomTypeRelation()
    {
        $type = factory(Type::class)->create();
        $typeCard = factory(TypeCard::class)->create([
            'designer_type_id' => $type->id
        ]);

        $result = $typeCard->room_type;
        $this->assertEquals($result->id, $type->id);
    }
}
