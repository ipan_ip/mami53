<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RoomPriceComponentTest extends MamiKosTestCase
{
    public function testPriceNameMaintenance(): void
    {
        $this->assertEquals('maintenance', RoomPriceComponent::PRICE_NAME_MAINTENANCE);
    }

    public function testPriceNameParking(): void
    {
        $this->assertEquals('parking', RoomPriceComponent::PRICE_NAME_PARKING);
    }

    public function testRoom(): void
    {
        $kost = factory(Room::class)->create();
        $component = factory(RoomPriceComponent::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $component->room->is($kost)
        );
    }
}
