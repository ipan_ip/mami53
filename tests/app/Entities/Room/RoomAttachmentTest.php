<?php

namespace App\Entities\RoomAttachment;

use App\Entities\Room\Room;
use App\Entities\Room\RoomAttachment;
use App\Test\MamiKosTestCase;

class RoomAttachmentTest extends MamiKosTestCase
{
    public function testGetActiveMatterportIdWithIsMatterportActiveIsFalse()
    {
        $room = factory(Room::class)->create();
        $roomAtttachment = factory(RoomAttachment::class)->state('inactive')->create([
            'designer_id' => $room->id
        ]);
        $this->assertNull($roomAtttachment->getActiveMatterportId());
    }

    public function testGetActiveMatterportIdWithIsMatterportActiveIsTrue()
    {
        $room = factory(Room::class)->create();
        $roomAtttachment = factory(RoomAttachment::class)->state('active')->create([
            'designer_id' => $room->id
        ]);

        $result = $roomAtttachment->getActiveMatterportId();
        $this->assertEquals($roomAtttachment->matterport_id, $result);
    }

    public function testRoomRelation()
    {
        $room = factory(Room::class)->create();
        $roomAtttachment = factory(RoomAttachment::class)->state('inactive')->create([
            'designer_id' => $room->id
        ]);

        $this->assertEquals($room->id, $roomAtttachment->room->id);
    }

    public function testGetVRTourLinkWithIsMatterportActiveIsFalse()
    {
        $room = factory(Room::class)->create();
        $roomAtttachment = factory(RoomAttachment::class)->state('inactive')->create([
            'designer_id' => $room->id
        ]);

        $result = $roomAtttachment->getVRTourLink();

        $this->assertEmpty($result);
    }

    public function testGetVRTourLinkWithIsMatterportActiveIsTrue()
    {
        $room = factory(Room::class)->create();
        $roomAtttachment = factory(RoomAttachment::class)->state('active')->create([
            'designer_id' => $room->id
        ]);

        $result = $roomAtttachment->getVRTourLink();
        $expectedResult = env('APP_URL') . '/vrtour/matterport/' . $roomAtttachment->matterport_id;

        $this->assertEquals($expectedResult, $result);
    }
}
