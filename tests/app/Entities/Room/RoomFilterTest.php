<?php

namespace App\Entities\Room;

use App\Entities\Landing\Landing;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Media\Media;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\User\UserChecker;
use App\Entities\User\UserCheckerTracker;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\DB;

class RoomFilterTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    public function testDoFilter()
    {
        $room = factory(Room::class)->create();

        $filter = new RoomFilter();
        $response = $filter->doFilter($room);

        $expectedWheres = [
            [
                "type" => "Basic",
                "column" => "is_active",
                "operator" => "=",
                "value" => "true",
                "boolean" => "and"
            ],
            [
                "type" => "Basic",
                "column" => "expired_phone",
                "operator" => "=",
                "value" => 0,
                "boolean" => "and"
            ],
            [
                "type" => "NotNull",
                "column" => "room_available",
                "boolean" => "and"
            ],
            [
                "type" => "Null",
                "column" => "apartment_project_id",
                "boolean" => "and"
            ],
            [
                "type" => "Basic",
                "column" => "price_monthly",
                "operator" => "!=",
                "value" => 0,
                "boolean" => "and"
            ]
        ];

        $this->assertEquals($expectedWheres, $response->getQuery()->wheres);
        $this->assertInstanceOf(Builder::class, $response);
        $this->assertInstanceOf(Room::class, $response->getModel());
    }

    public function testDoFilterWithNonActiveRoom()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'false',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter();
        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithExpiredPhone()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 1,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter();
        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithUnavailableRooms()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => null,
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter();
        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredSongId()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'ids' => [
                    $room->song_id
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithExcludedSongId()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 123456,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'exclude' => [
                    654321
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredAreaEmpty()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 123456,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'area' => []
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredAreaOtherCity()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 123456,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'area_city' => ""
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'area' => [
                    'Lainnya'
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredArea()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 123456,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'area_city' => "Sleman",
                'area_subdistrict' => "Depok"
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'place' => [
                    'Depok',
                    'Sleman'
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredAreaUsingEmptyArea()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 123456,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'area_city' => "Sleman",
                'area_subdistrict' => "Depok"
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'place' => []
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPropertyTypeKos()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'property_type' => 'kost'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPropertyTypeApartment()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'property_type' => 'apartment'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPropertyTypeApartmentAndTestingData()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'name' => 'Apartment 123 Mamitest'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'property_type' => 'apartment'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredUnitType()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'unit_type' => '2 BR'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'property_type' => 'apartment',
                'unit_type' => 3 // This should represents '2 BR' unit type
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredUnitType_String()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 987654321,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'unit_type' => '2 BR'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'ids' => [987654321],
                'property_type' => 'apartment',
                'unit_type' => '2 BR' // This should represents '2 BR' unit type
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredUnitType_StringLainnya()
    {
        $room = factory(Room::class)->create(
            [
                'song_id' => 987654321,
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'unit_type' => '7 BR'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'ids' => [987654321],
                'property_type' => 'apartment',
                'unit_type' => '7 BR' // This should represents '2 BR' unit type
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredUnitTypeWithInvalidIndex()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => $this->faker->numberBetween(100, 999),
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'unit_type' => '2 BR'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'property_type' => 'apartment',
                'unit_type' => 1 // <- code for unit_type = "1-Room Studio"
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFurnishedReturnAll()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'furnished' => 'all'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFurnishedReturnTypeZeroOrNull()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'furnished' => 2
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'furnished' => 0
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFurnishedReturnTypeNonZero()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'furnished' => 2
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'furnished' => 2
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredLocationUsingInvalidLocation()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'location' => [
                    [0, 0],
                    [0, 0]
                ]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredLocationUsingInvalidUserLocation()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'user_location' => [0, 0]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredLocationUsingValidLocation()
    {
        $fakeLatitude = $this->faker->randomFloat(8, -6.00000000, -7.00000000);
        $fakeLongitude = $this->faker->randomFloat(8, 106.00000000, 107.00000000);

        $fakeLatitude2 = $fakeLatitude + 0.5;
        $fakeLongitude2 = $fakeLongitude + 0.5;

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'latitude' => $fakeLatitude + 0.25,
                'longitude' => $fakeLongitude + 0.25
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'location' => [
                    [
                        $fakeLongitude,
                        $fakeLatitude
                    ],
                    [
                        $fakeLongitude2,
                        $fakeLatitude2
                    ]
                ]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredLocationUsingValidUserLocation()
    {
        $fakeLatitude = $this->faker->randomFloat(8, -6.00000000, -7.00000000);
        $fakeLongitude = $this->faker->randomFloat(8, 106.00000000, 107.00000000);

        $fakeLatitude2 = $fakeLatitude + 0.5;
        $fakeLongitude2 = $fakeLongitude + 0.5;

        // Required Landing data
        factory(Landing::class)->create(
            [
                'latitude_1' => $fakeLatitude,
                'longitude_1' => $fakeLongitude,
                'latitude_2' => $fakeLatitude2,
                'longitude_2' => $fakeLongitude2,
                'type' => 'area',
                'rent_type' => RoomFilter::RENT_TYPE_MONTH,
                'parent_id' => null
            ]
        );

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'latitude' => $fakeLatitude + 0.25,
                'longitude' => $fakeLongitude + 0.25
            ]
        );

        $this->generatePriceFilter($room);

        // Using key "user_location"
        $filter = new RoomFilter(
            [
                'user_location' => [
                    $fakeLongitude + 0.3,
                    $fakeLatitude + 0.3
                ]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredLocationUsingValidUserLocationButNoMatchedLanding()
    {
        $fakeLatitude = $this->faker->randomFloat(8, -6.00000000, -7.00000000);
        $fakeLongitude = $this->faker->randomFloat(8, 106.00000000, 107.00000000);

        $fakeLatitude2 = $fakeLatitude + 0.5;
        $fakeLongitude2 = $fakeLongitude + 0.5;

        // Required Landing data
        factory(Landing::class)->create(
            [
                'latitude_1' => $fakeLatitude,
                'longitude_1' => $fakeLongitude,
                'latitude_2' => $fakeLatitude2,
                'longitude_2' => $fakeLongitude2,
                'type' => 'area',
                'rent_type' => RoomFilter::RENT_TYPE_MONTH,
                'parent_id' => null
            ]
        );

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'latitude' => $fakeLatitude + 0.25,
                'longitude' => $fakeLongitude + 0.25
            ]
        );

        $this->generatePriceFilter($room);

        // Using key "user_location"
        $filter = new RoomFilter(
            [
                'user_location' => [
                    $fakeLongitude + 10,
                    $fakeLatitude + 10
                ]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredGender()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000),
                'gender' => 1
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'gender' => [0, 1]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredRentType()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => $this->faker->numberBetween(100000, 1000000)
            ]
        );

        // yearly price is zero
        factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_monthly' => $room->price_monthly,
                'final_price_yearly' => 0
            ]
        );

        $filter = new RoomFilter(
            [
                'rent_type' => 3
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredPriceRange()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => 4,
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'rent_type' => 2,
                'price_range' => [
                    2000000,
                    15000000
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }


    public function testDoFilterWithFilteredPriceRangeAndDailyRentTypeGetEmptyRoom()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'price_daily' => 50000
            ]
        );

        factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_daily' => $room->price_daily
            ]
        );

        $filter = new RoomFilter(
            [
                'rent_type' => 0,
                'price_range' => [
                    60000,
                    10000000
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredPriceRangeAndDailyRentTypeGetRoom()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'price_daily'   => 50000
            ]
        );

        factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_daily' => 50000
            ]
        );

        $filter = new RoomFilter(
            [
                'rent_type' => 0,
                'price_range' => [
                    40000,
                    100000
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPriceRangeWithEmptyPrices()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'rent_type' => 2,
                'price_range' => []
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPriceRangeWithEmptyRentType()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'price_range' => [
                    0,
                    15000000
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredPriceRangeWithPriceQuarterly()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_quarterly' => 3000000
            ]
        );

        factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_monthly' => 1000000,
                'final_price_quarterly' => 3000000
            ]
        );

        $filter = new RoomFilter(
            [
                'rent_type' => 4,
                'price_range' => [
                    2000000,
                    15000000
                ]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredClass()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'class' => 100
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'class' => 99
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredTopOwner()
    {
        $user = factory(User::class)->create(
            [
                'is_top_owner' => 'true'
            ]
        );

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $user->id
            ]
        );

        $filter = new RoomFilter(
            [
                'top_owner' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredTags()
    {
        $tags = factory(Tag::class, 5)->create(
            [
                'is_for_filter' => 1
            ]
        );

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $createdTagIds = [];
        foreach ($tags as $tag) {
            factory(RoomTag::class)->create(
                [
                    'designer_id' => $room->id,
                    'tag_id' => $tag->id
                ]
            );

            $createdTagIds[] = $tag->id;
        }

        $filter = new RoomFilter(
            [
                'tag_ids' => $createdTagIds
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithIncorrectTypeTags()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        // tag_ids value should be array
        $filter = new RoomFilter(
            [
                'tag_ids' => 'true'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithEmptyArrayTags()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        // tag_ids value should be array
        $filter = new RoomFilter(
            [
                'tag_ids' => []
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredTagsAndWrongTagIdRequest()
    {
        $tag1 = factory(Tag::class)->create(
            [
                'is_for_filter' => 1
            ]
        );

        $tag2 = factory(Tag::class)->create(
            [
                'is_for_filter' => 0
            ]
        );

        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        /*
           this is wrong request because $tag2->id isn't for filter,
           so the filter will skip this tag
        */
        $createdTagIds = [
            $tag2->id
        ];

        // the room doesnt have RoomTag with tag_id = 2
        factory(RoomTag::class)->create(
            [
                'designer_id' => $room->id,
                'tag_id' => $tag1->id
            ]
        );

        $filter = new RoomFilter(
            [
                'tag_ids' => $createdTagIds
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithFilteredRoomName()
    {
        // Truncate table 'designer' before test
        $this->truncateTableDesigner();

        // Commit the DB transaction so function MATCH..AGAINST could work
        DB::commit();

        $room = factory(Room::class)
            ->states(
                [
                    'notTesting',
                    'mamirooms'
                ]
            )
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Campur Exclusive Palagan Sleman 212'
                ]
            );

        $this->generatePriceFilter($room);

        // create other rooms
        factory(Room::class, 3)
            ->states(
                [
                    'notTesting',
                    'mamirooms'
                ]
            )
            ->create();

        $filter = new RoomFilter(
            [
                'room_name' => 'palagan'
            ]
        );

        $this->assertCount(4, Room::all());

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertCount(1, $response->get());

        // Truncate table 'designer' after test
        $this->truncateTableDesigner();
    }

    public function testDoFilterWithFilteredFreeCriteria()
    {
        $room = factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => $this->faker->numberBetween(1, 10),
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'name' => 'Kos Exclusive Mamirooms 123'
            ]
        );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'criteria' => 'mami room'
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredPromotion()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_promoted' => true
                ]
            );

        $this->generatePriceFilter($room);

        // Create expired promotion
        factory(Promotion::class)->create(
            [
                'designer_id' => $room->id,
                'verification' => 'true',
                'start_date' => $this->faker->dateTimeBetween('-30 days', '-14 days'),
                'finish_date' => $this->faker->dateTimeBetween('-14 days', '-7 days')
            ]
        );

        $filter = new RoomFilter(
            [
                'promotion' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredBooking()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123'
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'booking' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithFilteredKostLevel()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123'
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'level_info' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
        $this->assertEquals(0, $response->get()->count());
    }

    public function testDoFilterWithTestingDataFiltering()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_testing' => 1
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'hide_mamitest' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertEmpty($response->get());
    }

    public function testDoFilterWithNonTestingDataFiltering()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_testing' => 1
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'hide_mamitest' => false
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertNotEmpty($response->get());
        $this->assertEquals(1, $response->get()->count());
    }

    public function testDoFilterWithSortingDisabled()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'disable_sorting' => true,
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledUsingNoFieldKey()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'direction' => '-'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledUsingNoDirectionKey()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'price'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledUsingKeyFields()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'fields' => 'price',
                    'direction' => '-'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledBySortScore()
    {
        $this->markTestSkipped('https://devel.wejoin.us:3000/ali/mami53/-/jobs/149816');
        $rows = $this->faker->numberBetween(5, 10);
        $scores = [
            100,
            1000,
            10000,
            100000,
        ];

        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) use ($scores) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->randomElement($scores);
                    $room->kost_updated_date = $this->faker->dateTimeBetween('-30 days', 'now');
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'price',
                    'direction' => '-'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomScore = $response->pluck('sort_score')->toArray()[0];
        $firstRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[0]);
        $secondRoomScore = $response->pluck('sort_score')->toArray()[1];
        $secondRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[1]);

        $this->assertTrue($firstRoomScore >= $secondRoomScore);
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByPriceAscending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'price',
                    'direction' => 'asc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomPrice = $response->pluck('price_monthly')->toArray()[0];
        $secondRoomPrice = $response->pluck('price_monthly')->toArray()[1];

        $this->assertTrue($firstRoomPrice <= $secondRoomPrice);
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByPriceDescending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(10, 20) * 100000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'price',
                    'direction' => 'desc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomPrice = $response->pluck('price_monthly')->toArray()[0];
        $secondRoomPrice = $response->pluck('price_monthly')->toArray()[1];

        $this->assertTrue($firstRoomPrice >= $secondRoomPrice);
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByAscendingWithPriceQuarterly()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->price_quarterly = $this->faker->numberBetween(200, 300) * 10000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly,
                            'final_price_quarterly' => $room->price_quarterly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'rent_type' => '4',
                'sorting' => [
                    'field' => 'price',
                    'direction' => 'asc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter
            ->doFilter($rooms)
            ->get();

        $firstRoomPrice = $response->toArray()[0]['price_quarterly'];
        $secondRoomPrice = $response->toArray()[1]['price_quarterly'];

        $this->assertTrue($firstRoomPrice <= $secondRoomPrice);
        $this->assertNotEmpty($response);
        $this->assertEquals($rows, $response->count());
    }

    public function testDoFilterWithSortingEnabledByDescendingWithPriceSemiannually()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->price_semiannually =  $this->faker->numberBetween(200, 300) * 10000;
                    $room->sort_score = $this->faker->numberBetween(1000, 10000);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly,
                            'final_price_semiannually' => $room->price_semiannually
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'rent_type' => '5',
                'sorting' => [
                    'field' => 'price',
                    'direction' => 'desc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter
            ->doFilter($rooms)
            ->get();

        $firstRoomPrice = $response->toArray()[0]['price_semiannually'];
        $secondRoomPrice = $response->toArray()[1]['price_semiannually'];

        $this->assertTrue($firstRoomPrice >= $secondRoomPrice);
        $this->assertNotEmpty($response);
        $this->assertEquals($rows, $response->count());
    }

    public function testDoFilterWithSortingEnabledByAvailabilityAscending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->room_available = $this->faker->numberBetween(10, 100);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'availability',
                    'direction' => 'asc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomAvailability = $response->pluck('room_available')->toArray()[0];
        $secondRoomAvailability = $response->pluck('room_available')->toArray()[1];

        $this->assertTrue($firstRoomAvailability <= $secondRoomAvailability);
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByAvailabilityDescending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->room_available = $this->faker->numberBetween(10, 100);
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'availability',
                    'direction' => 'desc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomAvailability = $response->pluck('room_available')->toArray()[0];
        $secondRoomAvailability = $response->pluck('room_available')->toArray()[1];

        $this->assertTrue($firstRoomAvailability >= $secondRoomAvailability);
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByUpdatedDateAscending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->kost_updated_date = $this->faker->dateTimeBetween('-30 days', 'now');
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'last_update',
                    'direction' => 'asc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[0]);
        $secondRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[2]);

        $this->assertTrue($secondRoomUpdated->greaterThanOrEqualTo($firstRoomUpdated));
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testDoFilterWithSortingEnabledByUpdatedDateDescending()
    {
        $rows = $this->faker->numberBetween(5, 10);
        factory(Room::class, $rows)
            ->create()
            ->each(
                function ($room) {
                    $room->is_active = 'true';
                    $room->expired_phone = 0;
                    $room->room_available = $this->faker->numberBetween(1, 10);
                    $room->apartment_project_id = null;
                    $room->price_monthly = $this->faker->numberBetween(100, 150) * 10000;
                    $room->kost_updated_date = $this->faker->dateTimeBetween('-30 days', 'now');
                    $room->save();
                    factory(PriceFilter::class)->create(
                        [
                            'designer_id' => $room->id,
                            'final_price_monthly' => $room->price_monthly
                        ]
                    );
                }
            );

        $filter = new RoomFilter(
            [
                'sorting' => [
                    'field' => 'last_update',
                    'direction' => 'desc'
                ],
            ]
        );

        $rooms = Room::query();
        $response = $filter->doFilter($rooms);

        $firstRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[0]);
        $secondRoomUpdated = Carbon::parse($response->pluck('kost_updated_date')->toArray()[2]);

        $this->assertTrue($firstRoomUpdated->greaterThanOrEqualTo($secondRoomUpdated));
        $this->assertNotEmpty($response->get());
        $this->assertEquals($rows, $response->get()->count());
    }

    public function testGetString()
    {
        $filter = new RoomFilter(
            [
                'promotion' => true,
                'property_type' => 'kost'
            ]
        );

        $this->assertEmpty($filter->getString());
        $this->assertEquals('', $filter->getString());
    }

    public function testGetStringWithPriceRange()
    {
        $filter = new RoomFilter(
            [
                'price_range' => [
                    1000000,
                    15000000
                ],
            ]
        );

        $this->assertNotEmpty($filter->getString());
        $this->assertSame(
            'Rp. ' . Price::shorten(1000000) . '~Rp. ' . Price::shorten(15000000),
            $filter->getString()
        );
    }

    public function testGetStringWithRentType()
    {
        $number = $this->faker->randomNumber(1);
        $filter = new RoomFilter(
            [
                'rent_type' => $number,
            ]
        );

        $this->assertNotEmpty($filter->getString());
        $this->assertSame(
            Price::rentTypeDescription($number),
            $filter->getString()
        );
    }

    public function testGetStringWithGender()
    {
        $filter = new RoomFilter(
            [
                'gender' => [
                    0,
                    1,
                    2
                ],
            ]
        );

        $this->assertNotEmpty($filter->getString());
        $this->assertSame(
            'campur+putra+putri',
            $filter->getString()
        );
    }

    public function testGetStringWithValidTagIds()
    {
        $rows = $this->faker->numberBetween(2, 5);
        factory(Tag::class, $rows)->create(
            [
                'is_for_filter' => 1
            ]
        );

        $createdTagIds = Tag::pluck("id")->toArray();
        $createdTagNames = Tag::pluck("name")->toArray();

        $filter = new RoomFilter(
            [
                'tag_ids' => $createdTagIds,
            ]
        );

        $this->assertNotEmpty($filter->getString());
        $this->assertSame(
            implode('&', $createdTagNames),
            $filter->getString()
        );
    }

    public function testDoFilterWithMamiCheckerFilterReturnRoom()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123'
                ]
            );

        $this->generatePriceFilter($room);

        $photo = factory(Media::class)->create();

        $user = factory(User::class)->create(
            [
                'photo_id' => $photo->id
            ]
        );

        $checker = factory(UserChecker::class)->create(
            [
                'user_id' => $user->id
            ]
        );

       factory(UserCheckerTracker::class)->create(
            [
                'user_checker_id' => $checker->id,
                'designer_id' => $room->id
            ]
        );

        $filter = new RoomFilter(
            [
                'mamichecker' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(1, $response->get());
    }

    public function testDoFilterWithMamiCheckerFilterReturnEmpty()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123'
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'mamichecker' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(0, $response->get());
    }

    public function testDoFilterWithMamiRoomsFilterReturnRoom()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_mamirooms' => 1
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'mamirooms' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(1, $response->get());
    }

    public function testDoFilterWithMamiRoomsFilterReturnEmpty()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => $this->faker->numberBetween(1, 10),
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_mamirooms' => 0
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'mamirooms' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(0, $response->get());
    }

    public function testGetStringWithInValidTagIds()
    {
        $rows = $this->faker->numberBetween(2, 5);
        factory(Tag::class, $rows)->create(
            [
                'is_for_filter' => 0
            ]
        );

        $createdTagIds = Tag::pluck("id")->toArray();

        $filter = new RoomFilter(
            [
                'tag_ids' => $createdTagIds,
            ]
        );

        // tag instance isn't for filter, so it should be ignored
        $this->assertEmpty($filter->getString());
    }

    public function testDoFilterWithRoomIsAvailableReturnRoom()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => 2,
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_mamirooms' => 1
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'is_available' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(1, $response->get());
    }

    public function testDoFilterWithRoomIsAvailableFilterReturnEmpty()
    {
        // available room is 0
        $room = factory(Room::class)
            ->create(
                [
                    'is_active' => 'true',
                    'expired_phone' => 0,
                    'room_available' => 0,
                    'apartment_project_id' => null,
                    'price_monthly' => 1000000,
                    'name' => 'Kos Exclusive Mamirooms 123',
                    'is_mamirooms' => 0
                ]
            );

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'is_available' => 1
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(0, $response->get());
    }

    public function testFilterVirtualTour_Success()
    {
        $room = factory(Room::class)->states(
            [
                'active',
                'with-small-rooms',
                'availablePriceMonthly'
            ])
            ->create();
        factory(RoomAttachment::class)->states('active')->create([
            'designer_id' => $room->id
        ]);

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'virtual_tour' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertTrue(in_array($room->id, $response->get()->pluck('id')->toArray()));
    }

    public function testFilterVirtualTour_Fail()
    {
        $room = factory(Room::class)->states(
            [
                'active',
                'with-small-rooms',
            ])
            ->create();
        factory(RoomAttachment::class)->states('active')->create([
            'designer_id' => $room->id
        ]);

        $filter = new RoomFilter(
            [
                'virtual_tour' => true
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertFalse(in_array($room->id, $response->get()->pluck('id')->toArray()));
    }

    private function truncateTableDesigner()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('designer')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function testDoFilterWithSpecificGoldPlusFilter()
    {
        $room = $this->generateKosForGoldPlusTest();

        $this->generatePriceFilter($room);

        $goldPlus1 = $this->generateKosLevelGoldPlus1();

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldPlus1->id
            ]
        );

        $filter = new RoomFilter(
            [
                'goldplus' => [1]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertCount(1, $response->get());
    }

    public function testDoFilterWithSpecificGoldPlusFilterReturnEmpty()
    {
        $room = $this->generateKosForGoldPlusTest();

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'goldplus' => [1]
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertEmpty($response->get());
    }


    public function testDoFilterWithAllGoldPlusFilter()
    {
        $rooms = factory(Room::class, 4)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => 0,
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'is_mamirooms' => 0
            ]
        );

        foreach ($rooms as $room) {
            $this->generatePriceFilter($room);
        }

        $goldLevel1 = $this->generateKosLevelGoldPlus1();
        $goldLevel2 = $this->generateKosLevelGoldPlus2();
        $goldLevel3 = $this->generateKosLevelGoldPlus3();
        $goldLevel4 = $this->generateKosLevelGoldPlus4();

        $goldPlusIds = [
            $goldLevel1->id,
            $goldLevel2->id,
            $goldLevel3->id,
            $goldLevel4->id,
        ];

        $i = 0;

        foreach ($rooms as $room) {
            factory(KostLevelMap::class)->create(
                [
                    'kost_id' => $room->song_id,
                    'level_id' => $goldPlusIds[$i]
                ]
            );
            $i++;
        }

        $filter = new RoomFilter(
            [
                'goldplus' => [0]
            ]
        );

        $response = $filter->doFilter($room);

        $this->assertCount(4, $response->get());
    }

    public function testDoFilterWithEmptyValueGoldPlusFilter()
    {
        $room = $this->generateKosForGoldPlusTest();

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'goldplus' => []
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertCount(1, $response->get());
    }

    public function testDoFilterWithIncorrectGoldPlusFilter()
    {
        $room = $this->generateKosForGoldPlusTest();

        $this->generatePriceFilter($room);

        $filter = new RoomFilter(
            [
                'goldplus' => true
            ]
        );

        $response = $filter->doFilter($room);
        $this->assertCount(1, $response->get());
    }

    public function testIsFlashSaleLandingReturnTrue()
    {
        $filter = new RoomFilter(
            [
                'flash_sale' => true,
                'landings'   => ['kost-abepura']
            ]
        );

        $method = $this->getNonPublicMethodFromClass(RoomFilter::class, 'isFlashSaleLanding');

        $response = $method->invokeArgs($filter, []);

        $this->assertTrue($response);
    }

    public function testIsFlashSaleLandingWithoutFlashSaleFilterReturnFalse()
    {
        $filter = new RoomFilter(
            [
                'landings'   => ['kost-abepura']
            ]
        );

        $method = $this->getNonPublicMethodFromClass(RoomFilter::class, 'isFlashSaleLanding');

        $response = $method->invokeArgs($filter, []);

        $this->assertFalse($response);
    }

    public function testIsFlashSaleLandingWithoutLandingFilterReturnFalse()
    {
        $filter = new RoomFilter(
            [
                'flash_sale' => true
            ]
        );

        $method = $this->getNonPublicMethodFromClass(RoomFilter::class, 'isFlashSaleLanding');

        $response = $method->invokeArgs($filter, []);

        $this->assertFalse($response);
    }

    private function generateKosForGoldPlusTest()
    {
        return factory(Room::class)->create(
            [
                'is_active' => 'true',
                'expired_phone' => 0,
                'room_available' => 0,
                'apartment_project_id' => null,
                'price_monthly' => 1000000,
                'name' => 'Kos Exclusive Mamirooms 123',
                'is_mamirooms' => 0
            ]
        );
    }

    private function generateKosLevelGoldPlus1()
    {
        return factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus1'),
                'name' => 'Mamikos Goldplus 1',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );
    }

    private function generateKosLevelGoldPlus2()
    {
        return factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus2'),
                'name' => 'Mamikos Goldplus 2',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );
    }

    private function generateKosLevelGoldPlus3()
    {
        return factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus3'),
                'name' => 'Mamikos Goldplus 3',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );
    }

    private function generateKosLevelGoldPlus4()
    {
        return factory(KostLevel::class)->create(
            [
                'id' => config('kostlevel.id.goldplus4'),
                'name' => 'Mamikos Goldplus 4',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );
    }

    private function generatePriceFilter($room)
    {
        return factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_monthly' => $room->price_monthly
            ]
        );
    }
}
