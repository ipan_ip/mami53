<?php

namespace App\Entities\Room;

use App\Entities\Level\KostLevelValue;
use App\Test\MamiKosTestCase;

class ReviewReplyTest extends MamiKosTestCase
{
    public function testGetTotalBenefitKosByIds()
    {
        $room = factory(Room::class)->create();
        $kostLevelValue = factory(KostLevelValue::class)->create();
        $kostValue = factory(KostValue::class)->create([
            'designer_id' => $room->id,
            'kost_level_value_id' => $kostLevelValue->id
        ]);

        factory(KostValue::class)->create([
            'designer_id' => factory(Room::class)->create(),
            'kost_level_value_id' => factory(KostLevelValue::class)->create()
        ]);

        $this->assertEquals(1, KostValue::getTotalBenefitKosByIds($room->id, $kostLevelValue->id));
    }
}
