<?php

namespace App\Entities\Room;

use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\User;

class ReviewTest extends MamiKosTestCase
{
    public function testGetReviewerPhotoProfile()
    {
        $media = factory(Media::class)->create();

        $user = factory(User::class)->create(['photo_id' => $media->id]);

        $review = factory(Review::class)->create(['user_id' => $user->id]);

        $this->assertNotNull($review->getReviewerPhotoProfile());
    }

    public function testGetReviewerPhotoProfileReturnNull()
    {
        $user = factory(User::class)->create();

        $review = factory(Review::class)->create(['user_id' => $user->id]);

        $this->assertNull($review->getReviewerPhotoProfile());
    }
}