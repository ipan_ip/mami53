<?php

namespace App\Entities\Room;

use App\Entities\Room\Review;
use App\Entities\Room\ReviewReply;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomReviewReplyTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testUserRelationship()
    {
        $reviewReply = factory(ReviewReply::class)->create();

        $user = factory(User::class)->create();

        $reviewReply->setRelation('user', $user);

        $this->assertEquals($user->id, $reviewReply->user->id);
    }

    public function testReviewRelationship()
    {
        $reviewReply = factory(ReviewReply::class)->create();

        $review = factory(Review::class)->create();

        $reviewReply->setRelation('review', $review);

        $this->assertEquals($review->id, $reviewReply->review->id);
    }

    public function testReviewRelation()
    {
        $review = factory(Review::class)->create();
        $reviewReplay = factory(ReviewReply::class)->create([
            'review_id' => $review->id
        ]);

        $this->assertEquals($review->id, $reviewReplay->review->id);
    }

    public function testScopeLive()
    {
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create([
            'user_id' => $user->id,
            'status' => 1,
        ]);

        $result = Review::where('user_id', $user->id)->live()->first();
        $this->assertEquals($review->id, $result->id);
    }

    public function testInsertOrUpdateWithExistingRecord()
    {
        $review = factory(Review::class)->create();
        $user = factory(User::class)->create();

        factory(ReviewReply::class)->create([
            'review_id' => $review->id,
            'user_id' => $user->id,
            'content' => 'Content',
            'status' => '1'
        ]);

        $data = [
            'user_id' => $user->id,
            'review_id' => $review->id,
            'content' => 'Update Content'
        ];

        $result = ReviewReply::insertOrUpdate($data);

        $this->assertEquals($data['user_id'], $result->user_id);
        $this->assertEquals($data['review_id'], $result->review_id);
        $this->assertEquals($data['content'], $result->content);
        $this->assertEquals(0, $result->status);
    }

    public function testInsertOrUpdateWithoutExistingRecord()
    {
        $review = factory(Review::class)->create();
        $user = factory(User::class)->create();

        $data = factory(ReviewReply::class)->make([
            'review_id' => $review->id,
            'user_id' => $user->id,
            'content' => 'New Content',
            'status' => '1',
        ])->toArray();

        $result = ReviewReply::insertOrUpdate($data);

        $this->assertEquals($data['user_id'], $result->user_id);
        $this->assertEquals($data['review_id'], $result->review_id);
        $this->assertEquals($data['content'], $result->content);
        $this->assertEquals($data['status'], $result->status);
    }
}
