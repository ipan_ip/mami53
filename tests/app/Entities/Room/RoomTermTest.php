<?php

namespace App\Entities\Room;

use App\Entities\Room\Element\RoomTermDocument;
use App\Test\MamiKosTestCase;

class RoomTermTest extends MamiKosTestCase
{
    public function testRelationToRoom()
    {
        $room = factory(Room::class)->create();
        $roomTerm = factory(RoomTerm::class)->create(['designer_id' => $room->id]);

        $this->assertSame($room->id, $roomTerm->room->id);
    }

    public function testRelationToRoomTermDocument()
    {
        $roomTerm = factory(RoomTerm::class)->create();
        $roomTermDocument = factory(RoomTermDocument::class)->create(['designer_term_id' => $roomTerm->id]);

        $this->assertSame($roomTerm->room_term_document->first()->id, $roomTermDocument->id);
    }
}
