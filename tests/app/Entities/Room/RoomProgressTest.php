<?php

namespace App\Entities\Room;

use App\Entities\Document\Document;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Media\Media;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Element\TypeCard;
use App\Entities\Room\Element\Unit;
use App\Test\MamiKosTestCase;
use App\User;

class RoomProgressTest extends MamiKosTestCase
{
    public function testGetLocation()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getLocation();

        $expectedResult = [
            'id' => $room->song_id,
            'latitude' => $room->latitude,
            'longitude' => $room->longitude,
            'city' => $room->area_city,
            'subdistrict' => $room->area_subdistrict,
            'address' => $room->address,
            'fac_near' => $room->fac_near_other,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetInformation()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getInformation();

        $expectedResult = [
            'room_id' => $room->song_id,
            'name' => $room->name,
            'gender' => (int) $room->gender,
            'building_year' => $room->building_year,
            'description' => $room->description,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRule()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id,
            'description' => 'Term Description',
        ]);
        $media = factory(Media::class)->create();
        $document = factory(Document::class)->create();
        factory(RoomTermDocument::class, 5)->create([
            'designer_term_id' => $roomTerm->id,
            'real_file_name' => 'Term Document',
            'document_id' => $document->id,
            'media_id' => $media->id,
        ]);
        $this->mockAlternatively(RoomTermDocument::class)->shouldReceive('roomDocument')->andReturn([
            'id' => 1,
            'file_id' => 1,
            'type' => 'Type 1',
            'file_name' => 'File Name',
            'media_path' => 'media/path',
            'document_path' => 'document/path'
        ]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getRule();

        $this->assertEquals($room->song_id, $result['room_id']);
        $this->assertEquals($roomTerm->description, $result['rule']);
        $this->assertNotEmpty($result['document']);
    }

    public function testGetRoomPhotos()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $cards = factory(CardPremium::class, 5)->create([
            'designer_id' => $room->id,
            'source' => 'new_owner',
            'photo_id' => factory(Media::class)->create(),
        ]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getRoomPhotos();

        $roomPhotos = [];
        foreach ($cards as $card) {
            if ($card->photo) {
                $roomPhotos[] = [
                    'card_id' => $card->id,
                    'description' => $card->description,
                    'photo_id' => $card->photo->id,
                    'photo_url' => $card->photo->getMediaUrl()['medium'],
                ];
            }
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'kos_photos' => $roomPhotos,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRoomTypesWithOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $roomProgress = new RoomProgress($room->song_id, $user, ['room_type_id' => $types[0]->id]);

        $result = $roomProgress->getRoomTypes();

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_types' => [
                'room_type_id' => $types[0]->id,
                'name_room_type' => $types[0]->name,
                'maximum_occupancy' => $types[0]->maximum_occupancy,
                'tenant_type' => explode(',', $types[0]->tenant_type),
                'room_size' => $types[0]->room_size,
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRoomTypesWithoutOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getRoomTypes();

        $roomTypes = [];
        foreach ($types as $type) {
            $roomTypes[] = [
                'room_type_id' => $type->id,
                'name_room_type' => $type->name,
                'maximum_occupancy' => $type->maximum_occupancy,
                'tenant_type' => explode(',', $type->tenant_type),
                'room_size' => $type->room_size,
            ];
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_types' => $roomTypes
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRoomUnitsWithOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $units = factory(Unit::class, 5)->create([
            'designer_type_id' => $types[0]->id
        ]);

        $roomProgress = new RoomProgress($room->song_id, $user, ['room_type_id' => $types[0]->id]);

        $result = $roomProgress->getRoomUnits();

        $roomUnits = [];
        foreach ($units as $unit) {
            $roomUnits[] = [
                'room_unit_id' => $unit->id,
                'room_name' => $unit->name,
                'floor_name' => $unit->floor,
                'is_active' => $unit->is_active,
            ];
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_type_id' => $types[0]->id,
            'total_room' => $types[0]->total_room,
            'available_room_units' => $roomUnits,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetRoomUnitsWithoutOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $units = factory(Unit::class, 5)->create([
            'designer_type_id' => $types[0]->id
        ]);

        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getRoomUnits();

        $roomUnits = [];
        foreach ($units as $unit) {
            $roomUnits[] = [
                'room_unit_id' => $unit->id,
                'room_name' => $unit->name,
                'floor_name' => $unit->floor,
                'is_active' => $unit->is_active,
            ];
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_type_id' => $types[0]->id,
            'total_room' => $types[0]->total_room,
            'available_room_units' => $roomUnits,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testgetRoomTypePhotosWithOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $typeCards = factory(TypeCard::class, 5)->create([
            'designer_type_id' => $types[0]->id,
            'photo_id' => factory(Media::class)->create(),
        ]);

        $roomProgress = new RoomProgress($room->song_id, $user, ['room_type_id' => $types[0]->id]);

        $result = $roomProgress->getRoomTypePhotos();

        $roomTypePhotos = [];
        foreach ($typeCards as $card) {
            $roomTypePhotos[] = [
                'card_id' => $card->id,
                'description' => $card->description,
                'photo_id' => $card->photo->id,
                'photo_url' => $card->photo->getMediaUrl()['medium'],
            ];
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_type_id' => $types[0]->id,
            'kos_type_photos' => $roomTypePhotos,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testgetRoomTypePhotosWithoutOption()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $types = factory(Type::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $typeCards = factory(TypeCard::class, 5)->create([
            'designer_type_id' => $types[0]->id,
            'photo_id' => factory(Media::class)->create(),
        ]);

        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getRoomTypePhotos();

        $roomTypePhotos = [];
        foreach ($typeCards as $card) {
            $roomTypePhotos[] = [
                'card_id' => $card->id,
                'description' => $card->description,
                'photo_id' => $card->photo->id,
                'photo_url' => $card->photo->getMediaUrl()['medium'],
            ];
        }

        $expectedResult = [
            'room_id' => $room->song_id,
            'room_type_id' => $types[0]->id,
            'kos_type_photos' => $roomTypePhotos,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetBank()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $mamipayOwner = factory(MamipayOwner::class)->create([
            'user_id' => $user->id
        ]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->getBank();

        $expectedResult = [
            'room_id' => $room->song_id,
            'bank_name' => $mamipayOwner->bank_name,
            'bank_account_name' => $mamipayOwner->bank_account_owner,
            'bank_account_number' => $mamipayOwner->bank_account_number
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithRoomIsNull()
    {
        $user = factory(User::class)->create();
        $roomProgress = new RoomProgress(0, $user, []);

        $result = $roomProgress->response(0);

        $expectedResult = [
            'status' => false,
            'meta' => [
                'message' => ['Room tidak ditemukan'],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs1()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(1);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getLocation(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs2()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(2);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getInformation(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs3()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(3);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getRule(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs4()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(4);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => null,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs5()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(5);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getRoomPhotos(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs6()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(6);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getRoomTypes(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs7()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(7);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getRoomUnits(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs8()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(8);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => null,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs9()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(9);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getRoomTypePhotos(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs10()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(10);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getPrice(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs11()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(11);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => null,
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIs12()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(12);

        $expectedResult = [
            'status' => true,
            'meta' => [
                'message' => ['Sukses']
            ],
            'room' => $roomProgress->getBank(),
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testResponseWithStepIsGreaterThan12()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([]);
        $roomProgress = new RoomProgress($room->song_id, $user, []);

        $result = $roomProgress->response(999);

        $expectedResult = [
            'status' => false,
            'meta' => [
                'message' => ['Invalid step number!'],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }
}
