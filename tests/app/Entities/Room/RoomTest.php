<?php

namespace App\Entities\Room;

use Carbon\Carbon;

use App\Entities\Activity\PhotoBy;
use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp3;
use App\Entities\Booking\BookingAcceptanceRate;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Level\KostLevelValue;
use App\Entities\Generate\ListingReason;
use App\Entities\Media\Media;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\LevelHistory;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Property\Property;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Element\Facility;
use App\Entities\Room\Element\Report;
use App\Entities\Room\Element\RoomPriceComponent;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\User\History;
use App\Entities\User\Notification;
use App\Entities\Room\Element\TagType;
use App\Enums\Room\PropertyType;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use StatsLib;
use Illuminate\Database\Eloquent\Collection;
use App\User;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\Call;
use App\Entities\Revision\Revision;
use App\Entities\Room\Element\Card\Group;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use \Illuminate\Database\Eloquent\Builder;
use App\Entities\Apartment\ApartmentProject;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class RoomTest extends MamiKosTestCase
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        factory(KostLevel::class)->create(
            [
                'name' => 'regular',
                'is_regular' => 1,
                'is_hidden' => 0
            ]
        );

        $this->mockAlternatively('overload:App\Libraries\ElasticsearchApi');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testCardRelation()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Element\Card::class)->create([
            'designer_id' => $room->id,
        ]);

        $this->assertEquals(1, $room->cards->count());
    }

    public function testPropertyIconRelation()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Element\Card::class)->create([
            'designer_id' => $room->id,
            'group' => Group::BUILDING_FRONT
        ]);

        $this->assertEquals(1, $room->property_icon->count());
    }

    public function testCardPremiumRelation()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Element\CardPremium::class)->create([
            'designer_id' => $room->id,
        ]);

        $this->assertEquals(1, $room->premium_cards->count());
    }

    public function testBbkRejectReasonRelation()
    {
        $room1 = factory(\App\Entities\Room\Room::class)->create();
        $room2 = factory(\App\Entities\Room\Room::class)->create();
        
        $listingReasons1 = factory(ListingReason::class, 2)->create([
            'from' => 'bbk',
            'listing_id' => $room1->id
        ]);

        $listingReasons2 = factory(ListingReason::class, 3)->create([
            'from' => 'bbk',
            'listing_id' => $room2->id
        ]);

        $bbk_rejection_reason1 = $room1->bbk_reject_reason;
        $bbk_rejection_reason2 = $room2->bbk_reject_reason;

        $this->assertEquals($listingReasons1->count(), $bbk_rejection_reason1->count());
        $this->assertEquals($listingReasons2->count(), $bbk_rejection_reason2->count());

        $this->assertEquals($listingReasons1[0]->id, $bbk_rejection_reason1[0]->id);
        $this->assertEquals($listingReasons1[1]->id, $bbk_rejection_reason1[1]->id);

        $this->assertEquals($listingReasons2[0]->id, $bbk_rejection_reason2[0]->id);
        $this->assertEquals($listingReasons2[1]->id, $bbk_rejection_reason2[1]->id);
        $this->assertEquals($listingReasons2[2]->id, $bbk_rejection_reason2[2]->id);
    }

    public function testScopeWithLatestBbkRejectReason()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        
        $oldestReason = factory(ListingReason::class)->create([
            'from' => 'bbk',
            'listing_id' => $room->id
        ]);
        $latestReason = factory(ListingReason::class)->create([
            'from' => 'bbk',
            'listing_id' => $room->id
        ]);

        $room = Room::withLatestBbkRejectReason()->find($room->id);

        $this->assertEquals($latestReason->id, $room->latest_bbk_reject_reason->id);
    }

    public function testGetFromElasticSearchShouldOk()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();

        $this->mock->shouldReceive('get')->once()->andReturn(['data' => 'success']);

        $response = $room->getFromElasticsearch();
        $this->assertEquals(['data' => 'success'], $response);
    }

    public function testGetFromElasticSearchShouldThrowMissing404Exception()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $exception = new \Elasticsearch\Common\Exceptions\Missing404Exception;

        $this->mock->shouldReceive('get')->once()->andThrow($exception);

        $response = $room->getFromElasticsearch();
        $this->assertNull($response);
    }

    public function testGetFromElasticSearchShouldThrowError()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $exception = new \Exception;

        $this->mock->shouldReceive('get')->once()->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $response = $room->getFromElasticsearch();
        $this->assertNull($response);
    }

    public function testAddToElasticsearchShouldOk()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();

        $this->mock->shouldReceive('add')->once()->andReturn(['data' => 'success']);

        $result = $room->addToElasticsearch();
        $this->assertEquals(['data' => 'success'], $result);
    }

    public function testAddToElasticssearchShouldThrowErrorWhenNodeNotFound()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $exception = new \Exception;

        $this->mock->shouldReceive('add')->once()->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $response = $room->addToElasticsearch();
        $this->assertNull($response);
    }

    public function testDeleteFromElasticsearchGetEmptyRoomAndReturnNull()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();

        $this->mock->shouldReceive('get')->once()->andReturn(null);

        $response = $room->deleteFromElasticsearch();
        $this->assertEquals(null, $response);
    }

    public function testDeleteFromElasticsearchShouldOk()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();

        $this->mock->shouldReceive('get')->once()->andReturn(['data' => 'data room']);
        $this->mock->shouldReceive('delete')->once()->andReturn(['data' => 'delete success']);

        $response = $room->deleteFromElasticsearch();
        $this->assertEquals(['data' => 'delete success'], $response);
    }

    public function testDeleteFromElasticsearchShouldThrowErrorExceptionMissing404Exception()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $exception = new \Elasticsearch\Common\Exceptions\Missing404Exception;

        $this->mock->shouldReceive('get')->once()->andThrow($exception);;

        $response = $room->deleteFromElasticsearch();
        $this->assertNull($response);
    }

    public function testDeleteFromElasticsearchShouldThrowErrorException()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $exception = new \Exception;

        $this->mock->shouldReceive('get')->once()->andReturn(['data' => 'data room']);
        $this->mock->shouldReceive('delete')->once()->andThrow($exception);;

        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $response = $room->deleteFromElasticsearch();
        $this->assertNull($response);
    }

    public function testFacilityShouldOk()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $tag = factory(\App\Entities\Room\Element\Tag::class)->create();
        factory(\App\Entities\Room\Element\RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id,
        ]);

        $room = Room::find($room->id);

        $response = $room->facility();
        $this->assertNotNull($response);
        $this->assertInstanceOf(Facility::class, $response);
    }

    public function testIsShouldPremiumPhotoTrue()
    {
        $photoId = $this->faker->numberBetween(1, 9999);

        $room = factory(\App\Entities\Room\Room::class)->create(
            [
                'photo_id' => $photoId
            ]
        );

        factory(\App\Entities\Room\Element\CardPremium::class)->create([
            'photo_id' => $room->photo_id
        ]);

        $this->assertTrue($room->isPremiumPhoto());
    }

    public function testIsPremiumPhotoShouldFalse()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $this->assertFalse($room->isPremiumPhoto());
    }

    public function testHasPremiumPhotosShouldTrue()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Element\CardPremium::class)->create([
            'designer_id' => $room->id,
        ]);

        $this->assertTrue($room->hasPremiumPhotos());
    }

    public function testHasPremiumPhotosShouldFalse()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $this->assertFalse($room->hasPremiumPhotos());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetReviewShouldOk()
    {
        $user = factory(\App\User::class)->make();
        $room = factory(\App\Entities\Room\Room::class)->make();

        $mockReview = $this->mockPartialAlternatively('overload:App\Entities\Room\Review');
        $mockReview->shouldReceive('getReview')->once()->andReturn(['data'=> 'anything']);

        $response = $room->getReview($user, false);
        $this->assertEquals(['data'=> 'anything'], $response);
    }

    public function testRatingShouldCorrect()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'designer_id' => $room->id,
            'rating' => 4,
        ]);
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'designer_id' => $room->id,
            'rating' => 2,
        ]);

        $response = $room->rating();
        $this->assertNotNull($response);
        $this->assertEquals(3, $response);
    }

    public function testReviewsCountShouldCorrect()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'designer_id' => $room->id,
            'rating' => 4,
        ]);
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'designer_id' => $room->id,
            'rating' => 2,
        ]);

        $response = $room->reviewsCount();

        $this->assertNotNull($response);
        $this->assertEquals(2, $response);
    }

    public function testAvailableRatingShouldCorrectReturnTrue()
    {
        $room = factory(\App\Entities\Room\Room::class)->make();
        $this->assertTrue($room->available_rating(null));
    }

    public function testAvailableRatingShouldCorrectReturnFalse()
    {
        $user = factory(\App\User::class)->create();
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'rating' => 4,
        ]);

        $this->assertFalse($room->available_rating($user));
    }

    public function testIsActiveShouldCorrect()
    {
        $activeRoom = factory(\App\Entities\Room\Room::class)->make(['is_active' => 'true']);
        $this->assertTrue($activeRoom->isActive());

        $nonActiveRoom = factory(\App\Entities\Room\Room::class)->make(['is_active' => 'false']);
        $this->assertFalse($nonActiveRoom->isActive());
    }

    public function testIsMamiroomsShouldCorrect()
    {
        $mamiroom = factory(\App\Entities\Room\Room::class)->make(['is_mamirooms' => 1]);
        $this->assertTrue($mamiroom->isMamirooms());

        $nonMamiroom = factory(\App\Entities\Room\Room::class)->make(['is_mamirooms' => 0]);
        $this->assertFalse($nonMamiroom->isMamirooms());
    }

    public function testIsPremiumShouldCorrect()
    {
        $premiumRoom = factory(\App\Entities\Room\Room::class)->make(['is_promoted' => 'true']);
        $this->assertTrue($premiumRoom->isPremium());

        $nonPremiumRoom = factory(\App\Entities\Room\Room::class)->make(['is_promoted' => 'false']);
        $this->assertFalse($nonPremiumRoom->isPremium());
    }

    public function testIsBisaBookingShouldCorrect()
    {
        $bbkRoom = factory(\App\Entities\Room\Room::class)->make(['is_booking' => 1]);
        $this->assertTrue($bbkRoom->isBisaBooking());

        $nonBbkRoom = factory(\App\Entities\Room\Room::class)->make(['is_booking' => 0]);
        $this->assertFalse($nonBbkRoom->isBisaBooking());
    }

    public function testUpdateSortScoreShouldSuccessUpdateSortScore()
    {
        $room = factory(\App\Entities\Room\Room::class)->create(['sort_score' => -1]);
        $room->updateSortScore();

        // assert with checking on db
        $roomResult = Room::find($room->id);
        $this->assertNotEquals(-1, $roomResult->sort_score);
    }

    public function testGetRealPriceShouldCorrect()
    {
        $room = factory(\App\Entities\Room\Room::class)->create([
            'price_daily' => 100000,
            'price_weekly' => 600000,
            'price_monthly' => 1000000,
        ]);

        $bookingDiscount = factory(\App\Entities\Booking\BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'price' => 120000,
            'is_active' => 1,
        ]);

        $response = $room->getRealPrices();
        $this->assertEquals(100000, $response['price_daily']);
        $this->assertEquals(600000, $response['price_weekly']);
        $this->assertEquals(120000, $response['price_monthly']);
    }

    public function testGetAllActiveDiscountShouldCorrect()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $bookingDiscount = factory(\App\Entities\Booking\BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
        ]);

        $response = $room->getAllActiveDiscounts();
        $this->assertEquals(1, count($response));
    }

    public function testGetIsGuaranteeShouldCorrectReturnTrue()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $tag = factory(\App\Entities\Room\Element\Tag::class)->create([
            'name' => Room::TAG_GUARANTEE_NAME,
        ]);
        factory(\App\Entities\Room\Element\RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id,
        ]);

        $this->assertTrue($room->getIsGuarantee());
    }

    public function testGetFacilitiesForListWithCodeList()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'Kamar Mandi',
                'code' => 'list'
            ]
        );

        factory(\App\Entities\Room\Element\RoomTag::class)->create(
            [
                'tag_id' => $tag->id,
                'designer_id' => $room->id,
            ]
        );

        $response = $room->getFacilitiesForList();

        $this->assertCount(1, $response);
        $this->assertEquals($tag->name, $response[0]);
    }

    public function testGetFacilitiesForListWithoutCodeList()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'AC',
                'code' => ''
            ]
        );

        factory(\App\Entities\Room\Element\RoomTag::class)->create(
            [
                'tag_id' => $tag->id,
                'designer_id' => $room->id,
            ]
        );

        $response = $room->getFacilitiesForList();

        $this->assertCount(0, $response);
        $this->assertEquals([], $response);
    }

    public function testGetFacilitiesForListWithDuplicatedTags()
    {
        $room = factory(Room::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'Akses 24 Jam',
                'code' => 'list'
            ]
        );

        factory(RoomTag::class, mt_rand(2, 4))
            ->create()
            ->each(
                function ($roomTag) use ($room, $tag) {
                    $roomTag->designer_id = $room->id;
                    $roomTag->tag_id = $tag->id;
                    $roomTag->save();
                }
            );

        $response = $room->getFacilitiesForList();

        $this->assertCount(1, $response);
    }

    public function testGetFacilitiesForListWithSorting()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        $tag1 = factory(Tag::class)->create(
            [
                'id'   => 5,
                'name' => 'Kamar Mandi',
                'code' => 'list'
            ]
        );

        $tag2 = factory(Tag::class)->create(
            [
                'id'   => 4,
                'name' => 'Kasur',
                'code' => 'list'
            ]
        );

        factory(\App\Entities\Room\Element\RoomTag::class)->create(
            [
                'tag_id' => $tag1->id,
                'designer_id' => $room->id,
            ]
        );

        factory(\App\Entities\Room\Element\RoomTag::class)->create(
            [
                'tag_id' => $tag2->id,
                'designer_id' => $room->id,
            ]
        );

        $response = $room->getFacilitiesForList();

        $this->assertCount(2, $response);
        $this->assertEquals($tag2->name, $response[0]);
        $this->assertEquals($tag1->name, $response[1]);
    }


    public function testGetIsGuaranteeShouldCorrectReturnFalse()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $tag = factory(\App\Entities\Room\Element\Tag::class)->create();
        factory(\App\Entities\Room\Element\RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id,
        ]);

        $this->assertFalse($room->getIsGuarantee());
    }

    public function testGetUsedBalanceShouldReturnTodayValue()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Premium\AdHistory::class)->create(
            [
                'total' => 23000,
                'designer_id' => $room,
            ]
        );

        $this->assertEquals(23000, $room->getUsedBalance(StatsLib::RangeToday));
        $this->assertEquals(0, $room->getUsedBalance(StatsLib::RangeYesterday));
    }

    public function testGetUsedBalanceFromUnpromoteRoom()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        $this->assertEquals(0, $room->getUsedBalance());
    }

    public function testPropertyRelationship()
    {
        $property = factory(Property::class)->create();
        $room = factory(Room::class)->create(['property_id' => $property->id]);
        $this->assertEquals($property->id, $room->property->id);
    }

    public function testProgress(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => 'App\Entities\Room\Room',
            'progressable_id' => $room->id
        ]);

        $this->assertTrue($room->progress()->first()->is($progress));
    }

    private function generateRelatedRoomByRadius5Km()
    {
        $user = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.690629236580304',
                'longitude'     => '110.42276155203581',
                'gender'        => 2,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => $user,
                'expired_phone' => 0,
            ]);

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.6953217235723885',
                'longitude'     => '110.41623909026384',
                'gender'        => 0,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => $user2,
                'expired_phone' => 0,
            ]
        );
    }

    private function generatedRelatedApartmentByRadius5Km()
    {
        $user = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.690629236580304',
                'longitude'             => '110.42276155203581',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => $user2,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-2',
            ]
        );


        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.6953217235723885',
                'longitude'             => '110.41623909026384',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => $user,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-3',
            ]
        );
    }

    public function testGetRelatedRoomByRadius5KmShouldSuccess()
    {
        $this->app->device = null;

        $room = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.6959314141342325',
                'longitude'     => '110.41652172803879',
                'gender'        => 2,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => factory(\App\User::class)->create(),
                'expired_phone' => 0,
            ]);

        $this->generateRelatedRoomByRadius5Km();

        $suggestion = $room->getRelatedByRadius(PropertyType::KOS);

        $this->assertCount(2, $suggestion['data']);
    }

    public function testGetRelatedApartmentByRadius5KmShouldSuccess()
    {
        $this->app->device = null;

        $room = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.6959314141342325',
                'longitude'             => '110.41652172803879',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => factory(\App\User::class)->create(),
                'expired_phone'         => 0,
                'slug'                  => 'apartment-1',
            ]
        );

        $this->generatedRelatedApartmentByRadius5Km();

        $suggestion = $room->getRelatedByRadius(PropertyType::APARTMENT);

        $this->assertCount(2, $suggestion['data']);
    }

    public function testGetNearbyRoomPriceByRadius5KmShouldSuccess()
    {
        $user = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();
        $apartmentProject = factory(\App\Entities\Apartment\ApartmentProject::class)->create();

        /**
         * Room stuff start
         */
        $room = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.6959314141342325',
                'longitude'     => '110.41652172803879',
                'gender'        => 2,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => $user,
                'expired_phone' => 0,
                'price_daily'   => 55000,
                'price_monthly' => 500000,
                'price_yearly'  => 6000000
            ]
        );

        $room2 = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.690629236580304',
                'longitude'     => '110.42276155203581',
                'gender'        => 2,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => $user2,
                'expired_phone' => 0,
            ]
        );

        $room3 = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'      => '-7.6953217235723885',
                'longitude'     => '110.41623909026384',
                'gender'        => 0,
                'is_promoted'   => 'false',
                'is_active'     => true,
                'user_id'       => $user2,
                'expired_phone' => 0,
                'price_monthly' => 600000,
                'price_yearly'  => 8000000
            ]
        );
        /**
         * Room stuff end
         */

        /**
         * Apartment stuff start
         */
        $apartment = factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.6959314141342325',
                'longitude'             => '110.41652172803879',
                'apartment_project_id'  => $apartmentProject->id,
                'is_active'             => true,
                'user_id'               => $user,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-1',
            ]
        );

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.6959314141342325',
                'longitude'             => '110.41652172803879',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => $user,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-11',
                'price_daily'           => 500000,
                'price_monthly'          => 3000000,
                'price_yearly'          => 23000000
            ]
        );

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-7.690629236580304',
                'longitude'             => '110.42276155203581',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => $user2,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-2',
                'price_daily'           => 650000,
                'price_monthly'          => 3500000,
                'price_yearly'          => 29000000
            ]
        );

        factory(\App\Entities\Room\Room::class)->create(
            [
                'latitude'              => '-9.6953217235723885',
                'longitude'             => '110.41623909026384',
                'apartment_project_id'  => factory(\App\Entities\Apartment\ApartmentProject::class)->create(),
                'is_active'             => true,
                'user_id'               => $user2,
                'expired_phone'         => 0,
                'slug'                  => 'apartment-3',
                'price_daily'           => 800000,
                'price_monthly'         => 8000000,
                'price_yearly'          => 33000000
            ]
        );
        /**
         * Apartment stuff end
         */

        $apartmentNP = $apartment->getNearbyPriceByRadius(5000);
        $roomNP = $room2->getNearbyPriceByRadius(5000);

        $apartmentShouldReturn = [
            'daily' => [
                'average' => 575000,
                'min'     => 500000,
                'max'     => 650000,
            ],
            'monthly' => [
                'average' => 3250000,
                'min'     => 3000000,
                'max'     => 3500000,
            ],
            'yearly' => [
                'average' => 26000000,
                'min'     => 23000000,
                'max'     => 29000000,
            ],
            'weekly' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
            'quarterly' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
            'semiannually' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
        ];

        $roomShouldReturn = [
            'daily' => [
                'average' => 55000,
                'min'     => 55000,
                'max'     => 55000,
            ],
            'monthly' => [
                'average' => 550000,
                'min'     => 500000,
                'max'     => 600000,
            ],
            'yearly' => [
                'average' => 7000000,
                'max'     => 8000000,
                'min'     => 6000000,
            ],
            'weekly' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
            'quarterly' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
            'semiannually' => [
                'average'   => 0,
                'max'       => 0,
                'min'       => 0
            ],
        ];

        $this->assertEquals($roomShouldReturn, $roomNP);
        $this->assertEquals($apartmentShouldReturn, $apartmentNP);
    }

    public function testGetGoldLevelStatusGiveResponseGp1()
    {
        $room = factory(Room::class)->create();
        $kostLevelId = 9; // GP 1 on env.testing

        // delete goldplus if exist on db testing
        $level = KostLevel::find($kostLevelId);
        if ($level) $level->delete();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => $kostLevelId,
                'name' => 'Mamikos Goldplus 1',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals($goldLevel->name, $room->getGoldLevelStatus());
    }

    public function testGetGoldLevelStatusGiveResponseGp2()
    {
        $room = factory(Room::class)->create();
        $kostLevelId = 183; // GP 2 on env.testing

        // delete goldplus if exist on db testing
        $level = KostLevel::find($kostLevelId);
        if ($level) $level->delete();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => $kostLevelId,
                'name' => 'Mamikos Goldplus 2',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals($goldLevel->name, $room->getGoldLevelStatus());
    }

    public function testGetGoldLevelStatusGiveResponseGp3()
    {
        $room = factory(Room::class)->create();
        $kostLevelId = 10; // GP 3 on env.testing

        // delete goldplus if exist on db testing
        $level = KostLevel::find($kostLevelId);
        if ($level) $level->delete();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => $kostLevelId,
                'name' => 'Mamikos Goldplus 3',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals($goldLevel->name, $room->getGoldLevelStatus());
    }

    public function testGetGoldLevelStatusGiveResponseGp4()
    {
        $room = factory(Room::class)->create();
        $kostLevelId = 187; // GP 4 on env.testing

        // delete goldplus if exist on db testing
        $level = KostLevel::find($kostLevelId);
        if ($level) $level->delete();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'id' => $kostLevelId,
                'name' => 'Mamikos Goldplus 4',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals($goldLevel->name, $room->getGoldLevelStatus());
    }

    public function testGetGoldLevelStatusGiveResponseNonGp()
    {
        $room = factory(Room::class)->create();

        $goldLevel = factory(KostLevel::class)->create(
            [
                'name' => 'OYO',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $goldLevel->id
            ]
        );

        $this->assertEquals('Non-GP', $room->getGoldLevelStatus());
    }

    public function testGetTaskNameAttribute(): void
    {
        $room = factory(Room::class)->create();
        $expected = $room->name;
        $actual = $room->getTaskNameAttribute();

        $this->assertEquals($expected, $actual);
    }

    public function testGetAreaFormattedAttribute()
    {
        $room = factory(Room::class)->make();
        $this->assertNotEmpty($room->area_formatted);
    }
    public function testBookinAcceptanceRateRelation()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $this->assertInstanceOf(BookingAcceptanceRate::class, $room->booking_acceptance_rate);
        $this->assertEquals($bar->id, $room->booking_acceptance_rate->id);
        $this->assertEquals($bar->rate, $room->booking_acceptance_rate->rate);
    }

    public function testBookingOwnerRequestRelation()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
        ]);

        $this->assertInstanceOf(Collection::class, $room->booking_owner_requests);
        $this->assertEquals(1, $room->booking_owner_requests->count());
    }

    public function testKostValuesRelationship()
    {
        $room = factory(Room::class)->create();

        // Creating `kost_level_value` with pivot
        $valuesCount = $this->faker->numberBetween(3, 5);
        $room->kost_values()->saveMany(
            factory(KostLevelValue::class, $valuesCount)->create()
        );

        $kostValues = $room->kost_values();

        $this->assertInstanceOf(BelongsToMany::class, $kostValues);
        $this->assertEquals($valuesCount, $kostValues->count());
    }

    public function testGetPriceQuarterlyAttribute()
    {
        $room = factory(Room::class)->create([
            'price_quarterly' => 3000000
        ]);
        $this->assertEquals(3000000, $room->price_quarterly);
    }

    public function testGetPriceSemiannuallyAttribute()
    {
        $room = factory(Room::class)->create([
            'price_semiannually' => 5500000
        ]);
        $this->assertEquals(5500000, $room->price_semiannually);
    }

    public function testGetIsKosPartner()
    {
        $room = factory(Room::class)->create();
        $partnerLevel = factory(KostLevel::class)->create([
            'name' => 'OYO Partner',
            'is_regular' => 0,
            'is_partner' => 1,
            'is_hidden' => 0
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $partnerLevel->id
        ]);
        $this->assertTrue($room->getIsKosPartner());
    }
    
    public function testGetIsKosPartnerReturnFalse()
    {
        $room = factory(Room::class)->create();
        $partnerLevel = factory(KostLevel::class)->create([
            'name' => 'Mamitest Level Partner',
            'is_regular' => 0,
            'is_partner' => 0,
            'is_hidden' => 0
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $partnerLevel->id
        ]);
        $this->assertFalse($room->getIsKosPartner());
    }

    public function testGetPageUrl()
    {
        $room = factory(Room::class)->make(['slug' => 'sari-roti']);
        $baseUrl = config('app.url');
        $this->assertEquals($baseUrl . "/room/sari-roti", $room->getPageUrl());
    }

    public function testGetRecommendationByRadiusWithExpandAbleLogic()
    {
        app()->device = null;

        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null,
            'expired_phone' => 0,
            'room_available' => 2,
            'gender' => 2,
            'price_monthly' => 1000000,
            'longitude' => 110.38789987564088,
            'latitude' => -7.756302042883416,
        ]);

        $recommendedRooms = factory(Room::class, 10)->create([
            'is_active' => 'true',
            'apartment_project_id' => null,
            'expired_phone' => 0,
            'room_available' => 2,
            'gender' => 2,
            'price_monthly' => 1000000,
            'longitude' => ($room->longitude + 0.0001),
            'latitude' => ($room->latitude + 0.0001),
        ]);

        $tag = factory(Tag::class)->create(['id' => 1]);

        foreach ($recommendedRooms as $recommendedRoom) {
                factory(RoomTag::class)->create(
                    [
                        'designer_id' => $recommendedRoom->id,
                        'tag_id' => $tag->id
                    ]
                );
        }

        $response = $room->getRecommendationByRadius();
        $this->assertEquals(10, $response['total_room']);
        $this->assertEquals(($room->price_monthly - 150000), $response['filters']['price_range'][0]);
        $this->assertEquals(($room->price_monthly + 150000), $response['filters']['price_range'][1]);
    }

    public function testGetRecommendationByRadiusWithoutExpandAbleLogic()
    {
        app()->device = null;

        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null,
            'expired_phone' => 0,
            'room_available' => 2,
            'gender' => 2,
            'price_monthly' => 1000000,
            'longitude' => 110.38789987564088,
            'latitude' => -7.756302042883416,
        ]);

        $recommendedRooms = factory(Room::class, 25)->create([
            'is_active' => 'true',
            'apartment_project_id' => null,
            'expired_phone' => 0,
            'room_available' => 2,
            'gender' => 2,
            'price_monthly' => 1000000,
            'longitude' => ($room->longitude + 0.0001),
            'latitude' => ($room->latitude + 0.0001),
        ]);

        $tag = factory(Tag::class)->create(['id' => 1]);

        foreach ($recommendedRooms as $recommendedRoom) {
            factory(RoomTag::class)->create(
                [
                    'designer_id' => $recommendedRoom->id,
                    'tag_id' => $tag->id
                ]
            );
        }

        $response = $room->getRecommendationByRadius();

        $this->assertEquals(20, $response['total_room']);
        $this->assertEquals(($room->price_monthly - 100000), $response['filters']['price_range'][0]);
        $this->assertEquals(($room->price_monthly + 100000), $response['filters']['price_range'][1]);
    }

    public function testGetRecommendationPriceRangeWithIsExpandedTrue()
    {
        $monthlyPrice = 1000000;
        $room = new room;

        $method= $this->getNonPublicMethodFromClass(Room::class, 'getRecommendationPriceRange');

        $response = $method->invokeArgs(
            $room,
            [
                $monthlyPrice,
                true
            ]);
        $this->assertEquals(($monthlyPrice - 150000), $response['lower_limit']);
        $this->assertEquals(($monthlyPrice + 150000), $response['upper_limit']);
    }

    public function testGetRecommendationPriceRangeWithIsExpandedFalse()
    {
        $monthlyPrice = 1000000;
        $room = new room;

        $method= $this->getNonPublicMethodFromClass(Room::class, 'getRecommendationPriceRange');

        $response = $method->invokeArgs(
            $room,
            [
                $monthlyPrice,
                false
            ]);
        $this->assertEquals(($monthlyPrice - 100000), $response['lower_limit']);
        $this->assertEquals(($monthlyPrice + 100000), $response['upper_limit']);
    }

    public function testGetRecommendationPriceRangeWithIsExpandedTrueAndMonthlyPrice150000()
    {
        $monthlyPrice = 150000;
        $room = new room;

        $method= $this->getNonPublicMethodFromClass(Room::class, 'getRecommendationPriceRange');

        $response = $method->invokeArgs(
            $room,
            [
                $monthlyPrice,
                true
            ]);
        $this->assertEquals($monthlyPrice, $response['lower_limit']);
        $this->assertEquals(($monthlyPrice + 150000), $response['upper_limit']);
    }

    public function testGetRecommendationPriceRangeWithIsExpandedFalseAndMonthlyPrice100000()
    {
        $monthlyPrice = 100000;
        $room = new room;

        $method= $this->getNonPublicMethodFromClass(Room::class, 'getRecommendationPriceRange');

        $response = $method->invokeArgs(
            $room,
            [
                $monthlyPrice,
                false
            ]);
        $this->assertEquals($monthlyPrice, $response['lower_limit']);
        $this->assertEquals(($monthlyPrice + 100000), $response['upper_limit']);
    }

    public function testIsRoomGPLevelTrue()
    {
        $room = factory(Room::class)->create([
            'song_id' => 123
        ]);

        $kostLevel = factory(KostLevel::class)->create([
            'id' => 9,
            'name' => 'Goldplus 1'
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $kostLevel->id
        ]);

        $kostLevel->getGoldplusLevelIds();

        $result = $room->isRoomGPLevel();
        $this->assertTrue($result);
    }

    public function testIsRoomGPLevelFalse()
    {
        $room = factory(Room::class)->create([
            'song_id' => 123
        ]);
        $kostLevel = factory(KostLevel::class)->create([
            'id' => 6,
            'name' => 'Non GP'
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $kostLevel->id
        ]);

        $kostLevel->getGoldplusLevelIds();

        $result = $room->isRoomGPLevel();
        $this->assertFalse($result);
    }
    
    /**
     * @group BG-3096
     */
    public function testNotificationRelation()
    {
        $room = factory(Room::class)->create(['is_active' => 1]);
        factory(Notification::class)->create([
            'designer_id' => $room->id,
            'type' => 'chat',
            'identifier_type' => 'Room'
        ]);

        $this->assertInstanceOf(Collection::class, $room->notifications);
        $this->assertEquals(1, $room->notifications->count());
    }

    /**
     * @group BG-3096
     */
    public function testLevelHistoriesRelation()
    {
        $room = factory(Room::class)->create(['is_active' => 1]);
        factory(LevelHistory::class)->create(['level_id' => $room->id]);

        $this->assertInstanceOf(Collection::class, $room->level_histories);
        $this->assertEquals(1, $room->level_histories->count());
    }

    /**
     * @group BG-3096
     */
    public function testPhotoByRelation()
    {
        $room = factory(Room::class)->create(['is_active' => 1]);
        factory(PhotoBy::class)->create(['identifier' => $room->id]);
        
        $this->assertInstanceOf(Collection::class, $room->photo_by);
        $this->assertEquals(1, $room->photo_by->count());
    }

    /**
     * @group BG-3096
     */
    public function testOwnerSurveyRelation()
    {
        $room = factory(Room::class)->create(['is_active' => 1]);
        factory(SurveySatisfaction::class)->create(['identifier' => $room->id]);

        $this->assertInstanceOf(Collection::class, $room->owner_survey);
        $this->assertEquals(1, $room->owner_survey->count());
    }

    /**
     * @group BG-3096
     */
    public function testFacilitySettingRelation()
    {
        $room   = factory(Room::class)->create();
        factory(RoomFacilitySetting::class)->create([
            'designer_id'   => $room->id,
        ]);

        $this->assertInstanceOf(RoomFacilitySetting::class, $room->facility_setting);
    }

    public function testgetLatestOwnerUserId() {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => 10]);
        sleep(1); // give time to create the next one to make sure time gap.
        factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => 20]);
        $this->assertEquals(20, $room->getLatestOwnerUserId());
    }

    public function testgetLatestOwnerUserIdWithOwnerNotFound() {
        $room = factory(Room::class)->create();
        $this->assertEquals(null, $room->getLatestOwnerUserId());
    }

    public function testPotentialProperty(): void
    {
        $kost = factory(Room::class)->create();
        $potential = factory(PotentialProperty::class)->create(['designer_id' => $kost->id]);
        $this->assertTrue(
            $kost->potential_property->is($potential)
        );
    }

    public function testPriceComponents(): void
    {
        $kost = factory(Room::class)->create();
        $component = factory(RoomPriceComponent::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $kost->price_components->first()->is($component)
        );
    }

    public function testReports(): void
    {
        $kost = factory(Room::class)->create();
        $report = factory(Report::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $kost->reports->first()->is($report)
        );
    }

    public function testHistoryUser(): void
    {
        $kost = factory(Room::class)->create();
        $history = factory(History::class)->create(['designer_id' => $kost->id]);

        $this->assertTrue(
            $kost->history_user->first()->is($history)
        );
    }

    /**
     * @group PMS-495
     */
    public function testDesignerRuleTagsRelation()
    {
        $room = factory(Room::class)->create();
        $tag = factory(Tag::class)->create();
        factory(TagType::class)->create([
            'tag_id' => $tag->id,
            'name' => 'kos_rule'
        ]);
        $roomTag = factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id
        ]);

        $this->assertEquals($roomTag->id, $room->designer_rule_tags[0]->id);
    }

    /**
     * @group PMS-495
     */
    public function testSyncRoomRuleTags()
    {
        $room = factory(Room::class)->create();
        $tags = factory(Tag::class, 3)->create();
        factory(TagType::class)->create([
            'tag_id' => $tags[0]->id,
            'name' => 'kos_rule'
        ]);
        factory(TagType::class)->create([
            'tag_id' => $tags[1]->id,
            'name' => 'not_kos_rule'
        ]);
        factory(TagType::class)->create([
            'tag_id' => $tags[2]->id,
            'name' => 'kos_rule'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tags[0]->id,
            'designer_id' => $room->id
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tags[1]->id,
            'designer_id' => $room->id
        ]);

        $room->syncRoomRuleTags([$tags[2]->id]);
        $roomTags = $room->designer_tags->pluck('tag_id')->toArray();

        $this->assertEmpty(array_diff([$tags[1]->id, $tags[2]->id], $roomTags));
    }

    /**
     * @group PMS-495
     */
    public function testSyncRoomTags()
    {
        $room = factory(Room::class)->create();
        $tags = factory(Tag::class, 3)->create();
        factory(TagType::class)->create([
            'tag_id' => $tags[0]->id,
            'name' => 'kos_rule'
        ]);
        factory(TagType::class)->create([
            'tag_id' => $tags[1]->id,
            'name' => 'not_kos_rule'
        ]);
        factory(TagType::class)->create([
            'tag_id' => $tags[2]->id,
            'name' => 'not_kos_rule'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tags[0]->id,
            'designer_id' => $room->id
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tags[1]->id,
            'designer_id' => $room->id
        ]);

        $room->syncRoomTags([$tags[2]->id]);
        $roomTags = $room->designer_tags->pluck('tag_id')->toArray();

        $this->assertEmpty(array_diff([$tags[0]->id, $tags[2]->id], $roomTags));
    }

    /*
     * Room's Geolocation
     * Ref task: https://mamikos.atlassian.net/browse/BG-3555
     */
    public function testGeolocationRelation()
    {
        factory(Room::class)
            ->create()
            ->each(function ($room) {
                $room->geolocation()->save(
                    factory(Geolocation::class)->make()
                );
            });

        $room = Room::with('geolocation')->first();

        $this->assertInstanceOf(Geolocation::class, $room->geolocation);
    }

    public function testGetChatCount()
    {
        $owner = factory(User::class)->create();
        $kost = factory(Room::class)->create();

        $kostOwnerRelation = factory(RoomOwner::class)->create([
                'designer_id' => $kost->id, 
                'user_id' => $owner->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

        // create 2 calls before $kostOwnerRelation->created_at 
        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->subDay()
        ]);

        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->subHours(1)
        ]);

        // create 1 soft-deleted call before $kostOwnerRelation->created_at 
        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->subHours(1),
            'deleted_at' => Carbon::parse($kostOwnerRelation->created_at)->subMinutes(30)
        ]);

        // create 1 call at $kostOwnerRelation->created_at
        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => $kostOwnerRelation->created_at
        ]);

        // create 2 calls after $kostOwnerRelation->created_at 
        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->addHours(1)
        ]);

        // create 1 soft-deleted calls after $kostOwnerRelation->created_at 
        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->addHours(1),
            'deleted_at' => Carbon::parse($kostOwnerRelation->created_at)->addMinutes(30),
        ]);

        $call = factory(Call::class)->create([
            'designer_id' => $kost->id,
            'created_at' => Carbon::parse($kostOwnerRelation->created_at)->addDay()
        ]);

        $this->assertEquals(3, $kost->getChatCount(true));
        $this->assertEquals(5, $kost->getChatCount(false));
        $this->assertEquals(5, $kost->getChatCount());
    }

    public function testRevisionHistory(): void
    {
        $room = factory(Room::class)->create();
        $expected = factory(Revision::class)->create([
            'revisionable_type' => Room::class,
            'revisionable_id' => $room->id,
        ]);

        $this->assertTrue($room->revision_histories()->first()->is($expected));
    }

    /**
     * @group PMS-495
     */
    public function testDesignerPaymentTagRelation()
    {
        $room = factory(Room::class)->create();
        $tag = factory(Tag::class)->create([
            'type' => 'keyword'
        ]);
        $roomTag = factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id
        ]);

        $this->assertEquals($roomTag->id, $room->designer_payment_tag->id);
    }

    /**
     * @group PMS-495
     */
    public function testSyncPaymentTag()
    {
        $room = factory(Room::class)->create();
        $tags = factory(Tag::class, 2)->create([
            'type' => 'keyword'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tags[0]->id,
            'designer_id' => $room->id
        ]);

        $room->syncPaymentTag($tags[1]->id);
        $roomTag = $room->designer_payment_tag->tag_id;

        $this->assertEquals($tags[1]->id, $roomTag);
    }

    public function testPotentialPropertyUnpaid()
    {
        $this->assertInstanceOf(HasOne::class, (new Room)->potential_property_unpaid());
    }

    public function testPremiumRequest()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->premium_request());
    }

    public function testReferralTracking()
    {
        $this->assertInstanceOf(HasOne::class, (new Room)->referral_tracking());
    }

    public function testClassHistory()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->class_history());
    }

    public function testReportSummary()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->report_summary());
    }

    public function testReportSummaryWithinMonth()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->report_summary_within_month());
    }

    public function testAgentData()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->agent_data());
    }

    public function testTrackingUpdate()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->tracking_update());
    }

    public function testListingReason()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->listing_reason());
    }

    public function testDummyDesigner()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->dummy_designer());
    }

    public function testDummyPhoto()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->dummy_photo());
    }

    public function testDummyReview()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->dummy_review());
    }

    public function testRoomVerify()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->room_verify());
    }

    public function testNormalizedCity()
    {
        $this->assertInstanceOf(HasOne::class, (new Room)->normalized_city());
    }

    public function testRoomPrices()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->room_prices());
    }

    public function testRoomPriceAdditional()
    {
        $this->assertInstanceOf(HasMany::class, (new Room)->room_price_additional());
    }

    public function testAreaBigMapper()
    {
        $this->assertInstanceOf(HasOne::class, (new Room)->area_big_mapper());
    }

    public function testScopeIsApartment()
    {
        $apartmentProject = factory(ApartmentProject::class)->create();
        $room = factory(Room::class)->create([
            'apartment_project_id' => $apartmentProject->id
        ]);

        $res = Room::with('apartment_project')->isApartment()->find($room->id);

        $this->assertInstanceOf(Room::class, $res);
        $this->assertInstanceOf(ApartmentProject::class, $res->apartment_project);
    }

    public function testScopeSearchIdExists()
    {
        $room = factory(Room::class)->create();

        $this->assertInstanceOf(Room::class, Room::search($room->id)->first());
    }

    public function testScopeSearchIdNotExists()
    {
        $room = factory(Room::class)->create([
            'name' => 'testing'
        ]);

        $this->assertInstanceOf(Room::class, Room::search($room->name)->first());
    }

    public function testScopeWhereDuplicate()
    {
        $room = factory(Room::class)->create();
        factory(Room::class)->create([
            'duplicate_from' => $room->id
        ]);

        $this->assertInstanceOf(Room::class, Room::whereDuplicate()->first());
    }
}
