<?php

namespace App\Entities\Room;

use App\Test\MamiKosTestCase;
use App\User;

class ExpiredByTest extends MamiKosTestCase
{
    public function testRelationToRoom()
    {
        $room = factory(Room::class)->create();
        $expiredBy = factory(ExpiredBy::class)->create(['designer_id' => $room->id]);

        $this->assertSame($room->id, $expiredBy->room->id);
    }

    public function testInsertFirstExpiredBy()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);

        ExpiredBy::insertExpired($roomOwner, 'admin');

        $this->assertDatabaseHas('designer_expired', ['designer_id' => $room->id]);
    }

    public function testInsertSecondExpiredBy()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        factory(ExpiredBy::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'expired_by' => 'owner',
        ]);

        ExpiredBy::insertExpired($roomOwner, 'admin');

        $this->assertDatabaseHas('designer_expired', ['designer_id' => $room->id, 'expired_by' => 'admin']);
    }

    public function testDeleteExpiredBy()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        factory(ExpiredBy::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'expired_by' => 'owner',
        ]);

        ExpiredBy::deleteExpired($room);

        $this->assertSoftDeleted('designer_expired', ['designer_id' => $room->id, 'expired_by' => 'owner']);
    }
}
