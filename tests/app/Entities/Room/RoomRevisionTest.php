<?php

namespace App\Entities\RoomRevision;

use App\Entities\Room\RoomRevision;
use App\Test\MamiKosTestCase;
use App\User;

class RoomRevisionTest extends MamiKosTestCase
{
    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $roomRevision = factory(RoomRevision::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertEquals($user->id, $roomRevision->user->id);
    }
}
