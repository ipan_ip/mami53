<?php

namespace App\Entities\Room;

use App\Entities\Room\Room;
use App\Entities\Room\RoomPriceType;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomPriceTypeTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    public function testRoomRelationship()
    {
        $roomPriceType = factory(RoomPriceType::class)->create();

        $room = factory(Room::class)->create();

        $roomPriceType->setRelation('room', $room);

        $this->assertEquals($room->id, $roomPriceType->room->id);
    }
}