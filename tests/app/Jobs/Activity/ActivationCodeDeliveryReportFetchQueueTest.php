<?php

namespace App\Jobs\Activity;

use App\Test\MamiKosTestCase;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeDeliveryReport;
use App\Services\Activity\ActivationCodeService;
use Mockery;
use App\Jobs\Activity\ActivationCodeDeliveryReportFetchQueue;
use Exception;

/**
 * @property \Mockery\MockInterface|\Mockery\LegacyMockInterface $activationCodeservice
 */
class ActivationCodeDeliveryReportFetchQueueTest extends MamiKosTestCase
{
    private $activationCodeservice;

    protected function setUp(): void
    {
        parent::setUp();
        $this->activationCodeservice = $this->mock(ActivationCodeService::class);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    private function createJob($activationCode, $providerMessageId): ActivationCodeDeliveryReportFetchQueue
    {
        /** @var ActivationCodeDeliveryReportFetchQueue  */
        $job = $this->app->make(ActivationCodeDeliveryReportFetchQueue::class, [
            'activationCode' => $activationCode,
            'providerMessageId' => $providerMessageId,
        ]);
        return $job;
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App/Jobs/Activity/ActivationCodeDeliveryReportFetchQueue
     */
    public function testHandleValidReport()
    {
        $this->activationCodeservice->shouldReceive('fetchDeliveryReport')->once()->andReturn(factory(ActivationCodeDeliveryReport::class)->make());
        $this->activationCodeservice->shouldReceive('updateDeliveryReport')->once();
        $job = $this->createJob(factory(ActivationCode::class)->make(), str_random(10));
        dispatch_now($job);
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App/Jobs/Activity/ActivationCodeDeliveryReportFetchQueue
     */
    public function testHandleNullConstructParam()
    {
        $job = $this->createJob(null, str_random(10));
        dispatch_now($job);
        $this->activationCodeservice->shouldNotHaveReceived('fetchDeliveryReport');
        $this->activationCodeservice->shouldNotHaveReceived('updateDeliveryReport');
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App/Jobs/Activity/ActivationCodeDeliveryReportFetchQueue
     */
    public function testHandleNullReport()
    {
        $this->activationCodeservice->shouldReceive('fetchDeliveryReport')->once()->andReturn(null);
        $job = $this->createJob(factory(ActivationCode::class)->make(), str_random(10));
        dispatch_now($job);
        $this->activationCodeservice->shouldNotHaveReceived('updateDeliveryReport');
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1500
     * @group App/Jobs/Activity/ActivationCodeDeliveryReportFetchQueue
     */
    public function testFailedForCoverage()
    {
        $job = $this->createJob(factory(ActivationCode::class)->make(), str_random(10));
        $job->failed(new Exception());
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1500
     * @group App/Jobs/Activity/ActivationCodeDeliveryReportFetchQueue
     */
    public function testHandleFailedOnLastAttempt()
    {
        $job = $this->createJob(null, str_random(10));
        $job->tries = 1; // Simulate last attempt
        dispatch_now($job);
        $this->assertTrue(true);
    }
}
