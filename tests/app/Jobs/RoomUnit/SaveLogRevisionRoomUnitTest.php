<?php

namespace App\Jobs\RoomUnit;

use App\Entities\Log\RoomUnitRevision;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class SaveLogRevisionRoomUnitTest extends MamiKosTestCase
{
    
    public function testSaveLogRevision()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
        ]);

        $job = new SaveLogRevisionRoomUnit(
            'create',
            $user->id,
            $user->name,
            date('Y-m-d H:i:s'),
            $roomUnit->toArray()
        );
        $job->handle();

        $logRevision = RoomUnitRevision::where('designer_id', $room->id)->first();
        $this->assertEquals('create', $logRevision->action);
        $this->assertEquals($roomUnit->id, $logRevision->old_value['id']);
    }
}
