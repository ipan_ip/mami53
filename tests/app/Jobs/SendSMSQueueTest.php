<?php

namespace App\Jobs;

use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Queue;
use Mockery;

/**
 * Class SendSMSQueueTest
 * @package App\Jobs
 */
class SendSMSQueueTest extends MamiKosTestCase
{
    const PHONE_NUMBER  = '0822xxxx324';
    const MESSAGE       = 'xxxxxx';

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Libraries\SMSLibrary');
    }

    public function testJobQueuePhoneParamValidSuccess()
    {
        $phone = self::PHONE_NUMBER;
        Queue::fake();

        SendSMSQueue::dispatch(self::PHONE_NUMBER, self::MESSAGE);

        Queue::assertPushed(SendSMSQueue::class, function ($job) use ($phone) {
            return $this->getNonPublicAttributeFromClass('phoneNumber', $job) === $phone;
        });
    }

    public function testJobQueueMessageParamValidSuccess()
    {
        $message = self::MESSAGE;
        Queue::fake();

        SendSMSQueue::dispatch(self::PHONE_NUMBER, self::MESSAGE);

        Queue::assertPushed(SendSMSQueue::class, function ($job) use ($message) {
            return $this->getNonPublicAttributeFromClass('message', $job) === $message;
        });
    }

    public function testJobQueueNotPushedFailed()
    {
        Queue::fake();
        Queue::assertNotPushed(SendSMSQueue::class);
    }

    public function testJobQueueTwicePushSuccess()
    {
        Queue::fake();

        // first push
        SendSMSQueue::dispatch(self::PHONE_NUMBER, self::MESSAGE);

        // second push
        SendSMSQueue::dispatch(self::PHONE_NUMBER, self::MESSAGE);

        // check queue push twice ( 2 )
        Queue::assertPushed(SendSMSQueue::class, 2);
    }

    public function testHandle_Success(): void
    {
        $this->mock->shouldReceive('smsVerification')
                ->andReturn([]);

        Queue::fake();
        SendSMSQueue::dispatch(self::PHONE_NUMBER, self::MESSAGE);
        Queue::assertPushed(SendSMSQueue::class, 1);
        
        $job = new SendSMSQueue(self::PHONE_NUMBER, self::MESSAGE);
        $this->assertNull($job->handle());
    }

    public function testJobQueueNotPushedFailed_BugsnagCalled()
    {
        $exception = new \Exception('test');

        $this->mock->shouldReceive('smsVerification')
                ->andReturn([]);

        Bugsnag::shouldReceive('notifyException')->once();
        
        $job = new SendSMSQueue(self::PHONE_NUMBER, self::MESSAGE);
        $this->assertNull($job->failed($exception));
    }
}