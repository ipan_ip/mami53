<?php

namespace App\Jobs\MoEngage;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Queue;
use Mockery;
use phpmock\MockBuilder;

/**
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SendMoEngageTenantPropertiesTest extends MamiKosTestCase
{
    private $bugSnag;

    protected function setUp() : void
    {
        parent::setUp();
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testHandleNonProduction_Success(): void
    {
        $tenant = factory(User::class)->create();
        $mamipayTenantId = factory(MamipayTenant::class)->create([
            'user_id' => $tenant->id,
            'email' => $tenant->email,
            'name' => $tenant->name
        ]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $mamipayTenantId->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);

        Queue::fake();
        SendMoEngageTenantProperties::dispatch($tenant->id);
        Queue::assertPushed(SendMoEngageTenantProperties::class, 1);
        
        $job = new SendMoEngageTenantProperties($tenant->id);
        $this->assertNull($job->handle());
    }

    public function testHandleProduction_Success(): void
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('env')
            ->setFunction(
                function () {
                    return 'production';
                }
            );
        $this->env = $builder->build();
        $this->env->enable();

        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayTenantId = factory(MamipayTenant::class)->create([
            'user_id' => $tenant->id,
            'email' => $tenant->email,
            'name' => $tenant->name
        ]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $mamipayTenantId->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);

        Queue::fake();
        SendMoEngageTenantProperties::dispatch($tenant->id);
        Queue::assertPushed(SendMoEngageTenantProperties::class, 1);
        
        $job = new SendMoEngageTenantProperties($tenant->id);
        $this->assertNull($job->handle());

        $this->env->disable();
    }

    public function testFailed_Success(): void
    {
        $exception = new \Exception('test');

        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayTenantId = factory(MamipayTenant::class)->create([
            'user_id' => $tenant->id,
            'email' => $tenant->email,
            'name' => $tenant->name
        ]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $mamipayTenantId->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);

        $this->bugSnag->shouldReceive('notifyException')
            ->once()
            ->with('Failed to send tracker: ' . $exception->getMessage());
        
        $job = new SendMoEngageTenantProperties($tenant->id);
        $this->assertNull($job->failed($exception));
    }

    public function testCheckActiveContractViaMamipayTenantTrue()
    {
        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => $tenant->id,
            'email' => $tenant->email,
            'name' => $tenant->name
        ]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $mamipayTenant->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);

        $sendMoEngageTenant = new SendMoEngageTenantProperties($tenant->id);
        $method = $this->getNonPublicMethodFromClass(
            SendMoEngageTenantProperties::class,
            'checkActiveContract'
        );
        $res = $method->invokeArgs(
            $sendMoEngageTenant,
            [
                $tenant->id
            ]
        );

        $this->assertTrue($res);
    }

    public function testCheckActiveContractViaMamipayTenantFalse()
    {
        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => $tenant->id,
            'email' => $tenant->email,
            'name' => $tenant->name
        ]);
        factory(MamipayContract::class)->create([
            'tenant_id' => $mamipayTenant->id,
            'status' => MamipayContract::STATUS_TERMINATED
        ]);

        $sendMoEngageTenant = new SendMoEngageTenantProperties($tenant->id);
        $method = $this->getNonPublicMethodFromClass(
            SendMoEngageTenantProperties::class,
            'checkActiveContract'
        );
        $res = $method->invokeArgs(
            $sendMoEngageTenant,
            [
                $tenant->id
            ]
        );

        $this->assertFalse($res);
    }

    public function testCheckActiveContractViaBookingUserTrue()
    {
        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayContract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE
        ]);
        factory(BookingUser::class)->create([
            'contract_id' => $mamipayContract->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'user_id' => $tenant->id
        ]);

        $sendMoEngageTenant = new SendMoEngageTenantProperties($tenant->id);
        $method = $this->getNonPublicMethodFromClass(
            SendMoEngageTenantProperties::class,
            'checkActiveContract'
        );
        $res = $method->invokeArgs(
            $sendMoEngageTenant,
            [
                $tenant->id
            ]
        );

        $this->assertTrue($res);
    }

    public function testCheckActiveContractViaBookingUserFalse()
    {
        $tenant = factory(User::class)->states('tenant')->create();
        $mamipayContract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_FINISHED
        ]);
        factory(BookingUser::class)->create([
            'contract_id' => $mamipayContract->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'user_id' => $tenant->id
        ]);

        $sendMoEngageTenant = new SendMoEngageTenantProperties($tenant->id);
        $method = $this->getNonPublicMethodFromClass(
            SendMoEngageTenantProperties::class,
            'checkActiveContract'
        );
        $res = $method->invokeArgs(
            $sendMoEngageTenant,
            [
                $tenant->id
            ]
        );

        $this->assertFalse($res);
    }
}