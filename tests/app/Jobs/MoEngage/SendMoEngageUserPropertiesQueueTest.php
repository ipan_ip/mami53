<?php

namespace App\Jobs\MoEngage;

use App\Test\MamiKosTestCase;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use Illuminate\Support\Facades\Queue;
use Mockery;

/**
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 *
 * Class SendMoEngageUserPropertiesQueue
 * @package App\Jobs\Booking
 */
class SendMoEngageUserPropertiesQueueTest extends MamiKosTestCase
{
    protected $id;
    protected $props;
    private $bugSnag;

    protected function setUp() : void
    {
        parent::setUp();
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->mockAlternatively('App\Channel\MoEngage\MoEngageUserPropertiesApi');

        $this->id = '12321';
        $this->props = ['first_name' => 'lala'];
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testHandle_Success(): void
    {
        $this->mock->shouldReceive('addNewUserProperties')
                ->with($this->id, $this->props)
                ->andReturn(null);

        Queue::fake();
        SendMoEngageUserPropertiesQueue::dispatch($this->id, $this->props);
        Queue::assertPushed(SendMoEngageUserPropertiesQueue::class, 1);
        
        $job = new SendMoEngageUserPropertiesQueue($this->id, $this->props);
        $this->assertNull($job->handle());
    }

    public function testHandle_ExceptionThrown(): void
    {
        $this->mock->shouldReceive('addNewUserProperties')
                ->with($this->id, [])
                ->andReturn(null);

        $this->bugSnag->shouldReceive('notifyException')->once()->with('Property userId and properties cannot be null');
        
        $job = new SendMoEngageUserPropertiesQueue($this->id, []);
        $this->assertNull($job->handle());
    }

    public function testFailed_Success(): void
    {
        $exception = new \Exception('test');

        $this->mock->shouldReceive('addNewUserProperties')
                ->with($this->id, $this->props)
                ->andReturn(null);

        $this->bugSnag->shouldReceive('notifyException')->once()->with('Failed to send tracker: ' . $exception->getMessage());
        
        $job = new SendMoEngageUserPropertiesQueue($this->id, $this->props);
        $this->assertNull($job->failed($exception));
    }
}