<?php

namespace App\Jobs\MoEngage;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\MoEngage\SendMoEngageOwnerGoldPlusStatus;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Queue;
use Mockery;
use phpmock\MockBuilder;

/**
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SendMoEngageOwnerGoldPlusStatusTest extends MamiKosTestCase
{
    protected $id;
    protected $props;
    private $bugSnag, $env;

    protected function setUp() : void
    {
        parent::setUp();
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->mockAlternatively('App\Channel\MoEngage\MoEngageUserPropertiesApi');

        $this->id = '12321';
        $this->props = ['is_goldplus' => mt_rand(0,1)];
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testHandleProduction_Success(): void
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('env')
            ->setFunction(
                function () {
                    return 'production';
                }
            );
        $this->env = $builder->build();
        $this->env->enable();

        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        Queue::fake();
        SendMoEngageOwnerGoldPlusStatus::dispatch($this->id, $this->props);
        Queue::assertPushed(SendMoEngageOwnerGoldPlusStatus::class, 1);
        
        $job = new SendMoEngageOwnerGoldPlusStatus($this->id, $this->props);
        $this->assertNull($job->handle());

        $this->env->disable();
    }

    public function testFailedProduction_Success(): void
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('env')
            ->setFunction(
                function () {
                    return 'production';
                }
            );
        $this->env = $builder->build();
        $this->env->enable();

        $exception = new \Exception('test');

        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $this->bugSnag->shouldReceive('notifyException')->once()->with('Failed to send tracker: ' . $exception->getMessage());
        
        $job = new SendMoEngageOwnerGoldPlusStatus($this->id, $this->props);
        $this->assertNull($job->failed($exception));

        $this->env->disable();
    }

    public function testHandleNonProduction_Success(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        Queue::fake();
        SendMoEngageOwnerGoldPlusStatus::dispatch($this->id, $this->props);
        Queue::assertPushed(SendMoEngageOwnerGoldPlusStatus::class, 1);
        
        $job = new SendMoEngageOwnerGoldPlusStatus($this->id, $this->props);
        $this->assertNull($job->handle());
    }

    public function testFailedNonProduction_Success(): void
    {
        $exception = new \Exception('test');

        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $this->bugSnag->shouldReceive('notifyException')->once()->with('Failed to send tracker: ' . $exception->getMessage());
        
        $job = new SendMoEngageOwnerGoldPlusStatus($this->id, $this->props);
        $this->assertNull($job->failed($exception));
    }

    public function testCheckGoldplus_True()
    {
        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        $rooms = factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $reflector = new \ReflectionClass(SendMoEngageOwnerGoldPlusStatus::class);

        $dummyMethod = $reflector->getMethod('checkGoldplus');
        $dummyMethod->setAccessible(true);

        $dummyProp = $reflector->getProperty('ownerId');
        $dummyProp->setAccessible(true);
        $dummyProp->setValue($reflector, $owner->id);

        $res = $dummyMethod->invoke(new SendMoEngageOwnerGoldPlusStatus($owner->id), $rooms);

        $this->assertTrue($res);
    }

    public function testCheckGoldplus_False()
    {
        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos 1']);
        $rooms = factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $reflector = new \ReflectionClass(SendMoEngageOwnerGoldPlusStatus::class);

        $dummyMethod = $reflector->getMethod('checkGoldplus');
        $dummyMethod->setAccessible(true);

        $dummyProp = $reflector->getProperty('ownerId');
        $dummyProp->setAccessible(true);
        $dummyProp->setValue($reflector, $owner->id);

        $res = $dummyMethod->invoke(new SendMoEngageOwnerGoldPlusStatus($owner->id), $rooms);

        $this->assertFalse($res);
    }

    public function testCheckBBKStatus_True()
    {
        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        $rooms = factory(Room::class, 3)->states('availableBooking')
            ->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $reflector = new \ReflectionClass(SendMoEngageOwnerGoldPlusStatus::class);

        $dummyMethod = $reflector->getMethod('checkBBKStatus');
        $dummyMethod->setAccessible(true);

        $dummyProp = $reflector->getProperty('ownerId');
        $dummyProp->setAccessible(true);
        $dummyProp->setValue($reflector, $owner->id);

        $res = $dummyMethod->invoke(new SendMoEngageOwnerGoldPlusStatus($owner->id), $rooms);

        $this->assertTrue($res);
    }

    public function testCheckBBKStatus_False()
    {
        $owner = factory(User::class)->states('owner')->create();
        $level = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        $rooms = factory(Room::class, 3)->create()
            ->each(function($room) use($level, $owner) {
                factory(KostLevelMap::class)->create(
                    [
                        'kost_id' => $room->song_id,
                        'level_id' => $level->id
                    ]
                );

                factory(RoomOwner::class)->create(
                    [
                        'designer_id' => $room->id,
                        'user_id' => $owner->id
                    ]
                );
            });

        $reflector = new \ReflectionClass(SendMoEngageOwnerGoldPlusStatus::class);

        $dummyMethod = $reflector->getMethod('checkBBKStatus');
        $dummyMethod->setAccessible(true);

        $dummyProp = $reflector->getProperty('ownerId');
        $dummyProp->setAccessible(true);
        $dummyProp->setValue($reflector, $owner->id);

        $res = $dummyMethod->invoke(new SendMoEngageOwnerGoldPlusStatus($owner->id), $rooms);

        $this->assertFalse($res);
    }
}