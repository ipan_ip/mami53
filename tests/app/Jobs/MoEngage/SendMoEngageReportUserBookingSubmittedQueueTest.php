<?php

namespace App\Jobs\MoEngage;

use App\Entities\Booking\BookingUser;
use App\Http\Helpers\BookingUserHelper;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Queue;
use Mockery;

class SendMoEngageReportUserBookingSubmittedQueueTest extends MamiKosTestCase
{
    protected $bookingUser;
    protected $interface;
    private $bugSnag;

    protected function setUp() : void
    {
        parent::setUp();
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');

        $this->bookingUser = factory(BookingUser::class)->make();
        $this->interface = BookingUserHelper::INTERFACE_IOS;
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testJobsSuccess(): void
    {
        $this->mock->shouldReceive('reportUserBookingSubmitted')
            ->with($this->bookingUser, $this->interface)
            ->andReturn(null);

        $interface = $this->interface;
        Queue::fake();
        SendMoEngageReportUserBookingSubmittedQueue::dispatch($this->bookingUser, $this->interface);
        Queue::assertPushed(SendMoEngageReportUserBookingSubmittedQueue::class, function ($job) use ($interface) {
            return $this->getNonPublicAttributeFromClass('interface', $job) === $interface;
        });

        $job = new SendMoEngageReportUserBookingSubmittedQueue($this->bookingUser, $this->interface);
        $this->assertNull($job->handle());
    }

    public function testJobsFailedIsSuccess(): void
    {
        $exception = new \Exception('test');

        $this->mock->shouldReceive('reportUserBookingSubmitted')
            ->with($this->bookingUser, $this->interface)
            ->andReturn(null);

        $this->bugSnag->shouldReceive('notifyException')->once()->with('Failed to send tracker [User] Booking Submitted: ' . $exception->getMessage());

        $job = new SendMoEngageReportUserBookingSubmittedQueue($this->bookingUser, $this->interface);
        $this->assertNull($job->failed($exception));
    }
}