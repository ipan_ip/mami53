<?php

namespace App\Jobs;

use App\Facades\SendBirdFacade;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Queue;

/**
 * Class SendBirdNotificationQueueTest
 * @package App\Jobs
 */
class SendBirdNotificationQueueTest extends MamiKosTestCase
{
    const CHANNEL_URL   = 'https://url';
    const MESSAGE       = 'messaxxa';
    const TENANT_ID     = 1;

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testJobQueueChannelUrlParamValidSuccess()
    {
        $channelUrl = self::CHANNEL_URL;
        Queue::fake();

        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);

        Queue::assertPushed(SendBirdNotificationQueue::class, function ($job) use ($channelUrl) {
            return $this->getNonPublicAttributeFromClass('channelUrl', $job) === $channelUrl;
        });
    }

    public function testJobQueueMessageParamValidSuccess()
    {
        $message = self::MESSAGE;
        Queue::fake();

        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);

        Queue::assertPushed(SendBirdNotificationQueue::class, function ($job) use ($message) {
            return $this->getNonPublicAttributeFromClass('message', $job) === $message;
        });
    }

    public function testJobQueueTenantIdParamValidSuccess()
    {
        $tenantId = self::TENANT_ID;
        Queue::fake();

        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);

        Queue::assertPushed(SendBirdNotificationQueue::class, function ($job) use ($tenantId) {
            return $this->getNonPublicAttributeFromClass('tenantId', $job) === $tenantId;
        });
    }

    public function testJobQueueNotPushedFailed()
    {
        Queue::fake();
        Queue::assertNotPushed(SendBirdNotificationQueue::class);
    }

    public function testJobQueueTwicePushSuccess()
    {
        Queue::fake();

        // first push
        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);

        // second push
        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);

        // check queue push twice ( 2 )
        Queue::assertPushed(SendBirdNotificationQueue::class, 2);
    }

    public function testHandle_Success(): void
    {
        SendBirdFacade::shouldReceive('sendMessageToGroupChannel')->once()->andReturn([]);

        Queue::fake();
        SendBirdNotificationQueue::dispatch(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);
        Queue::assertPushed(SendBirdNotificationQueue::class, 1);
        
        $job = new SendBirdNotificationQueue(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);
        $this->assertNull($job->handle());
    }

    public function testFailed_Success(): void
    {
        $exception = new \Exception('test');

        Bugsnag::shouldReceive('notifyException')->once();
        
        $job = new SendBirdNotificationQueue(self::CHANNEL_URL, self::TENANT_ID, self::MESSAGE);
        $this->assertNull($job->failed($exception));
    }
}