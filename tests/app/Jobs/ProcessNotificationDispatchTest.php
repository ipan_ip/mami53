<?php

namespace App\Jobs;

use App\Entities\Notif\NotificationWhatsappTemplate;
use Mockery;
use App\Test\MamiKosTestCase;

class ProcessNotificationDispatchTest extends MamiKosTestCase
{
    public function testHandle_Success()
    {
        $data = [];
        $waTemplate = Mockery::mock('alias:App\Entities\Notif\NotificationWhatsappTemplate');
        $waTemplate->shouldReceive('send')->once()->with($waTemplate, $data);

        $job = new ProcessNotificationDispatch($waTemplate, $data);
        $this->assertNull($job->handle());
    }

    public function testFailed_Success()
    {
        $data = [];
        $waTemplate = Mockery::mock('alias:App\Entities\Notif\NotificationWhatsappTemplate');

        $job = new ProcessNotificationDispatch($waTemplate, $data);
        $this->assertNull($job->failed(new \Exception()));
    }
}
