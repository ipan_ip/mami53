<?php

namespace App\Jobs\Owner;

use App\Entities\Owner\ActivityType;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class MarkActivityTest extends MamiKosTestCase
{
    public function testActivityDispatchTypeUpdateSuccess()
    {
        $nowStart = Carbon::now();
        Carbon::setTestNow($nowStart);
        $user = factory(User::class)->create();
        $this->assertDatabaseMissing('owner_recent_activity', ['user_id' => $user->id]);
        $job = new MarkActivity($user->id, ActivityType::TYPE_UPDATE_PROPERTY);
        $job->handle();
        $this->assertDatabaseHas('owner_recent_activity', ['user_id' => $user->id, 'updated_property_at' => $nowStart]);
    }

    public function testActivityDispatchTypeLoginSuccess()
    {
        $nowStart = Carbon::now();
        Carbon::setTestNow($nowStart);
        $user = factory(User::class)->create();
        $this->assertDatabaseMissing('owner_recent_activity', ['user_id' => $user->id]);
        $job = new MarkActivity($user->id, ActivityType::TYPE_LOGIN);
        $job->handle();
        $this->assertDatabaseHas('owner_recent_activity', ['user_id' => $user->id, 'logged_in_at' => $nowStart]);
    }

    /**
     * @group PMS-495
     */ 
    public function testTags()
    {
        $nowStart = Carbon::now();
        Carbon::setTestNow($nowStart);
        $user = factory(User::class)->create();
        $this->assertDatabaseMissing('owner_recent_activity', ['user_id' => $user->id]);
        $job = new MarkActivity($user->id, ActivityType::TYPE_LOGIN);

        $this->assertEquals($job->tags(), ['owner', 'activity', 'userId:' . $user->id]);
    }
}
