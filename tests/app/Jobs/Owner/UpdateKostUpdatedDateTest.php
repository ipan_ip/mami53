<?php

namespace App\Jobs\Owner;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class UpdateKostUpdatedDateTest extends MamiKosTestCase
{
    
    public function testRoomDateIsUpdated()
    {
        $lastMonth = Carbon::now()->subMonth();
        $expectedDate = Carbon::now();
        Carbon::setTestNow($expectedDate);
        $user = factory(User::class)->create();
        $rooms = factory(Room::class, 4)->state('active')->create(['kost_updated_date' => $lastMonth]);
        $rooms->each(function ($room) use ($user) {
            factory(RoomOwner::class)->state('owner')->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
            ]);
        });

        $job = new UpdateKostUpdatedDate($user->id);
        $job->handle();
        RoomOwner::with('room')->where('user_id', $user->id)->get()->each(function ($roomOwner) use ($expectedDate){
            $this->assertEquals($expectedDate, $roomOwner->room->kost_updated_date);
        });

    }
}
