<?php

namespace App\Jobs\Owner;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Services\Thanos\ThanosService;
use App\Test\MamiKosTestCase;
use App\User;

class OwnerRevertThanosTest extends MamiKosTestCase
{
    
    public function testRevertThanos()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => 'false',
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        factory(ThanosHidden::class)->create([
            'designer_id' => $room->id,
            'snapped' => true
        ]);

        $service = new ThanosService();
        $job = new OwnerRevertThanos($room->id);
        $job->handle($service);

        $updatedRoom = Room::find($room->id);
        $this->assertEquals('true', $updatedRoom->is_active);
    }

    /**
     * @group PMS-495
     */ 
    public function testTags()
    {
        $room = factory(Room::class)->create();

        $job = new OwnerRevertThanos($room->id);
        $this->assertEquals($job->tags(), ['owner', 'thanos', 'kostId:' . $room->id]);
    }
}
