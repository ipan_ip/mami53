<?php

namespace App\Jobs\Promoted;

use App\Entities\Promoted\AdsDisplayTracker;
use App\Entities\Room\Room;
use App\User;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class AdsDisplayTrackerQueueTest extends MamiKosTestCase
{
    public function setUp(): void 
    {
        parent::setUp();
        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        // Reset test carbon mock
        Carbon::setTestNow();
    }

    public function testAdsDisplayTrackerSuccess()
    {
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $user = factory(User::class)->create();

        app()->device = null;
        app()->user = $user;

        $this->assertDatabaseMissing('designer_ads_display_tracker', ['designer_id' => $room1->song_id]);
        $this->assertDatabaseMissing('designer_ads_display_tracker', ['designer_id' => $room2->song_id]);

        $job = new AdsDisplayTrackerQueue([$room1->song_id, $room2->song_id], null);
        $job->handle();

        $this->assertDatabaseHas('designer_ads_display_tracker', ['designer_id' => $room1->song_id]);
        $this->assertDatabaseHas('designer_ads_display_tracker', ['designer_id' => $room2->song_id]);
    }
}