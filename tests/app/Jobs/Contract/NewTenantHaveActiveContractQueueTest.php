<?php

namespace App\Jobs\Contract;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Queue;

/**
 * Class NewTenantHaveActiveContractQueueTest
 * @package App\Jobs\Contract
 */
class NewTenantHaveActiveContractQueueTest extends MamiKosTestCase
{
    public function testJobsFailedArgumentError(): void
    {
        // expect error argument not exist
        $this->expectException(\ArgumentCountError::class);

        // run test
        NewTenantHaveActiveContractQueue::dispatch();
    }

    public function testJobsFailedWrongArgumentError(): void
    {
        // expect error argument not valid
        $this->expectException(\TypeError::class);

        // run test
        NewTenantHaveActiveContractQueue::dispatch(null);
    }

    public function testJobsSuccess(): void
    {
        // prepare data
        $userId = 1;

        // declare fake queue
        Queue::fake();

        // dispatch jobs
        NewTenantHaveActiveContractQueue::dispatch($userId);

        // run test
        Queue::assertPushed(NewTenantHaveActiveContractQueue::class, function ($job) use ($userId) {
            return $this->getNonPublicAttributeFromClass('userId', $job) === $userId;
        });
    }
}