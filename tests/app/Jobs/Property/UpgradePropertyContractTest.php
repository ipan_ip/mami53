<?php

namespace App\Jobs\Property;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\PropertyLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\Property\PropertyContractService;
use App\Test\MamiKosTestCase;
use App\User;

class UpgradePropertyContractTest extends MamiKosTestCase
{
    protected $kostLevelRepository;
    protected $roomUnitRepository;
    protected $propertyContractService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->kostLevelRepository = $this->mockAlternatively(KostLevelRepository::class);
        $this->roomUnitRepository = $this->mockAlternatively(RoomUnitRepository::class);
        $this->propertyContractService = $this->mockAlternatively(PropertyContractService::class);
    }

    protected function createGoldplusLevel()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        return [
            'property' => $propertyLevel,
            'kost' => $kostLevel,
            'room' => $roomLevel
        ];
    }

    public function testHandle()
    {
        $levels = $this->createGoldplusLevel();
        $property = factory(Property::class)->create();
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $levels['property']->id
        ]);
        factory(PropertyContractDetail::class)->create([
            'property_contract_id' => $contract->id,
            'property_id' => $property->id
        ]);
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $kost->song_id,
            'level_id' => $levels['kost']->id
        ]);
        $room = factory(RoomUnit::class)->create([
            'designer_id' => $kost->id,
            'room_level_id' => $levels['room']->id
        ]);
        $admin = factory(User::class)->create();

        $this->kostLevelRepository
            ->shouldReceive('changeLevel')
            ->withArgs(function ($argKost, $argKostLevel, $argUser, $argIsForce) use ($kost, $levels, $admin) {
                return $argKost->is($kost) && $argKostLevel->is($levels['kost']) && ($argIsForce === true) && $argUser->is($admin);
            })
            ->once();

        $this->roomUnitRepository
            ->shouldReceive('assignNewLevelToAllUnits')
            ->withArgs(function ($argKostId, $argRoomLevel, $argAdmin) use ($kost, $levels, $admin) {
                return ($argKostId === $kost->id) && ($argRoomLevel->is($levels['room'])) && $argAdmin->is($admin);
            })
            ->once();

        $this->propertyContractService
            ->shouldReceive('calculateTotalRoomAndPackage')
            ->withArgs(function ($argContract) use ($contract) {
                return ($argContract->is($contract));
            })
            ->once();

        UpgradePropertyContract::dispatch($contract, $admin);
    }
}
