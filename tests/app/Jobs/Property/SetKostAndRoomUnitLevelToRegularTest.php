<?php

namespace App\Jobs\Property;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\PropertyLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Level\KostLevelRepository;
use App\Test\MamiKosTestCase;
use App\User;

class SetKostAndRoomUnitLevelToRegularTest extends MamiKosTestCase
{
    protected $kostLevelRepo;

    protected function setUp()
    {
        parent::setUp();

        $this->kostLevelRepo = $this->mockAlternatively(KostLevelRepository::class);
    }

    private function createRegularLevels(): array
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['is_regular' => true, 'property_level_id' => $propertyLevel->id]);
        $roomLevel = factory(RoomLevel::class)->create(['is_regular' => true, 'kost_level_id' => $kostLevel->id]);

        return [
            'property' => $propertyLevel,
            'kost' => $kostLevel,
            'room' => $roomLevel
        ];
    }

    private function createLevels(): array
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        return [
            'property' => $propertyLevel,
            'kost' => $kostLevel,
            'room' => $roomLevel
        ];
    }

    private function assertRepoCalled($kost, $regularLevels, $admin): void
    {
        $this->kostLevelRepo
            ->shouldReceive('changeLevel')
            ->withArgs(function ($argRoom, $argLevel, $argUser, $bool) use ($kost, $regularLevels, $admin) {
                return ($argRoom->is($kost)) && ($argUser->is($admin)) && ($argLevel->is($regularLevels['kost'])) && ($bool === true);
            })
            ->once();

        $this->kostLevelRepo
            ->shouldReceive('with')
            ->with('room_level')
            ->andReturn($this->kostLevelRepo);

        $this->kostLevelRepo
            ->shouldReceive('where')
            ->with('is_regular', true)
            ->andReturn($this->kostLevelRepo);

        $kostLevelToReturn = $regularLevels['kost'];
        $kostLevelToReturn->load('room_level');
        $this->kostLevelRepo
            ->shouldReceive('first')
            ->andReturn($kostLevelToReturn);
    }

    public function testHandleShouldSetKostLevelToRegular(): void
    {
        $regularLevels = $this->createRegularLevels();
        $levels = $this->createLevels();

        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $levels['property']->id
        ]);
        $property = factory(Property::class)->create();
        factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $propertyContract->id
        ]);

        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $map = factory(KostLevelMap::class)->create(['kost_id' => $kost->song_id, 'level_id' => $levels['kost']]);

        $admin = factory(User::class)->create();

        $this->assertRepoCalled($kost, $regularLevels, $admin);

        SetKostAndRoomUnitLevelToRegular::dispatch($propertyContract, $admin);
    }

    public function testHandleShouldSetRoomUnitLevelToRegular(): void
    {
        $regularLevels = $this->createRegularLevels();
        $levels = $this->createLevels();

        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $levels['property']->id
        ]);
        $property = factory(Property::class)->create();
        factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $propertyContract->id
        ]);

        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $roomUnit = factory(RoomUnit::class)->create(['room_level_id' => $levels['room'], 'designer_id' => $kost->id]);
        $map = factory(KostLevelMap::class)->create(['kost_id' => $kost->song_id, 'level_id' => $levels['kost']]);

        $admin = factory(User::class)->create();

        $this->assertRepoCalled($kost, $regularLevels, $admin);

        SetKostAndRoomUnitLevelToRegular::dispatch($propertyContract, $admin);

        $this->assertDatabaseHas('designer_room', ['id' => $roomUnit->id, 'room_level_id' => $regularLevels['room']->id]);
    }

    public function testHandleShouldNotSetOtherRoomUnitLevelToRegular(): void
    {
        $regularLevels = $this->createRegularLevels();
        $levels = $this->createLevels();

        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => $levels['property']->id
        ]);
        $property = factory(Property::class)->create();
        factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $propertyContract->id
        ]);

        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $roomUnit = factory(RoomUnit::class)->create(['room_level_id' => $levels['room'], 'designer_id' => factory(Room::class)->create()]);
        $map = factory(KostLevelMap::class)->create(['kost_id' => $kost->song_id, 'level_id' => $levels['kost']]);

        $admin = factory(User::class)->create();

        $this->assertRepoCalled($kost, $regularLevels, $admin);

        SetKostAndRoomUnitLevelToRegular::dispatch($propertyContract, $admin);

        $this->assertDatabaseMissing('designer_room', ['id' => $roomUnit->id, 'room_level_id' => $regularLevels['room']->id]);
    }
}
