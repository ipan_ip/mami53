<?php

namespace app\Jobs\Booking;

use App\Entities\Booking\BookingAcceptanceRate;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Repositories\Booking\BookingStatusChangeRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Queue;
use App\Jobs\Booking\BookingAcceptanceRateCalculation;


class BookingAcceptanceRateCalculationTest extends MamiKosTestCase
{
    public function testJobsSuccess(): void
    {
        Queue::fake();
        BookingAcceptanceRateCalculation::dispatch();
        Queue::assertPushed(BookingAcceptanceRateCalculation::class);
    }

    public function testJobsCalculation()
    {
        self::markTestSkipped('This flaky test will be fixed later');

        $room = factory(Room::class)
            ->states(
                [
                    'availableBooking',
                    'active'
                ]
            )
            ->create();

        $trx1 = factory(BookingUser::class)->create(
            [
                'designer_id' => $room->song_id,
                'status' => BookingUser::BOOKING_STATUS_CONFIRMED
            ]
        );

        $trx2 = factory(BookingUser::class)->create(
            [
                'designer_id' => $room->song_id,
                'status' => BookingUser::BOOKING_STATUS_EXPIRED
            ]
        );

        $statusChangeRepo = $this->app->make(BookingStatusChangeRepositoryEloquent::class);

        $this->cleanUpData();

        // added historical data
        $statusChangeRepo->addChange($trx1, $trx1->status, null, null);
        $statusChangeRepo->addChange($trx2, $trx2->status, null, null);

        $job = new BookingAcceptanceRateCalculation();
        $this->assertNull($job->handle());

        $this->assertDatabaseHas(
            'booking_acceptance_rate',
            [
                'designer_id' => $room->id,
                'rate' => 100,
            ]
        );
    }

    private function cleanUpData()
    {
        $query = BookingAcceptanceRate::query();
        if ($query->count()) {
            $query->delete();
        }
    }
}