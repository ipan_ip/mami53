<?php

namespace App\Jobs\Booking;

use App\Entities\Activity\Call;
use App\Entities\Activity\Question;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Queue;

/**
 * Class BookingSendBirdAutoChatQueueTest
 * @package App\Jobs\Booking
 */
class BookingSendBirdAutoChatQueueTest extends MamiKosTestCase
{
    private function setupParams()
    {
        return [
            'booking'       => factory(BookingUser::class)->make(['id' => 1]),
            'user'          => factory(User::class)->make(['id' => 1]),
            'room'          => factory(Room::class)->make(['id' => 1]),
            'channel_url'   => 'https://url'
        ];
    }

    public function testJobQueueBookingParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        Queue::assertPushed(BookingSendBirdAutoChatQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('booking', $job)->id === $params['booking']->id;
        });
    }

    public function testJobQueueUserParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        Queue::assertPushed(BookingSendBirdAutoChatQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('user', $job)->id === $params['user']->id;
        });
    }

    public function testJobQueueRoomParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        Queue::assertPushed(BookingSendBirdAutoChatQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('room', $job)->id === $params['room']->id;
        });
    }

    public function testJobQueueChannelUrlParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        Queue::assertPushed(BookingSendBirdAutoChatQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('channelUrl', $job) === $params['channel_url'];
        });
    }

    public function testJobQueueNotPushedFailed()
    {
        Queue::fake();
        Queue::assertNotPushed(BookingSendBirdAutoChatQueue::class);
    }

    public function testJobQueueTwicePushSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        // first push
        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        // second push
        BookingSendBirdAutoChatQueue::dispatch($params['booking'], $params['user'], $params['room'], $params['channel_url']);

        // check queue push twice ( 2 )
        Queue::assertPushed(BookingSendBirdAutoChatQueue::class, 2);
    }
}