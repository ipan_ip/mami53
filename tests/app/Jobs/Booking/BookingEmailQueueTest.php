<?php

namespace app\Jobs\Booking;

use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;
use App\Jobs\Booking\BookingEmailQueue;

class BookingEmailQueueTest extends MamiKosTestCase
{
    const SUBJECT = 'You have a new booking';
    const TEMPLATE_PATH = 'emails.booking.owner.submit-booking';

    public function testJobsSuccess(): void
    {
        $user = factory(User::class)->create();
        $template = [
            'owner_name' => $user->name,
            'tenant_name' => 'Maudy Ayunda',
            'room_name' => 'Kos Pengharapan',
            'duration' => '2 Bulan',
            'checkin_date' => Carbon::now()->addMonth(2)->format('Y-m-d'),
            'subject' => self::SUBJECT,
            'url' => config('app.url').'/ownerpage/manage/all/booking'
        ];

        Queue::fake();
        BookingEmailQueue::dispatch($user, self::SUBJECT, $template, self::TEMPLATE_PATH);
        Queue::assertPushed(BookingEmailQueue::class);
    }
}