<?php

namespace App\Jobs;

use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Queue;

/**
 * Class SendPushNotificationQueueTest
 * @package App\Jobs
 */
class SendPushNotificationQueueTest extends MamiKosTestCase
{
    private function setupParams()
    {
        return [
            'template' => [
                'title'     => 'titxxt',
                'message'   => 'messxxage',
                'schema'    => 'https://url'
            ],
            'user_id' => 1,
            'is_mamipay' => false
        ];
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Entities\Notif\SendNotif');
    }

    public function testJobQueueTemplateParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        SendPushNotificationQueue::dispatch($params['template'], $params['user_id'], $params['is_mamipay']);

        Queue::assertPushed(SendPushNotificationQueue::class, function ($job) {
            return is_array($this->getNonPublicAttributeFromClass('template', $job));
        });
    }

    public function testJobQueueMessageParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        SendPushNotificationQueue::dispatch($params['template'], $params['user_id'], $params['is_mamipay']);

        Queue::assertPushed(SendPushNotificationQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('userId', $job) === $params['user_id'];
        });
    }

    public function testJobQueueIsMamipayParamValidSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        SendPushNotificationQueue::dispatch($params['template'], $params['user_id'], $params['is_mamipay']);

        Queue::assertPushed(SendPushNotificationQueue::class, function ($job) use ($params) {
            return $this->getNonPublicAttributeFromClass('mamiPayUser', $job) === $params['is_mamipay'];
        });
    }

    public function testJobQueueNotPushedFailed()
    {
        Queue::fake();
        Queue::assertNotPushed(SendPushNotificationQueue::class);
    }

    public function testJobQueueNotPushedFailed_BugsnagCalled()
    {
        $params = $this->setupParams();
        $exception = new \Exception('test');

        $this->mock->shouldReceive('setUserIds')
                ->andReturn(null);
        $this->mock->shouldReceive('send')
                ->andReturn(false);

        Bugsnag::shouldReceive('notifyException')->once();
        
        $job = new SendPushNotificationQueue($params['template'], $params['user_id'], $params['is_mamipay']);
        $this->assertNull($job->failed($exception));
    }

    public function testJobQueueTwicePushSuccess()
    {
        $params = $this->setupParams();
        Queue::fake();

        // first push
        SendPushNotificationQueue::dispatch($params['template'], $params['user_id'], $params['is_mamipay']);

        // second push
        SendPushNotificationQueue::dispatch($params['template'], $params['user_id'], $params['is_mamipay']);

        // check queue push twice ( 2 )
        Queue::assertPushed(SendPushNotificationQueue::class, 2);
    }
}