<?php

namespace App\Jobs;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class RecalculateRoomSortScoreTest extends MamiKosTestCase
{
    public function testHandle()
    {
        Queue::fake();

        $rows = 10;
        $rooms = factory(Room::class, $rows)->create();

        foreach ($rooms as $room) {
            RecalculateRoomSortScore::dispatch($room);
        }

        Queue::assertPushed(
            RecalculateRoomSortScore::class,
            $rows
        );
    }

    public function testHandle_TrueDryRun()
    {
        Queue::fake();

        $rows = 10;
        $rooms = factory(Room::class, $rows)->create();

        foreach ($rooms as $room) {
            RecalculateRoomSortScore::dispatch($room, true);
        }

        Queue::assertPushed(
            RecalculateRoomSortScore::class,
            $rows
        );
    }

    public function testFailed_Success(): void
    {
        $exception = new \Exception('test');

        Log::shouldReceive('channel->error')->once();

        Bugsnag::shouldReceive('leaveBreadcrumb')->once();
        Bugsnag::shouldReceive('notifyException')->once();

        $room = factory(Room::class)->create();
        $job = new RecalculateRoomSortScore($room, true);
        $this->assertNull($job->failed($exception));
    }
}
