<?php

namespace App\Jobs\Room;

use App\Entities\Room\Element\Verification;
use App\Entities\Room\Room;
use App\Services\Owner\Kost\InputService;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Bugsnag\Client;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;

class MarkReadyToVerifyTest extends MamiKosTestCase
{
    /**
    * @group PMS-495
    */
    public function testJobRoomActiveSkipJob()
    {
        $room = factory(Room::class)->states(['active', 'attribute-complete-state'])->create();
        factory(Verification::class)->create([
            'designer_id' => $room->id,
            'is_ready' => true,
            'updated_at' => '2019-12-01 08:08:08',
            'created_at' => '2019-12-01 08:08:08'
        ]);

        Carbon::setTestNow('2021-01-01 08:08:08');
        $job = new MarkReadyToVerify($room->id);
        $job->handle(app()->make(InputService::class));

        $this->assertDatabaseMissing(
            (new Verification())->getTable(),
            ['designer_id' => $room->id, 'updated_at' => '2021-01-01 08:08:08']
        );
    }

    /**
    * @group PMS-495
    */
    public function testJobRoomInactiveSuccess()
    {
        $room = factory(Room::class)->states(['inactive', 'attribute-complete-state'])->create();
        $job = new MarkReadyToVerify($room->id);
        $job->handle(app()->make(InputService::class));

        $this->assertDatabaseHas((new Verification())->getTable(), ['designer_id' => $room->id, 'is_ready' => true]);
    }

    /**
    * @group PMS-495
    */
    public function testJobHasMarkReadySkipJob()
    {
        $room = factory(Room::class)->states(['inactive', 'attribute-complete-state'])->create();
        factory(Verification::class)->create([
            'designer_id' => $room->id,
            'is_ready' => true,
            'updated_at' => '2019-12-01 08:08:08',
            'created_at' => '2019-12-01 08:08:08'
        ]);

        Carbon::setTestNow('2021-01-01 08:08:08');
        $job = new MarkReadyToVerify($room->id);
        $job->handle(app()->make(InputService::class));

        $this->assertDatabaseHas(
            (new Verification())->getTable(),
            ['designer_id' => $room->id, 'updated_at' => '2019-12-01 08:08:08']
        );
    }

    /**
    * @group PMS-495
    */
    public function testJobHasMarkNotReadyUpdateJob()
    {
        $room = factory(Room::class)->states(['inactive', 'attribute-complete-state'])->create();
        factory(Verification::class)->create([
            'designer_id' => $room->id,
            'is_ready' => false,
            'updated_at' => '2019-12-01 08:08:08',
            'created_at' => '2019-12-01 08:08:08'
        ]);

        Carbon::setTestNow('2021-01-01 08:08:08');
        $job = new MarkReadyToVerify($room->id);
        $job->handle(app()->make(InputService::class));

        $this->assertDatabaseHas(
            (new Verification())->getTable(),
            ['designer_id' => $room->id, 'updated_at' => '2021-01-01 08:08:08']
        );
    }
}
