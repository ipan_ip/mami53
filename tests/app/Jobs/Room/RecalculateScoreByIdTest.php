<?php

namespace app\Jobs\Booking;

use App\Entities\Room\Room;
use App\Jobs\Room\RecalculateScoreById;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use LplCriteriaSeeder;

class RecalculateScoreByIdTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->seed(LplCriteriaSeeder::class);
    }

    protected function tearDown(): void
    {
        DB::table('lpl_criteria')->truncate();
        parent::tearDown();
    }

    public function testJobsSuccess(): void
    {
        $room = factory(Room::class)->states([
            'active',
            'mamirooms',
            'availableBooking'
        ])->create(['sort_score' => 128]);

        Queue::fake();
        RecalculateScoreById::dispatch($room->id);
        Queue::assertPushed(RecalculateScoreById::class);

        $job = new RecalculateScoreById($room->id);
        $this->assertNull($job->handle());

        $this->assertEquals(2696, Room::where('id', $room->id)->first()->sort_score);
    }

}