<?php

namespace app\Jobs\Booking;

use App\Entities\Room\Room;
use App\Jobs\Room\RecalculateScoreBatcher;
use App\Jobs\Room\RecalculateScoreById;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Queue;

class RecalculateScoreBatcherTest extends MamiKosTestCase
{

    public function testJobsSuccess(): void
    {
        $rooms = factory(Room::class, 2)->create();

        Queue::fake();
        RecalculateScoreBatcher::dispatch($rooms);
        Queue::assertPushed(RecalculateScoreBatcher::class);
    }

}