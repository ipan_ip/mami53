<?php

namespace App\Jobs\Room;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Room\Geolocation;
use App\Entities\Room\Room;
use App\Http\Helpers\GeolocationMappingHelper;
use App\Test\MamiKosTestCase;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;

class GeolocationMapperTest extends MamiKosTestCase
{
    public function testHandle()
    {
        // Mock Room model
        $room = factory(Room::class)->create();

        // Mock helper
        $mockedHelper = $this->getMockBuilder(GeolocationMappingHelper::class)
            ->setMethods(
                [
                    'syncGeocode'
                ]
            )
            ->getMock();

        $mockedHelper
            ->expects($this->any())
            ->method('syncGeocode')
            ->with(
                [
                    $room,
                    false
                ]
            );

        // Execute Job class as "command"
        $geolocationMapper = new GeolocationMapper($room, false);
        $geolocationMapper->handle();
    }
}
