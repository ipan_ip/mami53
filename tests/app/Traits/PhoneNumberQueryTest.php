<?php

namespace App\Traits;

use App\Test\MamiKosTestCase;
use App\Traits\PhoneNumberQuery;

class PhoneNumberQueryTest extends MamiKosTestCase
{
    use PhoneNumberQuery;

    /**
     * @group UG
     * @group UG-1340
     * @group App/Traits/PhoneNumberQuery
     */
    public function testCleanPhoneNumber0Prefix()
    {
        $phoneNumber = '8111'.mt_rand(111111, 999999);
        $this->assertEquals($phoneNumber, $this->cleanPhoneNumber('0' . $phoneNumber));
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Traits/PhoneNumberQuery
     */
    public function testCleanPhoneNumber62Prefix()
    {
        $phoneNumber = '8111'.mt_rand(111111, 999999);
        $this->assertEquals($phoneNumber, $this->cleanPhoneNumber('62' . $phoneNumber));
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Traits/PhoneNumberQuery
     */
    public function testCleanPhoneNumberPlus62Prefix()
    {
        $phoneNumber = '8111'.mt_rand(111111, 999999);
        $this->assertEquals($phoneNumber, $this->cleanPhoneNumber('+62' . $phoneNumber));
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Traits/PhoneNumberQuery
     */
    public function testCreatePrefixedNumbers()
    {
        $phoneNumber = '8111'.mt_rand(111111, 999999);
        $this->assertEquals(['0' . $phoneNumber, '62' . $phoneNumber, '+62' . $phoneNumber], $this->createPrefixedNumbers($phoneNumber));
    }
}
