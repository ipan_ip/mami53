<?php

namespace App\Transformers\Cms;

use App\Test\MamiKosTestCase;
use App\Transformers\Cms\RoomTransformer;

class RoomTransformerTest extends MamiKosTestCase
{
        
    /** @test */
    public function room_collection_can_be_transform_without_area()
    {
        $kost = factory(\App\Entities\Room\Room::class)->make(["song_id" => 12]);

        $transformedRoom = (new RoomTransformer)->transform($kost);
        
        $this->assertEquals($kost->song_id, $transformedRoom['kost_id']);
        $this->assertEquals($kost->name, $transformedRoom['kost_name']);
        $this->assertEquals($kost->area, $transformedRoom['area_name']);
        $this->assertEquals($kost->status, $transformedRoom['status']);
        $this->assertEquals($kost->area_city, $transformedRoom['area_city']);
        $this->assertEquals($kost->area_subdistrict, $transformedRoom['area_subdistrict']);
        $this->assertEquals($kost->area_big, $transformedRoom['area_big']);
        $this->assertEquals($kost->room_available, $transformedRoom['room']['available']);
        $this->assertEquals($kost->room_count, $transformedRoom['room']['total']);
        $this->assertEquals(($kost->room_count - $kost->room_available), $transformedRoom['room']['occupied']);
        $this->assertEquals(0, $transformedRoom['area_id']);
    }

    /** @test */
    public function room_collection_can_be_transform_with_area()
    {
        $area = factory(\App\Entities\Area\Area::class)->create(["name" => "surabaya"]);
        $kost = factory(\App\Entities\Room\Room::class)->make(["song_id" => 12, "area_city" => "surabaya"]);

        $transformedRoom = (new RoomTransformer)->transform($kost);

        $this->assertEquals($kost->song_id, $transformedRoom['kost_id']);
        $this->assertEquals($kost->name, $transformedRoom['kost_name']);
        $this->assertEquals($kost->area, $transformedRoom['area_name']);
        $this->assertEquals($kost->status, $transformedRoom['status']);
        $this->assertEquals($kost->area_city, $transformedRoom['area_city']);
        $this->assertEquals($kost->area_subdistrict, $transformedRoom['area_subdistrict']);
        $this->assertEquals($kost->area_big, $transformedRoom['area_big']);
        $this->assertEquals($kost->room_available, $transformedRoom['room']['available']);
        $this->assertEquals($kost->room_count, $transformedRoom['room']['total']);
        $this->assertEquals(($kost->room_count - $kost->room_available), $transformedRoom['room']['occupied']);
        $this->assertNotEquals(0, $transformedRoom['area_id']);
        $this->assertEquals($area->id, $transformedRoom['area_id']);
    }
}