<?php

namespace App\Transformers\Cms;

use App\Test\MamiKosTestCase;

class ContractTransformerTest extends MamiKosTestCase
{
    
    public function testMamipayContractCollectionCanBeTransform()
    {
        $mamipayContract = factory(\App\Entities\Mamipay\MamipayContract::class)->make();

        // prepare $mamipayContract->user
        $owner_profile = factory(\App\Entities\Mamipay\MamipayOwner::class)->make();
        $owner_profile->user = factory(\App\User::class)->make();
        $mamipayContract->owner_profile = $owner_profile;

        // prepare $mamipayContract->tenant
        $tenant = factory(\App\Entities\Mamipay\MamipayTenant::class)->make(['user_id' => null]);
        $mamipayContract->tenant = $tenant;

        // prepare $mamipayContract->kost
        $kost = factory(\App\Entities\Mamipay\MamipayContractKost::class)->make();
        $room = factory(\App\Entities\Room\Room::class)->make(); 

        $kost->room = $room;
        $mamipayContract->kost = $kost;
        
        // run the tested function
        $transformedContract = (new ContractTransformer)->transform($mamipayContract);

        $this->assertEquals($owner_profile->user->email, $transformedContract['owner']['email']);
        $this->assertEquals($owner_profile->user->name, $transformedContract['owner']['name']);
        $this->assertEquals($owner_profile->user->phone_number, $transformedContract['owner']['phone_number']);

        $this->assertEquals($tenant->email, $transformedContract['tenant']['email']);
        $this->assertEquals($tenant->name, $transformedContract['tenant']['name']);
        $this->assertEquals($tenant->phone_number, $transformedContract['tenant']['phone_number']);
        
        $this->assertEquals($room-> song_id, $transformedContract['kost']['kost_id']);
        $this->assertEquals($room->name, $transformedContract['kost']['kost_name']);
        $this->assertEquals($room->area, $transformedContract['kost']['area_name']);
        $this->assertEquals($room->status, $transformedContract['kost']['status']);

        $this->assertEquals($mamipayContract->start_date, $transformedContract['start_date']);
        $this->assertEquals($mamipayContract->end_date, $transformedContract['end_date']);
        $this->assertEquals($mamipayContract->status, $transformedContract['status']); 

        $userTenant = factory(\App\User::class)->create();
        $tenant = factory(\App\Entities\Mamipay\MamipayTenant::class)->create(['user_id' => $userTenant->id]);
        $mamipayContract->tenant = $tenant;
        $transformedContract = (new ContractTransformer)->transform($mamipayContract);

        $this->assertEquals($tenant->user_id, $transformedContract['tenant']['id']);
    }

}