<?php

namespace App\Transformers\Cms;

use App\Entities\ComplaintSystem\RoleType;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;

class UserTransformerTest extends MamiKosTestCase
{
    
    public function testUserCollectionCanBeTransform()
    {
        $user = factory(\App\User::class)->state('owner')->make();

        $transformedUser = (new UserTransformer)->transform($user);

        $this->assertEquals($user->id, $transformedUser['id']);
        $this->assertEquals($user->email, $transformedUser['email']);
        $this->assertEquals($user->phone_number, $transformedUser['phone_number']);
        $this->assertEquals($user->name, $transformedUser['name']);
        $this->assertEquals($user->isVerified(), $transformedUser['status']);
        $this->assertEquals(RoleType::TYPE_OWNER, $transformedUser['role']);

        $user->is_owner = 'false';
        $transformedUser = (new UserTransformer)->transform($user);
        $this->assertEquals(RoleType::TYPE_TENANT, $transformedUser['role']);

        $user->role = UserRole::Administrator;
        $transformedUser = (new UserTransformer)->transform($user);
        $expectedRole = UserTransformer::ROLE_MAP[$user->role] ?? RoleType::TYPE_TENANT;
        $this->assertEquals($expectedRole, $transformedUser['role']);
    }
}
