<?php

namespace App\Transformers;

use App\Entities\Room\Review;
use App\Test\MamiKosTestCase;

class ReviewTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $transformer = new ReviewTransformer();
        $review = factory(Review::class)->create();

        $actual = $transformer->transform($review);
        $expected = [
            'id' => $review->id,
            'created_at' => $review->created_at,
            'updated_at' => $review->updated_at
        ];

        $this->assertEquals($expected, $actual);
    }
}
