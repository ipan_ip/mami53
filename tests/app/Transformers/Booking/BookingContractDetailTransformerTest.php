<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingContractDetailTransformer;

class BookingContractDetailTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingContractDetailTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingContractDetailCanBeTransform(): void
    {
        // prepare data
        $tenantEntity = factory(MamipayTenant::class)->create();
        $contractEntity = factory(MamipayContract::class)->create(['tenant_id' => $tenantEntity->id]);
        $roomEntity = factory(Room::class)->create();
        factory(MamipayInvoice::class)->create(['contract_id' => $contractEntity->id]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contractEntity->id,
            'designer_id' => $roomEntity->id
        ]);

        // run test
        $transform = $this->transform->transform($contractEntity);
        $assertHasKey = [
            '_id',
            'name',
            'badroom',
            'address',
            'city',
            'price_title',
            'price_title_format',
            'available_room',
            'photo_url',
            'status_kost',
            'facilities',
            'status',
            'duration',
            'duration_string',
            'start_date',
            'end_date',
            'gender',
            'type',
            'tenant_name',
            'tenant_phone_number',
            'tenant_email',
            'tenant_va_number_bni',
            'tenant_va_number_hana',
            'invoice'
        ];
        foreach ($assertHasKey as $item) $this->arrayHasKey($item, $transform);
    }

    public function testRentTypeRequestReturnDefaultValue(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            BookingContractDetailTransformer::class,
            'rentTypeRequest'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            []
        );
        $this->assertSame('2', $result);
    }
}