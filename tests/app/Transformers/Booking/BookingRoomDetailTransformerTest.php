<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingRoomDetailTransformer;
use App\User;
use Carbon\Carbon;

class BookingRoomDetailTransformerTest extends MamiKosTestCase
{
    
    private const PRICE_FORMATED = [
        'price_daily' => 'daily',
        'price_weekly' => 'weekly',
        'price_monthly' => 'monthly',
        'price_yearly' => 'yearly',
        'price_quarterly' => 'quarterly',
        'price_semiannually' => 'semiannually'
    ];

    public function testBookingRoomDetailCanBeTransformAllFieldExist()
    {
        // create room data
        $room = factory(Room::class)->create();

        // main test function
        $transform = (new BookingRoomDetailTransformer())->transform($room);

        // test case
        foreach($this->expectedBookingRoomDetailStructure() as $value){
            $this->assertArrayHasKey($value, $transform);
        }

        // type data
        $this->assertIsInt($transform['id']);
        $this->assertIsBool($transform['is_flash_sale']);
        $this->assertIsBool($transform['is_booking_with_calendar']);
    }

    public function testBookingRoomDetailCanBeTransform()
    {
        // create room data
        $room = factory(Room::class)->create([
            'price_daily' => 200000,
            'price_weekly' => 600000,
            'price_monthly' => 1500000,
            'gender' => 1
        ]);
        // create booking designer data
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id
        ]);

        $priceFormats = $this->generatePriceFormat($room);
        $rentCountFormats = $this->generateRentCountFormat($room);

        // getting price data
        $priceTitle         = $room->price() ? $room->price()->priceTitle(Price::strRentType(2)) : null;
        $priceTitleFormat   = $room->price() ? $room->price()->priceTitleFormats(2, true) : null;

        // main test function
        $transform = (new BookingRoomDetailTransformer())->transform($room);

        $this->assertEquals($room->name, $transform['name']);
        $this->assertEquals($room->slug, $transform['slug']);
        $this->assertEquals($room->gender, $transform['gender']);
        $this->assertEquals($room->available_room, $transform['available_room']);
        $this->assertFalse($transform['isMarriedCouple']);

        $this->assertEquals($priceTitle, $transform['price_title']);
        $this->assertEquals($priceTitleFormat['currency_symbol'], $transform['price_title_format']['currency_symbol']);
        $this->assertEquals($priceTitleFormat['price'], $transform['price_title_format']['price']);
        $this->assertEquals($priceTitleFormat['rent_type_unit'], $transform['price_title_format']['rent_type_unit']);

        $this->assertIsArray($transform['booking_designer']);
        if ($transform['booking_designer'] !== null) {
            foreach ($transform['booking_designer'] as $item) {
                $this->assertEquals($bookingDesigner->id, $item['id']);
                $this->assertEquals($bookingDesigner->name, $item['name']);
            }
        }

        foreach ($priceFormats as $formatKey => $priceFormat) {
            if ($priceFormat !== null) {
                $this->assertEquals($priceFormat['currency_symbol'], $transform['price_title_formats'][$formatKey]['currency_symbol']);
                $this->assertEquals($priceFormat['price'], $transform['price_title_formats'][$formatKey]['price']);
                $this->assertEquals($priceFormat['rent_type_unit'], $transform['price_title_formats'][$formatKey]['rent_type_unit']);
                $this->assertEquals($priceFormat['format'], $transform['price_title_formats'][$formatKey]['format']);
            }
        }

        foreach ($rentCountFormats as $formatKey => $rentCountFormat) {
            if ($rentCountFormat !== null) {
                $this->assertEquals($rentCountFormat['format'], $transform['rent_count_type'][$formatKey]['format']);
                $this->assertEquals($rentCountFormat['label'], $transform['rent_count_type'][$formatKey]['label']);
                if (isset($rentCountFormat['data'])) {
                    foreach ($rentCountFormat['data'] as $dataKey => $data) {
                        $this->assertEquals($rentCountFormat['data'][$dataKey], $transform['rent_count_type'][$formatKey]['data'][$dataKey]);
                    }
                }
            }
        }
    }

    public function testBookingRoomDetailCanBeTransformOwnerNotPremium()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        // create song id
        $room->song_id = $room->id;
        $room->save();

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        // run test
        $transform = (new BookingRoomDetailTransformer())->transform($room);
        $this->assertEquals(config('booking.prebook_checkin_normal_month'), $transform['max_month_checkin']);
    }

    public function testBookingRoomDetailCanBeTransformOwnerPremiumAndKostLevelNotGoldPlus()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(1)->format('Y-m-d')
        ]);
        $room = factory(Room::class)->create();
        // create song id
        $room->song_id = $room->id;
        $room->save();

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        // now prebook active on owner premium and kost level gold plus
        $level = factory(KostLevel::class)->create(['name' => 'Premium']);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $transform = (new BookingRoomDetailTransformer())->transform($room);
        $this->assertEquals(config('booking.prebook_checkin_normal_month'), $transform['max_month_checkin']);
    }

    public function testBookingRoomDetailCanBeTransformOwnerPremium()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(1)->format('Y-m-d')
        ]);
        $room = factory(Room::class)->create();
        // create song id
        $room->song_id = $room->id;
        $room->save();

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        // now prebook active on owner premium and kost level gold plus
        $level = factory(KostLevel::class)->create([
            'id' => config('kostlevel.id.goldplus1'),
            'name' => 'Mamikos Goldplus 1'
        ]);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $transform = (new BookingRoomDetailTransformer())->transform($room);
        $this->assertEquals(config('booking.prebook_checkin_max_month'), $transform['max_month_checkin']);
    }

    private function generatePriceFormat(Room $room)
    {
        return collect($room->price()->priceTitleFormats([0,1,2,3,4,5], true))->map(function ($data, $key) {
            if ($data !== null)
                $data['format'] = isset(self::PRICE_FORMATED[$key]) ? self::PRICE_FORMATED[$key] : null;
            return $data;
        });
    }

    private function generateRentCountFormat(Room $room)
    {
        return collect(RentCountHelper::getAliasesRentCount($room->price()))->map(function ($data) {
            if ($data !== null && isset($data['format']))
                $data['data'] = RentCountHelper::getRentCount($data['format']);
            return $data;
        });
    }

    private function expectedBookingRoomDetailStructure(): array
    {
        return [
            'id',
            'name',
            'slug',
            'address',
            'province',
            'city',
            'gender',
            'price_title',
            'price_title_format',
            'price_title_formats',
            'price',
            'rent_count_type',
            'subdistrict',
            'available_room',
            'photo_url',
            'status_kost',
            'booking_designer',
            'max_renter',
            'facilities',
            'fac_room',
            'isMarriedCouple',
            'max_month_checkin',
            'is_flash_sale',
            'level_info',
            'is_booking_with_calendar',
            'booking_time_restriction_active',
            'available_booking_time_start',
            'available_booking_time_end',
        ];
    }
}