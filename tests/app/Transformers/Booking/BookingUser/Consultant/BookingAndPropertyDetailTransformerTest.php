<?php

namespace app\Transformers\Booking\BookingUser\Consultant;

use App\Entities\Booking\BookingStatus;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Level\KostLevel;
use App\Entities\Booking\BookingUser;
use App\Transformers\Booking\BookingUser\Consultant\BookingAndPropertyDetailTransformer;

class BookingAndPropertyDetailTransformerTest extends MamiKosTestCase
{
    public function testTransformerWithoutPhoto()
    {
        $room = factory(Room::class)->create();
        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $transformer = new BookingAndPropertyDetailTransformer();
        $result = $transformer->transform($booking);

        $expected = [
            'booking_code' => $booking->booking_code,
            'booking_status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
            'contact_name' => $booking->contact_name,
            'property_id' => $booking->designer_id,
            'property_name' => $booking->room->name,
            'property_owner_name' => $booking->room->owner_name,
            'property_owner_phone' => $booking->room->owner_phone,
            'property_photo' => null,
            'kost_level' => $booking->room->level->toArray(),
            'reason' => $booking->reject_reason ?? $booking->cancel_reason,
            'last_status_update_by' => '-'
            ];

        $this->assertEquals($expected, $result);
    }

    public function testTransformerWithPhoto()
    {
        $propertyPhoto = factory(Media::class)->create();
        $room = factory(Room::class)->create(['photo_id' => $propertyPhoto->id]);
        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $transformer = new BookingAndPropertyDetailTransformer();
        $result = $transformer->transform($booking);

        $expected = [
            'booking_code' => $booking->booking_code,
            'booking_status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
            'contact_name' => $booking->contact_name,
            'property_id' => $booking->designer_id,
            'property_name' => $booking->room->name,
            'property_owner_name' => $booking->room->owner_name,
            'property_owner_phone' => $booking->room->owner_phone,
            'property_photo' => [
                'real' => $propertyPhoto->getMediaUrl()['real'],
                'small' => $propertyPhoto->getMediaUrl()['small'],
                'medium' => $propertyPhoto->getMediaUrl()['medium'],
                'large' => $propertyPhoto->getMediaUrl()['large'],
            ],
            'kost_level' => $booking->room->level->toArray(),
            'reason' => $booking->reject_reason ?? $booking->cancel_reason,
            'last_status_update_by' => '-'
            ];

        $this->assertEquals($expected, $result);
    }

    public function testTransformerWithLevel()
    {
        $room = factory(Room::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['name' => 'GP1']);
        $room->level()->attach($kostLevel);
        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);

        $transformer = new BookingAndPropertyDetailTransformer();
        $result = $transformer->transform($booking);

        $this->assertTrue($result['kost_level'][0]['name'] === 'GP1');
    }

    public function testTransformerWithLastStatusUpdateBy()
    {
        $room = factory(Room::class)->create();
        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id]);
        factory(BookingStatus::class)->create([
            'booking_user_id' => $booking->id,
            'changed_by_role' => 'admin'
        ]);

        $transformer = new BookingAndPropertyDetailTransformer();
        $result = $transformer->transform($booking);

        $expected = [
            'booking_code' => $booking->booking_code,
            'booking_status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
            'contact_name' => $booking->contact_name,
            'property_id' => $booking->designer_id,
            'property_name' => $booking->room->name,
            'property_owner_name' => $booking->room->owner_name,
            'property_owner_phone' => $booking->room->owner_phone,
            'property_photo' => null,
            'kost_level' => $booking->room->level->toArray(),
            'reason' => $booking->reject_reason ?? $booking->cancel_reason,
            'last_status_update_by' => 'admin'
            ];

        $this->assertEquals($expected, $result);
    }
}