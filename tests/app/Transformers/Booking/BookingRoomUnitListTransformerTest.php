<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingRoomUnitListTransformer;

class BookingRoomUnitListTransformerTest extends MamiKosTestCase
{
    const PRETEXT_UNIT_NAME         = 'Kamar';
    const PRETEXT_UNIT_FLOOR        = 'Lantai';
    const LEVEL_MAMIKOS_GOLD_PLUS_1 = 'Mamikos Goldplus 1';

    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingRoomUnitListTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTransformAndSuccess()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $roomUnitEntity = factory(RoomUnit::class)->create([
            'name' => 'kamar A2',
            'floor' => 'lantai 1',
            'designer_id' => $roomEntity->id
        ]);

        // run test
        $transform = $this->transform->transform($roomUnitEntity);
        $assertHasKey = ['id', 'name', 'floor', 'available', 'disable'];
        foreach ($assertHasKey as $item) $this->assertArrayHasKey($item, $transform);
    }

    public function testGenerateNameWithPreText(): void
    {
        // prepare data
        $room = '1';

        // run test
        $result = $this->getPrivateMethod('generateUnitName', [$room]);
        $this->assertSame(self::PRETEXT_UNIT_NAME . ' ' . $room, $result);
    }

    public function testGenerateNameWithoutPreText(): void
    {
        // prepare data
        $room = 'Kamar 1';

        // run test
        $result = $this->getPrivateMethod('generateUnitName', [$room]);
        $this->assertSame($room, $result);
    }

    public function testGenerateUnitFloorWithPreText(): void
    {
        // prepare data
        $floor = '1';

        // run test
        $result = $this->getPrivateMethod('generateUnitFloor', [$floor]);
        $this->assertSame(self::PRETEXT_UNIT_FLOOR . ' ' . $floor, $result);
    }

    public function testGenerateUnitFloorWithoutPreText(): void
    {
        // prepare data
        $floor = 'Lantai 1';

        // run test
        $result = $this->getPrivateMethod('generateUnitFloor', [$floor]);
        $this->assertSame($floor, $result);
    }

    public function testCheckAvailableSuccess(): void
    {
        $roomUnitEntity = factory(RoomUnit::class)->make([
            'occupied' => 0
        ]);
        $result = $this->getPrivateMethod('checkAvailable', [$roomUnitEntity]);
        $this->assertTrue($result);
    }

    public function testCheckDisableSuccess(): void
    {
        $roomUnitEntity = factory(RoomUnit::class)->make([
            'occupied' => 1
        ]);
        $result = $this->getPrivateMethod('checkDisable', [$roomUnitEntity]);
        $this->assertTrue($result);
    }

    public function testCheckGoldPlusSuccess(): void
    {
        $roomLevelEntity = factory(RoomLevel::class)->create([
            'name' => self::LEVEL_MAMIKOS_GOLD_PLUS_1
        ]);
        $roomEntity = factory(Room::class)->create();
        $roomUnitEntity = factory(RoomUnit::class)->create([
            'designer_id' => $roomEntity->id,
            'room_level_id' => $roomLevelEntity->id
        ]);

        $result = $this->getPrivateMethod('checkGoldPlus', [$roomUnitEntity]);
        $this->assertTrue($result);
    }

    private function getPrivateMethod(string $className, array $args = [])
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingRoomUnitListTransformer::class,
            $className
        );

        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}