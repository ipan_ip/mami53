<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserListTransformer;
use App\User;
use Carbon\Carbon;
use Mockery;

/**
 *  Test BookingUserListTransformer
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class BookingUserListTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserCollectionCanBeTransform()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create(['song_id' => 1]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id
        ]);
        $bookingUser = factory(BookingUser::class)->create([
            'booking_code' => 'MM01',
            'contact_name' => 'Syifandi',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_designer_id' => $bookingDesigner->id,
            'user_id' => $user->id,
            'is_married' => 1,
            'designer_id' => $room->song_id,
            'contract_id' => 1
        ]);

        Mockery::mock('overload:App\Http\Helpers\MamipayApiHelper')
            ->shouldReceive('makeRequest')
            ->andReturnUsing(function (){

                $detail = new \StdClass;;
                $detail->confirm_expired_date = Carbon::now()->format('Y-m-d');
                $detail->room_number = 1001;
                $detail->first_invoice = '';

                $resp = new \StdClass;
                $resp->booking_detail = $detail;

                return $resp;
            });


        Mockery::mock('App\Http\Helpers\BookingInvoiceHelper')
            ->shouldReceive('getAmountByContractId')
            ->andReturn([
                'base' => 10000,
                'total' => [
                    'price_total' => 1000
                ]
            ]);

        $transformed = (new BookingUserListTransformer())->transform($bookingUser);

        $this->assertIsArray($transformed);
        $this->assertIsArray($transformed['room']);
        $this->assertIsArray($transformed['room']['fac_room']);
        $this->assertIsArray($transformed['room']['price']);
        $this->assertIsInt($transformed['room']['price']['daily']);
        $this->assertIsInt($transformed['room']['price']['weekly']);
        $this->assertIsInt($transformed['room']['price']['monthly']);
        $this->assertIsInt($transformed['room']['price']['quarterly']);
        $this->assertIsInt($transformed['room']['price']['semiannualy']);
        $this->assertIsInt($transformed['room']['price']['yearly']);
        $this->assertEquals($bookingUser->id, $transformed['_id']);
        $this->assertEquals($bookingUser->booking_code, $transformed['booking_code']);
        $this->assertEquals($bookingUser->contact_name, $transformed['name']);
        $this->assertEquals(Carbon::parse($bookingUser->created_at)->format('Y-m-d H:i:s'), $transformed['created_at']);
        $this->assertEquals($room->song_id, $transformed['room']['_id']);

        // assert has key
        $this->assertArrayHasKey('_id', $transformed);
        $this->assertArrayHasKey('booking_code', $transformed);
        $this->assertArrayHasKey('name', $transformed);
        $this->assertArrayHasKey('phone_number', $transformed);
        $this->assertArrayHasKey('description', $transformed);
        $this->assertArrayHasKey('duration', $transformed);
        $this->assertArrayHasKey('duration_string', $transformed);
        $this->assertArrayHasKey('rent_count_type', $transformed);
        $this->assertArrayHasKey('checkin_date', $transformed);
        $this->assertArrayHasKey('checkin_date_formatted', $transformed);
        $this->assertArrayHasKey('checkout_date', $transformed);
        $this->assertArrayHasKey('checkout_date_formatted', $transformed);
        $this->assertArrayHasKey('payment_status', $transformed);
        $this->assertArrayHasKey('is_fully_paid', $transformed);
        $this->assertArrayHasKey('has_checked_in', $transformed);
        $this->assertArrayHasKey('status', $transformed);
        $this->assertArrayHasKey('cancel_reason', $transformed);
        $this->assertArrayHasKey('scheduled_date', $transformed);
        $this->assertArrayHasKey('unpaid_invoice_url', $transformed);
        $this->assertArrayHasKey('invoice_url', $transformed);
        $this->assertArrayHasKey('invoice_number', $transformed);
        $this->assertArrayHasKey('invoice_total_amount', $transformed);
        $this->assertArrayHasKey('payment_expired_date', $transformed);
        $this->assertArrayHasKey('room', $transformed);
        $this->assertArrayHasKey('group_channel_url', $transformed);
        $this->assertArrayHasKey('created_at', $transformed);
        $this->assertArrayHasKey('is_flash_sale', $transformed);
        $this->assertArrayHasKey('flash_sale', $transformed);
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }
}