<?php

namespace app\Transformers\Booking;

use App\Transformers\Booking\BookingAcceptanceRateAdminTransformer;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Entities\Booking\BookingAcceptanceRate;

class BookingAcceptanceRateAdminTransformerTest extends MamiKosTestCase
{

    public function testTransform()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $transformed = (new BookingAcceptanceRateAdminTransformer())->transform($bar);

        $this->assertEquals($transformed['room_name'], $room->name);
        $this->assertEquals($transformed['id'], $bar->id);
        $this->assertEquals($transformed['owner_name'], $room->owner_name);
        $this->assertEquals($transformed['owner_phone'], $room->owner_phone);
        $this->assertEquals($transformed['designer_id'], $room->id);
        $this->assertEquals($transformed['avg_time'], $bar->average_time);
        $this->assertEquals($transformed['rate'], $bar->rate);
    }

}
