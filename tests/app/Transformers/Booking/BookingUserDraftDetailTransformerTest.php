<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Test\MamiKosTestCase;

class BookingUserDraftDetailTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserDraftDetailCanBeTransform()
    {
        // prepare data
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'is_married' => 1
        ]);

        // run test
        $transform = (new BookingUserDraftDetailTransformer)->transform($bookingUserDraftEntity);

        // assert test
        $this->assertEquals($bookingUserDraftEntity->id, $transform['id']);
        $this->assertEquals($bookingUserDraftEntity->rent_count_type, $transform['rent_count_type']);
        $this->assertEquals($bookingUserDraftEntity->duration, $transform['duration']);
        $this->assertEquals($bookingUserDraftEntity->tenant_name, $transform['tenant_name']);
        $this->assertEquals($bookingUserDraftEntity->tenant_phone, $transform['tenant_phone']);
        $this->assertEquals($bookingUserDraftEntity->tenant_introduction, $transform['tenant_introduction']);
        $this->assertEquals($bookingUserDraftEntity->tenant_job, $transform['tenant_job']);
        $this->assertEquals($bookingUserDraftEntity->tenant_work_place, $transform['tenant_work_place']);
        $this->assertEquals($bookingUserDraftEntity->tenant_description, $transform['tenant_description']);
        $this->assertEquals($bookingUserDraftEntity->total_renter_count, $transform['total_rent_count']);
        $this->assertEquals($bookingUserDraftEntity->is_married, $transform['is_married']);
    }
}