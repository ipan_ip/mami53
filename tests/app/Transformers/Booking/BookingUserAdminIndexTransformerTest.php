<?php
namespace app\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\StatusChangedBy;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Repositories\Booking\BookingUserRepository;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserAdminIndexTransformer;
use App\User;
use Carbon\Carbon;

class BookingUserAdminIndexTransformerTest extends MamiKosTestCase
{
    protected $bookingUserRepository;

    protected function setUp()
    {
        parent::setUp();

        $this->bookingUserRepository = $this->app->make(BookingUserRepository::class);
    }

    public function testTransformWithNoRoom()
    {
        $booking = factory(BookingUser::class)->create();
        $actual = (new BookingUserAdminIndexTransformer())->transform($booking);

        $expected = [
            "booking_code" => $booking->booking_code,
            "contact_name" => "",
            "contact_phone" => "",
            "is_room_deleted" => true,
            "room_name" => null,
            "owner_phone" => null,
            "area_city" => null,
            "is_room_mamirooms" => null,
            "is_room_premium_photo" => null,
            "is_room_testing" => null,
            "listing_price" => null,
            "original_price" => null,
            "checkin_date" => !is_null($booking->checkin_date) ? $booking->checkin_date->toDateString() : null,
            "checkout_date" => Carbon::parse($booking->checkout_date)->format('Y-m-d'),
            'stay_duration' => $booking->stay_duration,
            "rent_count_type" => !is_null($booking->rent_count_type) ? $booking->rent_count_type : BookingUser::MONTHLY_TYPE,
            "status" => $booking->status,
            "changed_by" => StatusChangedBy::TENANT,
            "id" => $booking->id,
            'is_booked' => ($booking->status == BookingUser::BOOKING_STATUS_BOOKED),
            'is_verified' => ($booking->status == BookingUser::BOOKING_STATUS_VERIFIED),
            'is_paid' => ($booking->status == BookingUser::BOOKING_STATUS_PAID),
            'is_confirmed' => ($booking->status == BookingUser::BOOKING_STATUS_CONFIRMED),
            'is_rejected' => ($booking->status == BookingUser::BOOKING_STATUS_REJECTED),
            'is_cancelled' => ($booking->status == BookingUser::BOOKING_STATUS_CANCELLED),
            'is_cancelled_by_admin' => ($booking->status == BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN),
            'is_terminated' => ($booking->status == BookingUser::BOOKING_STATUS_TERMINATED),
            'is_expired' => ($booking->status == BookingUser::BOOKING_STATUS_EXPIRED),
            'is_expired_date' => ($booking->status == BookingUser::BOOKING_STATUS_EXPIRED_DATE),
            'is_not_paid' => ($booking->status == BookingUser::BOOKING_STATUS_EXPIRED),
            'is_not_confirmed' => in_array($booking->status, [BookingUser::BOOKING_STATUS_EXPIRED_DATE, BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER]),
            'is_guarantee_requested' => ($booking->status == BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED),
            'has_checked_in' => ($booking->status == BookingUser::BOOKING_STATUS_CHECKED_IN),
            'has_been_booked' => (in_array($booking->status, [BookingUser::BOOKING_STATUS_BOOKED])),
            'has_checked_out' => (!is_null($booking->contract) ? ($booking->contract->end_date < Carbon::now()) : false),
            "is_instant_booking" => false,
            "additional_prices" => null,
            "rent_counts" => null,
            "note" => [
                "category" => "",
                "content" => "",
                "created_at" => "",
                "created_by" => "",
                "updated_at" => "",
                "updated_by" => "",
            ],
            "created_at" => $booking->created_at->toDateTimeString(),
            "created_by" => "unknown",
            "expired_date" => null,
            "expired_date_by_owner" => $this->bookingUserRepository->getExpiredBySLA($booking),
            "expired_date_by_tenant" => null,
            "room_available" => 0,
            "kost_level" => "No",
            "from_pots" => false,
            "transfer_permission" => null,
            "transfer_permission_rule" => null,
            "tags" => null,
            "guest_total" => null,
            "kos_last_update" => null,
            "instant_booking_owner_id" => null,
            "additional_prices_by_owner" => null
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testBookingUserTransformOnAdminPage()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'id' => '160951',
            'song_id' => '33928883'
        ]);

        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'id' => '1749',
            'booking_designer_id' => $bookingDesigner->id,
            'booking_code' => 'MAUDYCODE19',
            'contact_name' => 'Maudy Ayunda',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'user_id' => $user->id,
            'checkin_date' => '2020-04-15',
            'checkout_date' => '2020-04-15',
            'rent_count_type' => BookingUser::MONTHLY_TYPE,
            'transfer_permission' => BookingUser::CONFIRMED
        ]);

        $transformed = (new BookingUserAdminIndexTransformer())->transform($bookingUser);
        $this->assertIsArray($transformed);
        $this->assertIsArray($transformed['note']);
        $this->assertEquals($bookingUser->id, $transformed['id']);
        $this->assertEquals($bookingUser->booking_code, $transformed['booking_code']);
        $this->assertEquals($bookingUser->checkin_date, $transformed['checkin_date']);
        $this->assertEquals($bookingUser->checkout_date, $transformed['checkout_date']);
        $this->assertEquals($bookingUser->rent_count_type, $transformed['rent_count_type']);
        $this->assertEquals($room->name, $transformed['room_name']);
        $this->assertEquals($room->area_city, $transformed['area_city']);
    }

    public function testGetKostLevelInGoldPlusAndOyoIsNull(): void
    {
        // prepare data
        $room = null;

        // run test
        $this->assertEquals('No', $this->getKostLevelGoldPlusAndOyo($room));
    }

    public function testGetKostLevelInGoldPlusAndOyoIsGoldPlus(): void
    {
        // create room data
        $room = factory(Room::class)->create();

        // level
        $level = factory(KostLevel::class)->create([
            'id' => config('kostlevel.id.goldplus1'),
            'name' => 'Mamikos Goldplus 1'
        ]);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $this->assertEquals($level->name, $this->getKostLevelGoldPlusAndOyo($room));
    }

    public function testGetKostLevelInGoldPlusAndOyoIsOyo(): void
    {
        // create room data
        $room = factory(Room::class)->create();

        // level
        $level = factory(KostLevel::class)->create(['name' => 'OYO']);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $this->assertEquals($level->name, $this->getKostLevelGoldPlusAndOyo($room));
    }

    private function getKostLevelGoldPlusAndOyo(?Room $room): string
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingUserAdminIndexTransformer::class,
            'getKostLevelInGoldPlusAndOyo'
        );
        $transformer = $this->app->make(BookingUserAdminIndexTransformer::class);

        $result = $method->invokeArgs(
            $transformer, [$room]
        );

        return $result;
    }

}