<?php


namespace app\Transformers\Booking;

use App\Entities\Booking\BookingStatus;
use App\Transformers\Booking\BookingUserStatusChangedTransformer;
use App\Test\MamiKosTestCase;

class BookingUserStatusChangedTransformerTest extends MamiKosTestCase
{
    
    public function testTransformer()
    {
        $bookingStatus = factory(BookingStatus::class)->create();

        $transformed = (new BookingUserStatusChangedTransformer())->transform($bookingStatus);
        $this->assertIsArray($transformed);
        $this->assertEquals($bookingStatus->status, $transformed['status']);
        $this->assertEquals($bookingStatus->changed_by, $transformed['changed_by']);
        $this->assertEquals($bookingStatus->created_at, $transformed['created_at']);
        $this->assertEquals($bookingStatus->updated_at, $transformed['updated_at']);
    }
}