<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class BookingUserDraftHomepageShortcutTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserDraftHomepageShortcutCanBeTransformAllFieldExist()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create([
            'gender' => 1,
            'room_available' => 5
        ]);
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'is_married' => 1,
            'designer_id' => $roomEntity->id,
            'rent_count_type' => 'monthly',
            'duration' => 1,
            'checkin' => Carbon::today()->format('Y-m-d')
        ]);

        // run test
        $transform = (new BookingUserDraftHomepageShortcutTransformer)->transform($bookingUserDraftEntity);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('name', $transform);
        $this->assertArrayHasKey('slug', $transform);
        $this->assertArrayHasKey('city', $transform);
        $this->assertArrayHasKey('subdistrict', $transform);
        $this->assertArrayHasKey('type', $transform);
        $this->assertArrayHasKey('available_room', $transform);
        $this->assertArrayHasKey('price', $transform);
        $this->assertArrayHasKey('image', $transform);
        $this->assertArrayHasKey('checkin', $transform);
        $this->assertArrayHasKey('rent_duration', $transform);
        $this->assertArrayHasKey('is_flash_sale', $transform);
    }

    public function testBookingUserDraftHomepageShortcutCanBeTransform()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create([
            'gender' => 1,
            'room_available' => 5
        ]);
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'is_married' => 1,
            'designer_id' => $roomEntity->id,
            'rent_count_type' => 'monthly',
            'duration' => 1,
            'checkin' => Carbon::today()->format('Y-m-d')
        ]);

        // run test
        $transform = (new BookingUserDraftHomepageShortcutTransformer)->transform($bookingUserDraftEntity);

        // assert test
        $this->assertEquals($roomEntity->name, $transform['name']);
        $this->assertEquals($roomEntity->slug, $transform['slug']);
        $this->assertEquals('putra', $transform['type']);
        $this->assertEquals($roomEntity->available_room, $transform['available_room']);
        $this->assertEquals($bookingUserDraftEntity->checkin, $transform['checkin']);
        $this->assertEquals(RentCountHelper::getFormatRentCount($bookingUserDraftEntity->duration, $bookingUserDraftEntity->rent_count_type), $transform['rent_duration']);
    }
}
