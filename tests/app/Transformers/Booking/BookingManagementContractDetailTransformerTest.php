<?php

namespace App\Transformers\Booking\BookingUser\Consultant;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Mamipay\MamipayAdditionalCost;
use App\Entities\Mamipay\MamipayBillingRule;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Room\Room;
use App\Entities\SLA\SLAConfiguration;
use App\Entities\SLA\SLAVersion;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class BookingManagementContractDetailTransformerTest extends MamiKosTestCase
{
    use WithFaker;

    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = new BookingManagementContractDetailTransformer();
    }

    public function testTransformWithBookedStatus(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-16 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create()->song_id
        ]);

        factory(BookingUserRoom::class)->create([
            'booking_user_id' => $booking->id,
            'price' => 1500000,
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-16 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 1500000,
                'transferred_to_owner' => 1500000,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithBookedStatusAndPercentageCharge(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN,
            'value' => 60
        ]);

        $level = factory(KostLevel::class)->create([
            'charging_fee' => 10,
            'charging_type' => 'percentage'
        ]);

        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-16 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => $room->song_id
        ]);

        factory(BookingUserRoom::class)->create([
            'booking_user_id' => $booking->id,
            'price' => 1500000,
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-16 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 1500000,
                'transferred_to_owner' => 1350000,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithBookedStatusAndAmountCharge(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN,
            'value' => 60
        ]);

        $level = factory(KostLevel::class)->create([
            'charging_fee' => 150000,
            'charging_type' => 'amount'
        ]);

        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-16 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => $room->song_id
        ]);

        factory(BookingUserRoom::class)->create([
            'booking_user_id' => $booking->id,
            'price' => 1500000,
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-16 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 1500000,
                'transferred_to_owner' => 1350000,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithBookedStatusAndWithoutBookingUserRoom(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-16 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['price_daily' => 1500000])->song_id
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-16 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 1500000,
                'transferred_to_owner' => 1500000,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithBookedStatusAndNoPrice(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN,
            'value' => 60
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-16 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['price_daily' => null])->song_id
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-16 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 0,
                'transferred_to_owner' => 0,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithNonBookedStatus(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $contract = factory(MamipayContract::class)->create();

        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create([
            'charging_fee' => 3000000,
            'charging_type' => 'amount'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $level->id,
            'kost_id' => $room->song_id
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-17 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'contract_id' => $contract->id,
            'designer_id' => $room->song_id
        ]);

        factory(MamipayBillingRule::class)->create([
            'tenant_id' => $contract->tenant_id,
            'deposit_amount' => 1000000,
            'room_id' => $booking->designer_id
        ]);

        $dp = factory(MamipayInvoice::class)->create([
            'amount' => 3000000,
            'transfer_amount' => 1500000,
            'invoice_number' => 'DP/XXXX/XXXXXXX',
            'contract_id' => $booking->contract_id
        ]);

        $st = factory(MamipayInvoice::class)->create([
            'amount' => 3000000,
            'transfer_amount' => 1500000,
            'invoice_number' => 'ST/XXXX/XXXXXXX',
            'contract_id' => $booking->contract_id
        ]);

        factory(MamipayAdditionalCost::class)->create([
            'invoice_id' => $dp->id,
            'cost_value' => 1000
        ]);

        factory(MamipayAdditionalCost::class)->create([
            'invoice_id' => $st->id,
            'cost_value' => 1000
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-17 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 6000000,
                'transferred_to_owner' => 3000000,
                'down_payment' => 3000000,
                'deposit' => 1000000,
                'additional_cost' => 2000
            ]
        ];

        $actual = $this->transformer->transform($booking);

        $this->assertEquals($expected, $actual);
    }
}
