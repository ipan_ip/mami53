<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDesignerPriceComponentListTransformer;

class BookingDesignerPriceComponentListTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingDesignerPriceComponentListTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTransformAndSuccess()
    {
        // prepare data
        $priceComponentEntity = factory(BookingDesignerPriceComponent::class)->make();

        // run test
        $transform = $this->transform->transform($priceComponentEntity);
        $assertHasKey = ['price_name', 'price_label', 'regular_price', 'regular_price_string', 'price', 'price_string'];
        foreach ($assertHasKey as $item) $this->assertArrayHasKey($item, $transform);
    }
}