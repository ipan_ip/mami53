<?php

namespace app\Transformers\Booking;

use App\Entities\Activity\Call;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Transformers\Booking\BookingMyRoomDetailTransformer;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Transformers\Contract\ContractUserTransformer;
use App\User;
use Mockery;

class BookingMyRoomDetailTransformerTest extends MamiKosTestCase
{
    /**
     * @runTestsInSeparateProcesses
     * @preserveGlobalState disabled
     */
    public function testBookingMyRoomDetailTransform()
    {
        $room = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id
        ]);

        $user = factory(User::class)->create();

        $contract = factory(MamipayContract::class)->create();
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'booking_code' => 'MM01',
            'contact_name' => 'Maudy Ayunda',
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'booking_designer_id' => $bookingDesigner->id,
            'user_id' => $user->id,
            'contract_id' => $contract->id
        ]);

        $mockHelper = Mockery::mock('overload:App\Http\Helpers\BookingInvoiceHelper');
        $mockHelper->shouldReceive('getAmountByContractId')
            ->andReturn([]);

        $mockHelper->shouldReceive('withPlatformParam')
            ->andReturn('G1Vq0?platform=web');

        $transformed = (new BookingMyRoomDetailTransformer())->transform($bookingUser);
        $this->assertEquals($contract->id, $transformed['contract_id']);
        $this->assertIsArray($transformed['room_data']);
        $this->assertIsArray($transformed['booking_data']);
        $this->assertIsArray($transformed['prices']);
        $this->assertEquals($room->name, $transformed['room_data']['name']);
        $this->assertEquals($bookingUser->status, $transformed['booking_data']['status']);
    }

    public function testGetGroupChannelUrl(): void
    {
        // prepare data
        $userId = 1;
        $roomId = 1;
        $chatGroupId = 'sendbrid';
        $callEntity = factory(Call::class)->create([
            'chat_group_id' => $chatGroupId,
            'user_id' => $userId,
            'designer_id' => $roomId
        ]);

        // run test
        $result = $this->getPrivateFunction('getGroupChannelUrl', [$roomId, $userId]);
        $this->assertNotNull($result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            BookingMyRoomDetailTransformer::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->app->make(BookingMyRoomDetailTransformer::class),
            $args
        );

        return $result;
    }
}