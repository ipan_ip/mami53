<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserDetailTransformer;
use App\User;
use Carbon\Carbon;

class BookingUserDetailTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingUserDetailTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingUserDetailCanBeTransform(): void
    {
        // prepare data
        $roomMediaEntity = factory(Media::class)->create();
        $roomEntity = factory(Room::class)->create([
            'photo_id' => $roomMediaEntity->id
        ]);
        $bookingDesignerEntity = factory(BookingDesigner::class)->create([
            'designer_id' => $roomEntity->id
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesignerEntity->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'checkin_date' => Carbon::now()->format('Y-m-d')
        ]);
        $userOwnerEntity = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userOwnerEntity->id
        ]);

        // run test
        $transform = $this->transform->transform($bookingUserEntity);
        $arrayKeys = [
            'room_data',
            'booking_data',
            'guests',
            'payment',
            'bank_accounts',
            'user_id',
            'cs_id',
        ];
        foreach ($arrayKeys as $item) $this->arrayHasKey($item, $transform);
    }
}