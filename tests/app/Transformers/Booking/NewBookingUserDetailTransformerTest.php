<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingStatus;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Libraries\MamipayApi;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\NewBookingUserDetailTransformer;
use App\User;
use Mockery;

/**
 *  Test NewBookingUserDetailTransformer
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class NewBookingUserDetailTransformerTest extends MamiKosTestCase
{
    protected $transform, $bugSnag;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->transform = $this->app->make(NewBookingUserDetailTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testTransformRoomIsNull(): void
    {
        // prepare data
        $bookingDesignerEntity = factory(BookingDesigner::class)->create();
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesignerEntity->id
        ]);

        // mock bugsnag
        $this->bugSnag->shouldReceive('notifyException');

        // run test
        $transformed = $this->transform->transform($bookingUserEntity);
        $this->assertIsArray($transformed);
    }

    public function testBookingUserCollectionCanBeTransform(): void
    {
        // mock data
        $this->mock(MamipayApi::class, function ($mock) {
            $mock->shouldReceive('get')->andReturn(null);
        });

        // prepare data
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create(['song_id' => 1]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id
        ]);
        $contract = factory(MamipayContract::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'booking_code' => 'MM01',
            'contact_name' => 'Syifandi',
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'booking_designer_id' => $bookingDesigner->id,
            'user_id' => $user->id,
            'is_married' => 1,
            'contract_id' => $contract->id,
        ]);
        factory(BookingStatus::class)->create([
            'booking_user_id' => $bookingUser->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $transformed = $this->transform->transform($bookingUser);

        $this->assertIsArray($transformed);
        $this->assertIsArray($transformed['user_booking']);
        $this->assertIsArray($transformed['room_data']);
        $this->assertIsArray($transformed['booking_data']);
        $this->assertIsArray($transformed['payment']);
        $this->assertIsArray($transformed['note']);
        $this->assertEquals($bookingUser->id, $transformed['id']);
        $this->assertEquals($bookingUser->contact_name, $transformed['user_booking']['name']);
        $this->assertEquals($room->song_id, $transformed['room_data']['_id']);
        $this->assertEquals($bookingUser->booking_code, $transformed['booking_data']['booking_code']);
        $this->assertTrue($transformed['booking_data']['is_married']);
        $this->assertEquals(BookingUser::BOOKING_PAYMENT_STATUS[$bookingUser->status], $transformed['payment']['status']);

        // define test
        $assertIsArray = ['user_booking', 'room_data', 'booking_data', 'payment', 'note'];
        $assertHasKey = ['id', 'user_booking', 'room_data', 'booking_data', 'payment', 'note', 'statuses', 'is_flash_sale', 'flash_sale'];

        // test data is array
        foreach ($assertIsArray as $item) $this->assertIsArray($transformed[$item]);

        // test data is exist
        foreach ($assertHasKey as $item) $this->assertArrayHasKey($item, $transformed);
    }

    public function testGetUnpaidInvoiceUrlIsNull(): void
    {
        // prepare data
        $bookingUserEntity = factory(BookingUser::class)->make();

        // get method
        $method = $this->getNonPublicMethodFromClass(
            NewBookingUserDetailTransformer::class,
            'getUnpaidInvoiceUrl'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$bookingUserEntity]
        );
        $this->assertNull($result);
    }

    public function testGetUnpaidInvoiceUrlSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $invoiceEntity = factory(MamipayInvoice::class)->create([
            'contract_id' => $contractEntity->id,
            'status' => MamipayInvoice::STATUS_UNPAID
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'contract_id' => $contractEntity->id
        ]);

        // get method
        $method = $this->getNonPublicMethodFromClass(
            NewBookingUserDetailTransformer::class,
            'getUnpaidInvoiceUrl'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$bookingUserEntity]
        );
        $this->assertNotNull($result);
    }

    public function testIsFullyPaidSuccess(): void
    {
        // prepare data
        $bookingUserEntity = factory(BookingUser::class)->make(['contract_id' => 1]);
        // get method
        $method = $this->getNonPublicMethodFromClass(
            NewBookingUserDetailTransformer::class,
            'isFullyPaid'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$bookingUserEntity]
        );
        $this->assertFalse($result);
    }
}