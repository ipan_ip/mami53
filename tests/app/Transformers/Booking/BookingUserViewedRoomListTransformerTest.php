<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\Entities\User\UserDesignerViewHistory;
use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class BookingUserViewedRoomListTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserViewedRoomListCanBeTransformAllFieldExist()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $userDesignerViewHistoryEntity = factory(UserDesignerViewHistory::class)->create([
            'designer_id' => $roomEntity->id
        ]);

        // run test
        $transform = (new BookingUserViewedRoomListTransformer)->transform($userDesignerViewHistoryEntity);

        // test
        $this->assertArrayHasKey('room_id', $transform);
        $this->assertArrayHasKey('viewed_at', $transform);
        $this->assertArrayHasKey('name', $transform);
        $this->assertArrayHasKey('slug', $transform);
        $this->assertArrayHasKey('city', $transform);
        $this->assertArrayHasKey('subdistrict', $transform);
        $this->assertArrayHasKey('available_room', $transform);
        $this->assertArrayHasKey('price', $transform);
        $this->assertArrayHasKey('image', $transform);
        $this->assertArrayHasKey('is_flash_sale', $transform);
    }

    public function testBookingUserViewedRoomListCanBeTransform()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create([
            'gender' => 1,
            'room_available' => 5,
            'price_monthly' => 10000
        ]);
        $userDesignerViewHistoryEntity = factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id
        ]);

        // run test
        $transform = (new BookingUserViewedRoomListTransformer)->transform($userDesignerViewHistoryEntity);

        // assert test
        $this->assertEquals($roomEntity->name, $transform['name']);
        $this->assertEquals($roomEntity->room_available, $transform['available_room']);
    }
}
