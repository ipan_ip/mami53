<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDraftPriceTransformer;

class BookingDraftPriceTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingDraftPriceTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingDraftCanBeTransform(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create([
            'price_monthly' => 500000
        ]);
        $roomPriceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomEntity->id,
            'component' => MamipayRoomPriceComponentType::DP,
            'price' => 0,
            'is_active' => 1,
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $roomPriceComponentEntity->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => 20
        ]);

        // run test
        $transform = $this->transform->transform($roomEntity);
        $assertHasKey = [
            'sort',
            'label',
            'type',
            'price',
            'price_label',
            'deposit',
            'deposit_label',
            'admin_fee',
            'admin_fee_label',
            'down_payment',
            'down_payment_label',
            'total_off_first_payment',
            'total_off_first_payment_label',
            'total_off_next_payment',
            'total_off_next_payment_label',
            'remaining_payment',
            'remaining_payment_label',
            'is_flash_sale',
            'flash_sale_discount',
            'flash_sale_discount_label',
            'additional_total',
            'additional_total_label',
            'additionals',
        ];
        foreach ($assertHasKey as $item) $this->arrayHasKey($item, $transform[0]);
    }

    public function testGetLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('getLabel', ['monthly']);
        $this->assertSame('Per Bulan', $result);
    }

    public function testGetPriceLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('priceLabel', [0]);
        $this->assertSame('Rp0', $result);
    }

    public function testGetPriceLabelMinus(): void
    {
        // run test
        $result = $this->getPrivateFunction('priceLabel', [-1000]);
        $this->assertSame('-Rp1.000', $result);
    }

    public function testGetDeposit(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomEntity->id,
            'component' => MamipayRoomPriceComponentType::DEPOSIT,
            'price' => 500000,
            'is_active' => 1,
        ]);

        // run test
        $result = $this->getPrivateFunction('getDeposit', [$roomEntity->mamipay_price_components]);
        $this->assertEquals(500000, $result);
    }

    public function testGetDownPayment(): void
    {
        // prepare data
        $price = 500000;
        $dp = 20;
        $roomEntity = factory(Room::class)->create();
        $roomPriceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomEntity->id,
            'component' => MamipayRoomPriceComponentType::DP,
            'price' => 0,
            'is_active' => 1,
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $roomPriceComponentEntity->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => $dp
        ]);
        $discount = ($dp / 100 ) * $price;

        // run test
        $result = $this->getPrivateFunction('getDownPayment', [$roomEntity, $price]);
        $this->assertEquals($discount, $result);
    }

    public function testGetAdditionals(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomEntity->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'name' => 'Iuran Air',
            'price' => 30000,
            'is_active' => 1,
        ]);

        // run test
        $result = $this->getPrivateFunction('getAdditionals', [$roomEntity->mamipay_price_components, 'monthly']);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('total', $result);
        $this->assertArrayHasKey('name', $result['data'][0]);
        $this->assertArrayHasKey('price', $result['data'][0]);
        $this->assertArrayHasKey('price_label', $result['data'][0]);
    }

    public function testGetTotalFirstPayment(): void
    {
        // prepare data
        $price      = 50000;
        $additional = 40000;
        $deposit    = 100000;
        $discount   = 25000;
        $adminFe    = 9000;

        // run test
        $result = $this->getPrivateFunction(
            'getTotalFirstPayment',
            [$price, $additional, $deposit, $discount, $adminFe]
        );
        $this->assertIsInt($result);
    }

    public function testGetTotalNextPayment(): void
    {
        // prepare data
        $price      = 50000;
        $additional = 40000;
        $adminFe    = 9000;

        // run test
        $result = $this->getPrivateFunction(
            'getTotalNextPayment',
            [$price, $additional, $adminFe]
        );
        $this->assertIsInt($result);
    }

    public function testGetAdminFee(): void
    {
        // run test
        $result = $this->getPrivateFunction(
            'getAdminFee',
            []
        );
        $this->assertIsInt($result);
    }

    public function testGetFlashSalePercentageWithoutDiscount(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        // run test
        $result = $this->getPrivateFunction(
            'getFlashSalePercentage',
            [$roomEntity, 'monthly']
        );
        $this->assertSame(0, $result);
    }

    public function testGetFlashSalePercentageWithDiscount(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        factory(BookingDiscount::class)->create([
            'designer_id' => $roomEntity->id,
            'price_type' => 'monthly',
            'discount_value' => 50
        ]);
        // run test
        $result = $this->getPrivateFunction(
            'getFlashSalePercentage',
            [$roomEntity, 'monthly']
        );
        $this->assertNotSame(0, $result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            BookingDraftPriceTransformer::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}