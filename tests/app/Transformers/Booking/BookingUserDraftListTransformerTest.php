<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class BookingUserDraftListTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserDraftListCanBeTransform()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create([
            'gender' => 1,
            'room_available' => 5
        ]);
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'is_married' => 1,
            'designer_id' => $roomEntity->id,
            'rent_count_type' => 'monthly',
            'duration' => 1,
            'checkin' => Carbon::today()->format('Y-m-d')
        ]);

        // run test
        $transform = (new BookingUserDraftListTransformer)->transform($bookingUserDraftEntity);

        // assert test
        $this->assertEquals($bookingUserDraftEntity->id, $transform['id']);
        $this->assertTrue($transform['is_married']);
        $this->assertEquals($roomEntity->name, $transform['room']['name']);
        $this->assertEquals($roomEntity->slug, $transform['room']['slug']);
        $this->assertEquals('putra', $transform['room']['type']);
        $this->assertEquals($roomEntity->available_room, $transform['room']['available_room']);
        $this->assertEquals(RentCountHelper::getFormatRentCount($bookingUserDraftEntity->duration, $bookingUserDraftEntity->rent_count_type), $transform['room']['rent_duration']);
    }
}
