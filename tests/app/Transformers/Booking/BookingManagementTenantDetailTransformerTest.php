<?php

namespace App\Transformers\Consultant\BookingManagement;

use App\User;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\User\UserMedia;
use App\Transformers\Booking\BookingUser\Consultant\BookingManagementTenantDetailTransformer;

class BookingManagementTenantDetailTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = new BookingManagementTenantDetailTransformer();
    }

    public function testTransformWithTenantPhoto(): void
    {
        $photoProfile = factory(Media::class)->create();
        $photoIdentifier = factory(MamipayMedia::class)->create();
        $photoSelfie = factory(MamipayMedia::class)->create();

        $user = factory(User::class)->create([
            'introduction' => 'test', 
            'photo_id' => $photoProfile->id,
            'gender' => 'male',
            'marital_status' => 'Belum Kawin',
            'job' => 'karyawan'
        ]);
        
        factory(MamipayTenant::class)->create([
            'user_id' => $user->id, 
            'photo_identifier_id' => $photoIdentifier->id, 
            'photo_document_id' => $photoSelfie->id,
        ]);

        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $actual = $this->transformer->transform($booking);
        $expected = [
            'id' => $user->id,
            'name' => $user->name,
            'phone_number' => $user->phone_number,
            'email' => $user->email,
            'about_tenant' => $user->introduction,
            'photo_profile' => [
                'small' => $photoProfile->getMediaUrl()['small'],
                'medium' => $photoProfile->getMediaUrl()['medium'],
                'large' => $photoProfile->getMediaUrl()['large'],
            ],
            'photo_identifier' => [
                'small' => $photoIdentifier->getCachedUrlsAttribute()['small'],
                'medium' => $photoIdentifier->getCachedUrlsAttribute()['medium'],
                'large' => $photoIdentifier->getCachedUrlsAttribute()['large'],
            ],
            'photo_selfie' => [
                'small' => $photoSelfie->getCachedUrlsAttribute()['small'],
                'medium' => $photoSelfie->getCachedUrlsAttribute()['medium'],
                'large' => $photoSelfie->getCachedUrlsAttribute()['large'],
            ],
            'gender' => $user->gender,
            'marital_status' => $user->marital_status,
            'occupation' => ucfirst($user->job),
        ];

        $this->assertEquals($expected, $actual);
    }


    public function testTransformWithUserPhoto(): void
    {
        $photoProfile = factory(Media::class)->create();

        $user = factory(User::class)->create([
            'introduction' => 'test', 
            'photo_id' => $photoProfile->id,
            'gender' => 'male',
            'marital_status' => 'Belum Kawin',
            'job' => 'karyawan'
        ]);

        $photoIdentifier = factory(UserMedia::class)->create([
            'user_id' => $user->id,
            'description' => 'photo_identity'
        ]);

        $photoSelfie = factory(UserMedia::class)->create([
            'user_id' => $user->id,
            'description' => 'selfie_identity'
        ]);

        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $actual = $this->transformer->transform($booking);
        $expected = [
            'id' => $user->id,
            'name' => $user->name,
            'phone_number' => $user->phone_number,
            'email' => $user->email,
            'about_tenant' => $user->introduction,
            'photo_profile' => [
                'small' => $photoProfile->getMediaUrl()['small'],
                'medium' => $photoProfile->getMediaUrl()['medium'],
                'large' => $photoProfile->getMediaUrl()['large'],
            ],
            'photo_identifier' => [
                'small' => $photoIdentifier->getMediaUrl()['small'],
                'medium' => $photoIdentifier->getMediaUrl()['medium'],
                'large' => $photoIdentifier->getMediaUrl()['large'],
            ],
            'photo_selfie' => [
                'small' => $photoSelfie->getMediaUrl()['small'],
                'medium' => $photoSelfie->getMediaUrl()['medium'],
                'large' => $photoSelfie->getMediaUrl()['large'],
            ],
            'gender' => $user->gender,
            'marital_status' => $user->marital_status,
            'occupation' => ucfirst($user->job),
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithoutPhoto(): void
    {
        $user = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['user_id' => $user->id]);

        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $actual = $this->transformer->transform($booking);
        $expected = [
            'id' => $user->id,
            'name' => $user->name,
            'phone_number' => $user->phone_number,
            'email' => $user->email,
            'about_tenant' => $user->introduction,
            'photo_profile' => [],
            'photo_identifier' => [],
            'photo_selfie' => [],
            'gender' => $user->gender,
            'marital_status' => $user->marital_status,
            'occupation' => $user->job,
        ];

        $this->assertEquals($expected, $actual);
    }
}