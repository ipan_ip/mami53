<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingPayment;
use App\Entities\Booking\BookingUser;
use App\Entities\Premium\Bank;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingPaymentDetailTransformer;

class BookingPaymentDetailTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingPaymentDetailTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingInvoicesCanBeTransform(): void
    {
        // prepare data
        $bankEntity = factory(Bank::class)->create();
        $bookingUserEntity = factory(BookingUser::class)->create();
        $bookingPaymentEntity = factory(BookingPayment::class)->create([
            'booking_user_id' => $bookingUserEntity->id,
            'bank_account_id' => $bankEntity->id
        ]);
        // run test
        $transform = $this->transform->transform($bookingPaymentEntity);
        $arrayKeys = [
            'id',
            'booking_code',
            'bank_destination',
            'bank_source',
            'payment_type',
            'total_payment',
            'payment_date',
            'confirmation_date'
        ];
        foreach ($arrayKeys as $item) $this->arrayHasKey($item, $transform);
    }
}