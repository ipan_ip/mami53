<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserFlashSale;
use App\Entities\FlashSale\FlashSale;
use App\Test\MamiKosTestCase;

class BookingUserFlashSaleTransformerTest extends MamiKosTestCase
{
    
    public function testBookingUserFlashSaleCanBeTransform()
    {
        // prepare data
        $bookingUserEntity = factory(BookingUser::class)->create([
            'price' => 500000,
            'mamikos_discount_value' => 50000,
            'owner_discount_value' => 50000
        ]);
        $flashSaleEntity = factory(FlashSale::class)->create();
        $bookingUserFlashSaleEntity = factory(BookingUserFlashSale::class)->create([
            'booking_user_id' => $bookingUserEntity->id,
            'flash_sale_id' => $flashSaleEntity->id
        ]);

        // total discount
        $totalDiscount = $bookingUserEntity->mamikos_discount_value + $bookingUserEntity->owner_discount_value;

        // run test
        $transform = (new BookingUserFlashSaleTransformer)->transform($bookingUserFlashSaleEntity);
        $this->assertEquals($bookingUserEntity->price, $transform['rent_price']);
        $this->assertEquals($totalDiscount, $transform['discount']);
        $this->assertArrayHasKey('rent_price', $transform);
        $this->assertArrayHasKey('discount', $transform);
        $this->assertArrayHasKey('price_after_discount', $transform);
        $this->assertArrayHasKey('percentage', $transform);
    }
}
