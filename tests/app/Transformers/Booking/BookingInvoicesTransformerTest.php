<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingInvoicesTransformer;
use Carbon\Carbon;

class BookingInvoicesTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingInvoicesTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingInvoicesCanBeTransform(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $invoiceEntity = factory(MamipayInvoice::class)->create([
            'scheduled_date' => Carbon::now(),
            'paid_at' => Carbon::now(),
            'status' => MamipayInvoice::STATUS_PAID,
            'contract_id' => $contractEntity
        ]);

        // run test
        $transform = $this->transform->transform($invoiceEntity);
        foreach ($this->transformKeys() as $item) $this->arrayHasKey($item, $transform);
    }

    public function testBookingInvoicesCanBeTransformCheckScheduledIsMinus(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $invoiceEntity = factory(MamipayInvoice::class)->create([
            'scheduled_date' => Carbon::now()->addDays(-2),
            'paid_at' => Carbon::now(),
            'status' => MamipayInvoice::STATUS_PAID,
            'contract_id' => $contractEntity
        ]);

        // run test
        $transform = $this->transform->transform($invoiceEntity);
        foreach ($this->transformKeys() as $item) $this->arrayHasKey($item, $transform);
    }

    private function transformKeys(): array
    {
        return [
            '_id',
            'name',
            'invoice_number',
            'shortlink',
            'scheduled_date',
            'paid_at',
            'amount',
            'amount_string',
            'status',
            'late_payment',
            'status_payment',
            'manual_reminder',
            'last_payment'
        ];
    }
}