<?php

namespace Test\App\Transformers\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Repositories\Booking\BookingDesignerPriceComponentRepository;
use App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDesignerListTransformer;
use App\Transformers\Booking\BookingDraftPriceTransformer;

class BookingDesignerListTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(BookingDesignerListTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBookingDesignerListCanBeTransform(): void
    {
        // prepare data
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'type' => BookingDesigner::BOOKING_TYPE_MONTHLY
        ]);
        factory(BookingDesignerPriceComponent::class)->create([
            'booking_designer_id' => $bookingDesigner->id,
            'price_name' => 'base'
        ]);
        // run test
        $transform = $this->transform->transform($bookingDesigner);
        $assertHasKey = [
            'id',
            'room_id',
            'name',
            'type',
            'description',
            'max_guest',
            'available_room',
            'minimum_stay',
            'guaranteed',
            'price_unit',
            'prices'
        ];
        foreach ($assertHasKey as $item) $this->arrayHasKey($item, $transform);
    }
}