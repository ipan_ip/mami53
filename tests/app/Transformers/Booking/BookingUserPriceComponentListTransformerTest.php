<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserPriceComponent;
use App\Test\MamiKosTestCase;

class BookingUserPriceComponentListTransformerTest extends MamiKosTestCase
{
    
    public function testTransformerReturnNull()
    {
        $priceComponent = new BookingUserPriceComponent();
        // run test
        $transform = (new BookingUserPriceComponentListTransformer())->transform($priceComponent);
        $this->assertIsArray($transform);
        $this->assertNull($transform['price_name']);
        $this->assertNull($transform['price_label']);
        $this->assertEquals((float) 0, $transform['price_total']);
        $this->assertEquals($this->numberFormat(), $transform['price_total_string']);
    }

    public function testTransformerReturnSuccess()
    {
        $priceComponent = factory(BookingUserPriceComponent::class)->create([
            'price_unit_regular' => 3500000,
            'price_unit' => 3500000,
            'quantity' => 1,
            'price_total' => 3500000
        ]);
        // run test
        $transform = (new BookingUserPriceComponentListTransformer())->transform($priceComponent);
        $this->assertIsArray($transform);
        $this->assertEquals($priceComponent->price_label, $transform['price_name']);
        $this->assertEquals($priceComponent->price_label, $transform['price_label']);
        $this->assertEquals((float) $priceComponent->price_total, $transform['price_total']);
        $this->assertEquals($this->numberFormat($priceComponent->price_total), $transform['price_total_string']);
    }

    private function numberFormat(int $price = 0): string
    {
        return 'Rp. ' . number_format($price, 0, ',', '.');
    }
}