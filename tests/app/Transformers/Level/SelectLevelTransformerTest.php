<?php

namespace App\Transformers\Level;

use App\Entities\Level\PropertyLevel;
use App\Test\MamiKosTestCase;

class SelectLevelTransformerTest extends MamiKosTestCase
{
    /**
     *  Instance of transformer
     *
     *  @var SelectLevelTransformer
     */
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(SelectLevelTransformer::class);
    }

    public function testTransform(): void
    {
        $level = factory(PropertyLevel::class)->create();
        $actual = $this->transformer->transform($level);

        $this->assertEquals(
            [
                'id' => $level->id,
                'name' => $level->name
            ],
            $actual
        );
    }
}
