<?php

namespace App\Transformers\Consultant\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantMapping;
use App\Test\MamiKosTestCase;

class AdminBSETransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantMapping::class)->create([
            'area_city' => 'Jakarta',
            'consultant_id' => $consultant->id
        ]);
        factory(ConsultantMapping::class)->create([
            'area_city' => 'Surabaya',
            'consultant_id' => $consultant->id
        ]);

        $consultant->load('mapping');

        $transformer = new AdminBSETransformer();
        $actual = $transformer->transform($consultant);
        $expected = [
            'id' => $consultant->id,
            'name' => $consultant->name,
            'area_city' => ['Jakarta', 'Surabaya']
        ];

        $this->assertEquals($expected, $actual);
    }
}
