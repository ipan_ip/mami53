<?php

namespace App\Transformers\Consultant\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Test\MamiKosTestCase;

class SalesMotionListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = new SalesMotionListTransformer();
    }

    public function testTransform(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $consultant->id
        ]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN
        ]);

        $actual = $this->transformer->transform($consultant);
        $this->assertEquals(
            [
                'id' => $consultant->id,
                'is_assigned' => true,
                'name' => $consultant->name,
                'role' => [ConsultantRole::ADMIN]
            ],
            $actual
        );
    }

    public function testTransformWithConsultantWithoutRole(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $consultant->id
        ]);

        $actual = $this->transformer->transform($consultant);
        $this->assertEquals(
            [
                'id' => $consultant->id,
                'is_assigned' => true,
                'name' => $consultant->name,
                'role' => []
            ],
            $actual
        );
    }

    public function testTransformWithConsultantWithMultipleRole(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $consultant->id
        ]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN
        ]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::SUPPLY
        ]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::DEMAND
        ]);

        $actual = $this->transformer->transform($consultant);
        $this->assertEquals(
            [
                'id' => $consultant->id,
                'is_assigned' => true,
                'name' => $consultant->name,
                'role' => [ConsultantRole::ADMIN, ConsultantRole::SUPPLY, ConsultantRole::DEMAND]
            ],
            $actual
        );
    }

    public function testTransformWithConsultantNotAssignedToSalesMotion(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN
        ]);

        $actual = $this->transformer->transform($consultant);
        $this->assertEquals(
            [
                'id' => $consultant->id,
                'is_assigned' => false,
                'name' => $consultant->name,
                'role' => [ConsultantRole::ADMIN]
            ],
            $actual
        );
    }
}
