<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;

class SalesMotionAdminDetailTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $photo = factory(Media::class)->create();
        $salesMotion = factory(SalesMotion::class)->create(['media_id' => $photo->id]);
        $transformer = new SalesMotionAdminDetailTransformer();
        $actual = $transformer->transform($salesMotion);

        $expected = [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'requirement' => $salesMotion->requirement,
            'benefit' => $salesMotion->benefit,
            'max_participant' => $salesMotion->max_participant,
            'link' => $salesMotion->url,
            'photos' => [
                'small' => $photo->getMediaUrl()['small'],
                'medium' => $photo->getMediaUrl()['medium'],
                'large' => $photo->getMediaUrl()['large'],
            ],
            'media_id' => $salesMotion->media_id,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'created_at' => $salesMotion->created_at->toString(),
            'updated_at' => $salesMotion->updated_at->toString(),
            'created_by' => $salesMotion->createdBy->name,
            'updated_by' => $salesMotion->lastUpdatedBy->name,
            'is_active' => $salesMotion->is_active
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithNoPhoto(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['media_id' => null]);
        $transformer = new SalesMotionAdminDetailTransformer();
        $actual = $transformer->transform($salesMotion);

        $expected = [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'requirement' => $salesMotion->requirement,
            'benefit' => $salesMotion->benefit,
            'max_participant' => $salesMotion->max_participant,
            'link' => $salesMotion->url,
            'photos' => [
                'small' => null,
                'medium' => null,
                'large' => null,
            ],
            'media_id' => null,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'created_at' => $salesMotion->created_at->toString(),
            'updated_at' => $salesMotion->updated_at->toString(),
            'created_by' => $salesMotion->createdBy->name,
            'updated_by' => $salesMotion->lastUpdatedBy->name,
            'is_active' => $salesMotion->is_active
        ];

        $this->assertEquals($expected, $actual);
    }
}
