<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Test\MamiKosTestCase;
use App\User;

class SalesMotionAdminListTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $user = factory(User::class)->create();
        $salesMotion = factory(SalesMotion::class)->create(['updated_by' => $user->id]);
        $expected = [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => $user->name,
            'is_active' => $salesMotion->is_active
        ];

        $transformer = new SalesMotionAdminListTransformer();
        $actual = $transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithNullUpdatedBy(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['updated_by' => null]);
        $expected = [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => null,
            'is_active' => $salesMotion->is_active
        ];

        $transformer = new SalesMotionAdminListTransformer();
        $actual = $transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }
}
