<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ProgressListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        $this->transformer = new ProgressListTransformer();
    }

    public function testTransformWithProperty(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id
        ]);
        $actual = $this->transformer->transform($progress);

        $this->assertEquals(
            [
                'status' => $progress->status,
                'name' => $room->name,
                'type' => 'property',
                'updated_at' => $progress->updated_at->toString()
            ],
            $actual
        );
    }

    public function testTransformerWithContract(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id
        ]);
        $actual = $this->transformer->transform($progress);

        $this->assertEquals(
            [
                'status' => $progress->status,
                'name' => $tenant->name,
                'type' => 'contract',
                'updated_at' => $progress->updated_at->toString()
            ],
            $actual
        );
    }

    public function testTransformWithPotentialTenant(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
            'progressable_id' => $tenant->id
        ]);
        $actual = $this->transformer->transform($progress);

        $this->assertEquals(
            [
                'status' => $progress->status,
                'name' => $tenant->name,
                'type' => 'dbet',
                'updated_at' => $progress->updated_at->toString()
            ],
            $actual
        );
    }
}
