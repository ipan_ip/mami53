<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class SalesMotionAssigneeAdminListTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $consultant = factory(Consultant::class)->create();
        $mapping = factory(SalesMotionAssignee::class)->create([
            'consultant_id' => $consultant->id
        ]);
        $transformer = new SalesMotionAssigneeAdminListTransformer();

        $expected = [
            'consultant' => $consultant->name
        ];
        $actual = $transformer->transform($mapping);

        $this->assertEquals($expected, $actual);
    }
}
