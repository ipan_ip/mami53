<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ProgressDetailTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transformer = $this->app->make(ProgressDetailTransformer::class);
    }

    public function testTransformWithKost(): void
    {
        $property = factory(Room::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'media_id' => 0,
            'progressable_id' => $property->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
            'created_at' => null,
            'updated_at' => null
        ]);

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $property->name,
            'type' => 'property',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => null,
            'photo' => null,
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => null,
            'updated_at' => null,
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithDBET(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'media_id' => 0,
            'progressable_id' => $tenant->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
            'created_at' => null,
            'updated_at' => null
        ]);

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $tenant->name,
            'type' => 'dbet',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => null,
            'photo' => null,
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => null,
            'updated_at' => null,
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithContract(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(SalesMotionProgress::class)->create([
            'media_id' => 0,
            'progressable_id' => $contract->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
            'created_at' => null,
            'updated_at' => null
        ]);

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $tenant->name,
            'type' => 'contract',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => null,
            'photo' => null,
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => null,
            'updated_at' => null,
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithSalesMotion(): void
    {
        $property = factory(Room::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'media_id' => 0,
            'progressable_id' => $property->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
            'created_at' => null,
            'updated_at' => null
        ]);

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $property->name,
            'type' => 'property',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => $salesMotion->created_by_division,
            'photo' => null,
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => null,
            'updated_at' => null,
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithPhoto(): void
    {
        $property = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'media_id' => $photo->id,
            'progressable_id' => $property->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
            'created_at' => null,
            'updated_at' => null
        ]);
        $urls = $photo->getMediaUrl();

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $property->name,
            'type' => 'property',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => null,
            'photo' => [
                'small' => $urls['small'],
                'medium' => $urls['medium'],
                'large' => $urls['large']
            ],
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => null,
            'updated_at' => null,
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }

    public function testTransform(): void
    {
        $property = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $progress = factory(SalesMotionProgress::class)->create([
            'media_id' => $photo->id,
            'progressable_id' => $property->id,
            'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
        ]);
        $urls = $photo->getMediaUrl();

        $expected = [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $property->name,
            'type' => 'property',
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => null,
            'photo' => [
                'small' => $urls['small'],
                'medium' => $urls['medium'],
                'large' => $urls['large']
            ],
            'documents' => $progress->consultant_documents->map->only(['id', 'file_name']),
            'created_at' => $progress->created_at->toString(),
            'updated_at' => $progress->updated_at->toString(),
        ];

        $actual = $this->transformer->transform($progress);

        $this->assertEquals($expected, $actual);
    }
}
