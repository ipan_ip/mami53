<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class DataAssociatedTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(DataAssociatedTransformer::class);
    }

    public function testTransform(): void
    {
        $room = factory(Room::class)->create();
        $actual = $this->transformer->transform($room);

        $expected = [
            'id' => $room->id,
            'name' => $room->name
        ];

        $this->assertEquals($expected, $actual);
    }
}
