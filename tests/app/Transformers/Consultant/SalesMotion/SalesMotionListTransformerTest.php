<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Test\MamiKosTestCase;

class SalesMotionListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(SalesMotionListTransformer::class);
    }

    public function testTransform(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['updated_by' => null]);

        $expected = [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'is_active' => $salesMotion->is_active,
            'progress_status_deal' => 0,
            'progress_status_interested' => 0,
            'progress_status_not_interested' => 0,
            'end_date' => $salesMotion->end_date,
            'created_at' => $salesMotion->created_at,
            'updated_at' => $salesMotion->updated_at,
            'updated_by' => null,
        ];

        $actual = $this->transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithCount(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['updated_by' => null]);

        factory(SalesMotionProgress::class)->create([
            'status' => SalesMotionProgress::STATUS_DEAL,
            'consultant_sales_motion_id' => $salesMotion->id
        ]);

        factory(SalesMotionProgress::class, 2)->create([
            'status' => SalesMotionProgress::STATUS_INTERESTED,
            'consultant_sales_motion_id' => $salesMotion->id
        ]);

        factory(SalesMotionProgress::class, 3)->create([
            'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
            'consultant_sales_motion_id' => $salesMotion->id
        ]);

        $salesMotion = SalesMotion::withCount(['progress_status_deal', 'progress_status_interested', 'progress_status_not_interested'])->first();

        $expected = [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'is_active' => $salesMotion->is_active,
            'progress_status_deal' => 1,
            'progress_status_interested' => 2,
            'progress_status_not_interested' => 3,
            'end_date' => $salesMotion->end_date,
            'created_at' => $salesMotion->created_at,
            'updated_at' => $salesMotion->updated_at,
            'updated_by' => null,
        ];

        $actual = $this->transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }
}
