<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\User;

class SalesMotionDetailTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(SalesMotionDetailTransformer::class);
    }

    public function testTransformWithoutUpdatedByAndPhoto(): void
    {
        $salesMotion = factory(SalesMotion::class)->create([
            'media_id' => null,
            'updated_by' => null
        ]);

        $expected = [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'is_active' => $salesMotion->is_active,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'benefit' => $salesMotion->benefit,
            'requirement' => $salesMotion->requirement,
            'max_participant' => $salesMotion->max_participant,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => null,
            'url' => $salesMotion->url,
            'photo' => null
        ];

        $actual = $this->transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }

    public function testTransform(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $urls = [
            'small' => $urls['small'],
            'medium' => $urls['medium'],
            'large' => $urls['large']
        ];

        $user = factory(User::class)->create();

        $salesMotion = factory(SalesMotion::class)->create([
            'media_id' => $photo->id,
            'updated_by' => $user->id
        ]);

        $expected = [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'is_active' => $salesMotion->is_active,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'benefit' => $salesMotion->benefit,
            'requirement' => $salesMotion->requirement,
            'max_participant' => $salesMotion->max_participant,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => $user->name,
            'url' => $salesMotion->url,
            'photo' => $urls
        ];

        $actual = $this->transformer->transform($salesMotion);

        $this->assertEquals($expected, $actual);
    }
}
