<?php

namespace App\Transformers\Consultant\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Test\MamiKosTestCase;
use App\User;

class AdminListTransformerTest extends MamiKosTestCase
{
    
    protected $transformer;

    protected function setUp()
    {
        parent::setUp();
        $this->transformer = new AdminListTransformer();
    }

    /**
     * @group UG
     * @group UG-4226
     */
    public function testTransform(): void
    {
        $owner = factory(PotentialOwner::class)->create(['updated_by' => null]);
        $actual = $this->transformer->transform($owner);
        $expected = [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'is_hostile' => false,
            'total_property' => $owner->total_property,
            'followup_status' => 'new',
            'bbk_status' => 'non-bbk',
            'created_at' => $owner->created_at->toString(),
            'updated_at' => $owner->updated_at->toString(),
            'updated_by' => null
        ];

        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4226
     */
    public function testTransformWithUpdatedOwner(): void
    {
        $user = factory(User::class)->create(['hostility' => 1]);
        $owner = factory(PotentialOwner::class)->create([
            'user_id' => $user->id,
            'updated_by' => factory(User::class)->create()->id
        ]);
        $actual = $this->transformer->transform($owner);
        $expected = [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'is_hostile' => true,
            'followup_status' => 'new',
            'created_at' => $owner->created_at->toString(),
            'updated_at' => $owner->updated_at->toString(),
            'updated_by' => $owner->last_updated_by->name,
            'total_property' => $owner->total_property,
            'bbk_status' => 'non-bbk',
            'followup_status' => 'new'
        ];

        $this->assertEquals($expected, $actual);
    }
}
