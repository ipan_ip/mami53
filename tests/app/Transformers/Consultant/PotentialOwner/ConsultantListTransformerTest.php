<?php

namespace App\Transformers\Consultant\PotentialOwner;


use App\User;
use App\Test\MamiKosTestCase;
use League\Fractal\TransformerAbstract;
use App\Entities\Consultant\PotentialOwner;

class ConsultantListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp()
    {
        parent::setUp();
        $this->transformer = new ConsultantListTransformer();
    }

    public function testTransform(): void
    {
        $user = factory(User::class)->create();
        $owner = factory(PotentialOwner::class)->create()->toArray();
        $owner['last_updater_name'] = $user->name;

        $expected = [
            'id' => $owner['id'],
            'name' => $owner['name'],
            'phone_number' => $owner['phone_number'],
            'email' => $owner['email'],
            'date_to_visit' => $owner['date_to_visit'],
            'total_property' => $owner['total_property'],
            'total_room' => $owner['total_room'],
            'bbk_status' => $owner['bbk_status'],
            'followup_status' => $owner['followup_status'],
            'updated_at' => $owner['updated_at'],
            'updated_by' => $owner['last_updater_name']
        ];

        $owner = ['_source' => $owner];
        $this->assertEquals($expected, $this->transformer->transform($owner));
    }
}