<?php

namespace App\Transformers\Consultant\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Test\MamiKosTestCase;
use App\User;

class ConsultantDetailTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp()
    {
        parent::setUp();
        $this->transformer = new ConsultantDetailTransformer();
    }

    public function testTransform(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'created_by' => factory(User::class),
            'updated_by' => null
        ]);
        $actual = $this->transformer->transform($owner);
        $expected = [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'email' => !is_null($owner->email) ? $owner->email : '-',
            'total_property' => $owner->total_property,
            'total_room' => $owner->total_room,
            'notes' => $owner->remark,
            'date_to_visit' => $owner->date_to_visit,
            'bbk_status' => $owner->bbk_status,
            'followup_status' => $owner->followup_status,
            'created_at' => $owner->created_at->toDateTimeString(),
            'created_by' => $owner->created_by_user->name,
            'updated_at' => $owner->updated_at->toDateTimeString(),
            'updated_by' => null
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithUpdatedOwner(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'created_by' => factory(User::class),
            'updated_by' => factory(User::class)
        ]);
        $actual = $this->transformer->transform($owner);
        $expected = [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'email' => !is_null($owner->email) ? $owner->email : '-',
            'total_property' => $owner->total_property,
            'total_room' => $owner->total_room,
            'notes' => $owner->remark,
            'date_to_visit' => $owner->date_to_visit,
            'bbk_status' => $owner->bbk_status,
            'followup_status' => $owner->followup_status,
            'created_at' => $owner->created_at->toDateTimeString(),
            'created_by' => $owner->created_by_user->name,
            'updated_at' => $owner->updated_at->toDateTimeString(),
            'updated_by' => $owner->last_updated_by->name
        ];

        $this->assertEquals($expected, $actual);
    }
}
