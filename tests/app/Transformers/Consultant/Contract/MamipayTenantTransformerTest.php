<?php

namespace App\Transformers\Consultant\Contract;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayTenant;
use App\Http\Helpers\PhoneNumberHelper;
use App\Test\MamiKosTestCase;

class MamipayTenantTransformerTest extends MamiKosTestCase
{
    // 'tenant' => [
    //     'id' => $tenant->id,
    //     'name' => $tenant->name,
    //     'photo' => $tenantPhoto ? $tenantPhoto->cached_urls : null,
    //     'photo_id' => $tenantPhoto ? $tenantPhoto->id : null,
    //     'photo_identifier' => $tenantPhotoId ? $tenantPhotoId->cached_urls : null,
    //     'photo_identifier_id' => $tenantPhotoId ? $tenantPhotoId->id : null,
    //     'photo_document' => $tenantPhotoDocument ? $tenantPhotoDocument->cached_urls : null,
    //     'photo_document_id' => $tenantPhotoDocument ? $tenantPhotoDocument->id : null,
    //     'photo_document_name' => $tenantPhotoDocument ? $tenantPhotoDocument->description : null,
    //     'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
    //     'room_number' => null,
    //     'email' => $tenant->email,
    //     'occupation' => $tenant->occupation,
    //     'gender' => $tenant->gender_formatted,
    //     'gender_value' => $tenant->gender,
    //     'marital_status' => $tenant->marital_status,
    //     'parent_name' => $tenant->parent_name,
    //     'parent_phone_number' => $tenant->parent_phone_number,
    //     'price' => !is_null($potentialTenant) ? $potentialTenant->price : null,
    //     'due_date' => !is_null($potentialTenant) ? $potentialTenant->due_date : null,
    // ]

    public function testTransform(): void
    {
        $tenantPhotoDocument = factory(MamipayMedia::class)->create();
        $tenantPhotoId = factory(MamipayMedia::class)->create();
        $tenantPhoto = factory(MamipayMedia::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'gender' => 'male',
            'photo_document_id' => $tenantPhotoDocument->id,
            'photo_identifier_id' => $tenantPhotoId->id,
            'photo_id' => $tenantPhoto->id
        ]);
        $potentialTenant = factory(PotentialTenant::class)->create([
            'phone_number' => PhoneNumberHelper::sanitizeNumber($tenant->phone_number)
        ]);

        $transformer = new MamipayTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => $tenantPhoto->cached_urls,
                'photo_id' => $tenantPhoto->id,
                'photo_identifier' => $tenantPhotoId->cached_urls,
                'photo_identifier_id' => $tenantPhotoId->id,
                'photo_document' => $tenantPhotoDocument->cached_urls,
                'photo_document_id' => $tenantPhotoDocument->id,
                'photo_document_name' => $tenantPhotoDocument->description,
                'phone_number' => $tenant->phone_number,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => $potentialTenant->price,
                'due_date' => $potentialTenant->due_date,
            ]
        ];

        $this->assertEquals($expected, $transformer->transform($tenant));
    }

    public function testTransformWithoutPotentialTenant(): void
    {
        $tenantPhotoDocument = factory(MamipayMedia::class)->create();
        $tenantPhotoId = factory(MamipayMedia::class)->create();
        $tenantPhoto = factory(MamipayMedia::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'gender' => 'male',
            'photo_document_id' => $tenantPhotoDocument->id,
            'photo_identifier_id' => $tenantPhotoId->id,
            'photo_id' => $tenantPhoto->id
        ]);

        $transformer = new MamipayTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => $tenantPhoto->cached_urls,
                'photo_id' => $tenantPhoto->id,
                'photo_identifier' => $tenantPhotoId->cached_urls,
                'photo_identifier_id' => $tenantPhotoId->id,
                'photo_document' => $tenantPhotoDocument->cached_urls,
                'photo_document_id' => $tenantPhotoDocument->id,
                'photo_document_name' => $tenantPhotoDocument->description,
                'phone_number' => $tenant->phone_number,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => null,
                'due_date' => null,
            ]
        ];

        $this->assertEquals($expected, $transformer->transform($tenant));
    }

    public function testTransformWithoutPhoto(): void
    {
        $tenantPhotoDocument = factory(MamipayMedia::class)->create();
        $tenantPhotoId = factory(MamipayMedia::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'gender' => 'male',
            'photo_document_id' => $tenantPhotoDocument->id,
            'photo_identifier_id' => $tenantPhotoId->id,
        ]);
        $potentialTenant = factory(PotentialTenant::class)->create([
            'phone_number' => PhoneNumberHelper::sanitizeNumber($tenant->phone_number)
        ]);

        $transformer = new MamipayTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => null,
                'photo_id' => null,
                'photo_identifier' => $tenantPhotoId->cached_urls,
                'photo_identifier_id' => $tenantPhotoId->id,
                'photo_document' => $tenantPhotoDocument->cached_urls,
                'photo_document_id' => $tenantPhotoDocument->id,
                'photo_document_name' => $tenantPhotoDocument->description,
                'phone_number' => $tenant->phone_number,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => $potentialTenant->price,
                'due_date' => $potentialTenant->due_date,
            ]
        ];

        $this->assertEquals($expected, $transformer->transform($tenant));
    }

    public function testTransformWithoutPhotoDocument(): void
    {
        $tenantPhotoId = factory(MamipayMedia::class)->create();
        $tenantPhoto = factory(MamipayMedia::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'gender' => 'male',
            'photo_identifier_id' => $tenantPhotoId->id,
            'photo_id' => $tenantPhoto->id
        ]);
        $potentialTenant = factory(PotentialTenant::class)->create([
            'phone_number' => PhoneNumberHelper::sanitizeNumber($tenant->phone_number)
        ]);

        $transformer = new MamipayTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => $tenantPhoto->cached_urls,
                'photo_id' => $tenantPhoto->id,
                'photo_identifier' => $tenantPhotoId->cached_urls,
                'photo_identifier_id' => $tenantPhotoId->id,
                'photo_document' => null,
                'photo_document_id' => null,
                'photo_document_name' => null,
                'phone_number' => $tenant->phone_number,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => $potentialTenant->price,
                'due_date' => $potentialTenant->due_date,
            ]
        ];

        $this->assertEquals($expected, $transformer->transform($tenant));
    }

    public function testTransformWithoutPhotoIdentifier(): void
    {
        $tenantPhotoDocument = factory(MamipayMedia::class)->create();
        $tenantPhoto = factory(MamipayMedia::class)->create();
        $tenant = factory(MamipayTenant::class)->create([
            'gender' => 'male',
            'photo_document_id' => $tenantPhotoDocument->id,
            'photo_id' => $tenantPhoto->id
        ]);
        $potentialTenant = factory(PotentialTenant::class)->create([
            'phone_number' => PhoneNumberHelper::sanitizeNumber($tenant->phone_number)
        ]);

        $transformer = new MamipayTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => $tenantPhoto->cached_urls,
                'photo_id' => $tenantPhoto->id,
                'photo_identifier' => null,
                'photo_identifier_id' => null,
                'photo_document' => $tenantPhotoDocument->cached_urls,
                'photo_document_id' => $tenantPhotoDocument->id,
                'photo_document_name' => $tenantPhotoDocument->description,
                'phone_number' => $tenant->phone_number,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => $potentialTenant->price,
                'due_date' => $potentialTenant->due_date,
            ]
        ];

        $this->assertEquals($expected, $transformer->transform($tenant));
    }
}
