<?php

namespace App\Transformers\Consultant\Contract;

use App\Entities\Consultant\PotentialTenant;
use App\Test\MamiKosTestCase;

class PotentialTenantTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $transformer = new PotentialTenantTransformer();

        $expected = [
            'tenant' => [
                'id' => null,
                'name' => $tenant->name,
                'photo' => null,
                'photo_id' => null,
                'photo_identifier' => null,
                'photo_identifier_id' => null,
                'photo_document' => null,
                'photo_document_id' => null,
                'photo_document_name' => null,
                'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->job_information,
                'gender' => $tenant->gender,
                'gender_value' => $tenant->gender,
                'marital_status' => null,
                'parent_name' => null,
                'parent_phone_number' => null,
                'price' => $tenant->price,
                'due_date' => $tenant->due_date
            ]
        ];

        $this->assertEquals(
            $expected,
            $transformer->transform($tenant)
        );
    }
}
