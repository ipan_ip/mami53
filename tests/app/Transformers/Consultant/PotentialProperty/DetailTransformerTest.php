<?php

namespace App\Transformers\Consultant\PotentialProperty;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\PotentialProperty\DetailTransformer;

class DetailTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        $this->transformer = new DetailTransformer();
    }

    public function testTransformWithoutPhoto(): void
    {
        $property = factory(PotentialProperty::class)->create(['media_id' => null]);
        $actual = $this->transformer->transform($property);
        $expected = [
            'id' => $property->id,
            'name' => $property->name,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'photo' => [],
            'total_room' => $property->total_room,
            'media_id' => $property->media_id,
            'product_offered' => $property->extractOfferedProduct(),
            'priority' => ($property->is_priority ? 'high' : 'low'),
            'owner_name' => (!is_null($property->potential_owner) ? $property->potential_owner->name : null),
            'owner_phone' => (!is_null($property->potential_owner) ? $property->potential_owner->phone_number : null),
            'bbk_status' => $property->bbk_status,
            'followup_status' => $property->followup_status,
            'notes' => $property->remark,
            'created_at' => !is_null($property->created_at) ? $property->created_at->toDateTimeString() : null,
            'created_by' => is_null($property->first_created_by) ? null : $property->first_created_by->name,
            'updated_at' => !is_null($property->updated_at) ? $property->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($property->last_updated_by) ? null : $property->last_updated_by->name
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithPhoto(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $property = factory(PotentialProperty::class)->create(['media_id' => $photo->id]);
        $actual = $this->transformer->transform($property);
        $expected = [
            'id' => $property->id,
            'name' => $property->name,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'photo' => [
                'small' => $urls['small'],
                'medium' => $urls['medium'],
                'large' => $urls['large']
            ],
            'total_room' => $property->total_room,
            'media_id' => $property->media_id,
            'product_offered' => $property->extractOfferedProduct(),
            'priority' => ($property->is_priority ? 'high' : 'low'),
            'owner_name' => (!is_null($property->potential_owner) ? $property->potential_owner->name : null),
            'owner_phone' => (!is_null($property->potential_owner) ? $property->potential_owner->phone_number : null),
            'bbk_status' => $property->bbk_status,
            'followup_status' => $property->followup_status,
            'notes' => $property->remark,
            'created_at' => !is_null($property->created_at) ? $property->created_at->toDateTimeString() : null,
            'created_by' => is_null($property->first_created_by) ? null : $property->first_created_by->name,
            'updated_at' => !is_null($property->updated_at) ? $property->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($property->last_updated_by) ? null : $property->last_updated_by->name
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithoutOwner(): void
    {
        $property = factory(PotentialProperty::class)->create(['media_id' => null]);
        $actual = $this->transformer->transform($property);
        $expected = [
            'id' => $property->id,
            'name' => $property->name,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'photo' => [],
            'total_room' => $property->total_room,
            'media_id' => $property->media_id,
            'product_offered' => $property->extractOfferedProduct(),
            'priority' => ($property->is_priority ? 'high' : 'low'),
            'owner_name' => null,
            'owner_phone' => null,
            'bbk_status' => $property->bbk_status,
            'followup_status' => $property->followup_status,
            'notes' => $property->remark,
            'created_at' => !is_null($property->created_at) ? $property->created_at->toDateTimeString() : null,
            'created_by' => is_null($property->first_created_by) ? null : $property->first_created_by->name,
            'updated_at' => !is_null($property->updated_at) ? $property->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($property->last_updated_by) ? null : $property->last_updated_by->name
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithOwner(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create([
            'media_id' => null,
            'consultant_potential_owner_id' => $owner->id
        ]);
        $actual = $this->transformer->transform($property);
        $expected = [
            'id' => $property->id,
            'name' => $property->name,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'photo' => [],
            'total_room' => $property->total_room,
            'media_id' => $property->media_id,
            'product_offered' => $property->extractOfferedProduct(),
            'priority' => ($property->is_priority ? 'high' : 'low'),
            'owner_name' => $owner->name,
            'owner_phone' => $owner->phone_number,
            'bbk_status' => $property->bbk_status,
            'followup_status' => $property->followup_status,
            'notes' => $property->remark,
            'created_at' => !is_null($property->created_at) ? $property->created_at->toDateTimeString() : null,
            'created_by' => is_null($property->first_created_by) ? null : $property->first_created_by->name,
            'updated_at' => !is_null($property->updated_at) ? $property->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($property->last_updated_by) ? null : $property->last_updated_by->name
        ];

        $this->assertEquals($expected, $actual);
    }
}
