<?php

namespace App\Transformers\Consultant\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use App\Test\MamiKosTestCase;
use App\User;

class ListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transformer = new ListTransformer();
    }

    public function testTransformWithHighPriority(): void
    {
        $user = factory(User::class)->create();
        $property = factory(PotentialProperty::class)->create(['is_priority' => true])->toArray();
        $property['owner_id'] = $property['consultant_potential_owner_id'];
        $property['creator_name'] = $user->name;
        $property['offered_product'] = ['gp', 'bbk'];

        $expected = [
            'id' => $property['id'],
            'owner_id' => $property['owner_id'],
            'name' => $property['name'],
            'area_city' => $property['area_city'],
            'province' => $property['province'],
            'created_by' => $property['creator_name'],
            'bbk_status' => $property['bbk_status'],
            'followup_status' => $property['followup_status'],
            'product_offered' => $property['offered_product'],
            'priority' => 'high',
            'created_at' => $property['created_at']
        ];

        $property = ['_source' => $property];
        $this->assertEquals($expected, $this->transformer->transform($property));
    }

    public function testTransformWithLowPriority(): void
    {
        $user = factory(User::class)->create();
        $property = factory(PotentialProperty::class)->create(['is_priority' => false])->toArray();
        $property['owner_id'] = $property['consultant_potential_owner_id'];
        $property['creator_name'] = $user->name;
        unset($property['offered_product']);

        $expected = [
            'id' => $property['id'],
            'owner_id' => $property['owner_id'],
            'name' => $property['name'],
            'area_city' => $property['area_city'],
            'province' => $property['province'],
            'created_by' => $property['creator_name'],
            'bbk_status' => $property['bbk_status'],
            'followup_status' => $property['followup_status'],
            'product_offered' => [],
            'priority' => 'low',
            'created_at' => $property['created_at']
        ];

        $property = ['_source' => $property];
        $this->assertEquals($expected, $this->transformer->transform($property));
    }
}
