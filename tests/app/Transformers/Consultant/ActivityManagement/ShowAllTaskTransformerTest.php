<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ShowAllTaskTransformerTest extends MamiKosTestCase
{
    public function testTransformWithoutProgress(): void
    {
        $stage = factory(ActivityForm::class)->create();
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress,
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithBacklogStage(): void
    {
        $stage = new BacklogStage();
        $stage->progress = collect();
        $stage->progress_count = 5;
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress,
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithToDoStage(): void
    {
        $stage = new ToDoStage();
        $stage->progress = collect();
        $stage->progress_count = 5;
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress,
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformShouldTransformTask(): void
    {
        $stage = new ToDoStage();
        $progress = factory(ActivityProgress::class)->create();
        $stage->progress = collect([$progress]);
        $stage->progress_count = 5;
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => collect([
                [
                    "id" => $progress->id,
                    "progressable_id" => null,
                    "name" => null,
                    "progress" => 0
                ]
            ]),
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithZeroStageCount(): void
    {
        $room = factory(Room::class)->create();
        $stage = factory(ActivityForm::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $stage->consultant_activity_funnel_id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id
        ]);

        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress->map(function ($task) {
                return [
                    'id' => $task->id,
                    'progressable_id' => $task->progressable->id,
                    'name' => $task->progressable->name,
                    'progress' => 0
                ];
            }),
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithProgress(): void
    {
        $room = factory(Room::class)->create();
        $stage = factory(ActivityForm::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $stage->consultant_activity_funnel_id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'current_stage' => 1
        ]);

        $transformer = new ShowAllTaskTransformer(5);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress->map(function ($task) {
                return [
                    'id' => $task->id,
                    'progressable_id' => $task->progressable->id,
                    'name' => $task->progressable->name,
                    'progress' => 20
                ];
            }),
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithContractProgress(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $stage = factory(ActivityForm::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $stage->consultant_activity_funnel_id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id,
            'current_stage' => 1
        ]);

        $transformer = new ShowAllTaskTransformer(5);

        $expected = [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'detail' => $stage->getDetail(),
            'tasks' => $stage->progress->map(function ($task) {
                return [
                    'id' => $task->id,
                    'progressable_id' => $task->progressable->id,
                    'name' => $task->progressable->tenant->name,
                    'progress' => 20
                ];
            }),
            'tasks_count' => $stage->progress_count
        ];

        $actual = $transformer->transform($stage);

        $this->assertEquals($expected, $actual);
    }

    public function testTransformTaskWithMamipayContract(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 5,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id,
            'current_stage' => 1
        ]);
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $task->id,
            'progressable_id' => $task->progressable->id,
            'name' => $task->progressable->tenant->name,
            'progress' => 0
        ];

        $this->assertEquals($expected, $transformer->transformTask($task));
    }

    public function testTransformTask(): void
    {
        $room = factory(Room::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 5,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'current_stage' => 1
        ]);
        $transformer = new ShowAllTaskTransformer(0);

        $expected = [
            'id' => $task->id,
            'progressable_id' => $task->progressable->id,
            'name' => $task->progressable->name,
            'progress' => 0
        ];

        $this->assertEquals($expected, $transformer->transformTask($task));
    }

    public function testTransformTaskWithNonZeroCount(): void
    {
        $room = factory(Room::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 5,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'current_stage' => 1
        ]);
        $transformer = new ShowAllTaskTransformer(5);

        $expected = [
            'id' => $task->id,
            'progressable_id' => $task->progressable->id,
            'name' => $task->progressable->name,
            'progress' => 20
        ];

        $this->assertEquals($expected, $transformer->transformTask($task));
    }
}
