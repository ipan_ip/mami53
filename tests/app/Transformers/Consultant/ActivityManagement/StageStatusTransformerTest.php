<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Test\MamiKosTestCase;

class StageStatusTransformerTest extends MamiKosTestCase
{
    public function testTransformWithBacklogStage(): void
    {
        $transformer = new StageStatusTransformer(0);

        $stage = new BacklogStage();
        $actual = $transformer->transform($stage);

        $this->assertEquals(
            [
                'stage' => $stage->getFormattedStage(),
                'completed' => $stage->getIsCompleted(0)
            ],
            $actual
        );
    }

    public function testTransformWithToDoStage(): void
    {
        $transformer = new StageStatusTransformer(1);

        $stage = new ToDoStage();
        $actual = $transformer->transform($stage);

        $this->assertEquals(
            [
                'stage' => $stage->getFormattedStage(),
                'completed' => $stage->getIsCompleted(1)
            ],
            $actual
        );
    }

    public function testTransformWithActivityForm(): void
    {
        $transformer = new StageStatusTransformer(0);

        $stage = factory(ActivityForm::class)->create();
        $actual = $transformer->transform($stage);

        $this->assertEquals(
            [
                'stage' => $stage->getFormattedStage(),
                'completed' => $stage->getIsCompleted(0)
            ],
            $actual
        );
    }
}
