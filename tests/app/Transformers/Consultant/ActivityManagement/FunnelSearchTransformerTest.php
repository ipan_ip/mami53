<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class FunnelSearchTransformerTest extends MamiKosTestCase
{
    public function testTransformWithoutProgress(): void
    {
        $room = factory(Room::class)->create();
        $transformer = new FunnelSearchTransformer(5, 1);

        $actual = $transformer->transform($room);

        $this->assertEquals(
            [
                'id' => null,
                'progressable_id' => $room->id,
                'name' => $room->name,
                'progress' => 0
            ],
            $actual
        );
    }

    public function testTransformWithOtherConsultantProgress(): void
    {
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_id' => 2
        ]);
        $transformer = new FunnelSearchTransformer(5, 1);

        $actual = $transformer->transform($room);

        $this->assertEquals(
            [
                'id' => null,
                'progressable_id' => $room->id,
                'name' => $room->name,
                'progress' => 0
            ],
            $actual
        );
    }

    public function testTransformWithProgress(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_id' => 1,
            'current_stage' => 1
        ]);
        $transformer = new FunnelSearchTransformer(5, 1);

        $actual = $transformer->transform($room);

        $this->assertEquals(
            [
                'id' => $progress->id,
                'progressable_id' => $room->id,
                'name' => $room->name,
                'progress' => 20
            ],
            $actual
        );
    }

    public function testTransformShouldReturnCorrectConsultantProgress(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_id' => 1,
            'current_stage' => 1
        ]);

        factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_id' => 2,
            'current_stage' => 2
        ]);
        $transformer = new FunnelSearchTransformer(5, 1);

        $actual = $transformer->transform($room);

        $this->assertEquals(
            [
                'id' => $progress->id,
                'progressable_id' => $room->id,
                'name' => $room->name,
                'progress' => 20
            ],
            $actual
        );
    }

    public function testTransformWith0TotalStage(): void
    {
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_id' => 1,
            'current_stage' => 1
        ]);
        $transformer = new FunnelSearchTransformer(0, 1);

        $actual = $transformer->transform($room);

        $this->assertEquals(
            [
                'id' => $progress->id,
                'progressable_id' => $room->id,
                'name' => $room->name,
                'progress' => 0
            ],
            $actual
        );
    }
}
