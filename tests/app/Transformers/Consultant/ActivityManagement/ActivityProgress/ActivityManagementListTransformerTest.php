<?php

namespace App\Transformers\Consultant\ActivityManagement\ActivityProgress;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Presenters\Consultant\ActivityManagement\ActivityProgressPresenter;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Test\MamiKosTestCase;

class ActivityManagementListTransformerTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = $this->app->make(ActivityProgressRepository::class);
    }

    private function query()
    {
        return $this->repo
            ->setPresenter(
                new ActivityProgressPresenter(
                    new ActivityManagementListTransformer()
                )
            )
            ->list()
            ->get();
    }

    public function testTransformWithDBET(): void
    {
        $progress = factory(ActivityProgress::class)->states('withDBET', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->query();

        $totalStage = $progress->funnel->total_stage;
        $expected = [
            'data' => [
                [
                    "current_stage" => $progress->current_stage,
                    "data_type" => 'DBET',
                    "funnel_id" => $progress->consultant_activity_funnel_id,
                    "id" => $progress->id,
                    "name" => $progress->progressable->name,
                    "progress_percentage" => $totalStage === 0 ? 0 : (int)($progress->current_stage / $totalStage * 100),
                    "progressable" => [
                        "id" => $progress->progressable->id
                    ],
                    "stage_name" => ($progress->current_stage === 0) ? 'Tugas Aktif' : $form->name,
                    "total_stage" => $totalStage
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithProperty(): void
    {
        $progress = factory(ActivityProgress::class)->states('withProperty', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->query();

        $totalStage = $progress->funnel->total_stage;
        $expected = [
            'data' => [
                [
                    "current_stage" => $progress->current_stage,
                    "data_type" => 'Properti',
                    "funnel_id" => $progress->consultant_activity_funnel_id,
                    "id" => $progress->id,
                    "name" => $progress->progressable->name,
                    "progress_percentage" => $totalStage === 0 ? 0 : (int)($progress->current_stage / $totalStage * 100),
                    "progressable" => [
                        "id" => $progress->progressable->id
                    ],
                    "stage_name" => ($progress->current_stage === 0) ? 'Tugas Aktif' : $form->name,
                    "total_stage" => $totalStage
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithPotentialProperty(): void
    {
        $progress = factory(ActivityProgress::class)->states('withPotentialProperty', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->query();

        $totalStage = $progress->funnel->total_stage;
        $expected = [
            'data' => [
                [
                    "current_stage" => $progress->current_stage,
                    "data_type" => 'Properti Potensial',
                    "funnel_id" => $progress->consultant_activity_funnel_id,
                    "id" => $progress->id,
                    "name" => $progress->progressable->name,
                    "progress_percentage" => $totalStage === 0 ? 0 : (int)($progress->current_stage / $totalStage * 100),
                    "progressable" => [
                        "id" => $progress->progressable->id
                    ],
                    "stage_name" => ($progress->current_stage === 0) ? 'Tugas Aktif' : $form->name,
                    "total_stage" => $totalStage
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithPotentialOwner(): void
    {
        $progress = factory(ActivityProgress::class)->states('withPotentialOwner', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->query();

        $totalStage = $progress->funnel->total_stage;
        $expected = [
            'data' => [
                [
                    "current_stage" => $progress->current_stage,
                    "data_type" => 'Owner Potensial',
                    "funnel_id" => $progress->consultant_activity_funnel_id,
                    "id" => $progress->id,
                    "name" => $progress->progressable->name,
                    "progress_percentage" => $totalStage === 0 ? 0 : (int)($progress->current_stage / $totalStage * 100),
                    "progressable" => [
                        "id" => $progress->progressable->id
                    ],
                    "stage_name" => ($progress->current_stage === 0) ? 'Tugas Aktif' : $form->name,
                    "total_stage" => $totalStage
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithDefault(): void
    {
        $progress = factory(ActivityProgress::class)->states('withForm')->create();
        $progress->related_table = '';
        $progress->total_stage = 0;

        $actual = (new ActivityManagementListTransformer())->transform($progress);

        $expected = [
            "current_stage" => $progress->current_stage,
            "data_type" => '',
            "funnel_id" => null,
            "id" => $progress->id,
            "name" => '',
            "progress_percentage" => 0,
            "progressable" => [
                "id" => null
            ],
            "stage_name" => null,
            "total_stage" => 0
        ];

        $this->assertEquals($expected, $actual);
    }
}
