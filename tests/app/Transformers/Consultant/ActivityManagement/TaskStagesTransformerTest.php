<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Test\MamiKosTestCase;

class TaskStagesTransformerTest extends MamiKosTestCase
{
    public function testTranformWithNonCurrentStage(): void
    {
        $transformer = new TaskStagesTransformer(2);
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);

        $this->assertEquals(
            [
                'id' => $stage->getId(),
                'name' => $stage->getName(),
                'is_current_stage' => false
            ],
            $transformer->transform($stage)
        );
    }

    public function testTransformWithCurrentStage(): void
    {
        $transformer = new TaskStagesTransformer(1);
        $stage = factory(ActivityForm::class)->create(['stage' => 1]);

        $this->assertEquals(
            [
                'id' => $stage->getId(),
                'name' => $stage->getName(),
                'is_current_stage' => true
            ],
            $transformer->transform($stage)
        );
    }

    public function testTransformWithBacklogStage(): void
    {
        $transformer = new TaskStagesTransformer(1);
        $stage = new BacklogStage();

        $this->assertEquals(
            [
                'id' => $stage->getId(),
                'name' => $stage->getName(),
                'is_current_stage' => false
            ],
            $transformer->transform($stage)
        );
    }

    public function testTransformWithToDoStage(): void
    {
        $transformer = new TaskStagesTransformer(1);
        $stage = new ToDoStage();

        $this->assertEquals(
            [
                'id' => $stage->getId(),
                'name' => $stage->getName(),
                'is_current_stage' => false
            ],
            $transformer->transform($stage)
        );
    }
}
