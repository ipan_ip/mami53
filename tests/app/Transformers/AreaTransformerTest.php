<?php

namespace App\Transformers;

use App\Entities\Area\Area;
use App\Test\MamiKosTestCase;

class AreaTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $area = factory(Area::class)->create();
        $transformer = new AreaTransformer();

        $actual = $transformer->transform($area);
        $expected = [
            'id' => $area->id,
            'created_at' => $area->created_at,
            'updated_at' => $area->updated_at
        ];

        $this->assertEquals($expected, $actual);
    }
}
