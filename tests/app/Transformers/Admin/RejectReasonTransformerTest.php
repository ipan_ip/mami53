<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\RejectReason;
use App\Test\MamiKosTestCase;

class RejectReasonTransformerTest extends MamiKosTestCase
{
    public function testTransform()
    {
        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);

        $transformedReason = (new RejectReasonTransformer())->transform($rejectReason);

        $this->assertEquals($rejectReason->id, $transformedReason['id']);
        $this->assertEquals($rejectReason->content, $transformedReason['content']);
    }
}
