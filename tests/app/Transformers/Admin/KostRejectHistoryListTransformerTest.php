<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\ListingRejectReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class KostRejectHistoryListTransformerTest extends MamiKosTestCase
{
    public function testTransform()
    {
        $admin = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);
        $this->createListingReason($room, $rejectReason, $admin);
        $this->createListingReason($room, $rejectReason, $admin);

        $listingReasons = ListingReason::where('listing_id', $room->id)->get();

        $transformedHistory = (new KostRejectHistoryListTransformer(new KostRejectHistoryTransformer()))
            ->transform($listingReasons);

        $this->assertEquals(
            count($transformedHistory['history']),
            $listingReasons->count()
        );
    }

    private function createListingReason(Room $room, RejectReason $rejectReason, User $admin): ListingReason
    {
        $listingReason = factory(ListingReason::class)->create([
            'from' => 'room',
            'listing_id' => $room->id,
            'user_id' => $admin->id,
            'content' => 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah).'
        ]);

        factory(ListingRejectReason::class)->create([
            'scraping_listing_reason_id' => $listingReason->id,
            'reject_reason_id' => $rejectReason->id,
        ]);

        return $listingReason;
    }
}
