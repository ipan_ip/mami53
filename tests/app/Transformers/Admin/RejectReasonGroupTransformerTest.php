<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\RejectReason;
use App\Test\MamiKosTestCase;
use DatabaseSeeder;
use RejectReasonSeeder;

class RejectReasonGroupTransformerTest extends MamiKosTestCase
{
    public function testTransform()
    {
        app(DatabaseSeeder::class)->call(RejectReasonSeeder::class);
        $rejectReasons = RejectReason::all();

        $transformedReasonGroup = (new RejectReasonGroupTransformer(new RejectReasonTransformer()))
            ->transform($rejectReasons);

        foreach (RejectReason::GROUP as $group) {
            $this->assertEquals(
                count($transformedReasonGroup[$group]),
                $rejectReasons->where('group', $group)->count()
            );
        }
    }
}
