<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\ListingRejectReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class KostRejectHistoryTransformerTest extends MamiKosTestCase
{
    public function testTransform()
    {
        $admin = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $listingReason = factory(ListingReason::class)->create([
            'from' => 'room',
            'listing_id' => $room->id,
            'user_id' => $admin->id,
            'content' => 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah).'
        ]);
        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);
        factory(ListingRejectReason::class)->create([
            'scraping_listing_reason_id' => $listingReason->id,
            'reject_reason_id' => $rejectReason->id,
        ]);

        $transformedReject = (new KostRejectHistoryTransformer())->transform($listingReason);

        $this->assertEquals($listingReason->id, $transformedReject['id']);
        $this->assertEquals($admin->name, $transformedReject['admin_name']);
        $this->assertEquals($listingReason->created_at, $transformedReject['date']);
        $this->assertEquals($listingReason->content, $transformedReject['reject_reason']);
    }
}
