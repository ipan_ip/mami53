<?php

namespace App\Transformers\Mamipay;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayOwner;
use App\Test\MamiKosTestCase;

class AdminInvoiceTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transformer = new AdminInvoiceTransformer();
    }

    public function testTransform(): void
    {
        $owner = factory(MamipayOwner::class)->create();
        $contract = factory(MamipayContract::class)->create(['owner_id' => $owner->user_id]);
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $actual = $this->transformer->transform($invoice);

        $expected = [
            'invoice_number' => $invoice->invoice_number,
            'paid_at' => $invoice->paid_at,
            'total_paid_amount' => $invoice->total_paid_amount,
            'transfer_status' => $invoice->transfer_status,
            'transfer_at' => $invoice->transfer_at,
            'transfer_amount' => $invoice->transfer_amount,
            'bank_name' => $owner->bank_name,
            'bank_account_number' => $owner->bank_account_number,
            'bank_account_owner' => $owner->bank_account_owner
        ];

        $this->assertEquals($expected, $actual);
    }
}
