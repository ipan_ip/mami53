<?php

namespace App\Transformers\Mamipay\Invoice;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\Misc\ManualPayout;
use App\Entities\Mamipay\Payout\PayoutTransaction;
use App\Entities\Mamipay\TransferStatus;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class AdminBookingInvoiceTransformerTest extends MamiKosTestCase
{
    use WithFaker;

    public function testTransform(): void
    {
        $userId = $this->faker->numberBetween(1, 1000000);
        $contract = factory(MamipayContract::class)->create(['owner_id' => $userId]);
        $ownerProfile = factory(MamipayOwner::class)->create(['user_id' => $userId]);
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'transfer_status' => TransferStatus::TRANSFERRED, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $manualPayout = factory(ManualPayout::class)->create(['invoice_id' => $invoice->id]);
        $firstTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $secondTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);

        $expected = [
            'paid_at' => $invoice->paid_at,
            'transfer_status' => TransferStatus::TRANSFERRED,
            'transfer_permission' => $manualPayout->type,
            'transfer_permission_status' => $manualPayout->status,
            'bank_account' => $ownerProfile->bank_name,
            'bank_account_number' => $ownerProfile->bank_account_number,
            'bank_account_owner' => $ownerProfile->bank_account_owner
        ];

        $this->assertEquals($expected, (new AdminBookingInvoiceTransformer())->transform($invoice));
    }

    public function testTransformWithoutManualPayout(): void
    {
        $userId = $this->faker->numberBetween(1, 1000000);
        $contract = factory(MamipayContract::class)->create(['owner_id' => $userId]);
        $ownerProfile = factory(MamipayOwner::class)->create(['user_id' => $userId]);
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'transfer_status' => TransferStatus::TRANSFERRED, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $firstTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $secondTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);

        $expected = [
            'paid_at' => $invoice->paid_at,
            'transfer_status' => TransferStatus::TRANSFERRED,
            'transfer_permission' => 'disbursement',
            'transfer_permission_status' => TransferStatus::TRANSFERRED,
            'bank_account' => $ownerProfile->bank_name,
            'bank_account_number' => $ownerProfile->bank_account_number,
            'bank_account_owner' => $ownerProfile->bank_account_owner
        ];

        $this->assertEquals($expected, (new AdminBookingInvoiceTransformer())->transform($invoice));
    }

    public function testTransformWithoutOwnerProfile(): void
    {
        $userId = $this->faker->numberBetween(1, 1000000);
        $contract = factory(MamipayContract::class)->create(['owner_id' => $userId]);
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'transfer_status' => TransferStatus::TRANSFERRED, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $firstTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);
        $secondTermyn = factory(PayoutTransaction::class)->create(['contract_invoice_id' => $invoice->id, 'transfer_status' => TransferStatus::TRANSFERRED]);

        $expected = [
            'paid_at' => $invoice->paid_at,
            'transfer_status' => TransferStatus::TRANSFERRED,
            'transfer_permission' => 'disbursement',
            'transfer_permission_status' => TransferStatus::TRANSFERRED,
            'bank_account' => null,
            'bank_account_number' => null,
            'bank_account_owner' => null
        ];

        $this->assertEquals($expected, (new AdminBookingInvoiceTransformer())->transform($invoice));
    }
}
