<?php

namespace Test\App\Transformers\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Test\MamiKosTestCase;
use App\Transformers\Mamipay\MamipayRoomPriceComponentTransformer;

class MamipayRoomPriceComponentTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(MamipayRoomPriceComponentTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTransformAndSuccess()
    {
        // prepare data
        $roomPriceComponentEntity = factory(MamipayRoomPriceComponent::class)->create();

        // run test
        $transform = $this->transform->transform($roomPriceComponentEntity);
        $assertHasKey = ['id', 'type', 'name', 'price', 'price_label', 'meta'];
        foreach ($assertHasKey as $item) $this->assertArrayHasKey($item, $transform);
    }

    public function testGetMetaDataNull(): void
    {
        // prepare data
        $roomPriceComponentEntity = factory(MamipayRoomPriceComponent::class)->create();

        // run test
        $result = $this->getPrivateMethod('getMetaData', [$roomPriceComponentEntity]);
        $this->assertNull($result);
    }

    public function testGetMetaDataNotNull(): void
    {
        // prepare data
        $roomPriceComponentEntity = factory(MamipayRoomPriceComponent::class)->create();
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $roomPriceComponentEntity->id
        ]);

        // run test
        $result = $this->getPrivateMethod('getMetaData', [$roomPriceComponentEntity]);
        $this->assertNotNull($result);
    }

    public function testGetPriceLabel(): void
    {
        // prepare data
        $price = 50000;

        // run test
        $result = $this->getPrivateMethod('getPriceLabel', [$price]);
        $this->assertNotNull($result);
    }

    private function getPrivateMethod(string $className, array $args = [])
    {
        $method = $this->getNonPublicMethodFromClass(
            MamipayRoomPriceComponentTransformer::class,
            $className
        );

        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}