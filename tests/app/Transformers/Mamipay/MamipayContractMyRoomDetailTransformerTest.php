<?php

namespace App\Transformers\Mamipay;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Libraries\MamipayApi;
use App\Test\MamiKosTestCase;
use App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use Mockery;

/**
 * @property MamipayContractMyRoomDetailTransformer $mamipayContractMyRoomDetailTransformer
 */
class MamipayContractMyRoomDetailTransformerTest extends MamiKosTestCase
{
    use WithFaker;

    private $mamipayContractMyRoomDetailTransformer;

    private const REQUIRED_RESPONSE_STRUCTURE = [
        'contract_id',
        'room_data' => [
            'photo' => [
                'real',
                'small',
                'medium',
                'large',
            ],
        ],
        'booking_data',
        'prices',
    ];

    protected function setUp() : void
    {
        parent::setUp();
        $this->mock(MamipayApi::class, function ($mock) {
            $mock->shouldReceive('get')->andReturn(null);
        });
        $this->mamipayContractMyRoomDetailTransformer = $this->app->make(MamipayContractMyRoomDetailTransformer::class);
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractWithSingleInvoice()
    {
        $this->actingAs(factory(User::class)->make());
        $mamipayContract = factory(MamipayContract::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => factory(User::class)->create(),
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
        ]);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform($mamipayContract);
        $this->assertNotNull($result);
        $this->convertToTestResponse($result)->assertJsonStructure(self::REQUIRED_RESPONSE_STRUCTURE);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractWithMultipleInvoice()
    {
        $this->actingAs(factory(User::class)->make());
        $mamipayContract = factory(MamipayContract::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => factory(User::class)->create(),
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addDay(1),
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addMonth(1),
        ]);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform($mamipayContract);
        $this->assertNotNull($result);
        $this->convertToTestResponse($result)->assertJsonStructure(self::REQUIRED_RESPONSE_STRUCTURE);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractWithoutKostPhoto()
    {
        $this->actingAs(factory(User::class)->make());
        $mamipayContract = factory(MamipayContract::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => factory(User::class)->create(),
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addDay(1),
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addMonth(1),
        ]);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform($mamipayContract);
        $this->assertNotNull($result);
        $this->convertToTestResponse($result)->assertJsonStructure(self::REQUIRED_RESPONSE_STRUCTURE);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractHasCheckedInShouldBeTrue()
    {
        $this->actingAs(factory(User::class)->make());
        $mamipayContract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
        ]);
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => factory(User::class)->create(),
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addDay(1),
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addMonth(1),
        ]);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform($mamipayContract);
        $this->assertNotNull($result);
        $this->convertToTestResponse($result)->assertJsonStructure(self::REQUIRED_RESPONSE_STRUCTURE);
        $this->assertTrue(true === $result['booking_data']['has_checked_in']); // strict equals
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractNullOwner()
    {
        $this->actingAs(factory(User::class)->make());
        $mamipayContract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
        ]);
        $room = factory(Room::class)->create();
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addDay(1),
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
            'scheduled_date' => Carbon::today()->addMonth(1),
        ]);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform($mamipayContract);
        $this->assertNotNull($result);
        $this->convertToTestResponse($result)->assertJsonStructure(self::REQUIRED_RESPONSE_STRUCTURE);
        $this->assertFalse($result['room_data']['is_premium_owner']);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractNullMamipayContractShouldThrow()
    {
        $this->actingAs(factory(User::class)->make());
        $this->expectException(Exception::class);
        $result = $this->mamipayContractMyRoomDetailTransformer->transform(null);
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractGetTypeNullUnit()
    {
        $this->assertEmpty($this->executePrivateGetType(null));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractGetTypeEmptyUnit()
    {
        $this->assertEmpty($this->executePrivateGetType(''));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractGetTypeNonRegisteredUnit()
    {
        $this->assertEquals('NOT_REGISTERED_UNIT_YET', $this->executePrivateGetType('NOT_REGISTERED_UNIT_YET'));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractGetTypeRegisteredUnit()
    {
        $this->assertEquals(MamipayContract::RENT_TYPE_WORDS[MamipayContract::UNIT_3MONTH], $this->executePrivateGetType(MamipayContract::UNIT_3MONTH));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractConvertMamipayStatusToBookingStatusActive()
    {
        $this->assertEquals(BookingUser::BOOKING_STATUS_CHECKED_IN, $this->executePrivateConvertMamipayStatusToBookingStatus(MamipayContract::STATUS_ACTIVE));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractConvertMamipayStatusToBookingStatusFinished()
    {
        $this->assertEquals(BookingUser::BOOKING_STATUS_FINISHED, $this->executePrivateConvertMamipayStatusToBookingStatus(MamipayContract::STATUS_FINISHED));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractConvertMamipayStatusToBookingStatusTerminated()
    {
        $this->assertEquals(BookingUser::BOOKING_STATUS_TERMINATED, $this->executePrivateConvertMamipayStatusToBookingStatus(MamipayContract::STATUS_TERMINATED));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractConvertMamipayStatusToBookingStatusBooked()
    {
        $this->assertEquals(BookingUser::BOOKING_STATUS_BOOKED, $this->executePrivateConvertMamipayStatusToBookingStatus(MamipayContract::STATUS_BOOKED));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractConvertMamipayStatusToBookingStatusNonRegistered()
    {
        $status = 'RANDOM STATUS';
        $this->assertEquals($status, $this->executePrivateConvertMamipayStatusToBookingStatus($status));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractIsFullyPaidActiveContract()
    {
        $status = $this->faker()->randomElement([
            MamipayContract::STATUS_BOOKED,
            MamipayContract::STATUS_FINISHED,
            MamipayContract::STATUS_TERMINATED,
            $this->faker()->randomLetter, // Random status
        ]);
        $this->assertEquals(false, $this->executePrivateIsFullyPaid($status));
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer
     */
    public function testTransformContractIsFullyPaidNonActiveContract()
    {
        $this->assertEquals(true, $this->executePrivateIsFullyPaid(MamipayContract::STATUS_ACTIVE));
    }

    public function testTransformContractRoomNumber(): void
    {
        // prepare data
        $contractTypeKost = factory(MamipayContractKost::class)->create([
            'room_number' => 10
        ]);

        // run test
        $response = $this->executePrivateRoomNumber($contractTypeKost);
        $this->assertEquals($contractTypeKost->room_number, $response);
    }

    private function executePrivateGetType(?string $contractDurationUnit) {
        return $this->getNonPublicMethodFromClass(MamipayContractMyRoomDetailTransformer::class, 'getType')
            ->invokeArgs($this->mamipayContractMyRoomDetailTransformer, [$contractDurationUnit]);
    }

    private function executePrivateConvertMamipayStatusToBookingStatus(?string $mamipayContractStatus) {
        return $this->getNonPublicMethodFromClass(MamipayContractMyRoomDetailTransformer::class, 'convertMamipayStatusToBookingStatus')
            ->invokeArgs($this->mamipayContractMyRoomDetailTransformer, [$mamipayContractStatus]);
    }

    private function executePrivateIsFullyPaid(...$params) {
        return $this->getNonPublicMethodFromClass(MamipayContractMyRoomDetailTransformer::class, 'isFullyPaid')
            ->invokeArgs($this->mamipayContractMyRoomDetailTransformer, $params);
    }

    private function executePrivateRoomNumber($mamipayContractKost) {
        return $this->getNonPublicMethodFromClass(MamipayContractMyRoomDetailTransformer::class, 'getRoomNumber')
            ->invokeArgs($this->mamipayContractMyRoomDetailTransformer, [$mamipayContractKost]);
    }

    private function convertToTestResponse(array $response): TestResponse
    {
        return new TestResponse(new JsonResponse($response));
    }
}
