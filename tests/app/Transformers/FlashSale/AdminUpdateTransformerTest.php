<?php

namespace App\Transformers\FlashSale;

use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class AdminUpdateTransformerTest extends MamiKosTestCase
{
    
    public function testTransform()
    {
        $banner        = factory(Media::class)->create();
        $flashSale     = factory(FlashSale::class)->create([
            'name'       => 'Flash Sale Mamikos',
            'photo_id'   => $banner->id,
            'start_time' => '2018-06-01 19:00:00',
            'end_time'   => '2020-06-01 19:00:00',
            'is_active'  => 1,
        ]);
        factory(FlashSaleArea::class, 3)->create(['flash_sale_id' => $flashSale->id]);

        // mock now() to see running flashsale
        $mockDate = Carbon::create(2019, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $result = (new AdminUpdateTransformer)->transform($flashSale);

        // assert
        $this->assertEquals($flashSale->id, $result['id']);
        $this->assertEquals(strtoupper($flashSale->name), $result['name']);
        $this->assertEquals($flashSale->created_by, $result['created_by']);
        $this->assertEquals(3, $result['areas_count']);
        $this->assertEquals(3, count($result['areas']));
        $this->assertTrue($result['is_running']);
        $this->assertTrue($result['is_active']);
        $this->assertNotNull($result['banner']);
    }
}
