<?php

namespace App\Transformers\FlashSale;

use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Transformers\FlashSale\WebIndexTransformer;
use Carbon\Carbon;

class WebIndexTransformerTest extends MamiKosTestCase
{
    
    public function testTransformShouldReturnNull()
    {
        $result = (new WebIndexTransformer(null))->transform(null);
        $this->assertEquals([], $result);
    }

    public function testTransformShouldReturnData()
    {
        $banner         = factory(Media::class)->create();
        $flashSale      = factory(FlashSale::class)->create([
            'photo_id'   => $banner->id,
            'start_time' => '2018-06-01 19:00:00',
            'end_time'   => '2020-06-01 19:00:00',
            'is_active'  => 1,
        ]);

        // feed areas
        factory(FlashSaleArea::class, 3)->create(['flash_sale_id' => $flashSale->id]);

        // mock time now()
        $mockDate = Carbon::create(2019, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $result = (new WebIndexTransformer(null))->transform($flashSale);

        $this->assertEquals($flashSale->id, $result['id']);
        $this->assertEquals($flashSale->name, $result['name']);
        $this->assertTrue($result['is_running']);
        $this->assertNotNull($result['banner']);
        $this->assertEquals(3, $result['areas_count']);
        $this->assertEquals(4, count($result['areas'])); // will always +1
    }

    public function testCompileArea()
    {
        // TODO: Skip for now
        $this->assertTrue(true);
    }

}