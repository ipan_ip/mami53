<?php

namespace App\Transformers\Owner\StatusHistory;

use App\Entities\Owner\StatusHistory;
use App\Test\MamiKosTestCase;
use App\User;

class ListTransformerTest extends MamiKosTestCase
{
    public function testTransformWithAdmin(): void
    {
        $admin = factory(User::class)->states(['tenant', 'admin'])->create();
        $history = factory(StatusHistory::class)->create(['created_by' => $admin->id]);

        $transformer = new ListTransformer();
        $actual = $transformer->transform($history);
        $expected = [
            'id' => $history->id,
            'timestamp' => $history->created_at->toString(),
            'history' => $history->new_status,
            'update_by' => [
                'id' => $admin->id,
                'name' => $admin->name,
                'email' => $admin->email,
                'is_owner' => false
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithOwner(): void
    {
        $admin = factory(User::class)->states(['owner'])->create();
        $history = factory(StatusHistory::class)->create(['created_by' => $admin->id]);

        $transformer = new ListTransformer();
        $actual = $transformer->transform($history);
        $expected = [
            'id' => $history->id,
            'timestamp' => $history->created_at->toString(),
            'history' => $history->new_status,
            'update_by' => [
                'id' => $admin->id,
                'name' => $admin->name,
                'email' => $admin->email,
                'is_owner' => true
            ]
        ];

        $this->assertEquals($expected, $actual);
    }
}
