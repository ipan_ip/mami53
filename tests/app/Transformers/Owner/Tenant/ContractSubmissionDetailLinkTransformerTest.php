<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractSubmissionDetailLinkTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractSubmissionDetailLinkTransformer::class);
    }

    public function testOwnerTenantDashboardCanBeTransform(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $dbetLink = factory(DbetLink::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $transform = (new ContractSubmissionDetailLinkTransformer)->transform($dbetLink);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('designer_id', $transform);
        $this->assertArrayHasKey('code', $transform);
        $this->assertArrayHasKey('is_required_identity', $transform);
        $this->assertArrayHasKey('is_required_due_date', $transform);
        $this->assertArrayHasKey('due_date', $transform);
        $this->assertArrayHasKey('room_name', $transform);
        $this->assertArrayHasKey('room_photo', $transform);
        $this->assertArrayHasKey('room_province', $transform);
        $this->assertArrayHasKey('room_city', $transform);
        $this->assertArrayHasKey('room_district', $transform);
        $this->assertArrayHasKey('room_gender', $transform);
        $this->assertArrayHasKey('room_price', $transform);
        $this->assertArrayHasKey('is_mamirooms', $transform);
        $this->assertArrayHasKey('level_info', $transform);
        $this->assertArrayHasKey('property_type', $transform);
    }

    public function testGetPrice(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        // run test
        $result = $this->getPrivateFunction('getPrice', [$room]);
        $this->assertIsArray($result);
    }

    public function testGetAdditional(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $roomAdditionalPrice = $room->mamipay_price_components;
        // run test
        $result = $this->getPrivateFunction('getAdditionals', [$roomAdditionalPrice, 'monthly']);
        $this->assertIsArray($result);
    }

    public function testGetLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('getLabel', ['monthly']);
        $this->assertSame('Per Bulan', $result);
    }

    public function testGetPriceLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('priceLabel', [0]);
        $this->assertSame('Rp0', $result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractSubmissionDetailLinkTransformer::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}