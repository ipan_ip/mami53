<?php

namespace App\Transformers\Dbet;

use App\Entities\Dbet\DbetLink;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Tenant\OwnerTenantLinkTransformer;

class OwnerTenantLinkTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(OwnerTenantLinkTransformer::class);
    }

    public function testOwnerTenantDashboardCanBeTransform(): void
    {
        // prepare data
        $dbetLink = factory(DbetLink::class)->create();

        // run test
        $transform = (new OwnerTenantLinkTransformer)->transform($dbetLink);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('code', $transform);
        $this->assertArrayHasKey('designer_id', $transform);
        $this->assertArrayHasKey('is_required_identity', $transform);
        $this->assertArrayHasKey('is_required_due_date', $transform);
        $this->assertArrayHasKey('due_date', $transform);
    }
}