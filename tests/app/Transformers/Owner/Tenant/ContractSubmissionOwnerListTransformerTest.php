<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractSubmissionOwnerListTransformerTest extends MamiKosTestCase
{
    const PRETEXT_UNIT_NAME         = 'Kamar';
    const PRETEXT_UNIT_FLOOR        = 'Lantai';
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractSubmissionOwnerListTransformer::class);
    }

    public function testOwnerListCanBeTransform(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $dbetLinkRegistered = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $transform = (new ContractSubmissionOwnerListTransformer)->transform($dbetLinkRegistered);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('room_number', $transform);
        $this->assertArrayHasKey('floor_number', $transform);
        $this->assertArrayHasKey('bill_label', $transform);
        $this->assertArrayHasKey('bill_price', $transform);
        $this->assertArrayHasKey('bill_price_label', $transform);
        $this->assertArrayHasKey('is_goldplus', $transform);
        $this->assertArrayHasKey('fullname', $transform);
    }

    public function testGetBillLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('getBillLabel', ['month']);
        $this->assertSame('Tagihan per Bulan', $result);
    }

    public function testGetPriceLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('priceLabel', [0]);
        $this->assertSame('Rp0', $result);
    }

    public function testGenerateNameWithPreText(): void
    {
        // prepare data
        $room = '1';

        // run test
        $result = $this->getPrivateFunction('generateUnitName', [$room]);
        $this->assertSame(self::PRETEXT_UNIT_NAME . ' ' . $room, $result);
    }

    public function testGenerateNameWithoutPreText(): void
    {
        // prepare data
        $room = 'Kamar 1';

        // run test
        $result = $this->getPrivateFunction('generateUnitName', [$room]);
        $this->assertSame($room, $result);
    }

    public function testGenerateUnitFloorWithPreText(): void
    {
        // prepare data
        $floor = '1';

        // run test
        $result = $this->getPrivateFunction('generateUnitFloor', [$floor]);
        $this->assertSame(self::PRETEXT_UNIT_FLOOR . ' ' . $floor, $result);
    }

    public function testGenerateUnitFloorWithoutPreText(): void
    {
        // prepare data
        $floor = 'Lantai 1';

        // run test
        $result = $this->getPrivateFunction('generateUnitFloor', [$floor]);
        $this->assertSame($floor, $result);
    }

    public function testCheckGoldPlusRoomUnitIsFalse(): void
    {
        // prepare data
        $roomUnitEntity = factory(RoomUnit::class)->make();

        // run test
        $result = $this->getPrivateFunction('checkGoldPlus', [$roomUnitEntity]);
        $this->assertFalse($result);
    }

    public function testGetAdditionalPriceDetailIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        $result = $this->getPrivateFunction('getAdditionalPriceDetail', [$data]);

        // run test
        $this->assertNull($result);
    }

    public function testGetAdditionalPriceDetailIsSuccess(): void
    {
        // prepare data
        $additionalPrice = [];
        $additionalPrice[] = [
            'name' => 'Listrik',
            'price' => 50000,
        ];

        $room = factory(Room::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id,
            'additional_price_detail' => json_encode($additionalPrice)
        ]);

        $result = $this->getPrivateFunction('getAdditionalPriceDetail', [$data]);

        // run test
        $this->assertIsArray($result);
    }

    public function testGetTotalBillPriceIsSuccess(): void
    {
        // prepare data
        $additionalPrice = [];
        $additionalPrice[] = [
            'name' => 'Listrik',
            'price' => 50000,
        ];

        $data = factory(DbetLinkRegistered::class)->create([
            'price' => 500000,
            'additional_price_detail' => json_encode($additionalPrice)
        ]);

        // run test
        $result = $this->getPrivateFunction('getTotalBillPrice', [$data]);
        $this->assertIsInt($result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractSubmissionOwnerListTransformer::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}