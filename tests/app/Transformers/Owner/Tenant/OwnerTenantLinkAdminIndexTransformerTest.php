<?php

namespace App\Transformers\Dbet;

use App\Entities\Dbet\DbetLink;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Tenant\OwnerTenantLinkAdminIndexTransformer;

class OwnerTenantLinkAdminIndexTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(OwnerTenantLinkAdminIndexTransformer::class);
    }

    public function testOwnerTenantDashboardCanBeTransform(): void
    {
        // prepare data
        $dbetLink = factory(DbetLink::class)->create();

        // run test
        $transform = (new OwnerTenantLinkAdminIndexTransformer)->transform($dbetLink);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('code', $transform);
        $this->assertArrayHasKey('link', $transform);
        $this->assertArrayHasKey('room_name', $transform);
        $this->assertArrayHasKey('room_province', $transform);
        $this->assertArrayHasKey('room_city', $transform);
        $this->assertArrayHasKey('owner_name', $transform);
        $this->assertArrayHasKey('owner_phone', $transform);
    }

    public function testGenerateLinkCodeSuccess(): void
    {
        // prepare data
        $dbetLink = factory(DbetLink::class)->create(['code' => mt_rand(1, 5)]);

        $method = $this->getPrivateFunction('generateLinkCode');
        $result = $method->invokeArgs($this->transform, [$dbetLink]);

        // run test
        $this->assertNotNull($result);
    }

    private function getPrivateFunction($function)
    {
        $method = $this->getNonPublicMethodFromClass(
            OwnerTenantLinkAdminIndexTransformer::class,
            $function
        );
        return $method;
    }
}