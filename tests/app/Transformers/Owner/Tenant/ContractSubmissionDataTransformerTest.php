<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractSubmissionDataTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractSubmissionDataTransformer::class);
    }

    public function testContractSubmissionDataCanBeTransform(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $dbetLinkRegistered = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $transform = (new ContractSubmissionDataTransformer)->transform($dbetLinkRegistered);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('designer_id', $transform);
        $this->assertArrayHasKey('designer_room_id', $transform);
        $this->assertArrayHasKey('room_name', $transform);
        $this->assertArrayHasKey('fullname', $transform);
        $this->assertArrayHasKey('phone', $transform);
        $this->assertArrayHasKey('job', $transform);
        $this->assertArrayHasKey('work_place', $transform);
        $this->assertArrayHasKey('due_date', $transform);
        $this->assertArrayHasKey('rent_count_type', $transform);
        $this->assertArrayHasKey('price', $transform);
        $this->assertArrayHasKey('group_channel_url', $transform);
        $this->assertArrayHasKey('status', $transform);
        $this->assertArrayHasKey('identity_id', $transform);
        $this->assertArrayHasKey('identity', $transform);
        $this->assertArrayHasKey('additional_price', $transform);
    }

    public function testGetAdditionalPriceDetailIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        $method = $this->getPrivateFunction('getAdditionalPriceDetail');
        $result = $method->invokeArgs($this->transform, [$data]);

        // run test
        $this->assertNull($result);
    }

    public function testGetAdditionalPriceDetailIsSuccess(): void
    {
        // prepare data
        $additionalPrice = [];
        $additionalPrice[] = [
            'name' => 'Listrik',
            'price' => 50000,
        ];

        $room = factory(Room::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id,
            'additional_price_detail' => json_encode($additionalPrice)
        ]);

        $method = $this->getPrivateFunction('getAdditionalPriceDetail');
        $result = $method->invokeArgs($this->transform, [$data]);

        // run test
        $this->assertIsArray($result);
    }

    private function getPrivateFunction($function)
    {
        $method = $this->getNonPublicMethodFromClass(
            ContractSubmissionDataTransformer::class,
            $function
        );
        return $method;
    }
}