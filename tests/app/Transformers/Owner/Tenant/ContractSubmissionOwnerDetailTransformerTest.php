<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractSubmissionOwnerDetailTransformerTest extends MamiKosTestCase
{
    const PRETEXT_UNIT_NAME         = 'Kamar';
    const PRETEXT_UNIT_FLOOR        = 'Lantai';
    const MALE_LABEL_ID             = 'Laki - Laki';
    const FEMALE_LABEL_ID           = 'Perempuan';
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractSubmissionOwnerDetailTransformer::class);
    }

    public function testOwnerListCanBeTransform(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $dbetLinkRegistered = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id
        ]);

        // run test
        $transform = (new ContractSubmissionOwnerDetailTransformer)->transform($dbetLinkRegistered);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('is_revised_price', $transform);
        $this->assertArrayHasKey('is_empty_room', $transform);
        $this->assertArrayHasKey('rent_count_type', $transform);
        $this->assertArrayHasKey('original_price', $transform);
        $this->assertArrayHasKey('original_price_label', $transform);
        $this->assertArrayHasKey('revised_price', $transform);
        $this->assertArrayHasKey('revised_price_label', $transform);
        $this->assertArrayHasKey('fullname', $transform);
        $this->assertArrayHasKey('phone', $transform);
        $this->assertArrayHasKey('gender', $transform);
        $this->assertArrayHasKey('job', $transform);
        $this->assertArrayHasKey('work_place', $transform);
        $this->assertArrayHasKey('identity_id', $transform);
        $this->assertArrayHasKey('identity', $transform);
        $this->assertArrayHasKey('due_date', $transform);
        $this->assertArrayHasKey('bill_label', $transform);
        $this->assertArrayHasKey('bill_price', $transform);
        $this->assertArrayHasKey('bill_price_label', $transform);
        $this->assertArrayHasKey('additional_price', $transform);
        $this->assertArrayHasKey('status', $transform);
        $this->assertArrayHasKey('created_at', $transform);
        $this->assertArrayHasKey('room', $transform);
        $this->assertArrayHasKey('tenant', $transform);
        $this->assertArrayHasKey('owner_phone_number', $transform);
        $this->assertArrayHasKey('owner_email', $transform);
        $this->assertArrayHasKey('contract_id', $transform);
    }

    public function testGetBillLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('getBillLabel', ['month']);
        $this->assertSame('Tagihan per Bulan', $result);
    }

    public function testGetPriceLabel(): void
    {
        // run test
        $result = $this->getPrivateFunction('priceLabel', [0]);
        $this->assertSame('Rp0', $result);
    }

    public function testGenerateNameWithPreText(): void
    {
        // prepare data
        $room = '1';

        // run test
        $result = $this->getPrivateFunction('generateUnitName', [$room]);
        $this->assertSame(self::PRETEXT_UNIT_NAME . ' ' . $room, $result);
    }

    public function testGenerateNameWithoutPreText(): void
    {
        // prepare data
        $room = 'Kamar 1';

        // run test
        $result = $this->getPrivateFunction('generateUnitName', [$room]);
        $this->assertSame($room, $result);
    }

    public function testGenerateUnitFloorWithPreText(): void
    {
        // prepare data
        $floor = '1';

        // run test
        $result = $this->getPrivateFunction('generateUnitFloor', [$floor]);
        $this->assertSame(self::PRETEXT_UNIT_FLOOR . ' ' . $floor, $result);
    }

    public function testGenerateUnitFloorWithoutPreText(): void
    {
        // prepare data
        $floor = 'Lantai 1';

        // run test
        $result = $this->getPrivateFunction('generateUnitFloor', [$floor]);
        $this->assertSame($floor, $result);
    }

    public function testCheckGoldPlusRoomUnitIsFalse(): void
    {
        // prepare data
        $roomUnitEntity = factory(RoomUnit::class)->make();

        // run test
        $result = $this->getPrivateFunction('checkGoldPlus', [$roomUnitEntity]);
        $this->assertFalse($result);
    }

    public function testGetAdditionalPriceDetailIsNull(): void
    {
        // prepare data
        $data = factory(DbetLinkRegistered::class)->create();

        $result = $this->getPrivateFunction('getAdditionalPriceDetail', [$data]);

        // run test
        $this->assertNull($result);
    }

    public function testGetAdditionalPriceDetailIsSuccess(): void
    {
        // prepare data
        $additionalPrice = [];
        $additionalPrice[] = [
            'name' => 'Listrik',
            'price' => 50000,
        ];

        $room = factory(Room::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $room->id,
            'additional_price_detail' => json_encode($additionalPrice)
        ]);

        $result = $this->getPrivateFunction('getAdditionalPriceDetail', [$data]);

        // run test
        $this->assertIsArray($result);
    }

    public function testGetGenderLabelIsEmpty(): void
    {
        // run test
        $result = $this->getPrivateFunction('getGenderLabel', ['']);
        $this->assertEmpty($result);
    }

    public function testGetGenderLabelIsMale(): void
    {
        // run test
        $result = $this->getPrivateFunction('getGenderLabel', ['male']);
        $this->assertEquals(self::MALE_LABEL_ID, $result);
    }

    public function testGetGenderLabelIsFemale(): void
    {
        // run test
        $result = $this->getPrivateFunction('getGenderLabel', ['female']);
        $this->assertEquals(self::FEMALE_LABEL_ID, $result);
    }

    public function testGetOriginalPriceIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();

        // run test
        $result = $this->getPrivateFunction('getOriginalPrice', [$room, 'month']);
        $this->assertIsInt($result);
    }

    public function testConvertContractRentTypeToRoomRentTypeIsNull(): void
    {
        // run test
        $result = $this->getPrivateFunction('convertContractRentTypeToRoomRentType', ['monthly']);
        $this->assertNull($result);
    }

    public function testConvertContractRentTypeToRoomRentTypeIsSuccess(): void
    {
        // run test
        $result = $this->getPrivateFunction('convertContractRentTypeToRoomRentType', ['month']);
        $this->assertSame($result, 'monthly');
    }

    public function testCheckIsPriceRevisedIsFalse(): void
    {
        // run test
        $result = $this->getPrivateFunction('checkIsPriceRevised', [100000, 100000]);
        $this->assertFalse($result);
    }

    public function testCheckIsPriceRevisedIsTrue(): void
    {
        // run test
        $result = $this->getPrivateFunction('checkIsPriceRevised', [100000, 200000]);
        $this->assertTrue($result);
    }

    public function testGetTotalBillPriceIsSuccess(): void
    {
        // prepare data
        $additionalPrice = [];
        $additionalPrice[] = [
            'name' => 'Listrik',
            'price' => 50000,
        ];

        $data = factory(DbetLinkRegistered::class)->create([
            'price' => 500000,
            'additional_price_detail' => json_encode($additionalPrice)
        ]);

        // run test
        $result = $this->getPrivateFunction('getTotalBillPrice', [$data]);
        $this->assertIsInt($result);
    }

    private function getPrivateFunction($className, $args)
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractSubmissionOwnerDetailTransformer::class,
            $className
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            $args
        );

        return $result;
    }
}