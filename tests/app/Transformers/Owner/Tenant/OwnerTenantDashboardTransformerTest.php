<?php

namespace App\Transformers\Dbet;

use App\Entities\Mamipay\MamipayContract;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Tenant\OwnerTenantDashboardTransformer;

class OwnerTenantDashboardTransformerTest extends MamiKosTestCase
{
    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(OwnerTenantDashboardTransformer::class);
    }

    public function testOwnerTenantDashboardCanBeTransform(): void
    {
        // run test
        $transform = (new OwnerTenantDashboardTransformer)->transform();

        // test
        $this->assertArrayHasKey('total_off_contract', $transform);
        $this->assertArrayHasKey('total_off_contract_submission', $transform);
        $this->assertArrayHasKey('total_off_contract_not_recorded', $transform);
        $this->assertArrayHasKey('dynamic_onboarding_label', $transform);
        $this->assertArrayHasKey('enable_dbet_link', $transform);
        $this->assertArrayHasKey('help_link', $transform);
    }
}