<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomTerm;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Common\MediaTransformer;

class InformationTransformerTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */    
    public function testTransform()
    {
        $room = factory(Room::class)->create();
        $tag = factory(Tag::class)->create();
        factory(TagType::class)->create([
            'tag_id' => $tag->id,
            'name' => 'kos_rule'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id
        ]);
        $media = factory(Media::class)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $media->id
        ]);

        $transformedRoom = (new InformationTransformer(new MediaTransformer))->transform($room);

        $this->assertEquals($room->song_id, $transformedRoom['_id']);
        $this->assertEquals([$tag->id], $transformedRoom['list_rules']);
        $this->assertEquals($media->id, $transformedRoom['photo_rules'][0]['id']);
    }
}
