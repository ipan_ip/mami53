<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;

class PriceTransformerTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */    
    public function testTransform()
    {
        $room = factory(Room::class)
            ->state('has-price-remark')
            ->create();

        $tag = factory(Tag::class)->create([
            'type' => 'keyword'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id
        ]);
        $additional = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
        ]);
        $deposit = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::DEPOSIT,
        ]);
        $dp = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::DP,
        ]);
        $dpDetail = factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $dp->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => 50,
        ]);
        $fine = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => MamipayRoomPriceComponentType::FINE,
        ]);
        $fineDurationType = factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE,
            'value' => 'day',
        ]);
        $fineLength = factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH,
            'value' => 10,
        ]);


        $transformedRoom = app()->make(PriceTransformer::class)->transform($room);

        $this->assertEquals($room->song_id, $transformedRoom['_id']);
        $this->assertEquals($tag->id, $transformedRoom['min_payment']);
        $this->assertEquals($additional->id, $transformedRoom['additional_cost'][0]['id']);
        $this->assertEquals($additional->price, $transformedRoom['additional_cost'][0]['price']);
        $this->assertEquals($additional->name, $transformedRoom['additional_cost'][0]['name']);
        $this->assertEquals($deposit->price, $transformedRoom['deposit_fee']['price']);
        $this->assertEquals($dpDetail->value, $transformedRoom['down_payment']['percentage']);
        $this->assertEquals($fineDurationType->value, $transformedRoom['fine']['duration_type']);
        $this->assertEquals($fineLength->value, $transformedRoom['fine']['length']);
        $this->assertEquals($room->price_remark, $transformedRoom['price_remark']);
    }
}
