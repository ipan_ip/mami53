<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RoomAllotmentTransformerTest extends MamiKosTestCase
{
    /**
     * @group PMS-495
     */
    public function testTransform()
    {
        $room = factory(Room::class)->create([
            'size'              => '5x5',
            'room_count'        => 10,
            'room_available'    => 5
        ]);

        $transformedRoom = (new RoomAllotmentTransformer())->transform($room);

        $this->assertEquals($room->song_id, $transformedRoom['_id']);
        $this->assertEquals($room->size, $transformedRoom['size']);
        $this->assertEquals($room->room_count, $transformedRoom['room_count']);
        $this->assertEquals($room->room_available, $transformedRoom['room_available']);
    }
}
