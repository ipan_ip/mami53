<?php

namespace App\Transformers\Apartment;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class DetailTransformerTest extends MamiKosTestCase
{
    
    public function testGetUpdateAt()
    {
        $time = Carbon::create(2020, 9, 22, 12);
        Carbon::setTestNow($time);
        $apartment = factory(Room::class)->create(["created_at" => $time]);
        $detailTransformer = $this->app->make(DetailTransformer::class);

        $method = $this->getNonPublicMethodFromClass(DetailTransformer::class, "getUpdatedAt");
        $result = $method->invokeArgs($detailTransformer, [$apartment]);

        $this->assertEquals($time, $result);
    }

    public function testGetUpdateAtNull()
    {
        $time = Carbon::create(2020, 9, 22, 12);
        Carbon::setTestNow($time);
        $apartment = factory(Room::class)->create();
        $detailTransformer = $this->app->make(DetailTransformer::class);

        $method = $this->getNonPublicMethodFromClass(DetailTransformer::class, "getUpdatedAt");
        $result = $method->invokeArgs($detailTransformer, [$apartment]);

        $this->assertEquals($time, $result);
    }

}
