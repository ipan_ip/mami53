<?php

namespace App\Transformers\Property;

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Test\MamiKosTestCase;
use App\User;

class PropertyPackageListTransformerTest extends MamikosTestCase
{
    public function testTransformWithoutProperty(): void
    {
        $level = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $level->id]);

        $expected = [
            'id' => $contract->id,
            'owner_name' => null,
            'status' => $contract->status,
            'total_property' => $contract->properties_count,
            'owner_phone_number' => null,
            'owner_id' => null,
            'level' => $level->name,
            'total_package' => $contract->total_package,
            'total_room_package' => $contract->total_room_package,
            'joined_at' => $contract->joined_at,
            'ended_at' => $contract->ended_at,
            'properties' => [],
            'created_at' => $contract->created_at->toDateTimeString(),
            'updated_at' => !is_null($contract->updated_at) ? $contract->updated_at->toDateTimeString() : null
        ];

        $this->assertEquals($expected, (new PropertyPackageListTransformer())->transform($contract));
    }

    public function testTransformWithMultipleProperty(): void
    {
        $level = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $level->id]);
        $user = factory(User::class)->create(['is_owner' => 'true']);
        $properties = factory(Property::class, 2)->create(['owner_user_id' => $user->id])
            ->each(function ($property) use ($contract) {
                factory(PropertyContractDetail::class)->create(['property_contract_id' => $contract->id, 'property_id' => $property->id]);
            });

        $contract->load('property_level', 'properties', 'properties.owner_user');

        $expected = [
            'id' => $contract->id,
            'owner_name' => $user->name,
            'status' => $contract->status,
            'total_property' => $contract->properties_count,
            'owner_phone_number' => $user->phone_number,
            'owner_id' => $user->id,
            'level' => $level->name,
            'total_package' => $contract->total_package,
            'total_room_package' => $contract->total_room_package,
            'joined_at' => $contract->joined_at,
            'ended_at' => $contract->ended_at,
            'properties' => [$properties[0]->name, $properties[1]->name],
            'created_at' => $contract->created_at->toDateTimeString(),
            'updated_at' => !is_null($contract->updated_at) ? $contract->updated_at->toDateTimeString() : null
        ];

        $this->assertEquals($expected, (new PropertyPackageListTransformer())->transform($contract));
    }
}
