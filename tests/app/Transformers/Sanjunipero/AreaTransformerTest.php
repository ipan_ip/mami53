<?php

namespace App\Transformers\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Collection;

class AreaTransformerTest extends MamiKosTestCase
{
    public function testTransform_NotEmpty()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $result = (new AreaTransformer)->transform($dynamicLandingParent);

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertSame($dynamicLandingChild->id, $result->first()['id']);
    }

    public function testTransform_Empty()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();

        $result = (new AreaTransformer)->transform($dynamicLandingParent);

        $this->assertTrue($result->isEmpty());
    }
}