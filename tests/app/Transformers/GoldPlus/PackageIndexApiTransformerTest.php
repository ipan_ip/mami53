<?php

namespace App\Transformers\GoldPlus;

use App\Entities\GoldPlus\Package;
use App\Constants\Periodicity;
use App\Test\MamiKosTestCase;

class PackageIndexApiTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(PackageIndexApiTransformer::class);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Monthly()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 145000,
            'periodicity' => Periodicity::MONTHLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp145.000/bulan untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP2Monthly()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp2',
            'price' => 245000,
            'periodicity' => Periodicity::MONTHLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP2]),
            'price_description' => 'Rp245.000/bulan untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP3Monthly()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp3',
            'price' => 545000,
            'periodicity' => Periodicity::MONTHLY,
            'unit_type' => Package::UNIT_TYPE_SINGLE,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => 'Jaminan Uang Kembali',
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP3]),
            'price_description' => 'Rp545.000/bulan untuk setiap kos yang dipilih.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Daily()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 15000,
            'periodicity' => Periodicity::DAILY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp15.000/hari untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Weekly()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 15000,
            'periodicity' => Periodicity::WEEKLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp15.000/minggu untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Quarterly()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 1115000,
            'periodicity' => Periodicity::QUARTERLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp1.115.000/3 bulan untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Semianually()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 1115000,
            'periodicity' => Periodicity::SEMIANUALLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp1.115.000/6 bulan untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Transformers\GoldPlus\PackageIndexApiTransformer
     */
    public function testTransformGP1Anually()
    {
        $package = factory(Package::class)->create([
            'code' => 'gp1',
            'price' => 2115000,
            'periodicity' => Periodicity::ANUALLY,
            'unit_type' => Package::UNIT_TYPE_ALL,
        ]);

        $actual = $this->transformer->transform($package);

        $expected = [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => null,
            'price' => $package->price,
            'charging' => __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]),
            'price_description' => 'Rp2.115.000/tahun untuk semua kos Anda.',
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
        $this->assertEquals($expected, $actual);
    }

    private function loadDetailHtml($package) {
        $path = resource_path('views/goldplus/' . $package->code . '.html');
        $html = '';

        if (file_exists($path)) {
           $html = file_get_contents($path) ;
        }

        return $html;
    }
}
