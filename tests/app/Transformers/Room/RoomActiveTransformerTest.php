<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Promoted\ViewPromote;

class RoomActiveTransformerTest extends MamiKosTestCase
{
    
    public function testListRoomActiveTransformer()
    {
        factory(Room::class)->create([
            "id" => 1,
            "song_id" => 1,
            "gender" => 1,
            "is_promoted" => 'true'
        ]);
        
        factory(ViewPromote::class)->create([
            "id" => 1,
            "total" => 1000,
            "used" => 0,
            "history" => 500,
            "is_active" => 1,
            "designer_id" => 1,
        ]);

        $room = Room::with('view_promote')->where('id', 1)->first();
        $transformedRoom = (new RoomActiveTransformer)->transform($room);
        $this->assertTrue($transformedRoom['is_promoted']);
    }
}