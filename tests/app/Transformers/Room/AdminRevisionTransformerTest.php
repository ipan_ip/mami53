<?php

namespace App\Transformers\Room;

use App\Entities\Revision\Revision;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class AdminRevisionTransformerTest extends MamiKosTestCase
{
    public function testTransformWithAdmin(): void
    {
        $admin = factory(User::class)->states('tenant', 'admin')->create();
        $room = factory(Room::class)->create();
        $revision = factory(Revision::class)->create([
            'revisionable_type' => Room::class,
            'revisionable_id' => $room->id,
            'user_id' => $admin->id,
            'key' => 'name',
            'old_value' => 'Kost kisan',
            'new_value' => 'Kost kostan'
        ]);

        $transformer = new AdminRevisionTransformer();
        $actual = $transformer->transform($revision);
        $expected = [
            'id' => $revision->id,
            'timestamp' => $revision->created_at->toString(),
            'field' => 'name',
            'old_value' => 'Kost kisan',
            'new_value' => 'Kost kostan',
            'update_by' => [
                'id' => $admin->id,
                'name' => $admin->name,
                'email' => $admin->email,
                'is_owner' => false
            ]
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformerWithOwner(): void
    {
        $admin = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create();
        $revision = factory(Revision::class)->create([
            'revisionable_type' => Room::class,
            'revisionable_id' => $room->id,
            'user_id' => $admin->id,
            'key' => 'name',
            'old_value' => 'Kost kisan',
            'new_value' => 'Kost kostan'
        ]);

        $transformer = new AdminRevisionTransformer();
        $actual = $transformer->transform($revision);
        $expected = [
            'id' => $revision->id,
            'timestamp' => $revision->created_at->toString(),
            'field' => 'name',
            'old_value' => 'Kost kisan',
            'new_value' => 'Kost kostan',
            'update_by' => [
                'id' => $admin->id,
                'name' => $admin->name,
                'email' => $admin->email,
                'is_owner' => true
            ]
        ];

        $this->assertEquals($expected, $actual);
    }
}
