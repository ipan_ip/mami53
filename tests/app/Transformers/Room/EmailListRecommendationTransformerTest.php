<?php

namespace App\Transformers\Room;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class EmailListRecommendationTransformerTest extends MamiKosTestCase
{
    public function setUp() : void
    {
        parent::setUp();
        $this->tranformer = new EmailListRecommendationTransformer;
    }

    public function testRentTypeRequestWithoutFilters()
    {
        $reflection = new \ReflectionClass(EmailListRecommendationTransformer::class);
        $method = $reflection->getMethod('rentTypeRequest');
        $method->setAccessible(true);
        $result = $method->invokeArgs(new EmailListRecommendationTransformer, []);

        $this->assertEquals('2', $result);
    }

    public function testRentTypeRequestWithFilters()
    {
        request()->merge(['filters' => [
            'rent_type' => 1
        ]]);
        $reflection = new \ReflectionClass(EmailListRecommendationTransformer::class);
        $method = $reflection->getMethod('rentTypeRequest');
        $method->setAccessible(true);
        $result = $method->invokeArgs(new EmailListRecommendationTransformer, []);

        $this->assertEquals(1, $result);
    }

    public function testTransform()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $media->id
        ]);

        $result = $this->tranformer->transform($room);

        $expectedResutl = [
            '_id' => $room->song_id,
            'price_title_format' => null,
            'room_title' => $room->name,
            'share_url' => $room->share_url,
            'photo_url' => $media->getMediaUrl(),
        ];

        $this->assertEquals($expectedResutl, $result);
    }
}
