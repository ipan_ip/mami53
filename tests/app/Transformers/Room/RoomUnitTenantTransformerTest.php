<?php

namespace App\Transformers\Room;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Test\MamiKosTestCase;

class RoomUnitTransformerTenantTest extends MamiKosTestCase
{
    
    public function testTransformReturnExpectedStructure()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
        ]);
        $unit = factory(RoomUnit::class)->create([
            "designer_id" => $room->id
        ]);

        $transformedRoomUnit = (new RoomUnitWithTenantTransformer())->transform($unit);

        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
    }

    public function testTransformWithTenant()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'room_count'     => 1,
            'room_available' => 0,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
        ]);
        $unit = factory(RoomUnit::class)->create([
            'name'          => 'Kamar 101',
            'floor'         => '1',
            'designer_id'   => $room->id,
            'occupied'      => 1,
        ]);
        $tenant = factory(MamipayTenant::class)->create([
            'name'          => 'John Doe',
            'phone_number'  => '0812345678910'
        ]);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'status'        => 'active',
            'end_date'      => date("Y-m-d"),
        ]);
        factory(MamipayContractKost::class)->create([
            'designer_id'       => $room->id,
            'contract_id'       => $contract->id,
            'designer_room_id'  => $unit->id,
        ]);

        $transformedRoomUnit = (new RoomUnitWithTenantTransformer())->transform($unit);

        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertEquals($unit->name, $transformedRoomUnit['name']);
        $this->assertEquals($unit->floor, $transformedRoomUnit['floor']);
        $this->assertEquals($unit->occupied, $transformedRoomUnit['occupied']);
        $this->assertTrue($transformedRoomUnit['disable']);
        $this->assertEquals($tenant->name, $transformedRoomUnit['tenant_name']);
        $this->assertEquals($tenant->phone_number, $transformedRoomUnit['tenant_phone']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedContractStructure(): array
    {
        return [
            "id",
            "name",
            "floor",
            "occupied",
            "disable",
            "tenant_name",
            "tenant_phone",
        ];
    }
}
