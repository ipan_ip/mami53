<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class LineRecommendationTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $transformer = new LineRecommendationTransformer();
        $room = factory(Room::class)->create();

        $actual = $transformer->transform($room);
        $expected = [
            "_id"                 => $room->song_id,
            "gender"              => $room->gender,
            "room-title"          => $room->name,
            'photos'              => \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'price'               => $room->price_monthly,
            'slug'                => $room->slug
        ];

        $this->assertEquals($expected, $actual);
    }
}
