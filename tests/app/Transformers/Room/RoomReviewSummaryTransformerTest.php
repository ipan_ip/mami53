<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Transformers\Room\RoomReviewSummaryTransformer;
use App\Entities\Media\Media;
use App\Entities\Room\Review;
use Illuminate\Support\Facades\Config;

class RoomReviewSummaryTransformerTest extends MamiKosTestCase
{
    /** @var RoomReviewSummaryTransformer */
    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(RoomReviewSummaryTransformer::class);
        Config::set('rating.scale', 5);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Transformers\Room\RoomReviewSummaryTransformer
     */
    public function testTransformWithMedia(): void
    {
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)
            ]);
        $review = factory(Review::class)->create([
            'designer_id' => $room->id,
            'scale' => 5,
            'cleanliness' => 5,
            'comfort' => 5,
            'safe' => 5,
            'price' => 5,
            'room_facility' => 5,
            'public_facility' => 5,
            'status' => 'live'
        ]);

        $room = Room::with('avg_review')->withCount('avg_review')->where('id', $room->id)->first();

        $transformed = $this->transformer->transform($room);
        $expected = [
            'id' => $room->song_id,
            'room_title' => $room->name,
            'photo' => $room->photo->getMediaUrl(),
            'review_count' => 1,
            'review_avg_rating' => 5
        ];

        $this->assertEquals($expected, $transformed);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Transformers\Room\RoomReviewSummaryTransformer
     */
    public function testTransformWithoutMedia(): void
    {
        $room = factory(Room::class)->create();
        $review = factory(Review::class)->create([
            'designer_id' => $room->id,
            'scale' => 5,
            'cleanliness' => 5,
            'comfort' => 5,
            'safe' => 5,
            'price' => 5,
            'room_facility' => 5,
            'public_facility' => 5,
            'status' => 'live'
        ]);

        $review = factory(Review::class)->create([
            'designer_id' => $room->id,
            'scale' => 5,
            'cleanliness' => 4,
            'comfort' => 4,
            'safe' => 4,
            'price' => 4,
            'room_facility' => 4,
            'public_facility' => 4,
            'status' => 'live'
        ]);

        $room = Room::with('avg_review')->withCount('avg_review')->where('id', $room->id)->first();

        $transformed = $this->transformer->transform($room);
        $expected = [
            'id' => $room->song_id,
            'room_title' => $room->name,
            'photo' => [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ],
            'review_count' => 2,
            'review_avg_rating' => 4.5
        ];

        $this->assertEquals($expected, $transformed);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Transformers\Room\RoomReviewSummaryTransformer
     */
    public function testTransformWithoutReview(): void
    {
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)
        ]);

        $room = Room::with('avg_review')->withCount('avg_review')->where('id', $room->id)->first();

        $transformed = $this->transformer->transform($room);
        $expected = [
            'id' => $room->song_id,
            'room_title' => $room->name,
            'photo' => $room->photo->getMediaUrl(),
            'review_count' => 0,
            'review_avg_rating' => 0
        ];

        $this->assertEquals($expected, $transformed);
    }
}