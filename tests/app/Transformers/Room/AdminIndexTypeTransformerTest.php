<?php

namespace App\Transformers\Room;

use App\Entities\Room\Element\Type;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class AdminIndexTypeTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $room = factory(Room::class)->create();
        $type = factory(Type::class)->create(['designer_id' => $room->id, 'is_available' => true]);
        $transformer = new AdminIndexTypeTransformer();

        $actual = $transformer->transform($type);

        $expected = [
            "id" => $type->id,
            "room_id" => $type->room->id,
            "name" => $type->name,
            "total_room" => $type->total_room,
            "room_size" => $type->room_size,
            "maximum_occupancy" => $type->maximum_occupancy,
            "tenant_type" => ['0'],
            "has_room_units" => $type->units_count > 0,
            "facility_count" => $type->facilities->count(),
            "is_facility_set" => $type->facility_setting_count > 0,
            "created_date" => Carbon::createFromFormat('Y-m-d H:i:s', $type->created_at)->format('d M Y'),
            "created_time" => Carbon::createFromFormat('Y-m-d H:i:s', $type->created_at)->format('@ H:i'),
            "updated_date" => Carbon::createFromFormat('Y-m-d H:i:s', $type->updated_at)->format('d M Y'),
            "updated_time" => Carbon::createFromFormat('Y-m-d H:i:s', $type->updated_at)->format('@ H:i'),
            "caution" => null,
            "photo_url" => null,
            "photo_url_large" => null,
            "is_available" => true,
        ];

        $this->assertEquals($expected, $actual);
    }
}
