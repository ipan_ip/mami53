<?php

namespace App\Transformers\Room;

use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RecommendationTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $device = factory(UserDevice::class)->create();
        $this->app->device = $device;

        $room = factory(Room::class)->create();
        $transformer = new RecommendationTransformer();

        $actual = $transformer->transform($room);
        $expected = [
            "_id" => $room->song_id,
            "gender" => $room->gender,
            "status" => $room->status,
            "room-title" => $room->name,
            'photo_url' => \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'price_title' => $room->price()->priceTitleTime(app()->device->last_rent_type),
            'price-title' => $room->price()->priceTitleTime(app()->device->last_rent_type),
            'room-title' => $room->name,
            'min_month' => $room->min_month,
            'available_room' => $room->available_room,
            'label' => $room->single_label,
            'is_booking' => $room->is_booking == 0 ? false : true,
            'has_video' => $room->youtube_id == null ? false : true,
            'label_array' => $room->label
        ];

        $this->assertEquals($expected, $actual);
    }
}
