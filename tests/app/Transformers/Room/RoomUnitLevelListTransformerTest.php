<?php

namespace App\Transformers\Room;

use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RoomUnitLevelListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(RoomUnitLevelListTransformer::class);
    }

    public function testTransformWithNoLevel(): void
    {
        $room = factory(Room::class)->create();
        $unit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);
        $actual = $this->transformer->transform($unit);
        $expected = [
            'id' => $unit->id,
            'name' => $unit->name,
            'floor' => $unit->floor,
            'occupied' => $unit->occupied,
            'level' => 'Reguler'
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithLevel(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(RoomLevel::class)->create();
        $unit = factory(RoomUnit::class)->create(['designer_id' => $room->id, 'room_level_id' => $level->id]);
        $actual = $this->transformer->transform($unit);
        $expected = [
            'id' => $unit->id,
            'name' => $unit->name,
            'floor' => $unit->floor,
            'occupied' => $unit->occupied,
            'level' => $level->name
        ];

        $this->assertEquals($expected, $actual);
    }
}
