<?php

namespace App\Transformers\Room;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Premium\ClickPricing;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Entities\Generate\ListingReason;
use Illuminate\Database\Eloquent\Collection;

class ListingOwnerPropertyTransformerTest extends MamiKosTestCase
{
    public function testTransformNullToEmptyArray()
    {
        $this->assertEquals([], (new ListingOwnerPropertyTransformer)->transform(null));
    }

    public function testRoomIsTransformable()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertSame($roomEntity->song_id, $transformedRoom['_id']);
    }

    public function testRoomIsTranformableWhenIsPremiumOwner()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(2)]);

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertTrue($transformedRoom['is_premium_owner']);
        $this->assertSame(0, $transformedRoom['apartment_project_id']);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
    }

    public function testRoomIsTranformableWhenIncomplete()
    {
        $roomEntity = factory(Room::class)->create(['address' => '']);
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(2)]);

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id,
            'status' => RoomOwner::ROOM_ADD_STATUS
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertTrue($transformedRoom['incomplete']);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
    }

    public function testRoomIsTranformableRejectRemark()
    {
        $rejectRemark = 'REJECTED';
        $roomEntity = factory(Room::class)->create(['reject_remark' => $rejectRemark]);
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(2)]);

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id,
            'status' => RoomOwner::ROOM_UNVERIFY_STATUS
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertSame($rejectRemark, $transformedRoom['reject_remark']);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
    }

    public function testRoomApartmentIsTranformableWithUnitTypeRoom()
    {
        foreach (ApartmentProject::UNIT_TYPE as $unitType) {
            $roomEntity = Factory(Room::class)->make(['unit_type' => $unitType]);
            $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
            $this->assertSame(
                ApartmentProject::UNIT_TYPE_ROOMS[$unitType] ?? null,
                $transformedRoom['unit_type_rooms']
            );
        }
    }

    public function testRoomIsTranformableWithThanosSnap()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        factory(ThanosHidden::class)->create([
            'designer_id' => $roomEntity->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['hidden']);
    }

    public function testRoomIsTranformableWithThanosReversedSnap()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        factory(ThanosHidden::class)->create([
            'designer_id' => $roomEntity->id,
            'snapped' => false
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertFalse($transformedRoom['hidden']);
    }

    public function testRoomIsTranformableWithBbkRequestStatusNull()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        $room = Room::with('owners.user')
            ->withLatestBbkRejectReason()
            ->find($roomEntity->id);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($room);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertNull($transformedRoom['bbk_request_status']);
    }

    public function testRoomIsTranformableWithBbkRequestStatusApprove()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        factory(BookingOwnerRequest::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingOwnerRequest::BOOKING_APPROVE,
        ]);

        $room = Room::with('owners.user')
            ->withLatestBbkRejectReason()
            ->find($roomEntity->id);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($room);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertEquals('approve', $transformedRoom['bbk_request_status']);
    }

    public function testRoomIsTranformableWithBbkRequestStatusWaiting()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        factory(BookingOwnerRequest::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingOwnerRequest::BOOKING_WAITING,
        ]);

        $room = Room::with('owners.user')
            ->withLatestBbkRejectReason()
            ->find($roomEntity->id);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($room);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertEquals('waiting', $transformedRoom['bbk_request_status']);
    }

    public function testRoomIsTranformableWithBbkRequestStatusReject()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        $listingReason = factory(ListingReason::class)->create([
            'from' => 'bbk',
            'listing_id' => $roomEntity->id
        ]);

        factory(BookingOwnerRequest::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingOwnerRequest::BOOKING_REJECT,
        ]);

        $room = Room::with('owners.user')
            ->withLatestBbkRejectReason()
            ->find($roomEntity->id);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($room);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertEquals('reject', $transformedRoom['bbk_request_status']);
        $this->assertEquals($listingReason->content, $transformedRoom['bbk_reject_reason']);
    }

    public function testRoomIsTranformableWithBbkRequestStatusNotActive()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        factory(BookingOwnerRequest::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingOwnerRequest::BOOKING_NOT_ACTIVE,
        ]);

        $room = Room::with('owners.user')
            ->withLatestBbkRejectReason()
            ->find($roomEntity->id);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($room);

        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertEquals('not_active', $transformedRoom['bbk_request_status']);
        $this->assertFalse($transformedRoom['gp_badge']['show']);
    }

    public function testRoomIsTranformableWithGpBadge1()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->gp1->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 1', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithGpBadge2()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->gp2->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 2', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithGpBadge3()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->gp3->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 3', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithGpBadge4()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->gp4->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 4', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithNewGpBadge1()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->ngp1->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 1', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithNewGpBadge2()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->ngp2->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 2', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithNewGpBadge3()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->ngp3->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 3', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithNewGpBadge4()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->ngp4->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 4', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithPromoGpBadge1()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->pgp1->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 1', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithPromoGpBadge2()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->pgp2->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 2', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithPromoGpBadge3()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->pgp3->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 3', $transformedRoom['gp_badge']['label']);
    }

    public function testRoomIsTranformableWithPromoGpBadge4()
    {
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $roomEntity->song_id,
            'level_id' => $this->pgp4->id
        ]);

        $roomEntity->load(['owners.user']);
        $transformedRoom = (new ListingOwnerPropertyTransformer)->transform($roomEntity);
        $this->assertSame(array_keys($transformedRoom), $this->expectedContractStructure());
        $this->assertTrue($transformedRoom['gp_badge']['show']);
        $this->assertEquals('GoldPlus 4', $transformedRoom['gp_badge']['label']);
    }

    protected function setUp(): void
    {
        parent::setUp();

        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CHAT)->make()->toArray()
        );

        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CLICK)->make()->toArray()
        );
        $this->prepareGoldplusLevel();
        $this->prepareNewGoldplusLevel();
        $this->preparePromoGoldplusLevel();
    }

    private function expectedContractStructure(): array
    {
        return [
            '_id',
            'address',
            'apartment_project_id',
            'area_formatted',
            'available_room',
            'bbk_request_status',
            'bbk_reject_reason',
            'chat_count',
            'chat_price',
            'click_price',
            'floor',
            'furnished_status',
            'gender',
            'gp_badge',
            'has_round_photo',
            'has_video',
            'hidden',
            'incomplete',
            'input_progress',
            'is_booking',
            'is_editable',
            'is_premium_owner',
            'is_promoted',
            'keyword',
            'level_info',
            'love_count',
            'min_month',
            'not_updated',
            'photo_url',
            'price_3_month',
            'price_6_month',
            'price_daily_usd',
            'price_daily',
            'price_monthly_usd',
            'price_monthly',
            'price_title_time_usd',
            'price_title_time',
            'price_type',
            'price_weekly_usd',
            'price_weekly',
            'price_yearly_usd',
            'price_yearly',
            'promo_title',
            'rating',
            'reject_remark',
            'review_count',
            'room_count',
            'room_title',
            'share_url',
            'size',
            'status_kos',
            'status_kost',
            'status',
            'survey_count',
            'unique_code',
            'unit_type_rooms',
            'unit_type',
            'view_count',
            'permissions',
        ];
    }

    private function prepareGoldplusLevel()
    {
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $kostLevel = factory(KostLevel::class, 4)->create();

        $this->setGoldPlus($kostLevel, 'Goldplus ', 'gp');

        Config::set('kostlevel.id.goldplus1', $this->gp1->id);
        Config::set('kostlevel.id.goldplus2', $this->gp2->id);
        Config::set('kostlevel.id.goldplus3', $this->gp3->id);
        Config::set('kostlevel.id.goldplus4', $this->gp4->id);
    }

    private function prepareNewGoldplusLevel()
    {
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $kostLevel = factory(KostLevel::class, 4)->create();

        $this->setGoldPlus($kostLevel, 'New Goldplus ', 'ngp');
    }

    private function preparePromoGoldplusLevel()
    {
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $kostLevel = factory(KostLevel::class, 4)->create();

        $this->setGoldPlus($kostLevel, 'Promo Goldplus ', 'pgp');
    }

    private function setGoldPlus(Collection $kostLevel, $goldplusPrefixName, $returnValue)
    {
        $gp = $returnValue;
        $index = 1;
        foreach ($kostLevel as $goldPlus) {
            $golplus = $gp . $index;
            $this->$golplus = $goldPlus;
            $this->$golplus->name = $goldplusPrefixName . $index++;
            $this->$golplus->save();
        }
    }
}
