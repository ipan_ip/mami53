<?php

namespace App\Transformers\Room\FinancialReport;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RoomTransformerTest extends MamiKosTestCase
{
    
    public function testTransformReturnExpectedStructure()
    {
        $room = factory(Room::class)->create();
        $transformedRoom = (new RoomTransformer())->transform($room);
        
        $this->assertEquals(
            array_keys($transformedRoom),
            [
                'id',
                'name',
                'address',
                'gender',
                'room_available',
                'price_monthly',
                'image'
            ]
        );
    }
}