<?php

namespace App\Transformers\Room;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;

class ListingOwnerTransformerTest extends MamiKosTestCase
{
    public function testTransformWithStatusAdd()
    {
        $room = $this->createUserAndRoom();

        $transformedRoom = (new ListingOwnerTransformer())->transform($room);

        $this->assertEquals($room->song_id, $transformedRoom['_id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoom));
    }

    public function testTransformWithPremiumOwner()
    {
        $room = $this->createUserAndRoom([], 'verified', ['date_owner_limit' => date('Y-m-d')]);

        $transformedRoom = (new ListingOwnerTransformer())->transform($room);

        $this->assertTrue($transformedRoom['is_premium_owner']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoom));
    }

    public function testTransformWithStatusUnverified()
    {
        $room = $this->createUserAndRoom([], 'unverified');

        $transformedRoom = (new ListingOwnerTransformer())->transform($room);

        $this->assertEquals('unverified', $transformedRoom['status_kost']);
        $this->assertNull($transformedRoom['reject_remark']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoom));
    }

    /**
     * Create User, Room, and RoomOwner Entity
     *
     * @param array $dataRoom
     * @param string $statusRoomOwner
     * @param array $dataUser
     *
     * @return \App\Entities\Room\Room
     */
    private function createUserAndRoom(array $dataRoom = [], string $statusRoomOwner = 'add', array $dataUser = [])
    {
        $user = factory(User::class)->create($dataUser);
        $room = factory(Room::class)->create($dataRoom);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => $statusRoomOwner,
        ]);

        return $room;
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedContractStructure(): array
    {
        return [
            '_id',
            'apartment_project_id',
            'price_title_time',
            'price_title_time_usd',
            'price_type',
            'price_title',
            'room_title',
            'address',
            'share_url',
            'has_round_photo',
            'gender',
            'status',
            'status-title',
            'available_room',
            'status_kost',
            'incomplete',
            'min_month',
            'photo_url',
            'keyword',
            'status_kos',
            'owner_phone',
            'is_booking',
            'rating',
            'review_count',
            'owner_phone_array',
            'survey_count',
            'chat_count',
            'love_count',
            'view_count',
            'click_count',
            'is_premium_owner',
            'label',
            'label_array',
            'is_promoted',
            'has_video',
            'promo_title',
            'click_price',
            'chat_price',
            'unit_type',
            'unit_type_rooms',
            'floor',
            'furnished_status',
            'size',
            'reject_remark',
            'not_updated',
            'price_daily',
            'price_weekly',
            'price_monthly',
            'price_yearly',
            'price_daily_usd',
            'price_weekly_usd',
            'price_monthly_usd',
            'price_yearly_usd',
            'price_3_month',
            'price_6_month',
            'unique_code',
            'input_progress',
            'level_info',
            'is_editable',
            'permissions'
        ];
    }
}
