<?php

namespace App\Transformers\Room;

use App\Entities\Level\RoomLevel;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomUnit;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Collection;

class RoomUnitTransformerTest extends MamiKosTestCase
{
    public function testTransformReturnExpectedStructure()
    {
        [$user, $room, $unit] = $this->prepareEntity();
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertFalse($transformedRoomUnit['gp_badge']['show']);
    }

    public function testTransformRoomUnitWithGpLevel()
    {
        $this->prepareGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->gp1->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithGpLevel2()
    {
        $this->prepareGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->gp2->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithGpLevel3()
    {
        $this->prepareGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->gp3->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithGpLevel4()
    {
        $this->prepareGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->gp4->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithNewGpLevel1()
    {
        $this->prepareNewGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->ngp1->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }
    
    public function testTransformRoomUnitWithNewGpLevel2()
    {
        $this->prepareNewGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->ngp2->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithNewGpLevel3()
    {
        $this->prepareNewGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->ngp3->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithNewGpLevel4()
    {
        $this->prepareNewGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->ngp4->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithPromoGpLevel1()
    {
        $this->preparePromoGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->pgp1->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithPromoGpLevel2()
    {
        $this->preparePromoGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->pgp2->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithPromoGpLevel3()
    {
        $this->preparePromoGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->pgp3->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    public function testTransformRoomUnitWithPromoGpLevel4()
    {
        $this->preparePromoGoldplusLevel();
        [$user, $room, $unit] = $this->prepareEntity();
        $unit->room_level_id = $this->pgp4->id;
        $transformedRoomUnit = (new RoomUnitTransformer())->transform($unit);
        $this->assertEquals($unit->id, $transformedRoomUnit['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoomUnit));
        $this->assertTrue($transformedRoomUnit['gp_badge']['show']);
        $this->assertEquals('GoldPlus', $transformedRoomUnit['gp_badge']['label']);
    }

    private function prepareEntity()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'room_available' => 1,
            'room_count' => 1
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        $unit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'occupied' => false
        ]);

        return [$user, $room, $unit];
    }

    private function prepareGoldplusLevel()
    {
        $kostLevel = factory(RoomLevel::class, 4)->create();
        $this->setGoldPlus($kostLevel, 'Goldplus ', 'gp');

        Config::set('roomlevel.id.goldplus1', $this->gp1->id);
        Config::set('roomlevel.id.goldplus2', $this->gp2->id);
        Config::set('roomlevel.id.goldplus3', $this->gp3->id);
        Config::set('roomlevel.id.goldplus4', $this->gp4->id);
    }

    private function prepareNewGoldplusLevel()
    {
        $kostLevel = factory(RoomLevel::class, 4)->create();
        $this->setGoldPlus($kostLevel, 'New Goldplus ', 'ngp');
    }

    private function preparePromoGoldplusLevel()
    {
        $kostLevel = factory(RoomLevel::class, 4)->create();
        $this->setGoldPlus($kostLevel, 'Promo Goldplus ', 'pgp');
    }

    private function setGoldPlus(Collection $roomLevel, $goldplusPrefixName, $returnValue)
    {
        $gp = $returnValue;
        $index = 1;
        foreach ($roomLevel as $goldPlus) {
            $golplus = $gp . $index;
            $this->$golplus = $goldPlus;
            $this->$golplus->name = $goldplusPrefixName . $index++;
            $this->$golplus->is_regular = 0;
            $this->$golplus->is_hidden = 0;
            $this->$golplus->save();
        }
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedContractStructure(): array
    {
        return [
            'id',
            'name',
            'floor',
            'occupied',
            'disable',
            'gp_badge'
        ];
    }
}
