<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use Illuminate\Support\Facades\Config;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\GoldPlus\Enums\KostStatus;
use App\Entities\Media\Media;

class GoldplusAcquisitionRoomTransformerTest extends MamiKosTestCase
{
    /** @var GoldplusAcquisitionRoomTransformer */
    private $transformer;

    private $oldGP1, $newGP1, $roomGP1;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(GoldplusAcquisitionRoomTransformer::class);
        $this->setupGoldplusKostLevel();

    }

    private function setupGoldplusKostLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $this->oldGP1 = factory(KostLevel::class)->create();
        Config::set('kostlevel.id.goldplus1', $this->oldGP1->id);

        $this->newGP1 = factory(KostLevel::class)->create();
        Config::set('kostlevel.ids.goldplus1', "{$this->oldGP1->id},{$this->newGP1->id}");
        Config::set('kostlevel.id.new_goldplus1', $this->newGP1->id);

        $this->roomGP1 = factory(RoomLevel::class)->create();
        Config::set('roomlevel.id.goldplus1', $this->roomGP1->id);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformNullRoom()
    {
        $this->assertEquals([], $this->transformer->transform(null));
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomWithNewGP()
    {
        $room = factory(Room::class)->create();
        $room->changeLevel($this->newGP1->id);

        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => $this->newGP1->id,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::ACTIVE,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ],
            'room_count' => $room->room_count,
            'new_gp' => true
        ], $this->transformer->transform($room));
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomWithOldGP()
    {
        $room = factory(Room::class)->create();
        $room->changeLevel($this->oldGP1->id);

        factory(RoomUnit::class,3)->create([
            'designer_id' => $room->id,
            'room_level_id' => $this->roomGP1->id
        ]);
        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => $this->oldGP1->id,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::ACTIVE,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ],
            'room_count' => 3,
            'new_gp' => false
        ], $this->transformer->transform($room));
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomWithOnReviewAndPhoto()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create(['photo_id' => $photo->id]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => 'new',
            'potential_gp_total_room' => 20
        ]);
        
        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => null,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::ON_REVIEW,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => $photo->getMediaUrl(),
            'room_count' => 20,
            'new_gp' => true
        ], $this->transformer->transform($room));
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomWithUnpaidPotentialProperty()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create(['photo_id' => $photo->id]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
            'potential_gp_total_room' => 20
        ]);
        
        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => null,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::UNPAID,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => $photo->getMediaUrl(),
            'room_count' => 20,
            'new_gp' => true
        ], $this->transformer->transform($room));
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomWithApprovedPotentialPropertyShouldActiveMember()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create(['photo_id' => $photo->id]);
        $room->changeLevel($this->oldGP1->id);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_APPROVED,
            'potential_gp_total_room' => 20
        ]);
        
        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => $this->oldGP1->id,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::ACTIVE,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => $photo->getMediaUrl(),
            'room_count' => 0,
            'new_gp' => false
        ], $this->transformer->transform($room));
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testTransformRoomNewGPLevelWithPotentialProperty()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create(['photo_id' => $photo->id, 'room_count' => 1]);
        $room->changeLevel($this->newGP1->id);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp2',
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
            'potential_gp_total_room' => 20
        ]);
        
        $this->assertEquals([
            'id' => $room->song_id,
            'gp_status' => [
                'key' => $this->newGP1->id,
                'value' => 'Goldplus 1'
            ],
            'membership_status' => KostStatus::ACTIVE,
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => $photo->getMediaUrl(),
            'room_count' => 1,
            'new_gp' => true
        ], $this->transformer->transform($room));
    }

    private function callFormatGpStatus(Room $room): array
    {
        return $this->callNonPublicMethod(
            GoldplusAcquisitionRoomTransformer::class,
            'formatGPstatus',
            [
                $room,
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4072
     * @group App\Transformers\Room\GoldplusAcquisitionRoomTransformer
     */
    public function testFormatGPstatusEmptyOfferedProduct()
    {
        $spiedTransformer = $this->spy(GoldplusAcquisitionRoomTransformer::class);
        $spiedTransformer->shouldReceive('alreadyNewGPLevel')->andReturn(false);
        $spiedTransformer->shouldReceive('hasPotentialPropertyUnpaid')->andReturn(true);

        $room = factory(Room::class)->make();
        $potentialProperty = factory(PotentialProperty::class)->make([
            'offered_product' => null,
        ]);
        $room->setRelation('potential_property', $potentialProperty);

        $response = $this->callFormatGpStatus($room);
        $this->assertEqualsCanonicalizing([
            'key' => null,
            'value' => null,
        ], $response);
    }
}
