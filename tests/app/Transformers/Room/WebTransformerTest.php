<?php

namespace App\Transformers\Room;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class WebTransformerTest extends MamiKosTestCase
{

    public function testWebTransformerCanBeTransformAllFieldExist()
    {
        // create user data
        $user = factory(User::class)->create();

        // create room data
        $room = factory(Room::class)->create();

        // create room owner
        factory(RoomOwner::class)
            ->state('owner')
            ->create(
                [
                    'designer_id' => $room->id,
                    'user_id' => $user->id
                ]
            );
        
        // create room tags
        factory(RoomFacility::class, 10)
            ->create(
                [
                    'designer_id' => $room->id,
                    'tag_id' => factory(Tag::class)->create(),
                    'photo_id' => factory(Media::class)->create()
                ]
            );
        $room->is_indexed = null;
        $transformWebRoom = (new WebTransformer())->transform($room);
        
        // main test function
        foreach($this->expectedWebStructure() as $value){
            $this->assertArrayHasKey($value, $transformWebRoom);
        }
        

    }

    private function expectedWebStructure(): array
    {
        return [
            '_id',
            'seq',
            'love_by_me',
            'city_code',
            'youtube_id',
            'name_slug',
            'slug',
            'area_city_keyword',
            'area_city',
            'area_subdistrict',
            'gender',
            'status',
            'available_room',
            'index_status',
            'is_indexed',
            'room_title',
            'location_id',
            'location',
            'latitude',
            'longitude',
            'address',
            'description',
            'html_description',
            'remarks',
            'min_month',
            'price_remark',
            'price_keyword',
            'urllanding',
            'namalanding',
            'breadcrumbs',
            'cards',
            'view_count',
            'love_count',
            'call_count',
            'is_booking',
            'is_mamirooms',
            'goldplus',
            'goldplus_status',
            'already_booked',
            'booking_type',
            'pay_for',
            'size',
            'photo_url',
            'price_tag',
            'price_title_formats',
            'price_daily',
            'price_weekly',
            'price_monthly',
            'price_quarterly',
            'price_semiannualy',
            'price_yearly',
            'price_title',
            'class',
            'class_badge',
            'top_facilities',
            'facility_photos',
            'facility_count',
            'fac_room',
            'fac_share',
            'fac_bath',
            'fac_near',
            'fac_park',
            'fac_price',
            'fac_keyword',
            'fac_room_other',
            'fac_bath_other',
            'fac_share_other',
            'fac_near_other',
            'fac_room_icon',
            'fac_share_icon',
            'fac_bath_icon',
            'fac_near_icon',
            'fac_park_icon',
            'fac_price_icon',
            'owner_id',
            'owner_header',
            'owner_name',
            'owner_gender',
            'owner_photo_url',
            'is_premium_owner',
            'owner_chat_name',
            'phone',
            'expired_phone',
            'updated_at',
            'verification_status',
            'has_photo_round',
            'photo_360',
            'map_guide',
            'not_updated',
            'number_success_kos_trx',
            'number_success_owner_trx',
            'admin_id',
            'is_promoted',
            'has_video',
            'rating',
            'review_count',
            'promotion',
            'report_types',
            'price_title_time',
            'building_year',
            'enable_chat',
            'allow_phonenumber_via_chat',
            'checker',
            'unique_code',
            'level_info',
            'booking',
            'is_flash_sale',
            'flash_sale',
            'is_booking_with_calendar',
            'max_month_checkin',
            'booking_time_restriction_active',
            'available_booking_time_start',
            'available_booking_time_end',
        ];
    }
}
