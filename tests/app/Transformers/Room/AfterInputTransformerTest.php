<?php

namespace App\Transformers\Room;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;

class AfterInputTransformerTest extends MamiKosTestCase
{
    
    public function testTransformReturnExpectedStructure()
    {
        $room = $this->createUserAndRoom();

        $transformedRoom = (new AfterInputTransformer())->transform($room);

        $this->assertEquals($room->id, $transformedRoom['room_id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoom));
    }

    /**
     * Create User, Room, and RoomOwner Entity
     *
     * @param array $dataRoom
     * @param string $statuRoomOwner
     * @param array $dataUser
     *
     * @return \App\Entities\Room\Room
     */
    private function createUserAndRoom(array $dataRoom = [], string $statuRoomOwner = 'add', array $dataUser = [])
    {
        $user = factory(User::class)->create($dataUser);
        $room = factory(Room::class)->create($dataRoom);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => $statuRoomOwner,
        ]);

        return $room;
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedContractStructure(): array
    {
        return [
            "_id",
            "room_id",
            "code",
            "slug",
            "gender",
            "status",
            "available_room",
            "room_title",
            "location_id",
            "cards",
            "size",
            "photo_url",
            "price_daily",
            "price_monthly",
            "price_yearly",
            "price_weekly",
            "price_shown",
            "incomplete",
        ];
    }
}
