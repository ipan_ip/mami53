<?php

namespace App\Transformers\Room;

use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ExpiredBy;
use App\Entities\Room\RoomStatusLabel;
use App\Entities\Room\ThanosHidden;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class AdminIndexTwoTransformerTest extends MamiKosTestCase
{

    public function testTransformReturnExpectedStructure()
    {
        $room = $this->createUserAndRoom();

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals($room->id, $transformedRoom['id']);
        $this->assertSame($this->expectedContractStructure(), array_keys($transformedRoom));
    }

    public function testTransformWithKosUpdatedDate()
    {
        $room = $this->createUserAndRoom(['kost_updated_date' => '2011-07-02 09:15:09.0']);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('02-07-2011', $transformedRoom['kost_updated_date']);
    }

    public function testTransformWithNullKosUpdatedDate()
    {
        $room = $this->createUserAndRoom(['kost_updated_date' => null]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $expectedDate = Carbon::createFromFormat('d-m-Y H:i:s', $room->last_update)->format('d-m-Y');

        $this->assertEquals($expectedDate, $transformedRoom['kost_updated_date']);
    }

    public function testTransformWithPromotedKos()
    {
        $room = $this->createUserAndRoom(['is_promoted' => 'true']);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('true', $transformedRoom['is_promoted']);
    }

    public function testTransformWithAdminRemark()
    {
        $room = $this->createUserAndRoom(['admin_remark' => 'Langsung datang saja']);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('Langsung datang saja', $transformedRoom['admin_remark']);
    }

    public function testTransformWithDateOwnerLimit()
    {
        $room = $this->createUserAndRoom([], 'verified', ['date_owner_limit' => date("Y-m-d", time() + 86400)]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertTrue($transformedRoom['is_premium_owner']);
    }

    public function testTransformWithAgentStatus()
    {
        $room = $this->createUserAndRoom(['agent_status' => 'Pemilik Kos']);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('Pemilik Kos', $transformedRoom['agent_status']);
    }

    public function testTransformWithPrice()
    {
        $room = $this->createUserAndRoom([
            'price_daily'   => 100000,
            'price_weekly'  => 200000,
            'price_monthly' => 300000,
            'price_yearly'  => 400000,
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(100000, $transformedRoom['price_daily']);
        $this->assertEquals(200000, $transformedRoom['price_weekly']);
        $this->assertEquals(300000, $transformedRoom['price_monthly']);
        $this->assertEquals(400000, $transformedRoom['price_yearly']);
    }

    public function testTransformWithRoomAvailableAndRoomCount()
    {
        $room = $this->createUserAndRoom([
            'room_available'    => 7,
            'room_count'        => 11,
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(7, $transformedRoom['room_available']);
        $this->assertEquals(11, $transformedRoom['room_count']);
    }

    public function testTransformWithVerifiedPhoneAndAddress()
    {
        $room = $this->createUserAndRoom([
            'is_verified_phone'     => true,
            'is_verified_address'   => true,
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertTrue($transformedRoom['is_verified_phone']);
        $this->assertTrue($transformedRoom['is_verified_address']);
    }

    public function testTransformWithVisitedKos()
    {
        $room = $this->createUserAndRoom(['is_visited_kost' => true]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertTrue($transformedRoom['is_visited_kost']);
    }

    public function testTransformWithOwnerAndManagerPhone()
    {
        $room = $this->createUserAndRoom([
            'owner_phone'   => '08123456789',
            'manager_phone' => '08522222222',
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('08123456789', $transformedRoom['owner_phone']);
        $this->assertEquals('08522222222', $transformedRoom['manager_phone']);
    }

    public function testTransformWithGender()
    {
        $room = $this->createUserAndRoom(['gender' => 2]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(2, $transformedRoom['gender']);
    }

    public function testTransformWithExpiredPhone()
    {
        $room = $this->createUserAndRoom(['expired_phone' => 1]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(1, $transformedRoom['expired_phone']);
    }

    public function testTransformWithRoomHidden()
    {
        $room = $this->createUserAndRoom(['is_active' => 'false']);
        factory(ThanosHidden::class)->create(['designer_id' => $room->id]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::INACTIVE_THANOS, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusVerified()
    {
        $room = $this->createUserAndRoom([], 'verified');

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::ACTIVE, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusUnverified()
    {
        $room = $this->createUserAndRoom([], 'unverified');

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::REJECTED, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusAdd()
    {
        $room = $this->createUserAndRoom(['gender' => null]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::DRAFT, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusClaim()
    {
        $room = $this->createUserAndRoom([], 'claim');

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::CLAIM, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusEdited()
    {
        $room = $this->createUserAndRoom([], RoomOwner::LABEL_ROOM_OWNER_EDITED);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::WAITING_VERIFICATION, $transformedRoom['kos_status']);
    }

    public function testTransformWithExpiredBy()
    {
        $room = factory(Room::class)->create(['is_active' => 'false']);

        factory(ExpiredBy::class)->create([
            'designer_id'   => $room->id,
            'expired_by'    => 'admin',
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('admin', $transformedRoom['expired_by']);
    }

    public function testTransformWithExpiredByNull()
    {
        $room = $this->createUserAndRoom(['is_active' => 'true']);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertNull($transformedRoom['expired_by']);
    }

    public function testTransformWithExpiredByOwner()
    {
        $room = factory(Room::class)->create(['is_active' => 'false']);

        factory(ExpiredBy::class)->create([
            'designer_id'   => $room->id,
            'expired_by'    => 'owner',
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('owner', $transformedRoom['expired_by']);
        $this->assertEquals(RoomStatusLabel::INACTIVE_OWNER, $transformedRoom['kos_status']);
    }

    public function testTransformWithoutExpiredBy()
    {
        $room = $this->createUserAndRoom();

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertNull($transformedRoom['expired_by']);
    }

    public function testTransformWithoutOwnerActive()
    {
        $room = factory(Room::class)->state('active')->create();

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::ACTIVE_NO_OWNER, $transformedRoom['kos_status']);
    }

    public function testTransformWithoutOwnerInactive()
    {
        $room = factory(Room::class)->create(['is_active' => 'false', 'expired_phone' => 1]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::NO_OWNER, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusAddComplete()
    {
        $room = $this->createUserAndRoom([
            'owner_name' => 'pemilik',
            'owner_phone' => '08123888888',
            'manager_name' => 'manager',
            'manager_phone' => '08123777777',
            'latitude' => 100,
            'longitude' => 100,
            'address' => 'Jl. Somewhere no. Something',
            'photo_count' => 2,
            'description' => 'Kost Mevvah'
        ]);

        $tags = factory(Tag::class, 2)->create();

        factory(RoomTag::class, $tags->count())->create([
            'tag_id' => $tags->shift()->id,
            'designer_id' => $room->id,
        ]);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::WAITING_VERIFICATION, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusDraft1()
    {
        $room = $this->createUserAndRoom([], RoomOwner::ROOM_EARLY_EDIT_STATUS);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::DRAFT, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusEditStatus()
    {
        $room = $this->createUserAndRoom([], RoomOwner::ROOM_EDIT_STATUS);

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals('('.RoomOwner::LABEL_ROOM_OWNER_EDITED.')' . RoomStatusLabel::WAITING_VERIFICATION, $transformedRoom['kos_status']);
    }

    public function testTransformWithKosStatusUnknown()
    {
        $room = $this->createUserAndRoom([], 'unknown');

        $transformedRoom = (new AdminIndexTwoTransformer())->transform($room);

        $this->assertEquals(RoomStatusLabel::UNKNOWN, $transformedRoom['kos_status']);
    }

    /**
     * Create User, Room, and RoomOwner Entity
     *
     * @param array $dataRoom
     * @param string $statusRoomOwner
     * @param array $dataUser
     *
     * @return \App\Entities\Room\Room
     */
    private function createUserAndRoom(array $dataRoom = [], string $statusRoomOwner = 'add', array $dataUser = [])
    {
        $user = factory(User::class)->create($dataUser);
        $room = factory(Room::class)->create($dataRoom);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => $statusRoomOwner,
        ]);

        return $room;
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedContractStructure(): array
    {
        return [
            'id',
            'photo_small',
            'kost_updated_date',
            'is_promoted',
            'is_premium_owner',
            'name',
            'admin_remark',
            'address',
            'agent_status',
            'price_daily',
            'price_weekly',
            'price_monthly',
            'price_yearly',
            'room_available',
            'room_count',
            'hide_allotment',
            'is_verified_phone',
            'is_verified_address',
            'is_visited_kost',
            'owner_phone',
            'manager_phone',
            'gender',
            'expired_phone',
            'kos_status',
            'expired_by',
            'room_unit_empty',
            'room_unit_occupied',
            'room_unit_empty_with_contract',
            'owner_id'
        ];
    }
}
