<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class AdminIndexBenefitTransformerTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new AdminIndexBenefitTransformer();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTransform()
    {
        $room = factory(Room::class)->create();

        $result = $this->class->transform($room);
        $this->assertEquals($room->id, $result['id']);
        $this->assertEquals($room->song_id, $result['song_id']);
        $this->assertEquals($room->slug, $result['slug']);
        $this->assertEquals($room->name, $result['name']);
        $this->assertIsArray($result['benefit']);
    }
}
