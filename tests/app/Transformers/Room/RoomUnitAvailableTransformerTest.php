<?php

namespace App\Transformers\Room;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class RoomUnitAvailableTransformerTest extends MamiKosTestCase
{
    public function testTransformWithNoActiveContract(): void
    {
        $transformer = new RoomUnitAvailableTransformer();
        $kost = factory(Room::class)->create();
        $contract = factory(MamipayContract::class)->create(['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()->addDays(5)->toDateString()]);
        $unit = factory(RoomUnit::class)->create([
            'name'      => 'Kost',
            'floor'     => 1,
            'occupied'  => true,
            'designer_id' => $kost->id
        ]);
        factory(MamipayContractKost::class)->create(['contract_id' => $contract->id, 'designer_id' => $kost->id, 'designer_room_id' => $unit->id]);

        $this->assertSame(
            [
                'id' => $unit->id,
                'name' => 'Kost',
                'floor' => 1,
                'occupied' => true,
                'has_active_contract' => true
            ],
            $transformer->transform($unit)
        );
    }

    public function testTransformWithActiveContract(): void
    {
        $transformer = new RoomUnitAvailableTransformer();
        $unit = new RoomUnit([
            'name'      => 'Kost',
            'floor'     => 1,
            'occupied'  => true,
        ]);
        $unit->contract_id = null;
        $unit->id = 1;

        $this->assertSame(
            [
                'id' => 1,
                'name' => 'Kost',
                'floor' => 1,
                'occupied' => true,
                'has_active_contract' => false
            ],
            $transformer->transform($unit)
        );
    }
}
