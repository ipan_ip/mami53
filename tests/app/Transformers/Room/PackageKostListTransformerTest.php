<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Entities\Room\Element\RoomUnit;
use App\Transformers\Room\PackageKostListTransformer;

class PackageKostListTransformerrTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp()
    {
        parent::setUp();
        $this->transformer = new PackageKostListTransformer();
    }

    public function testTransformTransformerWithLevel()
    {
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'is_regular' => 0,
            'is_hidden' => 0
        ]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true'
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => false,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformTransformerWithoutLevel()
    {
        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true'
        ]);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'is_assignable' => false,
            'has_been_assigned' => false,
            'owner_name' => $kost->owner_name,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Reguler',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithExpiredContract(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $property->property_contracts->first()->property_level->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => false,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithAssignableKost(): void
    {
        $property = factory(Property::class)->state('withActiveContract')->create();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $property->property_contracts->first()->property_level->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus1(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_1;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus1Promo(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_1_PROMO;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus2(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_2;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus2Promo(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_2_PROMO;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus3(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_3;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithManualGoldplus3Promo(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $contract->assigned_by = 5;
        $contract->save();
        $propertyLevel = $contract->property_level;
        $propertyLevel->name = PropertyLevel::GOLDPLUS_3_PROMO;
        $propertyLevel->save();

        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithNonAssignedKost(): void
    {
        $property = factory(Property::class)->state('withActiveContract')->create();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $property->property_contracts->first()->property_level->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithAssignedKost(): void
    {
        $property = factory(Property::class)->state('withActiveContract')->create();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $property->property_contracts->first()->property_level->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true',
            'property_id' => $property->id
        ]);

        factory(RoomUnit::class)->create(['designer_id' => $kost->id, 'room_level_id' => $roomLevel->id]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => true,
            'has_been_assigned' => true,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithoutPropertyLevel(): void
    {
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        factory(RoomLevel::class)->create(['kost_level_id' => $level->id]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true'
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => false,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testTransformWithoutRoomLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'property_level_id' => $propertyLevel->id,
            'is_regular' => 0,
            'is_hidden' => 0
        ]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'is_active' => 'true'
        ]);

        $kost->level()->attach($level);

        $actual = $this->transformer->transform($kost);
        $expected = [
            'id' => $kost->id,
            'kost_name' => $kost->name,
            'owner_name' => $kost->owner_name,
            'is_assignable' => false,
            'has_been_assigned' => false,
            'area_city' => $kost->area_city,
            'total_rooms' => $kost->room_count,
            'level' => 'Mamikos Goldplus 1',
            'updated_at' => $kost->updated_at->toDateTimeString()
        ];

        $this->assertEquals($expected, $actual);
    }
}
