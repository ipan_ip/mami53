<?php

namespace App\Transformers\Room;

use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class MetaTransformerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testTransform()
    {
        $this->app->user = null;
        $this->app->device = null;

        // create User
        $user = factory(User::class)
            ->create();

        // create Room
        $room = factory(Room::class)
            ->create();

        // create Room Owner
        factory(RoomOwner::class)
            ->state('owner')
            ->create(
                [
                    'designer_id' => $room->id,
                    'user_id' => $user->id
                ]
            );

        // Create Room Tags
        factory(RoomFacility::class, 5)
            ->create(
                [
                    'designer_id' => $room->id,
                    'tag_id' => factory(Tag::class)->create()
                ]
            );

        $transformedRoomMeta = (new MetaTransformer())->transform($room);

        $this->assertEquals($room->song_id, $transformedRoomMeta['room_id']);
        $this->assertSame($this->expectedMetaStructure(), array_keys($transformedRoomMeta));
    }

    /**
     * Create array with expected contract structure
     *
     * @return array
     */
    private function expectedMetaStructure(): array
    {
        return [
            'already_booked',
            'available_booking_time_end',
            'available_booking_time_start',
            'available_room',
            'booking_time_restriction_active',
            'checker_object',
            'city',
            'fac_room',
            'fac_room_icon',
            'formatted_updated_at',
            'gender',
            'goldplus_status',
            'is_booking',
            'is_flash_sale',
            'is_mamirooms',
            'is_premium_owner',
            'is_promoted',
            'owner_id',
            'owner_name',
            'price_title_formats',
            'room_active_booking',
            'room_id',
            'room_name',
            'room_photos',
            'share_url',
            'subdistrict',
            'tenant_name',
            'updated_at'
        ];
    }

    public function testGetUpdateAt()
    {
        $room = factory(Room::class)->create(["created_at" => "2020-06-02 09:39:46"]);
        $metaTransformer = $this->app->make(MetaTransformer::class);

        $method = $this->getNonPublicMethodFromClass(MetaTransformer::class, 'getUpdatedAt');
        $result = $method->invokeArgs($metaTransformer, [$room]);

        $this->assertEquals("2020-06-02 09:39:46", $result);
    }

    public function testGetUpdateAtWithKostUpdateAt()
    {
        $room = factory(Room::class)->create(["kost_updated_date" => "2020-06-02 10:46:46"]);
        $metaTransformer = $this->app->make(MetaTransformer::class);

        $method = $this->getNonPublicMethodFromClass(MetaTransformer::class, 'getUpdatedAt');
        $result = $method->invokeArgs($metaTransformer, [$room]);

        $this->assertEquals("2020-06-02 10:46:46", $result);
    }
}
