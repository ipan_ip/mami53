<?php

namespace App\Transformers\Room;

use App\Entities\Activity\PhotoBy;
use App\Entities\Agent\Agent;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Media\Media;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\RoomOwner;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class AdminIndexTransformerTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new AdminIndexTransformer();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTransformWithPhotoIdIsNull()
    {
        $room = factory(Room::class)->create([
            'photo_id' => null,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'photo_id' => $room->photo_id,
            'caution' => true,
            'caution_messages' => ['Properti ini tidak mempunyai foto cover! Silakan set melalui tombol <strong><i class="fa fa-cog"></i> > Edit Media</strong> atau <strong><i class="fa fa-cog"></i> > Edit Booking Media</strong>'],
            'photo_url' => null,
            'photo_url_large' => null,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithPhotoIdIsNotNull()
    {
        $photo = factory(Media::class)->create([]);
        $room = factory(Room::class)->create([
            'photo_id' => $photo->id,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'photo_id' => $room->photo_id,
            'caution' => false,
            'caution_messages' => [],
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $this->assertNotNull($result['photo_url_large']);
        $this->assertNotNull($result['photo_url']);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithKostUpdatedDateIsNull()
    {
        $room = factory(Room::class)->create([
            'created_at' => now(),
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'updated_at' => $room->created_at->format('d-m-Y H:i:s'),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithKostUpdatedDateIsNotNull()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'kost_updated_date' => now(),
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'updated_at' => $room->kost_updated_date->format('d-m-Y H:i:s'),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithIsActiveIsFalse()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_active' => 'false',
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'is_active' => $room->is_active,
            'url_verify' => URL::route('admin.room.verify.action', [$room->id]),
            'justverify' => URL::route('admin.room.justverify.action', [$room->id]),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithIsActiveIsNotFalse()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_active' => 'true',
        ]);

        $expectedResult = [
            'id' => $room->id,
            'is_active' => $room->is_active,
            'song_id' => $room->song_id,
            'url_verify' => URL::route('admin.room.unverify.action', [$room->id]),
            'justverify' => URL::route('admin.room.justverify.action', [$room->id]),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithIsVerifiedByMamikosIs1()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_verified_by_mamikos' => 1,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'url_verified_by_mamikos' => URL::route('admin.room.unverify_mamikos.action', [$room->id]),
            'verified_by_mamikos' => ($room->is_verified_by_mamikos == 1) ? true : false,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithIsVerifiedByMamikosIsNot1()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_verified_by_mamikos' => 0,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'url_verified_by_mamikos' => URL::route('admin.room.verify_mamikos.action', [$room->id]),
            'verified_by_mamikos' => ($room->is_verified_by_mamikos == 1) ? true : false,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithStatusIs0()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'status' => 0,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'status' => $room->status,
            'url_available' => URL::route('admin.room.full', [$room->id]),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithStatusIsNot0()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'status' => 1,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'url_available' => URL::route('admin.room.available', [$room->id]),
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithHiddenByThanosIsNullRoomActiveIsTrueAndKosStatusesIsNotAdd()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_active' => 'true',
        ]);
        $expectedResult = [
            'id' => $room->id,
            'is_active' => $room->is_active,
            'song_id' => $room->song_id,
            'url_verify' => URL::route('admin.room.unverify.action', [$room->id]),
            'hidden_by_thanos' => $room->hidden_by_thanos == null ? false : true,
            'can_be_thanosed' => true,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithHiddenByThanosIsNullRoomActiveIsTrueAndIncompleteRoomIsFalse()
    {
        $room = factory(Room::class)->create([
            'created_at' => Carbon::now()->subDays(30),
            'is_active' => 'true',
            'owner_name' => 'owner name',
            'owner_phone' => '081234566',
            'manager_name' => '081111111',
            'manager_phone' => '081222222',
            'latitude' => 0.0,
            'longitude' => 0.0,
            'address' => 'address',
            'room_available' => 5,
            'room_count' => 5,
            'gender' => 1,
            'photo_count' => 5,
            'apartment_project_id' => null,
        ]);

        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);

        $tag = factory(Tag::class)->create();
        factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $room->id,
        ]);

        $expectedResult = [
            'id' => $room->id,
            'is_active' => $room->is_active,
            'song_id' => $room->song_id,
            'url_verify' => URL::route('admin.room.unverify.action', [$room->id]),
            'hidden_by_thanos' => $room->hidden_by_thanos == null ? false : true,
            'can_be_thanosed' => true,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithOwnersIsNotNull()
    {
        $room = factory(Room::class)->create([]);
        $user = factory(User::class)->create([]);
        factory(RoomOwner::class)->create([
            'user_id' => $user,
            'designer_id' => $room->id,
        ]);
        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'check_owner' => 1,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithOwnersIsNull()
    {
        $room = factory(Room::class)->create([]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'check_owner' => 0,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);

        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithPhotoByIsNotNull()
    {
        $room = factory(Room::class)->create([]);
        factory(PhotoBy::class)->create([
            'identifier' => $room->id
        ]);
        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'photo_by_url' => $room->photo_by[0]->photo,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithPhotoByIsNull()
    {
        $room = factory(Room::class)->create([]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'photo_by_url' => null,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithAgentRelationship()
    {
        $agent = factory(Agent::class)->create();
        $room = factory(Room::class)->create([
            'agent_id' => $agent->id
        ]);

        $expectedResult = [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'agent_name' => $room->agents->name,
        ];
        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testTransformWithoutAgentRelationship()
    {
        $room = factory(Room::class)->create([]);

        $expectedResult = [
            'agent_name' => $room->agent_name,
        ];

        $expectedResult = array_merge($expectedResult,$this->getTransformerResultData($room));

        $result = $this->class->transform($room);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    private function getTransformerResultData(Room $room)
    {
        $videoTourUrl = $room->getVideoUrlFromMamicheckerAttribute();

        return [
            'id' => $room->id,
            'song_id' => $room->song_id,
            'apartment_project_id' => $room->apartment_project_id,
            'name' => $room->name,
            'photo_id' => $room->photo_id,
            'price_monthly' => $room->price()->priceTitleFormats(2, true),
            'owner_phone' => $room->getOriginal('owner_phone'),
            'apartment_project_slug' => !is_null($room->apartment_project_id) ? $room->apartment_project->slug : '',
            'manager_phone' => $room->manager_phone,
            'view_count' => $room->view_count + $room->web_view_count,
            'match_count' => $room->match_count,
            'agent_status' => $room->agent_status,
            'verificator' => $room->verificator,
            'latitude' => $room->latitude,
            'longitude' => $room->longitude,
            'attachment_id' => ($room->attachment ? $room->attachment->id : null),
            'room_available' => $room->room_available,
            'area_city' => $room->area_city,
            'is_booking' => $room->is_booking == 0 ? false : true,
            'deleted_at' => $room->deleted_at,
            'comment_count' => 0,
            'share_count' => 0,
            'expired_phone' => $room->expired_phone,
            'slug' => $room->slug,
            'url_set_status_add' => URL::route('admin.room.setstatusadd.action', [$room->id]),
            'thanos_url' => URL::route('admin.room.snap.action', [$room->id]),
            'unthanos_url' => URL::route('admin.room.revertSnap.action', [$room->id]),
            'created_at' => $room->created_at->format("d-m-Y H:i:s"),
            'justverify' => URL::route('admin.room.justverify.action', [$room->id]),
            'by_mamichecker' => is_null($videoTourUrl) ? false : true,
            'last_update_from' => $room->lastupdate_from,
            'agent_note' => $room->agent_note,
            'is_premium_owner' => $room->getIsOwnedByPremiumOwner(),
            'sort_score' => $room->sort_score,
            'level' => $room->level->first(),
            'is_premium_photo' => $room->isPremiumPhoto() ? 1 : 0,
            'is_premium_photo_available' => $room->hasPremiumPhotos() ? 1 : 0,
            'unique_code' => $room->getUniqueCode(),
            'is_mamirooms' => ($room->is_mamirooms),
            'is_testing' => ($room->is_testing),
            'is_premium' => ($room->is_promoted == 'true' ? true : false),
			"has_room_types"  => (int) $room->types_count > 0,
			"facility_count"  => $room->facilities->count(),
			"is_facility_set" => $room->facility_setting_count > 0,
        ];
    }
}
