<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Transformers\Room\DetailTransformer;

class DetailTransformerTest extends MamiKosTestCase
{
    
    public function testGetUpdateAt()
    {
        $room = factory(Room::class)->create(["created_at" => "2020-06-02 09:39:46"]);
        $detailTransformer = $this->app->make(DetailTransformer::class);

        $method = $this->getNonPublicMethodFromClass(DetailTransformer::class, 'getUpdatedAt');
        $result = $method->invokeArgs($detailTransformer, [$room]);

        $this->assertEquals("2020-06-02 09:39:46", $result);
    }

    public function testGetUpdateAtWithKostUpdateAt()
    {
        $room = factory(Room::class)->create(["kost_updated_date" => "2020-06-02 10:46:46"]);
        $detailTransformer = $this->app->make(DetailTransformer::class);

        $method = $this->getNonPublicMethodFromClass(DetailTransformer::class, 'getUpdatedAt');
        $result = $method->invokeArgs($detailTransformer, [$room]);

        $this->assertEquals("2020-06-02 10:46:46", $result);
    }

}
