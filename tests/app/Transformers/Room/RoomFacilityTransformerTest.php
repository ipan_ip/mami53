<?php

namespace App\Transformers\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Facility\FacilityType;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFacility;
use App\Test\MamiKosTestCase;
use App\User;

class RoomFacilityTransformerTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->roomFacilityTransformer = new RoomFacilityTransformer;
    }

    public function testCombinePhotoSimilarTagWithoutFacilityName()
    {
        $data = [
            [
                'facility_name' => []
            ]
        ];

        $reflection = new \ReflectionClass(RoomFacilityTransformer::class);
        $method = $reflection->getMethod('combinePhotoOnSimilarTag');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->roomFacilityTransformer, [$data]);

        $this->assertEquals($data[0]['facility_name'], $result[0]['facility_name']);
    }

    public function testTransformWithRoomFaclities()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();
        $category = factory(FacilityCategory::class)->create([
            'name' => RoomFacilityTransformer::CATEGORY_NAME
        ]);
        $facilityType = factory(FacilityType::class)->create([
            'facility_category_id' => $category->id,
            'creator_id' => $user->id
        ]);
        $tag = factory(Tag::class)->create([
            'facility_type_id' => $facilityType->id
        ]);
        $photo = factory(Media::class)->create();
        $roomFacility = factory(RoomFacility::class)->create([
            'designer_id' => $room->id,
            'tag_id' => $tag->id,
            'creator_id' => $user->id,
            'photo_id' => $photo->id
        ]);

        $result = $this->roomFacilityTransformer->transform($room);
        $this->assertEquals($category->id, $result[0]['category_id']);
        $this->assertEquals($category->name, $result[0]['category_name']);
        $this->assertEquals($tag->name, $result[0]['photos'][0]['facility_name']);
        $this->assertEquals($photo->getMediaUrl()['medium'], $result[0]['photos'][0]['photo_urls'][0]);
    }

    public function testTransformWithoutRoomFaclities()
    {
        $room = factory(Room::class)->create();
        factory(FacilityCategory::class)->create([
            'name' => RoomFacilityTransformer::CATEGORY_NAME
        ]);

        $result = $this->roomFacilityTransformer->transform($room);
        $this->assertEmpty($result);
    }
}
