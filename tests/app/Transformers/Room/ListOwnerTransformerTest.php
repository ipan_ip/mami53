<?php

namespace App\Transformers\Room;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Cache;

class ListOwnerTransformerTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->listOwnerTransformer = new ListOwnerTransformer;
    }

    public function testTransformListOwner()
    {
        $room = factory(Room::class)->create([
            'photo_id' => 1,
            'unit_type' => '1-Room Studio',
        ]);
        
        $key = 'popupallocated:' . $room->song_id;
        Cache::forever($key, 'value');
        
        $result = $this->listOwnerTransformer->transform($room);
        $this->assertEquals('array', gettype($result));
    }

    public function testmapPopoverData()
    {
        $reflection = new \ReflectionClass(ListOwnerTransformer::class);
        $method = $reflection->getMethod('mapPopoverData');
        $method->setAccessible(true);

        $data = [
            'popover_title' => 'title',
            'description_1' => 'desription 1',
            'description_2' => 'description 2',
            'total' => 10
        ];

        $formatedTotal = number_format($data['total'], 0, '.', '.');

        $expectedResult = [
            [
                'title' => $data['popover_title'],
                'content' => $data['description_1'] . $data['description_2'],
                'saldo' => $formatedTotal,
                'list' => null,
            ],
            [
                'title' => 'Alokasi saldo berhasil !',
                'content' => 'Semua saldo dialokasikan ke ' . $data['description_2'],
                'saldo' => $formatedTotal,
                'list' => null,
            ],
            [
                'title' => 'Selamat Iklan Anda Sudah Aktif !',
                'content' => 'Anda dapat aktifkan iklan disini',
                'saldo' => $formatedTotal,
                'list' => null,
            ]
        ];

        $result = $method->invokeArgs($this->listOwnerTransformer, [$data]);
        $this->assertEquals($result, $expectedResult);
    }

    public function testgetFromDetailCacheWithValidCacheShouldReturnmapProverData()
    {
        $reflection = new \ReflectionClass(ListOwnerTransformer::class);
        $method = $reflection->getMethod('getFromDetailCache');
        $method->setAccessible(true);

        $key = 'key';
        $cacheKey = 'detailpopover:' . $key;
        Cache::shouldReceive('has')->with($cacheKey)->once()->andReturn(true);

        $data = [
            'title' => 'title',
            'description_1' => 'desription 1',
            'description_2' => 'description 2',
            'saldo_total' => 10
        ];

        Cache::shouldReceive('get')->with($cacheKey)->once()->andReturn($data);

        $formatedTotal = number_format($data['saldo_total'], 0, '.', '.');

        $expectedResult = [
            [
                'title' => $data['title'],
                'content' => $data['description_1'] . $data['description_2'],
                'saldo' => $formatedTotal,
                'list' => null,
            ],
            [
                'title' => 'Alokasi saldo berhasil !',
                'content' => 'Semua saldo dialokasikan ke ' . $data['description_2'],
                'saldo' => $formatedTotal,
                'list' => null,
            ],
            [
                'title' => 'Selamat Iklan Anda Sudah Aktif !',
                'content' => 'Anda dapat aktifkan iklan disini',
                'saldo' => $formatedTotal,
                'list' => null,
            ]
        ];

        $result = $method->invokeArgs($this->listOwnerTransformer, [$key]);
        $this->assertEquals($result, $expectedResult);
    }

    public function testgetFromDetailCacheWithInalidCacheShouldReturnNull()
    {
        $reflection = new \ReflectionClass(ListOwnerTransformer::class);
        $method = $reflection->getMethod('getFromDetailCache');
        $method->setAccessible(true);

        $key = 'key';
        $cacheKey = 'detailpopover:' . $key;
        Cache::shouldReceive('has')->with($cacheKey)->once()->andReturn(false);

        $expectedResult = null;

        $result = $method->invokeArgs($this->listOwnerTransformer, [$key]);
        $this->assertEquals($result, $expectedResult);
    }
}
