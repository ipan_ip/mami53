<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Booking\BookingDiscount;
use App\Test\MamiKosTestCase;

class ListingOwnerUpdateTransformerTest extends MamiKosTestCase
{
    
    public function testTransformIsNotApartment()
    {
        $room = factory(Room::class)->make();
        
        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $this->assertEquals(false, $transformedRoom['is_apartment']);
    }

    public function testTransformIsApartment()
    {
        $room = factory(Room::class)->make([
            'apartment_project_id' => 1
        ]);

        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $this->assertEquals(true, $transformedRoom['is_apartment']);
    }

    public function testTransformWithWithPricePer3Month()
    {
        $room = factory(Room::class)->create([
            'price_quarterly' => 2000000
        ]);

        $room = Room::find($room->id);

        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $this->assertEquals(2000000, $transformedRoom['price_3_month']);
    }

    public function testTransformWithoutDiscount()
    {
        $room = factory(Room::class)->make();

        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $this->assertEquals('', $transformedRoom['warning_message']);
    }

    public function testTransformWithDiscount()
    {
        $room = Factory(Room::class)->create();

        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'price' => 100000,
            'is_active' => true
        ]);

        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $this->assertNotEquals('', $transformedRoom['warning_message']);
    }

    public function testTransformWithValidResponsesStructure()
    {
        $room = Factory(Room::class)->create();

        $transformedRoom = (new ListingOwnerUpdateTransformer)->transform($room);

        $keys = [
            '_id','is_apartment',"price_type",'room_title','price_daily','price_weekly',
            'price_monthly','price_yearly','price_3_month','price_6_month',
            'room_available','size','room_total','inside_bathroom','outside_bathroom',
            'with_listrik','without_listrik',"photo_url","gender","min_month",'status',
            'status-title','available_room',"warning_message","is_price_flash_sale"   
        ];

        foreach($keys as $key){
            $this->assertArrayHasKey($key,$transformedRoom);
        } 

    }
}
