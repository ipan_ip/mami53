<?php

namespace App\Transformers\Room;

use App\Test\MamiKosTestCase;
use App\Transformers\Room\GoldplusRoomTransformer;
use App\Entities\Room\Room;
use App\Http\Helpers\GoldplusRoomHelper;

class GoldplusRoomTransformerTest extends MamiKosTestCase
{
    /** @var GoldplusRoomTransformer */
    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->app->make(GoldplusRoomTransformer::class);
    }

    /**
     * @group UG
     * @group UG-2624
     * @group App\Transformers\Room\GoldplusRoomTransformer
     */
    public function testTransformNullRoom()
    {
        $this->assertEquals([], $this->transformer->transform(null));
    }

    /**
     * @group UG
     * @group UG-2624
     * @group App\Transformers\Room\GoldplusRoomTransformer
     */
    public function testTransformNotNullRoom()
    {
        /** @var Room */
        $room = factory(Room::class)->make();
        $this->assertEquals([
            'id' => $room->song_id,
            'room_title' => $room->name,
            'area_formatted' => $room->area_formatted,
            'address' => $room->address,
            'photo' => [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ],
            'gender' => (int) $room->gender,
            'gp_status' => [
                'key' => '',
                'value' => '',
            ],
            'statistic_url' => $this->executeGetAutoLoginStatisticUrl($room->song_id),
        ], $this->transformer->transform($room));
    }

    public function executeGetAutoLoginStatisticUrl(int $songId): string
    {
        return $this->callNonPublicMethod(GoldplusRoomTransformer::class, 'getAutoLoginStatisticUrl', [
            GoldplusRoomHelper::getGoldplusStatisticWebUrl($songId)
        ]);
    }

    /**
     * @group UG
     * @group UG-4489
     * @group App\Transformers\Room\GoldplusRoomTransformer
     */
    public function testStatisticUrl()
    {
        /** @var Room */
        $room = factory(Room::class)->make();
        $statisticUrl = $this->executeGetAutoLoginStatisticUrl($room->song_id);
        $this->assertNotEmpty($statisticUrl);
        $this->assertContains(
            GoldplusRoomHelper::getGoldplusStatisticWebUrl($room->song_id),
            urldecode($statisticUrl)
        );
        $this->assertContains(route('webowner.issue.token.for.webview'), $statisticUrl);
    }
}
