<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ExistingInputTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $transformer = new ExistingInputTransformer();
        $kost = factory(Room::class)->create(['is_active' => 'true']);

        $actual = $transformer->transform($kost);
        $expected = [
            "_id" => $kost->song_id,
            'name' => $kost->name,
            'share_url' => $kost->share_url,
            'photo' => null
        ];

        $this->assertEquals($expected, $actual);
    }
}
