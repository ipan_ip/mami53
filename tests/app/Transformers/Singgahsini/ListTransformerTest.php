<?php

namespace App\Transformers\Singgahsini;

use App\Constants\Periodicity;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;

class ListTransformerTest extends MamiKosTestCase
{
    private $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testTransform()
    {
        $priceMonthly = 500000;
        $expectedPriceMonthly = '500.000';
        $media = factory(Media::class)->create();
        $expectedRealImageUrl = 'https://static.mamikos.com/public/storage/images/'.$media->file_name;
        $room = factory(Room::class)->states('with-slug')->create([
            'price_monthly' => $priceMonthly
        ]);
        $expectedShareUrl = 'https://mamikos.com/room/'.$room->slug;
        factory(Card::class, 3)->create([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
            'designer_id' => $room->id,
            'photo_id' => $media->id
        ]);

        $res = (new ListTransformer)->transform($room);

        $this->assertIsArray($res);
        $this->assertSame($room->song_id, $res['_id']);
        $this->assertSame($expectedRealImageUrl, $res['photo_url']['real']);
        $this->assertSame($room->name, $res['room-title']);
        $this->assertSame($room->area_city, $res['city']);
        $this->assertSame($room->area_subdistrict, $res['subdistrict']);
        $this->assertSame($expectedPriceMonthly, $res['price']['price']);
        $this->assertSame('bulanan', $res['rentType']);
        $this->assertSame($expectedShareUrl, $res['share_url']);
    }

    public function testGetCoverImagePremium_ImageCoverAvailable()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $cardPremium = factory(CardPremium::class, 3)->make([
            'designer_id' => $room->id,
            'description' => 'foto-kamar-as-cover'
        ])->each(function ($card) use($media) {
            $card->photo_id = $media->id;
            $card->save();
        });
        $room->premium_cards = $cardPremium;

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getCoverImage', [$room]);

        $this->assertIsArray($res);
    }

    public function testGetCoverImagePremium_ImageCoverNotAvailable()
    {
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create()->id
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getCoverImage', [$room]);

        $this->assertIsArray($res);
    }

    public function testGetCoverImage_ImageCoverAvailable()
    {
        $room = factory(Room::class)->create();
        factory(Card::class, 3)->create([
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
            'designer_id' => $room->id,
            'photo_id' => factory(Media::class)->create()->id
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getCoverImage', [$room]);

        $this->assertIsArray($res);
    }

    public function testGetCoverImage_ImageCoverNotAvailable()
    {
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create()->id
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getCoverImage', [$room]);

        $this->assertIsArray($res);
    }

    public function testGetAvailablePrice_Monthly()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 2);
        $this->assertSame($res->typeString, Periodicity::BULANAN);
    }

    public function testGetAvailablePrice_Yearly()
    {
        $room = factory(Room::class)->create([
            'price_yearly' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 3);
        $this->assertSame($res->typeString, Periodicity::TAHUNAN);
    }

    public function testGetAvailablePrice_Weekly()
    {
        $room = factory(Room::class)->create([
            'price_weekly' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 1);
        $this->assertSame($res->typeString, Periodicity::MINGGUAN);
    }

    public function testGetAvailablePrice_Daily()
    {
        $room = factory(Room::class)->create([
            'price_daily' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 0);
        $this->assertSame($res->typeString, Periodicity::HARIAN);
    }

    public function testGetAvailablePrice_Quarterly()
    {
        $room = factory(Room::class)->create([
            'price_quarterly' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 4);
        $this->assertSame($res->typeString, 'Per 3 Bulan');
    }

    public function testGetAvailablePrice_Annually()
    {
        $room = factory(Room::class)->create([
            'price_semiannually' => 100000
        ]);

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 5);
        $this->assertSame($res->typeString, 'Per 6 Bulan');
    }

    public function testGetAvailablePrice_Empty()
    {
        $room = factory(Room::class)->create();

        $res = $this->callNonPublicMethod(ListTransformer::class, 'getAvailablePrice', [$room]);

        $this->assertIsObject($res);
        $this->assertSame($res->typeInt, 2);
        $this->assertSame($res->typeString, Periodicity::BULANAN);
    }
}