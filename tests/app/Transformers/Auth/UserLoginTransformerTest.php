<?php

namespace App\Transformers\Auth;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class UserLoginTransformerTest extends MamiKosTestCase
{

    use WithFaker;

    public function testTransform()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true',
            'token' => str_random(60)
        ]);

        $this->withSession(['url_previous' => config('app.url')])->get('/');

        $transform = (new UserLoginTransformer())->transform($user);
        $this->assertIsArray($transform);
        $this->assertEquals($user->id, $transform['auth']['user_id']);
        $this->assertEquals($user->token, $transform['auth']['user_token']);
        $this->assertEquals($user->email, $transform['profile']->email);
        $this->assertEquals(config('app.url'), $transform['redirect_path']);
    }

    public function testTransformWithoutSessionPrevUrl()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true',
            'token' => str_random(60)
        ]);

        $transform = (new UserLoginTransformer())->transform($user);
        $this->assertIsArray($transform);
        $this->assertEquals(config('app.url'), $transform['redirect_path']);
    }

    public function testTransformWithRefererHeader()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true',
            'token' => str_random(60)
        ]);
        $currentUrl = $this->faker->url;

        $request = request();
        $request->headers->set('referer', $currentUrl);

        $transformer = new UserLoginTransformer();
        $transformer->getUrlGenerator()->setRequest($request);

        $transform = $transformer->transform($user);
        $this->assertIsArray($transform);
        $this->assertEquals($currentUrl, $transform['redirect_path']);
    }
}
