<?php

namespace App\Transformers\Notification;

use App\Entities\User\Notification;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class NotificationTransformerTest extends MamiKosTestCase
{
    private $model;

    protected function setUp() : void
    {
        parent::setUp();

        $this->model = $this->createMock(Notification::class);
    }

    public function testTransform_UrlNull_Success()
    {
        $carbon = $this->createMock(Carbon::class);
        $carbon->method('format')->willReturn($this->anything());
        $this->model->method('__get')->will($this->returnCallback(function ($key) use ($carbon) {
            if ($key === 'created_at') {
                return $carbon;
            }
            if ($key === 'category') {
                return null;
            }
            if ($key === 'url') {
                return null;
            }
            return $this->anything();
        }));

        $transformer = new NotificationTransformer();
        $result = $transformer->transform($this->model);
        $this->assertIsArray($result);
    }

    public function testTransform_UrlNotValid_Success()
    {
        $carbon = $this->createMock(Carbon::class);
        $carbon->method('format')->willReturn($this->anything());
        $this->model->method('__get')->will($this->returnCallback(function ($key) use ($carbon) {
            if ($key === 'created_at') {
                return $carbon;
            }
            if ($key === 'category') {
                return null;
            }
            if ($key === 'url') {
                return 'asd';
            }
            return $this->anything();
        }));

        $transformer = new NotificationTransformer();
        $result = $transformer->transform($this->model);
        $this->assertIsArray($result);
    }

    public function testTransform_UrlValid_Success()
    {
        $carbon = $this->createMock(Carbon::class);
        $carbon->method('format')->willReturn($this->anything());
        $this->model->method('__get')->will($this->returnCallback(function ($key) use ($carbon) {
            if ($key === 'created_at') {
                return $carbon;
            }
            if ($key === 'category') {
                return null;
            }
            if ($key === 'url') {
                return 'http://mamikos.com';
            }
            return $this->anything();
        }));

        $transformer = new NotificationTransformer();
        $result = $transformer->transform($this->model);
        $this->assertIsArray($result);
    }
}
