<?php

namespace App\Transformers\Notification;

use App\Entities\Notif\Category;
use App\Test\MamiKosTestCase;

class CategoryTransformerTest extends MamikosTestCase
{
    private $model;

    protected function setUp() : void
    {
        $this->model = $this->createMock(Category::class);
    }

    public function testTransform()
    {
        $transformer = new CategoryTransformer();
        $result = $transformer->transform($this->model);
        $this->assertIsArray($result);
    }
}
