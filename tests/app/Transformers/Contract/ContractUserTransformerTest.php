<?php

namespace App\Transformers\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractUserTransformerTest extends MamiKosTestCase
{
    protected $transform;

    const ROOM_RATING = 4.7;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractUserTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testContractUserCanBeTransformAllFieldExist(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $contractEntity = factory(MamipayContract::class)->create();
        factory(MamipayContractKost::class)->create([
            'designer_id' => $roomEntity->id,
            'contract_id' => $contractEntity->id
        ]);

        // mock rating helper
        // $this->mockRating();

        // run test
        $transform = $this->transform->transform($contractEntity);
        foreach ($this->fieldMustExist() as $field) {
            $this->assertArrayHasKey($field, $transform);
        }
    }

    public function testGetReviewDataSuccess(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserTransformer::class,
            'getReviewData'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );
        $this->assertNull($result);
    }

    public function testGetReviewRatingIsNull(): void
    {
        // prepare data
        $room = factory(Room::class)->make();

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserTransformer::class,
            'getReviewRating'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$room]
        );
        $this->assertNull($result);
    }

    private function fieldMustExist()
    {
        return [
            'id',
            'start_date',
            'end_date',
            'duration',
            'rent_count_type',
            'duration_string',
            'room',
            'review',
        ];
    }
}