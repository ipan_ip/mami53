<?php

namespace App\Transformers\Contract;

use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractAdminListTransformerTest extends MamiKosTestCase
{
    const WHATSAPP_API_URL = 'https://api.whatsapp.com/send?phone={phone}';

    protected $transform;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractAdminListTransformer::class);
    }

    public function testContractAdminListCanBeTransformAllFieldExist(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->make();

        // run test
        $transform = (new ContractAdminListTransformer)->transform($contractEntity);

        // test
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('status', $transform);
        $this->assertArrayHasKey('duration_formatted', $transform);
        $this->assertArrayHasKey('start_date', $transform);
        $this->assertArrayHasKey('end_date', $transform);
        $this->assertArrayHasKey('room', $transform);
        $this->assertArrayHasKey('room_unit', $transform);
        $this->assertArrayHasKey('room_owner', $transform);
        $this->assertArrayHasKey('tenant', $transform);
        $this->assertArrayHasKey('room_level', $transform);
        $this->assertArrayHasKey('room_unit_level', $transform);
        $this->assertArrayHasKey('is_from_booking', $transform);
        $this->assertArrayHasKey('is_from_consultant', $transform);
        $this->assertArrayHasKey('is_from_owner', $transform);
        $this->assertArrayHasKey('booking_user', $transform);
        $this->assertArrayHasKey('room_number', $transform);
    }

    public function testGetRoomOwnerDataSuccess(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->make();

        $method = $this->getPrivateFunction('getRoomOwnerData');
        $result = $method->invokeArgs($this->transform, [$roomEntity]);

        // run test
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('phone_number', $result);
    }

    public function testGetBookingUserDataSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->make();

        $method = $this->getPrivateFunction('getBookingUserData');
        $result = $method->invokeArgs($this->transform, [$contractEntity]);

        // run test
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('reject_reason', $result);
    }

    public function testGetTenantWhatsappPhoneSuccess(): void
    {
        // prepare data
        $phoneOriginal = '08223416799';
        $phoneInternationalFormat = '628223416799';
        $urlFormatted = str_replace('{phone}', $phoneInternationalFormat, self::WHATSAPP_API_URL);

        $method = $this->getPrivateFunction('getTenantWhatsappPhone');
        $result = $method->invokeArgs($this->transform, [$phoneOriginal]);

        // run test
        $this->assertEquals($urlFormatted, $result);
    }

    public function testGetRoomNumberSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $contractTypeKostEntity = factory(MamipayContractKost::class)->create([
            'contract_id' => $contractEntity->id,
            'room_number' => 4
        ]);

        $method = $this->getPrivateFunction('getRoomNumber');
        $result = $method->invokeArgs($this->transform, [$contractEntity]);

        // run test
        $this->assertEquals($contractTypeKostEntity->room_number, $result);
    }

    public function testGetRoomLevelSuccess(): void
    {
        // create room data
        $roomEntity = factory(Room::class)->create();
        $levelEntity = factory(KostLevel::class)->create(['name' => 'Golplus']);
        // add level to kost / room
        $roomEntity->changeLevel($levelEntity->id);

        $method = $this->getPrivateFunction('getRoomLevel');
        $result = $method->invokeArgs($this->transform, [$roomEntity]);

        // run test
        $this->assertEquals($levelEntity->name, $result['name']);
    }

    private function getPrivateFunction($function)
    {
        $method = $this->getNonPublicMethodFromClass(
            ContractAdminListTransformer::class,
            $function
        );
        return $method;
    }
}
