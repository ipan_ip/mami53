<?php

namespace App\Transformers\Contract;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserFlashSale;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ContractUserDetailTransformerTest extends MamiKosTestCase
{
    protected $transform;

    const ROOM_RATING = 4.7;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transform = $this->app->make(ContractUserDetailTransformer::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testContractUserDetailCanBeTransformAllFieldExist(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $contractEntity = factory(MamipayContract::class)->create();
        factory(MamipayContractKost::class)->create([
            'designer_id' => $roomEntity->id,
            'contract_id' => $contractEntity->id
        ]);

        // run test
        $transform = $this->transform->transform($contractEntity);
        foreach ($this->fieldMustExist() as $field) {
            $this->assertArrayHasKey($field, $transform);
        }
    }

    public function testGetReviewDataSuccess(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getReviewData'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );
        $this->assertNull($result);
    }

    public function testGetReviewRatingIsNull(): void
    {
        // prepare data
        $room = factory(Room::class)->make();

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getReviewRating'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$room]
        );
        $this->assertNull($result);
    }

    public function testGetRoomNumberSuccess(): void
    {
        // prepare data
        $contract = factory(MamipayContract::class)->create();
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'room_number' => 1
        ]);

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getRoomNumber'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$contract]
        );
        $this->assertSame('1', $result);
    }

    public function testGetInternalMamipayContractSuccess(): void
    {
        // prepare data
        $contract = factory(MamipayContract::class)->make([
            'id' => 1
        ]);

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getInternalMamipayContract'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$contract]
        );

        $this->assertNull($result);
    }

    public function testGetBookedDataSuccess(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getBookedData'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );

        $this->assertNull($result);
    }

    public function testGetChannelChatUrlSuccess(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getChannelChatUrl'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );

        $this->assertNull($result);
    }

    public function testGetTotalDiscountSuccess(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getTotalDiscount'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );

        $this->assertSame(0, $result);
    }

    public function testGetRoomPriceNull(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getRoomPrice'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );

        $rentTypes = ['daily', 'weekly', 'monthly', 'quarterly', 'semiannualy', 'yearly'];
        foreach ($rentTypes as $type) {
            $this->assertArrayHasKey($type, $result);
            $this->assertNull($result[$type]);
        }
    }

    public function testGetRoomPriceSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create([
            'price_daily' => 200000,
            'price_monthly' => 600000
        ]);
        $contract = factory(MamipayContract::class)->create();
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id
        ]);

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getRoomPrice'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$contract, null]
        );
        $this->assertSame($room->price_daily, $result['daily']);
        $this->assertSame($room->price_monthly, $result['monthly']);
    }

    public function testGetIsFlashSaleNull(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getFlashSale'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null]
        );

        $this->assertFalse($result['is_flash_sale']);
        $this->assertNull($result['flash_sale']);
    }

    public function testGetIsFlashSaleSuccess(): void
    {
        // prepare data
        $flashSaleEntity = factory(FlashSale::class)->create();
        $bookingUserEntity = factory(BookingUser::class)->create([
            'price' => 50000
        ]);
        factory(BookingUserFlashSale::class)->create([
            'booking_user_id' => $bookingUserEntity->id,
            'flash_sale_id' => $flashSaleEntity->id
        ]);

        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getFlashSale'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [$bookingUserEntity]
        );

        $this->assertTrue($result['is_flash_sale']);
        $this->assertArrayHasKey('rent_price', $result['flash_sale']);
        $this->assertArrayHasKey('discount', $result['flash_sale']);
        $this->assertArrayHasKey('price_after_discount', $result['flash_sale']);
        $this->assertArrayHasKey('percentage', $result['flash_sale']);
    }

    public function testGetRoomFacilityNull(): void
    {
        // get method
        $method = $this->getNonPublicMethodFromClass(
            ContractUserDetailTransformer::class,
            'getRoomFacility'
        );

        // run test
        $result = $method->invokeArgs(
            $this->transform,
            [null, null]
        );
        $facilities = ['top_facilities', 'fac_room', 'fac_share', 'fac_bath', 'fac_near', 'fac_park', 'fac_price'];
        foreach ($facilities as $facility) {
            $this->assertArrayHasKey($facility, $result);
            $this->assertNull($result[$facility]);
        }
    }

    private function fieldMustExist()
    {
        return [
            'id',
            'booking_code',
            'description',
            'start_date',
            'end_date',
            'duration',
            'rent_count_type',
            'duration_string',
            'contract_type',
            'total_contract_amount',
            'basic_amount',
            'total_discount',
            'already_booked',
            'room_active_booking',
            'group_channel_url',
            'is_flash_sale',
            'flash_sale',
            'review',
            'room',
            'owner',
            'tenant'
        ];
    }
}