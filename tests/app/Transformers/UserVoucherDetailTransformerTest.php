<?php

namespace App\Transformers;

use App;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherPublicCampaign;
use App\Test\MamiKosTestCase;

class UserVoucherDetailTransformerTest extends MamiKosTestCase
{
    
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = App::make(UserVoucherDetailTransformer::class);
    }

    public function testTransform_GetTransformedModelArray()
    {
        $mediaId = 1;
        $campaignId = 2;
        $voucherId = 3;
        factory(MamipayMedia::class)->create(['id' => $mediaId]);
        factory(MamipayVoucherPublicCampaign::class)->create(
            [
                'id' => $campaignId,
                'media_id' => $mediaId
            ]
        );
        $voucher = factory(MamipayVoucher::class)->create(
            [
                'id' => $voucherId,
                'public_campaign_id' => $campaignId,
                'for_booking' => 1,
                'for_recurring' => 1,
                'for_pelunasan' => 1,
                'applicable_type' => 'kost_id'
            ]
        );

        $result = $this->class->transform($voucher);

        $this->assertIsArray($result);
        $this->assertEquals($result['id'], $voucherId);
        $this->assertEquals(true, $this->getNonPublicAttributeFromClass('hasCampaign', $this->class));
        $this->assertEquals(true, $this->getNonPublicAttributeFromClass('hasCampaignMedia', $this->class));
    }
}
