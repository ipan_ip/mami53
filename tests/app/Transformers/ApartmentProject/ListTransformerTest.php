<?php

namespace App\Transformers\ApartmentProject;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Test\MamiKosTestCase;

class ListTransformerTest extends MamiKosTestCase
{
    protected $transformer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transformer = new ListTransformer();
    }

    public function testTransform(): void
    {
        $project = factory(ApartmentProject::class)->create();
        
        $result = $this->transformer->transform($project);

        $expected = [
            '_id' => $project->id,
            'name' => $project->name,
            'share_url' => $project->share_url,
            'photo' => (new ApartmentProjectStyle())->listPhoto($project),
            'code' => $project->project_code,
            'floors' => $project->floor,
            'city' => $project->area_city,
            'subdistrict' => $project->area_subdistrict,
            'unit_types' => $project->types->pluck('type_name')
        ];

        $this->assertEquals($expected, $result);
    }
}
