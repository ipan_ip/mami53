<?php

namespace App\Transformers\KostLevelValue;

use App\Entities\Level\KostLevelValue;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;

class KostLevelValueTransformerTest extends MamiKosTestCase
{
    
    /**
     * @group BG-3000
     */
    public function testTransform()
    {
        $media = factory(Media::class)->create();
        $kostValue = factory(KostLevelValue::class)->create([
            'image_id' => $media->id,
            'small_image_id' => $media->id,
        ]);

        $result = (new KostLevelValueTransformer)->transform($kostValue);

        $this->assertEquals($kostValue->id, $result['id']);
        $this->assertEquals($kostValue->name, $result['title']);
        $this->assertEquals($kostValue->description, $result['description']);
        $this->assertEquals($media->getMediaUrl()['real'], $result['icon_url']);
        $this->assertEquals($media->getMediaUrl()['real'], $result['icon_small_url']);
    }
    
    /**
     * @group BG-3000
     */
    public function testTransformWhenMediaNull()
    {
        $kostValue = factory(KostLevelValue::class)->create([
            'image_id' => 0,
            'small_image_id' => 0
        ]);

        $result = (new KostLevelValueTransformer)->transform($kostValue);

        $this->assertEquals($kostValue->id, $result['id']);
        $this->assertEquals($kostValue->name, $result['title']);
        $this->assertEquals($kostValue->description, $result['description']);
        $this->assertEquals(null, $result['icon_url']);
        $this->assertEquals(null, $result['icon_small_url']);
    }
}