<?php

namespace App\Transformers\CardPremium;

use App\Test\MamiKosTestCase;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\Element\CardPremium;
use App\Transformers\CardPremium\ListCardPremiumTransformer;

use Config;

class ListCardPremiumTransformerTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testListCardPremiumTransformerShouldBeValid()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id'  => $photo->id,
            'is_active' => true
        ]);
        $card   = factory(CardPremium::class)->create([
            'designer_id' => $room->id,
            'type'        => 'image',
            'photo_id'    => $photo->id,
        ]);

        $transform = (new ListCardPremiumTransformer)->transform($card);

        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name, $transform['photo_url']['real']);
        $this->assertequals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-240x320.jpg', $transform['photo_url']['small']);
        $this->assertequals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-360x480.jpg', $transform['photo_url']['medium']);
        $this->assertequals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-540x720.jpg', $transform['photo_url']['large']);
    }
}
