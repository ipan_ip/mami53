<?php

namespace App\Transformers\ActivityLog;

use App\Entities\Activity\ActivityLog;
use App\Test\MamiKosTestCase;
use App\User;

class ListTransformerTest extends MamiKosTestCase
{
    public function testTransform(): void
    {
        $user = factory(User::class)->create();
        $log = factory(ActivityLog::class)->make(['causer_id' => $user->id]);

        $transformer = new ListTransformer(collect([$user]));

        $expected = [
            'id' => $log->id,
            'user_name' => $user->name,
            'causer_id' => $log->causer_id,
            'log_name' => $log->log_name,
            'description' => $log->description,
            'subject_type' => $log->subject_type,
            'subject_id' => $log->subject_id,
            'created_at' => $log->created_at,
            'properties' => null,
            'ip' => ''
        ];

        $this->assertEquals($expected, $transformer->transform($log));
    }

    public function testTransformWithInvalidUser(): void
    {
        $log = factory(ActivityLog::class)->make();

        $transformer = new ListTransformer(collect([]));

        $expected = [
            'id' => $log->id,
            'user_name' => 'USER NOT FOUND',
            'causer_id' => $log->causer_id,
            'log_name' => $log->log_name,
            'description' => $log->description,
            'subject_type' => $log->subject_type,
            'subject_id' => $log->subject_id,
            'created_at' => $log->created_at,
            'properties' => null,
            'ip' => ''
        ];

        $this->assertEquals($expected, $transformer->transform($log));
    }
}
