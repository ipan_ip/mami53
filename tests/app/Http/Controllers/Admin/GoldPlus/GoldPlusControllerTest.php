<?php

namespace App\Http\Controllers\Admin\GoldPlus;

use App\Entities\GoldPlus\Package;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GoldPlusControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const GOLDPLUS_PACKAGE_BASE_URL = 'admin/gold-plus/package';
    private $user;

    protected function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testListPackageReturnSuccess(): void
    {
        factory(Package::class)->create();
        $this->actingAs($this->user);
        $response = $this->get(self::GOLDPLUS_PACKAGE_BASE_URL);
        $response->assertOk();
        $response->assertViewIs('admin.contents.gold-plus.package.index');
    }

    /**
     * @group UG
     * @group UG-4924
     */
    public function testEditPackageReturnSuccess(): void
    {
        $package = factory(Package::class)->create();
        $this->actingAs($this->user);
        $response = $this->get(self::GOLDPLUS_PACKAGE_BASE_URL . '/edit/' . $package->id);
        $response->assertOk();
        $response->assertViewIs('admin.contents.gold-plus.package.edit');
    }

    /**
     * @group UG
     * @group UG-4924
     */
    public function testUpdatePackageReturnSuccess(): void
    {
        $package = factory(Package::class)->create([
            'price' => 100
        ]);
        $data = [
            'price' => 200,
            'description' => $package->description
        ];
        $this->actingAs($this->user);
        $response = $this->put(self::GOLDPLUS_PACKAGE_BASE_URL . '/edit/' . $package->id, $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::GOLDPLUS_PACKAGE_BASE_URL);
        $this->assertDatabaseHas('goldplus_package', 
            [
                'id' => $package->id,
                'price' => $data['price'],
                'description' => $package->description
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4924
     */
    public function testUpdatePackageWithEmptyDescriptionReturnSuccess(): void
    {
        $package = factory(Package::class)->create([
            'price' => 100,
            'description' => $this->faker->sentence(2)
        ]);
        $data = [
            'price' => 100,
            'description' => ''
        ];
        $this->actingAs($this->user);
        $response = $this->put(self::GOLDPLUS_PACKAGE_BASE_URL . '/edit/' . $package->id, $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::GOLDPLUS_PACKAGE_BASE_URL);
        $this->assertDatabaseHas('goldplus_package', 
            [
                'id' => $package->id,
                'price' => $package->price,
                'description' => $data['description']
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4924
     */
    public function testUpdatePackageWithTooLongDescriptionReturnFalse(): void
    {
        $package = factory(Package::class)->create([
            'price' => 100,
            'description' => $this->faker->sentence(2)
        ]);
        $data = [
            'price' => 200,
            'description' => str_random(251)
        ];
        $this->actingAs($this->user);
        $response = $this->put(self::GOLDPLUS_PACKAGE_BASE_URL . '/edit/' . $package->id, $data);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['description']);
        $this->assertDatabaseHas('goldplus_package', 
            [
                'id' => $package->id,
                'price' => $package->price,
                'description' => $package->description
            ]
        );
    }
}
