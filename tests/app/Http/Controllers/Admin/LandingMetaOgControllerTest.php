<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Landing\LandingMetaOg;
use App\Repositories\Landing\LandingMetaOgRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\{
    LengthAwarePaginator,
    Paginator
};
use Illuminate\View\View as customView;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class LandingMetaOgControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const SAVE = 'garuda/stories/jobs/cv/save';
    private const UPDATE = 'garuda/stories/jobs/cv/update';
    private $repo, $faker, $controller;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->mockPartialAlternatively(LandingMetaOgRepositoryEloquent::class);
        $this->controller = app()->make(LandingMetaOgController::class);
        $this->faker = Faker::create();
    }

    public function testIndex_Success()
    {
        $length = 10;
        $perPage = 2;

        app()->user = factory(User::class)->make();
        $landingMetaOg = factory(LandingMetaOg::class, $length)->make();

        $landingMetaOg = new LengthAwarePaginator(
            $landingMetaOg->forPage(Paginator::resolveCurrentPage() , $perPage),
            $landingMetaOg->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        $this->repo->shouldReceive('index')->andReturn($landingMetaOg);

        $res = $this->controller->index();

        $this->assertSame($res->getData()['boxTitle'], $this->controller::INDEX_TITLE);
        $this->assertEquals($res->getData()['rows'], $landingMetaOg);
        $this->assertInstanceOf(customView::class, $res);
    }

    public function testCreate_Success()
    {
        app()->user = factory(User::class)->make();

        $res = $this->controller->create();

        $this->assertSame($res->getData()['boxTitle'], $this->controller::CREATE_TITLE);
        $this->assertInstanceOf(customView::class, $res);
    }

    public function testEdit_Success()
    {
        app()->user = factory(User::class)->make();
        $landingMetaOg = factory(LandingMetaOg::class)->make([
            'id' => mt_rand(1, 999999)
        ]);

        $this->repo->shouldReceive('findById')->with($landingMetaOg->id)->andReturn($landingMetaOg);

        $res = $this->controller->edit($landingMetaOg->id);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::EDIT_TITLE);
        $this->assertInstanceOf(customView::class, $res);
    }

    public function testSave_ValidatorImageCalled()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->paragraph(3),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $res = $this->controller->save($req);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['image']),
            $this->controller->validationMessages['image.url']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_Success()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('save')->with($req)->andReturn(true);
        $res = $this->controller->save($req);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::SAVE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_Fail()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('save')->with($req)->andReturn(false);
        $res = $this->controller->save($req);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::SAVE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_ValidatorImageCalled()
    {
        $landingMetaOg = factory(LandingMetaOg::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$landingMetaOg->id,
            'POST',
            [
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->paragraph(3),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $res = $this->controller->update($req, $landingMetaOg->id);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['image']),
            $this->controller->validationMessages['image.url']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_Success()
    {
        $landingMetaOg = factory(LandingMetaOg::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$landingMetaOg->id,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('edit')->with($req, $landingMetaOg->id)->andReturn(true);
        $res = $this->controller->update($req, $landingMetaOg->id);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_Fail()
    {
        $landingMetaOg = factory(LandingMetaOg::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$landingMetaOg->id,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('edit')->with($req, $landingMetaOg->id)->andReturn(false);
        $res = $this->controller->update($req, $landingMetaOg->id);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}