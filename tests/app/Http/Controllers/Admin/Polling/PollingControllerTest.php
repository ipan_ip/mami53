<?php

namespace app\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PollingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testIndexPolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);

        $response = $this->actingAs($user)->get(route('admin.polling.index'));
        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.polling.index');
        $response->assertSee('ini-key');
    }

    public function testCreatePolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.create'));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.polling.create');
        $response->assertViewHas('boxTitle');
    }

    public function testEditPolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $polling = factory(Polling::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.edit', $polling->id));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.polling.edit');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('polling');
    }

    public function testStorePolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $formData = [
            'key' => 'key-dummy',
            'descr' => 'descr-dummy',
            'is_active' => 1
        ];

        $response = $this->actingAs($user)->post(route('admin.polling.store'), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new Polling)->getTable(), $formData);
    }

    public function testUpdatePolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $formData = [
            'key' => 'key-dummy',
            'descr' => 'descr-dummy',
            'is_active' => 1
        ];
        
        $polling = factory(Polling::class)->create([
            'key' => 'asdf',
            'descr' => 'lorem',
            'is_active' => 0,
        ]);
        $response = $this->actingAs($user)->put(route('admin.polling.update', $polling->id), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseMissing((new Polling)->getTable(), ['key' => 'asdf']);
        $this->assertDatabaseHas((new Polling)->getTable(), $formData);
    }

    public function testDeletePolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $questionOption = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $responseDelete = $this->actingAs($user)->delete(route('admin.polling.destroy', $polling->id));
        $responseDelete->assertStatus(302);
        $responseDelete->assertSessionHas('message');
        
        $this->assertDatabaseMissing((new Polling)->getTable(), ['key' => 'ini-key', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'ini-question', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
