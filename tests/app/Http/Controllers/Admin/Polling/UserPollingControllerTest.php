<?php

namespace app\Http\Controllers\Admin\Polling;

use App\Entities\Polling\UserPolling;
use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UserPollingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testIndexUserPolling_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        factory(UserPolling::class)->create([
            'user_id' => $user->id,
            'answer' => 'oke gaes'
        ]);

        $response = $this->actingAs($user)->get(route('admin.polling.user.index'));
        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.user.index');
        $response->assertSee('oke gaes');
    }
}
