<?php

namespace app\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PollingQuestionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testIndexPollingQuestion_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        factory(PollingQuestion::class)->create([
            'question' => 'apa kabar?'
        ]);

        $response = $this->actingAs($user)->get(route('admin.polling.question.index'));
        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.question.index');
        $response->assertSee('apa kabar?');
    }

    public function testCreatePollingQuestion_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.question.create'));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.question.create');
        $response->assertViewHas('boxTitle');
    }

    public function testEditPollingQuestion_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $pollingQuestion = factory(PollingQuestion::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.question.edit', $pollingQuestion->id));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.question.edit');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('question');
    }

    public function testStorePollingQuestion_ShouldSuccess()
    {
        $polling = factory(Polling::class)->create();
        $user = factory(User::class)->create();
        $formData = [
            'polling_id' => $polling->id,
            'question' => 'dummy',
            'type' => PollingQuestion::TYPE_SELECTIONLIST,
            'is_active' => 1
        ];

        $response = $this->actingAs($user)->post(route('admin.polling.question.store'), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PollingQuestion)->getTable(), $formData);
    }

    public function testUpdatePollingQuestion_ShouldSuccess()
    {
        $polling = factory(Polling::class)->create();
        $user = factory(User::class)->create();
        $formData = [
            'polling_id' => $polling->id,
            'question' => 'dummy',
            'type' => PollingQuestion::TYPE_SELECTIONLIST,
            'is_active' => 1
        ];
        
        $pollingQuestion = factory(PollingQuestion::class)->create([
            'question' => 'asdf',
            'type' => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => 0,
        ]);
        $response = $this->actingAs($user)->put(route('admin.polling.question.update', $pollingQuestion->id), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'asdf']);
        $this->assertDatabaseHas((new PollingQuestion)->getTable(), $formData);
    }

    public function testDeletePollingQuestion_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $questionOption = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $responseDelete = $this->actingAs($user)->delete(route('admin.polling.question.destroy', $question->id));
        $responseDelete->assertStatus(302);
        $responseDelete->assertSessionHas('message');
        
        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'ini-question', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
