<?php

namespace app\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PollingOptionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testIndexPollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        factory(PollingQuestionOption::class)->create([
            'option' => 'oke gaes'
        ]);

        $response = $this->actingAs($user)->get(route('admin.polling.option.index'));
        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.option.index');
        $response->assertSee('oke gaes');
    }

    public function testCreatePollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.option.create'));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.option.create');
        $response->assertViewHas('boxTitle');
    }

    public function testEditPollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $pollingOption = factory(PollingQuestionOption::class)->create();

        $response = $this->actingAs($user)->get(route('admin.polling.option.edit', $pollingOption->id));

        $response->assertOk();
        $response->assertViewIs('admin.contents.polling.option.edit');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('option');
    }

    public function testStorePollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $question = factory(PollingQuestion::class)->create();
        $formData = [
            'question_id' => $question->id,
            'option' => 'dummy',
            'setting' => null,
            'sequence' => 1,
        ];

        $response = $this->actingAs($user)->post(route('admin.polling.option.store'), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PollingQuestionOption)->getTable(), $formData);
    }

    public function testUpdatePollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $question = factory(PollingQuestion::class)->create();
        $formData = [
            'question_id' => $question->id,
            'option' => 'dummy',
            'setting' => null,
            'sequence' => 1,
        ];
        
        $pollingOption = factory(PollingQuestionOption::class)->create([
            'option' => 'asdf',
            'sequence' => 99,
        ]);
        $response = $this->actingAs($user)->put(route('admin.polling.option.update', $pollingOption->id), $formData);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'asdf']);
        $this->assertDatabaseHas((new PollingQuestionOption)->getTable(), $formData);
    }

    public function testDeletePollingOption_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $option = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $responseDelete = $this->actingAs($user)->delete(route('admin.polling.option.destroy', $option->id));
        $responseDelete->assertStatus(302);
        $responseDelete->assertSessionHas('message');
        
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
