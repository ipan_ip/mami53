<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;

class RoomControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithoutEvents;
    use WithFaker;

    private const URL_CREATE_ROOM_DUPLICATE_PREFIX = '/admin/room/duplicate/';
    private const URL_CREATE_NEW_ROOM = '/admin/room/';
    private const URL_GET_FILTER_BY_ROOM_ID = '/admin/room/price-filter/%u';

    public function setUp() : void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->state('admin')->make());
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesRoomNotFound()
    {
        $data = [];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . rand(1, 10), $data);

        $response->assertOk();
        $response->assertJson([
            'status' => false
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesRoomInvalidDuplicateCountMoreThan5()
    {
        $room = factory(Room::class)->create();
        $data = [
            'duplicate_count' => 10,
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.room.duplicate.error.more_than_limit', ['limit' => RoomController::ROOM_DUPLICATE_UPPER_LIMIT]),
            ],
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesRoomInvalidDuplicateCountLessThan1()
    {
        $room = factory(Room::class)->create();
        $data = [
            'duplicate_count' => 0
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.room.duplicate.error.less_than_limit', ['limit' => RoomController::ROOM_DUPLICATE_LOWER_LIMIT]),
            ],
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesRoomValidDuplicateCountShouldReturnNonEmptyDuplicatesResponse()
    {
        $room = factory(Room::class)->create();
        $data = [
            'duplicate_count' => 2
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);
        $responseJson = $response->json();

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
        $this->assertNotEmpty($responseJson['duplicates']);
        $this->assertEquals(2, count($responseJson['duplicates']));
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesRoomDuplicatedRoomShouldSavedInDatabase()
    {
        $room = factory(Room::class)->create();
        $data = [
            'duplicate_count' => 2
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);
        $responseJson = $response->json();

        $response->assertOk();

        $duplicatedIds = [];
        foreach ($responseJson['duplicates'] as $duplicate) {
            $duplicatedIds[] = $duplicate['id'];
        }

        $this->assertEquals(2, Room::whereIn('id', $duplicatedIds)->count());
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesWithPhotoShouldHasDuplicateFromInformation()
    {
        $room = factory(Room::class)->create();
        $data = [
            'duplicate_count' => 2,
            'is_with_photo' => true,
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);

        $this->assertEquals(200, $response->getStatusCode(), Room::all());
        $this->assertEquals(2, Room::where('duplicate_from', $room->id)->count());
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomDuplicatesShouldDuplicateTheOwnersInformation()
    {
        $room = factory(Room::class)->create();
        $roomOwners = factory(RoomOwner::class, 2)->make();
        $room->owners()->saveMany($roomOwners);
        $data = [
            'duplicate_count' => 1,
        ];
        $response = $this->postJson(self::URL_CREATE_ROOM_DUPLICATE_PREFIX . $room->id, $data);

        $duplicatedRoomId = $response->json()['duplicates'][0]['id'];

        $this->assertEquals(2, RoomOwner::where('designer_id', $duplicatedRoomId)->count(), RoomOwner::all());
        $this->assertEquals($roomOwners->pluck('user_id'), RoomOwner::where('designer_id', $duplicatedRoomId)->get()->pluck('user_id'), RoomOwner::all());
    }

    /**
     * @group UG
     * @group UG-1221
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomWithRoomOwnerPhoneNumberExistShouldBeLinked()
    {
        $user = factory(User::class)->state('owner')->create();
        $data = [
            'name' => $this->faker()->firstName(),
            'price_monthly' => 450000,
            'room_count' => 3,
            'room_available' => 2,
            'address' => $this->faker()->address,
            'longitude' => $this->faker()->longitude,
            'latitude' => $this->faker()->latitude,
            'owner_name' => $this->faker()->name,
            'owner_phone' => $user->phone_number,
            'agent_name' => $this->faker()->name,
            'area_city' => $this->faker()->name,
        ];

        $previousRooms = Room::all();

        $response = $this->post(self::URL_CREATE_NEW_ROOM, $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHasNoErrors();
        $response->assertSessionMissing('error_message');

        $newRooms = Room::all()->whereNotIn('id', $previousRooms->pluck('id')->toArray());
        $newRoom = $newRooms->first();

        $this->assertEquals(1, $newRooms->count());
        $this->assertNotNull($newRoom);
        $this->assertEquals($data['name'], $newRoom->name);

        $roomOwner = $newRoom->owners->first();
        $this->assertNotNull($roomOwner);
        $this->assertEquals($user->id, $roomOwner->user_id);
        $this->assertEquals(RoomOwner::ROOM_ADD_STATUS, $roomOwner->status);
        $this->assertEquals(RoomOwner::STATUS_TYPE_KOS_OWNER, $roomOwner->owner_status);
    }

    /**
     * @group UG
     * @group UG-1221
     * @group App/Http/Controllers/Admin/RoomController
     */
    public function testCreateRoomWithRoomOwnerPhoneNumberNotFoundShouldNotBeLinked()
    {
        $data = [
            'name' => $this->faker()->firstName(),
            'price_monthly' => 450000,
            'room_count' => 3,
            'room_available' => 2,
            'address' => $this->faker()->address,
            'longitude' => $this->faker()->longitude,
            'latitude' => $this->faker()->latitude,
            'owner_name' => $this->faker()->name,
            'owner_phone' => $this->faker()->phoneNumber,
            'agent_name' => $this->faker()->name,
            'area_city' => $this->faker()->name,
        ];

        $previousRooms = Room::all();

        $response = $this->post(self::URL_CREATE_NEW_ROOM, $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHasNoErrors();
        $response->assertSessionMissing('error_message');

        $newRooms = Room::all()->whereNotIn('id', $previousRooms->pluck('id')->toArray());
        $newRoom = $newRooms->first();

        $this->assertEquals(1, $newRooms->count());
        $this->assertNotNull($newRoom);
        $this->assertEquals($data['name'], $newRoom->name);
        $this->assertEmpty($newRoom->owners);
    }

    /**
     * @group BG-3405
     */
    public function testGetPriceFilterKostNotFound()
    {
        $roomId = $this->faker()->randomDigit;
        $room = Room::find($roomId);
        if ($room) $room->delete();

        $url = sprintf(self::URL_GET_FILTER_BY_ROOM_ID, $roomId);
        $response = $this->get($url); // call api

        $response->assertStatus(200);
        $json = $response->json();
        $this->assertEquals('kamar tidak ditemukan', $json['message']);
        $this->assertEquals(false, $json['status']);
    }

    /**
     * @group BG-3405
     */
    public function testGetPriceFilterWithNullPriceFilter()
    {
        $room = factory(Room::class)->create();
        $url = sprintf(self::URL_GET_FILTER_BY_ROOM_ID, $room->id);
        $response = $this->get($url); // call api

        $response->assertStatus(200);
        $json = $response->json();
        $this->assertEquals(true, $json['status']);
        $this->assertEquals(null, $json['data']);
    }

    /**
     * @group BG-3405
     */
    public function testGetPriceFilterWithValidData()
    {
        $montylyAdditionalPrice = 100;
        $room = factory(Room::class)->create([
            'price_daily'         => 100,
            'price_weekly'        => 1000,
            'price_monthly'       => 10000,
            'price_quarterly'     => 100000,
            'price_semiannually'  => 1000000,
            'price_yearly'        => 10000000,
        ]);
        $priceFilter = factory(PriceFilter::class)->create(
            [
                'designer_id'               => $room->id,
                'final_price_daily'         => $room->price_daily,
                'final_price_weekly'        => $room->price_weekly,
                'final_price_monthly'       => $room->price_monthly + $montylyAdditionalPrice,
                'final_price_quarterly'     => $room->price_quarterly,
                'final_price_semiannually'  => $room->price_semiannually,
                'final_price_yearly'        => $room->price_yearly
            ]
        );

        $url = sprintf(self::URL_GET_FILTER_BY_ROOM_ID, $room->id);
        $response = $this->get($url); // call api

        $response->assertStatus(200);
        $json = $response->json();
        $this->assertEquals(true, $json['status']);
        $this->assertEquals($priceFilter['final_price_daily'], $json['data']['final_price_daily']);
        $this->assertEquals($priceFilter['final_price_weekly'], $json['data']['final_price_weekly']);
        $this->assertEquals($priceFilter['final_price_monthly'], $json['data']['final_price_monthly']);
        $this->assertEquals($priceFilter['final_price_quarterly'], $json['data']['final_price_quarterly']);
        $this->assertEquals($priceFilter['final_price_semiannually'], $json['data']['final_price_semiannually']);
        $this->assertEquals($priceFilter['final_price_yearly'], $json['data']['final_price_yearly']);
    }
}
