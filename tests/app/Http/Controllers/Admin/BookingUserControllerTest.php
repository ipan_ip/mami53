<?php

namespace app\Http\Controllers\Admin;

use App\Criteria\Consultant\AdminBSECriteria;
use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingOwner;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingReject;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Consultant\BookingUserRemark;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\PaginationHelper;
use App\Libraries\BookingUserExporter;
use App\Libraries\SMSLibrary;
use App\Presenters\Consultant\ConsultantPresenter;
use App\Repositories\Booking\BookingUserRepositoryEloquent;
use App\Repositories\Booking\RejectReason\BookingRejectReasonRepositoryEloquent;
use App\Repositories\Consultant\ConsultantRepository;
use App\Repositories\Consultant\ConsultantRepositoryEloquent;
use App\Repositories\UserDataRepositoryEloquent;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Maatwebsite\Excel\Facades\Excel;
use Mockery;

use App\Test\MamiKosTestCase;

class BookingUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const BOOKING_TENANT_URL = 'admin/booking/users';
    private const GET_TENANT_URL = 'admin/users/tenant';
    private const GET_RENT_COUNTS_URL = 'admin/booking/rent-count/';
    private const CREATE_BOOKING_URL = 'admin/booking/users/create';
    private const DOWNLOAD_BOOKING_TENANT_DATA_URL = 'admin/booking/users/download';

    private const TENANT_ID = '1725551';

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function mockBookingUser($bookId)
    {
        return factory(BookingUser::class)->make(['id' => $bookId]);
    }

    public function testGetAllTenant()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json(
            'GET',
            self::GET_TENANT_URL.'?search=Sandiah&page=1');

        $response->assertStatus(200);
    }

    public function testGetDetailTenantById()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // Mock fake data from repository
        $this->mockAlternatively(UserDataRepositoryEloquent::class);
        $this->mock->shouldReceive('getUserById')
            ->once()
            ->andReturn([
                'data' => [
                    'id' => 1725551,
                    'email' => 'ahsansandiah@gmail.com',
                    'phone_number' => '087846928987',
                    "name" => 'Ahsan Anwar Sandiah',
                    'gender' => 'male',
                    'job' => 'Lainnya',
                    'is_verify' => 1,
                    'role' => 'user'
                ]
            ]);

        $response = $this->json('GET',self::GET_TENANT_URL.'/'.self::TENANT_ID);
        $response->assertStatus(200);
    }

    public function testGetRentCountsIsNotNull()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET',self::GET_RENT_COUNTS_URL.BookingUser::MONTHLY_TYPE);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'data' => [
                '1 Bulan',
                '2 Bulan',
                '3 Bulan',
                '4 Bulan',
                '5 Bulan',
                '6 Bulan',
                '7 Bulan',
                '8 Bulan',
                '9 Bulan',
                '10 Bulan',
                '11 Bulan',
                '12 Bulan'
            ]
        ]);
    }

    public function testGetRentCountsIsNull()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET',self::GET_RENT_COUNTS_URL.'0');

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'data' => null
        ]);
    }

    public function testCreateBooking()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->call('GET', self::CREATE_BOOKING_URL);
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateOrCreateBookingPhoneNumberIsInvalid()
    {
        $params = $this->defaultValidParams();
        $params['contact_phone'] = '082121xxx21'; // make phone number invalid

        $checkPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($params['contact_phone']);
        $this->assertFalse(false, $checkPhoneNumber);
    }

    public function testStoreBooking()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('newBookRoom')
            ->andReturn($this->defaultValidParams());

        $response = $this->call('POST', self::BOOKING_TENANT_URL);
        // response status 302 is redirect page
        $this->assertEquals(302, $response->status());
    }

    public function testEditBooking()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $bookingTenant = $this->mockBookingUser(1704);

        $response = $this->call('GET', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/edit');

        if (is_null($response->getOriginalContent())) {
            // response status 302 is redirect page
            $this->assertEquals(302, $response->status());
            $this->assertNull($response->getOriginalContent());
        } else {
            $response->assertViewHas('boxTitle', 'Update Booking - MAMI0404001704');
            $response->assertViewHas('booking');
            $response->assertViewHas('bookingUserRoom');
        }
    }

    public function testUpdateBooking()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $bookingTenant = $this->mockBookingUser(1704);

        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('updateBooking')
                ->andReturn([]);

        $response = $this->call('PUT', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id);
        // response status 302 is redirect page
        $this->assertEquals(302, $response->status());
    }

    private function defaultValidParams()
    {
        return [
            'checkin' => Carbon::now()->format('Y-m-d'),
            'rent_count_type' => 'monthly',
            'duration' => 1,
            'contact_name' => 'Si Bujang',
            'contact_phone' => '087846918987',
            'contact_job'   => 'mahasiswa',
            'contact_gender' => 'male',
            'contact_email' => 'bujangdeveloper@gmail.com',
        ];
    }

    public function testGetListBookingIsSuccess()
    {
        // Mock fake data from repository
        $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $this->mock->shouldReceive('getListBookingFromAdminPage')
            ->once()
            ->andReturn($this->defaultResponseBookingList());

        $this->mock->shouldReceive('getListBookingFromAdminPage')
            ->once()
            ->andReturn(1);

        $this->mock->shouldReceive('getKostLevelGoldPlusAndOyoFilter')
            ->once()
            ->andReturn([]);

        $presenter = \Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->once()
            ->andReturn($this->defaultResponseBookingUserListTransformer());


        $repoCancel = \Mockery::mock(BookingRejectReasonRepositoryEloquent::class);

        $repoCancel->shouldReceive('getListByType')
            ->andReturn([]);

        $repoCancel->shouldReceive('getListByType')
            ->andReturn([]);

        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->call('GET', self::BOOKING_TENANT_URL);
        $response->assertViewHas('rejectReasons');
        $response->assertViewHas('cancelReasons');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('statusChangedBy');
        $response->assertViewHas('status');
        $response->assertViewHas('kostLevels');
    }

    public function defaultResponseBookingList()
    {
        $data = [
            [
                'id' => 1413,
                'booking_designer_id' => 85,
                'user_id' => 1936834,
                'booking_code' => 'MAMI0303001413',
                'checkin_date' => '2020-03-31',
                'checkout_date' => '2020-04-31',
                'created_at' => '2020-04-01 14:59:38'
            ]
        ];

        return PaginationHelper::pagination($data, count($data), 20, 20);
    }

    public function defaultResponseBookingUserListTransformer()
    {
        return [
            'data' => [
                [
                    "booking_code" => "MAMI0404001498",
                    "contact_name" => "Hhgfy",
                    "contact_phone" => "0885285886",
                    "is_room_deleted" => false,
                    "room_name" => "Kost Zahira Papua",
                    "owner_phone" => "089604239098",
                    "area_city" => "Yogyakarta",
                    "is_room_mamirooms" => 0,
                    "is_room_premium_photo" => false,
                    "is_room_testing" => 0,
                    "listing_price" => "120.000",
                    "original_price" => "120.000",
                    "checkin_date" => "2020-04-24",
                    "checkout_date" => "2020-05-24",
                    "stay_duration" => 1,
                    "rent_count_type" => "monthly",
                    "status" => "booked",
                    "changed_by" => "tenant",
                    "id" => 1498,
                    "is_booked" => true,
                    "is_verified" => false,
                    "is_paid" => false,
                    "is_confirmed" => false,
                    "is_rejected" => false,
                    "is_cancelled" => false,
                    "is_cancelled_by_admin" => false,
                    "is_terminated" => false,
                    "is_expired" => false,
                    "is_expired_date" => false,
                    "is_guarantee_requested" => false,
                    "has_checked_in" => false,
                    "has_been_booked" => true,
                    "is_instant_booking" => 0,
                    "additional_prices" => false,
                    "rent_counts" => [
                        "monthly"
                    ],
                    "note" => [
                        "category" => "",
                        "content" => "",
                        "created_at" => '2020-04-01 14:59:38',
                        "created_by" => "",
                        "updated_at" => "",
                        "updated_by" => ""
                    ],
                    "created_at" => '2020-04-01 14:59:38',
                    "created_by" => "tenant",
                    'from_pots' => true,
                    'room_available' => 1,
                    'has_checked_out' => true,
                    'tags' => collect([
                        factory(Tag::class)->create()
                    ]),
                    'guest_total' => 5,
                    'kos_last_update' => Carbon::now(),
                    'additional_prices_by_owner' => [],
                    'instant_booking_owner_id' => 5
                ]
            ]
        ];
    }

    public function testCancelBookingNotFound()
    {
        // prepare data
        $user = factory(User::class)->make();

        // run test
        $response = $this->actingAs($user)->json('GET', self::BOOKING_TENANT_URL . '/1');
        $response->assertRedirect('', $response);
        $response->assertSessionHas('error_message', 'Booking tidak ditemukan');
    }

    public function testCancelBookingStatusFailed()
    {
        // prepare data
        $user = factory(User::class)->make();

        $bookingTenant = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_TERMINATED
        ]);

        $response = $this->actingAs($user)->json('POST', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/cancel');
        $response->assertRedirect('', $response);
        $response->assertSessionHas('error_message', 'Aksi tidak dapat dilakukan');
    }

    public function testCancelBooking()
    {
        $user = factory(User::class)->make();

        $bookingTenant = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        factory(BookingReject::class)->create([
            'id' => 1
        ]);

        $params = [
            'cancel_id' => 1
        ];

        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('cancelBookingByAdmin')
            ->andReturn([]);

        $response = $this->actingAs($user)->json('POST', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/cancel', $params);

        $response->assertRedirect('', $response);
        $response->assertSessionHas('message', 'Booking dibatalkan');
    }

    public function testRejectBookingNotFound()
    {
        // prepare data
        $user = factory(User::class)->make();

        // run test
        $response = $this->actingAs($user)->json('GET', self::BOOKING_TENANT_URL . '/1');
        $response->assertRedirect('', $response);
        $response->assertSessionHas('error_message', 'Booking tidak ditemukan');
    }

    public function testRejectBookingStatusFailed()
    {
        // prepare data
        $user = factory(User::class)->make();

        $bookingTenant = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_TERMINATED
        ]);

        $response = $this->actingAs($user)->json('POST', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/reject');
        $response->assertRedirect('', $response);
        $response->assertSessionHas('error_message', 'Aksi tidak dapat dilakukan');
    }

    public function testRejectBooking()
    {
        $user = factory(User::class)->make();

        $bookingTenant = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        factory(BookingReject::class)->create([
            'id' => 1
        ]);

        $params = [
            'reject_id' => 1
        ];

        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('rejectBookingByAdmin')
            ->andReturn([]);

        $response = $this->actingAs($user)->json('POST', self::BOOKING_TENANT_URL. '/' .$bookingTenant->id. '/reject', $params);
        $response->assertRedirect('', $response);
        $response->assertSessionHas('message', 'Booking ditolak');
    }

    public function testShowWithEmptyBookingUser()
    {
        // prepare data
        $user = factory(User::class)->make();

        // run test
        $response = $this->actingAs($user)->json('GET', self::BOOKING_TENANT_URL . '/1');
        $response->assertRedirect('', $response);
        $response->assertSessionHas('error_message', 'Booking tidak ditemukan');
    }

    public function testShowSuccess()
    {
        // prepare data
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $room->id]);
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'booking_designer_id' => $bookingDesigner->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED
        ]);

        $presenter = \Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->once()
            ->andReturn($this->defaultResponseShow($bookingUser->id));

        // run test
        $response = $this->actingAs($user)->json('GET', self::BOOKING_TENANT_URL . '/' . $bookingUser->id);
        $response->assertViewIs('admin.contents.booking.booking_users_detail');
        $response->assertViewHas('bookingUser');
        $response->assertViewHas('prices');
        $response->assertViewHas('totalFirstInvoice');
        $response->assertViewHas('tenant');
        $response->assertViewHas('contract');
        $response->assertViewHas('invoices');
        $response->assertViewHas('adminFee');
        $response->assertViewHas('revisionables');
        $response->assertViewHas('noteHistories');
    }

    public function testShowShouldOnlyShowDeletedNote(): void
    {
        // prepare data
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $room->id]);
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'booking_designer_id' => $bookingDesigner->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED
        ]);

        $firstNote = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $bookingUser->id,
            'deleted_at' => Carbon::now()
        ]);

        $secondNote = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $bookingUser->id,
            'deleted_at' => Carbon::now()->addMinutes(5)
        ]);

        factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $bookingUser->id
        ]);

        $presenter = \Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->once()
            ->andReturn($this->defaultResponseShow($bookingUser->id));

        $response = $this->actingAs($user)->json('GET', self::BOOKING_TENANT_URL . '/' . $bookingUser->id);
        $noteResults = $response->getOriginalContent()->getData()['noteHistories'];

        $this->assertTrue($noteResults[0]->is($secondNote));
        $this->assertTrue($noteResults[1]->is($firstNote));
        $this->assertCount(2, $noteResults);
    }

    public function defaultResponseShow($bookId)
    {
        return [
            "data" => [
                "id" => $bookId,
                "user_booking" => [
                    "name" => "Maudy Ayunda",
                    "phone" => "08999999999",
                    "email" => "maudylopepolepel@gmail.com",
                    "job" => "lainnya",
                    "identity" => null,
                    "gender" => "male",
                    "introduction" => "Qweqweqwe qwe Qwe Qwe qeqwe ppp",
                ],
                "room_data" => [
                    "name" => "Kost Zaniya Papua",
                    "area_city" => "Jayapura",
                    "area_subdistrict" => "Abepura",
                    "area_label" => "Abepura, Jayapura, Papua",
                    "phone" => "089604239098",
                    "number" => null,
                    "type" => null,
                    "gender" => 1,
                    "floor" => null,
                    "address" => "Jl. Pogung",
                ],
                "booking_data" => [
                    "booking_code" => "MAMI0404001763",
                    "name" => "Menunggu konfirmasi",
                    "duration" => 1,
                    "rent_count_type" => "monthly",
                    "duration_string" => "1 Bulan",
                    "checkin" => "2020-04-22 14:00",
                    "checkout" => "2020-05-22 11:00",
                    "checkin_formatted" => "Wednesday, 22 Apr 2020 14:00",
                    "checkout_formatted" => "Friday, 22 May 2020 11:00",
                    "guest_total" => 1,
                    "is_married" => false,
                    "status" => "booked",
                    "tenant_checkin_time" => null,
                    "cancel_reason" => null,
                    "date" => "2020-04-08 12:15:42",
                    "prices" => [
                        "base" => [
                            "price_name" => "Base",
                            "price_label" => "Base",
                            "price_total" => 700000.0,
                            "price_total_string" => "Rp. 700.000",
                            "price_type" => ""
                        ],
                        "other" => [],
                        "total" => [
                            "price_name" => "Total",
                            "price_label" => "Total",
                            "price_total" => 700000.0,
                            "price_total_string" => "Rp. 700.000",
                            "price_type" => ""
                        ],
                        "fine" => null
                    ],
                    "from_pots" => true,
                    "original_price" => "Rp. 1.500.000",
                ],
                "payment" => [],
                "note" => [
                    "content" => "",
                    "activity" => "",
                    "created_at" => "",
                    "created_by" => "",
                    "updated_at" => "",
                    "updated_by" => "",
                ],
                "statuses" => [],
            ]
        ];
    }

    public function testDownloadIsSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $bookingUsers = factory(BookingUser::class)->make();

        $response = Excel::download(new BookingUserExporter($bookingUsers), 'booking.csv');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function defaultResponseDownloadBookingList()
    {
        return [
            [
                'id' => 1413,
                'booking_designer_id' => 85,
                'user_id' => 1936834,
                'booking_code' => 'MAMI0303001413',
                'checkin_date' => '2020-03-31',
                'checkout_date' => '2020-04-31',
                'created_at' => '2020-04-01 14:59:38'
            ]
        ];
    }

    public function testSetTransferPermissionStatusIsSuccess()
    {
        $user = factory(User::class)->create();
        $bookingTenant = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED
        ]);

        $response = $this->call('POST', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/transfer-permission');
        $response->assertSessionHas('message', 'Set Transfer Permission Status Success');
    }

    public function testSetTransferPermissionStatusIsFailed()
    {
        $user = factory(User::class)->create();
        $bookingTenant = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $response = $this->call('POST', self::BOOKING_TENANT_URL.'/'.$bookingTenant->id.'/transfer-permission');
        $response->assertSessionHas('error_message', 'Booking does not have permission for refund or disburse');
    }

    protected function createBookingWithInstantBookingOwner(): BookingUser
    {
        $kost = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $booking = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_designer_id' => $bookingDesigner->id,
            'created_at' => Carbon::now()->subDay()
        ]);
        $owner = factory(User::class)->create();
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => $owner->id]);

        return $booking;
    }

    public function testCountInProcess(): void
    {
        $this->createBookingWithInstantBookingOwner();

        factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_CHECKED_IN,
            'created_at' => Carbon::now()->subDay()
        ]);

        $willPay = factory(BookingUser::class)->create(['created_at' => Carbon::now()->subDay()]);
        factory(ConsultantNote::class)->create(['noteable_type' => 'booking_user', 'noteable_id' => $willPay->id, 'activity' => BookingUserRemark::WILL_PAY]);

        factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'created_at' => Carbon::now()->subDay()
        ]);

        $response = $this->json('GET', route('admin.booking.users.count.in-process') . '?created_at=yesterday');
        $result = $response->getOriginalContent();
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $response->assertStatus(200);
    }

    public function testCountOnGoing(): void
    {
        $survey = factory(BookingUser::class)->create(['created_at' => Carbon::now()->subDay()]);
        factory(ConsultantNote::class)->create([
            'noteable_type' => 'booking_user',
            'noteable_id' => $survey->id,
            'activity' => BookingUserRemark::SURVEY
        ]);

        $response = $this->json('GET', route('admin.booking.users.count.on-going') . '?created_at=yesterday');
        $response->assertJson([
            'data' => [
                'survey' => 1,
            ]
        ]);
    }

    public function testCountFinished(): void
    {
        $rejected = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_REJECTED,
            'created_at' => Carbon::now()->subDay()
        ]);

        $cancelled = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_CANCELLED,
            'created_at' => Carbon::now()->subDay()
        ]);

        $response = $this->json('GET', route('admin.booking.users.count.finished') . '?created_at=yesterday');
        $response->assertJson([
            'data' => [
                'rejected' => 1,
                'cancelled' => 1,
            ]
        ]);
    }

    public function testGetAdminBSE(): void
    {
        $repo = $this->mockAlternatively(ConsultantRepository::class);
        $repo->shouldReceive('pushCriteria')
            ->with(AdminBSECriteria::class)
            ->andReturn($repo);

        $repo->shouldReceive('pushCriteria')
            ->withArgs(function ($arg) {
                return ($arg instanceof PaginationCriteria);
            })
            ->andReturn($repo);

        $repo->shouldReceive('setPresenter')
            ->withArgs(function ($arg) {
                return ($arg instanceof ConsultantPresenter);
            })
            ->andReturn($repo);

        $repo->shouldReceive('with')
            ->with('mapping')
            ->andReturn($repo);

        $repo->shouldReceive('get')
            ->andReturn([
                'data' => [
                    [
                        'id' => 1,
                        'name' => 'Konsultan',
                        'area_city' => ['Jakarta']
                    ]
                ]
            ]);

        $repo->shouldReceive('pushCriteria')
            ->with(AdminBSECriteria::class)
            ->andReturn($repo);

        $repo->shouldReceive('count')
            ->andReturn(1);

        $actual = $this->get('/admin/booking/admin-bse');
        $expected = [
            'data' => [
                [
                    'id' => 'all',
                    'name' => 'All',
                    'area_city' => []
                ],
                [
                    'id' => 1,
                    'name' => 'Konsultan',
                    'area_city' => ['Jakarta']
                ]
             ],
             'pagination' => [
                 'more' => false
             ]
        ];

        $actual->assertJson($expected);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }
}
