<?php

namespace App\Http\Controllers\Admin\Api\Kost;

use App\Entities\Generate\RejectReason;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RejectReasonControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const INDEX_ROUTE = 'admin.api.kost.reject-reason';
    const PREVIEW_ROUTE = 'admin.api.kost.reject-preview';

    private $admin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->admin = factory(User::class)->create();
    }

    public function testGetIndexSucess()
    {
        $response = $this->get(route(self::INDEX_ROUTE));

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => RejectReason::GROUP,
        ]);
    }

    public function testPreviewEmptyParams()
    {
        $response = $this->get(route(
            self::PREVIEW_ROUTE,
            [
                'reject_reason_ids' => [],
                'reject_reason_other' => [],
            ]
        ));

        $response->assertStatus(400);
        $this->assertEquals($response->getData()->meta->message, 'Alasan ditolak harus diisi.');
    }

    public function testGetPreviewSucess()
    {
        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);

        $response = $this->get(route(
            self::PREVIEW_ROUTE,
            [
                'reject_reason_ids' => [$rejectReason->id],
                'reject_reason_other' => [],
            ]
        ));

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => [
                'reason',
            ],
        ]);
        $this->assertEquals($response->getData()->data->reason, 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah). ');
    }
}
