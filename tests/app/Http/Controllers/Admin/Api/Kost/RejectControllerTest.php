<?php

namespace App\Http\Controllers\Admin\Api\Kost;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\ListingRejectReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RejectControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const RETRIEVE_ROUTE = 'admin.api.kost.reject.history';
    const STORE_ROUTE = 'admin.api.kost.reject.save';

    private $admin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->admin = factory(User::class)->create();
    }

    public function testGetSucess()
    {
        $room = factory(Room::class)->create();
        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);

        $listingReason = $this->createListingReason($room, $rejectReason);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['id' => $room->id]));

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => [
                'history'
            ],
        ]);

        $history = $response->getData()->data->history;
        $this->assertEquals($history[0]->admin_name, $this->admin->name);
        $this->assertEquals($history[0]->reject_reason, $listingReason->content);
    }

    public function testStoreRoomNotFound()
    {
        $response = $this->post(
            route(self::RETRIEVE_ROUTE, ['id' => 123]),
            [
                'reject_reason_ids' => [1],
                'reject_reason_other' => [],
            ]
        );

        $response->assertStatus(404);
    }

    public function testStoreEmptyParams()
    {
        $owner = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $response = $this->post(
            route(self::RETRIEVE_ROUTE, ['id' => $room->id]),
            [
                'reject_reason_ids' => [],
                'reject_reason_other' => [],
            ]
        );

        $response->assertStatus(400);
        $this->assertEquals($response->getData()->meta->message, 'Alasan ditolak harus diisi.');
    }

    public function testStoreKosStatusDraft()
    {
        $owner = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_EARLY_EDIT_STATUS,
        ]);

        $response = $this->post(
            route(self::RETRIEVE_ROUTE, ['id' => $room->id]),
            [
                'reject_reason_ids' => [1],
                'reject_reason_other' => [],
            ]
        );

        $response->assertStatus(400);
        $this->assertEquals($response->getData()->meta->message, 'Kos tidak bisa direject.');
    }

    public function testStoreSuccess()
    {
        $owner = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $rejectReason = factory(RejectReason::class)->create([
            'group' => 'address',
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);

        $response = $this->post(
            route(self::RETRIEVE_ROUTE, ['id' => $room->id]),
            [
                'reject_reason_ids' => [$rejectReason->id],
                'reject_reason_other' => [],
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new ListingReason())->getTable(), ['listing_id' => $room->id]);
        $this->assertDatabaseHas((new ListingRejectReason())->getTable(), ['reject_reason_id' => $rejectReason->id]);

        $roomOwner->refresh();
        $this->assertEquals($roomOwner->status, RoomOwner::ROOM_UNVERIFY_STATUS);
    }

    private function createListingReason(Room $room, RejectReason $rejectReason): ListingReason
    {
        $listingReason = factory(ListingReason::class)->create([
            'from' => 'room',
            'listing_id' => $room->id,
            'user_id' => $this->admin->id,
            'content' => 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah).'
        ]);

        factory(ListingRejectReason::class)->create([
            'scraping_listing_reason_id' => $listingReason->id,
            'reject_reason_id' => $rejectReason->id,
        ]);

        return $listingReason;
    }
}
