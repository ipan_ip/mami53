<?php

namespace App\Http\Controllers\Admin\Testing;

use App\Repositories\UserDataRepository;
use App\Services\UserService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Pagination\LengthAwarePaginator;

class TestersControllerTest extends MamiKosTestCase
{

    use WithoutEvents, WithoutMiddleware;

    private const TESTERS_INDEX_PATH = 'admin/testing/testers';
    private const TESTERS_RECYCLE_PHONE_NUMBER_PATH = 'admin/testing/testers/%s/recycle-phone';
    private const TESTERS_MARK_AS_TESTER_PATH = 'admin/testing/testers/%s/mark-as-tester';

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->userService = $this->mockPartialAlternatively(UserService::class);
        $this->app->instance(UserService::class, $this->userService);
        $this->controller = $this->app->make(TestersController::class);
    }

    public function testIndex()
    {
        $mockPaginationResult = new LengthAwarePaginator([], 0, 15);
        $this->userService->shouldReceive('getUsersFromAdmin')->andReturn($mockPaginationResult);

        $this->actingAs($this->user);
        $response = $this->call('GET', self::TESTERS_INDEX_PATH);
        $response->assertViewIs('admin.contents.testing.testers.index');
    }

    public function testRecyclePhoneNumber()
    {
        $this->actingAs($this->user);
        $this->userService->shouldReceive('recyclePhoneNumber')->andReturn(true);
        
        $response = $this->post(sprintf(self::TESTERS_RECYCLE_PHONE_NUMBER_PATH, $this->user->id));
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::TESTERS_INDEX_PATH);
    }

    public function testMarkAsTester()
    {
        $this->actingAs($this->user);
        $this->userService->shouldReceive('setAsTester')->andReturn(true);
        $data = [
            'flag' => 1
        ];
        $response = $this->post(sprintf(self::TESTERS_MARK_AS_TESTER_PATH, $this->user->id), $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::TESTERS_INDEX_PATH);
    }
}