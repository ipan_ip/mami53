<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Point\PointUser;
use Illuminate\Http\UploadedFile;
use App\Entities\Point\PointHistory;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UserPointControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected function setUp() : void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function testIndexPage()
    {
        $user = factory(User::class)->make();
        $this->actingAs($user);

        $response = $this->get(route('admin.point.user.index'));
        $response->assertStatus(200);
    }

    public function testIndexPageCanFilterable()
    {
        // prepare data
        $users = factory(User::class, 3)->create([
            'is_owner' => 'false',
        ])->each(function ($user) {
            $user->point_user()->save(factory(PointUser::class)->make([
                'is_blacklisted' => 1,
            ]));
        });

        $this->actingAs($users->first());
        $url = route('admin.point.user.index', [
            'keyword' => $users->first()->name,
            'status' => 'blacklist',
            'user' => 'tenant',
        ]);

        $response = $this->get($url);
        $response->assertStatus(200);
        $response->assertSee($users->first()->email);
        $response->assertDontSee($users->slice(1, 1)->first()->email);
        $response->assertDontSee($users->slice(2, 1)->first()->email);
    }
    
    public function testIndexPage_ViewOnlyPermissions()
    {
        $user = factory(User::class)->create();
        $role = Role::create([
            'name' => 'role-view-point',
        ]);
        $perm = Permission::create([
            'name' => 'view-point'
        ]);
        $role->perms()->attach($perm->id);
        $user->roles()->attach($role->id);

        $response = $this->actingAs($user)->get(route('admin.point.user.index'));
        $response->assertDontSee('#popup-bulk-adjust-point');
        $response->assertDontSee('#popup-bulk-blacklist');
        $response->assertDontSee('data-target="#popup-adjust-point"');
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testUpdateAdjustPoint()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->create();
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);
        $this->app->instance(PointUser::class, $pointUser);

        $response = $this->actingAs($user)->put(route('admin.point.user.updateAdjustPoint', $pointUser->id), [
            'amount' => 10,
            'note' => $notes
        ]);

        $response->assertStatus(302);
        $this->assertDatabaseHas($pointUser->getTable(), [
            'user_id' => $pointUser->user_id,
            'total' => 15,
        ]);
        $this->assertDatabaseHas((new PointHistory)->getTable(), [
            'user_id' => $pointUser->user_id,
            'balance' => 15,
            'value' => 10,
            'notes' => $notes,
        ]);
    }

    public function testUpdateBulkAdjustPoint()
    {
        $auth = factory(User::class)->create();
        foreach ([1936666, 1936667] as $userId) {
            factory(User::class)->create([
                'id' => $userId
            ]);
            $pointUser = factory(PointUser::class)->create([
                'user_id' => $userId,
                'total' => 5,
                'is_blacklisted' => 0
            ]);
        }

        $file = $this->getUploadableFile('point-bulk-adjust-point.csv');
        $response = $this->actingAs($auth)
            ->put(route('admin.point.user.updateBulkAdjustPoint'), [
                'csv_bulk_adjust_point' => $file
            ]);
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas($pointUser->getTable(), [
            'total' => 0,
        ]);
        $this->assertDatabaseHas($pointUser->getTable(), [
            'total' => 15,
        ]);
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testUpdateBlacklist()
    {
        $user = factory(User::class)->create();
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
        ]);
        $this->app->instance(PointUser::class, $pointUser);

        $response = $this->actingAs($user)->put(route('admin.point.user.updateBlacklist', $pointUser->id), [
            'is_blacklisted' => 1,
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas($pointUser->getTable(), [
            'user_id' => $pointUser->user_id,
            'is_blacklisted' => 1,
        ]);
    }
    
    public function testUpdateBulkBlacklist()
    {
        $auth = factory(User::class)->create();
        foreach ([1936666, 1936667] as $userId) {
            factory(User::class)->create([
                'id' => $userId
            ]);
            $pointUser = factory(PointUser::class)->create([
                'user_id' => $userId,
                'is_blacklisted' => 0,
            ]);
        }

        $file = $this->getUploadableFile('point-bulk-blacklist.csv');
        $response = $this->actingAs($auth)
            ->put(route('admin.point.user.updateBulkBlacklist'), [
                'csv_bulk_blacklist' => $file
            ]);
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas($pointUser->getTable(), [
            'is_blacklisted' => 1,
        ]);
    }
}
