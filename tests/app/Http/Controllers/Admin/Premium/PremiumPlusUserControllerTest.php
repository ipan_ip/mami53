<?php
namespace app\Http\Controllers\Admin\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Premium\PremiumPlusUser;
use App\User;
use App\Entities\Consultant\Consultant;
use App\Entities\Room\Room;

class PremiumPlusUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const PREMIUM_PLUS_PAGE = '/admin/premium-plus';
    private const PREMIUM_PLUS_CREATE_PAGE = '/admin/premium-plus/create';
    private const PREMIUM_PLUS_EDIT_PAGE = '/admin/premium-plus/%s/edit';
    public $postParam = [];

    protected function setUp() : void
    {
        parent::setUp();
        $this->postParam = [
            'name' => 'Jono',
            'phone_number' => '087839439484',
            'email' => 'me@mail.com',
            'request_date' => date('Y-m-d H:i:s'),
            'activation_date' => date('Y-m-d H:i:s'),
            'start_date' => date('Y-m-d H:i:s'),
            'end_date' => date('Y-m-d H:i:s', strtotime("+60 days")),
            'premium_rate' => 10,
            'static_rate' => 1000000,
            'registered_room' => 10,
            'guaranteed_room' => 5,
            'grace_period' => 6
        ];
    }

    public function testIndexPage()
    {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->call('GET', self::PREMIUM_PLUS_PAGE);
        $response->assertViewIs('admin.contents.premium-plus.index');
        $response->assertStatus(200);
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('premiumPlusUser');
    }

    public function testCreatePage()
    {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->call('GET', self::PREMIUM_PLUS_CREATE_PAGE);
        $response->assertViewIs('admin.contents.premium-plus.form');
        $response->assertStatus(200);
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('formMethod');
    }

    public function testEditPageNotFoundData()
    {
        $editPage = sprintf(self::PREMIUM_PLUS_EDIT_PAGE, 0);
        $response = $this->get($editPage);
        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'User not found');
    }

    public function testEditPageSuccess()
    {
        $user = factory(User::class)->create();
        $premiumPlusUser = factory(PremiumPlusUser::class)->create([
            'user_id' => $user->id,
            'consultant_id' => factory(Consultant::class)->create()
        ]);

        $editPage = sprintf(self::PREMIUM_PLUS_EDIT_PAGE, $premiumPlusUser->id);
        $response = $this->actingAs($user)->call('GET', $editPage);
        $this->assertContains('title', $response->content());
    }

    public function testStoreFailed()
    {
        $response = $this->post(self::PREMIUM_PLUS_PAGE, []);
        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    public function testStoreSuccess()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id,
        ]);

        $room = factory(Room::class)->create();
        
        $data = $this->postParam;
        $data['consultant_phone'] = $user->phone_number;
        $data['admin_phone'] = $user->phone_number;
        $data['designer_id'] = $room->id;

        $response = $this->post(self::PREMIUM_PLUS_PAGE, $data);
        $response->assertStatus(302);
        // $response->assertSessionHas('message', 'Berhasil menambah akun GP4');
    }

    public function testUpdateFailed()
    {
        $editPage = sprintf(self::PREMIUM_PLUS_PAGE.'/0');
        $response = $this->put($editPage);
        $response->assertStatus(302);
    }

    public function testUpdateSuccess()
    {
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $editPage = sprintf(self::PREMIUM_PLUS_PAGE.'/'.$premiumPlusUser->id);
        $user = factory(User::class)->create();
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id,
        ]);
        $this->actingAs($user);
        $room = factory(Room::class)->create();
        
        $data = $this->postParam;
        $data['consultant_phone'] = $user->phone_number;
        $data['admin_phone'] = $user->phone_number;
        $data['designer_id'] = $room->id;

        $response = $this->put($editPage, $data);
        $response->assertStatus(302);
    }
}