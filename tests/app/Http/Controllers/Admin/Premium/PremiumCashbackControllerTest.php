<?php
namespace app\Http\Controllers\Admin\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;

class PremiumCashbackControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const PREMIUM_CASHBACK_PAGE = '/admin/premium-cashback';

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testIndexPage()
    {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->call('GET', self::PREMIUM_CASHBACK_PAGE);
        $response->assertStatus(200);
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('premiumCashbackHistory');
    }

}