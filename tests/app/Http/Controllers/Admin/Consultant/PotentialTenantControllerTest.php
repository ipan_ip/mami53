<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Services\Consultant\RoomIndexService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PotentialTenantControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    protected $user;
    protected $role;
    protected $permission;
    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();
        $this->consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $this->user->attachRole($this->role);
        $this->role->attachPermission($this->permission);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testIndex()
    {
        $response = $this->actingAs($this->user)
            ->get('admin/consultant/potential-tenant');

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.potential-tenant-index');
    }

    public function testDataWithPermissionSeeAllData()
    {
        factory(PotentialTenant::class)->create(['consultant_id' => $this->consultant->id + 1]);
        factory(PotentialTenant::class)->create(['consultant_id' => $this->consultant->id]);

        $permission = factory(Permission::class)->create(['name' => 'access-all-consultant-potential-tenant']);
        $this->role->attachPermission($permission);

        $response = $this->actingAs($this->user)
            ->json('GET', 'admin/consultant/potential-tenant/data');

        $response->assertSuccessful();

        $data = json_decode($response->getContent());
        $this->assertEquals(2, $data->total);
    }

    public function testDataWithoutPermissionSeeAllData()
    {
        factory(PotentialTenant::class)->create(['consultant_id' => $this->consultant->id + 1]);
        factory(PotentialTenant::class)->create(['consultant_id' => $this->consultant->id]);

        $response = $this->actingAs($this->user)
            ->json('GET', 'admin/consultant/potential-tenant/data');

        $response->assertSuccessful();

        $data = json_decode($response->getContent());
        $this->assertEquals(1, $data->total);
    }

    public function testDataWithLimit()
    {
        factory(PotentialTenant::class, 10)->create(['consultant_id' => $this->consultant->id]);

        $response = $this->actingAs($this->user)
            ->json('GET', 'admin/consultant/potential-tenant/data', ['limit' => 5]);

        $response->assertSuccessful();

        $data = json_decode($response->getContent());
        $this->assertEquals(10, $data->total);
        $this->assertEquals(5, count($data->rows));
    }

    public function testDataWithOffset()
    {
        $tenants = factory(PotentialTenant::class, 10)->create(['consultant_id' => $this->consultant->id]);

        $response = $this->actingAs($this->user)
            ->json('GET', 'admin/consultant/potential-tenant/data', ['offset' => 5]);

        $response->assertSuccessful();

        $data = json_decode($response->getContent());
        $this->assertEquals(10, $data->total);
        $this->assertEquals(5, count($data->rows));
        $this->assertEquals($tenants[4]->id, $data->rows[0]->id);
    }

    public function testDataWithSearchParam()
    {
        factory(PotentialTenant::class)->create(['consultant_id' => $this->consultant->id, 'name' => 'Test']);

        $response = $this->actingAs($this->user)
            ->json('GET', 'admin/consultant/potential-tenant/data', ['search' => 'test']);

        $response->assertSuccessful();
        $data = json_decode($response->getContent());
        $this->assertEquals(1, $data->total);
        $this->assertEquals('Test', $data->rows[0]->name);
    }
}
