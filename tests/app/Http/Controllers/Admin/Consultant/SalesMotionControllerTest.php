<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class SalesMotionControllerTest extends MamiKosTestCase
{
    protected $consultant;

    protected function setUp()
    {
        parent::setUp();
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->consultant = factory(User::class)->create();

        $this->consultant->attachRole($role);
        $role->attachPermission(factory(Permission::class)->create(['name' => 'admin-access']));
        $role->attachPermission(factory(Permission::class)->create(['name' => 'access-consultant']));
    }

    public function testIndex(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get('/admin/consultant/sales-motion');

        $response->assertViewIs('admin.contents.consultant.sales-motion.index');
    }

    public function testIndexDataShouldReturnCorrectData(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data');
        $response->assertJson(
            [
                'status' => true,
                'sales_motions' => [
                    [
                        'id' => $salesMotion->id,
                        'task_name' => $salesMotion->name,
                        'sales_type' => $salesMotion->type,
                        'start_date' => $salesMotion->start_date,
                        'due_date' => $salesMotion->end_date,
                        'updated_at' => $salesMotion->updated_at->toString(),
                        'updated_by' => $salesMotion->lastUpdatedBy->name,
                        'is_active' => $salesMotion->is_active
                    ]
                ]
            ]
        );
    }

    public function testDetailData(): void
    {
        $media = factory(Media::class)->create();
        $salesMotion = factory(SalesMotion::class)->create(['media_id' => $media->id]);
        $consultant = factory(Consultant::class)->create();
        $assignee = factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $consultant->id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get("/admin/consultant/sales-motion/detail/$salesMotion->id/data");

        $response->assertSuccessful();
        $response->assertJson([
            'status' => true,
            'sales_motion' => [
                'id' => $salesMotion->id,
                'name' => $salesMotion->name,
                'created_by_division' => $salesMotion->created_by_division,
                'objective' => $salesMotion->objective,
                'terms_and_condition' => $salesMotion->terms_and_condition,
                'requirement' => $salesMotion->requirement,
                'benefit' => $salesMotion->benefit,
                'max_participant' => $salesMotion->max_participant,
                'link' => $salesMotion->url,
                'photos' => [
                    'small' => $media->getMediaUrl()['small'],
                    'medium' => $media->getMediaUrl()['medium'],
                    'large' => $media->getMediaUrl()['large'],
                ],
                'start_date' => $salesMotion->start_date,
                'due_date' => $salesMotion->end_date,
                'created_at' => $salesMotion->created_at->toString(),
                'updated_at' => $salesMotion->updated_at->toString(),
                'created_by' => $salesMotion->createdBy->name,
                'updated_by' => $salesMotion->lastUpdatedBy->name,
                'is_active' => $salesMotion->is_active
            ],
            'total' => 1,
            'assignees' => [
                [
                    'consultant' => $assignee->consultant->name
                ]
            ]
        ]);
    }

    public function testIndexDataWithoutLimitShouldUseDefaultLimit(): void
    {
        factory(SalesMotion::class, 12)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data');
        $response->assertJsonCount(PaginationCriteria::DEFAULT_LIMIT, 'sales_motions');
    }

    public function testIndexDataWithLimitShouldReturnCorrectLimit(): void
    {
        factory(SalesMotion::class, 12)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?limit=11');
        $response->assertJsonCount(11, 'sales_motions');
    }

    public function testIndexDataWithLimitShouldReturnCorrectTotal(): void
    {
        factory(SalesMotion::class, 12)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?limit=11');

        $response->assertJson(
            [
                'status' => true,
                'total' => 12
            ]
        );
    }

    public function testIndexDataWithOffsetShouldReturnCorrectOffset(): void
    {
        factory(SalesMotion::class, 12)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?offset=10');
        $response->assertJsonCount(2, 'sales_motions');
    }

    public function testIndexDataWithOffsetShouldReturnCorrectCount(): void
    {
        factory(SalesMotion::class, 12)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?offset=10');

        $response->assertJson(
            [
                'status' => true,
                'total' => 12
            ]
        );
    }

    public function testIndexDataWithSearchShouldReturnMatchedName(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['name' => 'test']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?q=es');
        $response->assertJson(
            [
                'status' => true,
                'sales_motions' => [
                    [
                        'id' => $salesMotion->id,
                        'task_name' => $salesMotion->name,
                        'sales_type' => $salesMotion->type,
                        'start_date' => $salesMotion->start_date,
                        'due_date' => $salesMotion->end_date,
                        'updated_at' => $salesMotion->updated_at->toString(),
                        'updated_by' => $salesMotion->lastUpdatedBy->name,
                        'is_active' => $salesMotion->is_active
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithSalesMotionNotFound(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get("/admin/consultant/sales-motion/detail/0/data");

        $response->assertNotFound();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 404,
                'code' => 1,
                'severity' => 'Error',
                'message' => 'Route: Resource Not Found, it must be mistype in URL.'
            ]
        ]);
    }

    public function testIndexDataWithSearchShouldNotReturnNonMatchingName(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['name' => 'toast']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data?q=es');
        $response->assertJsonCount(0, 'sales_motions');
    }

    public function testIndexDataShouldSortByUpdatedAtDesc(): void
    {
        $lastUpdated = factory(SalesMotion::class)->create(['updated_at' => Carbon::now()->addHour()]);
        $updatedBefore = factory(SalesMotion::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', '/admin/consultant/sales-motion/data');
        $response->assertJson(
            [
                'status' => true,
                'sales_motions' => [
                    [
                        'id' => $lastUpdated->id,
                        'task_name' => $lastUpdated->name,
                        'sales_type' => $lastUpdated->type,
                        'start_date' => $lastUpdated->start_date,
                        'due_date' => $lastUpdated->end_date,
                        'updated_at' => $lastUpdated->updated_at->toString(),
                        'updated_by' => $lastUpdated->lastUpdatedBy->name,
                        'is_active' => $lastUpdated->is_active
                    ],
                    [
                        'id' => $updatedBefore->id,
                        'task_name' => $updatedBefore->name,
                        'sales_type' => $updatedBefore->type,
                        'start_date' => $updatedBefore->start_date,
                        'due_date' => $updatedBefore->end_date,
                        'updated_at' => $updatedBefore->updated_at->toString(),
                        'updated_by' => $updatedBefore->lastUpdatedBy->name,
                        'is_active' => $updatedBefore->is_active
                    ]
                ]
            ]
        );
    }

    private function getConsultantListUrl($salesMotionId, ?array $queries = null): string
    {
        $url = "/admin/consultant/sales-motion/$salesMotionId/consultant";

        if (!is_null($queries)) {
            $url .= '?';
            foreach ($queries as $query) {
                $url .= $query;
            }
        }

        return $url;
    }

    public function testListConsultantWithNonIntegerId(): void
    {
        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl('test'));

        $actual->assertNotFound();
    }

    public function testListConsultantShouldReturnCorrectCount(): void
    {
        factory(Consultant::class, 3)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(['total' => 3]);
    }

    public function testListConsultantWithDefaultLimitShouldLimit(): void
    {
        factory(Consultant::class, 11)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJsonCount(10, 'consultants');
    }

    public function testListConsultantWithCustomLimitShouldLimit(): void
    {
        factory(Consultant::class, 11)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['limit=11']));

        $actual->assertJsonCount(11, 'consultants');
    }

    public function testListConsultantWithLimitShouldReturnCorrectTotal(): void
    {
        factory(Consultant::class, 15)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['limit=11']));

        $actual->assertJson(['total' => 15]);
    }

    public function testListConsultantWithOffsetShouldOffset(): void
    {
        factory(Consultant::class, 11)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['offset=10']));

        $actual->assertJsonCount(1, 'consultants');
    }

    public function testListConsultantWithOffsetShouldReturnCorrectCount(): void
    {
        factory(Consultant::class, 11)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['offset=10']));

        $actual->assertJson(['total' => 11]);
    }

    public function testListConsultantShouldReturnCorrectRole(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::ADMIN
            ]
        );
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => [ConsultantRole::ADMIN]
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithMultipleRoleShouldReturnCorrectRoles(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::ADMIN
            ]
        );
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::DEMAND
            ]
        );
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => [ConsultantRole::ADMIN, ConsultantRole::DEMAND]
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithRoleShouldReturnCorrectCount(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::ADMIN
            ]
        );
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['role=supply']));

        $actual->assertJson(['total' => 0]);
    }

    public function testListConsultantWithDifferentRoleShouldNotReturn(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::ADMIN
            ]
        );
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['role=supply']));

        $actual->assertJsonMissing(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => [ConsultantRole::ADMIN]
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithRoleShouldReturn(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(
            [
                'consultant_id' => $consultant->id,
                'role' => ConsultantRole::ADMIN
            ]
        );
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id, ['role=admin']));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => [ConsultantRole::ADMIN]
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithoutRole(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => []
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithSalesMotion(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => $consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => true,
                        'name' => $consultant->name,
                        'role' => []
                    ]
                ]
            ]
        );
    }

    public function testListConsultantWithOtherSalesMotion(): void
    {
        $consultant = factory(Consultant::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => $consultant->id,
                'consultant_sales_motion_id' => factory(SalesMotion::class)->create()->id
            ]
        );

        $actual = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('GET', $this->getConsultantListUrl($salesMotion->id));

        $actual->assertJson(
            [
                'consultants' => [
                    [
                        'id' => $consultant->id,
                        'is_assigned' => false,
                        'name' => $consultant->name,
                        'role' => []
                    ]
                ]
            ]
        );
    }

    public function testActivateWithoutEndDateParamShouldNotPassedValidation()
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/activate/' . $salesMotion->id);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['end_date' => []]
            ]
        );
    }

    public function testActivateWithEndDateParamThatNotDateShouldNotPassedValidation()
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/activate/' . $salesMotion->id, ['end_date' => 'testing']);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['end_date' => []]
            ]
        );
    }

    public function testActivateWithCorrectEndDateParamShouldPassedValidation()
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/activate/' . $salesMotion->id, ['end_date' => '2020-07-30']);

        $response->assertJson(
            [
                'status' => true,
                'message' => 'Sales motion successfully activated'
            ]
        );
    }

    public function testDeactivate()
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/deactivate/' . $salesMotion->id);

        $response->assertJson(
            [
                'status' => true,
                'message' => 'Sales motion successfully deactivated'
            ]
        );
    }

    public function testStoreWithCurrentDateIsBetweenStartDateAndEndDateParamShouldSetStatusActive()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->subDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => true
            ]
        );

        $newId = $response->getOriginalContent()['sales_motion_id'];
        $salesMotion = SalesMotion::find($newId);

        $this->assertEquals(true, $salesMotion->is_active);
    }

    public function testStoreWithNullMediaId(): void
    {
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->subDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => null
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => true
            ]
        );
    }

    public function testStoreShouldSaveInDatabaseCorrectly(): void
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->subDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $this->assertDatabaseHas(
            'consultant_sales_motion',
            [
                'name' => 'test sales motion',
                'type' => SalesMotion::TYPE_CAMPAIGN,
                'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
                'start_date' => Carbon::now()->subDay()->format('Y-m-d'),
                'end_date' => Carbon::now()->addDay()->format('Y-m-d'),
                'objective' => 'test',
                'terms_and_condition' => 'test',
                'requirement' => 'test',
                'benefit' => 'test',
                'max_participant' => '20 orang',
                'url' => 'http://www.google.com',
                'media_id' => '1',
                'updated_by' => null,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testStoreWithCurrentDateNotBetweenStartDateAndEndDateParamShouldSetStatusNotActive()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => true
            ]
        );

        $newId = $response->getOriginalContent()['sales_motion_id'];
        $salesMotion = SalesMotion::find($newId);

        $this->assertEquals(false, $salesMotion->is_active);
    }

    public function testStoreWithoutNameParamShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['name' => []]
            ]
        );
    }

    public function testStoreWithoutTypeParamShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['type' => []]
            ]
        );
    }

    public function testStoreWithUndefinedTypeParamShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => 'test type',
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['type' => []]
            ]
        );
    }

    public function testStoreWithoutStartDateParamShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['start_date' => []]
            ]
        );
    }

    public function testStoreWithoutStartDateParamNotDateShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => 'test',
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['start_date' => []]
            ]
        );
    }

    public function testStoreWithoutEndDateParamShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['end_date' => []]
            ]
        );
    }

    public function testStoreWithoutEndDateParamNotDateShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'end_date' => 'test',
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['end_date' => []]
            ]
        );
    }

    public function testStoreWithoutCreatedByDivisionParamNotDateShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => 'consultant',
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['created_by_division' => []]
            ]
        );
    }

    public function testStoreWithUndefinedCreatedByDivisionParamNotDateShouldNotPassedValidation()
    {
        factory(Media::class)->create(['id' => 1]);
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['created_by_division' => []]
            ]
        );
    }

    public function testStoreWithEmptyMediaIdShouldPassValidation()
    {
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_MARKETING,
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => ''
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => true
            ]
        );
    }

    public function testStoreWithUndefinedMediaIdShouldNotPassValidation()
    {
        $input = [
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_MARKETING,
            'start_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'end_date' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '50'
        ];

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('POST', '/admin/consultant/sales-motion/store/', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['media_id' => []]
            ]
        );
    }

    public function testSyncAssigneeWithoutConsultantsParamShouldNotPassedValidation()
    {
        factory(SalesMotion::class)->create(['id' => 10]);
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('PUT', '/admin/consultant/sales-motion/10/sync/assignee');

        $response->assertJson(
            [
                'status' => false,
                'message' => ['consultants' => []]
            ]
        );
    }

    public function testSyncAssigneeWithConsultantsParamNotArrayShouldNotPassedValidation()
    {
        factory(SalesMotion::class)->create(['id' => 10]);

        $input = [
            'consultants' => '10'
        ];
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('PUT', '/admin/consultant/sales-motion/10/sync/assignee', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['consultants' => []]
            ]
        );
    }

    public function testSyncAssigneeWithConsultantsParamNotExistInConsultantTableShouldNotPassedValidation()
    {
        factory(SalesMotion::class)->create(['id' => 10]);

        $input = [
            'consultants' => ['10']
        ];
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('PUT', '/admin/consultant/sales-motion/10/sync/assignee', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['consultants.0' => []]
            ]
        );
    }

    public function testSyncAssigneeSuccessAddedNewAssignee()
    {
        $salesMotion = factory(SalesMotion::class)->create(['id' => 10]);
        factory(Consultant::class)->create(['id' => 10]);

        $input = [
            'consultants' => ['10']
        ];
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('PUT', '/admin/consultant/sales-motion/10/sync/assignee', $input);

        $response->assertJson(
            [
                'status' => true,
                'message' => 'Assignee successfully updated'
            ]
        );

        $this->assertEquals(1, $salesMotion->consultants()->count());
    }

    public function testSyncAssigneeShouldUpdateAssignee()
    {
        factory(Consultant::class)->create(['id' => 10]);
        factory(Consultant::class)->create(['id' => 20]);

        $salesMotion = factory(SalesMotion::class)->create(['id' => 10]);
        $salesMotion->consultants()->sync([10]);

        $this->assertEquals(1, $salesMotion->consultants()->count());
        $this->assertEquals(10, $salesMotion->consultants[0]->id);

        $input = [
            'consultants' => [20]
        ];
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->json('PUT', '/admin/consultant/sales-motion/10/sync/assignee', $input);

        $response->assertJson(
            [
                'status' => true,
                'message' => 'Assignee successfully updated'
            ]
        );

        $salesMotion = SalesMotion::find($salesMotion->id);

        $this->assertEquals(1, $salesMotion->consultants()->count());
        $this->assertEquals(20, $salesMotion->consultants[0]->id);
    }

    public function testListAssignedConsultantShouldReturnAssignedConsultant(): void
    {
        factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => 1,
                'consultant_sales_motion_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->get('/admin/consultant/sales-motion/1/assigned/consultant');

        $response->assertJson([
            'assignee_ids' => [1]
        ]);
    }

    public function testListAssignedConsultantShouldNotReturnNonAssignedConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->get('/admin/consultant/sales-motion/1/assigned/consultant');

        $response->assertJson([
            'assignee_ids' => []
        ]);
    }

    public function testListAssignedConsultantShouldNotReturnOtherSalesMotion(): void
    {
        factory(SalesMotionAssignee::class)->create(
            [
                'consultant_id' => 1,
                'consultant_sales_motion_id' => 2
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->get('/admin/consultant/sales-motion/1/assigned/consultant');

        $response->assertJson([
            'assignee_ids' => []
        ]);
    }

    public function testDetailDataWithAssigneeEmpty(): void
    {
        $media = factory(Media::class)->create();
        $salesMotion = factory(SalesMotion::class)->create(['media_id' => $media->id]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get("/admin/consultant/sales-motion/detail/$salesMotion->id/data");

        $response->assertSuccessful();
        $response->assertJson([
            'status' => true,
            'sales_motion' => [
                'id' => $salesMotion->id,
                'name' => $salesMotion->name,
                'created_by_division' => $salesMotion->created_by_division,
                'objective' => $salesMotion->objective,
                'terms_and_condition' => $salesMotion->terms_and_condition,
                'requirement' => $salesMotion->requirement,
                'benefit' => $salesMotion->benefit,
                'max_participant' => $salesMotion->max_participant,
                'link' => $salesMotion->url,
                'photos' => [
                    'small' => $media->getMediaUrl()['small'],
                    'medium' => $media->getMediaUrl()['medium'],
                    'large' => $media->getMediaUrl()['large'],
                ],
                'start_date' => $salesMotion->start_date,
                'due_date' => $salesMotion->end_date,
                'created_at' => $salesMotion->created_at->toString(),
                'updated_at' => $salesMotion->updated_at->toString(),
                'created_by' => $salesMotion->createdBy->name,
                'updated_by' => $salesMotion->lastUpdatedBy->name,
                'is_active' => $salesMotion->is_active
            ],
            'total' => 0,
            'assignees' => []
        ]);
    }

    public function testUpdateWithNullMediaId(): void
    {
        factory(Media::class)->create(['id' => 2]);
        $salesMotion = factory(SalesMotion::class)->create([
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => '2020-09-03',
            'end_date' => '2020-10-02',
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1',
            'is_active' => true
        ]);
        
        $input = [
            'name' => 'test sales motion updated',
            'type' => SalesMotion::TYPE_PROMOTION,
            'created_by_division' => SalesMotion::DIVISION_MARKETING,
            'end_date' => '2020-10-01',
            'objective' => 'test updated',
            'terms_and_condition' => 'test updated',
            'requirement' => 'test updated',
            'benefit' => 'test updated',
            'max_participant' => '21 orang',
            'url' => 'http://www.googlee.com',
            'media_id' => null,
            'is_active' => false,
        ];

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('PUT', '/admin/consultant/sales-motion/update/'.$salesMotion->id, $input)
            ->assertJson([
                'status' => true,
                'message' => 'Successfully updated',
                'sales_motion_id' => $salesMotion->id
            ]);

        $response->assertJson(['status' => true]);
    }

    public function testUpdateShouldSaveInDatabaseCorrectly()
    {
        factory(Media::class)->create(['id' => 2]);
        $salesMotion = factory(SalesMotion::class)->create([
            'name' => 'test sales motion',
            'type' => SalesMotion::TYPE_CAMPAIGN,
            'created_by_division' => SalesMotion::DIVISION_COMMERCIAL,
            'start_date' => '2020-09-03',
            'end_date' => '2020-10-02',
            'objective' => 'test',
            'terms_and_condition' => 'test',
            'requirement' => 'test',
            'benefit' => 'test',
            'max_participant' => '20 orang',
            'url' => 'http://www.google.com',
            'media_id' => '1',
            'is_active' => true
        ]);
        
        $input = [
            'name' => 'test sales motion updated',
            'type' => SalesMotion::TYPE_PROMOTION,
            'created_by_division' => SalesMotion::DIVISION_MARKETING,
            'end_date' => '2020-10-01',
            'objective' => 'test updated',
            'terms_and_condition' => 'test updated',
            'requirement' => 'test updated',
            'benefit' => 'test updated',
            'max_participant' => '21 orang',
            'url' => 'http://www.googlee.com',
            'media_id' => '2',
            'is_active' => false,
        ];

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('PUT', '/admin/consultant/sales-motion/update/'.$salesMotion->id, $input)
            ->assertJson([
                'status' => true,
                'message' => 'Successfully updated',
                'sales_motion_id' => $salesMotion->id
            ]);

        $input['updated_by'] = $this->consultant->id;
        $this->assertDatabaseHas('consultant_sales_motion', $input);

    }

    public function testUpdateShouldNotChangeStartDate()
    {
        factory(Media::class)->create(['id' => 2]);
        $salesMotion = factory(SalesMotion::class)->create([
            'start_date' => '2020-09-03',
            'end_date' => '2020-10-02',
        ]);
        $input = [
            'name' => 'test sales motion updated',
            'type' => SalesMotion::TYPE_PROMOTION,
            'created_by_division' => SalesMotion::DIVISION_MARKETING,
            'start_date' => '2020-09-13',
            'end_date' => '2020-10-01',
            'is_active' => false,
        ];

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('PUT', '/admin/consultant/sales-motion/update/'.$salesMotion->id, $input)
            ->assertJson([
                'status' => true,
                'message' => 'Successfully updated',
                'sales_motion_id' => $salesMotion->id
            ]);
        
        $this->assertDatabaseHas('consultant_sales_motion', [
            'id' => $salesMotion->id,
            'start_date' => $salesMotion->start_date,
            'end_date' => $input['end_date']
        ]);
    }

    public function testAssignAllWithInvalidRole(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'test']));

        $response->assertSessionHasErrors([
            'role' => 'The selected role is invalid.'
        ]);
    }

    public function testAssignAllWithAdminRole(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'admin']));

        $this->assertDatabaseHas('consultant_sales_motion_assignee', ['consultant_id' => $consultant->id, 'consultant_sales_motion_id' => $salesMotion->id]);
    }

    public function testAssignAllWithSupplyRole(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'supply']));

        $this->assertDatabaseHas('consultant_sales_motion_assignee', ['consultant_id' => $consultant->id, 'consultant_sales_motion_id' => $salesMotion->id]);
    }

    public function testAssignAllWithDemandRole(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'demand']));

        $this->assertDatabaseHas('consultant_sales_motion_assignee', ['consultant_id' => $consultant->id, 'consultant_sales_motion_id' => $salesMotion->id]);
    }

    public function testAssignAllWithTwoRolesConsultant(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);

        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'demand']));

        $this->assertDatabaseHas('consultant_sales_motion_assignee', ['consultant_id' => $consultant->id, 'consultant_sales_motion_id' => $salesMotion->id]);
    }

    public function testAssignAllShouldAssignConsultants(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultants = factory(Consultant::class, 2)->create();

        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultants[0]->id,
            'role' => 'demand'
        ]);

        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultants[1]->id,
            'role' => 'demand'
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'demand']));

        $this->assertDatabaseHas('consultant_sales_motion_assignee', [
            'consultant_id' => $consultants[0]->id,
            'consultant_sales_motion_id' => $salesMotion->id
        ]);
        $this->assertDatabaseHas('consultant_sales_motion_assignee', [
            'consultant_id' => $consultants[1]->id,
            'consultant_sales_motion_id' => $salesMotion->id
        ]);
    }

    public function testAssignAllShouldNotDoubleAssignedConsultant(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create();

        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);

        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $consultant->id
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(route('admin.consultant.sales-motion.assign-all', ['id' => $salesMotion->id, 'role' => 'demand']));

        $this->assertEquals(
            1,
            SalesMotionAssignee::where([
                'consultant_sales_motion_id' => $salesMotion->id,
                'consultant_id' => $consultant->id
            ])->count()
        );
    }
}
