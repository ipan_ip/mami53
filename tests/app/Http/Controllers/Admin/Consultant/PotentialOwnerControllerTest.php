<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\PotentialOwner;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Http\Helpers\PhoneNumberHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PotentialOwnerControllerTest extends MamiKosTestCase
{

    protected const INDEX = '/admin/consultant/potential-owner';
    protected const INDEX_DATA = '/admin/consultant/potential-owner/data';
    protected const BULK_UPLOAD = '/admin/consultant/potential-owner/bulk';
    protected const DETAIL = '/admin/consultant/potential-owner/detail/';
    protected const STORE = '/admin/consultant/potential-owner/store';
    protected const UPDATE = '/admin/consultant/potential-owner/update';
    protected const DELETE = '/admin/consultant/potential-owner/';

    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->consultant = factory(User::class)->create();

        $role->attachPermission(factory(Permission::class)->create(['name' => 'admin-access']));
        $role->attachPermission(factory(Permission::class)->create(['name' => 'access-consultant']));
        $role->attachPermission(factory(Permission::class)->create(['name' => 'access-potential-owner']));
        $this->consultant->attachRole($role);
    }

    public function testIndex(): void
    {
        $expected = 'admin.contents.consultant.potential-owner.index';
        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX);

        $actual->assertViewIs($expected);
    }

    public function testIndexData(): void
    {
        $user = factory(User::class)->create(['hostility' => 0]);
        $owner = factory(PotentialOwner::class)->create(['user_id' => $user->id]);

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA);
        $expected = [
            'total' => 1,
            'rows' => [
                [
                    'id' => $owner->id,
                    'name' => $owner->name,
                    'phone_number' => $owner->phone_number,
                    'is_hostile' => false,
                    'total_property' => $owner->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $owner->updated_at->toString(),
                    'updated_by' => null
                ]
            ]
        ];

        $actual->assertJson($expected);
    }

    public function testIndexDataWithOwnerHostility(): void
    {
        $user = factory(User::class)->create(['hostility' => 2]);
        $owner = factory(PotentialOwner::class)->create(['user_id' => $user->id]);

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA);
        $expected = [
            'total' => 1,
            'rows' => [
                [
                    'id' => $owner->id,
                    'name' => $owner->name,
                    'phone_number' => $owner->phone_number,
                    'is_hostile' => true,
                    'total_property' => $owner->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $owner->updated_at->toString(),
                    'updated_by' => null
                ]
            ]
        ];

        $actual->assertJson($expected);
    }

    public function testIndexDataShouldShowUseCorrectLimit(): void
    {
        $owner = factory(PotentialOwner::class, 16)->create();

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA);

        $actual->assertJsonCount(15, 'rows');
    }

    public function testIndexDataShouldShowMatchingOwnerName(): void
    {
        $owner = factory(PotentialOwner::class)->create(
            [
                'name' => 'test',
                'phone_number' => '0811111111'
            ]
        );

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA . '?query=es');
        $expected = [
            'total' => 1,
            'rows' => [
                [
                    'id' => $owner->id,
                    'name' => $owner->name,
                    'phone_number' => $owner->phone_number,
                    'total_property' => $owner->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $owner->updated_at->toString(),
                    'updated_by' => null
                ]
            ]
        ];

        $actual->assertJson($expected);
    }

    public function testIndexDataShouldShowMatchingOwnerPhone(): void
    {
        $owner = factory(PotentialOwner::class)->create(
            [
                'name' => 'test',
                'phone_number' => '0811111111'
            ]
        );

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA . '?query=11');
        $expected = [
            'total' => 1,
            'rows' => [
                [
                    'id' => $owner->id,
                    'name' => $owner->name,
                    'phone_number' => $owner->phone_number,
                    'total_property' => $owner->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $owner->updated_at->toString(),
                    'updated_by' => null
                ]
            ]
        ];

        $actual->assertJson($expected);
    }

    public function testUpdateFailedValidationOnName()
    {
        $input = [
            'phone_number' => '08111111',
            'email' => 'test@email.com',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        factory(PotentialOwner::class)->create(['id' => '1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['name' => []]
            ]
        );
    }

    public function testUpdateNotPassValidationOnPhoneNumber(): void
    {
        $input = [
            'name' => 'test',
            'email' => 'test@email.com',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        factory(PotentialOwner::class)->create(['id' => '1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => ['phone_number' => []]
            ]
        );
    }

    public function testUpdatePhoneNumberIsUsedByOtherOwnerShouldNotUpdate(): void
    {
        $input = [
            'name' => 'test',
            'phone_number' => '08111111',
            'email' => 'test@email.com',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        factory(PotentialOwner::class)->create(['id' => '1']);
        factory(PotentialOwner::class)->create(['phone_number' => '8111111']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $response->assertJson(
            [
                'status' => false,
                'message' => [
                    'phone_number' => ['Phone number is already used by other owner']
                ]
            ]
        );
    }

    public function testUpdateSuccess(): void
    {
        $input = [
            'name' => 'test',
            'phone_number' => '08111111',
            'email' => 'test@email.com',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        factory(PotentialOwner::class)->create(['id' => '1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $response->assertJson(
            [
                'status' => true,
                'message' => 'Potential Owner data successfully updated'
            ]
        );
    }

    public function testUpdateWithEmptyEmailShouldSaveCorrectly(): void
    {
        $input = [
            'name' => 'test',
            'phone_number' => '08111111',
            'email' => '',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        $owner = factory(PotentialOwner::class)->create(['id' => '1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $owner->id,
            'name' => 'test',
            'phone_number' => '8111111',
            'email' => null,
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ]);
    }

    public function testUpdateWithDuplicateNullEmailShouldSave(): void
    {
        $input = [
            'name' => 'test',
            'phone_number' => '08111111',
            'email' => '',
            'remark' => 'test note',
            'date_to_visit' => '2020-07-28'
        ];

        factory(PotentialOwner::class)->create(['email' => null]);
        $owner = factory(PotentialOwner::class)->create(['id' => '1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->post(self::UPDATE . '/1', $input);

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $owner->id,
            'name' => 'test',
            'phone_number' => '8111111',
            'email' => null,
            'remark' => 'test note'
        ]);
    }

    public function testBulkUpload(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $response->assertJson(['status' => true]);
    }

    public function testBulkUploadWithInvalidFile(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => 'a']);

        $response->assertSessionHasErrors([
            'file' => 'The file must be a file.'
        ]);
    }

    public function testIndexDataShouldSortByUpdatedAtDesc(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $lastUpdated = factory(PotentialOwner::class)->create(['updated_at' => Carbon::now()->addHour()]);

        $actual = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::INDEX_DATA);
        $expected = [
            'total' => 2,
            'rows' => [
                [
                    'id' => $lastUpdated->id,
                    'name' => $lastUpdated->name,
                    'phone_number' => $lastUpdated->phone_number,
                    'total_property' => $lastUpdated->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $lastUpdated->updated_at->toString(),
                    'updated_by' => null,
                ],
                [
                    'id' => $owner->id,
                    'name' => $owner->name,
                    'phone_number' => $owner->phone_number,
                    'total_property' => $owner->total_property,
                    'followup_status' => 'new',
                    'updated_at' => $owner->updated_at->toString(),
                    'updated_by' => null,
                ]
            ]
        ];

        $actual->assertJson($expected);
    }

    /* public function testDetailSuccess(): void
    {
        factory(PotentialOwner::class)->create(
            [
                'file' => 'The file must be a file.'
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->get(self::DETAIL.'10');

        $response->assertViewIs('admin.contents.consultant.potential-owner.detail');
    } */

    public function testBulkUploadShouldSaveIntoDatabase(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0822222222'),
            'email' => 'jane.doe@example.com',
            'total_property' => 0,
            'followup_status' => 'new',
            'remark' => 'Ibunya guling guling',
            'created_by' => $this->consultant->id,
        ]);
    }

    public function testBulkUploadShouldUpdateExistingPhoneNumber(): void
    {
        $existing = factory(PotentialOwner::class)->create([
            'name' => 'John Doe',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0811111111'),
            'email' => 'john.doe@example.com',
            'total_property' => 0,
            'total_room' => 0,
            'remark' => 'Bapaknya loncat loncat',
            'created_by' => 0,
        ]);

        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertDatabaseMissing('consultant_potential_owner', [
            'name' => 'John Doe',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0811111111'),
            'email' => 'john.doe@example.com',
            'total_property' => 0,
            'total_room' => 0,
            'remark' => 'Bapaknya loncat loncat',
            'created_by' => 0,
        ]);

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $existing->id,
            'name' => 'Jaen Doe',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0811111111'),
            'email' => 'jaen.doe@example.com',
            'total_property' => 0,
            'total_room' => 0,
            'remark' => 'Bapaknya menggeliat',
            'created_by' => 0,
            'updated_by' => $this->consultant->id
        ]);
    }

    public function testBulkUploadShouldRejectNonUniqueEmail(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertDatabaseMissing('consultant_potential_owner', [
            'name' => 'Jim Doe',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0833333333'),
            'email' => 'john.doe@example.com',
            'total_property' => 0,
            'total_room' => 0,
            'remark' => 'Jimmy jimmy jimbowololo',
            'created_by' => 0,
        ]);
    }

    public function testBulkUploadShouldSaveNullEmailAndNote(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertDatabaseMissing('consultant_potential_owner', [
            'name' => 'Jombo',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0844444444'),
            'email' => null,
            'total_property' => 0,
            'total_room' => 0,
            'remark' => null,
            'created_by' => 0,
        ]);
    }

    public function testBulkUploadShouldSaveDuplicateNullEmail(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertDatabaseMissing('consultant_potential_owner', [
            'name' => 'Jembo',
            'phone_number' => PhoneNumberHelper::sanitizeNumber('0855555555'),
            'email' => null,
            'total_property' => 0,
            'total_room' => 0,
            'remark' => null,
            'created_by' => 0,
        ]);
    }

    public function testBulkUploadShouldInsertCorrectCount(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $this->assertEquals(4, PotentialOwner::count());
    }

    public function testBulkUploadShouldReturnStatusAndCount(): void
    {
        $file = $this->getUploadableFile('consultant/potential-owner-import.csv');
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD, ['file' => $file]);

        $response->assertJson([
            'status' => true,
            'data' => [
                'success_count' => 5,
                'failed_count' => 1,
                'failed' => [
                    ['jane.doe@example.com' => 'Email potential owner sudah dipakai']
                ]
            ]
        ]);
    }

    public function testBulkUploadWithMissingFile(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->post(self::BULK_UPLOAD);

        $response->assertSessionHasErrors(
            [
                'file' => 'The file field is required.'
            ]
        );
    }

    public function testDeleteWithInvalidId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->delete(self::DELETE . 0);

        $response->assertNotFound();
        $response->assertJson(['status' => false]);
    }

    public function testDelete(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->consultant
            )
            ->delete(self::DELETE . $owner->id);

        $response->assertOk();
        $response->assertJson(['status' => true]);
    }
}
