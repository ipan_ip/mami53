<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\User;
use Carbon\Carbon;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Entrust\Role;
use App\Entities\Level\KostLevel;
use App\Entities\Property\Property;
use App\Entities\Entrust\Permission;
use App\Entities\Level\KostLevelMap;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Consultant\ConsultantRoom;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ConsultantRoomMappingControllerTest extends MamiKosTestCase
{
    
    protected $consultant;
    protected $adminAccess;

    protected function setUp(): void
    {
        parent::setUp();

        $this->consultant = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $this->adminAccess = factory(Permission::class)->create(['name' => 'admin-access']);
        $role->attachPermission($this->adminAccess);
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant-room-mapping'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-mapping-system'])
        );
        $this->consultant->attachRole($role);
    }

    public function testIndexDataWithoutParamsHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(factory(Room::class)->make(['area_city' => 'Sleman']));
        $room = $consultant->rooms()->first();

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/data/'
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'consultant_id' => $consultant->id,
                        'consultant' => $consultant->name,
                        'area' => $room->area_city,
                        'property_mapped' => 1
                    ]
                ]
            ]
        );
    }

    public function testIndexDataWithoutParamsHaveNoData()
    {
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/data/'
        );
        $response->assertJson(
            [
                'total' => 0,
                'rows' => []
            ]
        );
    }

    public function testIndexDataWithParamSearchByConsultant()
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(factory(Room::class)->make(['area_city' => 'Sleman']));
        $room = $consultant->rooms()->first();

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/data/',
            ['searchBy' => 'consultant', 'search' => $consultant->name]
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'consultant_id' => $consultant->id,
                        'consultant' => $consultant->name,
                        'area' => $room->area_city,
                        'property_mapped' => 1
                    ]
                ]
            ]
        );
    }

    public function testIndexDataWithParamSearchByPropertyName()
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(factory(Room::class)->make(['area_city' => 'Sleman']));
        $room = $consultant->rooms()->first();

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/data/',
            ['searchBy' => 'property', 'search' => $room->name]
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'consultant_id' => $consultant->id,
                        'consultant' => $consultant->name,
                        'area' => $room->area_city,
                        'property_mapped' => 1
                    ]
                ]
            ]
        );
    }

    public function testIndexDataWithParamSearchByAreaCity()
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(factory(Room::class)->make(['area_city' => 'Sleman']));
        $room = $consultant->rooms()->first();

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/data/',
            ['searchBy' => 'area', 'search' => $room->area_city]
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'consultant_id' => $consultant->id,
                        'consultant' => $consultant->name,
                        'area' => $room->area_city,
                        'property_mapped' => 1
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithoutParamsHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);
        $goldLevel = factory(KostLevel::class)->create([
            'id' => 123,
            'name' => 'Mamikos Goldplus 1',
            'is_regular' => 0,
            'is_hidden' => 0
        ]);
        $room->level()->attach($goldLevel);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman'
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung',
                        'kost_level' => 'Mamikos Goldplus 1'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithoutKostLevel()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman'
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung',
                        'kost_level' => null
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithoutParamsHaveNoData()
    {
        $consultant = factory(Consultant::class)->create();

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman'
        );
        $response->assertJson(
            [
                'total' => 0,
                'rows' => []
            ]
        );
    }

    public function testAddDataWithParamFilterKostLevelHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);
        $otherRoom = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);

        $goldLevel = factory(KostLevel::class)->create([
                'id' => 123,
                'name' => 'Mamikos Goldplus 1',
                'is_regular' => 0,
                'is_hidden' => 0
        ]);
        $room->level()->attach($goldLevel);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['level_id' => $goldLevel->id]
        );

        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung',
                        'kost_level' => 'Mamikos Goldplus 1'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithParamSearchHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['search' => $room->name]
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithParamFilterPropertyTypeFreeListingHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['property_type' => 'free_listing']
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Free Listing'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithParamFilterPropertyTypeBookingHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['property_type' => 'booking']
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithParamFilterPropertyTypeBookingAndSearchHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 1, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['search' => $room->name, 'property_type' => 'booking']
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Booking Langsung'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithParamFilterPropertyTypeFreeListingAndSearchHaveData()
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'true', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['search' => $room->name, 'property_type' => 'free_listing']
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Free Listing'
                    ]
                ]
            ]
        );
    }

    public function testAddDataShouldNotDisplayRoomMappedToOtherAdminChat(): void
    {
        $consultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => ConsultantRole::ADMIN_CHAT
        ]);
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'true', 'expired_phone' => false]);
        $otherConsultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(['consultant_id' => $otherConsultant->id, 'role' => ConsultantRole::ADMIN_CHAT]);
        factory(ConsultantRoom::class)->create(['designer_id' => $room->id, 'consultant_id' => $otherConsultant->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['search' => $room->name, 'property_type' => 'free_listing']
        );
        $response->assertJson(
            [
                'total' => 0,
                'rows' => []
            ]
        );
    }

    public function testAddDataWithNonAdminChatShouldDisplayRoomMappedToOtherAdminChat(): void
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'true', 'expired_phone' => false]);
        $otherConsultant = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create(['consultant_id' => $otherConsultant->id, 'role' => ConsultantRole::ADMIN_CHAT]);
        factory(ConsultantRoom::class)->create(['designer_id' => $room->id, 'consultant_id' => $otherConsultant->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            ['search' => $room->name, 'property_type' => 'free_listing']
        );
        $response->assertJson(
            [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $room->id,
                        'song_id' => $room->song_id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'type' => 'Free Listing'
                    ]
                ]
            ]
        );
    }

    public function testAddDataWithNonActiveKostShouldReturnEmpty(): void
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'false', 'expired_phone' => false]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            []
        );

        $response->assertJson([
            'total' => 0,
            'rows' => []
        ]);
    }

    public function testAddDataWithExpiredPhoneKostShouldReturnEmpty(): void
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->create(['area_city' => 'Sleman', 'is_booking' => 0, 'is_active' => 'true', 'expired_phone' => true]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/add/data/' . $consultant->id . '/Sleman',
            []
        );

        $response->assertJson([
            'total' => 0,
            'rows' => []
        ]);
    }

    public function testDetailDataWithInvalidIdShouldReturnFalse(): void
    {
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/detail/data/0/Sleman'
        );
        $response->assertJson(['status' => false, 'meta' => ['message' => 'Consultant not found']]);
    }

    public function testDetailDataWithInvalidPermission(): void
    {
        $consultant = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $role->attachPermission($this->adminAccess);
        $consultant->attachRole($role);

        $response = $this->setWebApiCookie()->actingAs($consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/detail/data/0/Sleman'
        );
        $response->assertStatus(403);
    }

    public function testDetailDataShouldReturnData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(factory(Room::class)->make(['area_city' => 'Sleman']));
        $room = $consultant->rooms()->first();

        $goldLevel = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'is_hidden' => 0
        ]);
        $room->level()->attach($goldLevel);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman'
        );

        $response->assertJson(
            [
                'status' => true,
                'count' => 1,
                'consultant' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                ],
                'data' => [
                    [
                        'id' => $room->id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'is_booking' => $room->is_booking,
                        'kost_level' => 'Mamikos Goldplus 1'
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithPropertyNameSuffixSearchShouldReturnData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(
            factory(Room::class)->make(
                [
                    'name' => 'test',
                    'area_city' => 'Sleman'
                ]
            )
        );
        $room = $consultant->rooms()->first();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman' . '?search=st')
        );
        $response->assertJson(
            [
                'status' => true,
                'count' => 1,
                'consultant' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                ],
                'data' => [
                    [
                        'id' => $room->id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'is_booking' => $room->is_booking,
                        'kost_level' => null
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithPropertyNamePrefixSearchShouldReturnData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(
            factory(Room::class)->make(
                [
                    'name' => 'test',
                    'area_city' => 'Sleman'
                ]
            )
        );
        $room = $consultant->rooms()->first();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman' . '?search=te')
        );
        $response->assertJson(
            [
                'status' => true,
                'count' => 1,
                'consultant' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                ],
                'data' => [
                    [
                        'id' => $room->id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'is_booking' => $room->is_booking
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithPropertyNameSearchShouldReturnData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(
            factory(Room::class)->make(
                [
                    'name' => 'test',
                    'area_city' => 'Sleman'
                ]
            )
        );
        $room = $consultant->rooms()->first();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman' . '?search=test')
        );
        $response->assertJson(
            [
                'status' => true,
                'count' => 1,
                'consultant' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                ],
                'data' => [
                    [
                        'id' => $room->id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'is_booking' => $room->is_booking
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithInvalidPropertyNameShouldNotReturnData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(
            factory(Room::class)->make(
                [
                    'name' => 'test',
                    'area_city' => 'Sleman'
                ]
            )
        );
        $room = $consultant->rooms()->first();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman' . '?search=dest')
        );
        $response->assertJsonCount(0, 'data');
        $response->assertJsonMissing(
            [
                'count' => 1,
                'data' => [
                    [
                        'id' => $room->id,
                        'name' => $room->name,
                        'area_city' => $room->area_city,
                        'is_booking' => $room->is_booking
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithDefaultLimitShouldReturnCorrectAmount(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->saveMany(factory(Room::class, 20)->make(['area_city' => 'Sleman']));
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman')
        );
        $response->assertJsonCount(15, 'data');
    }

    public function testDetailDataWithCustomLimitShouldReturnCorrectAmount(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->saveMany(factory(Room::class, 4)->make(['area_city' => 'Sleman']));
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman' . '?limit=2')
        );
        $response->assertJsonCount(2, 'data');
    }

    public function testDetailDataWithDefaultOffsetShouldReturnFirstPageData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->saveMany(factory(Room::class, 16)->make(['area_city' => 'Sleman']));
        $rooms = $consultant->rooms()->get();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman')
        );
        $response->assertJson(
            [
                'count' => 16,
                'consultant' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                ],
                'data' => [
                    [
                        'id' => $rooms[0]->id,
                        'name' => $rooms[0]->name,
                        'area_city' => $rooms[0]->area_city,
                        'is_booking' => $rooms[0]->is_booking
                    ],
                    [
                        'id' => $rooms[1]->id,
                        'name' => $rooms[1]->name,
                        'area_city' => $rooms[1]->area_city,
                        'is_booking' => $rooms[1]->is_booking
                    ],
                    [
                        'id' => $rooms[2]->id,
                        'name' => $rooms[2]->name,
                        'area_city' => $rooms[2]->area_city,
                        'is_booking' => $rooms[2]->is_booking
                    ],
                    [
                        'id' => $rooms[3]->id,
                        'name' => $rooms[3]->name,
                        'area_city' => $rooms[3]->area_city,
                        'is_booking' => $rooms[3]->is_booking
                    ],
                    [
                        'id' => $rooms[4]->id,
                        'name' => $rooms[4]->name,
                        'area_city' => $rooms[4]->area_city,
                        'is_booking' => $rooms[4]->is_booking
                    ],
                    [
                        'id' => $rooms[5]->id,
                        'name' => $rooms[5]->name,
                        'area_city' => $rooms[5]->area_city,
                        'is_booking' => $rooms[5]->is_booking
                    ],
                    [
                        'id' => $rooms[6]->id,
                        'name' => $rooms[6]->name,
                        'area_city' => $rooms[6]->area_city,
                        'is_booking' => $rooms[6]->is_booking
                    ],
                    [
                        'id' => $rooms[7]->id,
                        'name' => $rooms[7]->name,
                        'area_city' => $rooms[7]->area_city,
                        'is_booking' => $rooms[7]->is_booking
                    ],
                    [
                        'id' => $rooms[8]->id,
                        'name' => $rooms[8]->name,
                        'area_city' => $rooms[8]->area_city,
                        'is_booking' => $rooms[8]->is_booking
                    ],
                    [
                        'id' => $rooms[9]->id,
                        'name' => $rooms[9]->name,
                        'area_city' => $rooms[9]->area_city,
                        'is_booking' => $rooms[9]->is_booking
                    ],
                    [
                        'id' => $rooms[10]->id,
                        'name' => $rooms[10]->name,
                        'area_city' => $rooms[10]->area_city,
                        'is_booking' => $rooms[10]->is_booking
                    ],
                    [
                        'id' => $rooms[11]->id,
                        'name' => $rooms[11]->name,
                        'area_city' => $rooms[11]->area_city,
                        'is_booking' => $rooms[11]->is_booking
                    ],
                    [
                        'id' => $rooms[12]->id,
                        'name' => $rooms[12]->name,
                        'area_city' => $rooms[12]->area_city,
                        'is_booking' => $rooms[12]->is_booking
                    ],
                    [
                        'id' => $rooms[13]->id,
                        'name' => $rooms[13]->name,
                        'area_city' => $rooms[13]->area_city,
                        'is_booking' => $rooms[13]->is_booking
                    ],
                    [
                        'id' => $rooms[14]->id,
                        'name' => $rooms[14]->name,
                        'area_city' => $rooms[14]->area_city,
                        'is_booking' => $rooms[14]->is_booking
                    ]
                ]
            ]
        );
    }

    public function testDetailDataWithDefaultOffsetShouldNotReturnSecondPageData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $consultant->rooms()->saveMany(factory(Room::class, 16)->make(['area_city' => 'Sleman']));
        $rooms = $consultant->rooms()->get();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            ('/admin/consultant/room-mapping/detail/data/' . $consultant->id . '/Sleman')
        );
        $response->assertJsonMissing(
            [
                'data' => [
                    'id' => $consultant->id,
                    'name' => $consultant->name,
                    'rooms' => [
                        [
                            'id' => $rooms[15]->id,
                            'name' => $rooms[15]->name,
                            'area_city' => $rooms[15]->area_city,
                            'is_booking' => $rooms[15]->is_booking
                        ]
                    ]
                ]
            ]
        );
    }

    public function testGetConsultantShouldReturnData(): void
    {
        $consultants = factory(Consultant::class, 4)->create();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    ['id' => $consultants[0]->id, 'name' => $consultants[0]->name],
                    ['id' => $consultants[1]->id, 'name' => $consultants[1]->name],
                    ['id' => $consultants[2]->id, 'name' => $consultants[2]->name],
                    ['id' => $consultants[3]->id, 'name' => $consultants[3]->name],
                ]
            ]
        );
    }

    public function testGetConsultantShouldReturnFourData(): void
    {
        $consultants = factory(Consultant::class, 4)->create();
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant'
        );
        $response->assertJsonCount(4, 'data');
    }

    public function testGetConsultantWithSearchPrefixNameShouldReturnMatchingConsultant(): void
    {
        $consultant = factory(Consultant::class)->create(
            [
                'name' => 'test'
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant?search=te'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    ['id' => $consultant->id, 'name' => 'test'],
                ]
            ]
        );
    }

    public function testGetConsultantWithSearchSuffixNameShouldReturnMatchingConsultant(): void
    {
        $consultant = factory(Consultant::class)->create(
            [
                'name' => 'test'
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant?search=st'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    ['id' => $consultant->id, 'name' => 'test'],
                ]
            ]
        );
    }

    public function testGetConsultantWithSearchNameShouldReturnMatchingConsultant(): void
    {
        $consultant = factory(Consultant::class)->create(
            [
                'name' => 'test'
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant?search=test'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    ['id' => $consultant->id, 'name' => 'test'],
                ]
            ]
        );
    }

    public function testGetConsultantWithSearchNameShouldNotReturnNonMatchingConsultant(): void
    {
        $consultant = factory(Consultant::class)->create(
            [
                'name' => 'test'
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'GET',
            '/admin/consultant/room-mapping/consultant?search=dest'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => []
            ]
        );
    }

    public function testReassignShouldReturnTrue(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $consultant->rooms()->save(
            factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'true', 'expired_phone' => false])
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $response->assertJson(['status' => true]);
    }

    public function testReassignShouldChangeConsultantInDatabase(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'true', 'expired_phone' => false]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseHas(
            'consultant_designer',
            [
                'consultant_id' => $otherConsultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testReassignShouldDeleteNonActiveKostFromMapping(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'false']);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $otherConsultant->id,
                'designer_id' => $room->id
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testReassignShouldDeleteExpiredPhoneKostFromMapping(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'expired_phone' => true]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $otherConsultant->id,
                'designer_id' => $room->id
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testReassignShouldDeleteSoftDeletedKostFromMapping(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'deleted_at' => Carbon::now()]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testReassignShouldDeleteNonIndexedKostFromMapping(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'false', 'expired_phone' => true, 'deleted_at' => Carbon::now()]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testReassignShouldDeleteOldMappingInDatabase(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'true', 'expired_phone' => false]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id,
            ]
        );
    }

    public function testReassignShouldNotDoubleAssignConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'true', 'expired_phone' => false]);
        $consultant->rooms()->save($room);
        $otherConsultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );

        $this->assertEquals(1, ConsultantRoom::where('consultant_id', $otherConsultant->id)->where('designer_id', $room->id)->count());
    }

    public function testReassignShouldNotDeleteRoomInDatabase(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherConsultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul', 'is_active' => 'true', 'expired_phone' => false]);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/reassign',
            [
                'old_consultant_id' => $consultant->id,
                'new_consultant_id' => $otherConsultant->id,
                'area_city' => 'Bantul'
            ]
        );
        $this->assertDatabaseHas(
            'designer',
            [
                'id' => $room->id
            ]
        );
    }

    public function testDestroyShouldReturnTrue(): void
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul']);
        $consultant->rooms()->save($room);

        $response = $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/delete',
            [
                'consultant_id' => $consultant->id,
                'property_ids' => [$room->id]
            ]
        );
        $response->assertJson(['status' => true]);
    }

    public function testDestroyShouldDeletedRecord(): void
    {
        $consultant = factory(Consultant::class)->create();
        $room = factory(Room::class)->make(['area_city' => 'Bantul']);
        $consultant->rooms()->save($room);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/delete',
            [
                'consultant_id' => $consultant->id,
                'property_ids' => [$room->id]
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
    }

    public function testDestroyWithTwoMappingShouldDeletedRecord(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherRoom = factory(Room::class)->make(['area_city' => 'Bantul']);
        $room = factory(Room::class)->make(['area_city' => 'Bantul']);
        $consultant->rooms()->save($room);
        $consultant->rooms()->save($otherRoom);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/delete',
            [
                'consultant_id' => $consultant->id,
                'property_ids' => [$room->id, $otherRoom->id]
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $room->id
            ]
        );
        $this->assertDatabaseMissing(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $otherRoom->id
            ]
        );
    }

    public function testDestroyShouldNotDeleteOtherMapping(): void
    {
        $consultant = factory(Consultant::class)->create();
        $otherRoom = factory(Room::class)->make(['area_city' => 'Bantul']);
        $room = factory(Room::class)->make(['area_city' => 'Bantul']);
        $consultant->rooms()->save($room);
        $consultant->rooms()->save($otherRoom);

        $this->setWebApiCookie()->actingAs($this->consultant)->json(
            'POST',
            '/admin/consultant/room-mapping/delete',
            [
                'consultant_id' => $consultant->id,
                'property_ids' => [$room->id]
            ]
        );
        $this->assertDatabaseHas(
            'consultant_designer',
            [
                'consultant_id' => $consultant->id,
                'designer_id' => $otherRoom->id
            ]
        );
    }
}
