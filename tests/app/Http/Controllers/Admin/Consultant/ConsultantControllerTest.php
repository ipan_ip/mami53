<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use App\Facades\SendBirdFacade;

class ConsultantControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    protected $user;
    protected $role;
    protected $permission;
    protected $userRepositoryMock;
    protected $consultantMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();

        $this->user->attachRole($this->role);
        $this->role->attachPermission($this->permission);

        $this->userRepositoryMock = \Mockery::mock('App\Repositories\UserDataRepository');
        $this->consultantMock = \Mockery::mock('App\Entities\Consultant\Consultant');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }

    public function testIndexWithoutParam()
     {
         factory(Consultant::class)->create(['user_id' => $this->user->id]);

         $response = $this->actingAs($this->user)
             ->get('admin/consultant');
         $response->assertSuccessful();
         $response->assertViewIs('admin.contents.consultant.index');
         $response->assertViewHas('consultants');
     }

     public function testIndexWithParam()
     {
         factory(Consultant::class)->create(['user_id' => $this->user->id]);

         $response = $this->actingAs($this->user)
             ->get('admin/consultant', ['key' => $this->user->name]);
         $response->assertSuccessful();
         $response->assertViewIs('admin.contents.consultant.index');
         $response->assertViewHas('consultants');
     }

    public function testIndexUserDontHavePhoto()
    {
        factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $response = $this->actingAs($this->user)
            ->get('admin/consultant');

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.index');
        $response->assertViewHas('consultants');

        $consultants = $response->getOriginalContent()->getData()['consultants'];
        $this->assertNull($consultants[0]->photo_url);
    }

    public function testIndexUserHavePhoto()
    {
        factory(Consultant::class)->create(['user_id' => $this->user->id]);
        factory(Media::class)->create(['id' => $this->user->photo_id]);

        $response = $this->actingAs($this->user)
            ->get('admin/consultant');

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.index');
        $response->assertViewHas('consultants');

        $consultants = $response->getOriginalContent()->getData()['consultants'];
        $this->assertNotNull($consultants[0]->photo_url);
    }

    public function testStoreUserNotFound()
    {
        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $this->user->id+1,
                    'name' => $this->user->name,
                    'email' => $this->user->email,
                    'chat_id' => '',
                    'roles' => ['admin']
                ]
            );

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message','User not found');
    }

    public function testStoreForExistingUser()
    {
        SendBirdFacade::shouldReceive('updateUserMetadata')
            ->with($this->user->id,['role' => 'admin'])
            ->andReturnTrue();

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $this->user->id,
                    'name' => $this->user->name,
                    'email' => $this->user->email,
                    'chat_id' => '',
                    'roles' => ['admin']
                ]
            );

        $this->assertEquals(302, $response->getStatusCode());

        $consultant = Consultant::where('user_id', $this->user->id)->first();
        $this->assertNotNull($consultant);
    }

    public function testStoreForNewUser()
    {
        SendBirdFacade::shouldReceive('updateUserMetadata')
            ->andReturnTrue();

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => null,
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'chat_id' => '',
                    'photo' => UploadedFile::fake()->image('photo2.jpg'),
                    'roles' => ['admin']
                ]
            );
        $this->assertEquals(302, $response->getStatusCode());

        $consultant = Consultant::where('email', 'test@email.com')->first();
        $newUser = User::where('email', 'test@email.com')->first();
        $this->assertNotNull($consultant);
        $this->assertNotNull($newUser);
    }

    public function testStoreConsultantAlreadyExist()
    {
        factory(Consultant::class)->create(['user_id'=>$this->user->id]);

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $this->user->id,
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'chat_id' => '',
                    'photo' => UploadedFile::fake()->image('photo2.jpg'),
                    'roles' => ['admin']
                ]
            );

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message','User already registered');
    }

    public function testStoreWithRolesShouldSave(): void
    {
        SendBirdFacade::shouldReceive('updateUserMetadata')
            ->with($this->user->id,['role' => 'admin'])
            ->andReturnTrue();

        $user = factory(User::class)->create();
        $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'chat_id' => '',
                    'roles' => ['admin']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'admin'
        ]);
    }

    public function testStoreWithTwoRolesShouldSaveBoth(): void
    {
        SendBirdFacade::shouldReceive('updateUserMetadata')
            ->with($this->user->id,['role' => 'admin'])
            ->andReturnTrue();;

        $user = factory(User::class)->create();
        $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'chat_id' => '',
                    'roles' => ['admin', 'demand']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'admin'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'demand'
        ]);
    }

    public function testStoreWithThreeRoleShouldSaveAll(): void
    {
        SendBirdFacade::shouldReceive('updateUserMetadata')
            ->with($this->user->id,['role' => 'admin'])
            ->andReturnTrue();

        $user = factory(User::class)->create();
        $this->actingAs($this->user)
            ->post(
                'admin/consultant/store',
                [
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'chat_id' => '',
                    'roles' => ['admin', 'demand', 'supply']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'admin'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'demand'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => Consultant::where('user_id', $user->id)->first()->id,
            'role' => 'supply'
        ]);
    }

    public function testStorePhoto()
    {
        $this->userRepositoryMock
            ->shouldReceive('uploadProfilePhoto')
            ->once()
            ->andReturn('');

        $consultantController = new ConsultantController($this->userRepositoryMock);
        $response = $consultantController->storePhoto('', $this->user);

        $this->assertNotNull($response);
    }

    public function testUpdate()
    {
        SendBirdFacade::shouldReceive('updateUser')
            ->andReturn('');

        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin']
                ]
            );
        $consultant = Consultant::find($consultant->id);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals('test', $consultant->name);
        $this->assertEquals('test@email.com', $consultant->email);
    }

    public function testUpdateExistingConsultantWithRolesShouldSaveRole(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);
    }

    public function testUpdateExistingConsultantWithTwoRolesShouldSaveRoles(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin', 'demand']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);
    }

    public function testUpdateExistingConsultantWithThreeRolesShouldSaveRoles(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin', 'demand', 'supply']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);
    }

    public function testUpdateExistingConsultantWithExistingRoleShouldSaveRole(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin', 'supply']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);
    }

    public function testUpdateExistingConsultantWithExistingRoleShouldNotDeleteOldRole(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin', 'supply']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);
    }

    public function testUpdateExistingConsultantShouldReplaceRole(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);
        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);
    }

    public function testUpdateExistingConsultantShouldReplaceRoles(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => factory(User::class)->create()->id]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);
        factory(ConsultantRole::class)->create([
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);

        $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . $consultant->id,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin']
                ]
            );

        $this->assertDatabaseHas('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'admin'
        ]);
        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'supply'
        ]);
        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => 'demand'
        ]);
    }

    public function testUpdateConsultantNotFound()
    {
        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/update/' . 10,
                [
                    'name' => 'test',
                    'email' => 'test@email.com',
                    'roles' => ['admin']
                ]
            );
        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Consultant not found');
    }

    public function testDestroy()
    {
        $room = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->roles()->save($role);

        $response = $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertEquals(302, $response->getStatusCode());

        $checkConsultant = Consultant::where('user_id', $this->user->id)->first();
        $this->assertNull($checkConsultant);
    }

    public function testDestroyShouldDeleteRooms(): void
    {
        $room = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertDatabaseMissing('consultant_designer', [
            'consultant_id' => $consultant->id,
            'designer_id' => $room->id
        ]);
    }

    public function testDestroyWithTwoRoomsShouldDeleteBoth(): void
    {
        $room = factory(Room::class)->make();
        $otherRoom = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->rooms()->save($otherRoom);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertDatabaseMissing('consultant_designer', [
            'consultant_id' => $consultant->id,
            'designer_id' => $room->id
        ]);
        $this->assertDatabaseMissing('consultant_designer', [
            'consultant_id' => $consultant->id,
            'designer_id' => $otherRoom->id
        ]);
    }

    public function testDestroyShouldNotDeleteOriginalRoom(): void
    {
        $room = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id
        ]);
    }

    public function testDestroyShouldDeleteRoles(): void
    {
        $room = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => $role->role
        ]);
    }

    public function testDestroyWithTwoRolesShouldDeleteBoth(): void
    {
        $room = factory(Room::class)->make();
        $role = factory(ConsultantRole::class)->make();
        $otherRole = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);
        $consultant->roles()->save($role);
        $consultant->roles()->save($otherRole);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => $role->role
        ]);
        $this->assertSoftDeleted('consultant_role', [
            'consultant_id' => $consultant->id,
            'role' => $otherRole->role
        ]);
    }

    public function testDestoryExistingWithoutRolesAndRoomsShouldReturnTrue(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $response = $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $response->assertStatus(302);
    }

    public function testDestroyExistingWithoutRolesAndRoomsShouldDeleteConsultant(): void
    {
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant', [
            'id' => $consultant->id
        ]);
    }

    public function testDestroyExistingWithoutRolesShouldReturnTrue(): void
    {
        $room = factory(Room::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);

        $response = $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $response->assertStatus(302);
    }

    public function testDestroyExistingWithoutRolesShouldDeleteConsultant(): void
    {
        $room = factory(Room::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant', [
            'id' => $consultant->id
        ]);
    }

    public function testDestroyExistingWithoutRolesShouldDeleteRoomsMapping(): void
    {
        $room = factory(Room::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->rooms()->save($room);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertDatabaseMissing('consultant_designer', [
            'consultant_id' => $consultant->id,
            'designer_id' => $room->id
        ]);
    }

    public function testDestroyExistingWithoutRoomsShouldReturnTrue(): void
    {
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->roles()->save($role);

        $response = $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $response->assertStatus(302);
    }

    public function testDestroyExistingWithoutRoomsShouldDeleteConsultant(): void
    {
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant', [
            'id' => $consultant->id
        ]);
    }

    public function testDestroyExistingWithoutRoomsShouldDeleteRoles(): void
    {
        $role = factory(ConsultantRole::class)->make();
        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $consultant->roles()->save($role);

        $this->actingAs($this->user)
            ->get('/admin/consultant/delete/' . $consultant->id);

        $this->assertSoftDeleted('consultant_role', [
            'id' => $role->id,
            'consultant_id' => $consultant->id,
            'role' => $role->role
        ]);
    }

    public function testGetRoomChat()
    {
        $permissionChat = 'access-consultant-chat-' . $this->user->id;
        $this->role->attachPermission($permissionChat);

        $consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $response = $this->actingAs($this->user)
            ->get('admin/consultant/room-chat/' . $consultant->id);

        $this->assertEquals(302, $response->getStatusCode());
    }
}