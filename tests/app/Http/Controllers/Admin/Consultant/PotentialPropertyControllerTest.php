<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;

class PotentialPropertyControllerTest extends MamiKosTestCase
{
    use WithFaker;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();
        factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $this->user->attachRole($role);
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'delete-potential-property'])
        );
    }

    /**
     * @group UG
     * @group UG-4226
     */
    public function testGetPropertyByOwner(): void
    {
        $kost = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 1
        ]);
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json('GET', '/admin/consultant/potential-owner/1/property/data');
        $response->assertJson([
            'total' => 1,
            'properties' => [
                [
                    'id' => $kost->id,
                    'name' => $kost->name,
                    'address' => $kost->address,
                    'area_city' => $kost->area_city,
                    'province' => $kost->province,
                    'total_room' => $kost->total_room,
                    'offered_product' => $kost->extractOfferedProduct(),
                    'remark' => $kost->remark
                ]
            ]
        ]);
    }

    public function testMediaUploadForPotentialPropertyShouldSaveIntoDatabase(): void
    {
        return;
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json('POST', '/admin/media', [
                'media' => UploadedFile::fake()->image('property.png'),
                'media_type' => Media::CONSULTANT_PHOTO_TYPE,
                'need_watermark' => 'false'
            ]);

        $this->assertDatabaseHas('media', [
            'id' => $response->getData()->media->id
        ]);
    }

    public function testStoreWithMissingName(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word,
                    'is_priority' => $this->faker->boolean
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['name' => ['The name field is required.']]
        ]);
    }

    public function testStoreWithEmptyName(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => '',
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['name' => ['The name field is required.']]
        ]);
    }

    public function testStoreWithNullName(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => null,
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['name' => ['The name field is required.']]
        ]);
    }

    public function testStoreWithTooLongName(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => $this->faker->sentence(70),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['name' => ['The name may not be greater than 255 characters.']]
        ]);
    }

    public function testStoreWithoutExistantMediaId(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => 0,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['media_id' => ['The selected media id is invalid.']]
        ]);
    }

    public function testStoreWithoutAddress(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['address' => ['The address field is required.']]
        ]);
    }

    public function testStoreWithNullAddress(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => null,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['address' => ['The address field is required.']]
        ]);
    }

    public function testStoreWithEmptyAddress(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => '',
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['address' => ['The address field is required.']]
        ]);
    }

    public function testStoreWithTooLongAddress(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->sentence(70),
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['address' => ['The address may not be greater than 255 characters.']]
        ]);
    }

    public function testStoreWithoutAreaCity(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['area_city' => ['The area city field is required.']]
        ]);
    }

    public function testStoreWithNullAreaCity(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => null,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['area_city' => ['The area city field is required.']]
        ]);
    }

    public function testStoreWithEmptyAreaCity(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => '',
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['area_city' => ['The area city field is required.']]
        ]);
    }

    public function testStoreWithTooLongAreaCity(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->sentence(70),
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['area_city' => ['The area city may not be greater than 255 characters.']]
        ]);
    }

    public function testStoreWithoutProvince(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['province' => ['The province field is required.']]
        ]);
    }

    public function testStoreWithNullProvince(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => null,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['province' => ['The province field is required.']]
        ]);
    }

    public function testStoreWithEmptyProvince(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => '',
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['province' => ['The province field is required.']]
        ]);
    }

    public function testStoreWithTooLongProvince(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->sentence(255),
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['province' => ['The province may not be greater than 255 characters.']]
        ]);
    }

    public function testStoreWithoutIntegerTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => 'a',
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['total_room' => ['The total room must be an integer.']]
        ]);
    }

    public function testStoreWithInvalidOfferedProduct(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => 'gp',
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['offered_product' => ['The offered product must be an array.']]
        ]);
    }

    public function testStoreWithInvalidOfferedProductItem(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => ['pro'],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['offered_product.0' => ['The selected offered_product.0 is invalid.']]
        ]);
    }

    public function testStoreWithTooLongRemark(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->sentence(70)
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['remark' => ['The remark may not be greater than 255 characters.']]
        ]);
    }

    public function testStoreWithoutPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['is_priority' => ['The is priority field is required.']]
        ]);
    }

    public function testStoreWithNullPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word,
                    'is_priority' => null
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['is_priority' => ['The is priority field is required.']]
        ]);
    }

    public function testStoreWithEmptyPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word,
                    'is_priority' => ''
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['is_priority' => ['The is priority field is required.']]
        ]);
    }

    public function testStoreWithInvalidPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'name' => ('Kost ' . $this->faker->address),
                    'media_id' => $media->id,
                    'address' => $this->faker->address,
                    'area_city' => $this->faker->city,
                    'province' => $this->faker->city,
                    'total_room' => $this->faker->numberBetween(1, 500),
                    'offered_product' => [ 
                        $this->faker->randomElement([
                            PotentialProperty::PRODUCT_GP,
                            PotentialProperty::PRODUCT_BBK
                        ])
                    ],
                    'remark' => $this->faker->word,
                    'is_priority' => 'benar'
                ]
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['is_priority' => ['The is priority field must be true or false.']]
        ]);
    }

    public function testStoreShouldReturnCorrectResponse(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $response->assertJson([
            'status' => true,
            'property' => $expected
        ]);
    }

    public function testStoreShouldSaveIntoDatabase(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                [
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'followup_status' => 'new',
                    'is_priority' => $property->is_priority,
                    'media_id' => $media->id,
                    'name' => $property->name,
                    'offered_product' => ['gp1'],
                    'potential_gp_room_list' => $this->faker->text(1024),
                    'potential_gp_total_room' => $this->faker->numberBetween(1, 255),
                    'province' => $property->province,
                    'remark' => $property->remark,
                    'total_room' => $property->total_room,
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', [
            'name' => $property->name,
            'media_id' => $media->id,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'total_room' => $property->total_room,
            'offered_product' => implode(',', ['gp1']),
            'remark' => $property->remark,
            'is_priority' => $property->is_priority,
            'created_by' => $this->user->id
        ]);
    }

    public function testStoreWithTwoOfferedProductShouldSave(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;
        $expected['offered_product'] = ['gp1', 'gp2'];

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $expected['created_by'] = $this->user->id;
        $expected['offered_product'] = implode(',', $expected['offered_product']);
        $this->assertDatabaseHas('consultant_potential_property', $expected);
    }

    public function testStoreShouldIncrementOwnerTotalPropertyAndRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 5,
            'total_room' => 2,
        ]);
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $owner->id,
            'total_property' => ($owner->total_property + 1),
            'total_room' => ($owner->total_room + $expected['total_room']),
            'updated_by' => $this->user->id
        ]);
    }

    public function testStoreWithoutTotalRoomShouldReturnFail(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['total_room'] = null;
        $expected['media_id'] = $media->id;

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $response->assertJson([
            'status' => false
        ]);
    }

    public function testStoreWithoutRemarkShouldSave(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['remark'] = '';
        $expected['media_id'] = $media->id;

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $expected['created_by'] = $this->user->id;
        $expected['offered_product'] = implode(',', $expected['offered_product']);
        $this->assertDatabaseHas('consultant_potential_property', $expected);
    }

    public function testStoreWithoutPhotoShouldReturnSuccess(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $expected = $this->makePostParam();
        unset($expected['media_id']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $response->assertJson([
            'status' => true,
            'property' => $expected
        ]);
    }

    public function testStoreWithoutPhotoShouldSave(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $expected = $this->makePostParam();
        unset($expected['media_id']);

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            );

        $expected['created_by'] = $this->user->id;
        $expected['offered_product'] = implode(',', $expected['offered_product']);
        $this->assertDatabaseHas('consultant_potential_property', $expected);
    }

    public function testStoreWithNullPhotoShouldReturnSave(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $expected = $this->makePostParam();

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/$owner->id/property",
                $expected
            )->assertJson([
                'status' => true,
                'property' => $expected
            ]);

        $expected['created_by'] = $this->user->id;
        $expected['offered_product'] = implode(',', $expected['offered_product']);
        $this->assertDatabaseHas('consultant_potential_property', $expected);
    }

    public function testStoreWithInvalidPotentialOwnerId(): void
    {
        $expected = $this->makePostParam();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'POST',
                "/admin/consultant/potential-owner/0/property",
                $expected
            );

        $response->assertNotFound();
    }


    public function testUpdateWithInvalidOfferedProduct(): void
    {
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => factory(PotentialOwner::class)->create()->id]);
        $media = factory(Media::class)->create();

        $params = $this->makePostParam();
        $params['offered_product'] = ['unrecognized'];

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/$property->id/property",
                $params
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['offered_product.0' => ['The selected offered_product.0 is invalid.']]
        ]);
    }

    public function testUpdateWithTooLongRemark(): void
    {
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => factory(PotentialOwner::class)->create()->id]);
        $media = factory(Media::class)->create();
        $params = $this->makePostParam();
        $params['remark'] = $this->faker->sentence(70);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/$property->id/property",
                $params
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['remark' => ['The remark may not be greater than 255 characters.']]
        ]);
    }

    public function testUpdateWithInvalidPriority(): void
    {
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => factory(PotentialOwner::class)->create()->id]);
        $media = factory(Media::class)->create();

        $params = $this->makePostParam();
        $params['is_priority'] = 'benar';

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/$property->id/property",
                $params
            );

        $response->assertStatus(501);
        $response->assertJson([
            'status' => false,
            'messages' => ['is_priority' => ['The is priority field must be true or false.']]
        ]);
    }

    public function testUpdateShouldReturnSucces(): void
    {
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => factory(PotentialOwner::class)->create()->id]);
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/$property->id/property",
                $expected
            );

        $response->assertJson([
            'status' => true
        ]);
    }

    public function testUpdateWithTwoOfferedProductShouldSave(): void
    {
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => factory(PotentialOwner::class)->create()->id]);
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;
        $expected['offered_product'] = ['gp1', 'gp2'];

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/$property->id/property",
                $expected
            );

        $response->assertJson([
            'status' => true
        ]);
    }

    public function testUpdateWithInvalidPotentialPropertyIdShouldNotUpdateOwner(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 1,
            'total_room' => 5
        ]);
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => $owner->id,
            'total_room' => 5
        ]);
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/0/property",
                $expected
            );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $owner->id,
            'total_property' => $owner->total_property,
            'total_room' => $owner->total_room,
            'updated_by' => $owner->updated_by
        ]);
    }

    public function testUpdateWithInvalidPotentialPropertyIdShouldReturnNotFoundResponse(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 1,
            'total_room' => 5
        ]);
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => $owner->id,
            'total_room' => 5
        ]);
        $media = factory(Media::class)->create();

        $expected = $this->makePostParam();
        $expected['media_id'] = $media->id;

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'PATCH',
                "/admin/consultant/potential-owner/0/property",
                $expected
            );

        $response->assertNotFound();
    }

    public function testDeleteShouldReturnSuccess(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 1,
            'total_room' => 5
        ]);
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => $owner->id,
            'total_room' => 5
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'DELETE',
                "/admin/consultant/potential-owner/$property->id/property"
            );
        $response->assertJson(['status' => true]);
    }

    public function testDeleteShouldDelete(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 1,
            'total_room' => 5
        ]);
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => $owner->id,
            'total_room' => 5
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'DELETE',
                "/admin/consultant/potential-owner/$property->id/property"
            );

        $this->assertDatabaseMissing('consultant_potential_property', ['id' => $property->id]);
    }

    public function testDeleteShouldUpdateOwner(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'total_property' => 1,
            'total_room' => 5
        ]);
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => $owner->id,
            'total_room' => 5
        ]);

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->json(
                'DELETE',
                "/admin/consultant/potential-owner/$property->id/property"
            );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'id' => $owner->id,
            'total_property' => 0,
            'total_room' => 0
        ]);
    }

    private function makePostParam()
    {
        return [
            'address' => $this->faker->address,
            'area_city' => $this->faker->city,
            'followup_status' => 'new',
            'is_priority' => $this->faker->boolean,
            'media_id' => null,
            'name' => ('Kost ' . $this->faker->address),
            'offered_product' => [ 
                $this->faker->randomElement([
                    PotentialProperty::PRODUCT_GP1,
                    PotentialProperty::PRODUCT_GP2,
                    PotentialProperty::PRODUCT_GP3,
                ])
            ],
            'potential_gp_room_list' => $this->faker->text(1024),
            'potential_gp_total_room' => $this->faker->numberBetween(1, 255),
            'province' => $this->faker->city,
            'remark' => $this->faker->word,
            'total_room' => $this->faker->numberBetween(1, 500),
        ];
    }
}
