<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantMapping;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class ConsultantMappingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    protected $user;
    protected $role;
    protected $permission;
    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();

        $this->user->attachRole($this->role);
        $this->role->attachPermission($this->permission);

        $this->consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
    }

    protected function tearDown(): void
    {
        if (class_exists('Mockery')) {
            \Mockery::close();
        }

        parent::tearDown();
    }

    public function testIndexWithoutParam()
    {
        factory(ConsultantMapping::class)->create(['consultant_id' => $this->consultant->id]);

        $response = $this->actingAs($this->user)
            ->get('admin/consultant/mapping');

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.mapping-index');
        $response->assertViewHas('mappings');
        $response->assertViewHas('cities');

        $mappings = $response->getOriginalContent()->getData()['mappings'];
        $this->assertEquals(1, count($mappings));
    }

    public function testIndexWithParamName()
    {
        factory(ConsultantMapping::class)->create(['consultant_id' => $this->consultant->id]);

        $response = $this->actingAs($this->user)
            ->get('admin/consultant/mapping', ['name' => $this->user->name]);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.mapping-index');
        $response->assertViewHas('mappings');
        $response->assertViewHas('cities');

        $mappings = $response->getOriginalContent()->getData()['mappings'];
        $this->assertEquals(1, count($mappings));
        $this->assertEquals($this->consultant->id, $mappings[0]->consultant_id);
    }

    public function testIndexWithParamCity()
    {
        factory(ConsultantMapping::class)->create(
            ['consultant_id' => $this->consultant->id, 'area_city' => 'Surabaya']
        );

        $response = $this->actingAs($this->user)
            ->get('admin/consultant/mapping', ['city' => 'Surabaya']);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.mapping-index');
        $response->assertViewHas('mappings');
        $response->assertViewHas('cities');

        $mappings = $response->getOriginalContent()->getData()['mappings'];
        $this->assertEquals(1, count($mappings));
        $this->assertEquals('Surabaya', $mappings[0]->area_city);
    }

    public function testStoreSuccess()
    {
        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/mapping/store',
                ['consultant_id' => $this->consultant->id, 'city' => ['Surabaya'], 'is_mamirooms' => [1]]
            );

        $this->assertEquals(302, $response->getStatusCode());

        $response->assertSessionHasNoErrors();

        $mapping = ConsultantMapping::where('consultant_id', $this->consultant->id)
            ->where('area_city', 'Surabaya')
            ->where('is_mamirooms', true)
            ->first();

        $this->assertNotNull($mapping);
        $this->assertEquals($this->consultant->id, $mapping->consultant_id);
        $this->assertEquals('Surabaya', $mapping->area_city);
    }

    public function testStoreFailedValidation()
    {
        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/store');

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('errors');

        $sessions = session('errors');
        $this->assertEquals(3, $sessions->count());
        $this->assertNotNull($sessions->get('city'));
        $this->assertNotNull($sessions->get('consultant_id'));
        $this->assertNotNull($sessions->get('is_mamirooms'));
    }

    public function testStoreFailedConsultantNotFound()
    {
        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/mapping/store',
                ['consultant_id' => $this->consultant->id + 1, 'city' => ['Surabaya'], 'is_mamirooms' => [1]]
            );

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Sorry, the data consultant not found');
    }

    public function testStoreFailedMappingAlreadyExist()
    {
        factory(ConsultantMapping::class)->create(
            ['consultant_id' => $this->consultant->id, 'area_city' => 'Surabaya', 'is_mamirooms' => 1]
        );

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/mapping/store',
                ['consultant_id' => $this->consultant->id, 'city' => ['Surabaya'], 'is_mamirooms' => [1]]
            );

        $countMapping = ConsultantMapping::where('consultant_id', $this->consultant->id)
            ->where('is_mamirooms', true)
            ->where('area_city', 'Surabaya')
            ->count();

        $this->assertEquals(1, $countMapping);
    }

    public function testStoreWithMany()
    {
        factory(ConsultantMapping::class)->create(
            ['consultant_id' => $this->consultant->id, 'area_city' => 'Surabaya', 'is_mamirooms' => 1]
        );
        factory(ConsultantMapping::class)->create(
            ['consultant_id' => $this->consultant->id, 'area_city' => 'Jakarta', 'is_mamirooms' => true]
        );
        factory(ConsultantMapping::class)->create(
            ['consultant_id' => $this->consultant->id, 'area_city' => 'Jakarta', 'is_mamirooms' => false]
        );

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/mapping/store',
                ['consultant_id' => $this->consultant->id, 'city' => ['Surabaya', 'Jakarta', 'Semarang'], 'is_mamirooms' => [true, false]]
            );

        $response->assertSessionHasNoErrors();

        $countMapping = ConsultantMapping::where('consultant_id', $this->consultant->id)
            ->whereIn('is_mamirooms', [true, false])
            ->whereIn('area_city', ['Surabaya', 'Jakarta', 'Semarang'])
            ->count();

        $this->assertEquals(6, $countMapping);
    }

    public function testStoreFailedThrowException()
    {
        $exception = new \Exception();
        $mappingMock = \Mockery::mock(ConsultantMapping::class);
        $mappingMock->shouldReceive('save')->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')->andReturn(null);

        $this->expectException(\Exception::class);

        $response = $this->actingAs($this->user)
            ->post(
                'admin/consultant/mapping/store',
                ['consultant_id' => $this->consultant->id, 'city' => ['Surabaya'], 'is_mamirooms' => [1]]
            );

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Failed add consultant mapping');
    }

    public function testUpdateSuccess()
    {
        $mapping = factory(ConsultantMapping::class)->create(['consultant_id' => $this->consultant->id, 'area_city' => 'Jakarta']);

        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/update/' . $mapping->id, ['city' => 'Surabaya']);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('message', 'Successfully update consultant mapping');

        $mapping = ConsultantMapping::find($mapping->id);
        $this->assertNotNull($mapping);
        $this->assertEquals('Surabaya', $mapping->area_city);
    }

    public function testUpdateFailedValidation()
    {
        $mapping = factory(ConsultantMapping::class)->create(['area_city' => 'Jakarta']);

        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/update/' . $mapping->id);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('errors');

        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('city'));
    }

    public function testUpdateFailedConsultantMappingNotFound()
    {
        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/update/10000', ['city' => 'Surabaya']);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Sorry, the data mapping not found');
    }

    public function testUpdateFailedConsultantMappingAlreadyExist()
    {
        $mapping = factory(ConsultantMapping::class)->create(['consultant_id' => $this->consultant->id, 'area_city' => 'Jakarta', 'is_mamirooms' => false]);

        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/update/' . $mapping->id, ['city' => 'Jakarta']);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Sorry, the data consultant already registered');
    }

    public function testUpdateFailedThrowException()
    {
        $mapping = factory(ConsultantMapping::class)->create(['area_city' => 'Jakarta']);

        $exception = new \Exception();
        $mappingMock = \Mockery::mock(ConsultantMapping::class);
        $mappingMock->shouldReceive('save')->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')->andReturn(null);

        $this->expectException(\Exception::class);

        $response = $this->actingAs($this->user)
            ->post('admin/consultant/mapping/update/' . $mapping->id, ['city' => 'Surabaya']);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertSessionHas('error_message', 'Failed add consultant mapping');
    }

    public function testDestroySuccess()
    {
        $mapping = factory(ConsultantMapping::class)->create(['area_city' => 'Jakarta']);
        $response = $this->actingAs($this->user)
            ->delete('admin/consultant/mapping/delete/' . $mapping->id);

        $response->assertJson(['status' => true]);
    }

    public function testDestroyFailedConsultantMappingNotFound()
    {
        $response = $this->actingAs($this->user)
            ->delete('admin/consultant/mapping/delete/10000');

        $response->assertJson(['status' => false]);
    }
}