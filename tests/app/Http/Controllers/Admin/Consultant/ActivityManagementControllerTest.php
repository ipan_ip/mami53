<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\InputType;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Http\Controllers\Admin\Consultant\ActivityManagementController;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Input;

class ActivityManagementControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $user;
    protected $role;

    protected const FUNNEL_INDEX_URL = 'admin/consultant/activity-management';
    protected const FUNNEL_DATA_API = 'admin/consultant/activity-management/funnel';
    protected const FUNNEL_SHOW_URL = 'admin/consultant/activity-management/funnel/';
    protected const STAGING_DATA_API = 'admin/consultant/activity-management/staging/';
    protected const STAGING_DETAIL_URL = 'admin/consultant/activity-management/staging/detail/';
    protected const STAGING_DETAIL_API = 'admin/consultant/activity-management/forms/';
    protected const FORM_INPUT_UPDATE_API = 'admin/consultant/activity-management/forms/';
    protected const FORM_INPUT_ADD_API = 'admin/consultant/activity-management/forms/';

    protected const FUNNEL_CREATE_URL = 'admin/consultant/activity-management/create/funnel';
    protected const FUNNEL_STORE_URL = 'admin/consultant/activity-management/store/funnel';
    protected const FUNNEL_EDIT_URL = 'admin/consultant/activity-management/edit/funnel/';
    protected const FUNNEL_UPDATE_URL = 'admin/consultant/activity-management/update/funnel/';
    protected const FUNNEL_DELETE_URL = 'admin/consultant/activity-management/delete/funnel/';

    protected const STAGING_CREATE_URL = 'admin/consultant/activity-management/create/staging/';
    protected const STAGING_STORE_URL = 'admin/consultant/activity-management/store/staging/';
    protected const STAGING_UPDATE_URL = 'admin/consultant/activity-management/update/staging/';
    protected const STAGING_DELETE_URL = 'admin/consultant/activity-management/delete/staging/';

    protected const FORM_INPUT_DELETE_URL = 'admin/consultant/activity-management/delete/forms/';

    protected function setUp(): void
    {
        parent::setUp();

        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();

        $this->user->attachRole($this->role);
        $this->role->attachPermission(factory(Permission::class)->create(['name' => 'access-consultant']));
        $this->role->attachPermission(factory(Permission::class)->create(['name' => 'access-activity-management']));
        $this->role->attachPermission(factory(Permission::class)->create(['name' => 'access-delete-activity-management']));
    }

    public function testIndex(): void
    {
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_INDEX_URL);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-index');
    }

    public function testFunnelDataShouldReturnSuccess(): void
    {
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_DATA_API);

        $response->assertSuccessful();
    }

    public function testFunnelDataShouldReturnTrue(): void
    {
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_DATA_API);

        $response->assertJson(['status' => true]);
    }

    public function testFunnelDataWithOneFunnelShouldReturnCorrectTotal(): void
    {
        factory(ActivityFunnel::class)->create();

        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_DATA_API);

        $response->assertJson(['total' => 1]);
    }

    public function testFunnelDataWithMoreThanDefaultPaginationShouldReturnCorrectTotal(): void
    {
        $funnelCount = (ActivityManagementController::DEFAULT_PAGINATION + 1);
        factory(ActivityFunnel::class, $funnelCount)->create();

        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_DATA_API);

        $response->assertJson(['total' => $funnelCount]);
    }

    public function testFunnelDataWithOffsetShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $otherFunnel = factory(ActivityFunnel::class)->create(['updated_at' => Carbon::now()->addMinute()]);

        $response = $this->actingAs($this->user)
            ->get((self::FUNNEL_DATA_API . '?offset=' . 1));
        $response->assertJson(
            [
                'total' => 2,
                'funnels' => [
                    [
                        'id' => $funnel->id,
                        'funnel_name' => $funnel->name,
                        'role' => $funnel->consultant_role,
                        'data_type' => $funnel->related_table,
                        'staging' => $funnel->total_stage,
                        'updated_at' => $funnel->updated_at
                    ]
                ]
            ]
        );
    }

    public function testFunnelDataWithOffsetShouldOnlyReturnCorrectData(): void
    {
        $defaulPagination = ActivityManagementController::DEFAULT_PAGINATION;
        $funnelCount = ($defaulPagination + 1);
        $funnels = factory(ActivityFunnel::class, $funnelCount)->create();

        $response = $this->actingAs($this->user)
            ->get((self::FUNNEL_DATA_API . '?offset=' . $defaulPagination));
        $response->assertJsonCount(1, 'funnels');
    }

    public function testFunnelDataWithCustomLimitShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $otherFunnel = factory(ActivityFunnel::class)->create(['updated_at' => Carbon::now()->subHour()]);

        $response = $this->actingAs($this->user)
            ->get((self::FUNNEL_DATA_API . '?limit=1'));
        $response->assertJson(
            [
                'total' => 2,
                'funnels' => [
                    [
                        'id' => $funnel->id,
                        'funnel_name' => $funnel->name,
                        'role' => $funnel->consultant_role,
                        'data_type' => $funnel->related_table,
                        'staging' => $funnel->total_stage,
                        'updated_at' => $funnel->updated_at
                    ]
                ]
            ]
        );
    }

    public function testFunnelDataWithCustomLimitAndOffsetShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $otherFunnel = factory(ActivityFunnel::class)->create(['updated_at' => Carbon::now()->subHour()]);

        $response = $this->actingAs($this->user)
            ->get((self::FUNNEL_DATA_API . '?limit=1&offset=1'));
        $response->assertJson(
            [
                'total' => 2,
                'funnels' => [
                    [
                        'id' => $otherFunnel->id,
                        'funnel_name' => $otherFunnel->name,
                        'role' => $otherFunnel->consultant_role,
                        'data_type' => $otherFunnel->related_table,
                        'staging' => $otherFunnel->total_stage,
                        'updated_at' => $otherFunnel->updated_at
                    ]
                ]
            ]
        );
    }

    public function testFunnelDataWithCustomLimitShouldReturnOnlyCorrectData(): void
    {
        $defaulPagination = ActivityManagementController::DEFAULT_PAGINATION;
        $funnelCount = ($defaulPagination + 1);
        $funnels = factory(ActivityFunnel::class, $funnelCount)->create();

        $response = $this->actingAs($this->user)
            ->get((self::FUNNEL_DATA_API . '?limit=1'));
        $response->assertJsonCount(1, 'funnels');
    }

    public function testShow(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();

        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_SHOW_URL . $funnel->id);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-detail');
    }

    public function testShowShouldHaveCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_SHOW_URL . $funnel->id);

        $response->assertViewHas(
            [
                'funnel_name' => $funnel->name,
                'consultant_role' => $funnel->consultant_role,
                'data_type' => $funnel->related_table,
            ]
        );
    }

    public function testStagingDataShouldReturnSuccess(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->make();
        $form->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ]
        ];
        $funnel->forms()->save($form);
        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DATA_API . $funnel->id));
        $response->assertSuccessful();
    }

    public function testStagingDataShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->make();
        $form->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ]
        ];
        $funnel->forms()->save($form);
        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DATA_API . $funnel->id));
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'stages' => [
                    [
                        'id' => $form->id,
                        'stage_phase' => $form->stage,
                        'stage_name' => $form->name,
                        'description' => $form->detail,
                        'input_form' => 1
                    ]
                ]
            ]
        );
    }

    public function testStagingDataWithTwoFormInputsShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();

        $form = factory(ActivityForm::class)->make();
        $form->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ],
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 2
            ]
        ];
        $funnel->forms()->save($form);

        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DATA_API . $funnel->id));
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'stages' => [
                    [
                        'id' => $form->id,
                        'stage_phase' => $form->stage,
                        'stage_name' => $form->name,
                        'description' => $form->detail,
                        'input_form' => 2
                    ],
                ]
            ]
        );
    }

    public function testStagingDataWithTwoStagesShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();

        $form = factory(ActivityForm::class)->make();
        $form->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ]
        ];
        $funnel->forms()->save($form);

        $otherForm = factory(ActivityForm::class)->make();
        $otherForm->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 2
            ]
        ];
        $funnel->forms()->save($otherForm);

        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DATA_API . $funnel->id));
        $response->assertJson(
            [
                'status' => true,
                'total' => 2,
                'stages' => [
                    [
                        'id' => $form->id,
                        'stage_phase' => $form->stage,
                        'stage_name' => $form->name,
                        'description' => $form->detail,
                        'input_form' => 1
                    ],
                    [
                        'id' => $otherForm->id,
                        'stage_phase' => $otherForm->stage,
                        'stage_name' => $otherForm->name,
                        'description' => $otherForm->detail,
                        'input_form' => 1
                    ]
                ]
            ]
        );
    }

    public function testStagingDataWithTwoFormInputsAndTwoStagesShouldReturnCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();

        $form = factory(ActivityForm::class)->make();
        $form->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ],
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 2
            ]
        ];
        $funnel->forms()->save($form);

        $otherForm = factory(ActivityForm::class)->make();
        $otherForm->form_element = [
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ],
            [
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 2
            ]
        ];
        $funnel->forms()->save($otherForm);

        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DATA_API . $funnel->id));
        $response->assertJson(
            [
                'status' => true,
                'total' => 2,
                'stages' => [
                    [
                        'id' => $form->id,
                        'stage_phase' => $form->stage,
                        'stage_name' => $form->name,
                        'description' => $form->detail,
                        'input_form' => 2
                    ],
                    [
                        'id' => $otherForm->id,
                        'stage_phase' => $otherForm->stage,
                        'stage_name' => $otherForm->name,
                        'description' => $otherForm->detail,
                        'input_form' => 2
                    ]
                ]
            ]
        );
    }

    public function testStageDetailShouldReturnCorrectView(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'form_element' => [
                'test' => 'test' // Represent one form for testing purpose
            ]
        ]);
        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DETAIL_URL . $stage->id));

        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-staging-detail');
    }

    public function testStageDetailShouldHaveCorrectData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'form_element' => [
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DETAIL_URL . $stage->id));

        $response->assertViewHas(
            [
                'funnel_name' => $funnel->name,
                'consultant_role' => $funnel->consultant_role,
                'data_type' => $funnel->related_table,
                'stage_name' => $stage->name,
            ]
        );
    }

    public function testFormsDataShouldReturnCorrectData(): void
    {
        $stage = factory(ActivityForm::class)->create(
            [
                'form_element' => [
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DETAIL_API . $stage->id));
        $response->assertJson(
            [
                'total' => 1,
                'forms' => [
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
    }

    public function testFormsWithTwoInputsShouldReturnCorrectData(): void
    {
        $stage = factory(ActivityForm::class)->create(
            [
                'form_element' => [
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ],
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 2
                    ]
                ]
            ]
        );
        $stage->save();

        $response = $this->actingAs($this->user)
            ->get((self::STAGING_DETAIL_API . $stage->id));
        $response->assertJson(
            [
                'total' => 2,
                'forms' => [
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ],
                    [
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 2
                    ]
                ]
            ]
        );
    }

    public function testCreateFunnel(): void
    {
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_CREATE_URL);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-create-update-funnel');
    }

    public function testStoreFunnelValidationPassed(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
    }

    public function testStoreFunnelValidationFailedOnName(): void
    {
        $input = ['role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('name'));
    }

    public function testStoreFunnelValidationFailedOnRole(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'consultant', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('role'));
    }

    public function testStoreFunnelValidationFailedOnRelatedTable(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => 'consultant'];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('relatedTable'));
    }

    public function testStoreFunnelWithRoom(): void
    {
        $input = ['name' => 'Room Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_PROPERTY];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'name' => 'Room Funnel',
            'consultant_role' => 'admin',
            'related_table' => ActivityFunnel::TABLE_PROPERTY
        ]);
    }
    
    public function testStoreFunnelWithMamipayContract(): void
    {
        $input = ['name' => 'Contract Funnel', 'role' => 'admin', 'relatedTable' => 'mamipay_contract'];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'name' => 'Contract Funnel',
            'consultant_role' => 'admin',
            'related_table' => 'mamipay_contract'
        ]);
    }

    public function testStoreFunnelWithPotentialTenant(): void
    {
        $input = ['name' => 'Potential Tenant Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'name' => 'Potential Tenant Funnel',
            'consultant_role' => 'admin',
            'related_table' => ActivityFunnel::TABLE_DBET
        ]);
    }

    public function testStoreFunnelWithPotentialProperty(): void
    {
        $input = ['name' => 'Potential Property Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_STORE_URL, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'name' => 'Potential Property Funnel',
            'consultant_role' => 'admin',
            'related_table' => 'consultant_potential_property'
        ]);
    }

    public function testEditFunnelFound(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['id' => 1]);
        $response = $this->actingAs($this->user)
            ->get(self::FUNNEL_EDIT_URL . $funnel->id);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-create-update-funnel');
    }

    public function testUpdateFunnelValidationPassed(): void
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);
        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . '1', $input);

        $response->assertStatus(302);
    }

    public function testUpdateValidationPassedButFunnelNotFound(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . '1', $input);

        $response->assertStatus(302);
        $sessions = session('error_message');
        $this->assertNotNull($sessions);
    }

    public function testUpdateValidationFailedOnName(): void
    {
        $input = ['role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . '1', $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('name'));
    }

    public function testUpdateValidationFailedOnRole(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'consultant', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . '1', $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('role'));
    }

    public function testUpdateValidationFailedOnRelatedTable(): void
    {
        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => 'consultant'];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . '1', $input);

        $response->assertStatus(302);
        $sessions = session('errors');
        $this->assertEquals(1, $sessions->count());
        $this->assertNotNull($sessions->get('relatedTable'));
    }

    public function testUpdateFunnelWithProperty(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => 'mamipay_contract'
        ]);

        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_PROPERTY];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . $funnel->id, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'id' => $funnel->id,
            'name' => $input['name'],
            'consultant_role' => $input['role'],
            'related_table' => $input['relatedTable']
        ]);
    }

    public function testUpdateFunnelWithPotentialTenant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => 'mamipay_contract'
        ]);

        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => ActivityFunnel::TABLE_DBET];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . $funnel->id, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'id' => $funnel->id,
            'name' => $input['name'],
            'consultant_role' => $input['role'],
            'related_table' => $input['relatedTable']
        ]);
    }

    public function testUpdateFunnelWithPotentialProperty(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => 'mamipay_contract'
        ]);

        $input = ['name' => 'Funnel', 'role' => 'admin', 'relatedTable' => 'consultant_potential_property'];

        $response = $this->actingAs($this->user)
            ->post(self::FUNNEL_UPDATE_URL . $funnel->id, $input);

        $response->assertStatus(302);
        $this->assertDatabaseHas('consultant_activity_funnel', [
            'id' => $funnel->id,
            'name' => $input['name'],
            'consultant_role' => $input['role'],
            'related_table' => $input['relatedTable']
        ]);
    }

    public function testCreateStaging(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['id' => 1]);
        $response = $this->actingAs($this->user)
            ->get(self::STAGING_CREATE_URL . $funnel->id);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.consultant.consultant-activity-management-create-staging');
    }

    public function testStoreStagingSuccess(): void
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);
        $input = [
            'stages' => [
                [
                    'name' => 'Test',
                    'detail' => 'Coba Bikin Staging',
                    'stage' => 1
                ]
            ]
        ];

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::STAGING_STORE_URL . '1',
            $input
        );

        $response->assertJson(['status' => true]);
    }

    public function testStoreStagingFailed(): void
    {
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('POST', self::STAGING_STORE_URL . '1', []);

        $response->assertJson(['status' => false]);
    }

    public function testUpdateStagingSuccess(): void
    {
        factory(ActivityForm::class)->create(['id' => 1, 'form_element' => '']);
        $input = [
            'name' => 'Test',
            'detail' => 'Coba Bikin Staging',
            'stage' => 1
        ];

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::STAGING_UPDATE_URL . '1',
            $input
        );

        $response->assertJson(['status' => true]);
    }

    public function testUpdateFailed(): void
    {
        $input = [
            'name' => 'Test',
            'detail' => 'Coba Bikin Staging',
            'stage' => 1
        ];

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::STAGING_UPDATE_URL . '1',
            $input
        );

        $response->assertJson(['status' => false]);
    }

    public function testDeleteFunnelSuccess()
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);
        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::FUNNEL_DELETE_URL . '1'
        );

        $response->assertJson(['status' => true]);
    }

    public function testDeleteFunnelFailed()
    {
        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::FUNNEL_DELETE_URL . '1'
        );

        $response->assertJson(['status' => false]);
    }

    public function testDeleteStagingSuccess()
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);
        factory(ActivityForm::class)->create(['id' => 1, 'consultant_activity_funnel_id' => 1, 'form_element' => []]);

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::STAGING_DELETE_URL . '1'
        );

        $response->assertJson(['status' => true]);
    }

    public function testDeleteStagingFailed()
    {
        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::STAGING_DELETE_URL . '1'
        );

        $response->assertJson(['status' => false]);
    }

    public function testDeleteFormInputSuccess()
    {
        $formElements = [
            [
                "id" => 1,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 1
            ],
            [
                "id" => 5,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 2
            ]
        ];

        factory(ActivityForm::class)->create(
            [
                'id' => 1,
                'form_element' => $formElements,
                'consultant_activity_funnel_id' => 1,
                'updated_by' => $this->user->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::FORM_INPUT_DELETE_URL . '1/1'
        );

        $response->assertJson(['status' => true]);
    }

    public function testDeleteFormInputFailed()
    {
        factory(ActivityForm::class)->create(
            [
                'id' => 1,
                'form_element' => [],
                'consultant_activity_funnel_id' => 1,
                'updated_by' => $this->user->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'POST',
            self::FORM_INPUT_DELETE_URL . '2/1'
        );

        $response->assertJson(['status' => false]);
    }

    /**
     *  Test form update with type that is not in InputType
     *
     *  @return void
     *  @see App\Entities\Consultant\ActivityManagement\InputType
     */
    public function testFormUpdateWithInvalidTypeShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => 'test',
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'type' => ['The value you have entered is invalid.']
            ]
        ]);
    }

    public function testFormUpdateWithEmptyTypeShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => null,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'type' => ['The type field is required.']
            ]
        ]);
    }

    public function testFormUpdateWithEmptyLabelShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'label' => null,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'label' => ['The label field is required.']
            ]
        ]);
    }

    /**
     *  Test form update with checkbox type without sending value
     *
     *  Checkbox type must have values field
     *
     *  @return void
     */
    public function testFormUpdateWithCheckboxTypeAndEmptyValuesShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CHECKBOX,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'values' => ['Values must be filled if type has multiple value option.']
            ]
        ]);
    }

    /**
     *  Test form update with radio type without sending value
     *
     *  Radio type must have values field
     *
     *  @return void
     */
    public function testFormUpdateWithRadioTypeAndEmptyValuesShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::RADIO,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'values' => ['Values must be filled if type has multiple value option.']
            ]
        ]);
    }

    /**
     *  Test form update with select type without sending values
     *
     *  Select type must have values field
     *
     *  @return void
     */
    public function testFormUpdateWithSelectTypeAndEmptyValuesShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::SELECT,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'values' => ['Values must be filled if type has multiple value option.']
            ]
        ]);
    }

    public function testFormUpdateWithEmptyOrderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => null,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'order' => ['The order field is required.']
            ]
        ]);
    }

    public function testFormUpdateWithNonIntegerOrderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => 'a',
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'order' => ['The order must be a number.']
            ]
        ]);
    }

    public function testFormUpdateWithZeroOrderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => 0,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'order' => ['The order must be at least 1.']
            ]
        ]);
    }

    /**
     *  Test change form input type from text to clock picker when including empty array as values
     *
     *  @return void
     */
    public function testFormUpdateWithTimeTypeAndEmptyValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CLOCK_PICKER,
                'values' => [],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CLOCK_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to clock picker
     *
     *  @return void
     */
    public function testFormUpdateWithTimeTypeShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CLOCK_PICKER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CLOCK_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to clock picker
     *
     *  @return void
     */
    public function testFormUpdateWithTimeTypeAndWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CLOCK_PICKER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CLOCK_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to select when including invalid values field
     *
     *  @return void
     */
    public function testFormUpdateWithTimeTypeAndValuesShouldNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CLOCK_PICKER,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CLOCK_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to date picker when including empty values field
     *
     *  @return void
     */
    public function testFormUpdateWithDateTypeAndEmptyValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::DATE_PICKER,
                'values' => [],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::DATE_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test  change form input from text to date
     *
     *  @return void
     */
    public function testFormUpdateWithDateTypeAndWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::DATE_PICKER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::DATE_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to date picker
     *
     *  @return void
     */
    public function testFormUpdateWithDateTypeShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::DATE_PICKER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::DATE_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to date when including invalid values field
     *
     *  @return void
     */
    public function testFormUpdateWithDateTypeAndValuesShouldNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::DATE_PICKER,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::DATE_PICKER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to text when including empty values field
     *
     *  @return void
     */
    public function testFormUpdateWithTextTypeAndEmptyValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::TEXT,
                'values' => [],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to text
     *
     *  @return void
     */
    public function testFormUpdateWithTextTypeAndWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::TEXT,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to text when including invalid values field
     *
     *  @return void
     */
    public function testFormUpdateWithTextTypeAndValuesShouldNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::TEXT,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to text
     *
     *  @return void
     */
    public function testFormUpdateWithTextTypeShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::TEXT,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to number when sending empty values field
     *
     *  @return void
     */
    public function testFormUpdateWithNumberTypeAndEmptyValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::NUMBER,
                'values' => [],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::NUMBER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to number
     *
     *  @return void
     */
    public function testFormUpdateWithNumberTypeAndWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::NUMBER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::NUMBER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to number when sending invalid values field
     *
     *  @return void
     */
    public function testFormUpdateWithNumberTypeAndValuesShouldNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::NUMBER,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::NUMBER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to number
     *
     *  @return void
     */
    public function testFormUpdateWithNumberTypeShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::NUMBER,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::NUMBER,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to file when sending empty values field
     *
     *  @return void
     */
    public function testFormUpdateWithFileTypeAndEmptyValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::UPLOAD_BUTTON,
                'values' => [],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::UPLOAD_BUTTON,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to file
     *
     *  @return void
     */
    public function testFormUpdateWithFileTypeAndWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::UPLOAD_BUTTON,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::UPLOAD_BUTTON,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to file when sending invalid values field
     *
     *  @return void
     */
    public function testFormUpdateWithFileTypeAndValuesShouldNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::UPLOAD_BUTTON,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::UPLOAD_BUTTON,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to file
     *
     *  @return void
     */
    public function testFormUpdateWithFileTypeShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::UPLOAD_BUTTON,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::UPLOAD_BUTTON,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to select when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithSelectTypeAndValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::SELECT,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from radio to select when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithSelectTypeShouldReturnTrueAndSave(): void
    {
        $form = factory(ActivityForm::class)->create([
            'form_element' => [
                [
                    'id' => 1,
                    'type' => InputType::RADIO,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 2,
                    ],
                    'order' => 1
                ]
            ]
        ]);

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::SELECT,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to radio when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithRadioTypeAndValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::RADIO,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::RADIO,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to radio when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithRadioTypeAndValuesShouldUpdate(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::RADIO,
                'values' => [
                    'test' => 2
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::RADIO,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 2
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from text to checkbox when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithCheckboxTypeAndValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CHECKBOX,
                'values' => [
                    'test' => 1
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CHECKBOX,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    /**
     *  Test change form input from select to checkbox when sending values field
     *
     *  @return void
     */
    public function testFormUpdateWithCheckboxTypeAndValuesShouldUpdate(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'type' => InputType::CHECKBOX,
                'values' => [
                    'test' => 2
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::CHECKBOX,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 2
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    public function testFormUpdateWithLabelShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'label' => '111',
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => '111',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    public function testFormUpdateWithPlaceholderShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'placeholder' => 'placeholder',
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'placeholder',
                    'values' => [],
                    'order' => 1
                ]
            ]
        );
    }

    public function testFormUpdateWithValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'values' => [
                    'test' => 2,
                ],
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 2
                    ],
                    'order' => 1
                ]
            ]
        );
    }

    public function testFormUpdateWithOrderShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();

        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => 2,
                'id' => 1
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 2
                ]
            ]
        );
    }

    public function testFormUpdateWithInvalidStageId(): void
    {
        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . 0), [
                'order' => 2,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'id' => ['Stage could not be found']
            ]
        ]);
    }

    public function testFormUpdateWithInvalidInputId(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => 2,
                'id' => 0
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'id' => ['Input id could not be found']
            ]
        ]);
    }

    public function testFormUpdateWithInvalidOrder(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $form->form_element = [
            [
                'id' => 1,
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 1
            ],
            [
                'id' => 2,
                'type' => InputType::TEXT,
                'label' => 'test',
                'placeholder' => 'test',
                'values' => [],
                'order' => 2
            ]
        ];
        $form->save();
        $response = $this->actingAs($this->user)
            ->patch((self::FORM_INPUT_UPDATE_API . $form->id), [
                'order' => 2,
                'id' => 1
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'order' => ['Order selected has been used']
            ]
        ]);
    }

    public function testFormAddWithoutValuesShouldReturnTrueAndSave(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 2,
                ]]
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ],
                [
                    'id' => 2,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 2
                ]
            ]
        );
    }

    public function testFormAddWithValuesShouldReturnTrueAndNotSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1,
                    ]
                ]]
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ],
                [
                    'id' => 2,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 2
                ]
            ]
        );
    }

    public function testFormAddWithValuesShouldReturnTrueAndSaveValues(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson(['status' => true]);
        $updatedForm = ActivityForm::find($form->id);

        $this->assertEquals(
            $updatedForm->form_element,
            [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ],
                [
                    'id' => 2,
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ],
                    'order' => 2
                ]
            ]
        );
    }

    public function testFormAddWithInvalidStageId(): void
    {
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . 0), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 1,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'id' => ['Stage could not be found']
            ]
        ]);
    }

    public function testFormAddWithInvalidOrder(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 1,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.order' => ['Order selected has been used']
            ]
        ]);
    }

    public function testFormAddWithMissingTypeShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.type' => ['The type field is required.']
            ]
        ]);
    }

    public function testFormAddWithMissingLabelShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'placeholder' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.label' => ['The label field is required.']
            ]
        ]);
    }

    public function testFormAddWithMissingPlaceholderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => true,
        ]);
    }

    public function testFormAddWithMissingOrderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.order' => ['The order field is required.']
            ]
        ]);
    }

    public function testFormAddWithMissingValuesShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 2,
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.values' => ['Values must be filled if type has multiple value option.']
            ]
        ]);
    }

    public function testFormAddWithTooShortLabelShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => '',
                    'placeholder' => 'test',
                    'order' => 2,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.label' => ['The label field is required.']
            ]
        ]);
    }

    public function testFormAddWithZeroOrderShouldReturnFalse(): void
    {
        $form = $this->getDummyFormWithoutValues();
        $response = $this->actingAs($this->user)
            ->post((self::FORM_INPUT_ADD_API . $form->id), [
                'fields' => [[
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'order' => 0,
                    'values' => [
                        'test' => 1
                    ]
                ]]
            ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'fields.0.order' => ['The order must be at least 1.']
            ]
        ]);
    }

    /**
     *  Get dummy form that has text type and values
     *
     *  @return ActivityForm
     */
    private function getDummyFormWithoutValues(): ActivityForm
    {
        return factory(ActivityForm::class)->create([
            'form_element' => [
                [
                    'id' => 1,
                    'type' => InputType::TEXT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [],
                    'order' => 1
                ]
            ]
        ]);
    }

    /**
     *  Get dummy form that has select type and values
     *
     *  @return ActivityForm
     */
    private function getDummyFormWithValues(): ActivityForm
    {
        return factory(ActivityForm::class)->create([
            'form_element' => [
                [
                    'id' => 1,
                    'type' => InputType::SELECT,
                    'label' => 'test',
                    'placeholder' => 'test',
                    'values' => [
                        'test' => 1,
                    ],
                    'order' => 1
                ]
            ]
        ]);
    }
}
