<?php

namespace App\Http\Controllers\Admin\Component\Room;

use App\Entities\Activity\ActivityLog;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\ThanosHidden;
use App\Repositories\Room\RoomUnitRepositoryEloquent;
use App\Services\Thanos\ThanosService;
use App\Test\MamiKosTestCase;
use Mockery;

class RoomSetterTraitTest extends MamiKosTestCase
{
    
    private $activityLog;

    public function setUp() : void
    {
        parent::setUp();
        $this->activityLog = $this->mockAlternatively(ActivityLog::class);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testMarkAsFullSuccess()
    {
        $roomEntity = factory(Room::class)->create([
            'room_count' => 10,
            'room_available' => 8
        ]);
        $repository = app()->make(RoomUnitRepositoryEloquent::class);
        $repository->backFillRoomUnit($roomEntity);

        $mockTrait = $this->getMockForTrait(RoomSetterTrait::class);

        $this->activityLog->shouldReceive('LogUpdate');
        $mockTrait->markAsFull($roomEntity->id);

        $roomEntity->refresh();
        $this->assertEquals(0, $roomEntity->room_available);
        $roomEntity->load('room_unit_empty');
        $this->assertEquals(0, $roomEntity->room_unit_empty->count());
    }

    public function testSnap()
    {
        $room = factory(Room::class)->state('inactive')->create();
        $thanosService = app()->make(ThanosService::class);
        $mockTrait = $this->getMockForTrait(RoomSetterTrait::class);
        $mockTrait->snap($room->id, $thanosService);

        $this->assertEquals('false', $room->is_active);
        $this->assertNotNull(ThanosHidden::where('designer_id', $room->id)->hidden());
    }

    public function testRevertSnap()
    {
        $room = factory(Room::class)->state('active')->create();
        $thanosService = app()->make(ThanosService::class);
        $mockTrait = $this->getMockForTrait(RoomSetterTrait::class);
        $mockTrait->revertSnap($room->id, $thanosService);

        $this->assertEquals('true', $room->is_active);
        $this->assertNotNull(ThanosHidden::where('designer_id', $room->id)->shown());
    }
}
