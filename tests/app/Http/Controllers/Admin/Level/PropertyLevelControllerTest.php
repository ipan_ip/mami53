<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Services\Level\PropertyLevelService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PropertyLevelControllerTest extends MamiKosTestCase
{
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $role = factory(Role::class)->create();
        $this->user = factory(User::class)->create();

        $role->attachPermissions([
            factory(Permission::class)->create(['name' => 'admin-access']),
            factory(Permission::class)->create(['name' => 'access-kost-property'])
        ]);
        $this->user->attachRole($role);
    }

    public function testCreateShouldHaveBoxTitle(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->get(route('admin.property.level.create'));

        $response->assertViewHas('boxTitle', 'Property Level Management');
    }

    public function testCreateShouldHavePageTitle(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->get(route('admin.property.level.create'));

        $response->assertViewHas('pageTitle', 'Property Level Management');
    }

    public function testCreateShouldHaveKostLevels(): void
    {
        factory(KostLevel::class, 5)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->get(route('admin.property.level.create'));

        $response->assertViewHas('kostLevels', KostLevel::select('id', 'name')->get());
    }

    public function testCreateShouldNotHaveAssignedKostLevels(): void
    {
        factory(KostLevel::class)->create(['property_level_id' => 5]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->get(route('admin.property.level.create'));

        $response->assertViewHas('kostLevels', KostLevel::select('id', 'name')->doesntHave('property_level')->get());
    }

    public function testStoreWithMissingName(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'name' => 'Nama Property harus diisi'
        ]);
    }

    public function testStoreWithNullName(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => null,
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'name' => 'Nama Property harus diisi'
        ]);
    }

    public function testStoreWithWhitespaceName(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => ' ',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'name' => 'Nama Property harus diisi'
        ]);
    }

    public function testStoreWithMissingMaxRooms(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'max_rooms' => 'Jumlah kamar maksimal harus diisi',
        ]);
    }

    public function testStoreWithNullMaxRooms(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => null,
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'max_rooms' => 'Jumlah kamar maksimal harus diisi',
        ]);
    }

    public function testStoreWithWhitespaceMaxRooms(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => ' ',
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'max_rooms' => 'Jumlah kamar maksimal harus diisi',
        ]);
    }

    public function testStoreWithNonNumericMaxRooms(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 'test',
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'max_rooms' => 'Jumlah kamar harus berupa angka',
        ]);
    }

    public function testStoreWithZeroMaxRooms(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 0,
                    'minimum_charging' => 50000,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'max_rooms' => 'Jumlah kamar harus lebih dari 0',
        ]);
    }

    public function testStoreWithMissingMinimumCharging(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'minimum_charging' => 'Minimum charging harus diisi',
        ]);
    }

    public function testStoreWithWhitespaceMinimumCharging(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => ' ',
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'minimum_charging' => 'Minimum charging harus diisi',
        ]);
    }

    public function testStoreWithNullMinimumCharging(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => null,
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'minimum_charging' => 'Minimum charging harus diisi',
        ]);
    }

    public function testStoreWithNonNumericMinimumCharging(): void
    {
        $level = factory(KostLevel::class)->create();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 'test',
                    'kost_level_id' => $level->id
                ]
            );

        $response->assertSessionHasErrors([
            'minimum_charging' => 'Minimum charging harus berupa angka',
        ]);
    }

    public function testStoreWithMissingKostLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                ]
            );

        $response->assertSessionHasErrors([
            'kost_level_id' => 'Kost Level harus diisi',
        ]);
    }

    public function testStoreWithWhitespaceKostLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => ' '
                ]
            );

        $response->assertSessionHasErrors([
            'kost_level_id' => 'Kost Level harus diisi',
        ]);
    }

    public function testStoreWithNullKostLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => null
                ]
            );

        $response->assertSessionHasErrors([
            'kost_level_id' => 'Kost Level harus diisi',
        ]);
    }

    public function testStoreWithNonIntegerKostLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => 'test'
                ]
            );

        $response->assertSessionHasErrors([
            'kost_level_id' => 'Kost Level tidak valid',
        ]);
    }

    public function testStoreWithInvalidKostLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(
                route('admin.property.level.store'),
                [
                    'name' => 'test',
                    'max_rooms' => 5,
                    'minimum_charging' => 50000,
                    'kost_level_id' => 1
                ]
            );

        $response->assertSessionHasErrors([
            'kost_level_id' => 'Kost Level tidak valid',
        ]);
    }

    /**
     * @TODO : Angga Bayu S --> do it on this blackout
     */
    // public function testStoreShouldReturnSuccess(): void
    // {
    //     $level = factory(KostLevel::class)->create();
    //     $response = $this->setWebApiCookie()
    //         ->actingAs($this->user)
    //         ->post(
    //             route('admin.property.level.store'),
    //             [
    //                 'name' => 'test',
    //                 'max_rooms' => 5,
    //                 'minimum_charging' => 50000,
    //                 'kost_level_id' => $level->id
    //             ]
    //         );

    //     $response->assertRedirect(route('admin.property.level.index'));
    // }

    /**
     * @group UG
     * @group UG-4704
     * @group App/Http/Controllers/Admin/Level/PropertyLevelController
     */
    public function testStoreShouldCallService(): void
    {
        $level = factory(KostLevel::class)->create();
        $property = factory(Property::class)->create();
        $params = [
            'name' => 'test',
            'max_rooms' => 5,
            'minimum_charging' => 50000,
            'kost_level_id' => $level->id
        ];

        $service = $this->mockAlternatively(PropertyLevelService::class);
        $service->shouldReceive('createPropertyLevel')
            ->with($params)
            ->andReturn($property);

        $this->setWebApiCookie()
            ->actingAs($this->user)
            ->post(route('admin.property.level.store'), $params);

    }

    public function testIndexShouldContainKostLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create([
            'property_level_id' => $propertyLevel->id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user)
            ->get(route('admin.property.level.index'))
            ->assertStatus(200);
        
        $result = $response->viewData('propertyLevels');
        $this->assertEquals($kostLevel->id, $result->items()[0]->kost_level->id);
        $this->assertEquals(1, $result->total());
    }
}
