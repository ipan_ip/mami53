<?php

namespace App\Http\Controllers\Admin\Level;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;

class RoomListControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testIndexRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testIndexRoomList_WithLimitPagination_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id, 'limit' => 3 ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(3, $result->perPage());
    }

    public function testIndexRoomList_WithFilterRoomName_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        factory(RoomUnit::class, 1)->create([
            'designer_id' => $room->id,
            'name' => 'Executive'
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id, 'room-name' => 'Executive' ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testIndexRoomList_WithFilterLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id, 'level-id' => $regularLevel->id ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(3, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testIndexRoomList_WithFilterAllLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id, 'level-id' => 0 ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(6, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testIndexRoomList_WithFilterUnassignedLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id, 'level-id' => -1 ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(3, $result->total());
        $this->assertEquals(20, $result->perPage());
    }
    
    public function testIndexRoomList_WhenBackFillRoomUnit_ShouldSuccess()
    {
        $room = factory(Room::class)->create([
            'room_count' => 5,
            'room_available' => 0
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.index', [ 'id' => $room->song_id ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.index');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnits');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomUnits');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testEditRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.edit', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.edit');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnit');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateRoomList_WithInvalidSongId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => $regularLevel->id,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.room-list.update', [ 'id' => 999, 'roomId' => $roomUnit->id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateRoomList_WithInvalidRoomId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => $regularLevel->id,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.room-list.update', [ 'id' => $room->song_id, 'roomId' => 999 ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateRoomList_WithInvalidLevelId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => 999,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.room-list.update', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => $regularLevel->id,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.room-list.update', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Room was successfully updated.');
        $data['is_charge_by_room'] = false;
        $this->assertDatabaseHas((new RoomUnit)->getTable(), $data);
        $this->assertDatabaseHas((new RoomLevelHistory)->getTable(), [
            'room_level_id' => $regularLevel->id,
            'room_id' => $roomUnit->id,
            'action' => HistoryActionEnum::ACTION_UPGRADE
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        $data = [
            'room_level_id' => $standardLevel->id,
            'is_charge_by_room' => false,
        ];

        $response = $this->put(route('admin.room-list.update', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Room was successfully updated.');
        $data['is_charge_by_room'] = false;
        $this->assertDatabaseHas((new RoomUnit)->getTable(), $data);
        $this->assertDatabaseHas((new RoomLevelHistory)->getTable(), [
            'room_level_id' => $standardLevel->id,
            'room_id' => $roomUnit->id,
            'action' => HistoryActionEnum::ACTION_UPGRADE
        ]);
    }

    public function testAssignAllRoomList_WithInvalidSongId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => $regularLevel->id,
            'is_charge_by_room' => true,
        ];

        $response = $this->post(route('admin.room-list.assignall', [ 'id' => 999 ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testAssignAllRoomList_WithInvalidLevelId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => 999,
            'is_charge_by_room' => true,
        ];

        $response = $this->post(route('admin.room-list.assignall', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testAssignAllRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $data = [
            'room_level_id' => $regularLevel->id,
            'is_charge_by_room' => true,
        ];

        $response = $this->post(route('admin.room-list.assignall', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'All Rooms were successfully updated.');
        $data['is_charge_by_room'] = false;
        $this->assertDatabaseHas((new RoomUnit)->getTable(), $data);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        $data = [
            'room_level_id' => $standardLevel->id,
            'is_charge_by_room' => false,
        ];

        $response = $this->post(route('admin.room-list.assignall', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'All Rooms were successfully updated.');
        $data['is_charge_by_room'] = false;
        $this->assertDatabaseHas((new RoomUnit)->getTable(), $data);
    }

    public function testHistoryRoomList_WithInvalidSongId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        factory(RoomLevelHistory::class, 5)->create([
            'room_id' => $roomUnit->id,
            'room_level_id' => $standardLevel->id,
            'user_id' => $this->user->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.history', [ 'id' => 999, 'roomId' => $roomUnit->id ]));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testHistoryRoomList_WithInvalidRoomId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        factory(RoomLevelHistory::class, 5)->create([
            'room_id' => $roomUnit->id,
            'room_level_id' => $standardLevel->id,
            'user_id' => $this->user->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.history', [ 'id' => $room->song_id, 'roomId' => 999 ]));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testHistoryRoomList_WithoutHistory_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.history', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testHistoryRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        factory(RoomLevelHistory::class, 5)->create([
            'room_id' => $roomUnit->id,
            'room_level_id' => $standardLevel->id,
            'user_id' => $this->user->id
        ]);

        $response = $this->actingAs($this->user)->get(route('admin.room-list.history', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.room-list.history');
        $response->assertViewHas('room');
        $response->assertViewHas('roomUnit');
        $response->assertViewHas('levelHistories');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('levelHistories');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(15, $result->perPage());
    }

    private function createRoomUnitAtLevel(Room $room, RoomLevel $roomLevel, int $count = 1)
    {
        for ($i=0; $i < $count; $i++) { 
            factory(RoomUnit::class)->create([
                'designer_id' => $room->id,
                'room_level_id' => $roomLevel->id,
            ]);
        }
    }
}