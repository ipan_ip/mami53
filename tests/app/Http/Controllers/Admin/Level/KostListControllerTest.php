<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Consultant\ConsultantRoom;
use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\Entities\GoldPlus\Package;
use App\Entities\GoldPlus\Submission;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Level\LevelHistory;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\GoldplusConsultantHelper;
use Illuminate\Database\Eloquent\Collection;
use App\Test\Traits\GoldplusConsultantConfig;

class KostListControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use GoldplusConsultantConfig;

    private $user;
    private $regularLevel;

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->regularLevel = factory(KostLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'is_hidden' => false,
            'order' => 1,
        ]);
    }

    public function testEditRoomList_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        $response = $this->actingAs($this->user)->get(route('admin.kost-list.edit', [ 'id' => $room->song_id ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.kost-level.kost-list.edit');
        $response->assertViewHas('kost');
        $response->assertViewHas('isChargeByRoom');
        $response->assertViewHas('kostLevel');
        $response->assertViewHas('kostLevelList');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateRoomList_OnForce_RegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $data = [
            'level' => $this->regularLevel->id,
            'is_force' => true,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.kost-list.update', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('record_updated', true);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit()->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(true, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level()->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories()->latest()->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
    }

    public function testUpdateRoomList_OnForce_NonRegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $data = [
            'level' => $standardLevel->id,
            'is_force' => true,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.kost-list.update', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('record_updated', true);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit()->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(true, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level()->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($standardLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories()->latest()->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
    }

    public function testUpdateRoomList_NotOnForce_RegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $data = [
            'level' => $this->regularLevel->id,
            'is_force' => false,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.kost-list.update', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('record_updated', true);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit()->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level()->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories()->latest()->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
        $this->assertNull($levelHistoryResult->flag);
    }

    public function testUpdateRoomList_NotOnForce_NonRegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $data = [
            'level' => $standardLevel->id,
            'is_force' => false,
            'is_charge_by_room' => true,
        ];

        $response = $this->put(route('admin.kost-list.update', [ 'id' => $room->song_id ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('record_updated', true);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit()->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level()->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories()->latest()->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
        $this->assertNull($levelHistoryResult->flag);

        $levelHistoryResult = LevelHistory::where([
                'kost_id' => $room->song_id, 
                'level_id' => $standardLevel->id
            ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(FlagEnum::FLAG_UPGRADE, $levelHistoryResult->flag);
        $this->assertNull($levelHistoryResult->action);
    }

    public function updateKostList(int $songId, array $data)
    {
        return $this->put(route('admin.kost-list.update', [ 'id' => $songId ]), $data);
    }

    public function prepareRoom(array $config = [], array $options = []): Room
    {
        $states = $options['states'] ?? [];
        /** @var Room $room */
        $room = factory(Room::class)->states($states)->create();
        /** @var RoomOwner */
        if (!isset($config['noOwner'])) {
            $roomOwnerAttribute = [
                'designer_id' => $room->id,
            ];
            if (isset($options['ownerId'])) {
                $roomOwnerAttribute['user_id'] = $options['ownerId'];
            }
            factory(RoomOwner::class)->create($roomOwnerAttribute);
        }
        if (isset($options['levelId'])) {
            factory(KostLevelMap::class)->create([
                'kost_id' => $room->song_id,
                'level_id' => $options['levelId'],
    
            ]);
        }
        return $room;
    }

    public function prepareRegularLevel(): KostLevel
    {
        return factory(KostLevel::class)->state('regular')->create([
            'id' => mt_rand(1111111, 9999999),
        ]);
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testUpdateRoomListFromGpWithGpSubmissionShouldSuccessAndUpdateGpSubmission()
    {
        $oldLevel = factory(KostLevel::class)->state('rand-gp1')->create();
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
        ]);
        /** @var Submission */
        $gpSubmission = factory(Submission::class)->create([
            'user_id' => $room->owners->first()->user_id,
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, [
                $room->id,
            ]),
            'package_id' => factory(Package::class)->state('gp1')->create(),
        ]);
        $newLevel = $this->prepareRegularLevel();

        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevel->id,
            'is_force' => true,
        ]);

        $room->refresh();
        $gpSubmission->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevel->id);
        $this->assertEquals(SubmissionStatus::DONE, $gpSubmission->status);
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testUpdateRoomListFromNewGpWithGpSubmissionNoOwnerShouldNotUpdateGpSubmission()
    {
        $oldLevel = factory(KostLevel::class)->state('rand-gp1')->create();
        /** @var Room $room */
        $room = $this->prepareRoom([
            'noOwner',
        ], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
        ]);
        /** @var Submission */
        $gpSubmission = factory(Submission::class)->create([
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, [
                $room->id,
            ]),
            'package_id' => factory(Package::class)->state('gp1')->create(),
        ]);
        /** @var KostLevel */
        $newLevel = $this->prepareRegularLevel();

        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevel->id,
            'is_force' => true,
        ]);
        $room->refresh();
        $gpSubmission->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevel->id);
        $this->assertEquals(SubmissionStatus::NEW, $gpSubmission->status);
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testUpdateRoomListFromNewGpWithGpSubmissionSomeListingDeactivated()
    {
        $oldLevel = factory(KostLevel::class)->state('rand-gp1')->create();
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
        ]);
        $inactiveRoom = $this->prepareRoom([], [
            'states' => ['inactive'],
            'levelId' => $oldLevel->id,
            'ownerId' => $room->owners->first()->user_id,
        ]);
        /** @var Submission */
        $gpSubmission = factory(Submission::class)->create([
            'user_id' => $room->owners->first()->user_id,
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, [
                $room->id,
                $inactiveRoom->id,
            ]),
        ]);
        $newLevel = $this->prepareRegularLevel();

        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevel->id,
            'is_force' => true,
        ]);

        $room->refresh();
        $gpSubmission->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevel->id);
        $this->assertEquals(SubmissionStatus::DONE, $gpSubmission->status);
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testUpdateRoomListFromNewGpWithGpSubmissionMultipleListingFirstListingProcessed()
    {
        $oldLevel = factory(KostLevel::class)->state('rand-gp1')->create();
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
        ]);
        $room2 = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
            'ownerId' => $room->owners->first()->user_id,
        ]);
        /** @var Submission */
        $gpSubmission = factory(Submission::class)->create([
            'user_id' => $room->owners->first()->user_id,
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, [
                $room->id,
                $room2->id,
            ]),
        ]);
        $newLevel = $this->prepareRegularLevel();

        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevel->id,
            'is_force' => true,
        ]);
        $room->refresh();
        $gpSubmission->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevel->id);
        $this->assertEquals(SubmissionStatus::PARTIAL, $gpSubmission->status);
    }

    /**
     * @group UG
     * @group UG-4087
     */
    public function testUpdateRoomListFromNewGpWithGpSubmissionAlreadyProcessedShouldSuccessAndNoUpdateSubmission()
    {
        $oldLevel = factory(KostLevel::class)->state('rand-gp1')->create();
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => $oldLevel->id,
        ]);
        /** @var Submission */
        $gpSubmission = factory(Submission::class)->state('allProcessed')->create([
            'user_id' => $room->owners->first()->user_id,
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, [
                $room->id,
            ]),
        ]);
        $newLevel = $this->prepareRegularLevel();

        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevel->id,
            'is_force' => true,
        ]);
        $room->refresh();
        $gpSubmission->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevel->id);
        $this->assertEquals(SubmissionStatus::DONE, $gpSubmission->status);
    }

    /**
     * @group UG
     * @group UG-4217
     */
    public function testUpdateRoomLevelToGp3Or4ShouldCreateConsultantRoomMapping()
    {
        $expectedConsultantId = mt_rand();

        $this->prepareAllGpLevels();
        $this->mockConfigGpConsultantIds([$expectedConsultantId]);
        $room = $this->prepareRoom([], [
            'states' => ['active'],
        ]);

        $newLevelId = collect(KostLevel::getGoldplusLevelIdsByGroups(3, 4))->random();
        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevelId,
            'is_force' => true,
        ]);
        $room->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevelId);

        /** @var ConsultantRoom */
        $consultantRoomMap = ConsultantRoom::where('designer_id', $room->id)->first();
        $this->assertNotNull($consultantRoomMap);
        $this->assertEquals($consultantRoomMap->consultant_id, $expectedConsultantId);
    }

    /**
     * @group UG
     * @group UG-4217
     */
    public function testUpdateRoomLevelToGp3Or4ShouldUpdateConsultantRoomMapping()
    {
        $expectedConsultantId = mt_rand(1, 9);

        $this->prepareAllGpLevels();
        $this->mockConfigGpConsultantIds([$expectedConsultantId]);
        $room = $this->prepareRoom([], [
            'states' => ['active'],
        ]);
        /** @var ConsultantRoom */
        $consultantRoomMap = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => mt_rand(11, 99),
        ]);

        $newLevelId = collect(KostLevel::getGoldplusLevelIdsByGroups(3, 4))->random();
        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevelId,
            'is_force' => true,
        ]);
        $room->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevelId);

        /** @var ConsultantRoom */
        $consultantRoomMap = ConsultantRoom::where('designer_id', $room->id)->first();
        $this->assertNotNull($consultantRoomMap);
        $this->assertEquals($consultantRoomMap->consultant_id, $expectedConsultantId);
    }

    /**
     * @group UG
     * @group UG-4217
     */
    public function testUpdateRoomLevelDowngradeFromGp3Or4ShouldRemoveConsultantRoomMapping()
    {
        $gpConsultantId = mt_rand(1, 9);

        $this->prepareAllGpLevels();
        $this->mockConfigGpConsultantIds([$gpConsultantId]);
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => collect(KostLevel::getGoldplusLevelIdsByGroups(3, 4))->random(),
        ]);
        /** @var ConsultantRoom */
        $consultantRoomMap = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => $gpConsultantId,
        ]);

        $newLevelId = $this->prepareRegularLevel()->id;
        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevelId,
            'is_force' => true,
        ]);
        $room->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevelId);

        /** @var ConsultantRoom */
        $consultantRoomMap = ConsultantRoom::where('designer_id', $room->id)->first();
        $this->assertNull($consultantRoomMap);
    }

    /**
     * @group UG
     * @group UG-4217
     */
    public function testUpdateRoomLevelDowngradeFromGp3Or4ToGp1or2ShouldRemoveConsultantRoomMapping()
    {
        $gpConsultantId = mt_rand(1, 9);

        $this->prepareAllGpLevels();
        $this->mockConfigGpConsultantIds([$gpConsultantId]);
        $room = $this->prepareRoom([], [
            'states' => ['active'],
            'levelId' => collect(KostLevel::getGoldplusLevelIdsByGroups(3, 4))->random(),
        ]);
        /** @var ConsultantRoom */
        $consultantRoomMap = factory(ConsultantRoom::class)->create([
            'designer_id' => $room->id,
            'consultant_id' => $gpConsultantId,
        ]);

        $newLevelId = collect(KostLevel::getGoldplusLevelIdsByGroups(1, 2))->random();
        $response = $this->updateKostList($room->song_id, [
            'level' => $newLevelId,
            'is_force' => true,
        ]);
        $room->refresh();

        $response->assertRedirect();
        $this->assertEquals($room->level_info['id'], $newLevelId);

        /** @var ConsultantRoom */
        $consultantRoomMap = ConsultantRoom::where('designer_id', $room->id)->first();
        $this->assertNull($consultantRoomMap);
    }
}
