<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithoutEvents;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use phpmock\MockBuilder;
use Mockery;

class KostLevelControllerTest extends MamiKosTestCase
{
    // use WithoutMiddleware;
    use WithoutEvents;

    private const GET_CREATE_KOST_LEVEL_DATA = 'admin/kost-level/level/create/';
    private const KOST_LEVEL_ROUTE = 'admin/kost-level/level/';

    private $kostLevel;

    protected function setUp()
    {
        parent::setUp();

        self::markTestSkipped('Temporarily skipped until after BG-2996 getting merged');

        $builder = new MockBuilder();
        $user = factory(User::class)->states('admin', 'couldAccessAdmin')->create();
        Role::where('name', 'admin')->first()->attachPermission(factory(Permission::class)->create(['name' => 'access-kost-level']));
        $this->actingAs($user);
        $this->kostLevel = factory(KostLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
            'is_partner' => false,
        ]);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testIndexKostLevel_ShouldSuccess()
    {
        factory(KostLevel::class, 4)->create();

        $response = $this->setWebApiCookie()->get(self::KOST_LEVEL_ROUTE);

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.kost-level.level.index');
        $response->assertViewHas('level');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('level');
        $this->assertEquals(5, $result->total());
    } 

    public function testCreateKostLevel_ShouldSuccess()
    {
        factory(RoomLevel::class)->create();
        $response = $this->setWebApiCookie()->get(self::GET_CREATE_KOST_LEVEL_DATA);

        $response->assertOk();
        $response->assertViewIs('admin.contents.kost-level.level.create');
        $response->assertViewHas('isRegularAvail');
        $response->assertViewHas('isPartnerAvail');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('roomLevels', RoomLevel::select('id', 'name')->get());
    }

    public function testStoreKostLevel_ShouldSuccess()
    {
        $roomlevel = factory(RoomLevel::class)->create();
        $data = $this->kostLevelForm();
        $additionData = [
            "benefits" => [],
            "criterias" => [],
            'room_level_id' => $roomlevel->id
        ];
        $postData = array_merge($data, $additionData);

        $response = $this->setWebApiCookie()->post(self::KOST_LEVEL_ROUTE, $postData);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHasNoErrors();
        $response->assertSessionMissing('error_message');
        $response->assertSessionHas('message', 'Level successfully added.');
        $this->assertDatabaseHas((new KostLevel)->getTable(), $data);
    }

    public function testStoreKostLevelShouldUpdateRoomLevelForeignKey(): void
    {
        $roomlevel = factory(RoomLevel::class)->create(['kost_level_id' => null]);
        $data = $this->kostLevelForm();
        $additionData = [
            "benefits" => [],
            "criterias" => [],
            'room_level_id' => $roomlevel->id
        ];
        $postData = array_merge($data, $additionData);

        $this->post(self::KOST_LEVEL_ROUTE, $postData);
        
        $this->assertDatabaseMissing('room_level', ['id' => $roomlevel->id, 'kost_level_id' => null]);
    }

    public function testStoreKostLevel_WithInvalidRequest_ShouldFail()
    {
        $response = $this->post(self::KOST_LEVEL_ROUTE, []);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreWithNonIntegerRoomLevelId(): void
    {
        $this->setWebApiCookie()->post(self::KOST_LEVEL_ROUTE, ['room_level_id' => 'test']);

        $errors = session('errors')->getMessages();
        $this->assertEquals(
            $errors,
            [
                'name' => ['The name field is required.'],
                'order' => ['The order field is required.'],
                'is_regular' => ['The is regular field is required.'],
                'is_hidden' => ['The is hidden field is required.'],
                'is_partner' => ['The is partner field is required.'],
                'has_value' => ['The has value field is required.'],
                'join_count' => ['The join count field must be present.'],
                'charging_fee' => ['The charging fee field must be present.'],
                'charging_type' => ['The charging type field is required.'],
                'charge_invoice_type' => ['The charge invoice type field is required.'],
                'room_level_id' => ['The room level id must be an integer.']
            ]
        );
    }

    public function testStoreWithInvalidRoomLevelId(): void
    {
        $response = $this->setWebApiCookie()->post(self::KOST_LEVEL_ROUTE, ['room_level_id' => 0]);

        $errors = session('errors')->getMessages();
        $this->assertEquals(
            $errors,
            [
                'name' => ['The name field is required.'],
                'order' => ['The order field is required.'],
                'is_regular' => ['The is regular field is required.'],
                'is_hidden' => ['The is hidden field is required.'],
                'is_partner' => ['The is partner field is required.'],
                'has_value' => ['The has value field is required.'],
                'join_count' => ['The join count field must be present.'],
                'charging_fee' => ['The charging fee field must be present.'],
                'charging_type' => ['The charging type field is required.'],
                'charge_invoice_type' => ['The charge invoice type field is required.'],
                'room_level_id' => ['The selected room level id is invalid.']
            ]
        );
    }

    public function testEditKostLevel_ShouldSuccess()
    {
        $id = KostLevel::first()->id;
        $response = $this->get(self::KOST_LEVEL_ROUTE . $id . "/edit");

        $response->assertOk();
        $response->assertViewIs('admin.contents.kost-level.level.edit');
        $response->assertViewHas('level');
        $response->assertViewHas('order');
        $response->assertViewHas('isRegularAvail');
        $response->assertViewHas('isPartnerAvail');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateKostLevel_ShouldSuccess()
    {
        $roomlevel = factory(RoomLevel::class)->create(['kost_level_id' => null]);
        $data = $this->kostLevelForm();
        $additionData = [
            "benefits" => [],
            "benefitIds" => [],
            "criterias" => [],
            "criteriaIds" => [],
            'room_level_id' => $roomlevel->id
        ];
        $postData = array_merge($data, $additionData);
        $id = KostLevel::first()->id;
        $response = $this->put(self::KOST_LEVEL_ROUTE . $id, $postData);

        $response->assertStatus(302); // redirect
        $response->assertSessionHasNoErrors();
        $response->assertSessionMissing('error_message');
        $response->assertSessionHas('message', 'Level successfully updated.');
        $this->assertDatabaseHas((new KostLevel)->getTable(), $data);
    }

    public function testUpdateKostLevelShouldUpdateRoomLevelForeignKey(): void
    {
        $roomlevel = factory(RoomLevel::class)->create(['kost_level_id' => null]);
        $data = $this->kostLevelForm();
        $additionData = [
            "benefits" => [],
            "criterias" => [],
            'room_level_id' => $roomlevel->id
        ];
        $postData = array_merge($data, $additionData);

        $res = $this->setWebApiCookie()->put(self::KOST_LEVEL_ROUTE . $this->kostLevel->id, $postData);

        $this->assertDatabaseHas('room_level', ['id' => $roomlevel->id, 'kost_level_id' => $this->kostLevel->id]);
    }

    public function testUpdateKostLevelShouldUpdateOldRoomLevelForeignKey(): void
    {
        $oldRoomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $this->kostLevel->id]);
        $newRoomLevel = factory(RoomLevel::class)->create(['kost_level_id' => null]);

        $data = $this->kostLevelForm();
        $additionData = [
            "benefits" => [],
            "criterias" => [],
            'room_level_id' => $newRoomLevel->id
        ];
        $postData = array_merge($data, $additionData);

        $res = $this->setWebApiCookie()->put(self::KOST_LEVEL_ROUTE . $this->kostLevel->id, $postData);

        $this->assertDatabaseHas('room_level', ['id' => $oldRoomLevel->id, 'kost_level_id' => null]);
        $this->assertDatabaseHas('room_level', ['id' => $newRoomLevel->id, 'kost_level_id' => $this->kostLevel->id]);
    }

    public function testUpdateKostLevel_WithInvalidRequest_ShouldFail()
    {
        $id = KostLevel::first()->id;
        $response = $this->put(self::KOST_LEVEL_ROUTE . $id, []);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateWithInvalidRoomLevelId(): void
    {
        $id = KostLevel::first()->id;
        $response = $this->put(self::KOST_LEVEL_ROUTE . $id, ['room_level_id' => 0]);

        $errors = session('errors')->getMessages();
        $this->assertEquals(
            $errors,
            [
                'name' => ['The name field is required.'],
                'order' => ['The order field is required.'],
                'is_regular' => ['The is regular field is required.'],
                'is_hidden' => ['The is hidden field is required.'],
                'is_partner' => ['The is partner field is required.'],
                'has_value' => ['The has value field is required.'],
                'join_count' => ['The join count field must be present.'],
                'charging_fee' => ['The charging fee field must be present.'],
                'charging_type' => ['The charging type field is required.'],
                'charge_invoice_type' => ['The charge invoice type field is required.'],
                'room_level_id' => ['The selected room level id is invalid.']
            ]
        );
    }

    public function testDestroyKostLevel_ShouldSuccess()
    {
        $id = KostLevel::first()->id;
        $response = $this->delete(self::KOST_LEVEL_ROUTE . $id);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Level successfully deleted.');
    }

    private function kostLevelForm()
    {
        return [
            "name" => "Reguler",
            "order" => 1,
            "is_regular" => true,
            "is_hidden" => false,
            "is_partner" => true,
            "has_value" => true,
            "join_count" => 1,
            "charging_fee" => 10,
            "notes" => "Qui explicabo autem placeat.",
            "charging_type" => 'amount',
            "charge_for_owner" => false,
            "charge_for_consultant" => true,
            "charge_for_booking" => true,
            "charge_invoice_type" => 'all',
        ];
    }
}
