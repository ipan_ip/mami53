<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\User;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Entrust\Role;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Services\Room\RoomService;
use App\Entities\Property\Property;
use App\Entities\Entrust\Permission;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\PropertyLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Property\PropertyContract;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Level\RoomLevelRepository;
use App\Entities\Property\PropertyContractDetail;
use App\Services\Property\PropertyContractService;
use App\Repositories\Level\KostLevelRepositoryEloquent;

class PropertyPackageControllerTest extends MamiKosTestCase
{
    protected const SEARCH_OWNER = '/admin/property-package/search-owner/';
    protected const DROPDOWN_LIST_URL = '/admin/property-package/dropdown-list';

    protected $consultant;

    /**
     *  @var PropertyContractService
     */
    protected $service;

    /**
     *  @var KostLevelRepository
     */
    protected $kostLevelRepository;

    /**
     *  @var RoomService
     */
    protected $roomService;

    /**
     *  @var RoomLevelRepository
     */
    protected $roomLevelRepository;

    protected function setUp()
    {
        parent::setUp();
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->consultant = factory(User::class)->create();

        $this->consultant->attachRole($role);
        $role->attachPermission(factory(Permission::class)->create(['name' => 'admin-access']));
        $role->attachPermission(factory(Permission::class)->create(['name' => 'access-consultant']));
        $this->service = $this->mockAlternatively(PropertyContractService::class);
        $this->kostLevelRepository = $this->mockAlternatively(KostLevelRepository::class);
        $this->roomService = $this->mockAlternatively(RoomService::class);
    }

    public function testGetLevels(): void
    {
        $firstLevel = factory(PropertyLevel::class)->create(['name' => 'test']);
        $secondLevel = factory(PropertyLevel::class)->create(['name' => 'tist']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.dropdown-list'));

        $response->assertJson([
            'data' => [
                [
                    'id' => $firstLevel->id,
                    'name' => $firstLevel->name
                ],
                [
                    'id' => $secondLevel->id,
                    'name' => $secondLevel->name
                ]
            ]
        ]);
    }

    public function testSearchOwnerByPhoneNumber()
    {
        $this->withoutExceptionHandling();
        factory(User::class, 2)->create();
        $owner = factory(User::class)->create([
            'is_owner' => 'true',
            'phone_number' => '081229507444'
        ]);

        factory(Property::class, 2)->create();
        $property = factory(Property::class, 2)->create(['owner_user_id' => $owner->id]);

        $expected = [
            'owner_id' => $owner->id,
            'owner_name' => $owner->name,
            'properties' => [
                [
                    'id' => $property[0]->id,
                    'name' => $property[0]->name
                ],
                [
                    'id' => $property[1]->id,
                    'name' => $property[1]->name
                ]
            ]
        ];

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', self::SEARCH_OWNER . '081229507444')
            ->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => $expected
        ]);
    }

    public function testSearchOwnerByPhoneNumberWithoutProperty()
    {
        $this->withoutExceptionHandling();
        factory(User::class, 2)->create();
        $owner = factory(User::class)->create([
            'is_owner' => 'true',
            'phone_number' => '081229507444'
        ]);


        $expected = [
            'owner_id' => $owner->id,
            'owner_name' => $owner->name,
            'properties' => []
        ];

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', self::SEARCH_OWNER . '081229507444')
            ->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => $expected
        ]);
    }

    public function testSearchOwnerByInvalidPhoneNumber()
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', self::SEARCH_OWNER . '081229507440')
            ->assertStatus(404);
        $response->assertJson([
            'status' => false,
            'message' => 'Owner tidak dapat ditemukan'
        ]);
    }

    public function testSearchOwnerByPhoneNumberShouldNotShowPropertyWithActiveContract(): void
    {
        $this->withoutExceptionHandling();
        factory(User::class, 2)->create();
        $owner = factory(User::class)->create([
            'is_owner' => 'true',
            'phone_number' => '081229507444'
        ]);

        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $propertyContract = factory(PropertyContract::class)->create(['status' => 'active']);
        $propertyContract->properties()->attach($property);

        $expected = [
            'owner_id' => $owner->id,
            'owner_name' => $owner->name,
            'properties' => []
        ];

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', self::SEARCH_OWNER . '081229507444')
            ->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => $expected
        ]);
    }

    public function testGetRoomLists(): void
    {
        $kost = factory(Room::class)->create();
        $level = factory(RoomLevel::class)->create();
        $unit = factory(RoomUnit::class)->create(['name' => 'testa', 'designer_id' => $kost->id, 'room_level_id' => $level->id]);
        $otherUnit = factory(RoomUnit::class)->create(['name' => 'testb', 'designer_id' => $kost->id]);

        $otherKost = factory(Room::class)->create();
        factory(RoomUnit::class)->create(['designer_id' => $otherKost->id]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.room-list', ['designerId' => $kost->id]));

        $response->assertJson([
            'data' => [
                [
                    'id' => $unit->id,
                    'name' => $unit->name,
                    'floor' => $unit->floor,
                    'occupied' => $unit->occupied,
                    'level' => $level->name
                ],
                [
                    'id' => $otherUnit->id,
                    'name' => $otherUnit->name,
                    'floor' => $otherUnit->floor,
                    'occupied' => $otherUnit->occupied,
                    'level' => 'Reguler'
                ]
            ]
        ]);
    }

    public function testGetKostListsByPropertyShouldReturnCorrectData(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();
        $level = factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1',
            'is_regular' => 0,
            'is_hidden' => 0
        ]);

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'property_id' => $property->id,
            'is_active' => 'true'
        ]);
        $otherKost = factory(Room::class)->create([
            'name' => 'test b',
            'property_id' => $property->id,
            'is_active' => 'true'
        ]);
        $kost->level()->attach($level);

        factory(Room::class)->create(['is_active' => 'true']);
        factory(Room::class)->create(['property_id' => $property->id]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-by-property', ['propertyId' => $property->id, 'propertyContractId' => $contract->id]))
            ->assertOk();

        $response->assertJson([
            'total' => 2,
            'data' => [
                [
                    'id' => $kost->id,
                    'kost_name' => $kost->name,
                    'owner_name' => $kost->owner_name,
                    'area_city' => $kost->area_city,
                    'total_rooms' => $kost->room_count,
                    'level' => $level->name,
                    'updated_at' => $kost->updated_at->toDateTimeString()
                ],
                [
                    'id' => $otherKost->id,
                    'kost_name' => $otherKost->name,
                    'owner_name' => $otherKost->owner_name,
                    'area_city' => $otherKost->area_city,
                    'total_rooms' => $otherKost->room_count,
                    'level' => 'Reguler',
                    'updated_at' => $otherKost->updated_at->toDateTimeString()
                ]
            ]
        ]);

        $response->assertJsonCount(2, 'data');
    }

    public function testGetRoomListsWithOffset(): void
    {
        $kost = factory(Room::class)->create();
        $level = factory(RoomLevel::class)->create();
        $unit = factory(RoomUnit::class)->create(['name' => 'testa', 'designer_id' => $kost->id, 'room_level_id' => $level->id]);
        $otherUnit = factory(RoomUnit::class)->create(['name' => 'testb', 'designer_id' => $kost->id]);

        $otherKost = factory(Room::class)->create();
        factory(RoomUnit::class)->create(['designer_id' => $otherKost->id]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.room-list', ['designerId' => $kost->id]) . '?offset=1');

        $response->assertJson([
            'data' => [
                [
                    'id' => $otherUnit->id,
                    'name' => $otherUnit->name,
                    'floor' => $otherUnit->floor,
                    'occupied' => $otherUnit->occupied,
                    'level' => 'Reguler'
                ]
            ]
        ]);
    }

    public function testGetKostListsByPropertyWithOffsetShouldReturnCorrectData(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();

        $kost = factory(Room::class)->create([
            'name' => 'test a',
            'property_id' => $property->id,
            'is_active' => 'true'
        ]);
        $otherKost = factory(Room::class)->create([
            'name' => 'test b',
            'property_id' => $property->id,
            'is_active' => 'true'
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-by-property', ['propertyId' => $property->id, 'propertyContractId' => $contract->id, 'offset' => 1]))
            ->assertOk();

        $response->assertJson([
            'total' => 2,
            'data' => [
                [
                    'id' => $otherKost->id,
                    'kost_name' => $otherKost->name,
                    'owner_name' => $otherKost->owner_name,
                    'area_city' => $otherKost->area_city,
                    'total_rooms' => $otherKost->room_count,
                    'level' => 'Reguler',
                    'updated_at' => $otherKost->updated_at->toDateTimeString()
                ]
            ]
        ]);

        $response->assertJsonCount(1, 'data');
    }

    public function testStoreWithInvalidInputShouldReturn501Status(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                []
            );

        $response->assertStatus(501);
    }

    public function testStoreWithoutAnyFields(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                []
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_id' => ['Nama properti harus diisi.'],
                'property_level_id' => ['Level properti harus diisi.'],
                'join_date' => ['Join date harus diisi.'],
                'custom_package' => ['Custom package harus diisi.'],
            ]
        ]);
    }

    public function testStoreWithInvalidPropertyLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['property_level_id' => 0]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_level_id' => ['Level properti yang dipilih tidak valid.'],
            ]
        ]);
    }

    public function testStoreWithNonIntegerPropertyLevelId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['property_level_id' => 'a']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_level_id' => ['Level properti harus angka.'],
            ]
        ]);
    }

    public function testStoreWithCorrectJoinDateFormat(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['join_date' => '2020-08-19 17:17:17']
            );

        $response->assertJsonMissing([
            'messages' => [
                'join_date' => ['Join date harus tanggal dengan format YYYY-MM-dd HH:mm:ss.'],
            ]
        ]);
    }

    public function testStoreWithMissingTimeJoinDate(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['join_date' => '2020-08-19']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'join_date' => ['Join date harus tanggal dengan format YYYY-MM-dd HH:mm:ss.'],
            ]
        ]);
    }

    public function testStoreWithNonBooleanCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => 'a']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);
    }

    public function testStoreWithZeroAsFalseCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => 0]
            );

        $response->assertJsonMissing([
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);
    }

    public function testStoreWithOneAsTrueCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => 1]
            );

        $response->assertJsonMissing([
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithStringFalseCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => 'false']
            );

        $response->assertJsonMissing([
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);
    }

    public function testStoreWithStringTrueCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => 'true']
            );

        $response->assertJsonMissing([
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithTrueCustomPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true]
            );

        $response->assertJsonMissing([
            'messages' => [
                'custom_package' => ['Custom package harus boolean.'],
            ]
        ]);

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithZeroTotalPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true, 'total_package' => 0]
            );

        $response->assertJsonMissing([
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithValidTotalPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true, 'total_package' => 1]
            );

        $response->assertJsonMissing([
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithEmptyStringTotalPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true, 'total_package' => '']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithNonIntegerTotalPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true, 'total_package' => 'a']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus angka.']
            ]
        ]);
    }

    public function testStoreWithNullTotalPackage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['custom_package' => true, 'total_package' => null]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['Total package harus angka.', 'Total package harus diisi jika custom package dipilih.']
            ]
        ]);
    }

    public function testStoreWithInvalidPropertyId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                ['property_id' => 0]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_id' => ['Nama properti yang dipilih tidak valid.']
            ]
        ]);
    }

    public function testStoreWithCustomPackage(): void
    {
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create();

        $params = [
            'property_level_id' => $propertyLevel->id,
            'joined_at' => '2020-09-25 17:17:17',
            'is_autocount' => false,
            'total_package' => 1,
        ];
        $contract = factory(PropertyContract::class)->create($params);

        $params['property_id'] = $property->id;
        $this->service
            ->shouldReceive('createPackage')
            ->with($params, $this->consultant)
            ->once()
            ->andReturn($contract);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                [
                    'property_id' => $property->id,
                    'property_level_id' => $propertyLevel->id,
                    'join_date' => '2020-09-25 17:17:17',
                    'custom_package' => true,
                    'total_package' => 1,
                ]
            );
    }

    public function testStoreWithNonCustomPackage(): void
    {
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create();

        $params = [
            'property_level_id' => $propertyLevel->id,
            'joined_at' => '2020-09-25 17:17:17',
            'is_autocount' => true,
            'total_package' => 0
        ];
        $contract = factory(PropertyContract::class)->create($params);

        $params['property_id'] = $property->id;
        $this->service
            ->shouldReceive('createPackage')
            ->with($params, $this->consultant)
            ->once()
            ->andReturn($contract);

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'POST',
                route('admin.property-package.store'),
                [
                    'property_id' => $property->id,
                    'property_level_id' => $propertyLevel->id,
                    'join_date' => '2020-09-25 17:17:17',
                    'custom_package' => false,
                    'total_package' => 1,
                ]
            );
    }

    public function testGetKostListsByPropertyWithLimitShouldReturnCorrectData(): void
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $contract = $property->property_contracts()->first();

        factory(Room::class, 5)->create([
            'property_id' => $property->id,
            'is_active' => 'true'
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-by-property', ['propertyId' => $property->id, 'propertyContractId' => $contract->id, 'limit' => 2]))
            ->assertOk();

        $response->assertJson([
            'total' => 5,
        ]);

        $response->assertJsonCount(2, 'data');
    }

    public function testGetKostLevelListsByPropertyWithNoKost()
    {
        $property = factory(Property::class)->state('withInactiveContract')->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-by-property', ['propertyId' => $property->id, 'propertyContractId' => $property->property_contracts()->first()->id]))
            ->assertOk();
        $response->assertJson([
            'total' => 0,
            'data' => []
        ]);
    }

    public function testGetKostLevelListsByPropertyWithInvalidPropertyId()
    {
        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-by-property', ['propertyId' => 0, 'propertyContractId' => 0]))
            ->assertNotFound()
            ->assertJson([
                'message' => 'No query results for model [App\\Entities\\Property\\Property].'
            ]);
    }

    public function testToggleAssignmentWithoutId(): void
    {
        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.toggle-assignment', []))
            ->assertStatus(501)
            ->assertJson([
                'messages' => [
                    'property_id' => ['The property id field is required.'],
                    'kost_id' => ['The kost id field is required.']
                ]
            ]);
    }

    public function testToggleAssignmentWithNullId(): void
    {
        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.toggle-assignment', ['property_id' => null, 'kost_id' => null]))
            ->assertStatus(501)
            ->assertJson([
                'messages' => [
                    'property_id' => ['The property id field is required.'],
                    'kost_id' => ['The kost id field is required.']
                ]
            ]);
    }

    public function testToggleAssignmentWithInvalidId(): void
    {
        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.toggle-assignment', ['property_id' => 0, 'kost_id' => 0]))
            ->assertStatus(501)
            ->assertJson([
                'messages' => [
                    'property_id' => ['The selected property id is invalid.'],
                    'kost_id' => ['The selected kost id is invalid.']
                ]
            ]);
    }

    public function testToggleAssignmentShouldCallRoomService(): void
    {
        $property = factory(Property::class)->create();
        $room = factory(Room::class)->create();

        $this->roomService->shouldReceive('togglePropertyAssignment')
            ->withArgs(function ($arg1, $arg2) use ($property, $room) {
                return $arg1->is($property) && $arg2->is($room);
            });

        $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.toggle-assignment', ['property_id' => $property->id, 'kost_id' => $room->id]));
    }

    private function createKostAndLevel(Property $property): Room
    {
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $level = factory(KostLevel::class)->create(['property_level_id' => $property->property_contracts->first()->property_level->id]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $kost->song_id,
            'level_id' => $level->id,
        ]);

        return $kost;
    }

    private function callAssignAllRoomsApi(Room $kost, PropertyContract $contract)
    {
        return $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.assign-all-rooms', ['designerId' => $kost->id, 'propertyContractId' => $contract->id]));
    }

    public function testAssignAllRoomsWithoutProperty(): void
    {
        $kost = factory(Room::class)->create();

        $response = $this->callAssignAllRoomsApi($kost, factory(PropertyContract::class)->create());
        $response->assertJson([
            'message' => 'Kost belum diassign ke properti.'
        ]);
    }

    public function testAssignAllRoomsWithoutKostLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);

        $response = $this->callAssignAllRoomsApi($kost, $property->property_contracts->first());
        $response->assertJson([
            'message' => 'Properti level belum diassign kost level.'
        ]);
    }

    public function testAssignAllRoomsWithoutRoomLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();
        $kost = $this->createKostAndLevel($property);

        $response = $this->callAssignAllRoomsApi($kost, $property->property_contracts->first());
        $response->assertJson([
            'message' => 'Kost level belum diassign room level.'
        ]);
    }

    public function testAssignAllRoomsWithOtherContract(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $contract = $property->property_active_contract;
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $contract->property_level->id]);
        factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);


        $response = $this->callAssignAllRoomsApi($kost, factory(PropertyContract::class)->create());
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    private function assertKostLevelChanged(PropertyContract $contract, Room $kost, KostLevel $kostLevel): void
    {
        $this->service->shouldReceive('calculateTotalRoomAndPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once();

        $this->kostLevelRepository->shouldReceive('changeLevel')
            ->withArgs(function ($arg1, $arg2, $arg3, $arg4) use ($kost, $kostLevel) {
                return ($arg1->is($kost) && $arg2->is($kostLevel) && $arg3->is($this->consultant) && $arg4);
            })
            ->once();
    }

    public function testAssignAllRoomsWithNonAssignedKostLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $contract = $property->property_active_contract;
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $contract->property_level->id]);
        factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $this->assertKostLevelChanged($contract, $kost, $kostLevel);

        $this->callAssignAllRoomsApi($kost, $contract);
    }

    public function testAssignAllRoomsShouldUpdateAllRoomUnits(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();
        $contract = $property->property_active_contract;
        $kost = $this->createKostAndLevel($property);
        $kostLevel = $kost->level()->first();

        $this->assertKostLevelChanged($contract, $kost, $kostLevel);

        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);

        $this->callAssignAllRoomsApi($kost, $contract);

        $this->assertDatabaseHas('designer_room', ['id' => $roomUnits[0]->id, 'room_level_id' => $roomLevel->id]);
        $this->assertDatabaseHas('designer_room', ['id' => $roomUnits[1]->id, 'room_level_id' => $roomLevel->id]);
    }

    public function testAssignAllRoomsWithInactiveAutoGp1(): void
    {
        $property = factory(Property::class)->states('withInactiveContract')->create();
        $contract = $property->property_contracts->first();
        $level = $contract->property_level;
        $level->name = PropertyLevel::GOLDPLUS_1;
        $level->save();
        $kost = $this->createKostAndLevel($property);
        $kostLevel = $kost->level()->first();

        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);

        $response = $this->callAssignAllRoomsApi($kost, $contract);
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    public function testAssignAllRoomsWithInactiveAutoGp1Promo(): void
    {
        $property = factory(Property::class)->states('withInactiveContract')->create();
        $contract = $property->property_contracts->first();
        $level = $contract->property_level;
        $level->name = PropertyLevel::GOLDPLUS_1_PROMO;
        $level->save();
        $kost = $this->createKostAndLevel($property);
        $kostLevel = $kost->level()->first();

        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);

        $response = $this->callAssignAllRoomsApi($kost, $contract);
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    public function testAssignAllRoomsWithInactiveAutoGp2(): void
    {
        $property = factory(Property::class)->states('withInactiveContract')->create();
        $contract = $property->property_contracts->first();
        $level = $contract->property_level;
        $level->name = PropertyLevel::GOLDPLUS_2;
        $level->save();
        $kost = $this->createKostAndLevel($property);
        $kostLevel = $kost->level()->first();

        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);

        $response = $this->callAssignAllRoomsApi($kost, $contract);
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    public function testAssignAllRoomsWithInactiveGp2Promo(): void
    {
        $property = factory(Property::class)->states('withInactiveContract')->create();
        $contract = $property->property_contracts->first();
        $level = $contract->property_level;
        $level->name = PropertyLevel::GOLDPLUS_2_PROMO;
        $level->save();
        $kost = $this->createKostAndLevel($property);
        $kostLevel = $kost->level()->first();

        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);

        $response = $this->callAssignAllRoomsApi($kost, $contract);
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    private function callUnassignAllRoomsApi(Room $kost, PropertyContract $contract)
    {
        return $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.unassign-all-rooms', ['designerId' => $kost->id, 'propertyContractId' => $contract->id]));
    }

    public function testUnassignAllRoomsWithUnassignedProperty(): void
    {
        $kost = factory(Room::class)->create();

        $response = $this->callUnassignAllRoomsApi($kost, factory(PropertyContract::class)->create());

        $response->assertJson([
            'message' => 'Kost belum diassign ke properti.'
        ]);
    }

    public function testUnassignAllRoomsWithNonActiveContract(): void
    {
        $property = factory(Property::class)->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);

        $response = $this->callUnassignAllRoomsApi($kost, factory(PropertyContract::class)->create());

        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    private function assertRegularKostLevelQuery($toReturn)
    {
        $this->kostLevelRepository->shouldReceive('where')
            ->with('is_regular', true)
            ->once()
            ->andReturn($this->kostLevelRepository)
            ->shouldReceive('first')
            ->with()
            ->once()
            ->andReturn($toReturn);
    }

    public function testUnassignAllRoomsWithoutRegularKostLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();

        $kost = factory(Room::class)->create(['property_id' => $property->id]);

        $this->assertRegularKostLevelQuery(null);

        $response = $this->callUnassignAllRoomsApi($kost, $property->property_active_contract);

        $response->assertJson([
            'message' => 'Regular kost level belum dibuat.'
        ]);
    }

    public function testUnassignAllRoomsWithoutRegularRoomLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();

        $kost = factory(Room::class)->create(['property_id' => $property->id]);

        $this->assertRegularKostLevelQuery(
            factory(KostLevel::class)->create(['is_regular' => true])
        );

        $response = $this->callUnassignAllRoomsApi($kost, $property->property_active_contract);

        $response->assertJson([
            'message' => 'Regular room level belum dibuat.'
        ]);
    }

    public function testUnassignAllRoomsShouldUpdateKostAndRoomLevel(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();

        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $regularKostLevel = factory(KostLevel::class)->create(['is_regular' => true]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);
        $regularRoomLevel = factory(RoomLevel::class)->create(['is_regular' => true]);

        $this->assertRegularKostLevelQuery($regularKostLevel);

        $this->assertKostLevelChanged($property->property_active_contract, $kost, $regularKostLevel);

        $this->callUnassignAllRoomsApi($kost, $property->property_active_contract);

        $this->assertDatabaseHas('designer_room', ['id' => $roomUnits[0]->id, 'room_level_id' => $regularRoomLevel->id]);
        $this->assertDatabaseHas('designer_room', ['id' => $roomUnits[1]->id, 'room_level_id' => $regularRoomLevel->id]);
    }

    public function testUnassignAllRoomsWithDifferentContract(): void
    {
        $property = factory(Property::class)->states('withActiveContract')->create();

        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $regularKostLevel = factory(KostLevel::class)->create(['is_regular' => true]);

        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $kost->id, 'room_level_id' => 0]);
        $regularRoomLevel = factory(RoomLevel::class)->create(['is_regular' => true]);

        $response = $this->callUnassignAllRoomsApi($kost, factory(PropertyContract::class)->create());
        $response->assertJson([
            'message' => 'Properti tidak memiliki kontrak aktif.'
        ]);
    }

    private function callGetOwnersUnmappedPropertiesApi(int $ownerId)
    {
        return $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.owner-property-list', ['ownerId' => $ownerId]));
    }

    public function testGetOwnersUnmappedPropertiesShouldReturnPropertyWithoutContract(): void
    {
        $property = factory(Property::class)->create(['owner_user_id' => 1]);

        $response = $this->callGetOwnersUnmappedPropertiesApi(1);

        $response->assertJson([
            'data' => [
                [
                    'id' => $property->id,
                    'name' => $property->name
                ]
            ]
        ]);
    }

    public function testGetOwnersUnmappedPropertiesShouldReturnPropertyWithInactiveContract(): void
    {
        $contract = factory(PropertyContract::class)->create(['status' => 'inactive']);
        $property = factory(Property::class)->create(['owner_user_id' => 1]);
        factory(PropertyContractDetail::class)->create([
            'property_contract_id' => $contract->id,
            'property_id' => $property->id
        ]);

        $response = $this->callGetOwnersUnmappedPropertiesApi(1);

        $response->assertJson([
            'data' => [
                [
                    'id' => $property->id,
                    'name' => $property->name
                ]
            ]
        ]);
    }

    public function testGetOwnersUnmappedPropertiesShouldNotReturnPropertyWithActiveContract(): void
    {
        factory(Property::class)->states('withActiveContract')->create(['owner_user_id' => 1]);

        $response = $this->callGetOwnersUnmappedPropertiesApi(1);

        $response->assertJson([
            'data' => []
        ]);

        $response->assertJsonCount(0, 'data');
    }

    public function testGetOwnersUnmappedPropertiesShouldNotReturnOtherOwnerProperty(): void
    {
        factory(Property::class)->create(['owner_user_id' => 2]);

        $response = $this->callGetOwnersUnmappedPropertiesApi(1);

        $response->assertJson([
            'data' => []
        ]);

        $response->assertJsonCount(0, 'data');
    }

    public function testAddPropertyWithoutInputs(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.add-property', []));
    }

    public function testUpdateWithCorrectData(): void
    {
        $this->withoutExceptionHandling();
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $propertyLevel->id, 'status' => 'active']);
        $contract->properties()->attach($property);

        $params = [
            'join_date' => now()->toDateTimeString(),
            'custom_package' => true,
            'total_package' => 3,
            'updated_by' => $this->consultant->id,
            'upgrade_to_gp2' => false
        ];

        $params2 = [
            'joined_at' => $params['join_date'],
            'is_autocount' => !$params['custom_package'],
            'total_package' => $params['total_package'],
            'updated_by' => $params['updated_by'],
            'upgrade_to_gp2' => true
        ];

        $this->service
            ->shouldReceive('updatePackage')
            ->withArgs(function ($arg) use ($contract, $params2) {
                return $arg->is($contract, $params2);
            })
            ->once()
            ->andReturn($contract);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                $params
            );
        $response->assertOk();
    }

    public function testUpdateWithUpgradeToGP2AndGP1Contract(): void
    {
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $propertyLevel->id, 'status' => 'active']);
        $contract->properties()->attach($property);

        $params = [
            'join_date' => now()->toDateTimeString(),
            'custom_package' => true,
            'total_package' => 3,
            'updated_by' => $this->consultant->id,
            'upgrade_to_gp2' => true
        ];

        $this->service
            ->shouldReceive('updatePackage')
            ->withAnyArgs()
            ->once()
            ->andReturn($contract);

        $this->service
            ->shouldReceive('upgradeToGoldplus2')
            ->withArgs(function ($argContract, $user) use ($contract) {
                return $argContract->is($contract) && $user->is($this->consultant);
            })
            ->once();

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                $params
            );
        $response->assertOk();
    }

    public function testUpdateWithUpgradeToGP2AndGP1PromoContract(): void
    {
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1_PROMO]);
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $propertyLevel->id, 'status' => 'active']);
        $contract->properties()->attach($property);

        $params = [
            'join_date' => now()->toDateTimeString(),
            'custom_package' => true,
            'total_package' => 3,
            'updated_by' => $this->consultant->id,
            'upgrade_to_gp2' => true
        ];

        $this->service
            ->shouldReceive('updatePackage')
            ->withAnyArgs()
            ->once()
            ->andReturn($contract);

        $this->service
            ->shouldReceive('upgradeToGoldplus2')
            ->withArgs(function ($argContract, $user) use ($contract) {
                return $argContract->is($contract) && $user->is($this->consultant);
            })
            ->once();

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                $params
            );
        $response->assertOk();
    }

    public function testUpdateWithUpgradeToGP2AndNonGP1Contract(): void
    {
        $property = factory(Property::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_2]);
        $contract = factory(PropertyContract::class)->create(['property_level_id' => $propertyLevel->id, 'status' => 'active']);
        $contract->properties()->attach($property);

        $params = [
            'join_date' => now()->toDateTimeString(),
            'custom_package' => true,
            'total_package' => 3,
            'updated_by' => $this->consultant->id,
            'upgrade_to_gp2' => true
        ];

        $this->service
            ->shouldReceive('updatePackage')
            ->withAnyArgs()
            ->once()
            ->andReturn($contract);

        $this->service
            ->shouldNotReceive('upgradeToGoldplus2');

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                $params
            );
        $response->assertOk();
    }

    public function testUpdateWithMissingTimeJoinDate(): void
    {
        $contract = factory(PropertyContract::class)->create(['status' => 'active']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                ['join_date' => '2020-08-19']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'join_date' => ['The join date does not match the format Y-m-d H:i:s.'],
            ]
        ]);
    }

    public function testAddPropertyWithInvalidContractId(): void
    {
        $property = factory(Property::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.add-property', ['property_id' => $property->id, 'property_contract_id' => 0]));

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_contract_id' => [
                    'The selected property contract id is invalid.'
                ]
            ]
        ]);
    }

    public function testUpdateWithInvalidCustomPackage(): void
    {
        $contract = factory(PropertyContract::class)->create(['status' => 'active']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                ['join_date' => '2020-08-19 17:57:18', 'custom_package' => 'false']
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'custom_package' => ['The custom package field must be true or false.'],
            ]
        ]);
    }

    public function testAddPropertyWithInvalidPropertyId(): void
    {
        $contract = factory(PropertyContract::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.add-property', ['property_id' => 0, 'property_contract_id' => $contract->id]));

        $response->assertJson([
            'status' => false,
            'messages' => [
                'property_id' => [
                    'The selected property id is invalid.'
                ]
            ]
        ]);
    }

    public function testUpdateWithCustomPackageMissingTotalPackage(): void
    {
        $contract = factory(PropertyContract::class)->create(['status' => 'active']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json(
                'PUT',
                route('admin.property-package.update', ['propertyContract' => $contract]),
                ['join_date' => '2020-08-19 17:57:18', 'custom_package' => true]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'total_package' => ['The total package field is required when custom package is 1.'],
            ]
        ]);
    }

    public function testAddPropertyShouldInsertIntoDatabase(): void
    {
        $property = factory(Property::class)->create();
        $contract = factory(PropertyContract::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.add-property', ['property_id' => $property->id, 'property_contract_id' => $contract->id]));

        $this->assertDatabaseHas('property_contract_detail', [
            'property_id' => $property->id,
            'property_contract_id' => $contract->id
        ]);
    }

    public function testAddPropertyShouldReturnCorrectResponse(): void
    {
        $property = factory(Property::class)->create();
        $contract = factory(PropertyContract::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('POST', route('admin.property-package.add-property', ['property_id' => $property->id, 'property_contract_id' => $contract->id]));

        $response->assertJson(['status' => true]);
    }

    public function testAddPropertyWithMappedProperty(): void
    {
        $property = factory(Property::class)->create();
        $contract = factory(PropertyContract::class)->create();
        factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $contract->id
        ]);

        $count = PropertyContractDetail::where('property_contract_id', $contract->id)
            ->where('property_id', $property->id)
            ->count();

        $this->assertEquals(1, $count);
    }

    public function testKostListAssignToPropertyShouldContainKostAssignedToProperty(): void
    {
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $room1 = factory(Room::class)->create(['property_id' => $property->id]);
        $room2 = factory(Room::class)->create(['property_id' => null]);
        $otherRoom = factory(Room::class)->create(['property_id' => null]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $room1->id,
            'user_id' => $owner->id,
        ]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $room2->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();

        // Should contain kost has been assigned to property
        $this->assertTrue(collect($response->json('rows'))->contains('id', $room1->id));
        $this->assertEquals(2, $response->json('total'));
    }
    
    

    public function testKostListAssignToPropertyShouldContainKostNotAssignedToOtherProperty(): void
    {
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $room = factory(Room::class)->create(['property_id' => null]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();
        
        // Should contain kost has not been assigned to other property
        $this->assertTrue(collect($response->json('rows'))->contains('id', $room->id));
        $this->assertEquals(1, $response->json('total'));
    }

    public function testKostListAssignToPropertyShouldNotContainKostAssignedToOtherProperty(): void
    {
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $room = factory(Room::class)->create(['property_id' => null]);
        $otherRoom = factory(Room::class)->create(['property_id' => factory(Property::class)]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $otherRoom->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();

        $this->assertTrue(collect($response->json('rows'))->contains('id', $room->id));
        // Should not contain kost has been assigned to other property
        $this->assertFalse(collect($response->json('rows'))->contains('id', $otherRoom->id));
        $this->assertEquals(1, $response->json('total'));
    }

    public function testKostListAssignToPropertyShouldNotContainKostNotBelongToOwner(): void
    {
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $room = factory(Room::class)->create(['property_id' => null]);
        $otherRoom = factory(Room::class)->create(['property_id' => null]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();

        $this->assertTrue(collect($response->json('rows'))->contains('id', $room->id));
        // Should not contain kost does not belong to this owner
        $this->assertFalse(collect($response->json('rows'))->contains('id', $otherRoom->id));
        $this->assertEquals(1, $response->json('total'));
    }

    public function testKostListAssignToPropertyShouldReturnCorrectKost(): void
    {
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $roomA = factory(Room::class)->create(['property_id' => $property->id]);
        $roomB = factory(Room::class)->create(['property_id' => null]);
        $otherRoomA = factory(Room::class)->create(['property_id' => factory(Property::class)]);
        $otherRoomB = factory(Room::class)->create(['property_id' => factory(Property::class)]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $roomA->id,
            'user_id' => $owner->id,
        ]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $roomB->id,
            'user_id' => $owner->id,
        ]);

        factory(RoomOwner::class)->states('owner')->create([
            'designer_id' => $otherRoomA->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();
        
        $actualResponse = collect($response->json('rows'));
        $this->assertTrue($actualResponse->contains('id', $roomA->id));
        $this->assertTrue($actualResponse->contains('id', $roomB->id));
        $this->assertFalse($actualResponse->contains('id', $otherRoomA->id));
        $this->assertFalse($actualResponse->contains('id', $otherRoomB->id));
        $this->assertEquals(2, $response->json('total'));
    }

    public function testKostListAssignToPropertyOnlyContainWithOwnerStatusAsKostOwnerAndStatusVerified(): void
    {
        $this->withoutExceptionHandling();
        $owner = factory(User::class)->create(['is_owner' => true]);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        $room = factory(Room::class)->create(['property_id' => $property->id]);
        $otherRoom1 = factory(Room::class)->create(['property_id' => null]);
        $otherRoom2 = factory(Room::class)->create(['property_id' => null]);

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'owner_status'=> RoomOwner::STATUS_TYPE_KOS_OWNER,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        factory(RoomOwner::class)->create([
            'designer_id' => $otherRoom1->id,
            'user_id' => $owner->id,
            'owner_status'=> RoomOwner::STATUS_TYPE_APARTMENT_OWNER,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        factory(RoomOwner::class)->create([
            'designer_id' => $otherRoom1->id,
            'user_id' => $owner->id,
            'owner_status'=> RoomOwner::STATUS_TYPE_KOS_OWNER_OLD,
            'status' => RoomOwner::ROOM_REJECT_STATUS
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant)
            ->json('GET', route('admin.property-package.kost-list-assign-to-property', ['owner' => $owner, 'property' => $property]))
            ->assertOk();

        $actualResponse = collect($response->json('rows'));
        $this->assertTrue($actualResponse->contains('id', $room->id));
        $this->assertFalse($actualResponse->contains('id', $otherRoom1->id));
        $this->assertFalse($actualResponse->contains('id', $otherRoom2->id));
        $this->assertEquals(1, $response->json('total'));
    }
}
