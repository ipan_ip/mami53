<?php

namespace App\Http\Controllers\Admin\Level;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Level\RoomLevel;

class RoomLevelControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $roomLevel;
    
    protected function setUp()
    {
        parent::setUp();

        $this->roomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);
    }

    public function testIndexRoomLevel_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        
        factory(RoomLevel::class, 4)->create();

        $response = $this->actingAs($user)->get(route('admin.room-level.index'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.level.index');
        $response->assertViewHas('level');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('level');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    } 

    public function testIndexRoomLevel_WithKeySearch_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        factory(RoomLevel::class, 4)->create();

        $response = $this->actingAs($user)->get(route('admin.room-level.index', [ 'q' => $this->roomLevel->name ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.level.index');
        $response->assertViewHas('level');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('level');
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    } 

    public function testIndexRoomLevel_WithLimitPagination_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        factory(RoomLevel::class, 4)->create();

        $response = $this->actingAs($user)->get(route('admin.room-level.index', [ 'limit' => 3 ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.level.index');
        $response->assertViewHas('level');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('level');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(3, $result->perPage());
    } 

    public function testCreateRoomLevel_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.room-level.create'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.level.add_edit');
        $response->assertViewHas('isRegularAvail');
        $response->assertViewHas('chargingTypeOptions');
        $response->assertViewHas('chargeInvoiceTypeOptions');
        $response->assertViewHas('boxTitle');
    }

    public function testStoreRoomLevel_WithInvalidRequest_ShouldFail()
    {
        $response = $this->post(route('admin.room-level.store', []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreRoomLevel_WithMoreThan100PercentChargingFee_ShouldFail()
    {
        $data = $this->roomLevelForm();

        $data['charging_type'] = RoomLevel::CHARGING_TYPE_PERCENTAGE;
        $data['charging_fee'] = 120;

        $response = $this->post(route('admin.room-level.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreRoomLevel_ShouldSuccess()
    {
        $data = $this->roomLevelForm();

        $response = $this->post(route('admin.room-level.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Room Level was successfully added.');
        $this->assertDatabaseHas((new RoomLevel)->getTable(), [
            'name' => $this->roomLevel->name,
        ]);
    }

    public function testEditRoomLevel_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.room-level.edit', $this->roomLevel->id));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.room-level.level.add_edit');
        $response->assertViewHas('level');
        $response->assertViewHas('isRegularAvail');
        $response->assertViewHas('chargingTypeOptions');
        $response->assertViewHas('chargeInvoiceTypeOptions');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateRoomLevel_WithInvalidRequest_ShouldFail()
    {
        $response = $this->put(route('admin.room-level.update', $this->roomLevel->id, []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    } 

    public function testUpdateRoomLevel_WithMoreThan100PercentChargingFee_ShouldFail()
    {
        $data = $this->roomLevelForm();

        $data['charging_type'] = RoomLevel::CHARGING_TYPE_PERCENTAGE;
        $data['charging_fee'] = 120;

        $response = $this->put(route('admin.room-level.update', $this->roomLevel->id), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateRoomLevel_ShouldSuccess()
    {
        $data = $this->roomLevelForm();

        $response = $this->put(route('admin.room-level.update', $this->roomLevel->id), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Room Level was successfully updated.');
        $this->assertDatabaseHas((new RoomLevel)->getTable(), $data);
    }

    public function testDestroyRoomLevel_ShouldSuccess()
    {
        $response = $this->delete(route('admin.room-level.destroy', $this->roomLevel->id));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Room Level was successfully deleted.');
    }

    private function roomLevelForm()
    {
        return [
            "name" => "Reguler",
            "order" => 1,
            "is_regular" => true,
            "is_hidden" => false,
            "notes" => "Qui explicabo autem placeat.",
            "charging_fee" => 10,
            "charging_type" => RoomLevel::CHARGING_TYPE_AMOUNT,
            "charge_for_owner" => false,
            "charge_for_consultant" => true,
            "charge_for_booking" => true,
            "charge_invoice_type" => RoomLevel::CHARGE_INVOICE_TYPE_ALL,
            "charging_name" => "My Charging Name"
        ];
    }
}
