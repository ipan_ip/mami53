<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardType;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RewardListControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public $user;
    public $otherUser;
    public $rewardType;
    public $reward;
    public $redeems;

    protected function setUp() : void
    {
        parent::setUp();
        $this->withoutExceptionHandling();

        $this->otherUser = factory(User::class)->create();
        $this->user = factory(User::class)->create();

        $this->rewardType = factory(RewardType::class)->create();
        $this->reward = factory(Reward::class)->create([
            'type_id' => $this->rewardType->id,
            'start_date' => now(),
            'end_date' => now(),
        ]);
        $this->redeems = factory(RewardRedeem::class, 3)->create([
            'reward_id' => $this->reward->id,
            'user_id' => $this->otherUser->id,
        ]);
        $this->reward->quotas()->createMany([
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL_USER, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY_USER, 'value' => 10])->toArray(),
        ]);
    }

    public function testIndexPage_ViewOnlyPermission()
    {
        $user = factory(User::class)->create();
        $role = Role::create([
            'name' => 'role-view-reward-loyalty',
        ]);
        $perm = Permission::create([
            'name' => 'view-reward-loyalty'
        ]);
        $role->perms()->attach($perm->id);
        $user->roles()->attach($role->id);

        $response = $this->actingAs($user)->get(route('admin.reward-loyalty.list.index'));
        $response->assertOk();
        $response->assertDontSee('Add Reward');
        $response->assertDontSee('Update');
    }
    
    public function testShowPage_ViewOnlyPermission()
    {
        $reward = $this->reward;
        $user = $this->user;
        $role = Role::create([
            'name' => 'role-view-reward-loyalty',
        ]);
        $perm = Permission::create([
            'name' => 'view-reward-loyalty'
        ]);
        $role->perms()->attach($perm->id);
        $user->roles()->attach($role->id);

        $this->app->instance(Reward::class, $reward);
        $response = $this->actingAs($user)->get(route('admin.reward-loyalty.list.show', $reward->id));
        $response->assertOk();
        $response->assertDontSee('data-form-action');
    }
    
    public function testUpdatePage_TotalQuotaLessThanSucceedRedeemed_ShouldFailed()
    {
        $user = factory(User::class)->create();
        $rewardType = factory(RewardType::class)->create();
        $reward = factory(Reward::class)->create([
            'type_id' => $rewardType->id,
            'start_date' => now(),
            'end_date' => now()->addDays(2),
        ]);
        factory(RewardRedeem::class, 3)->create([
            'reward_id' => $reward->id,
            'user_id' => factory(User::class)->create()->id,
            'status' => RewardRedeem::STATUS_SUCCESS
        ]);
        factory(RewardRedeem::class)->create([
            'reward_id' => $reward->id,
            'user_id' => factory(User::class)->create()->id,
            'status' => RewardRedeem::STATUS_FAILED
        ]);

        $reward->quotas()->createMany([
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL, 'value' => 2])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY, 'value' => 2])->toArray(),
        ]);

        $inputData = array_merge(factory(Reward::class)->make()->toArray(), [
            'type_id' => $rewardType->id,
            'start_date' => $reward->start_date,
            'end_date' => $reward->end_date,
            'quota' => [
                'total' => 2,
                'total_user' => 2,
            ]
        ]);

        $this->app->instance(Reward::class, $reward);

        $response = $this->actingAs($user)->put(route('admin.reward-loyalty.list.update', $reward->id), $inputData);
        $response->assertStatus(302);
        $response->assertSessionHas('errors');

        $this->assertEquals(3, $reward->getRedeemedCount());
    }
}
