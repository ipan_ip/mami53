<?php

namespace app\Http\Controllers\Admin\Room;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class KostControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $admin;
    protected $role;

    private const CREATE_KOST_URL = 'admin/kost/create';
    private const KOST_URL = 'admin/kost/';

    protected function setUp(): void
    {
        parent::setUp();

        $this->role = factory(Role::class)->create(['name' => 'admin']);
        $this->admin = factory(User::class)->create();

        $this->admin->attachRole($this->role);
        $this->role->attachPermission(factory(Permission::class)->create(['name' => 'access-kost']));
    }

    /**
    * @group PMS-495
    */    
    public function testAccessFormCreateSuccess()
    {
        $this->markTestSkipped('must be revisited.');
        $response = $this->actingAs($this->admin)->get(self::CREATE_KOST_URL);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.kost.form');
    }

    /**
    * @group PMS-495
    */    
    public function testAccessFormEditSuccess()
    {
        $this->markTestSkipped('must be revisited.');
        $kost = factory(Room::class)->create();

        $response = $this->actingAs($this->admin)->get(self::KOST_URL . $kost->id . '/edit');

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.kost.form');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreNewKostIncompleteParam()
    {
        $params = [
            'name' => 'Kos Tambora',
        ];

        $response = $this->actingAs($this->admin)->post(self::KOST_URL, $params);

        $response->assertSessionHasErrors([
            'gender' => 'The gender field is required.'
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testStoreNewKostSuccess()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);

        $params = array_merge(
            $this->generateParams(),
            [
                'name' => 'Kos Tambora Jetis Yogyakarta',
                'owner_phone' => $owner->phone_number,
                'property_name' => 'Kos Tambora',
            ]
        );

        $response = $this->actingAs($this->admin)->post(self::KOST_URL, $params);
        
        $kost = Room::where('name', 'Kos Tambora Jetis Yogyakarta')->first();

        $this->assertTrue($kost instanceof Room);

        $response->assertRedirect(route('admin.card.index', $kost->id));
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateKostWithoutUnitType()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);
        $kost = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $kost
        ]);

        $property = factory(Property::class)->create([
            'owner_user_id' => $owner->id,
            'name' => 'Kos Tambora',
            'has_multiple_type' => true,
        ]);

        $params = array_merge(
            $this->generateParams(),
            [
                'name' => 'Kos Tambora Jetis Yogyakarta',
                'owner_phone' => $owner->phone_number,
                'property_id' => $property->id,
            ]
        );

        $response = $this->actingAs($this->admin)->put(self::KOST_URL . $kost->id, $params);

        $response->assertSessionHasErrors([
            'unit_type' => 'Tipe kamar dari properti ini harus diisi.'
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateKostSuccess()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);
        $kost = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $kost
        ]);

        $property = factory(Property::class)->create([
            'owner_user_id' => $owner->id,
            'name' => 'Kos Tambora',
            'has_multiple_type' => true,
        ]);

        $params = array_merge(
            $this->generateParams(),
            [
                'name' => 'Kos Tambora Tipe A Jetis Yogyakarta',
                'owner_phone' => $owner->phone_number,
                'property_id' => $property->id,
                'unit_type' => 'Tipe A',
            ]
        );

        $response = $this->actingAs($this->admin)->put(self::KOST_URL . $kost->id, $params);

        $response->assertStatus(302);

        $kost->refresh();

        $this->assertTrue($kost instanceof Room);

        $this->assertEquals($kost->name, $params['name']);
    }

    private function generateParams()
    {
        return [
            'gender' => 1,
            'building_year' => 2020,
            'description' => 'this is kost description',
            'size' => '4x4',
            'room_count' => 5,
            'room_available' => 2,
            'address' => 'this is kost address',
            'latitude' => '-7.7858485',
            'longitude' => '110.3680087',
            'province' => 'Daerah Istimewa Yogyakarta',
            'area_city' => 'Kota Yogyakarta',
            'area_subdistrict' => 'Jetis',
            'price_monthly' => 1000000,
            'facility_share_ids' => [1],
            'facility_room_ids' => [2],
            'facility_bath_ids' => [3],
            'owner_name' => 'Budi',
            'agent_name' => 'Aggit',
        ];
    }
}
