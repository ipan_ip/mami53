<?php

namespace app\Http\Controllers\Admin\Room;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Property\Property;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PropertyControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $admin;
    protected $role;

    private const PROPERTY_URL = 'admin/kost/property/';

    protected function setUp(): void
    {
        parent::setUp();

        $this->role = factory(Role::class)->create(['name' => 'admin']);
        $this->admin = factory(User::class)->create();

        $this->admin->attachRole($this->role);
        $this->role->attachPermission(factory(Permission::class)->create(['name' => 'access-kost']));
    }

    /**
    * @group PMS-495
    */    
    public function testStoreNewPropertyIncompleteParam()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);

        $params = [
            'property_name' => 'Kos Tambora',
            'user_id' => $owner->id
        ];

        $response = $this->actingAs($this->admin)->post(self::PROPERTY_URL, $params);

        $response->assertSessionHasErrors([
            'has_multiple_type' => 'The has multiple type field is required.'
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testStoreNewKostSuccess()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);

        $params = [
            'property_name' => 'Kos Tambora',
            'user_id' => $owner->id,
            'has_multiple_type' => true
        ];

        $response = $this->actingAs($this->admin)->post(self::PROPERTY_URL, $params);
        
        $property = Property::where('name', 'Kos Tambora')->first();

        $this->assertTrue($property instanceof Property);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateKostIncompleteParam()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);
        $property = factory(Property::class)->create([
            'owner_user_id' => $owner->id,
            'name' => 'Kos Tambora',
            'has_multiple_type' => true,
        ]);

        $params = [
            'property_name' => 'Kos Lompobatang',
        ];

        $response = $this->actingAs($this->admin)->put(self::PROPERTY_URL . $property->id, $params);
        
        $response->assertSessionHasErrors([
            'has_multiple_type' => 'The has multiple type field is required.'
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdatePropertySuccess()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_owner' => 'true',
            'role' => 'user',
        ]);
        $property = factory(Property::class)->create([
            'owner_user_id' => $owner->id,
            'name' => 'Kos Tambora',
            'has_multiple_type' => true,
        ]);

        $params = [
            'property_name' => 'Kos Lompobatang',
            'has_multiple_type' => true
        ];

        $response = $this->actingAs($this->admin)->put(self::PROPERTY_URL . $property->id, $params);
        
        $property->refresh();

        $this->assertEquals($property->name, $params['property_name']);
    }
}
