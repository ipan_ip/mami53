<?php

namespace App\Http\Controllers\Admin\RewardLoyalty;

use App\Entities\Reward\Reward;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RewardTargetedUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testRemove_ShouldSuccess()
    {
        $reward = factory(Reward::class)->create();
        $reward->target()->sync([$this->user->id]);

        $response = $this->actingAs($this->user)->put(route('admin.reward-loyalty.list.targeted-user.remove', $reward->id));
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Reward was updated successfully.');

        $this->assertDatabaseMissing('reward_target', [
            'reward_id' => $reward->id,
            'user_id' => $this->user->id
        ]);
    }
}
