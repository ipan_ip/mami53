<?php

namespace App\Http\Controllers\Admin\Sanjunipero;

use App\Entities\Media\Media;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\{
    LengthAwarePaginator,
    Paginator
};
use Illuminate\View\View as CustomView;
use Mockery;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

function old($type)
{
    return DynamicLandingParentControllerTest::$functions->old($type);
}

class DynamicLandingParentControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const SAVE = 'admin/sanjunipero/parent/save';
    private const UPDATE = 'admin/sanjunipero/parent/update';
    public static $functions;
    private $repo, $faker, $controller;

    protected function setUp() : void
    {
        parent::setUp();

        self::$functions = Mockery::mock();
        $this->repo = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $this->controller = app()->make(DynamicLandingParentController::class);
        $this->faker = Faker::create();
    }

    public function testIndex_Success()
    {
        $length = 10;
        $perPage = 2;

        app()->user = factory(User::class)->make();
        $dynamicLandingParent = factory(DynamicLandingParent::class, $length)->make();

        $dynamicLandingParent = new LengthAwarePaginator(
            $dynamicLandingParent->forPage(Paginator::resolveCurrentPage() , $perPage),
            $dynamicLandingParent->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        $this->repo->shouldReceive('index')->andReturn($dynamicLandingParent);

        $res = $this->controller->index($this->repo);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::INDEX_TITLE);
        $this->assertEquals($res->getData()['rows'], $dynamicLandingParent);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testCreate_Success()
    {
        app()->user = factory(User::class)->make();

        self::$functions->shouldReceive('old');

        $res = $this->controller->create();

        $this->assertSame($res->getData()['boxTitle'], $this->controller::CREATE_TITLE);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testSave_Success()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('save')->with($req)->andReturn(true);
        $res = $this->controller->save($req, $this->repo);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::SAVE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_Fail()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('save')->with($req)->andReturn(false);
        $res = $this->controller->save($req, $this->repo);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::SAVE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_ValidatorImageCalled()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $res = $this->controller->save($req, $this->repo);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['image_url']),
            $this->controller->validationMessages['image_url.url']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testEdit_Success()
    {
        app()->user = factory(User::class)->make();
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);

        $this->repo->shouldReceive('findById')->with($dynamicLandingParent->id)->andReturn($dynamicLandingParent);

        $res = $this->controller->edit($this->repo, $dynamicLandingParent->id);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::EDIT_TITLE);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testUpdate_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('edit')->with($req, $dynamicLandingParent->id)->andReturn(true);
        $res = $this->controller->update($req, $this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_Fail()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $this->repo->shouldReceive('edit')->with($req, $dynamicLandingParent->id)->andReturn(false);
        $res = $this->controller->update($req, $this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_ValidatorImageCalled()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $res = $this->controller->update($req, $this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['image_url']),
            $this->controller->validationMessages['image_url.url']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testActivate_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => false
            ]
        );

        $this->repo->shouldReceive('activate')->with($dynamicLandingParent->id)->andReturn($dynamicLandingParent);
        $res = $this->controller->activate($this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testActivate_Fail()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => false
            ]
        );

        $this->repo->shouldReceive('activate')->with($dynamicLandingParent->id)->andReturn(null);
        $res = $this->controller->activate($this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testDeactivate_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => true
            ]
        );

        $this->repo->shouldReceive('deactivate')->with($dynamicLandingParent->id)->andReturn($dynamicLandingParent);
        $res = $this->controller->deactivate($this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testDeactivate_Fail()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->make([
            'id' => mt_rand(1, 999999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingParent->id,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->sentence(),
                'is_active' => true
            ]
        );

        $this->repo->shouldReceive('deactivate')->with($dynamicLandingParent->id)->andReturn(null);
        $res = $this->controller->deactivate($this->repo, $dynamicLandingParent->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testSetTempImage_Create()
    {
        $desktopMedia = factory(Media::class)->create();
        $desktopMediaId = $desktopMedia->id;

        $mobileMedia = factory(Media::class)->create();
        $mobileMediaId = $mobileMedia->id;

        self::$functions->shouldReceive('old')
            ->with('desktop_header_media_id')
            ->andReturn($desktopMediaId);

        self::$functions->shouldReceive('old')
            ->with('mobile_header_media_id')
            ->andReturn($mobileMediaId);

        $parent = factory(DynamicLandingParent::class)->create();

        $res = $this->callNonPublicMethod(
            DynamicLandingParentController::class,
            'setTempImage',
            [$parent, 'create']);
        
        $this->assertSame($res->desktop_header_media_id, $desktopMediaId);
        $this->assertSame($res->mobile_header_media_id, $mobileMediaId);
    }

    public function testSetTempImage_Update()
    {
        $desktopMedia = factory(Media::class)->create();
        $desktopMediaId = $desktopMedia->id;

        $mobileMedia = factory(Media::class)->create();
        $mobileMediaId = $mobileMedia->id;

        self::$functions->shouldReceive('old')
            ->with('desktop_header_media_id')
            ->andReturn($desktopMediaId);

        self::$functions->shouldReceive('old')
            ->with('mobile_header_media_id')
            ->andReturn($mobileMediaId);

        $parent = factory(DynamicLandingParent::class)->create([
            'desktop_header_media_id' => $desktopMediaId,
            'mobile_header_media_id' => $mobileMediaId,
        ]);

        $res = $this->callNonPublicMethod(
            DynamicLandingParentController::class,
            'setTempImage',
            [$parent, 'update']);
        
        $this->assertSame($res->desktop_header_media_id, $desktopMediaId);
        $this->assertSame($res->mobile_header_media_id, $mobileMediaId);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}