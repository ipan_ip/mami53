<?php

namespace App\Http\Controllers\Admin\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Repositories\Sanjunipero\DynamicLandingChildRepositoryEloquent;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\{
    LengthAwarePaginator,
    Paginator
};
use Illuminate\View\View as CustomView;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class DynamicLandingChildControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const INDEX = 'admin/sanjunipero/child/';
    private const SAVE = 'admin/sanjunipero/child/save';
    private const UPDATE = 'admin/sanjunipero/child/update';
    private $childRepo, $parentRepo, $faker, $controller;

    protected function setUp() : void
    {
        parent::setUp();

        $this->childRepo = $this->mockPartialAlternatively(DynamicLandingChildRepositoryEloquent::class);
        $this->parentRepo = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $this->controller = app()->make(DynamicLandingChildController::class);
        $this->faker = Faker::create();
    }

    public function testIndex_NoParentSuccess()
    {
        $length = 10;
        $perPage = 2;

        $req = $this->createFakeRequest(
            self::INDEX,
            'GET',
            [
                'parent' => null
            ]
        );

        $dynamicLandingChild = factory(DynamicLandingChild::class, $length)->make();

        $dynamicLandingChild = new LengthAwarePaginator(
            $dynamicLandingChild->forPage(Paginator::resolveCurrentPage() , $perPage),
            $dynamicLandingChild->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        $this->childRepo->shouldReceive('index')->andReturn($dynamicLandingChild);

        $res = $this->controller->index($req, $this->parentRepo, $this->childRepo);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::INDEX_TITLE);
        $this->assertEquals($res->getData()['rows'], $dynamicLandingChild);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testIndex_WithParentSuccess()
    {
        $length = 10;
        $perPage = 2;

        $req = $this->createFakeRequest(
            self::INDEX,
            'GET',
            [
                'parent' => 1
            ]
        );

        $dynamicLandingParent = factory(DynamicLandingParent::class)->make();
        $dynamicLandingChild = factory(DynamicLandingChild::class, $length)->make();
        $dynamicLandingParent->children = $dynamicLandingChild;

        $dynamicLandingChild = new LengthAwarePaginator(
            $dynamicLandingChild->forPage(Paginator::resolveCurrentPage() , $perPage),
            $dynamicLandingChild->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        $this->parentRepo->shouldReceive('getWithChildById')->andReturn($dynamicLandingParent);
        $this->childRepo->shouldReceive('index')->andReturn($dynamicLandingChild);

        $res = $this->controller->index($req, $this->parentRepo, $this->childRepo);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::INDEX_TITLE);
        $this->assertEquals($res->getData()['rows'], $dynamicLandingParent->children);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testSave_Success()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);
        $this->childRepo->shouldReceive('save')->with($req)->andReturn(true);
        $res = $this->controller->save($req, $this->childRepo);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::SAVE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_Fail()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);
        $this->childRepo->shouldReceive('save')->with($req)->andReturn(false);
        $res = $this->controller->save($req, $this->childRepo);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::SAVE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSave_ValidatorAreaNameCalled()
    {
        $req = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => null,
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);
        $res = $this->controller->save($req, $this->childRepo);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['area_name']),
            $this->controller->validationMessages['area_name.required']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testEdit_Success()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 999999)
        ]);

        $this->childRepo->shouldReceive('findById')->with($dynamicLandingChild->id)->andReturn($dynamicLandingChild);

        $res = $this->controller->edit($this->parentRepo, $this->childRepo, $dynamicLandingChild->id);

        $this->assertSame($res->getData()['boxTitle'], $this->controller::EDIT_TITLE);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testUpdate_Success()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingChild->id,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);        
        $this->childRepo->shouldReceive('edit')->with($req, $dynamicLandingChild->id)->andReturn(true);
        $res = $this->controller->update($req, $this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getSession()->all()['message'],
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_Fail()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingChild->id,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);
        $this->childRepo->shouldReceive('edit')->with($req, $dynamicLandingChild->id)->andReturn(false);
        $res = $this->controller->update($req, $this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getSession()->all()['errors']->getBags()['default']->messages()[0][0],
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testUpdate_ValidatorAreaNameCalled()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);
        $req = $this->createFakeRequest(
            self::UPDATE.$dynamicLandingChild->id,
            'POST',
            [
                'parent_id' => mt_rand(1, 99),
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => null,
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );

        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->states('active')->create([
                'parent_id' => $parent->id
            ]);
        $this->parentRepo->shouldReceive('findById')->andReturn($parent);
        $res = $this->controller->update($req, $this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            reset($res->getSession()->all()['errors']->getBags()['default']->messages()['area_name']),
            $this->controller->validationMessages['area_name.required']
        );
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testActivate_Success()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);

        $this->childRepo->shouldReceive('activate')->with($dynamicLandingChild->id)->andReturn($dynamicLandingChild);
        $res = $this->controller->activate($this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testActivate_Fail()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);

        $this->childRepo->shouldReceive('activate')->with($dynamicLandingChild->id)->andReturn(null);
        $res = $this->controller->activate($this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testDeactivate_Success()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);

        $this->childRepo->shouldReceive('deactivate')->with($dynamicLandingChild->id)->andReturn($dynamicLandingChild);
        $res = $this->controller->deactivate($this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_SUCCESS
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testDeactivate_Fail()
    {
        $dynamicLandingChild = factory(DynamicLandingChild::class)->make([
            'id' => mt_rand(1, 99999)
        ]);

        $this->childRepo->shouldReceive('deactivate')->with($dynamicLandingChild->id)->andReturn(null);
        $res = $this->controller->deactivate($this->childRepo, $dynamicLandingChild->id);

        $this->assertSame(
            $res->getData()->message,
            $this->controller::UPDATE_FAIL
        );
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}