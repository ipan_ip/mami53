<?php

namespace App\Http\Controllers\Admin;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Booking\BookingDiscount;

class DiscountManagementControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware; 
    
    private const VERIFY_ROOM_URL = 'admin/booking/discount/verify';
    private const REMOVE_DISCOUNT_URL = 'admin/booking/discount/remove';
    private const POST_BOOKING_URL = 'admin/booking/discount';

    public function testVerifyRoomNotFound()
    {
        $response = $this->post(self::VERIFY_ROOM_URL, ['slug' => 'anything']);

        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => 'Kost tidak ditemukan!',
        ]);
    }

    public function testVerifyRoomNoPriceMonthly()
    {
        $room   = factory(Room::class)->create([
            'slug' => 'kost-jogja-kost-putra',
            'price_monthly' => 0,
            'is_active' => true
        ]);
        $response = $this->post(self::VERIFY_ROOM_URL, ['slug' => $room->slug]);

        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => 'Kost tidak mempunyai harga sewa bulanan!',
        ]);
    }
    
    public function testVerifyRoomNonBisaBooking()
    {
        $room   = factory(Room::class)->create([
            'slug' => 'kost-jogja-kost-putra-2',
            'price_monthly' => 100000,
            'is_booking' => 0,
            'is_active' => true
        ]);
        $response = $this->post(self::VERIFY_ROOM_URL, ['slug' => $room->slug]);

        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => 'Kost tidak mempunyai fitur "Bisa Booking"!',
        ]);
    }

    public function testVerifyRoom()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id' => $photo->id,
            'slug' => 'kost-jogja-kost-putra-4',
            'price_monthly' => 100000,
            'is_booking' => 1,
            'is_active' => true
        ]);
        
        $response = $this->post(self::VERIFY_ROOM_URL, ['slug' => $room->slug]);

        $response->assertOk();
        $response->assertJson([
            'success' => true,
        ]);
    }

    public function testRemoveNotFound()
    {
        $response = $this->post(self::REMOVE_DISCOUNT_URL, ['id' => 12312]);

        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => "Gagal menemukan data discount. Silahkan refresh halaman dan coba lagi."
        ]);
    }

    public function testRemove()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 100000,
            'is_booking' => 1,
            'is_active' => true
        ]);
        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly'
        ]);

        $response = $this->post(self::REMOVE_DISCOUNT_URL, ['id' => $discount->id]);

        $response->assertOk();
        $response->assertJson([
            'success' => true,
        ]);
    }

    public function testRemoveNoPriceType()
    {
        $discount = factory(BookingDiscount::class)->create([
            'price_type' => ''
        ]);

        $response = $this->post(self::REMOVE_DISCOUNT_URL, ['id' => $discount->id]);

        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => "Gagal mengembalikan harga kost/apt. Silahkan refresh halaman dan coba lagi.",
        ]);
    }

    public function testStoreEmptyData()
    {
        $response = $this->json('POST', self::POST_BOOKING_URL, []);
        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => "Gagal menyimpan data discount. Mohon cek input anda.",
        ]);
    }

    public function testStoreWrongDiscountSource()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $param = $this->buildFormParam();
        $param['discount_source'] = 'anything_wrong';

        $response = $this->json('POST', self::POST_BOOKING_URL, $param);
        $response->assertOk();
        $response->assertJson([
            'success' => false,
            'message' => "Gagal menyimpan data discount. Mohon cek input anda.",
        ]);
    }

    // on success scenario
    public function testStoreSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $param = $this->buildFormParam();
        $param['designer_id']            = $room->id;
        $param['real_price']             = 1000000; // sejuta
        $param['markup_value']           = 100; // 100%
        $param['discount_type_owner']    = 'percentage';
        $param['discount_value_owner']   = 10;    // in %
        $param['discount_type_mamikos']  = 'nominal';
        $param['discount_value_mamikos'] = 100000; // in nominal

        $response = $this->json('POST', self::POST_BOOKING_URL, $param);
        $response->assertOk();
        $response->assertJson(['success' => true]);
        $this->assertDatabaseHas(
            'booking_discount',
            [
                'designer_id'              => $param['designer_id'],
                'discount_value'           => 300000,
                'discount_type'            => 'nominal',
                'discount_value_mamikos'   => 100000,
                'discount_type_mamikos'    => 'nominal',
                'discount_value_owner'     => 10,
                'discount_type_owner'      => 'percentage',
            ]
        );
    }

    public function testExtractDiscountProportionDataBothPercentage()
    {
        $discountController = $this->app->make(DiscountManagementController::class);

        $param  = $this->buildFormParam();
        $method = $this->getNonPublicMethodFromClass(DiscountManagementController::class, 'extractDiscountProportionData');
        $result = $method->invokeArgs($discountController, [$param]);

        $this->assertEquals('percentage', $result['discount_type']);
        $this->assertEquals(30, $result['discount_value']);
    }

    public function testExtractDiscountProportionDataBothNominal()
    {
        $discountController = $this->app->make(DiscountManagementController::class);

        $param  = $this->buildFormParam();
        $param['discount_type_mamikos']  = 'nominal';
        $param['discount_type_owner']    = 'nominal';
        $param['discount_value_owner']   = 15000;
        $param['discount_value_mamikos'] = 10000;

        $method = $this->getNonPublicMethodFromClass(DiscountManagementController::class, 'extractDiscountProportionData');
        $result = $method->invokeArgs($discountController, [$param]);

        $this->assertEquals('nominal', $result['discount_type']);
        $this->assertEquals(25000, $result['discount_value']);
    }

    public function testExtractDiscountProportionDataMixed()
    {
        $discountController = $this->app->make(DiscountManagementController::class);

        $param  = $this->buildFormParam();
        $param['real_price']             = 1000000; // sejuta
        $param['markup_value']           = 100; // 100%
        $param['discount_type_owner']    = 'percentage';
        $param['discount_value_owner']   = 10;    // in %
        $param['discount_type_mamikos']  = 'nominal';
        $param['discount_value_mamikos'] = 100000; // in nominal

        $method = $this->getNonPublicMethodFromClass(DiscountManagementController::class, 'extractDiscountProportionData');
        $result = $method->invokeArgs($discountController, [$param]);

        $this->assertEquals('nominal', $result['discount_type']);
        $this->assertEquals(300000, $result['discount_value']);
    }

    public function testExtractDiscountProportionDataMixed2()
    {
        $discountController = $this->app->make(DiscountManagementController::class);

        $param  = $this->buildFormParam();
        $param['real_price']             = 1000000; // sejuta
        $param['markup_value']           = 100; // 100%
        $param['discount_type_owner']    = 'nominal';
        $param['discount_value_owner']   = 400000;    // in %
        $param['discount_type_mamikos']  = 'percentage';
        $param['discount_value_mamikos'] = 10; // in nominal

        $method = $this->getNonPublicMethodFromClass(DiscountManagementController::class, 'extractDiscountProportionData');
        $result = $method->invokeArgs($discountController, [$param]);

        $this->assertEquals('nominal', $result['discount_type']);
        $this->assertEquals(600000, $result['discount_value']);
    }

    public function testGetPriceAfterMarkup()
    {
        $discountController = $this->app->make(DiscountManagementController::class);
        $method = $this->getNonPublicMethodFromClass(DiscountManagementController::class, 'getPriceAfterMarkup');

        $result = $method->invokeArgs($discountController, [10, 'percentage', 100]);
        $this->assertEquals(110, $result);

        $result = $method->invokeArgs($discountController, [50, 'nominal', 100]);
        $this->assertEquals(150, $result);
    }

    /**
     * This method providing for postInsert params
     * @return array
     */
    private function buildFormParam(): array
    {
        return [
            "designer_id"              => "163126",
            "price_type"               => "weekly",
            "price_type_origin"        => "price_weekly",
            "real_price"               => "1000000", // sejuta
            "ad_link"                  => config('app.url') . "/room/kost-bantul-kost-putra-eksklusif-test-jambu-1",
            "is_active"                => "1",
            "markup_type"              => "percentage",
            "markup_value"             => "100",
            "discount_type"            => "percentage",
            "discount_value"           => "0",
            "discount_type_mamikos"    => "percentage",
            "discount_type_owner"      => "percentage",
            "discount_value_mamikos"   => "10",
            "discount_value_owner"     => "20",
            "discount_source"          => "mamikos_and_owner"
        ];
    }
}
