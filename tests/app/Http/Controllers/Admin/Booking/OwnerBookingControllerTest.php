<?php

namespace app\Http\Controllers\Admin\Booking;

use App\Entities\Booking\BookingOwner;
use App\Entities\Generate\ListingReason;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Room\BookingOwnerRequestService;
use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OwnerBookingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const INSTANT_BOOKING_URL = '/admin/booking/owner/instant-booking';
    private const OWNER_API_URL = '/admin/users/owner';
    private const REJECT_REASON = '/admin/booking/owner/reject-reason';
    private const REQUEST_REJECT = '/admin/booking/owner/reject';

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testInstantBookingList()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::INSTANT_BOOKING_URL);
        $response->assertViewIs('admin.contents.booking.instant-booking.index');
        $response->assertStatus(200);
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('instantBooks');
    }

    public function defaultResponseInstantBookingList()
    {
        return [
            'id' => 27,
            'owner_id' => 1447076,
            'is_active' => 1,
            'rent_counts' => 'a:3:{i:0;s:6:\"weekly\";i:1;s:9:\"quarterly\";i:2;s:6:\"yearly\";}',
            'is_instant_booking' => 1,
            'additional_prices' => 'a:2:{i:0;a:2:{s:3:\"key\";s:7:\"listrik\";s:5:\"value\";s:5:\"35000\";}i:1;a:2:{s:3:\"key\";s:3:\"air\";s:5:\"value\";s:5:\"35000\";}}',
            'created_at' => '2020-05-26 15:59:59',
            'updated_at' => '2020-05-26 15:59:59',
            'deleted_at' => null,
            'user' => [
                'name' => 'Raga',
                'phone_number' => '08324324324',
            ]
        ];
    }

    public function testCreateInstantBooking()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->json('GET', self::INSTANT_BOOKING_URL.'/create');
        $response->assertViewIs('admin.contents.booking.instant-booking.create');
        $response->assertStatus(200);
    }

    public function testStoreInstantBookingIsNotOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => false
        ]);

        $response = $this->actingAs($user)->json('POST', self::INSTANT_BOOKING_URL.'/store', [
            'rent_count' => ['semiannually'],
            'is_instant_booking' => 1,
            'owner_id' => $user->id
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('', $response);
    }

    public function testStoreInstantBookingOwnerIsNotActiveBooking()
    {
        $user = factory(User::class)->create([
            'is_owner' => false
        ]);

        BookingOwner::isBookingAllRoom($user);

        $response = $this->actingAs($user)->json('POST', self::INSTANT_BOOKING_URL.'/store', [
            'rent_count' => ['semiannually'],
            'is_instant_booking' => 1,
            'owner_id' => $user->id
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('', $response);
    }

    public function testStoreInstantBooking()
    {
        $user = factory(User::class)->make();

        $mockOwnerBookingRepo = $this->mockPartialAlternatively('App\Repositories\Booking\Owner\OwnerBookingRepository');
        $mockOwnerBookingRepo->shouldReceive('storeInstantBook')
            ->andReturn([
                'owner_id' => '1447076',
                'is_active' => 1,
                'rent_counts' => 'a:3:{i:0;s:6:\"weekly\";i:1;s:12:\"semiannually\";i:2;s:6:\"yearly\";}',
                'additional_prices' => 'a:2:{i:0;a:2:{s:3:\"key\";s:7:\"listrik\";s:5:\"value\";s:5:\"35000\";}i:1;a:2:{s:3:\"key\";s:3:\"air\";s:5:\"value\";s:5:\"35000\";}}',
                'is_instant_booking' => true,
                'updated_at' => '2020-05-27 09:55:54',
                'created_at' => '2020-05-27 09:55:54',
                'id' => 28
            ]);

        $response = $this->actingAs($user)->json('POST', self::INSTANT_BOOKING_URL.'/store');
        $response->assertStatus(302);
        $response->assertRedirect('', $response);
    }

    public function testEditInstantBooking()
    {
        $user = factory(User::class)->make([
            'id' => 1
        ]);

        $bookingOwner = factory(BookingOwner::class)->make([
            'id' => 123,
            'owner_id' => $user->id
        ]);

        $mockOwnerBookingRepo = $this->mockPartialAlternatively('App\Repositories\Booking\Owner\OwnerBookingRepository');
        $mockOwnerBookingRepo->shouldReceive('getById')->andReturn([
            'status' => true,
            'message' => 'Data found',
            'data' => $bookingOwner
        ]);

        $mockOwnerBookingRepo->shouldReceive('getRentCountsActiveRooms')->andReturn([
            'status' => true,
            'message' => 'Data found',
            'data' => ['daily','weekly','monthly']
        ]);

        $response = $this->actingAs($user)->json('GET', self::INSTANT_BOOKING_URL.'/edit/'.$bookingOwner->id);
        $response->assertViewIs('admin.contents.booking.instant-booking.edit');
        $response->assertStatus(200);
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('instantBooking');
    }

    public function testUpdateInstantBooking()
    {
        $user = factory(User::class)->make([
            'id' => 1
        ]);

        $bookingOwner = factory(BookingOwner::class)->make([
            'id' => 123,
            'owner_id' => $user->id
        ]);

        $mockOwnerBookingRepo = $this->mockPartialAlternatively('App\Repositories\Booking\Owner\OwnerBookingRepository');
        $mockOwnerBookingRepo->shouldReceive('updateInstantBook')->andReturn([
            'status' => true,
            'message' => 'Data found',
            'data' => $bookingOwner
        ]);

        $response = $this->call('POST', self::INSTANT_BOOKING_URL.'/update/'.$bookingOwner->id);
        $response->assertStatus(302);
        $response->assertRedirect('', $response);
    }

    public function testRentCountActiveRoomsOwner()
    {
        $owner = factory(User::class)->create([
            'id' => 123,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'is_booking' => true,
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        $mockOwnerBookingRepo = $this->mockPartialAlternatively('App\Repositories\Booking\Owner\OwnerBookingRepository');
        $mockOwnerBookingRepo->shouldReceive('getRentCountsActiveRooms')->andReturn([
            'status' => true,
            'message' => 'Data found',
            'data' => ['daily','weekly','monthly']
        ]);

        $response = $this->actingAs($owner)->json('GET', self::OWNER_API_URL.'/'.$owner->id.'/rent-counts-active-rooms');
        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'message' => 'Data found',
            'data' => [
                'daily',
                'weekly',
                'monthly',
            ]
        ]);
    }

    public function testListRejectReasonWithValidRoomId()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $bbkRequest = factory(BookingOwnerRequest::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => BookingOwnerRequest::BOOKING_WAITING
        ]);

        $bbkRequest = factory(ListingReason::class)->create([
            'listing_id' => $room->id,
            'from' => 'bbk',
        ]);

        $response = $this->actingAs($user)->call('GET', self::REJECT_REASON."/$room->id");
        $response->assertStatus(200);
        $response->assertViewHas('room');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('reject_reason');
        $response->assertViewHas('reject_reason_select');
        $response->assertViewHas('bbkRequest');
    }

    public function testListRejectReasonWithInvalidRoomId()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->call('GET', self::REJECT_REASON.'/0');

        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Kos tidak ditemukan');
    }

    public function testRequestRejectWithTextLenghtIsGreaterThanTextMax()
    {
        $user = factory(User::class)->create();
        $text =  str_random(301);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT.'/0', [
            'reject_remark_text' => $text
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    public function testRequestRejectWithListingReasonAndRejectRemarkAreNull()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => null,
            'reject_remark' => 'textarea'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Alasan ditolak harus diisi');
    }

    public function testRequestRejectWithInvalidRoomId()
    {
        $user = factory(User::class)->create();
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/0", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Kost tidak ditemukan');
    }

    public function testRequestRejectWithRoomOwnerIsNull()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Pemilik kost tidak ditemukan');
    }

    public function testRequestRejectSingleReject()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea',
            'action' => ''
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('message', 'Permintaan aktifkan booking ditolak');
    }

    public function testRequestRejectBulkReject()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea',
            'action' => 'bulk'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('message', 'Permintaan aktifkan booking ditolak');
    }

    public function testRequestRejectSingleWitErrorException()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $mockOwnerBookingService = $this->mockPartialAlternatively(BookingOwnerRequestService::class);
        $mockOwnerBookingService->shouldReceive('singleReject')
            ->andThrow(new Exception());
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea',
            'action' => ''
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    public function testRequestRejectBulkWitErrorException()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $mockOwnerBookingService = $this->mockPartialAlternatively(BookingOwnerRequestService::class);
        $mockOwnerBookingService->shouldReceive('bulkReject')
            ->andThrow(new Exception());
        
        $text =  str_random(100);
        $response = $this->actingAs($user)->call('POST', self::REQUEST_REJECT."/$room->id", [
            'reject_remark_text' => $text,
            'reject_remark' => 'textarea',
            'action' => 'bulk'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }
}