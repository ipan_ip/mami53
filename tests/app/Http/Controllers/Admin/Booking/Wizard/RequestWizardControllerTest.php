<?php

namespace App\Http\Controllers\admin\Booking\Wizard;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

class RequestWizardControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const API_URL_VALIDATE_USER_BY_PHONE_PREFIX = '/admin/booking-request-wizard/validate-user-by-phone-number/';
    const API_URL_ADD_MAMIPAY_OWNER = '/admin/booking-request-wizard/add-mamipay-owner';

    const ERR_CODE_MAMIPAY_OWNER_REGISTERED = 0;
    const ERR_CODE_MAMIPAY_OWNER_USER_NOT_FOUND = 1;
    const ERR_CODE_MAMIPAY_OWNER_NOT_REGISTERED = 2;
    const ERR_CODE_MAMIPAY_OWNER_NO_PROPERTY_OWNED = 3;

    protected function setUp() : void
    {
        parent::setUp();
    }


    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }

    private function setActingAdmin()
    {
        $this->actingAs(factory(User::class)->make());
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testCheckUserByPhoneNumberUserFoundAndMamipayOwnerShouldSuccess()
    {
        $this->setActingAdmin();
        $phoneNumber = strval(rand(1111111111,9999999999));

        $user = factory(User::class)->states('owner')->create([
            'phone_number' => $phoneNumber,
        ]);
        factory(MamipayOwner::class)->create([
            'user_id' => $user->id,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER
        ]);

        $response = $this->getJson(self::API_URL_VALIDATE_USER_BY_PHONE_PREFIX . $phoneNumber)->json();

        $this->assertEquals(true, $response['status']);
        $this->assertEquals($user->id, $response['user_id']);
        $this->assertEquals(self::ERR_CODE_MAMIPAY_OWNER_REGISTERED, $response['error_code']);
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testCheckUserByPhoneNumberUserNotFound()
    {
        $this->setActingAdmin();
        $phoneNumber = strval(rand(1111111111,9999999999));

        $response = $this->getJson(self::API_URL_VALIDATE_USER_BY_PHONE_PREFIX . $phoneNumber)->json();

        $this->assertEquals(false, $response['status']);
        $this->assertArrayNotHasKey('user_id', $response);
        $this->assertEquals(self::ERR_CODE_MAMIPAY_OWNER_USER_NOT_FOUND, $response['error_code']);
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testCheckUserByPhoneNumberUserFoundButNotMamipayOwnerYet()
    {
        $this->setActingAdmin();
        $phoneNumber = strval(rand(1111111111,9999999999));

        $user = factory(User::class)->states('owner')->create([
            'phone_number' => $phoneNumber,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER_OLD
        ]);

        $response = $this->getJson(self::API_URL_VALIDATE_USER_BY_PHONE_PREFIX . $phoneNumber)->json();

        $this->assertEquals(false, $response['status']);
        $this->assertArrayNotHasKey('user_id', $response);
        $this->assertEquals(self::ERR_CODE_MAMIPAY_OWNER_NOT_REGISTERED, $response['error_code']);
    }

    /**
     * @group UG
     * @group UG-1152
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testCheckUserByPhoneNumberUserFoundButNotHavingProperty()
    {
        $this->setActingAdmin();
        $phoneNumber = strval(rand(1111111111,9999999999));

        $user = factory(User::class)->states('owner')->create([
            'phone_number' => $phoneNumber,
        ]);

        $response = $this->getJson(self::API_URL_VALIDATE_USER_BY_PHONE_PREFIX . $phoneNumber)->json();

        $this->assertEquals(false, $response['status']);
        $this->assertArrayNotHasKey('user_id', $response);
        $this->assertEquals(self::ERR_CODE_MAMIPAY_OWNER_NO_PROPERTY_OWNED, $response['error_code']);
    }

    /**
     * @group UG
     * @group UG-1152
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testCheckUserByPhoneNumberUserFoundButNotOwningAnyProperty()
    {
        $this->setActingAdmin();
        $phoneNumber = strval(rand(1111111111,9999999999));

        $user = factory(User::class)->states('owner')->create([
            'phone_number' => $phoneNumber,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'owner_status' => 'Associated but not as an owner'
        ]);

        $response = $this->getJson(self::API_URL_VALIDATE_USER_BY_PHONE_PREFIX . $phoneNumber)->json();

        $this->assertEquals(false, $response['status']);
        $this->assertArrayNotHasKey('user_id', $response);
        $this->assertEquals(self::ERR_CODE_MAMIPAY_OWNER_NO_PROPERTY_OWNED, $response['error_code']);
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testAddMamipayOwnerCompleteRequestValidAndRegisteredPhoneNumber()
    {
        $this->setActingAdmin();

        $user = factory(User::class)->states('owner')->create([
            'phone_number' => '08980881820',
        ]);

        $response = $this->postJson(self::API_URL_ADD_MAMIPAY_OWNER, [
            'owner_phone_number' => $user->phone_number,
            'owner_name' => str_random(20),
            'bank_account_owner' => str_random(20),
            'bank_name' => str_random(10),
            'bank_account_number' => str_random(11),
            'bank_account_city' => rand(1, 20),
        ])->json();

        $this->assertEquals(true, $response['status']);
        $this->assertArrayNotHasKey('error_message', $response);
        $this->assertNotNull(MamipayOwner::where('user_id', $user->id)->first());
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testAddMamipayOwnerCompleteRequestValidButNotRegisteredPhoneNumber()
    {
        $this->setActingAdmin();

        $response = $this->postJson(self::API_URL_ADD_MAMIPAY_OWNER, [
            'owner_phone_number' => '0808800808',
            'owner_name' => str_random(20),
            'bank_account_owner' => str_random(20),
            'bank_name' => str_random(10),
            'bank_account_number' => strval(rand(1111111111,9999999999)),
            'bank_account_city' => rand(1, 20),
        ])->json();

        $this->assertEquals(false, $response['status']);
        $this->assertNotEmpty($response['error_message']);
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testAddMamipayOwnerCompleteRequestInvalidAndNotRegisteredPhoneNumber()
    {
        $this->setActingAdmin();

        $response = $this->postJson(self::API_URL_ADD_MAMIPAY_OWNER, [
            'owner_phone_number' => '9918980881820',
            'owner_name' => str_random(20),
            'bank_account_owner' => str_random(20),
            'bank_name' => str_random(10),
            'bank_account_number' => strval(rand(1111111111,9999999999)),
            'bank_account_city' => rand(1, 20),
        ])->json();

        $this->assertEquals(false, $response['status']);
        $this->assertNotEmpty($response['error_message']);
    }

    /**
     * @group UG
     * @group UG-924
     * @group App/Http/Controllers/Admin/Booking/Wizard/RequestWizardController
     */
    public function testAddMamipayOwnerCompleteRequestValidButRegisteredAsMamipayAlreadyPhoneNumber()
    {
        $this->setActingAdmin();

        $user = factory(User::class)->create([
            'phone_number' => '0808808808',
            'is_owner' => 'true',
        ]);

        factory(MamipayOwner::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->postJson(self::API_URL_ADD_MAMIPAY_OWNER, [
            'owner_phone_number' => $user->phone_number,
            'owner_name' => str_random(20),
            'bank_account_owner' => str_random(20),
            'bank_name' => str_random(10),
            'bank_account_number' => strval(rand(1111111111,9999999999)),
            'bank_account_city' => rand(1, 20),
        ])->json();

        $this->assertEquals(false, $response['status']);
        $this->assertNotEmpty($response['error_message']);
    }
}
