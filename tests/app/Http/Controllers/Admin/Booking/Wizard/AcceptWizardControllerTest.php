<?php

namespace App\Http\Controllers\Admin\Booking\Wizard;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class AcceptWizardControllerTest extends MamiKosTestCase
{
    use WithoutEvents, WithoutMiddleware;

    private const DETAIL_BOOKING_ROUTE = 'admin.booking_accept_wizard.accept'; // GET
    private const ACCEPT_BOOKING_ROUTE = 'admin.booking_accept_wizard'; // POST

    private $user;
    private $httpClient;

    protected function setUp() : void
    {
        // mock GuzzleHttp/Client
        $this->httpClient = Mockery::mock('overload:GuzzleHttp\Client', 'GuzzleHttp\ClientInterface');

        parent::setUp();

        // create user
        $this->user = factory(User::class)->create();
    }

    public function testDetailBookingNotFound()
    {
        $this->actingAs($this->user);
        // run test
        $response = $this->call('GET', 'admin/booking/users/wizard/accept/1');
        $response->assertRedirect('admin/booking/users?#booking-users');
    }

    public function testDetailBookingInvalidStatus()
    {
        // define data
        $bookingUserEntity = $this->createBookingUserEntity(['status' => BookingUser::BOOKING_STATUS_VERIFIED]);

        // get url
        $url = route(self::DETAIL_BOOKING_ROUTE, $bookingUserEntity);

        // run test
        $response = $this->actingAs($this->user)->get($url);
        $response->assertStatus(302);
        $response->assertRedirect('/admin/booking/users?#booking-users');
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testDetailBookingSuccess()
    {
        // define data
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity(['designer_id' => $roomEntity->id]);
        $bookingUserEntity = $this->createBookingUserEntity([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_designer_id' => $bookingDesignerEntity->id
        ]);
        $this->app->instance(BookingUser::class, $bookingUserEntity);

        //get url
        $url = route(self::DETAIL_BOOKING_ROUTE, $bookingUserEntity);

        // run test
        $response = $this->actingAs($this->user)->get($url);

        $bookingRetrive = $response->getOriginalContent()->getData()['booking'];

        $response->assertOk();
        $response->assertViewIs('admin.contents.booking.wizard.user.accept_detail');
        $response->assertViewHas('contentHeader', 'Harga Sewa dan Tanggal Penagihan Tetap (Prorata)');
        $response->assertViewHas('steps', $this->getNumberSteps());
        $this->assertInstanceOf(BookingUser::class, $bookingRetrive);
        $this->assertEquals($bookingUserEntity->id, $bookingRetrive->id);
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testAcceptBookingFailed()
    {
        // define data
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity(['designer_id' => $roomEntity->id]);
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $this->user->id,
            'designer_id' => $roomEntity->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_designer_id' => $bookingDesignerEntity->id
        ]);
        $this->app->instance(BookingUser::class, $bookingUserEntity);

        //get url
        $url = route(self::DETAIL_BOOKING_ROUTE, $bookingUserEntity);

        // mock guzzle
        $guzzleBody = [
            'status' => false,
            'meta' => [
                'message' => 'Mocking error http client'
            ]
        ];
        $guzzleBody = (object) $guzzleBody;
        $guzzleResponse = new Response(200, ['Content-Type' => 'application/json'], json_encode($guzzleBody));
        $this->httpClient->shouldReceive('post')->andReturn($guzzleResponse);

        // run test
        $response = $this->actingAs($this->user)->post($url, ['rent_type' => 'monthly']);
        $response->assertRedirect('/', $response);
        $response->assertSessionHas('error_message', 'Mocking error http client');
        $response->assertStatus(302);
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testAcceptBookingSuccess()
    {
        // define data
        $roomEntity = $this->createRoomEntity();
        $bookingDesignerEntity = $this->createBookingDesignerEntity(['designer_id' => $roomEntity->id]);
        $bookingUserEntity = $this->createBookingUserEntity([
            'user_id' => $this->user->id,
            'designer_id' => $roomEntity->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_designer_id' => $bookingDesignerEntity->id
        ]);
        $this->app->instance(BookingUser::class, $bookingUserEntity);

        //get url
        $url = route(self::DETAIL_BOOKING_ROUTE, $bookingUserEntity);

        // mock guzzle
        $guzzleBody = [
            'status' => true,
        ];
        $guzzleBody = (object) $guzzleBody;
        $guzzleResponse = new Response(200, ['Content-Type' => 'application/json'], json_encode($guzzleBody));
        $this->httpClient->shouldReceive('post')->andReturn($guzzleResponse);

        // run test
        $response = $this->actingAs($this->user)->post($url, ['rent_type' => 'monthly']);
        $response->assertRedirect('/admin/booking/users?#booking-users', $response);
        $response->assertSessionHas('message', 'Booking berhasil diterima');
        $response->assertStatus(302);
    }

    private function createBookingUserEntity(array $overrideData = []): BookingUser
    {
        return factory(BookingUser::class)->create($overrideData);
    }

    private function createBookingDesignerEntity(array $overrideData = []): BookingDesigner
    {
        return factory(BookingDesigner::class)->create($overrideData);
    }

    private function createRoomEntity(array $overrideData = []): Room
    {
        $room = factory(Room::class)->create($overrideData);
        $room->song_id = $room->id;
        $room->save();

        return $room;
    }

    /**
     * This function copied from App\Http\Controllers\admin\Booking\Wizard\AcceptWizardController
     * @return array
     */
    private function getNumberSteps(): array
    {
        $steps = [];
        for ($x=1; $x <=4; $x++)
        {
            $steps[]=['title'=>$x];
        }
        return $steps;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }
}