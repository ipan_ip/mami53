<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Notif\NotifData;
use App\Entities\User\UserMedia;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use Mockery;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\User\UserVerificationAccount;
use App\Libraries\Notifications\SendNotifWrapper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserIdentityCardControllerTest extends MamikosTestCase
{
    use WithoutMiddleware;

    private const VERIFICATION_ACCOUNT_ADMIN_PAGE_URL = 'admin/account/verification-identity-card';
    private $user;

    protected function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->withoutExceptionHandling();
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardAssertOk()
    {
        $this->actingAs($this->user);
        factory(UserVerificationAccount::class)->make([
            'user_id' => $this->user->id
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL);
        $response->assertOk();
        $response->assertViewIs('admin.contents.user.list-verification-identity');
    }

    /**
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardHasViewIsSuccess()
    {
        $this->actingAs($this->user);
        factory(UserVerificationAccount::class)->make([
            'user_id' => $this->user->id
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL);
        $response->assertViewIs('admin.contents.user.list-verification-identity');
    }

    /**
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithParamUserProfileSuccess()
    {
        $name = str_random(16) . ' ' . str_random(10);
        $user = factory(User::class)->create([
            'name' => $name,
        ]);
        $this->actingAs($user);

        factory(UserVerificationAccount::class)->make([
            'user_id' => $user->id
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL, ['q' => $name]);
        $response->assertViewIs('admin.contents.user.list-verification-identity');
    }

    /**
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithViewHasData()
    {
        $name = str_random(16) . ' ' . str_random(10);
        $user = factory(User::class)->create([
            'name' => $name,
        ]);
        $this->actingAs($user);

        factory(UserVerificationAccount::class)->make([
            'user_id' => $user->id
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL, ['q' => $name]);
        $response->assertViewHas('datas');
        $response->assertViewHas('boxTitle');
    }

    /**
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithAssertSeeText()
    {
        $name = str_random(16) . ' ' . str_random(10);
        $user = factory(User::class)->create([
            'name' => $name,
        ]);
        $this->actingAs($user);

        factory(UserVerificationAccount::class)->make([
            'user_id' => $user->id
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL, ['q' => $name]);
        $response->assertSeeText('records per page');
        $response->assertSeeText('Verification Account');
    }

    private function setActingUser()
    {
        $name = str_random(16) . ' ' . str_random(10);
        $user = factory(User::class)->make([
            'name' => $name,
        ]);
        $this->actingAs($user);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterSubmitDate()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'created_at' => Carbon::create(2019, 01, 01, 12, 0)
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
            'created_at' => Carbon::create(2019, 01, 05, 12, 0)
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_submit_date=2019-01-01&to_submit_date=2019-01-02');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterSubmitDateInclusive()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'created_at' => Carbon::create(2019, 01, 01, 12, 0)
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'created_at' => Carbon::create(2019, 01, 05, 12, 0)
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_submit_date=2019-01-01&to_submit_date=2019-01-05');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(2, count($userVerifications));
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterSubmitDateFromShouldHasValidRange()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'created_at' => Carbon::create(2019, 01, 01, 12, 0)
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'created_at' => Carbon::create(2019, 01, 05, 12, 0)
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_submit_date=2019-01-02&to_submit_date=2019-01-01');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(2, count($userVerifications)); // Not filtered
        $this->assertNull($this->app->request->input('from_submit_date'));
        $this->assertNull($this->app->request->input('to_submit_date'));
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterVerificationDate()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
            'updated_at' => Carbon::create(2019, 01, 01, 12, 0),
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_verification_date=2019-01-01&to_verification_date=2019-01-02');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterVerificationDateShouldInclusive()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
            'updated_at' => Carbon::create(2019, 01, 01, 12, 0),
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_verification_date=2019-01-01&to_verification_date=2019-01-01');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterVerificationDateShouldHasValidRange()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
            'updated_at' => Carbon::create(2019, 01, 02, 12, 0),
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?from_verification_date=2019-01-02&to_verification_date=2019-01-01');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(2, count($userVerifications)); // Not filtered
        $this->assertNull($this->app->request->input('from_verification_date'));
        $this->assertNull($this->app->request->input('to_verification_date'));
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterIsOwner()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create([
            'is_owner' => 'true',
        ]);
        $excludedUserRequest = factory(User::class)->create([
            'is_owner' => 'false',
        ]);
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?is_owner=true');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterIsOwnerAndQuery()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create([
            'is_owner' => 'true',
            'name' => 'Arjuna'
        ]);
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?is_owner=false&q=juna');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(0, count($userVerifications));
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterStatusVerified()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_CANCELLED
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?status=VERIFIED');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterIdentityType()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
        ]);
        factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'type' => 'e_ktp'
        ]);
        factory(UserMedia::class)->create([
            'user_id' => $excludedUserRequest,
            'type' => 'passport'
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?identity_type=e_ktp');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardFilterUploadFrom()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $excludedUserRequest = factory(User::class)->create();
        $includedVerificationAccountRequest = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $excludedUserRequest->id,
        ]);
        factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'from' => 'booking'
        ]);
        factory(UserMedia::class)->create([
            'user_id' => $excludedUserRequest,
            'from' => 'user_profile'
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?upload_from=booking');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $this->assertEquals($includedVerificationAccountRequest->id, $userVerifications[0]->id);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithMultipleIdentityShouldShowLatestIdentity()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        $excludedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'type' => 'e_ktp',
            'file_path' => '/path/to',
            'file_name' => 'excluded-identity.jpg',
        ]);
        $excludedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'type' => 'e_ktp',
            'file_path' => '/path/to',
            'file_name' => 'excluded-selfie.jpg',
        ]);
        $includedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'type' => 'passport',
            'file_path' => '/path/to',
            'file_name' => 'included-identity.jpg',
        ]);
        $includedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'type' => 'passport',
            'file_path' => '/path/to',
            'file_name' => 'included-selfie.jpg',
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL);
        $response->assertDontSee($excludedIdentityPhoto->getMediaUrl()['small']);
        $response->assertDontSee($excludedSelfiePhoto->getMediaUrl()['small']);
        $response->assertSee($includedIdentityPhoto->getMediaUrl()['small']);
        $response->assertSee($includedSelfiePhoto->getMediaUrl()['small']);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithMultipleIdentityFilteredByOldIdentityShouldBeExcluded()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        $excludedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'from' => 'booking',
        ]);
        $excludedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'from' => 'booking',
        ]);
        $includedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'from' => 'user_profile',
        ]);
        $includedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'from' => 'user_profile',
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?upload_from=booking');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(0, count($userVerifications));
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testGetVerifyIdentityCardWithMultipleIdentityFilteredByLatestIdentityShouldBeIncluded()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create([
            'name' => 'A name that should be printed in view.'
        ]);
        factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
        ]);
        $excludedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'from' => 'booking',
        ]);
        $excludedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'from' => 'booking',
        ]);
        $includedIdentityPhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'photo_identity',
            'from' => 'user_profile',
        ]);
        $includedSelfiePhoto = factory(UserMedia::class)->create([
            'user_id' => $userRequest->id,
            'description' => 'selfie_identity',
            'from' => 'user_profile',
        ]);

        $response = $this->get(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . '?upload_from=user_profile');
        $userVerifications = $response->viewData('datas');
        $this->assertEquals(1, count($userVerifications));
        $response->assertSee($userRequest->name);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testVerifyIdentityCardRejectionWithoutReasonShouldRedirectWithError()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING
        ]);

        $response = $this->followingRedirects()->post(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . "/{$userVerificationAccount->id}", [
            'status' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
        ]);

        $response->assertSessionHas('error_message');
        $response->assertSessionMissing('messages');

        // Data should not changed
        $this->assertEquals(UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING, UserVerificationAccount::find($userVerificationAccount->id)->identity_card);
    }

    /**
     * @group UG
     * @group UG-651
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testVerifyIdentityCardRejectionWithReasonShouldRedirectWithoutError()
    {
        $this->setActingUser();

        $userRequest = factory(User::class)->create();
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $userRequest->id,
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING
        ]);

        $response = $this->followingRedirects()->post(self::VERIFICATION_ACCOUNT_ADMIN_PAGE_URL . "/{$userVerificationAccount->id}", [
            'status' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => str_random(20),
        ]);

        $response->assertSessionHas('messages');
        $response->assertSessionMissing('error_message');

        // Data should changed
        $this->assertEquals(UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED, UserVerificationAccount::find($userVerificationAccount->id)->identity_card);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateVerified()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.verified.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.verified.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.verified.body'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateCancelled()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_CANCELLED,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.cancelled.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.cancelled.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.cancelled.body'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateRejectedOtherReason()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => 'Not predefined reason',
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.rejected.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.rejected.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.rejected.body.other'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateRejectedNullReason()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => null,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.rejected.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.rejected.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.rejected.body.other'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateRejectedInvalidData()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => UserVerificationAccount::REJECT_REASON_DATA_INVALID,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.rejected.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.rejected.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.rejected.body.Data Tidak Valid'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateRejectedImageUnclear()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => UserVerificationAccount::REJECT_REASON_IMAGE_UNCLEAR,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.rejected.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.rejected.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.rejected.body.Gambar Tidak Jelas'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1244
     * @group App/Http/Controllers/Admin/UserIdentityCardController
     */
    public function testNotificationAfterUpdateRejectedImageRejected()
    {
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
            'reject_reason' => UserVerificationAccount::REJECT_REASON_IMAGE_REJECTED,
        ]);
        $this->mockSendNotifToUserIds(function (NotifData $notifData) {
            $this->assertEquals(__('notification.user-identity.rejected.title'), $notifData->title, "wrong title $notifData->title");
            $this->assertEquals(__('notification.user-identity.rejected.scheme'), $notifData->scheme, "wrong scheme $notifData->scheme");
            $this->assertEquals(__('notification.user-identity.rejected.body.Gambar Ditolak'), $notifData->message, "wrong body $notifData->message");
            return true;
        });
        $result = $this->invokeNotifVerify($userVerificationAccount);
        $this->assertTrue($result);
    }

    private function mockSendNotifToUserIds(callable $callable)
    {
        $this->mock(SendNotifWrapper::class, function ($mock) use ($callable) {
            $mock->shouldReceive('sendToUserIds')
                ->with(Mockery::on($callable), Mockery::any(), Mockery::any())
                ->once()
                ->andReturn(true);
        });
    }

    private function invokeNotifVerify(UserVerificationAccount $userVerificationAccount)
    {
        return $this->getNonPublicMethodFromClass(UserIdentityCardController::class, 'notifVerify')
            ->invokeArgs(app()->make(UserIdentityCardController::class), [$userVerificationAccount]);
    }
}
