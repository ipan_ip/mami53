<?php

namespace app\Http\Controllers\Admin\Agent;

use App\Entities\Property\Property;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AgentControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const LIST_ROOM_PROPERTIES_URL = 'admin/room/agent/properties';
    private const VERIFY_URL = 'admin/room/agent/verify';
    private const CHECK_PROPERTY_NAME_URL = 'admin/room/agent/check-property-name';
    private const ADD_PROPERTY_URL = 'admin/room/agent/property';
    private const CHECK_UNIT_TYPE_URL = 'admin/room/agent/check-unit-type';
    private const GET_DETAIL_PROPEPRTY = 'admin/room/agent/property';

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithInvalidRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $roomId = 0;

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$roomId", []);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Room tidak ditemukan',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithoutRoomOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", []);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithoutOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", []);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithRequestNameIsEmpty()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", []);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Tipe Kamar tidak boleh kosong',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithSameUnitTypeIgnoreCaseSensitiveAndSamePropertyIdAndDifferentRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();

        $duplicateName = 'test duplicate unit type';
        $property = factory(Property::class)->create([
            'name' => 'test name',
        ]);
        $room = factory(Room::class)->create([
            'property_id' => $property->id,
            'unit_type' => null,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $room1 = factory(Room::class)->create([
            'property_id' => $property->id,
            'unit_type' => $duplicateName,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room1->id,
        ]);

        $data = [
            'name' => strtoupper($duplicateName),
            'id_property' => $property->id,
        ];

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Tipe Kamar tidak boleh sama',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithSameUnitTypeIgnoreCaseSensitiveAndSamePropertyId()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();

        $duplicateName = 'test duplicate unit type';
        $property = factory(Property::class)->create([
            'name' => 'test name',
        ]);
        $room = factory(Room::class)->create([
            'property_id' => $property->id,
            'unit_type' => $duplicateName,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $data = [
            'name' => strtoupper($duplicateName),
            'id_property' => $property->id,
        ];

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", $data);

        $result = $response->json();

        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckUnitTypeWithSameUnitTypeIgnoreCaseSensitiveAndDifferentPropertyId()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();

        $duplicateName = 'test duplicate unit type';
        $property1 = factory(Property::class)->create([
            'name' => 'test name 1',
        ]);

        $room = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $room1 = factory(Room::class)->create([
            'unit_type' => $duplicateName,
            'property_id' => $property1->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room1->id,
        ]);

        $property2 = factory(Property::class)->create([
            'name' => 'test name 1',
        ]);

        $room2 = factory(Room::class)->create([
            'unit_type' => 'different property id',
            'property_id' => $property2->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room2->id,
        ]);

        $data = [
            'name' => strtoupper($duplicateName),
            'id_property' => $property2->id,
        ];

        $response = $this->actingAs($user)->call('GET', self::CHECK_UNIT_TYPE_URL . "/$room->id", $data);

        $result = $response->json();

        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithInvalidRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $roomId = 0;

        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$roomId", []);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Room tidak ditemukan',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithoutRoomOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();

        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$room->id", []);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithoutOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$room->id", []);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithRequestNameIsEmpty()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$room->id", []);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Nama Properti tidak boleh kosong',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithSameName()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $duplicateName = 'test duplicate name';
        factory(Property::class)->create([
            'name' => $duplicateName,
            'owner_user_id' => $owner->id
        ]);
        $data = [
            'name' => $duplicateName,
        ];
        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$room->id", $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Anda sudah memiliki nama properti yang sama',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testCheckPropertyNameWithDifferentName()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $duplicateName = 'test duplicate name';
        factory(Property::class)->create([
            'name' => $duplicateName,
            'owner_user_id' => $owner->id
        ]);
        $data = [
            'name' => 'test name',
        ];
        $response = $this->actingAs($user)->call('GET', self::CHECK_PROPERTY_NAME_URL . "/$room->id", $data);

        $result = $response->json();

        $response->assertOk();
        $this->assertTrue($result['success']);
    }

    /**
    * @group PMS-495
    */    
    public function testAddRoomPropertyWithInvalidRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $roomId = 0;

        $data = [
            'id' => $roomId
        ];
        $response = $this->actingAs($user)->call('POST', self::ADD_PROPERTY_URL, $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Room tidak ditemukan',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testAddRoomPropertyWithRequestNameIsEmpty()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $data = [
            'id' => $room->id
        ];

        $response = $this->actingAs($user)->call('POST', self::ADD_PROPERTY_URL, $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Nama Properti Tidak Boleh Kosong',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testAddRoomPropertyWithoutRoomOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();

        $data = [
            'id' => $room->id,
            'name' => 'test add property',
        ];
        $response = $this->actingAs($user)->call('POST', self::ADD_PROPERTY_URL, $data);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
        $this->assertEquals($result['messages'], 'Nama Properti berhasil ditambahkan.');

        $this->assertDatabaseHas('designer_owner', [
            'designer_id' => $room->id,
        ]);
        $this->assertInstanceOf(User::class, $room->owners()->first()->user);
    }

    /**
    * @group PMS-495
    */    
    public function testAddRoomPropertyWithOwner()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        $data = [
            'id' => $room->id,
            'name' => 'test add property',
        ];
        $response = $this->actingAs($user)->call('POST', self::ADD_PROPERTY_URL, $data);

        $result = $response->json();
        $response->assertOk();
        $this->assertTrue($result['success']);
        $this->assertEquals($result['messages'], 'Nama Properti berhasil ditambahkan.');
    }

    /**
    * @group PMS-495
    */    
    public function testGetRoomPropertyWithSelectedPropertyId()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $properties = factory(Property::class, 5)->create([
            'owner_user_id' => $owner->id
        ]);
        $room = factory(Room::class)->create([
            'property_id' => $properties[0]['id'],
            'unit_type' => 'test unit type',
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->actingAs($user)->call('GET', self::LIST_ROOM_PROPERTIES_URL . "/$room->id", []);

        $result = $response->json();

        $response->assertOk();
        $this->assertTrue($result['success']);
        $this->assertEquals($result['selected_property'], $properties[0]['id']);
        $this->assertEquals($result['unit_type'], $room->unit_type);
        $this->assertEquals($result['data'][0]['id'], $properties[0]['id']);
        $this->assertEquals($result['data'][1]['id'], $properties[1]['id']);
        $this->assertEquals($result['data'][2]['id'], $properties[2]['id']);
        $this->assertEquals($result['data'][3]['id'], $properties[3]['id']);
        $this->assertEquals($result['data'][4]['id'], $properties[4]['id']);
    }

    /**
    * @group PMS-495
    */    
    public function testGetRoomPropertyWithPropertyIdAndUnitTypeAreNull()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $properties = factory(Property::class, 5)->create([
            'owner_user_id' => $owner->id
        ]);
        $room = factory(Room::class)->create([
            'property_id' => null,
            'unit_type' => null,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
        ]);

        $response = $this->actingAs($user)->call('GET', self::LIST_ROOM_PROPERTIES_URL . "/$room->id", []);

        $result = $response->json();

        $response->assertOk();
        $this->assertTrue($result['success']);
        $this->assertEquals($result['selected_property'], $room->property_id);
        $this->assertEquals($result['unit_type'], $room->unit_type);
        $this->assertEquals($result['data'][0]['id'], $properties[0]['id']);
        $this->assertEquals($result['data'][1]['id'], $properties[1]['id']);
        $this->assertEquals($result['data'][2]['id'], $properties[2]['id']);
        $this->assertEquals($result['data'][3]['id'], $properties[3]['id']);
        $this->assertEquals($result['data'][4]['id'], $properties[4]['id']);
    }

    /**
    * @group PMS-495
    */    
    public function testAgentVerifyPropertyWithInvalidRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $roomId = 0;

        $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$roomId", []);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Room tidak ditemukan',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testAgentVerifyPropertyWithPropertyIdIsEmpty()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();

        $data = [
            'unit_type' => 'test unit type'
        ];
        $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Nama Properi Tidak Boleh Kosong',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testAgentVerifyPropertyWithInvalidIdProperty()
    {
        $user = factory(User::class)->state('admin')->make();
        $room = factory(Room::class)->create();

        $data = [
            'unit_type' => 'test unit type',
            'property_id' => -1,
        ];
        $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Pilih Nama Properti yang valid',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    /**
    * @group PMS-495
    */    
    public function testAgentVerifyPropertyWithSameUnitTypeWithDifferentRoomId()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $property = factory(Property::class)->create([
            'name' => 'test name',
            'owner_user_id' => $owner->id,
        ]);

        $room = factory(Room::class)->create();

        $duplicateName = 'test duplicate unit type';
        factory(Room::class)->create([
            'property_id' => $property->id,
            'unit_type' => $duplicateName
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $data = [
            'unit_type' => strtoupper($duplicateName),
            'property_id' => $property->id,
        ];
        $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'messages' => 'Tipe Kamar tidak boleh sama',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }

    // public function testAgentVerifyPropertyWithSameUnitTypeWithSameRoomId()
    // {
    //     $user = factory(User::class)->state('admin')->make();
    //     $owner = factory(User::class)->state('owner')->create();
    //     $property = factory(Property::class)->create([
    //         'name' => 'test name',
    //         'owner_user_id' => $owner->id,
    //     ]);

    //     $duplicateName = 'test duplicate unit type';
    //     $room = factory(Room::class)->create([
    //         'unit_type' => $duplicateName,
    //         'property_id' => $property->id,
    //     ]);

    //     factory(RoomOwner::class)->create([
    //         'user_id' => $owner->id,
    //         'designer_id' => $room->id,
    //     ]);

    //     $data = [
    //         'unit_type' => strtoupper($duplicateName),
    //         'property_id' => $property->id,
    //         'has_multiple_type' => 1,
    //     ];
    //     $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

    //     $result = $response->json();
    //     dd($result);

    //     $expectedResult = [
    //         'success' => false,
    //         'messages' => 'Mohon lengkapi data foto kos dan pastikan telah sesuai',
    //     ];

    //     $response->assertOk();
    //     $this->assertFalse($result['success']);
    //     $this->assertEquals($expectedResult['messages'], $result['messages']);
    // }

    // public function testAgentVerifyPropertyWithThereIsPhotoIsNotGrouped()
    // {
    //     $user = factory(User::class)->state('admin')->make();
    //     $owner = factory(User::class)->state('owner')->create();
    //     $property = factory(Property::class)->create([
    //         'name' => 'test name',
    //         'owner_user_id' => $owner->id,
    //     ]);

    //     $duplicateName = 'test duplicate unit type';
    //     $room = factory(Room::class)->create([
    //         'unit_type' => $duplicateName,
    //         'property_id' => $property->id,
    //         'photo_count' => 1,
    //     ]);

    //     factory(Card::class)->create([
    //         'designer_id' => $room->id,
    //         'type' => 'image',
    //         'group' => null,
    //     ]);
    //     factory(RoomOwner::class)->create([
    //         'user_id' => $owner->id,
    //         'designer_id' => $room->id,
    //     ]);

    //     $data = [
    //         'unit_type' => strtoupper($duplicateName),
    //         'property_id' => $property->id,
    //         'has_multiple_type' => 1,
    //     ];
    //     $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

    //     $result = $response->json();

    //     $expectedResult = [
    //         'success' => false,
    //         'messages' => 'Ada Foto yang belum digrup silahkan Atur Foto terlebih dahulu',
    //     ];

    //     $response->assertOk();
    //     $this->assertFalse($result['success']);
    //     $this->assertEquals($expectedResult['messages'], $result['messages']);
    // }

    /**
    * @group PMS-495
    */    
    public function testGetDetailPropertyWithIdPropertyNotFound()
    {
        $user = factory(User::class)->state('admin')->make();

        $response = $this->actingAs($user)->call('GET', self::GET_DETAIL_PROPEPRTY . "/0");

        $response->assertOk();
        $this->assertEmpty($response->content());
    }

    /**
    * @group PMS-495
    */    
    public function testGetDetailPropertyWithValidIdProperty()
    {
        $user = factory(User::class)->state('admin')->make();

        $property = factory(Property::class)->create();

        $response = $this->actingAs($user)->call('GET', self::GET_DETAIL_PROPEPRTY . "/$property->id");

        $result = $response->json();
        $response->assertOk();
        $this->assertNotEmpty($response->content());
        $this->assertEquals($property->id, $result['id']);
    }

    /**
    * @group PMS-495
    */    
    public function testAgentVerifyPropertyWithUnityTypeIsNotEmptyAndCentangIsNotActive()
    {
        $user = factory(User::class)->state('admin')->make();
        $owner = factory(User::class)->state('owner')->create();
        $property = factory(Property::class)->create([
            'name' => 'test name',
            'owner_user_id' => $owner->id,
        ]);

        $duplicateName = 'test duplicate unit type';
        $room = factory(Room::class)->create([
            'unit_type' => $duplicateName,
            'property_id' => $property->id,
            'photo_count' => 1,
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $data = [
            'unit_type' => strtoupper($duplicateName),
            'property_id' => $property->id,
            'has_multiple_type' => 0,
        ];
        $response = $this->actingAs($user)->call('POST', self::VERIFY_URL . "/$room->id", $data);

        $result = $response->json();

        $expectedResult = [
            'success' => false,
            'messages' => 'Jika ingin mengisi Tipe Kamar maka Centang Harus diaktifkan',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['messages'], $result['messages']);
    }
}
