<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Booking\BookingReject;
use App\User;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BookingRejectReasonControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const REJECT_BOOKING = 'admin/booking/reject-reason/';
    private const CREATE_REJECT_BOOKING = 'admin/booking/reject-reason/create';


    public function testGetListRejectBookingIsSuccess(): void
    {
        $this->loginAsUser();

        $this->get(self::REJECT_BOOKING)->assertViewHas('rejectReasons');
    }

    public function testCreateRejectReason(): void
    {
        $response = $this->loginAsUser()->get(self::CREATE_REJECT_BOOKING);

        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.booking.reject-reason.form');
    }

    public function testStoreRejectReasonSuccess(): void
    {
        $rejectReason = [
            'description' => 'hi',
            'is_active' => 1,
            'is_affecting_acceptance' => 1,
            'make_kost_not_available' => 0,
            'type' => 'reject'
        ];


        $response = $this->post(self::REJECT_BOOKING, $rejectReason);

        $response->assertStatus(302);
    }

    public function testStoreRejectReasonFailedOnDescription(): void
    {
        $rejectReason = [
            'description' => '',
            'is_active' => 1,
            'is_affecting_acceptance' => 1,
            'make_kost_not_available' => 0,
            'type' => 'reject'
        ];

        $response = $this->post(self::REJECT_BOOKING, $rejectReason);

        $response->assertSessionHasErrors();
    }

    public function testEditRejectReason()
    {
        $rejectReason = factory(BookingReject::class)->create();

        $response = $this->loginAsUser()->get(self::REJECT_BOOKING . $rejectReason->id . '/edit');
        $response->assertSuccessful();
        $response->assertViewIs('admin.contents.booking.reject-reason.form');
    }

    public function testUpdateRejectReason()
    {
        $rejectReason = factory(BookingReject::class)->create();

        $description = 'Kamar Vento Lagi Dibenerin';

        $rejectReason->description = $description;

        $response = $this->put(self::REJECT_BOOKING . $rejectReason->id);

        $response->assertStatus(302);

        $this->assertEquals($description, $rejectReason->description);
    }

    public function testDeleteRejectReason()
    {
        $rejectReason = factory(BookingReject::class)->create();

        $response = $this->delete(self::REJECT_BOOKING . $rejectReason->id);

        $response->assertStatus(302);
    }

    public function loginAsUser()
    {
        $user = factory(User::class)->make();
        return $this->actingAs($user);
    }
}