<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Activity\Action;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CardControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const PREVIEW_IMAGE_URL = '/admin/card/preview/';
    private const STORE_CARD_URL = 'admin/card/store/';
    private const DELETE_CARD_URL = 'admin/card/delete/';
    private const UPDATE_CARD_URL = 'admin/card/update/';
    private const UPDATE_PHOTO_GROUP_URL = 'admin/card/update-photo-group';

    public function testGetPreviewSuccessRedirect()
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create();

        $card = factory(Card::class)->create(
            [
                'designer_id' => $room->id,
                'photo_id' => $media->id
            ]
        );

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::PREVIEW_IMAGE_URL . $card->id);

        $response->assertRedirect($media->getMediaUrl()['real']);
        $this->assertEquals(302, $response->status());
    }

    public function testGetPreviewIsNotRedirecting()
    {
        $room = factory(Room::class)->create();

        $card = factory(Card::class)->create(
            [
                'designer_id' => $room->id,
                'photo_id' => 999
            ]
        );

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::PREVIEW_IMAGE_URL . $card->id);

        //because there is no media/photo, it should not redirect
        $this->assertNotEquals(302, $response->status());
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithEmptyCardsParams()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, []);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithTypeImageAndInvalidPhotoGroup()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => 'invalid-photo-group',
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Select Valid Photo Group');
    }

    /**
    * @group PMS-495
    */     
    public function testStoreCardWithPhotoIdIsEmpty()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'photo_id' => '',
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Photo has not been uploaded yet');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithTypeImageAndValidPhotoGroup()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'New Card Created');
        $this->assertDatabaseHas('designer_style', [
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX[$data['cards'][0]['group']],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_count' => 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithTypeIsNotImage()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'text',
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'New Card Created');
        $this->assertDatabaseHas('designer_style', [
            'type' => $data['cards'][0]['type'],
            'description' => $data['cards'][0]['description'],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_count' => 0,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithoutDescriptionField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: description');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithoutSourceField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: source');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithoutOrderingField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: ordering');
    }

    /**
    * @group PMS-495
    */    
    public function testStoreCardWithoutUrlOriField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('POST', self::STORE_CARD_URL . $room->id, $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: url_ori');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithEmptyCardsParam()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $card = factory(Card::class)->create();

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", []);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Aborted');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithTypeImageAndInvalidPhotoGroup()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $card = factory(Card::class)->create();

        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => 'invalid-photo-group',
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.create', $room->id));
        $response->assertSessionHas('error_message', 'Select Valid Photo Group');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithoutDescriptionField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $card = factory(Card::class)->create();

        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.edit', [$room->id, $card->id]));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: description');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithoutSourceField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $card = factory(Card::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.edit', [$room->id, $card->id]));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: source');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithoutOrderingField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $photo = factory(Media::class)->create();
        $card = factory(Card::class)->create();

        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'photo_id' => $photo->id,
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.edit', [$room->id, $card->id]));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: ordering');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithoutUrlOriField()
    {
        $user = factory(User::class)->make();
        $room = factory(Room::class)->create();
        $card = factory(Card::class)->create();
        $photo = factory(Media::class)->create();
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.edit', [$room->id, $card->id]));
        $response->assertSessionHas('error_message', 'Aborted. Undefined index: url_ori');
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithIsCoverIsTrueAndGroupIsNotInsideRoom()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX[$data['cards'][0]['group']],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
            'is_cover' => false,
        ]);

        $this->assertDatabaseMissing('designer',[
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_count' => 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithIsCoverIsTrueAndGroupIsInsideRoom()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => Group::INSIDE_ROOM,
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
            'is_cover' => true,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
            'photo_count' => 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardsTypeImageWithRoomPhotoIdIsEqualsToCardPhotoIdAndGroupIsNotInsideRoom()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $photo->id,
            'photo_count' => 1,
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX[$data['cards'][0]['group']],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
            'is_cover' => false,
        ]);

        $this->assertDatabaseMissing('designer',[
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_count' => 1,
        ]);
    }

    public function testUpdateCardsTypeImageWithRoomPhotoIdIsEqualsToCardPhotoIdAndGroupIsInsideRoom()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $photo->id,
            'photo_count' => 1,
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => Group::INSIDE_ROOM,
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
            'is_cover' => true,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
            'photo_count' => 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithTypeImageAndValidPhotoGroupWithActiveCoverIsFalse()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1,
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::INSIDE_ROOM,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX[$data['cards'][0]['group']],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
        ]);

        $this->assertDatabaseMissing('designer', [
            'id' => $room->id,
            'photo_id' => $photo->id,
        ]);

        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'photo_count' => 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdatePhotoGroupWithoutIdAndGroupParams()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_PHOTO_GROUP_URL, []);
        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'message' => 'The id field is required., Foto tidak ditemukan., The group field is required.',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['message'], $result['message']);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdatePhotoGroupTypeImageWithIsCoverIsTrueAndGroupIsNotInsideRoom()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create();
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM
        ]);
        $data = [
            'id' => $card->id,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ];

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_PHOTO_GROUP_URL, $data);

        $response->assertOk();
        $this->assertEquals('Tidak bisa merubah grup dari foto cover.', $response->json()['message']);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdatePhotoGroupWithInvalidIdAndGroup()
    {
        $data = [
            'id' => rand(1, 100),
            'group' => Group::BUILDING_FRONT,
        ];

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_PHOTO_GROUP_URL, $data);
        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'message' => 'The selected id is invalid., Foto tidak ditemukan.',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['message'], $result['message']);
        $this->assertDatabaseMissing('designer_style', $data);
        $this->assertDatabaseMissing('activity_log', [
            'log_name' => Action::KOST_PHOTO,
            'description' => "Update Kost Photo - $data[id]"
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdatePhotoGroupWithErrorException()
    {
        $data = [
            'id' => rand(1, 100),
            'group' => Group::BUILDING_FRONT,
            'type' => $this->faker->randomElement(['video_file', 'video', 'gif', 'text', 'webview_html', 'webview_url', 'link_webview', 'link', 'quiz', 'faisal', 'vote']),
        ];

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_PHOTO_GROUP_URL, $data);
        $result = $response->json();
        $expectedResult = [
            'success' => false,
            'message' => 'The selected id is invalid., Foto tidak ditemukan.',
        ];

        $response->assertOk();
        $this->assertFalse($result['success']);
        $this->assertEquals($expectedResult['message'], $result['message']);
        $this->assertDatabaseMissing('designer_style', $data);
        $this->assertDatabaseMissing('activity_log', [
            'log_name' => Action::KOST_PHOTO,
            'description' => "Update Kost Photo - $data[id]"
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testDeleteCardWithTypeImageWithPhotoCountGreaterThan0()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => false,
            'group' => Group::INSIDE_ROOM
        ]);

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::DELETE_CARD_URL."$room->id/$card->id");

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style deleted');
        $this->assertSoftDeleted('designer_style', [
            'id' => $card->id,
            'designer_id' => $card->designer_id,
            'photo_id' => $card->photo_id,
            'type' => 'image',
            'is_cover' => false,
            'group' => $card->group,
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => $room->photo_count - 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testDeleteCardWithTypeImageWithPhotoCountIsNotGreaterThan0()
    {
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => false,
            'group' => Group::INSIDE_ROOM
        ]);

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::DELETE_CARD_URL."$room->id/$card->id");

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style deleted');
        $this->assertSoftDeleted('designer_style', [
            'id' => $card->id,
            'designer_id' => $card->designer_id,
            'photo_id' => $card->photo_id,
            'type' => 'image',
            'is_cover' => false,
            'group' => $card->group,
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => 0,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testDeleteCardWithTypeIsNotImage()
    {
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type' => 'image',
        ]);

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::DELETE_CARD_URL."$room->id/$card->id");

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style deleted');
        $this->assertSoftDeleted('designer_style', [
            'id' => $card->id,
            'designer_id' => $card->designer_id,
            'type' => 'image',
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => 0,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testDeleteCardWithInvalidCardId()
    {
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);

        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->call('GET', self::DELETE_CARD_URL."$room->id/0");

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Not Found');

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => 0,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithChangeTypeImageToOtherTypeWithPhotoCountIsGreaterThan0()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'text',
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'description' => $data['cards'][0]['description'],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => $room->photo_count - 1,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithChangeTypeImageToOtherTypeWithPhotoCountIsNotGreaterThan0()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 0
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $photo->id,
            'type' => 'image',
            'is_cover' => true,
            'group' => $this->faker->randomElement([
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER,
            ])
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'text',
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'description' => $data['cards'][0]['description'],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => $room->photo_count,
        ]);
    }

    /**
    * @group PMS-495
    */    
    public function testUpdateCardWithChangeTypeTextToImage()
    {
        $user = factory(User::class)->make();
        $photo = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_count' => 1
        ]);
        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type' => 'text',
            'description' => $this->faker->sentence(3, true),
            'source' => null,
            'ordering' => rand(1, 100),
        ]);
        $data = [
            'cards' => [
                [
                    'type' => 'image',
                    'photo_id' => $photo->id,
                    'url_ori' => $this->faker->url,
                    'group' => $this->faker->randomElement([
                        Group::BUILDING_FRONT,
                        Group::INSIDE_BUILDING,
                        Group::ROADVIEW_BUILDING,
                        Group::ROOM_FRONT,
                        Group::BATHROOM,
                        Group::OTHER,
                    ]),
                    'description' => $this->faker->sentence(3, true),
                    'source' => null,
                    'ordering' => rand(1, 100),
                ]
            ]
        ];

        $response = $this->actingAs($user)->call('PUT', self::UPDATE_CARD_URL . "$room->id/$card->id", $data);

        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route('admin.card.index', $room->id));
        $response->assertSessionHas('message', 'Style Updated');
        $this->assertDatabaseHas('designer_style', [
            'id' => $card->id,
            'photo_id' => $photo->id,
            'type' => $data['cards'][0]['type'],
            'group' => $data['cards'][0]['group'],
            'description' => Card::DESCRIPTION_PREFIX[$data['cards'][0]['group']],
            'source' => $data['cards'][0]['source'],
            'ordering' => $data['cards'][0]['ordering'],
            'photo_url' => $data['cards'][0]['url_ori'],
        ]);

        $this->assertDatabaseHas('designer',[
            'id' => $room->id,
            'photo_count' => $room->photo_count + 1,
        ]);
    }
}
