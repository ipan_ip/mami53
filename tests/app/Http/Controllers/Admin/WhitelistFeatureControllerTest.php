<?php

namespace App\Http\Controllers\Admin;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Feature\WhitelistFeature;
use App\Repositories\WhitelistFeatureRepository;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class WhitelistFeatureControllerTest extends MamiKosTestCase
{
    use WithoutEvents, WithoutMiddleware;

    private const WHITELIST_FEATURES_BASE_URL = 'admin/whitelist-features';
    private $user;

    protected function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testIndex()
    {
        $this->actingAs($this->user);
        $response = $this->call('GET', self::WHITELIST_FEATURES_BASE_URL);
        $response->assertViewIs('admin.contents.whitelist-feature.index');
    }

    public function testCreate()
    {
        $this->actingAs($this->user);
        $response = $this->call('GET', self::WHITELIST_FEATURES_BASE_URL . '/create');
        $response->assertViewIs('admin.contents.whitelist-feature.form');
    }

    public function testStore()
    {
        $this->actingAs($this->user);
        $data = [
            'name' => 'feature_a',
            'user_id' => 14
        ];
        $response = $this->post(self::WHITELIST_FEATURES_BASE_URL, $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL);
    }

    public function testStoreFailed()
    {
        $this->actingAs($this->user);
        $data = [
            'name' => 'feature_a',
            'user_id' => 14
        ];
        $mockedRepo = $this->mockAlternatively(WhitelistFeatureRepository::class);
        $mockedRepo->shouldReceive('save')->andReturn([false, 'error', null]);
        $response = $this->post(self::WHITELIST_FEATURES_BASE_URL, $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL . '/create');
    }

    public function testEdit()
    {
        factory(WhitelistFeature::class)->create([
            'id' => 1
        ]);
        $this->actingAs($this->user);
        $response = $this->call('GET', self::WHITELIST_FEATURES_BASE_URL . '/1/edit');
        $response->assertViewIs('admin.contents.whitelist-feature.form');
    }

    public function testUpdate()
    {
        factory(WhitelistFeature::class)->create([
            'id' => 2
        ]);
        $this->actingAs($this->user);
        $data = [
            'name' => 'feature_b',
            'user_id' => 14
        ];
        $response = $this->put(self::WHITELIST_FEATURES_BASE_URL . '/2', $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL);
    }

    public function testUpdateFailed()
    {
        $seededFeature = factory(WhitelistFeature::class)->create([
            'id' => 2
        ]);
        $this->actingAs($this->user);
        $data = [
            'name' => 'feature_b',
            'user_id' => 14
        ];
        $mockedRepo = $this->mockPartialAlternatively(WhitelistFeatureRepository::class);
        $mockedRepo->shouldReceive('find')->with(2)->andReturn($seededFeature);
        $mockedRepo->shouldReceive('save')->andReturn([false, 'error', null]);
        $response = $this->put(self::WHITELIST_FEATURES_BASE_URL . '/2', $data);
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL . '/2/edit');
    }

    public function testDestroy()
    {
        factory(WhitelistFeature::class)->create([
            'id' => 5
        ]);
        $this->actingAs($this->user);
        $response = $this->call('DELETE', self::WHITELIST_FEATURES_BASE_URL . '/5');
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL);
    }
    
    public function testDestroyFailed()
    {
        $seededFeature = factory(WhitelistFeature::class)->create([
            'id' => 5
        ]);
        $this->actingAs($this->user);
        $mockedRepo = $this->mockAlternatively(WhitelistFeatureRepository::class);
        $mockedRepo->shouldReceive('find')->with(5)->andReturn($seededFeature);
        $mockedRepo->shouldReceive('delete')->andReturn(false);
        $response = $this->call('DELETE', self::WHITELIST_FEATURES_BASE_URL . '/5');
        $response->assertStatus(302);
        $response->assertRedirect('/' . self::WHITELIST_FEATURES_BASE_URL);
    }
}
