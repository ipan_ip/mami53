<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Point\Point;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ExpiryControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;
    private $nonSegmentOwner;
    private $nonSegmentTenant;
    private $mamiroomsTenant;
    private $pointSegmentOwner;
    private $pointSegmentTenant;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        // Non-segment
        $this->nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $this->nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);
        $this->mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $this->pointSegmentOwner = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);
        $this->pointSegmentTenant = factory(Point::class)->create([ 'target' => Point::TARGET_TENANT ]);
    }

    public function testIndexExpiry_ShouldSuccess()
    {
        $response = $this->actingAs($this->user)->get(route('admin.point.expiry.index'));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.expiry.index');
        $response->assertViewHas('owner_expiry');
        $response->assertViewHas('tenant_expiry');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('owner_expiry');
        $this->assertEquals($this->nonSegmentOwner->expiry_value, $result);

        $result = $response->viewData('tenant_expiry');
        $this->assertEquals($this->nonSegmentTenant->expiry_value, $result);
    } 

    public function testChangeExpiry_WithInvalidRequest_ShouldFail()
    {
        $data = [
            'owner_expiry_value' => 0,
            'tenant_expiry_value' => 0,
        ];

        $response = $this->post(route('admin.point.expiry.change', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testChangeExpiry_ShouldSuccess()
    {
        $data = [
            'owner_expiry_value' => 12,
            'tenant_expiry_value' => 10,
        ];

        $response = $this->post(route('admin.point.expiry.change', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Point Expiry successfully updated.');
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->nonSegmentOwner->id,
            'expiry_value' => $data['owner_expiry_value']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentOwner->id,
            'expiry_value' => $data['owner_expiry_value']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->nonSegmentTenant->id,
            'expiry_value' => $data['tenant_expiry_value']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->mamiroomsTenant->id,
            'expiry_value' => $data['tenant_expiry_value']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentTenant->id,
            'expiry_value' => $data['tenant_expiry_value']
        ]);
    }
}
