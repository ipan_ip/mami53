<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Point\Point;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SettingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;
    private $ownerRoomGroups;
    private $ownerActivities;
    private $tenantActivities;
    private $ownerNonSegment;
    private $tenantNonSegment;
    private $tenantMamirooms;
    private $pointSegmentOwner;
    private $pointSegmentTenant;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->ownerRoomGroups = factory(PointOwnerRoomGroup::class, 3)->create();
        $this->ownerActivities = factory(PointActivity::class, 3)->create(['is_active' => 1, 'target' => 'owner']);
        $this->tenantActivities = factory(PointActivity::class, 3)->create(['is_active' => 1, 'target' => 'tenant']);

        // Non-segment
        $this->ownerNonSegment = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $this->tenantNonSegment = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);
        $this->tenantMamirooms = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $this->pointSegmentOwner = factory(Point::class)->create(['target' => Point::TARGET_OWNER]);
        $this->pointSegmentTenant = factory(Point::class)->create(['target' => Point::TARGET_TENANT]);

        for ($i=0; $i < 1; $i++) { 
            $roomGroupCount = count($this->ownerRoomGroups);
            for ($j=0; $j < $roomGroupCount; $j++) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $this->ownerNonSegment->id,
                    'activity_id' => $this->ownerActivities[$i]->id,
                    'room_group_id' => $this->ownerRoomGroups[$j]->id
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $this->pointSegmentOwner->id,
                    'activity_id' => $this->ownerActivities[$i]->id,
                    'room_group_id' => $this->ownerRoomGroups[$j]->id
                ]);
            }
        }

        for ($i=0; $i < 1; $i++) { 
            // Non-segment
            factory(PointSetting::class)->create([
                'point_id' => $this->tenantNonSegment->id,
                'activity_id' => $this->tenantActivities[$i]->id
            ]);
            factory(PointSetting::class)->create([
                'point_id' => $this->tenantMamirooms->id,
                'activity_id' => $this->tenantActivities[$i]->id
            ]);

            // Segment
            factory(PointSetting::class)->create([
                'point_id' => $this->pointSegmentTenant->id,
                'activity_id' => $this->tenantActivities[$i]->id
            ]);
        }
    }

    public function testIndexSetting_WithOwnerPointTarget_ShouldSuccess()
    {
        $this->ownerActivities[2]->is_active = 0;
        $this->ownerActivities[2]->save();

        $response = $this->actingAs($this->user)->get(route('admin.point.setting.index', [ 'target' => 'owner' ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.setting.index');
        $response->assertViewHas('setting');
        $response->assertViewHas('pointSegments');
        $response->assertViewHas('limitOptions');
        $response->assertViewHas('activity');
        $response->assertViewHas('roomGroup');
        $response->assertViewHas('tnc');
        $response->assertViewHas('pointTarget');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('setting');
        $this->assertEquals(1, count($result[$this->ownerNonSegment->id]));
        $this->assertEquals(1, count($result[$this->pointSegmentOwner->id]));
        $this->assertEquals(3, count($result[$this->ownerNonSegment->id][$this->ownerActivities[0]->key]));
        $this->assertEquals(3, count($result[$this->pointSegmentOwner->id][$this->ownerActivities[0]->key]));

        $this->assertFalse(array_key_exists($this->ownerActivities[2]->key, $result[$this->ownerNonSegment->id]));
        $this->assertFalse(array_key_exists($this->ownerActivities[2]->key, $result[$this->pointSegmentOwner->id]));

        $result = $response->viewData('pointSegments');
        $this->assertEquals($this->ownerNonSegment->id, $result[0]->id);
        $this->assertEquals($this->pointSegmentOwner->id, $result[1]->id);

        $result = $response->viewData('roomGroup');
        foreach ($this->ownerRoomGroups as $index => $ownerRoomGroup) { 
            $this->assertEquals($ownerRoomGroup->id, $result[$index]->id);
        }

        $result = $response->viewData('tnc');
        $this->assertEquals($this->ownerNonSegment->tnc, $result);
    } 

    public function testIndexSetting_WithTenantPointTarget_ShouldSuccess()
    {
        $this->tenantActivities[2]->is_active = 0;
        $this->tenantActivities[2]->save();

        $response = $this->actingAs($this->user)->get(route('admin.point.setting.index', [ 'target' => 'tenant' ]));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.setting.index');
        $response->assertViewHas('setting');
        $response->assertViewHas('pointSegments');
        $response->assertViewHas('limitOptions');
        $response->assertViewHas('activity');
        $response->assertViewHas('roomGroup');
        $response->assertViewHas('tnc');
        $response->assertViewHas('pointTarget');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('setting');
        $this->assertEquals(1, count($result[$this->tenantNonSegment->id]));
        $this->assertEquals(1, count($result[$this->tenantMamirooms->id]));
        $this->assertEquals(1, count($result[$this->pointSegmentTenant->id]));
        $this->assertEquals(1, count($result[$this->tenantNonSegment->id][$this->tenantActivities[0]->key]));
        $this->assertEquals(1, count($result[$this->tenantMamirooms->id][$this->tenantActivities[0]->key]));
        $this->assertEquals(1, count($result[$this->pointSegmentTenant->id][$this->tenantActivities[0]->key]));

        $this->assertFalse(array_key_exists($this->tenantActivities[2]->key, $result[$this->tenantNonSegment->id]));
        $this->assertFalse(array_key_exists($this->tenantActivities[2]->key, $result[$this->tenantMamirooms->id]));
        $this->assertFalse(array_key_exists($this->tenantActivities[2]->key, $result[$this->pointSegmentTenant->id]));

        $result = $response->viewData('pointSegments');
        $this->assertEquals($this->tenantNonSegment->id, $result[0]->id);
        $this->assertEquals($this->tenantMamirooms->id, $result[1]->id);
        $this->assertEquals($this->pointSegmentTenant->id, $result[2]->id);

        $result = $response->viewData('roomGroup');
        $this->assertNull($result);

        $result = $response->viewData('tnc');
        $this->assertEquals($this->tenantNonSegment->tnc, $result);
    }

    public function testPostSetting_WithInvalidRequest_ShouldFail()
    {
        $data = [
            $this->pointSegmentOwner->id => [
                $this->ownerActivities[0]->id => [
                    $this->ownerRoomGroups[0]->id => []
                ]
            ]
        ];

        $response = $this->post(route('admin.point.setting.post', ['point' => $this->pointSegmentOwner->type, 'activity' => $this->ownerActivities[0]->key, 'target' => 'owner']), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testPostSetting_WithOwnerPointTarget_ShouldSuccess()
    {
        $data = [
            $this->pointSegmentOwner->id => [
                $this->ownerActivities[1]->id => [
                    $this->ownerRoomGroups[0]->id => [
                        'times_to_received' => 1,
                        'received_each' => 2,
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => 4,
                        'is_active' => true
                    ],
                    $this->ownerRoomGroups[1]->id => [
                        'times_to_received' => 1,
                        'received_each' => 4,
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => 8,
                        'is_active' => true
                    ],
                    $this->ownerRoomGroups[2]->id => [
                        'times_to_received' => 1,
                        'received_each' => 6,
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => 12,
                        'is_active' => true
                    ]
                ]
            ]
        ];

        $response = $this->post(route('admin.point.setting.post', ['point' => $this->pointSegmentOwner->type, 'activity' => $this->ownerActivities[1]->key, 'target' => 'owner']), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Setting successfully updated.');
        foreach ($data[$this->pointSegmentOwner->id][$this->ownerActivities[1]->id] as $item) {
            $this->assertDatabaseHas((new PointSetting)->getTable(), $item);
        }
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->pointSegmentOwner->id,
            'activity_id' => $this->ownerActivities[2]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->ownerNonSegment->id,
            'activity_id' => $this->ownerActivities[1]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->ownerNonSegment->id,
            'activity_id' => $this->ownerActivities[2]->id
        ]);
    }

    public function testPostSetting_WithTenantPointTarget_ShouldSuccess()
    {
        $data = [
            $this->pointSegmentTenant->id => [
                $this->tenantActivities[1]->id => [
                    0 => [
                        'times_to_received' => 1,
                        'received_each' => 2,
                        'limit_type' => PointSetting::LIMIT_WEEKLY,
                        'limit' => 4,
                        'is_active' => true
                    ]
                ]
            ]
        ];

        $response = $this->post(route('admin.point.setting.post', ['point' => $this->pointSegmentTenant->type, 'activity' => $this->tenantActivities[1]->key, 'target' => 'tenant']), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Setting successfully updated.');
        foreach ($data[$this->pointSegmentTenant->id][$this->tenantActivities[1]->id] as $item) {
            $this->assertDatabaseHas((new PointSetting)->getTable(), $item);
        }
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->pointSegmentTenant->id,
            'activity_id' => $this->ownerActivities[2]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->tenantNonSegment->id,
            'activity_id' => $this->tenantActivities[1]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->tenantNonSegment->id,
            'activity_id' => $this->tenantActivities[2]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->tenantMamirooms->id,
            'activity_id' => $this->tenantActivities[1]->id
        ]);
        $this->assertDatabaseMissing((new PointSetting)->getTable(), [
            'point_id' => $this->tenantMamirooms->id,
            'activity_id' => $this->tenantActivities[2]->id
        ]);
    }

    public function testTncSetting_WithInvalidRequest_ShouldFail()
    {
        $data = [
            'tnc' => '',
        ];

        $response = $this->post(route('admin.point.setting.tnc', [ 'target' => 'owner' ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testTncSetting_WithOwnerPointTarget_ShouldSuccess()
    {
        $data = [
            'tnc' => 'New Term & Conditions',
        ];

        $response = $this->post(route('admin.point.setting.tnc', [ 'target' => 'owner' ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Terms & Conditions successfully updated.');
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->ownerNonSegment->id,
            'tnc' => $data['tnc']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentOwner->id,
            'tnc' => $data['tnc']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->tenantNonSegment->id,
            'tnc' => $this->tenantNonSegment->tnc
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->tenantMamirooms->id,
            'tnc' => $this->tenantMamirooms->tnc
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentTenant->id,
            'tnc' => $this->pointSegmentTenant->tnc
        ]);
    }

    public function testTncSetting_WithTenantPointTarget_ShouldSuccess()
    {
        $data = [
            'tnc' => 'New Term & Conditions',
        ];

        $response = $this->post(route('admin.point.setting.tnc', [ 'target' => 'tenant' ]), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Terms & Conditions successfully updated.');
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->ownerNonSegment->id,
            'tnc' => $this->ownerNonSegment->tnc
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentOwner->id,
            'tnc' => $this->pointSegmentOwner->tnc
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->tenantNonSegment->id,
            'tnc' => $data['tnc']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->tenantMamirooms->id,
            'tnc' => $data['tnc']
        ]);
        $this->assertDatabaseHas((new Point)->getTable(), [
            'id' => $this->pointSegmentTenant->id,
            'tnc' => $data['tnc']
        ]);
    }
}
