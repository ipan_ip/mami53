<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Point\PointOwnerRoomGroup;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OwnerRoomGroupControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;
    private $roomGroup;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->roomGroup = factory(PointOwnerRoomGroup::class)->create();
    }

    public function testIndexOwnerRoomGroup_ShouldSuccess()
    {
        factory(PointOwnerRoomGroup::class, 4)->create();

        $response = $this->actingAs($this->user)->get(route('admin.point.room-group.index'));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.room-group.index');
        $response->assertViewHas('roomGroup');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('roomGroup');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testCreateOwnerRoomGroup_ShouldSuccess()
    {
        $response = $this->actingAs($this->user)->get(route('admin.point.room-group.create'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.room-group.edit');
        $response->assertViewHas('boxTitle');
    }

    public function testStoreOwnerRoomGroup_WithInvalidRequest_ShouldFail()
    {
        $response = $this->post(route('admin.point.room-group.store', []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreOwnerRoomGroup_WhenFloorIsGreaterThanCeil_ShouldFail()
    {
        $data = factory(PointOwnerRoomGroup::class)->make([
            'floor' => 3,
            'ceil' => 2
        ]);;

        $response = $this->post(route('admin.point.room-group.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreOwnerRoomGroup_ShouldSuccess()
    {
        $data = [
            'floor' => 2, 
            'ceil' => 3
        ];

        $response = $this->post(route('admin.point.room-group.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Owner Room Group successfully added.');
        $this->assertDatabaseHas((new PointOwnerRoomGroup)->getTable(), $data);
    }

    public function testEditOwnerRoomGroup_ShouldSuccess()
    {
        $response = $this->actingAs($this->user)->get(route('admin.point.room-group.edit', $this->roomGroup->id));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.room-group.edit');
        $response->assertViewHas('roomGroup');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateOwnerRoomGroup_WithInvalidRequest_ShouldFail()
    {
        $response = $this->put(route('admin.point.room-group.update', $this->roomGroup->id, []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateOwnerRoomGroup_WhenFloorIsGreaterThanCeil_ShouldFail()
    {
        $response = $this->put(route('admin.point.room-group.update', $this->roomGroup->id, ['floor' => 3, 'ceil' => 2]));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateOwnerRoomGroup_ShouldSuccess()
    {
        $data = [
            'floor' => 2, 
            'ceil' => 3
        ];

        $response = $this->put(route('admin.point.room-group.update', $this->roomGroup->id), $data);

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Owner Room Group successfully updated.');
        $this->assertDatabaseHas((new PointOwnerRoomGroup)->getTable(), $data);
    }

    public function testDestroyOwnerRoomGroup_ShouldSuccess()
    {
        $response = $this->delete(route('admin.point.room-group.destroy', $this->roomGroup->id));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Owner Room Group successfully deleted.');
    }
}