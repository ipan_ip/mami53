<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Point\Point;
use App\Entities\Point\PointBlacklistConfig;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BlacklistControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testIndexBlacklist_ShouldSuccess()
    {
        $response = $this->actingAs($this->user)->get(route('admin.point.blacklist.index'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.blacklist.index');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('blacklists');
    }

    public function testCreateBlacklist_ShouldSuccess()
    {
        $response = $this->actingAs($this->user)->get(route('admin.point.blacklist.create'));
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.blacklist.create');
        $response->assertViewHas('boxTitle');
    }

    public function testStoreBlacklist_WithInvalidRequest_ShouldFail()
    {
        $response = $this->post(route('admin.point.blacklist.store', []));

        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    public function testStoreBlacklist_ShouldSuccess()
    {
        $data = [
            'usertype' => 'kost_level',
            'value' => 2, // id of usertype, e.g: kost_level id
            'is_blacklisted' => 1,
        ];
        $response = $this->post(route('admin.point.blacklist.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PointBlacklistConfig)->getTable(), $data);
    }

    public function testToggleBlacklist_ShouldSuccess()
    {
        $blacklist = factory(PointBlacklistConfig::class)->create([
            'is_blacklisted' => 0
        ]);
        $response = $this->actingAs($this->user)->get(route('admin.point.blacklist.toggle', $blacklist->id));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PointBlacklistConfig)->getTable(), [
            'is_blacklisted' => 1
        ]);
    }
}
