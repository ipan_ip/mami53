<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Point\Point;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SegmentControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $user;
    private $nonSegmentOwner;
    private $nonSegmentTenant;
    private $mamiroomsTenant;
    private $pointSegmentOwner;
    private $pointSegmentTenant;

    private $kostLevel;
    private $roomLevel;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        // Non-segment
        $this->nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $this->nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);
        $this->mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $this->pointSegmentOwner = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);
        $this->pointSegmentTenant = factory(Point::class)->create([ 'target' => Point::TARGET_TENANT ]);

        $this->kostLevel = factory(KostLevel::class)->create();
        $this->roomLevel = factory(RoomLevel::class)->create();
    }

    public function testIndexSegment_ShouldSuccess()
    {
        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.index'));
        
        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.index');
        $response->assertViewHas('segments');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('segments');
        $this->assertEquals(6, $result->total());
        $this->assertEquals(20, $result->perPage());
    } 

    public function testIndexSegment_WithTypeSearch_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.index', [ 'type' => $this->pointSegmentOwner->type ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.index');
        $response->assertViewHas('segments');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('segments');
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    } 

    public function testIndexSegment_WithTargetSearch_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.index', [ 'target' => Point::TARGET_OWNER ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.index');
        $response->assertViewHas('segments');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('segments');
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    } 

    public function testIndexSegment_WithLimitPagination_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.index', [ 'limit' => 3 ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.index');
        $response->assertViewHas('segments');
        $response->assertViewHas('boxTitle');
        
        $result = $response->viewData('segments');
        $this->assertEquals(6, $result->total());
        $this->assertEquals(3, $result->perPage());
    } 

    public function testCreateSegment_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.create'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.edit');
        $response->assertViewHas('boxTitle');
        $response->assertViewHas('kostLevelList');
        $response->assertViewHas('roomLevelList');
    }

    public function testStoreSegment_WithInvalidRequest_ShouldFail()
    {
        $response = $this->post(route('admin.point.segment.store', []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreSegment_WithDuplicateType_ShouldFail()
    {
        $data = [
            'type' => 'regular',
            'title' => 'Booking Langsung'
        ];

        $response = $this->post(route('admin.point.segment.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testStoreSegmentOwner_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_OWNER,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $response = $this->post(route('admin.point.segment.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Segment successfully added.');
        $this->assertDatabaseHas((new Point)->getTable(), $data);
    }

    public function testStoreSegmentTenant_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_TENANT,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $response = $this->post(route('admin.point.segment.store', $data));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Segment successfully added.');
        $this->assertDatabaseHas((new Point)->getTable(), $data);
    }

    public function testEditSegment_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($this->user)->get(route('admin.point.segment.edit', $this->pointSegmentOwner->id));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.segment.edit');
        $response->assertViewHas('segment');
        $response->assertViewHas('kostLevelList');
        $response->assertViewHas('roomLevelList');
        $response->assertViewHas('boxTitle');
    }

    public function testUpdateSegment_WithInvalidRequest_ShouldFail()
    {
        $response = $this->put(route('admin.point.segment.update', $this->pointSegmentOwner->id, []));

        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateSegment_WithDuplicateType_ShouldFail()
    {
        $data = [
            'type' => 'regular',
            'title' => 'GoldPlus 1',
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $response = $this->put(route('admin.point.segment.update', $this->pointSegmentOwner->id), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('errors');
    }

    public function testUpdateSegmentOwner_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_OWNER,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $response = $this->put(route('admin.point.segment.update', $this->pointSegmentOwner->id), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Segment successfully updated.');
        $this->assertDatabaseHas((new Point)->getTable(), $data);
    }

    public function testUpdateSegmentTenant_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_TENANT,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $response = $this->put(route('admin.point.segment.update', $this->pointSegmentTenant->id), $data);
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Segment successfully updated.');
        $this->assertDatabaseHas((new Point)->getTable(), $data);
    }

    public function testDestroySegment_ShouldSuccess()
    {
        $response = $this->delete(route('admin.point.segment.destroy', $this->pointSegmentOwner->id));
        
        $response->assertStatus(302); // redirect
        $response->assertSessionHas('message', 'Segment successfully deleted.');
    }
}