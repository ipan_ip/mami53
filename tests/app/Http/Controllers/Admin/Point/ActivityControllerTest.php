<?php

namespace app\Http\Controllers\Admin;

use App\Entities\Point\PointActivity;
use App\User;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ActivityControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function testIndexActivity()
    {
        $user = factory(User::class)->create();
        factory(PointActivity::class)->create([
            'key' => 'ini-key',
        ]);

        $response = $this->actingAs($user)->get(route('admin.point.activity.index'));
        $response->assertOk();
        $response->assertSee('ini-key');
    }

    public function testCreateActivity_ShouldSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.point.activity.create'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.activity.edit');
        $response->assertViewHas('target');
        $response->assertViewHas('ownerReceivedPaymentGeneralAvailable');
        $response->assertViewHas('nonGeneralOwnerReceivedPaymentActivities');
        $response->assertViewHas('boxTitle');
    }

    public function testStoreActivityData()
    {
        $user = factory(User::class)->create();
        $formData = $this->getFormData();

        $response = $this->actingAs($user)->post(route('admin.point.activity.store'), $formData);
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PointActivity)->getTable(), [
            'key' => 'hi-key',
        ]);
    }

    public function testStoreReceivedPaymentGeneralActivity_WhenNonGeneralExist()
    {
        $user = factory(User::class)->create();
        factory(PointActivity::class)->create([
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY,
            'is_active' => 1
        ]);

        $formData = $this->getFormData();
        $formData['key'] = PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY;

        $response = $this->actingAs($user)->post(route('admin.point.activity.store'), $formData);
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PointActivity)->getTable(), [
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY,
            'is_active' => 1
        ]);
        $this->assertDatabaseHas((new PointActivity)->getTable(), [
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY,
            'is_active' => 0
        ]);
    }

    public function testEditActivity_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $pointActivity = factory(PointActivity::class)->create([
            'key' => 'not-updated-key',
        ]);

        $response = $this->actingAs($user)->get(route('admin.point.activity.edit', $pointActivity->id));

        $response->assertStatus(200);
        $response->assertViewIs('admin.contents.point.activity.edit');
        $response->assertViewHas('activity');
        $response->assertViewHas('target');
        $response->assertViewHas('ownerReceivedPaymentGeneralAvailable');
        $response->assertViewHas('nonGeneralOwnerReceivedPaymentActivities');
        $response->assertViewHas('boxTitle');
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testUpdateActivityData()
    {
        $user = factory(User::class)->create();
        $pointActivity = factory(PointActivity::class)->create([
            'key' => 'not-updated-key',
        ]);
        $formData = $this->getFormData();
        $url = route('admin.point.activity.update', $pointActivity->id);

        $this->app->instance(PointActivity::class, $pointActivity);
        $response = $this->actingAs($user)->put($url, $formData);
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseMissing((new PointActivity)->getTable(), [
            'key' => 'not-updated-key',
        ]);
    }

    public function testUpdateReceivedPaymentGeneralActivity_WhenNonGeneralExist()
    {
        $user = factory(User::class)->create();
        $user = factory(User::class)->create();
        factory(PointActivity::class)->create([
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY,
            'is_active' => 1
        ]);

        $pointActivity = factory(PointActivity::class)->create([
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY,
            'is_active' => 0
        ]);

        $formData = $this->getFormData();
        $formData['key'] = PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY;
        $formData['is_active'] = 1;
        
        $this->app->instance(PointActivity::class, $pointActivity);
        $response = $this->actingAs($user)->put(route('admin.point.activity.update', $pointActivity->id), $formData);
        
        $response->assertStatus(302);
        $response->assertSessionHas('message');
        $this->assertDatabaseHas((new PointActivity)->getTable(), [
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY,
            'is_active' => 1
        ]);
        $this->assertDatabaseHas((new PointActivity)->getTable(), [
            'key' => PointActivity::OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY,
            'is_active' => 0
        ]);
    }

    private function getFormData()
    {
        return [
            'target' => 'owner',
            'key' => 'hi-key',
            'name' => 'hi-name',
            'title' => 'hi-title',
            'description' => '<p>hi-description</p>',
            'is_active' => 1
        ];
    }
}
