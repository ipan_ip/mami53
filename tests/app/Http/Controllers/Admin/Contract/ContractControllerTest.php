<?php

namespace app\Http\Controllers\Admin\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use App\Test\MamiKosTestCase;

class ContractControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const CONTRACT_URL = 'admin/contract';
    private const CONTRACT_INVOICE_URL = 'admin/contract/invoice/{contractId}';
    private const CONTRACT_ROOM_ALLOTMENT_URL = 'admin/contract/room-allotment/{contractId}';

    public function testGetListBookingIsSuccess(): void
    {
        // prepare data
        $user = factory(User::class)->make();
        $this->actingAs($user);

        $response = $this->call('GET', self::CONTRACT_URL);
        $response->assertViewHas('contracts');
        $response->assertViewHas('contractRaws');
    }

    public function testGetShowInvoicesSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $invoiceEntity = factory(MamipayInvoice::class)->create([
            'contract_id' => $contractEntity->id
        ]);

        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', $contractEntity->id, self::CONTRACT_INVOICE_URL);

        // run test
        $response = $this->json('GET', $url);
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);
    }

    public function testGetRoomAllotmentDataContractNotFoundSuccess(): void
    {
        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', 1, self::CONTRACT_ROOM_ALLOTMENT_URL);

        // run test
        $response = $this->json('GET', $url);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Data kontrak tidak ditemukan'
            ]
        ]);
    }

    public function testGetRoomAllotmentDataSuccess(): void
    {
        // prepare data
        $contract = factory(MamipayContract::class)->create();
        $room = factory(Room::class)->create();
        $typeKost = factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id
        ]);
        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', $contract->id, self::CONTRACT_ROOM_ALLOTMENT_URL);

        // run test
        $response = $this->json('GET', $url);
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);
    }

    public function testPostChangeRoomAllotmentContractNotFoundSuccess(): void
    {
        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', 1, self::CONTRACT_ROOM_ALLOTMENT_URL);

        // run test
        $response = $this->json('POST', $url, []);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Data kontrak tidak ditemukan'
            ]
        ]);
    }

    public function testPostChangeRoomAllotmentNotAllowed(): void
    {
        // prepare data
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_TERMINATED
        ]);
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 1
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'designer_room_id' => $roomUnit->id
        ]);

        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', $contract->id, self::CONTRACT_ROOM_ALLOTMENT_URL);

        // run test
        $response = $this->json('POST', $url, [
            'contract_id' => $contract->id,
            'designer_room_id' => 1
        ]);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Kontrak tidak bisa dirubah'
            ]
        ]);
    }

    public function testPostChangeRoomAllotmentSuccess(): void
    {
        // prepare data
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE
        ]);
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 1
        ]);
        $roomUnitChange = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 2
        ]);
        $typeKost = factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'designer_room_id' => $roomUnit->id
        ]);
        $user = factory(User::class)->make();
        $this->actingAs($user);

        // mapping url
        $url = str_replace('{contractId}', $contract->id, self::CONTRACT_ROOM_ALLOTMENT_URL);

        // run test
        $response = $this->json('POST', $url, [
            'contract_id' => $contract->id,
            'designer_room_id' => $roomUnitChange->id
        ]);
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => 'Nomor kamar berhasil dirubah'
        ]);
    }
}