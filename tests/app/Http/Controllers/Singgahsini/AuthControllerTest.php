<?php

namespace App\Http\Controllers\Singgahsini;

use App\Entities\Api\ErrorCode;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AuthControllerTest extends MamiKosTestCase
{

    protected const LOGIN_URL = '/singgahsini/api/auth/login';
    protected const LOGOUT_URL = '/singgahsini/api/auth/logout';

    protected $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'role' => UserRole::Administrator, 
            'email' => 'admin@mail.com', 
            'password' => bcrypt(md5('password'))
        ]);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    /**
     * @group SS
     * @group SS-20
     * @group App/Http/Controllers/Singgahsini/AuthController
     */
    public function testPostLoginSuccess()
    {
        $response = $this->json(
            'POST',
            self::LOGIN_URL,
            ['email' => $this->user->email, 'password' => 'password']
        );

        $response->assertSuccessful();
        $response->assertJson([
            'status' => true,
            'user_id' => $this->user->id
        ]);
        $this->assertAuthenticatedAs($this->user);
    }

    /**
     * @group SS
     * @group SS-20
     * @group App/Http/Controllers/Singgahsini/AuthController
     */
    public function testPostLoginUserNotFoundShouldFail()
    {
        $response = $this->json(
            'POST',
            self::LOGIN_URL,
            ['email' => 'user@mail.com', 'password' => 'password']
        );

        $response->assertStatus(403);
        $response->assertJson([
            'status' => false,
            'issue' => [
                'code' => ErrorCode::INVALID_USER
            ]
        ]);
    }

    /**
     * @group SS
     * @group SS-20
     * @group App/Http/Controllers/Singgahsini/AuthController
     */
    public function testPostLoginWrongPasswordShouldFail()
    {
        $response = $this->json(
            'POST',
            self::LOGIN_URL,
            ['email' => $this->user->email, 'password' => 'password2']
        );

        $response->assertStatus(403);
        $response->assertJson([
            'status' => false,
            'issue' => [
                'code' => ErrorCode::INVALID_USER
            ]
        ]);
    }

    /**
     * @group SS
     * @group SS-20
     * @group App/Http/Controllers/Singgahsini/AuthController
     */
    public function testPostLoginValidationFailed()
    {
        $response = $this->json(
            'POST',
            self::LOGIN_URL,
            ['email' => '12', 'password' => '']
        );

        $response->assertStatus(422);
        $response->assertJson([
            'status' => false,
            'issue' => [
                'code' => ErrorCode::SINGGAHSINI_REGISTRATION
            ]
        ]);
    }

    /**
     * @group SS
     * @group SS-20
     * @group App/Http/Controllers/Singgahsini/AuthController
     */
    public function testLoginLogoutSuccess()
    {
        $responseLogin = $this->json(
            'POST',
            self::LOGIN_URL,
            ['email' => $this->user->email, 'password' => 'password']
        );
        $responseLogin->assertSuccessful();

        $tokenType = $responseLogin->json()['oauth']['token_type'];
        $accessToken = $responseLogin->json()['oauth']['access_token'];

        $responseLogout = $this->json(
            'POST',
            self::LOGOUT_URL,
            [],
            [
                'Authorization' => $tokenType.' '.$accessToken
            ]
        );

        $responseLogout->assertSuccessful();
        $responseLogout->assertJson([
            'status' => true,
        ]);
    }
}