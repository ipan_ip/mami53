<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class DashboardControllerTest extends MamiKosTestCase
{

    protected $user;
    protected $role;
    protected $permission;
    protected $room;
    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create();

        $this->user->attachRole($this->role);
        $this->role->attachPermission($this->permission);

        $this->room = factory(Room::class)->create(['area_city' => 'Surabaya']);
        $this->consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testDataWithoutPropertyAssignedToConsultant()
    {
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
        );
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
        );
        factory(MamipayContractKost::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(1, $data->hangingBooking);
        $this->assertEquals(1, $data->hangingPayment);
        $this->assertEquals(1, $data->recurringTenant);
        $this->assertEquals(1, $data->unpaidContract);
    }

    public function testDataWithPropertyAssignedToConsultant()
    {
        $user = factory(User::class)->create();
        $user->attachRole($this->role);
        $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);
        $consultant->rooms()->attach($this->room);

        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
        );
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
        );

        $response = $this->setWebApiCookie()->actingAs($user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(1, $data->hangingBooking);
        $this->assertEquals(1, $data->hangingPayment);
        $this->assertEquals(1, $data->recurringTenant);
        $this->assertEquals(1, $data->unpaidContract);
    }

    // public function testDataWithPropertyNotAssignedToConsultant()
    // {
    //     $designer = factory(Room::class)->create(['area_city' => 'Jakarta']);
    //     $user = factory(User::class)->create();
    //     $user->attachRole($this->role);
    //     $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);
    //     $consultant->rooms()->attach($this->room);

    //     $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $designer->id]);
    //     factory(BookingUser::class)->create(
    //         ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
    //     );
    //     factory(BookingUser::class)->create(
    //         ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
    //     );

    //     $contract = factory(MamipayContract::class)->create(
    //         ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
    //     );
    //     factory(MamipayContractKost::class)->create(
    //         ['contract_id' => $contract->id, 'designer_id' => $designer->id]
    //     );

    //     factory(MamipayInvoice::class)->create(
    //         ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
    //     );

    //     $response = $this->setWebApiCookie()->actingAs($user, 'passport')->get('consultant-tools/api/dashboard');
    //     $response->assertSuccessful();

    //     $data = json_decode($response->getContent())->data;

    //     $this->assertEquals(0, $data->hangingBooking);
    //     $this->assertEquals(0, $data->hangingPayment);
    //     $this->assertEquals(0, $data->recurringTenant);
    //     $this->assertEquals(0, $data->unpaidContract);
    // }

    public function testDataHangingBookingNotFound()
    {
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(0, $data->hangingBooking);
        $this->assertEquals(1, $data->hangingPayment);
        $this->assertEquals(1, $data->recurringTenant);
        $this->assertEquals(1, $data->unpaidContract);
    }

    public function testDataHangingPaymentNotFound()
    {
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(1, $data->hangingBooking);
        $this->assertEquals(0, $data->hangingPayment);
        $this->assertEquals(1, $data->recurringTenant);
        $this->assertEquals(1, $data->unpaidContract);
    }

    public function testDataRecurringTenantNotFound()
    {
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
        );
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::yesterday()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(1, $data->hangingBooking);
        $this->assertEquals(1, $data->hangingPayment);
        $this->assertEquals(0, $data->recurringTenant);
        $this->assertEquals(1, $data->unpaidContract);
    }

    public function testDataUnpaidContractNotFound()
    {
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $this->room->id]);
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]
        );
        factory(BookingUser::class)->create(
            ['booking_designer_id' => $bookingDesigner->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]
        );

        $contract = factory(MamipayContract::class)->create(
            ['status' => MamipayContract::STATUS_ACTIVE, 'end_date' => Carbon::now()]
        );
        factory(MamipayContractKost::class)->create(
            ['contract_id' => $contract->id, 'designer_id' => $this->room->id]
        );

        factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => MamipayInvoice::PAYMENT_STATUS_PAID]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->get('consultant-tools/api/dashboard');
        $response->assertSuccessful();

        $data = json_decode($response->getContent())->data;

        $this->assertEquals(1, $data->hangingBooking);
        $this->assertEquals(1, $data->hangingPayment);
        $this->assertEquals(1, $data->recurringTenant);
        $this->assertEquals(0, $data->unpaidContract);
    }

    public function testActivityProgressListNonConsultantShouldReturnAllProgress()
    {
        $user = factory(User::class)->create();
        $user->attachRole($this->role);
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET]);
        factory(PotentialTenant::class)->create(['id' => 1]);
        factory(PotentialTenant::class)->create(['id' => 2]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 1
            ]
        );
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 10,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 2
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 2
            ]
        );
    }

    public function testActivityProgressListConsultantShouldReturnOnlyHisProgress()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(PotentialTenant::class)->create(['id' => 1]);
        factory(PotentialTenant::class)->create(['id' => 2]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 1
            ]
        );
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 10,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 2
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1
            ]
        );
    }

    public function testActivityProgressListWithDataTypeFilterShouldReturnCorrectData()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(ActivityFunnel::class)->create(['id' => 2, 'related_table' => 'designer']);
        factory(PotentialTenant::class)->create(['id' => 1]);
        factory(PotentialTenant::class)->create(['id' => 2]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 1
            ]
        );
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'designer',
                'progressable_id' => 2
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list',
            ['data_type' => [ActivityFunnel::TABLE_DBET]]
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1
            ]
        );
    }

    public function testActivityProgressListForTypeContractShouldReturnTenantName()
    {
        $tenant = factory(MamipayTenant::class)->create();
        factory(ActivityFunnel::class)->create(
            ['id' => 2, 'related_table' => 'mamipay_contract', 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => $tenant->id]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'name' => $tenant->name
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListWithTotalStageZeroShouldReturnZeroAsProgressPercentage()
    {
        $tenant = factory(MamipayTenant::class)->create();
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(ActivityFunnel::class)->create(
            ['id' => 2, 'related_table' => 'mamipay_contract', 'total_stage' => 0, 'consultant_role' => 'admin']
        );
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => $tenant->id]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'progress_percentage' => 0
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListWithTotalStageNotZeroShouldReturnCorrectAmountAsProgressPercentage()
    {
        $tenant = factory(MamipayTenant::class)->create();
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(ActivityFunnel::class)->create(
            ['id' => 2, 'related_table' => 'mamipay_contract', 'total_stage' => 10, 'consultant_role' => 'admin']
        );
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => $tenant->id]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 1,
                'current_stage' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'progress_percentage' => 10
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListShouldReturnCorrectDataTypeForPotentialTenant()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(PotentialTenant::class)->create(['id' => 1]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'DBET'
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListShouldReturnCorrectDataTypeForPotentialProperty()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(PotentialProperty::class)->create(['id' => 1]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Properti Potensial'
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListShouldReturnCorrectDataTypeForContract()
    {
        $tenant = factory(MamipayTenant::class)->create();
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_CONTRACT, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => $tenant->id]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Kontrak'
                    ]
                ]
            ]
        );
    }

    public function testActivityProgressListShouldReturnCorrectDataTypeForProperty()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_PROPERTY, 'consultant_role' => 'admin']
        );
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(Room::class)->create(['id' => 1]);
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => 1
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/activity-list'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Properti'
                    ]
                ]
            ]
        );
    }

    public function testActiveFunnelListForOneAssignedRoleShouldReturnCorrectFunnel()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            ['id' => 2, 'related_table' => 'mamipay_contract', 'total_stage' => 10, 'consultant_role' => 'admin']
        );
        factory(ActivityFunnel::class)->create(
            ['id' => 3, 'related_table' => 'mamipay_contract', 'total_stage' => 10, 'consultant_role' => 'supply']
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-funnel'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1
            ]
        );
    }

    public function testActiveFunnelListForMultipleAssignedRoleShouldReturnCorrectFunnel()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'supply']);

        factory(ActivityFunnel::class)->create(
            ['id' => 2, 'related_table' => 'mamipay_contract', 'total_stage' => 10, 'consultant_role' => 'admin']
        );
        factory(ActivityFunnel::class)->create(
            ['id' => 3, 'related_table' => 'mamipay_contract', 'total_stage' => 10, 'consultant_role' => 'supply']
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-funnel'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 2
            ]
        );
    }

    public function testActiveDataTypeForTypeProperty()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );
        factory(ActivityFunnel::class)->create(
            [
                'id' => 3,
                'related_table' => ActivityFunnel::TABLE_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-data-type'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Data Properti'
                    ]
                ]
            ]
        );
    }

    public function testActiveDataTypeForTypePotentialTenant()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );
        factory(ActivityFunnel::class)->create(
            [
                'id' => 3,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-data-type'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'related_table' => ActivityFunnel::TABLE_DBET,
                        'data_type' => 'Data Penyewa',
                    ]
                ]
            ]
        );
    }

    public function testActiveDataTypeForTypeContract()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );
        factory(ActivityFunnel::class)->create(
            [
                'id' => 3,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-data-type'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'related_table' => ActivityFunnel::TABLE_CONTRACT,
                        'data_type' => 'Data Kontrak',
                    ]
                ]
            ]
        );
    }

    public function testActiveDataTypeForTypePotentialProperty()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );
        factory(ActivityFunnel::class)->create(
            [
                'id' => 3,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-data-type'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Data Properti Potensial'
                    ]
                ]
            ]
        );
    }

    public function testActiveDataTypeForTypePotentialOwner()
    {
        factory(ConsultantRole::class)->create(['consultant_id' => $this->user->consultant->id, 'role' => 'admin']);

        factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );
        factory(ActivityFunnel::class)->create(
            [
                'id' => 3,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/active-data-type'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'data_type' => 'Data Owner Potensial'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityWithoutSearchKey()
    {
        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity'
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'status' => false,
                'message' => 'Please input search value'
            ]
        );
    }

    public function testSearchActivityWithoutDataTypeParam()
    {
        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => 'test']
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'status' => false,
                'message' => 'Please select available data type'
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialTenantSearchByTenantName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $tenant->name, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialTenantSearchByTenantPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $tenant->phone_number, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialTenantSearchByTenantKostName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialTenantSearchByTenantKostOwnerPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10, 'owner_phone' => '085555555']);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->owner_phone, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    //TO DO: unit test that need mamipay_type_kost will activate later, still waiting for mas aji update mamipay submodule
    /*
    public function testSearchActivityWithDataTypeContractSearchByTenantName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['id' => 10, 'tenant_id' => $tenant->id]);
        factory(MamipayContractKost::class)->create(['designer_id' => 10, 'contract_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 10,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $tenant->name, 'data_type' => ActivityFunnel::TABLE_CONTRACT]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'Kontrak',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityWithDataTypeContractSearchByTenantPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['id' => 10, 'tenant_id' => $tenant->id]);
        factory(MamipayContractKost::class)->create(['designer_id' => 10, 'contract_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 10,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $tenant->phone_number, 'data_type' => ActivityFunnel::TABLE_CONTRACT]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'Kontrak',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityWithDataTypeContractSearchByKostName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['id' => 10, 'tenant_id' => $tenant->id]);
        factory(MamipayContractKost::class)->create(['designer_id' => 10, 'contract_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 10,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_CONTRACT]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'Kontrak',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityWithDataTypeContractSearchByKostOwnerPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_CONTRACT,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['id' => 10, 'tenant_id' => $tenant->id]);
        factory(MamipayContractKost::class)->create(['designer_id' => 10, 'contract_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => 10,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user)->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->owner_phone, 'data_type' => ActivityFunnel::TABLE_CONTRACT]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'Kontrak',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }
    */

    public function testSearchActivityWithDataTypeRoomSearchByKostName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $room = factory(Room::class)->create(['id' => 10, 'owner_phone' => '085555555']);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_PROPERTY]
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $room->name,
                        'data_type' => 'Properti',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypeRoomSearchByKostOwnerPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $room = factory(Room::class)->create(['id' => 10, 'owner_phone' => '085555555']);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->owner_phone, 'data_type' => ActivityFunnel::TABLE_PROPERTY]
        );
        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $room->name,
                        'data_type' => 'Properti',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialPropertySearchByName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'supply'
            ]
        );

        factory(PotentialOwner::class)->create(['id' => 1]);
        $room = factory(PotentialProperty::class)->create(
            ['id' => 1, 'name' => 'kost potensial', 'consultant_potential_owner_id' => 1]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => 'potensial', 'data_type' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $room->name,
                        'data_type' => 'Properti Potensial',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialPropertySearchByOwnerPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                'total_stage' => 10,
                'consultant_role' => 'supply'
            ]
        );

        $owner = factory(PotentialOwner::class)->create(['phone_number' => '08444444']);
        $room = factory(PotentialProperty::class)->create(
            [
                'id' => 1,
                'name' => 'kost potensial',
                'consultant_potential_owner_id' => $owner->id
            ]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => '08444', 'data_type' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $room->name,
                        'data_type' => 'Properti Potensial',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWithDataTypePotentialOwnerSearchByName()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER,
                'total_stage' => 10,
                'consultant_role' => 'supply'
            ]
        );

        $owner = factory(PotentialOwner::class)->create(['id' => 1, 'name' => 'test owner']);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
                'progressable_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => 'test', 'data_type' => ActivityFunnel::TABLE_POTENTIAL_OWNER]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $owner->name,
                        'data_type' => 'Owner Potensial',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityWithDataTypePotentialOwnerSearchByPhone()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER,
                'total_stage' => 10,
                'consultant_role' => 'supply'
            ]
        );

        $owner = factory(PotentialOwner::class)->create(['id' => 1, 'phone_number' => '0844474638']);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
                'progressable_id' => 1,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => '08444', 'data_type' => ActivityFunnel::TABLE_POTENTIAL_OWNER]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $owner->name,
                        'data_type' => 'Owner Potensial',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]

        );
    }

    public function testSearchActivityForUserNotRegisteredAsConsultantWillReturnAll()
    {
        $user = factory(User::class)->create();
        $user->attachRole($this->role);
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWillReturnTugasAktifAsStageNameWhenCurrentStageIs0()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 0
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => 'Tugas Aktif'
                    ]
                ]
            ]
        );
    }

    public function testSearchActivityWillReturnCorrectStageNameWhenCurrentStageIsNot0()
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'id' => 2,
                'related_table' => ActivityFunnel::TABLE_DBET,
                'total_stage' => 10,
                'consultant_role' => 'admin'
            ]
        );

        $tenant = factory(PotentialTenant::class)->create(['designer_id' => 10]);
        $room = factory(Room::class)->create(['id' => 10]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 2,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'consultant_id' => $this->user->consultant->id,
                'current_stage' => 1
            ]
        );
        $form = factory(ActivityForm::class)->create(
            ['consultant_activity_funnel_id' => 2, 'stage' => 1, 'form_element' => []]
        );

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard/search/activity',
            ['search' => $room->name, 'data_type' => ActivityFunnel::TABLE_DBET]
        );

        $response->assertSuccessful();

        $response->assertJson(
            [
                'total' => 1,
                'data' => [
                    [
                        'id' => $progress->id,
                        'current_stage' => $progress->current_stage,
                        'funnel_id' => $progress->consultant_activity_funnel_id,
                        'funnel_name' => $funnel->name,
                        'total_stage' => $funnel->total_stage,
                        'name' => $tenant->name,
                        'data_type' => 'DBET',
                        'stage_name' => $form->name
                    ]
                ]
            ]
        );
    }

    public function testDataWithNewSalesMotion(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(['is_active' => true]);
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $this->consultant->id
        ]);

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard'
        );

        $this->assertTrue($response->getData()->data->has_new_sales_motion);
    }

    public function testDataWithoutNewSalesMotion(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotionProgress::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $this->consultant->id
        ]);
        factory(SalesMotionAssignee::class)->create([
            'consultant_sales_motion_id' => $salesMotion->id,
            'consultant_id' => $this->consultant->id
        ]);

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard'
        );

        $this->assertFalse($response->getData()->data->has_new_sales_motion);
    }

    public function testDataWithoutAssignedSalesMotion(): void
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $response = $this->setWebApiCookie()->actingAs($this->user, 'passport')->json(
            'GET',
            'consultant-tools/api/dashboard'
        );

        $this->assertFalse($response->getData()->data->has_new_sales_motion);
    }
}
