<?php

namespace App\Http\Controllers\ConsultantTools;

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use stdClass;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Entrust\Role;
use App\Entities\User\UserRole;
use App\Entities\SLA\SLAVersion;
use App\Entities\User\UserMedia;
use App\Entities\Level\KostLevel;
use App\Entities\Entrust\Permission;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\SLA\SLAConfiguration;
use App\Entities\Consultant\Consultant;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Consultant\Booking\Status;
use App\Entities\Mamipay\MamipayBillingRule;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Consultant\ConsultantMapping;
use App\Entities\Mamipay\MamipayAdditionalCost;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use App\Entities\Consultant\Elasticsearch\BookingUserIndexQuery;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use GuzzleHttp\Psr7\Response;
use Mockery;

class BookingManagementControllerTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;
    use WithFaker;

    protected $user;
    protected $elasticsearch;
    protected const DETAIL_TENANT = '/consultant-tools/api/booking-management/detail/tenant/';

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock('overload:GuzzleHttp\Client', 'GuzzleHttp\ClientInterface');
        parent::setUp();
        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        factory(Consultant::class)->create(['user_id' => $this->user->id]);

        $this->user->attachRole($role);
        $role->attachPermission($permission);

        $this->elasticsearch = $this->mock('alias:Elasticsearch');
    }

    public function testIndexShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'query' => [
                        'match_all' => new stdClass()
                    ],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management');
    }

    public function testIndexWithActiveInstantBooking(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'bool' => [
                                                    'should' => [
                                                        ['term' => ['is_instant_booking' => 'true']]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?is_instant_booking[]=true');
    }

    public function testIndexWithNonActiveInstantBooking(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'bool' => [
                                                    'should' => [
                                                        ['term' => ['is_instant_booking' => 'false']]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?is_instant_booking[]=false');
    }

    public function testIndexWithBothInstantBooking(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'bool' => [
                                                    'should' => [
                                                        ['term' => ['is_instant_booking' => 'true']],
                                                        ['term' => ['is_instant_booking' => 'false']]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json(
                'GET',
                '/consultant-tools/api/booking-management?is_instant_booking[]=true&is_instant_booking[]=false'
            );
    }

    public function testIndexWithKostLevel(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'bool' => [
                                                    'should' => [
                                                        ['term' => ['kost_level_id' => 5]]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?kost_level[]=5');
    }

    public function testIndexWithMultipleKostLevel(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'bool' => [
                                                    'should' => [
                                                        ['term' => ['kost_level_id' => 5]],
                                                        ['term' => ['kost_level_id' => 6]],
                                                        ['term' => ['kost_level_id' => 7]]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?kost_level[]=5&kost_level[]=6&kost_level[]=7');
    }

    public function testIndexWithStatus(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            ['term' => ['status' => 'booked']]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?status=booked');
    }

    public function testIndexWithSearch(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'should' => [
                                ['match' => ['tenant_name' => 'test']],
                                ['match' => ['tenant_phone' => 'test']],
                                ['match' => ['kost_name' => 'test']],
                                ['match' => ['owner_phone' => 'test']]
                            ],
                            'minimum_should_match' => 1
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?search=test');
    }

    public function testIndexWithOffset(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 10,
                    'query' => [
                        'match_all' => new stdClass()
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?offset=10');
    }

    public function testIndexWithMappedConsultant(): void
    {
        $this->markTestSkipped();
        factory(ConsultantMapping::class)->create([
            'consultant_id' => $this->user->consultant->id,
            'area_city' => 'Sleman'
        ]);

        factory(ConsultantMapping::class)->create([
            'consultant_id' => $this->user->consultant->id,
            'area_city' => 'Bantul'
        ]);

        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'body' => [
                    'sort' => [
                        ['booking_created' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                'bool' => [
                                    'must' => [
                                        [
                                            'bool' => [
                                                'should' => [
                                                    ['term' => ['area_city' => 'Sleman']],
                                                    ['term' => ['area_city' => 'Bantul']]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management?search=test');
    }

    public function testIndexShouldReturnCorrectResponse(): void
    {
        $photo = factory(Media::class)->create();

        $esResponse = [
            'took' => 2,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => null,
                'hits' => [
                    [
                        '_index' => 'mamisearch-booking',
                        '_type' => '_doc',
                        '_id' => '18054',
                        '_score' => null,
                        '_source' => [
                            'booking_id' => 1414,
                            'checkin_date' => '2020-03-31',
                            'stay_duration' => 1,
                            'stay_duration_unit' => 'monthly',
                            'user_id' => 1936947,
                            'status' => Status::BOOKED,
                            'booking_created' => '2020-03-29 11:22:31',
                            'booking_confirmed' => null,
                            'created_at' => '2020-03-29 11:22:31',
                            'kost_name' => 'Kost Gani Putri',
                            'area_city' => 'Jayapura',
                            'owner_phone' => '089604239098',
                            'song_id' => 61301818,
                            'kost_level_id' => 11,
                            'kost_level_name' => 'OYO',
                            'tenant_name' => 'Pe',
                            'tenant_phone' => '083176400123',
                            'user_photo_id' => $photo->id,
                            'is_instant_booking' => false
                        ],
                        'sort' => [
                            'verified'
                        ]
                    ]
                ]
            ]
        ];

        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => '2020-03-27 17:00:00'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_LESS_EQUAL_TEN, 'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('search')
            ->andReturn($esResponse)
            ->once();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management');

        $response->assertJson([
            'status' => true,
            'total' => 1,
            'offset' => 0,
            'limit' => 10,
            'data' => [
                [
                    'booking_id' => 1414,
                    'checkin_date' => '2020-03-31',
                    'stay_duration' => 1,
                    'stay_duration_unit' => 'monthly',
                    'user_id' => 1936947,
                    'status' => Status::BOOKED,
                    'booking_created' => '2020-03-29 11:22:31',
                    'booking_confirmed' => null,
                    'created_at' => '2020-03-29 11:22:31',
                    'kost_name' => 'Kost Gani Putri',
                    'area_city' => 'Jayapura',
                    'owner_phone' => '089604239098',
                    'song_id' => 61301818,
                    'kost_level_id' => 11,
                    'kost_level_name' => 'OYO',
                    'tenant_name' => 'Pe',
                    'tenant_phone' => '083176400123',
                    'user_photo_id' => $photo->id,
                    'is_instant_booking' => false,
                    'photo' => $photo->getMediaUrl(),
                    'due_date' => '2020-03-29 12:22:31'
                ]
            ]
        ]);
    }

    public function testIndexWithConfirmedStatus(): void
    {
        $photo = factory(Media::class)->create();

        $esResponse = [
            'took' => 2,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => null,
                'hits' => [
                    [
                        '_index' => 'mamisearch-booking',
                        '_type' => '_doc',
                        '_id' => '18054',
                        '_score' => null,
                        '_source' => [
                            'booking_id' => 1414,
                            'checkin_date' => '2020-03-31',
                            'stay_duration' => 1,
                            'stay_duration_unit' => 'monthly',
                            'user_id' => 1936947,
                            'status' => Status::CONFIRMED,
                            'booking_created' => '2020-03-31 11:22:31',
                            'booking_confirmed' => '2020-03-31 13:32:00',
                            'created_at' => '2020-03-31 11:22:31',
                            'kost_name' => 'Kost Gani Putri',
                            'area_city' => 'Jayapura',
                            'owner_phone' => '089604239098',
                            'song_id' => 61301818,
                            'kost_level_id' => 11,
                            'kost_level_name' => 'OYO',
                            'tenant_name' => 'Pe',
                            'tenant_phone' => '083176400123',
                            'user_photo_id' => $photo->id,
                            'is_instant_booking' => false
                        ],
                        'sort' => [
                            'verified'
                        ]
                    ]
                ]
            ]
        ];

        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => '2020-03-27 17:00:00'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('search')
            ->andReturn($esResponse)
            ->once();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management');

        $response->assertJson([
            'status' => true,
            'total' => 1,
            'offset' => 0,
            'limit' => 10,
            'data' => [
                [
                    'booking_id' => 1414,
                    'checkin_date' => '2020-03-31',
                    'stay_duration' => 1,
                    'stay_duration_unit' => 'monthly',
                    'user_id' => 1936947,
                    'status' => Status::CONFIRMED,
                    'booking_created' => '2020-03-31 11:22:31',
                    'booking_confirmed' => '2020-03-31 13:32:00',
                    'created_at' => '2020-03-31 11:22:31',
                    'kost_name' => 'Kost Gani Putri',
                    'area_city' => 'Jayapura',
                    'owner_phone' => '089604239098',
                    'song_id' => 61301818,
                    'kost_level_id' => 11,
                    'kost_level_name' => 'OYO',
                    'tenant_name' => 'Pe',
                    'tenant_phone' => '083176400123',
                    'user_photo_id' => $photo->id,
                    'is_instant_booking' => false,
                    'photo' => $photo->getMediaUrl(),
                    'due_date' => '2020-03-31 14:32:00'
                ]
            ]
        ]);
    }

    public function testIndexWithDueDate(): void
    {
        $photo = factory(Media::class)->create();

        $esResponse = [
            'took' => 2,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => null,
                'hits' => [
                    [
                        '_index' => 'mamisearch-booking',
                        '_type' => '_doc',
                        '_id' => '18054',
                        '_score' => null,
                        '_source' => [
                            'booking_id' => 1414,
                            'checkin_date' => '2020-03-31',
                            'stay_duration' => 1,
                            'stay_duration_unit' => 'monthly',
                            'user_id' => 1936947,
                            'status' => Status::CONFIRMED,
                            'booking_created' => '2020-03-31 11:22:31',
                            'booking_confirmed' => '2020-03-31 13:32:00',
                            'created_at' => '2020-03-31 11:22:31',
                            'kost_name' => 'Kost Gani Putri',
                            'area_city' => 'Jayapura',
                            'owner_phone' => '089604239098',
                            'song_id' => 61301818,
                            'kost_level_id' => 11,
                            'kost_level_name' => 'OYO',
                            'tenant_name' => 'Pe',
                            'tenant_phone' => '083176400123',
                            'user_photo_id' => $photo->id,
                            'is_instant_booking' => false,
                            'due_date' => '2020-03-31 14:32:00'
                        ],
                        'sort' => [
                            'verified'
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->andReturn($esResponse)
            ->once();

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management');

        $response->assertJson([
            'status' => true,
            'total' => 1,
            'offset' => 0,
            'limit' => 10,
            'data' => [
                [
                    'booking_id' => 1414,
                    'checkin_date' => '2020-03-31',
                    'stay_duration' => 1,
                    'stay_duration_unit' => 'monthly',
                    'user_id' => 1936947,
                    'status' => Status::CONFIRMED,
                    'booking_created' => '2020-03-31 11:22:31',
                    'booking_confirmed' => '2020-03-31 13:32:00',
                    'created_at' => '2020-03-31 11:22:31',
                    'kost_name' => 'Kost Gani Putri',
                    'area_city' => 'Jayapura',
                    'owner_phone' => '089604239098',
                    'song_id' => 61301818,
                    'kost_level_id' => 11,
                    'kost_level_name' => 'OYO',
                    'tenant_name' => 'Pe',
                    'tenant_phone' => '083176400123',
                    'user_photo_id' => $photo->id,
                    'is_instant_booking' => false,
                    'photo' => $photo->getMediaUrl(),
                    'due_date' => '2020-03-31 14:32:00'
                ]
            ]
        ]);
    }

    public function testGetBookingStatusAndKostLevel()
    {
        $level = factory(KostLevel::class)->create(['name' => 'super']);
        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management/list-status-level')
            ->assertOk();

        $response->assertJson([
            'status' => true,
            'data' => [
                'status' => [
                    ['key' => 'Butuh Konfirmasi', 'label' => 'Butuh Konfirmasi'],
                    ['key' => 'Tunggu Pembayaran', 'label' => 'Tunggu Pembayaran'],
                    ['key' => 'Terbayar', 'label' => 'Terbayar'],
                    ['key' => 'Pembayaran Diterima', 'label' => 'Pembayaran Diterima'],
                    ['key' => 'Sewa Berakhir', 'label' => 'Sewa Berakhir'],
                    ['key' => 'Ditolak Pemilik', 'label' => 'Ditolak Pemilik'],
                    ['key' => 'Dibatalkan', 'label' => 'Dibatalkan'],
                    ['key' => 'Kadaluwarsa oleh Penyewa', 'label' => 'Kadaluwarsa oleh Penyewa'],
                    ['key' => 'Kadaluwarsa oleh Pemilik', 'label' => 'Kadaluwarsa oleh Pemilik'],
                    ['key' => 'Ditolak Admin', 'label' => 'Ditolak Admin'],
                ],
                'level' => [
                    [
                        'id' => $level->id,
                        'name' => $level->name
                    ]
                ]
            ]
        ]);
    }

    public function testGetBookingStatusAndKostLevelShouldSortByName()
    {
        $level2 = factory(KostLevel::class)->create(['name' => 'super 2']);
        $level1 = factory(KostLevel::class)->create(['name' => 'super 1']);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', '/consultant-tools/api/booking-management/list-status-level')
            ->assertOk();

        $response->assertJson([
            'status' => true,
            'data' => [
                'status' => [
                    ['key' => 'Butuh Konfirmasi', 'label' => 'Butuh Konfirmasi'],
                    ['key' => 'Tunggu Pembayaran', 'label' => 'Tunggu Pembayaran'],
                    ['key' => 'Terbayar', 'label' => 'Terbayar'],
                    ['key' => 'Pembayaran Diterima', 'label' => 'Pembayaran Diterima'],
                    ['key' => 'Sewa Berakhir', 'label' => 'Sewa Berakhir'],
                    ['key' => 'Ditolak Pemilik', 'label' => 'Ditolak Pemilik'],
                    ['key' => 'Dibatalkan', 'label' => 'Dibatalkan'],
                    ['key' => 'Kadaluwarsa oleh Penyewa', 'label' => 'Kadaluwarsa oleh Penyewa'],
                    ['key' => 'Kadaluwarsa oleh Pemilik', 'label' => 'Kadaluwarsa oleh Pemilik'],
                    ['key' => 'Ditolak Admin', 'label' => 'Ditolak Admin'],
                ],
                'level' => [
                    [
                        'id' => $level1->id,
                        'name' => $level1->name
                    ],
                    [
                        'id' => $level2->id,
                        'name' => $level2->name
                    ]
                ]
            ]
        ]);
    }

    public function testTenantDetail()
    {
        $photoProfile = factory(Media::class)->create();
        $photoIdentifier = factory(MamipayMedia::class)->create();
        $photoSelfie = factory(MamipayMedia::class)->create();

        $user = factory(User::class)->create([
            'introduction' => 'test',
            'photo_id' => $photoProfile->id,
            'gender' => 'male',
            'marital_status' => 'Belum Kawin',
            'job' => 'karyawan'
        ]);

        factory(MamipayTenant::class)->create([
            'user_id' => $user->id,
            'photo_identifier_id' => $photoIdentifier->id,
            'photo_document_id' => $photoSelfie->id,
        ]);

        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', self::DETAIL_TENANT . $booking->id)
            ->assertOk();

        $response->assertJson([
            'status' => true,
            'data' => [
                'id' => $user->id,
                'name' => $user->name,
                'phone_number' => $user->phone_number,
                'email' => $user->email,
                'about_tenant' => $user->introduction,
                'photo_profile' => [
                    'small' => $photoProfile->getMediaUrl()['small'],
                    'medium' => $photoProfile->getMediaUrl()['medium'],
                    'large' => $photoProfile->getMediaUrl()['large'],
                ],
                'photo_identifier' => $photoIdentifier->getCachedUrlsAttribute(),
                'photo_selfie' => $photoSelfie->getCachedUrlsAttribute(),
                'gender' => 'male',
                'marital_status' => 'Belum Kawin',
                'occupation' => 'Karyawan',
            ]
        ]);
    }

    public function testTenantDetailWithUserPhoto()
    {
        $photoProfile = factory(Media::class)->create();

        $user = factory(User::class)->create([
            'introduction' => 'test',
            'photo_id' => $photoProfile->id,
            'gender' => 'male',
            'marital_status' => 'Belum Kawin',
            'job' => 'karyawan'
        ]);

        $photoIdentifier = factory(UserMedia::class)->create([
            'user_id' => $user->id,
            'description' => 'photo_identity'
        ]);

        $photoSelfie = factory(UserMedia::class)->create([
            'user_id' => $user->id,
            'description' => 'selfie_identity'
        ]);
        
        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', self::DETAIL_TENANT . $booking->id)
            ->assertOk();

        $response->assertJson([
            'status' => true,
            'data' => [
                'id' => $user->id,
                'name' => $user->name,
                'phone_number' => $user->phone_number,
                'email' => $user->email,
                'about_tenant' => $user->introduction,
                'photo_profile' => [
                    'small' => $photoProfile->getMediaUrl()['small'],
                    'medium' => $photoProfile->getMediaUrl()['medium'],
                    'large' => $photoProfile->getMediaUrl()['large'],
                ],
                'photo_identifier' => [
                    'small' => $photoIdentifier->getMediaUrl()['small'],
                    'medium' => $photoIdentifier->getMediaUrl()['medium'],
                    'large' => $photoIdentifier->getMediaUrl()['large'],
                ],
                'photo_selfie' => [
                    'small' => $photoSelfie->getMediaUrl()['small'],
                    'medium' => $photoSelfie->getMediaUrl()['medium'],
                    'large' => $photoSelfie->getMediaUrl()['large'],
                ],
                'gender' => 'male',
                'marital_status' => 'Belum Kawin',
                'occupation' => 'Karyawan',
            ]
        ]);
    }

    public function testTenantDetailWithoutPhoto()
    {
        $user = factory(User::class)->create([
            'gender' => 'male',
            'marital_status' => 'Belum Kawin',
            'job' => 'karyawan'
        ]);

        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', self::DETAIL_TENANT . $booking->id)
            ->assertOk();

        $response->assertJson([
            'status' => true,
            'data' => [
                'id' => $user->id,
                'name' => $user->name,
                'phone_number' => $user->phone_number,
                'email' => $user->email,
                'about_tenant' => $user->introduction,
                'photo_profile' => [],
                'photo_identifier' => [],
                'photo_selfie' => [],
                'gender' => $user->gender,
                'marital_status' => $user->marital_status,
                'occupation' => ucfirst($user->job),
            ]
        ]);
    }

    public function testContractDetail(): void
    {
        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => '2020-08-10 10:10:10'
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $contract = factory(MamipayContract::class)->create();

        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create([
            'charging_fee' => 3000000,
            'charging_type' => 'amount'
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $booking = factory(BookingUser::class)->create([
            'checkin_date' => '2020-08-17',
            'checkout_date' => '2020-08-18',
            'created_at' => '2020-08-17 16:16:16',
            'stay_duration' => 1,
            'rent_count_type' => 'daily',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'contract_id' => $contract->id,
            'designer_id' => $room->song_id
        ]);

        factory(MamipayBillingRule::class)->create([
            'tenant_id' => $contract->tenant_id,
            'deposit_amount' => 1000000,
            'room_id' => $booking->designer_id
        ]);

        $dp = factory(MamipayInvoice::class)->create([
            'amount' => 3000000,
            'transfer_amount' => 1500000,
            'invoice_number' => 'DP/XXXX/XXXXXXX',
            'contract_id' => $booking->contract_id
        ]);

        $st = factory(MamipayInvoice::class)->create([
            'amount' => 3000000,
            'transfer_amount' => 1500000,
            'invoice_number' => 'ST/XXXX/XXXXXXX',
            'contract_id' => $booking->contract_id
        ]);

        factory(MamipayAdditionalCost::class)->create([
            'invoice_id' => $dp->id,
            'cost_value' => 1000
        ]);

        factory(MamipayAdditionalCost::class)->create([
            'invoice_id' => $st->id,
            'cost_value' => 1000
        ]);

        $expected = [
            'due_date' => $booking->due_date->toDateTimeString(),
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'checkin_date' => '2020-08-17',
                'checkout_date' => '2020-08-18',
                'booking_created' => '2020-08-17 16:16:16',
                'stay_duration' => 1,
                'stay_duration_unit' => 'daily'
            ],
            'invoice' => [
                'full_amount' => 6000000,
                'transferred_to_owner' => 3000000,
                'down_payment' => 3000000,
                'deposit' => 1000000,
                'additional_cost' => 2000
            ]
        ];

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', "/consultant-tools/api/booking-management/detail/contract/$booking->id");

        $response->assertJson([
            'data' => $expected
        ]);
    }

    public function testContractDetailWithInvalidId(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', "/consultant-tools/api/booking-management/detail/contract/0");

        $response->assertNotFound();
        $response->assertJson([
            'message' => __('api.exception.not_found')
        ]);
    }
    
    public function testAcceptBookingWithInvalidDesignerId()
    {
        $user = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['user_id' => $user->id]);
        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'designer_id' => factory(Room::class)->create()->song_id
        ]);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post('/consultant-tools/api/booking-management/accept/' . $booking->id, ['designer_room_id' => 'a'])
            ->assertStatus(501);

        $response->assertJson([
            'status' => false,
            'errors' => 'Kamar tidak valid.'
        ]);
    }

    public function testAcceptBookingWithInvalidBookingId()
    {
        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post('/consultant-tools/api/booking-management/accept/01', [])
            ->assertStatus(404);
    }

    /**
     * @runTestsInSeparateProcesses
     */
    public function testAcceptBookingSuccess()
    {
        // define data
        $user = factory(User::class)->create(['gender' => 'perempuan']);
        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'designer_id' => factory(Room::class)->create()->song_id
        ]);

        // mock guzzle
        $guzzleBody = [
            'status' => true,
        ];
        $guzzleBody = (object) $guzzleBody;
        $guzzleResponse = new Response(200, ['Content-Type' => 'application/json'], json_encode($guzzleBody));
        $this->httpClient
            ->shouldReceive('POST')
            ->withAnyArgs()
            ->andReturn($guzzleResponse);

        $response = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post('/consultant-tools/api/booking-management/accept/' . $booking->id, []);
        $response->assertStatus(200);
    }

    public function testTenantDetailWithInvalidId()
    {
        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->json('GET', self::DETAIL_TENANT . 0)
            ->assertNotFound()
            ->assertJson([
                'message' => __('api.exception.not_found')
            ]);
    }
}
