<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Media\Media;
use App\Presenters\PotentialPropertyPresenter;
use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\PotentialProperty\DetailTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use stdClass;

class PotentialPropertyControllerTest extends MamiKosTestCase
{

    public $consultant;
    protected $elasticsearch;
    protected const POTENTIAL_PROPERTY_INDEX = '/consultant-tools/api/potential-property';
    protected const POTENTIAL_PROPERTY_DETAIL = '/consultant-tools/api/potential-property/detail/';

    protected const POTENTIAL_PROPERTY_STORE = '/consultant-tools/api/potential-property/store';
    protected const POTENTIAL_PROPERTY_UPDATE = '/consultant-tools/api/potential-property/';
    protected const POTENTIAL_PROPERTY_CREATE = '/consultant-tools/api/potential-property';

    protected function setUp(): void
    {
        // $this->markTestSkipped();

        parent::setUp();

        $this->consultant = factory(User::class)->create();
        factory(Consultant::class)->create(
            [
                'user_id' => $this->consultant->id
            ]
        );
        $role = factory(Role::class)->create();
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $this->consultant->attachRole($role);

        $this->elasticsearch = $this->mock('alias:Elasticsearch');
    }

    public function testIndex(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithEmptySearch(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?search='
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithOwnerId(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['owner_id' => 1]]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '/1'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithOffset(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 10
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?offset=10'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithSortByLastUpdated(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?sort_by=last_updated'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithSortByLowPriority(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['is_priority' => ['order' => 'asc']]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?sort_by=priority_low'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithSortByHighPriority(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'sort' => [
                            ['is_priority' => ['order' => 'desc']]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?sort_by=priority_high'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithSearch(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'query' => [
                            'bool' => [
                                'should' => [
                                    ['match' => ['name' => 'Test']],
                                    ['match' => ['area_city' => 'Test']],
                                    ['match' => ['province' => 'Test']]
                                ],
                                'minimum_should_match' => 1
                            ]
                        ],
                        'from' => 0
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?search=Test'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithCreatedBy(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['created_by' => 'owner']]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?created_by=owner'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithCreatedByOwner(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['created_by' => 'owner']]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?created_by=owner'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithCreatedByConsultant(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['created_by' => 'consultant']]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?created_by=consultant'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithLowPriority(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['is_priority' => false]]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?priority=low'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testIndexWithHighPriority(): void
    {
        $indexes = [
            "took" => 2,
            "timed_out" => false,
            "_shards" => [
                "total" => 5,
                "successful" => 5,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    [
                        "_index" => "mamisearch-potential-property",
                        "_type" => "_doc",
                        "_id" => "1",
                        "_score" => null,
                        "_source" => [
                            "id" => 1,
                            "name" => "Test",
                            "area_city" => "Bekasi",
                            "province" => "Jawa Barat",
                            "created_by" => "consultant",
                            "creator_name" => "Banana Song",
                            "is_priority" => true,
                            "owner_id" => 12,
                            "created_at" => "2020-07-17 16:07:35",
                            "updated_at" => "2020-07-17 16:07:35"
                        ],
                        "sort" => [
                            1595002055000
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with(
                [
                    'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                    'body' => [
                        'sort' => [
                            ['updated_at' => ['order' => 'desc']]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['is_priority' => true]]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            )
            ->andReturn($indexes);

        $response = $this->actingAs($this->consultant, 'passport')->setWebApiCookie()->get(
            self::POTENTIAL_PROPERTY_INDEX . '?priority=high'
        );

        $response->assertJson(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function testUpdateWithInvalidIdShouldReturn404(): void
    {
        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(self::POTENTIAL_PROPERTY_UPDATE . 0);

        $response->assertNotFound();
        $response->assertJson(['message' => __('api.exception.not_found'), 'status' => false]);
    }

    public function testUpdateWithFailedValidation(): void
    {
        $property = factory(PotentialProperty::class)->create();

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(self::POTENTIAL_PROPERTY_UPDATE . $property->id);

        $response->assertStatus(501);
        $response->assertJson(['status' => false]);
    }

    public function testUpdateShouldUpdateOwnerTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'total_room' => 3]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 5]);
    }

    public function testUpdateWithNullTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'total_room' => 3]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => null,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 0]);
    }

    public function testUpdateWithoutTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'total_room' => 3]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 0]);
    }

    public function testUpdateWithHighPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'is_priority' => false]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'is_priority' => true]);
    }

    public function testUpdateWithLowPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'is_priority' => true]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'low',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'is_priority' => false]);
    }

    public function testUpdateWithNullPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'is_priority' => false]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => null,
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'is_priority' => true]);
    }

    public function testUpdateWithoutPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'is_priority' => false]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'is_priority' => true]);
    }

    public function testUpdateWithNullPhoto(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => $owner->id]);

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => null,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'media_id' => null]);
    }

    public function testUpdateWithoutPhoto(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => $owner->id]);

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                ]
            );

        $this->assertDatabaseHas('consultant_potential_property', ['id' => $property->id, 'media_id' => null]);
    }

    public function testUpdateShouldUpdateUpdatedBy(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => $owner->id]);

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            ['id' => $property->id, 'updated_by' => $this->consultant->id]
        );
    }

    public function testUpdate(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(['consultant_potential_owner_id' => $owner->id]);

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => 'test',
                    'address' => 'Jl. Pandega',
                    'area_city' => 'Sleman',
                    'province' => 'Daerah Istimewa Yogyakarta',
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'id' => $property->id,
                'name' => 'test',
                'address' => 'Jl. Pandega',
                'area_city' => 'Sleman',
                'province' => 'Daerah Istimewa Yogyakarta',
                'total_room' => 5,
                'offered_product' => 'gp1',
            ]
        );
    }

    public function testUpdateShouldReturnData(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->create(
            ['consultant_potential_owner_id' => $owner->id, 'is_priority' => true]
        );

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->put(
                self::POTENTIAL_PROPERTY_UPDATE . $property->id,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'total_room' => $property->total_room,
                    'product_offered' => 'gp1'
                ]
            );

        $response->assertJson(
            [
                'status' => true,
                'data' => (new DetailTransformer())->transform($property->refresh())
            ]
        );
    }

    public function testCreateWithFailedValidation(): void
    {
        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(self::POTENTIAL_PROPERTY_CREATE);

        $response->assertStatus(501);
        $response->assertJson(['status' => false]);
    }

    public function testCreateShouldUpdateOwnerTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 8]);
    }

    public function testCreateWithNullTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => null,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 3]);
    }

    public function testCreateWithoutTotalRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_room' => 3]);
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_room' => 3]);
    }

    public function testCreateShouldIncrementOwnerTotalProperty(): void
    {
        $owner = factory(PotentialOwner::class)->create(['total_property' => 0]);
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas('consultant_potential_owner', ['id' => $owner->id, 'total_property' => 1]);
    }

    public function testCreateWithHighPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'high',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'media_id' => $property->media_id,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'is_priority' => true,
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateWithLowPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => 'low',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'media_id' => $property->media_id,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'is_priority' => false,
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateWithNullPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'priority' => null,
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'media_id' => $property->media_id,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'is_priority' => true,
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateWithoutPriority(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => $property->media_id,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'media_id' => $property->media_id,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateWithNullPhoto(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => null,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'media_id' => null,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateWithoutPhoto(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $property = factory(PotentialProperty::class)->make();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => $property->name,
                'address' => $property->address,
                'area_city' => $property->area_city,
                'province' => $property->province,
                'total_room' => 5,
                'offered_product' => 'gp1',
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreate(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => 'test',
                    'address' => 'Jl. Pandega',
                    'area_city' => 'Sleman',
                    'province' => 'Daerah Istimewa Yogyakarta',
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'owner_id' => $owner->id
                ]
            );

        $this->assertDatabaseHas(
            'consultant_potential_property',
            [
                'name' => 'test',
                'address' => 'Jl. Pandega',
                'area_city' => 'Sleman',
                'province' => 'Daerah Istimewa Yogyakarta',
                'total_room' => 5,
                'offered_product' => 'gp1',
                'consultant_potential_owner_id' => $owner->id,
                'created_by' => $this->consultant->id
            ]
        );
    }

    public function testCreateShouldReturnData(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->post(
                self::POTENTIAL_PROPERTY_CREATE,
                [
                    'name' => 'test',
                    'address' => 'Jl. Pandega',
                    'area_city' => 'Sleman',
                    'province' => 'Daerah Istimewa Yogyakarta',
                    'total_room' => 5,
                    'product_offered' => 'gp1',
                    'owner_id' => $owner->id
                ]
            );

        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'name' => 'test',
                    'address' => 'Jl. Pandega',
                    'area_city' => 'Sleman',
                    'province' => 'Daerah Istimewa Yogyakarta',
                    'total_room' => 5,
                    'product_offered' => ['gp1'],
                    'photo' => [],
                    'priority' => 'high',
                    'owner_name' => $owner->name,
                    'owner_phone' => $owner->phone_number
                ]
            ]
        );
    }

    public function testDetailShouldReturnCorrectData()
    {
        $media = factory(Media::class)->create();
        $urls = $media->getMediaUrl();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $media->id,
                'created_by' => factory(User::class),
                'updated_by' => factory(User::class)
            ]
        );

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->get(
                self::POTENTIAL_PROPERTY_DETAIL . $property->id
            );

        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                    'media_id' => $property->media_id,
                    'total_room' => $property->total_room,
                    'product_offered' => $property->extractOfferedProduct(),
                    'priority' => ($property->is_priority ? 'high' : 'low'),
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'notes' => $property->remark,
                    'created_at' => $property->created_at->toDateTimeString(),
                    'created_by' => $property->first_created_by->name,
                    'updated_at' => $property->updated_at->toDateTimeString(),
                    'updated_by' => $property->last_updated_by->name
                ]
            ]
        );
    }

    public function testDeleteSuccess()
    {
        $property = factory(PotentialProperty::class)->create();

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->delete(self::POTENTIAL_PROPERTY_UPDATE . $property->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('consultant_potential_property', ['id' => $property->id]);
    }

    public function testDeleteWithInvalidId()
    {
        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->delete(self::POTENTIAL_PROPERTY_UPDATE . '0')
            ->assertStatus(404)
            ->assertJson(
                [
                    'message' => __('api.exception.not_found')
                ]
            );
    }

    public function testDeleteShouldDecrementOwnerTotalPropertyAndRoom(): void
    {
        $owner = factory(PotentialOwner::class)->create(
            [
                'total_room' => 5,
                'total_property' => 1
            ]
        );
        $property = factory(PotentialProperty::class)->create(
            [
                'consultant_potential_owner_id' => $owner->id,
                'total_room' => 5
            ]
        );

        $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->delete(self::POTENTIAL_PROPERTY_UPDATE . $property->id)
            ->assertSuccessful();

        $this->assertDatabaseHas(
            'consultant_potential_owner',
            [
                'id' => $owner->id,
                'total_room' => 0,
                'total_property' => 0
            ]
        );
    }

    public function testDetailWithoutPhoto()
    {
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => 0
            ]
        );

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->get(
                self::POTENTIAL_PROPERTY_DETAIL . $property->id
            );

        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'photo' => null,
                    'total_room' => $property->total_room,
                    'product_offered' => $property->extractOfferedProduct(),
                    'priority' => ($property->is_priority ? 'high' : 'low'),
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'notes' => $property->remark,
                ]
            ]
        );
    }

    public function testDetailWithoutCreatedByAndUpdatedBy()
    {
        $property = factory(PotentialProperty::class)->create(
            [
                'created_by' => 0,
                'updated_by' => null
            ]
        );

        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->get(
                self::POTENTIAL_PROPERTY_DETAIL . $property->id
            );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'name' => $property->name,
                    'address' => $property->address,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'total_room' => $property->total_room,
                    'product_offered' => $property->extractOfferedProduct(),
                    'priority' => ($property->is_priority ? 'high' : 'low'),
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'notes' => $property->remark,
                    'created_at' => $property->created_at->toDateTimeString(),
                    'created_by' => null,
                    'updated_at' => $property->updated_at->toDateTimeString(),
                    'updated_by' => null
                ]
            ]
        );
    }

    public function testDetailWithInvalidId()
    {
        $response = $this->actingAs($this->consultant, 'passport')
            ->setWebApiCookie()
            ->get(
                self::POTENTIAL_PROPERTY_DETAIL . '0'
            );

        $response->assertNotFound();
        $response->assertJson(
            [
                'status' => false,
                'message' => 'Potential property tidak bisa ditemukan'
            ]
        );
    }
}
