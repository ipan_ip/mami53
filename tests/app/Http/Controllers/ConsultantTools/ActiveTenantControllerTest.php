<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use stdClass;

class ActiveTenantControllerTest extends MamiKosTestCase
{

    private const DEFAULT_PER_PAGE = 10;
    private const INDEX_URL = 'consultant-tools/api/tenant/active';
    private const SHOW_URL = 'consultant-tools/api/tenant/active/';

    public function testIndexAssertTrue()
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();
        
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexWithAssignedConsultantShouldReturnActiveTenant(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $typeKost = $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();
        factory(ConsultantRoom::class)->create([
            'designer_id' => $typeKost->designer_id,
            'consultant_id' => $consultant->consultant->id
        ]);
        
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexWithConsultantAssignedToOtherKostShouldNotReturnActiveTenant(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $typeKost = $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();
        factory(ConsultantRoom::class)->create([
            'designer_id' => 1,
            'consultant_id' => $consultant->consultant->id
        ]);
        
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL);
        $response->assertOk();
        $response->assertJsonMissing([
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(0, 'data');
    }

    public function testIndexAssertQueryEmailPrefixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'test@doe.com',
            'name' => 'namenamename' // Prevent incidental successful assertion through name
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryEmailSuffixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.test',
            'name' => 'namenamename' // Prevent incidental successful assertion through name
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryEmailFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@test.com',
            'name' => 'namenamename' // Prevent incidental successful assertion through name
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryEmailNotFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Email not matching %test%
            'name' => 'namenamename' // Prevent incidental successful assertion through name
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJsonMissing([
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(0, 'data');
    }

    public function testIndexAssertQueryNamePrefixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'testname'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryNameSuffixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametest'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryNameFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametestname'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryNameNotFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'namenamename' // Name not matching %test%
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=test');
        $response->assertOk();
        $response->assertJsonMissing([
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(0, 'data');
    }

    public function testIndexAssertQueryPhoneNumberPrefixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametestname', // Prevent incidental successful assertion through name
            'phone_number' => '08112345111'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=0811');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryPhoneNumberSuffixFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametestname', // Prevent incidental successful assertion through name
            'phone_number' => '08112345111'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=111');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryPhoneNumberFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametestname', // Prevent incidental successful assertion through name
            'phone_number' => '08112345111'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=2345');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 1,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(1, 'data');
    }

    public function testIndexAssertQueryPhoneNumberNotFound()
    {
        $tenant = factory(MamipayTenant::class)->create([
            'email' => 'doe@doe.com', // Prevent incidental successful assertion through email
            'name' => 'nametestname', // Prevent incidental successful assertion through name
            'phone_number' => '08112345111'
        ]);
        $contract = $this->createDummyActiveContract($tenant->id);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);
        $consultant = $this->createDummyConsultant();

        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?q=6789');
        $response->assertOk();
        $response->assertJsonMissing([
            'data' => [[
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ]]
        ]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     *  When sort is not defined in the query string, the controller will sort based on contract created at
     */
    public function testIndexAssertSortBy()
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'consultant_id' => null,
            'tenant_id' => $tenant->id,
            'created_at' => Carbon::now()->subDay(1),
        ]);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);

        $otherTenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'consultant_id' => null,
            'tenant_id' => $otherTenant->id,
            'created_at' => Carbon::now(),
        ]);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);

        $consultant = $this->createDummyConsultant();
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL);
        $response->assertOk();
        $response->assertJson([
            'data' => [
                [
                    'id' => $otherTenant->id,
                    'name' => $otherTenant->name,
                    'room_name' => $otherTenant->contracts()->first()->kost->room->name,
                    'created_at' => $otherTenant->contracts()->first()->created_at,
                    'scheduled_date' => $otherTenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ],
                [
                    'id' => $tenant->id,
                    'name' => $tenant->name,
                    'room_name' => $tenant->contracts()->first()->kost->room->name,
                    'created_at' => $tenant->contracts()->first()->created_at,
                    'scheduled_date' => $tenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ]
            ]
        ]);
        $response->assertJsonCount(2, 'data');
    }

    public function testIndexAssertSortByCreatedAt()
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'consultant_id' => null,
            'tenant_id' => $tenant->id,
            'created_at' => Carbon::now()->subDay(1),
        ]);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);

        $otherTenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'consultant_id' => null,
            'tenant_id' => $otherTenant->id,
            'created_at' => Carbon::now(),
        ]);
        $this->createDummyInvoices($contract->id);
        $this->createDummyRoom($contract->id);

        $consultant = $this->createDummyConsultant();
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?sort=created_at');
        $response->assertOk();
        $response->assertJson([
            'data' => [
                [
                    'id' => $otherTenant->id,
                    'name' => $otherTenant->name,
                    'room_name' => $otherTenant->contracts()->first()->kost->room->name,
                    'created_at' => $otherTenant->contracts()->first()->created_at,
                    'scheduled_date' => $otherTenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ],
                [
                    'id' => $tenant->id,
                    'name' => $tenant->name,
                    'room_name' => $tenant->contracts()->first()->kost->room->name,
                    'created_at' => $tenant->contracts()->first()->created_at,
                    'scheduled_date' => $tenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ]
            ]
        ]);
        $response->assertJsonCount(2, 'data');
    }

    public function testIndexAssertSortByScheduledDate()
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = $this->createDummyActiveContract($tenant->id);
        factory(MamipayInvoice::class)->create([
            'from_booking' => true,
            'contract_id' => $contract->id,
            'status' => MamipayInvoice::STATUS_UNPAID,
            'scheduled_date' => Carbon::now()
        ]);
        $this->createDummyRoom($contract->id);

        $otherTenant = factory(MamipayTenant::class)->create();
        $contract = $this->createDummyActiveContract($otherTenant->id);
        factory(MamipayInvoice::class)->create([
            'from_booking' => true,
            'contract_id' => $contract->id,
            'status' => MamipayInvoice::STATUS_UNPAID,
            'scheduled_date' => Carbon::now()->addMonth(1)
        ]);
        $this->createDummyRoom($contract->id);

        $consultant = $this->createDummyConsultant();
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?sort=scheduled_date');
        $response->assertOk();
        $response->assertJson([
            'data' => [
                [
                    'id' => $tenant->id,
                    'name' => $tenant->name,
                    'room_name' => $tenant->contracts()->first()->kost->room->name,
                    'created_at' => $tenant->contracts()->first()->created_at,
                    'scheduled_date' => $tenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ],
                [
                    'id' => $otherTenant->id,
                    'name' => $otherTenant->name,
                    'room_name' => $otherTenant->contracts()->first()->kost->room->name,
                    'created_at' => $otherTenant->contracts()->first()->created_at,
                    'scheduled_date' => $otherTenant->contracts()->first()
                        ->invoices()->latest('scheduled_date')->first()->scheduled_date
                ]
            ]
        ]);
        $response->assertJsonCount(2, 'data');
    }

    public function testIndexAssertPaginationFirstPage()
    {
        $data = [];
        factory(MamipayTenant::class, 10)->create()->each(function ($tenant) {
            $contract = $this->createDummyActiveContract($tenant->id);
            $this->createDummyInvoices($contract->id);
            $this->createDummyRoom($contract->id);

            $data[] = [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ];
        });

        $consultant = $this->createDummyConsultant();
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 10,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [$data]
        ]);
        $response->assertJsonCount(10, 'data');
    }

    public function testIndexAssertPaginationSecondPage()
    {
        $data = [];
        $missing = [];

        // These tenants will be on the first page and thus, not displayed
        factory(MamipayTenant::class, 10)->create()->each(function ($tenant) {
            $contract = $this->createDummyActiveContract($tenant->id);
            $this->createDummyInvoices($contract->id);
            $this->createDummyRoom($contract->id);

            $missing[] = [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ];
        });

        // These tenants will be on the second page and thus, displayed.
        factory(MamipayTenant::class, 10)->create()->each(function ($tenant) {
            $contract = $this->createDummyActiveContract($tenant->id);
            $this->createDummyInvoices($contract->id);
            $this->createDummyRoom($contract->id);

            $data[] = [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'room_name' => $tenant->contracts()->first()->kost->room->name,
                'created_at' => $tenant->contracts()->first()->created_at,
                'scheduled_date' => $tenant->contracts()->first()
                    ->invoices()->latest('scheduled_date')->first()->scheduled_date
            ];
        });

        $consultant = $this->createDummyConsultant();
        $response = $this->setWebApiCookie()->actingAs($consultant,'passport')->get(self::INDEX_URL . '?offset=10');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'total' => 20,
            'limit' => self::DEFAULT_PER_PAGE,
            'data' => [$data]
        ]);
        $response->assertJsonMissing([
            'data' => [$missing]
        ]);
        $response->assertJsonCount(10, 'data');
    }

    /**
     *  Create one dummy paid invoice and one dummy unpaid invoice
     *
     *  @param int $contractId
     *
     *  @return void
     */
    private function createDummyInvoices(int $contractId): void
    {
        factory(MamipayInvoice::class)->create([
            'from_booking' => true,
            'contract_id' => $contractId,
            'status' => MamipayInvoice::STATUS_PAID
        ]);

        factory(MamipayInvoice::class)->create([
            'from_booking' => true,
            'contract_id' => $contractId,
            'status' => MamipayInvoice::STATUS_UNPAID
        ]);
    }

    /**
     *  Create dummy consultant to call APIs
     *
     *  @return User
     */
    private function createDummyConsultant(): User
    {
        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $consultant = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $consultant->attachRole($role);
        $role->attachPermission($permission);
        $consultant->consultant()
            ->save(
                factory(Consultant::class)->make()
            );

        return $consultant;
    }

    /**
     *  Create dummy active contract
     *
     *  @param int $tenantId
     *
     *  @return MamipayContract
     */
    private function createDummyActiveContract(int $tenantId): MamipayContract
    {
        return factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'consultant_id' => null,
            'tenant_id' => $tenantId,
            'created_at' => Carbon::now()
        ]);
    }

    /**
     *  Create dummy room
     *
     *  @param int $contractId
     *
     *  @return MamipayContractKost
     */
    private function createDummyRoom(int $contractId): MamipayContractKost
    {
        return factory(MamipayContractKost::class)->create([
            'contract_id' => $contractId,
            'designer_id' => factory(Room::class)->create()->id
        ]);
    }
}
