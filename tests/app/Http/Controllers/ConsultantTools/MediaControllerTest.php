<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Media\Media;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class MediaControllerTest extends MamiKosTestCase
{
    protected const DELETE = '/consultant-tools/api/media/';

    protected function setUp(): void
    {
        parent::setUp();

        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $this->user->attachRole($role);
        $role->attachPermission($permission);

        Storage::fake('testing');
    }

    // public function testDeleteShouldDeleteFromStorage(): void
    // {
    //     $file = UploadedFile::fake()->image('test.jpg');

    //     $base = Config::get('api.media.folder_data');
    //     $folderType = 'consultant';
    //     $folderDate = date('Y-m-d');
    //     $location = "{$base}/{$folderType}/{$folderDate}";

    //     $file->store($location);
    //     $media = factory(Media::class)->create([
    //         'file_path' => $location,
    //         'file_name' => $file->hashName()
    //     ]);

    //     $this->setWebApiCookie()
    //         ->actingAs(
    //             $this->user
    //         )
    //         ->delete((self::DELETE . $media->id));

    //     Storage::assertMissing(($location . '/' . $file->hashName()));
    // }

    // public function testDeleteShouldDeleteDatabase(): void
    // {
    //     $file = UploadedFile::fake()->image('test.jpg');

    //     $base = Config::get('api.media.folder_data');
    //     $folderType = 'consultant';
    //     $folderDate = date('Y-m-d');
    //     $location = "{$base}/{$folderType}/{$folderDate}";

    //     $file->store($location);
    //     $media = factory(Media::class)->create([
    //         'file_path' => $location,
    //         'file_name' => $file->hashName()
    //     ]);

    //     $this->setWebApiCookie()
    //         ->actingAs(
    //             $this->user
    //         )
    //         ->delete((self::DELETE . $media->id));

    //     $this->assertSoftDeleted('media', ['id' => $media->id]);
    // }

    // public function testDeleteShouldReturnTrue(): void
    // {
    //     $file = UploadedFile::fake()->image('test.jpg');

    //     $base = Config::get('api.media.folder_data');
    //     $folderType = 'consultant';
    //     $folderDate = date('Y-m-d');
    //     $location = "{$base}/{$folderType}/{$folderDate}";

    //     $file->store($location);
    //     $media = factory(Media::class)->create([
    //         'file_path' => $location,
    //         'file_name' => $file->hashName()
    //     ]);

    //     $response = $this->setWebApiCookie()
    //         ->actingAs(
    //             $this->user
    //         )
    //         ->delete((self::DELETE . $media->id));

    //     $response->assertJson(['status' => true]);
    // }

    public function testDeleteWithInvalidIdShouldReturnNotFound(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,'passport'
            )
            ->delete((self::DELETE . 0));

        $response->assertNotFound();
        $response->assertJson([
            'message' => __('api.exception.not_found')
        ]);
    }
}
