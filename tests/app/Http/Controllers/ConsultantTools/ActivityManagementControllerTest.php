<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityProgressDetail;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

class ActivityManagementControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();
        
        // temporary solution, skip failed test due to migrate to paratest
        $this->markTestSkipped('Skip test for now');

        $this->consultant = factory(User::class)->create();
        $this->consultant->consultant()->save(factory(Consultant::class)->make());
        $role = factory(Role::class)->create();
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $this->consultant->attachRole($role);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        Mockery::close();
    }

    public function testShowAllTaskShouldReturnCorrectProgress(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $task = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id,
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [],
                        'tasks_count' => 0,
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1,
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0,
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithBacklogShouldReturnCorrectProgress(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1,
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0,
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0,
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturnFinishedProgress(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $task = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [],
                        'tasks_count' => 0,
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0,
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 100
                            ]
                        ],
                        'tasks_count' => 1,
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldNotShowDeletedBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(Room::class)->create()->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.0.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithPotentialTenantShouldNotShowDeletedBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $potential = factory(PotentialTenant::class)->create();
        factory(PotentialTenant::class)->create()->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.0.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $potential->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithContractShouldNotShowDeletedBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        factory(MamipayContract::class)->create(['tenant_id' => $tenant->id])->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.0.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $tenant->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithPotentialOwnerhouldNotShowDeletedBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $expected = factory(PotentialOwner::class)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.0.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $expected->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [],
                        'tasks_count' => 0
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldShowDeletedToDo(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherRoom = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $otherRoom->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        )->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.1.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherRoom->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [],
                        'tasks_count' => 0
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldShowDeletedTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherRoom = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $otherRoom->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        )->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(1, 'stages.2.tasks');
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherRoom->name,
                                'progress' => 0
                            ]
                        ]
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => []
                    ],
                    [
                        'name' => $otherForm->name,
                        'detail' => $otherForm->detail,
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 100
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturn10BacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(Room::class, 12)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.0.tasks');
    }

    public function testShowAllTaskShouldWithPotentialTenantShouldReturn10BacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(PotentialTenant::class, 12)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.0.tasks');
    }

    public function testShowAllTaskShouldWithPotentialPropertyShouldReturn10BacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(PotentialProperty::class, 12)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.0.tasks');
    }

    public function testShowAllTaskShouldWithContractShouldReturn10BacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(MamipayTenant::class, 12)->create()->each(
            function ($tenant) {
                $tenant->contracts()->save(factory(MamipayContract::class)->make());
            }
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.0.tasks');
    }

    public function testShowAllTaskShouldReturn10ToDoTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(ActivityProgress::class, 12)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        )->each(
            function ($task) {
                $task->progressable_type = 'App\Entities\Room\Room';
                $task->progressable_id = factory(Room::class)->create()->id;
                $task->save();
            }
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.1.tasks');
    }

    public function testShowAllTaskShouldReturn10OtherStageTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        factory(ActivityProgress::class, 12)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'consultant_id' => $this->consultant->consultant->id
            ]
        )->each(
            function ($task) {
                $task->progressable_type = 'App\Entities\Room\Room';
                $task->progressable_id = factory(Room::class)->create()->id;
                $task->save();
            }
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonCount(10, 'stages.2.tasks');
    }

    public function testShowAllTaskWithOtherConsultantTaskShouldReturnBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 5,
            'consultant_id' => 0
        ]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithOwnTaskShouldNotReturnAsBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 5,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJsonMissing(
            [
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithOtherFunnelTaskShouldReturnAsBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $otherFunnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $otherFunnel->id,
            'current_stage' => 5,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturnBacklogForDeletedTask(): void
    {
        $otherFunnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)
            ->create(
                [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'current_stage' => 5,
                'consultant_id' => $this->consultant->consultant->id
                ]
            )
            ->delete();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $otherFunnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ],
                        'tasks_count' => 1
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturnBacklogSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $otherRoom = factory(Room::class)->create(
            [
                'updated_at' => Carbon::now()->addMinute()
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherRoom->name,
                                'progress' => 0,
                            ],
                            [
                                'name' => $room->name,
                                'progress' => 0
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithPotentialTenantShouldReturnBacklogSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $potential = factory(PotentialTenant::class)->create();
        $otherPotential = factory(PotentialTenant::class)->create(
            [
                'updated_at' => Carbon::now()->addMinute()
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherPotential->name,
                                'progress' => 0,
                            ],
                            [
                                'name' => $potential->name,
                                'progress' => 0
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithPotentialPropertyShouldReturnBacklogSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $potential = factory(PotentialProperty::class)->create();
        $otherPotential = factory(PotentialProperty::class)->create(
            [
                'updated_at' => Carbon::now()->addMinute()
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherPotential->name,
                                'progress' => 0,
                            ],
                            [
                                'name' => $potential->name,
                                'progress' => 0
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskWithContractShouldReturnBacklogSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $otherTenant = factory(MamipayTenant::class)->create();
        factory(MamipayContract::class)->create(
            [
                'updated_at' => Carbon::now()->addMinute(),
                'tenant_id' => $otherTenant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => [
                            [
                                'name' => $otherTenant->name,
                                'progress' => 0,
                            ],
                            [
                                'name' => $tenant->name,
                                'progress' => 0
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturnToDoSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherRoom = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 0,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $otherRoom->id,
                'updated_at' => Carbon::now()->addMinute(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => []
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => [
                            [
                                'name' => $otherRoom->name,
                            ],
                            [
                                'name' => $room->name
                            ]
                        ]
                    ],
                ]
            ]
        );
    }

    public function testShowAllTaskShouldReturnOtherTaskSortByUpdatedAt(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherRoom = factory(Room::class)->create();
        factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 1,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $otherRoom->id,
                'updated_at' => Carbon::now()->addMinute(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/tasks'
        );

        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'name' => 'Daftar Tugas',
                        'tasks' => []
                    ],
                    [
                        'name' => 'Tugas Aktif',
                        'tasks' => []
                    ],
                    [
                        'name' => $form->name,
                        'tasks' => [
                            [
                                'name' => $otherRoom->name,
                            ],
                            [
                                'name' => $room->name
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testSearchTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson(
            [
                'status' => true,
                'count' => 1,
                'funnel_id' => $funnel->id,
                'tasks' => [
                    [
                        'id' => $progress->id,
                        'progressable_id' => $contract->id,
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithContract(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_CONTRACT
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id
        ]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $contract->id,
                    'name' => $tenant->name,
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskWithPotentialTenant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $tenant->id,
                    'name' => $tenant->name,
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskWithPotentialOwner(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(PotentialOwner::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_owner',
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $tenant->id,
                    'name' => $tenant->name,
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskWithRoom(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $room->id,
                    'name' => $room->name,
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskWithBacklogShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // To Do task
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/backlog'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => null,
                    'progressable_id' => $contract->id,
                    'name' => $tenant->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskWithBacklogAndPotentialTenantShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // To Do task
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $tenant = factory(PotentialTenant::class)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/backlog'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => null,
                    'progressable_id' => $tenant->id,
                    'name' => $tenant->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskWithBacklogAndRoomShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // To Do task
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $room = factory(Room::class)->create();
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/backlog'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => null,
                    'progressable_id' => $room->id,
                    'name' => $room->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskWithToDoShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // Backlog task
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);

        // To Do task
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/todo'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $contract->id,
                    'name' => $tenant->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskWithToDoAndPotentialTenantShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // Backlog task
        $tenant = factory(PotentialTenant::class)->create();

        // To Do task
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenant->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/todo'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $tenant->id,
                    'name' => $tenant->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskWithToDoAndRoomShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        // Other stage task
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        // Backlog task
        $room = factory(Room::class)->create();

        // To Do task
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/todo'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $room->id,
                    'name' => $room->name,
                    'progress' => 0
                ]
            ]
        ]);
    }

    public function testSearchTaskShouldReturnCorrectAmount(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $progress = factory(ActivityProgress::class, 12)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        )->each(
            function ($task) {
                $task->progressable_id = factory(MamipayContract::class)->create(
                    [
                        'tenant_id' => factory(MamipayTenant::class)->create()->id
                    ]
                )->id;
                $task->save();
            }
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJsonCount(10, 'tasks');
    }

    public function testSearchTaskWithKeywordShouldNotReturnUnmatchedTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => 'mamipay_contract'
        ]);

        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        $otherProgress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => factory(MamipayContract::class)->create([
                'tenant_id' => factory(MamipayTenant::class)->create([
                    'name' => 'tist'
                ])->id
            ])->id,
            'current_stage' => $form->stage,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => factory(MamipayTenant::class)->create(['name' => 'test'])->id
        ]);

        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contract->id,
            'current_stage' => $form->stage,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id . '?q=test'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 1,
            'offset' => 0,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $contract->id,
                    'name' => 'test',
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskShouldReturnCorrectProgress(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 2
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => 1,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJson([
            'status' => true,
            'count' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $contract->id,
                    'name' => $tenant->name,
                    'progress' => 50
                ]
            ]
        ]);
    }

    public function testSearchTaskWithOffsetShouldNotReturnOffsetTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => 'mamipay_contract'
        ]);

        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        $otherProgress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => factory(MamipayContract::class)->create([
                'tenant_id' => factory(MamipayTenant::class)->create([
                    'name' => 'tist'
                ])->id
            ])->id,
            'current_stage' => $form->stage,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => factory(MamipayTenant::class)->create(['name' => 'test'])->id,
            'updated_at' => Carbon::now()->subHour()
        ]);
        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contract->id,
            'current_stage' => $form->stage,
            'consultant_id' => $this->consultant->consultant->id,
        ]);
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id . '?offset=1'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson([
            'status' => true,
            'count' => 2,
            'offset' => 1,
            'funnel_id' => $funnel->id,
            'tasks' => [
                [
                    'id' => $progress->id,
                    'progressable_id' => $contract->id,
                    'name' => 'test',
                    'progress' => 100
                ]
            ]
        ]);
    }

    public function testSearchTaskShouldNotRetrieveTaskFromOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );

        $otherStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 2
            ]
        );
        $otherTenant = factory(MamipayTenant::class)->create();
        $otherContract = factory(MamipayContract::class)->create(['tenant_id' => $otherTenant->id]);
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $otherContract->id,
                'current_stage' => $otherStage->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => factory(MamipayTenant::class)->create()->id]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $otherTenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskShouldNotRetrieveTaskFromOtherFunnel(): void
    {
        $otherFunnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $otherStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'stage' => 1
            ]
        );
        $otherTenant = factory(MamipayTenant::class)->create();
        $otherContract = factory(MamipayContract::class)->create(['tenant_id' => $otherTenant->id]);
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $otherContract->id,
                'current_stage' => $otherStage->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => factory(MamipayTenant::class)->create()->id]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $otherTenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskInBacklogAndOtherFunnel(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 0
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskInToDoAndOtherFunnel(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'todo'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 0
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskInOtherFunnel(): void
    {
        $otherFunnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $otherForm = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'stage' => 1
            ]
        );
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $otherFunnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $otherForm->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $otherFunnel->id . '/' . $otherForm->id
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskInBacklogAndTaskByOtherConsultant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => 0
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 0
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskInToDoAndTaskByOtherConsultant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => 0,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => 0,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => 0
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'todo'
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 0
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithTaskByOtherConsultant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $otherProgress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => 0
            ]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . $form->id
        );

        $response->assertJsonCount(1, 'tasks');
        $response->assertJson(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithExistingContractTaskShouldNotReturnInBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id]
        );
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contract->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(0, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithExistingRoomTaskShouldNotReturnInBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $room = factory(Room::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => $room->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(0, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $room->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithExistingPotentialTenantTaskShouldNotReturnInBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(PotentialTenant::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(0, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testSearchTaskWithExistingPotentialPropertyShouldNotReturnInBacklog(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
            ]
        );
        $form = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $tenant = factory(PotentialProperty::class)->create();
        $progress = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => $tenant->id,
                'current_stage' => $form->stage,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/' . $funnel->id . '/' . 'backlog'
        );

        $response->assertJsonCount(0, 'tasks');
        $response->assertJsonMissing(
            [
                'tasks' => [
                    [
                        'name' => $tenant->name,
                        'progress' => 100
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithMissingFunnelId(): void
    {
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'tasks' => [1]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'funnel_id' => [
                        'The funnel id field is required.'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithInvalidFunnelId(): void
    {
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => 0,
                    'tasks' => [1]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'funnel_id' => [
                        'The selected funnel id is invalid.'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithMissingTasksField(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks' => [
                        'The tasks field is required.'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithNonArrayTasks(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => 'test'
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks' => [
                        'The tasks must be an array.'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithNonIntegerArray(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => ['test']
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks.0' => [
                        'The tasks.0 must be a number.',
                        'The tasks.0 must be an integer.'
                    ]
                ]
            ]
        );
    }

    public function testTasksActicationWithOneInvalidPotentialTenantId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        $tenants = factory(PotentialTenant::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => [$tenants[0]->id, 0, $tenants[1]->id]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks.*' => [
                        'Invalid task id'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithInvalidContractId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $contracts = factory(MamipayContract::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => [$contracts[0]->id, 0, $contracts[1]->id]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks.*' => [
                        'Invalid task id'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithInvalidRoomId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $rooms = factory(Room::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => [$rooms[0]->id, 0, $rooms[1]->id]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'tasks.*' => [
                        'Invalid task id'
                    ]
                ]
            ]
        );
    }

    public function testTasksActivationWithRooms(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $rooms = factory(Room::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$rooms[0]->id, $rooms[1]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'App\Entities\Room\Room')
            ->whereIn('progressable_id', [$rooms[0]->id, $rooms[1]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'App\Entities\Room\Room',
            'progressable_id' => $rooms[0]->id,
            'current_stage' => 0
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'App\Entities\Room\Room',
            'progressable_id' => $rooms[1]->id,
            'current_stage' => 0
        ]);
        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithPotentialProperty(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
            ]
        );
        $rooms = factory(PotentialProperty::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$rooms[0]->id, $rooms[1]->id]
            ]);
        $taskIds = ActivityProgress::where('progressable_type', ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY)
            ->whereIn('progressable_id', [$rooms[0]->id, $rooms[1]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $rooms[0]->id,
            'current_stage' => 0
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $rooms[1]->id,
            'current_stage' => 0
        ]);
        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithPotentialTenant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        $tenants = factory(PotentialTenant::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/tasks/activate',
                [
                    'funnel_id' => $funnel->id,
                    'tasks' => [$tenants[0]->id, $tenants[1]->id]
                ]
            );

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'potential_tenant',
            'progressable_id' => $tenants[0]->id,
            'current_stage' => 0
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'potential_tenant',
            'progressable_id' => $tenants[1]->id,
            'current_stage' => 0
        ]);
    }

    public function testTasksActicationWithContract(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $contracts = factory(MamipayContract::class, 2)->create();
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$contracts[0]->id, $contracts[1]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'mamipay_contract')
            ->whereIn('progressable_id', [$contracts[0]->id, $contracts[1]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contracts[0]->id,
            'current_stage' => 0
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contracts[1]->id,
            'current_stage' => 0
        ]);
        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithSoftDeletedPotentialTenant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_DBET
            ]
        );
        $tenants = factory(PotentialTenant::class, 4)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenants[2]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherSoftDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'potential_tenant',
                'progressable_id' => $tenants[3]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$tenants[0]->id, $tenants[1]->id, $tenants[2]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'mamipay_contract')
            ->whereIn('progressable_id', [$tenants[0]->id, $tenants[1]->id, $tenants[2]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'potential_tenant',
            'progressable_id' => $tenants[0]->id,
            'current_stage' => 0,
        ]);
        $this->assertSoftDeleted('consultant_activity_progress', [
            'id' => $otherSoftDeleted->id,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'potential_tenant',
            'progressable_id' => $tenants[1]->id,
            'current_stage' => 0
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithSoftDeletedRoom(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $rooms = factory(Room::class, 4)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $rooms[2]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherSoftDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $rooms[3]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$rooms[0]->id, $rooms[1]->id, $rooms[2]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'App\Entities\Room\Room')
            ->whereIn('progressable_id', [$rooms[0]->id, $rooms[1]->id, $rooms[2]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'App\Entities\Room\Room',
            'progressable_id' => $rooms[0]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertSoftDeleted('consultant_activity_progress', [
            'id' => $otherSoftDeleted->id,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'App\Entities\Room\Room',
            'progressable_id' => $rooms[1]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithSoftDeletedRoomOnly(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_PROPERTY
            ]
        );
        $room = factory(Room::class)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'App\Entities\Room\Room',
                'progressable_id' => $room->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$room->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'App\Entities\Room\Room')
            ->whereIn('progressable_id', [$room->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTasksActivationWithSoftDeletedPotentialProperty(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
            ]
        );
        $rooms = factory(PotentialProperty::class, 4)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => $rooms[2]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherSoftDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => $rooms[3]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$rooms[0]->id, $rooms[1]->id, $rooms[2]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY)
            ->whereIn('progressable_id', [$rooms[0]->id, $rooms[1]->id, $rooms[2]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $rooms[0]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertSoftDeleted('consultant_activity_progress', [
            'id' => $otherSoftDeleted->id,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $rooms[1]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTaskActivationWithSoftDeletedPotentialOwner(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER
            ]
        );
        $owners = factory(PotentialOwner::class, 4)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
                'progressable_id' => $owners[2]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherSoftDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
                'progressable_id' => $owners[3]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$owners[0]->id, $owners[1]->id, $owners[2]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'mamipay_contract')
            ->whereIn('progressable_id', [$owners[0]->id, $owners[1]->id, $owners[2]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owners[0]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertSoftDeleted('consultant_activity_progress', [
            'id' => $otherSoftDeleted->id,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owners[1]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTaskActivationWithSoftDeletedContract(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(
            [
                'related_table' => 'mamipay_contract'
            ]
        );
        $contracts = factory(MamipayContract::class, 4)->create();
        $softDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contracts[2]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $otherSoftDeleted = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => $contracts[3]->id,
                'current_stage' => 0,
                'deleted_at' => Carbon::now(),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $response = $this->actingAs($this->consultant)
            ->post('/consultant-tools/api/activity-management/tasks/activate', [
                'funnel_id' => $funnel->id,
                'tasks' => [$contracts[0]->id, $contracts[1]->id, $contracts[2]->id]
            ]);

        $taskIds = ActivityProgress::where('progressable_type', 'mamipay_contract')
            ->whereIn('progressable_id', [$contracts[0]->id, $contracts[1]->id, $contracts[2]->id])
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contracts[0]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertSoftDeleted('consultant_activity_progress', [
            'id' => $otherSoftDeleted->id,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeleted->id,
            'deleted_at' => null,
            'consultant_id' => $this->consultant->consultant->id
        ]);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_id' => $this->consultant->consultant->id,
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => 'mamipay_contract',
            'progressable_id' => $contracts[1]->id,
            'current_stage' => 0,
            'consultant_id' => $this->consultant->consultant->id
        ]);

        $response->assertJson([
            'task_ids' => $taskIds
        ]);
    }

    public function testTaskStatusWithInvalidTaskId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $response = $this->actingAs($this->consultant)
            ->get('/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . (- 1) . '/status');

        $response->assertJson(['status' => false]);
    }

    public function testTaskStatus(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $completedStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );
        $currentStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 2
            ]
        );
        $incompleteStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 3
            ]
        );
        $task = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'current_stage' => 2
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->get('/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . $task->id . '/status');
        $response->assertJson([
            'status' => true,
            'funnel_name' => $funnel->name,
            'current_stage' => [
                'name' => $currentStage->name,
                'detail' => $currentStage->detail,
                'number' => 3
            ],
            'progress' => [
                [
                    'stage' => 1,
                    'completed' => true
                ],
                [
                    'stage' => $completedStage->stage + 1,
                    'completed' => true
                ],
                [
                    'stage' => $currentStage->stage + 1,
                    'completed' => true,
                ],
                [
                    'stage' => $incompleteStage->stage + 1,
                    'completed' => false
                ]
            ]
        ]);
    }

    public function testTaskStatusWithTugasAktifTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        $anotherStage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0
        ]);

        $response = $this->actingAs($this->consultant)
            ->get('/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . $task->id . '/status');
        $response->assertJson([
            'status' => true,
            'funnel_name' => $funnel->name,
            'current_stage' => [
                'name' => 'Tugas Aktif',
                'detail' => '',
                'number' => 1
            ],
            'progress' => [
                [
                    'stage' => 1,
                    'completed' => true
                ],
                [
                    'stage' => $stage->stage + 1,
                    'completed' => false
                ],
                [
                    'stage' => $anotherStage->stage + 1,
                    'completed' => false,
                ]
            ]
        ]);
    }

    public function testTaskStatusWithBacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        $anotherStage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);

        $response = $this->actingAs($this->consultant)
            ->get('/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . 0 . '/status');
        $response->assertJson([
            'status' => true,
            'funnel_name' => $funnel->name,
            'current_stage' => [
                'name' => 'Daftar Tugas',
                'detail' => '',
                'number' => 1
            ],
            'progress' => [
                [
                    'stage' => 1,
                    'completed' => false,
                ],
                [
                    'stage' => $stage->stage + 1,
                    'completed' => false
                ],
                [
                    'stage' => $anotherStage->stage + 1,
                    'completed' => false,
                ]
            ]
        ]);
    }

    public function testShowForm(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1,
                'form_element' => [
                    [
                        'id' => 4,
                        'type' => 'text',
                        'label' => 'Nama Kos',
                        'placeholder' => 'Nama Kos',
                        'values' => [],
                        'order' => 3
                    ],
                    [
                        'id' => 5,
                        'type' => 'radio',
                        'label' => 'Jenis Kelamin',
                        'placeholder' => 'Jenis Kelamin',
                        'values' => [
                            'Laki-laki',
                            'Perempuan'
                        ],
                        'order' => 4
                    ]
                ]
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->get('/consultant-tools/api/activity-management/stage/' . $stage->id . '/form');

        $response->assertJson(
            [
                'form' => [
                    [
                        'id' => 4,
                        'type' => 'text',
                        'label' => 'Nama Kos',
                        'placeholder' => 'Nama Kos',
                        'values' => [],
                        'order' => 3
                    ],
                    [
                        'id' => 5,
                        'type' => 'radio',
                        'label' => 'Jenis Kelamin',
                        'placeholder' => 'Jenis Kelamin',
                        'values' => [
                            'Laki-laki',
                            'Perempuan'
                        ],
                        'order' => 4
                    ]
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithInvalidId(): void
    {
        $form = factory(ActivityForm::class)->create();
        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/task/' . 0,
                ['stage_id' => $form->id]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'taskId' => [
                        'Task id could not be found'
                    ]
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithoutInput(): void
    {
        $task = factory(ActivityProgress::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id
        ]);

        $response = $this->actingAs($this->consultant)
            ->post(
                '/consultant-tools/api/activity-management/task/' . $task->id,
                ['stage_id' => $form->id]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'inputs' => [
                        'The inputs field is required.'
                    ]
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithEmptyInputs(): void
    {
        $task = factory(ActivityProgress::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id
        ]);

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $form->id,
                    'inputs' => []
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'inputs' => [
                        'The inputs field is required.'
                    ]
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithInvalidInputs(): void
    {
        $task = factory(ActivityProgress::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id
        ]);

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $form->id,
                    'inputs' => [1]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'inputs.0' => [
                        'The inputs.0 must be an array.'
                    ],
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithEmptyInputsArray(): void
    {
        $task = factory(ActivityProgress::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id
        ]);

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $form->id,
                    'inputs' => [[]]
                ]
            );

        $response->assertJson(
            [
                'status' => false,
                'messages' => [
                    'inputs.0.id' => [
                        'The inputs.0.id field is required.'
                    ],
                    'inputs.0.value' => [
                        'The inputs.0.value field is required when inputs.0.media_id is not present.'
                    ]
                ]
            ]
        );
    }

    public function testTaskCreateOrUpdateWithInvalidStageId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => 0,
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'test'
                        ]
                    ]
                ]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'stage_id' => [
                    'The selected stage id is invalid.'
                ]
            ]
        ]);
    }

    public function testTaskCreateOrUpdateWithoutStageId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'test'
                        ]
                    ]
                ]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'stage_id' => [
                    'The stage id field is required.'
                ]
            ]
        ]);
    }

    public function testTaskCreateOrUpdateWithoutExistingProgressDetail(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => 'text',
                        'label' => 'Nama Kos',
                        'placeholder' => 'Nama Kos',
                        'values' => [],
                        'order' => 3
                    ],
                    [
                        'id' => 2,
                        'type' => 'radio',
                        'label' => 'Jenis Kelamin',
                        'placeholder' => 'Jenis Kelamin',
                        'values' => [
                            'Laki-laki',
                            'Perempuan'
                        ],
                        'order' => 4
                    ]
                ]
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $stage->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'Kost Abepura Selatan'
                        ],
                        [
                            'id' => 2,
                            'value' => 'Laki-Laki'
                        ]
                    ]
                ]
            );

        $taskData = ActivityProgressDetail::where('consultant_activity_form_id', $stage->id)
            ->where('consultant_activity_progress_id', $task->id)
            ->first()
            ->value;

        $this->assertSame(
            [
                [
                    'id' => 1,
                    'value' => 'Kost Abepura Selatan'
                ],
                [
                    'id' => 2,
                    'value' => 'Laki-Laki'
                ]
            ],
            $taskData
        );

        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $task->id,
            'current_stage' => 1,
        ]);
    }

    public function testTaskCreateOrUpdateWithExistingProgressDetail(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => 'text',
                        'label' => 'Nama Kos',
                        'placeholder' => 'Nama Kos',
                        'values' => [],
                        'order' => 3
                    ],
                    [
                        'id' => 2,
                        'type' => 'radio',
                        'label' => 'Jenis Kelamin',
                        'placeholder' => 'Jenis Kelamin',
                        'values' => [
                            'Laki-laki',
                            'Perempuan'
                        ],
                        'order' => 4
                    ]
                ]
            ]
        );
        $existingTaskData = factory(ActivityProgressDetail::class)->create(
            [
                'consultant_activity_progress_id' => $task->id,
                'consultant_activity_form_id' => $stage->id,
                'value' => [
                    [
                        'id' => 1,
                        'value' => 'Kost Abepura Selatan'
                    ],
                    [
                        'id' => 2,
                        'value' => 'Laki-Laki'
                    ]
                ]
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $stage->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'Kost Abepura Utara'
                        ],
                        [
                            'id' => 2,
                            'value' => 'Perempuan'
                        ]
                    ]
                ]
            );

        $taskData = ActivityProgressDetail::where('consultant_activity_form_id', $stage->id)
            ->where('consultant_activity_progress_id', $task->id)
            ->first()
            ->value;
        $taskCount = ActivityProgressDetail::where('consultant_activity_form_id', $stage->id)
            ->where('consultant_activity_progress_id', $task->id)
            ->count();

        $this->assertSoftDeleted(
            'consultant_activity_progress_detail',
            [
                'id' => $existingTaskData->id
            ]
        );
        $this->assertSame(
            [
                [
                    'id' => 1,
                    'value' => 'Kost Abepura Utara'
                ],
                [
                    'id' => 2,
                    'value' => 'Perempuan'
                ]
            ],
            $taskData
        );
    }

    public function testTaskCreateOrUpdateShouldIncrementStage(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
            ]
        );
        $otherStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 2,
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $stage->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'Kost Abepura Utara'
                        ],
                        [
                            'id' => 2,
                            'value' => 'Perempuan'
                        ]
                    ]
                ]
            );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'current_stage' => 1
            ]
        );
    }

    public function testTaskCreateOrUpdateShouldIncrementStageToLastStage(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
            ]
        );
        $otherStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 2,
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $otherStage->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'Kost Abepura Utara'
                        ],
                        [
                            'id' => 2,
                            'value' => 'Perempuan'
                        ]
                    ]
                ]
            );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'current_stage' => 2
            ]
        );
    }

    public function testTaskCreateOrUpdateWithInvalidMediaId(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $stage->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'media_id' => 0
                        ],
                    ]
                ]
            );

        $response->assertJson([
            'status' => false,
            'messages' => [
                'inputs.0.media_id' => [
                    'The selected inputs.0.media_id is invalid.'
                ]
            ]
        ]);
    }

    public function testTaskCreateOrUpdateWithMediaId(): void
    {
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => 1000,
            'stage' => 1,
        ]);
        $task = factory(ActivityProgress::class)->create(
            [
                'consultant_activity_funnel_id' => 1000,
                'current_stage' => 0
            ]
        );

        $media = factory(Media::class)->create();

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'stage_id' => $form->id,
                    'inputs' => [
                        [
                            'id' => 1,
                            'media_id' => $media->id
                        ],
                    ]
                ]
            );

        $response->assertJson([
            'status' => true,
        ]);

        $detail = ActivityProgressDetail::where('consultant_activity_progress_id', $task->id)
            ->where('consultant_activity_form_id', $form->id);
        
        $this->assertSame(1, $detail->count());
        $this->assertSame(
            [
                [
                    'id' => 1,
                    'media_id' => $media->id
                ]
            ],
            $detail->first()->value
        );
    }

    public function testTaskCreateOrUpdateShouldNotIncrementLastStage(): void
    {
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 2
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 1,
            ]
        );
        $otherStage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
                'stage' => 2,
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->post(
                ('/consultant-tools/api/activity-management/task/' . $task->id),
                [
                    'inputs' => [
                        [
                            'id' => 1,
                            'value' => 'Kost Abepura Utara'
                        ],
                        [
                            'id' => 2,
                            'value' => 'Perempuan'
                        ]
                    ]
                ]
            );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'current_stage' => 2
            ]
        );
    }

    public function testDetailData()
    {
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/funnel/1/data/1'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'data' => []
            ]
        );
    }

    public function testProgressDetailValue()
    {
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/progress/detail/1'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'data' => []
            ]
        );
    }

    public function testGetTaskStagesWithInvalidFunnelId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0,
                'consultant_activity_funnel_id' => $funnel->id
            ]
        );

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/task/0/' . $task->id . '/stages'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => false
            ]
        );
    }

    public function testGetTaskStagesWithInvalidTaskId(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/task/' . $funnel->id . '/-1/stages'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => false
            ]
        );
    }

    public function testGetTaskStagesWithBacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/task/' . $funnel->id . '/0/stages'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'id' => null,
                        'name' => 'Daftar Tugas',
                        'is_current_stage' => true,
                    ],
                    [
                        'id' => null,
                        'name' => 'Tugas Aktif',
                        'is_current_stage' => false,
                    ],
                    [
                        'id' => $stage->id,
                        'name' => $stage->name,
                        'is_current_stage' => false,
                    ]
                ]
            ]
        );
    }

    public function testGetTaskStagesWithToDoTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 0,
                'consultant_activity_funnel_id' => $funnel->id
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . $task->id . '/stages'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'id' => null,
                        'name' => 'Daftar Tugas',
                        'is_current_stage' => false,
                    ],
                    [
                        'id' => null,
                        'name' => 'Tugas Aktif',
                        'is_current_stage' => true,
                    ],
                    [
                        'id' => $stage->id,
                        'name' => $stage->name,
                        'is_current_stage' => false,
                    ]
                ]
            ]
        );
    }

    public function testGetTaskStages(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $task = factory(ActivityProgress::class)->create(
            [
                'current_stage' => 1,
                'consultant_activity_funnel_id' => $funnel->id
            ]
        );
        $stage = factory(ActivityForm::class)->create(
            [
                'consultant_activity_funnel_id' => $funnel->id,
                'stage' => 1
            ]
        );

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/task/' . $funnel->id . '/' . $task->id . '/stages'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => true,
                'stages' => [
                    [
                        'id' => null,
                        'name' => 'Daftar Tugas',
                        'is_current_stage' => false,
                    ],
                    [
                        'id' => null,
                        'name' => 'Tugas Aktif',
                        'is_current_stage' => false,
                    ],
                    [
                        'id' => $stage->id,
                        'name' => $stage->name,
                        'is_current_stage' => true,
                    ]
                ]
            ]
        );
    }

    public function testMoveTaskToBacklog(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/backlog',
            ['task_id' => $task->id]
        );

        $response->assertSuccessful();
        $response->assertJson(['status' => true]);

        $this->assertSoftDeleted('consultant_activity_progress', ['id' => $task->id]);
    }

    public function testMoveTaskToBacklogWithMissingTaskId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/backlog',
            []
        );

        $response->assertSuccessful();
        $response->assertJson([
            'status' => false,
            'message' => 'The task id field is required.'
        ]);

        $this->assertDatabaseHas('consultant_activity_progress', ['id' => $task->id]);
    }

    public function testMoveTaskToBacklogWithInvalidTaskId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/backlog',
            ['task_id' => 'a']
        );

        $response->assertSuccessful();
        $response->assertJson([
            'status' => false,
            'message' => 'The task id must be an integer.'
        ]);

        $this->assertDatabaseHas('consultant_activity_progress', ['id' => $task->id]);
    }

    public function testMoveTaskToBacklogWithIncorrectId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/backlog',
            ['task_id' => 0]
        );

        $response->assertSuccessful();
        $response->assertJson([
            'status' => false,
            'message' => 'The selected task id is invalid.'
        ]);

        $this->assertDatabaseHas('consultant_activity_progress', ['id' => $task->id]);
    }

    public function testProgressHistoryProgressNotFoundShouldReturnStatusFalse(): void
    {
        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/progress/history/1'
        );

        $response->assertSuccessful();
        $response->assertJson(
            [
                'status' => false,
                'message' => 'Progress tidak ditemukan'
            ]
        );
    }

    public function testProgressHistoryShouldReturnNotEmptyData(): void
    {
        $progress = factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 0, 'consultant_activity_funnel_id' => 2]
        );

        $response = $this->actingAs($this->consultant)->get(
            '/consultant-tools/api/activity-management/progress/history/10'
        );

        $response->assertJson(
            [
                'data' => [['status_name' => 'Created to "Tugas Aktif"']]
            ]
        );
    }

    public function testMoveToToDo(): void
    {
        $task = factory(ActivityProgress::class)->create([
            'current_stage' => 5
        ]);

        $progressRepo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $this->app->instance(ActivityProgressRepository::class, $progressRepo);
        $progressRepo
            ->shouldReceive('moveToToDo')
            ->with($task->id);

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/todo',
            [$task->id]
        );
    }

    public function testMoveToToDoWithEmptyId(): void
    {
        $progressRepo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $this->app->instance(ActivityProgressRepository::class, $progressRepo);
        $progressRepo->shouldNotReceive('moveToToDo');

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/todo',
            []
        );

        $response->assertJson([
            'status' => false,
            'message' => 'The task id field is required.'
        ]);
    }

    public function testMoveToToDoWithInvalidId(): void
    {
        $progressRepo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $this->app->instance(ActivityProgressRepository::class, $progressRepo);
        $progressRepo
            ->shouldNotReceive('moveToToDo')
            ->with('a');

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/todo',
            [
                'task_id' => 'a'
            ]
        );

        $response->assertJson([
            'status' => false,
            'message' => 'The task id must be an integer.'
        ]);
    }

    public function testMoveToToDoWithNonExistantTaskId(): void
    {
        $progressRepo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $this->app->instance(ActivityProgressRepository::class, $progressRepo);
        $progressRepo
            ->shouldNotReceive('moveToToDo')
            ->with(0);

        $response = $this->actingAs($this->consultant)->post(
            '/consultant-tools/api/activity-management/task/todo',
            [
                'task_id' => 0
            ]
        );

        $response->assertJson([
            'status' => false,
            'message' => 'The selected task id is invalid.'
        ]);
    }
}
