<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AuthControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    protected $user;
    protected $role;
    protected $permission;

    public function setUp() : void
    {
        parent::setUp();

        $this->permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $this->role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $this->user->attachRole($this->role);
        $this->role->attachPermission($this->permission);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testPostLoginSuccess()
    {
        $this->markTestSkipped('Error on develop branch');
        $response = $this->json(
            'POST',
            '/consultant-tools/login',
            ['email' => $this->user->email, 'password' => 'password']
        );

        $response->assertSuccessful();
        $response->assertJson([
            'status' => 'success',
            'message' => 'logged in',
            'user_id' => $this->user->id
        ]);
        $this->assertAuthenticatedAs($this->user);
    }

    public function testPostLoginUserNotFound()
    {
        $response = $this->json(
            'POST',
            '/consultant-tools/login',
            ['email' => 'test2@email.com', 'password' => 'password']
        );

        $response->assertSuccessful();

        $response = json_decode($response->getContent());

        $this->assertEquals('error', $response->status);
        $this->assertEquals("Data yang Kamu Masukkan Salah" . PHP_EOL . "Harap periksa kembali.", $response->message);
    }

    public function testPostLoginWrongPassword()
    {
        $response = $this->json(
            'POST',
            '/consultant-tools/login',
            ['email' => $this->user->email, 'password' => 'password2']
        );

        $response->assertSuccessful();

        $response = json_decode($response->getContent());

        $this->assertEquals('error', $response->status);
        $this->assertEquals("Data yang Kamu Masukkan Salah" . PHP_EOL . "Harap periksa kembali.", $response->message);
    }

}