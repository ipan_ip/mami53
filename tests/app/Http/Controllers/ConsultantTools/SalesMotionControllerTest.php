<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\User;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Entrust\Role;
use App\Entities\Entrust\Permission;
use App\Entities\Consultant\Consultant;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SalesMotionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $consultant;

    protected const SALES_MOTION_DETAIL = '/consultant-tools/api/sales-motion/';
    protected const SALES_MOTION_LIST = '/consultant-tools/api/sales-motion/list';

    protected function setUp(): void
    {
        parent::setUp();

        $this->consultant = factory(User::class)->create();
        factory(Consultant::class)->create(
            [
                'user_id' => $this->consultant->id
            ]
        );

        $role = factory(Role::class)->create();
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $this->consultant->attachRole($role);
    }

    public function testDetail(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create(
            [
                'media_id' => $photo->id
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->setWebApiCookie()
            ->get(self::SALES_MOTION_DETAIL . $salesMotion->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'sales_motion' => [
                    'id' => $salesMotion->id,
                    'task_name' => $salesMotion->name,
                    'sales_type' => $salesMotion->type,
                    'created_by_division' => $salesMotion->created_by_division,
                    'start_date' => $salesMotion->start_date,
                    'due_date' => $salesMotion->end_date,
                    'is_active' => $salesMotion->is_active,
                    'objective' => $salesMotion->objective,
                    'terms_and_condition' => $salesMotion->terms_and_condition,
                    'benefit' => $salesMotion->benefit,
                    'requirement' => $salesMotion->requirement,
                    'max_participant' => $salesMotion->max_participant,
                    'updated_at' => $salesMotion->updated_at->toString(),
                    'updated_by' => $salesMotion->lastUpdatedBy->name,
                    'url' => $salesMotion->url,
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                ]
            ]
        );
    }

    public function testDetailWithoutPhoto(): void
    {
        $salesMotion = factory(SalesMotion::class)->create(
            [
                'media_id' => null
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->setWebApiCookie()
            ->get(self::SALES_MOTION_DETAIL . $salesMotion->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'sales_motion' => [
                    'id' => $salesMotion->id,
                    'task_name' => $salesMotion->name,
                    'sales_type' => $salesMotion->type,
                    'created_by_division' => $salesMotion->created_by_division,
                    'start_date' => $salesMotion->start_date,
                    'due_date' => $salesMotion->end_date,
                    'is_active' => $salesMotion->is_active,
                    'objective' => $salesMotion->objective,
                    'terms_and_condition' => $salesMotion->terms_and_condition,
                    'benefit' => $salesMotion->benefit,
                    'requirement' => $salesMotion->requirement,
                    'max_participant' => $salesMotion->max_participant,
                    'updated_at' => $salesMotion->updated_at->toString(),
                    'updated_by' => $salesMotion->lastUpdatedBy->name,
                    'url' => $salesMotion->url,
                    'photo' => null,
                ]
            ]
        );
    }

    public function testDetailWithoutUpdatedBy(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create(
            [
                'media_id' => $photo->id,
                'updated_by' => 0
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->setWebApiCookie()
            ->get(self::SALES_MOTION_DETAIL . $salesMotion->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'sales_motion' => [
                    'id' => $salesMotion->id,
                    'task_name' => $salesMotion->name,
                    'sales_type' => $salesMotion->type,
                    'created_by_division' => $salesMotion->created_by_division,
                    'start_date' => $salesMotion->start_date,
                    'due_date' => $salesMotion->end_date,
                    'is_active' => $salesMotion->is_active,
                    'objective' => $salesMotion->objective,
                    'terms_and_condition' => $salesMotion->terms_and_condition,
                    'benefit' => $salesMotion->benefit,
                    'requirement' => $salesMotion->requirement,
                    'max_participant' => $salesMotion->max_participant,
                    'updated_at' => $salesMotion->updated_at->toString(),
                    'updated_by' => 0,
                    'url' => $salesMotion->url,
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                ]
            ]
        );
    }

    public function testDetailWithoutUrl(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create(
            [
                'media_id' => $photo->id,
                'url' => null
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->setWebApiCookie()
            ->get(self::SALES_MOTION_DETAIL . $salesMotion->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'sales_motion' => [
                    'id' => $salesMotion->id,
                    'task_name' => $salesMotion->name,
                    'sales_type' => $salesMotion->type,
                    'created_by_division' => $salesMotion->created_by_division,
                    'start_date' => $salesMotion->start_date,
                    'due_date' => $salesMotion->end_date,
                    'is_active' => $salesMotion->is_active,
                    'objective' => $salesMotion->objective,
                    'terms_and_condition' => $salesMotion->terms_and_condition,
                    'benefit' => $salesMotion->benefit,
                    'requirement' => $salesMotion->requirement,
                    'max_participant' => $salesMotion->max_participant,
                    'updated_at' => $salesMotion->updated_at->toString(),
                    'updated_by' => $salesMotion->lastUpdatedBy->name,
                    'url' => null,
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                ]
            ]
        );
    }

    public function testDetailWithInvalidId(): void
    {
        $response = $this->actingAs($this->consultant)
            ->setWebApiCookie()
            ->get(self::SALES_MOTION_DETAIL . 'a');
        $response->assertStatus(404);
        $response->assertJson(
            [
                'status' => false,
                'message' => 'Sales motion tidak bisa ditemukan'
            ]
        );
    }

    public function testListWithoutLimitParam()
    {
        $salesMotion = factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PROMOTION, 'name' => 'test sales motion']
        );
        factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PRODUCT, 'name' => 'coba coba aja']
        );

        $salesMotion->consultants()->sync([$this->consultant->consultant->id]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->json('GET', self::SALES_MOTION_LIST);

        $response->assertJson(
            [
                'status' => true,
                'total' => 1
            ]
        );
    }

    public function testListWithLimitParam()
    {
        $salesMotion = factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PROMOTION, 'name' => 'test sales motion']
        );
        factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PRODUCT, 'name' => 'coba coba aja']
        );

        $salesMotion->consultants()->sync([$this->consultant->consultant->id]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => $this->consultant->consultant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $response = $this->actingAs($this->consultant)
            ->json('GET', self::SALES_MOTION_LIST, ['limit' => 5]);

        $response->assertJson(
            [
                'status' => true,
                'total' => 1
            ]
        );
    }
}
