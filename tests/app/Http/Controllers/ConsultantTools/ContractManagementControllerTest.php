<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayAdditionalCost;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Consultant\RoomIndexService;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ContractManagementControllerTest extends MamiKosTestCase
{
    protected $consultant;
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->consultant = factory(User::class)->create();
        $this->consultant->consultant()->save(factory(Consultant::class)->make());
        $role = factory(Role::class)->create();
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $this->consultant->attachRole($role);

        $this->service = $this->mock(RoomIndexService::class);
    }

    public function testListShouldReturnCorrectData(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithPageTwoPaginationShouldReturnPageTwoContract(): void
    {
        // Skipped contract
        $skippedConsultant = factory(Consultant::class)->create();
        $skippedTenant = factory(MamipayTenant::class)->create();
        $skippedContract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => $skippedTenant->id,
                'consultant_id' => $skippedConsultant->id,
                'created_at' => Carbon::now()->addDay(1)
            ]
        );
        $skippedRoom = factory(Room::class)->create();
        $skippedContract->type_kost()->save(
            factory(MamipayContractKost::class)->make(['designer_id' => $skippedRoom->id])
        );
        $skippedInvoice = factory(MamipayInvoice::class)->create(['contract_id' => $skippedContract->id]);

        // Contract returned by API
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id, 'created_at' => Carbon::now()]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?offset=1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 2,
                'limit' => 10,
                'offset' => '1',
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithPageTwoPaginationShouldNotReturnPageOneContract(): void
    {
        // Skipped contract
        $skippedConsultant = factory(Consultant::class)->create();
        $skippedTenant = factory(MamipayTenant::class)->create();
        $skippedContract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => $skippedTenant->id,
                'consultant_id' => $skippedConsultant->id,
                'created_at' => Carbon::now()->addDay(1)
            ]
        );
        $skippedRoom = factory(Room::class)->create();
        $skippedContract->type_kost()->save(
            factory(MamipayContractKost::class)->make(['designer_id' => $skippedRoom->id])
        );
        $skippedInvoice = factory(MamipayInvoice::class)->create(['contract_id' => $skippedContract->id]);

        // Contract returned by API
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id, 'created_at' => Carbon::now()]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?offset=1'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $skippedContract->id,
                        'name' => $skippedTenant->name,
                        'property_name' => $skippedRoom->name,
                        'contract_status' => $skippedContract->status,
                        'room_number' => $skippedContract->type_kost->room_number,
                        'invoice_id' => $skippedInvoice->id,
                        'status' => $skippedInvoice->status,
                        'paid_from' => $skippedInvoice->transfer_reference,
                        'scheduled_date' => $skippedInvoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $skippedContract->end_date,
                        'price' => $skippedInvoice->amount,
                        'from_booking' => is_null($skippedInvoice->from_booking) ? 0 : $skippedInvoice->from_booking,
                        'consultant_name' => $skippedConsultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateFromShouldReturnContractThatHasScheduledDateAfterDateFrom(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 12, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_from=2020-11-1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateFromShouldReturnContractThatHasScheduledDateAtDateFrom(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 11, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_from=2020-11-1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateFromShouldNotReturnContractThatHasScheduledDateBeforeDateFrom(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 10, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_from=2020-11-1'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateToShouldReturnContractThatHasScheduledDateBeforeDateTo(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 10, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_to=2020-11-1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateToShouldReturnContractThatHasScheduledDateAtDateTo(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 11, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_to=2020-11-1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateToShouldNotReturnContractThatHasScheduledDateAfterDateTo(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 12, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_to=2020-11-1'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithDateFromAndDateToShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        // Scheduled date will be filtered due to `date_from` query
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'scheduled_date' => Carbon::createFromDate(2020, 10, 1)]
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?date_to=2020-11-1&date_from=2020-9-1'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarKostNameInPrefix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=te'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarKostNameInSuffix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=st'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarKostName(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=test'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithNonMatchingKostName(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=bedt'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantPhoneInSuffix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=678'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantPhoneInPrefix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=081'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantPhone(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=0812345678'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldNotReturnContractWithNonMatchingTenantPhone(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'test']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=87654'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantNameInPrefix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'test', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=te'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantNameInSuffix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'test', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=st'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarTenantName(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'test', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=test'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithNonMatchingTenantName(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'test', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'invoice_number' => 'XXXXXXX']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=dddd'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarInvoiceNumberInPreffix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'test']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=te'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarInvoiceNumberInSuffix(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'test']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=st'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldReturnContractWithSimilarInvoiceNumber(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'test']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=test'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithSearchShouldNotReturnContractWithNonMatchingInvoiceNumber(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['name' => 'aaa', 'phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['name' => 'aaaa']);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'invoice_number' => 'test']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?search=ddd'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithContractStatusActiveShouldReturnMatchingContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id, 'status' => 'active']
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?contract_status[]=active'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithContractStatusTerminatedShouldNotReturnNonTerminatedContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id, 'status' => 'terminated']
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?contract_status[]=active'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithAllContractStatusShouldReturnAllContract(): void
    {
        $terminatedConsultant = factory(Consultant::class)->create();
        $terminatedTenant = factory(MamipayTenant::class)->create();
        $terminatedContract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => $terminatedTenant->id,
                'consultant_id' => $terminatedConsultant->id,
                'status' => 'terminated',
                'created_at' => '2020-12-01'
            ]
        );
        $terminatedRoom = factory(Room::class)->create();
        $terminatedContract->type_kost()->save(
            factory(MamipayContractKost::class)->make(['designer_id' => $terminatedRoom->id])
        );
        $terminatedInvoice = factory(MamipayInvoice::class)->create(['contract_id' => $terminatedContract->id]);

        $activeConsultant = factory(Consultant::class)->create();
        $activeTenant = factory(MamipayTenant::class)->create();
        $activeContract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => $activeTenant->id,
                'consultant_id' => $activeConsultant->id,
                'status' => 'active',
                'created_at' => '2020-11-01'
            ]
        );
        $activeRoom = factory(Room::class)->create();
        $activeContract->type_kost()->save(
            factory(MamipayContractKost::class)->make(['designer_id' => $activeRoom->id])
        );
        $activeInvoice = factory(MamipayInvoice::class)->create(['contract_id' => $activeContract->id]);

        $cancelledConsultant = factory(Consultant::class)->create();
        $cancelledTenant = factory(MamipayTenant::class)->create();
        $cancelledContract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => $cancelledTenant->id,
                'consultant_id' => $cancelledConsultant->id,
                'status' => 'cancelled',
                'created_at' => '2020-09-01'
            ]
        );
        $cancelledRoom = factory(Room::class)->create();
        $cancelledContract->type_kost()->save(
            factory(MamipayContractKost::class)->make(['designer_id' => $cancelledRoom->id])
        );
        $cancelledInvoice = factory(MamipayInvoice::class)->create(['contract_id' => $cancelledContract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?contract_status[]=active&contract_status[]=terminated&contract_status[]=cancelled'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $terminatedContract->id,
                        'name' => $terminatedTenant->name,
                        'property_name' => $terminatedRoom->name,
                        'contract_status' => $terminatedContract->status,
                        'room_number' => $terminatedContract->type_kost->room_number,
                        'invoice_id' => $terminatedInvoice->id,
                        'status' => $terminatedInvoice->status,
                        'paid_from' => $terminatedInvoice->transfer_reference,
                        'scheduled_date' => $terminatedInvoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $terminatedContract->end_date,
                        'price' => is_null($terminatedInvoice->amount) ? 0 : $terminatedInvoice->amount,
                        'from_booking' => is_null(
                            $terminatedInvoice->from_booking
                        ) ? 0 : $terminatedInvoice->from_booking,
                        'consultant_name' => $terminatedConsultant->name
                    ],
                    [
                        'id' => $activeContract->id,
                        'name' => $activeTenant->name,
                        'property_name' => $activeRoom->name,
                        'contract_status' => $activeContract->status,
                        'room_number' => $activeContract->type_kost->room_number,
                        'invoice_id' => $activeInvoice->id,
                        'status' => $activeInvoice->status,
                        'paid_from' => $activeInvoice->transfer_reference,
                        'scheduled_date' => $activeInvoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $activeContract->end_date,
                        'price' => is_null($activeInvoice->amount) ? 0 : $activeInvoice->amount,
                        'from_booking' => is_null($activeInvoice->from_booking) ? 0 : $activeInvoice->from_booking,
                        'consultant_name' => $activeConsultant->name
                    ],
                    [
                        'id' => $cancelledContract->id,
                        'name' => $cancelledTenant->name,
                        'property_name' => $cancelledRoom->name,
                        'contract_status' => $cancelledContract->status,
                        'room_number' => $cancelledContract->type_kost->room_number,
                        'invoice_id' => $cancelledInvoice->id,
                        'status' => $cancelledInvoice->status,
                        'paid_from' => $cancelledInvoice->transfer_reference,
                        'scheduled_date' => $cancelledInvoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $cancelledContract->end_date,
                        'price' => is_null($cancelledInvoice->amount) ? 0 : $cancelledInvoice->amount,
                        'from_booking' => is_null(
                            $cancelledInvoice->from_booking
                        ) ? 0 : $cancelledInvoice->from_booking,
                        'consultant_name' => $cancelledConsultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithUnpaidStatusShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'status' => 'unpaid']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=unpaid'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithUnpaidDeadlineStatusShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => 'paid_deadline']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=paid_deadline'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithUnpaidLateStatusShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'status' => 'unpaid_late']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=unpaid_late'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithPaidStatusShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'status' => 'paid']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=paid'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithPaidLateStatusShouldReturnContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'status' => 'paid_late']);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=paid_late'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithNotInMamipayStatusShouldReturnContractWithNotInMamipayStatus(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'status' => 'not_in_mamipay']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=not_in_mamipay'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithNotInMamipayStatusShouldReturnContractWithNotInMamipayTransferReference(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(
            ['contract_id' => $contract->id, 'transfer_reference' => 'not_in_mamipay']
        );

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?paid_status[]=not_in_mamipay'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByConsultantShouldReturnContractCreatedByConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=consultant'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByConsultantShouldNotReturnContractNotCreatedByConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => null]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=consultant'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByOwnerShouldReturnContractCreatedByOwner(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => null]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'from_booking' => 0]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=owner'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => null
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByOwnerShouldNotReturnContractCreatedByConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=owner'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByOwnerShouldNotReturnContractCreatedByTenant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => null]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'from_booking' => 1]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=owner'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => null
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByTenantShouldReturnContractCreatedByTenant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => null]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'from_booking' => 1]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=tenant'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => null
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByTenantShouldNotReturnContractCreatedByOwner(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => null]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'from_booking' => 0]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=tenant'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => null
                    ]
                ]
            ]
        );
    }

    public function testListWithCreatedByTenantShouldNotReturnContractCreatedByConsultant(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create();
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id, 'from_booking' => 0]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?created_by[]=tenant'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithIsBookingShouldReturnContractWithBookingKost(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['is_booking' => true]);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?is_booking=true'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListIsBookingShouldNotReturnContractWithNonBookingKost(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['is_booking' => false]);
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?is_booking=true'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithMappedConsultantShouldShowMappedContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['is_booking' => true]);
        factory(ConsultantRoom::class)->create(
            [
                'designer_id' => $room->id,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?is_booking=true'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testListWithMappedConsultantShouldNotShowNonMappedContract(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'consultant_id' => $consultant->id]
        );
        $room = factory(Room::class)->create(['is_booking' => true]);
        factory(ConsultantRoom::class)->create(
            [
                'designer_id' => $room->id + 1,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $contract->type_kost()->save(factory(MamipayContractKost::class)->make(['designer_id' => $room->id]));
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => $contract->id]);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract?is_booking=true'
        );
        $response->assertOk();
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $contract->id,
                        'name' => $tenant->name,
                        'property_name' => $room->name,
                        'contract_status' => $contract->status,
                        'room_number' => $contract->type_kost->room_number,
                        'invoice_id' => $invoice->id,
                        'status' => $invoice->status,
                        'paid_from' => $invoice->transfer_reference,
                        'scheduled_date' => $invoice->scheduled_date->format('Y-m-d'),
                        'end_date' => $contract->end_date,
                        'price' => is_null($invoice->amount) ? 0 : $invoice->amount,
                        'from_booking' => is_null($invoice->from_booking) ? 0 : $invoice->from_booking,
                        'consultant_name' => $consultant->name
                    ]
                ]
            ]
        );
    }

    public function testSearchTenantWithNonExistingPhoneNumber(): void
    {
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/tenant/search?phone_number=0812345678'
        );

        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'tenant' => [
                        'id' => null,
                        'name' => null,
                        'photo' => null,
                        'photo_id' => null,
                        'photo_identifier' => null,
                        'photo_identifier_id' => null,
                        'photo_document' => null,
                        'photo_document_id' => null,
                        'photo_document_name' => null,
                        'phone_number' => '0812345678',
                        'room_number' => null,
                        'email' => null,
                        'occupation' => null,
                        'gender' => null,
                        'gender_value' => null,
                        'photo_id' => null,
                        'photo_identifier_id' => null,
                        'photo_document_id' => null,
                        'marital_status' => null,
                        'parent_name' => null,
                        'parent_phone_number' => null,
                        'price' => null,
                        'due_date' => null,
                    ]
                ]
            ]
        );
    }

    public function testSearchTenantWithActiveContract(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0812345678']);
        $contract = factory(MamipayContract::class)->create(
            ['tenant_id' => $tenant->id, 'status' => 'active']
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/tenant/search?phone_number=0812345678'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => false,
                'message' => [
                    'title' => __('api.input.tenant.active.title'),
                    'message' => __('api.input.tenant.active.message')
                ]
            ]
        );
    }

    // public function testSearchTenantWithMamipayTenantNumber(): void
    // {
    //     $tenantPhotoId = factory(MamipayMedia::class)->create();
    //     $tenantPhotoDocument = factory(MamipayMedia::class)->create();
    //     $tenantPhoto = factory(MamipayMedia::class)->create();
    //     $tenant = factory(MamipayTenant::class)->create(
    //         [
    //             'phone_number' => '0812345678',
    //             'photo_identifier_id' => $tenantPhotoId->id,
    //             'photo_document_id' => $tenantPhotoDocument->id,
    //             'photo_id' => $tenantPhoto->id
    //         ]
    //     );
    //     $potentialTenant = factory(PotentialTenant::class)->create(['phone_number' => $tenant->phone_number]);

    //     $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
    //         'GET',
    //         '/consultant-tools/api/tenant/search?phone_number=0812345678'
    //     );
    //     $response->assertOk();
    //     $response->assertJson(
    //         [
    //             'status' => true,
    //             'data' => [
    //                 'tenant' => [
    //                     'id' => $tenant->id,
    //                     'name' => $tenant->name,
    //                     'photo' => $tenantPhoto ? $tenantPhoto->cached_urls : null,
    //                     'photo_id' => $tenantPhoto ? $tenantPhoto->id : null,
    //                     'photo_identifier' => $tenantPhotoId ? $tenantPhotoId->cached_urls : null,
    //                     'photo_identifier_id' => $tenantPhotoId ? $tenantPhotoId->id : null,
    //                     'photo_document' => $tenantPhotoDocument ? $tenantPhotoDocument->cached_urls : null,
    //                     'photo_document_id' => $tenantPhotoDocument ? $tenantPhotoDocument->id : null,
    //                     'photo_document_name' => $tenantPhotoDocument ? $tenantPhotoDocument->description : null,
    //                     'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
    //                     'room_number' => null,
    //                     'email' => $tenant->email,
    //                     'occupation' => $tenant->occupation,
    //                     'gender' => $tenant->gender_formatted,
    //                     'gender_value' => $tenant->gender,
    //                     'marital_status' => $tenant->marital_status,
    //                     'parent_name' => $tenant->parent_name,
    //                     'parent_phone_number' => $tenant->parent_phone_number,
    //                     'price' => !is_null($potentialTenant) ? $potentialTenant->price : null,
    //                     'due_date' => !is_null($potentialTenant) ? $potentialTenant->due_date : null,
    //                 ]
    //             ]
    //         ]
    //     );
    // }

    public function testSearchTenantWithPotentialTenantNumber(): void
    {
        $tenant = factory(PotentialTenant::class)->create(['phone_number' => '0812345678']);
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/tenant/search?phone_number=0812345678'
        );
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'tenant' => [
                        'id' => null,
                        'name' => $tenant->name,
                        'photo' => null,
                        'photo_id' => null,
                        'photo_identifier' => null,
                        'photo_identifier_id' => null,
                        'photo_document' => null,
                        'photo_document_id' => null,
                        'photo_document_name' => null,
                        'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
                        'room_number' => null,
                        'email' => $tenant->email,
                        'occupation' => $tenant->job_information,
                        'gender' => $tenant->gender,
                        'gender_value' => $tenant->gender,
                        'marital_status' => null,
                        'parent_name' => null,
                        'parent_phone_number' => null,
                        'price' => $tenant->price,
                        'due_date' => $tenant->due_date
                    ]
                ]
            ]
        );
    }

    public function testSearchPropertyWithMappedConsultant(): void
    {
        factory(ConsultantRoom::class)->create(
            [
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
        $result = [
            'took' => 113,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => 0.105360515,
                'hits' => [
                    [
                        '_index' => 'mamisearch-room',
                        '_type' => '_doc',
                        '_id' => '20',
                        '_score' => 0.105360515,
                        '_source' => [
                            'room_id' => 20,
                            'song_id' => 122,
                            'apartment_id' => null,
                            'title' => 'Kos Exclusive Kirlin.payton',
                            'slug' => '',
                            'array_gender' => 0,
                            'gender' => 'campur',
                            'type' => 'kos',
                            'code' => '',
                            'is_mamipay_active' => true,
                            'consultant_ids' => [
                                5,
                                6,
                                7
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->service->shouldReceive('searchKostWithActiveMamipay')
            ->once()
            ->with('exclusive', $this->consultant->consultant->id)
            ->andReturn($result);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/search-property/exclusive'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => 122,
                        'name' => 'Kos Exclusive Kirlin.payton'
                    ]
                ]
            ]
        );
    }

    public function testSearchPropertyWithUnmappedConsultant(): void
    {
        $result = [
            'took' => 113,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => 0.105360515,
                'hits' => [
                    [
                        '_index' => 'mamisearch-room',
                        '_type' => '_doc',
                        '_id' => '20',
                        '_score' => 0.105360515,
                        '_source' => [
                            'room_id' => 20,
                            'song_id' => 122,
                            'apartment_id' => null,
                            'title' => 'Kos Exclusive Kirlin.payton',
                            'slug' => '',
                            'array_gender' => 0,
                            'gender' => 'campur',
                            'type' => 'kos',
                            'code' => '',
                            'is_mamipay_active' => true,
                            'consultant_ids' => [
                                5,
                                6,
                                7
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->service->shouldReceive('searchKostWithActiveMamipay')
            ->once()
            ->with('exclusive')
            ->andReturn($result);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/search-property/exclusive'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => 122,
                        'name' => 'Kos Exclusive Kirlin.payton'
                    ]
                ]
            ]
        );
    }

    public function testGetInvoiceYearShouldReturnData(): void
    {
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d')
            ]
        );
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2019-01-01')->format('Y-m-d')
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/year-list/1'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'first_year' => '2018',
                    'last_year' => '2019'
                ]
            ]
        );
    }

    public function testGetInvoiceYearWithOnlyOneInvoiceShouldReturnData(): void
    {
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d')
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/year-list/1'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'first_year' => '2018',
                    'last_year' => '2018'
                ]
            ]
        );
    }

    public function testGetInvoiceYearWithOnlyThreeInvoiceShouldReturnData(): void
    {
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d')
            ]
        );
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2019-01-01')->format('Y-m-d')
            ]
        );
        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 1,
                'scheduled_date' => Carbon::parse('2020-01-01')->format('Y-m-d')
            ]
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/year-list/1'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    'first_year' => '2018',
                    'last_year' => '2020'
                ]
            ]
        );
    }

    public function testInvoiceShouldReturnData(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount + $costs->sum('cost_value')),
                        'amount' => 35000,
                        'note' => $invoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $cost->id,
                                'invoice_id' => $cost->invoice_id,
                                'cost_title' => $cost->cost_title,
                                'cost_value' => (int)$cost->cost_value,
                                'cost_type' => $cost->cost_type,
                                'sort_order' => $cost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithMultipleAdditionalCostShouldReturnCorrectTotalAmount(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->additional_costs()->saveMany(
            factory(MamipayAdditionalCost::class, 2)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount + $costs->sum('cost_value')),
                        'amount' => 35000,
                        'note' => $invoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $cost->id,
                                'invoice_id' => $cost->invoice_id,
                                'cost_title' => $cost->cost_title,
                                'cost_value' => (int)$cost->cost_value,
                                'cost_type' => $cost->cost_type,
                                'sort_order' => $cost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithNoAdditionalCostShouldReturnCorrectTotalAmount(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount),
                        'amount' => 35000,
                        'note' => $invoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => []
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithNoConsultantNoteShouldReturnEmptyNote(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount + $costs->sum('cost_value')),
                        'amount' => 35000,
                        'note' => '',
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $cost->id,
                                'invoice_id' => $cost->invoice_id,
                                'cost_title' => $cost->cost_title,
                                'cost_value' => (int)$cost->cost_value,
                                'cost_type' => $cost->cost_type,
                                'sort_order' => $cost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithMultipleInvoiceShouldReturnRequestedYear(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2019-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2019'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount + $costs->sum('cost_value')),
                        'amount' => $invoice->amount,
                        'note' => $invoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $cost->id,
                                'invoice_id' => $cost->invoice_id,
                                'cost_title' => $cost->cost_title,
                                'cost_value' => (int)$cost->cost_value,
                                'cost_type' => $cost->cost_type,
                                'sort_order' => $cost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithMultipleInvoiceShouldNotReturnOtherYear(): void
    {
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d')
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2019-12-01')->format('Y-m-d'),
                'amount' => 35000,
            ]
        );
        $invoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $cost = $invoice->additional_costs()->first();
        $costs = $invoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $invoice->id,
                        'scheduled_date' => $invoice->scheduled_date,
                        'status' => $invoice->status,
                        'total_cost' => (int)($invoice->amount + $costs->sum('cost_value')),
                        'amount' => 35000,
                        'note' => $invoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $invoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $cost->id,
                                'invoice_id' => $cost->invoice_id,
                                'cost_title' => $cost->cost_title,
                                'cost_value' => (int)$cost->cost_value,
                                'cost_type' => $cost->cost_type,
                                'sort_order' => $cost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceWithMultipleInvoiceShouldReturnSortedInvoice(): void
    {
        $firstInvoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-12-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $firstInvoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $firstInvoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $firstCost = $firstInvoice->additional_costs()->first();
        $firstCosts = $firstInvoice->additional_costs()->get();

        $secondInvoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => 2,
                'scheduled_date' => Carbon::parse('2018-11-01')->format('Y-m-d'),
                'amount' => 35000
            ]
        );
        $secondInvoice->additional_costs()->save(
            factory(MamipayAdditionalCost::class)->make()
        );
        $secondInvoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $secondCost = $secondInvoice->additional_costs()->first();
        $secondCosts = $secondInvoice->additional_costs()->get();

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/invoice/2/2018'
        );
        $response->assertJsonMissing(
            [
                'data' => [
                    [
                        'id' => $firstInvoice->id,
                        'scheduled_date' => $firstInvoice->scheduled_date,
                        'status' => $firstInvoice->status,
                        'total_cost' => (int)($firstInvoice->amount + $firstCosts->sum('cost_value')),
                        'amount' => 35000,
                        'note' => $firstInvoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $firstInvoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $firstCost->id,
                                'invoice_id' => $firstCost->invoice_id,
                                'cost_title' => $firstCost->cost_title,
                                'cost_value' => (int)$firstCost->cost_value,
                                'cost_type' => $firstCost->cost_type,
                                'sort_order' => $firstCost->sort_order
                            ]
                        ]
                    ],
                    [
                        'id' => $secondInvoice->id,
                        'scheduled_date' => $secondInvoice->scheduled_date,
                        'status' => $secondInvoice->status,
                        'total_cost' => (int)($secondInvoice->amount + $secondCosts->sum('cost_value')),
                        'amount' => 35000,
                        'note' => $secondInvoice->consultant_note->content,
                        'link' => config('api.mamipay.invoice') . $secondInvoice->shortlink,
                        'additional_costs' => [
                            [
                                'id' => $secondCost->id,
                                'invoice_id' => $secondCost->invoice_id,
                                'cost_title' => $secondCost->cost_title,
                                'cost_value' => (int)$secondCost->cost_value,
                                'cost_type' => $secondCost->cost_type,
                                'sort_order' => $secondCost->sort_order
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    public function testInvoiceNoteWithInvalidIdShouldReturnError(): void
    {
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            '/consultant-tools/api/contract/invoice/note/2018'
        );

        $response->assertJson(
            [
                'status' => false,
                'meta' => [
                    'severity' => 'INVALID_REQUEST',
                    'message' => 'Invalid Request'
                ]
            ]
        );
    }

    public function testInvoiceNoteWithExistingNoteShouldDeleteOldNote(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $oldNote = $invoice->consultant_note;
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            ('/consultant-tools/api/contract/invoice/note/' . $invoice->id),
            [
                'content' => 'test',
                'status' => 'paid'
            ]
        );

        $this->assertDatabaseMissing(
            'consultant_note',
            [
                'id' => $oldNote->id,
                'content' => $oldNote->content,
                'activity' => $oldNote->status,
                'consultant_id' => $oldNote->consultant_id
            ]
        );
    }

    public function testInvoiceNoteWithExistingNoteShouldReturnSuccess(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            ('/consultant-tools/api/contract/invoice/note/' . $invoice->id),
            [
                'content' => 'test',
                'status' => 'paid'
            ]
        );

        $response->assertJson(
            [
                'status' => 'success'
            ]
        );
    }

    public function testInvoiceNoteWithExistingNoteShouldSaveNewNote(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $invoice->consultant_note()->save(
            factory(ConsultantNote::class)->make()
        );
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            ('/consultant-tools/api/contract/invoice/note/' . $invoice->id),
            [
                'content' => 'test',
                'status' => 'paid'
            ]
        );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => 'test',
                'activity' => ('invoice-' . $invoice->status),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testInvoiceNoteWithoutExistingNoteShouldReturnSuccess(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            ('/consultant-tools/api/contract/invoice/note/' . $invoice->id),
            [
                'content' => 'test',
                'status' => 'paid'
            ]
        );

        $response->assertJson(
            [
                'status' => 'success'
            ]
        );
    }

    public function testInvoiceNoteWithoutExistingNoteShouldSaveNewNote(): void
    {
        $invoice = factory(MamipayInvoice::class)->create();
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'POST',
            ('/consultant-tools/api/contract/invoice/note/' . $invoice->id),
            [
                'content' => 'test',
                'status' => 'paid'
            ]
        );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => 'test',
                'activity' => ('invoice-' . $invoice->status),
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testTenantInfoWithInvalidIdShouldReturnError(): void
    {
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/contract/tenant-info/2018'
        );
        $response->assertJson(
            [
                'status' => false,
                'meta' => [
                    'severity' => 'INVALID_REQUEST',
                    'message' => 'Invalid Request'
                ]
            ]
        );
    }

    public function testTenantInfoShouldReturnCorrectData(): void
    {
        $tenant = factory(MamipayTenant::class)->create();
        $contract = factory(MamipayContract::class)->create(['tenant_id' => $tenant->id]);
        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            ('/consultant-tools/api/contract/tenant-info/' . $contract->id)
        );
        $response->assertJson(
            [
                'data' => [
                    'id' => $tenant ? $tenant->id : null,
                    'name' => $tenant ? $tenant->name : null,
                    'profile_picture' => $tenant && $tenant->user && $tenant->user->photo ? $tenant->user->photo->getMediaUrl(
                    ) : null
                ]
            ]
        );
    }
}
