<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ConsultantDocumentControllerTest extends MamiKosTestCase
{
    protected const UPLOAD_SALES_MOTION_PROGRESS = '/consultant-tools/api/document/sales-motion-progress';
    protected const DELETE_DOCUMENT = '/consultant-tools/api/document/';
    protected const DOWNLOAD_DOCUMENT = '/consultant-tools/api/document/';

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $this->user->attachRole($role);
        $role->attachPermission($permission);

        Storage::fake('s3');
    }

    protected function tearDown(): void
    {
        Storage::fake('s3');
        parent::tearDown();
    }

    public function testUploadSalesMotionProgressWithMissingFile(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS);

        $response->assertStatus(501);
        $response->assertJson([
            'messages' => [
                'file' => ['The file field is required.']
            ]
        ]);
    }

    public function testUploadSalesMotionProgressWithNonFile(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => '1']);

        $response->assertStatus(501);
        $response->assertJson([
            'messages' => [
                'file' => ['The file must be a file.']
            ]
        ]);
    }

    public function testUploadSalesMotionProgressWithInvalidFileType(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => UploadedFile::fake()->create('test.txt', 100)]);

        $response->assertStatus(501);
        $response->assertJson([
            'messages' => [
                'file' => ['The file must be a file of type: pdf, docx, doc.']
            ]
        ]);
    }

    public function testUploadSalesMotionProgressWithFileBiggerThan5MB(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => UploadedFile::fake()->create('test.pdf', 5121)]);

        $response->assertStatus(501);
        $response->assertJson([
            'messages' => [
                'file' => ['The file may not be greater than 5120 kilobytes.']
            ]
        ]);
    }

    public function testUploadSalesMotionProgressShouldSaveIntoDB(): void
    {
        $file = UploadedFile::fake()->create('test.pdf', 5120);
        $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        $this->assertDatabaseHas('consultant_document', [
            'documentable_type' => null,
            'documentable_id' => null,
            'file_path' => (config('consultant.document.folder_location') . '/' . 'sales_motion_progress'),
            'file_name' => $file->hashName()
        ]);
    }

    public function testUploadSalesMotionProgressShouldSaveFile(): void
    {
        $file = UploadedFile::fake()->create('test.pdf', 5120);
        $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        Storage::assertExists(
            config('consultant.document.folder_location') . '/' . 'sales_motion_progress' . '/' . $file->hashName()
        );
    }

    public function testUploadSalesMotionProgressWithPdfShouldReturnTrue(): void
    {
        $file = UploadedFile::fake()->create('test.pdf', 5120);
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        $response->assertJson(['status' => true]);
    }

    public function testUploadSalesMotionProgressShouldReturnValidId(): void
    {
        $file = UploadedFile::fake()->create('test.pdf', 5120);
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        $id = $response->getData()->document_id;

        $document = ConsultantDocument::find($id);

        $this->assertTrue(
            $file->hashName() === $document->file_name
        );
    }

    public function testUploadSalesMotionProgressWithDocxShouldReturnTrue(): void
    {
        $file = UploadedFile::fake()->create('test.docx', 5120);
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        $response->assertJson(['status' => true]);
    }

    public function testUploadSalesMotionProgressWithDocShouldReturnTrue(): void
    {
        $file = UploadedFile::fake()->create('test.doc', 5120);
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->post(self::UPLOAD_SALES_MOTION_PROGRESS, ['file' => $file]);

        $response->assertJson(['status' => true]);
    }

    // public function testDeleteShouldDeleteFromDatabase(): void
    // {
    //     $file = UploadedFile::fake()->create('document.pdf', 1500);
    //     $document = factory(ConsultantDocument::class)->create([
    //         'file_path' => (config('consultant.document.folder_location') . '/' . 'sales_motion_progress'),
    //         'file_name' => $file->hashName()
    //     ]);
    //     $file->store((config('consultant.document.folder_location') . '/' . 'sales_motion_progress'));

    //     $this->setWebApiCookie()
    //         ->actingAs(
    //             $this->user
    //         )
    //         ->delete((self::DELETE_DOCUMENT . $document->id));

    //     $this->assertSoftDeleted('consultant_document', ['id' => $document->id]);
    // }

    // public function testDeleteShouldDeleteFromStorage(): void
    // {
    //     $file = UploadedFile::fake()->create('document.pdf', 1500);
    //     $document = factory(ConsultantDocument::class)->create([
    //         'file_path' => (config('consultant.document.folder_location') . '/' . 'sales_motion_progress'),
    //         'file_name' => $file->hashName()
    //     ]);
    //     $file->store((config('consultant.document.folder_location') . '/' . 'sales_motion_progress'));

    //     $this->setWebApiCookie()
    //         ->actingAs(
    //             $this->user
    //         )
    //         ->delete((self::DELETE_DOCUMENT . $document->id));

    //     Storage::assertMissing(
    //         config('consultant.document.folder_location') . '/' . 'sales_motion_progress' . '/' . $file->hashName()
    //     );
    // }

    public function testDeleteWithInvalidIdShouldReturnNotFoundMessage(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->delete((self::DELETE_DOCUMENT . 0));

        $response->assertJson([
            'message' => __('api.exception.not_found')
        ]);
    }

    public function testDeleteWithInvalidShouldReturn404Code(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->delete((self::DELETE_DOCUMENT . 0));

        $response->assertNotFound();
    }

    public function testDownload(): void
    {
        $file = UploadedFile::fake()->create('document.pdf', 1500);
        $document = factory(ConsultantDocument::class)->create([
            'file_path' => (config('consultant.document.folder_location') . '/' . 'sales_motion_progress'),
            'file_name' => $file->hashName()
        ]);
        $file->store((config('consultant.document.folder_location') . '/' . 'sales_motion_progress'));

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->get((self::DOWNLOAD_DOCUMENT . $document->id));

        $response->assertHeader('content-type', 'application/pdf');
    }
}
