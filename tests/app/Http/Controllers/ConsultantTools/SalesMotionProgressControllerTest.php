<?php

namespace App\Http\Controllers\ConsultantTools;

use App\User;
use Carbon\Carbon;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Entrust\Role;
use App\Entities\User\UserRole;
use App\Entities\Entrust\Permission;
use App\Entities\Consultant\Consultant;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SalesMotionProgressControllerTest extends MamiKosTestCase
{
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $this->user->attachRole($role);
        $role->attachPermission($permission);
    }

    public function testIndexShouldReturnCorrectNameAndSort(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );
        $room = factory(Room::class)->create();
        factory(SalesMotionProgress::class)->create(
            [
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => $room->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );
        $potential = factory(PotentialTenant::class)->create();
        factory(SalesMotionProgress::class)->create(
            [
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'progressable_id' => $potential->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()->addHours(2)
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id");

        $response->assertjson(
            [
                'progress' => [
                    [
                        'name' => $potential->name
                    ],
                    [
                        'name' => $room->name
                    ],
                    [
                        'name' => $tenant->name
                    ]
                ]
            ]
        );
    }

    public function testIndexShouldReturnCorrectCount(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class, 11)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id");

        $response->assertJson(['count' => 11]);
    }

    public function testIndexShouldUseDefaultLimit(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class, 11)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id");

        $response->assertJsonCount(10, 'progress');
    }

    public function testIndexWithLimit(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class, 11)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?limit=11");

        $response->assertJsonCount(11, 'progress');
    }

    public function testIndexWithOffset(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class, 11)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?offset=10");

        $response->assertJsonCount(1, 'progress');
    }

    public function testIndexWithOffsetShouldReturnCorrectCount(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        factory(SalesMotionProgress::class, 11)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?offset=10");

        $response->assertJson(['count' => 11]);
    }

    public function testIndexShouldNotReturnOtherConsultantProgress(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        factory(SalesMotionProgress::class)->create(
            [
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id + 1,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id");

        $response->assertJson(
            [
                'count' => 1,
                'progress' => [
                    [
                        'status' => $progress->status,
                        'name' => $tenant->name,
                        'type' => 'contract',
                        'updated_at' => $progress->updated_at->toString()
                    ]
                ]
            ]
        );
    }

    public function testIndexWithInterestedStatus(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?status=interested");

        $response->assertJson(
            [
                'count' => 1,
                'progress' => [
                    [
                        'status' => $progress->status,
                        'name' => $tenant->name,
                        'type' => 'contract',
                        'updated_at' => $progress->updated_at->toString()
                    ]
                ]
            ]
        );
    }

    public function testIndexWithDealStatus(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?status=deal");

        $response->assertJson(
            [
                'count' => 1,
                'progress' => [
                    [
                        'status' => $progress->status,
                        'name' => $tenant->name,
                        'type' => 'contract',
                        'updated_at' => $progress->updated_at->toString()
                    ]
                ]
            ]
        );
    }

    public function testIndexWithNotInterestedStatus(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?status=not_interested");

        $response->assertJson(
            [
                'count' => 1,
                'progress' => [
                    [
                        'status' => $progress->status,
                        'name' => $tenant->name,
                        'type' => 'contract',
                        'updated_at' => $progress->updated_at->toString()
                    ]
                ]
            ]
        );
    }

    public function testIndexWithDifferentStatusShouldNotReturnDifferentStatus(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?status=interested");

        $response->assertJson(
            [
                'count' => 0,
                'progress' => []
            ]
        );
    }

    public function testIndexWithStatusShouldReturnCorrectCount(): void
    {
        $consultant = factory(Consultant::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'progressable_type' => 'mamipay_contract',
                'progressable_id' => factory(MamipayContract::class)
                    ->create(
                        [
                            'tenant_id' => $tenant->id
                        ]
                    )
                    ->id,
                'consultant_id' => $consultant->id,
                'updated_at' => Carbon::now()
            ]
        );

        $response = $this->setWebApiCookie()
            ->actingAs(
                $this->user,
                'passport'
            )
            ->json('GET', "/consultant-tools/api/sales-motion/progress/$consultant->id?status=interested");

        $response->assertJson(
            [
                'count' => 1,
            ]
        );
    }

    public function testDetailWithTypeProperty(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $property = factory(Room::class)->create();
        $salesMotion = factory(SalesMotion::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => $photo->id,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => $property->id,
                'consultant_sales_motion_id' => $salesMotion->id,
            ]
        );
        $document = factory(ConsultantDocument::class)->create(
            [
                'documentable_id' => $progress->id
            ]
        );
        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/" . $progress->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'progress' => [
                    'id' => $progress->id,
                    'name' => $progress->progressable->task_name,
                    'type' => 'property',
                    'progressable_id' => $progress->progressable->id,
                    'status' => $progress->status,
                    'remark' => $progress->remark,
                    'created_by_division' => $salesMotion->created_by_division,
                    'created_at' => $progress->created_at->toString(),
                    'updated_at' => $progress->updated_at->toString(),
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                    'documents' => [
                        [
                            'id' => $document->id,
                            'file_name' => $document->file_name
                        ]
                    ]
                ]
            ]
        );
    }

    public function testDetailWithTypeContract(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create();
        $contract = factory(MamipayContract::class)->create(
            [
                'tenant_id' => factory(MamipayTenant::class)
            ]
        );
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => $photo->id,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => $contract->id,
                'consultant_sales_motion_id' => $salesMotion->id,
            ]
        );
        $document = factory(ConsultantDocument::class)->create(
            [
                'documentable_id' => $progress->id
            ]
        );

        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/" . $progress->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'progress' => [
                    'name' => $progress->progressable->task_name,
                    'type' => 'contract',
                    'progressable_id' => $progress->progressable->id,
                    'status' => $progress->status,
                    'remark' => $progress->remark,
                    'created_by_division' => $salesMotion->created_by_division,
                    'created_at' => $progress->created_at->toString(),
                    'updated_at' => $progress->updated_at->toString(),
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                    'documents' => [
                        [
                            'id' => $document->id,
                            'file_name' => $document->file_name
                        ]
                    ]
                ]
            ]
        );
    }

    public function testDetailWithTypeDBET(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create();
        $potentialTenant = factory(PotentialTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => $photo->id,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'progressable_id' => $potentialTenant->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        $document = factory(ConsultantDocument::class)->create(
            [
                'documentable_id' => $progress->id
            ]
        );

        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/" . $progress->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'progress' => [
                    'name' => $progress->progressable->task_name,
                    'type' => 'dbet',
                    'progressable_id' => $progress->progressable->id,
                    'status' => $progress->status,
                    'remark' => $progress->remark,
                    'created_by_division' => $salesMotion->created_by_division,
                    'created_at' => $progress->created_at->toString(),
                    'updated_at' => $progress->updated_at->toString(),
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                    'documents' => [
                        [
                            'id' => $document->id,
                            'file_name' => $document->file_name
                        ]
                    ]
                ]
            ]
        );
    }

    public function testDetailWithMultiDocuments(): void
    {
        $photo = factory(Media::class)->create();
        $urls = $photo->getMediaUrl();
        $salesMotion = factory(SalesMotion::class)->create();
        $property = factory(Room::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => $photo->id,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => $property->id,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        $documents = factory(ConsultantDocument::class, 2)->create(
            [
                'documentable_id' => $progress->id
            ]
        );
        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/" . $progress->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'progress' => [
                    'name' => $progress->progressable->task_name,
                    'type' => 'property',
                    'progressable_id' => $progress->progressable->id,
                    'status' => $progress->status,
                    'remark' => $progress->remark,
                    'created_by_division' => $salesMotion->created_by_division,
                    'created_at' => $progress->created_at->toString(),
                    'updated_at' => $progress->updated_at->toString(),
                    'photo' => [
                        'small' => $urls['small'],
                        'medium' => $urls['medium'],
                        'large' => $urls['large']
                    ],
                    'documents' => [
                        [
                            'id' => $documents[0]->id,
                            'file_name' => $documents[0]->file_name
                        ],
                        [
                            'id' => $documents[1]->id,
                            'file_name' => $documents[1]->file_name
                        ]
                    ]
                ]
            ]
        );
    }

    public function testDetailWithMissingPhotoAndMissingDocumentAndMissingSalesMotion()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $potentialTenant = factory(PotentialTenant::class)->create();
        $progress = factory(SalesMotionProgress::class)->create(
            [
                'media_id' => 0,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'progressable_id' => $potentialTenant->id,
                'consultant_sales_motion_id' => 0
            ]
        );

        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/" . $progress->id);
        $response->assertOk();
        $response->assertJson(
            [
                'status' => true,
                'progress' => [
                    'name' => $progress->progressable->task_name,
                    'type' => 'dbet',
                    'progressable_id' => $progress->progressable->id,
                    'status' => $progress->status,
                    'remark' => $progress->remark,
                    'created_by_division' => null,
                    'created_at' => $progress->created_at->toString(),
                    'updated_at' => $progress->updated_at->toString(),
                    'photo' => null,
                    'documents' => null
                ]
            ]
        );
    }

    public function testDetailWithInvalidId(): void
    {
        $response = $this->actingAs($this->user, 'passport')
            ->setWebApiCookie()
            ->get("/consultant-tools/api/sales-motion/progress/detail/0");
        $response->assertStatus(404);
        $response->assertJson(
            [
                'status' => false,
                'message' => 'Laporan tidak bisa ditemukan'
            ]
        );
    }

    public function testStoreWithProperty()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class),
                'progressable_type' => 'property',
                'progressable_id' => factory(Room::class),
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful()
            ->assertJson(
                [
                    'status' => true,
                    'message' => 'Data laporan telah berhasil ditambahkan'
                ]
            );

        $progress['consultant_id'] = $this->user->consultant->id;
        $progress['progressable_type'] = SalesMotionProgress::MORPH_TYPE_PROPERTY;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithContract()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => factory(MamipayContract::class),
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithDBET()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'progressable_id' => factory(PotentialTenant::class),
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithInvalidType()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class),
                'progressable_type' => 'invalid',
                'progressable_id' => 1,
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'progressable_type' => ['The selected progressable type is invalid.']
                    ]
                ]
            );

        $progress['consultant_id'] = $this->user->consultant->id;
        $this->assertDatabaseMissing('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithInvalidSalesMotion()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => 0,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'consultant_sales_motion_id' => ['The selected consultant sales motion id is invalid.']
                    ]
                ]
            );
    }

    public function testStoreWithInvalidMedia()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => 0,
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'media_id' => ['The selected media id is invalid.']
                    ]
                ]
            );
    }

    public function testStoreWithInvalidStatus()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'status' => 'true',
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'status' => ['The selected status is invalid.']
                    ]
                ]
            );
    }

    public function testStoreWithInvalidRemark()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'remark' => str_random(601)
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'remark' => ['The remark may not be greater than 600 characters.']
                    ]
                ]
            );
    }

    public function testStoreWithNonConsultantUser()
    {
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(403)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => 'Hanya consultant yang dapat menambahkan laporan'
                ]
            );
    }

    public function testStoreWithDocument()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $document = factory(ConsultantDocument::class)->create(
            [
                'documentable_id' => null
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'document_ids' => [$document->id],
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        unset($progress['document_ids'], $progress['consultant_id']);
        $salesMotionProgress = SalesMotionProgress::where($progress)->first();

        $this->assertEquals($salesMotionProgress->id, $document->refresh()->documentable_id);
        $this->assertDatabaseHas('consultant_document', $document->toArray());
    }

    public function testStoreWithTwoDocuments()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $document = factory(ConsultantDocument::class, 2)->create(
            [
                'documentable_id' => null
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'document_ids' => [$document[0]->id, $document[1]->id],
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        unset($progress['document_ids'], $progress['consultant_id']);
        $salesMotionProgress = SalesMotionProgress::where($progress)->first();

        $this->assertEquals($salesMotionProgress->id, $document[0]->refresh()->documentable_id);
        $this->assertEquals($salesMotionProgress->id, $document[1]->refresh()->documentable_id);
    }

    public function testStoreWithMoreThanTwoDocuments()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $document = factory(ConsultantDocument::class, 3)->create(
            [
                'documentable_id' => null
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'document_ids' => [$document[0]->id, $document[1]->id, $document[2]->id],
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'document_ids' => ['The document ids may not have more than 2 items.']
                    ]
                ]
            );
    }

    public function testStoreWithInvalidDocumentId()
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );

        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_DBET,
                'media_id' => factory(Media::class),
                'document_ids' => [0],
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertStatus(501)
            ->assertJson(
                [
                    'status' => false,
                    'errors' => [
                        'document_ids.0' => ['The selected document_ids.0 is invalid.']
                    ]
                ]
            );
    }

    public function testStoreWithNoDocumentIdsShouldSave(): void
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class)->create()->id,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => factory(MamipayContract::class)
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithNullMediaIdShouldSave(): void
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => null,
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => factory(MamipayContract::class)
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithoutMediaIdShouldSave(): void
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => factory(MamipayContract::class)
            ]
        )->toArray();

        unset($progress['media_id']);

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        $progress['media_id'] = null;
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }

    public function testStoreWithEmptyArrayDocumentIdsShouldSave(): void
    {
        factory(Consultant::class)->create(
            [
                'user_id' => $this->user->id
            ]
        );
        $progress = factory(SalesMotionProgress::class)->make(
            [
                'consultant_sales_motion_id' => factory(SalesMotion::class),
                'media_id' => factory(Media::class),
                'progressable_type' => SalesMotionProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => factory(MamipayContract::class),
                'document_ids' => [],
            ]
        )->toArray();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->post("/consultant-tools/api/sales-motion/progress/store", $progress)
            ->assertSuccessful();

        $progress['consultant_id'] = $this->user->consultant->id;
        unset($progress['document_ids']);
        $this->assertDatabaseHas('consultant_sales_motion_progress', $progress);
    }
}
