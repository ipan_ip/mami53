<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use stdClass;

class PropertyManagementControllerTest extends MamiKosTestCase
{

    protected $elasticsearch;
    protected $user;
    protected $consultant;

    protected const LIST = '/consultant-tools/api/property';

    protected function setUp(): void
    {
        parent::setUp();

        $role = factory(Role::class)->create();
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );
        $this->user = factory(User::class)->create();
        $this->consultant = factory(Consultant::class)->create(['user_id' => $this->user->id]);
        $this->user->attachRole($role);
        $this->elasticsearch = $this->mock('alias:Elasticsearch');
    }

    protected function getESResponse($isBooking = false, $isPromoted = false)
    {
        return [
            "took" => 1,
            "timed_out" => false,
            "_shards" => [
                "total" => 1,
                "successful" => 1,
                "skipped" => 0,
                "failed" => 0
            ],
            "hits" => [
                "total" => 1,
                "max_score" => null,
                "hits" => [
                    0 => [
                        "_index" => "mamisearch-room",
                        "_type" => "_doc",
                        "_id" => "1000002912",
                        "_score" => null,
                        "_source" => [
                            "room_id" => 1000002912,
                            "song_id" => 66621038,
                            "apartment_id" => null,
                            "title" => "Kost Test",
                            "slug" => "kost-yogyakarta-kost-putri-murah-kost-08980385440-1-duplikat-1",
                            "array_gender" => 2,
                            "gender" => "putri",
                            "type" => "kos",
                            "code" => "617D8",
                            "is_mamipay_active" => true,
                            "room_available" => 1,
                            "room_count" => 1,
                            "owner_status" => "verified",
                            "is_booking" => $isBooking,
                            "is_promoted" => $isPromoted,
                            "updated_at" => "2020-8-6 13:56:54"
                        ],
                        "sort" => [
                            0 => 1596722214000
                        ]
                    ]
                ]
            ]
        ];
    }

    public function testListShouldReturnCorrectResponse()
    {
        $this->elasticsearch->shouldReceive('search')
            ->withAnyArgs()
            ->once()
            ->andReturn($this->getESResponse());

        $actual = $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST);

        $actual->assertJson(
            [
                'status' => true,
                'total' => 1,
                'limit' => 10,
                'offset' => 0,
                'data' => [
                    [
                        "id" => 66621038,
                        "name" => "Kost Test",
                        "available_room" => 1,
                        "used_room" => 0,
                        "updated_at" => "2020-8-6 13:56:54",
                        "status" => "verified"
                    ]
                ]
            ]
        );
    }

    public function testListShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'sort' => [
                            [
                                'updated_at' => ['order' => 'desc']
                            ]
                        ],
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'from' => 0
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST);
    }

    public function testListWithSearchShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'query' => [
                            'bool' => [
                                'should' => [
                                    [
                                        'match' => [
                                            'title' => 'test'
                                        ]
                                    ]
                                ],
                                'minimum_should_match' => 1
                            ]
                        ],
                        "sort" => [
                            [
                                "updated_at" => [
                                    "order" => "desc"
                                ]
                            ]
                        ],
                        'from' => 0
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?search=test');
    }

    public function testListSortDescByUpdatedAtShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'sort' => [
                            [
                                'updated_at' => ['order' => 'desc']
                            ]
                        ],
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'from' => 0
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?sort_by=updated_at');
    }

    public function testListWithFilterBookingShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'sort' => [
                            [
                                'updated_at' => ['order' => 'desc']
                            ]
                        ],
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                ['term' => ['is_booking' => true]]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'from' => 0
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?filter[]=booking');
    }

    public function testListWithPremiumFilterShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'sort' => [
                            [
                                'updated_at' => [
                                    'order' => 'desc'
                                ]
                            ]
                        ],
                        'from' => 0,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'term' => [
                                                        'is_promoted' => true
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?filter[]=premium');
    }

    public function testListWithFilterFreeListingShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    "index" => "mamisearch-room",
                    "body" => [
                        "sort" => [
                            [
                                "updated_at" => [
                                    "order" => "desc"
                                ]
                            ]
                        ],
                        "from" => 0,
                        "query" => [
                            "bool" => [
                                "filter" => [
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "term" => [
                                                        "is_booking" => false
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?filter[]=free_listing');
    }

    public function testListWithOffsetShouldQueryCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    'index' => 'mamisearch-room',
                    'body' => [
                        'sort' => [
                            [
                                'updated_at' => ['order' => 'desc']
                            ]
                        ],
                        'query' => [
                            'match_all' => new stdClass()
                        ],
                        'from' => 10
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?offset=10');
    }

    public function testListShouldReturnRoomMappedToConsultant(): void
    {
        factory(ConsultantRoom::class)->create(
            [
                'designer_id' => factory(Room::class)->create()->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        $this->elasticsearch->shouldReceive('search')
            ->with(
                [
                    "index" => "mamisearch-room",
                    "body" => [
                        "sort" => [
                            [
                                "updated_at" => [
                                    "order" => "desc"
                                ]
                            ]
                        ],
                        "from" => 0,
                        "query" => [
                            "bool" => [
                                "filter" => [
                                    [
                                        "bool" => [
                                            "must" => [
                                                [
                                                    "term" => [
                                                        "consultant_ids" => $this->consultant->id
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            )
            ->once()
            ->andReturn($this->getESResponse());

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST);
    }

    public function testListWithEmptyArratKostLevelIds(): void
    {
        $this->elasticsearch
            ->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-room',
                'body' => [
                    'sort' => [
                        [
                            "updated_at" => [
                                "order" => "desc"
                            ]
                        ]
                    ],
                    'query' => [
                        'match_all' => new stdClass()
                    ],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()
            ->actingAs($this->user, 'passport')
            ->get(self::LIST . '?kost_level_ids[]=');
    }
}
