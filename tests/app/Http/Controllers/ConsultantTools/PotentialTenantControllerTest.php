<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\PotentialTenantRemarks;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Consultant\RoomIndexService;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use stdClass;

class PotentialTenantControllerTest extends MamiKosTestCase
{
    use WithFaker;

    protected $elasticsearch;
    protected $consultant;
    protected $nonConsultant;
    protected $role;
    protected $service;

    protected const FILTER_REGISTERED = 'registered';
    protected const FILTER_NON_REGISTERED = 'non_registered';

    protected function setUp(): void
    {
        parent::setUp();
        $this->role = factory(Role::class)->create();
        $this->role->attachPermission(
            factory(Permission::class)->create(['name' => 'admin-access'])
        );
        $this->role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-consultant'])
        );

        $this->consultant = factory(User::class)->create();
        $this->consultant->consultant()->save(factory(Consultant::class)->make());
        $this->consultant->attachRole($this->role);

        $this->nonConsultant = factory(User::class)->create();
        $this->nonConsultant->attachRole($this->role);

        $this->elasticsearch = $this->mock('alias:Elasticsearch');
        $this->service = $this->mock(RoomIndexService::class);
    }

    public function testListWithNonConsultant(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        ['updated_at' => ['order' => 'desc']]
                    ],
                    'query' => [
                        'match_all' => new stdClass()
                    ],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential');
    }

    public function testListWithConsultant(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        ['updated_at' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            ['term' => ['created_by_consultant' => $this->consultant->consultant->id]]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->consultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential');
    }

    public function testListWithAccessAllConsultant(): void
    {
        $this->role->attachPermission(
            factory(Permission::class)->create(['name' => 'access-all-consultant-potential-tenant'])
        );

        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        ['updated_at' => ['order' => 'desc']]
                    ],
                    'from' => 0,
                    'query' => ['match_all' => new stdClass()]
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->consultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential');
    }

    public function testListWithRegisteredFilter(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        [
                            'updated_at' => ['order' => 'desc']
                        ]
                    ],
                    'from' => 0,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'bool' => [
                                        'must' => [
                                            ['term' => ['is_registered' => true]]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?filter[]=' . self::FILTER_REGISTERED);
    }

    public function testListWithNonRegisteredFilter(): void
    {
        $this->elasticsearch->shouldReceive('search')
        ->with([
            'index' => 'mamisearch-potential-tenant',
            'body' => [
                'sort' => [
                    [
                        'updated_at' => ['order' => 'desc']
                    ]
                ],
                'from' => 0,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['is_registered' => false]]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?filter[]=' . self::FILTER_NON_REGISTERED);
    }

    public function testListWithSearch(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'query' => [
                        'bool' => [
                            'should' => [
                                ['match' => ['name' => 'es']],
                                ['match' => ['area_city' => 'es']],
                                ['match' => ['email' => 'es']],
                                ['match' => ['phone_number' => 'es']]
                            ],
                            'minimum_should_match' => 1
                        ]
                    ],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?search=es');
    }

    public function testListWithEmptySearch(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        [
                            'updated_at' => ['order' => 'desc']
                        ]
                    ],
                    'query' => ['match_all' => new stdClass()],
                    'from' => 0
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?search=');
    }

    public function testListShouldOffsetCorrectly(): void
    {
        $this->elasticsearch->shouldReceive('search')
            ->with([
                'index' => 'mamisearch-potential-tenant',
                'body' => [
                    'sort' => [
                        ['updated_at' => ['order' => 'desc']]
                    ],
                    'query' => [
                        'match_all' => new stdClass()
                    ],
                    'from' => 10
                ]
            ])
            ->once();

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?offset=10');
    }

    public function testListShouldReturnCorrectResponse(): void
    {
        $this->elasticsearch
            ->shouldReceive('search')
            ->withAnyArgs()
            ->andReturn([
                'took' => 9,
                'timed_out' => false,
                '_shards' => [
                    'total' => 5,
                    'successful' => 5,
                    'skipped' => 0,
                    'failed' => 0
                ],
                'hits' => [
                    'total' => 24,
                    'max_score' => null,
                    'hits' => [
                        [
                            '_index' => 'mamisearch-potential-tenant',
                            '_type' => '_doc',
                            '_id' => '170',
                            '_score' => null,
                            '_source' => [
                                'id' => 170,
                                'name' => 'baggy',
                                'email' => 'baggy@mamitest.com',
                                'phone_number' => '081771771771',
                                'kost_name' => 'Kost Estingte',
                                'area_city' => 'Bantul',
                                'is_registered' => false,
                                'song_id' => 88737652,
                                'note_content' => 'notes',
                                'note_activity' => 'remark-has_paid_to_owner',
                                'scheduled_date' => '2020-04-30',
                                'duration_unit' => '3_month',
                                'designer_id' => 155239,
                                'created_by_consultant' => 3,
                                'updated_at' => '2020-04-27 10:56:58',
                                'consultant_ids' => [1, 4]
                            ],
                            'sort' => [1587985018000]
                        ]
                    ]
                ]
            ]);

        $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', '/consultant-tools/api/tenant/potential?offset=10');
    }

    public function testDetail(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_ACTIVE,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => true,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithoutNote(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_ACTIVE,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => true,
                        'remarks_note' => null,
                        'remarks_category' => null,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithUnregisteredRoom(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_ACTIVE,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => null,
                        'property_name' => $potentialTenant->property_name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => null,
                        'owner_phone' => null,
                        'registered' => false,
                        'active_contract' => true,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithRegisteredTenant(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        factory(User::class)->create(
            [
                'is_owner' => 'false',
                'phone_number' => '081111111111',
                'role' => 'user'
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_ACTIVE,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => true,
                        'active_contract' => true,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithFinishedContract(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_FINISHED,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => false,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithTerminatedContract(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_TERMINATED,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => false,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithBookedContract(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '081111111111']);
        factory(MamipayContract::class)->create(
            [
                'tenant_id' => $tenant->id,
                'status' => MamipayContract::STATUS_BOOKED,
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => false,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testDetailWithoutContract(): void
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id
            ]
        );
        $potentialTenant = factory(PotentialTenant::class)->create(
            [
                'designer_id' => $room->id,
                'phone_number' => '081111111111'
            ]
        );
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $potentialTenant->id
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/$potentialTenant->id");

        $response->assertJson(
            [
                'data' => [
                    [
                        'id' => $potentialTenant->id,
                        'name' => $potentialTenant->name,
                        'phone_number' => $potentialTenant->phone_number,
                        'property_id' => $room->song_id,
                        'property_name' => $room->name,
                        'email' => $potentialTenant->email,
                        'gender' => $potentialTenant->gender,
                        'job_information' => $potentialTenant->job_information,
                        'due_date' => $potentialTenant->due_date,
                        'price' => $potentialTenant->price,
                        'owner_name' => $owner->name,
                        'owner_phone' => $owner->phone_number,
                        'registered' => false,
                        'active_contract' => false,
                        'remarks_note' => $note->content,
                        'remarks_category' => $note->activity,
                        'scheduled_date' => $potentialTenant->scheduled_date,
                        'duration_unit' => $potentialTenant->duration_unit
                    ]
                ]
            ]
        );
    }

    public function testSearchTenantByPhoneNumberWithRegisteredTenant(): void
    {
        $user = factory(User::class)->create(
            [
                'phone_number' => '0811111111',
                'role' => 'user',
                'is_owner' => 'false'
            ]
        );
        $tenant = factory(PotentialTenant::class)->create(
            [
                'phone_number' => '0811111111',
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/search-tenant/$user->phone_number");

        $response->assertJson(
            [
                'data' => [
                    'id' => $tenant->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone_number' => $user->phone_number,
                    'gender' => $user->gender,
                    'job_information' => $tenant->job_information
                ]
            ]
        );
    }

    public function testSearchTenantByPhoneNumberWithUnregisteredTenant(): void
    {
        $tenant = factory(PotentialTenant::class)->create(
            [
                'email' => 'test@test.com',
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/search-tenant/$tenant->phone_number");

        $response->assertJson(
            [
                'data' => [
                    'id' => $tenant->id,
                    'name' => $tenant->name,
                    'email' => $tenant->email,
                    'phone_number' => $tenant->phone_number,
                    'gender' => $tenant->gender,
                    'job_information' => $tenant->job_information
                ]
            ]
        );
    }

    public function testSearchTenantByPhoneNumberWithNonExistantNumber(): void
    {
        $user = factory(User::class)->create(
            [
                'phone_number' => '0811111111',
                'role' => 'user',
                'is_owner' => 'false'
            ]
        );
        $tenant = factory(PotentialTenant::class)->create(
            [
                'phone_number' => '0811111111',
            ]
        );

        $response = $this->setWebApiCookie()->actingAs($this->nonConsultant, 'passport')
            ->json('GET', "/consultant-tools/api/tenant/potential/search-tenant/082222222222");

        $response->assertJson(
            [
                'data' => [
                    'id' => null,
                    'name' => null,
                    'email' => null,
                    'phone_number' => null,
                    'gender' => null,
                    'job_information' => null
                ]
            ]
        );
    }

    public function testSearchPropertyByNameWithMappedConsultant(): void
    {
        factory(ConsultantRoom::class)->create(['consultant_id' => $this->consultant->consultant->id]);

        $result = [
            'took' => 113,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => 0.105360515,
                'hits' => [
                    [
                        '_index' => 'mamisearch-room',
                        '_type' => '_doc',
                        '_id' => '20',
                        '_score' => 0.105360515,
                        '_source' => [
                            'room_id' => 20,
                            'song_id' => 122,
                            'apartment_id' => null,
                            'title' => 'Kos Exclusive Kirlin.payton',
                            'slug' => '',
                            'array_gender' => 0,
                            'gender' => 'campur',
                            'type' => 'kos',
                            'code' => '',
                            'is_mamipay_active' => true,
                            'consultant_ids' => [
                                5,
                                6,
                                7
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->service->shouldReceive('search')
            ->once()
            ->with('exclusive', $this->consultant->consultant->id)
            ->andReturn($result);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/tenant/potential/search-property/exclusive'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => 122,
                        'name' => 'Kos Exclusive Kirlin.payton'
                    ]
                ]
            ]
        );
    }

    public function testSearchPropertyByNameWithUnmappedConsultant(): void
    {
        $result = [
            'took' => 113,
            'timed_out' => false,
            '_shards' => [
                'total' => 5,
                'successful' => 5,
                'skipped' => 0,
                'failed' => 0
            ],
            'hits' => [
                'total' => 1,
                'max_score' => 0.105360515,
                'hits' => [
                    [
                        '_index' => 'mamisearch-room',
                        '_type' => '_doc',
                        '_id' => '20',
                        '_score' => 0.105360515,
                        '_source' => [
                            'room_id' => 20,
                            'song_id' => 122,
                            'apartment_id' => null,
                            'title' => 'Kos Exclusive Kirlin.payton',
                            'slug' => '',
                            'array_gender' => 0,
                            'gender' => 'campur',
                            'type' => 'kos',
                            'code' => '',
                            'is_mamipay_active' => true,
                            'consultant_ids' => [
                                5,
                                6,
                                7
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->service->shouldReceive('search')
            ->once()
            ->with('exclusive')
            ->andReturn($result);

        $response = $this->setWebApiCookie()->actingAs($this->consultant, 'passport')->json(
            'GET',
            '/consultant-tools/api/tenant/potential/search-property/exclusive'
        );
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => 122,
                        'name' => 'Kos Exclusive Kirlin.payton'
                    ]
                ]
            ]
        );
    }

    public function testAddOrUpdateWithoutIdAndRoomIdShouldCreateNewPotentialTenant(): void
    {
        $params = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_name' => $this->faker->buildingNumber,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => $this->faker->word,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_tools_potential_tenant',
            [
                'name' => $params['name'],
                'email' => $params['email'],
                'phone_number' => $params['phone_number'],
                'gender' => $params['gender'],
                'job_information' => $params['job_information'],
                'designer_id' => null,
                'property_name' => $params['room_name'],
                'price' => $params['price'],
                'due_date' => $params['due_date'],
                'scheduled_date' => $params['scheduled_date'],
                'duration_unit' => $params['duration_unit'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'activity' => $params['remarks_category'],
                'content' => $params['remarks_note'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateWithoutIdShouldCreateNewPotentialTenant(): void
    {
        $room = factory(Room::class)->create();
        $params = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => $this->faker->word,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_tools_potential_tenant',
            [
                'name' => $params['name'],
                'email' => $params['email'],
                'phone_number' => $params['phone_number'],
                'gender' => $params['gender'],
                'job_information' => $params['job_information'],
                'designer_id' => $room->id,
                'property_name' => $room->name,
                'price' => $params['price'],
                'due_date' => $params['due_date'],
                'scheduled_date' => $params['scheduled_date'],
                'duration_unit' => $params['duration_unit'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'activity' => $params['remarks_category'],
                'content' => $params['remarks_note'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateWithoutIdAndNoteShouldCreateNewPotentialTenant(): void
    {
        $room = factory(Room::class)->create();
        $params = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_tools_potential_tenant',
            [
                'name' => $params['name'],
                'email' => $params['email'],
                'phone_number' => $params['phone_number'],
                'gender' => $params['gender'],
                'job_information' => $params['job_information'],
                'designer_id' => $room->id,
                'property_name' => $room->name,
                'price' => $params['price'],
                'due_date' => $params['due_date'],
                'scheduled_date' => $params['scheduled_date'],
                'duration_unit' => $params['duration_unit'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    // TODO: Add test case where only note is empty or remove this todo

    public function testAddOrUpdateShouldUpdatePotentialTenant(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $room = factory(Room::class)->create();
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => $this->faker->word,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_tools_potential_tenant',
            [
                'id' => $tenant->id,
                'name' => $params['name'],
                'email' => $params['email'],
                'phone_number' => $params['phone_number'],
                'gender' => $params['gender'],
                'job_information' => $params['job_information'],
                'designer_id' => $room->id,
                'property_name' => $room->name,
                'price' => $params['price'],
                'due_date' => $params['due_date'],
                'scheduled_date' => $params['scheduled_date'],
                'duration_unit' => $params['duration_unit'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateShouldCreateNewNote(): void
    {
        $room = factory(Room::class)->create();
        $tenant = factory(PotentialTenant::class)->create();
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => $this->faker->word,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id,
                'activity' => $params['remarks_category'],
                'content' => $params['remarks_note'],
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateShouldSoftDeleteOldNote(): void
    {
        $room = factory(Room::class)->create();
        $tenant = factory(PotentialTenant::class)->create();
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => $this->faker->word,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertSoftDeleted(
            'consultant_note',
            [
                'id' => $note->id,
                'activity' => $note->activity,
                'content' => $note->content,
                'noteable_type' => $note->noteable_type,
                'noteable_id' => $note->noteable_id,
                'consultant_id' => $note->consultant_id
            ]
        );
        $this->assertDatabaseHas(
            'consultant_note',
            [
                'activity' => $params['remarks_category'],
                'content' => $params['remarks_note'],
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateWithOnlyRemarksCategoryShouldSave(): void
    {
        $room = factory(Room::class)->create();
        $tenant = factory(PotentialTenant::class)->create();
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'activity' => $params['remarks_category'],
                'content' => null,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateWithEmptyNoteShouldSave(): void
    {
        $room = factory(Room::class)->create();
        $tenant = factory(PotentialTenant::class)->create();
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => '',
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'activity' => $params['remarks_category'],
                'content' => null,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testAddOrUpdateWithNullNoteShouldSave(): void
    {
        $room = factory(Room::class)->create();
        $tenant = factory(PotentialTenant::class)->create();
        $params = [
            'id' => $tenant->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'job_information' => $this->faker->randomElement(['Mahasiswa', 'Karyawan']),
            'room_id' => $room->song_id,
            'price' => $this->faker->numberBetween(1000000, 6000000),
            'due_date' => $this->faker->numberBetween(1, 31),
            'scheduled_date' => $this->faker->date(),
            'duration_unit' => $this->getRandomDurationUnit(),
            'remarks_category' => $this->getRandomRemarks(),
            'remarks_note' => null,
        ];
        $response = $this
            ->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/addOrUpdate', $params);

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'activity' => $params['remarks_category'],
                'content' => null,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'consultant_id' => $this->consultant->consultant->id
            ]
        );
    }

    public function testSaveNoteWithNonExistingPotentialTenant(): void
    {
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json('POST', '/consultant-tools/api/tenant/potential/save-note/0');
        $response->assertStatus(401);
        $response->assertJson(
            [
                'status' => false,
                'meta' => [
                    'response_code' => 401,
                    'code' => 1,
                    'severity' => 'INVALID_REQUEST',
                    'message' => 'Invalid Request'
                ]
            ]
        );
    }

    public function testSaveNoteShouldSoftDeleteOldNote(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'content' => 'Hello hello',
                    'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );
        $this->assertSoftDeleted(
            'consultant_note',
            [
                'id' => $note->id,
                'content' => $note->content,
                'noteable_type' => $note->noteable_type,
                'noteable_id' => $note->noteable_id,
                'activity' => $note->activity
            ]
        );
    }

    public function testSaveNoteWithExistingNoteShouldCreateNewNote(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $note = factory(ConsultantNote::class)->create(
            [
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'content' => 'Hello hello',
                    'note_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => 'Hello hello',
                'consultant_id' => $this->consultant->consultant->id,
                'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
    }

    public function testSaveNoteWithoutExistingNoteShouldCreateNewNote(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'content' => 'Hello hello',
                    'note_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => 'Hello hello',
                'consultant_id' => $this->consultant->consultant->id,
                'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
    }

    public function testSaveNoteWithOnlyRemarksShouldSave(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'note_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => null,
                'consultant_id' => $this->consultant->consultant->id,
                'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
    }

    public function testSaveNoteWithEmptyContentShouldSave(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'content' => '',
                    'note_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => null,
                'consultant_id' => $this->consultant->consultant->id,
                'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
    }

    public function testSaveNoteWithNullContentShouldSave(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $response = $this->setWebApiCookie()
            ->actingAs($this->consultant, 'passport')
            ->json(
                'POST',
                "/consultant-tools/api/tenant/potential/save-note/$tenant->id",
                [
                    'content' => null,
                    'note_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY
                ]
            );

        $this->assertDatabaseHas(
            'consultant_note',
            [
                'content' => null,
                'consultant_id' => $this->consultant->consultant->id,
                'activity' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
                'noteable_id' => $tenant->id
            ]
        );
    }

    private function getRandomRemarks(): string
    {
        return $this->faker->randomElement(
            [
                PotentialTenantRemarks::REMARKS_CHAT_UNRESPONSIVE,
                PotentialTenantRemarks::REMARKS_DOES_NOT_INHABIT_ROOM,
                PotentialTenantRemarks::REMARKS_HAS_ACTIVE_CONTRACT,
                PotentialTenantRemarks::REMARKS_HAS_BOOKING_DATA,
                PotentialTenantRemarks::REMARKS_HAS_PAID_TO_OWNER,
                PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                PotentialTenantRemarks::REMARKS_OTHER,
                PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                PotentialTenantRemarks::REMARKS_PHONE_NUMBER_UNREACHABLE,
                PotentialTenantRemarks::REMARKS_REJECT_PAYMENT_TO_MAMIPAY,
                PotentialTenantRemarks::REMARKS_WANT_TO_CONFIRM_TO_OWNER,
            ]
        );
    }

    private function getRandomDurationUnit(): string
    {
        return $this->faker->randomElement(
            [
                'day',
                'week',
                'month',
                'year',
                '3_month',
                '6_month'
            ]
        );
    }
}
