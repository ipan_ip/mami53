<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Criteria\Room\RoomUnitCriteria;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Presenters\Room\RoomUnitPresenter;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Booking\BookingUserRepositoryEloquent;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\Room\RoomUnitRepositoryEloquent;
use App\Repositories\RoomRepository;
use App\Repositories\RoomRepositoryEloquent;
use App\Services\RoomUnit\RoomUnitService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Collection as SupportCollection;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Prettus\Repository\Criteria\RequestCriteria;

class RoomUnitControllerTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;
    use WithFaker;
    use WithoutMiddleware;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $permission = factory(Permission::class)->create(['name' => 'access-consultant']);
        $role = factory(Role::class)->create(['name' => 'consultant']);
        $this->user = factory(User::class)->create(
            ['role' => UserRole::Administrator, 'email' => 'test@email.com', 'password' => bcrypt(md5('password'))]
        );

        $this->user->attachRole($role);
        $role->attachPermission($permission);
    }

    public function testGetRoomAvailableWithInvalidRoom(): void
    {
        $roomRepository = Mockery::mock(RoomRepositoryEloquent::class);
        $roomRepository->shouldReceive('where')
            ->andReturn($roomRepository);
        $roomRepository->shouldReceive('first')
            ->andReturn(null);

        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 0 . '/available');
        $response->assertJson(
            [
                'status' => false,
                'message' => 'Kost tidak bisa ditemukan'
            ]
        );
    }

    public function testGetRoomAvailable(): void
    {
        $room = factory(Room::class)->create();
        $unit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 'Kost',
            'floor' => 1,
            'occupied' => true,
        ]);
        $unit->contract_id = null;
        $unit->id = 1;
        $units = new Collection([$unit]);
        $bookingRepository = Mockery::mock(BookingUserRepositoryEloquent::class);
        $bookingRepository->shouldReceive('getRoomUnitList')
            ->andReturn(
                $units
            );
        $roomRepository = Mockery::mock(RoomRepositoryEloquent::class);
        $roomRepository->shouldReceive('where')
            ->andReturn($roomRepository);
        $roomRepository->shouldReceive('first')
            ->andReturn($room);

        $this->app->instance(BookingUserRepository::class, $bookingRepository);
        $this->app->instance(RoomRepository::class, $roomRepository);
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 1 . '/available');
        $response->assertJson(
            [
                'status' => true,
                'room_units' => [
                    [
                        'id' => 1,
                        'has_active_contract' => false,
                        'name' => 'Kost',
                        'floor' => 1,
                        'occupied' => true
                    ]
                ]
            ]
        );
    }

    public function testGetRoomAvailableWithRoomNeedBackfillShouldReturnCorrectly(): void
    {
        $room = factory(Room::class)->create([
            'room_count' => 2,
            'room_available' => 1,
        ]);

        $roomRepository = Mockery::mock(RoomRepositoryEloquent::class);
        $roomRepository->shouldReceive('where')
            ->andReturn($roomRepository);
        $roomRepository->shouldReceive('first')
            ->andReturn($room);

        $this->app->instance(RoomRepository::class, $roomRepository);
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 1 . '/available');

        $response->assertJson([
            'status' => true,
            'room_units' => [
                [
                    'name' => '1',
                    'floor' => '1',
                    'occupied' => false,
                    'has_active_contract' => false
                ]
            ]
        ]);
    }

    public function testGetRoomAvailableWithRoomNeedBackfillShouldCallBackfill(): void
    {
        $room = factory(Room::class)->create([
            'room_count' => 2,
            'room_available' => 1,
        ]);

        $roomUnit = collect([factory(RoomUnit::class)->make(['occupied' => false, 'designer_id' => $room->id])]);
        $roomRepository = Mockery::mock(RoomRepositoryEloquent::class);
        $roomRepository->shouldReceive('where')
            ->andReturn($roomRepository);
        $roomRepository->shouldReceive('first')
            ->andReturn($room);

        $roomUnitRepository = Mockery::mock(RoomUnitRepositoryEloquent::class);
        $roomUnitRepository->shouldReceive('backFillRoomUnit')
            ->withArgs(function ($arg) use ($room) {
                return method_exists($arg, 'is') && $arg->is($room);
            })
            ->once()
            ->andReturn($roomUnit);

        $this->app->instance(RoomRepository::class, $roomRepository);
        $this->app->instance(RoomUnitRepository::class, $roomUnitRepository);
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 1 . '/available');
    }

    public function testGetRoomListShouldReturnCorrectly(): void
    {
        $repo = Mockery::mock(RoomUnitRepositoryEloquent::class);
        $roomRepo = Mockery::mock(RoomRepositoryEloquent::class);
        $this->app->instance(RoomRepository::class, $roomRepo);
        $this->app->instance(RoomUnitRepository::class, $repo);

        $roomRepo->shouldReceive('popCriteria')
            ->with(Mockery::type(RequestCriteria::class))
            ->andReturn($roomRepo);

        $roomRepo->shouldReceive('popCriteria')
            ->with(Mockery::type('App\Criteria\Room\ActiveCriteria'))
            ->andReturn($roomRepo);

        $roomRepo->shouldReceive('where')
            ->with('song_id', 1)
            ->once()
            ->andReturn($roomRepo);

        $roomRepo->shouldReceive('first')
            ->once()
            ->andReturn(
                factory(Room::class)->create()
            );

        $repo->shouldReceive('setPresenter')
            ->with(Mockery::type(RoomUnitPresenter::class))
            ->andReturn($repo);

        $repo->shouldReceive('pushCriteria')
            ->with(Mockery::type(RoomUnitCriteria::class))
            ->andReturn($repo);
        $repo->shouldReceive('getRoomUnitCount')
            ->andReturn(1);
        $repo->shouldReceive('getRoomUnitList')
            ->andReturn(
                [
                    'data' => [
                        [
                            'id' => 123541,
                            'name' => 'Kamar Kapal Pecah',
                            'floor' => 1,
                            'occupied' => true,
                            'disable' => true,
                        ]
                    ]
                ]
            );

        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 1);

        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => 123541,
                        'name' => 'Kamar Kapal Pecah',
                        'floor' => 1,
                        'occupied' => true,
                        'disable' => true,
                    ]
                ],
                'pagination' => [
                    'page' => 1,
                    'limit' => 500,
                    'offset' => 0,
                    'total' => 1,
                    'has-more' => false,
                ]
            ]
        );
    }

    public function testGetTotalRoomNotFoundShouldReturn0AsTotalRoom()
    {
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/1/total');

        $response->assertSuccessful();
        $response->assertJson(['total_room' => 0]);
    }

    public function testGetTotalRoomFoundShouldReturnCorrectTotalRoom()
    {
        factory(Room::class)->create(['song_id' => 1, 'room_count' => 10]);
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/1/total');

        $response->assertSuccessful();
        $response->assertJson(['total_room' => 10]);
    }

    public function testGetRoomListShouldReturnCorrectData(): void
    {
        $this->app->instance(RoomUnitRepository::class, $this->app->make(RoomUnitRepositoryEloquent::class));

        $room = factory(Room::class)->create();
        $unit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);

        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . $room->song_id);

        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $unit->id,
                    'name' => $unit->name,
                    'floor' => $unit->floor,
                    'occupied' => $unit->occupied,
                    'disable' => $unit->disable
                ]
            ]
        ]);
    }

    public function testGetRoomListWithInvalidSongId(): void
    {
        $this->app->instance(RoomUnitRepository::class, $this->app->make(RoomUnitRepositoryEloquent::class));

        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . 0);

        $response->assertJson([
            'status' => false,
            'message' => 'Kost tidak dapat ditemukan'
        ]);
    }

    public function testCrudBulk(): void
    {
        $room = factory(Room::class)->create();
        $unit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id
        ]);
        $repo = $this->app->make(RoomUnitRepositoryEloquent::class);
        $this->app->instance(RoomUnitRepository::class, $repo);
        $response = $this->actingAs($this->user)
            ->setWebApiCookie()
            ->json('GET', '/consultant-tools/api/room-unit/' . $room->song_id);
        $response->assertJson(
            [
                'status' => true,
                'data' => [
                    [
                        'id' => $unit->id,
                        'name' => $unit->name,
                        'floor' => $unit->floor,
                        'occupied' => $unit->occupied,
                        'disable' => false,
                    ]
                ],
                'pagination' => [
                    'page' => 1,
                    'limit' => 500,
                    'offset' => 0,
                    'total' => 1,
                    'has-more' => false,
                ]
            ]
        );
    }
}
