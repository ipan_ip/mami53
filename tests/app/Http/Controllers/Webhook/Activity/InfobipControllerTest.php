<?php

namespace App\Http\Controllers\Webhook\Activity;

use App\Entities\Activity\ActivationCode;
use App\Test\MamiKosTestCase;
use Exception;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class InfobipControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    const URL_PREFIX_INFOBIP_DELIVERY_REPORT_WEBHOOK = 'hook/v1/activity/infobip/delivery-report/';

    public function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }
    
    /**
     * @group UG
     * @group UG-1433
     * @group App/Http/Controllers/Webhook/Activity/InfobipController
     */
    public function testPostDeliveryReportInvalidId()
    {
        $response = $this->post(self::URL_PREFIX_INFOBIP_DELIVERY_REPORT_WEBHOOK . mt_rand());
        $response->assertStatus(500);
    }
    
    /**
     * @group UG
     * @group UG-1433
     * @group App/Http/Controllers/Webhook/Activity/InfobipController
     */
    public function testPostDeliveryReportInvalidRequest()
    {
        /** @var ActivationCode */
        $activationCode = factory(ActivationCode::class)->create();
        $response = $this->post(self::URL_PREFIX_INFOBIP_DELIVERY_REPORT_WEBHOOK . $activationCode->id, []);
        $response->assertStatus(500);
    }
    
    /**
     * @group UG
     * @group UG-1433
     * @group App/Http/Controllers/Webhook/Activity/InfobipController
     */
    public function testPostDeliveryReportValidRequest()
    {
        /** @var ActivationCode */
        $activationCode = factory(ActivationCode::class)->create();
        $data = json_decode('{
            "results": [
                {
                    "bulkId": "BULK-ID-123-xyz",
                    "messageId": "MESSAGE-ID-123-xyz",
                    "to": "41793026727",
                    "sentAt": "2019-11-09T16:00:00.000+0000",
                    "doneAt": "2019-11-09T16:00:00.000+0000",
                    "smsCount": 1,
                    "price": {
                        "pricePerMessage": 0.01,
                        "currency": "EUR"
                    },
                    "status": {
                        "groupId": 3,
                        "groupName": "EXPIRED",
                        "id": 3,
                        "name": "EXPIRED_EXPIRED",
                        "description": "Message delivered to handset"
                    },
                    "error": {
                        "groupId": 0,
                        "groupName": "Ok",
                        "id": 0,
                        "name": "NO_ERROR",
                        "description": "No Error",
                        "permanent": false
                    }
                }
            ]
        }', true);
        $response = $this->postJson(self::URL_PREFIX_INFOBIP_DELIVERY_REPORT_WEBHOOK . $activationCode->id, $data);
        $response->assertStatus(200);
        $activationCode->refresh();

        $this->assertNotNull($activationCode->delivery_report);
    }
}