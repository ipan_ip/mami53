<?php
namespace Test\App\Http\Controllers\Web;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Entities\Premium\PremiumPackage;

class PremiumControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    private const PREMIUM_PURCHASE = '/garuda/owner/premium/purchase';

    public function setUp() : void
    {
        parent::setUp();
    }

    public function testPremiumPurchaseNotfoundPackage()
    {
        $response = $this->json('POST', self::PREMIUM_PURCHASE, []);
        $responseJson = json_decode($response->getContent());
        
        $this->assertFalse($responseJson->status);
    }

    public function testPremiumPurchaseNotOwner()
    {
        $premiumPackage = factory(PremiumPackage::class)->create();
        $user = factory(User::class)->create([
            'is_owner' => 'false',
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::PREMIUM_PURCHASE, [
            'package_id' => $premiumPackage->id
        ]);

        $responseJson = json_decode($response->getContent());
        $this->assertFalse($responseJson->status);
    }

    public function testPremiumPurchaseSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create();
        $user = factory(User::class)->create([
            'is_owner' => 'true',
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::PREMIUM_PURCHASE, [
            'package_id' => $premiumPackage->id
        ]);

        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }
}