<?php

namespace App\Http\Controllers\Web\Billing;

use App\Test\MamiKosTestCase;
use Illuminate\View\View;

class LandingControllerTest extends MamiKosTestCase
{
    public function testIndex()
    {
        $res = (new LandingController)->index();
        $this->assertInstanceOf(View::class, $res);
    }
}