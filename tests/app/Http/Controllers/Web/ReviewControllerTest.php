<?php

namespace test\app\Http\Controllers\Web;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\Review;
use App\Entities\Device\UserDevice;
use phpmock\MockBuilder;
use Mockery;

class ReviewControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_URL_REVIEW_BY_ID = 'garuda/stories/review/%u';
    private const DELETE_URL_REVIEW_BY_ID = 'admin/review/destroy/%u';

    private $mockForAppDevice;

    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetListRiview()
    {
        // prepare data
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);

        // impersonate web request
        $this->app->device = null;

        factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => $room->id,
            'comfort'            => 4,
            'cleanliness'        => 4,
            'safe'               => 4,
            'price'              => 4,
            'room_facility'      => 4,
            'public_facility'    => 4,
            'content'            => 'Review Content',
            'scale'              => 4,
        ]);

        // call api
        $url        = sprintf(self::GET_URL_REVIEW_BY_ID, $room->song_id);
        $response   = $this->actingAs($user)->json('GET', $url);
        $json       = json_decode($response->getContent());
        $review     = $json->review[0];

        // assert 
        $response->assertStatus(200);

        $this->assertEquals(5, $review->clean);
        $this->assertEquals(5, $review->happy);
        $this->assertEquals(5, $review->safe);
        $this->assertEquals(5, $review->pricing);
        $this->assertEquals(5, $review->room_facilities);
        $this->assertEquals(5, $review->public_facilities);
    }

    public function testGetListReviewForWeb()
    {
        // prepare data
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);

        // impersonate web request
        $this->app->device = null;

        factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => $room->id,
            'comfort'            => 2.4,
            'cleanliness'        => 2.4,
            'safe'               => 2.4,
            'price'              => 2.4,
            'room_facility'      => 2.4,
            'public_facility'    => 2.4,
            'content'            => 'Review Content',
            'scale'              => 2.4,
        ]);

        // call api
        $url        = sprintf(self::GET_URL_REVIEW_BY_ID, $room->song_id);
        $response   = $this->actingAs($user)->json('GET', $url);
        $json       = json_decode($response->getContent());
        $review     = $json->review[0];

        // assert 
        $response->assertStatus(200);

        $this->assertEquals(3, $review->clean);
        $this->assertEquals(3, $review->happy);
        $this->assertEquals(3, $review->safe);
        $this->assertEquals(3, $review->pricing);
        $this->assertEquals(3, $review->room_facilities);
        $this->assertEquals(3, $review->public_facilities);
    }

    public function testGetListReviewUnsupport5StarDevice()
    {
        // prepare data
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);

        // impersonate android request as Unsupport device
        $version  = ((int) UserDevice::MIN_RATING_5_APP_VERSION_ANDROID) - 1000000;
        $device   = factory(UserDevice::class)->state('login')->create([
            'user_id'           => $user->id,
            'app_version_code'  => $version,
        ]);
        $this->app->device = $device;

        factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => $room->id,
            'comfort'            => 2.4,
            'cleanliness'        => 2.4,
            'safe'               => 2.4,
            'price'              => 2.4,
            'room_facility'      => 2.4,
            'public_facility'    => 2.4,
            'content'            => 'Review Content',
            'scale'              => 2.4,
        ]);

        // call api
        $url        = sprintf(self::GET_URL_REVIEW_BY_ID, $room->song_id);
        $response   = $this->actingAs($user)->json('GET', $url);
        $json       = json_decode($response->getContent());
        $review     = $json->review[0];

        // assert 
        $response->assertStatus(200);

        $this->assertEquals(2, $review->clean);
        $this->assertEquals(2, $review->happy);
        $this->assertEquals(2, $review->safe);
        $this->assertEquals(2, $review->pricing);
        $this->assertEquals(2, $review->room_facilities);
        $this->assertEquals(2, $review->public_facilities);
    }

    public function testGetListReviewUnsupport5StarDeviceWithMaxReview()
    {
        // prepare data
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);

        // impersonate android request as Unsupport device
        $version  = ((int) UserDevice::MIN_RATING_5_APP_VERSION_ANDROID) - 1000000;
        $device   = factory(UserDevice::class)->state('login')->create([
            'user_id'           => $user->id,
            'app_version_code'  => $version,
        ]);
        $this->app->device = $device;

        factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => $room->id,
            'comfort'            => 4,
            'cleanliness'        => 4,
            'safe'               => 4,
            'price'              => 4,
            'room_facility'      => 4,
            'public_facility'    => 4,
            'content'            => 'Review Content',
            'scale'              => 4,
        ]);

        // call api
        $url        = sprintf(self::GET_URL_REVIEW_BY_ID, $room->song_id);
        $response   = $this->actingAs($user)->json('GET', $url);
        $json       = json_decode($response->getContent());
        $review     = $json->review[0];

        // assert 
        $response->assertStatus(200);

        $this->assertEquals(4, $review->clean);
        $this->assertEquals(4, $review->happy);
        $this->assertEquals(4, $review->safe);
        $this->assertEquals(4, $review->pricing);
        $this->assertEquals(4, $review->room_facilities);
        $this->assertEquals(4, $review->public_facilities);
    }

    public function testDestroy()
    {
        // prepare data
        $room = factory(Room::class)->create(['is_active' => true]);
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);
        $rating = factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => $room->id,
            'content'            => 'Bagus kosnya cak!',
        ]);

        // run destroy review
        $url = sprintf(self::DELETE_URL_REVIEW_BY_ID, $rating->id);
        $response = $this->actingAs($user)->get($url);

        // assert
        $response->assertStatus(302);
        $response->assertRedirect('/admin/review');
    }

    public function testDestroyWhenReviewDoenstExist()
    {
        // prepare data
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);

        // run destroy review
        $url = sprintf(self::DELETE_URL_REVIEW_BY_ID, 9999);
        $response = $this->actingAs($user)->get($url);

        // assert
        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Review unavailable');
    }

    public function testDestroyWhenRoomDoensExist()
    {
        // prepare data
        $user = factory(User::class)->create(['password' => Hash::make("user_password")]);
        $rating = factory(Review::class)->create([
            'status'             => 'live',
            'user_id'            => $user->id,
            'designer_id'        => 99999, // unavailable
            'content'            => 'Bagus kosnya cak!',
        ]);

        // run destroy review
        $url = sprintf(self::DELETE_URL_REVIEW_BY_ID, $rating->id);
        $response = $this->actingAs($user)->get($url);

        // assert
        $response->assertStatus(302);
        $response->assertSessionHas('error_message', 'Delete Failed, Room is not exist');
    }
}
