<?php

namespace App\Http\Controllers\Web;

use App\Entities\User\UserSocial;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Socialite;
use Laravel\Socialite\SocialiteManager;
use SocialiteProviders\Manager\OAuth2\User as OAuth2User;
use SocialiteProviders\Apple\Provider;
use App\User;

class AuthControllerTest extends MamiKosTestCase
{

    use WithFaker;
    use WithoutMiddleware;

    private const URL_CALLBACK_APPLE = '/auth/apple/callback';

    public function setUp() : void
    {
        parent::setUp();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInRegisterFlowWithFullInformation()
    {
        $mockUserData = [
            'email' => $this->faker()->email,
            'name' => $this->faker()->name,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
        $userSocial = UserSocial::where('identifier', $mockUserData['id'])->first();
        $this->assertNotNull($userSocial);
        $this->assertEquals($mockUserData['email'], $userSocial->email);
        $this->assertEquals($mockUserData['name'], $userSocial->name);
        $this->assertEquals($mockUserData['token'], $userSocial->token);
        $user = User::find($userSocial->user_id);
        $this->assertNotNull($user);
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInRegisterFlowWithoutName()
    {
        $mockUserData = [
            'email' => $this->faker()->email,
            'name' => null,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
        $userSocial = UserSocial::where('identifier', $mockUserData['id'])->first();
        $this->assertNotNull($userSocial);
        $this->assertEquals($mockUserData['email'], $userSocial->email);
        $this->assertEquals(User::DEFAULT_APPLE_USER_NAME, $userSocial->name);
        $this->assertEquals($mockUserData['token'], $userSocial->token);
        $user = User::find($userSocial->user_id);
        $this->assertNotNull($user);
        $this->assertEquals(User::DEFAULT_APPLE_USER_NAME, $user->name);
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInRegisterFlowWithoutNameAndEmail()
    {
        $mockUserData = [
            'email' => null,
            'name' => null,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
        $userSocial = UserSocial::where('identifier', $mockUserData['id'])->first();
        $this->assertNotNull($userSocial);
        $this->assertEquals(UserSocial::DEFAULT_EMAIL, $userSocial->email);
        $this->assertEquals(User::DEFAULT_APPLE_USER_NAME, $userSocial->name);
        $this->assertEquals($mockUserData['token'], $userSocial->token);
        $user = User::find($userSocial->user_id);
        $this->assertNotNull($user);
        $this->assertEquals(User::DEFAULT_APPLE_USER_NAME, $user->name);
        $this->assertNull($user->email);
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInLoginFlowWithFullInformation()
    {
        $mockUserData = [
            'email' => $this->faker()->email,
            'name' => $this->faker()->name,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        $this->createExistingUserSocial($mockUserData);
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInLoginFlowWithoutName()
    {
        $mockUserData = [
            'email' => $this->faker()->email,
            'name' => null,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        $this->createExistingUserSocial($mockUserData);
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
    }
    
    /**
     * @group UG
     * @group UG-2570
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInLoginFlowWithoutNameAndEmail()
    {
        $mockUserData = [
            'email' => null,
            'name' => null,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        $this->createExistingUserSocial($mockUserData);
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response->assertRedirect();
    }

    /**
     * @group UG
     * @group UG-2577
     * @group App\Http\Controllers\Web\AuthController
     */
    public function testAppleSignInRedirectToCorrectUrl()
    {
        session()->flash('url', url('/favicon.ico'));
        $mockUserData = [
            'email' => null,
            'name' => null,
            'token' => $this->faker()->uuid,
            'id' => $this->faker()->uuid,
        ];
        /** @var \Mockery\MockInterface|Provider */
        $mockDriver = $this->spy(Provider::class);
        $mockDriver->shouldReceive('user')->andReturn((new OAuth2User())->map($mockUserData));
        Socialite::shouldReceive('driver')->andReturn($mockDriver);
        $response = $this->post(self::URL_CALLBACK_APPLE);
        $response = $this->followOneRedirect($response);
        $this->assertEquals(url('/favicon.ico'), $response->headers->get('Location'));
    }

    /**
     * Following only one redirection
     *
     * @param  \Illuminate\Http\Response  $response
     * @return \Illuminate\Http\Response|\Illuminate\Foundation\Testing\TestResponse
     */
    private function followOneRedirect($response)
    {
        if ($response->isRedirect()) {
            $response = $this->get($response->headers->get('Location'));
        }
        return $response;
    }
    /**
     * Create user social and user account to simulate existing user social flow
     *
     * @param array $userData
     * @return void
     */
    private function createExistingUserSocial(array $userData): void
    {
        $user = factory(User::class)->create([
            'name' => $userData['name'] ?? 'TEST',
            'email' => $userData['email'],
        ]);
        factory(UserSocial::class)->create([
            'user_id' => $user,
            'name' => $userData['name'] ?? UserSocial::DEFAULT_NAME,
            'email' => $userData['email'] ?? UserSocial::DEFAULT_EMAIL,
            'identifier' => $userData['id'],
            'token' => $userData['token'],
        ]);
    }
}