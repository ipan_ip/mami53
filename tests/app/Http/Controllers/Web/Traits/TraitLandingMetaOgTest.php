<?php

namespace App\Http\Controllers\Web\Traits;

use App\Constants\Periodicity;
use App\Entities\Landing\LandingMetaOg;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;

class TraitLandingMetaOgTest extends MamiKosTestCase
{
    use TraitLandingMetaOg;

    protected function setUp() : void
    {
        parent::setUp();

        $this->faker = Faker::create();
    }

    public function testSetLandingMetaOgHomepage_Success()
    {
        $pageType = LandingMetaOg::HOMEPAGE;
        $meta = factory(LandingMetaOg::class)->states('active', $pageType)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgHomepage_Null()
    {
        $pageType = LandingMetaOg::HOMEPAGE;

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn(null);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame(LandingMetaOg::HOMEPAGE_DEFAULT_TITLE, $res['title']);
        $this->assertSame(LandingMetaOg::HOMEPAGE_DEFAULT_DESCRIPTION, $res['description']);
        $this->assertSame(LandingMetaOg::HOMEPAGE_DEFAULT_KEYWORDS, $res['keywords']);
        $this->assertSame(LandingMetaOg::HOMEPAGE_DEFAULT_IMAGE, $res['image_url']);
    }

    public function testSetLandingMetaOgBookingOwner_Success()
    {
        $pageType = LandingMetaOg::LANDING_BOOKING_OWNER;
        $meta = factory(LandingMetaOg::class)->states('active', $pageType)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgBookingOwner_Null()
    {
        $pageType = LandingMetaOg::LANDING_BOOKING_OWNER;

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn(null);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame(LandingMetaOg::BOOKING_OWNER_DEFAULT_TITLE, $res['title']);
        $this->assertSame(LandingMetaOg::BOOKING_OWNER_DEFAULT_DESCRIPTION, $res['description']);
        $this->assertSame(LandingMetaOg::BOOKING_OWNER_DEFAULT_KEYWORDS, $res['keywords']);
        $this->assertSame(LandingMetaOg::BOOKING_OWNER_DEFAULT_IMAGE, $res['image_url']);
    }

    public function testSetLandingMetaOgLandingArea_Success()
    {
        $pageType = LandingMetaOg::LANDING_AREA;
        $meta = factory(LandingMetaOg::class)->states('active', $pageType)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([
                'heading_1' => 'Kost Testing Heading',
                'keyword' => 'Kost Testing',
                'price_min' => mt_rand(1, 99999),
                'breadcrumb' => [
                    [
                        "url" => $this->faker->url(),
                        "name" => "Home"
                    ],
                    [
                        "url" => $this->faker->url(),
                        "name" => "Kost Jogja"
                    ]
                ],
                'dummy_counter' => mt_rand(1, 99999),
                'rent_type' => mt_rand(0, 3),
                'type' => 'area'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgLandingArea_Null()
    {
        $pageType = LandingMetaOg::LANDING_AREA;

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn(null);

        $res = $this->setLandingMetaOg([
                'heading_1' => 'Landing Kost Testing Heading',
                'keyword' => 'Kost Testing',
                'price_min' => mt_rand(1, 99999),
                'breadcrumb' => [
                    [
                        "url" => $this->faker->url(),
                        "name" => "Home"
                    ],
                    [
                        "url" => $this->faker->url(),
                        "name" => "Kost Jogja"
                    ]
                ],
                'dummy_counter' => mt_rand(1, 99999),
                'rent_type' => mt_rand(0, 3),
                'type' => 'area'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame('Landing Kost Testing Heading', $res['title']);
        $this->assertSame(LandingMetaOg::LANDING_DEFAULT_IMAGE, $res['image_url']);
    }

    public function testSetLandingMetaOgDetail_Success()
    {
        $pageType = LandingMetaOg::DETAIL_KOST;
        $meta = factory(LandingMetaOg::class)->states('active', $pageType)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([
                'detail' => [
                    'area_city' => $this->faker->city,
                    'room_title' => 'Detail Kost Testing Room Title',
                    'photo_url' => [
                        'medium' => $this->faker->imageUrl(640, 480)
                    ],
                    'top_facilities' => [
                        [
                            "id" => 1,
                            "name" => "K. Mandi Dalam",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/JKp9z6Xm.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/TCWpQeCV.png"
                        ],
                        [
                            "id" => 3,
                            "name" => "Shower",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/PybSBNxB.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/fY7Fjerx.png"
                        ],
                        [
                            "id" => 10,
                            "name" => "Kasur",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/KrCtQKLC.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/1BsAad8X.png"
                        ]
                    ],
                    'price_monthly' => mt_rand(1, 99999),
                    'is_booking' => mt_rand(0, 1),
                    'gender' => mt_rand(0, 2)
                ],
                'meta_description' => $this->faker->paragraph(3),
                'type' => 'area'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgDetail_Null()
    {
        $pageType = LandingMetaOg::DETAIL_KOST;
        $fakerImageUrl = $this->faker->imageUrl(640, 480);

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn(null);

        $res = $this->setLandingMetaOg([
                'detail' => [
                    'area_city' => $this->faker->city,
                    'room_title' => 'Detail Kost Testing Room Title',
                    'photo_url' => [
                        'medium' => $fakerImageUrl
                    ],
                    'top_facilities' => [
                        [
                            "id" => 1,
                            "name" => "K. Mandi Dalam",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/JKp9z6Xm.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/TCWpQeCV.png"
                        ],
                        [
                            "id" => 3,
                            "name" => "Shower",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/PybSBNxB.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/fY7Fjerx.png"
                        ],
                        [
                            "id" => 10,
                            "name" => "Kasur",
                            "photo_url" => "https://static8.kerupux.com/uploads/tags/KrCtQKLC.png",
                            "small_photo_url" => "https://static8.kerupux.com/uploads/tags/1BsAad8X.png"
                        ]
                    ],
                    'price_monthly' => mt_rand(1, 99999),
                    'is_booking' => mt_rand(0, 1),
                    'gender' => mt_rand(0, 2)
                ],
                'meta_description' => $this->faker->paragraph(3),
                'type' => 'area'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame('Detail Kost Testing Room Title', $res['title']);
        $this->assertSame($fakerImageUrl, $res['image_url']);
    }

    public function testSetTopFacilities_Empty()
    {
        $res = $this->setTopFacilities([]);
        $this->assertSame($res, '');
    }

    public function testSetRentType_Monthly()
    {
        $rowLanding['detail']['price_monthly'] = 250000;
        $res = $this->setRentType($rowLanding);
        $this->assertSame($res, Periodicity::BULANAN);
    }

    public function testSetRentType_Yearly()
    {
        $rowLanding['detail']['price_yearly'] = 250000;
        $res = $this->setRentType($rowLanding);
        $this->assertSame($res, Periodicity::TAHUNAN);
    }

    public function testSetRentType_Weekly()
    {
        $rowLanding['detail']['price_weekly'] = 250000;
        $res = $this->setRentType($rowLanding);
        $this->assertSame($res, Periodicity::MINGGUAN);
    }

    public function testSetRentType_Daily()
    {
        $rowLanding['detail']['price_daily'] = 250000;
        $res = $this->setRentType($rowLanding);
        $this->assertSame($res, Periodicity::HARIAN);
    }

    public function testSetPropertyType_Success()
    {
        $rowLanding['detail']['is_booking'] = true;
        $res = $this->setPropertyType($rowLanding);
        $this->assertSame($res, self::$booking_langsung);
    }

    public function testSetPropertyType_Empty()
    {
        $rowLanding['detail']['is_booking'] = false;
        $res = $this->setPropertyType($rowLanding);
        $this->assertSame($res, '');
    }

    public function testSetGender_Success()
    {
        $rowLanding['detail']['gender'] = 1;
        $res = $this->setGender($rowLanding);
        $this->assertSame($res, Room::GENDER[1]);
    }

    public function testSetGender_Empty()
    {
        $rowLanding = [];
        $res = $this->setGender($rowLanding);
        $this->assertSame($res, '');
    }

    public function testSetPromotionDiscount_Success()
    {
        $promo = $this->faker->creditCardType;
        $rowLanding['detail']['promotion'] = (object) [
            'title' => $promo,
            'content' => $this->faker->sentence(5),
            'from' => date("Y-m-d"),
            'to' => date("Y-m-d")
        ];
        $res = $this->setPromotionDiscount($rowLanding);
        $this->assertSame($res, $promo);
    }

    public function testSetPromotionDiscount_Empty()
    {
        $rowLanding = [];
        $res = $this->setPromotionDiscount($rowLanding);
        $this->assertSame($res, '');
    }

    public function testSetLandingMetaOgLandingCampus_Success()
    {
        $pageType = LandingMetaOg::LANDING_AREA;
        $meta = factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_CAMPUS)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([
                'heading_1' => 'Landing Kost Testing Heading',
                'keyword' => 'Kost Testing',
                'price_min' => mt_rand(1, 99999),
                'breadcrumb' => [
                    [
                        "url" => $this->faker->url(),
                        "name" => "Home"
                    ],
                    [
                        "url" => $this->faker->url(),
                        "name" => "Kost Jogja"
                    ]
                ],
                'dummy_counter' => mt_rand(1, 99999),
                'rent_type' => mt_rand(0, 3),
                'type' => 'campus'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgLandingCampus_Null()
    {
        $pageType = LandingMetaOg::LANDING_CAMPUS;

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn(null);

        $res = $this->setLandingMetaOg([
                'heading_1' => 'Landing Kost Testing Heading',
                'keyword' => 'Kost Testing',
                'price_min' => mt_rand(1, 99999),
                'breadcrumb' => [
                    [
                        "url" => $this->faker->url(),
                        "name" => "Home"
                    ],
                    [
                        "url" => $this->faker->url(),
                        "name" => "Kost Jogja"
                    ]
                ],
                'dummy_counter' => mt_rand(1, 99999),
                'rent_type' => mt_rand(0, 3),
                'type' => 'campus'
            ],
            [
                "hari",
                "minggu",
                "bulan",
                "tahun"
            ],
            $pageType);

        $this->assertSame('Landing Kost Testing Heading', $res['title']);
        $this->assertSame(LandingMetaOg::LANDING_DEFAULT_IMAGE, $res['image_url']);
    }

    public function testSetLandingMetaOgCari_Success()
    {
        $pageType = LandingMetaOg::LANDING_CARI;
        $meta = factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_CARI)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgPromosiKost_Success()
    {
        $pageType = LandingMetaOg::LANDING_PROMOSI_KOST;
        $meta = factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_PROMOSI_KOST)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgPaketPremium_Success()
    {
        $pageType = LandingMetaOg::LANDING_PAKET_PREMIUM;
        $meta = factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_PAKET_PREMIUM)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }

    public function testSetLandingMetaOgPromoKost_Success()
    {
        $pageType = LandingMetaOg::LANDING_PROMO_KOST;
        $meta = factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_PROMO_KOST)->create();

        $this->mockPartialAlternatively(LandingMetaOgRepository::class)
            ->shouldReceive('getFirstActiveLastUpdated')
            ->with($pageType)
            ->andReturn($meta);

        $res = $this->setLandingMetaOg([], [], $pageType);

        $this->assertSame($meta->title, $res['title']);
        $this->assertSame($meta->description, $res['description']);
        $this->assertSame($meta->keywords, $res['keywords']);
        $this->assertSame($meta->image, $res['image_url']);
    }
}