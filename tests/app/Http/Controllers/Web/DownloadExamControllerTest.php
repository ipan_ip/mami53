<?php

namespace App\Http\Controllers\Web;

use App\Repositories\DownloadExam\DownloadExamRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class DownloadExamControllerTest extends MamiKosTestCase
{
    public function testDownload_NotLogin()
    {
        $idFake = mt_rand(0, 99);
        $url = 'download-soal/download/'.$idFake;
        $req = $this->createFakeRequest(
            $url,
            'GET',
            []
        );

        $res = (new DownloadExamController)->download($req, $idFake);
        $this->assertSame($res->getSession()->all()['flash-message'], DownloadExamController::ANDA_HARUS_LOGIN);
    }

    public function testDownload_InvalidLink()
    {
        $mockRepo = $this->mockAlternatively(DownloadExamRepository::class);
        $mockRepo->shouldReceive('getById')
            ->andReturn(null);

        $user  = factory(User::class)->create();
        $this->actingAs($user);

        $idFake = mt_rand(0, 99);
        $url = 'download-soal/download/'.$idFake;
        $req = $this->createFakeRequest(
            $url,
            'GET',
            []
        );

        $res = (new DownloadExamController)->download($req, $idFake);
        $this->assertSame($res->getSession()->all()['flash-message'], DownloadExamController::LINK_TIDAK_VALID);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}