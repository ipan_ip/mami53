<?php

namespace App\Http\Controllers\Web\Sanjunipero;

use App\Test\MamiKosTestCase;
use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View as CustomView;
use phpmock\MockBuilder;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class LandingControllerTest extends MamiKosTestCase
{
    use WithFaker;

    private $mock;

    protected function setUp() : void
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == DynamicLandingParent::GOLDPLUS_1_CONFIG) {
                        return 9;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_2_CONFIG) {
                        return 7;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_3_CONFIG) {
                        return 10;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_4_CONFIG) {
                        return 101;
                    }

                    if ($key == DynamicLandingParent::OYO_CONFIG) {
                        return 11;
                    }

                    if ($key == DynamicLandingParent::APARKOST_CONFIG) {
                        return 429;
                    }

                    return '';
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        $this->mock->disable();
    }

    public function testIndex()
    {
        $res = (new LandingController)->index();
        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testParent_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn($dynamicLandingParent);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->parent(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug
        );

        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testParent_Redirect()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn(null);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->parent(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug
        );

        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testChild_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn($dynamicLandingParent);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->child(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug,
            $dynamicLandingChild->slug
        );

        $this->assertInstanceOf(CustomView::class, $res);
    }

    public function testChild_RedirectParentNull()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn(null);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->child(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug,
            $dynamicLandingChild->area_name
        );

        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testChild_RedirectParentNullPartTwo()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn(null);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->child(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            'empty-slug',
            $dynamicLandingChild->area_name
        );

        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testChild_RedirectParentActive()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('inactive')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn($dynamicLandingParent);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->child(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug,
            $dynamicLandingChild->area_name
        );

        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testChild_RedirectParentInactive()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('inactive')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('inactive')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $mockDynamicLandingParent = $this->mockPartialAlternatively(DynamicLandingParentRepositoryEloquent::class);
        $mockDynamicLandingParent->shouldReceive('findWithChildByActiveSlug')->andReturn($dynamicLandingParent);

        $req = $this->createFakeRequest('', 'GET', []);

        $res = (new LandingController)->child(
            $req,
            app()->make(DynamicLandingParentRepositoryEloquent::class),
            $dynamicLandingParent->slug,
            $dynamicLandingChild->area_name
        );

        $this->assertSame(302, $res->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $res);
    }

    public function testSetAreaFromParent()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $res = $this->callNonPublicMethod(LandingController::class, 'setAreaFromParent', [$dynamicLandingParent]);
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testSetFilters_AllGoldplus()
    {
        $allGoldplusInString = implode(
            ',',
            [
                config(DynamicLandingParent::GOLDPLUS_1_CONFIG),
                config(DynamicLandingParent::GOLDPLUS_2_CONFIG),
                config(DynamicLandingParent::GOLDPLUS_3_CONFIG),
                config(DynamicLandingParent::GOLDPLUS_4_CONFIG)
            ]);

        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create([
            'type_kost' => DynamicLandingParent::ALL_GOLDPLUS
        ]);

        $res = $this->callNonPublicMethod(LandingController::class, 'setFilters', [$dynamicLandingParent]);

        $this->assertArrayHasKey('level_info', $res);
        $this->assertSame($res['level_info'], $allGoldplusInString);
    }

    public function testSetFilters_Goldplus1()
    {
        $allGoldplusInString = config(DynamicLandingParent::GOLDPLUS_1_CONFIG);

        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create([
            'type_kost' => DynamicLandingParent::GOLDPLUS_1
        ]);

        $res = $this->callNonPublicMethod(LandingController::class, 'setFilters', [$dynamicLandingParent]);

        $this->assertArrayHasKey('level_info', $res);
        $this->assertEquals($res['level_info'], $allGoldplusInString);
    }

    public function testSetFilters_EmptyString()
    {
        $this->mock->disable();
        
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == DynamicLandingParent::GOLDPLUS_1_CONFIG) {
                        return null;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_2_CONFIG) {
                        return null;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_3_CONFIG) {
                        return null;
                    }

                    if ($key == DynamicLandingParent::GOLDPLUS_4_CONFIG) {
                        return null;
                    }

                    return '';
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();

        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create([
            'type_kost' => DynamicLandingParent::GOLDPLUS_1
        ]);

        $res = $this->callNonPublicMethod(LandingController::class, 'setFilters', [$dynamicLandingParent]);

        $this->assertArrayHasKey('level_info', $res);
        $this->assertEquals($res['level_info'], '');
    }

    public function testSetAttributeBoolean_True()
    {
        $res = $this->callNonPublicMethod(
            LandingController::class,
            'setAttributeBoolean',
            [
                implode( ',', [DynamicLandingParent::MAMIROOMS, DynamicLandingParent::ALL_GOLDPLUS] ),
                DynamicLandingParent::MAMIROOMS
            ]
        );

        $this->assertTrue($res);
    }

    public function testSetAttributeBoolean_False()
    {
        $res = $this->callNonPublicMethod(
            LandingController::class,
            'setAttributeBoolean',
            [
                implode( ',', [DynamicLandingParent::MAMIROOMS, DynamicLandingParent::ALL_GOLDPLUS] ),
                DynamicLandingParent::MAMI_CHECKER
            ]
        );

        $this->assertFalse($res);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}