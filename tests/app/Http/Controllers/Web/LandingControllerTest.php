<?php

namespace App\Http\Controllers\Web;

use App\Entities\Landing\LandingMetaOg;
use App\Test\MamiKosTestCase;
use App\Repositories\Landing\LandingMetaOgRepository;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\View\View as CustomView;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class LandingControllerTest extends MamiKosTestCase
{
    private $controller;

    protected function setUp() : void
    {
        parent::setUp();

        $this->controller = app()->make(LandingController::class);
        $this->faker = Faker::create();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testPromoKost()
    {
        $req = $this->createFakeRequest(
            '',
            'GET',
            []
        );
        $res = $this->controller->promoKost($req);
        $this->assertInstanceOf(CustomView::class, $res);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}