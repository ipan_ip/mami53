<?php

namespace App\Http\Controllers\Web\Singgahsini;

use App\Entities\Feeds\ImageFacebook;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Repositories\Singgahsini\RoomRepository as SinggahsiniRoom;
use App\Repositories\TagRepository;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\View\View;

class LandingControllerTest extends MamiKosTestCase
{
    private $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testIndex()
    {
        $rooms = factory(Room::class, 2)
            ->create()
            ->each(function($item) {
                factory(Card::class, 3)->create([
                    'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover',
                    'designer_id' => $item->id,
                    'photo_id' => factory(Media::class)->create()->id
                ]);
            });

        $this->mockPartialAlternatively(SinggahsiniRoom::class)
            ->shouldReceive('getRooms')
            ->andReturn($rooms);

        $res = (new LandingController)->index();

        $this->assertInstanceOf(View::class, $res);
    }

    public function testRegister()
    {
        $tags = factory(Tag::class, 6)->create();
        $this->mockPartialAlternatively(TagRepository::class)
            ->shouldReceive('getFacilitySinggahsiniRegistration')
            ->once()
            ->andReturn($tags);

        $res = (new LandingController)->register();

        $this->assertInstanceOf(View::class, $res);
    }
}