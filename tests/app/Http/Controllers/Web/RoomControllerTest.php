<?php

namespace test\app\Http\Controllers\Web;

use App\Entities\Landing\LandingMetaOg;
use App\Test\MamiKosTestCase;
use App\Http\Controllers\Web\RoomController;
use App\Repositories\RoomRepositoryEloquent;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RoomControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = new RoomRepositoryEloquent(app());
    }

    public function testSetMetaDescription_TitleGenderCampur()
    {
        $kostName = 'lala';
        $result = (new RoomController($this->repository))->setMetaDescription($kostName, 0);
        $this->assertEquals(
            $result,
            'Tersedia '.$kostName.' murah. Sewa kamar kost campur dengan fasilitas lengkap. Temukan promonya & pesan sekarang hanya di mamikos.com!'
        );
    }

    public function testSetMetaDescription_TitleGenderPutra()
    {
        $kostName = 'lulu';
        $result = (new RoomController($this->repository))->setMetaDescription($kostName, 1);
        $this->assertEquals(
            $result,
            'Tersedia '.$kostName.' murah. Sewa kamar kost putra dengan fasilitas lengkap. Temukan promonya & pesan sekarang hanya di mamikos.com!'
        );
    }

    public function testSetMetaDescription_TitleGenderPutri()
    {
        $kostName = 'lili';
        $result = (new RoomController($this->repository))->setMetaDescription($kostName, 2);
        $this->assertEquals(
            $result,
            'Tersedia '.$kostName.' murah. Sewa kamar kost putri dengan fasilitas lengkap. Temukan promonya & pesan sekarang hanya di mamikos.com!'
        );
    }

    public function testSetMetaDescription_NoTitleNoGender()
    {
        $result = (new RoomController($this->repository))->setMetaDescription('', '');
        $this->assertEquals(
            $result,
            LandingMetaOg::DETAIL_DEFAULT_DESCRIPTION
        );
    }

    public function testSetMetaDescription_NullTitleNullGender()
    {
        $result = (new RoomController($this->repository))->setMetaDescription(null, null);
        $this->assertEquals(
            $result,
            LandingMetaOg::DETAIL_DEFAULT_DESCRIPTION
        );
    }
}