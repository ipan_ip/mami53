<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ContractControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_CONTRACT_URL = 'oauth/cms/contract/';

    public function testGetContractByIdNotFoundWhenParamInvalid()
    {
        $response = $this->json('GET', self::GET_CONTRACT_URL.'1222');
        $jsonResult = $response->getData();

        $response->assertStatus(404);
        $this->assertEquals(ResponseType::REQUIRED_PARAM_MISSING, $jsonResult->result);
        $this->assertNull($jsonResult->contract);
    }

    public function testGetContractByIdSuccess()
    {
        // Given setup mamipayContract
        $owner  = factory(User::class)->create();
        $tenantUser  = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create(['user_id' => $tenantUser->id]);
        $kost   = factory(Room::class)->create();
        
        factory(MamipayOwner::class)->create(['user_id' => $owner->id]);
        $mamipayContract    = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'owner_id'      => $owner->id,
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id'   => $mamipayContract->id,
            'designer_id'   => $kost->id,
        ]);

        // When controller called
        $response = $this->json('GET', self::GET_CONTRACT_URL . $mamipayContract->id);

        // Then assertion
        $response->assertStatus(200);
        $response->assertJson([
            'result'    => ResponseType::SUCCESS,
            'contract'  => [
                'contract_id'   => $mamipayContract->id,
                'tenant'        => [
                    'id'            => $tenant->user_id,
                    'email'         => $tenant->email,
                    'name'          => $tenant->name,
                    'phone_number'  => $tenant->phone_number,
                ],
                'owner'         => [
                    'id'            => $owner->id,
                    'email'         => $owner->email,
                    'name'          => $owner->name,
                    'phone_number'  => $owner->phone_number,
                ],
                'kost'          => [
                    'kost_id'       => $kost->song_id,
                    'kost_name'     => $kost->name,
                ],
                'start_date'    => $mamipayContract->start_date,
                'end_date'      => $mamipayContract->end_date,
                'status'        => $mamipayContract->status,
            ]
        ]);
    }

}
