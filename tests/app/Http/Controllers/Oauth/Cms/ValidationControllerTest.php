<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\ComplaintSystem\ResponseType;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Test\MamiKosTestCase;

class ValidationControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_VALIDATION_URL = 'oauth/cms/validation/';

    public function testGetValidationUnauthorized()
    {
        $response = $this->json('GET', self::GET_VALIDATION_URL);

        $response->assertStatus(401);
    }

    public function testGetValidationMissingParam()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id' => 1,
            'contract_id' => 1
        ]);

        $response->assertStatus(422);
    }

    public function testGetValidationInvalidInput()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id' => 1,
            'contract_id' => 1,
            'user_id' => 1
        ]);

        $response->assertStatus(404);
        $response->assertJson([
            'result' => ResponseType::INVALID_INPUT
        ]);
    }

    public function testGetValidationRoomNotMatch()
    {
        $owner          = factory(User::class)->create();
        $tenant         = factory(MamipayTenant::class)->create();
        $room           = factory(Room::class)->create();
        $unmatchedRoom  = factory(Room::class)->create();
        $this->actingAs($owner);
        
        factory(MamipayOwner::class)->create(['user_id' => $owner->id]);
        $mamipayContract = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'owner_id'      => $owner->id,
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id'   => $mamipayContract->id,
            'designer_id'   => $room->id,
        ]);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id'       => $unmatchedRoom->song_id,
            'contract_id'   => $mamipayContract->id,
            'user_id'       => $owner->id
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'result'        => ResponseType::INVALID_INPUT,
            'message'       => "Contract and Kos Inputed didn't Match"
        ]);
    }

    public function testGetValidationUserNotRelated()
    {
        $owner          = factory(User::class)->create();
        $unrelatedUser  = factory(User::class)->create();
        $tenant         = factory(MamipayTenant::class)->create();
        $room           = factory(Room::class)->create();
        $this->actingAs($owner);
        
        factory(MamipayOwner::class)->create(['user_id' => $owner->id]);
        $mamipayContract = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'owner_id'      => $owner->id,
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id'   => $mamipayContract->id,
            'designer_id'   => $room->id,
        ]);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id'       => $room->song_id,
            'contract_id'   => $mamipayContract->id,
            'user_id'       => $unrelatedUser->id
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'result'        => ResponseType::INVALID_INPUT,
            'message'       => 'Contract not related to user'
        ]);
    }

    public function testGetValidationAsOwnerSuccess()
    {
        $owner  = factory(User::class)->create();
        $tenant = factory(MamipayTenant::class)->create();
        $room   = factory(Room::class)->create();
        $this->actingAs($owner);
        
        factory(MamipayOwner::class)->create(['user_id' => $owner->id]);
        $mamipayContract = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'owner_id'      => $owner->id,
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id'   => $mamipayContract->id,
            'designer_id'   => $room->id,
        ]);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id'       => $room->song_id,
            'contract_id'   => $mamipayContract->id,
            'user_id'       => $owner->id
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'result'    => ResponseType::SUCCESS,
            'contract'  => [
                'relation'  => [
                    'code'      => 2,
                    'message'   => 'User is Owner on This Contract'
                ]
            ],
            'user'      => [
                'id'        => $owner->id
            ],
            'kost'      => [
                'kost_id'   => $room->song_id
            ]
        ]);
    }

    public function testGetValidationAsTenantSuccess()
    {
        $owner      = factory(User::class)->create();
        $tenantUser = factory(User::class)->create();
        $tenant     = factory(MamipayTenant::class)->create(['user_id' => $tenantUser->id]);
        $room       = factory(Room::class)->create();
        $this->actingAs($tenantUser);
        
        factory(MamipayOwner::class)->create(['user_id' => $owner->id]);
        $mamipayContract = factory(MamipayContract::class)->create([
            'tenant_id'     => $tenant->id,
            'owner_id'      => $owner->id,
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id'   => $mamipayContract->id,
            'designer_id'   => $room->id,
        ]);

        $response = $this->json('GET', self::GET_VALIDATION_URL, [
            'kost_id'       => $room->song_id,
            'contract_id'   => $mamipayContract->id,
            'user_id'       => $tenantUser->id
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'result'    => ResponseType::SUCCESS,
            'contract'  => [
                'relation'  => [
                    'code'      => 1,
                    'message'   => 'User is Tenant on This Contract'
                ]
            ],
            'user'      => [
                'id'        => $tenantUser->id
            ],
            'kost'      => [
                'kost_id'   => $room->song_id
            ]
        ]);
    }
}
