<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayOwner;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PermissionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_PERMISSION_URL = 'oauth/cms/permission/';

    public function testGetPermissionUnauthorize()
    {
        $response = $this->json('GET', self::GET_PERMISSION_URL);

        $response->assertStatus(401);
    }

    public function testGetPermissionMissingParam()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $resources = [
            [
                'resource_type' => 1,
                'resource_id' => 1,
            ]
        ];

        $response = $this->json('GET', self::GET_PERMISSION_URL, ['resources' => json_encode($resources)]);

        $response->assertStatus(422);
    }

    public function testGetPermissionAsAdminGranted()
    {
        $user = factory(User::class)->create([
            'role' => 'administrator'
        ]);
        $this->actingAs($user);

        $room = factory(Room::class)->create();

        $mamipayContract = factory(MamipayContract::class)->create();

        $resources = [
            [
                'resource_type' => 1,
                'resource_id' => $room->song_id,
                'role' => 1
            ],
            [
                'resource_type' => 2,
                'resource_id' => $mamipayContract->id,
                'role' => 1
            ]
        ];

        $response = $this->json('GET', self::GET_PERMISSION_URL, ['resources' => json_encode($resources)]);

        $response->assertStatus(200);
        $response->assertJson([
            'permissions' => [
                [
                    'resource_type' => 1,
                    'resource_id' => $room->song_id,
                    'role' => 1,
                    'grant' => 1
                ],
                [
                    'resource_type' => 2,
                    'resource_id' => $mamipayContract->id,
                    'role' => 1,
                    'grant' => 1
                ]
            ]
        ]);
    }

    public function testGetPermissionAsOwnerGranted()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);
        $this->actingAs($user);

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([ 
            "designer_id" => $room->id, 
            "user_id" => $user->id,
        ]);

        $unownedRoom = factory(Room::class)->create();

        factory(MamipayOwner::class)->create(['user_id' => $user->id]);
        $mamipayContract = factory(MamipayContract::class)->create([
            'owner_id' => $user->id,
        ]);

        $unownedMamipayContract = factory(MamipayContract::class)->create();

        $resources = [
            [
                'resource_type' => 1,
                'resource_id' => $room->song_id,
                'role' => 1
            ],
            [
                'resource_type' => 1,
                'resource_id' => $unownedRoom->song_id,
                'role' => 1
            ],
            [
                'resource_type' => 2,
                'resource_id' => $mamipayContract->id,
                'role' => 1
            ],
            [
                'resource_type' => 2,
                'resource_id' => $unownedMamipayContract->id,
                'role' => 1
            ]
        ];

        $response = $this->json('GET', self::GET_PERMISSION_URL, ['resources' => json_encode($resources)]);

        $response->assertStatus(200);
        $response->assertJson([
            'permissions' => [
                [
                    'resource_type' => 1,
                    'resource_id' => $room->song_id,
                    'role' => 1,
                    'grant' => 1
                ],
                [
                    'resource_type' => 1,
                    'resource_id' => $unownedRoom->song_id,
                    'role' => 1,
                    'grant' => 0
                ],
                [
                    'resource_type' => 2,
                    'resource_id' => $mamipayContract->id,
                    'role' => 1,
                    'grant' => 1
                ],
                [
                    'resource_type' => 2,
                    'resource_id' => $unownedMamipayContract->id,
                    'role' => 1,
                    'grant' => 0
                ]
            ]
        ]);
    }

    public function testGetPermissionAsTenantGranted()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = factory(Room::class)->create();

        $mamipayContract = factory(MamipayContract::class)->create();

        $resources = [
            [
                'resource_type' => 1,
                'resource_id' => $room->song_id,
                'role' => 2
            ],
            [
                'resource_type' => 2,
                'resource_id' => $mamipayContract->id,
                'role' => 2
            ]
        ];

        $response = $this->json('GET', self::GET_PERMISSION_URL, ['resources' => json_encode($resources)]);

        $response->assertStatus(200);
        $response->assertJson([
            'permissions' => [
                [
                    'resource_type' => 1,
                    'resource_id' => $room->song_id,
                    'role' => 2,
                    'grant' => 1
                ],
                [
                    'resource_type' => 2,
                    'resource_id' => $mamipayContract->id,
                    'role' => 2,
                    'grant' => 1
                ],
            ]
        ]);
    }
}
