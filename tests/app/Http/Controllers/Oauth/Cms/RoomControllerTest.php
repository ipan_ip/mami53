<?php

namespace test\app\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;

class RoomControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_ROOM_URL = 'oauth/cms/rooms/';
    private $mockForAppDevice;

    public function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockAlternatively('App\Repositories\RoomRepositoryEloquent');

        // mock app()->user; return to null coz we will use mock Auth with $this->actingAs($user);
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    /** @test */
    public function get_kost_by_song_id_success_when_param_valid()
    {
        $mockKost = factory(\App\Entities\Room\Room::class)->make(["song_id" => 12]);
        
        // mock repository
        $this->mock->shouldReceive('getRoomBySongId')->once()->andReturn($mockKost);

        $response = $this->json('GET', self::GET_ROOM_URL.'12');
        $jsonResult = $response->getData();

        $response->assertStatus(200);
        $this->assertEquals(ResponseType::SUCCESS, $jsonResult->result);
        $this->assertEquals($mockKost->song_id, $jsonResult->kost->kost_id);
        $this->assertEquals($mockKost->name, $jsonResult->kost->kost_name);
        $this->assertEquals($mockKost->area, $jsonResult->kost->area_name);
        $this->assertEquals($mockKost->status, $jsonResult->kost->status);
        $this->assertEquals($mockKost->area_city, $jsonResult->kost->area_city);
        $this->assertEquals($mockKost->area_subdistrict, $jsonResult->kost->area_subdistrict);
        $this->assertEquals($mockKost->room_available, $jsonResult->kost->room->available);
        $this->assertEquals($mockKost->room_count, $jsonResult->kost->room->total);
        $this->assertEquals(($mockKost->room_count - $mockKost->room_available), $jsonResult->kost->room->occupied);
    }

    /** @test */
    public function get_kost_by_song_id_not_found_when_param_invalid()
    {        
        // mock repository
        $this->mock->shouldReceive('getRoomBySongId')->once()->andReturn(null);

        $response = $this->json('GET', self::GET_ROOM_URL.'2');
        $jsonResult = $response->getData();

        $response->assertStatus(404);
        $this->assertEquals(ResponseType::REQUIRED_PARAM_MISSING, $jsonResult->result);
        $this->assertEquals(null, $jsonResult->kost);
    }

    public function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

}