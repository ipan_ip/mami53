<?php

namespace test\app\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Entities\ComplaintSystem\RoleType;
use App\Repositories\UserDataRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;

class UserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_USER_URL = 'oauth/cms/user/';
    private $mockForAppDevice;

    public function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively(UserDataRepositoryEloquent::class);
    }

    public function testGetUserByIdSuccessWhenParamValid()
    {
        $mockUser = factory(\App\User::class)->state('owner')->make(['id' => 25, 'is_verify' => 1]);

        $this->mock->shouldReceive('getUserById')->once()->andReturn($mockUser);

        $response = $this->json('GET', self::GET_USER_URL . '25');
        $jsonResult = $response->getData();

        $response->assertStatus(200);
        $this->assertEquals(ResponseType::SUCCESS, $jsonResult->result);
        $this->assertEquals($mockUser->id, $jsonResult->user->id);
        $this->assertEquals($mockUser->email, $jsonResult->user->email);
        $this->assertEquals($mockUser->phone_number, $jsonResult->user->phone_number);
        $this->assertEquals($mockUser->name, $jsonResult->user->name);
        $this->assertEquals($mockUser->isVerified(), $jsonResult->user->status);

        $this->assertEquals(RoleType::TYPE_OWNER, $jsonResult->user->role);
    }

    public function testGetUserByIdNotFoundWhenParamInvalid()
    {
        $this->mock->shouldReceive('getUserById')->once()->andReturn(null);

        $response = $this->json('GET', self::GET_USER_URL . '22');
        $jsonResult = $response->getData();

        $response->assertStatus(404);
        $this->assertEquals(ResponseType::INVALID_INPUT, $jsonResult->result);
        $this->assertEquals(null, $jsonResult->user);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }
}
