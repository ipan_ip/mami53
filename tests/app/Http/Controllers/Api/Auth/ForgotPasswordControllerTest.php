<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entities\Activity\ActivationCodeType;
use App\Enums\Activity\ActivationCodeVia;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Activity\ActivationCode;
use App\User;

class ForgotPasswordControllerTest extends MamiKosTestCase
{
    use WithFaker;

    private function callForgotPassword($for, $via, $destination, $code, $password)
    {
        return $this->post(
            '/api/v1/auth/user/forgot-password',
            [
                'for' => $for,
                'via' => $via,
                'destination' => $destination,
                'code' => $code,
                'password' => $password,
                'password_confirmation' => $password
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Controllers\Api\Auth\ForgotPasswordController
     */
    public function testForgotPasswordWithCorrectCodeReturnTrue()
    {
        $user = factory(User::class)->create();
        $newPassword = $this->faker->password;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::WHATSAPP(),
            'destination' => $user->phone_number
        ]);

        $response = $this->callForgotPassword(
            $activationCode->for,
            $activationCode->via->value,
            $user->phone_number,
            $activationCode->code,
            $newPassword
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Controllers\Api\Auth\ForgotPasswordController
     */
    public function testForgotPasswordWithIncorrectCodeReturnFalse()
    {
        $user = factory(User::class)->create();
        $newPassword = $this->faker->password;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);

        $response = $this->callForgotPassword(
            $activationCode->for,
            $activationCode->via->value,
            $user->phone_number,
            '1234',
            $newPassword
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.tenant.verification.code.invalid')
        ]);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Controllers\Api\Auth\ForgotPasswordController
     */
    public function testForgotPasswordWithNullPasswordReturnFalse()
    {
        $user = factory(User::class)->create();
        $newPassword = null;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);

        $response = $this->callForgotPassword(
            $activationCode->for,
            $activationCode->via->value,
            $user->phone_number,
            $activationCode->code,
            $newPassword
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.password.required')
        ]);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Controllers\Api\Auth\ForgotPasswordController
     */
    public function testForgotPasswordWithInvalidPasswordTypeReturnFalse()
    {
        $user = factory(User::class)->create();
        $newPassword = 123423123;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);

        $response = $this->callForgotPassword(
            $activationCode->for,
            $activationCode->via->value,
            $user->phone_number,
            $activationCode->code,
            $newPassword
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.password.string')
        ]);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Controllers\Api\Auth\ForgotPasswordController
     */
    public function testForgotPasswordWithoutPasswordConfirmationReturnFalse()
    {
        $user = factory(User::class)->create();
        $newPassword = $this->faker->password;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);

        $response = $this->post(
            '/api/v1/auth/user/forgot-password',
            [
                'for' => $activationCode->for,
                'via' => $activationCode->via->value,
                'destination' => $user->phone_number,
                'code' => $activationCode->code,
                'password' => $newPassword,
                'password_confirmation' => null
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.password.confirmed')
        ]);
    }
}
