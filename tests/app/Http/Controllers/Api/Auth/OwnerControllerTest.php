<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Activity\Enums\VerificationMethod;
use App\Entities\Device\UserDevice;
use App\Entities\User\UserSocial;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;

class OwnerControllerTest extends MamiKosTestCase
{
    use WithFaker;

    private const POST_OWNER_EDIT_PHONE = 'api/v1/owner/edit/phone';
    private const POST_OWNER_EDIT_PHONE_REQUEST = 'api/v1/owner/edit/phone/request';
    private const POST_OWNER_FORGET_PASSWORD = 'api/v1/owner/forget/password';
    private const POST_OWNER_FORGET_PASSWORD_REQUEST = 'api/v1/owner/forget/password/request';
    private const POST_OWNER_FORGET_PASSWORD_CHECK = 'api/v1/owner/forget/password/check';
    private const POST_OWNER_FORGET_PASSWORD_IDENTIFIER = 'api/v1/owner/forget/password/identifier';

    public function testOwnerEditPhoneAssertTrue()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    public function testOwnerEditPhoneAssertDatabaseChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $this->assertDatabaseHas('user', [
            'phone_number' => $newPhone,
            'email' => $owner->email,
            'name' => $owner->name,
        ]);
    }

    public function testOwnerEditPhoneWithNonUniquePhoneAssertFalse()
    {
        $otherOwner = factory(User::class)->states('owner')->create();
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $otherOwner->phone_number,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $otherOwner->phone_number,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false
        ]);
    }

    public function testOwnerEditPhoneNonUniqueAssertDatabaseNotChanged()
    {
        $otherOwner = factory(User::class)->states('owner')->create();
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $otherOwner->phone_number,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $otherOwner->phone_number,
                'verification_code' => $code->code
            ]
        );

        $this->assertDatabaseMissing('user', [
            'phone_number' => $otherOwner->phone_number,
            'email' => $owner->email,
            'name' => $owner->name,
        ]);
    }

    public function testOwnerEditPhoneWithoutPhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.phone_number.required')]
            ]
        ]);
    }

    public function testOwnerEditPhoneWithoutVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.code.required')]
            ]
        ]);
    }

    public function testOwnerEditPhoneWithoutVerificationCodeAssertDatabaseNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
            ]
        );

        $this->assertDatabaseMissing('user', [
            'phone_number' => $newPhone,
            'email' => $owner->email,
            'name' => $owner->name,
        ]);
    }

    public function testOwnerEditPhoneWithExpiredCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'expired_at' => Carbon::now()->subMinute(1),
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.owner.verification.code.invalid')]
            ]
        ]);
    }

    public function testOwnerEditPhoneWithExpiredCodeAssertDatabaseNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'expired_at' => Carbon::now()->subMinute(1),
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = $code->phone_number;

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $this->assertDatabaseMissing('user', [
            'phone_number' => $newPhone,
            'email' => $owner->email,
            'name' => $owner->name,
        ]);
    }

    public function testOwnerEditPhoneWithWrongNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = '0812345678';

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.owner.verification.code.invalid')]
            ]
        ]);
    }

    public function testOwnerEditPhoneWithWrongNumberAssertDatabaseNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $newPhone = '0812345678';

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE),
            [
                'phone_number' => $newPhone,
                'verification_code' => $code->code
            ]
        );

        $this->assertDatabaseMissing('user', [
            'phone_number' => $newPhone,
            'email' => $owner->email,
            'name' => $owner->name,
        ]);
    }

    public function testOwnerEditPhoneRequestAssertTrue()
    {
        $owner = factory(User::class)->states('owner')->create();
        $newPhone = '0812345678';

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE_REQUEST),
            [
                'phone_number' => $newPhone,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    public function testOwnerEditPhoneRequestAssertDatabaseInserted()
    {
        $owner = factory(User::class)->states('owner')->create();
        $newPhone = '0812345678';

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE_REQUEST),
            [
                'phone_number' => $newPhone,
            ]
        );

        $this->assertDatabaseHas('user_verification_code', [
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER,
            'deleted_at' => null,
        ]);
    }

    public function testOwnerEditPhoneRequestTooFrequentAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $newPhone = '0812345678';
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE_REQUEST),
            [
                'phone_number' => $newPhone,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00'])
            ]
        ]);
    }

    public function testOwnerEditPhoneRequestTooFrequentAssertDatabaseNotSoftDeleted()
    {
        $owner = factory(User::class)->states('owner')->create();
        $newPhone = '0812345678';
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE_REQUEST),
            [
                'phone_number' => $newPhone,
            ]
        );

        $this->assertDatabaseHas('user_verification_code', [
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER,
            'code' => $code->code,
            'deleted_at' => null,
        ]);
    }

    public function testOwnerEditPhoneRequestAssertOldVerificationCodeSoftDeleted()
    {
        $owner = factory(User::class)->states('owner')->create();
        $newPhone = '0812345678';
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER,
            'created_at' => Carbon::now()->subMinute(5),
            'updated_at' => Carbon::now()->subMinute(5)
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_EDIT_PHONE_REQUEST),
            [
                'phone_number' => $newPhone,
            ]
        );

        $this->assertSoftDeleted('user_verification_code', [
            'phone_number' => $newPhone,
            'for' => ActivationCodeType::OWNER_EDIT_PHONE_NUMBER,
            'code' => $code->code,
        ]);
    }

    public function testOwnerForgetPasswordAssertTrue()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code,
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    public function testOwnerForgetPasswordAssertDatabaseHasChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code,
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $this->assertTrue(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordWithPasswordNotConfirmedAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code,
                'password' => 'password',
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false
        ]);
    }

    public function testOwnerForgetPasswordWithPasswordNotConfirmedAssertDatabaseNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code,
                'password' => 'password',
            ]
        );

        $this->assertFalse(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordWithoutPhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'verification_code' => $code->code,
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [
                    __('api.input.phone_number.required')
                ]
            ]
        ]);
    }

    public function testOwnerForgetPasswordWithoutVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [
                    __('api.input.code.required')
                ]
            ]
        ]);
    }

    public function testOwnerForgetPasswordWithoutVerificationCodeAssertDatabaseHasNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $this->assertFalse(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordWithInvalidVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => 'YYYY'
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => 'XXXX'
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [
                    __('api.owner.verification.code.invalid')
                ]
            ]
        ]);
    }

    public function testOwnerForgetPasswordWithInvalidVerificationCodeAssertDatabaseHasNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => 'YYYY'
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => 'XXXX'
            ]
        );

        $this->assertFalse(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordWithExpiredVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'expired_at' => Carbon::now()->subMinute(1)
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [
                    __('api.owner.verification.code.invalid')
                ]
            ]
        ]);
    }

    public function testOwnerForgetPasswordWithExpiredVerificationCodeAssertDatabaseHasNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'expired_at' => Carbon::now()->subMinute(1)
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => $code->code
            ]
        );

        $this->assertFalse(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordWithNonUniquePhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $otherOwner = factory(User::class)->states('owner')->create([
            'phone_number' => $owner->phone_number
        ]);

        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.owner.duplicate'),
            ],
            'title' => __('api.input.forget_password.failed'),
            'is_forbidden' => true
        ]);
    }

    public function testOwnerForgetPasswordWithNonUniquePhoneNumberAssertDatabaseHasNotChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $otherOwner = factory(User::class)->states('owner')->create([
            'phone_number' => $owner->phone_number
        ]);
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD),
            [
                'phone_number' => $owner->phone_number,
                'password' => 'password',
                'password_confirmation' => 'password',
                'verification_code' => $code->code
            ]
        );

        $this->assertFalse(Hash::check('password', $owner->fresh()->password)); // We need to load the new model data due to password change
        $this->assertFalse(Hash::check('password', $otherOwner->fresh()->password)); // We need to load the new model data due to password change
    }

    public function testOwnerForgetPasswordRequestAssertTrue()
    {
        $owner = factory(User::class)->states('owner')->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    public function testOwnerForgetPasswordRequestAssertDatabaseHasChanged()
    {
        $owner = factory(User::class)->states('owner')->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $this->assertDatabaseHas('user_verification_code', [
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'deleted_at' => null
        ]);
    }

    public function testOwnerForgetPasswordRequestAssertOldVerificationCodeSoftDeleted()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'created_at' => Carbon::now()->subMinute(2)
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $this->assertSoftDeleted('user_verification_code', [
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => $code->code
        ]);
    }

    public function testOwnerForgetPasswordRequestWithUnregisteredPhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '0811111111'
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => '0822222222',
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.not_found'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testOwnerForgetPasswordRequestWithInvalidPhoneNumberAssertDatabaseHasChanged()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '0811111111'
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => '0822222222',
            ]
        );

        $this->assertDatabaseMissing('user_verification_code', [
            'phone_number' => '0822222222',
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'deleted_at' => null
        ]);
    }

    public function testOwnerForgetPasswordRequestTooFrequentAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
            ]
        ]);
    }

    public function testOwnerForgetPasswordRequestTooFrequentAssertDatabaseNotSoftDeleted()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $this->assertDatabaseHas('user_verification_code', [
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => $code->code,
            'deleted_at' => null
        ]);
    }

    public function testOwnerForgetPasswordCheckAssertTrue()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_CHECK),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    public function testOwnerForgetPasswordCheckWithoutPhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_CHECK),
            [
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.phone_number.required')]
            ]
        ]);
    }

    public function testOwnerForgetPasswordCheckWithoutVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_CHECK),
            [
                'phone_number' => $owner->phone_number,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.code.required')]
            ]
        ]);
    }

    public function testOwnerForgetPasswordCheckWithInvalidVerificationCodeAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => 'XXXX'
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_CHECK),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => 'YYYY'
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.owner.verification.code.invalid')
            ],
            'is_forbidden' => false
        ]);
    }

    public function testOwnerForgetPasswordCheckWithNonUniquePhoneNumberAssertFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $otherOwner = factory(User::class)->states('owner')->create([
            'phone_number' => $owner->phone_number
        ]);
        
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_CHECK),
            [
                'phone_number' => $owner->phone_number,
                'verification_code' => $code->code
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.owner.duplicate'),
            ],
            'title' => __('api.input.forget_password.failed'),
            'is_forbidden' => true
        ]);
    }

    public function testTenantForgetPasswordRequestInOwnerPage()
    {
        $tenant = factory(User::class)->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $tenant->id
        ]);
        $tenant->last_login = $device->id;
        $tenant->save();

        $response = $this->actingAs($tenant)->json('POST', 
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST), [
            'phone_number' => $tenant->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.wrong_section'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testOwnerForgetPasswordRequestDuplicateOwnerTenant()
    {
        $tenant = factory(User::class)->create();
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => $tenant->phone_number
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $tenant->id
        ]);
        $tenant->last_login = $device->id;
        $tenant->save();

        $response = $this->actingAs($tenant)->json('POST', 
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST), [
            'phone_number' => $tenant->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.tenant.duplicate'),
            ],
            'title' => __('api.input.forget_password.failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testTenantForgetPasswordRequestInOwnerPageWhenRegisterUsingSocialite()
    {
        $tenant = factory(User::class)->create();
        $social = factory(UserSocial::class)->create([
            'email' => $tenant->email,
            'name' => $tenant->name,
            'user_id' => $tenant->id
        ]);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $tenant->id
        ]);
        $tenant->last_login = $device->id;
        $tenant->save();

        $response = $this->actingAs($tenant)->json('POST', 
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_REQUEST), [
            'phone_number' => $tenant->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.social'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithPhoneNumberShouldReturnTrue()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '0812345689',
        ]);
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => $owner->phone_number]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierShouldReturnCorrectResponse()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '0812345689',
        ]);
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => $owner->phone_number]
        );
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'key' => VerificationMethod::SMS()->value['key'],
                    'label' => VerificationMethod::SMS()->value['label'],
                    'value' => $owner->phone_number
                ],[
                    'key' => VerificationMethod::WA()->value['key'],
                    'label' => VerificationMethod::WA()->value['label'],
                    'value' => $owner->phone_number
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithDuplicatePhoneNumberShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '0812345689',
        ]);
        factory(User::class)->create(['phone_number' => '0812345689']);
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => $owner->phone_number]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.forget_password.duplicate.message'),
            'contact' => [
                'cta_message' => __('api.input.forget_password.duplicate.cta_message'),
                'wa_number' => __('api.input.forget_password.duplicate.wa_number'),
                'pretext' => __('api.input.forget_password.duplicate.pretext', ['identifier' => $owner->phone_number]),
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithURegisteredPhoneNumberAsTenantShouldReturnFalse()
    {
        $tenant = factory(User::class)->create();
        $this->createUserDevice($tenant);

        $response = $this->actingAs($tenant)->json(
            'POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            [
                'phone_number' => $tenant->phone_number,
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.owner_not_found')
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithMinPhoneNumberShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            [
                'phone_number' => '082124',
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.min', ['min' => 8]),
            'contact' => null
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithMaxPhoneNumberShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            [
                'phone_number' => '082124012312312',
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.max', ['max' => 14])
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithInvalidPhoneNumberFormatShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => '6284012312312']
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.regex')
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithInvalidPhoneNumberTypeShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => 84012312312]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.invalid')
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\OwnerController
     */
    public function testOwnerForgetPasswordIdentifierWithUnregisteredPhoneNumberShouldReturnFalse()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->createUserDevice($owner);

        $response = $this->actingAs($owner)->json('POST',
            $this->withAccessToken(self::POST_OWNER_FORGET_PASSWORD_IDENTIFIER),
            ['phone_number' => '082341341234']
        );

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.exists')
        ]);
    }

    /**
     * Create UserDevice data
     * 
     * @param User $user
     * @return void
     */
    private function createUserDevice(User $user): void
    {
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();
    }
}
