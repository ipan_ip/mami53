<?php

namespace Test\App\Http\Web;

use Illuminate\Support\Facades\Hash;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Device\UserDevice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\User\UserSocial;
use App\Test\MamiKosTestCase;
use App\User;
use phpmock\MockBuilder;
use Mockery;
use App\Entities\Activity\Enums\VerificationMethod;

/**
 *  Test route handled by TenantController
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *  @see App\Http\Controllers\Api\Auth\TenantController
 *
 */
class TenantControllerTest extends MamiKosTestCase
{
    
    private const POST_VERIFICATION_REQUEST_URL = 'api/v1/auth/tenant/verification/request';
    private const POST_VERIFICATION_CHECK_URL = 'api/v1/auth/tenant/verification/check';
    private const POST_TENANT_REGISTER_URL = 'api/v1/auth/tenant/register';
    private const POST_TENANT_LOGIN_URL = 'api/v1/auth/tenant/login';
    private const POST_TENANT_FORGET_PASSWORD_REQUEST = 'api/v1/user/forget/password/request';
    private const POST_TENANT_FORGET_PASSWORD_CHECK = 'api/v1/user/forget/password/check';
    private const POST_TENANT_FORGET_PASSWORD = 'api/v1/user/forget/password';
    private const POST_TENANT_CHANGE_PASSWORD = 'api/v1/user/change-password';
    private const POST_TENANT_FORGET_PASSWORD_IDENTIFIER = 'api/v1/user/forget/password/identifier';
    private $mockForAppDevice;

    protected function setUp(): void
    {
        parent::setUp();

        // mock app()->user; return to null coz we will use mock Auth with $this->actingAs($user);
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api\Auth')
            ->setName("app")
            ->setFunction(function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    protected function tearDown(): void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_with_phone_number_too_short()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            ['phone_number' => '0812345']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => __('api.input.phone_number.min', ['min' => 8])
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1422
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function testPhoneNumberAlreadyExistInDatabase()
    {
        $phoneNumber = '0856111'.mt_rand(1111, 999999);
        $user = factory(User::class)->create([
            'password'      => Hash::make('secretsecre'),
            'phone_number'  => $phoneNumber,
        ]);

        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            ['phone_number' => $phoneNumber]
        );
        
        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => __('api.input.phone_number.unique')
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1422
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function testPhoneNumberAlreadyExistInDatabaseWithValidJsonStructure()
    {
        $phoneNumber = '0856111'.mt_rand(1111, 999999);
        $user = factory(User::class)->create([
            'password'      => Hash::make('secretsecre'),
            'phone_number'  => $phoneNumber,
        ]);

        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            ['phone_number' => $phoneNumber]
        );
        
        $response->assertJsonStructure([
            'status',
            'meta'  => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_with_phone_number_too_long()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            ['phone_number' => '081234567891231']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => __('api.input.phone_number.max', ['max' => 14])
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_with_no_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            []
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => __('api.input.phone_number.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_with_invalid_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_REQUEST_URL,
            ['phone_number' => '00123456789']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => __('api.input.phone_number.regex')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_check_with_no_phone_nunber()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_CHECK_URL,
            ['verification_code' => 'XXXX']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => [
                    __('api.input.phone_number.required')
                ]
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_request_verification_check_with_no_verification_code()
    {
        $response = $this->json(
            'POST',
            self::POST_VERIFICATION_CHECK_URL,
            ['phone_number' => '0812345678']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'message' => [
                    __('api.input.code.required')
                ]
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_no_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'phone_number' => __('api.input.phone_number.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_no_email()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'email' => __('api.input.email.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_no_password()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'password' => __('api.input.password.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_no_name()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'name' => __('api.input.name.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_no_verification_code()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '08123456789',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'verification_code' => __('api.input.code.required')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_invalid_email()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'email' => __('api.input.email.email')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_short_name()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'Jo',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'name' => __('api.input.name.min', ['min' => 3])
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_long_name()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => str_random(51),
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'name' => __('api.input.name.max', ['max' => 50])
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_invalid_name()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => '12345',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'name' => __('api.input.name.alpha')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_short_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '0812345',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'phone_number' => __('api.input.phone_number.min', ['min' => 8])
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_long_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '081234567892341',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'phone_number' => __('api.input.phone_number.max', ['max' => 14])
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_invalid_phone_number()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '00123456789',
                'verification_code' => 'XXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'phone_number' => __('api.input.phone_number.regex')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_short_verification_code()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'verification_code' => __('api.owner.verification.code.invalid')
            ]
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_registration_with_too_long_verification_code()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => '08123456789',
                'verification_code' => 'XXXXX',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'messages'  => [
                'verification_code' => __('api.owner.verification.code.invalid')
            ]
        ]);
    }

    /**
     * @test
     * @group UG
     * @group UG-1340
     * @group App/Http/Web/TenantControllerTest
     */
    public function post_tenant_registration_with_pregistered_mamipay_tenant_data()
    {
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => null,
            'phone_number' => '08123456789',
        ]);
        /** @var ActivationCode */
        $activationCode = factory(ActivationCode::class)->create([
            'code' => '5275',
            'phone_number' => $mamipayTenant->phone_number,
            'for' => ActivationCodeType::TENANT_VERIFICATION,
        ]);
        $response = $this->postJson(
            self::POST_TENANT_REGISTER_URL,
            [
                'email' => 'test@johndoe.com',
                'name' => 'John Doe',
                'password' => 'secret',
                'phone_number' => $mamipayTenant->phone_number,
                'verification_code' => $activationCode->code,
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => true,
        ]);
        // mamipay tenant data should be filled
        $mamipayTenant->refresh();
        $this->assertNotNull($mamipayTenant->user);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_login()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('secretsecre')
        ]);
        
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_login_with_wrong_email()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('secretsecre')
        ]);
        
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => ('x' . $user->email),
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.auth.tenant.email.invalid'),
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_login_with_wrong_phone_number()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('secretsecre')
        ]);
        
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'phone_number' => ($user->phone_number . '0'),
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.auth.tenant.phone_number.invalid'),
        ]);
    }

    /**
     *  @test
     *
     *  Due to mistakes in the past, there are duplicate email in our db :(
     */
    public function post_tenant_login_with_duplicate_email()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('secretsecre')
        ]);

        $duplicate = factory(User::class)->create([
            'email' => $user->email,
            'password' => Hash::make('secretsecre')
        ]);
        
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $duplicate->email,
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.auth.tenant.email.forbidden'),
        ]);
    }

    /**
     * @test
     *
     *   Due to mistakes in the past, there are duplicate phone number in our db :(
     */
    public function post_tenant_login_with_duplicate_phone_number()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('secretsecre')
        ]);

        $duplicate = factory(User::class)->create([
            'phone_number' => $user->phone_number,
            'password' => Hash::make('secretsecre')
        ]);
        
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'phone_number' => $duplicate->phone_number,
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.auth.tenant.phone_number.forbidden'),
        ]);
    }

    /**  @test */
    public function post_tenant_login_with_invalid_email()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => 'xxxxxxx',
                'password' => 'secretsecre',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.email.email')
        ]);
    }

    /** @test */
    public function post_tenant_forget_password_request()
    {
        $user = factory(User::class)->create();

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_REQUEST,
            ['phone_number' => $user->phone_number]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     *  Test request forget password too frequently
     *
     *  @test
     */
    public function post_tenant_forget_password_request_too_frequent()
    {
        $user = factory(User::class)->create();

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_REQUEST,
            ['phone_number' => $user->phone_number]
        );

        $duplicate = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_REQUEST,
            ['phone_number' => $user->phone_number]
        );

        $duplicate->assertStatus(200);
        $duplicate->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.tenant.verification.code.too_frequent', ['minutes' => '01.00'])
            ]
        ]);
    }

    /** @test */
    public function post_tenant_forget_password_check_with_invalid_code()
    {
        $user = factory(User::class)->create();

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_CHECK,
            [
                'phone_number' => $user->phone_number,
                'verification_code' => 'XXXX'
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'messages' => [
                'verification_code' => __('api.tenant.verification.code.invalid')
            ]
        ]);
    }

    /** @test */
    public function post_tenant_forget_password_check()
    {
        $user = factory(User::class)->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $user->phone_number,
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD
        ]);

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_CHECK,
            [
                'phone_number' => $user->phone_number,
                'verification_code' => $code->code
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true
        ]);
    }

    /** @test */
    public function post_tenant_forget_password_with_invalid_code()
    {
        $user = factory(User::class)->create();
        $password = str_random(10);

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD,
            [
                'phone_number' => $user->phone_number,
                'verification_code' => 'XXXXX',
                'password' => $password,
                'password_confirmation' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'messages' => [
                'verification_code' => __('api.owner.verification.code.invalid')
            ]
        ]);
    }

    /** @test */
    public function post_tenant_forget_password_when_register_using_socialite()
    {
        $user = factory(User::class)->create();
        $social = factory(UserSocial::class)->create([
            'email' => $user->email,
            'name' => $user->name,
            'user_id' => $user->id
        ]);

        $response = $this->json('POST', self::POST_TENANT_FORGET_PASSWORD_REQUEST, [
                'phone_number' => $user->phone_number,
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.social'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    /** @test */
    public function post_tenant_forget_password()
    {
        $user = factory(User::class)->create();
        $code = factory(ActivationCode::class)->create([
            'phone_number' => $user->phone_number,
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD
        ]);

        $password = str_random(10);

        $forgotPasswordResponse = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD,
            [
                'phone_number' => $user->phone_number,
                'verification_code' => $code->code,
                'password' => $password,
                'password_confirmation' => $password
            ]
        );

        $forgotPasswordResponse->assertStatus(200);
        $forgotPasswordResponse->assertJson([
            'status' => true,
        ]);

        $loginResponse = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => $password,
            ]
        );

        $loginResponse->assertStatus(200);
        $loginResponse->assertJson([
            'status' => true,
        ]);
    }

    /** @test */
    public function post_tenant_change_password()
    {
        $oldPassword = str_random(15);
        $newPassword = str_random(16);

        $user = factory(User::class)->create([
            'password' => Hash::make($oldPassword)
        ]);

        $device = factory(UserDevice::class)->state('login')->create(['user_id' => $user->id]);
        $user->last_login = $device->id;
        $user->save();

        $response = $this->actingAs($user)->json(
            'POST',
            $this->withAccessToken(self::POST_TENANT_CHANGE_PASSWORD),
            [
                'old_password' => $oldPassword,
                'new_password' => $newPassword
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
        ]);

        $loginResponse = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => $newPassword,
            ]
        );

        $loginResponse->assertStatus(200);
        $loginResponse->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_change_password_with_unchanged_password()
    {
        $oldPassword = str_random(15);
        $user = factory(User::class)->create([
            'password' => Hash::make($oldPassword)
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();

        $response = $this->actingAs($user)->json(
            'POST',
            $this->withAccessToken(self::POST_TENANT_CHANGE_PASSWORD),
            [
                'old_password' => $oldPassword,
                'new_password' => $oldPassword
            ]
        );
        
        $response->assertStatus(200);
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_change_password_with_invalid_password()
    {
        $oldPassword = str_random(15);
        $newPassword = str_random(16);

        $user = factory(User::class)->create([
            'password' => Hash::make($oldPassword)
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();

        $response = $this->actingAs($user)->json(
            'POST',
            $this->withAccessToken(self::POST_TENANT_CHANGE_PASSWORD),
            [
                'old_password' => 'secret',
                'new_password' => $newPassword
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
        ]);

        $loginResponse = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => $newPassword,
            ]
        );

        $loginResponse->assertStatus(200);
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_change_password_with_no_old_password()
    {
        $oldPassword = str_random(15);
        $newPassword = str_random(16);

        $user = factory(User::class)->create([
            'password' => Hash::make($oldPassword)
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();

        $response = $this->actingAs($user)->json(
            'POST',
            $this->withAccessToken(self::POST_TENANT_CHANGE_PASSWORD),
            [
                'new_password' => $newPassword
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false
        ]);

        $loginResponse = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => $newPassword,
            ]
        );

        $loginResponse->assertStatus(200);
        $loginResponse->assertJson([
            'status' => false,
        ]);
    }

    /**
     * @test
     * @group Test/App/Http/Web/TenantControllerTest 
     */
    public function post_tenant_change_password_with_no_new_password()
    {
        $oldPassword = 'secretsecre';
        $newPassword = 'secretsecret';

        $user = factory(User::class)->create([
            'password' => Hash::make($oldPassword)
        ]);

        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();

        $response = $this->actingAs($user)->json(
            'POST',
            $this->withAccessToken(self::POST_TENANT_CHANGE_PASSWORD),
            [
                'old_password' => 'secret',
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
        ]);

        $loginResponse = $this->json(
            'POST',
            self::POST_TENANT_LOGIN_URL,
            [
                'email' => $user->email,
                'password' => $newPassword,
            ]
        );

        $loginResponse->assertStatus(200);
        $loginResponse->assertJson([
            'status' => false,
        ]);
    }

    public function testTenantForgetPasswordRequestWithNonUniquePhoneNumber()
    {
        $tenant = factory(User::class)->create();
        $otherTenant = factory(User::class)->create([
            'phone_number' => $tenant->phone_number
        ]);

        $response = $this->json('POST', self::POST_TENANT_FORGET_PASSWORD_REQUEST, [
            'phone_number' => $tenant->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.tenant.duplicate'),
            ],
            'title' => __('api.input.forget_password.failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testOwnerForgetPasswordRequestInTenantPage()
    {
        $owner = factory(User::class)->states('owner')->create();

        $response = $this->json('POST', self::POST_TENANT_FORGET_PASSWORD_REQUEST, [
            'phone_number' => $owner->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.wrong_section'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testTenantForgetPasswordRequestDuplicateOwnerTenant()
    {
        $owner = factory(User::class)->states('owner')->create();
        $tenant = factory(User::class)->create([
            'phone_number' => $owner->phone_number
        ]);

        $response = $this->json('POST', self::POST_TENANT_FORGET_PASSWORD_REQUEST, [
            'phone_number' => $owner->phone_number
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.tenant.duplicate'),
            ],
            'title' => __('api.input.forget_password.failed'),
            'is_forbidden' => true,
        ]);
    }

    public function testUnregisteredTenantForgetPasswordRequest()
    {
        $response = $this->json('POST', self::POST_TENANT_FORGET_PASSWORD_REQUEST, [
            'phone_number' => '081111222398'
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('api.input.forget_password.not_found'),
            ],
            'title' => __('api.input.forget_password.send_failed'),
            'is_forbidden' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithPhoneNumberShouldReturnTrue()
    {
        $user = factory(User::class)->create();

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => $user->phone_number]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierShouldReturnCorrectResponse()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            [
                'phone_number' => $user->phone_number, 
            ]
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'key' => VerificationMethod::SMS()->value['key'],
                    'label' => VerificationMethod::SMS()->value['label'],
                    'value' => $user->phone_number
                ],[
                    'key' => VerificationMethod::WA()->value['key'],
                    'label' => VerificationMethod::WA()->value['label'],
                    'value' => $user->phone_number
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithoutPhoneReturnFalse()
    {
        $response = $this->json('POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            []
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.required')
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithMinPhoneNumberReturnFalse()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => '0823223']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.min', ['min' => 8])
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithMaxPhoneNumberReturnFalse()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => '082323123221230']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.max', ['max' => 14])
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithUnregisteredPhoneNumberReturnFalse()
    {
        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => '08231123122']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.exists')
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithDuplicatePhoneNumberReturnFalse()
    {
        factory(User::class)->states('owner')->create([
            'phone_number' => '0812345689',
        ]);
        $tenant = factory(User::class)->create([
            'phone_number' => '0812345689',
        ]);

        $response = $this->json(
            'POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => $tenant->phone_number]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.forget_password.duplicate.message'),
            'contact' => [
                'cta_message' => __('api.input.forget_password.duplicate.cta_message'),
                'wa_number' => __('api.input.forget_password.duplicate.wa_number'),
                'pretext' => __('api.input.forget_password.duplicate.pretext', ['identifier' => $tenant->phone_number]),
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithInvalidPhoneNumberReturnFalse()
    {
        $response = $this->json('POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => +6285123435]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.invalid'),
            'contact' => null
        ]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Controllers\Api\Auth\TenantController
     */
    public function testTenantForgetPasswordIdentifierWithInvalidPhoneNumberFormatReturnFalse()
    {
        $response = $this->json('POST',
            self::POST_TENANT_FORGET_PASSWORD_IDENTIFIER,
            ['phone_number' => '+6285123435']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'message' => __('api.input.phone_number.regex')
        ]);
    }
}
