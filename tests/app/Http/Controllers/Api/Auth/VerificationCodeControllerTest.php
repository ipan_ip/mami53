<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entities\Activity\ActivationCodeType;
use App\Enums\Activity\ActivationCodeVia;
use App\Test\MamiKosTestCase;
use App\Http\Controllers\Api\Auth\VerificationCodeController;
use App\Services\Activity\ActivationCodeService;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use App\Entities\Activity\ActivationCode;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class VerificationCodeControllerTest extends MamiKosTestCase
{
    use WithFaker;
    use WithoutMiddleware;

    /** @var ActivationCodeService|MockInterface */
    private $activationCodeServie;

    protected function setUp()
    {
        parent::setUp();
        $this->activationCodeServie = $this->mock(ActivationCodeService::class);

    }

    private function callRequest($for, $via, $destination)
    {
        return $this->post(
            '/api/v1/auth/code/request',
            [
                'for' => $for,
                'via' => $via,
                'destination' => $destination,
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testRequest()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::getRandomInstance()->value;
        $destination = $this->fakeValidPhoneNumber();

        $activationCode = factory(ActivationCode::class)->make();

        $this->activationCodeServie->shouldReceive('getActivationCode')->andReturn(null);
        $this->activationCodeServie->shouldReceive('createActivationCode')->andReturn($activationCode);
        $this->activationCodeServie->shouldReceive('sendActivationCode');
        $this->activationCodeServie->shouldReceive('getAllowedResendTime')->andReturn(
            Carbon::createFromTimestamp($this->faker->unixTime)
        );

        $response = $this->callRequest(
            $for,
            $via,
            $destination
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testRequestResend()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::getRandomInstance()->value;
        $destination = $this->fakeValidPhoneNumber();

        $activationCode = factory(ActivationCode::class)->create();
        Carbon::setTestNow($activationCode->created_at->addDay(1));

        $this->activationCodeServie->shouldReceive('getActivationCode')->andReturn($activationCode);
        $this->activationCodeServie->shouldReceive('resendActivationCode');
        $this->activationCodeServie->shouldReceive('getAllowedResendTime')->andReturn(
            Carbon::createFromTimestamp($this->faker->unixTime)
        );

        $response = $this->callRequest(
            $for,
            $via,
            $destination
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testRequestViaSmsButUsingPhoneNumber()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::SMS;
        $destination = $this->faker->email;

        $response = $this->callRequest(
            $for,
            $via,
            $destination
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
    }

    private function fakeValidPhoneNumber()
    {
        return $this->faker->numerify('081#########');
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testRequestInvalidRequest()
    {
        $for = str_random(16);
        $via = mt_rand(9000, 9999);
        $destination = $this->fakeValidPhoneNumber();

        $activationCode = factory(ActivationCode::class)->make();

        $this->activationCodeServie->shouldReceive('getActivationCode')->andReturn(null);
        $this->activationCodeServie->shouldReceive('createActivationCode')->andReturn($activationCode);
        $this->activationCodeServie->shouldReceive('sendActivationCode');
        $this->activationCodeServie->shouldReceive('getAllowedResendTime')->andReturn(
            Carbon::createFromTimestamp($this->faker->unixTime)
        );

        $response = $this->callRequest(
            $for,
            $via,
            $destination
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
    }

    private function callCheck($for, $via, $destination, $code)
    {
        return $this->post(
            '/api/v1/auth/code/check',
            [
                'for' => $for,
                'via' => $via,
                'destination' => $destination,
                'code' => $code,
            ]
        );
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testCheckCorrectCode()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::getRandomInstance()->value;
        $destination = $this->fakeValidPhoneNumber();

        $activationCode = factory(ActivationCode::class)->create([
            'code' => '' . mt_rand(1000, 9999),
        ]);

        $this->activationCodeServie->shouldReceive('getActivationCode')->andReturn($activationCode);

        $response = $this->callCheck(
            $for,
            $via,
            $destination,
            $activationCode->code
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testCheckBadRequest()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::getRandomInstance()->value;
        $destination = $this->fakeValidPhoneNumber();

        $this->activationCodeServie->shouldNotReceive('getActivationCode');

        $response = $this->callCheck(
            $for,
            $via,
            $destination,
            str_random(2)
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
    }

    /**
     * @group UG
     * @group UG-4760
     */
    public function testCheckIncorrectCode()
    {
        $for = ActivationCodeType::getRandomInstance()->value;
        $via = ActivationCodeVia::getRandomInstance()->value;
        $destination = $this->fakeValidPhoneNumber();

        $activationCode = factory(ActivationCode::class)->create([
            'code' => '' . mt_rand(1000, 9999),
        ]);

        $this->activationCodeServie->shouldReceive('getActivationCode')->andReturn($activationCode);

        $wrongCode = substr($activationCode->code, 0, 3) . ((int) $activationCode->code[-1] + 1);

        $response = $this->callCheck(
            $for,
            $via,
            $destination,
            $wrongCode
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
    }
}
