<?php

namespace App\Http\Controllers\Api\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Entities\Premium\PremiumPlusUser;
use App\Entities\Premium\PremiumPlusInvoice;
use App\Entities\Room\Room;

class PremiumPlusInvoiceControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const PREMIUM_PLUS_INVOICE_URL = 'api/v1/premium/plus/invoice/%s';
    private $mockRepository;

    public function testGetDetailInvoice()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);
        $room = factory(Room::class)->create();
        $userGP4 = factory(PremiumPlusUser::class)->create([
            'designer_id' => $room->id,
            'user_id'   => $user,
            'phone_number'  => $user->phone_number
        ]);
        $invoiceNumber = "2020/09/23/0234";
        $invoice = factory(PremiumPlusInvoice::class)->create([
            "name"  => "Pembayaran Bulan ke-1",
            "premium_plus_user_id"  => $userGP4,
            "invoice_number" => $invoiceNumber,
            'status'    => PremiumPlusInvoice::INVOICE_STATUS_UNPAID
        ]);

        app()->user = $user;

        $url = sprintf(self::PREMIUM_PLUS_INVOICE_URL, $invoice->shortlink);
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', $url);
        $response->assertOk();
        $response->assertJsonFragment([
            'invoice_number' => $invoiceNumber
        ]);
    }
}