<?php
namespace Test\App\Http\Controllers\Api\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;

class PremiumAllocationControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const PREMIUM_ALLOCATION_URL = 'api/v1/premium/allocation/123';
    private const SETTING_ALLOCATION_STATUS_URL = 'api/v1/premium/allocation/daily/setting';
    private const DEACTIVATE_ROOM_ALLOCATION_URL = 'api/v1/premium/allocation/%s/deactivate';
    private $mockPremiumAllocationRepository;
    private $mockPremiumRepository;

    public function setUp() : void
    {
        parent::setUp();
        // setup to mock repository
        $this->mockPremiumAllocationRepository = $this->mockAlternatively('App\Repositories\Premium\PremiumAllocationRepository');
        $this->mockPremiumRepository = $this->mockAlternatively('App\Repositories\PremiumRepository');
    }

    public function testPremiumAllocationNotAuthentication()
    {
        $response = $this->json('POST', self::PREMIUM_ALLOCATION_URL, []);
        $responseJson = json_decode($response->getContent());
        $this->assertFalse($responseJson->status);
    }

    public function testPremiumAllocationAuthenticationNotOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::PREMIUM_ALLOCATION_URL);
        $responseJson = json_decode($response->getContent());
        $this->assertFalse($responseJson->status);
    }

    public function testPremiumAllocationSuccess()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $room = factory(Room::class)->create([
            'song_id' => 123
        ]);

        factory(RoomOwner::class)->create([
            'status' => 'verified',
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        
        $this->mockPremiumAllocationRepository
            ->shouldReceive('premiumAllocation')
            ->andReturn([
                "status" => true
            ]);

        $response = $this->json('POST', self::PREMIUM_ALLOCATION_URL, ["allocation" => 5000]);
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }

    public function testChangeDailyAllocationStatusNotAuthentication()
    {
        $response = $this->json('GET', self::SETTING_ALLOCATION_STATUS_URL);
        $response->assertStatus(500);
    }

    public function testChangeDailyAllocationStatusWithAuthenticationNotOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('GET', self::SETTING_ALLOCATION_STATUS_URL);
        $responseJson = json_decode($response->getContent());
        $this->assertFalse($responseJson->status);
    }

    public function testChangeDailyAllocationStatusSuccess()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $premium = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "expired_date" => null,
            "daily_allocation" => 1
        ]);

        $this->mockPremiumRepository
            ->shouldReceive('settingDailyAllocationStatus')
            ->andReturn([
                "status" => true
            ]);

        $response = $this->json('GET', self::SETTING_ALLOCATION_STATUS_URL);
        $responseJson = json_decode($response->getContent());
        $premiumRequest = PremiumRequest::find($premium->id);
        
        $this->assertEquals(0, $premiumRequest->daily_allocation);
        $this->assertTrue($responseJson->status);
    }

    public function testDeactivateRoomAllocationSuccess()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $room = factory(Room::class)->create([
            "is_active" => "true",
            "song_id" => 123,
            "is_promoted" => "true",
        ]);
        
        $roomPromote = factory(ViewPromote::class)->create([
            "designer_id" => $room->id,
            "premium_request_id" => factory(PremiumRequest::class)->create([
                "user_id" => $user->id,
                "status" => "1",
                "expired_date" => null,
                "daily_allocation" => 1
            ])->id,
            "is_active" => 1
        ]);

        $this->mockPremiumAllocationRepository
            ->shouldReceive('deactivatePremiumPromote')
            ->with(ViewPromote::class)
            ->andReturn();
        
        $url = sprintf(self::DEACTIVATE_ROOM_ALLOCATION_URL, 123);
        $response = $this->json('GET', $url);
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }

}