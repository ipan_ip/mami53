<?php
namespace Test\App\Http\Controllers\Api\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Psr\Http\Message\ResponseInterface;
use Config;

class AbTestPremiumControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const CHECK_OWNER_AB_TEST_PREMIUM = 'api/v1/owner/premium/ab-test/check';

    public function testIsNotOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);

        $this->actingAs($user);
        app()->user = $user;
        $response = $this->json('GET', self::CHECK_OWNER_AB_TEST_PREMIUM);
        $responseJson = json_decode($response->getContent());
        $this->assertEquals($responseJson->result, null);
    }

    public function testIsOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $this->actingAs($user);
        app()->user = $user;
        $response = $this->json('GET', self::CHECK_OWNER_AB_TEST_PREMIUM);
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }
}