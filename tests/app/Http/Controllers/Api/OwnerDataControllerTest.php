<?php

namespace Test\App\Http\Web;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use App\Entities\Activity\Call;
use App\Entities\Activity\Love;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Media\Media;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\Promotion;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Review;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\Owner\UpdateKostUpdatedDate;
use App\Test\MamiKosTestCase;
use App\User;
use phpmock\MockBuilder;
use Mockery;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Collection;

/**
 *  Test route handled by TenantController
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *  @see App\Http\Controllers\Api\Auth\TenantController
 *
 */
class OwnerDataControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_LIST_EXPENSES = 'api/v1/owner/data/finance/list-expense';
    private const GET_SPECIFIC_KOST = 'api/v1/owner/data/finance/kost';
    private const GET_LIST_TENANT = 'api/v1/owner/data/finance/list-tenant';
    private const GET_FINANCE_LIST_KOST = 'api/v1/owner/data/finance/list-kost';
    private const GET_INCOME_URL = 'api/v1/owner/data/income';
    private const POST_UPDATE_ROOM_URL = 'garuda/owner/update_room_price';
    private const POST_UPDATE_ALL_PROPERTY = 'garuda/owner/data/update-room';
    private const GET_OWNER_DATA_PROFILE = 'api/v1/owner/data/profile';
    private const POST_OWNER_EMAIL_VERIFICATION = 'api/v1/owner/verification/email';
    private const GET_ROOM_ACTIVE_URL = 'api/v1/owner/data/list/active';
    private const OWNER_KOST_LIST_API = 'api/v1/owner/data/kos';

    private $mockForAppDevice;

    public function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockAlternatively('App\Repositories\OwnerDataRepository');

        // mock app()->user; return to null coz we will use mock Auth with $this->actingAs($user);
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->faker = Faker::create();
    }

    /** @test */
    public function getListKostsExpense_UserNotLogin()
    {
        Bugsnag::shouldReceive('notifyException')->once();

        $response = $this->json('GET', self::GET_LIST_EXPENSES.'/'.mt_rand(111111, 999999));

        $response->assertStatus(200);
    }

    /** @test */
    public function getListKostsExpense_StatusKostFalse()
    {
        $kostId = mt_rand(111111, 999999);
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(false);

        $response = $this->json('GET', self::GET_LIST_EXPENSES.'/'.$kostId);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => 'Kost '.$kostId.' bukan milik '.$user->name
        ]);
    }

    /** @test */
    public function getListKostsExpense_DefaultOffsetLimit()
    {
        $length = 3;
        $kostId = mt_rand(111111, 999999);
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        for ($x=0; $x<$length; $x++) {
            $result[] = [
                "name" => "Saldo Iklan Terpakai",
                "type" => "Mamikos",
                "expense" => mt_rand(1111111, 99999999),
                "date" => "2020-06-18",
                "image" => "public/storage/images/".$this->faker->userName."-240x320.jpg",
            ];
        }

        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getListPremiumExpensesSpecificKost')
            ->once()
            ->andReturn($result);

        $response = $this->json('GET', self::GET_LIST_EXPENSES.'/'.$kostId);
        $arrayResponse = json_decode($response->content(), true);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $result
        ]);
        $this->assertEquals($length, count($arrayResponse['data']));
    }

    /** @test */
    public function getListKostsExpense_CustomOffsetLimit()
    {
        $length = 5;
        $kostId = mt_rand(111111, 999999);
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        for ($x=0; $x<$length; $x++) {
            $result[] = [
                "name" => "Saldo Iklan Terpakai",
                "type" => "Mamikos",
                "expense" => mt_rand(1111111, 99999999),
                "date" => "2020-06-18",
                "image" => "public/storage/images/".$this->faker->userName."-240x320.jpg",
            ];
        }

        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getListPremiumExpensesSpecificKost')
            ->once()
            ->andReturn($result);

        $response = $this->json('GET', self::GET_LIST_EXPENSES.'/'.$kostId.'?offset=0'.'&limit='.$length);
        $arrayResponse = json_decode($response->content(), true);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $result
        ]);
        $this->assertEquals($length, count($arrayResponse['data']));
    }

    /** @test */
    public function getListTenant_UserNotFound()
    {
        $room = factory(Room::class)->create();
        Bugsnag::shouldReceive('notifyException')->once();
        $response = $this->json('GET', self::GET_LIST_TENANT.'/'.$room->song_id);

        $response->assertStatus(200);
    }

    /** @test */
    public function getListTenant_VerifyKostFailed()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);
        $kostId = mt_rand(111111, 999999);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(false);

        $response = $this->json('GET', self::GET_LIST_TENANT.'/'.$kostId);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => 'Kost '.$kostId.' bukan milik '.$user->name
        ]);
    }

    /** @test */
    public function getListTenant_SuccessDefaultOffsetLimit()
    {
        $length = 3;
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);
        $room = factory(Room::class)->create();

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);

        for ($x=0; $x<$length; $x++) {
            $result[] = [
                "title" => "Pembayaran bulan ke-1",
                "total_amount" => "750000",
                "type" => "Mamipay",
                "tenant_name" => "dolorem quas eligendi",
                "tenant_photo" => "https://s3.ap-southeast-1.amazonaws.com/mamipay/cache/voucher_campaign/20200404/H6tGWcEJ-small.jpg",
                "room_name" => ""
            ];
        }

        // mock repository
        $this->mock->shouldReceive('getListTenants')->once()->andReturn($result);

        $response = $this->json('GET', self::GET_LIST_TENANT.'/'.$room->song_id);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => true,
            'data'      => $result,
        ]);
    }

    /** @test */
    public function getListTenant_SuccessCustomOffsetLimit()
    {
        $length = 10;
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);
        $room = factory(Room::class)->create();

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);

        for ($x=0; $x<$length; $x++) {
            $result[] = [
                "title" => "Pembayaran bulan ke-1",
                "total_amount" => "750000",
                "type" => "Mamipay",
                "tenant_name" => "dolorem quas eligendi",
                "tenant_photo" => "https://s3.ap-southeast-1.amazonaws.com/mamipay/cache/voucher_campaign/20200404/H6tGWcEJ-small.jpg",
                "room_name" => ""
            ];
        }

        // mock repository
        $this->mock->shouldReceive('getListTenants')->once()->andReturn($result);

        $response = $this->json('GET', self::GET_LIST_TENANT.'/'.$room->song_id);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => true,
            'data'      => $result,
        ]);
    }

    /** @test */
    public function getVerifyKostWithItsOwnerBySongId_fail()
    {
        $kostId = mt_rand(111111, 999999);
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(false);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => 'Kost '.$kostId.' bukan milik '.$user->name
        ]);
    }

    /** @test */
    public function getFinanceKost_SemesterInvalid()
    {
        $kostId = 85539445;
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?semester=4&year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => 'invalid parameter'
        ]);
    }

    /** @test */
    public function getFinanceKost_SemesterSuccess()
    {
        $kostId = 85539445;
        $expected = [
            'total_amount' => 1000000,
            'total_expense' => 1000000,
            'room' => [],
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getFinanceSpecificKostSemester')
            ->once()
            ->andReturn($expected);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?semester=1&year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $expected
        ]);
    }

    /** @test */
    public function getFinanceKost_AllTimeSuccess()
    {
        $kostId = 85539445;
        $expected = [
            'total_amount' => 1000000,
            'total_expense' => 1000000,
            'room' => [],
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getFinanceSpecificKost')
            ->once()
            ->andReturn($expected);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $expected
        ]);
    }

    /** @test */
    public function getFinanceKost_YearlySuccess()
    {
        $kostId = 85539445;
        $expected = [
            'total_amount' => 1000000,
            'total_expense' => 1000000,
            'room' => [],
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getFinanceSpecificKost')
            ->once()
            ->andReturn($expected);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $expected
        ]);
    }

    /** @test */
    public function getFinanceKost_MonthlyFail()
    {
        $kostId = 85539445;
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?year=akjfkadjfdkafa&month=fjsdhfsdjfsdl');

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => 'invalid parameter'
        ]);
    }

    /** @test */
    public function getFinanceKost_PreviousMonthYearCached()
    {
        $kostId = 85539445;
        $expected = [
            'total_amount' => 1000000,
            'total_expense' => 1000000,
            'room' => [],
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        Cache::shouldReceive('rememberForever')->once()->andReturn($expected);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?year=2019&month=5');

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $expected
        ]);
    }

    /** @test */
    public function getFinanceKost_MonthYearSuccess()
    {
        $kostId = 85539445;
        $expected = [
            'total_amount' => 1000000,
            'total_expense' => 1000000,
            'room' => [],
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('verifyKostWithItsOwnerBySongId')
            ->once()
            ->andReturn(true);
        $this->mock->shouldReceive('getFinanceSpecificKost')
            ->once()
            ->andReturn($expected);

        $response = $this->json('GET', self::GET_SPECIFIC_KOST.'/'.$kostId.'?year='.date('Y').'&month='.date('n'));

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $expected
        ]);
    }

    /** @test */
    public function getDashboardTotalIncomeSemester_ParamValid()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getTotalIncomeSemester')->once()->andReturn(900000);
        $this->mock->shouldReceive('getTotalPremiumExpenseSemester')->once()->andReturn(100000);

        $response = $this->json('GET', self::GET_INCOME_URL.'?semester=1&year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 900000,
            'total_expense'  => 100000,
        ]);
    }

    public function get_list_kosts_income_user_not_login()
    {
        $response = $this->json('GET', self::GET_FINANCE_LIST_KOST);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => "Anda belum login."
        ]);
    }

    /** @test */
    public function getDashboardTotalIncomeSemester_SemesterIntNotValid()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_INCOME_URL.'?semester=13&year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'data'      => null,
            'message'   => "invalid parameter"
        ]);
    }

    public function get_list_kosts_income_keyword_not_null()
    {
        $kosts = [
            [
                "total_amount" => 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost",
                    "address" => "Tobelo, Gamsungi, Kabupaten Halmahera Utara",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ],
            [
                "total_amount"=> 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost A1 umami",
                    "address" => "Tobelo, Gamsungi, Tobelo",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ]
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getFinanceKostFromName')
            ->once()
            ->andReturn($kosts);

        $response = $this->json(
            'GET',
            self::GET_FINANCE_LIST_KOST,
            ["keyword" => 'umami']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $kosts
        ]);
    }

    /** @test */
    public function getDashboardTotalIncomeSemester_SemesterStringNotValid()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('getTotalIncome')->once()->andReturn(900000);
        $this->mock->shouldReceive('getTotalPremiumExpense')->once()->andReturn(100000);

        $response = $this->json('GET', self::GET_INCOME_URL.'?semester=fdafad&year=2020');

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 900000,
            'total_expense' => 100000
        ]);
    }


    /** @test */
    public function get_list_kosts_income_limit_offset_not_null()
    {
        $kosts = [
            [
                "total_amount" => 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost",
                    "address" => "Tobelo, Gamsungi, Kabupaten Halmahera Utara",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ],
            [
                "total_amount"=> 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost A1 umami",
                    "address" => "Tobelo, Gamsungi, Tobelo",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ]
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getFinanceListKosts')
            ->once()
            ->andReturn($kosts);

        $response = $this->json(
            'GET',
            self::GET_FINANCE_LIST_KOST,
            [
                "offset" => 0,
                "limit" => 2
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $kosts
        ]);
    }

    /** @test */
    public function get_list_kosts_income_keyword_not_null_not_empty()
    {
        $kosts = [
            [
                "total_amount" => 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost",
                    "address" => "Tobelo, Gamsungi, Kabupaten Halmahera Utara",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ],
            [
                "total_amount"=> 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost A1 umami",
                    "address" => "Tobelo, Gamsungi, Tobelo",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ]
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getFinanceKostFromName')
            ->once()
            ->andReturn($kosts);

        $response = $this->json(
            'GET',
            self::GET_FINANCE_LIST_KOST,
            ["keyword" => 'umami']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $kosts
        ]);
    }

    /** @test */
    public function get_list_kosts_income_keyword_empty()
    {
        $kosts = [
            [
                "total_amount" => 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost",
                    "address" => "Tobelo, Gamsungi, Kabupaten Halmahera Utara",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ],
            [
                "total_amount"=> 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost A1 umami Kost A1 umami",
                    "address" => "Tobelo, Gamsungi, Tobelo",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ],
            [
                "total_amount"=> 100706500,
                "room" => [
                    "id" => 85539445,
                    "name" => "Kost LALALALALALA",
                    "address" => "Tobelo, Gamsungi, Tobelo",
                    "gender" => "campur",
                    "room_available" => 22,
                    "price_monthly" => 60000000,
                    "image" => Config::get('api.media.cdn_url')
                ]
            ]
        ];
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getFinanceListKosts')
            ->once()
            ->andReturn($kosts);

        $response = $this->json(
            'GET',
            self::GET_FINANCE_LIST_KOST,
            ["keyword" => '']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'data'   => $kosts
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_fail_when_user_not_authenticated()
    {
        $response = $this->json('GET', self::GET_INCOME_URL);

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'message'   => "Anda belum login."
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_fail_when_no_parameter()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getTotalIncome')->once()->andReturn(900000);
        $this->mock->shouldReceive('getTotalPremiumExpense')->once()->andReturn(100000);

        $response = $this->json('GET', self::GET_INCOME_URL);

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 900000,
            'total_expense'  => 100000,
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_with_input_year_only()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getTotalIncome')
            ->once()->andReturn(1000000);
        $this->mock->shouldReceive('getTotalPremiumExpense')->once()->andReturn(100000);

        $response = $this->json('GET', self::GET_INCOME_URL, ["year" => 2020]);

        $response->assertOk();
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 1000000,
            'total_expense' => 100000
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_with_input_year_and_month_incorect_numeric()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json(
            'GET',
            self::GET_INCOME_URL,
            ["year" => "2020ABC", "month" => "FEBURARYY"]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'    => false,
            'data'      => null,
            'message'   => "invalid parameter"
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_with_current_input_year_and_month()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock repository
        $this->mock->shouldReceive('getTotalIncome')
            ->once()->andReturn(898900);
        $this->mock->shouldReceive('getTotalPremiumExpense')->once()->andReturn(190900);

        $response = $this->json(
            'GET',
            self::GET_INCOME_URL,
            ["year" => date('Y'), "month" => date('n')]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 898900,
            'total_expense' => 190900
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_with_previous_input_month_and_year()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock cache
        Cache::shouldReceive('rememberForever')->once()->andReturn([
            'total_amount' => 898900,
            'total_expense' => 190900,
        ]);

        $response = $this->json(
            'GET',
            self::GET_INCOME_URL,
            ["year" => '2020', "month" => '2']
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 898900,
            'total_expense' => 190900
        ]);
    }

    /** @test */
    public function get_dashboard_total_income_with_previous_input_year_and_month()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        // mock cache
        Cache::shouldReceive('rememberForever')->once()->andReturn([
            'total_amount' => 898900,
            'total_expense' => 190900,
        ]);

        $response = $this->json(
            'GET',
            self::GET_INCOME_URL,
            ["year" => '2010', "month" => date('n')]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status'        => true,
            'total_amount'  => 898900,
            'total_expense' => 190900
        ]);
    }

    /** @test */
    public function check_validation_update_room_only_month_is_checked()
    {
        // setup user
        $mockUser = factory(User::class)->create();
        $this->actingAs($mockUser);

        // setup kost
        $mockKost = factory(Room::class)->create();
        factory(RoomOwner::class)->create([ "designer_id" => $mockKost->id, "user_id" => $mockUser->id]);

        // api call
        $response = $this->json(
            'POST',
            self::POST_UPDATE_ROOM_URL,
            [
                "room_id"           => $mockKost->song_id,
                "room_available"    => 3,
                "price_daily"       => 0,
                "price_weekly"      => 0,
                "price_monthly"     => 9000,
                "price_yearly"      => 0,
                "3_month"           => 0,
                "6_month"           => 0,
            ]
        );

        $responseJson = json_decode($response->getContent());
        $errorMessages = $responseJson->messages;

        // assert
        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
        $this->assertEquals(array(
            "Harga Sewa Bulanan harus diisi dan harus lebih dari 10000",
        ), $errorMessages);
    }

    /** @test */
    public function check_validation_update_room_fail_price_must_above_1000()
    {
        // setup user
        $mockUser = factory(User::class)->create();
        $this->actingAs($mockUser);

        // setup kost
        $mockKost = factory(Room::class)->create();
        factory(RoomOwner::class)->create([ "designer_id" => $mockKost->id, "user_id" => $mockUser->id]);

        // api call
        $response = $this->json(
            'POST',
            self::POST_UPDATE_ROOM_URL,
            [
                "room_id"           => $mockKost->song_id,
                "room_available"    => 3,
                "price_daily"       => 4000,
                "price_weekly"      => 9999,
                "price_monthly"     => 8888,
                "price_yearly"      => 4123,
                "3_month"           => 5555,
                "6_month"           => 4444,
            ]
        );

        $responseJson = json_decode($response->getContent());
        $errorMessages = $responseJson->messages;

        // assert
        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
        $this->assertEquals(array(
            "Harga Sewa Bulanan harus diisi dan harus lebih dari 10000",
            "Harga Tahunan harus lebih besar 10000",
            "Harga Mingguan harus lebih besar 10000",
            "Harga Harian harus lebih besar 10000",
            "Harga 3 Bulanan harus lebih besar 10000",
            "Harga 6 Bulanan harus lebih besar 10000",
        ), $errorMessages);
    }

    /** @test */
    public function check_validation_update_room_fail_price_must_gt_or_equal_1000()
    {
        // setup user
        $mockUser = factory(User::class)->create();
        $this->actingAs($mockUser);

        // setup kost
        $mockKost = factory(Room::class)->create();
        factory(RoomOwner::class)->create([ "designer_id" => $mockKost->id, "user_id" => $mockUser->id]);

        // api call
        $response = $this->json(
            'POST',
            self::POST_UPDATE_ROOM_URL,
            [
                "room_id"           => $mockKost->song_id,
                "room_available"    => 3,
                "price_daily"       => 10000,
                "price_weekly"      => 10000,
                "price_monthly"     => 10000,
                "price_yearly"      => 10000,
                "3_month"           => 10000,
                "6_month"           => 9999,
            ]
        );

        $responseJson = json_decode($response->getContent());
        $errorMessages = $responseJson->messages;

        // assert
        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
        $this->assertEquals(array(
            "Harga 6 Bulanan harus lebih besar 10000",
        ), $errorMessages);
    }

    /** @test */
    public function testUpdateAllPropertyUnauthorized()
    {
        $response = $this->json('POST', self::POST_UPDATE_ALL_PROPERTY);

        $response->assertStatus(401);
    }

    /** @test */
    public function testUpdateAllPropertyNotOwner()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('POST', self::POST_UPDATE_ALL_PROPERTY);

        $response->assertStatus(404);
    }

    /** @test */
    public function testUpdateAllPropertyIsOwner()
    {
        $user = factory(\App\User::class)->state('owner')->create();
        $this->actingAs($user);
        Queue::fake();
        $response = $this->json('POST', self::POST_UPDATE_ALL_PROPERTY);

        Queue::assertPushed(UpdateKostUpdatedDate::class, function ($job) use ($user) {
            return $job->userId === $user->id;
        });

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'meta',
            'activity' => [
                'logged_in_at',
                'updated_property_at'
            ]
        ]);
    }

    public function testGetProfileUnauthorizedReturnFalse()
    {
        $response = $this->json('GET', self::GET_OWNER_DATA_PROFILE);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
    }

    public function testGetProfileNotOwnerReturnFalse()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_OWNER_DATA_PROFILE);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testSimpleUpdateNotifWithInactiveRoom()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_OWNER_DATA_PROFILE);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 333,
                'severity' => 'OK',
                'message' => 'No owner data'
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testSimpleUpdateNotifWithInactiveRoomAndExactJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_OWNER_DATA_PROFILE);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailSuccessfully()
    {
        $email = 'coba@getnada.com';
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);

        $response->assertStatus(200);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailWhichNotAuthorizedUser()
    {
        $email = 'coba@getnada.com';
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => 'Data user tidak ditemukan'
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailSuccessWithExactApiResultStructure()
    {
        $email = 'coba@getnada.com';
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'message'
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailWhichUserNotFound()
    {
        $email = 'coba@getnada.com';
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailWhichInvalidEmail()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = str_random(16);
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailWhichInvalidEmailAndExactApiResponse()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = str_random(16);
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'message'
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailWhichInvalidRfc2822Email()
    {
        $email = 'Kharisma@facebook.com';
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = str_random(16);
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $email]);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailAlreadyUsedByOtherAccount()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = 'test@getnada.com';
        $userData = factory(\App\User::class)->create([
            'email' => $email
        ]);
        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $userData->email]);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => 'Email sudah digunakan oleh akun lain'
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerDataController
     */
    public function testPostVerificationEmailAlreadyUsedByOtherAccountAndExactJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('POST', self::POST_OWNER_EMAIL_VERIFICATION, ['email' => $user->email]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'message'
        ]);
    }

    public function testGetListRoomActiveFailed()
    {
        $response = $this->json('GET', self::GET_ROOM_ACTIVE_URL."/kos");
        $responseObj = json_decode($response->getContent());
        $this->assertFalse($responseObj->status);
    }

    public function testGetListRoomActiveSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_ROOM_ACTIVE_URL."/kos");
        $responseObj = json_decode($response->getContent());
        $this->assertTrue($responseObj->status);
    }

    public function testGetOwnedKosFailedNoUser()
    {
        $response = $this->get(self::OWNER_KOST_LIST_API);
        $response->assertStatus(401);
    }

    public function testGetOwnedKosFailedParamNotInteger()
    {
        $user = factory(User::class)->state('owner')->create();
        $this->actingAs($user);
        $response = $this->get(self::OWNER_KOST_LIST_API . '?limit="1 or \'1=1\'"');
        $response->assertStatus(422);
    }

    public function testGetOwnedKosSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'view' => 10000,
            'expired_date' => null,
            'status' => '1',
            'used' => 10,
            'allocated' => 180,
            'daily_allocation' => 1,
        ]);

        $roundPhoto = factory(Media::class)->create();
        $normalPhoto = factory(Media::class)->create();

        $roomCount = 15;

        $rooms = factory(Room::class, $roomCount)->states([
            'active',
            'notMamirooms',
            'availablePriceMonthly',
            'with-small-rooms',
            'with-unique-code',
            'with-all-prices'
        ])->create([
            'photo_round_id' => $roundPhoto->id,
            'photo_id' => $normalPhoto->id
        ]);

        $tagRoom = factory(Tag::class, 6)->create([
            'type'=> 'fac_room'
        ]);

        $tagBath = factory(Tag::class, 6)->create([
            'type'=> 'fac_bath'
        ]);

        $tagShare = factory(Tag::class, 6)->create([
            'type'=> 'fac_share'
        ]);

        $tagKeyword = factory(Tag::class, 12)->create([
            'type'=> 'keyword'
        ]);

        $tags = $tagRoom->concat($tagBath)->concat($tagShare);

        $rooms->each(function ($room) use ($user, $tags, $tagKeyword) {

            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS,
            ]);
        });

        $bookingDiscountCollect = collect(['daily', 'weekly', 'quarterly', 'semiannually', 'annually']);

        $rooms->nth(2, 1)->each(function ($room) use ($bookingDiscountCollect, $tags, $tagKeyword) {

            $tagsUsed = $tags->random(rand(8, $tags->count()));
            factory(RoomTag::class, $tagsUsed->count())->create([
                'tag_id' => $tagsUsed->shift()->id,
                'designer_id' => $room->id,
            ]);
            $keywordTag = $tagKeyword->random(rand(3, $tagKeyword->count()));
            factory(RoomTag::class, $keywordTag->count())->make([
                'designer_id' => $room->id,
            ])->each(function ($roomTag) use ($keywordTag) {
                $roomTag->tag_id = $keywordTag->shift()->id;
                $roomTag->save();
            });

            $bookingDiscount = $bookingDiscountCollect;
            $bookingDiscount->each(function ($discount) use ($room) {
                factory(BookingDiscount::class)->state($discount)->create([
                    'designer_id' => $room->id,
                    'is_active' => 1,
                    'price' => 999999
                ]);
            });
        });

        $rooms->nth(3, 1)->each(function ($room) {
            factory(Love::class, rand(1, 18))->create([
                'designer_id' => $room->id
            ]);

            factory(Review::class, rand(1, 13))->create([
                'designer_id' => $room->id,
                'status' => 'live'
            ]);
        });

        $rooms->nth(4, 1)->each(function ($room) use ($premiumRequest) {
            factory(Call::class, rand(1, 18))->create([
                'designer_id' => $room->id
            ]);

            factory(ViewPromote::class)->create([
                'designer_id' => $room->id,
                'premium_request_id' => $premiumRequest->id,
                'total' => 20,
                'is_active' => 1,
                'used' => 10,
                'history' => 0,
                'daily_budget' => 20
            ]);

            factory(Promotion::class)->create([
                'designer_id' => $room->id
            ]);
        });

        $this->actingAs($user);
        $response = $this->get(self::OWNER_KOST_LIST_API);
        $response->assertOk();
        $body = $response->getData();
        $this->assertEquals($roomCount, count($body->data->rooms));
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                "code",
                "severity",
                "message",
            ],
            "page",
            "next-page",
            "limit",
            "offset",
            "has-more",
            "total",
            "toast",
            "data" => [
                'rooms'
            ]
        ]);
    }

    public function testGetOwnedKosWithRoomIdSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'view' => 10000,
            'expired_date' => null,
            'status' => '1',
            'used' => 10,
            'allocated' => 180,
            'daily_allocation' => 1,
        ]);

        $roundPhoto = factory(Media::class)->create();
        $normalPhoto = factory(Media::class)->create();

        $roomCount = 15;

        $rooms = factory(Room::class, $roomCount)->states([
            'active',
            'notMamirooms',
            'availablePriceMonthly',
            'with-small-rooms',
            'with-unique-code',
            'with-all-prices'
        ])->create([
            'photo_round_id' => $roundPhoto->id,
            'photo_id' => $normalPhoto->id
        ]);

        $targettedRoom = $rooms->random();

        $tagRoom = factory(Tag::class, 6)->create([
            'type'=> 'fac_room'
        ]);

        $tagBath = factory(Tag::class, 6)->create([
            'type'=> 'fac_bath'
        ]);

        $tagShare = factory(Tag::class, 6)->create([
            'type'=> 'fac_share'
        ]);

        $tagKeyword = factory(Tag::class, 12)->create([
            'type'=> 'keyword'
        ]);

        $tags = $tagRoom->concat($tagBath)->concat($tagShare);

        $rooms->each(function ($room) use ($user, $tags, $tagKeyword) {
            $tagsUsed = $tags->random(rand(8, $tags->count()));
            factory(RoomTag::class, $tagsUsed->count())->create([
                'tag_id' => $tagsUsed->shift()->id,
                'designer_id' => $room->id,
            ]);
            $keywordTag = $tagKeyword->random(rand(3, $tagKeyword->count()));
            factory(RoomTag::class, $keywordTag->count())->make([
                'designer_id' => $room->id,
            ])->each(function ($roomTag) use ($keywordTag) {
                $roomTag->tag_id = $keywordTag->shift()->id;
                $roomTag->save();
            });

            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS,
            ]);
        });

        $bookingDiscountCollect = collect(['daily', 'weekly', 'quarterly', 'semiannually', 'annually']);

        $rooms->nth(2, 1)->each(function ($room) use ($bookingDiscountCollect) {
            $bookingDiscount = $bookingDiscountCollect;
            $bookingDiscount->each(function ($discount) use ($room) {
                factory(BookingDiscount::class)->state($discount)->create([
                    'designer_id' => $room->id,
                    'is_active' => 1,
                    'price' => 999999
                ]);
            });
        });

        $rooms->nth(3, 1)->each(function ($room) {
            factory(Love::class, rand(1, 18))->create([
                'designer_id' => $room->id
            ]);

            factory(Review::class, rand(1, 13))->create([
                'designer_id' => $room->id,
                'status' => 'live'
            ]);
        });

        $rooms->nth(4, 1)->each(function ($room) use ($premiumRequest) {
            factory(Call::class, rand(1, 18))->create([
                'designer_id' => $room->id
            ]);

            factory(ViewPromote::class)->create([
                'designer_id' => $room->id,
                'premium_request_id' => $premiumRequest->id,
                'total' => 20,
                'is_active' => 1,
                'used' => 10,
                'history' => 0,
                'daily_budget' => 20
            ]);

            factory(Promotion::class)->create([
                'designer_id' => $room->id
            ]);
        });

        $this->actingAs($user);
        $response = $this->get(self::OWNER_KOST_LIST_API . '?limit=3&room_id=' . $targettedRoom->song_id);
        $response->assertOk();
        $body = $response->getData();
        $this->assertEquals(3, count($body->data->rooms));
        $this->assertEquals($targettedRoom->song_id, $body->data->rooms[0]->_id);


        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                "code",
                "severity",
                "message",
            ],
            "page",
            "next-page",
            "limit",
            "offset",
            "has-more",
            "total",
            "toast",
            "data" => [
                'rooms'
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4071
     */
    public function testGetOwnedKosWithExcludeUnverified()
    {
        $user = factory(User::class)->create();
        /** @var Collection $rooms */
        $rooms = factory(Room::class)->times(4)->create();
        // Make first 2 rejected
        $rooms->slice(0, 2)->each(function (Room $room) use ($user) {
            factory(RoomOwner::class)->create([
                'designer_id' => $room,
                'user_id' => $user,
                'status' => collect(array_diff(RoomOwner::ROOM_STATUSES_ALL, [
                    RoomOwner::ROOM_VERIFY_STATUS,
                    RoomOwner::ROOM_ADD_STATUS,
                ]))->random(),
            ]);
        });
        // Make the rest of it verified
        $rooms->slice(2, null)->each(function (Room $room) use ($user) {
            factory(RoomOwner::class)->create([
                'designer_id' => $room,
                'user_id' => $user,
                'status' => collect([
                    RoomOwner::ROOM_VERIFY_STATUS,
                    RoomOwner::ROOM_ADD_STATUS,
                ])->random(),
            ]);
        });

        $query = http_build_query([
            'limit' => 10,
            'offset' => 0,
            'exclude_unverified_flag' => 1,
        ]);
        $response = $this->actingAs($user)->getJson(self::OWNER_KOST_LIST_API . "?" . $query);

        $expectedRoomCount = 2; // Since only 2 that verified.

        $response->assertOk();
        $this->assertEquals($expectedRoomCount, count($response->json()['data']['rooms']));
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

}