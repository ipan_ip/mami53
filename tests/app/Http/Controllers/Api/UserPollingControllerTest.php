<?php

namespace App\Http\Controller\Api;

use App\Entities\Polling\Polling;
use App\Services\Polling\PollingService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UserPollingControllerTest extends MamiKosTestCase
{
    private const URL_DETAIL_USER_POLLING = 'garuda/user/polling/detail/%s';
    private const URL_SUBMIT_USER_POLLING = 'garuda/user/polling/submit/%s';

    private $service;

    protected function setUp() : void
    {
        parent::setUp();
        
        $this->service = $this->createMock(PollingService::class);
        $this->app->instance(PollingService::class, $this->service);
    }

    public function testPollingDetail_PollingNotFound_ShouldFail()
    {
        $user = factory(User::class)->state('owner')->create();

        $polling = factory(Polling::class)->create(['is_active' => true]);

        $this->service->expects($this->once())
            ->method('getPollingDetail')
            ->willReturn(NULL);
        
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', sprintf(self::URL_DETAIL_USER_POLLING, 'abcdef'), []);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code'          => 404
            ]
        ]);
    }

    public function testPollingDetail_HasSubmitted_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $polling = factory(Polling::class)->create(['is_active' => true]);

        $this->service->expects($this->once())
            ->method('hasSubmittedPolling')
            ->willReturn(true);
        
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', sprintf(self::URL_DETAIL_USER_POLLING, 'abcdef'), []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code'          => 200
            ],
            'data' => [
                'has_submitted' => true
            ]
        ]);
    }

    public function testPollingDetail_HasntSubmitted_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $polling = factory(Polling::class)->create(['is_active' => true]);

        $this->service->expects($this->once())
            ->method('hasSubmittedPolling')
            ->willReturn(false);

        $this->service->expects($this->once())
            ->method('getPollingDetail')
            ->willReturn([
                'data' => [
                    'id'            => 101,
                    'key'           => 'exit_goldplus',
                    'description'   => 'User polling when they unsubscribe goldplus'
                ]
            ]);
        
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', sprintf(self::URL_DETAIL_USER_POLLING, 'abcdef'), []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code'          => 200
            ],
            'data' => [
                'id'            => 101,
                'key'           => 'exit_goldplus',
                'description'   => 'User polling when they unsubscribe goldplus',
                'has_submitted' => false
            ]
        ]);
    }

    public function testSubmitPolling_PollingNotFound_ShouldFail()
    {
        $user = factory(User::class)->state('owner')->create();

        $this->service->expects($this->once())
            ->method('submitPolling')
            ->willReturn([
                'status'        => false,
                'message'       => 'Polling does not exist',
                'action_code'   => 'error'
            ]);
        
        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', sprintf(self::URL_SUBMIT_USER_POLLING, 'abcdef'), []);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code'          => 200,
                'message' => 'Polling does not exist'
            ],
            'action_code' => 'error'
        ]);
    }

    public function testSubmitPolling_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $this->service->expects($this->once())
            ->method('submitPolling')
            ->willReturn([
                'status'        => true,
                'message'       => 'User Polling has been successfully submitted',
                'action_code'   => 'exit'
            ]);
        
        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', sprintf(self::URL_SUBMIT_USER_POLLING, 'abcdef'), []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code'          => 200,
                'message' => 'User Polling has been successfully submitted'
            ],
            'action_code' => 'exit'
        ]);
    }
}