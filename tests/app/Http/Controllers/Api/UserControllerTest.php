<?php

namespace App\Http\Controllers\Api;

use App\Entities\Booking\BookingUser;
use App\Entities\User\Notification;
use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\UserNotificationCounter;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class UserControllerTest extends MamiKosTestCase
{
    use WithFaker;

    private const CHANGE_CHAT_NAME_URL = 'garuda/owner/chat/change_name';
    private const GET_SETTINGS_URL = 'garuda/setting/notif';
    private const CHANGE_SETTINGS_URL = 'garuda/setting/notif';
    private const NOTIFICATION_COUNTER_URL = 'api/v1/notification/counter';
    private const NOTIFICATION_READ_URL = 'api/v1/notification/read';
    private const PHONE_CHECK_URL = 'api/v1/phone/check';
    private const EDIT_NAME_URL = 'api/v1/owner/edit/name';

    public function testChangeUserChatNameAssertTrue()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_CHAT_NAME_URL, [
            'name' => 'test test'
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    public function testChangeUserChatNameWithoutAuthenticationAssertFalse()
    {
        $response = $this->setWebApiCookie()->json('POST', self::CHANGE_CHAT_NAME_URL, [
            'name' => 'test test'
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => 'User tidak ditemukan'
            ]
        ]);
    }

    public function testGetNotifAssertTrue()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::GET_SETTINGS_URL);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'user' => [
                'id' => $user->id,
                'phone' => $user->phone_number,
                'email' => $user->email,
                'username' => $user->name,
                'gender' => $user->gender,
            ]
        ]);
    }

    public function testGetNotifWithoutAuthenticationAssertFalse()
    {
        $response = $this->setWebApiCookie()->json('GET', self::GET_SETTINGS_URL);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => 'User not found'
        ]);
    }

    public function testChangeSettingsWithUserAuthenticationAssertTrue()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'alarm_email' => 'true',
                'app_chat' => 'true'
            ]
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'settings' => [
                [
                    'key' => 'alarm_email',
                    'value' => 'true'
                ],
                [
                    'key' => 'app_chat',
                    'value' => 'true'
                ]
            ]
        ]);
    }

    public function testChangeSettingsWithUserAuthenticationAssertDatabaseChanged()
    {
        $user = factory(User::class)->create();

        $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'alarm_email' => 'true',
                'app_chat' => 'true'
            ]
        ]);

        $this->assertDatabaseHas('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'alarm_email',
            'value' => 'true',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'app_chat',
            'value' => 'true',
            'deleted_at' => null,
        ]);
    }

    public function testChangeSettingsWithOwnerAuthenticationAssertTrue()
    {
        $user = factory(User::class)->states('owner')->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'update_kost' => 'true',
                'alarm_email' => 'true',
                'app_chat' => 'true',
            ]
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'settings' => [
                [
                    'key' => 'alarm_email',
                    'value' => 'true'
                ],
                [
                    'key' => 'app_chat',
                    'value' => 'true'
                ],
                [
                    'key' => 'update_kost',
                    'value' => 'true'
                ],
            ]
        ]);
    }

    public function testChangeSettingsWithOwnerAuthenticationAssertDatabaseChanged()
    {
        $user = factory(User::class)->states('owner')->create();

        $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'update_kost' => 'true',
                'alarm_email' => 'true',
                'app_chat' => 'true',
            ]
        ]);

        $this->assertDatabaseHas('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'alarm_email',
            'value' => 'true',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'app_chat',
            'value' => 'true',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'update_kost',
            'value' => 'true'
        ]);
    }

    public function testChangeSettingsWithInvalidSettingAssertTrue()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'update_kost' => 'true',
            ]
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'settings' => [
                [
                    'key' => 'alarm_email',
                    'value' => 'true'
                ],
                [
                    'key' => 'app_chat',
                    'value' => 'true'
                ]
            ]
        ]);
        $response->assertJsonMissing([
            'settings' => [[
                'key' => 'update_kost',
                'value' => 'true'
            ]]
        ]);
    }

    public function testChangeSettingsWithInvalidSettingAssertDatabaseNotChanged()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'update_kost' => 'true',
            ]
        ]);

        $this->assertDatabaseMissing('setting_notification', [
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'update_kost',
            'value' => 'true',
            'deleted_at' => null,
        ]);
    }

    public function testChangeSettingsWithMalformedParamAssertFalse()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', self::CHANGE_SETTINGS_URL, [
            'update_kost' => 'true',
            'alarm_email' => 'true',
            'app_chat' => 'true',
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => 'Format request tidak tepat',
        ]);
    }

    public function testChangeSettingsWithoutAuthenticationAssertFalse()
    {
        $response = $this->setWebApiCookie()->json('POST', self::CHANGE_SETTINGS_URL, [
            'settings' => [
                'update_kost' => 'true',
                'alarm_email' => 'true',
                'app_chat' => 'true',
            ]
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'message' => 'User not found',
        ]);
    }

    public function testGetCounterAssertOk()
    {
        $user = $this->createDummyUser();

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertOk();
    }

    public function testGetCounterAssertTrue()
    {
        $user = $this->createDummyUser();

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertJson(['status' => true]);
    }

    public function testGetCounterAssertNotificationCount()
    {
        $user = $this->createDummyUser();
        $user->notification()->save(
            factory(Notification::class)->make()
        );

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertJson([
            'notification_count' => 1
        ]);
    }

    public function testGetCounterAssertCounterOwner()
    {
        $user = $this->createDummyUser();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'chat',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'telp',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'update_kost',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertJson([
            'counter' => [
                'owner' => 8
            ]
        ]);
    }

    public function testGetCounterAssertCounterRecommendation()
    {
        $user = $this->createDummyUser();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertJson([
            'counter' => [
                'recommendation' => 2
            ]
        ]);
    }

    public function testGetCounterAssertBookingTotal()
    {
        $user = $this->createDummyUser();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);

        factory(BookingUser::class, 2)->create([
            'is_open' => false,
            'designer_id' => $room->song_id
        ]);

        $response = $this->actingAs($user)->json('GET', self::NOTIFICATION_COUNTER_URL, $this->accessToken());
        $response->assertJson(['booking_total' => 2]);
    }

    public function testReadCounterAssertTrue()
    {
        $user = $this->createDummyUser();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'chat',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'telp',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'update_kost',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $response = $this->actingAs($user)->json('POST', self::NOTIFICATION_READ_URL, $this->accessToken());
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'counter' => [
                'recommendation' => 0,
                'owner' => 0
            ]
        ]);
    }

    public function testPhoneNumberCheckWithUnregisteredPhoneAssertUnregistered()
    {
        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            ($this->accessToken() + ['phone_number' => '0811111111'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'phone_registered' => false
        ]);
    }

    public function testPhoneNumberCheckWithRegisteredPhoneAssertFalse()
    {
        factory(User::class)->create(['phone_number' => '0811111111']);

        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            ($this->accessToken() + ['phone_number' => '0811111111'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'phone_registered' => false
        ]);
    }

    public function testPhoneNumberCheckWithTooShortPhoneAssertFalse()
    {
        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            ($this->accessToken() + ['phone_number' => '0811'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'messages' => [
                'Format Nomor HP tidak valid (08xxxxxxxx)'
            ]
        ]);
    }

    public function testPhoneNumberCheckWithTooLongPhoneAssertFalse()
    {
        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            ($this->accessToken() + ['phone_number' => '0811111111111111'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'messages' => [
                'Format Nomor HP tidak valid (08xxxxxxxx)'
            ]
        ]);
    }

    public function testPhoneNumberCheckWithMalformedPhoneAssertFalse()
    {
        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            ($this->accessToken() + ['phone_number' => '+628111111'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'messages' => [
                'Format Nomor HP tidak valid (08xxxxxxxx)'
            ]
        ]);
    }

    public function testPhoneNumberCheckWithoutPhoneNumberAssertFalse()
    {
        $response = $this->json(
            'POST',
            self::PHONE_CHECK_URL,
            $this->accessToken()
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'messages' => [
                'Nomor HP harus diisi'
            ]
        ]);
    }

    public function testEditNameAssertTrue()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $response = $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => 'test tust'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
    }

    public function testEditNameAssertDatabaseChanged()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => 'test tust'])
        );
        $this->assertDatabaseHas('user', ['id' => $user->id, 'name' => 'test tust']);
    }

    public function testEditNameAssertWithTooShortNameAssertFalse()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $response = $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => 'te'])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.name.min', ['min' => 3])]
            ]
        ]);
    }

    public function testEditNameAssertWithTooShortNameAssertDatabaseNotChanged()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => 'te'])
        );
        $this->assertDatabaseHas('user', ['id' => $user->id, 'name' => $user->name]);
        $this->assertDatabaseMissing('user', ['id' => $user->id, 'name' => ['name' => 'te']]);
    }

    public function testEditNameAssertWithTooLongNameAssertFalse()
    {
        $name = $this->faker->regexify('[A-Za-z]{256}');
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $response = $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => $name])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.name.max', ['max' => 255])]
            ]
        ]);
    }

    public function testEditNameAssertWithTooLongNameAssertDatabaseNotChanged()
    {
        $name = $this->faker->regexify('[A-Za-z]{256}');
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => $name])
        );
        $this->assertDatabaseHas('user', ['id' => $user->id, 'name' => $user->name]);
        $this->assertDatabaseMissing('user', ['id' => $user->id, 'name' => $name]);
    }

    public function testEditNameAssertWithInvalidNameAssertFalse()
    {
        $name = $this->faker->regexify('[0-9]{100}');
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $response = $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => $name])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.name.alpha')]
            ]
        ]);
    }

    public function testEditNameAssertWithInvalidNameAssertDatabaseNotChanged()
    {
        $name = $this->faker->regexify('[0-9]{100}');
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            ($this->accessToken() + ['name' => $name])
        );
        $this->assertDatabaseHas('user', ['id' => $user->id, 'name' => $user->name]);
        $this->assertDatabaseMissing('user', ['id' => $user->id, 'name' => $name]);
    }

    public function testEditNameAssertWithoutNameAssertFalse()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $response = $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            $this->accessToken()
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => [__('api.input.name.required')]
            ]
        ]);
    }

    public function testEditNameAssertWithoutNameAssertDatabaseNotChanged()
    {
        $user = $this->createDummyUser();
        $user->name = 'test test';
        $user->save();
        $this->actingAs($user)->json(
            'POST',
            self::EDIT_NAME_URL,
            $this->accessToken()
        );
        $this->assertDatabaseHas('user', ['id' => $user->id, 'name' => $user->name]);
    }

    /**
     *  Create dummy user to perform request as
     *
     *  @return User
     */
    private function createDummyUser(): User
    {
        $user = factory(User::class)->create(['last_login' => 5]);
        $user->devices()->save(
            factory(UserDevice::class)->state('login')->make(['id' => 5])
        );

        return $user;
    }
}
