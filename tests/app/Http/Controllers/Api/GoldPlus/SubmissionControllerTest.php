<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\Consultant\PotentialOwner;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use GoldplusPackageSeeder;
use App\Entities\Device\UserDevice;
use App\Entities\GoldPlus\Submission;
use App\Entities\Level\PropertyLevel;
use App\Entities\Level\GoldplusLevel;

class SubmissionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed([GoldplusPackageSeeder::class]);
        $this->setupGoldplusPropertyLevel();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    private function setupGoldplusPropertyLevel()
    {
        factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1_PROMO]);
        factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_2_PROMO]);
    }

    private function setUser(User $user)
    {
        app()->user = $user;
    }

    private function callStore(array $param)
    {
        return $this->postJson('/api/v1/goldplus/submissions', $param);
    }

    private function createBookingRoom(
        User $owner,
        bool $isBooking = true,
        ?string $bbkStatus = BookingOwnerRequest::BOOKING_APPROVE
    ) {
        /** @var Room */
        $room = factory(Room::class)->create([
            'is_active' => true,
            'is_booking' => $isBooking,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $owner,
        ]);
        if (!is_null($bbkStatus)) {
            factory(BookingOwnerRequest::class)->create([
                'designer_id' => $room,
                'user_id' => $owner,
                'status' => $bbkStatus,
            ]);
        }
        return $room;
    }

    /**
     * @group UG
     * @group UG-4279
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGetSubmissionOnboardingContent()
    {
        $owner = factory(User::class)->states('owner')->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();

        $response = $this->actingAs($owner)
            ->json('GET', $this->withAccessToken('/api/v1/goldplus/submissions/onboarding')
        );

        $response->assertOk()
            ->assertJson(['title' => __('api.goldplus.onboarding.title')])
            ->assertJson(['data' => __('api.goldplus.onboarding.items')]);
    }

    /**
     * @group UG
     * @group UG-4281
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGetSubmissionTncContent()
    {
        $response = $this->getJson('/api/v1/goldplus/submissions/tnc');
        $response->assertOk();
    }


    /**
     * @group UG
     * @group UG-4066
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGp1SubmissionWithAllPropertyBbkShouldFlagOwnerAsBbk()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $room = $this->createBookingRoom($user);
        $room2 = $this->createBookingRoom($user);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP1,
            'listing_ids' => [],
        ]);
        $response->assertJson([
            'status' => true,
            'submission' => [
                'listing_ids' => [
                    $room->id,
                    $room2->id,
                ],
            ],
        ]);
        /** @var PotentialOwner */
        $potentialOwner = PotentialOwner::whereUserId($user->id)->first();
        $this->assertNotNull($potentialOwner);
        $this->assertEquals(PotentialOwner::BBK_STATUS_BBK, $potentialOwner->bbk_status);
    }

    /**
     * @group UG
     * @group UG-4066
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGp1SubmissionWithAllPropertyNonBbkButNotRequestedShouldFlagOwnerAsNonBbk()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $room = $this->createBookingRoom($user, false, null);
        $room2 = $this->createBookingRoom($user, false, null);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP1,
            'listing_ids' => [],
        ]);
        $response->assertJson([
            'status' => true,
            'submission' => [
                'listing_ids' => [
                    $room->id,
                    $room2->id,
                ],
            ],
        ]);
        /** @var PotentialOwner */
        $potentialOwner = PotentialOwner::whereUserId($user->id)->first();
        $this->assertNotNull($potentialOwner);
        $this->assertEquals(PotentialOwner::BBK_STATUS_NON_BBK, $potentialOwner->bbk_status);
    }

    /**
     * @group UG
     * @group UG-4066
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGp1SubmissionWithAllPropertyRejectedBbkShouldFlagOwnerAsNonBbk()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $room = $this->createBookingRoom($user, false, BookingOwnerRequest::BOOKING_REJECT);
        $room2 = $this->createBookingRoom($user, false, BookingOwnerRequest::BOOKING_REJECT);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP1,
            'listing_ids' => [],
        ]);
        $response->assertJson([
            'status' => true,
            'submission' => [
                'listing_ids' => [
                    $room->id,
                    $room2->id,
                ],
            ],
        ]);
        /** @var PotentialOwner */
        $potentialOwner = PotentialOwner::whereUserId($user->id)->first();
        $this->assertNotNull($potentialOwner);
        $this->assertEquals(PotentialOwner::BBK_STATUS_NON_BBK, $potentialOwner->bbk_status);
    }

    /**
     * @group UG
     * @group UG-4066
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGp1SubmissionWithoutOwnedPropertyShouldFlagOwnerAsNonBbk()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP1,
            'listing_ids' => [],
        ]);
        $response->assertJson([
            'status' => true,
            'submission' => [
                'listing_ids' => [],
            ],
        ]);
        /** @var PotentialOwner */
        $potentialOwner = PotentialOwner::whereUserId($user->id)->first();
        $this->assertNotNull($potentialOwner);
        $this->assertEquals(PotentialOwner::BBK_STATUS_NON_BBK, $potentialOwner->bbk_status);
    }

    /**
     * @group UG
     * @group UG-4066
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testGp3SubmissionWithPartialPropertyPotentialOwnerTotalPropertyShouldIncludeAllProperty()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $room = $this->createBookingRoom($user);
        $this->createBookingRoom($user);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP3,
            'listing_ids' => [
                $room->song_id,
            ],
        ]);
        $response->assertJson([
            'status' => true,
        ]);
        /** @var PotentialOwner */
        $potentialOwner = PotentialOwner::whereUserId($user->id)->first();
        $this->assertNotNull($potentialOwner);
        $this->assertEquals(2, $potentialOwner->total_property);
    }

    /**
     * @group UG
     * @group UG-4803
     * @group App\Http\Controllers\Api\GoldPlus\SubmissionController
     */
    public function testSubmissionWithAlreadyHasActiveSubmissionShouldReturnFalse()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $room = $this->createBookingRoom($user);
        
        factory(Submission::class)->create(['user_id' => $user->id]);
        $response = $this->callStore([
            'code' => PotentialProperty::PRODUCT_GP3,
            'listing_ids' => [
                $room->song_id,
            ],
        ]);

        $response->assertJson([
            'status' => false,
            'meta' => [
                'message' => __('goldplus.error.submission.has_active_submission')
            ]
        ]);
    }
}
