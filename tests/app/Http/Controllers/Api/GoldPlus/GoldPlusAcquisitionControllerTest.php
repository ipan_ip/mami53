<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevel;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\GoldPlus\Enums\KostStatus;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GoldPlusAcquisitionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const URL_LIST_GOLDPLUS_ROOMS = '/api/v1/room/goldplus/list-kost';
    private const URL_GOLDPLUS_FILTERS = '/api/v1/room/goldplus/list-kost/filters';
    private const JSON_STRUCTURE_GOLDPLUS_ROOMS = [
        'status',
        'data' => [
            '*' => [
                'id',
                'room_title',
                'address',
                'photo' => [
                    'real',
                    'small',
                    'medium',
                    'large',
                ],
                'room_count',
                'membership_status' => [
                    'key',
                    'value'
                ],
                'gp_status' => [
                    'key',
                    'value',
                ],
                'new_gp'
            ],
        ],
        'has_more',
    ];

    private $oldGP1, $oldGP2, $oldGP3, $oldGP4;
    private $newGP1, $newGP2, $newGP3, $newGP4;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupGoldplusKostLevel();
    }

    private function setupGoldplusKostLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        //old oldGP
        [$this->oldGP1, $this->oldGP2, $this->oldGP3, $this->oldGP4] = factory(KostLevel::class)->times(4)->create();
        Config::set('kostlevel.id.goldplus1', $this->oldGP1->id);
        Config::set('kostlevel.id.goldplus2', $this->oldGP2->id);
        Config::set('kostlevel.id.goldplus3', $this->oldGP3->id);
        Config::set('kostlevel.id.goldplus4', $this->oldGP4->id);

        [$this->newGP1, $this->newGP2, $this->newGP3, $this->newGP4] = factory(KostLevel::class)->times(4)->create();
        Config::set('kostlevel.id.new_goldplus1', $this->newGP1->id);
        Config::set('kostlevel.id.new_goldplus2', $this->newGP2->id);
        Config::set('kostlevel.id.new_goldplus3', $this->newGP3->id);

        Config::set('kostlevel.ids.goldplus1', "{$this->oldGP1->id},{$this->newGP1->id}");
        Config::set('kostlevel.ids.goldplus2', "{$this->oldGP2->id},{$this->newGP2->id}");
        Config::set('kostlevel.ids.goldplus3', "{$this->oldGP3->id},{$this->newGP3->id}");
        Config::set('kostlevel.ids.goldplus4', "{$this->oldGP4->id},{$this->newGP4->id}");
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function actingAs($user, $driver = null)
    {
        $this->app->user = $user;
        if (is_null($user)) {
            return $this;
        }
        return parent::actingAs($user, $driver);
    }

    private function createGoldPlusRoom(User $user, int $gpLevelId): Room
    {
        /** @var Room */
        $room = factory(Room::class)->create();
        $room->changeLevel($gpLevelId);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified'
        ]);
        return $room;
    }

    private function createRoom(User $user): Room
    {
        /** @var Room */
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'add'
        ]);
        return $room;
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetGoldplusFilterOptions()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_GOLDPLUS_FILTERS);
        $response->assertOk();
        $this->assertCount(4, $response->json()['data']);
        $response->assertJson([
            'data' => [
                array_merge(KostStatus::ALL, ['is_new_badge' => false]),
            ],
        ]);
        foreach (array_slice($response->json()['data'], 1) as $key => $value) {
            // For each status filters (excluding all). Key should be in form of integer > 0. Name should not be empty
            $this->assertTrue($value['key'] > 0);
            $this->assertNotEmpty($value['value']);
        }
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetGoldplusFilterOptionsWithUnpaidKost()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = $this->createRoom($user);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
        ]);

        $response = $this->getJson(self::URL_GOLDPLUS_FILTERS);
        $response->assertOk();
        $response->assertJson([
            'data' => [
                array_merge(KostStatus::ALL, ['is_new_badge' => false]),
                array_merge(KostStatus::ACTIVE, ['is_new_badge' => false]),
                array_merge(KostStatus::UNPAID, ['is_new_badge' => true]),
                array_merge(KostStatus::ON_REVIEW, ['is_new_badge' => false]),
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetGoldplusFilterOptionsWithoutUnpaidKost()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_GOLDPLUS_FILTERS);
        $response->assertOk();
        $response->assertJson([
            'data' => [
                array_merge(KostStatus::ALL, ['is_new_badge' => false]),
                array_merge(KostStatus::ACTIVE, ['is_new_badge' => false]),
                array_merge(KostStatus::UNPAID, ['is_new_badge' => false]),
                array_merge(KostStatus::ON_REVIEW, ['is_new_badge' => false]),
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusEmptyRoom()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 0
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [],
            'has_more' => false,
            'button' => true
        ]);
    }
    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusMissingParam()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
        ]));
        $response->assertStatus(400);
        $response->assertJson([
            'status' => false,
        ]);
        $response->assertJsonStructure([
            'messages' => [
                'offset',
                'status'
            ],
        ]);
    }
    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusMissingUser()
    {
        $this->actingAs(null);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
        ]));
        $response->assertStatus(401);
        $response->assertJson([
            'status' => false,
        ]);
    }
    
    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusWithOldGP()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $gp1Room = $this->createGoldPlusRoom($user, $this->oldGP1->id);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 0
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $gp1Room->song_id,
                    'gp_status' => [
                        'value' => 'Goldplus 1'
                    ],
                    'new_gp' => false
                ]
            ],
            'button' => true,
            'has_more' => false,
        ]);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusWithNewGP()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $gp1Room = $this->createGoldPlusRoom($user, $this->newGP1->id);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 0
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $gp1Room->song_id,
                    'gp_status' => [
                        'value' => 'Goldplus 1'
                    ],
                    'new_gp' => true
                ]
            ],
            'button' => false,
            'has_more' => false,
        ]);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusWithOnReview()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $room = $this->createRoom($user);
        $potProperty = factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => 'new',
            'potential_gp_total_room' => 20
        ]);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 0
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $room->song_id,
                    'gp_status' => [
                        'value' => 'Goldplus 1'
                    ],
                    'new_gp' => true
                ]
            ],
            'button' => false,
            'has_more' => false,
        ]);
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusWithUnpaid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $room = $this->createRoom($user);
        $potProperty = factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp1',
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
            'potential_gp_total_room' => 20
        ]);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 3
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $room->song_id,
                    'gp_status' => [
                        'value' => 'Goldplus 1'
                    ],
                    'new_gp' => true
                ]
            ],
            'button' => false,
            'has_more' => false,
        ]);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusFilterAllNonEmptyRoom()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->newGP1->id);
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $this->createGoldPlusRoom($user, $this->oldGP3->id);
        $this->createGoldPlusRoom($user, $this->oldGP4->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'button' => false,
            'has_more' => false,
        ]);
        $this->assertCount(4, $response->json()['data']);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusFilteredNonEmptyResult()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = $this->createGoldPlusRoom($user, $this->oldGP3->id);
        $otherRoom = $this->createGoldPlusRoom($user, $this->newGP3->id);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'offered_product' => 'gp3',
            'followup_status' => 'new',
            'potential_gp_total_room' => 20
        ]);

        $statusFilterId = $this->faker()->randomElement([1,2]);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'status' => $statusFilterId,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => false,
        ]);
        $this->assertCount(1, $response->json()['data']);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusHasMore()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->newGP1->id);
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $this->createGoldPlusRoom($user, $this->newGP3->id);
        $this->createGoldPlusRoom($user, $this->newGP4->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 2,
            'offset' => 0,
            'status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => true,
            'button' => false
        ]);
        $this->assertCount(2, $response->json()['data']);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusOffset()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->newGP1->id);
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $this->createGoldPlusRoom($user, $this->newGP3->id);
        $this->createGoldPlusRoom($user, $this->oldGP2->id);
        $this->createGoldPlusRoom($user, $this->oldGP3->id);
        $this->createGoldPlusRoom($user, $this->oldGP4->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 3,
            'offset' => 3,
            'status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => false,
            'button' => false
        ]);
        $this->assertCount(3, $response->json()['data']);
    }

    /**
     * @group UG
     * @group UG-4052
     * @group App\Http\Controllers\Api\GoldplusAcquisitionController
     */
    public function testGetListKostsGoldplusHasPhoto()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = $this->createGoldPlusRoom($user, $this->newGP1->id);
        /** @var Media */
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 2,
            'offset' => 0,
            'status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'photo' => $photo->getMediaUrl(),
                ]
            ]
        ]);
    }
}
