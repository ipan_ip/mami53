<?php

namespace App\Http\Controllers\Api\GoldPlus;

use Mockery;
use App\User;
use phpmock\MockBuilder;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevel;
use App\Entities\Device\UserDevice;
use App\Entities\GoldPlus\Submission;
use Illuminate\Support\Facades\Config;
use App\Entities\Consultant\PotentialProperty;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class WidgetControllerTest extends MamiKosTestCase 
{
    use WithoutMiddleware;
    
    private const GET_GOLDPLUS_WIDGET = 'api/v1/room/goldplus/widget';
    private const GET_GOLDPLUS_WIDGET_ROOM = 'api/v1/room/goldplus/widget-room';

    private $gp1;
    private $gp2;
    private $gp3;
    private $gp4;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setupGoldplusKostLevel();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group App/Http/Controllers/Api/GoldPlus/WidgetController
     */
    public function testGoldPlusWidget()
    {
        $owner = factory(User::class)->states('owner')->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $owner->id
        ]);
        $owner->last_login = $device->id;
        $owner->save();
        
        $response = $this->actingAs($owner)
            ->json('GET', $this->withAccessToken(self::GET_GOLDPLUS_WIDGET)
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data' => [
                'date',
                'statistic' => [
                    '*' => [
                        'label',
                        'value',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-3979
     * @group App/Http/Controllers/Api/GoldPlus/WidgetController
     */
    public function testGoldPlusWidgetExpectRunTimeException()
    {   
        $response = $this->json('GET', $this->withAccessToken(self::GET_GOLDPLUS_WIDGET_ROOM))->json();
        $this->assertEquals('RuntimeException', $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.runtime_exception.user_id_was_null'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-3979
     * @group App/Http/Controllers/Api/GoldPlus/WidgetController
     */
    public function testGoldPlusWidgetStructure()
    {
        $owner = factory(User::class)->states('owner')->create();
        
        $response = $this->actingAs($owner)
            ->json('GET', $this->withAccessToken(self::GET_GOLDPLUS_WIDGET_ROOM)
        );
        $response->assertOk();
        $response->assertJson([
            'status' => true
        ]);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data' => [
                '*' => [
                    'label',
                    'value',
                ],
            ],
        ]);
    }
    
    /**
     * @group UG
     * @group UG-3979
     * @group App/Http/Controllers/Api/GoldPlus/WidgetController
     */
    public function testGoldPlusWidgetShouldReturnCorrectData()
    {
        $owner = factory(User::class)->states('owner')->create();
        
        $normalRoom1 = $this->createRoom($owner);
        $normalRoom2 = $this->createRoom($owner);
        $goldplus1 = $this->createGoldPlusRoom($owner, $this->gp1->id);

        $inReview1 = $this->createGoldPusOnReviewRoom($normalRoom1);
        $inReview2 = $this->createGoldPusOnReviewRoom($normalRoom2);
        $inReview2 = $this->createGoldPusOnReviewRoom($goldplus1);
        $goldplus2 = $this->createGoldPlusRoom($owner, $this->gp2->id);
        $goldplus3 = $this->createGoldPlusRoom($owner, $this->gp3->id);
        $goldplus4 = $this->createGoldPlusRoom($owner, $this->gp4->id);
        
        $response = $this->actingAs($owner)
            ->json('GET', $this->withAccessToken(self::GET_GOLDPLUS_WIDGET_ROOM)
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'label' => __('api.goldplus.widget.label.active-goldplus'),
                    'value' => 3,
                    'button_visible' => false
                ],
                [
                    'label' => __('api.goldplus.widget.label.in-review-goldplus'),
                    'value' => 3,
                    'button_visible' => true
                ],
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-3979
     * @group App/Http/Controllers/Api/GoldPlus/WidgetController
     */
    public function testGoldPlusWidgetWithoutInReview()
    {
        $owner = factory(User::class)->states('owner')->create();
        
        $normalRoom = $this->createRoom($owner);
        $goldplus1 = $this->createGoldPlusRoom($owner, $this->gp1->id);
        $goldplus2 = $this->createGoldPlusRoom($owner, $this->gp2->id);
        
        $response = $this->actingAs($owner)
            ->json('GET', $this->withAccessToken(self::GET_GOLDPLUS_WIDGET_ROOM)
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'label' => __('api.goldplus.widget.label.active-goldplus'),
                    'value' => 2,
                    'button_visible' => false
                ]
            ],
        ]);
        $response->assertJsonCount(1, 'data');
        $response->assertJsonMissing([
            'data' => [
                [
                    'label' => __('api.goldplus.widget.label.in-review-goldplus')
                ]
            ],
        ]);
    }
    
    private function setupGoldplusKostLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $this->gp1 = factory(KostLevel::class)->state('gp1')->create();
        $this->gp2 = factory(KostLevel::class)->state('gp2')->create();
        $this->gp3 = factory(KostLevel::class)->state('gp3')->create();
        $this->gp4 = factory(KostLevel::class)->state('gp4')->create();
    }

    private function createRoom(User $user): Room {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified'
        ]);
        return $room;
    }

    private function createGoldPlusRoom(User $user, int $gpLevelId): Room
    {
        /** @var Room */
        $room = factory(Room::class)->create();
        $room->changeLevel($gpLevelId);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified'
        ]);
        return $room;
    }

    private function createGoldPusOnReviewRoom(Room $room)
    {
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => 'new',
        ]);
    }
}
