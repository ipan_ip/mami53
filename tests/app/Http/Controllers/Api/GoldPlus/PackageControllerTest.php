<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use GoldplusPackageSeeder;

class PackageControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const URL_GOLDPLUS_PACKAGE = '/api/v1/goldplus/packages';
    private const JSON_STRUCTURE_GOLDPLUS_PACKAGE = [
        'status',
        'packages' => [
            '*' => [
                'id',
                'code',
                'name',
                'description',
                'price',
                'charging',
                'price_description',
                'periodicity',
                'unit_type',
                'detail_package_html',
            ],
        ],
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->seed([GoldplusPackageSeeder::class]);
    }

    /**
     * @group UG
     * @group UG-4341
     * @group App\Http\Controllers\Api\GoldPlus\PackageController
     */
    public function testGetGoldplusPackage()
    {
        $response = $this->getJson(self::URL_GOLDPLUS_PACKAGE);
        $response->assertOk()
            ->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_PACKAGE);
    }
}
