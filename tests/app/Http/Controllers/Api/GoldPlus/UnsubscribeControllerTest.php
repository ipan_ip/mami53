<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\BaselineType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

class UnsubscribeControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const GET_UNSUBSCRIBE_GOLDPLUS_STATISTIC_API = 'api/v1/goldplus/unsubscribe/statistic';

    protected function setUp() : void
    {
        parent::setUp();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group LOY
     * @group LOY-2528
     * @group App/Http/Controllers/Api/GoldPlus/UnsubscribeController
     */
    public function testGetStatisticWithoutGpStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'            => $designerId,
            'song_id'       => $songId,
            'name'          => $name,
            'gender'        => $gender,
            'address'       => $address,
            'area'          => $area,
            'is_active'     => $isActive,
            'price_monthly' => 100000
        ]);

        $response = $this->json('GET', self::GET_UNSUBSCRIBE_GOLDPLUS_STATISTIC_API, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => null
        ]);
    }

    /**
     * @group LOY
     * @group LOY-2528
     * @group App/Http/Controllers/Api/GoldPlus/UnsubscribeController
     */
    public function testGetStatisticWithExpensiveRoomShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'            => $designerId,
            'song_id'       => $songId,
            'name'          => $name,
            'gender'        => $gender,
            'address'       => $address,
            'area'          => $area,
            'is_active'     => $isActive,
            'price_monthly' => 100000
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id
        ]);

        $response = $this->json('GET', self::GET_UNSUBSCRIBE_GOLDPLUS_STATISTIC_API, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'price_title'   => [],
                'gp_statistic'  => []
            ]
        ]);
        $response->assertJsonFragment([
            'price_title' => [
                'currency_symbol' => 'Rp',
                'price' => 100000,
                'rent_type_unit' => 'bulan',
            ]
        ]);
    }

    /**
     * @group LOY
     * @group LOY-2528
     * @group App/Http/Controllers/Api/GoldPlus/UnsubscribeController
     */
    public function testGetStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'            => $designerId,
            'song_id'       => $songId,
            'name'          => $name,
            'gender'        => $gender,
            'address'       => $address,
            'area'          => $area,
            'is_active'     => $isActive,
            'price_monthly' => 100000
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $favGrowthValue = mt_rand(1, 1000);
        $favGpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $favGrowthValue,
                    GoldplusStatisticReportType::LAST_THIRTY_DAYS
                )
        );

        (int) $chatGrowthValue = mt_rand(1, 1000);
        $favGpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $chatGrowthValue,
                    GoldplusStatisticReportType::LAST_THIRTY_DAYS
                )
        );

        $response = $this->json('GET', self::GET_UNSUBSCRIBE_GOLDPLUS_STATISTIC_API, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'price_title'   => [],
                'gp_statistic'  => []
            ]
        ]);
        $response->assertJsonFragment([
            'price_title' => [
                'currency_symbol' => 'Rp',
                'price' => 100000,
                'rent_type_unit' => 'bulan',
            ]
        ]);
        $response->assertJsonFragment([
            GoldplusStatisticType::FAVORITE => [
                'label' => GoldplusStatisticType::FAVORITE_LABEL,
                'growth_type' => GrowthType::UP,
                'value' => $favGrowthValue.'%',
            ]
        ]);
        $response->assertJsonFragment([
            GoldplusStatisticType::CHAT => [
                'label' => GoldplusStatisticType::CHAT_LABEL,
                'growth_type' => GrowthType::UP,
                'value' => $chatGrowthValue.'%',
            ]
        ]);
    }

    /**
     * Payload for test
     * 
     * @return array
     */
    private function payloadForStatisticType(
        int $userId, 
        Room $room, 
        string $availableReportType,
        string $gpCreatedAt,
        string $filterType,
        int $growthValue,
        string $reportType = GoldplusStatisticReportType::YESTERDAY
        ): array
    {
        $totalLastTwoMonthsVisit = mt_rand(1, 1000);
        $growthType     = GrowthType::UP;
        $baselineType   = BaselineType::GROWTH;

        return [
            'room'  => [
                'id'        => $room->id,
                'song_id'   => $room->song_id,
                'name'      => $room->name,
                'gender'    => $room->gender,
                'address'   => $room->address,
                'area'      => $room->area,
                'is_active' => $room->is_active,
            ],
            'owner' => [
                'user_id'   => $userId,
            ],
            'statistic' => [
                'key'       => $filterType,
                'total'     => $totalLastTwoMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => mt_rand(1, 100),
                    'currdate_to_gp_diff'   => mt_rand(1, 100),
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => mt_rand(1, 100),
                'start_baseline_date_range_value'   => date('Y-m-d'),
                'available_report_type'             => $availableReportType,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => $reportType,
            ],
            'gp'    => [
                'level_id'      => 11,
                'gp_created_at' => $gpCreatedAt,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Agt',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Sep',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Okt',
                        'y' => mt_rand(1, 10),
                    ],
                ]   
            ]
        ];
    }
}