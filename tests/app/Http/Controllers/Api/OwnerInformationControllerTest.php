<?php

namespace App\Http\Controllers\Api;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

class OwnerInformationControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_OWNER_INFORMATION = 'api/v1/owner/information';

    protected function setUp() : void
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction(function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    public function testGetChargingBbkFalse()
    {
        Carbon::setTestNow(Carbon::create(2020, 8, 7, 0, 0, 0));

        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_INFORMATION);

        $response->assertOk();
        $data = $response->getData();
        $this->assertFalse($data->announcement->bbk_charging);
    }

    public function testGetChargingBbkTrue()
    {
        Carbon::setTestNow(Carbon::create(2020, 8, 7, 0, 0, 0));

        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_INFORMATION);

        $response->assertOk();
        $data = $response->getData();
        $this->assertTrue($data->announcement->bbk_charging);
    }

    // temporary
    public function testGetChargingBbkFalseBefore7Aug()
    {
        Carbon::setTestNow(Carbon::create(2020, 8, 6, 0, 0, 0));

        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_INFORMATION);

        $response->assertOk();
        $data = $response->getData();
        $this->assertFalse($data->announcement->bbk_charging);
    }

    // temporary
    public function testGetChargingBbkTrueBefore7Aug()
    {
        Carbon::setTestNow(Carbon::create(2020, 8, 6, 0, 0, 0));

        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        $level = factory(KostLevel::class)->create([
            'name' => 'Booking Fee',
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id,
        ]);
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_INFORMATION);

        $response->assertOk();
        $data = $response->getData();
        $this->assertTrue($data->announcement->bbk_charging);
    }
}
