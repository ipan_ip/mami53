<?php

namespace App\Http\Controllers\Api\Faq;

use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;


class FaqControllerTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\Landing\LandingRepository');
    }

    public function testShow()
    {
        $faqController = $this->mockPartialAlternatively(FaqController::class);

        Cache::shouldReceive('remember')->once()->andReturn([]);
        //$faqController->shouldReceive('getCityKampusFromSlug')->andReturn('test');

        $res = $faqController->show('test');

        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function testGetData_Null()
    {
        $this->mock->shouldReceive('getLandingDetailBySlug')->once()->andReturn(null);

        $res = (new FaqController)->getData('test', $this->mock);
        
        $this->assertNull($res);
    }

    public function testGetData_NotNull()
    {
        $room = factory(Room::class)->make();
        $landing = factory(Landing::class)->make();
        $this->mock->shouldReceive('getLandingDetailBySlug')->once()->andReturn($landing);

        $this->mock->shouldReceive('getCheapestDaily')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestMonthly')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestWeekly')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestYearly')->once()->andReturn($room);
        $this->mock->shouldReceive('getAvailableBisaBooking')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestPutri')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestPutra')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestCampur')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestBebas24Jam')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestPasutri')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestKamarMandiDalam')->once()->andReturn($room);
        $this->mock->shouldReceive('getCheapestWifi')->once()->andReturn($room);

        $res = (new FaqController)->getData('test', $this->mock);

        $this->assertIsArray($res);
        $this->assertSame(
            array_keys($res),
            [
                'cheapest_daily',
                'cheapest_monthly',
                'cheapest_weekly',
                'cheapest_yearly',
                'available_bisaBooking',
                'cheapest_putri',
                'cheapest_putra',
                'cheapest_campur',
                'cheapest_bebas24Jam',
                'cheapest_pasutri',
                'cheapest_kamarMandiDalam',
                'cheapest_wifi'
            ]
        );
    }

    public function testGetCityKampusFromSlug()
    {
        $res = $this->callNonPublicMethod(FaqController::class, 'getCityKampusFromSlug', ['kost-jogja-murah']);

        $this->assertIsString($res);
        $this->assertSame($res, 'jogja');
    }
}