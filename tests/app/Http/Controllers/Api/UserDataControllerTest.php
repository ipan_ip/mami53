<?php
namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Channel\MoEngage\MoEngageUserPropertiesApi;
use App\Entities\User\UserSocial;
use App\Repositories\OwnerDataRepository;
use App\Test\MamiKosTestCase;
use App\User;

use phpmock\MockBuilder;

use Mockery;
use Config;

class UserDataControllerTest extends MamikosTestCase 
{

    use WithoutMiddleware;

    private const GET_EDIT_USER_PROFILE_API = 'api/v1/user/edit';
    private const GET_USER_PROFILE_API = 'api/v1/user/profile';
    private const POST_USER_UPDATE_API = 'api/v1/user/update';
    private const POST_USER_CHANGE_PROFILE_PHOTO_API = 'api/v1/user/media';
    private const GET_USER_JOBS_APPLY_API = 'api/v1/user/vacancy/apply';
    private const POST_SIMPLY_EDIT_API = 'api/v1/user/edit/simple';
    private const POST_CHANGE_PHONE_API = 'api/v1/setting/phone';
    private const POST_CHANGE_OCCUPATION_API = 'api/v1/user/occupation';
    private const POST_CHANGE_NAME_API = 'api/v1/setting/name';
    private const POST_CHANGE_EMAIL_API = 'api/v1/setting/email';
    private const POST_CHANGE_TERMS_API = 'api/v1/setting/terms';
    private const GET_IS_USER_SOCIAL_API = 'api/v1/user/user-social';

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\UserDataRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetEditValidJsonStructure()
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user);

        $this->mock->shouldReceive('getEditUser')->once()->andReturn([]);

        $response = $this->json('GET', self::GET_EDIT_USER_PROFILE_API);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetEditWrongUserType()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_EDIT_USER_PROFILE_API);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Anda bukan user'
            ],
            'data' => null,
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetEditWrongUserTypeValidJsonStructure()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_EDIT_USER_PROFILE_API);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data',

        ]);
    }


    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testProfileSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('getProfileUser')->once()->andReturn([]);

        $response = $this->json('GET', self::GET_USER_PROFILE_API);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testProfileStructureValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('getProfileUser')->once()->andReturn([]);

        $response = $this->json('GET', self::GET_USER_PROFILE_API);

        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testProfileWrongUserType()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_USER_PROFILE_API);

        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Anda bukan user'
            ],
            'data' => null,
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testProfileWrongUserTypeValidJsonStructure()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_USER_PROFILE_API);

        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data'
        ]);
    }

    /**
     * This method providing for postUpdate params
     * 
     * @return array
     */
    private function buildParamPostUpdate(): array
    {
        return [
            'birthday' => date('Y-m-d H:i:s'),
            'city' => 'Yogyakarta',
            'description' => str_random(16),
            'education' => 'SMA',
            'email' => 'test@'.str_random(10).'.com',
            'gender' => 'male',
            'jobs' => 'lainnya',
            'martial_status' => 'kawin',
            'name' => 'Test Name '.str_random(5),
            'phone_number_additional' => '085'.mt_rand(10000, 99999),
            'photo_id' => mt_rand(11111, 99999),
            'work_place' => str_random(16),
            'photo' => [
                'real'  => Config::get('api.media.cdn_url').'uploads/data/user/2019-01-03/KQw0kuAf.jpg',
                'small' => Config::get('api.media.cdn_url').'uploads/cache/data/user/2019-01-03/KQw0kuAf-240x320.jpg',
                'medium' => Config::get('api.media.cdn_url').'uploads/cache/data/user/2019-01-03/KQw0kuAf-360x480.jpg',
                'large' => Config::get('api.media.cdn_url').'uploads/cache/data/user/2019-01-03/KQw0kuAf-540x720.jpg',
            ]
            ];
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testPostUpdateHasWrongUserType()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);
        $param = $this->buildParamPostUpdate();

        $response = $this->json('POST', self::POST_USER_UPDATE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Anda bukan user'
            ],
            'data' => null,
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testPostUpdateHasWrongUserTypeWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);
        $param = $this->buildParamPostUpdate();

        $response = $this->json('POST', self::POST_USER_UPDATE_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data',
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testPostUpdateWithParamNameIsTooShort()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        //Replace name key array to be too short
        $param = array_replace(
            $this->buildParamPostUpdate(), 
            ['name' => str_random(2)]
        );
        
        $response = $this->json('POST', self::POST_USER_UPDATE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => [
                0 => 'Nama user min 3 karakter'
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testPostUpdateWithInvalidBirtdayParam()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        //Replace name key array to be too short
        $param = array_replace(
            $this->buildParamPostUpdate(), 
            ['birthday' => str_random(2)]
        );
        
        $response = $this->json('POST', self::POST_USER_UPDATE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => [
                0 => 'Tanggal lahir tidak sesui format'
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testPostUpdateWithInvalidGenderParam()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        //Replace name key array to be too short
        $param = array_replace(
            $this->buildParamPostUpdate(), 
            ['gender' => str_random(12)]
        );
        
        $response = $this->json('POST', self::POST_USER_UPDATE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'message' => [
                0 => 'Jenis kelamin tidak sesuai'
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeProfilePhotoSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('uploadProfilePhoto')->once()->andReturn([]);

        Storage::fake('photo');
        $photoImg = ['photo' => UploadedFile::fake()->image('photo.jpg')];

        $response = $this->json('POST', self::POST_USER_CHANGE_PROFILE_PHOTO_API, $photoImg);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeProfilePhotoSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('uploadProfilePhoto')->once()->andReturn([]);

        Storage::fake('photo');
        $photoImg = ['photo' => UploadedFile::fake()->image('photo.jpg')];

        $response = $this->json('POST', self::POST_USER_CHANGE_PROFILE_PHOTO_API, $photoImg);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeProfilePhotoWithInvalidPhotoFile()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        Storage::fake('photo');
        $photoImg = ['photo' => UploadedFile::fake()->image('photo.txt')];

        $response = $this->json('POST', self::POST_USER_CHANGE_PROFILE_PHOTO_API, $photoImg);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'message' => [
                0 => 'Jenis file tidak diperkenankan, harap menggunakan file JPG atau PNG'
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeProfilePhotoWithImageParamSizeTooLarge()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        Storage::fake('photo');
        $photoImg = ['photo' => UploadedFile::fake()->create('photo.jpg', 999999999999)];

        $response = $this->json('POST', self::POST_USER_CHANGE_PROFILE_PHOTO_API, $photoImg);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'message' => [
                0 => 'Ukuran file tidak boleh melebihi 2MB'
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeProfilePhotoWithImageParamSizeTooLargeValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        Storage::fake('photo');
        $photoImg = ['photo' => UploadedFile::fake()->create('photo.jpg', 999999999999)];

        $response = $this->json('POST', self::POST_USER_CHANGE_PROFILE_PHOTO_API, $photoImg);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'message' => [
                0
            ]
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetUserJobsApplySuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('getApply')->once()->andReturn([]);

        $response = $this->json('GET', self::GET_USER_JOBS_APPLY_API);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetUserJobsApplySuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('getApply')->once()->andReturn([]);

        $response = $this->json('GET', self::GET_USER_JOBS_APPLY_API);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetUserJobsApplyWithUserDataIsNull()
    {
        $response = $this->json('GET', self::GET_USER_JOBS_APPLY_API);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'data' => null,
        ]);
        
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testGetUserJobsApplyWithUserDataIsNullAndValidJsonStructure()
    {
        $response = $this->json('GET', self::GET_USER_JOBS_APPLY_API);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data'
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('updateUserPhone')->once()->andReturn([]);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Data berhasil update',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('updateUserPhone')->once()->andReturn([]);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code' ,
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditWithInvalidUserType()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'User tidak ditemukan',
            ],
        ]);
    }

     /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditWithInvalidUserTypeAndValidJsonStructure()
    {
        $user = factory(\App\User::class)->make(['is_owner' => 'true']);
        $this->actingAs($user);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code' ,
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditSuccessWithPhoneFilled()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $this->mock->shouldReceive('updateUserPhone')->once()->andReturn([]);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
            'phone' => '085611117778',
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Data berhasil update',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSimplyEditSuccessWithNotValidPhoneFilled()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);
        
        $param = [
            'for_param' => 'rekomendasi',
            'email' => $user->email,
            'phone' => str_random(13),
        ];

        $response = $this->json('POST', self::POST_SIMPLY_EDIT_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'No hp tidak sesuai format',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangePhoneNumberSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $message = str_random(16);
        $this->mock->shouldReceive('changePhoneNumber')
            ->once()
            ->andReturn([
                'status' => true,
                'message' => $message,
            ]);

        $param = ['phone' => '085612347865'];
        $response = $this->json('POST', self::POST_CHANGE_PHONE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message,
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangePhoneNumberSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $message = str_random(16);
        $this->mock->shouldReceive('changePhoneNumber')
            ->once()
            ->andReturn([
                'status' => true,
                'message' => $message,
            ]);

        $param = ['phone' => '085612347865'];
        $response = $this->json('POST', self::POST_CHANGE_PHONE_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangePhoneNumberWithParamPhoneNumberIsNull()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $param = ['phone' => null];
        $response = $this->json('POST', self::POST_CHANGE_PHONE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Nomor tidak boleh kosong',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangePhoneNumberWithParamPhoneNumberIsNotValid()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $param = ['phone' => str_random(12)];
        $response = $this->json('POST', self::POST_CHANGE_PHONE_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Maaf, Nomor tidak valid, silahkan gunakan nomor yang lain',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSetOccupationSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $param = ['occupation' => 'kerja'];
        $response = $this->json('POST', self::POST_CHANGE_OCCUPATION_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSetOccupationSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $param = ['occupation' => 'kerja'];
        $response = $this->json('POST', self::POST_CHANGE_OCCUPATION_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testSetOccupationWhichUserHasNotLoginYet()
    {
        $param = ['occupation' => 'kerja'];
        $response = $this->json('POST', self::POST_CHANGE_OCCUPATION_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Silakan login terlebih dahulu',
            ],
        ]);
    }
    
    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeNameSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $name = 'Michael Sancez';
        $message = str_random(16);
        $this->mock->shouldReceive('changeName')
            ->once()
            ->andReturn([
                'name' => $name,
                'message' => $message,
            ]);

        $param = ['name' => $name];
        $response = $this->json('POST', self::POST_CHANGE_NAME_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message,
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeNameSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $name = 'Michael Sancez';
        $message = str_random(16);
        $this->mock->shouldReceive('changeName')
            ->once()
            ->andReturn([
                'name' => $name,
                'message' => $message,
            ]);

        $param = ['name' => $name];
        $response = $this->json('POST', self::POST_CHANGE_NAME_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeNameWithParamNameIsTooLong()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $name = 'Michael Sancez testChangeNameWithParamNameIsTooLong';
        $param = ['name' => $name];

        $response = $this->json('POST', self::POST_CHANGE_NAME_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Nama user max 50 karakter',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeNameWithInvalidParamName()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $param = ['name' => 'abcdefgHIJKLMN 123:"<>!!)'];

        $response = $this->json('POST', self::POST_CHANGE_NAME_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Mohon masukkan karakter alfabet',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeEmailSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = 'test@test.com';
        $message = 'Email berhasil diubah. Cek folder inbox (atau spam) untuk memverifikasi email baru Anda.';
        $this->mock->shouldReceive('changeEmail')
            ->once()
            ->andReturn([
                'email' => $email,
                'message' => $message,
            ]);

        $param = ['email' => $email];
        $response = $this->json('POST', self::POST_CHANGE_EMAIL_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message,
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeEmailSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = 'test@test.com';
        $message = 'Email berhasil diubah. Cek folder inbox (atau spam) untuk memverifikasi email baru Anda.';
        $this->mock->shouldReceive('changeEmail')
            ->once()
            ->andReturn([
                'email' => $email,
                'message' => $message,
            ]);

        $param = ['email' => $email];
        $response = $this->json('POST', self::POST_CHANGE_EMAIL_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeEmailWithNotValidEmail()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $email = str_random();

        $param = ['email' => $email];
        $response = $this->json('POST', self::POST_CHANGE_EMAIL_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Maaf. Email tidak valid. Silahkan gunakan alamat email yang lain',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeTermsSuccess()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $status = true;
        $message = str_random(16);
        $this->mock->shouldReceive('changeTcStatus')
            ->once()
            ->andReturn([
                'status' => $status,
                'message' => $message,
            ]);

        $param = ['agree' => 'agree'];
        $response = $this->json('POST', self::POST_CHANGE_TERMS_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message,
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeTermsSuccessWithValidJsonStructure()
    {
        $user = factory(\App\User::class)->make();
        $this->actingAs($user);

        $status = true;
        $message = str_random(16);
        $this->mock->shouldReceive('changeTcStatus')
            ->once()
            ->andReturn([
                'status' => $status,
                'message' => $message,
            ]);

        $param = ['agree' => 'agree'];
        $response = $this->json('POST', self::POST_CHANGE_TERMS_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testChangeTermsFailed()
    {
        $user = factory(\App\User::class)->make(['agree_on_tc' => 'true']);
        $this->actingAs($user);
       
        $param = ['agree' => 'agree'];
        $response = $this->json('POST', self::POST_CHANGE_TERMS_API, $param);
        
        $response->assertOk();
        $response->assertJsonFragment([
            'status' => false,
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIssueChatTokenSuccess()
    {
        $userId = mt_rand(1, 9999);
        $user = factory(\App\User::class)->create(
            ['id' => $userId]
        );
        $this->actingAs($user);

        $response = $this->json('POST', 'api/v1/user/' . $userId. '/issue-chat-token');
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Berhasil issue chat token',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIssueChatTokenSuccessWithValidJsonStructure()
    {
        $userId = mt_rand(1, 9999);
        $user = factory(\App\User::class)->create(
            ['id' => $userId]
        );
        $this->actingAs($user);

        $response = $this->json('POST', 'api/v1/user/' . $userId. '/issue-chat-token');
        
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIssueChatWithEmptyUserId()
    {
        $userId = ' ';
        $user = factory(\App\User::class)->create(
            ['id' => $userId]
        );
        $this->actingAs($user);

        $response = $this->json('POST', 'api/v1/user/' . $userId. '/issue-chat-token');
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'User ID tidak boleh kosong',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIssueChatWithInvalidUserId()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->make(
            ['id' => $userId]
        );
        $this->actingAs($user);

        $response = $this->json('POST', 'api/v1/user/' . $userId. '/issue-chat-token');
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'User ID tidak valid',
            ],
        ]);
    }

    /**
     * 
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIssueChatWithInvalidUserIdWithJsonStructure()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->make(
            ['id' => $userId]
        );
        $this->actingAs($user);

        $response = $this->json('POST', 'api/v1/user/' . $userId. '/issue-chat-token');
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIsTenantUserSocialSuccess()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->create(
            ['id' => $userId]
        );
        $this->actingAs($user);

        factory(UserSocial::class)->create(['user_id' => $userId]);

        $this->mock->shouldReceive('isUserSocial')
            ->once()
            ->andReturn(true);

        $response = $this->json('GET', self::GET_IS_USER_SOCIAL_API);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIsTenantUserSocialSuccessWithValidJsonStructure()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->make(
            ['id' => $userId]
        );
        $this->actingAs($user);

        factory(UserSocial::class)->make(['user_id' => $userId]);

        $this->mock->shouldReceive('isUserSocial')
            ->once()
            ->andReturn(true);

        $response = $this->json('GET', self::GET_IS_USER_SOCIAL_API);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIsTenantUserSocialWhichUserTypeIsOwner()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)
            ->state('owner')->make(['id' => $userId]);
        $this->actingAs($user);

        factory(UserSocial::class)->make(['user_id' => $userId]);

        $response = $this->json('GET', self::GET_IS_USER_SOCIAL_API);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Anda bukan user',
            ],
            'data' => null,
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIsTenantUserSocialWhichUserTypeIsOwnerWithValidJsonStructure()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)
            ->state('owner')->make(['id' => $userId]);
        $this->actingAs($user);

        factory(UserSocial::class)->make(['user_id' => $userId]);

        $response = $this->json('GET', self::GET_IS_USER_SOCIAL_API);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data',
        ]);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/UserDataControllerTest
     */
    public function testIsTenantUserSocialThrowException()
    {
        $response = $this->json('GET', self::GET_IS_USER_SOCIAL_API)->json();
        $this->assertEquals($response['exception'], \Exception::class);
        $this->assertEquals($response['message'], __('api.tenant.error.not_found'));
    }
}
