<?php

namespace App\Http\Controller\Api;

use App\Entities\Notif\Category;
use App\Entities\User\Notification;
use App\Repositories\Notification\NotificationRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class NotificationControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const URL_GET_USER_NOTIF_LIST = 'api/v1/notifications';
    private const URL_GET_USER_NOTIF_COUNT = 'api/v1/notifications/counter';
    private const URL_NOTIF_CATEGORY = 'api/v1/notifications/categories';
    private const URL_READ_NOTIF = 'api/v1/notifications';

    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->createMock(NotificationRepositoryEloquent::class);
        $this->app->instance(NotificationRepositoryEloquent::class, $this->repo);
        $this->app->user = null;
        $this->app->device = null;
    }

    public function testIndex_getPaginatedResponse()
    {
        $userId = 1;
        $user = factory(User::class)->create(['id' => $userId]);
        $byUserResponse = factory(Notification::class, 5)->make(['user_id' => $userId])->toArray();
        $byUserResponse = $this->paginationResponse($byUserResponse);

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getByUser')
            ->willReturn($byUserResponse);
        $this->repo->expects($this->once())->method('setAsRead');

        $response = $this->actingAs($user)->json('GET', self::URL_GET_USER_NOTIF_LIST);

        $response->assertOk();
        $response->assertJsonFragment(['data' => $byUserResponse['data']]);
    }

    public function testIndex_NonLogin_StatusFalse()
    {
        $response = $this->json('GET', self::URL_GET_USER_NOTIF_LIST);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testCategories_getCategoriesWithAll()
    {
        $categoryA = factory(Category::class)->make();
        $categoryB = factory(Category::class)->make();
        $categoryListResponse = $this->paginationResponse([
            $categoryA->toArray(),
            $categoryB->toArray()
        ]);
        $allCategory = [
            'id' => 0,
            'name' => 'All',
            'icon' => '',
            'order' => 0
        ];

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('categoryList')
            ->willReturn($categoryListResponse);

        $response = $this->json('GET', self::URL_NOTIF_CATEGORY);
        array_unshift($categoryListResponse['data'], $allCategory);

        $response->assertOk();
        $response->assertJsonFragment(['data' => $categoryListResponse['data']]);
    }

    public function testCounter_getCountWithTotal()
    {
        $user = factory(User::class)->create();
        $countCategoryA = 10;
        $countCategoryB = 5;
        $unreadCountResponse = collect([
            ['category_id' => 1, 'unread' => $countCategoryA],
            ['category_id' => 2, 'unread' => $countCategoryB]
        ]);

        $this->repo->expects($this->once())
            ->method('getUserUnreadCount')
            ->willReturn($unreadCountResponse);

        $response = $this->actingAs($user)->json('GET', self::URL_GET_USER_NOTIF_COUNT);
        $unreadCountArray = $unreadCountResponse->toArray();
        array_unshift(
            $unreadCountArray,
            [
                'category_id' => 0,
                'unread' => $countCategoryA + $countCategoryB
            ]
        );

        $response->assertOk();
        $response->assertJsonFragment(['data' => $unreadCountArray]);
    }

    public function testCounter_NonLogin_StatusFalse()
    {
        $response = $this->json('GET', self::URL_GET_USER_NOTIF_COUNT);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testRead_getAllCountZero()
    {
        $user = factory(User::class)->create();
        $defaultResponse = ['category_id' => 0, 'unread' => 0];

        $this->repo->expects($this->once())->method('setAsRead');

        $response = $this->actingAs($user)->json('POST', self::URL_READ_NOTIF);

        $response->assertOk();
        $response->assertJsonFragment(['data' => $defaultResponse]);
    }

    public function testRead_NonLoginFromWeb_StatusFalse()
    {
        $response = $this->json('POST', self::URL_READ_NOTIF);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testRead_NonLoginFromApps_StatusTrue()
    {
        $this->app->device = ['id' => 1];

        $response = $this->json('POST', self::URL_READ_NOTIF);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true]);
    }

    private function paginationResponse($data)
    {
        return [
            'data' => $data,
            'meta' => [
                'pagination' => [
                    'current_page' => 1,
                    'total_pages' => 1,
                    'per_page' => 5
                ]
            ]
        ];
    }
}
