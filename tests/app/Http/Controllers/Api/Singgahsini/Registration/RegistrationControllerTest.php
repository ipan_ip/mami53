<?php

namespace App\Http\Controllers\Api\Singgahsini\Registration;

use App\Entities\Api\ErrorCode;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RegistrationControllerTest extends MamikosTestCase
{
    private const UPLOAD_IMAGE = 'garuda/singgahsini/upload-photo';
    private const REGISTER = 'garuda/singgahsini/register';

    protected $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testUploadPhoto_Failed()
    {
        $request = $this->createFakeRequest(
            self::UPLOAD_IMAGE,
            'POST',
            [
                'photo' => $this->faker->imageUrl($width = 640, $height = 480),
                'type'  => 0
            ]
        );

        $res = (new RegistrationController)->uploadPhoto($request);
        
        $this->assertFalse($res->getData()->status);
    }

    public function testUploadPhoto_Success()
    {
        Validator::shouldReceive('make->fails');

        $mockMedia = $this->mockPartialAlternatively(Media::class);
        $mockMedia->shouldReceive('postMedia')->andReturn(['id' => 10]);

        $request = $this->createFakeRequest(
            self::UPLOAD_IMAGE,
            'POST',
            [
                'photo' => $this->faker->imageUrl($width = 640, $height = 480),
                'type'  => 0
            ]
        );

        $res = (new RegistrationController)->uploadPhoto($request);

        $this->assertSame(RegistrationController::SUCCESS_UPLOAD_PHOTO, $res->getData()->data->message);
    }

    public function testRegister_Failed()
    {
        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            [
                'owner_name' => 'yasuo',
                'phone_number' => '9138121381',
                'city' => 'washington',
                'kost_name' => 'kost kost an',
                'address' => '',
            ]
        );

        $res = (new RegistrationController)->register($fakeRequest);

        $this->assertSame(200, $res->getStatusCode());
        $this->assertSame($res->getData()->status, false);
        $this->assertSame($res->getData()->issue->code, ErrorCode::SINGGAHSINI_REGISTRATION);
    }

    public function testRegister_Success()
    {
        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            [
                'owner_name' => 'yasuo',
                'phone_number' => '9138121381',
                'city' => 'washington',
                'kost_name' => 'kost kost an',
                'address' => 'deket rumah presiden',
            ]
        );

        $res = (new RegistrationController)->register($fakeRequest);

        $this->assertTrue($res->getData()->status);
        $this->assertSame(201, $res->getStatusCode());
    }

    public function testRegisterWithCompleteField_Success()
    {
        $this->withoutExceptionHandling();
        $media = factory(Media::class, 2)->create();
        $tag = factory(Tag::class)->create();

        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            [
                'owner_name' => 'yasuo',
                'phone_number' => '9138121381',
                'kebutuhan' => [1,2,3],
                'province' => 'west lake',
                'city' => 'washington',
                'subdistrict' => 'banyumanik',
                'address' => 'deket rumah presiden',
                'kost_name' => 'kost kost an',
                'total_room' => 100,
                'available_room' => 33,
                'price_monthly' => 1000000,
                'ac' => 0,
                'bathroom' => 1,
                'wifi' => 1,
                'foto_bangunan_tampak_depan' => [$media[0]->id],
                'foto_dalam_kamar' => [$media[1]->id],
                'general_facility' => [$tag->id, 0]
            ]
        );

        $res = (new RegistrationController)->register($fakeRequest);

        $this->assertTrue($res->getData()->status);
        $this->assertSame(201, $res->getStatusCode());
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}