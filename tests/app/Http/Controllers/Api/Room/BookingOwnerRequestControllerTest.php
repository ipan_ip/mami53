<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

class BookingOwnerRequestControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const POST_OWNER_BBK_REGISTER = 'api/v1/owner/bbk/register';
    private const GET_OWNER_BBK_STATUS = 'api/v1/owner/bbk/status';

    protected function setUp()
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction(function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown(): void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    public function testRegisterBulkBbkNotLogin()
    {
        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertStatus(401);
    }

    public function testRegisterBulkBbkNotMamipayOwner()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => false,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        $this->actingAs($owner);

        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertOk();
        $data = $response->getData();
        
        $this->assertEquals($data->meta->message, 'Gagal mengirim data, silakan ulangi.');
    }

    public function testRegisterBulkBbkMamipayOwnerPending()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => false,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_PENDING
        ]);
        $this->actingAs($owner);

        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertOk();
        $data = $response->getData();
        
        $this->assertEquals($data->meta->message, 'Gagal mengirim data, silakan ulangi.');
    }

    public function testRegisterBulkBbkMamipayOwnerRejected()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => false,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_REJECTED
        ]);
        $this->actingAs($owner);

        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertOk();
        $data = $response->getData();
        
        $this->assertEquals($data->meta->message, 'Gagal mengirim data, silakan ulangi.');
    }

    public function testRegisterBulkBbkMamipayOwner()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => false,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_APPROVED
        ]);
        $this->actingAs($owner);

        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals($data->meta->message, 'Fitur Booking Langsung berhasil diajukan');

        $bbkRequest = BookingOwnerRequest::where('designer_id', $room->id)->first();
        $this->assertEquals($owner->id, $bbkRequest->user_id);
        $this->assertEquals('waiting', $bbkRequest->status);
    }

    public function testRegisterBulkBbkStatusKosUnverified()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => false,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'unverified',
        ]);
        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_APPROVED
        ]);
        $this->actingAs($owner);

        $response = $this->json('POST', self::POST_OWNER_BBK_REGISTER);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals($data->meta->message, 'Fitur Booking Langsung berhasil diajukan');

        $bbkRequest = BookingOwnerRequest::where('designer_id', $room->id)->first();
        $this->assertNull($bbkRequest);
    }

    public function testGetBbkStatusApprove()
    {
        $this->createBookingOwnerRequest(BookingOwnerRequest::BOOKING_APPROVE);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(1, $data->bbk_status->approve);
        $this->assertEquals(0, $data->bbk_status->waiting);
        $this->assertEquals(0, $data->bbk_status->reject);
        $this->assertEquals(0, $data->bbk_status->other);
    }

    public function testGetBbkStatusWaiting()
    {
        $this->createBookingOwnerRequest(BookingOwnerRequest::BOOKING_WAITING);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(0, $data->bbk_status->approve);
        $this->assertEquals(1, $data->bbk_status->waiting);
        $this->assertEquals(0, $data->bbk_status->reject);
        $this->assertEquals(0, $data->bbk_status->other);
    }

    public function testGetBbkStatusReject()
    {
        $this->createBookingOwnerRequest(BookingOwnerRequest::BOOKING_REJECT);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(0, $data->bbk_status->approve);
        $this->assertEquals(0, $data->bbk_status->waiting);
        $this->assertEquals(1, $data->bbk_status->reject);
        $this->assertEquals(0, $data->bbk_status->other);
    }

    public function testGetBbkStatusNotActive()
    {
        $this->createBookingOwnerRequest(BookingOwnerRequest::BOOKING_NOT_ACTIVE);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(0, $data->bbk_status->approve);
        $this->assertEquals(0, $data->bbk_status->waiting);
        $this->assertEquals(0, $data->bbk_status->reject);
        $this->assertEquals(1, $data->bbk_status->other);
    }

    public function testGetBbkStatusOther()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(0, $data->bbk_status->approve);
        $this->assertEquals(0, $data->bbk_status->waiting);
        $this->assertEquals(0, $data->bbk_status->reject);
        $this->assertEquals(1, $data->bbk_status->other);
    }

    public function testGetBbkStatusKosUnverified()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'unverified',
        ]);
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_BBK_STATUS);

        $response->assertOk();
        $data = $response->getData();

        $this->assertEquals(0, $data->bbk_status->approve);
        $this->assertEquals(0, $data->bbk_status->waiting);
        $this->assertEquals(0, $data->bbk_status->reject);
        $this->assertEquals(0, $data->bbk_status->other);
    }

    private function createBookingOwnerRequest(string $status)
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true,
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified',
        ]);
        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => $status,
        ]);
        $this->actingAs($owner);
    }
}
