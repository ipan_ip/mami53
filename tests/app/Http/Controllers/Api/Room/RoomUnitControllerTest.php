<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Level\RoomLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RoomUnitControllerTest extends MamiKosTestCase
{

    public function testIndexFailedNoUserAct()
    {
        app()->user = null;
        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest();

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);

        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertEquals(401, $resultData->meta->code);
    }

    public function testIndexParameterValidationFailed()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest();

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);

        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertEquals(400, $resultData->meta->code);
    }

    public function testIndexParameterInputtedFailsTheValidation()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => null,
                'limit' =>  700,
                'offset' => -1
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);

        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertEquals(400, $resultData->meta->code);
    }

    public function testIndexSongIdNotFound()
    {
        $user = factory(User::class)->create();
        app()->user = $user;

        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomRepoMock->shouldReceive('getRoomWithOwnerBySongId')->andReturn(null);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => 888
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);

        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertEquals(404, $resultData->meta->code);
    }

    public function testIndexKosNotOwnedByUserAccess()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $room = factory(Room::class)->create(['room_count' => 4, 'room_available' => 3]);
        $roomOwner = factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => $user->id + 8 ]);

        $room->owners = collect([$roomOwner]);

        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomRepoMock->shouldReceive('getRoomWithOwnerBySongId')->andReturn($room);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => $room->song_id
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);

        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertEquals(401, $resultData->meta->code);
    }

    public function testIndexKosNotHavingRoomCount()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $room = factory(Room::class)->create(['room_count' => 0, 'room_available' => 0]);
        $roomOwner = factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => $user->id ]);

        $room->owners = collect([$roomOwner]);

        $roomRepoMock = $this->mockPartialAlternatively(RoomRepository::class);
        $roomRepoMock->shouldReceive('getRoomWithOwnerBySongId')->andReturn($room);
        $roomUnitRepoMock = $this->mockPartialAlternatively(RoomUnitRepository::class);

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => $room->song_id
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index($fakeRequest, $roomRepoMock, $roomUnitRepoMock);


        $this->assertInstanceOf(JsonResponse::class, $result);

        $resultData = $result->getData();
        $this->assertTrue($resultData->status);
        $this->assertEquals(200, $resultData->meta->code);
        $this->assertEmpty($resultData->data);
    }

    public function testIndexSuccessRetrievingRoomUnitNormalCase()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $unitCount = 15;
        $unitAvailable = floor($unitCount / 2);
        $room = factory(Room::class)->create(['room_count' => $unitCount, 'room_available' => $unitAvailable]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => $user->id]);

        $units = factory(RoomUnit::class, $unitCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $unitsOccupied = $units->nth(2, 0)->map(function ($unit) {
            $unit->occupied = true;
            $unit->save();
            return $unit;
        });
        $unitsOccupiedShiftAble = $unitsOccupied;

        factory(MamipayContract::class, $unitsOccupied->count())->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth(),
            'owner_id' => $user->id
        ])->each (function ($contract) use ($room, $unitsOccupiedShiftAble) {
            factory(MamipayContractKost::class)->create([
                'contract_id' => $contract->id,
                'designer_id' => $room->id,
                'designer_room_id' => $unitsOccupiedShiftAble->shift()->id
            ]);
        });

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => $room->song_id,
                'limit' => $unitCount,
                'offset' => 0
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index(
            $fakeRequest,
            app()->make(RoomRepository::class),
            app()->make(RoomUnitRepository::class)
        );

        $testResponse = new TestResponse($result);
        $testResponse->isOk();

        $testResponse->assertJsonStructure($this->expectedApiContracts());

        $data = $result->getData();

        $dataCollection = collect($data->data);

        $resultWithContract = $dataCollection->first(function ($item) {
            return $item->disable == true;
        });

        $this->assertNotNull($resultWithContract);

        $this->assertEquals($unitCount, $data->pagination->total);
        $this->assertEquals($unitCount, $data->pagination->limit);
        $this->assertEquals(1, $data->pagination->page);
        $this->assertFalse($data->pagination->{"has-more"});
    }

    public function testIndexSuccessRetrieveRoomUnitWithBackfill()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $unitCount = 15;
        $room = factory(Room::class)->create(['room_count' => $unitCount, 'room_available' => $unitCount]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => $user->id]);

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => $room->song_id,
                'limit' => 5,
                'offset' => 0
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index(
            $fakeRequest,
            app()->make(RoomRepository::class),
            app()->make(RoomUnitRepository::class)
        );

        $testResponse = new TestResponse($result);
        $testResponse->isOk();

        $testResponse->assertJsonStructure($this->expectedApiContracts());
    }

    public function testIndexSuccessRetrievingRoomUnitWithGoldplusLevel()
    {
        $this->prepareGoldplusLevel();
        $user = factory(User::class)->create();
        app()->user = $user;
        $unitCount = 15;
        $unitAvailable = floor($unitCount / 2);
        $room = factory(Room::class)->create(['room_count' => $unitCount, 'room_available' => $unitAvailable]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id, 'user_id' => $user->id]);

        $units = factory(RoomUnit::class, $unitCount)->create([
            'designer_id' => $room->id,
            'occupied' => false,
            'room_level_id' => $this->gp3->id
        ]);
        $unitsOccupied = $units->nth(2, 0)->map(function ($unit) {
            $unit->occupied = true;
            $unit->room_level_id = $this->gp3->id;
            $unit->save();

            return $unit;
        });
        $unitsOccupiedShiftAble = $unitsOccupied;

        factory(MamipayContract::class, $unitsOccupied->count())->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth(),
            'owner_id' => $user->id
        ])->each (function ($contract) use ($room, $unitsOccupiedShiftAble) {
            factory(MamipayContractKost::class)->create([
                'contract_id' => $contract->id,
                'designer_id' => $room->id,
                'designer_room_id' => $unitsOccupiedShiftAble->shift()->id
            ]);
        });

        $fakeRequest = $this->createFakeRequest(
            '/',
            'GET',
            [
                'song_id' => $room->song_id,
                'limit' => $unitCount,
                'offset' => 0
            ]
        );

        $controller = app()->make(RoomUnitController::class);
        $result = $controller->index(
            $fakeRequest,
            app()->make(RoomRepository::class),
            app()->make(RoomUnitRepository::class)
        );

        $testResponse = new TestResponse($result);
        $testResponse->isOk();
        $testResponse->assertJsonStructure($this->expectedApiContracts());

        $gpBadge = collect($result->getData()->data)->random()->gp_badge;
        $this->assertTrue($gpBadge->show);
        $this->assertEquals("GoldPlus", $gpBadge->label);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }

    private function prepareGoldplusLevel()
    {
        $index = 1;
        [$this->gp1, $this->gp2, $this->gp3, $this->gp4] = factory(RoomLevel::class, 4)
            ->make()->each(function ($level) use ($index) {
                $level->name = 'Goldplus ' . $index++;
                $level->is_regular = 0;
                $level->is_hidden = 0;
                $level->save();
            });
        Config::set('roomlevel.id.goldplus1', $this->gp1->id);
        Config::set('roomlevel.id.goldplus2', $this->gp2->id);
        Config::set('roomlevel.id.goldplus3', $this->gp3->id);
        Config::set('roomlevel.id.goldplus4', $this->gp4->id);
    }

    private function expectedApiContracts(): array
    {
        return [
            'status',
            'meta'  => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data' => [[
                'id',
                'name',
                'floor',
                'occupied',
                'disable',
                'gp_badge' => [
                    'show',
                    'label'
                ]
            ]],
            'pagination' => [
                'page',
                'limit',
                'offset',
                'total',
                'has-more'
            ]
        ];
    }
}
