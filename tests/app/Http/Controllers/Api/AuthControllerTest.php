<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\OAuthHelper;
use App\Test\MamiKosTestCase;
use App\Test\Traits\UsePassport;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\Entities\Device\UserDevice;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\User\SocialAccountType;
use App\Entities\User\UserSocial;
use Carbon\Carbon;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Libraries\SMSLibrary;
use AppleSignIn\ASPayload;
use ASDecoder;
use AppleSignIn\Vendor\JWT;

class AuthControllerTest extends MamikosTestCase
{
    use WithoutMiddleware, UsePassport;

    private const API_URL_DEVICE_REGISTER ='api/v1/auth/device/register';
    private const API_URL_LOGIN_OWNER ='api/v1/auth/owner/login';
    private const API_URL_VERIFY_OWNER = 'api/v1/auth/owner/phone/verify';
    private const API_URL_POST_SOCIAL = 'api/v1/auth/social';
    private const SOCIAL_TOKEN = 'ya29.ImGbB-Zls9dm9gBSbtP4rSGVGyO7K7TwMvlIUuce7NFgo6yMQutv-qeU8JoMz070_55zJWNrVgLdyveip07hivGTkSxO3FQP4XOrTK-cdDd158SzODAIlO4rKX1unagTwX5w';
    private const APPLE_SOCIAL_TOKEN = 'eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLmdpdC5hcHBsZXNpZ25pbi50ZXN0aW5nIiwiZXhwIjoxNTk5MTg5OTg1LCJpYXQiOjE1OTkxMDM1ODUsInN1YiI6IjAwMTQ5MC4yNjQ5YTFiNjc0M2I0ZTM0YjBlMmIwMmM5MTAwMzY0Zi4xNTE0IiwiYXRfaGFzaCI6Ild4bWQtamw1NUw2emJJN0t0MGdFSGciLCJlbWFpbCI6InRpYXJhLnRlbmF0MUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6InRydWUiLCJhdXRoX3RpbWUiOjE1OTkxMDM1ODIsIm5vbmNlX3N1cHBvcnRlZCI6dHJ1ZX0.kXp0ZDwmy3DMJS91cIXhOYVb34YeUJUx70vJhnOCkGMSQgBl818yKYIuhbqIoDooX5wGEd_XVZQq1WIe8tMaZ3ZD0QF19SRbcD-fhZxOba8TzVHnQEA6z7ILrnX44krXUUJATiUUrEH0II6qCZRGGQRe3LUvEZVuZTbVLcHoIF7gvulVTv08M9ekUJF_gwdRYcovTUVr1NryXFpmDiU7frsY3Mfn9TyZO5_NVZiVhuVmusMee8hGUDbT-8kWxUKWTd9xj6Y4W3v2mNK4tAC78RnxvlQA0AyXFaVnA2K-QqVEe8yaXtmqA83GtIR6kePCOMjzU0DKwofL3l9iFUi6mg';
    private const APPLE_SOCIAL_ID = '001490.2649a1b6743b4e34b0e2b02c9100364f.1514';
    private const APPLE_SOCIAL_TOKEN_ISSUED_TIMESTAMP = 1599103585;

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostDeviceRegisterHttpStatus200()
    {
        $deviceDetail = factory(\App\Entities\Device\UserDevice::class)->create([
            'identifier'            => str_random(16),
            'uuid'                  => sha1(str_random(16)),
            'platform'              => 'android',
            'platform_version_code' => '1.1.1',
            'model'                 => str_random(16),
            'email'                 => str_random(16).'@'.str_random(16).'.com',
            'app_version_code'      => mt_rand(1, 100),
        ]);

        $userDevice = UserDevice::where('identifier', $deviceDetail['identifier'])
            ->orderBy('created_at', 'desc')->first();
        
        $response = $this->json('POST', self::API_URL_DEVICE_REGISTER);
        $response->assertStatus(200);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostDeviceRegisterMatchExactResponseSuccess()
    {
        $deviceDetail = factory(\App\Entities\Device\UserDevice::class)->create([
            'identifier'            => str_random(16),
            'uuid'                  => sha1(str_random(16)),
            'platform'              => 'android',
            'platform_version_code' => '1.1.1',
            'model'                 => str_random(16),
            'email'                 => str_random(16).'@'.str_random(16).'.com',
            'app_version_code'      => mt_rand(1, 100),
        ]);

        $userDevice = UserDevice::where('identifier', $deviceDetail['identifier'])
            ->orderBy('created_at', 'desc')->first();
        
        $response = $this->json('POST', self::API_URL_DEVICE_REGISTER);
        $response->assertJson([
            'status' => true,
            'meta'  => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostDeviceRegisterAlreadyRegistered()
    {
        $identifier = str_random(16);
        $deviceData = [
            'identifier'            => $identifier,
            'uuid'                  => sha1(str_random(16)),
            'platform'              => 'android',
            'platform_version_code' => '1.1.1',
            'model'                 => str_random(16),
            'email'                 => str_random(16).'@'.str_random(16).'.com',
            'app_version_code'      => mt_rand(1, 100),
        ];
        $deviceDetail = factory(\App\Entities\Device\UserDevice::class)->create($deviceData);
        
        $params = [
            'device_identifier' => $identifier,
        ];

        $response = $this->json('POST', self::API_URL_DEVICE_REGISTER, $params);
        $response->assertJson([
            'status' => true,
            'meta'  => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostDeviceRegisterExactJsonStructure()
    {
        $identifier = str_random(16);
        $deviceData = [
            'identifier'            => $identifier,
            'uuid'                  => sha1(str_random(16)),
            'platform'              => 'android',
            'platform_version_code' => '1.1.1',
            'model'                 => str_random(16),
            'email'                 => str_random(16).'@'.str_random(16).'.com',
            'app_version_code'      => mt_rand(1, 100),
        ];
        $deviceDetail = factory(\App\Entities\Device\UserDevice::class)->create($deviceData);
        
        $params = [
            'device_identifier' => $identifier,
        ];

        $response = $this->json('POST', self::API_URL_DEVICE_REGISTER, $params);
        $response->assertJsonStructure([
            'status',
            'meta'  => [
                'response_code',
                'code',
                'severity',
                'message' 
            ],
            'register' => []
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerSuccess()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => $user->phone_number,
                'password' => $password,
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerSuccessWithValidJsonStructure()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => $user->phone_number,
                'password' => $password,
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'login' => [
                'redirect_path'
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerWithNoPassword()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => $user->phone_number,
                'password' => null,
            ]
        );

        $expectedJson = [
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "A validation error occurred",
            ],
            'message' => __('api.input.password.required')
        ];

        $response->assertStatus(200);
        $response->assertJson($expectedJson);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerWithNoPhoneNumber()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => null,
                'password' => $password,
            ]
        );

        $expectedJson = [
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "A validation error occurred",
            ],
            'message' => __('api.input.phone_number.required')
        ];

        $response->assertStatus(200);
        $response->assertJson($expectedJson);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerWithNoPhoneNumberAndNoPassword()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => null,
                'password' => null,
            ]
        );

        $expectedJson = [
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "A validation error occurred",
            ],
            'message' => __('api.input.phone_number.required')
        ];

        $response->assertStatus(200);
        $response->assertJson($expectedJson);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerMustValidJsonStructure()
    {
        $password = md5(str_random());
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => null,
                'password' => null,
            ]
        );

        $expectedJson = [
            'status',
            'meta' => [
                "response_code",
                "code",
                "severity",
                "message",
            ],
        ];

        $response->assertStatus(200);
        $response->assertJsonStructure($expectedJson);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerWithNotValidPassword()
    {
        $password = crc32('1');
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => $user->phone_number,
                'password' => $password,
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "A validation error occurred",
            ],
            'message' => __('api.input.password.string'),
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostLoginOwnerWithUserDataDoesNotExist()
    {
        $password = crc32('1');
        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password)
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_LOGIN_OWNER,
            [
                'phone_number' => $user->phone_number.'x',
                'password' => $password,
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "A validation error occurred",
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1040
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerAsTenant()
    {
        $phoneNumber = '08571'.mt_rand(11111, 99999);
        $password = sha1(\uniqid());

        $user = factory(User::class)->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => __('api.auth.owner.phone_number.legacy.forget_password'),
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerSuccess()
    {
        $mockSMSLibrary = $this->mockPartialAlternatively(SMSLibrary::class);
        $mockSMSLibrary->shouldReceive('smsVerification')->andReturn(true);
        $phoneNumber = '085714531190';
        $password = sha1(\uniqid());

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "The request has succeeded. An entity corresponding to the requested resource is sent in the response.",
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4842
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithPhoneNumberTooShort()
    {
        $mockSMSLibrary = $this->mockPartialAlternatively(SMSLibrary::class);
        $mockSMSLibrary->shouldReceive('smsVerification')->andReturn(true);
        $phoneNumber = '085'.mt_rand(1, 10);
        $password = sha1(\uniqid());

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "The phone number must be at least 8 characters., Format No. Handphone tidak valid (08xxxxxxxx)",
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4842
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithPhoneNumberTooLong()
    {
        $mockSMSLibrary = $this->mockPartialAlternatively(SMSLibrary::class);
        $mockSMSLibrary->shouldReceive('smsVerification')->andReturn(true);
        $phoneNumber = '085714531190'.mt_rand(11111, 99999);
        $password = sha1(\uniqid());

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "The phone number may not be greater than 14 characters., Format No. Handphone tidak valid (08xxxxxxxx)",
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithoutInvalidPhoneNumber()
    {
        $phoneNumber = str_random(13);
        $password = sha1(\uniqid());

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => null,
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "Format No. Handphone tidak valid (08xxxxxxxx), Parameter is_forget harus diisi",
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithNoPasswordFilled()
    {
        $phoneNumber = '085714531190';
        $password = null;

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber,
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "Password harus diisi",
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithPasswordIsTooShort()
    {
        $phoneNumber = '085714531190';
        $password = mt_rand(1, 2).''.str_random(1);

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber,
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'status' => false,
            'meta' => [
                "response_code" => 200,
                "code" => 200,
                "severity" => "OK",
                "message" => "Password minimal 6 karakter",
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testpostVerifyOwnerWithMustValidJsonStructure()
    {
        $phoneNumber = '085714531190';
        $password = null;

        $user = factory(User::class)->states('owner')->create([
            'password' => Hash::make($password),
            'phone_number' => $phoneNumber,
        ]);

        $response = $this->json(
            'POST',
            self::API_URL_VERIFY_OWNER,
            [
                'is_forget' => '1',
                'phone_number' => $phoneNumber,
                'password' => $password
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                "response_code",
                "code",
                "severity",
                "message",
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1352
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleReturnFailed(): void
    {
        $param = [
            'social_token'  => str_random(),
            'social_id'     => str_random(),
            'social_type'   => 'apple',
        ];

        app()->device = null;

        $appleMock  = ASDecoder::spy();
        $mockPayload = $appleMock->shouldReceive('getAppleSignInPayload')
            ->andReturn(null);
        $this->assertEquals($appleMock->getAppleSignInPayload(str_random()), null);

        $response = $this->json('POST', self::API_URL_POST_SOCIAL, $param);
        $response->assertOk();
        $response->assertJson([
            'status'    => false,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => __('api.apple-auth.authentication-failed'),
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1352
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleThatHasInvalidSocialType(): void
    {
        $param = [
            'social_token'  => str_random(),
            'social_id'     => str_random(),
            'social_type'   => 'apple'.mt_rand(1, 10),
        ];

        app()->device = null;

        $appleMock  = ASDecoder::spy();
        $mockPayload = $appleMock->shouldReceive('getAppleSignInPayload')->andReturn(null);
        $this->assertEquals($appleMock->getAppleSignInPayload(str_random()), null);

        $response = $this->json('POST', self::API_URL_POST_SOCIAL, $param)->json();
        $this->assertEquals($response['exception'], \Exception::class);
    }

    /**
     * @group UG
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleLoginFlowWithExpiredToken()
    {
        ASDecoder::spy()
            ->shouldReceive('getAppleSignInPayload')
            ->andThrow(\Firebase\JWT\ExpiredException::class);
        $user = factory(User::class)->state('tenant')->create();
        /** @var UserSocial */
        $userSocial = factory(UserSocial::class)->create([
            'user_id' => $user,
            'type' => SocialAccountType::APPLE,
            'identifier' => self::APPLE_SOCIAL_ID,
        ]);
        $param = [
            'social_token'  => self::APPLE_SOCIAL_TOKEN,
            'social_id'     => $userSocial->identifier,
            'social_type'   => $userSocial->type,
            'social_name'   => str_random(),
        ];

        $this->app->device = null;

        $response = $this->postJson(self::API_URL_POST_SOCIAL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
    }

    private function mockAppleLoginPayload(): void
    {
        ASDecoder::spy()
            ->shouldReceive('getAppleSignInPayload')
            ->andReturn(new ASPayload((object) [
                'sub' => self::APPLE_SOCIAL_ID,
                'iat' => self::APPLE_SOCIAL_TOKEN_ISSUED_TIMESTAMP
            ]));
    }

    /**
     * @group UG
     * @group UG-2626
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleLoginFlowWithExistingName()
    {
        $this->mockAppleLoginPayload();
        $existingName = str_random();
        $email = 'example@'.str_random().'.com';
        $user = factory(User::class)->state('tenant')->create([
            'name' => $existingName,
            'email' => $email,
        ]);
        /** @var UserSocial */
        $userSocial = factory(UserSocial::class)->create([
            'user_id' => $user,
            'type' => SocialAccountType::APPLE,
            'identifier' => self::APPLE_SOCIAL_ID,
            'email' => $email,
        ]);
        $param = [
            'social_token'  => self::APPLE_SOCIAL_TOKEN,
            'social_id'     => $userSocial->identifier,
            'social_type'   => $userSocial->type,
            'social_name'   => str_random(),
        ];

        $this->app->device = null;

        $response = $this->postJson(self::API_URL_POST_SOCIAL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);

        $user->refresh();
        // Name should not be changed
        $this->assertEquals($existingName, $user->name);
    }

    /**
     * @group UG
     * @group UG-2626
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleLoginFlowWithExistingDefaultName()
    {
        $this->mockAppleLoginPayload();
        $existingName = User::DEFAULT_APPLE_USER_NAME;
        $user = factory(User::class)->state('tenant')->create([
            'name' => $existingName,
        ]);
        /** @var UserSocial */
        $userSocial = factory(UserSocial::class)->create([
            'user_id' => $user,
            'type' => SocialAccountType::APPLE,
            'identifier' => self::APPLE_SOCIAL_ID,
        ]);
        $param = [
            'social_token'  => self::APPLE_SOCIAL_TOKEN,
            'social_id'     => $userSocial->identifier,
            'social_type'   => $userSocial->type,
            'social_name'   => str_random(),
        ];

        $this->app->device = null;

        $response = $this->postJson(self::API_URL_POST_SOCIAL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);

        $user->refresh();
        // Name should be changed
        $this->assertEquals($param['social_name'], $user->name);
    }

    /**
     * @group UG
     * @group UG-2626
     * @group App/Http/Controllers/Api/AuthController
     */
    public function testPostSocialWithAppleLoginFlowWithExistingDefaultNameButEmptySocialNameParam()
    {
        $this->mockAppleLoginPayload();
        $existingName = User::DEFAULT_APPLE_USER_NAME;
        $user = factory(User::class)->state('tenant')->create([
            'name' => $existingName,
        ]);
        /** @var UserSocial */
        $userSocial = factory(UserSocial::class)->create([
            'user_id' => $user,
            'type' => SocialAccountType::APPLE,
            'identifier' => self::APPLE_SOCIAL_ID,
        ]);
        $param = [
            'social_token'  => self::APPLE_SOCIAL_TOKEN,
            'social_id'     => $userSocial->identifier,
            'social_type'   => $userSocial->type,
            'social_name'   => '',
        ];

        $this->app->device = null;

        $response = $this->postJson(self::API_URL_POST_SOCIAL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);

        $user->refresh();
        // Name should not be changed, keep to apple since parameter social_name is empty
        $this->assertEquals($existingName, $user->name);
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\OwnerDataRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();

        // mock guzzle
        $fakeResponseToken = [
            'token_type'        => 'Bearer',
            'expires_in'        => 863999,
            'access_token'      => 'eyJ0eXAiOiJKV1QiLCJ',
            'refresh_token'     => 'def50200c3081c8af97',
        ];

        $this->mockPartialAlternatively(OAuthHelper::class)
            ->shouldReceive('issueTokenFor')
            ->andReturn($fakeResponseToken);

        $this->registerPassportEnvironment();
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }
}
