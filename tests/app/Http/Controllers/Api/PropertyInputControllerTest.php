<?php 
namespace App\Http\Controllers\Api;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\Http\Controllers\Api\PropertyInputController;
use App\User;
use App\Entities\Mamipay\MamipayOwner;
use App\Repositories\PropertyInputRepositoryEloquent;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Room;

class PropertyInputControllerTest extends MamiKosTestCase 
{
    use WithoutMiddleware;
    
    private const POST_INPUT_KOST_API_URL = '/api/v1/stories/input';

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\PropertyInputRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-490
     * @group App/Http/Controllers/Api/PropertyInputControllerTest
     */
    public function testInputKostSuccess()
    {
        $owner = factory(User::class)->state('owner')->create();
        $this->actingAs($owner);

        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_APPROVED,
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
        ]);

        $room = factory(Room::class)->create();

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 2,
            'room_count' => 5,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => 1,
        ];

        $this->mock->shouldReceive('saveNewKost')->once()->andReturn((object)[
            'id' => $room->id,
            'song_id' => mt_rand(1, 999999)
        ]);

        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        
        $result->assertOk();
        $result->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'data' => [
                'dialog_message' => 'Terima kasih telah mendaftarkan kost Anda di MamiKos!',
                'promo_input' => false,
                'trial_package' => null,
            ]
        ]);
    }

     /**
     * @group UG
     * @group UG-490
     * @group App/Http/Controllers/Api/PropertyInputControllerTest
     */
    public function testInputKostWhichNotMamipayOwner()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 2,
            'room_count' => 5,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => 1,
        ];
        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        $result->assertOk();
        $result->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'messages' => [
                0 => __('api.auto_bbk.error.invalid_autobbk_activation')
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-490
     * @group App/Http/Controllers/Api/PropertyInputControllerTest
     */
    public function testInputKostWhichNotValidAutobbkValueAndNotValidMamipayOwner()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 2,
            'room_count' => 5,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => str_random(3, 10),
        ];
        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        $result->assertOk();
        $result->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'messages' => [
                0 => 'The selected autobbk is invalid.',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-490
     * @group App/Http/Controllers/Api/PropertyInputControllerTest
     */
    public function testInputKostWhichNotLoginYet()
    {

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 2,
            'room_count' => 5,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => str_random(3, 10),
        ];

        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        $result->assertStatus(401);
        $result->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 401,
                'code' => 401,
                'severity' => 'UNAUTHORIZE',
                'message' => 'Not authorized.',
            ],
        ]);
    }

    public function testInputKostWithInvalidRoomCount()
    {
        $owner = factory(User::class)->state('owner')->create();
        $this->actingAs($owner);

        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_APPROVED,
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
        ]);

        $room = factory(Room::class)->create();

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 2,
            'room_count' => 511,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => 1,
        ];

        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        
        $result->assertOk();
        $result->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'messages' => [
                0 => 'Jumlah kamar tidak bisa lebih dari 500',
            ]
        ]);
    }

    public function testInputKostWithInvalidRoomCountAndRoomAvailable()
    {
        $owner = factory(User::class)->state('owner')->create();
        $this->actingAs($owner);

        factory(MamipayOwner::class)->create([
            'user_id' => $owner->id,
            'status' => MamipayOwner::STATUS_APPROVED,
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
        ]);

        $room = factory(Room::class)->create();

        $kostName = str_random(15);
        $param = [
            'address' => str_random(16),
            'agent_email' => str_random(16).'@'.str_random().'.com',
            'agent_name' => str_random(10),
            'agent_phone' => str_random(16),
            'agent_status' => 'Pemilik Kos',
            'animal' => 0,
            'building_year' => 0,
            'description' => str_random(16).str_random(16).str_random(16),
            'extend' => 0,
            'fac_bath' => [1, 5],
            'fac_property' => [0],
            'fac_room' => [14, 17, 19],
            'gender' => 1,
            'input_as' => 'Pemilik Kos',
            'is_agree_review' => true,
            'is_checkin' => false,
            'latitude' => 1.5E-322,
            'longitude' => 2.0E-322,
            'manager_name' => str_random(16). ' '.str_random(10),
            'manager_phone' => '08655'.mt_rand(11111, 99999),
            'name' => $kostName,
            'owner_name' => str_random(16),
            'owner_phone' => '0865'.mt_rand(11111, 99999),
            'photo_id' => 0,
            'photos' => [
                'cover' => 1660988,
                'bath' => [
                    1660991
                ],
                'bedroom' => [
                    1660990
                ],
                'main' => [1660989],
                'other' => [],
                '360' => 0,
            ],
            'price' => 0,
            '3_month' => 0,
            '6_month' => 0,
            'price_daily' => 0,
            'price_monthly' => 590000,
            'price_remark' => '-',
            'price_weekly' => 0,
            'price_yearly' => 0,
            'remarks' => '',
            'room_available' => 400,
            'room_count' => 200,
            'room_size' => '4x4',
            'selfie_photo' => 0,
            'user_id' => 0,
            'youtube_link' => '',
            'autobbk' => 1,
        ];

        $result = $this->json('POST', self::POST_INPUT_KOST_API_URL, $param);
        
        $result->assertOk();
        $result->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'messages' => [
                0 => 'Jumlah kamar harus lebih besar dari jumlah kamar tersedia',
            ]
        ]);
    }
}
