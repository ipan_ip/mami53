<?php

namespace App\Http\Controllers\Api\Me\GoldPlus;

use App\Entities\GoldPlus\Enums\GoldplusStatus;
use App\Entities\GoldPlus\Submission;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Illuminate\Foundation\Testing\TestResponse;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use Illuminate\Support\Facades\Config;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\Enums\InvoiceStatus;
use App\Entities\Mamipay\Invoice;
use App\Entities\Property\PropertyContractOrder;

class SubmissionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $oldGP1, $oldGP2, $oldGP3, $oldGP4;
    private $newGP1, $newGP2, $newGP3, $newGP4;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupGoldplusKostLevel();
    }

    private function setupGoldplusKostLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        //old oldGP
        $this->oldGP1 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1']);
        $this->oldGP2 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 2']);
        $this->oldGP3 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 3']);
        $this->oldGP4 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 4']);
        Config::set('kostlevel.id.goldplus1', $this->oldGP1->id);
        Config::set('kostlevel.id.goldplus2', $this->oldGP2->id);
        Config::set('kostlevel.id.goldplus3', $this->oldGP3->id);
        Config::set('kostlevel.id.goldplus4', $this->oldGP4->id);

        $this->newGP1 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 1 Promo']);
        $this->newGP2 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 2 Promo']);
        $this->newGP3 = factory(KostLevel::class)->create(['name' => 'Mamikos Goldplus 3 Promo']);
        Config::set('kostlevel.id.new_goldplus1', $this->newGP1->id);
        Config::set('kostlevel.id.new_goldplus2', $this->newGP2->id);
        Config::set('kostlevel.id.new_goldplus3', $this->newGP3->id);

        Config::set('kostlevel.ids.goldplus1', "{$this->oldGP1->id},{$this->newGP1->id}");
        Config::set('kostlevel.ids.goldplus2', "{$this->oldGP2->id},{$this->newGP2->id}");
        Config::set('kostlevel.ids.goldplus3', "{$this->oldGP3->id},{$this->newGP3->id}");
        Config::set('kostlevel.ids.goldplus4', "{$this->oldGP4->id}");
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    private function setUser(User $user)
    {
        app()->user = $user;
    }

    public function callGpSubmissions(): TestResponse
    {
        return $this->getJson('/api/v1/me/goldplus-submission');
    }

    public function callGpSubmissionsStatus(): TestResponse
    {
        return $this->getJson('/api/v1/me/goldplus-submission/status');
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionAllProcessed()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $submission = factory(Submission::class)->state('allProcessed')->create([
            'user_id' => $user,
        ]);
        $reponse = $this->callGpSubmissions();
        $reponse->assertJson([
            'status' => true,
            'submissions' => [],
        ]);
    }

    /**
     * @group UG
     * @group UG-4087
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionNotProcessedYet()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        $submission = factory(Submission::class)->create([
            'user_id' => $user,
        ]);
        $reponse = $this->callGpSubmissions();
        $reponse->assertJson([
            'status' => true,
            'submissions' => [
                [
                    'id' => $submission->id,
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithNoListing()
    {
        $user = factory(User::class)->create();
        $this->setUser($user);
        factory(Submission::class)->create(['user_id' => $user]);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => null
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithListing()
    {
        $user = factory(User::class)->create();
        $this->createRoom($user);
        $this->setUser($user);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::REGISTER
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithActiveSubmission()
    {
        $user = factory(User::class)->create();
        $this->createRoom($user);
        $this->setUser($user);
        factory(Submission::class)->create(['user_id' => $user]);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::REVIEW
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithOldGP1()
    {
        $user = factory(User::class)->create();
        $this->createRoom($user);
        $this->createGoldPlusRoom($user, $this->oldGP1->id);
        $this->setUser($user);
        factory(Submission::class)->create(['user_id' => $user]);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::GP1,
                    'label' => __('goldplus.status.submission.gp1')
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithOldGP2andGP4()
    {
        $user = factory(User::class)->create();
        $this->createRoom($user);
        $this->createGoldPlusRoom($user, $this->oldGP4->id);
        $this->createGoldPlusRoom($user, $this->oldGP2->id);
        $this->setUser($user);
        factory(Submission::class)->create(['user_id' => $user]);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::GP4
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithNewGP2()
    {
        $user = factory(User::class)->create();
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $this->setUser($user);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::GP2
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithNewGP2AndInvoice()
    {
        $user = factory(User::class)->create();
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $contractOrder = factory(PropertyContractOrder::class)->create([
            'owner_id' => $user->id,
            'package_type' => 'gp2'
        ]);

        $invoice = factory(Invoice::class)->create([
            'order_type' => PropertyContractOrder::ORDER_TYPE,
            'order_id' => $contractOrder->id
        ]);

        $this->setUser($user);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::WAITING,
                    'invoice_id' => $invoice->id
                ]
            ],
        ]);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Http\Controllers\Api\Me\GoldPlus\SubmissionController
     */
    public function testGetGpSubmissionStatusWithNewGP2AndPaidInvoice()
    {
        $user = factory(User::class)->create();
        $this->createGoldPlusRoom($user, $this->newGP2->id);
        $contractOrder = factory(PropertyContractOrder::class)->create([
            'owner_id' => $user->id,
            'package_type' => 'gp2'
        ]);

        $invoice = factory(Invoice::class)->create([
            'order_type' => PropertyContractOrder::ORDER_TYPE,
            'order_id' => $contractOrder->id,
            'status' => InvoiceStatus::PAID
        ]);

        $this->setUser($user);
        $reponse = $this->callGpSubmissionsStatus();
        $reponse->assertJson([
            'status' => true,
            'data' => [
                'gp_status' => [
                    'key' => GoldplusStatus::GP2,
                    'invoice_id' => null
                ]
            ],
        ]);
    }

    private function createRoom(User $user): Room {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified'
        ]);

        return $room;
    }

    private function createGoldPlusRoom(User $user, int $gpLevelId): Room
    {
        $room = factory(Room::class)->create();
        $room->changeLevel($gpLevelId);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified'
        ]);
        return $room;
    }
}
