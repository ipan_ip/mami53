<?php

namespace App\Http\Controllers\Api\Me\GoldPlus;

use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Property\PropertyContract;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Http\Controllers\Api\Me\GoldPlus\ContractController;
use App\Entities\Property\Property;
use Config;

class ContractControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const AVAILABLE_GP_GROUP = [1, 2, 3, 4];
    private const RESPONSE_STRUCTURE_ACTIVE_CONTRACT = [
        'status',
        'data' => [
            'show_label',
            'label_title',
            'label_description',
            'contract' => [
                'id',
                'name',
            ],
        ],
    ];

    private function callGetContracts()
    {
        return $this->getJson(action([ContractController::class, 'activeContract']));
    }

    private function createActingOwner(): User
    {
        app()->user = factory(User::class)->create();
        return app()->user;
    }

    private function createGpActiveContractProperty(User $user, int $gpGroup)
    {
        /** @var Property */
        $property = factory(Property::class)->state('withActiveContract')->create([
            'owner_user_id' => $user,
        ]);

        $this->configureGpLevel($property, $gpGroup);
    }

    private function createGpInactiveContractProperty(User $user, int $gpGroup)
    {
        /** @var Property */
        $property = factory(Property::class)->state('withInactiveContract')->create([
            'owner_user_id' => $user,
        ]);

        $this->configureGpLevel($property, $gpGroup);
    }

    private function configureGpLevel(Property $property, int $gpGroup)
    {
        /** @var PropertyContract */
        $propertyContract = $property->property_contracts->first();
        /** @var KostLevel */
        $kostLevel = factory(KostLevel::class)->create([
            'property_level_id' => $propertyContract->property_level_id,
        ]);

        Config::set('kostlevel.ids.goldplus' . $gpGroup, $kostLevel->id);
    }

    /**
     * @group UG
     * @group UG-4563
     */
    public function testHasGpActiveContract()
    {
        $user = $this->createActingOwner();
        $gpGroup = collect(self::AVAILABLE_GP_GROUP)->random();
        $this->createGpActiveContractProperty($user, $gpGroup);
        $response = $this->callGetContracts();
        
        $this->assertNotNull($response->json()['data']);
        $response->assertJsonStructure(self::RESPONSE_STRUCTURE_ACTIVE_CONTRACT);
    }

    /**
     * @group UG
     * @group UG-4563
     */
    public function testHasGpInactiveContract()
    {
        $user = $this->createActingOwner();
        $gpGroup = collect(self::AVAILABLE_GP_GROUP)->random();
        $this->createGpInactiveContractProperty($user, $gpGroup);
        $response = $this->callGetContracts();

        $this->assertNull($response->json()['data']['contract']);
    }

    /**
     * @group UG
     * @group UG-4563
     */
    public function testUnauthenticated()
    {
        app()->user = null;
        $response = $this->callGetContracts();
        $response->assertJson([
            'status' => false,
        ]);
    }
}
