<?php
namespace App\Http\Controllers\Api;

use App\Entities\Activity\ActivationCode;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Repositories\RoomRepository;
use App\Services\Thanos\ThanosService;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Mockery;
use phpmock\MockBuilder;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class OwnerControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const GET_OWNER_PROFILE_API = 'api/v1/owner/profile';
    private const GET_OWNER_NEWS_API = 'api/v1/owner/news';
    private const POST_OWNER_VERIFICATION_API = 'api/v1/owner/verification';
    private const POST_OWNER_VERIFICATION_REQUEST_API = 'api/v1/owner/verification/request';
    private const POST_OWNER_VERIFICATION_STORE_API = 'api/v1/owner/verification/store';
    private const POST_OWNER_VERIFICATION_CHECK_API = 'api/v1/owner/verification/check';
    private const POST_OWNER_EDIT_EMAIL_API = 'api/v1/owner/edit/email';
    private const GET_OWNER_EDIT_ROOM = 'api/v1/owner/edit';
    private const GET_OWNER_UPDATE_ROOM = 'garuda/owner/data/update';
    private const POST_OWNER_SET_PASSWORD_API = 'api/v1/owner/password/set';
    private const POST_OWNER_UPDATE_PASSWORD_API = 'api/v1/owner/password/update';
    private const GET_OWNER_CALL_ID_API = 'api/v1/owner/call';
    private const GET_OWNER_ONLINE_API = 'api/v1/owner/online';
    private const POST_OWNER_PINNED_API = 'api/v1/owner/pinned';
    private const GET_OWNER_CLAIM_ID_API = 'api/v1/owner/claim';
    private const POST_OWNER_CLAIM_POST_API = 'api/v1/owner/claim';
    private const POST_OWNER_DRAFT_API = 'api/v1/owner/draft';
    private const PUT_OWNER_AVAILABLE = 'api/v1/owner/available';
    private const GET_OWNER_LISTOWNER = 'api/v2/owner/list';

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\RoomRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetProfileOwnerSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'v' => 1
        ];
        $response = $this->json('GET', self::GET_OWNER_PROFILE_API);
        $response->assertOk();
        $response->assertJsonFragment([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetProfileOwnerSuccessWithValidJsonStructure()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'v' => 1
        ];
        $response = $this->json('GET', self::GET_OWNER_PROFILE_API);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'user' => [
                'user_id', 'is_verify', 'name', 'stories_count', 'phone_number', 'email',
                'chat_name', 'created_at', 'chat_setting', 'news', 'need_password',
            ],
            'membership' => [
                'status', 'active', 'expired', 'views_rp', 'views', 'active_package', 'package_name',
                'package_price', 'package_id', 'balance_id', 'balance_name', 'balance_price',
                'message_premium', 'countdown', 'cs_id',
            ],
            'promo',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetProfileOwnerWithoutLogin()
    {
        $owner = factory(User::class)->create();
        $this->actingAs($owner);

        $param = [
            'v' => 1
        ];

        $response = $this->json('GET', self::GET_OWNER_PROFILE_API)->json();
        $exceptionMessage = $response['message'];
        $type = $response['exception'];
        
        $this->assertEquals($exceptionMessage, 'Authorization: Sign-in is required.');
        $this->assertEquals($type, 'Exception');
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetNewsOwnerSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $this->mock->shouldReceive('getNewsOwner')->once()->andReturn([]);
        
        $response = $this->json('GET', self::GET_OWNER_NEWS_API);
        $response->assertOk();
        $response->assertJsonFragment([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetNewsOwnerWithoutLogin()
    {
        $response = $this->json('GET', self::GET_OWNER_NEWS_API)->json();

        $exceptionMessage = $response['message'];
        $type = $response['exception'];

        $this->assertEquals($exceptionMessage, 'Authorization: Sign-in is required.');
        $this->assertEquals($type, 'Exception');
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '08560154'.mt_rand(1111, 9999),
        ];
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJsonFragment([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Success',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1441
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithPhoneNumberAlreadyExists()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => $owner->phone_number,
        ];
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => __('api.input.phone_number.unique'),
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationSuccessWithValidJsonStructure()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '08560154'.mt_rand(1111, 9999),
        ];
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithParamPhoneNumberIsEmpty()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '',
        ];
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => __('api.input.phone_number.required')
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithParamPhoneNumberIsTooShort()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '0856'.mt_rand(111, 999),
        ];
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The phone number must be at least 8 characters.',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithParamPhoneNumberIsTooLong()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '0856'.mt_rand(111, 999).mt_rand(111, 999).mt_rand(111, 999).mt_rand(111, 999)
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The phone number may not be greater than 14 characters.'
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithParamPhoneNumberIsInvalid()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => mt_rand(11111, 9999999).mt_rand(1112, 9999)
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => __('api.input.phone_number.regex')
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostRequestVerificationWithTooFrequent()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'phone_number' => '085611118765'
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_REQUEST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00']),
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostStoreVerificationSuccess()
    {
        
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_STORE_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Success',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostStoreVerificationSuccessWithValidJsonStructure()
    {
        
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $owner->phone_number,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_STORE_API, $param);
        
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostStoreVerificationWithNotLoginYet()
    {
        $param = [
            'code' => str_random(13),
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_STORE_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Silakan login terlebih dahulu',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08561111'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Success',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithParamPhoneNumberIsTooShort()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'The phone number must be at least 8 characters.',
                    1 => __('api.input.phone_number.regex')
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithParamPhoneNumberIsTooLong()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '085677778987'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'The phone number may not be greater than 14 characters.',
                    1 => __('api.input.phone_number.regex')
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithParamPhoneNumberAlreadyExists()
    {
        $owner = factory(User::class)->states('owner')->create([
            'phone_number' => '081' . $this->faker()->randomNumber(9),
        ]);
        $this->actingAs($owner);

        $phoneNumber = $owner->phone_number;
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code,
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'The phone number has already been taken.',
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithInvalidCode()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08560987'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => $activationCode->code.str_random(3),
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'Kode tidak valid, harap periksa kembali.',
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithInvalidCodeType()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08560987'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => mt_rand(100, 999),
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'Kode tidak valid, harap periksa kembali.',
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithParamCodeIsTooShort()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08560987'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => mt_rand(1, 9),
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => 'Kode tidak valid, harap periksa kembali.',
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostVerificationCheckWithParamCodeIsEmpty()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $phoneNumber = '08560987'.mt_rand(1111, 9999);
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $phoneNumber,
            'for' => 'owner_verification',
            'expired_at' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
        ]);

        $param = [
            'code' => '',
            'phone_number' => $phoneNumber,
        ];

        $response = $this->json('POST', self::POST_OWNER_VERIFICATION_CHECK_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => [
                    0 => __('api.input.code.required'),
                ],
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testOwnerEditEmailSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'email' => str_random('13').'@'.str_random(10).'.com',
        ];

        $response = $this->json('POST', self::POST_OWNER_EDIT_EMAIL_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ]
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testOwnerEditEmailWithInvalidEmail()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'email' => 'aaa',
        ];

        $response = $this->json('POST', self::POST_OWNER_EDIT_EMAIL_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'email' => false,
            'message' => 'Tidak sesuai dengan format email.',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testOwnerEditEmailWithInvalidEmailAndValidJsonStructure()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'email' => 'aaa',
        ];

        $response = $this->json('POST', self::POST_OWNER_EDIT_EMAIL_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'email',
            'message',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testOwnerEditEmailWithNotLoginYet()
    {
        $param = [
            'email' => 'aaa',
        ];

        $response = $this->json('POST', self::POST_OWNER_EDIT_EMAIL_API, $param);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'email' => false,
            'message' => 'Gagal mengganti email',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordSuccess()
    {
        $owner = factory(User::class)->states('owner')->create([
            'password' => '',
        ]);
        $this->actingAs($owner);

        $param = [
            'password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordSuccessWithValidJsonStructure()
    {
        $owner = factory(User::class)->states('owner')->create([
            'password' => '',
        ]);
        $this->actingAs($owner);

        $param = [
            'password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordWithNotLoginYet()
    {
        $param = [
            'password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Silakan login terlebih dahulu',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordWithParamPasswordIsTooShort()
    {
        $owner = factory(User::class)->states('owner')->create([
            'password' => '',
        ]);
        $this->actingAs($owner);

        $param = [
            'password' => str_random(5),
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password minimal 6 karacter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordWithParamPasswordIsEmpty()
    {
        $owner = factory(User::class)->states('owner')->create([
            'password' => '',
        ]);
        $this->actingAs($owner);

        $param = [
            'password' => null,
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password harus diisi',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testSetPasswordWithParamPasswordIsAlreadyExists()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = [
            'password' => str_random(7),
        ];

        $response = $this->json('POST', self::POST_OWNER_SET_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Anda sudah punya password sebelumnya',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordSuccess()
    {
        $plainOldPassword = str_random(10);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);

    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithOldPasswordHasInvalidValue()
    {
        $plainOldPassword = str_random(10);
        $oldPassword = Hash::make(str_random(10));
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama Anda tidak valid.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithOldPasswordIsTooShort()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama minimal 6 karakter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNewPasswordIsTooShort()
    {
        $plainOldPassword = str_random(13);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(5),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Baru minimal 6 karakter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNewAndOldPasswordIsTooShort()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(5),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama minimal 6 karakter. Password Baru minimal 6 karakter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithOldPasswordIsEmpty()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => null,
            'new_password' => str_random(15),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama harus diisi',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNewPasswordIsEmpty()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => $oldPassword,
            'new_password' => null,
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Baru harus diisi',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithOldPasswordIsEmptyAndNewPasswordIsTooShort()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => null,
            'new_password' => str_random(3),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama harus diisi. Password Baru minimal 6 karakter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNewPasswordIsEmptyAndOldPasswordIsTooShort()
    {
        $plainOldPassword = str_random(3);
        $oldPassword = Hash::make($plainOldPassword);
        $owner = factory(User::class)->states('owner')->create([
            'password' => $oldPassword,
        ]);
        $this->actingAs($owner);
        
        $param = [
            'old_password' => str_random(3),
            'new_password' => str_random(13),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Password Lama minimal 6 karakter',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNotLoginYet()
    {
        $plainOldPassword = str_random(10);
        $oldPassword = Hash::make($plainOldPassword);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Silakan login terlebih dahulu',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testUpdatePasswordWithNotLoginYetAndValidJsonStructure()
    {
        $plainOldPassword = str_random(10);
        $oldPassword = Hash::make($plainOldPassword);
        
        $param = [
            'old_password' => $plainOldPassword,
            'new_password' => str_random(16),
        ];

        $response = $this->json('POST', self::POST_OWNER_UPDATE_PASSWORD_API, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetListCallRoomSuccess()
    {
        $owner = factory(User::class)->states('owner')->make();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $response = $this->json('GET', self::GET_OWNER_CALL_ID_API.'/'.$songId);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'call' => [],
            'clickable' => 'Untuk melihat nomor hp anda harus premium',
            'feature' => 'list histori dan telp dengan user , silakan upgrade akun anda menjadi premium.',
            'mamikos_phone' => '+6287739222850',
            'count' => 0,
            'page_total' => 1,
            'email' => true,
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetListCallRoomSuccessWithValidJsonStructure()
    {
        $owner = factory(User::class)->states('owner')->make();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $response = $this->json('GET', self::GET_OWNER_CALL_ID_API.'/'.$songId);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'call',
            'clickable',
            'feature',
            'mamikos_phone',
            'count',
            'page_total',
            'email',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetListCallRoomWithNotLoginYet()
    {
        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $response = $this->json('GET', self::GET_OWNER_CALL_ID_API.'/'.$songId);
        $response->assertOk();
        $response->assertJson([
            'call' => [],
            'clickable' => 'Untuk melihat nomor hp anda harus premium',
            'feature' => 'list histori dan telp dengan user , silakan upgrade akun anda menjadi premium.',
            'mamikos_phone' => '+6287738724848',
            'count' => 0,
            'email' => false,
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetListCallRoomWithNotLoginYetWithValidJsonStructure()
    {
        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $response = $this->json('GET', self::GET_OWNER_CALL_ID_API.'/'.$songId);
        $response->assertJsonStructure([
            'call',
            'clickable',
            'feature',
            'mamikos_phone',
            'count',
            'email',
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testInsertOnlineDataSuccess()
    {
        $owner = factory(User::class)->states('owner')->make();
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_OWNER_ONLINE_API);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testInsertOnlineDataIsNotLoginYet()
    {
        $response = $this->json('GET', self::GET_OWNER_ONLINE_API);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPinnedActionSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = ['status' => 'true'];
        $response = $this->json('POST', self::POST_OWNER_PINNED_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetClaimOwnerSuccess()
    {
        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = ['status' => 'true'];
        $response = $this->json('GET', self::GET_OWNER_CLAIM_ID_API.'/'.$songId);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
        ]);
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testGetClaimOwnerFail()
    {
        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $param = ['status' => 'true'];
        $response = $this->json('GET', self::GET_OWNER_CLAIM_ID_API.'/'.$songId.mt_rand(1, 9))->json();
        $this->assertEquals($response['message'], 'No query results for model [App\Entities\Room\Room].');
        $this->assertEquals($response['exception'], 'Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostClaimOwnerSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $param = [
            '_id' => $songId,
            'status' => 'true',
        ];
        $response = $this->json('POST', self::POST_OWNER_CLAIM_POST_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data' => 'Klaim disimpan'
        ]);
    }

    public function testEditRoomOwnerWithInvalidSongId()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);

        $url = self::GET_OWNER_EDIT_ROOM."/$songId";
        $response = $this->json('GET', $url)->json();
        $this->assertEquals($response['meta']['message'],'Kost tidak ditemukan');
        $this->assertEquals($response['meta']['messages'][0],'Kost tidak ditemukan');
    }

    public function testEditRoomOwnerWithValidSongId()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $tags = [];

        collect([
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ])->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });
        
        $room = factory(Room::class)->create();

        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->mock->shouldReceive('editJson')->once();

        $url = self::GET_OWNER_EDIT_ROOM."/$room->song_id";
        $response = $this->json('GET', $url);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'room',
            'incomplete'
        ]);
    }

    public function testGetKostDataForUpdateWithInvalidUser()
    {
        $songId = rand(1,999);
        $url = self::GET_OWNER_UPDATE_ROOM."/$songId";
        $response = $this->json('GET', $url)->json();

        $this->assertEquals($response['meta']['message'],'User tidak ditemukan');
        $this->assertEquals($response['meta']['messages'][0],'User tidak ditemukan');
    }

    public function testGetKostDataForUpdateWithInvalidSongId()
    {   
        $owner = factory(User::class)->states('owner')->create();

        $this->actingAs($owner);

        $songId = rand(1,999);
        $url = self::GET_OWNER_UPDATE_ROOM."/$songId";
        $response = $this->json('GET', $url)->json();

        $this->assertEquals($response['meta']['message'],'Kost tidak ditemukan');
        $this->assertEquals($response['meta']['messages'][0],'Kost tidak ditemukan');
    }

    public function testGetKostDataForUpdateWithInvalidOwner()
    {   
        $owner = factory(User::class)->states('owner')->create();

        $this->actingAs($owner);

        $room = factory(Room::class)->create();

        $url = self::GET_OWNER_UPDATE_ROOM."/$room->song_id";
        $response = $this->json('GET', $url)->json();

        $this->assertEquals($response['meta']['message'],'Anda bukan pemilik dari kost ini');
        $this->assertEquals($response['meta']['messages'][0],'Anda bukan pemilik dari kost ini');
    }

    public function testGetKostDataForUpdateWithValidData()
    {   
        $owner = factory(User::class)->states('owner')->create();        
        $this->actingAs($owner);

        $room = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $url = self::GET_OWNER_UPDATE_ROOM."/$room->song_id";
        $response = $this->json('GET', $url);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data' => [
                'name','slug','address','latitude','longitude','area_city',
                'area_subdistrict','gender','size','room_count','room_available',
                'facilities','price_remark','min_month','description','remarks',
                'photos','youtube_link','owner_name','owner_phone','owner_email',
                'building_year','price_daily','price_weekly','price_monthly',
                'price_3_month','price_6_month','price_yearly','is_price_flash_sale',
                ]
        ]);

    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostClaimOwnerWithIdParamIsEmpty()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $param = [
            '_id' => '',
            'status' => 'true',
        ];
        $response = $this->json('POST', self::POST_OWNER_CLAIM_POST_API, $param)->json();
        $this->assertEquals($response['message'], 'Input: Validation Error. _id : The  id field is required. ');
        $this->assertEquals($response['exception'], 'Exception');
    }

    /**
     * @group App/Http/Controllers/Api/OwnerController
     */
    public function testPostClaimOwnerWithStatusParamIsEmpty()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $songId = mt_rand(1, 99999);
        factory(Room::class)->create(['song_id' => $songId]);

        $param = [
            '_id' => $songId,
            'status' => null,
        ];
        $response = $this->json('POST', self::POST_OWNER_CLAIM_POST_API, $param)->json();
        $this->assertEquals($response['message'], 'Input: Validation Error. status : The status field is required. ');
        $this->assertEquals($response['exception'], 'Exception');
    }

    public function testPutOwnerAvailableWithoutRoom()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . 123, [])->json();

        $this->assertContains('Kost / Apartemen tidak ditemukan', $response['meta']['message']);
    }

    public function testPutOwnerAvailableWithoutParam()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => 'verified'
        ]);
        
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, [])->json();

        $this->assertContains('Ketersediaan Ruangan harus diisi', $response['messages']);
    }

    public function testPutOwnerAvailableWithMandatoryParam()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => 'verified'
        ]);
        
        $param = [
            "room_available"    => 10,
            "price_monthly"     => 120000,
            "price_yearly"      => 0,
            "price_weekly"      => 0,
            "price_daily"       => 0,
            "3_month"           => 0,
            "6_month"           => 0
        ];
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, $param)->json();

        $this->assertTrue($response['status']);
        $this->assertEquals(120000, $response['price_monthly_time']);
    }

    public function testPutOwnerAvailableWithThanos()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'is_active' => 'false'
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'verified'
        ]);
        factory(ThanosHidden::class)->create([
            'designer_id'   => $room->id,
            'snapped'       => true,
            'log'           => []
        ]);
        
        $param = [
            "room_available"    => 10,
            "price_monthly"     => 125000,
            "price_yearly"      => 0,
            "price_weekly"      => 0,
            "price_daily"       => 0,
            "3_month"           => 0,
            "6_month"           => 0
        ];
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, $param)->json();

        $updatedRoom = Room::find($room->id);
        
        $this->assertEquals('true', $updatedRoom->is_active);
        $this->assertTrue($response['status']);
        $this->assertEquals(125000, $response['price_monthly_time']);
    }

    public function testPutOwnerAvailableWithPriceTypeUsd()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => 1,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => 'verified'
        ]);
        
        $param = [
            "room_available"    => 1,
            "price_monthly"     => 120000,
            "price_yearly"      => 0,
            "price_weekly"      => 0,
            "price_daily"       => 0,
            "price_type"        => "usd"
        ];
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, $param)->json();

        $this->assertContains('Currency usd not found', $response['meta']['message']);
    }

    public function testPutOwnerAvailableWithActiveFlashSaleSuccess()
    {
        $room = $this->setRoomWithFlashSale('annually');

        $this->setRunningFlashSaleData('2');
        
        $param = [
            "room_available"    => 10,
            "price_monthly"     => 1200000,
            "price_yearly"      => 2500000,
            "price_weekly"      => 0,
            "price_daily"       => 0,
            "3_month"           => 0,
            "6_month"           => 0
        ];
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, $param)->json();

        $this->assertTrue($response['status']);
        $this->assertEquals(1200000, $response['price_monthly_time']);
    }

    public function testPutOwnerAvailableWithActiveFlashSaleFailed()
    {
        $room = $this->setRoomWithFlashSale('annually');

        $this->setRunningFlashSaleData('2');
        
        $param = [
            "room_available"    => 10,
            "price_monthly"     => 1200000,
            "price_yearly"      => 12000000,
            "price_weekly"      => 0,
            "price_daily"       => 0,
            "3_month"           => 0,
            "6_month"           => 0
        ];
        $response = $this->json('PUT', self::PUT_OWNER_AVAILABLE . '/' . $room->song_id, $param)->json();

        $this->assertContains('Promo ngebut aktif, tidak bisa update', $response['messages']);
    }

    public function testPostClaimRoomByOwnerDeniedOnHavingNotSignedUser()
    {
        $result = $this->post('garuda/owner/claim');
        $result->isOk();
        $this->assertEquals("Access Denied", $result->getData()->message);
    }

    public function testPostClaimRoomByOwnerDeniedOnHavingNotOwnerUser()
    {

        $user = factory(User::class)->state('tenant')->create();
        $this->actingAs($user);

        $result = $this->post('garuda/owner/claim');
        $result->isOk();
        $this->assertEquals("Access Denied", $result->getData()->message);
    }

    public function testPostClaimRoomByOwnerForwardedAsOwner()
    {
        $mockController = $this->mockPartialAlternatively(OwnerController::class);
        $mockController->shouldReceive('postClaimOwner')->once()->andReturn(['data' => 'oke']);

        $user = factory(User::class)->state('owner')->create();
        $this->actingAs($user);

        $result = $this->post('garuda/owner/claim');
        $this->assertEquals('oke', $result->getData()->data);
    }

    public function testGetListOwnerWithUserIsNull()
    {
        $response = $this->json('GET', self::GET_OWNER_LISTOWNER)->json();

        $exceptionMessage = $response['message'];
        $type = $response['exception'];

        $this->assertEquals($exceptionMessage, 'Authorization: Sign-in is required.');
        $this->assertEquals($type, 'Exception');
    }

    public function testGetListOwnerWithValidUser()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            ]);
        $this->actingAs($user);

        $response = $this->json('GET', self::GET_OWNER_LISTOWNER);
        
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'rooms' => [ 
                [
                    "_id", "apartment_project_id", "price_title_time", "room_title",
                    "address", "has_round_photo", "gender", "status", "min_month",
                    "photo_url" => [
                      "real",
                      "small",
                      "medium",
                      "large",
                    ],
                    "status_kost", "incomplete", "kamar_available", "view_count", "view_ads_count",
                    "love_count", "view_promote", "used_promote", "percen_promote",
                    "status_promote","message_premium","message_count","before_promote",
                    "progress_promote", "progress_promote_rp", "promote_saldo", "survey_count",
                    "review_count", "call_count", "available_survey_count", "price_daily",
                    "price_weekly_time", "price_monthly_time", "price_yearly_time", 
                    "telp_count", "slug","click_price","promotion","can_promotion","promo_title",
                    "reject_remark", "is_booking",
                ]
            ],
            'count'
        ]);
    }


    private function setRoomWithFlashSale($rentType)
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'price_monthly' => 1000000,
            'price_yearly' => 10000000,
            'latitude' => -8.014663865095645,
            'longitude' => 110.2936449049594,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => 'verified'
        ]);
        factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => $rentType,
            'price' => 2500000,
            'markup_type' => 'percentage',
            'markup_value' => 20,
            'discount_type' => 'percentage',
            'discount_value' => 10,
            'is_active' => 1
        ]);

        return $room;
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }

    public function testGetRoomHasBookingRequestAccessDenied()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $request = $this->get('api/v1/owner/booking/room/requests');
        $result = $request->getOriginalContent();
        $this->assertEquals(false, $result['status']);
        $this->assertEquals('Access Denied', $result['message']);
    }

    public function testGetRoomHasBookingRequestTrue()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);
        $this->actingAs($user);

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);
        factory(DbetLinkRegistered::class)->create([
            'status' => DbetLinkRegistered::STATUS_PENDING,
            'designer_id' => $room->id
        ]);

        $request = $this->get('api/v1/owner/booking/room/requests');
        $result = $request->getOriginalContent();
        $this->assertNotNull($result);
        $this->assertEquals(true, $result['status']);
        $this->assertIsArray($result['data']);
    }
}
