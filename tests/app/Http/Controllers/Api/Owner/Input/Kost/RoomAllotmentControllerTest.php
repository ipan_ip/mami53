<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Room\DataStage;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class RoomAllotmentControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.room.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.room.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.room.edit.put';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreIncompleteParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'size' => '4x4'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreFailedParam()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'size'              => '4x4',
                'room_count'        => 10,
                'room_available'    => 15
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'size'              => '4x4',
                'room_count'        => 10,
                'room_available'    => 5
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['size' => '4x4']);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_ROOM_ALLOTMENT, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testGetSuccess()
    {
        $room = $this->createRoom();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
    }

    /**
     * @group PMS-495
     */    
    public function testEditInvalidParams()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'size'              => '4.15x4',
                'room_count'        => 10,
                'room_available'    => 5,
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'size'              => '4x4.6',
                'room_count'        => 10,
                'room_available'    => 5,
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditSuccess()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'size'              => '5.5x5',
                'room_count'        => 10,
                'room_available'    => 5,
            ]
        );

        $response->assertOk();
        $this->assertEquals('5.5x5', $response->getData()->room->size);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_ROOM_ALLOTMENT, 'complete' => true, 'designer_id' => $room->id]);
    }

    private function createRoom()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->owner->id
        ]);

        return $room;
    }

    private function jsonStructure()
    {
        return [
            'status',
            'room' => [
                '_id',
                'size',
                'room_count',
                'room_available',
            ]
        ];
    }
}
