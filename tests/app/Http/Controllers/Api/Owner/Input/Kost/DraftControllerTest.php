<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Media\Media;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Element\TagType;
use App\User;

class DraftControllerTest extends MamiKosTestCase
{
    const DEFAULT_STAGES_ROUTE = 'oauth.user-act.owner.data.kos.input.draft.get.stage';
    const BY_ID_STAGES_ROUTE = 'oauth.user-act.owner.data.kos.input.draft.get.stage.by-id';

    const DRAFT_FLOW = [
        DataStage::STAGE_DATA => 'oauth.user-act.owner.data.kos.input.information.store',
        DataStage::STAGE_ADDRESS => 'oauth.user-act.owner.data.kos.input.address.edit.put',
        DataStage::STAGE_KOS_PHOTO => 'oauth.user-act.owner.data.kos.input.kost-photo.edit.put',
        DataStage::STAGE_ROOM_PHOTO => 'oauth.user-act.owner.data.kos.input.room-photo.edit.put',
        DataStage::STAGE_FACILITY => 'oauth.user-act.owner.data.kos.input.facilities.edit.put',
        DataStage::STAGE_ROOM_ALLOTMENT => 'oauth.user-act.owner.data.kos.input.room.edit.put',
        DataStage::STAGE_PRICE => 'oauth.user-act.owner.data.kos.input.price.edit.put',
    ];

    /**
     * @group PMS-495
     */    
    public function testMiddlewareRedirectedWhenNoUser()
    {
        $result = $this->get(
            route(self::DEFAULT_STAGES_ROUTE)
        );
        $result->assertRedirect();
    }

    /**
     * @group PMS-495
     */    
    public function testMiddlewareRedirectedWhenNotOwner()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'passport');
        $result = $this->get(
            route(self::DEFAULT_STAGES_ROUTE)
        );
        $result->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testShowDefaultStages()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $this->actingAs($user, 'passport');

        $result = $this->get(
            route(self::DEFAULT_STAGES_ROUTE)
        );

        $result->assertSuccessful();
        $result->assertJsonStructure([
            'status',
            'meta',
            'stages' => [
                DataStage::STAGE_ADDRESS,
                DataStage::STAGE_DATA,
                DataStage::STAGE_KOS_PHOTO,
                DataStage::STAGE_ROOM_PHOTO,
                DataStage::STAGE_FACILITY,
                DataStage::STAGE_ROOM_ALLOTMENT,
                DataStage::STAGE_PRICE,
            ]
        ]);
    }

    /**
     * @group PMS-495
     */    
    public function testShowStagesByIdRoomNotFound()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $this->actingAs($user, 'passport');

        $result = $this->get(
            route(self::BY_ID_STAGES_ROUTE, ['songId' => 888])
        );
        $result->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testShowStagesByIdRoomNotOwned()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $room = factory(Room::class)->states([
            'active',
            'with-small-rooms',
            'attribute-complete-state',
            'with-owner-collumn',
        ])->create();
        $this->actingAs($user, 'passport');

        $result = $this->get(
            route(self::BY_ID_STAGES_ROUTE, ['songId' => $room->song_id])
        );
        $result->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testShowStagesByIdSuccess()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $room = factory(Room::class)->states([
            'active',
            'with-small-rooms',
            'attribute-complete-state',
            'with-owner-collumn',
        ])->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $this->actingAs($user, 'passport');

        $result = $this->get(
            route(self::BY_ID_STAGES_ROUTE, ['songId' => $room->song_id])
        );

        $result->assertSuccessful();
        $result->assertJsonStructure([
            'status',
            'meta',
            'stages' => [
                DataStage::STAGE_DATA,
                DataStage::STAGE_ADDRESS,
                DataStage::STAGE_KOS_PHOTO,
                DataStage::STAGE_ROOM_PHOTO,
                DataStage::STAGE_FACILITY,
                DataStage::STAGE_ROOM_ALLOTMENT,
                DataStage::STAGE_PRICE,
            ],
            'is_active_photo_booking',
            'duplicate_from',
            'creation_flag'
        ]);
    }

    /**
     * @large
     * @group PMS-495
     */
    public function testDraftProgessChanges()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $this->actingAs($user, 'passport');

        factory(Subdistrict::class)->create([
            'area_city_id' => factory(City::class)->create([
                'area_province_id' => factory(Province::class)->create([
                    'name' => 'Jawa Timur'
                ])->id,
                'name' => 'Kota Malang'
            ])->id,
            'name' => 'Kecamatan Lowokwaru'
        ]);

        $response = $this->post(
            route(self::DRAFT_FLOW[DataStage::STAGE_DATA]),
            [
                'gender'    => 1,
                //'name'      => 'Kost Testing Draft Flow',
                'type_name' => 'Draft Flow',
                'description' => 'Kost Menakjubkan dan mevvah dengan fitur gudang',
                'property' => [
                    'name' => 'Kost Testing',
                    'allow_multiple' => true
                ]
            ]
        );
        $response->assertSuccessful();
        
        $roomSongId = $response->getData()->room->_id;
        $roomId = Room::select('id')->where('song_id', $roomSongId)->first()->id;

        $response = $this->get(
            route(self::BY_ID_STAGES_ROUTE, ['songId' => $roomSongId])
        );
        $response->assertSuccessful();
        $this->assertEquals(CreationFlag::CREATE_NEW, $response->getData()->creation_flag);

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_DATA, 'complete' => true, 'designer_id' => $roomId]
        );

        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_ADDRESS], ['songId' => $roomSongId]),
            [
                'address' => 'Perumahan Permata Jingga, Jl. Permata Jingga I, Tunggulwulung, Kec. Lowokwaru, Kota Malang, Jawa Timur 65149',
                'latitude' => -7.931561495562611,
                'longitude' => 112.61833398255018,
                'province' => 'Jawa Timur',
                'city' => 'Kota Malang',
                'subdistrict' => 'Kecamatan Lowokwaru'
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true, 'designer_id' => $roomId]
        );
        
        $frontBuilding = factory(Media::class, 3)->create();
        $insideBuilding = factory(Media::class, 3)->create();
        $roadviewBuilding = factory(Media::class, 3)->create();

        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_KOS_PHOTO], ['songId' => $roomSongId]),
            [
                'building_front' => $frontBuilding->pluck('id')->toArray(),
                'inside_building' => $insideBuilding->pluck('id')->toArray(),
                'roadview_building' => $roadviewBuilding->pluck('id')->toArray(),
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_KOS_PHOTO, 'complete' => true, 'designer_id' => $roomId]
        );

        $frontRoom = factory(Media::class, 3)->create();
        $insideRoom = factory(Media::class, 3)->create();
        $bathroom = factory(Media::class, 3)->create();
        $other = factory(Media::class, 3)->create();

        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_ROOM_PHOTO], ['songId' => $roomSongId]),
            [
                'cover' => $insideRoom->first()->id,
                'room_front' => $frontRoom->pluck('id')->toArray(),
                'inside_room' => $insideRoom->pluck('id')->toArray(),
                'bathroom' => $bathroom->pluck('id')->toArray(),
                'other' => $other->pluck('id')->toArray(),
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true, 'designer_id' => $roomId]
        );

        $tags = $this->prepareTags();
        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_FACILITY], ['songId' => $roomSongId]),
            [
                'public' => collect(array_merge($tags['fac_share'], $tags['fac_near']))->pluck('id')->toArray(),
                'bedroom' => collect($tags['fac_room'])->pluck('id')->toArray(),
                'bathroom' => collect($tags['fac_bath'])->pluck('id')->toArray(),
                'parking' => collect($tags['fac_park'])->pluck('id')->toArray()
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_FACILITY, 'complete' => true, 'designer_id' => $roomId]
        );
        $this->assertDatabaseHas(
            (new RoomTag())->getTable(),
            ['designer_id' => $roomId]
        );

        $roomAmmount = 12;
        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_ROOM_ALLOTMENT], ['songId' => $roomSongId]),
            [
                'size' => '3x3',
                'room_count' => $roomAmmount,
                'room_available' => $roomAmmount,
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_ALLOTMENT, 'complete' => true, 'designer_id' => $roomId]
        );
        $this->assertDatabaseHas(
            (new RoomUnit())->getTable(),
            ['designer_id' => $roomId]
        );
        
        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_PRICE], ['songId' => $roomSongId]),
            [
                'price' => [
                    'daily' => 80000,
                    'weekly' => 550000,
                    'monthly' => 1800000,
                    'quarterly' => 75000000,
                    'semiannually' => 12000000,
                    'yearly' => 20000000,
                ],
                // 'additional_cost' => [
                //     [
                //         'cost_name' => 'Listrik',
                //         'cost_price' => 50000
                //     ],
                //     [
                //         'cost_name' => 'Parkir',
                //         'cost_price' => 20000
                //     ]
                // ],
                // 'deposit_fee' => 200000,
                // 'down_payment' => 10
            ]
        );
        $response->assertSuccessful();

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $roomId]
        );

        $room = Room::with([
            'owners',
            'address_note',
            'cards.photo',
            'tags' => function ($query) {
                $query->with('types')->whereHas(
                    'types',
                    function ($query) {
                        $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                    }
                );
            },
            'room_unit',
            'data_stage'
        ])->where('song_id', $roomSongId)->first();

        $this->assertEquals(RoomOwner::ROOM_EARLY_EDIT_STATUS, $room->owners->first()->status);
        $this->assertCount(count(DataStage::AVAILABLE_STAGES), $room->data_stage);
        $this->assertCount($roomAmmount, $room->room_unit);
    }

    /**
     * @large
     * @group PMS-495
     */
    public function testVerifiedRoomProgessChangesToDraft()
    {
        $user = factory(User::class)->states(['owner'])->create();
        $room = factory(Room::class)->states([
            'active',
            'with-small-rooms',
            'attribute-complete-state',
            'with-owner-collumn',
        ])->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $this->actingAs($user, 'passport');

        $response = $this->put(
            route(self::DRAFT_FLOW[DataStage::STAGE_PRICE], ['songId' => $room->song_id]),
            [
                'price' => [
                    'daily' => 80000,
                    'weekly' => 550000,
                    'monthly' => 1800000,
                    'quarterly' => 75000000,
                    'semiannually' => 12000000,
                    'yearly' => 20000000,
                ]
            ]
        );
        $response->assertSuccessful();
        $room->load('owners');
        $this->assertEquals(RoomOwner::ROOM_EDIT_STATUS, $room->owners->first()->status);
    }

    private function prepareTags(
        ?array $source = null
    ): array {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }
}
