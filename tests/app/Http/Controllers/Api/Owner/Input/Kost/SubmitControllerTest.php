<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Media\Media;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Element\Verification;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class SubmitControllerTest extends MamiKosTestCase
{
    const SUBMIT_ROUTE = 'oauth.user-act.owner.data.kos.input.submit.put';
    const READY_TO_VERIF_ROUTE = 'oauth.user-act.owner.data.kos.input.submit.mark-as-ready';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitModelNotFound()
    {
        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => 123])
        );

        $response->assertStatus(404);
        $this->assertEquals('NOT FOUND', $response->getData()->meta->severity);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitNotOwnerReturn403()
    {
        $room = factory(Room::class)->create();

        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertStatus(403);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomNotDraft()
    {
        $room = $this->createRoom(RoomOwner::ROOM_EDIT_STATUS);
        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertStatus(400);
        $this->assertEquals('Kost status not draft.', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomNotMamipay()
    {
        $room = $this->createRoom();
        $this->createMedia($room);

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));
        
        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertStatus(400);
        $this->assertEquals('Mamipay profile not found.', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomStageNotComplete()
    {
        $room = $this->createRoom();
        $this->registerMamipay();
        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );
        
        $response->assertStatus(400);
        $this->assertEquals('Stage not complete.', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomStageFlagCreateNewFails()
    {
        $room = $this->createRoom();
        $this->createMedia($room, [Group::BUILDING_FRONT, Group::ROOM_FRONT,Group::INSIDE_ROOM]);

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->registerMamipay();

        factory(CreationFlag::class)->create([
            'designer_id' => $room->id,
            'create_from' => CreationFlag::CREATE_NEW,
            'log' => [],
        ]);

        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertStatus(400);

        $roomOwner = RoomOwner::where('designer_id', $room->id)->get()->first();
        $this->assertEquals(RoomOwner::ROOM_EARLY_EDIT_STATUS, $roomOwner->status);
        $this->assertEquals('Stage not complete.', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomStageFlagOldDataSuccess()
    {
        $room = $this->createRoom();
        $this->createMedia($room, [Group::BUILDING_FRONT, Group::ROOM_FRONT,Group::INSIDE_ROOM]);

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->registerMamipay();

        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertSuccessful();

        $roomOwner = RoomOwner::where('designer_id', $room->id)->get()->first();
        $this->assertEquals(
            RoomOwner::ROOM_ADD_STATUS,
            $roomOwner->status,
            "old data have no photo kos section completed is still okay"
        );
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomStageFlagDuplicateSuccess()
    {
        $room = $this->createRoom();
        $this->createMedia($room, [Group::BUILDING_FRONT, Group::ROOM_FRONT,Group::INSIDE_ROOM]);

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->registerMamipay();

        factory(CreationFlag::class)->create([
            'designer_id' => $room->id,
            'create_from' => CreationFlag::DUPLICATE_FROM_TYPE,
            'log' => [],
        ]);

        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertSuccessful();

        $roomOwner = RoomOwner::where('designer_id', $room->id)->get()->first();
        $this->assertEquals(
            RoomOwner::ROOM_ADD_STATUS,
            $roomOwner->status,
            "old data have no photo kos section completed is still okay"
        );
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitRoomStageSuccess()
    {
        $room = $this->createRoom();
        $this->createMedia($room);

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->registerMamipay();

        factory(CreationFlag::class)->create([
            'designer_id' => $room->id,
            'create_from' => CreationFlag::CREATE_NEW,
            'log' => [],
        ]);

        $response = $this->put(
            route(self::SUBMIT_ROUTE, ['songId' => $room->song_id])
        );

        $response->assertOk();

        $roomOwner = RoomOwner::where('designer_id', $room->id)->get()->first();
        $this->assertEquals(RoomOwner::ROOM_ADD_STATUS, $roomOwner->status);
        $this->assertDatabaseHas(
            (new BookingOwnerRequest())->getTable(),
            ['designer_id' => $room->id, 'status' => BookingOwnerRequest::BOOKING_WAITING]
        );
    }

    /**
     * @group PMS-495
     */
    public function testMarkReadySuccess()
    {
        $room = $this->createRoom();

        $response = $this->post(route(self::READY_TO_VERIF_ROUTE, ['songId' => $room->song_id]));

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Verification())->getTable(), ['designer_id' => $room->id, 'is_ready' => true]);
        $response->assertJsonStructure($this->markReadyResponseStructure());

    }

    /**
     * @group PMS-495
     */
    public function testMarkReadyNotOwnedFailed()
    {
        $room = $this->createRoom();

        $response = $this->actingAs(factory(User::class)->state('owner')->create())
            ->post(route(self::READY_TO_VERIF_ROUTE, ['songId' => $room->song_id]));

        $response->assertForbidden();

    }

    /**
     * @group PMS-495
     */
    public function testMarkReadyNotFound()
    {
        $response = $this->post(route(self::READY_TO_VERIF_ROUTE, ['songId' => 0]));

        $response->assertNotFound();

    }

    private function createRoom(string $status = RoomOwner::ROOM_EARLY_EDIT_STATUS)
    {
        $media = factory(Media::class)->create();
        $room = factory(Room::class)
            ->states([
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
                'with-address-note'
            ])
            ->create([
                'photo_id' => $media->id
            ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->owner->id,
            'status' => $status,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ]);

        $this->createCard($room, $media, Group::INSIDE_ROOM, true);

        return $room;
    }

    private function createMedia(
        Room $room,
        array $groups = [
            Group::BUILDING_FRONT,
            Group::INSIDE_BUILDING,
            Group::ROADVIEW_BUILDING,
            Group::ROOM_FRONT,
            Group::INSIDE_ROOM
        ]
    ): void {
        foreach ($groups as $group) {
            $this->createCard(
                $room,
                factory(Media::class)->create(),
                $group,
                false
            );
        }
    }

    private function createCard(Room $room, Media $media, string $group, bool $isCover)
    {
        return factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $room->id,
            'description' => $isCover ? Card::DESCRIPTION_PREFIX['cover'] : Card::DESCRIPTION_PREFIX[$group] . '-1',
            'group' => $group,
            'is_cover' => $isCover
        ]);
    }
    private function prepareTags(?array $source = null): array
    {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }

    private function registerMamipay()
    {
        return factory(MamipayOwner::class)->create([
            'user_id' => $this->owner->id,
            'status' => MamipayOwner::STATUS_APPROVED
        ]);
    }

    private function markReadyResponseStructure()
    {
        return [
            'status',
            'meta'
        ];
    }
}
