<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Input\Kost\KostPhotoTransformer;
use App\User;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class KostPhotoControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.kost-photo.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.kost-photo.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.kost-photo.edit.put';

    private $owner;

    /**
     * @group PMS-495
     */    
    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreUnValidParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'building_front' => [110],
                'inside_building' => 'not-array',
                'roadview_building' => [111],
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreLackingParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'building_front' => [],
                'inside_building' => [112],
                'roadview_building' => [111],
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreThrowExceptionReturn400()
    {
        app()->user = $this->owner;

        $mockService = $this->mock(InputService::class);
        $mockService->shouldReceive('createKost')->andThrow(new Exception("the exception"));
        $controller = new KostPhotoController();

        $response = $controller->store(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'building_front' => [1,2],
                        'inside_building' => [112],
                        'roadview_building' => [111],
                    ]
                )
            ),
            $mockService,
            app()->make(DataStageService::class),
            app()->make(KostPhotoTransformer::class)
        );

        $testResponse = new TestResponse($response);
        $testResponse->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        $medias1 = factory(Media::class, 4)->create();
        $medias2 = factory(Media::class, 4)->create();
        $medias3 = factory(Media::class, 4)->create();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'building_front' => $medias1->pluck('id')->toArray(),
                'inside_building' => $medias2->pluck('id')->toArray(),
                'roadview_building' => $medias3->pluck('id')->toArray(),
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'building_front',
                'inside_building',
                'roadview_building'
            ]
        ]);

        $this->assertDatabaseHas((new Card())->getTable(), ['photo_id' => $medias1->pluck('id')->random()]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_KOS_PHOTO, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testGetSucess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $this->prepareMedia($room->id, Group::INSIDE_BUILDING);
        $this->prepareMedia($room->id, Group::BUILDING_FRONT);
        $this->prepareMedia($room->id, Group::ROADVIEW_BUILDING);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'building_front',
                'inside_building',
                'roadview_building'
            ]
        ]);
        $this->assertEquals(3, count($response->getData()->room->building_front));
        $this->assertEquals(3, count($response->getData()->room->inside_building));
        $this->assertEquals(3, count($response->getData()->room->roadview_building));
        $this->assertEquals($room->song_id, $response->getData()->room->_id);
    }

    /**
     * @group PMS-495
     */    
    public function testEditUnValidParamsFailsProcess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'building_front' => 'invalid-param',
                'inside_building' => [1,2],
                'roadview_building' => [3,4],
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotOwned()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            8888
        );

        $insideBuilding = $this->prepareMedia($room->id, Group::INSIDE_BUILDING);
        $buildingFront = $this->prepareMedia($room->id, Group::BUILDING_FRONT);
        $roadviewBuilding = $this->prepareMedia($room->id, Group::ROADVIEW_BUILDING);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'building_front' => $buildingFront->pluck('id')->toArray(),
                'inside_building' => $insideBuilding->pluck('id')->toArray(),
                'roadview_building' => $roadviewBuilding->pluck('id')->toArray(),
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotFound()
    {
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => 888]),
            [
                'building_front' => [5,6],
                'inside_building' => [1,2],
                'roadview_building' => [3,4],
            ]
        );

        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testEditSuccess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $insideBuilding = $this->prepareMedia($room->id, Group::INSIDE_BUILDING);
        $buildingFront = $this->prepareMedia($room->id, Group::BUILDING_FRONT);
        $roadviewBuilding = $this->prepareMedia($room->id, Group::ROADVIEW_BUILDING);

        $newMedias = factory(Media::class, 4)->create();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'building_front' => $newMedias->pluck('id')->toArray(),
                'inside_building' => $insideBuilding->pluck('id')->toArray(),
                'roadview_building' => $roadviewBuilding->pluck('id')->toArray(),
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                'building_front',
                'inside_building',
                'roadview_building'
            ]
        ]);
        $this->assertEquals(4, count($response->getData()->room->building_front));
        $this->assertEquals(3, count($response->getData()->room->inside_building));
        $this->assertEquals(3, count($response->getData()->room->roadview_building));
        $this->assertEquals($room->song_id, $response->getData()->room->_id);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_KOS_PHOTO, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    private function prepareRoom(array $roomStates, int $ownerId)
    {
        $room = factory(Room::class)->states($roomStates)->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $ownerId,
            'designer_id' => $room->id,
        ]);

        return $room;
    }

    private function prepareMedia(
        int $roomId,
        string $groupType
    ) {
        $medias = factory(Media::class, 3)->create();

        $medias->each(function ($media, $key) use ($roomId, $groupType) {
            factory(Card::class)->create([
                'photo_id' => $media->id,
                'designer_id' => $roomId,
                'description' => Card::DESCRIPTION_PREFIX[$groupType] . '-' . $key,
                'group' => $groupType,
            ]);
        });

        return $medias;
    }
}
