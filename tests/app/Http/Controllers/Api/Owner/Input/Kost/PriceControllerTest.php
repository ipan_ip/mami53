<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Booking\BookingDiscount;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PriceControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.price.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.price.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.price.edit.put';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreIncompleteParameters()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'daily' => 100000
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreIncompleteFine()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'fine' => [
                    'price' => 10000
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'price_remark' => 'DP 500 rb'
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000, 'price_remark' => 'DP 500 rb']);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithPaymentTagSuccess()
    {
        $this->markTestSkipped();
        $tag = factory(Tag::class)->create([
            'type' => 'keyword'
        ]);

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'min_payment' => $tag->id
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000, 'price_remark' => null]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new RoomTag())->getTable(), ['tag_id' => $tag->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);

        $responseData = $response->getData();
        $this->assertEquals($tag->id, $responseData->room->min_payment);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithAdditionalCostSuccess()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'additional_cost' => [
                    [
                        'name' => 'Iuran Sampah',
                        'price' => 10000,
                        'is_active' => true
                    ]
                ]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000, 'price_remark' => null]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponent())->getTable(), ['component' => MamipayRoomPriceComponentType::ADDITIONAL]);

        $responseData = $response->getData();
        $this->assertEquals('Iuran Sampah', $responseData->room->additional_cost[0]->name);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithDepositSuccess()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'deposit_fee' => [
                    'price' => 10000,
                    'is_active' => true
                ]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000, 'price_remark' => null]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponent())->getTable(), ['component' => MamipayRoomPriceComponentType::DEPOSIT]);

        $responseData = $response->getData();
        $this->assertEquals(10000, $responseData->room->deposit_fee->price);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithDownPaymentWithInvalidPercentage()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'down_payment' => [
                    'percentage' => 25,
                    'is_active' => true
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithDownPaymentSuccess()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'down_payment' => [
                    'percentage' => 40,
                    'is_active' => true
                ]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponent())->getTable(), ['component' => MamipayRoomPriceComponentType::DP]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponentAdditional())->getTable(), ['value' => 40]);

        $responseData = $response->getData();
        $this->assertEquals(40, $responseData->room->down_payment->percentage);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithInvalidFineDurationType()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'fine' => [
                    'price' => 10000,
                    'duration_type' => 'year',
                    'length' => 5,
                    'is_active' => true
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithFineSuccess()
    {
        $this->markTestSkipped();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'fine' => [
                    'price' => 10000,
                    'duration_type' => 'day',
                    'length' => 5,
                    'is_active' => true
                ]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['price_monthly' => 100000]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_PRICE, 'complete' => true]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponent())->getTable(), ['component' => MamipayRoomPriceComponentType::FINE]);
        $this->assertDatabaseHas((new MamipayRoomPriceComponentAdditional())->getTable(), ['value' => 5]);

        $responseData = $response->getData();
        $this->assertEquals(10000, $responseData->room->fine->price);
        $this->assertEquals(5, $responseData->room->fine->length);
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testGetSuccess()
    {
        $room = $this->createRoom();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithPaymentTagSuccess()
    {
        $room = $this->createRoom();
        $tag = $this->assignPaymentTag($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($tag->id, $response->getData()->room->min_payment);
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithAdditionalCostSuccess()
    {
        $room = $this->createRoom();
        $additionalCost = $this->createAdditionalCost($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($additionalCost[0]->price, $response->getData()->room->additional_cost[0]->price);
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithDepositFeeSuccess()
    {
        $room = $this->createRoom();
        $depositFee = $this->createDepositFee($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($depositFee->price, $response->getData()->room->deposit_fee->price);
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithDownPaymentSuccess()
    {
        $room = $this->createRoom();
        $dp = $this->createDownPayment($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($dp->is_active, $response->getData()->room->down_payment->is_active);
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithFineSuccess()
    {
        $room = $this->createRoom();
        $fine = $this->createFine($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($fine->price, $response->getData()->room->fine->price);
    }

    /**
     * @group PMS-495
     */    
    public function testEditInvalidParams()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 0
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ]
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditSuccess()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'price_remark' => 'DP 500 rb'
            ]
        );

        $response->assertOk();
        $this->assertEquals(100000, $response->getData()->room->price->monthly);
        $this->assertEquals('DP 500 rb', $response->getData()->room->price_remark);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditNumberTooBigFailed()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 1000000000
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditRemovePaymentTagSuccess()
    {
        $room = $this->createRoom();
        $this->assignPaymentTag($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ]
            ]
        );

        $response->assertOk();
        $this->assertNull($response->getData()->room->min_payment);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditChangePaymentTagSuccess()
    {
        $room = $this->createRoom();
        $this->assignPaymentTag($room->id);

        $tag = factory(Tag::class)->create([
            'type' => 'keyword'
        ]);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'min_payment' => $tag->id
            ]
        );

        $response->assertOk();
        $this->assertEquals($tag->id, $response->getData()->room->min_payment);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithFlashSaleRunningSuccess()
    {
        $room = $this->createRoom('annually');
        $this->setRunningFlashSaleData('2');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ]
            ]
        );

        $response->assertOk();
        $this->assertEquals(100000, $response->getData()->room->price->monthly);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
        $this->assertTrue($response->getData()->room->is_price_flash_sale->annually);
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithFlashSaleRunningFailed()
    {
        $room = $this->createRoom('annually');
        $this->setRunningFlashSaleData('2');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000,
                    'yearly' => 10000000
                ]
            ]
        );
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testReplaceAdditionalCostSuccess()
    {
        $room = $this->createRoom();
        $this->createAdditionalCost($room->id);
        $price = $this->createAdditionalCost($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'additional_cost' => [
                    [
                        'id' => $price[0]->id,
                        'name' => 'Iuran Sampah',
                        'price' => 10000,
                        'is_active' => true
                    ]
                ]
            ]
        );
        
        $response->assertOk();
        $this->assertEquals($price[0]->id, $response->getData()->room->additional_cost[0]->id);
        $this->assertEquals(10000, $response->getData()->room->additional_cost[0]->price);
        $this->assertEquals(1, count($response->getData()->room->additional_cost));
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditAdditionalCostSuccess()
    {
        $room = $this->createRoom();
        $additionalCost = $this->createAdditionalCost($room->id);
        $this->createAdditionalCost($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'additional_cost' => [
                    [
                        'id' => $additionalCost[0]->id,
                        'name' => 'Iuran Sampah',
                        'price' => 10000,
                        'is_active' => true
                    ]
                ]
            ]
        );
        
        $response->assertOk();
        $this->assertEquals($additionalCost[0]->id, $response->getData()->room->additional_cost[0]->id);
        $this->assertEquals(10000, $response->getData()->room->additional_cost[0]->price);
        $this->assertEquals(1, count($response->getData()->room->additional_cost));
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditAdditionalCostNotFoundPriceId()
    {
        $room = $this->createRoom();
        $otherRoom = $this->createRoom();
        $priceComponent = $this->createAdditionalCost($otherRoom->id);
        $this->createAdditionalCost($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'additional_cost' => [
                    [
                        'id' => $priceComponent[0]->id,
                        'name' => 'Iuran Sampah',
                        'price' => 10000,
                        'is_active' => true
                    ]
                ]
            ]
        );
        $this->assertEquals('additional_cost Ids must be valid', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testEditAdditionalCostWithChangeOthersAdditionalCostShouldFail()
    {
        $roomA = $this->createRoom();
        $roomB = $this->createRoom();
        $additionalCostA = $this->createAdditionalCost($roomA->id, 3);
        $additionalCostB = $this->createAdditionalCost($roomB->id, 3);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $roomB->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'additional_cost' => [
                    [
                        'id' => $additionalCostA[0]->id,
                        'name' => 'Update Additional Cost Room A',
                        'price' => 22222,
                        'is_active' => true
                    ],
                    [
                        'id' => $additionalCostB[0]->id,
                        'name' => 'Update Additional Cost Room B1',
                        'price' => 44444,
                        'is_active' => true
                    ],
                    [
                        'id' => $additionalCostB[1]->id,
                        'name' => 'Update Additional Cost Room B2',
                        'price' => 55555,
                        'is_active' => true
                    ],
                    [
                        'id' => $additionalCostB[2]->id,
                        'name' => 'Update Additional Cost Room B2',
                        'price' => 55555,
                        'is_active' => true
                    ]
                ]
            ]
        );
        $this->assertEquals('additional_cost Ids must be valid', $response->getData()->meta->message);
    }

    /**
     * @group PMS-495
     */    
    public function testEditDepositFeeSuccess()
    {
        $room = $this->createRoom();
        $this->createDepositFee($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'deposit_fee' => [
                    'price' => 100000,
                    'is_active' => true
                ]
            ]
        );
        
        $response->assertOk();
        $this->assertEquals(100000, $response->getData()->room->deposit_fee->price);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditDownPaymentWithInvalidPercentage()
    {
        $room = $this->createRoom();
        $this->createDownPayment($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'down_payment' => [
                    'percentage' => 15,
                    'is_active' => true
                ]
            ]
        );
        
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditDownPaymentSuccess()
    {
        $room = $this->createRoom();
        $this->createDownPayment($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'down_payment' => [
                    'percentage' => 10,
                    'is_active' => true
                ]
            ]
        );
        
        $response->assertOk();
        $this->assertEquals(10, $response->getData()->room->down_payment->percentage);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditFineWithInvalidDurationType()
    {
        $room = $this->createRoom();
        $this->createFine($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'fine' => [
                    'price' => 50000,
                    'duration_type' => 'year',
                    'length' => 1,
                    'is_active' => true
                ]
            ]
        );
        
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditFineSuccess()
    {
        $room = $this->createRoom();
        $this->createFine($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'price' => [
                    'monthly' => 100000
                ],
                'fine' => [
                    'price' => 50000,
                    'duration_type' => 'month',
                    'length' => 1,
                    'is_active' => true
                ]
            ]
        );
        
        $response->assertOk();
        $responseData = $response->getData();
        $this->assertEquals(50000, $responseData->room->fine->price);
        $this->assertEquals('month', $responseData->room->fine->duration_type);
        $this->assertEquals(1, $responseData->room->fine->length);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_PRICE, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    private function createRoom($withDiscountType = null)
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 1000000,
            'price_yearly' => 10000000,
            'latitude' => -8.014663865095645,
            'longitude' => 110.2936449049594,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->owner->id
        ]);

        if ($withDiscountType !== null) {
            factory(BookingDiscount::class)->create([
                'designer_id' => $room->id,
                'price_type' => $withDiscountType,
                'price' => 2500000,
                'markup_type' => 'percentage',
                'markup_value' => 20,
                'discount_type' => 'percentage',
                'discount_value' => 10,
                'is_active' => 1
            ]);
        }

        return $room;
    }

    private function createAdditionalCost(int $roomId,int $total = 1)
    {
        return factory(MamipayRoomPriceComponent::class, $total)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL
        ]);
    }

    private function createDepositFee(int $roomId)
    {
        return factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::DEPOSIT
        ]);
    }

    private function createDownPayment(int $roomId)
    {
        $dp = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::DP
        ]);

        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $dp->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => 30
        ]);

        return $dp;
    }

    private function createFine(int $roomId)
    {
        $fine = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::FINE
        ]);

        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE,
            'value' => 'day'
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH,
            'value' => 3
        ]);

        return $fine;
    }

    private function assignPaymentTag(int $roomId)
    {
        $tag = factory(Tag::class)->create([
            'type' => 'keyword'
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $roomId
        ]);

        return $tag;
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        $landingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );

        return $landingData;
    }

    private function jsonStructure()
    {
        return [
            'status',
            'room' => [
                '_id',
                'price',
                'min_payment',
                'additional_cost',
                'deposit_fee',
                'down_payment',
                'fine',
                'is_price_flash_sale',
                'price_remark'
            ]
        ];
    }
}
