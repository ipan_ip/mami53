<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Room\DataStage;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Input\Kost\FacilitiesTransformer;
use App\User;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class FacilitiesControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.facilities.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.facilities.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.facilities.edit.put';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreUnvalidParams()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'public' => ['unvalid-integer'],
                'bedroom' => [110],
                'bathroom' => [110],
                'parking' => [110],
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreLackingParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'bedroom' => [110],
                'bathroom' => [110],
                'parking' => [110],
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreThrowExceptionReturn400()
    {
        app()->user = $this->owner;

        $mockService = $this->mock(InputService::class);
        $mockService->shouldReceive('createKost')->andThrow(new Exception("the exception"));
        $controller = new FacilitiesController();

        $response = $controller->store(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'public' => [110],
                        'bedroom' => [110],
                        'bathroom' => [110],
                        'parking' => [110],
                    ]
                )
            ),
            $mockService,
            app()->make(DataStageService::class),
            app()->make(FacilitiesTransformer::class)
        );

        $testResponse = new TestResponse($response);
        $testResponse->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        $tags = $this->prepareTags();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'public' => collect(
                    $tags['fac_share']
                )->pluck('id')->toArray(),
                'bedroom' => collect(
                    $tags['fac_room']
                )->pluck('id')->toArray(),
                'bathroom' => collect(
                    $tags['fac_bath']
                )->pluck('id')->toArray(),
                'parking' => collect(
                    $tags['fac_park']
                )->pluck('id')->toArray(),
                'near' => collect(
                    $tags['fac_near']
                )->pluck('id')->toArray()
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $responseId = $response->getData()->room->_id;

        $this->assertDatabaseHas((new Room())->getTable(), ['song_id' => $responseId]);
        $room = Room::where('song_id', $responseId)->first();
        $this->assertDatabaseHas((new RoomTag())->getTable(), ['designer_id' => $room->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_FACILITY, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */
    public function testGetSuccess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $this->assertEquals($room->song_id, $response->getData()->room->_id);
        $this->assertEquals(
            count(
                array_flatten($tags)
            ),
            count($response->getData()->room->public) +
                count($response->getData()->room->bedroom) +
                count($response->getData()->room->bathroom) +
                count($response->getData()->room->parking) +
                count($response->getData()->room->near)
        );
    }

    /**
     * @group PMS-495
     */    
    public function testGetDoubleFacilitiesSucess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $toBeDoubled = $tags['fac_bath'][0];

        factory(TagType::class)->create([
            'tag_id' => $toBeDoubled->id,
            'name' => 'fac_room'
        ]);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $this->assertEquals($room->song_id, $response->getData()->room->_id);
        $this->assertEquals(
            count(
                array_flatten($tags)
            ),
            count($response->getData()->room->public) +
            count($response->getData()->room->bedroom) +
            count($response->getData()->room->bathroom) +
            count($response->getData()->room->parking) +
            count($response->getData()->room->near),
            "The total ammount must not be added. The double category facility is categorized by last tag_type"
        );

        $this->assertNotEmpty(
            collect($response->getData()->room->bedroom)->where('id', $toBeDoubled->id)
        );

        $this->assertNotEmpty(
            collect($response->getData()->room->bedroom)->where('id', $toBeDoubled->id),
            "double facilities type use last type as category"
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditUnValidParamsFailsProcess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => 'string',
                'bedroom' => collect(
                    $tags['fac_room']
                )->pluck('id')->toArray(),
                'bathroom' => collect(
                    $tags['fac_bath']
                )->pluck('id')->toArray(),
                'parking' => collect(
                    $tags['fac_park']
                )->pluck('id')->toArray()
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotOwned()
    {
        [$room] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            8888
        );

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => [1, 2, 3, 4],
                'bedroom' => [1, 2, 3, 4],
                'bathroom' => [1, 2, 3, 4],
                'parking' => [1, 2, 3, 4]
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotFound()
    {
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => 888]),
            [
                'public' => [1, 2, 3, 4],
                'bedroom' => [1, 2, 3, 4],
                'bathroom' => [1, 2, 3, 4],
                'parking' => [1, 2, 3, 4]
            ]
        );

        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testEditSuccess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $originalTagsCount = count(array_flatten($tags));

        $room->loadMissing('tags.types');
        $this->assertDatabaseHas((new RoomTag())->getTable(), ['designer_id' => $room->id]);
        $this->assertEquals($originalTagsCount, $room->tags->count());

        $addTags = $this->prepareTags([
            'fac_room' => [
                'Televisi',
                'AC'
            ],
            'fac_bath' => [
                'Shower',
            ],
            'fac_park' => [
                'Parkir Sepeda'
            ],
            'fac_share' => [
                'Security 24 Jam',
                'Room Cleaning'
            ],
            'fac_near' => [
                'Tempat Ibadah'
            ],
        ]);

        foreach ($addTags as $key => $items) {
            $tags[$key] = array_merge($tags[$key], $items);
        }

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => collect(
                    $tags['fac_share']
                )->pluck('id')->toArray(),
                'bedroom' => collect(
                    $tags['fac_room']
                )->pluck('id')->toArray(),
                'bathroom' => collect(
                    $tags['fac_bath']
                )->pluck('id')->toArray(),
                'parking' => collect(
                    $tags['fac_park']
                )->pluck('id')->toArray(),
                'near' => collect(
                    $tags['fac_near']
                )->pluck('id')->toArray(),
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $newTagsCount = count(array_flatten($tags));
        $this->assertNotEquals($originalTagsCount, $newTagsCount);
        $this->assertEquals(
            count($tags['fac_room']),
            count($response->getData()->room->bedroom)
        );
        $this->assertEquals(
            count($tags['fac_bath']),
            count($response->getData()->room->bathroom)
        );
        $this->assertEquals(
            count($tags['fac_park']),
            count($response->getData()->room->parking)
        );
        $this->assertEquals(
            count($tags['fac_share']),
            count($response->getData()->room->public)
        );
        $this->assertEquals(
            count($tags['fac_near']),
            count($response->getData()->room->near)
        );

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_FACILITY, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditReduceSuccess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $originalTagsCount = count(array_flatten($tags));
        $room->loadMissing('tags.types');
        $this->assertDatabaseHas((new RoomTag())->getTable(), ['designer_id' => $room->id]);
        $this->assertEquals($originalTagsCount, $room->tags->count());

        $publicTags = $tags['fac_share'];
        $roomTags   = $tags['fac_room'];
        $bathTags   = $tags['fac_bath'];
        $parkTags   = $tags['fac_park'];
        $nearTags   = $tags['fac_near'];

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => collect($publicTags)
                    ->random(rand(1, count($publicTags) - 1))
                    ->pluck('id')->toArray(),
                'bedroom' => collect($roomTags)
                    ->random(rand(1, count($roomTags) - 1))
                    ->pluck('id')->toArray(),
                'bathroom' => collect($bathTags)
                    ->random(rand(1, count($bathTags) - 1))
                    ->pluck('id')->toArray(),
                'parking' => collect($parkTags)
                    ->random(rand(1, count($parkTags) - 1 ))
                    ->pluck('id')->toArray(),
                'near' => collect($nearTags)
                    ->random(rand(1, count($parkTags) - 1 ))
                    ->pluck('id')->toArray()
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $newTagsCount = count($response->getData()->room->bedroom) +
            count($response->getData()->room->bathroom) +
            count($response->getData()->room->parking) +
            count($response->getData()->room->public) +
            count($response->getData()->room->near);

        $this->assertLessThan($originalTagsCount, $newTagsCount);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_FACILITY, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditChangesSuccess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $addTags = $this->prepareTags([
            'fac_room' => [
                'Televisi',
                'AC'
            ],
            'fac_bath' => [
                'Shower',
                'Sabun Cair'
            ],
            'fac_park' => [
                'Parkir Sepeda'
            ],
            'fac_share' => [
                'Security 24 Jam',
                'Room Cleaning'
            ],
            'fac_near' => [
                'Tempat Ibadah',
                'Universitas'
            ],
        ]);

        $publicTags = collect($tags['fac_share'])->shuffle();
        $bedRoomTags = collect($tags['fac_room'])->shuffle();
        $bathRoomTags = collect($tags['fac_bath'])->shuffle();
        $parkingTags = collect($tags['fac_park'])->shuffle();
        $nearTags = collect($tags['fac_near'])->shuffle();

        $removedPublicTags = $publicTags->shift();
        $removedBedroomTags = $bedRoomTags->shift();
        $removedBathroomTags = $bathRoomTags->shift();
        $removedParkingTags = $parkingTags->shift();
        $removedNearTags = $nearTags->shift();

        $publicTags->concat($addTags['fac_share']);
        $bedRoomTags->concat($addTags['fac_room']);
        $bathRoomTags->concat($addTags['fac_bath']);
        $parkingTags->concat($addTags['fac_park']);
        $nearTags->concat($addTags['fac_near']);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => $publicTags->pluck('id')->toArray(),
                'bedroom' => $bedRoomTags->pluck('id')->toArray(),
                'bathroom' => $bathRoomTags->pluck('id')->toArray(),
                'parking' => $parkingTags->pluck('id')->toArray(),
                'near' => $nearTags->pluck('id')->toArray()
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $room = Room::with('tags.types')->find($room->id);
        $this->assertEmpty(
            $room->tags->whereIn(
                'name',
                [
                    $removedPublicTags->name,
                    $removedBedroomTags->name,
                    $removedBathroomTags->name,
                    $removedParkingTags->name,
                    $removedNearTags->name
                ]
            )
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_FACILITY, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithKosRulesAndMinPaymentChangesSuccess()
    {
        [$room, $tags] = $this->prepareRoomWithFacilities(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $nonFacilitiesTags = $this->prepareTags([
            'kos_rule' => [
                'Pasutri',
                'Bisa bawa hewan',
                'Tidak merokok'
            ],
            'keyword' => [
                'Min. 1 Bln'
            ],
        ]);

        $room->tags()->attach(collect(array_flatten($nonFacilitiesTags))->pluck('id'));

        $addTags = $this->prepareTags([
            'fac_room' => [
                'Televisi',
                'AC'
            ],
            'fac_bath' => [
                'Shower',
                'Sabun Cair'
            ],
            'fac_park' => [
                'Parkir Sepeda'
            ],
            'fac_share' => [
                'Security 24 Jam',
                'Room Cleaning'
            ],
            'fac_near' => [
                'Tempat Ibadah',
                'Universitas'
            ],
        ]);

        $publicTags = collect($tags['fac_share'])->shuffle();
        $bedRoomTags = collect($tags['fac_room'])->shuffle();
        $bathRoomTags = collect($tags['fac_bath'])->shuffle();
        $parkingTags = collect($tags['fac_park'])->shuffle();
        $nearTags = collect($tags['fac_near'])->shuffle();

        $removedPublicTags = $publicTags->shift();
        $removedBedroomTags = $bedRoomTags->shift();
        $removedBathroomTags = $bathRoomTags->shift();
        $removedParkingTags = $parkingTags->shift();
        $removedNearTags = $nearTags->shift();

        $publicTags->concat($addTags['fac_share']);
        $bedRoomTags->concat($addTags['fac_room']);
        $bathRoomTags->concat($addTags['fac_bath']);
        $parkingTags->concat($addTags['fac_park']);
        $nearTags->concat($addTags['fac_near']);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'public' => $publicTags->pluck('id')->toArray(),
                'bedroom' => $bedRoomTags->pluck('id')->toArray(),
                'bathroom' => $bathRoomTags->pluck('id')->toArray(),
                'parking' => $parkingTags->pluck('id')->toArray(),
                'near' => $nearTags->pluck('id')->toArray()
            ]
        );

        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedTransformerContract());

        $room = Room::with('tags.types')->find($room->id);
        $this->assertEmpty(
            $room->tags->whereIn(
                'name',
                [
                    $removedPublicTags->name,
                    $removedBedroomTags->name,
                    $removedBathroomTags->name,
                    $removedParkingTags->name,
                    $removedNearTags->name
                ]
            )
        );

        $this->assertNotEmpty(
            $room->tags->whereIn(
                'id',
                collect(array_flatten($nonFacilitiesTags))->pluck('id')->toArray()
            )
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_FACILITY, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    private function prepareRoomWithFacilities(
        array $roomStates,
        int $ownerId
    ): array {
        $room = factory(Room::class)->states($roomStates)->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $ownerId,
            'designer_id' => $room->id,
        ]);

        $tags = $this->prepareTags();

        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        return [$room, $tags];
    }

    private function prepareTags(
        ?array $source = null
    ): array {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }

    private function expectedTransformerContract(): array
    {
        return [
            'status',
            'meta',
            'room' => [
                '_id',
                'public' => [[
                    'id',
                    'name'
                ]],
                'bedroom' => [[
                    'id',
                    'name'
                ]],
                'bathroom' => [[
                    'id',
                    'name'
                ]],
                'parking' => [[
                    'id',
                    'name'
                ]],
                'near'
            ]
        ];
    }

    
}
