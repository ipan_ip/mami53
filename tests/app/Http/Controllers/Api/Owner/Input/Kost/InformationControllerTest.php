<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Test\MamiKosTestCase;
use App\User;

class InformationControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.information.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.information.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.information.edit.put';

    private $owner;

    /**
     * @group PMS-495
     */    
    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreIncompleteParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'name' => 'kost'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreDuplicateKostName()
    {
        factory(Room::class)->create([
            'name' => 'kost duplicate name'
        ]);

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'name'      => 'kost duplicate name'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithoutPropertySuccess()
    {
        $name = 'kost success type A';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'name'      => $name,
                'description' => 'Kost amazing with complete facility',
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['name' => $name]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithPropertyNoTypeNameFailed()
    {
        $name = 'kost success';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'name' => $name,
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithPropertyWithoutTypeNameSuccess()
    {
        $name = 'Kost Success';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'name' => $name,
                    'allow_multiple' => false
                ]
            ]
        );

        $response->assertSuccessful();

        $this->assertDatabaseHas((new Property())->getTable(), ['name' => $name]);
        $this->assertDatabaseHas((new Room())->getTable(), ['name' => "$name"]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true]);
        $this->assertDatabaseHas((new CreationFlag())->getTable(), ['create_from' => CreationFlag::CREATE_NEW]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithPropertySuccessPropertyCreated()
    {
        $name = 'Kost Success';
        $typeName = 'Type A';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'type_name' => $typeName,
                'property' => [
                    'name' => $name,
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertSuccessful();

        $this->assertDatabaseHas((new Property())->getTable(), ['name' => $name]);
        $this->assertDatabaseHas((new Room())->getTable(), ['unit_type' => $typeName]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true]);
        $this->assertDatabaseHas((new CreationFlag())->getTable(), ['create_from' => CreationFlag::CREATE_NEW]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithExistingPropertySuccess()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Simpan',
            'has_multiple_type' => true
        ]);
        $typeName = 'Type A';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'type_name' => $typeName,
                'property' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'allow_multiple' => false
                ]
            ]
        );

        $response->assertSuccessful();
        $property->refresh();

        $this->assertDatabaseHas((new Property())->getTable(), ['name' => $property->name]);
        $this->assertDatabaseHas((new Room())->getTable(), ['unit_type' => $typeName]);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true]);
        $this->assertDatabaseHas((new CreationFlag())->getTable(), ['create_from' => CreationFlag::CREATE_NEW]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithPropertyThenEditSuccess()
    {
        $name = 'Kost Success';
        $typeName = 'Type A';
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'type_name' => $typeName,
                'property' => [
                    'name' => $name,
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertSuccessful();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $response->getData()->room->_id]),
            [
                'gender' => 1,
                'type_name' => 'NON AC',
                'description' => 'Kost amazing with Edited complete facility',
                'property' => [
                    'id' => $response->getData()->property->id,
                    'name' => $name,
                    'allow_multiple' => true
                ]
            ]
        );

        $this->assertDatabaseHas((new Property())->getTable(), ['name' => $name]);
        $this->assertDatabaseHas((new Room())->getTable(), ['unit_type' => 'NON AC']);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithExistingPropertyNoIdFailed()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Simpan',
            'has_multiple_type' => false
        ]);

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'    => 1,
                'description' => 'Kost amazing with complete facility',
                'type_name' => '',
                'property' => [
                    'name' => $property->name,
                    'allow_multiple' => false
                ]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithRuleTagsSuccess()
    {
        $tag = factory(Tag::class)->create();
        factory(TagType::class)->create([
            'tag_id'    => $tag->id,
            'name'      => 'kos_rule'
        ]);

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'        => 1,
                'name'          => 'kost success',
                'description' => 'Kost amazing with complete facility',
                'list_rules'    => [$tag->id]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['name' => 'kost success']);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new RoomTag())->getTable(), ['tag_id' => $tag->id]);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true]);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithTermPhotoSuccess()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'gender'        => 1,
                'name'          => 'kost success',
                'photo_rules'   => [
                    [
                        'id' => 1234
                    ]
                ]
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['name' => 'kost success']);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas((new RoomTermDocument())->getTable(), ['media_id' => 1234]);
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testGetAccessedByNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testGetSuccess()
    {
        $room = $this->createRoom();

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithPropertySuccess()
    {
        $room = $this->createRoom(
            factory(Property::class)->create([
                'owner_user_id' => $this->owner->id,
                'name' => 'Kost Khusus Get',
                'has_multiple_type' => true
            ]),
            'Type X'
        );

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure(
            array_merge(
                $this->jsonStructure(),
                [
                    'property' => [
                        'id',
                        'name',
                        'allow_multiple'
                    ]
                ]
            )
        );
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithRuleTagSuccess()
    {
        $room = $this->createRoom();
        $tag = $this->assignRoomTag($room->id, 'kos_rule');

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals([$tag->id], $response->getData()->room->{'list_rules'});
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithNotRuleTagSuccess()
    {
        $room = $this->createRoom();
        $this->assignRoomTag($room->id, 'another_rule');

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals([], $response->getData()->room->{'list_rules'});
    }

    /**
     * @group PMS-495
     */    
    public function testGetWithRulePhotoSuccess()
    {
        $room = $this->createRoom();
        $media = $this->assignRulePhoto($room->id);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->jsonStructure());
        $this->assertEquals($media->id, $response->getData()->room->{'photo_rules'}[0]->id);
    }

    /**
     * @group PMS-495
     */    
    public function testEditInvalidParams()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 5,
                'name' => 'kost invalid param gender'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditNotOwnerReturnForbidden()
    {
        $room = factory(Room::class)->create();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'name' => 'kost not owner'
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithPropertySuccess()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Khusus Edit',
            'has_multiple_type' => true
        ]);
        $room = $this->createRoom($property, 'Type A');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'type_name' => 'NON AC',
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertOk();
        $this->assertEquals(
            "$property->name NON AC $room->area_subdistrict $room->area_city",
            $response->getData()->room->name
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            [
                'name' => DataStage::STAGE_DATA,
                'complete' => true,
                'designer_id' => $room->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithPropertyOnRoomWithoutAddressSuccess()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Khusus Edit',
            'has_multiple_type' => true
        ]);
        $room = $this->createRoom($property, 'Type A');

        $room->area_city = null;
        $room->area_subdistrict = null;
        $room->area_big = null;
        $room->save();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'type_name' => 'NON AC',
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertOk();
        $this->assertEquals(
            implode(' ', array_filter([$property->name, 'NON AC', $room->area_subdistrict . $room->area_city])),
            $response->getData()->room->name
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            [
                'name' => DataStage::STAGE_DATA,
                'complete' => true,
                'designer_id' => $room->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithPropertyNonMultipleTypeSuccess()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Khusus Edit',
            'has_multiple_type' => false
        ]);
        $room = $this->createRoom($property, '');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'allow_multiple' => false
                ]
            ]
        );

        $response->assertOk();
        $this->assertEquals(
            "$property->name $room->area_subdistrict $room->area_city",
            $response->getData()->room->name
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            [
                'name' => DataStage::STAGE_DATA,
                'complete' => true,
                'designer_id' => $room->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithPropertyChangePropertyNameSuccess()
    {
        $property = factory(Property::class)->create([
            'owner_user_id' => $this->owner->id,
            'name' => 'Kost Khusus Edit',
            'has_multiple_type' => true
        ]);
        $room = $this->createRoom($property, 'Type A');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'type_name' => 'NON AC',
                'description' => 'Kost amazing with complete facility',
                'property' => [
                    'id' => $property->id,
                    'name' => "Kost Cakep Edit",
                    'allow_multiple' => true
                ]
            ]
        );

        $response->assertOk();
        $this->assertEquals(
            "Kost Cakep Edit NON AC $room->area_subdistrict $room->area_city",
            $response->getData()->room->name
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            [
                'name' => DataStage::STAGE_DATA,
                'complete' => true,
                'designer_id' => $room->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditWithoutPropertySuccess()
    {
        $room = $this->createRoom();

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'name' => 'kost edit success',
                'description' => 'Kost amazing with complete facility'
            ]
        );

        $response->assertOk();
        $this->assertEquals('kost edit success', $response->getData()->room->name);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true, 'designer_id' => $room->id]);
    }

    /**
     * @group PMS-495
     */    
    public function testEditRuleTagSuccess()
    {
        //$this->markTestSkipped("skipped due the list not updated");
        $room = $this->createRoom();
        $this->assignRoomTag($room->id, 'kos_rule');

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'name' => 'kost edit rule tag',
                'list_rules' => []
            ]
        );

        $response->assertOk();
        $this->assertEquals([], $response->getData()->room->list_rules);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true, 'designer_id' => $room->id]);
    }

    /**
     * @group PMS-495
     */    
    public function testEditRulePhotoSuccess()
    {
        $room = $this->createRoom();
        $this->assignRulePhoto($room->id);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'gender' => 1,
                'name' => 'kost edit rule tag',
                'photo_rules' => []
            ]
        );

        $response->assertOk();
        $this->assertEquals([], $response->getData()->room->photo_rules);
        $this->assertDatabaseHas((new DataStage())->getTable(), ['name' => DataStage::STAGE_DATA, 'complete' => true, 'designer_id' => $room->id]);
    }

    private function createRoom(?Property $property = null, string $typeName = '', array $data = [])
    {
        if ($property !== null) {
            $data = [
                'property_id' => $property->id,
                'unit_type' => $typeName,
                'name' => "$property->name $typeName Blimbing Malang"
            ];
        }

        $room = factory(Room::class)->states(['with-address'])->create($data);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->owner->id
        ]);

        return $room;
    }

    private function assignRoomTag(int $roomId, string $tagType)
    {
        $tag = factory(Tag::class)->create();
        factory(TagType::class)->create([
            'tag_id' => $tag->id,
            'name' => $tagType
        ]);
        factory(RoomTag::class)->create([
            'tag_id' => $tag->id,
            'designer_id' => $roomId
        ]);

        return $tag;
    }

    private function assignRulePhoto(int $roomId)
    {
        $media = factory(Media::class)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $roomId
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $media->id
        ]);

        return $media;
    }

    private function jsonStructure()
    {
        return [
            'status',
            'room' => [
                '_id',
                'gender',
                'name',
                'type_name',
                'description',
                'list_rules',
                'photo_rules',
                'building_year',
                'manager_name',
                'manager_phone',
                'remark',
                'subdistrict',
                'city'
            ],
            'property'
        ];
    }
}
