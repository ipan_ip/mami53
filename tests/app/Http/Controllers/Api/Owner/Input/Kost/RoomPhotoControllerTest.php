<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Test\MamiKosTestCase;
use App\Transformers\Owner\Input\Kost\RoomPhotoTransformer;
use App\User;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RoomPhotoControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.room-photo.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.room-photo.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.room-photo.edit.put';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreUnValidParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'cover' => 110,
                'room_front' => 'invalid-array',
                'inside_room' => [110],
                'bathroom' => [5],
                'other' => [8, 9, 10]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreCoverNotInInsideRoomParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'cover' => 999,
                'room_front' => [199],
                'inside_room' => [1,2,3],
                'bathroom' => [5],
                'other' => [8, 9, 10]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreLackingParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'room_front' => [110],
                'other' => [8, 9, 10]
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreThrowExceptionReturn400()
    {
        app()->user = $this->owner;

        $mockService = $this->mock(InputService::class);
        $mockService->shouldReceive('createKost')->andThrow(new Exception("the exception"));
        $controller = new RoomPhotoController();

        $response = $controller->store(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'cover' => 1,
                        'room_front' => [199],
                        'inside_room' => [1,2,3],
                        'bathroom' => [5],
                        'other' => [8, 9, 10]
                    ]
                )
            ),
            $mockService,
            app()->make(DataStageService::class),
            app()->make(RoomPhotoTransformer::class)
        );

        $testResponse = new TestResponse($response);
        $testResponse->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        $medias = factory(Media::class, 6)->create();
        [$roomFront, $insideRoom, $bathroom] = $medias->pluck('id')->split(3)->toArray();

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'cover' => $insideRoom[0],
                'room_front' => $roomFront,
                'inside_room' => $insideRoom,
                'bathroom' => $bathroom,
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_front',
                'inside_room',
                'bathroom',
                'other'
            ]
        ]);

        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => array_shift($roomFront), 'group' => Group::ROOM_FRONT]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => array_shift($insideRoom), 'group' => Group::INSIDE_ROOM, 'is_cover' => true]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => array_shift($bathroom), 'group' => Group::BATHROOM]
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testGetSucess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $this->prepareCover($room->id);
        $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $this->prepareMedia($room->id, Group::INSIDE_ROOM);

        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_front',
                'inside_room',
                'bathroom',
                'other'
            ]
        ]);

        $this->assertEquals(3, count($response->getData()->room->room_front));
        $this->assertEquals(4, count($response->getData()->room->inside_room));
        $this->assertEquals($room->song_id, $response->getData()->room->_id);
    }

    /**
     * @group PMS-495
     */    
    public function testEditUnValidParamsFailsProcess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => 'string',
                'room_front' => [1,2],
                'inside_room' => [3,4],
                'bathroom' => [5,6],
            ]
        );
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotOwnedReturnForbidden()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            8888
        );
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => 3,
                'room_front' => [1,2],
                'inside_room' => [3,4],
                'bathroom' => [5,6],
            ]
        );
        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotFound()
    {
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => 888]),
            [
                'cover' => 3,
                'room_front' => [1,2],
                'inside_room' => [3,4],
                'bathroom' => [5,6],
            ]
        );
        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */
    public function testEditSuccess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $cover = $this->prepareCover($room->id);
        $roomFront = $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $insideRoom = $this->prepareMedia($room->id, Group::INSIDE_ROOM);

        $media = factory(Media::class)->create();

        $insideRoomArray = $insideRoom->pluck('id')->toArray();
        $insideRoomArray[] = $cover->id;

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => $cover->id,
                'room_front' => $roomFront->pluck('id')->toArray(),
                'inside_room' => $insideRoomArray,
                'bathroom' => [$media->id]
            ]
        );

        $response->assertSuccessful();
        $this->assertEquals(8, Card::where('designer_id', $room->id)->count());

        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => $roomFront[0]->id, 'group' => Group::ROOM_FRONT]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => $insideRoom[0]->id, 'group' => Group::INSIDE_ROOM]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => $media->id, 'group' => Group::BATHROOM]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['group' => Group::INSIDE_ROOM, 'is_cover' => true]
        );
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditNoChangesSuccess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $cover = $this->prepareCover($room->id);
        $roomFront = $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $insideRoom = $this->prepareMedia($room->id, Group::INSIDE_ROOM);

        $insideRoomArray = $insideRoom->pluck('id')->toArray();
        $insideRoomArray[] = $cover->id;

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => $cover->id,
                'room_front' => $roomFront->pluck('id')->toArray(),
                'inside_room' => $insideRoomArray,
            ]
        );

        $response->assertSuccessful();
        $this->assertEquals(7, Card::where('designer_id', $room->id)->count());

        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => $roomFront[0]->id, 'group' => Group::ROOM_FRONT]
        );
        $this->assertDatabaseHas(
            (new Card())->getTable(),
            ['photo_id' => $insideRoom[0]->id, 'group' => Group::INSIDE_ROOM]
        );

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditReduceFrontRoomCardsSuccess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $cover = $this->prepareCover($room->id);
        $roomFront = $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $insideRoom = $this->prepareMedia($room->id, Group::INSIDE_ROOM);

        $insideRoomArray = $insideRoom->pluck('id')->toArray();
        $insideRoomArray[] = $cover->id;

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => $cover->id,
                'room_front' => $roomFront->shuffle()->take(2)->pluck('id')->toArray(),
                'inside_room' => $insideRoomArray,
            ]
        );

        $response->assertSuccessful();
        $this->assertEquals(6, Card::where('designer_id', $room->id)->count());
        $this->assertEquals(2, count($response->getData()->room->room_front));

        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditRemoveOtherCardSuccess()
    {
        $room = $this->prepareRoom(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-address-note'
            ],
            $this->owner->id
        );
        $cover = $this->prepareCover($room->id);
        $roomFront = $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $insideRoom = $this->prepareMedia($room->id, Group::INSIDE_ROOM);
        $other = $this->prepareMedia($room->id, Group::OTHER);

        $insideRoomArray = $insideRoom->pluck('id')->toArray();
        $insideRoomArray[] = $cover->id;

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'cover' => $cover->id,
                'room_front' => $roomFront->pluck('id')->toArray(),
                'inside_room' => $insideRoomArray,
            ]
        );

        $response->assertSuccessful();
        $this->assertEquals(7, Card::where('designer_id', $room->id)->count());
        $this->assertEquals(0, count($response->getData()->room->other));
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ROOM_PHOTO, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    private function prepareRoom(array $roomStates, int $ownerId)
    {
        $room = factory(Room::class)->states($roomStates)->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $ownerId,
            'designer_id' => $room->id,
        ]);

        return $room;
    }

    private function prepareMedia(
        int $roomId,
        string $groupType
    ) {
        $medias = factory(Media::class, 3)->create();

        $medias->each(function ($media, $key) use ($roomId, $groupType) {
            factory(Card::class)->create([
                'photo_id' => $media->id,
                'designer_id' => $roomId,
                'description' => Card::DESCRIPTION_PREFIX[$groupType] . '-' . $key,
                'group' => $groupType,
            ]);
        });

        return $medias;
    }

    private function prepareCover(int $roomId)
    {
        $media = factory(Media::class)->create();

        factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $roomId,
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'group' => Group::INSIDE_ROOM,
            'is_cover' => true,
        ]);

        return $media;
    }
}
