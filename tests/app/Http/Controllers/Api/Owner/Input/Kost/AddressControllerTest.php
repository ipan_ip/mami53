<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Owner\Kost\InputService;
use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;

class AddressControllerTest extends MamiKosTestCase
{
    const STORE_ROUTE = 'oauth.user-act.owner.data.kos.input.address.store';
    const RETRIEVE_ROUTE = 'oauth.user-act.owner.data.kos.input.address.edit.retrieve';
    const UPDATE_ROUTE = 'oauth.user-act.owner.data.kos.input.address.edit.put';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreUnValidParameters()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 110,
                'longitude' => 'bukan-angka',
                'city' => 'kota',
                'subdistrict' => 'kecamatan'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreThrowExceptionReturn400()
    {
        app()->user = $this->owner;

        $mockService = $this->mock(InputService::class);
        $mockService->shouldReceive('createKost')->andThrow(new Exception("the exception"));

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => 'Jl. test menuju roma',
                'latitude' => 80,
                'longitude' => 110,
                'province' => 'provinsi',
                'city' => 'kota',
                'subdistrict' => 'kecamatan'
            ]
        );

        $testResponse = new TestResponse($response);
        $testResponse->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreSuccess()
    {
        [$province, $city, $subdistrict] = $this->prepareAreaComponent();

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 50,
                'longitude' => 50,
                'province' => $province->name,
                'city' => $city->name,
                'subdistrict' => 'Kecamatan' . $subdistrict->name
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['address' => 'jl. test menuju roma']);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true]
        );
    }

    /**
     * @group PMS-495
     */
    public function testStoreWithZeroValueInCoordinateThrowException()
    {
        [$province, $city, $subdistrict] = $this->prepareAreaComponent();

        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 0,
                'longitude' => 0,
                'province' => $province->name,
                'city' => $city->name,
                'subdistrict' => 'Kecamatan' . $subdistrict->name
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithEmoticonStrippedSuccess()
    {
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => "Alamat dengan Emoticon🤤🤤🤤",
                'latitude' => 40,
                'longitude' => 60,
                'province' => 'Jawa Timur',
                'city' => 'Jember',
                'subdistrict' => 'Jambu'
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['address' => 'Alamat dengan Emoticon']);
        $this->assertSame($response->getData()->room->address, 'Alamat dengan Emoticon');
    }

    /**
     * @group PMS-495
     */    
    public function testStoreWithAddressNoteSuccess()
    {
        [$province, $city, $subdistrict] = $this->prepareAreaComponent();
        $response = $this->post(
            route(self::STORE_ROUTE),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 40,
                'longitude' => 60,
                'province' => $province->name,
                'city' => $city->name,
                'subdistrict' => 'Kecamatan ' . $subdistrict->name,
                'note' => 'Sebelah kanan wartel'
            ]
        );

        $response->assertSuccessful();
        $this->assertDatabaseHas((new Room())->getTable(), ['address' => 'jl. test menuju roma']);
        $this->assertDatabaseHas((new RoomOwner())->getTable(), ['user_id' => $this->owner->id]);
        $this->assertDatabaseHas(
            (new AddressNote())->getTable(),
            [
                'note' => 'Sebelah kanan wartel',
                'area_province_id' =>  $province->id,
                'area_city_id' =>  $city->id,
                'area_subdistrict_id' =>  $subdistrict->id,
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testGetNotFoundReturn404()
    {
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => 888]));
        $response->assertNotFound();
    }

    public function testGetSucess()
    {
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );
        $response = $this->get(route(self::RETRIEVE_ROUTE, ['songId' => $room->song_id]));

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_id',
                'address',
                'longitude',
                'latitude',
                'city',
                'subdistrict',
                'note'
            ]
        ]);
    }

    /**
     * @group PMS-495
     */    
    public function testEditUnValidParamsFailsProcess()
    {
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 110,
                'longitude' => 'bukan-angka',
                'province' => 'Jawa Timur',
                'city' => 'kota',
                'subdistrict' => 'kecamatan'
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotOwned()
    {
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            8888
        );
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju ctroma',
                'latitude' => 50,
                'longitude' => 50,
                'province' => 'Jawa Timur',
                'city' => 'kota',
                'subdistrict' => 'kecamatan'
            ]
        );

        $response->assertForbidden();
    }

    /**
     * @group PMS-495
     */    
    public function testEditFailsKostNotFound()
    {
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => 888]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 50,
                'longitude' => 59,
                'province' => 'Jawa Timur',
                'city' => 'kota',
                'subdistrict' => 'kecamatan'
            ]
        );

        $response->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testEditKostSuccess()
    {
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn'
            ],
            $this->owner->id
        );

        factory(Subdistrict::class)->create([
            'area_city_id' => factory(City::class)->create([
                'area_province_id' => factory(Province::class)->create([
                    'name' => 'Jawa Timur'
                ])->id,
                'name' => 'Jember'
            ])->id,
            'name' => 'Jambu'
        ]);

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 50,
                'longitude' => 50,
                'province' => 'Jawa Timur',
                'city' => 'Jember',
                'subdistrict' => 'Jambu'
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_id',
                'address',
                'longitude',
                'latitude',
                'city',
                'subdistrict',
                'note'
            ]
        ]);
        $this->assertEquals('jl. test menuju roma', $response->getData()->room->address);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */
    public function testEditKostWithNewNoteSuccess()
    {
        [$province, $city, $subdistrict] = $this->prepareAreaComponent();
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn'
            ],
            $this->owner->id
        );
        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 50,
                'longitude' => 50,
                'province' => $province->name,
                'city' => $city->name,
                'subdistrict' => $subdistrict->name,
                'note' => 'Sebelah Planet Ban'
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_id',
                'address',
                'longitude',
                'latitude',
                'city',
                'subdistrict',
                'note'
            ]
        ]);
        $this->assertEquals('jl. test menuju roma', $response->getData()->room->address);
        $this->assertEquals('Sebelah Planet Ban', $response->getData()->room->note);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true, 'designer_id' => $room->id]
        );

        $this->assertDatabaseHas(
            (new AddressNote())->getTable(),
            [
                'note' => 'Sebelah Planet Ban',
                'area_province_id' =>  $province->id,
                'area_city_id' =>  $city->id,
                'area_subdistrict_id' =>  $subdistrict->id,
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditKostHaveNoteNotUpdatedSuccess()
    {
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 60,
                'longitude' => 40,
                'province' => 'Jawa Timur',
                'city' => 'Jember',
                'subdistrict' => 'Jambu'
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_id',
                'address',
                'longitude',
                'latitude',
                'city',
                'subdistrict',
                'note'
            ]
        ]);
        $this->assertEquals('jl. test menuju roma', $response->getData()->room->address);
        $this->assertEquals($room->address_note->note, $response->getData()->room->note);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true, 'designer_id' => $room->id]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testEditKostHaveNoteUpdateNoteSuccess()
    {
        [$province, $city, $subdistrict] = $this->prepareAreaComponent();
        $room = $this->prepareRoomWithAddress(
            [
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note'
            ],
            $this->owner->id
        );
        $originNote = $room->address_note->note;

        $this->assertDatabaseMissing(
            (new AddressNote())->getTable(),
            [
                'area_province_id' =>  $province->id,
                'area_city_id' =>  $city->id,
                'area_subdistrict_id' =>  $subdistrict->id,
            ]
        );

        $response = $this->put(
            route(self::UPDATE_ROUTE, ['songId' => $room->song_id]),
            [
                'address' => 'jl. test menuju roma',
                'latitude' => 60,
                'longitude' => 30,
                'province' => $province->name,
                'city' => $city->name,
                'subdistrict' => $subdistrict->name,
                'note' => 'Sebelah Service Printer'
            ]
        );
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'meta',
            'room' => [
                '_id',
                'room_id',
                'address',
                'longitude',
                'latitude',
                'city',
                'subdistrict',
                'note'
            ]
        ]);
        $this->assertEquals('jl. test menuju roma', $response->getData()->room->address);
        $this->assertNotEquals($originNote, $response->getData()->room->note);
        $this->assertEquals('Sebelah Service Printer', $response->getData()->room->note);
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['name' => DataStage::STAGE_ADDRESS, 'complete' => true, 'designer_id' => $room->id]
        );
        $this->assertDatabaseHas(
            (new AddressNote())->getTable(),
            [
                'area_province_id' =>  $province->id,
                'area_city_id' =>  $city->id,
                'area_subdistrict_id' =>  $subdistrict->id,
            ]
        );
    }

    private function prepareRoomWithAddress(array $states, int $ownerId)
    {
        $room = factory(Room::class)->states($states)->create();
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $ownerId,
            'designer_id' => $room->id
        ]);

        return $room;
    }

    private function prepareAreaComponent(): array {
        $province = factory(Province::class)->create(['name' => 'Jawa Timur']);
        $city = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kabupaten Jember']);
        $subdistrict = factory(Subdistrict::class)->create(['area_city_id' => $city->id, 'name' => 'Rambipuji']);

        return [$province, $city, $subdistrict];
    }
}
