<?php

namespace App\Http\Controllers\Api;

use App\Entities\Area\Province;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

class ProvinceControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const GET_PROVINCES = 'api/v2/owner/data/kos/address/province';

    protected function setUp() : void
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName('app')
            ->setFunction(function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group PMS-495
     */
    public function testIndex()
    {
        $owner = factory(User::class)->states('owner')->create();

        $id = 1;
        factory(Province::class, 5)->make()->each(function ($province) use (&$id) {
            $province->id = $id++;
            $province->save();
        });

        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_PROVINCES);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);
    }
}
