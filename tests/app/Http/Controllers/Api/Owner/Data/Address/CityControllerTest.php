<?php

namespace App\Http\Controllers\Api\Owner\Data\Address;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Test\MamiKosTestCase;
use App\User;

class CityControllerTest extends MamiKosTestCase
{
    private const ROUTE = 'oauth.user-act.owner.data.kos.address.list-city';
    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->state('owner')->create();
    }

    /**
     * @group PMS-495
     */
    public function testIndexProvinceNameEmptyInvalid()
    {
        $this->actingAs($this->user, 'passport')->get(route(self::ROUTE))->assertStatus(400);
    }

    /**
     * @group PMS-495
     */
    public function testIndexProvinceNameNotFound()
    {
        $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'definitely-not-found-province-name']
                )
            )->assertNotFound();
    }

    /**
     * @group PMS-495
     */    
    public function testIndexSuccessWithSingleProvince()
    {
        $province = factory(Province::class)->create(['id' => 88]);
        $id = 1 ;
        factory(City::class, 5)->make()->each(function ($city) use ($province, &$id) {
            $city->id = $id++;
            $city->area_province_id = $province->id;
            $city->name = $city->name;
            $city->save();
        });

        $response = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => $province->name]
                )
            );
        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedContractStructure());
        $this->assertCount(5, $response->getData()->data);
    }

    /**
     * @group PMS-495
     */
    public function testIndexSuccessCaseYogyakarta()
    {
        $province = factory(Province::class)->create([
            'id' => 85,
            'name' => 'DI Yogyakarta'
        ]);
        $id = 1 ;
        factory(City::class, 5)->make()->each(function ($city) use ($province, &$id) {
            $city->id = $id++;
            $city->area_province_id = $province->id;
            $city->name = $city->name;
            $city->save();
        });

        $response1 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'Jogja']
                )
            );
        $response1->assertSuccessful();
        $this->assertCount(5, $response1->getData()->data);

        $response2 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'Daerah Istimewa Yogyakarta']
                )
            );

        $response2->assertSuccessful();
        $this->assertCount(5, $response2->getData()->data);

        $response3 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'Yogyakarta']
                )
            );

        $response3->assertSuccessful();
        $this->assertCount(5, $response3->getData()->data);

        $response4 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'DI Yogyakarta']
                )
            );

        $response4->assertSuccessful();
        $this->assertCount(5, $response4->getData()->data);
    }

    /**
     * @group PMS-495
     */    
    public function testIndexSuccessCaseIbukota()
    {
        $province = factory(Province::class)->create([
            'id' => 85,
            'name' => 'DKI Jakarta'
        ]);
        $id = 1 ;
        factory(City::class, 5)->make()->each(function ($city) use ($province, &$id) {
            $city->id = $id++;
            $city->area_province_id = $province->id;
            $city->name = $city->name;
            $city->save();
        });

        $response1 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'Daerah Khusus Ibukota Jakarta']
                )
            );

        $response1->assertSuccessful();
        $this->assertCount(5, $response1->getData()->data);

        $response2 = $this->actingAs($this->user, 'passport')
            ->get(
                route(
                    self::ROUTE,
                    ['province_name' => 'Jakarta']
                )
            );

        $response2->assertSuccessful();
        $this->assertCount(5, $response2->getData()->data);
    }

    private function expectedContractStructure(): array
    {
        return [
            'status',
            'meta',
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ];
    }
}
