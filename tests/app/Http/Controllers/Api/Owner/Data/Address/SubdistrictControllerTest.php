<?php

namespace App\Http\Controllers\Api\Owner\Data\Address;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Test\MamiKosTestCase;
use App\User;

class SubdistrictControllerTest extends MamiKosTestCase
{
    private const ROUTE = 'oauth.user-act.owner.data.kos.address.list-subdistrict';
    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->state('owner')->create();
    }

    /**
     * @group PMS-495
     */
    public function testIndexCityEmptyInvalid()
    {
        $this->actingAs($this->user, 'passport')->get(route(self::ROUTE))->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testIndexCityNotFound()
    {
        $this->actingAs($this->user, 'passport')
            ->get(route(self::ROUTE, ['city' => 'definitely-not-found-city']))
            ->assertNotFound();
    }

    /**
     * @group PMS-495
     */       
    public function testIndexSuccessWithSingleCity()
    {
        $province = factory(Province::class)->create(['id' => 88]);
        $city = factory(City::class)->create(['id' => 88, 'area_province_id' => $province->id]);
        $id = 82;
        factory(Subdistrict::class, 5)->make()->each(function ($subdistrict) use ($city, &$id) {
            $subdistrict->id = $id++;
            $subdistrict->area_city_id = $city->id;
            $subdistrict->save();
        });

        $response = $this->actingAs($this->user, 'passport')
            ->get(route(self::ROUTE, ['city' => $city->name]));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedContractStructure());
    }

    /**
     * @group PMS-495
     */       
    public function testIndexSuccessWithKotaKabupatenCities()
    {
        $province = factory(Province::class)->create(['id' => 88]);
        $city = factory(City::class)->create(['id' => 88, 'area_province_id' => $province->id, 'name' => 'Kota Malang']);
        $id = 82;
        factory(Subdistrict::class, 5)->make()->each(function ($subdistrict) use ($city, &$id) {
            $subdistrict->id = $id++;
            $subdistrict->area_city_id = $city->id;
            $subdistrict->save();
        });

        $city = factory(City::class)->create(['id' => 89, 'area_province_id' => $province->id, 'name' => 'Kabupaten Malang']);
        factory(Subdistrict::class, 5)->make()->each(function ($subdistrict) use ($city, &$id) {
            $subdistrict->id = $id++;
            $subdistrict->area_city_id = $city->id;
            $subdistrict->save();
        });

        
        $response = $this->actingAs($this->user, 'passport')->get(route(self::ROUTE, ['city' => 'Malang']));
        $response->assertSuccessful();
        $response->assertJsonStructure($this->expectedContractStructure());

        $this->assertCount(10, $response->getData()->data);
    }

    private function expectedContractStructure(): array
    {
        return [
            'status',
            'meta',
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ];
    }
}
