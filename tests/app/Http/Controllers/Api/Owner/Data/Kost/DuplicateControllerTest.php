<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Media\Media;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use App\User;

class DuplicateControllerTest extends MamiKosTestCase
{
    private const POST_DUPLICATE = 'oauth.user-act.owner.data.kos.duplicate';

    private $owner;

    protected function setUp(): void
    {
        parent::setUp();
        $this->owner = factory(User::class)->states(['owner'])->create();
        $this->actingAs($this->owner, 'passport');
    }

    /**
     * @group PMS-495
     */
    public function testDuplicateKostNotOwner()
    {
        [$property, $sourceRoom] = $this->prepareRoomWithProperty(123);
        
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'room_type'     => 'Tipe Z',
                'property_id'   => $property->id,
                'duplicate_from' => $sourceRoom->song_id,
            ]
        );

        $response->assertStatus(403);
    }

    /**
     * @group PMS-495
     */
    public function testDuplicateKostInvalidParam()
    {
        [$property, $sourceRoom] = $this->prepareRoomWithProperty($this->owner->id);
        
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'property_id'   => $property->id,
                'duplicate_from' => $sourceRoom->song_id,
            ]
        );

        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */    
    public function testDuplicateKostPropertyNotFound()
    {
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'property_id'   => 123,
            ]
        );

        $response->assertStatus(404);
    }

    /**
     * @group PMS-495
     */    
    public function testDuplicateKostRoomNotFound()
    {
        [$property, $sourceRoom] = $this->prepareRoomWithProperty($this->owner->id);
        
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'room_type'     => 'Tipe Z',
                'property_id'   => $property->id,
                'duplicate_from' => 123,
            ]
        );

        $response->assertStatus(404);
    }

    /**
     * @group PMS-495
     */    
    public function testDuplicateKostSuccess()
    {
        [$property, $sourceRoom] = $this->prepareRoomWithProperty($this->owner->id);
        
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'room_type'     => 'Tipe Z',
                'property_id'   => $property->id,
                'duplicate_from' => $sourceRoom->song_id,
            ]
        );

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'stages' => [
                DataStage::STAGE_ADDRESS,
                DataStage::STAGE_DATA,
                DataStage::STAGE_KOS_PHOTO,
                DataStage::STAGE_ROOM_PHOTO,
                DataStage::STAGE_FACILITY,
                DataStage::STAGE_ROOM_ALLOTMENT,
                DataStage::STAGE_PRICE,
            ],
            'is_active_photo_booking'
        ]);

        $responseData = $response->getData();
        $this->assertTrue($responseData->stages->information->complete);
        $this->assertTrue($responseData->stages->address->complete);
        $this->assertTrue($responseData->stages->building_photo->complete);
        $this->assertTrue($responseData->stages->room_photo->complete);
        $this->assertTrue($responseData->stages->facility->complete);
        $this->assertTrue($responseData->stages->room_allotment->complete);
        $this->assertTrue($responseData->stages->price->complete);
        // Room is not using Premium Photo
        $this->assertFalse($responseData->is_active_photo_booking);

        $this->assertDatabaseHas((new CreationFlag())->getTable(), ['create_from' => CreationFlag::DUPLICATE_FROM_TYPE]);
    }

    /**
     * @group PMS-495
     */    
    public function testDuplicateKostCreateNewTypeService()
    {
        [$property, $sourceRoom] = $this->prepareRoomWithProperty($this->owner->id);
        
        $response = $this->post(
            route(self::POST_DUPLICATE),
            [
                'property_id'   => $property->id,
            ]
        );

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'stages' => [
                DataStage::STAGE_ADDRESS,
                DataStage::STAGE_DATA,
                DataStage::STAGE_KOS_PHOTO,
                DataStage::STAGE_ROOM_PHOTO,
                DataStage::STAGE_FACILITY,
                DataStage::STAGE_ROOM_ALLOTMENT,
                DataStage::STAGE_PRICE,
            ],
            'is_active_photo_booking'
        ]);

        $responseData = $response->getData();
        $this->assertFalse($responseData->stages->information->complete);
        $this->assertTrue($responseData->stages->address->complete);
        $this->assertTrue($responseData->stages->building_photo->complete);
        $this->assertFalse($responseData->stages->room_photo->complete);
        $this->assertFalse($responseData->stages->facility->complete);
        $this->assertFalse($responseData->stages->room_allotment->complete);
        $this->assertFalse($responseData->stages->price->complete);
        // Room is not using Premium Photo
        $this->assertFalse($responseData->is_active_photo_booking);
        $this->assertDatabaseHas((new CreationFlag())->getTable(), ['create_from' => CreationFlag::DUPLICATE_WITHOUT_TYPE]);
    }

    private function prepareRoomWithProperty(int $userId)
    {
        $property = factory(Property::class)->create([
            'name' => 'Kost Mawar',
            'owner_user_id' => $userId
        ]);
        $room = factory(Room::class)
            ->states([
                'active',
                'with-small-rooms',
                'attribute-complete-state',
                'with-owner-collumn',
                'with-address-note',
                'with-all-prices'
            ])
            ->create([
                'property_id' => $property->id,
                'area_city' => 'Kota Solo',
                'area_subdistrict' => 'Kelayar',
            ]);
        factory(RoomOwner::class)->states(['owner'])->create([
            'user_id' => $userId,
            'designer_id' => $room->id,
        ]);

        $this->prepareRoomTerm($room);
        $this->prepareRoomTag($room);
        $this->prepareMedia($room->id, Group::INSIDE_BUILDING);
        $this->prepareMedia($room->id, Group::BUILDING_FRONT);
        $this->prepareMedia($room->id, Group::ROADVIEW_BUILDING);
        $this->prepareCover($room->id);
        $this->prepareMedia($room->id, Group::ROOM_FRONT);
        $this->prepareMedia($room->id, Group::INSIDE_ROOM);
        $this->prepareMedia($room->id, Group::OTHER);
        $this->createAdditionalCost($room->id);
        $this->createDepositFee($room->id);
        $this->createDownPayment($room->id);
        $this->createFine($room->id);

        return [$property, $room];
    }

    private function prepareRoomTag($room)
    {
        $tags = $this->prepareTags();

        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));
    }

    private function prepareTags(
        ?array $source = null
    ): array {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }

    private function prepareRoomTerm(Room $room)
    {
        $medias = factory(Media::class, 2)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[0]->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[1]->id
        ]);
    }

    private function prepareMedia(
        int $roomId,
        string $groupType
    ) {
        $medias = factory(Media::class, 3)->create();

        $medias->each(function ($media, $key) use ($roomId, $groupType) {
            factory(Card::class)->create([
                'photo_id' => $media->id,
                'designer_id' => $roomId,
                'description' => Card::DESCRIPTION_PREFIX[$groupType] . '-' . $key,
                'group' => $groupType,
            ]);
        });

        return $medias;
    }

    private function prepareCover(int $roomId)
    {
        $media = factory(Media::class)->create();

        factory(Card::class)->create([
            'photo_id' => $media->id,
            'designer_id' => $roomId,
            'description' => Card::DESCRIPTION_PREFIX['cover'],
            'group' => Group::INSIDE_ROOM,
            'is_cover' => true,
        ]);

        return $media;
    }

    private function createAdditionalCost(int $roomId)
    {
        return factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::ADDITIONAL
        ]);
    }

    private function createDepositFee(int $roomId)
    {
        return factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::DEPOSIT
        ]);
    }

    private function createDownPayment(int $roomId)
    {
        $dp = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::DP
        ]);

        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $dp->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => 30
        ]);

        return $dp;
    }

    private function createFine(int $roomId)
    {
        $fine = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'component' => MamipayRoomPriceComponentType::FINE
        ]);

        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE,
            'value' => 'day'
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $fine->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH,
            'value' => 3
        ]);

        return $fine;
    }
}
