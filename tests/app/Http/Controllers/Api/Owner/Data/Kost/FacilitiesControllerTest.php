<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\User;
use App\Entities\Room\Element\TagType;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Element\Tag;
use function factory;

class FacilitiesControllerTest extends MamiKosTestCase
{
    private const GET_FACILITIES_LIST = 'oauth/owner/data/kos/facilities';

    /**
     * @group PMS-495
     */    
    public function testIndexWithValidRole()
    {
        $this->markTestSkipped('must be revisited. possibly because excluded tag id');
        $owner = factory(User::class)->state('owner')->create();
        $tags = $this->prepareTags();
        
        $response = $this->actingAs($owner,'passport')->get(self::GET_FACILITIES_LIST);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => [
                'public',
                'bedroom',
                'bathroom',
                'parking',
                'near'
            ]
        ]);

        $this->assertEquals(
            count($tags['fac_room']),
            count($response->getData()->data->bedroom)
        );
        $this->assertEquals(
            count($tags['fac_bath']),
            count($response->getData()->data->bathroom)
        );
        $this->assertEquals(
            count($tags['fac_park']),
            count($response->getData()->data->parking)
        );
        $this->assertEquals(
            count($tags['fac_near']),
            count($response->getData()->data->near)
        );
        $this->assertEquals(
            count($tags['fac_share']),
            count($response->getData()->data->public)
        );
    }

    /**
     * @group PMS-495
     */
    public function testIndexWithInvalidRole()
    {
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'passport')->json('GET', self::GET_FACILITIES_LIST);

        $response->assertStatus(403);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    private function prepareTags(?array $source = null): array
    {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }
}
