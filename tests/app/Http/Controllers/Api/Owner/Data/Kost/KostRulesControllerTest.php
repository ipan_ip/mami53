<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\User;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Element\TagType;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Element\Tag;
use function factory;

class KostRulesControllerTest extends MamiKosTestCase
{
    private const RETRIEVE_LIST_ROUTE = 'oauth.user-act.owner.data.kos.list-rules';

    /**
     * @group PMS-495
     */    
    public function testIndexWithInvalidRole()
    {
        $user = factory(User::class)->state('tenant')->create(['role' => 'user']);
        $response = $this->actingAs($user, 'passport')->get(route(self::RETRIEVE_LIST_ROUTE));
        $response->assertStatus(403);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    /**
     * @group PMS-495
     */
    public function testIndexWithValidRole()
    {
        $this->markTestSkipped('must be revisited. possibly because excluded tag id');
        $owner = factory(User::class)->state('owner')->create();
        $tags = $this->prepareTags([TagGroup::RULE_TAG_TYPE => [
            'Pasutri',
            'Kunci 24 Jam',
            'Boleh Bawa Heman Peliharaan'
        ]]);
        
        $response = $this->actingAs($owner, 'passport')->get(route(self::RETRIEVE_LIST_ROUTE));

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);

        $this->assertEquals(
            count($tags[TagGroup::RULE_TAG_TYPE]),
            count($response->getData()->data)
        );
    }

    private function prepareTags(?array $source = null): array
    {
        $source = $source ?? [
            TagGroup::RULE_TAG_TYPE => [
                'Pasutri',
                'Kunci 24 Jam',
                'Boleh Bawa Heman Peliharaan'
            ],
            TagGroup::BATHROOM_TAG_TYPE => [
                'Air Panas',
                'Kloset Duduk'
            ]
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }
}
