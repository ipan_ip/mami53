<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Log\LogTemp;
use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class ListControllerTest extends MamiKosTestCase
{
    private const LIST_ROUTE = 'oauth.user-act.owner.data.property.list';

    /**
     * @group PMS-495
     */    
    public function testIndexAllSuccess()
    {
        $user = factory(User::class)->state('owner')->create();
        $properties = factory(Property::class, 4)->create(['owner_user_id' => $user->id]);
        foreach ($properties as $property) {
            $room = factory(Room::class)->create([
                'property_id' => $property->id
            ]);
            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => RoomOwner::ROOM_EDIT_STATUS
            ]);
        }

        $this->mock(LogTemp::class)->shouldReceive()->logApiRequest()->andReturn(null);
        $response = $this->actingAs($user, 'passport')->get(route(self::LIST_ROUTE, ['filter' => 'all']));

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => [[
                'id',
                'name',
                'allow_multiple',
                'icon'
            ]]
        ]);
    }

    /**
     * @group PMS-495
     */    
    public function testIndexHaveNoMultipleProperty()
    {
        $user = factory(User::class)->state('owner')->create();
        $properties = factory(Property::class, 4)->create(['owner_user_id' => $user->id, 'has_multiple_type' => false]);
        foreach ($properties as $property) {
            $room = factory(Room::class)->create([
                'property_id' => $property->id
            ]);
            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => RoomOwner::ROOM_EDIT_STATUS
            ]);
        }

        $this->mock(LogTemp::class)->shouldReceive()->logApiRequest()->andReturn(null);
        $response = $this->actingAs($user, 'passport')->get(route(self::LIST_ROUTE, ['filter' => 'multiple']));

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);

        $this->assertCount(0, $response->getData()->data);
    }

    /**
     * @group PMS-495
     */
    public function testIndexHaveMultipleProperty()
    {
        $user = factory(User::class)->state('owner')->create();
        $properties = factory(Property::class, 4)->create(['owner_user_id' => $user->id, 'has_multiple_type' => true]);
        foreach ($properties as $key => $property) {
            $room = factory(Room::class)->create([
                'property_id' => $property->id
            ]);
            factory(RoomOwner::class)->create([
                'designer_id' => $room->id,
                'user_id' => $user->id,
                'status' => $key === 3 ? RoomOwner::ROOM_EARLY_EDIT_STATUS : RoomOwner::ROOM_EDIT_STATUS
            ]);
        }

        $this->mock(LogTemp::class)->shouldReceive()->logApiRequest()->andReturn(null);
        $response = $this->actingAs($user, 'passport')->get(route(self::LIST_ROUTE, ['filter' => 'multiple']));

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);

        $this->assertCount(3, $response->getData()->data);
    }
}
