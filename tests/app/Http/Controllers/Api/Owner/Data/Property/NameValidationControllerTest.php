<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class NameValidationControllerTest extends MamiKosTestCase
{
    private const PROPERTY_NAME_ENDPOINT = 'oauth.user-act.owner.data.property.validation.name';
    private const TYPE_NAME_ENDPOINT = 'oauth.user-act.owner.data.property.validation.type.name';

    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->state('owner')->create();
    }

    /**
     * @group PMS-495
     */
    public function testPropertyNameEmptyFailed()
    {
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::PROPERTY_NAME_ENDPOINT),
            [
                'name' => ''
            ]
        );
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */
    public function testPropertyNameUniqueSuccess()
    {
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::PROPERTY_NAME_ENDPOINT),
            [
                'name' => 'Definitely Not Used Property Name'
            ]
        );

        $response->assertSuccessful();
        $this->assertTrue($response->getData()->available, "Name Available");
    }

    /**
     * @group PMS-495
     */
    public function testPropertyNameNotUniqueReturnFalse()
    {
        factory(Property::class)->create([
            'name' => 'Not Unique Property',
            'owner_user_id' => $this->user->id
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::PROPERTY_NAME_ENDPOINT),
            [
                'name' => 'Not Unique Property'
            ]
        );

        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available, "Name Unavailable");
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeEmptynameFailed()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            []
        );
        $response->assertStatus(400);
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeNotUniqueOnUnitTypeReturnFalse()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => 'Not Unique Type'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Not Unique Type'
            ]
        );
        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available);
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeNotUniqueOnNameReturnFalse()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => true
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => null,
            'name' => "The Property Type A Blimbing Kota Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type A'
            ]
        );
        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available, 'Name Unavailable');
    }

    public function testKostTypeNotUniqueCaseInsensitiveOnNameReturnFalse()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => true
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => null,
            'name' => "The Property Type A Blimbing Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type a'
            ]
        );
        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available, 'Name Unavailable');
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeNotUniqueOnNameWithoutDistrictAndCityReturnFalse()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => true
        ]);

        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => null,
            'name' => "The Property Type A",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type A'
            ]
        );
        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available, 'Name Unavailable');
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeUniqueSuccess()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => true
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => 'Type A',
            'name' => "The Property Type A Blimbing Kota Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type B'
            ]
        );
        $response->assertSuccessful();
        $this->assertTrue($response->getData()->available, 'Name Available');
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeUniqueMultiplePropertyFalseSuccess()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => false
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => null,
            'name' => "The Property Blimbing Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type A'
            ]
        );
        $response->assertSuccessful();
        $this->assertTrue($response->getData()->available, 'Name Available');
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeNotUniqueMultiplePropertyFalseSuccess()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id,
            'has_multiple_type' => false
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => 'Type A',
            'name' => "The Property Blimbing Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type a'
            ]
        );
        $response->assertSuccessful();
        $this->assertFalse($response->getData()->available, 'Name Unavailable');
    }

    /**
     * @group PMS-495
     */
    public function testKostTypeNotOwnedForbidden()
    {
        $property = factory(Property::class)->create([
            'name' => 'The Property',
            'owner_user_id' => $this->user->id + 8
        ]);
        factory(Room::class)->state('active')->create([
            'property_id' => $property->id,
            'unit_type' => null,
            'name' => "The Property Type A Blimbing Kota Malang",
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ]);
        $response = $this->actingAs($this->user, 'passport')->post(
            route(self::TYPE_NAME_ENDPOINT, ['id' => $property->id]),
            [
                'name' => 'Type A'
            ]
        );
        $response->assertNotFound();
    }
}
