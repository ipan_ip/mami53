<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class TypeControllerTest extends MamiKosTestCase
{
    private const LIST_ROUTE = 'oauth.user-act.owner.data.property.by-id.types';

    /**
     * @group PMS-495
     */    
    public function testIndexSuccess()
    {
        $user = factory(User::class)->state('owner')->create();
        $property = factory(Property::class)->create([
            'owner_user_id' => $user->id,
            'has_multiple_type' => true,
            'name' => 'Kost Lawak'
        ]);

        $type = ['A', 'B', 'C', 'D'];
        factory(Room::class, 4)->states(['active'])->make([
            'property_id' => $property->id,
            'area_subdistrict' => 'Blimbing',
            'area_city' => 'Kota Malang'
        ])->each(
            function ($room, $index) use ($property, $user, $type) {
                $room->unit_type = $type[$index];
                $room->name = "$property->name $room->unit_type $room->area_subdistrict $room->area_city";
                $room->save();

                factory(RoomOwner::class)->state('owner')->create([
                    'designer_id' => $room->id,
                    'user_id' => $user->id,
                    'status' => $index === 3 ? RoomOwner::ROOM_EARLY_EDIT_STATUS : RoomOwner::ROOM_EDIT_STATUS
                ]);
            }
        );

        $response = $this->actingAs($user, 'passport')->get(
            route(self::LIST_ROUTE, ['id' => $property->id])
        );

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta',
            'data' => [[
                '_id',
                'name',
                'unit_type'
            ]]
        ]);
        $this->assertCount(3, $response->getData()->data, "Only count room with status waiting or verified");
    }
}
