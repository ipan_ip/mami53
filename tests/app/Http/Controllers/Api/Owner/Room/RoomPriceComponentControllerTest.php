<?php

namespace App\Http\Controllers\Api\Owner\Room;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use function GuzzleHttp\Psr7\str;


class RoomPriceComponentControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const API_GET_PRICE_COMPONENT   = 'api/v1/owner/room/price-component/{songId}';
    const API_POST_PRICE_COMPONENT  = 'api/v1/owner/room/price-component/{songId}';
    const API_PUT_PRICE_COMPONENT   = 'api/v1/owner/room/price-component/{priceComponentId}';
    const API_DELETE_PRICE_COMPONENT= 'api/v1/owner/room/price-component/{priceComponentId}';
    const API_POST_SWITCH_PRICE_COMPONENT  = 'api/v1/owner/room/price-component/toggle/{songId}/{type}';

    public function testGetAdditionalPrice(): void
    {
        $response = $this->getComponentPrice();
        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testCreateAdditionalPrice(): void
    {
        $response = $this->createComponentPrice([
            'type' => MamipayRoomPriceComponentType::ADDITIONAL,
            'name' => 'Iuran Sampah',
            'price' => 30000
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testCreateFinePrice(): void
    {
        $response = $this->createComponentPrice([
            'type' => MamipayRoomPriceComponentType::FINE,
            'price' => 60000,
            'fine_maximum_length' => 1,
            'fine_duration_type' => 'week'
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testCreateDepositPrice(): void
    {
        $response = $this->createComponentPrice([
            'type' => MamipayRoomPriceComponentType::DEPOSIT,
            'price' => 500000,
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testCreateDPPrice(): void
    {
        $response = $this->createComponentPrice([
            'type' => MamipayRoomPriceComponentType::DP,
            'percentage' => 30,
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testUpdateAdditionalPrice(): void
    {
        // prepare data
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'component' => MamipayRoomPriceComponentType::ADDITIONAL,
            'name' => 'Iuran Sampah',
            'label' => 'Biaya Tambahan Lain',
            'price'  => 30000,
            'is_active' => 1
        ]);

        $response = $this->updateComponentPrice($priceComponentEntity->id, [
            'type' => MamipayRoomPriceComponentType::ADDITIONAL,
            'name' => 'Bayar Listrik',
            'price' => 50000
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testUpdateFinePrice(): void
    {
        // prepare data
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'component' => MamipayRoomPriceComponentType::FINE,
            'price'  => 30000,
            'is_active' => 1
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $priceComponentEntity->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH,
            'value' => 1
        ]);

        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $priceComponentEntity->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE,
            'value' => 'week'
        ]);

        $response = $this->updateComponentPrice($priceComponentEntity->id, [
            'type' => MamipayRoomPriceComponentType::FINE,
            'price' => 100000,
            'fine_maximum_length' => 10,
            'fine_duration_type' => 'day'
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testUpdateDepositPrice(): void
    {
        // prepare data
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'component' => MamipayRoomPriceComponentType::DEPOSIT,
            'price'  => 500000,
            'is_active' => 1
        ]);

        $response = $this->updateComponentPrice($priceComponentEntity->id, [
            'type' => MamipayRoomPriceComponentType::DEPOSIT,
            'price' => 400000,
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testUpdateDPPrice(): void
    {
        // prepare data
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'component' => MamipayRoomPriceComponentType::DP
        ]);
        factory(MamipayRoomPriceComponentAdditional::class)->create([
            'mamipay_designer_price_component_id' => $priceComponentEntity->id,
            'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
            'value' => 50
        ]);

        $response = $this->updateComponentPrice($priceComponentEntity->id, [
            'type' => MamipayRoomPriceComponentType::DP,
            'percentage' => 30,
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testDestroySuccess(): void
    {
        // prepare data
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'component' => MamipayRoomPriceComponentType::DP
        ]);
        $userEntity = factory(User::class)->create();

        // map data
        $url = str_replace('{priceComponentId}', $priceComponentEntity->id, self::API_DELETE_PRICE_COMPONENT);
        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'DELETE',
            $this->withAccessToken($url)
        );

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    public function testSwitchAdditionalPrice(): void
    {
        $response = $this->switchComponentPrice(MamipayRoomPriceComponentType::ADDITIONAL, [
            'active' => 1
        ]);

        // run test
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
        ]);
    }

    /**
     * @param array $dataBugsnag
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function createComponentPrice(array $data = [])
    {
        // prepare data
        $room = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        // map data
        $url = str_replace('{songId}', $room->song_id, self::API_POST_PRICE_COMPONENT);
        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'POST',
            $this->withAccessToken($url),
            $data
        );
        return $response;
    }

    /**
     * @param $id
     * @param array $data
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function updateComponentPrice($id, array $data =[])
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // map data
        $url = str_replace('{priceComponentId}', $id, self::API_PUT_PRICE_COMPONENT);
        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'PUT',
            $this->withAccessToken($url),
            $data
        );
        return $response;
    }

    /**
     * @param array $dataBugsnag
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function getComponentPrice(array $data = [])
    {
        // prepare data
        $room = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        // map data
        $url = str_replace('{songId}', $room->song_id, self::API_GET_PRICE_COMPONENT);
        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken($url),
            $data
        );
        return $response;
    }

    /**
     * @param array $dataBugsnag
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function switchComponentPrice($type, array $data = [])
    {
        // prepare data
        $room = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();

        // map data
        $url = str_replace('{songId}', $room->song_id, self::API_POST_SWITCH_PRICE_COMPONENT);
        $url = str_replace('{type}', $type, $url);
        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'POST',
            $this->withAccessToken($url),
            $data
        );
        return $response;
    }
}
