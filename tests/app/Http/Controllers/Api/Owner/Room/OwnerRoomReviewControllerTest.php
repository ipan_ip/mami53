<?php

namespace App\Http\Controllers\Api\Owner\Room;

use App\Entities\Media\Media;
use App\Entities\Room\Review;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Room\RoomOwner;
use Illuminate\Support\Facades\Config;

class OwnerRoomReviewControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    const API_INDEX_ROOM_REVIEW   = 'api/v1/owner/room/review';

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('rating.scale', 5);
    }

    private function createRoom(User $user): Room
    {
        /** @var Room */
        $room = factory(Room::class)->create(['photo_id' => factory(Media::class)]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);
        return $room;
    }

    private function createReview(Room $room, ?float $rating = null): Review
    {
        $rating = is_null($rating) ? rand(25, 50) / 10 : $rating;
        $review = factory(Review::class)->create([
            'designer_id' => $room->id,
            'scale' => 5,
            'cleanliness' => $rating,
            'comfort' => $rating,
            'safe' => $rating,
            'price' => $rating,
            'room_facility' => $rating,
            'public_facility' => $rating,
            'status' => 'live'
        ]);
        
        return $review;
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexStructureResponse(): void
    {
        $userEntity = factory(User::class)->create();
        $room = $this->createRoom($userEntity);
        $this->createReview($room);

        app()->user = $userEntity;
        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 5,
                'offset' => 0
            ])
        );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta',
            'pagination' => [
                'current_page',
                'limit',
                'has_more'
            ],
            'data' => [
                [
                    'id',
                    'room_title',
                    'photo' => [
                        'real',
                        'small',
                        'medium',
                        'large'
                    ],
                    'review_count',
                    'review_avg_rating'
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexShouldReturnCorrectResponse(): void
    {
        $userEntity = factory(User::class)->create();
        $room = $this->createRoom($userEntity);
        $this->createReview($room, 4.3);
        $this->createReview($room, 4.1);

        app()->user = $userEntity;
        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 5,
                'offset' => 0
            ])
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'pagination' => [
                'current_page' => 1,
                'limit' => 5,
                'has_more' => false
            ],
            'data' => [
                [
                    'id' => $room->song_id,
                    'room_title' => $room->name,
                    'review_count' => 2,
                    'review_avg_rating' => 4.2
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexWithCorrectLimitAndOffset(): void
    {
        $userEntity = factory(User::class)->create();
        $room1 = $this->createRoom($userEntity);
        $this->createReview($room1);
        $this->createReview($room1);
        $this->createReview($room1);

        $room2 = $this->createRoom($userEntity);
        $this->createReview($room2);
        $this->createReview($room2);
        
        $room3 = $this->createRoom($userEntity);
        $this->createReview($room3, 4.1);

        app()->user = $userEntity;

        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 2,
                'offset' => 0
            ])
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'pagination' => [
                'current_page' => 1,
                'limit' => 2,
                'has_more' => true
            ],
            'data' => [
                [
                    'id' => $room1->song_id,
                    'review_count' => 3,
                ], [
                    'id' => $room2->song_id,
                    'review_count' => 2,
                ]
            ]
        ]);

        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 2,
                'offset' => 2
            ])
        );

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'pagination' => [
                'current_page' => 2,
                'limit' => 2,
                'has_more' => false
            ],
            'data' => [
                [
                    'id' => $room3->song_id,
                    'room_title' => $room3->name,
                    'review_count' => 1,
                    'review_avg_rating' => 4.1
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexWithoutLimitShouldReturnFalse(): void
    {
        $userEntity = factory(User::class)->create();
        $room = $this->createRoom($userEntity);
        $this->createReview($room, 4.3);
        $this->createReview($room, 4.1);

        app()->user = $userEntity;
        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'offset' => 0
            ])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'error' => [
                'messages' => __('validation.required', ['attribute' => 'limit'])
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexWithoutOffsetShouldReturnFalse(): void
    {
        $userEntity = factory(User::class)->create();
        $room = $this->createRoom($userEntity);
        $this->createReview($room, 4.3);
        $this->createReview($room, 4.1);

        app()->user = $userEntity;
        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 5
            ])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'error' => [
                'messages' => __('validation.required', ['attribute' => 'offset'])
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App\Http\Controllers\Api\Owner\Room\OwnerRoomReviewController
     */
    public function testIndexWithUserTenantShouldReturnFalse(): void
    {
        $userEntity = factory(User::class)->create(['is_owner' => 'false']);

        app()->user = $userEntity;
        $response = $this->actingAs($userEntity)->json(
            'GET',
            $this->withAccessToken(self::API_INDEX_ROOM_REVIEW). '&' . http_build_query([
                'limit' => 5,
                'offset' => 0
            ])
        );
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'issue' => [
                'message' => "Invalid user."
            ]
        ]);
    }
}
