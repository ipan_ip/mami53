<?php

namespace App\Http\Controllers\Api\Owner;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

class ConfigControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_CONFIG_AB_TEST = 'api/v1/config/ab-test/owner-experience';

    protected function setUp() : void
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction(function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    public function testGetConfigSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $response = $this->json('GET', self::GET_CONFIG_AB_TEST);

        $response->assertOk();
        $response->assertJsonStructure($this->expectedApiContracts());

        $responseData = $response->getData();
        $experimentId = env('OX_FORCE_BBK_AB_TEST_EXPERIMENT_ID', null);
        $useVarian = env('OX_FORCE_BBK_AB_TEST_USE_VARIAN', false);
        $varian = env('OX_FORCE_BBK_AB_TEST_PREFERRED_VARIAN', 'control');

        $this->assertEquals($experimentId, $responseData->data->force_bbk->experiment_id);
        $this->assertEquals($useVarian, $responseData->data->force_bbk->use_varian);
        $this->assertEquals($varian, $responseData->data->force_bbk->varian);
    }

    private function expectedApiContracts(): array
    {
        return [
            'status',
            'meta'  => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data' => [
                'force_bbk' => [
                    'experiment_id',
                    'use_varian',
                    'varian'
                ]
            ],
        ];
    }
}
