<?php 
namespace App\Http\Controllers\Api;

use App\Entities\Mamipay\MamipayTenant;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\Entities\Activity\ActivationCode;
use App\Entities\User\UserVerificationAccount;
use App\User;
use Bugsnag;
use Carbon\Carbon;
use Config;

class UserVerificationAccountControllerTest extends MamiKosTestCase 
{

    use WithoutMiddleware;

    private const POST_VERIFICATION_EMAIL_API_URL = 'api/v1/user/verification/email';
    private const POST_VERIFICATION_PHONE_NUMBER_CHECK_OTP_API_URL = 'api/v1/user/verification/verify-code-otp';
    private const GET_VERIFICATION_IDENTITY_OPTIONS_URL = 'api/v1/user/verification/identity-card/options';

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\UserDataRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
        Bugsnag::spy();
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testSendVerificationEmailSuccess()
    {
        $email = 'test@'.str_random(5).'.com';
        $user = factory(\App\User::class)->create();
        $this->actingAs($user);

        $param = [
            'email' => $email,
            'location' => "-7.1232323, 21321321",
        ];

        $response = $this->json('POST', self::POST_VERIFICATION_EMAIL_API_URL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Buka email anda untuk verifikasi',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testSendVerificationEmailWithNotValidEmail()
    {
        $email = 'test'.str_random(5).'.com';
        $user = factory(\App\User::class)->create();
        $this->actingAs($user);

        $param = [
            'email' => $email,
            'location' => "-7.1232323, 21321321",
        ];

        $response = $this->json('POST', self::POST_VERIFICATION_EMAIL_API_URL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Format email tidak sesuai',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testSendVerificationEmailSuccessWithValidJsonResponse()
    {
        $email = str_random(10).'@'.str_random(15).'.'.str_random(5);
        $user = factory(\App\User::class)->create();
        $this->actingAs($user);

        $param = [
            'email' => $email,
            'location' => "-7.1232323, 21321321",
        ];

        $response = $this->json('POST', self::POST_VERIFICATION_EMAIL_API_URL, $param);
        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testSendVerificationEmailWithOwnerEmail()
    {
        $owner = factory(\App\User::class)->state('owner')->create();
        $user = factory(\App\User::class)->create(['email' => $owner->email,]);

        $this->actingAs($user);

        $param = [
            'email' => $owner->email,
            'location' => "-7.1232323, 21321321",
        ];

        $response = $this->json('POST', self::POST_VERIFICATION_EMAIL_API_URL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => __('api.tenant.verification.email.exists'),
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testSendVerificationEmailWithNoUserData()
    {
        $email = str_random(10).'@'.str_random(15).'.'.str_random(5);
        
        $param = [
            'email' => $email,
            'location' => "-7.1232323, 21321321",
        ];

        $response = $this->json('POST', self::POST_VERIFICATION_EMAIL_API_URL, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Data user tidak ditemukan',
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-1046
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testVerificationCheckOtpWithPregisteredMamipayTenant()
    {
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => null,
            'phone_number' => '0811231123',
        ]);
        /** @var ActivationCode */
        $activationCode = factory(ActivationCode::class)->create([
            'phone_number' => $mamipayTenant->phone_number,
            'code' => 'XXX1',
            'expired_at' => Carbon::now()->addDay(1),
        ]);
        /** @var User */
        $user = factory(User::class)->create([
            'phone_number' => null,
        ]);

        $response = $this->actingAs($user)->json('POST', self::POST_VERIFICATION_PHONE_NUMBER_CHECK_OTP_API_URL, [
            'phone_number' => $mamipayTenant->phone_number,
            'verification_code' => 'XXX1',
        ]);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);

        $mamipayTenant->refresh();
        $user->refresh();
        $this->assertEquals($mamipayTenant->phone_number, $user->phone_number);
        $this->assertNotNull($mamipayTenant->user);
        $this->assertEquals($mamipayTenant->user->id, $user->id);
    }
    /**
     * @group UG
     * @group UG-1461
     * @group App/Http/Controllers/Api/UserVerificationAccountControllerTest
     */
    public function testGetIdentityCardOptions()
    {
        $response = $this->actingAs(factory(User::class)->make())->getJson(self::GET_VERIFICATION_IDENTITY_OPTIONS_URL);
        $response->assertOk();
        $response->assertJsonStructure([
            'data'
        ]);
        $json = $response->json();
        $this->assertNotEmpty($json['data']);
        // For each item, at least has title
        foreach ($json['data'] as $option) {
            $this->assertArrayHasKey('title', $option);
            $this->assertNotEmpty($option['title']);
        }
    }

    public function getVerificationEmail(string $verificationLink)
    {
        // Disabling debug to ignore mix not found issue.
        Config::set('app.debug', false);
        return $this->get('/verification/user/email/' . $verificationLink);
    }

    public function createVerificationEmailLink(string $email, int $userId, int $expiredTimestamp, string $phoneNumber)
    {
        return base64_encode(
            $email . "&" .
            $userId . "&" .
            date("Y-m-d H:i:s", $expiredTimestamp) . "&" .
            $phoneNumber
        );
    }

    /**
     * @group UG
     * @group UG-3886
     */
    public function testVerificationEmailValidLink()
    {
        /** @var User */
        $user = factory(User::class)->create();
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user,
            'verify_link' => $this->createVerificationEmailLink(
                $user->email,
                $user->id,
                strtotime('+2 hours'),
                $user->phone_number
            ),
        ]);
        $response = $this->getVerificationEmail($userVerificationAccount->verify_link);
        $response->assertRedirect('/success/sukses-email-verifikasi');

        $userVerificationAccount->refresh();
        $this->assertEquals(true, $userVerificationAccount->is_verify_email);
    }

    /**
     * @group UG
     * @group UG-3886
     */
    public function testVerificationEmailFromProdIssueReport()
    {
        /** @var User */
        $user = factory(User::class)->create([
            'id' => 2942527,
            'email' => 'testmamiwebdua@gmail.com',
            'phone_number' => '08193487573',
        ]);
        Carbon::setTestNow(
            Carbon::create(2020, 11, 17, 9, 55, 14)
        );
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $user,
            'verify_link' => $this->createVerificationEmailLink(
                $user->email,
                $user->id,
                Carbon::create(2020, 11, 17, 11, 55, 14)->timestamp,
                $user->phone_number
            ),
        ]);
        $response = $this->getVerificationEmail($userVerificationAccount->verify_link);
        $response->assertRedirect('/success/sukses-email-verifikasi');

        $userVerificationAccount->refresh();
        $this->assertEquals(true, $userVerificationAccount->is_verify_email);
    }
}
