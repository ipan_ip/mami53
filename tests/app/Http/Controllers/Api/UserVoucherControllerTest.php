<?php

namespace App\Http\Controller\Api;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Room\Room;
use App\Presenters\UserVoucherPresenter;
use App\Repositories\Voucher\VoucherRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class UserVoucherControllerTest extends MamiKosTestCase
{
    use WithoutEvents, WithoutMiddleware;

    private const URL_GET_USER_VOUCHER_LIST = 'api/v1/user/voucher/list?type=%s';
    private const URL_GET_USER_VOUCHER_DETAIL = 'api/v1/user/voucher/detail/%d';
    private const URL_GET_USER_VOUCHER_COUNT = 'api/v1/user/voucher/count';

    private $repo;
    private $presenter;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->createMock(VoucherRepositoryEloquent::class);
        $this->presenter = $this->createMock(UserVoucherPresenter::class);
        $this->app->instance(VoucherRepositoryEloquent::class, $this->repo);
        $this->app->instance(UserVoucherPresenter::class, $this->presenter);
        $this->app->user = null;
        $this->app->device = null;
    }

    public function testIndex_TypeActive_getPaginatedResponse()
    {
        $userId = 1;
        $userEmail = 'arif@mamiteam.com';
        $userJob = 'mahasiswa';
        $userWorkplace = 'Universitas Mamikos';
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $listResponse = factory(MamipayVoucher::class, 5)->make()->toArray();
        $listResponse = $this->paginationResponse($listResponse);

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getActiveUserVoucherList')
            ->willReturn($listResponse);

        $type = 'active';
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $type));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $listResponse['data']]);
    }

    public function testIndex_TypeExpired_getPaginatedResponse()
    {
        $userId = 1;
        $userEmail = 'arif@mamiteam.com';
        $userJob = 'mahasiswa';
        $userWorkplace = 'Universitas Mamikos';
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $listResponse = factory(MamipayVoucher::class, 5)->make()->toArray();
        $listResponse = $this->paginationResponse($listResponse);

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getExpiredUserVoucherList')
            ->willReturn($listResponse);

        $type = 'expired';
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $type));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $listResponse['data']]);
    }

    public function testIndex_TypeUsed_getPaginatedResponse()
    {
        $userId = 1;
        $userEmail = 'arif@mamiteam.com';
        $userJob = 'mahasiswa';
        $userWorkplace = 'Universitas Mamikos';
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $listResponse = factory(MamipayVoucher::class, 5)->make()->toArray();
        $listResponse = $this->paginationResponse($listResponse);

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getUsedUserVoucherList')
            ->willReturn($listResponse);

        $type = 'used';
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $type));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $listResponse['data']]);
    }

    public function testIndex_TypeUsed_EmptyUserProfile_getPaginatedResponse()
    {
        $userId = 1;
        $userEmail = null;
        $userJob = null;
        $userWorkplace = null;
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $listResponse = factory(MamipayVoucher::class, 5)->make()->toArray();
        $listResponse = $this->paginationResponse($listResponse);

        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getUsedUserVoucherList')
            ->willReturn($listResponse);

        $type = 'used';
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $type));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $listResponse['data']]);
    }

    public function testIndex_TypeUsed_EmptyEmail_getPaginatedResponse()
    {
        $userId = 1;
        $userEmail = null;
        $userJob = 'mahasiswa';
        $userWorkplace = 'Universitas Mamikos';
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $listResponse = factory(MamipayVoucher::class, 5)->make()->toArray();
        $listResponse = $this->paginationResponse($listResponse);
        
        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getUsedUserVoucherList')
            ->willReturn($listResponse);

        $type = 'used';
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $type));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $listResponse['data']]);
    }

    public function testIndex_NonLogin_StatusFalse()
    {
        $status = 'active';
        $response = $this->json('GET', sprintf(self::URL_GET_USER_VOUCHER_LIST, $status));

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testShow_getTransformedResponse()
    {
        $userId = 1;
        $user = factory(User::class)->create(['id' => $userId]);
        $showResponse = factory(MamipayVoucher::class)->make()->toArray();
        $showResponse = $this->paginationResponse($showResponse);

        $this->presenter->expects($this->once())->method('setResponseType');
        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getByID')
            ->willReturn($showResponse);

        $voucherId = 1;
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_DETAIL, $voucherId));

        $response->assertOk();
        $response->assertJsonFragment(['data' => $showResponse['data']]);
    }

    public function testShow_NonLogin_StatusFalse()
    {
        $voucherId = 1;
        $response = $this->json('GET', sprintf(self::URL_GET_USER_VOUCHER_DETAIL, $voucherId));

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testShow_NotFoundVoucherId()
    {
        $userId = 1;
        $user = factory(User::class)->create(['id' => $userId]);

        $this->presenter->expects($this->once())->method('setResponseType');
        $this->repo->expects($this->once())->method('setPresenter');
        $this->repo->expects($this->once())
            ->method('getByID')
            ->willReturn(null);

        $voucherId = 9999; // not found id
        $notfoundResponse = $this->notfoundResponse();
        $response = $this->actingAs($user)->json('GET', sprintf(self::URL_GET_USER_VOUCHER_DETAIL, $voucherId));
        
        $response->assertOk();
        $response->assertJsonMissing(['data']);
        $response->assertJsonFragment(['status' => false]);
        $response->assertJsonFragment($notfoundResponse['meta']);
    }

    public function testCount_getCount()
    {
        $userId = 1;
        $userEmail = 'arif@mamiteam.com';
        $userJob = 'mahasiswa';
        $userWorkplace = 'Universitas Mamikos';
        $kostId = 2;
        $kostCity = 'Surabaya';
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        factory(Room::class)->create(
            [
                'song_id' => $kostId,
                'area_city' => $kostCity
            ]
        );
        factory(BookingUser::class)->create(
            [
                'user_id' => $userId,
                'designer_id' => $kostId,
                'status' => BookingUser::BOOKING_STATUS_VERIFIED
            ]
        );
        $countResponse = 1;

        $this->repo->expects($this->once())
            ->method('countActiveUserVoucher')
            ->willReturn($countResponse);

        $response = $this->actingAs($user)->json('GET', self::URL_GET_USER_VOUCHER_COUNT);

        $response->assertOk();
        $response->assertJsonFragment(['count' => $countResponse]);
    }

    public function testCount_EmptyUserProfile_getZeroCount()
    {
        $userId = 1;
        $userEmail = null;
        $userJob = null;
        $userWorkplace = null;
        $user = factory(User::class)->create(
            [
                'id' => $userId,
                'email' => $userEmail,
                'job' => $userJob,
                'work_place' => $userWorkplace
            ]
        );
        factory(MamipayTenant::class)->create([
            'user_id' => $userId,
            'email' =>$userEmail,
        ]);
        $countResponse = 0;

        $response = $this->actingAs($user)->json('GET', self::URL_GET_USER_VOUCHER_COUNT);

        $response->assertOk();
        $response->assertJsonFragment(['count' => $countResponse]);
    }

    public function testCount_NonLogin_StatusFalse()
    {
        $response = $this->json('GET', self::URL_GET_USER_VOUCHER_COUNT);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    private function paginationResponse($data)
    {
        return [
            'data' => $data,
            'meta' => [
                'pagination' => [
                    'count' => 1,
                    'current_page' => 1,
                    'per_page' => 5
                ]
            ]
        ];
    }

    private function notfoundResponse()
    {
        return [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 404,
                'severity' => 'NOT FOUND',
                'message' => 'The server has not found anything matching the request.',
            ]
        ];
    }
}
