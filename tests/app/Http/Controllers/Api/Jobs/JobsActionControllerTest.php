<?php
namespace App\Http\Controllers\Api\Jobs;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Tracker\InputAdsTracker;
use App\Repositories\Jobs\JobsRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class JobsActionControllerTest extends MamikosTestCase
{
    use WithoutMiddleware;

    private const CV_URL_ADD = 'garuda/stories/jobs/cv/add';
    private const CV_URL_FILE = 'garuda/stories/jobs/cv/file';

    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->mockPartialAlternatively(JobsRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testAddCv_UserNotLogin()
    {
        app()->user = null;
        
        $fakeRequest = $this->createFakeRequest();
        $controller = app()->make(JobsActionController::class);
        $response = $controller->addCv($fakeRequest);

        $this->assertInstanceOf(JsonResponse::class, $response);

        $testResponse = new TestResponse($response);
        $testResponse->isOk();

        $this->assertSame($testResponse->getData()->messages, $controller::HARUS_LOGIN);
        $testResponse->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'messages'
        ]);
    }

    public function testAddCv_LowonganTidakDitemukan()
    {
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        app()->user = $user;

        $vacancy = factory(Vacancy::class)->create([
            'slug' => $slug,
            'user_id' => $user->id,
            'status' => Vacancy::VERIFIED,
            'is_active' => 0
        ]);

        $this->repository->shouldReceive('getVacancyApplyByVacancyId')->andReturn(0);
        $this->repository->shouldReceive('getVacancyById')->andReturn(null);

        $fakeRequest = $this->createFakeRequest(
            '/garuda/stories/jobs/cv/add',
            'POST',
            [
                'user'               => $this->faker->name,
                'phone'              => $this->faker->randomNumber,
                'address'            => $this->faker->address,
                'identifier'         => $vacancy->id,
                'education'          => $this->faker->name,
                'skill'              => $this->faker->sentence,
                'job_experience'     => $this->faker->sentence,
                'last_salary'        => mt_rand(11111, 99999),
                'expectation_salary' => mt_rand(11111, 99999),
                'cv'                 => null
            ]
        );
        $controller = app()->make(JobsActionController::class);
        $response = $controller->addCv($fakeRequest);

        $this->assertSame(reset($response->getData()->messages), $controller::LOWONGAN_TIDAK_DITEMUKAN);
    }

    public function testAddCv_SudahKirimCV()
    {
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        app()->user = $user;

        $vacancy = factory(Vacancy::class)->create([
            'slug' => $slug,
            'user_id' => $user->id,
            'status' => Vacancy::VERIFIED,
            'is_active' => 1
        ]);

        $this->repository->shouldReceive('getVacancyApplyByVacancyId')->andReturn(mt_rand(1, 10));
        $this->repository->shouldReceive('getVacancyById')->andReturn($vacancy);

        $fakeRequest = $this->createFakeRequest(
            '/garuda/stories/jobs/cv/add',
            'POST',
            [
                'user'               => $this->faker->name,
                'phone'              => $this->faker->randomNumber,
                'address'            => $this->faker->address,
                'identifier'         => $vacancy->id,
                'education'          => $this->faker->name,
                'skill'              => $this->faker->sentence,
                'job_experience'     => $this->faker->sentence,
                'last_salary'        => mt_rand(11111, 99999),
                'expectation_salary' => mt_rand(11111, 99999),
                'cv'                 => null
            ]
        );
        $controller = app()->make(JobsActionController::class);
        $response = $controller->addCv($fakeRequest);

        $this->assertSame(reset($response->getData()->messages), $controller::SUDAH_KIRIM_CV);
    }

    public function testAddCv_Success()
    {
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        app()->user = $user;

        $vacancy = factory(Vacancy::class)->create([
            'slug' => $slug,
            'user_id' => $user->id,
            'status' => Vacancy::VERIFIED,
            'is_active' => 1
        ]);

        $this->repository->shouldReceive('getVacancyApplyByVacancyId')->andReturn(0);
        $this->repository->shouldReceive('getVacancyById')->andReturn($vacancy);

        $fakeRequest = $this->createFakeRequest(
            '/garuda/stories/jobs/cv/add',
            'POST',
            [
                'user'               => $this->faker->name,
                'phone'              => $this->faker->randomNumber,
                'address'            => $this->faker->address,
                'identifier'         => $vacancy->id,
                'education'          => $this->faker->name,
                'skill'              => $this->faker->sentence,
                'job_experience'     => $this->faker->sentence,
                'last_salary'        => mt_rand(11111, 99999),
                'expectation_salary' => mt_rand(11111, 99999),
                'cv'                 => null,
                'utm'                => $this->faker->name,
            ]
        );
        $controller = app()->make(JobsActionController::class);
        $this->mockPartialAlternatively(InputAdsTracker::class)->shouldReceive('saveFromCampaignQuery');
        $response = $controller->addCv($fakeRequest);

        $this->assertSame($response->getData()->meta->message, $this->repository::BERHASIL_MENGIRIM_CV);
    }

    public function testAddFile_UserNotLogin()
    {
        app()->user = null;

        $fakeRequest = $this->createFakeRequest();
        $controller = app()->make(JobsActionController::class);
        $response = $controller->addFile($fakeRequest);

        $this->assertInstanceOf(JsonResponse::class, $response);

        $testResponse = new TestResponse($response);
        $testResponse->isOk();

        $this->assertSame($testResponse->getData()->messages, $controller::HARUS_LOGIN);
        $testResponse->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message',
            ],
            'messages'
        ]);
    }

    public function testAddFile_ValidatorFileEmpty()
    {
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        app()->user = $user;

        $vacancy = factory(Vacancy::class)->create([
            'slug' => $slug,
            'user_id' => $user->id,
            'status' => Vacancy::VERIFIED,
            'is_active' => 0
        ]);

        $this->repository->shouldReceive('getVacancyApplyByVacancyId')->andReturn(0);
        $this->repository->shouldReceive('getVacancyById')->andReturn(null);

        $fakeRequest = $this->createFakeRequest(
            '/garuda/stories/jobs/cv/file',
            'POST',
            [
                'user'               => $this->faker->name,
                'phone'              => $this->faker->randomNumber,
                'address'            => $this->faker->address,
                'identifier'         => $vacancy->id,
                'education'          => $this->faker->name,
                'skill'              => $this->faker->sentence,
                'job_experience'     => $this->faker->sentence,
                'last_salary'        => mt_rand(11111, 99999),
                'expectation_salary' => mt_rand(11111, 99999),
                'cv'                 => $_FILES
            ]
        );
        $controller = app()->make(JobsActionController::class);
        $response = $controller->addFile($fakeRequest);

        $this->assertSame(reset($response->getData()->messages), $controller->validationMessages['cv.required']);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}