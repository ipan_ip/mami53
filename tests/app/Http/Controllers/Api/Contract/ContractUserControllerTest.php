<?php
namespace Test\App\Http\Controllers\Api\Booking;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserDraft;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\User\UserDesignerViewHistory;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ContractUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_LIST_CONTRACT_URL     = 'api/v1/user/contract';
    private const GET_DETAIL_CONTRACT_URL   = 'api/v1/user/contract/:contractId';

    public function testGetListContractSuccess(): void
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;
        $params = [];
        $params['limit'] = 10;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_LIST_CONTRACT_URL), $params);
        $response->assertStatus(200);
        $response->assertJson(
            $this->defaultResponse(true, '', [
                'data' => null,
                'current_page' => 1,
                'from' => null,
                'last_page' => 1,
                'next_page_url' => null,
                'per_page' => $params['limit'],
                'prev_page_url' => null,
                'to' => 0,
                'total' => 0,
                'has_more' => false
            ])
        );
    }

    public function testGetDetailContractSuccess(): void
    {
        // prepare variable
        $userEntity = factory(User::class)->create();
        $tenantEntity = factory(MamipayTenant::class)->create([
            'user_id' => $userEntity->id
        ]);
        $roomEntity = factory(Room::class)->create();
        $contractEntity = factory(MamipayContract::class)->create([
            'tenant_id' => $tenantEntity->id
        ]);
        factory(MamipayContractKost::class)->create([
            'designer_id' => $roomEntity->id,
            'contract_id' => $contractEntity->id
        ]);

        app()->user = $userEntity;

        // mapping url
        $url = str_replace(':contractId', $contractEntity->id, self::GET_DETAIL_CONTRACT_URL);

        // run test
        $response = $this->actingAs($userEntity)->json('GET', $this->withAccessToken($url));
        $response->assertJsonStructure([
            'status',
            'meta',
            'data'
        ]);
    }

    private function defaultResponse(bool $status = true, string $message = '', $data = null): array
    {
        if ($status && empty($message))
            $message = 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.';

        $response = [];
        $response['status'] = $status;
        $response['meta'] = [
            'response_code' => 200,
            'code' => 200,
            'severity' => "OK",
            'message' => $message
        ];
        if ($data !== null) $response['data'] = $data;

        return $response;
    }
}