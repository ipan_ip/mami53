<?php
namespace App\Http\Controllers\Api\Sanjunipero;

use App\Entities\Room\Room;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Sanjunipero\RoomRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\Transformers\Room\ListTransformer;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class ListControllerTest extends MamikosTestCase
{
    private const SAVE = 'garuda/sanjunipero/list';
    private const COUNT = 5;

    private $repo, $faker, $controller, $transformer;

    protected function setUp() : void
    {
        parent::setUp();

        $this->transformer = $this->mockPartialAlternatively(ListTransformer::class);
        $this->repo = $this->mockPartialAlternatively(RoomRepositoryEloquent::class);
        $this->controller = app()->make(ListController::class);
        $this->faker = Faker::create();
    }

    public function testIndex_CacheAvailable()
    {
        $this->app->device = null;
        $expected = Api::responseData([
            "status" => true
        ]);

        Cache::shouldReceive('has')->once()->andReturn(true);
        Cache::shouldReceive('get')->once()->andReturn($expected);

        $res = $this->controller->index();
        
        $this->assertInstanceof(JsonResponse::class, $res);
    }

    public function testIndex_CacheNotAvailable()
    {
        $this->app->device = null;
        $rooms = factory(Room::class, self::COUNT)
            ->states('active', 'notTesting', 'with-room-unit')
            ->create([
                'updated_at' => Carbon::now()->toDateTimeString(),
                'sort_score' => 5000
            ]);

        Cache::shouldReceive('has')->andReturn(false);
        Cache::shouldReceive('remember');
        Cache::shouldReceive('put');

        $this->repo->shouldReceive('show->map')->andReturn($rooms);
        $this->repo->shouldReceive('getCount')->andReturn(5);

        $req = $this->setFakeRequest();

        $res = (new ListController($req, $this->repo))->index();

        $this->assertSame($rooms->pluck('id')->first(), $res->getData()->rooms[0]->id);
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    private function setFakeRequest()
    {
        return $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                "filters" => [
                    "gender" => [],
                    "price_range" => [],
                    "tags_ids" => [],
                    "level_info" => "",
                    "rent_type" => 2,
                    "property_type" => "all",
                    "random_seeds" => 230,
                    "booking" => 0,
                    "flash_sale" => false,
                    "mamirooms" => false,
                    "include_promoted" => false,
                    "mamichecker" => false,
                    "virtual_tour" => false
                ],
                "sorting" => [
                    "field" => "price",
                    "direction" => "-"
                ],
                "location" => [],
                "point" => [],
                "include_promoted" => false,
                "limit" => self::COUNT,
                "offset" => 0,
                "slug" => "kos/oyo/jakarta-utara"
            ]
        );
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}