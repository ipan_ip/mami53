<?php

namespace App\Http\Controllers\Api\RoomAllotment;

use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Exceptions\RoomAllotmentException;
use App\Presenters\Room\RoomUnitPresenter;
use App\Services\RoomUnit\RoomUnitService;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\Report;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RoomAllotmentControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        app()->user = $this->user;
        $this->room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 1]);
        factory(RoomOwner::class)->create(['designer_id' => $this->room->id, 'user_id' => $this->user->id]);
    }

    public function testAddRoomUnitShouldFailValidation()
    {
        $params = [
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->addRoomUnit(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $serviceMock,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(400, $resultData->meta->code);
    }

    public function testAddRoomUnitShouldReturnValidResponse()
    {
        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'kost test',
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);

        $result = $controller->addRoomUnit(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            app()->make(RoomUnitService::class),
            app()->make(RoomUnitPresenter::class)
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());
    }

    public function testAddRoomUnitShouldBetrimmed()
    {
        $params = [
            'song_id' => $this->room->song_id,
            'name' => '  kost test  ',
            'floor' => '  1 ',
            'occupied' => false,
        ];

        $trimmedParams = [
            'song_id' => $params['song_id'],
            'name' => trim($params['name']),
            'floor' => trim($params['floor']),
            'occupied' => $params['occupied'],
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $roomUnitMock = $this->mock(RoomUnit::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('addRoomUnit')->with($trimmedParams)->once()->andReturn($roomUnitMock);

        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);
        $presenterMock->shouldReceive('present')->once()->with($roomUnitMock)->andReturn(['test success']);

        $result = $controller->addRoomUnit(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $serviceMock,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertEquals('test success', $resultData->unit[0]);
    }

    public function testAddRoomUnitThrowException()
    {
        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'kost test',
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('addRoomUnit')->with($params)->once()->andThrow(new ModelNotFoundException("test exception"));

        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->addRoomUnit(
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $serviceMock,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(400, $resultData->meta->code);
        $this->assertSame('test exception', $resultData->meta->message);
    }

    public function testEditRoomUnitShouldFailValidation()
    {
        $params = [
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->editRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            888,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(400, $resultData->meta->code);
    }

    public function testEditRoomUnitRoomUnauthorized()
    {
        $params = [
            'song_id' => 888,
            'name' => 'Room Name',
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->editRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            888,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(401, $resultData->meta->code);
        $this->assertSame("Unauthorized", $resultData->meta->message);
    }

    public function testEditRoomUnitRoomUnitUnauthorized()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 1]);
        $params = [
            'song_id' => $room->song_id,
            'name' => 'Room Name',
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->editRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            888,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(401, $resultData->meta->code);
        $this->assertSame("Unauthorized", $resultData->meta->message);
    }

    public function testEditRoomUnitRoomUnitChangeNameShouldReturnSuccess()
    {
        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id]);
        $availableUnit = $roomUnits->first();
        $availableUnit->occupied = false;
        $availableUnit->save();

        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'Room Name',
            'floor' => 2,
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateSingleUnit')->andReturn($availableUnit);

        $result = $controller->editRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $availableUnit->id,
            app()->make(RoomUnitPresenter::class)
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();

        $this->assertSame(200, $resultData->meta->code);
        $this->assertSame('Anda berhasil update data kamar', $resultData->meta->message);
    }

    public function testEditRoomUnitRoomUnitChangeAvailabilityShouldReturnSuccess()
    {
        // $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 1]);
        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id]);
        $availableUnit = $roomUnits->first();
        $availableUnit->occupied = false;
        $availableUnit->save();

        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'Room Name',
            'floor' => 2,
            'occupied' => true,
        ];

        $this->assertEquals(2, $this->room->room_count);
        $this->assertEquals(1, $this->room->room_available);

        $controller = app()->make(RoomAllotmentController::class);

        $result = $controller->editRoomUnit(
            app()->make(RoomUnitService::class),
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $availableUnit->id,
            app()->make(RoomUnitPresenter::class)
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $this->room->refresh();
        $this->assertEquals(2, $this->room->room_count);
        $this->assertEquals(0, $this->room->room_available);

        $resultData = $result->getData();
        $this->assertSame(200, $resultData->meta->code);
        $this->assertSame('Kamar Terisi Bertambah 1', $resultData->meta->message);
    }

    public function testEditRoomUnitRoomUnitChangeOccupiedShouldReturnSuccess()
    {
        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id]);
        $availableUnit = $roomUnits->first();
        $availableUnit->occupied = true;
        $availableUnit->save();

        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'Room Name',
            'floor' => 2,
            'occupied' => false,
        ];

        $this->assertEquals(2, $this->room->room_count);
        $this->assertEquals(1, $this->room->room_available);

        $controller = app()->make(RoomAllotmentController::class);

        $result = $controller->editRoomUnit(
            app()->make(RoomUnitService::class),
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $availableUnit->id,
            app()->make(RoomUnitPresenter::class)
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $this->room->refresh();
        $this->assertEquals(2, $this->room->room_count);
        $this->assertEquals(2, $this->room->room_available);

        $resultData = $result->getData();
        $this->assertSame(200, $resultData->meta->code);
        $this->assertSame('Kamar Kosong Bertambah 1', $resultData->meta->message);
    }

    public function testEditRoomUnitRoomUnitThrowException()
    {
        $roomUnit = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id, 'occupied' => false])->first();
        $params = [
            'song_id' => $this->room->song_id,
            'name' => 'Room Name',
            'floor' => '1',
            'occupied' => false,
        ];

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateSingleUnit')->andThrow(new ModelNotFoundException("unit not found"));
        $presenterMock = $this->mockAlternatively(RoomUnitPresenter::class);

        $result = $controller->editRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    $params
                )
            ),
            $roomUnit->id,
            $presenterMock
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(404, $resultData->meta->code);
        $this->assertSame("unit not found", $resultData->meta->message);
    }

    public function testDeleteRoomUnitThrowExceptionModelNotFound()
    {
        $roomUnit = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id, 'occupied' => false])->first();

        $controller = app()->make(RoomAllotmentController::class);
        $serviceMock = $this->mockAlternatively(RoomUnitService::class);

        $serviceMock->shouldReceive('deleteRoomUnit')->andThrow(new ModelNotFoundException("unit not found"));

        $result = $controller->deleteRoomUnit(
            $serviceMock,
            $roomUnit->id
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();
        $this->assertSame(404, $resultData->meta->code);
        $this->assertSame("unit not found", $resultData->meta->message);
    }

    public function testDeleteRoomUnitReturnSuccess()
    {
        $roomUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $this->room->id]);
        $availableUnit = $roomUnits->first();
        $availableUnit->occupied = true;
        $availableUnit->save();

        $this->assertEquals(2, $this->room->room_count);

        $controller = app()->make(RoomAllotmentController::class);
        $result = $controller->deleteRoomUnit(
            app()->make(RoomUnitService::class),
            $availableUnit->id
        );

        $this->room->refresh();
        $this->assertEquals(1, $this->room->room_count);
        $this->assertEquals(1, $this->room->room_available);
        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());

        $resultData = $result->getData();

        $this->assertSame(200, $resultData->meta->code);
        $this->assertSame("Room Unit {$availableUnit->id} berhasil dihapus", $resultData->messages);
    }

    public function testUpdateBulkRoomUnitShouldReturnValidResponse()
    {
        $controller = app()->make(RoomAllotmentController::class);

        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateBulk')->andReturn(['success' => [], 'failed' => []]);
        $result = $controller->updateBulkRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'song_id' => $this->room->song_id,
                        'add' => [],
                        'update' => [],
                        'delete' => [],
                    ]
                )
            )
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());
        $resultData = $result->getData();
        $this->assertTrue($resultData->status);
    }

    public function testUpdateBulkRoomUnitThrowNotFoundException()
    {
        $controller = app()->make(RoomAllotmentController::class);

        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateBulk')->andThrow(new ModelNotFoundException("model not found"));

        $result = $controller->updateBulkRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'song_id' => $this->room->song_id,
                        'add' => [],
                        'update' => [],
                        'delete' => [],
                    ]
                )
            )
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());
        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertSame(404, $resultData->meta->code);
        $this->assertSame("model not found", $resultData->meta->message);
    }

    public function testUpdateBulkRoomUnitThrowRoomAllotmentException()
    {
        $controller = app()->make(RoomAllotmentController::class);

        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateBulk')->andThrow(new RoomAllotmentException(400, "room allotment exception"));

        $result = $controller->updateBulkRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'song_id' => $this->room->song_id,
                        'add' => [],
                        'update' => [],
                        'delete' => [],
                    ]
                )
            )
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());
        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
        $this->assertSame(400, $resultData->meta->code);
        $this->assertSame("room allotment exception", $resultData->meta->message);
    }

    public function testUpdateBulkRoomUnitShouldFailValidation()
    {
        $controller = app()->make(RoomAllotmentController::class);

        $serviceMock = $this->mockPartialAlternatively(RoomUnitService::class);
        $serviceMock->shouldReceive('updateBulk')->andReturn(['success' => [], 'failed' => []]);
        $result = $controller->updateBulkRoomUnit(
            $serviceMock,
            Request::createFromBase(
                SymfonyRequest::create(
                    '/',
                    'POST',
                    [
                        'song_id' => null,
                        'add' => [],
                        'update' => [],
                        'delete' => [],
                    ]
                )
            )
        );

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals(200, $result->getStatusCode());
        $resultData = $result->getData();
        $this->assertFalse($resultData->status);
    }

    public function testGetBugsnagCallbackReportClosure()
    {
        $params = [];
        $controller = new RoomAllotmentController();
        $controllerReflection = new \ReflectionClass(RoomAllotmentController::class);
        $method = $controllerReflection->getMethod('getBugsnagCallbackReport');
        $method->setAccessible(true);
        $metaReporter = $method->invokeArgs(
            $controller,
            $params
        );

        $this->assertIsCallable($metaReporter);
        $reportMock = $this->createMock(Report::class);
        $reportMock->method('setSeverity')->with('info')->willReturn($this->anything());
        $reportMock->method('setMetaData')->with(['data' => $params])->willReturn($this->anything());
        $metaReporter($reportMock);
    }
}
