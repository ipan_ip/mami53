<?php
namespace App\Http\Controllers\Api;

use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\BaselineType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use phpmock\MockBuilder;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use RuntimeException;
use App\Entities\Media\Media;
use App\Entities\Level\KostLevel;
use Illuminate\Foundation\Testing\WithFaker;

class GoldplusStatisticControllerTest extends MamiKosTestCase
{

    use WithoutMiddleware;
    use WithFaker;

    private const GET_CHAT_LIST_FILTER_GOLDPLUS_API = 'api/v1/room/goldplus/statistics/filters/'.GoldplusStatisticType::CHAT;
    private const GET_VISIT_LIST_FILTER_GOLDPLUS_API = 'api/v1/room/goldplus/statistics/filters/'.GoldplusStatisticType::VISIT;
    private const GET_UNIQUE_VISIT_LIST_FILTER_GOLDPLUS_API = 'api/v1/room/goldplus/statistics/filters/'.GoldplusStatisticType::UNIQUE_VISIT;
    private const GET_FAVORITE_LIST_FILTER_GOLDPLUS_API = 'api/v1/room/goldplus/statistics/filters/'.GoldplusStatisticType::FAVORITE;
    private const GET_DETAIL_KOST_GOLDPLUS_API = 'api/v1/room/goldplus/';
    private const GET_GP_STATISTIC_GOLDPLUS_API = 'garuda/room/goldplus/';

    protected function setUp() : void
    {
        parent::setUp();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
        $this->app->user = null;
        $this->app->device = null;
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetChatFilterListsExpectRuntimeException()
    {
        $response = $this->json('GET', self::GET_CHAT_LIST_FILTER_GOLDPLUS_API)->json();
        $this->assertEquals('RuntimeException', $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.runtime_exception.user_id_was_null'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetChatFilterListShouldReturnOk()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payload(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT
                )
        );

        $expectedResult = [
            GoldplusStatisticReportType::YESTERDAY => [
                'key'   => GoldplusStatisticReportType::YESTERDAY,
                'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
            ],
            GoldplusStatisticReportType::LAST_SEVEN_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
                'value' => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
            ],
            GoldplusStatisticReportType::LAST_THIRTY_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
                'value' => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
            ]
        ];

        $param = ['id' => $room->song_id];
        $response = $this->json('GET', self::GET_CHAT_LIST_FILTER_GOLDPLUS_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => $expectedResult
        ]);
    }

    /**
     * @group UG
     * @group UG-3969
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetVisitFilterListExpectRuntimeException()
    {
        $response = $this->json('GET', self::GET_VISIT_LIST_FILTER_GOLDPLUS_API)->json();
        $this->assertEquals('RuntimeException', $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.runtime_exception.user_id_was_null'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetVisitFilterListShouldReturnOk()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payload(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::VISIT
                )
        );

        $expectedResult = [
            GoldplusStatisticReportType::YESTERDAY => [
                'key'   => GoldplusStatisticReportType::YESTERDAY,
                'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
            ],
            GoldplusStatisticReportType::LAST_SEVEN_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
                'value' => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
            ],
            GoldplusStatisticReportType::LAST_THIRTY_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
                'value' => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
            ]
        ];

        $param = ['id' => $room->song_id];
        $response = $this->json('GET', self::GET_VISIT_LIST_FILTER_GOLDPLUS_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => $expectedResult
        ]);
    }

    /**
     * @group UG
     * @group UG-3969
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetUniqueVisitFilterListExpectRuntimeException()
    {
        $response = $this->json('GET', self::GET_UNIQUE_VISIT_LIST_FILTER_GOLDPLUS_API)->json();
        $this->assertEquals('RuntimeException', $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.runtime_exception.user_id_was_null'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetUniqueVisitFilterListShouldReturnOk()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payload(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::UNIQUE_VISIT
                )
        );

        $expectedResult = [
            GoldplusStatisticReportType::YESTERDAY => [
                'key'   => GoldplusStatisticReportType::YESTERDAY,
                'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
            ],
            GoldplusStatisticReportType::LAST_SEVEN_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
                'value' => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
            ],
            GoldplusStatisticReportType::LAST_THIRTY_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
                'value' => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
            ]
        ];

        $param = ['id' => $room->song_id];
        $response = $this->json('GET', self::GET_UNIQUE_VISIT_LIST_FILTER_GOLDPLUS_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => $expectedResult
        ]);
    }

    /**
     * @group UG
     * @group UG-3969
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetFavoriteFilterListExpectRuntimeException()
    {
        $response = $this->json('GET', self::GET_FAVORITE_LIST_FILTER_GOLDPLUS_API)->json();
        $this->assertEquals('RuntimeException', $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.runtime_exception.user_id_was_null'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetFavoriteFilterListShouldReturnOk()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payload(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE
                )
        );

        $expectedResult = [
            GoldplusStatisticReportType::YESTERDAY => [
                'key'   => GoldplusStatisticReportType::YESTERDAY,
                'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
            ],
            GoldplusStatisticReportType::LAST_SEVEN_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
                'value' => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
            ],
            GoldplusStatisticReportType::LAST_THIRTY_DAYS => [
                'key'   => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
                'value' => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
            ]
        ];

        $param = ['id' => $room->song_id];
        $response = $this->json('GET', self::GET_FAVORITE_LIST_FILTER_GOLDPLUS_API, $param);
        $response->assertOk();
        $response->assertJson([
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => $expectedResult
        ]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetDetailKostGpShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $kostLevel = factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);
        
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->changeLevel($kostLevel->id);
        $room->save();

        $response = $this->json('GET', self::GET_DETAIL_KOST_GOLDPLUS_API.''.$room->song_id);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data' => [
                'id'            => $room->song_id,
                'room_title'    => $room->name,
                'area_formatted' => $room->area_formatted,
                'address'       => $room->address,
            ]
        ]);
    }

     /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetDetailKostGpExpectRuntimeException()
    {
        $response = $this->json('GET', 
            self::GET_DETAIL_KOST_GOLDPLUS_API.''.mt_rand(1, 10))->json();
        $this->assertEquals(
            __('goldplus-statistic.error.runtime_exception.user_id_was_null'), 
            $response['message']
        );
        $this->assertEquals(RuntimeException::class, $response['exception']);
    }
    
    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpVisitingStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::VISIT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::VISIT, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'filter_list'   => [
                    GoldplusStatisticReportType::YESTERDAY => [
                        'key'   => GoldplusStatisticReportType::YESTERDAY,
                        'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    ],
                ],
                'label' => [
                    'name'  => GoldplusStatisticType::VISIT_STATISTIC_TITLE_LABEL
                ],
                'statistic' => [
                    GoldplusStatisticType::VISIT => [
                        'label' => GoldplusStatisticType::VISIT_LABEL,
                        'value' => $growthValue,
                    ],
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpVisitingStatisticWithInvalidParameter()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::VISIT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(1, 10000)];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::VISIT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(__('goldplus-statistic.error.filter.report_type_not_valid'), $response['message']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpVisitingStatisticWithNoUserData()
    {
        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    mt_rand(1, 9999), 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::VISIT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::VISIT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.runtime_exception.user_id_was_null'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpUniqueVisitingStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::UNIQUE_VISIT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::UNIQUE_VISIT, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'filter_list'   => [
                    GoldplusStatisticReportType::YESTERDAY => [
                        'key'   => GoldplusStatisticReportType::YESTERDAY,
                        'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    ],
                ],
                'label' => [
                    'name'  => GoldplusStatisticType::UNIQUE_VISIT_STATISTIC_TITLE_LABEL
                ],
                'statistic' => [
                    GoldplusStatisticType::UNIQUE_VISIT => [
                        'label' => GoldplusStatisticType::UNIQUE_VISIT_LABEL,
                        'value' => $growthValue,
                    ],
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpUniqueVisitingStatisticWithInvalidParameter()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::UNIQUE_VISIT,
                    $growthValue
                )
            );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.str_random()];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::UNIQUE_VISIT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.filter.report_type_not_valid'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpUniqueVisitingStatisticExpectRuntimeException()
    {
        $ownerId        = mt_rand(1, 9999);
        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $ownerId, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::UNIQUE_VISIT,
                    $growthValue
                )
            );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.str_random()];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::UNIQUE_VISIT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.runtime_exception.user_id_was_null'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpChatStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::CHAT, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'filter_list'   => [
                    GoldplusStatisticReportType::YESTERDAY => [
                        'key'   => GoldplusStatisticReportType::YESTERDAY,
                        'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    ],
                ],
                'label' => [
                    'name'  => GoldplusStatisticType::CHAT_STATISTIC_TITLE_LABEL
                ],
                'statistic' => [
                    GoldplusStatisticType::CHAT => [
                        'label' => GoldplusStatisticType::CHAT_LABEL,
                        'value' => $growthValue,
                    ],
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpChatStatisticWithInvalidParameter()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(11111, 999999)];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::CHAT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.filter.report_type_not_valid'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpChatStatisticExpectRuntimeException()
    {
        $ownerId = mt_rand(11111, 999999);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $ownerId, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(11111, 999999)];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::CHAT, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.runtime_exception.user_id_was_null'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpFavoriteStatisticShouldReturnSuccess()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::FAVORITE, $param);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data'  => [
                'id'            => $songId,
                'room_title'    => $name,
                'filter_list'   => [
                    GoldplusStatisticReportType::YESTERDAY => [
                        'key'   => GoldplusStatisticReportType::YESTERDAY,
                        'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    ],
                ],
                'label' => [
                    'name'  => GoldplusStatisticType::FAVORITE_STATISTIC_TITLE_LABEL
                ],
                'statistic' => [
                    GoldplusStatisticType::FAVORITE => [
                        'label' => GoldplusStatisticType::FAVORITE_LABEL,
                        'value' => $growthValue,
                    ],
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpFavoriteStatisticWithInvalidParameter()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(1, 10)];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::FAVORITE, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.filter.report_type_not_valid'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpFavoriteStatisticExpectRuntimeException()
    {
        $ownerId = mt_rand(111, 99999);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $ownerId, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(1, 10)];
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songId.'/statistics/'.GoldplusStatisticType::FAVORITE, $param)->json();
        $this->assertEquals(RuntimeException::class, $response['exception']);
        $this->assertEquals(
            __('goldplus-statistic.error.runtime_exception.user_id_was_null'),
            $response['message']
        );
    }

    /**
     * @group UG
     * @group UG-4225
     * @group App/Http/Controllers/Api/GoldplusStatisticController
     */
    public function testGetGpFavoriteStatisticExpectRuntimeExceptionInvalidSongId()
    {
        $ownerId = mt_rand(111, 99999);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode(',', [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ]);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $ownerId, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        );

        $param = ['filter_list' => GoldplusStatisticReportType::YESTERDAY.mt_rand(1, 10)];
        $songIdParam = $this->faker->randomLetter . str_random(16);
        $response = $this->json('GET', 
            self::GET_GP_STATISTIC_GOLDPLUS_API.''.$songIdParam.'/statistics/'.GoldplusStatisticType::FAVORITE, $param)->json();
        $this->assertEquals(\Symfony\Component\Debug\Exception\FatalThrowableError::class, $response['exception']);
    }

    /**
     * Payload for test
     * 
     * @return array
     */
    private function payloadForStatisticType(
        int $userId, 
        Room $room, 
        string $availableReportType,
        string $gpCreatedAt,
        string $filterType,
        int $growthValue
        ): array
    {
        $totalLastTwoMonthsVisit = mt_rand(1, 1000);
        $growthType     = GrowthType::UP;
        $baselineType   = BaselineType::GROWTH;

        return [
            'room'  => [
                'id'        => $room->id,
                'song_id'   => $room->song_id,
                'name'      => $room->name,
                'gender'    => $room->gender,
                'address'   => $room->address,
                'area'      => $room->area,
                'is_active' => $room->is_active,
            ],
            'owner' => [
                'user_id'   => $userId,
            ],
            'statistic' => [
                'key'       => $filterType,
                'total'     => $totalLastTwoMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => mt_rand(1, 100),
                    'currdate_to_gp_diff'   => mt_rand(1, 100),
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => mt_rand(1, 100),
                'start_baseline_date_range_value'   => date('Y-m-d'),
                'available_report_type'             => $availableReportType,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::YESTERDAY,
            ],
            'gp'    => [
                'level_id'      => 11,
                'gp_created_at' => $gpCreatedAt,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Agt',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Sep',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Okt',
                        'y' => mt_rand(1, 10),
                    ],
                ]   
            ]
        ];
    }

    /**
     * Payload for test
     * 
     * @return array
     */
    private function payload(
        int $userId, 
        Room $room, 
        string $availableReportType,
        string $gpCreatedAt,
        string $filterType): array
    {
        $totalLastTwoMonthsVisit = mt_rand(1, 1000);
        $growthType     = GrowthType::UP;
        $growthValue    = mt_rand(1, 10);
        $baselineType   = BaselineType::GROWTH;

        return [
            'room'  => [
                'id'        => $room->id,
                'song_id'   => $room->song_id,
                'name'      => $room->name,
                'gender'    => $room->gender,
                'address'   => $room->address,
                'area'      => $room->area,
                'is_active' => $room->is_active,
            ],
            'owner' => [
                'user_id'   => $userId,
            ],
            'statistic' => [
                'key'       => $filterType,
                'total'     => $totalLastTwoMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => mt_rand(1, 100),
                    'currdate_to_gp_diff'   => mt_rand(1, 100),
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => mt_rand(1, 100),
                'start_baseline_date_range_value'   => date('Y-m-d'),
                'available_report_type'             => $availableReportType,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            ],
            'gp'    => [
                'level_id'      => 11,
                'gp_created_at' => $gpCreatedAt,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Agt',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Sep',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Okt',
                        'y' => mt_rand(1, 10),
                    ],
                ]   
            ]
        ];
    }
}