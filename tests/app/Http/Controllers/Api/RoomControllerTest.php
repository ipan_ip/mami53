<?php

namespace App\Http\Controllers\Api;

use App\Entities\Activity\Call;
use App\Entities\Booking\BookingAcceptanceRate;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\Card;
use App\Entities\Media\Media;
use App\Entities\Room\BookingOwnerRequest;
use App\Repositories\RoomRepositoryEloquent;
use App\Services\Sendbird\GroupChannelService;
use App\Services\Sendbird\GroupChannel\Channel;
use App\Test\MamiKosTestCase;
use App\User;
use App\Validators\RoomValidator;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevelMap;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Jobs\KostIndexer\IndexKostJob;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use Illuminate\Support\Facades\Bus;
use Mockery;
use Config;
use Mockery\MockInterface;

class RoomControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_DETAIL_KOS = 'api/v1/stories/%u';
    private const GET_PHOTO_BY_ROOM_ID = 'api/v1/stories/%u/photos';
    private const POST_CALL_BY_ROOM_ID = 'api/v1/stories/%u/call';
    private const GET_COMPARE_ROOM = 'api/v1/stories/compare/%u';
    private const GET_NEARBY_BY_ROOM_ID = 'api/v1/stories/nearby/%u';
    private const ROOM_BY_SLUG = 'api/v1/room/%u';

    public function testGetPhotoByRoomIdControllerNotFound()
    {
        $url = sprintf(self::GET_PHOTO_BY_ROOM_ID, 12);
        $response = $this->json('GET', $url);

        $response->assertStatus(200);
        $responseJson = json_decode($response->getContent());

        $this->assertEquals(false, $responseJson->status);
        $this->assertEquals('Data tidak ditemukan', $responseJson->meta->message);
    }

    public function testGetPhotoByRoomIdControllerWithRegularCard()
    {
        $room   = factory(Room::class)->create(['is_active' => true]);
        $photo  = factory(Media::class)->create();
        $card   = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => 'image',
            'photo_id'    => $photo->id,
        ]);

        $url = sprintf(self::GET_PHOTO_BY_ROOM_ID, $room->song_id);
        $response = $this->json('GET', $url);

        $response->assertStatus(200);
        $responseJson = json_decode($response->getContent());

        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name, $responseJson->data[0]->photo_url->real);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-240x320.jpg', $responseJson->data[0]->photo_url->small);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-360x480.jpg', $responseJson->data[0]->photo_url->medium);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-540x720.jpg', $responseJson->data[0]->photo_url->large);
    }

    public function testGetPhotoByRoomIdControllerWithPremiumCard()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id'  => $photo->id,
            'is_active' => true
        ]);
        $cardPremium   = factory(CardPremium::class)->create([
            'designer_id' => $room->id,
            'type'        => 'image',
            'photo_id'    => $photo->id,
        ]);

        $url = sprintf(self::GET_PHOTO_BY_ROOM_ID, $room->song_id);
        $response = $this->json('GET', $url);

        $response->assertStatus(200);
        $responseJson = json_decode($response->getContent());

        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name, $responseJson->data[0]->photo_url->real);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-240x320.jpg', $responseJson->data[0]->photo_url->small);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-360x480.jpg', $responseJson->data[0]->photo_url->medium);
        $this->assertEquals(Config::get('api.media.cdn_url').'public/storage/images/'.$photo->file_name.'-540x720.jpg', $responseJson->data[0]->photo_url->large);
    }

    public function testPostCall_NoUser_ReturnFalse()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $room = factory(Room::class)->create([
            'is_active' => true
        ]);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    public function testPostCall_SuspiciousUser_ReturnFalse()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $user = factory(User::class)->create([
            'id' => 325904
        ]);
        $room = factory(Room::class)->create([
            'is_active' => true
        ]);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->actingAs($user)->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPostCall_RoomLimitExceeded_ReturnFalse()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true
        ]);

        $call = Mockery::mock('overload:App\Entities\Activity\Call');
        $call->shouldReceive('getIsDailyQuestionRoomLimitExceeded')->andReturn(true);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->actingAs($user)->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testPostCall_DailyLimitExceeded_ReturnFalse()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $call = Mockery::mock('overload:App\Entities\Activity\Call');
        $call->shouldReceive('getIsDailyQuestionRoomLimitExceeded')->andReturn(false);
        $call->shouldReceive('getIsDailyQuestionLimitExceeded')->andReturn(true);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->actingAs($user)->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * * @group UG
     * @group UG-4179
     */
    public function testPostCall_ExistingCall_ReturnFalse()
    {

        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $user = factory(User::class)->create();
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true,
            'photo_id' => $media->id
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $call = Mockery::mock('overload:App\Entities\Activity\Call');
        $call->shouldReceive('getIsDailyQuestionRoomLimitExceeded')->andReturn(false);
        $call->shouldReceive('getIsDailyQuestionLimitExceeded')->andReturn(false);

        $repo->expects($this->once())->method('addCall')->willReturn(false);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->actingAs($user)->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @group UG
     * @group UG-4179
     */
    public function testPostCall_ReturnTrue()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);

        $user = factory(User::class)->create();
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true,
            'photo_id' => $media->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $call = Mockery::mock('overload:App\Entities\Activity\Call');
        $call->shouldReceive('getIsDailyQuestionRoomLimitExceeded')->andReturn(false);
        $call->shouldReceive('getIsDailyQuestionLimitExceeded')->andReturn(false);
        $call->chat_admin_id = 123;
        $call->chat_group_id = 'some_url';
        $call->id = 1;

        $repo->expects($this->once())->method('addCall')->willReturn($call);

        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $room->song_id);
        $response = $this->actingAs($user)->json('POST', $url);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    private function createRoomWithLevelMap(User $user, ?int $levelId): Room
    {
        $room = factory(Room::class)->create([
            'user_id'   => $user->id
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id
        ]);

        if (!is_null($levelId)) {
            if (KostLevel::canMakeRegularLevel()) {
                factory(KostLevel::class)->state('regular')->create();
            }
            if (is_null(KostLevel::find($levelId))) {
                factory(KostLevel::class)->create([
                    'id' => $levelId,
                ]);
            }
            $room->level()->attach($levelId);
        }

        return $room;
    }

    /**
     * @return RoomRepositoryEloquent|MockObject
     */
    private function createMockRoomRepository()
    {
        $repo = $this->createMock(RoomRepositoryEloquent::class);
        $validator = $this->createMock(RoomValidator::class);
        $this->app->instance(RoomRepositoryEloquent::class, $repo);
        $this->app->instance(RoomValidator::class, $validator);
        return $repo;
    }

    /**
     * @return Call|MockInterface
     */
    public function mockCall()
    {
        $call = Mockery::mock('overload:App\Entities\Activity\Call');
        $call->shouldReceive('getIsDailyQuestionRoomLimitExceeded')->andReturn(false);
        $call->shouldReceive('getIsDailyQuestionLimitExceeded')->andReturn(false);
        $call->chat_admin_id = 123;
        $call->chat_group_id = 'some_url';
        $call->id = 1;
        return $call;
    }

    public function postCallBySongId(User $user, int $songId)
    {
        $url = sprintf(self::POST_CALL_BY_ROOM_ID, $songId);
        return $this->actingAs($user)->json('POST', $url);
    }

    /**
     * @group UG
     * @group UG-2559
     * @group UG-4179
     * @group UG-4487
     */
    public function testPostCallGp3()
    {
        $repo = $this->createMockRoomRepository();

        $user = factory(User::class)->create();

        $room = $this->createRoomWithLevelMap($user, config('kostlevel.id.goldplus3'));

        $call = $this->mockCall();

        $repo->expects($this->once())->method('addCall')->willReturn($call);

        $response = $this->postCallBySongId($user, $room->song_id);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    /**
     * @group UG
     * @group UG-4487
     */
    public function testPostCallNoLevelMap()
    {
        $repo = $this->createMockRoomRepository();

        $user = factory(User::class)->create();

        $room = $this->createRoomWithLevelMap($user, null);

        $call = $this->mockCall();

        $repo->expects($this->once())->method('addCall')->willReturn($call);
        $response = $this->postCallBySongId($user, $room->song_id);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    /**
     * @group UG
     * @group UG-4487
     */
    public function testPostCallChannelDataHaveMismatchCustomTypeNonGp()
    {
        $repo = $this->createMockRoomRepository();

        $user = factory(User::class)->create();

        $room = $this->createRoomWithLevelMap($user, null);
        // Non GP will have empty custom type.
        $expectedCustomType = '';

        $call = $this->mockCall();

        $repo->expects($this->once())->method('addCall')->willReturn($call);
        $response = $this->postCallBySongId($user, $room->song_id);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    /**
     * @group UG
     * @group UG-4487
     */
    public function testPostCallChannelDataHaveMismatchCustomTypeGp()
    {
        $repo = $this->createMockRoomRepository();

        $user = factory(User::class)->create();

        $expectedCustomType = 'gp1';
        $room = $this->createRoomWithLevelMap(
            $user,
            collect(KostLevel::getGoldplusLevelIdsByGroups(1))->random()
        );

        $call = $this->mockCall();

        $repo->expects($this->once())->method('addCall')->willReturn($call);
        $response = $this->postCallBySongId($user, $room->song_id);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    /**
     * @group UG
     * @group UG-4487
     */
    public function testPostCallChannelDataHaveSameCustomType()
    {
        $repo = $this->createMockRoomRepository();

        $user = factory(User::class)->create();

        $expectedCustomType = 'gp1';
        $room = $this->createRoomWithLevelMap(
            $user,
            collect(KostLevel::getGoldplusLevelIdsByGroups(1))->random()
        );

        $call = $this->mockCall();

        $repo->expects($this->once())->method('addCall')->willReturn($call);
        $response = $this->postCallBySongId($user, $room->song_id);

        $response->assertOk();
        $response->assertJsonFragment(['status' => true, 'result' => true]);
    }

    public function testCompareNearbyRoom()
    {
        $room   = factory(Room::class)->create(['is_active' => true]);

        $url = sprintf(self::GET_COMPARE_ROOM, $room->song_id);
        $response = $this->json('GET', $url);

        $response->assertStatus(200);
    }

    public function testGetNearbyByRoomIdIsSuccess()
    {
        $room   = factory(Room::class)->create(['is_active' => true]);

        $url = sprintf(self::GET_NEARBY_BY_ROOM_ID, $room->song_id);
        $response = $this->json('GET', $url);

        $response->assertStatus(200);
    }

    /**
     * @group UG
     * @group KOS-16007
     */
    public function testGetDetailKosAccessFromMobileBisaBookingKos()
    {
        // setup data
        $room = factory(Room::class)->create([
            'is_active' => true,
            'is_booking' => true,
        ]);
        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);
        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => '2018-06-01 19:00:00',
        ]);

        $url = sprintf(self::GET_DETAIL_KOS, $room->song_id);

        // hit api detail kos
        $response = $this->json('GET', $url);

        // assertion
        $response->assertStatus(200);
        $responseJson = json_decode($response->getContent());
        $this->assertEquals(true, $responseJson->status);

        // assert is_booking
        $this->assertEquals(true, $responseJson->story->is_booking); 

        // assert booking_acceptance_rate
        $barResult = $responseJson->story->booking;
        $this->assertEquals($ownerRequest->created_at->toDateTimeString(), $barResult->active_from);
        $this->assertEquals(80, $barResult->accepted_rate);
        $this->assertEquals($bar->average_time/60, $barResult->average_time);
    }

     /**
     * @group UG
     * @group KOS-16007
     */
    public function testGetDetailKosAccessFromMobileRegularKos()
    {
        // setup data
        $room = factory(Room::class)->create([
            'is_active' => true,
            'is_booking' => false,
        ]);

        $url = sprintf(self::GET_DETAIL_KOS, $room->song_id);

        // hit api detail kos
        $response = $this->json('GET', $url);

        // assertion
        $response->assertStatus(200);
        $responseJson = json_decode($response->getContent());
        $this->assertEquals(true, $responseJson->status);

        // assert is_booking
        $this->assertEquals(false, $responseJson->story->is_booking); 

        // assert booking_acceptance_rate
        $barResult = $responseJson->story->booking;
        $this->assertEquals(null, $barResult->active_from);
        $this->assertEquals(null, $barResult->accepted_rate);
        $this->assertEquals(null, $barResult->average_time);
    }

    public function testIndexKostSearch()
    {
        Bus::fake();
        $roomId = 10;
        $response = $this->json('POST', 'hook/v1/stories/' . $roomId . '/index-kost-search');
        Bus::assertDispatched(IndexKostJob::class);
        $response->assertStatus(200);
    }

    // setup

    protected function setUp() : void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->setService();

        $this->app->user = null;
        $this->app->device = null;
    }

    protected function setService()
    {
        $channel = $this->makeChannel();
        $gcSvc = $this->createMock(GroupChannelService::class);
        $gcSvc->method('findOrCreateChannelForTenantAndRoom')
            ->willReturn($channel);
        $this->app->instance(
            GroupChannelService::class,
            $gcSvc
        );
    }

    protected function makeChannel()
    {
        $channel = new Channel();
        $channel->channel_url = 'some-channel-url';
        return $channel;
    }

}
