<?php

namespace App\Http\Controllers\Api;

use App\Entities\Level\KostLevel;
use App\Entities\Media\Media;
use App\Entities\Point\PointUser;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use App\Entities\Reward\RewardTargetConfig;
use App\Entities\Reward\RewardType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Config;

class RewardControllerTest extends MamiKosTestCase
{
    
    private const URL_GET_REWARD_LIST = 'garuda/reward/list';
    private const URL_GET_REWARD_DETAIL = 'garuda/reward/detail/';
    private const URL_POST_REWARD_REDEEM = 'garuda/reward/redeem/';
    private const URL_GET_USER_REDEEM_LIST = 'garuda/user/redeem/list';
    private const URL_GET_USER_REDEEM_DETAIL = 'garuda/user/redeem/detail/';

    /**
     * @var \App\User
     */
    protected $user;

    /**
     * @var \App\Entities\Reward\Reward
     */
    protected $reward;

    /**
     * @var \App\Entities\Level\KostLevel
     */
    protected $kostLevels;

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $media = factory(Media::class)->create();
        $type = factory(RewardType::class)->create([
            'key' => 'shipping_goods'
        ]);
        $this->reward = factory(Reward::class)->create([
            'media_id' => $media->id,
            'type_id' => $type->id,
            'redeem_value' => 25,
            'user_target' => Reward::ALL_REWARD_USER_TARGET,
            'is_active' => 1,
            'is_published' => 1,
            'redeem_currency' => 'point',
        ]);
        $this->reward->target()->attach($this->user->id);

        $this->reward->quotas()->createMany([
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL_USER])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY_USER])->toArray(),
        ]);
        $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $this->user->id,
            'status' => RewardRedeem::STATUS_FAILED
        ]));

        $this->kostLevels = factory(KostLevel::class, 3)->create();

        \Config::set('kostlevel.id.goldplus1', $this->kostLevels[0]->id);
        \Config::set('kostlevel.id.goldplus2', $this->kostLevels[1]->id);
        \Config::set('kostlevel.id.goldplus3', $this->kostLevels[2]->id);
    }

    public function testIndex_RewardList()
    {
        $reward = $this->reward;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', self::URL_GET_REWARD_LIST, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => [],
            'all_redeemed' => false,
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ]
        ]);
    }

    public function testShow_RewardDetail()
    {
        $reward = $this->reward;
        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible' => true
        ]);
    }

    public function testShow_RewardDetail_WithTargetedOwnerSegment_WithNonGoldPlusOwner_ShouldNotEligible()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $this->user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);
        }

        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[0]->id
        ]));

        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible'=> false
        ]);
    }

    public function testShow_RewardDetail_WithTargetedOwnerSegment_WithUntargetedGoldPlusOwner_ShouldNotEligible()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $this->user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->kostLevels[1]->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[0]->id
        ]));

        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible'=> false
        ]);
    }

    public function testShow_RewardDetail_WithTargetedOwnerSegment_WithTargetedGoldPlusOwner_ShouldEligible()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $this->user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->kostLevels[0]->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[0]->id
        ]));

        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible'=> true
        ]);
    }

    public function testShow_RewardDetail_WithTargetedOwnerSegment_WithMultiTargetedGoldPlusOwner_ShouldEligible()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $this->user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->kostLevels[$i]->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[0]->id
        ]));

        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible'=> true
        ]);
    }

    public function testShow_RewardDetail_WithMultiTargetedOwnerSegment_WithTargetedGoldPlusOwner_ShouldEligible()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $this->user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);
            
            $rooms[$i]->level()->attach($this->kostLevels[2]->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[0]->id
        ]));
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostLevels[2]->id
        ]));

        $urlDetail = self::URL_GET_REWARD_DETAIL . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'id' => $reward->id,
            'name' => $reward->name,
            'description' => $reward->description,
            'tnc' => $reward->tnc,
            'howto' => $reward->howto,
            'start_date' => date_format($reward->start_date, 'Y-m-d H:i:s'),
            'end_date' => date_format($reward->end_date, 'Y-m-d H:i:s'),
            'redeem_value' => $reward->redeem_value,
            'type' => $reward->type->key,
            'remaining_quota' => [
                'total' => $reward->getRemainingQuota()['total'],
                'daily' => $reward->getRemainingQuota()['daily'],
                'total_user' => $reward->getRemainingQuota()['total_user'],
                'daily_user' => $reward->getRemainingQuota()['daily_user'],
            ],
            'redeem' => [
                [
                    'id' => $reward->redeems->first()->public_id,
                    'status' => $reward->redeems->first()->status
                ]
            ],
            'image' => $reward->media->getMediaUrl(),
            'is_eligible'=> true
        ]);
    }

    public function testShow_RewardDetailNotFound()
    {
        $urlDetail = self::URL_GET_REWARD_DETAIL . 999;
        $response = $this->setWebApiCookie()->actingAs($this->user)->json('GET', $urlDetail, []);
        $response->assertOk();
        $response->assertJson([
            'status' => false,
        ]);
        $response->assertJsonMissing(['data']);
    }

    public function testRedeem_RewardRedeemSuccess()
    {
        $reward = $this->reward;
        $user = $this->user;
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500, // should be -25 based on "redeem_value" that we already set above.
        ]);

        $urlRedeem = self::URL_POST_REWARD_REDEEM . $reward->id;
        $response = $this->setWebApiCookie()->actingAs($user)->json('POST', $urlRedeem, [
            'recipient_name' => 'The Name',
            'recipient_phone' => 'The Phone',
            'recipient_notes' => 'The Notes',
            'recipient_data' => 'The Data',
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
        $this->assertDatabaseHas($pointUser->getTable(), [
            'total' => 475
        ]);
    }

    public function testUserRedeemList_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);
        
        $urlRedeemList = self::URL_GET_USER_REDEEM_LIST;
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', $urlRedeemList, [
            'target' => 'all'
        ]);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
        $response->assertJsonFragment([
            'id' => $redeem->public_id,
            'status' => $redeem->status,
            'reward' => [
                'name' => $reward->name,
                'image' => $redeem->reward->media->getMediaUrl()
            ]
        ]);
    }

    public function testUserRedeemDetail_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);
        $dateNow = now();
        $redeem->status_history()->save(factory(RewardRedeemStatusHistory::class)->make([
            'user_id' => $user->id,
            'action' => RewardRedeem::STATUS_FAILED,
            'created_at' => $dateNow,
        ]));

        $urlRedeemDetail = self::URL_GET_USER_REDEEM_DETAIL . $redeem->public_id;
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', $urlRedeemDetail);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
        $response->assertJsonFragment([
            'id' => $redeem->public_id,
            'status' => $redeem->status,
            'notes' => $redeem->notes,
            'reward' => [
                'id' => $redeem->reward->id,
                'name' => $redeem->reward->name,
                'image' => $redeem->reward->media->getMediaUrl(),
                'redeem_value' => $redeem->reward->redeem_value,
            ],
            'history' => [
                [
                    'status' => RewardRedeem::STATUS_FAILED,
                    'date' => $dateNow->toDateTimeString(),
                ]
            ]
        ]);
    }

    public function testUserRedeemDetail_NoHistory()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);

        $urlRedeemDetail = self::URL_GET_USER_REDEEM_DETAIL . $redeem->public_id;
        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', $urlRedeemDetail);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
        ]);
        $response->assertJsonFragment([
            'id' => $redeem->public_id,
            'status' => $redeem->status,
            'notes' => $redeem->notes,
            'reward' => [
                'id' => $redeem->reward->id,
                'name' => $redeem->reward->name,
                'image' => $redeem->reward->media->getMediaUrl(),
                'redeem_value' => $redeem->reward->redeem_value,
            ],
            'history' => null,
        ]);
    }
}
