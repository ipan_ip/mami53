<?php
namespace App\Http\Controllers\Api;

use App\Test\MamiKosTestCase;
use App\User;
use App\Validators\RoomValidator;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevelMap;
use App\Services\Sendbird\GroupChannelService;
use App\Entities\Room\Room;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Activity\Call;

class RoomMetaControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_META_KOS = 'api/v1/stories/meta/%u';

    protected function setUp() : void
    {
        parent::setUp();

        $gcSvc = $this->createMock(GroupChannelService::class);
        $gcSvc->method('findOrCreateChannelForTenantAndRoom')->willReturn(['channel_url' => 'some_url']);
        $this->app->instance(
            GroupChannelService::class,
            $gcSvc
        );
        $this->app->user = null;
        $this->app->device = null;
    }

    /**
     * @group UG
     * @group UG-4089
     * @group App/Http/Controllers/Api/RoomController
     */
    public function testGetRoomMeta(): void
    {
        $userId = mt_rand(1, 1000000);
        $adminId = mt_rand(1, 1000000);
        
        $owner = factory(User::class)->states('owner')->create();
        //$this->actingAs($owner);

        $songId = mt_rand(111111, 999999);
        $room = factory(Room::class)->create([
            'user_id'   => $owner->id,
            'song_id'   => $songId,
            'is_active' => 'true',
            'is_verified_kost' => 1
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id
        ]);
        factory(Call::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            ]
        );

        $levelId = config('kostlevel.id.goldplus4');
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);

        $url = sprintf(self::GET_META_KOS, $room->song_id);

        // hit api detail kos
        $response = $this->json('GET', $url);
        $response->assertOk();
        $this->assertEquals($response->json()['data']['owner_id'], null);
    }
}