<?php

namespace App\Http\Controllers\Api;

use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Media\Media;

class GoldplusControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const URL_LIST_GOLDPLUS_ROOMS = '/api/v1/room/goldplus';
    private const URL_GOLDPLUS_FILTERS = '/api/v1/room/goldplus/filters';
    private const JSON_STRUCTURE_GOLDPLUS_ROOMS = [
        'status',
        'data' => [
            '*' => [
                'id',
                'room_title',
                'area_formatted',
                'address',
                'photo' => [
                    'real',
                    'small',
                    'medium',
                    'large',
                ],
                'gender',
                'gp_status' => [
                    'key',
                    'value',
                ],
            ],
        ],
        'has_more',
    ];
    private const URL_GOLDPLUS_ONBOARDING = '/api/v1/goldplus/onboarding/kost-business';

    private $gp1;
    private $gp2;
    private $gp3;
    private $gp4;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupGoldplusKostLevel();
    }

    private function setupGoldplusKostLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        $this->gp1 = factory(KostLevel::class)->state('gp1')->create();
        $this->gp2 = factory(KostLevel::class)->state('gp2')->create();
        $this->gp3 = factory(KostLevel::class)->state('gp3')->create();
        $this->gp4 = factory(KostLevel::class)->state('gp4')->create();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function actingAs(?Authenticatable $user, $driver = null)
    {
        $this->app->user = $user;
        if (is_null($user)) {
            return $this;
        }
        return parent::actingAs($user, $driver);
    }

    private function createGoldPlusRoom(User $user, int $gpLevelId): Room
    {
        /** @var Room */
        $room = factory(Room::class)->create();
        $room->changeLevel($gpLevelId);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
        ]);
        return $room;
    }

    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetGoldplusFilterOptions()
    {
        $response = $this->getJson(self::URL_GOLDPLUS_FILTERS);
        $response->assertOk();
        $this->assertCount(5, $response->json()['data']);
        $response->assertJson([
            'data' => [
                [
                    'key' => __('api.goldplus.filter-all.key'),
                    'value' => __('api.goldplus.filter-all.value'),
                ],
            ],
        ]);
        foreach (array_slice($response->json()['data'], 1) as $key => $value) {
            // For each GP filters (excluding all). Key should be in form of integer > 0. Name should not be empty
            $this->assertTrue($value['key'] > 0);
            $this->assertNotEmpty($value['value']);
        }
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusEmptyRoom()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [],
            'has_more' => false,
        ]);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusMissingParam()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
        ]));
        $response->assertStatus(400);
        $response->assertJson([
            'status' => false,
        ]);
        $response->assertJsonStructure([
            'messages' => [
                'offset'
            ],
        ]);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusMissingUser()
    {
        $this->actingAs(null);
        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
        ]));
        $response->assertStatus(401);
        $response->assertJson([
            'status' => false,
        ]);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusNonEmptyRoom()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $gp1Room = $this->createGoldPlusRoom($user, $this->gp1->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'id' => $gp1Room->song_id,
                    'gp_status' => [
                        'key' => $this->gp1->id,
                    ]
                ]
            ],
            'has_more' => false,
        ]);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusFilterAllNonEmptyRoom()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->gp1->id);
        $this->createGoldPlusRoom($user, $this->gp2->id);
        $this->createGoldPlusRoom($user, $this->gp3->id);
        $this->createGoldPlusRoom($user, $this->gp4->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'gp_status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => false,
        ]);
        $this->assertCount(4, $response->json()['data']);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusFilteredNonEmptyResult()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->gp1->id);
        $this->createGoldPlusRoom($user, $this->gp2->id);
        $this->createGoldPlusRoom($user, $this->gp3->id);
        $this->createGoldPlusRoom($user, $this->gp4->id);
        $gpStatusFilterId = $this->faker()->randomElement([
            $this->gp1->id,
            $this->gp2->id,
            $this->gp3->id,
            $this->gp4->id,
        ]);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 5,
            'offset' => 0,
            'gp_status' => $gpStatusFilterId,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => false,
        ]);
        $this->assertCount(1, $response->json()['data']);
    }
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusHasMore()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->createGoldPlusRoom($user, $this->gp1->id);
        $this->createGoldPlusRoom($user, $this->gp2->id);
        $this->createGoldPlusRoom($user, $this->gp3->id);
        $this->createGoldPlusRoom($user, $this->gp4->id);

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 2,
            'offset' => 0,
            'gp_status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'has_more' => true,
        ]);
        $this->assertCount(2, $response->json()['data']);
    }

    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusHasPhoto()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = $this->createGoldPlusRoom($user, $this->gp1->id);
        /** @var Media */
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 2,
            'offset' => 0,
            'gp_status' => 0,
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
            'data' => [
                [
                    'photo' => $photo->getMediaUrl(),
                ]
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Controllers\Api\GoldplusController
     */
    public function testGetListKostsGoldplusEmptyGpStatusBecauseIosIssue()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = $this->createGoldPlusRoom($user, $this->gp1->id);
        /** @var Media */
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();

        $response = $this->getJson(self::URL_LIST_GOLDPLUS_ROOMS . '?' . http_build_query([
            'limit' => 2,
            'offset' => 0,
            'gp_status' => '',
        ]));
        $response->assertOk();
        $response->assertJsonStructure(self::JSON_STRUCTURE_GOLDPLUS_ROOMS);
        $response->assertJson([
            'status' => true,
        ]);
    }

    /**
     * @group UG
     * @group UG-5482
     * @group App/Http/Controllers/Api/GoldplusController
     */
    public function testGetGoldplusOnboardingWordsReturnSuccess(): void 
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(self::URL_GOLDPLUS_ONBOARDING);
        $response->assertOk();
        $response->assertJson([
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
            ],
            'data' => __('api.goldplus.onboarding-gp-business'),
        ]);
    }

    /**
     * @group UG
     * @group UG-5482
     * @group App/Http/Controllers/Api/GoldplusController
     */
    public function testGetGoldplusOnboardingWordsReturnFalse(): void 
    {
        $this->app->user = null;
        $this->app->device = null;

        $expectedResponse = [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code'          => 333,
                'severity'      => 'Error',
                'message'      => 'Authorization: Sign-in is required.'
            ]
        ];

        $response = $this->get(self::URL_GOLDPLUS_ONBOARDING);
        $response->assertJson($expectedResponse);
    }
}
