<?php

namespace Test\App\Http\Controllers\Api\Midtrans;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\Entities\Classes\MidtransToken;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Classes\MidtransFinish;

class SnapControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    
    private const POST_SNAPTOKEN_URL = 'api/v1/midtrans/snaptoken';
    private const POST_SNAPTOKEN_FINISH_URL = 'api/v1/midtrans/snapfinish';
    private $mockForAppDevice;

    public function setUp() : void
    {
        parent::setUp();
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });
        
        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    public function testGetSnapTokenMidtransNull()
    {
        $response = $this->json('POST', self::POST_SNAPTOKEN_URL, []);
        $response->assertJson([
            'token' => null
        ]);
    }

    public function testGetSnapTokenMidtransSuccess()
    {
        $user = factory(User::class)->create([
            'id' => 1
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $package = factory(PremiumPackage::class)->create([
            'id' => 1,
            'for' => 'package',
            'name' => 'package name',
            'price' => 10000,
            'sale_price' => 10000,
            'start_date' => date('Y-m-d'),
            'sale_limit_date' => date('Y-m-d H:i:s'),
            'total_day' => 30,
            'view' => 10000,
            'is_active' => '1'
        ]);
        
        $postParams = [
                "package_id" => $package->id,
            ];
        
        $response = $this->json('POST', self::POST_SNAPTOKEN_URL, $postParams);
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }

    public function testSnapFinishMidtransSuccess()
    {
        $user = factory(User::class)->create([
            'id' => 2
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 1,
            "expired_date" => date('Y-m-d'),
            "expired_status" => 'false'
        ]);

        $payment = factory(Payment::class)->create([
            'id' => 1,
            'order_id' => 123,
            'user_id' => $user->id,
            'premium_request_id' => $premiumRequest->id,
        ]);
        
        $postParams = [
            "user_id" => $user->id,
            "order_id" => $payment->order_id,
            "transaction_id" => "kjdkaj8981921jkq",
            "payment_type" => "bank_transfer",
            "transaction_status" => "pending",
            "payment_code" => "jdsj22",
            "pdf_url" => "",
            "biller_code" => "73287232",
            "bill_key" => "273827382783",
            "gross_amount" => 10000
        ];
        
        $response = $this->json('POST', self::POST_SNAPTOKEN_FINISH_URL, $postParams);
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
        $this->mockForAppDevice->disable();
    }
}