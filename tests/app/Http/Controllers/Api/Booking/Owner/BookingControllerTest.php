<?php
namespace Test\App\Http\Controllers\Api\Booking\Owner;

use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use phpmock\MockBuilder;

/**
 *  Test BookingController
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class BookingControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    private const INSTANT_BOOK_URL = 'api/v1/owner/booking/instant/register';

    protected function setUp() : void
    {
        parent::setUp();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName('app')
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    private function request(string $method, string $url, array $params = [], $user = null)
    {
        $user = ($user == null ? $this->defaultUserAuthenticated() : $user);
        return $this->actingAs($user)->json($method, $url, $params);
    }

    public function testInstantBookingUnauthenticated()
    {
        $request = $this->json('POST', self::INSTANT_BOOK_URL);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Data User tidak ditemukan'));
    }

    public function testInstantBookingUserIsNotOwner()
    {
        $user = factory(User::class)->create(['is_owner' => false]);
        $request = $this->request('POST', self::INSTANT_BOOK_URL, [], $user);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Maaf Anda tidak terdaftar sebagai owner'));
    }

    public function testInstantBookingIsBookingFalse()
    {
        $user = factory(User::class)->create([
            'is_owner' => true
        ]);

        $room = factory(Room::class)->create([
            'is_booking' => false
        ]);

        factory(RoomOwner::class)->create([
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $request = $this->request('POST', self::INSTANT_BOOK_URL, [], $user);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Maaf Anda tidak aktif fitur booking'));
    }

    public function testInstantBookingRentCountsIsEmpty()
    {
        $request = $this->request('POST', self::INSTANT_BOOK_URL);
        $request->assertStatus(200);
    }

    public function testInstantBookingSuccess()
    {
        $user = factory(User::class)->create([
            'is_owner' => true
        ]);

        $room = factory(Room::class)->create([
            'is_booking' => true
        ]);

        factory(RoomOwner::class)->create([
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $request = $this->request('POST', self::INSTANT_BOOK_URL, ['rent_counts' => ['semiannually'], 'is_instant_booking' => 1], $user);
        $request->assertStatus(200);
        $request->assertJsonFragment([
            'status' => true,
        ]);
    }

    private function defaultResponseError( string $message = 'General Error' )
    {
        return [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message
            ]
        ];
    }

    private function defaultUserAuthenticated($isOwner = true)
    {
        $data = [];
        if ($isOwner) $data['is_owner'] = true;

        $user = factory(User::class)->create($data);
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();
        return $user;
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        $this->mockForAppDevice->disable();
    }
}