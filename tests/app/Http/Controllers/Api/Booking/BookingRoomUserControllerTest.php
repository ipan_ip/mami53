<?php
namespace App\Http\Controllers\Api\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Presenters\Mamipay\MamipayContractPresenter;
use App\Repositories\Contract\ContractRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;

/**
 *  Test BookingRoomUserController
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class BookingRoomUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    private const ROOM_WITH_CONTRACT_URL = 'api/v1/user/booking/room';
    private $mockForAppDevice;

    private const ROOM_VALID_RESPONSE_STRUCTURE = [
        'status',
        'meta' => [
            'response_code',
            'code',
            'severity',
            'message'
        ],
        'data' => [
            'room_data',
            'booking_data',
            'prices',
            'invoice',
            'years',
            'contract_id'
        ]
    ];

    protected function setUp() : void
    {
        parent::setUp();

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName('app')
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    public function testGetRoomWithContractUnauthenticated()
    {
        $request = $this->json('GET', self::ROOM_WITH_CONTRACT_URL);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Data user tidak ditemukan'));
    }

    public function testGetRoomWithContractMyRoomIsNull()
    {
        $user = factory(User::class)->create();

        app()->user = $user;
        $request = $this->actingAs($user)->json('GET', self::ROOM_WITH_CONTRACT_URL);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Data booking tidak ditemukan'));
    }

    public function testGetRoomWithContractMamipayContractIsNull()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $room->id]);
        factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn(null);

        app()->user = $user;
        $request = $this->actingAs($user)->json('GET', self::ROOM_WITH_CONTRACT_URL);
        $request->assertStatus(200);
        $request->assertJsonFragment($this->defaultResponseError('Data contract tidak ditemukan'));
    }

    public function testGetRoomWithContractSuccess()
    {
        $user               = factory(User::class)->create();
        $room               = factory(Room::class)->create();
        $bookingDesigner    = factory(BookingDesigner::class)->create(['designer_id' => $room->id]);
        $bookingUser        = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'booking_designer_id' => $bookingDesigner->id,
            'contract_id' => 2103201
        ]);
        factory(MamipayContract::class)->create(['id' => $bookingUser->contract_id]);

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn([
                'data' => [
                    'room_data' => [],
                    'booking_data' => [],
                    'prices' => null,
                    'contract_id' => $bookingDesigner->contract_id,
                ]
            ]);

        app()->user = $user;
        $request = $this->actingAs($user)->json('GET', self::ROOM_WITH_CONTRACT_URL);
        $request->assertStatus(200);
        $request->assertJsonStructure(self::ROOM_VALID_RESPONSE_STRUCTURE);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Http\Controllers\Api\Booking\BookingRoomUserController
     *
     * @return void
     */
    public function testGetRoomWithConsultantContract()
    {
        $user = factory(User::class)->create();
        $this->mock(ContractRepository::class, function ($mock) {
            $mock->shouldReceive('getMyRoomContract')->andReturn(factory(MamipayContract::class)->make());
        });
        $this->mock(MamipayContractPresenter::class, function($mock) {
            $mock->shouldReceive('present')->andReturn([
                'data' => [
                    'room_data' => [],
                    'booking_data' => [],
                    'prices' => null,
                    'contract_id' => 9038,
                ]
            ]);
        });
        $this->actingAs($user);
        $response = $this->getJson(self::ROOM_WITH_CONTRACT_URL);
        $response->assertOk();
        $response->assertJsonStructure(self::ROOM_VALID_RESPONSE_STRUCTURE);
    }

    private function defaultResponseError( string $message = 'General Error' )
    {
        return [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message
            ]
        ];
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }
}