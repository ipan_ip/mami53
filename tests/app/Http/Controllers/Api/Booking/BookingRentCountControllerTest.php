<?php

namespace App\Http\Controllers\Api\Booking;

use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BookingRentCountControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware, WithoutEvents;

    // define url
    private const GET_RENT_COUNT_URL        = 'api/v1/user/booking/rent-count/{type}'; // GET
    private const GET_RENT_COUNT_BY_ROOM_URL= 'api/v1/user/booking/rent-count/{type}/room/{roomId}'; // GET
    private const GET_CHECKOUT_DATE_URL     = 'api/v1/user/booking/estimate-checkout'; // POST

    // define date type
    private const WEEKLY        = 'weekly';
    private const MONTHLY       = 'monthly';
    private const QUARTERLY     = 'quarterly';
    private const SEMIANNUALLY  = 'semiannually';
    private const YEARLY        = 'yearly';

    public function testGetRentCountWeeklySuccess()
    {
        $this->rentCountTesting(self::WEEKLY);
    }

    public function testGetRentCountMonthlySuccess()
    {
        $this->rentCountTesting(self::MONTHLY);
    }

    public function testGetRentCountQuarterlySuccess()
    {
        $this->rentCountTesting(self::QUARTERLY);
    }

    public function testGetRentCountSemiannuallySuccess()
    {
        $this->rentCountTesting(self::SEMIANNUALLY);
    }

    public function testGetRentCountYearlySuccess()
    {
        $this->rentCountTesting(self::YEARLY);
    }

    public function testGetRentCountByRoomSuccess()
    {
        // data
        $rentCountRetrive = RentCountHelper::getRentCount(self::QUARTERLY);

        // get url
        $url = str_replace('{type}', self::QUARTERLY, self::GET_RENT_COUNT_BY_ROOM_URL);
        $url = str_replace('{roomId}', 100000, $url);

        // run test
        $response = $this->request('GET', $url);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $rentCountRetrive));
    }

    public function testGetCheckoutDateWeeklySuccess()
    {
        // data
        $data = [
            'rent_count_type' => 'weekly',
            'checkin_date' => Carbon::today()->format('Y-m-d'),
            'duration' => 1
        ];
        $checkoutDateRetrive = Carbon::parse($data['checkin_date'])->addWeeks($data['duration'])->format('Y-m-d');

        // run test
        $response = $this->request('POST', self::GET_CHECKOUT_DATE_URL, $data);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $checkoutDateRetrive));
    }

    public function testGetCheckoutDateQuarterlySuccess()
    {
        // data
        $data = [
            'rent_count_type' => 'quarterly',
            'checkin_date' => Carbon::today()->format('Y-m-d'),
            'duration' => 1
        ];
        $checkoutDateRetrive = Carbon::parse($data['checkin_date'])->addQuarters($data['duration'])->format('Y-m-d');

        // run test
        $response = $this->request('POST', self::GET_CHECKOUT_DATE_URL, $data);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $checkoutDateRetrive));
    }

    public function testGetCheckoutDateSemiannuallySuccess()
    {
        // data
        $data = [
            'rent_count_type' => 'semiannually',
            'checkin_date' => Carbon::today()->format('Y-m-d'),
            'duration' => 1
        ];
        $checkoutDateRetrive = Carbon::parse($data['checkin_date'])->addMonths($data['duration'] * 6)->format('Y-m-d');

        // run test
        $response = $this->request('POST', self::GET_CHECKOUT_DATE_URL, $data);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $checkoutDateRetrive));
    }

    public function testGetCheckoutDateYearlySuccess()
    {
        // data
        $data = [
            'rent_count_type' => 'yearly',
            'checkin_date' => Carbon::today()->format('Y-m-d'),
            'duration' => 1
        ];
        $checkoutDateRetrive = Carbon::parse($data['checkin_date'])->addYear($data['duration'])->format('Y-m-d');

        // run test
        $response = $this->request('POST', self::GET_CHECKOUT_DATE_URL, $data);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $checkoutDateRetrive));
    }

    public function testGetCheckoutDateMonthlySuccess()
    {
        // data
        $data = [
            'rent_count_type' => 'montly',
            'checkin_date' => Carbon::today()->format('Y-m-d'),
            'duration' => 1
        ];
        $checkoutDateRetrive = Carbon::parse($data['checkin_date'])->addMonth($data['duration'])->format('Y-m-d');

        // run test
        $response = $this->request('POST', self::GET_CHECKOUT_DATE_URL, $data);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $checkoutDateRetrive));
    }

    private function rentCountTesting(string $type = ''): void
    {
        // data
        $rentCountRetrive = RentCountHelper::getRentCount($type);

        // get url
        $url = str_replace('{type}', $type, self::GET_RENT_COUNT_URL);

        // run test
        $response = $this->request('GET', $url);
        $response->assertOk();
        $response->assertJson($this->defaultResponse(true, '', $rentCountRetrive));
    }

    private function request(string $method, string $url, array $params = [], $user = null)
    {
        $user = ($user == null ? $this->createEntityUser() : $user);
        return $this->actingAs($user)->json($method, $url, $params);
    }

    private function createEntityUser(array $overrideData = []): User
    {
        return factory(User::class)->create($overrideData);
    }

    private function defaultResponse(bool $status = true, string $message = '', $data = null): array
    {
        if ($status && empty($message))
            $message = 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.';

        $response = [];
        $response['status'] = $status;
        $response['meta'] = [
            'response_code' => 200,
            'message' => $message
        ];
        if ($data !== null) $response['data'] = $data;

        return $response;
    }
}