<?php
namespace Test\App\Http\Controllers\Api\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Device\UserDevice;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\BookingUserHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use phpmock\MockBuilder;

/**
 *  Test NewBookingUserController
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class NewBookingUserControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const LIST_BOOKING_URL          = 'api/v1/user/booking';
    private const CREATE_BOOKING_URL        = 'api/v1/user/booking';
    private const GET_DETAIL_ROOM_URL       = 'api/v1/user/booking/detail-room';
    private const GET_DETAIL_BOOKING_URL    = 'api/v1/user/booking/detail';
    private const CANCEL_BOOKING_URL        = 'api/v1/user/booking/cancel';

    private $bookingUserId = 1;

    private $mockForAppDevice;

    protected function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockAlternatively('App\Repositories\Booking\BookingUserRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName('app')
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();

        // without jobs
        $this->withoutJobs();
    }

    private function request(string $method, string $url, array $params = [], $user = null)
    {
        $user = ($user == null ? $this->defaultUserAuthenticated() : $user);
        return $this->actingAs($user)->json($method, $url, $params);
    }

    private function requestCreateBooking(int $roomId, int $roomTypeId, array $params, bool $auth = false)
    {
        $roomId = $roomId ?: 1;
        $roomTypeId = $roomTypeId ?: 1;
        return $this->request(
            'POST',
            $this->withAccessToken(self::CREATE_BOOKING_URL . '/' . $roomId . '/' . $roomTypeId),
            $params
        );
    }

    private function requestGetDetailRoom(int $roomId)
    {
        $roomId = $roomId ?: 1;
        return $this->request('GET', $this->withAccessToken(self::GET_DETAIL_ROOM_URL . '/' . $roomId));
    }

    private function requestGetDetailBooking(int $bookId, User $user = null)
    {
        $bookId = $bookId ?: 1;
        return $this->request('GET', $this->withAccessToken(self::GET_DETAIL_BOOKING_URL . '/' . $bookId), [], $user);
    }

    private function requestCancelBooking(string $bookingCode, array $params, User $user = null)
    {
        $bookingCode = $bookingCode ?: 1;
        return $this->request(
            'POST',
            $this->withAccessToken(self::CANCEL_BOOKING_URL . '/' . $bookingCode),
            $params,
            $user
        );
    }

    private function defaultValidParams()
    {
        return [
            'rent_count_type' => 'quarterly',
            'duration' => 1,
            'checkin' => Carbon::now()->format('Y-m-d'),
            'checkout' => Carbon::parse(Carbon::now()->format('Y-m-d'))->addQuarters(1)->format('Y-m-d'),
            'contact_name' => 'Syifaxxx',
            'contact_phone' => '082234167880',
            'contact_job'   => 'mahasiswa',
            'contact_gender' => 'male'
        ];
    }

    private function defaultValidParamCancelBooking()
    {
        return ['cancel_reason' => 'Sudah dapat kos baru'];
    }

    private function defaultResponseError( string $message = 'General Error' )
    {
        return [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => $message
            ]
        ];
    }

    private function defaultResponseBookingUserListTransformer()
    {
        return [
            '_id' => 1,
            'name' => 'room',
            'address' => 'xx',
            'type' => 'premium',
            'gender' => 'male',
            'photo' => 'https://url',
            'url' => 'https://url',
            'slug' => 'room',
            'floor' => null,
            'number' => 1,
            'latitude' => null,
            'longitude' => null,
            'top_facilities' => null,
            'fac_room' => null,
            'fac_share' => null,
            'fac_bath' => null,
            'fac_near' => null,
            'fac_park' => null,
            'fac_price' => null,
            'price' => null
        ];
    }

    private function defaultUserAuthenticated()
    {
        $user = factory(User::class)->create();
        $device = factory(UserDevice::class)->state('login')->create([
            'user_id' => $user->id
        ]);
        $user->last_login = $device->id;
        $user->save();
        return $user;
    }

    private function createRoom(array $data)
    {
        return factory(Room::class)->create($data);
    }

    private function createBookingDesigner(Room $room, array $data)
    {
        $default = ['designer_id' => $room->id];
        return factory(BookingDesigner::class)->create(array_merge($default, $data));
    }

    private function createBookingUser(array $data)
    {
        $this->bookingUserId += 1;
        $default = [
            'id' => $this->bookingUserId,
            'checkout_date' => Carbon::now()->addMonth(1)
        ];
        return factory(BookingUser::class)->create(array_merge($default, $data));
    }

    public function testCreateBookingEmptyRequest()
    {
        $response = $this->requestCreateBooking(1, 1, [], true);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
        ]);
    }

    public function testCreateBookingNotAuthenticated()
    {
        $response = $this->requestCreateBooking(1, 1, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
        ]);
    }

    public function testCreateBookingCheckoutDateInvalid()
    {
        $room = $this->createRoom(['id' => 10, 'song_id' => 10, 'is_booking' => 0]);
        $params = $this->defaultValidParams();
        unset($params['checkout']);
        $params['checkout'] = Carbon::today()->format('Y-m-d');

        $response = $this->requestCreateBooking($room->song_id, 1, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Tanggal Checkout tidak valid'));
    }

    public function testCreateBookingPreebookInvalid()
    {
        $checkoutErrorMessage = 'Tanggal checkin tidak bisa melebihi :month bulan dari hari ini';
        $maxMonthCheckInNormal = (int) env('MAX_MONTH_CHECKIN_NORMAL', 2);
        $room = $this->createRoom(['id' => 11, 'song_id' => 11, 'is_booking' => 0]);
        $params = $this->defaultValidParams();
        unset($params['checkin']);
        $params['checkin'] = Carbon::today()->addMonth(3)->format('Y-m-d');
        $params['checkout'] =  Carbon::parse($params['checkin'])->addQuarters(1)->format('Y-m-d');

        $response = $this->requestCreateBooking($room->song_id, 1, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError(str_replace(':month', $maxMonthCheckInNormal, $checkoutErrorMessage)));
    }

    public function testCreateBookingPreebookSuccess()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(1)->format('Y-m-d')
        ]);
        $room = $this->createRoom(['id' => 12, 'song_id' => 12, 'is_booking' => 0]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        // now prebook active on owner premium and kost level gold plus
        $level = factory(KostLevel::class)->create([
            'id' => config('kostlevel.id.goldplus1'),
            'name' => 'Mamikos Goldplus 1'
        ]);

        // add level to kost / room
        $room->changeLevel($level->id);

        $params = $this->defaultValidParams();
        unset($params['checkin']);
        $params['checkin'] = Carbon::today()->addMonth(3)->format('Y-m-d');
        $params['checkout'] =  Carbon::parse($params['checkin'])->addQuarters(1)->format('Y-m-d');

        $response = $this->requestCreateBooking($room->song_id, 1, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Fitur booking tidak aktif untuk kost ini'));

        $this->assertTrue(BookingUserHelper::isPremiumOwner($roomOwner));
    }

    public function testCreateBookingRoomNotExist()
    {
        $response = $this->requestCreateBooking(1, 1, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Kamar tidak ditemukan'));
    }

    public function testCreateBookingRoomInactiveFeatureBooking()
    {
        $room = $this->createRoom(['id' => 1, 'song_id' => 1, 'is_booking' => 0]);
        $response = $this->requestCreateBooking($room->id, 1, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Fitur booking tidak aktif untuk kost ini'));
    }

    public function testCreateBookingGenderInvalid()
    {
        $room = $this->createRoom(['id' => 2, 'song_id' => 2, 'is_booking' => 1, 'gender' => 2]);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $response = $this->requestCreateBooking($room->id, 1, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Gagal, periksa kembali tipe kost Anda.'));
    }

    public function testCreateBookingRoomInactive()
    {
        $room = $this->createRoom(['id' => 3, 'song_id' => 3, 'is_booking' => 1, 'gender' => 1]);
        $bookingDesigner = $this->createBookingDesigner($room, ['id' => 1, 'is_active' => 0]);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $response = $this->requestCreateBooking($room->id, $bookingDesigner->id, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Tipe Kamar tidak ditemukan'));
    }

    public function testCreateBookingPhoneNumberInvalid()
    {
        $room = $this->createRoom(['id' => 4, 'song_id' => 4, 'is_booking' => 1, 'gender' => 1]);
        $bookingDesigner = $this->createBookingDesigner($room, ['id' => 2, 'is_active' => 1]);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $params = $this->defaultValidParams();
        $params['contact_phone'] = '082121xxx21'; // make phone number invalid

        $response = $this->requestCreateBooking($room->id, $bookingDesigner->id, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Mohon maaf, No Handphone tidak valid'));
    }

    public function testCreateBookingFlashSaleIsInvalid()
    {
        $room = $this->createRoom(['id' => 4, 'song_id' => 4, 'is_booking' => 1, 'gender' => 1]);
        $bookingDesigner = $this->createBookingDesigner($room, ['id' => 2, 'is_active' => 1]);

        $params = $this->defaultValidParams();
        $params['is_flash_sale'] = true;

        $response = $this->requestCreateBooking($room->id, $bookingDesigner->id, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('flash_sale_terminated'));
    }

    public function testCreateBookingSuccess()
    {
        $room = $this->createRoom(['id' => 5, 'song_id' => 5, 'is_booking' => 1, 'gender' => 1]);
        $bookingDesigner = $this->createBookingDesigner($room, ['id' => 3, 'is_active' => 1]);
        $bookingUser = $this->createBookingUser(['id' => 1, 'designer_id' => $room->song_id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $this->mock->shouldReceive('newBookRoom')
            ->once()
            ->andReturn($bookingUser);

        $controller = Mockery::mock('App\Http\Controllers\Api\Booking\NewBookingUserController', 'App\Http\Controllers\Api\BaseController');
        $controller->shouldReceive('updateUserProfile')
            ->andReturn([
                'data' => [
                    'name' => 'Syifandi'
                ]
            ]);

        $bookingUserHelper = $this->mockAlternatively('overload:App\Http\Helpers\BookingUserHelper');

        $bookingUserHelper->shouldReceive('getCheckoutDate')
            ->once()
            ->andReturn($this->defaultValidParams()['checkout']);

        $bookingUserHelper->shouldReceive('getInterfaceForMoEngage')
            ->andReturn('desktop');

        $controller->shouldReceive('sendNotification')
            ->andReturn(true);

        $controller->shouldReceive('sendWhatsApp')
            ->andReturn(true);

        $bookingService = $this->mockAlternatively('overload:App\Services\Booking\BookingService');
        $bookingService->shouldReceive('sendEmailWhenSubmitBookingToOwner')
                        ->andReturn(true);

        $bookingService->shouldReceive('sendChatAfterBookingSuccess')
                        ->once()
                        ->andReturn('some_channel_url');

        $params = $this->defaultValidParams();
        $params['contact_phone'] = '082231238539';
        $params['session_id'] = '98ec0b79-c357-471c-b833-fdd4f0d819b5';

        $response = $this->requestCreateBooking($room->id, $bookingDesigner->id, $params, true);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => true,
        ]);
    }

    public function testDetailRoomNotFound()
    {
        $response = $this->requestGetDetailRoom(10002);

        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Data Kos tidak ditemukan'));
    }

    public function testDetailRoomBookingDesignerNotFound()
    {
        $room = $this->createRoom(['id' => 6, 'song_id' => 6, 'is_booking' => 1, 'gender' => 1]);

        $this->mock->shouldReceive('checkTenantAlreadyBooked')
            ->once()
            ->andReturn(null);

        $response = $this->requestGetDetailRoom($room->id);

        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Maaf ada kendala saat melakukan booking, silahkan kontak CS Kami'));
    }

    public function testDetailRoomSuccess()
    {
        $room = $this->createRoom(['id' => 7, 'song_id' => 7, 'is_booking' => 1, 'gender' => 1, 'price_monthly' => 100000]);
        $this->createBookingDesigner($room, ['id' => 4, 'is_active' => 1]);
        app()->device = factory(UserDevice::class)->create();

        $this->mock->shouldReceive('checkTenantAlreadyBooked')
            ->once()
            ->andReturn(null);

        $response = $this->requestGetDetailRoom($room->id);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data' => [
                'room',
                'user',
                'draft'
            ]
        ]);
    }

    public function testDetailBookingNotFound()
    {
        $response = $this->requestGetDetailBooking(1111);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Data booking tidak ditemukan'));
    }

    public function testDetailBookingInvalidTransformer()
    {
        $user = $this->defaultUserAuthenticated();
        $bookingUser = $this->createBookingUser([
            'id' => 2,
            'booking_code' => 'MM-01',
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'user_id' => $user->id,
        ]);

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn(null);

        $response = $this->requestGetDetailBooking($bookingUser->id);

        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Data booking tidak ditemukan'));
    }

    public function testDetailBookingSuccess()
    {
        $user = $this->defaultUserAuthenticated();
        $bookingUser = $this->createBookingUser([
            'id' => 3,
            'booking_code' => 'MM-01',
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'user_id' => $user->id,
        ]);

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn([
                'data' => [
                    'id' => $bookingUser->id
                ]
            ]);

        $response = $this->requestGetDetailBooking($bookingUser->id, $user);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data'
        ]);
    }

    public function testListBookingTransformDataNull()
    {
        $user = $this->defaultUserAuthenticated();

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn(null);

        $response = $this->actingAs($user)->json('GET', $this->withAccessToken('api/v1/user/booking'), []);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Data booking tidak ditemukan'));
    }

    public function testListBookingTransformDataInvalid()
    {
        $user = $this->defaultUserAuthenticated();

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn(null);

        $response = $this->actingAs($user)->json('GET', $this->withAccessToken('api/v1/user/booking'), []);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Data booking tidak ditemukan'));
    }

    public function testListBookingSuccess()
    {
        $user = $this->defaultUserAuthenticated();

        $presenter = Mockery::mock('overload:App\Presenters\BookingUserPresenter', 'Prettus\Repository\Presenter\FractalPresenter\FractalPresenter');
        $presenter->shouldReceive('present')
            ->andReturn([
                'data' => $this->defaultResponseBookingUserListTransformer()
            ]);

        $response = $this->actingAs($user)->json('GET', $this->withAccessToken('api/v1/user/booking'), []);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'meta' => [
                'response_code',
                'code',
                'severity',
                'message'
            ],
            'data' => [
                'data' => array_keys($this->defaultResponseBookingUserListTransformer()),
                'current_page',
                'from',
                'last_page',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
                'has-more'
            ]
        ]);
    }

    public function testCancelBookingEmptyRequest()
    {
        $response = $this->requestCancelBooking(1, []);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
        ]);
    }

    public function testCancelBookingNotFound()
    {
        $response = $this->requestCancelBooking(1, $this->defaultValidParamCancelBooking());
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Booking tidak ditemukan'));
    }

    public function testCancelBookingAlreadyVerified()
    {
        $user = $this->defaultUserAuthenticated();
        $bookingUser = $this->createBookingUser([
            'id' => 4,
            'booking_code' => 'MM-01',
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'user_id' => $user->id,
        ]);
        $response = $this->requestCancelBooking($bookingUser->booking_code, $this->defaultValidParamCancelBooking(), $user);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Booking tidak dapat dibatalkan'));
    }

    public function testCancelBookingRoomNotFound()
    {
        $user = $this->defaultUserAuthenticated();
        $bookingUser = $this->createBookingUser([
            'id' => 5,
            'booking_code' => 'MM-02',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'user_id' => $user->id,
            'designer_id' => 10000
        ]);
        $response = $this->requestCancelBooking($bookingUser->booking_code, $this->defaultValidParamCancelBooking(), $user);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Kamar tidak ditemukan'));
    }

    public function testCancelBookingFailedPostToRepository()
    {
        $user = $this->defaultUserAuthenticated();
        $room = $this->createRoom(['id' => 8, 'song_id' => 8, 'is_booking' => 1, 'gender' => 1, 'price_monthly' => 100000]);

        $bookingUser = $this->createBookingUser([
            'id' => 6,
            'booking_code' => 'MM-03',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $this->mock->shouldReceive('cancelBooking')
            ->andReturn(false);

        $response = $this->requestCancelBooking($bookingUser->booking_code, $this->defaultValidParamCancelBooking(), $user);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Pembatalan Booking tidak bisa dilakukan'));
    }

    public function testCancelBookingSuccess()
    {
        $user = $this->defaultUserAuthenticated();
        $room = $this->createRoom(['id' => 9, 'song_id' => 9, 'is_booking' => 1, 'gender' => 1, 'price_monthly' => 100000]);

        $bookingUser = $this->createBookingUser([
            'id' => 7,
            'booking_code' => 'MM-04',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $this->mock->shouldReceive('cancelBooking')
            ->andReturn(true);

        $bookingNotificationHelper = $this->mockAlternatively('App\Http\Helpers\BookingNotificationHelper');
        $bookingNotificationHelper->shouldReceive('notifCancelBookingToOwner')->andReturn(true);
        $bookingNotificationHelper->shouldReceive('sendSmsCancelBookingToOwner')->andReturn(true);
        $bookingNotificationHelper->shouldReceive('notifCancelBookingToTenant')->andReturn(true);

        //$category = $this->mockAlternatively('overload:App\Entities\Notif\Category');
        //$category->shouldReceive('getNotificationCategory')
        //    ->andReturn(null);

        $response = $this->requestCancelBooking($bookingUser->booking_code, $this->defaultValidParamCancelBooking(), $user);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => true,
            'meta' => [
                'response_code' => 200,
                'code' => 200,
                'severity' => 'OK',
                'message' => 'Pembatalan Booking telah berhasil'
            ],
            'data' => [
                'cancel_status' => true
            ]
        ]);
    }

    public function testCreateBookingRoomDontHaveOwner()
    {
        $room = $this->createRoom(['id' => 13, 'song_id' => 13, 'is_booking' => 1, 'gender' => 1]);
        $response = $this->requestCreateBooking($room->id, 1, $this->defaultValidParams(), true);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponseError('Silahkan hubungi CS untuk melanjutkan proses booking'));
    }

    protected function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }
}