<?php
namespace Test\App\Http\Controllers\Api\Booking;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\Entities\User\UserDesignerViewHistory;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BookingUserDraftControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_DRAFT_BOOKING_URL     = 'api/v1/user/draft-booking';
    private const CREATE_DRAFT_BOOKING_URL  = 'api/v1/user/draft-booking';
    private const GET_HOMEPAGE_SHORTCUT_URL = 'api/v1/user/draft-booking/shortcut';
    private const DESTROY_DRAFT_BOOKING_URL = 'api/v1/user/draft-booking/{draftBookingId}';
    private const GET_LAST_SEEN_URL         = 'api/v1/user/draft-booking/last-seen';
    private const DESTROY_LAST_SEEN_URL     = 'api/v1/user/draft-booking/last-seen/{roomId}';
    private const GET_NOTIFICATION_URL      = 'api/v1/user/draft-booking/notification';

    public function testIndexIsEmpty()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;
        $params = [];
        $params['limit'] = 10;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_DRAFT_BOOKING_URL), $params);
        $response->assertStatus(200);
        $response->assertJson(
            $this->defaultResponse(true, '', [
                'data' => null,
                'current_page' => 1,
                'from' => null,
                'last_page' => 1,
                'next_page_url' => null,
                'per_page' => $params['limit'],
                'prev_page_url' => null,
                'to' => 0,
                'total' => 0,
                'has_more' => false
            ])
        );
    }

    public function testIndexSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'room_available' => 5
        ]);
        factory(BookingUserDraft::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);
        app()->user = $user;
        $params = [];
        $params['limit'] = 10;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_DRAFT_BOOKING_URL), $params);
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse());
    }

    public function testCreateRoomIsEmpty()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('POST', $this->withAccessToken(self::CREATE_DRAFT_BOOKING_URL), []);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponse(false, 'Kamar tidak boleh kosong'));
    }

    public function testCreateRoomIsInvalid()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;

        // params
        $params = ['room_id' => 1];

        // run test
        $response = $this->actingAs($user)->json('POST', $this->withAccessToken(self::CREATE_DRAFT_BOOKING_URL), $params);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponse(false, 'Kamar tidak di temukan'));
    }

    public function testCreateDraftBookingSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        app()->user = $user;

        // params
        $params = [
            'room_id' => $room->id,
            'rent_count_type' => BookingUser::MONTHLY_TYPE
        ];

        // run test
        $response = $this->actingAs($user)->json('POST', $this->withAccessToken(self::CREATE_DRAFT_BOOKING_URL), $params);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponse());
    }

    public function testCreateDraftBookingSuccessWithFullParams()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        app()->user = $user;

        // params
        $params = [
            'room_id' => $room->id,
            'rent_count_type' => BookingUser::MONTHLY_TYPE,
            'duration' => 1,
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' => Carbon::today()->addMonth(1)->format('Y-m-d'),
            'contact_name' => 'Syifandi Mulyanto',
            'contact_phone' => '082234167880',
            'contact_job' => 'Kuliah',
            'contact_introduction' => 'Saya seorang web developer',
            'contact_work_place' => 'PT Git Gow Ayo',
            'total_renter' => 1,
            'is_married' => true
        ];

        // run test
        $response = $this->actingAs($user)->json('POST', $this->withAccessToken(self::CREATE_DRAFT_BOOKING_URL), $params);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->defaultResponse());
    }

    public function testGetHomepageShortcutIsNull()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_HOMEPAGE_SHORTCUT_URL));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(true, '', ['total' => 0, 'room' => null]));
    }

    public function testGetHomepageShortcutSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'room_available' => 5
        ]);
        factory(BookingUserDraft::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_HOMEPAGE_SHORTCUT_URL));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(true, '', ['total' => 1]));
    }

    public function testDestroyRoomNotFound()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $url  = str_replace('{draftBookingId}', 1, self::DESTROY_DRAFT_BOOKING_URL);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('DELETE', $this->withAccessToken($url));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(false, 'Room tidak ditemukan'));
    }


    public function testDestroyRoomSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $bookingUserDraft = factory(BookingUserDraft::class)->create([
            'user_id' => $user->id
        ]);
        $url  = str_replace('{draftBookingId}', $bookingUserDraft->id, self::DESTROY_DRAFT_BOOKING_URL);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('DELETE', $this->withAccessToken($url));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(true, 'Draft booking berhasil dihapus'));
    }

    public function testLastSeenIsEmpty()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;
        $params = [];
        $params['limit'] = 10;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_LAST_SEEN_URL), $params);
        $response->assertStatus(200);
        $response->assertJson(
            $this->defaultResponse(true, '', [
                'data' => null,
                'current_page' => 1,
                'from' => null,
                'last_page' => 1,
                'next_page_url' => null,
                'per_page' => $params['limit'],
                'prev_page_url' => null,
                'to' => 0,
                'total' => 0,
                'has_more' => false
            ])
        );
    }

    public function testLastSeenSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'price_monthly' => 1000000
        ]);
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'viewed_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        app()->user = $user;
        $params = [];
        $params['limit'] = 10;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_LAST_SEEN_URL), $params);
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse());
    }

    public function testGetNotificationIsEmpty()
    {
        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;
        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_NOTIFICATION_URL), []);
        $response->assertJson(
            $this->defaultResponse(true, '', [
                'badge' => false,
                'total_of_draft' => 0,
                'total_of_last_seen' => 0
            ])
        );
    }

    public function testGetNotificationSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'room_available' => 5
        ]);
        factory(BookingUserDraft::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED,
            'read' => false
        ]);
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'read' => false
        ]);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('GET', $this->withAccessToken(self::GET_NOTIFICATION_URL), []);
        $response->assertJson(
            $this->defaultResponse(true, '', [
                'badge' => true,
                'total_of_draft' => 1,
                'total_of_last_seen' => 1
            ])
        );
    }

    public function testDestroyViewedRoomNotFound()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $url  = str_replace('{roomId}', 1, self::DESTROY_LAST_SEEN_URL);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('DELETE', $this->withAccessToken($url));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(false, 'Room tidak ditemukan'));
    }


    public function testDestroyViewedRoomSuccess()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $url  = str_replace('{roomId}', $room->id, self::DESTROY_LAST_SEEN_URL);
        app()->user = $user;

        // run test
        $response = $this->actingAs($user)->json('DELETE', $this->withAccessToken($url));
        $response->assertStatus(200);
        $response->assertJson($this->defaultResponse(true, 'Kos Baru Dilihat berhasil dihapus'));
    }

    private function defaultResponse(bool $status = true, string $message = '', $data = null): array
    {
        if ($status && empty($message))
            $message = 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.';

        $response = [];
        $response['status'] = $status;
        $response['meta'] = [
            'response_code' => 200,
            'code' => 200,
            'severity' => "OK",
            'message' => $message
        ];
        if ($data !== null) $response['data'] = $data;

        return $response;
    }
}