<?php

namespace app\Http\Controllers\Api\Booking;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;

class BookingRejectControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private $mockForAppDevice;

    const GET_BY_TYPE = 'api/v1/booking/get-reject-reason/cancel';

    protected function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockAlternatively('App\Repositories\Booking\RejectReason\BookingRejectReasonRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName('app')
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }


    public function testGetAllByType()
    {
        $this->mock->shouldReceive('getAllByType')
            ->once()
            ->andReturn([
                [
                    'id' => 4,
                    'description' => 'Sudah tidak tersedia lagi kamar yang bisa dibooking',
                    'is_active' => 1,
                    'is_affecting_acceptance' => 0,
                    'deleted_at' => null,
                    'created_at' => '2020-06-22 11:03:49',
                    'updated_at' => '2020-09-10 11:06:18',
                    'type' => 'cancel',
                    'make_kost_not_available' => 1
                ]
            ]);

        // prepare variable
        $user = factory(User::class)->create();
        app()->user = $user;

        $response = $this->actingAs($user)->json('get', $this->withAccessToken(self::GET_BY_TYPE), []);
        $response->assertStatus(200);
        $this->assertNotNull($response);
    }
}