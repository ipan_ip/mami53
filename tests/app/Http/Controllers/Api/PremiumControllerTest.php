<?php
namespace App\Http\Api;

use App\Entities\Premium\Bank;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\AdHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\View;
use App\Repositories\PremiumRepository;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;
use App\User;
use StatsLib;

class PremiumControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_HISTORY_URL = 'api/v1/owner/premium/history';
    private const API_GET_BANK_URL = 'api/v1/premium/bank';
    private const API_GET_BALANCE_PACKAGE_URL = 'api/v1/premium/balance';
    private const API_GET_PREMIUM_PACKAGE_URL = 'api/v1/premium/package';
    private const API_POST_REQUEST_OWNER_URL = 'api/v1/owner/request';
    private const API_POST_AUTO_APPROVE_TRIAL_URL = 'api/v1/owner/premium/trial';
    private const API_GET_CONFIRMATION_URL = 'api/v1/owner/premium/confirmation';
    private const API_POST_BUY_BALANCE_URL = 'api/v1/owner/balance';
    private const API_GET_FAVORITE_BY_CLICK = 'api/v1/premium/ads/favorite';
    private $mockForAppDevice;
    private $mockPremiumRepo;
    private $mockPremiumService;

    public function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockPremiumRepo = $this->mockAlternatively('App\Repositories\PremiumRepository');
        $this->mockPremiumService = $this->mockAlternatively('App\Services\PremiumService');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }

    public function testGetListBank()
    {
        $bankData = [];
        $seedBank = factory(Bank::class, 3)->make();
        foreach($seedBank as $item) {
            $bankData[] = [
                "id" => $item->id,
                "name" => $item->name,
                "account_name" => $item->account_name,
                "number" => $item->number,
                "logo" => ""
            ];
        }
        $this->mockPremiumRepo->shouldReceive('getBankAccount')->andReturn($bankData);
        $response = $this->json('GET', self::API_GET_BANK_URL);
        $responseJson = json_decode($response->getContent());
        $this->assertEquals(
            array_column($bankData, 'name'),
            array_column($responseJson->bank_accounts, 'name')
        );
    }

    public function testGetPremiumPackage()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        app()->user = $user;

        $packageDataFile = file_get_contents(storage_path('test/json/premium/premium_package.json'));
        $packageData = json_decode($packageDataFile, true);
        $this->mockPremiumService->shouldReceive('getPremiumPackages')->andReturn($packageData);
        $response = $this->json('GET', self::API_GET_PREMIUM_PACKAGE_URL . '?v=2');
        $responseJson = json_decode($response->getContent());        
        $this->assertEquals(
            array_column($packageData['packages'], 'name'),
            array_column($responseJson->packages, 'name')
        );
        $this->assertEquals(
            array_column($packageData['features'], 'name'),
            array_column($responseJson->features, 'name')
        );
    }

    public function testGetPremiumBalance()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        app()->user = $user;

        $packageDataFile = file_get_contents(storage_path('test/json/premium/balance_package.json'));
        $packageData = json_decode($packageDataFile, true);
        $this->mockPremiumRepo->shouldReceive('getPremiumPackage')->andReturn($packageData);
        $response = $this->json('GET', self::API_GET_BALANCE_PACKAGE_URL);
        $responseJson = json_decode($response->getContent());
        $this->assertEquals(
            array_column($packageData['packages'], 'name'),
            array_column($responseJson->packages, 'name')
        );
        $this->assertEquals(
            array_column($packageData['features'], 'name'),
            array_column($responseJson->features, 'name')
        );
    }

    public function testRequestOwner()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        app()->user = $user;

        $mockedResult = [
            "action" => true,
            "message"=> "Terima kasih anda telah melakukan upgrade akun",
            "proses" => "Tunggu Konfirmasi",
            "jumlah" => "IDR 100.000",
            "active_package" => 2,
            "countdown" => 3600,
        ];
        $this->mockPremiumRepo->shouldReceive('prosesRequestOwner')->andReturn($mockedResult);
        $params = [
            'is_cancel' => true
        ];
        $response = $this->json('POST', self::API_POST_REQUEST_OWNER_URL, $params);
        $responseArray = json_decode($response->getContent(), true);
        $this->assertFalse($responseArray['status']);
    }

    public function testBuyBalance()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('+7 day'))
        ]);
        $premiumPackage =  factory(PremiumPackage::class)->create();
        factory(PremiumRequest::class)->create([
            'premium_package_id' => $premiumPackage->id,
            'user_id' => $user->id
        ]);
        $mockedResult = [
            "action"  => true,
            "balance_id" => 34,
            "message" => "Terima Kasih sudah melakukan pembelian view",
            "proses"  => "Tunggu Konfirmasi",
            "views"   => 200,
            "price"   => "10.000"
        ];
        $this->actingAs($user);
        app()->user = $user;

        $this->mockPremiumRepo->shouldReceive('processBuyBalance')->andReturn($mockedResult);

        $response = $this->json('POST', self::API_POST_BUY_BALANCE_URL);
        $responseArray = json_decode($response->getContent(), true);
        $this->assertFalse($responseArray['status']);
    }

    public function testBuyBalanceWithUserNotPremium()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => null
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::API_POST_BUY_BALANCE_URL);
        $responseArray = json_decode($response->getContent(), true);
        $this->assertFalse($responseArray['status']);
    }

    public function testAutoApproveTrial()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        app()->user = $user;

        $this->mockPremiumRepo->shouldReceive('autoApproveTrial')->andReturn([
            'status' => true,
            'message' => null
        ]);

        $response = $this->json('POST', self::API_POST_AUTO_APPROVE_TRIAL_URL);
        $responseObj = json_decode($response->getContent());
        $this->assertTrue($responseObj->status);
    }

    public function testAutoApproveTrialWithNoUser()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::API_POST_AUTO_APPROVE_TRIAL_URL);
        $responseObj = json_decode($response->getContent());
        $this->assertEquals(false, $responseObj->status);
        $this->assertEquals("User tidak ditemukan", $responseObj->message);
    }

    public function testAutoApproveTrialWithUserPremium()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('POST', self::API_POST_AUTO_APPROVE_TRIAL_URL);
        $responseObj = json_decode($response->getContent());
        $this->assertEquals(false, $responseObj->status);
        $this->assertEquals("Anda tidak bisa melakukan trial", $responseObj->message);
    }

    public function testGetConfirmation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        app()->user = $user;

        $mockedConfirmationData = [
            "membership" => [],
            "bank" => []
        ];
        $this->mockPremiumRepo->shouldReceive('getConfirmation')->andReturn($mockedConfirmationData);
        $response = $this->json('GET', self::API_GET_CONFIRMATION_URL);
        $responseArray = json_decode($response->getContent(), true);
        $responseArrayKeys = array_keys($responseArray);
        $expectedResponseKeys = [
            "status",
            "message",
            "membership",
            "bank"
        ];
        foreach($expectedResponseKeys as $key) {
            $this->assertContains(
                $key,
                $responseArrayKeys
            );
        }
    }

    public function testGetConfirmationWithNoUser()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);
        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('GET', self::API_GET_CONFIRMATION_URL);
        $responseObj = json_decode($response->getContent());
        $this->assertEquals(false, $responseObj->status);
        $this->assertEquals('Anda belum login ', $responseObj->message);
    }

    public function testGetMostClickByAdHistory()
    {
        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 9
        ]);
        $room2 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 99
        ]);
        $room3 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 98
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        factory(AdHistory::class)->create([
            'designer_id'   => $room1->id,
            'type'          => AdHistory::TYPE_CLICK,
            'total_click'   => 1,
        ]);

        factory(AdHistory::class)->create([
            'designer_id'   => $room1->id,
            'type'          => AdHistory::TYPE_CLICK,
            'total_click'   => 1,
        ]);
        
        factory(AdHistory::class)->create([
            'designer_id'   => $room2->id,
            'type'          => AdHistory::TYPE_CLICK,
            'total_click'   => 1,
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json('GET', self::API_GET_FAVORITE_BY_CLICK . '?range=' . StatsLib::RangeAll . '&source=' . View::SOURCE_ADS);
        $responseObj = json_decode($response->getContent());
        $data = $responseObj->data;
        $mostFavorite = $data[0]; // most favorite index is first!
        $this->assertEquals(true, $responseObj->status);
        $this->assertEquals($room1->song_id, $mostFavorite->_id);
    }

    /** @test */
    public function get_premium_request_history_not_owner()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'is_owner' => 'false'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $response = $this->json(
            'GET',
            self::GET_HISTORY_URL
        );

        $responseJson = json_decode($response->getContent());
        $this->assertFalse($responseJson->status);
    }

    /** @test */
    public function get_premium_request_history_owner()
    {
        $user = factory(User::class)->create([
            'id' => 2,
            'is_owner' => 'true'
        ]);

        $this->actingAs($user);
        app()->user = $user;

        $this->mockPremiumRepo->shouldReceive('getHistory')
                ->with($user)
                ->andReturn([]);

        $response = $this->json(
            'GET',
            self::GET_HISTORY_URL
        );
        $responseJson = json_decode($response->getContent());
        $this->assertTrue($responseJson->status);
    }

    public function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }

}
