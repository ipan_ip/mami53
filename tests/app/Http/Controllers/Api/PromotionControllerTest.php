<?php

namespace App\Http\Controllers\Api;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PromotionControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    private const GET_PROMO_CITIES_LIST = 'garuda/filters/promo';
    
    public function testGetPromoFilters()
    {
        $response = $this->json('GET', self::GET_PROMO_CITIES_LIST);
        $response->assertStatus(200);
        $jsonResult = $response->getData();

        $this->assertEquals('Semua Kota', $jsonResult->cities->all);
        $this->assertEquals(23, count((array) $jsonResult->cities));
    }

    public function testGetPromoFiltersWithDailyType()
    {
        $response = $this->json('GET', self::GET_PROMO_CITIES_LIST.'?type=daily');
        $response->assertStatus(200);
        $jsonResult = $response->getData();

        $this->assertEquals('Semua Kota', $jsonResult->cities->all);
        $this->assertEquals(33, count((array) $jsonResult->cities));
    }
}
