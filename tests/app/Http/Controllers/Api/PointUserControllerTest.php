<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Media\Media;
use App\Entities\Level\KostLevel;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardType;
use App\Entities\Point\Point;
use App\Entities\Point\PointUser;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class PointUserControllerTest extends MamiKosTestCase
{
    
    private const URL_GET_TOTAL_POINT = 'garuda/user/point/total';
    private const URL_GET_EXPIRY = 'garuda/user/point/expiry';
    private const URL_GET_TNC = 'garuda/user/point/tnc';
    private const URL_GET_HISTORY_GUIDELINES = 'garuda/user/point/history';
    private const URL_GET_ACTIVITY_GUIDELINES = 'garuda/user/point/activity';
    private const URL_GET_SCHEME_GUIDELINES = 'garuda/user/point/scheme';

    public function testTotalPoint_PointUserExist_ReturnPoint()
    {
        $user = factory(User::class)->create();
        $expiredDate = 
        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 5,
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ]));
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 1,
            'expired_date' => $expiredDateCarbon
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 2,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 3,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => -4,
            'expired_date' => null
        ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_TOTAL_POINT, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => $pointUser->total,
            'near_expired_point' => 4,
            'near_expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);
    }

    public function testTotalPoint_PointUserNotExist_ReturnZero()
    {
        $user = factory(User::class)->create();

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_TOTAL_POINT, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => 0
        ]);
    }

    public function testTotalPoint_ForOwner_PointUserBlacklisted_ReturnNull()
    {
        $user = factory(User::class)->state('owner')->create();
        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 5,
            'is_blacklisted' => 1,
        ]));

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_TOTAL_POINT, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => null
        ]);
    }

    public function testTotalPoint_ForTenant_PointUserBlacklisted_ReturnNull()
    {
        $user = factory(User::class)->state('tenant')->create();
        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 5,
            'is_blacklisted' => 1,
        ]));

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_TOTAL_POINT, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => null
        ]);
    }

    public function testExpiry_PointUserExist_WithoutPointUsed_ReturnPoint()
    {
        $user = factory(User::class)->state('owner')->create();
        $pointOwner = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE, 
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 12
        ]);
        $pointTenant = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT, 
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 10
        ]);

        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 8,
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ]));
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 1,
            'expired_date' => $expiredDateCarbon
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 2,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 3,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_EXPIRY, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => 8,
            'expiry_in' => '12 bulan'
        ]);
        $response->assertJsonFragment([
            'expired_date' => Carbon::now()->endOfMonth()->format('Y-m-d H:i:s'),
            'total_point' => 2,
        ]);
        $response->assertJsonFragment([
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()->format('Y-m-d H:i:s'),
            'total_point' => 6,
        ]);
    }

    public function testExpiry_PointUserExist_WithPointUsed_ReturnPoint()
    {
        $user = factory(User::class)->state('owner')->create();
        $pointOwner = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE, 
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 12
        ]);
        $pointTenant = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT, 
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 10
        ]);

        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 4,
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ]));
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 1,
            'expired_date' => $expiredDateCarbon
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 2,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 3,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => -4,
            'expired_date' => null
        ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_EXPIRY, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => 4,
            'expiry_in' => '12 bulan'
        ]);
        $response->assertJsonFragment([
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()->format('Y-m-d H:i:s'),
            'total_point' => 4,
        ]);
    }

    public function testExpiry_PointUserNotExist_ReturnZero()
    {
        $user = factory(User::class)->state('owner')->create();
        $pointOwner = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE, 
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 10
        ]);
        $pointTenant = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT, 
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 8
        ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_EXPIRY, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => 0,
            'expiry_in' => '10 bulan',
            'near_expiry' => []
        ]);
    }

    public function testExpiry_PointUserBlacklisted_ReturnNull()
    {
        $user = factory(User::class)->state('owner')->create();
        $pointUser = $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 5,
            'is_blacklisted' => 1,
        ]));

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_EXPIRY, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'point' => null,
            'expiry_in' => null,
            'near_expiry' => null
        ]);
    }

    public function testTnc()
    {
        $owner = factory(User::class)->state('owner')->create();
        $tenant = factory(User::class)->state('tenant')->create();
        $pointOwner = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE, 
            'target' => Point::TARGET_OWNER,
            'tnc' => '<p>lorem owner tnc</p>'
        ]);
        $pointTenant = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT, 
            'target' => Point::TARGET_TENANT,
            'tnc' => '<p>lorem tenant tnc</p>'
        ]);

        $response = $this->setWebApiCookie()->actingAs($owner)->json('GET', self::URL_GET_TNC, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'tnc' => $pointOwner->tnc,
        ]);

        $response = $this->setWebApiCookie()->actingAs($tenant)->json('GET', self::URL_GET_TNC, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'tnc' => $pointTenant->tnc,
        ]);
    }

    public function testHistory()
    {
        $user = factory(User::class)->create();
        $user->point_user()->save(factory(PointUser::class)->make([
            'total' => 5,
            'is_blacklisted' => 0,
        ]));

        $media = factory(Media::class)->create();
        $type = factory(RewardType::class)->create([
            'key' => 'shipping_goods'
        ]);
        $reward = factory(Reward::class)->create([
            'media_id' => $media->id,
            'type_id' => $type->id,
            'redeem_value' => 25,
            'is_active' => 1,
            'is_published' => 1,
            'redeem_currency' => 'point',
        ]);
        $reward->target()->attach($user->id);
        $redeem = $reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $user->id,
            'status' => RewardRedeem::STATUS_FAILED
        ]));

        $invoice = factory(MamipayInvoice::class)->create();

        $pointActivity = factory(PointActivity::class)->create([
            'target' => 'owner'
        ]);
        $pointHistoryActivity = $user->point_history()->save(factory(PointHistory::class)->make([
            'activity_id' => $pointActivity,
            'value' => 10,
            'balance' => 15,
        ]));
        $pointHistoryAdjustment = $user->point_history()->save(factory(PointHistory::class)->make([
            'activity_id' => 0,
            'value' => 15,
            'balance' => 30,
            'notes' => 'adjustment'
        ]));
        $pointHistoryRedeemReward = $user->point_history()->save(factory(PointHistory::class)->make([
            'activity_id' => null,
            'redeem_id' => $redeem->id,
            'value' => -25,
            'balance' => 5,
        ]));
        $pointHistoryRedeemInvoice = $user->point_history()->save(factory(PointHistory::class)->make([
            'user_id' => $user->id,
            'redeemable_type' => 'room_contract',
            'redeemable_id' => $invoice->id,
            'redeem_value' => 2,
            'value' => -2,
        ]));
        $pointHistoryExpired = $user->point_history()->save(factory(PointHistory::class)->make([
            'activity_id' => null,
            'value' => -5,
            'balance' => 0,
            'notes' => 'Poin Kedaluwarsa'
        ]));

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_HISTORY_GUIDELINES, []);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'is_blacklisted' => false,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'title' => $pointHistoryActivity->activity->title,
            'value' => $pointHistoryActivity->value,
        ]);
        $response->assertJsonFragment([
            'title' => $pointHistoryAdjustment->notes,
            'value' => $pointHistoryAdjustment->value,
        ]);
        $response->assertJsonFragment([
            'title' => 'Penukaran ' . $reward->name,
            'value' => $pointHistoryRedeemReward->value,
        ]);
        $response->assertJsonFragment([
            'title' => 'Potongan MamiPoin',
            'value' => $pointHistoryRedeemInvoice->value,
        ]);
        $response->assertJsonFragment([
            'title' => 'Poin Kedaluwarsa',
            'value' => $pointHistoryExpired->value,
        ]);
    }

    public function testActivityGuidelines()
    {
        $owner = factory(User::class)->state('owner')->create();
        $tenant = factory(User::class)->state('tenant')->create();
        $pointActivityOwner = factory(PointActivity::class, 2)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);
        $pointActivityTenant = factory(PointActivity::class, 2)->create([
            'target' => 'tenant',
            'is_active' => 1
        ]);
        $pointActivityOwnerFirst = $pointActivityOwner->first();
        $pointActivityTenantFirst = $pointActivityTenant->first();

        $response = $this->setWebApiCookie()->actingAs($owner)->json('GET', self::URL_GET_ACTIVITY_GUIDELINES, ['target' => 'owner']);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'title' => $pointActivityOwnerFirst->title,
            'description' => $pointActivityOwnerFirst->description,
        ]);

        $response = $this->setWebApiCookie()->actingAs($tenant)->json('GET', self::URL_GET_ACTIVITY_GUIDELINES, ['target' => 'tenant']);
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        $response->assertJsonFragment([
            'title' => $pointActivityTenantFirst->title,
            'description' => $pointActivityTenantFirst->description,
        ]);
    }

    public function testSchemeGuidelines_WithNoSegmentTenant()
    {
        $user = factory(User::class)->state('tenant')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'tenant',
            'is_active' => 1
        ]);

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Booking Langsung', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentTenant->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => 0,
                    'received_each' => 4,
                    'limit' => 40,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);

                factory(PointSetting::class)->create([
                    'point_id' => $mamiroomsTenant->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => 0,
                    'received_each' => 5,
                    'limit' => 50,
                    'limit_type' => PointSetting::LIMIT_YEARLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, ['target' => 'tenant']);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentTenant, true, $pointActivities, [
            'received_each' => 4,
            'limit' => 40,
            'limit_type' => 'bulan',
        ]);
        $this->assertScheme($response, $mamiroomsTenant, false, $pointActivities, [
            'received_each' => 5,
            'limit' => 50,
            'limit_type' => 'tahun',
        ]);
    }

    public function testSchemeGuidelines_WithNoSegmentOwner()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        factory(RoomOwner::class)->create(
            [
                'user_id' => $user->id,
                'designer_id' => function () {
                    return factory(Room::class)->create(
                        [
                            'room_count' => 10,
                            'is_booking' => true
                        ]
                    )->id;
                },
                'status' => 'verified'
            ]
        );

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => -1,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, ['target' => 'owner']);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, true, $pointActivities, [
            'received_each' => 4,
            'limit' => 40,
            'limit_type' => '',
        ]);
    }

    public function testSchemeGuidelines_WithoutSettings_OwnerDoesntHaveActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, true, $pointActivities);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities);
    }

    public function testSchemeGuidelines_WithoutSettings_OwnerHasActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities);
        $this->assertScheme($response, $pointSegment2, true, $pointActivities);
    }

    public function testSchemeGuidelines_WithoutSettings_OwnerHasActivePackageAndNonGoldplus()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $kostLevels[0]->key = KostLevel::GP_LEVEL_KEY_PREFIX.'1';
        $kostLevels[0]->save();

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);

            $kostLevels[$i]->order = 1 + $i;
            $kostLevels[$i]->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostLevels[0]->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostLevels[1]->id, 'target' => Point::TARGET_OWNER ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities);
    }

    public function testSchemeGuidelines_WithoutSettings_OwnerHasActivePackageWhichHasntMappedToPointSegment()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities);
    }

    public function testSchemeGuidelines_WithInactiveSettings_OwnerDoesntHaveActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, true, $pointActivities);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities);
    }

    public function testSchemeGuidelines_WithInactiveSettings_OwnerHasActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities);
        $this->assertScheme($response, $pointSegment2, true, $pointActivities);
    }

    public function testSchemeGuidelines_WithInactiveSettings_OwnerHasActivePackageAndNonGoldplus()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $kostLevels[0]->key = KostLevel::GP_LEVEL_KEY_PREFIX.'1';
        $kostLevels[0]->save();

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);

            $kostLevels[$i]->order = 1 + $i;
            $kostLevels[$i]->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostLevels[0]->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostLevels[1]->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities);
    }

    public function testSchemeGuidelines_WithInactiveSettings_OwnerHasActivePackageWhichHasntMappedToPointSegment()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);
        
        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities);
    }

    public function testSchemeGuidelines_WithActiveSetting_OwnerDoesntHaveActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);

        $this->assertScheme($response, $nonSegmentOwner, true, $pointActivities, [
            'received_each' => 4,
            'limit' => 12,
            'limit_type' => 'bulan',
        ]);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities, [
            'received_each' => 8,
            'limit' => 24,
            'limit_type' => 'minggu',
        ]);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities, [
            'received_each' => 10,
            'limit' => 48,
            'limit_type' => 'minggu',
        ]);
    }

    public function testSchemeGuidelines_WithActiveSetting_OwnerHasActivePackage()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostGpLevel2->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities, [
            'received_each' => 4,
            'limit' => 12,
            'limit_type' => 'bulan',
        ]);
        $this->assertScheme($response, $pointSegment1, false, $pointActivities, [
            'received_each' => 8,
            'limit' => 24,
            'limit_type' => 'minggu',
        ]);
        $this->assertScheme($response, $pointSegment2, true, $pointActivities, [
            'received_each' => 10,
            'limit' => 48,
            'limit_type' => 'minggu',
        ]);
    }

    public function testSchemeGuidelines_WithActiveSetting_OwnerHasActivePackageAndNonGoldplus()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $kostLevels[0]->key = KostLevel::GP_LEVEL_KEY_PREFIX.'1';
        $kostLevels[0]->save();

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);

            $kostLevels[$i]->order = 1 + $i;
            $kostLevels[$i]->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostLevels[0]->id, 'target' => Point::TARGET_OWNER ]);
        $pointSegment2 = factory(Point::class)->create([ 'type' => 'goldplus_2', 'kost_level_id' => $kostLevels[1]->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment2->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 10,
                    'limit' => 48,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities, [
            'received_each' => 4,
            'limit' => 12,
            'limit_type' => 'bulan',
        ]);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities, [
            'received_each' => 8,
            'limit' => 24,
            'limit_type' => 'minggu',
        ]);
        $this->assertScheme($response, $pointSegment2, false, $pointActivities, [
            'received_each' => 10,
            'limit' => 48,
            'limit_type' => 'minggu',
        ]);
    }

    public function testSchemeGuidelines_WithActiveSetting_OwnerHasActivePackageWhichHasntMappedToPointSegment()
    {
        $user = factory(User::class)->state('owner')->create();
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);

        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);
            
            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = 1 + $i;
            ${'kostGpLevel'.($i+1)}->save();
        }

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'title' => 'Booking Langsung', 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'title' => 'Regular', 'target' => Point::TARGET_TENANT ]);
        $mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'title' => 'Mamirooms', 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment1 = factory(Point::class)->create([ 'type' => 'goldplus_1', 'kost_level_id' => $kostGpLevel1->id, 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment1->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'is_active' => true
                ]);
            }
        }

        $response = $this->setWebApiCookie()->actingAs($user)->json('GET', self::URL_GET_SCHEME_GUIDELINES, []);

        $response->assertOk();
        $response->assertJson([
            'status' => true,
            'data' => []
        ]);
        
        $this->assertScheme($response, $nonSegmentOwner, false, $pointActivities, [
            'received_each' => 4,
            'limit' => 12,
            'limit_type' => 'bulan',
        ]);
        $this->assertScheme($response, $pointSegment1, true, $pointActivities, [
            'received_each' => 8,
            'limit' => 24,
            'limit_type' => 'minggu',
        ]);
    }

    private function assertScheme($response, $segment, $isActive, $activities, $setting = null)
    {
        $response->assertJsonFragment([
            'type' => $segment->type,
            'title' => $segment->title,
            'is_active' => $isActive,
            'activities' => $activities->map(function ($activity) use ($setting) {
                return [
                    'title' => $activity->title,
                    'description' => $activity->description,
                    'point_details' => !is_null($setting) ? [
                        'min_point' => $setting['received_each'],
                        'max_point' => $setting['limit'],
                        'limit_type' => $setting['limit_type']
                    ] : NULL,
                ];
            })->toArray()
        ]);
    }
}
