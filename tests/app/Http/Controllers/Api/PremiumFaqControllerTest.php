<?php
namespace App\Http\Web;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;
use Mockery;

class PremiumFaqControllerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    private const GET_PREMIUM_FAQ_URL = 'api/v1/premium/faq';
    private $mockForAppDevice;

    public function setUp() : void
    {
        parent::setUp();

        // setup to mock repository
        $this->mockAlternatively('App\Repositories\PremiumFaqRepository');

        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Controllers\Api')
            ->setName("app")
            ->setFunction( function () {
                return (object) ['user' => null];
            });

        $this->mockForAppDevice = $builder->build();
        $this->mockForAppDevice->enable();
    }
    
    /** @test */
    public function get_premium_faq_success()
    {
        $limit = 20;
        $this->mock->shouldReceive('getPremiumFaq')
                ->with($limit)
                ->andReturn([]);

        $response = $this->json(
            'GET', 
            self::GET_PREMIUM_FAQ_URL
        );
        
        $response->assertStatus(200);
        $response->assertJsonFragment([
            "status" => true,
        ]);
    }

    public function tearDown() : void
    {
        Mockery::close();
        $this->mockForAppDevice->disable();
        parent::tearDown();
    }
}