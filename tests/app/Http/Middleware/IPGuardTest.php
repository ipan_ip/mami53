<?php

namespace App\Http\Middleware;

use App\Libraries\RequestHelper;
use App\Test\MamiKosTestCase;
use Config;
use Exception;
use Illuminate\Http\Request;
use Mockery;

class IPGuardTest extends MamiKosTestCase
{
    public function testIPAllowed()
    {
        Config::set('ip.vpn', ['127.0.0.1']);
        $requestHelper = Mockery::mock('alias:App\Libraries\RequestHelper');
        $requestHelper->shouldReceive('getClientIpRegardingCF')->andReturn('127.0.0.1');
        
        $middleware = new IPGuard();
        $result = $middleware->handle(
            Request::create('/', 'GET'),
            function ($request) {
                return 'Allowed';
            }
        );

        $this->assertEquals('Allowed', $result);
    }

    public function testIPNotAllowed()
    {
        Config::set('ip.vpn', ['127.0.0.1']);
        $requestHelper = Mockery::mock('alias:App\Libraries\RequestHelper');
        $requestHelper->shouldReceive('getClientIpRegardingCF')->andReturn('127.0.0.2');

        $this->expectException(Exception::class);
        
        $middleware = new IPGuard();
        $result = $middleware->handle(
            Request::create('/', 'GET'),
            function ($request) {
                return 'Allowed';
            }
        );
    }
}
