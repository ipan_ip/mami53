<?php

namespace App\Http\Middleware;

use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mockery;

class WebviewAuthMiddlewareTest extends MamiKosTestCase
{
    //private $user;

    // public function testHandleExceptionRedirectedToLogin()
    // {
    //     Auth::shouldReceive('check')->andThrow(new Exception("the Exception"));
    //     $middleware = new WebviewAuthMiddleware();

    //     $result = $middleware->handle(Request::create('/', 'POST'), function ($request) {
    //         return $request;
    //     });

    //     $this->assertInstanceOf(RedirectResponse::class, $result);
    // }

    public function testHandleUserLoggedInSessionSuccess()
    {
        Auth::shouldReceive('check')->andReturn(true);

        $middleware = new WebviewAuthMiddleware();

        $result = $middleware->handle(Request::create('/', 'POST'), function ($request) {
            return 'correctMundo';
        });

        $this->assertEquals('correctMundo', $result);
    }

    // public function testHandleUserLoggedWithToken()
    // {
    //     $this->markTestSkipped("Skipped due there is object middleware injected");
    //     $user = factory(User::class)->state('owner')->create();

    //     Auth::shouldReceive('check')->andReturn(false);
    //     Auth::shouldReceive('loginUsingId')->andReturn(true);
    //     app()->user = $user;
    //     $middleware = new WebviewAuthMiddleware();

    //     $result = $middleware->handle(Request::create('/', 'POST'), function ($request) {
    //         return 'correctMundo';
    //     });

    //     $this->assertEquals('correctMundo', $result);

    //     Mockery::close();
    // }
}
