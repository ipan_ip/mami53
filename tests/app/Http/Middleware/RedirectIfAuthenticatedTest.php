<?php

namespace App\Http\Middleware;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RedirectIfAuthenticatedTest extends MamiKosTestCase
{
    public function testRedirectedWhenLoggedIn()
    {
        $user = factory(User::class)->state('owner')->create();
        $this->actingAs($user, 'web');

        $middleware = new RedirectIfAuthenticated();

        $result = $middleware->handle(
            Request::create('/ownerpage', 'POST'),
            function ($request) {
                return 'Accessing Controller';
            },
            'web'
        );

        $this->assertInstanceOf(RedirectResponse::class, $result);
    }

    public function testNotRedirectedWhenNotLoggedIn()
    {
        $middleware = new RedirectIfAuthenticated();

        $result = $middleware->handle(
            Request::create('/ownerpage', 'POST'),
            function ($request) {
                return 'Accessing Controller';
            },
            'web'
        );

        $this->assertEquals('Accessing Controller', $result);
    }
}
