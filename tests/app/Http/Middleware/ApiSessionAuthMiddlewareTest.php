<?php

namespace App\Http\Middleware;

use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiSessionAuthMiddlewareTest extends MamiKosTestCase
{
    public function testContinuedWhenLoggedIn()
    {
        $user = factory(User::class)->state('owner')->make();
        Auth::shouldReceive('user')->andReturn($user);
        
        $middleware = new ApiSessionAuthMiddleware();
        $result = $middleware->handle(
            Request::create('/ownerpage', 'POST'),
            function ($request) {
                return 'Accessing Controller';
            },
            'web'
        );

        $this->assertEquals('Accessing Controller', $result);
    }

    public function testRespondedWithMessageWhenNotLoggedIn()
    {
        Auth::shouldReceive('user')->andReturn(null);
        $middleware = new ApiSessionAuthMiddleware();

        $response = $middleware->handle(
            Request::create('/ownerpage', 'POST'),
            function ($request) {
                return 'Accessing Controller';
            },
            'web'
        );

        $this->assertFalse($response->getData()->status);
    }
}
