<?php


namespace app\Http\Helpers;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Http\Helpers\RentCountHelper;
use App\Test\MamiKosTestCase;

class RentCountHelperTest extends MamiKosTestCase
{
    
    public function testGetRentCountWeekly()
    {
        $rentCountWeekly = RentCountHelper::getRentCountWeekly();
        $this->assertIsArray($rentCountWeekly);
        $this->assertEquals("1 Minggu", $rentCountWeekly[0]);
        $this->assertEquals("2 Minggu", $rentCountWeekly[1]);
        $this->assertEquals("3 Minggu", $rentCountWeekly[2]);
        $this->assertEquals("4 Minggu", $rentCountWeekly[3]);
    }

    public function testGetRentCountWeeklyByKeyIsNotEmpty()
    {
        $rentCountWeekly = RentCountHelper::getRentCountWeeklyByKey(2);
        $this->assertEquals("2 Minggu", $rentCountWeekly);
    }

    public function testGetRentCountWeeklyByKeyIsEmpty()
    {
        $rentCountWeekly = RentCountHelper::getRentCountWeeklyByKey(13);
        $this->assertEquals("", $rentCountWeekly);
        $this->assertEmpty($rentCountWeekly);
    }

    public function testGetRentCountMonthly()
    {
        $rentCountMonthly = RentCountHelper::getRentCountMonthly();
        $this->assertIsArray($rentCountMonthly);
        $this->assertEquals("1 Bulan", $rentCountMonthly[0]);
        $this->assertEquals("2 Bulan", $rentCountMonthly[1]);
        $this->assertEquals("3 Bulan", $rentCountMonthly[2]);
        $this->assertEquals("4 Bulan", $rentCountMonthly[3]);
    }

    public function testGetRentCountMonthlyByKeyIsNotEmpty()
    {
        $rentCountMonthly = RentCountHelper::getRentCountMonthlyByKey(2);
        $this->assertEquals("2 Bulan", $rentCountMonthly);
    }

    public function testGetRentCountMonthlyByKeyIsEmpty()
    {
        $rentCountMonthly = RentCountHelper::getRentCountMonthlyByKey(13);
        $this->assertEquals("", $rentCountMonthly);
        $this->assertEmpty($rentCountMonthly);
    }

    public function testGetRentCountQuarterly()
    {
        $rentCountQuarterly = RentCountHelper::getRentCountQuarterly();
        $this->assertIsArray($rentCountQuarterly);
        $this->assertEquals("3 Bulan", $rentCountQuarterly[0]);
        $this->assertEquals("6 Bulan", $rentCountQuarterly[1]);
        $this->assertEquals("9 Bulan", $rentCountQuarterly[2]);
        $this->assertEquals("12 Bulan", $rentCountQuarterly[3]);
    }

    public function testGetRentCountQuarterlyByKeyIsNotEmpty()
    {
        $rentCountQuarterly = RentCountHelper::getRentCountQuarterlyByKey(2);
        $this->assertEquals("6 Bulan", $rentCountQuarterly);
    }

    public function testGetRentCountQuarterlyByKeyIsEmpty()
    {
        $rentCountQuarterly = RentCountHelper::getRentCountQuarterlyByKey(5);
        $this->assertEquals("", $rentCountQuarterly);
        $this->assertEmpty($rentCountQuarterly);
    }

    public function testGetRentCountSemiannually()
    {
        $rentCountSemiannually = RentCountHelper::getRentCountSemiannually();
        $this->assertIsArray($rentCountSemiannually);
        $this->assertEquals("6 Bulan", $rentCountSemiannually[0]);
        $this->assertEquals("12 Bulan", $rentCountSemiannually[1]);
    }

    public function testGetRentCountSemiannuallyByKeyIsNotEmpty()
    {
        $rentCountSemiannually = RentCountHelper::getRentCountSemiannuallyByKey(1);
        $this->assertEquals("6 Bulan", $rentCountSemiannually);
    }

    public function testGetRentCountSemiannuallyByKeyIsEmpty()
    {
        $rentCountSemiannually = RentCountHelper::getRentCountSemiannuallyByKey(3);
        $this->assertEquals("", $rentCountSemiannually);
        $this->assertEmpty($rentCountSemiannually);
    }

    public function testGetRentCountYearly()
    {
        $rentCountYearly = RentCountHelper::getRentCountYearly();
        $this->assertIsArray($rentCountYearly);
        $this->assertEquals("1 Tahun", $rentCountYearly[0]);
        $this->assertEquals("2 Tahun", $rentCountYearly[1]);
        $this->assertEquals("3 Tahun", $rentCountYearly[2]);
    }

    public function testGetRentCountYearlyByKeyIsNotEmpty()
    {
        $rentCountYearly = RentCountHelper::getRentCountYearlyByKey(1);
        $this->assertEquals("1 Tahun", $rentCountYearly);
    }

    public function testGetRentCountYearlyByKeyIsEmpty()
    {
        $rentCountYearly = RentCountHelper::getRentCountYearlyByKey(4);
        $this->assertEquals("", $rentCountYearly);
        $this->assertEmpty($rentCountYearly);
    }

    public function testGetFormatRentCount()
    {
        $formatRentCount  = RentCountHelper::getFormatRentCount(4, BookingUser::SEMIANNUALLY_TYPE);
        $this->assertEquals("24 Bulan", $formatRentCount);
        $this->assertNotEmpty($formatRentCount);
        $this->assertNotNull($formatRentCount);
    }

    public function testGetAliasesRentCount()
    {
        $room = factory(Room::class)->create([
            'price_weekly' => 67000000,
            'price_monthly' => 12345000,
            'price_yearly' => 324345345000,
        ]);
        $price = $room->price();

        $aliasesRentCount = RentCountHelper::getAliasesRentCount($price);
        $this->assertIsArray($aliasesRentCount);
        $this->assertIsArray($aliasesRentCount[0]);
        $this->assertIsArray($aliasesRentCount[1]);
        $this->assertIsArray($aliasesRentCount[2]);
        $this->assertEquals('weekly', $aliasesRentCount[0]['format']);
        $this->assertEquals('Mingguan', $aliasesRentCount[0]['label']);
        $this->assertEquals('monthly', $aliasesRentCount[1]['format']);
        $this->assertEquals('Bulanan', $aliasesRentCount[1]['label']);
        $this->assertEquals('yearly', $aliasesRentCount[2]['format']);
        $this->assertEquals('Tahunan', $aliasesRentCount[2]['label']);
    }

    public function testGetFormatRentTypeMamipay()
    {
        $formatRentTypeMamiPAY = RentCountHelper::getFormatRentTypeMamipay(BookingUser::QUARTERLY_TYPE);
        $this->assertNotEmpty($formatRentTypeMamiPAY);
        $this->assertEquals(MamipayContract::UNIT_3MONTH, $formatRentTypeMamiPAY);
    }

    public function testGetFormatPrice()
    {
        $formatPrice = RentCountHelper::getFormatPrice(BookingUser::SEMIANNUALLY_TYPE);
        $this->assertNotEmpty($formatPrice);
        $this->assertEquals(5, $formatPrice);
    }

    public function testGetMinimumDuration()
    {
        // value based on month
        // example: 7 => 7 Month / 7 Bulan
        $minDuration = 7;
        $result = RentCountHelper::getMinimumDuration($minDuration, BookingUser::SEMIANNUALLY_TYPE);
        $this->assertIsArray($result);
        $this->assertEquals("12 Bulan", $result[0]);
    }

    public function testGetRentCount()
    {
        $rentCount = RentCountHelper::getRentCount(BookingUser::SEMIANNUALLY_TYPE);
        $this->assertIsArray($rentCount);
        $this->assertEquals("6 Bulan", $rentCount[0]);
        $this->assertEquals("12 Bulan", $rentCount[1]);
    }
}