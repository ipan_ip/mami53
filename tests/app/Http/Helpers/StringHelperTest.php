<?php

namespace app\Http\Helpers;

use App\Http\Helpers\StringHelper;
use App\Test\MamiKosTestCase;

class StringHelperTest extends MamiKosTestCase
{
    public function testExplodeAndCastToIntNull()
    {
        $this->assertEquals([], StringHelper::explodeAndCastToInt(',', null));
    }

    public function testExplodeAndCastToIntEmpty()
    {
        $this->assertEquals([], StringHelper::explodeAndCastToInt(',', ''));
    }

    public function testExplodeAndCastToIntWhitespacesOnly()
    {
        $this->assertEquals([], StringHelper::explodeAndCastToInt(',', '   '));
    }

    public function testExplodeAndCastToInt()
    {
        $this->assertEquals([1,2,3], StringHelper::explodeAndCastToInt(',', '1,2,3'));
    }

    public function testExplodeAndCastToIntContainWhitespace()
    {
        $this->assertEquals([1,2,3], StringHelper::explodeAndCastToInt(',', '  1, 2, 3 '));
    }

    public function testExplodeAndCastToIntEmptyDelimiter()
    {
        $this->assertEquals([], StringHelper::explodeAndCastToInt("", str_random()));
    }

    public function testExplodeAndCastToIntWithZeroValueWillBeExcluded()
    {
        // "0" will evaluated as empty using empty("0"). Hence will be excluded.
        $this->assertEquals([], StringHelper::explodeAndCastToInt(",", "0"));
    }
}
