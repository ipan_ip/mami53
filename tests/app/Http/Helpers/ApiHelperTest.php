<?php

namespace App\Http\Helpers;

use App\Http\Helpers\ApiHelper;
use App\Test\MamiKosTestCase;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use phpmock\MockBuilder;

class ApiHelperTest extends MamiKosTestCase
{
    public function testRemoveEmojiWithNullInput() {
        $result = ApiHelper::removeEmoji(null);

        $this->assertSame($result, '');
    }

    public function testRemoveEmojiWithEmptyInput() {
        $result = ApiHelper::removeEmoji('');

        $this->assertSame($result, '');
    }

    public function testRemoveEmojiOnNormalSlug() {
        $slug = 'kost-bandar-lampung-kost-campur-eksklusif-kost-lumungga-teluk-betung-utara-tipe-b-bandar-lampung';
        $result = ApiHelper::removeEmoji($slug);

        $this->assertSame($result, $slug);
    }

    public function testRemoveEmojiFromSlug() {
        $slug = 'apartemen-maple-park/apartemen-maple-park-tower-a-lantai-3-unit-mppa001-tipe-studio-25-jakarta-utara-1room-studio-1🤤🤤🤤';
        $correctUrl = 'apartemen-maple-park/apartemen-maple-park-tower-a-lantai-3-unit-mppa001-tipe-studio-25-jakarta-utara-1room-studio-1';
        $result = ApiHelper::removeEmoji($slug);

        $this->assertSame($correctUrl, $result);
    }
    
    public function testCleanHttpCheckingReturnEmptyStringWhenWhiteSpace()
    {
        $this->assertEquals('', ApiHelper::cleanHttpChecking(''));
        $this->assertEquals('', ApiHelper::cleanHttpChecking(' '));
        $this->assertEquals('', ApiHelper::cleanHttpChecking("\n"));
        $this->assertEquals('', ApiHelper::cleanHttpChecking('    '));
        $this->assertEquals('', ApiHelper::cleanHttpChecking(" \n "));
    }

    public function testCleanHttpCheckingReturnInputWhenHaveHttp()
    {
        $this->assertEquals(
            'http://mamikos.com/', 
            ApiHelper::cleanHttpChecking("http://mamikos.com/")
        );

        $this->assertEquals(
            'http://mamikos.com/kost/kost-jakarta-murah', 
            ApiHelper::cleanHttpChecking("http://mamikos.com/kost/kost-jakarta-murah")
        );
    }
    
    public function testCleanHttpCheckingReturnInputWhenHaveHttps()
    {
        $this->assertEquals(
            'https://mamikos.com/', 
            ApiHelper::cleanHttpChecking("https://mamikos.com/")
        );

        $this->assertEquals(
            'https://mamikos.com/kost/kost-jakarta-murah', 
            ApiHelper::cleanHttpChecking("https://mamikos.com/kost/kost-jakarta-murah")
        );
    }

    public function testCleanHttpCheckingReturnWithHttpWhenPath()
    {
        $this->assertEquals(
            'http://mamikos.com', 
            ApiHelper::cleanHttpChecking("mamikos.com")
        );

        $this->assertEquals(
            'http://mamikos.com/kost/kost-jakarta-murah', 
            ApiHelper::cleanHttpChecking("mamikos.com/kost/kost-jakarta-murah")
        );
    }

    public function testRandomStringBasic()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
                ->setName("mt_rand")
                ->setFunction( function () { return 123123; });
        
        $mock = $builder->build();
        $mock->enable(); // mock mt_rand()

        $this->assertEquals(ApiHelper::random_string('basic'), 123123);
        $mock->disable();
    }

    public function testRandomStringAlphaNumeric()
    {
        $this->assertTrue( !preg_match( "/[^A-Za-z]+/", ApiHelper::random_string('alpha')) );
        $this->assertTrue( !preg_match( "/[^A-Za-z0-9]+/", ApiHelper::random_string('alnum')) );
        $this->assertTrue( !preg_match( "/[^a-z0-9]+/", ApiHelper::random_string('lowalnum')) );
        $this->assertTrue( !preg_match( "/[^0-9]+/", ApiHelper::random_string('numeric')) );
        $this->assertTrue( !preg_match( "/[^1-9]+/", ApiHelper::random_string('nozero')) );
    }

    public function testRandomStringUniqueMd25()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
                ->setName("md5")
                ->setFunction( function () { return "92049debbe566ca5782a3045cf300a3c"; });
        
        $mock = $builder->build();
        $mock->enable(); // mock md5()

        $this->assertEquals(ApiHelper::random_string('unique'), "92049debbe566ca5782a3045cf300a3c");
        $this->assertEquals(ApiHelper::random_string('md5'), "92049debbe566ca5782a3045cf300a3c");
        
        $mock->disable();
    }
    
    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testTraceThrowError()
    {
        $msg = str_random(15);
        $e = new \Exception($msg);

        $expected   = ApiHelper::traceThrowError($e);
        $result = '. Error: ' . $e->getMessage() . ', File: ' . $e->getFile() . ', Line: ' . $e->getLine();

        $this->assertEquals($result, $expected);
    }

    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testGenerateTokenNotEmpty()
    {
        $this->assertNotEmpty(ApiHelper::generateToken(str_random(10)));
    }


    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testExceptionHandler()
    {
        $exceptionMsg   = str_random(17);
        $httpCode      = 200;

        $exception = new \Exception($exceptionMsg);
        $result   = json_decode(
            ApiHelper::exceptionHandler($exception, $httpCode, true, true)->content(),
            true
        );
        
        $expected = [
            'status'    => false,
            'meta'      => [
                'response_code' => $httpCode,
                'code'          => true,
                'severity'      => 'Error',
                'message'       => true,
            ]
        ];

        $this->assertEquals($result, $expected);
    }

     /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testResponseError()
    {
        $message = str_random(11);
        $result = ApiHelper::responseError($message, 
            false, 
            200,
            false, 
            false, 
            false, 
            false
        );
        $resultArr = json_decode($result->content(), true);

        $this->assertEquals($resultArr, [
            'status'    => false,
            'meta'      => [
                'response_code' => 200,
                'code'          => 1,
                'severity'      => 'Error',
                'message'       => $message,
            ]
        ]);
    }

    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testResponseData()
    {
        $testValue = str_random(11);
        $data = [
            'test' => $testValue,
        ];
        $result = ApiHelper::responseData(
            $data, 
            false, 
            true, 
            200,
            null, 
            null, 
            null, 
            false
        );
        $resultArr = json_decode($result->content(), true);
        $this->assertEquals($resultArr, [
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'test'  => $testValue,
        ]);
    }

    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testResponseDataWithCountDataIsTrue()
    {
        $testValue = str_random(11);
        $data = [
            'test' => $testValue,
            'stories'   => range(1, 10),
        ];
        $result = ApiHelper::responseData(
            $data, 
            true, 
            true, 
            200,
            null, 
            null, 
            null, 
            false
        );
        $resultArr = json_decode($result->content(), true);

        $this->assertEquals($resultArr, [
            'status'    => true,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
            ],
            'test'  => $testValue,
            'counts' => count(range(1, 10)),
            'stories'   => range(1, 10)
        ]);
    }

    /**
     * @group UG
     * @group UG-730
     * @group App/Http/Helpers/ApiHelper
     */
    public function testValidationError()
    {
        $errorStr = str_random(23);
        $result = ApiHelper::validationError($errorStr);
        $resultArr = json_decode($result->content(), true);

        $this->assertEquals($resultArr, [
            'status'    => false,
            'meta'      => [
                'response_code' => 200,
                'code'          => 200,
                'severity'      => 'OK',
                'message'       => 'A validation error occurred',
            ],
            'message'   => $errorStr,
        ]);
    }

    /**
     * @group UG
     * @group UG-1311
     * @group App/Http/Helpers/ApiHelper
     */
    public function testForcePagiationShouldReturnValid()
    {
        $limit  = mt_rand(1, 1000);
        $offset = mt_rand(1, 1000);
        $order  = array_rand(['asc', 'desc']);
        
        $expectedArray  = [
            'limit'     => $limit,
            'offset'    => $offset,
            'order'     => $order
        ];

        $result = ApiHelper::forcePagination($limit, $offset, $order);
        $this->assertEquals($expectedArray, $result);
    }

    /**
     * @group UG
     * @group UG-1313
     * @group App/Http/Helpers/ApiHelper
     */
    public function testSanitizeCityOrDistrictName()
    {
        $this->assertEquals("Malang,  Sukun", ApiHelper::sanitizeCityOrDistrictName("Malang, Kecamatan Sukun"));
    }

    /**
     * @group UG
     * @group UG-1313
     * @group App/Http/Helpers/ApiHelper
     */
    public function testGetDummyImageList()
    {
        $dummyImageList = ApiHelper::getDummyImageList();
        $this->assertArrayHasKey('real', $dummyImageList);
        $this->assertArrayHasKey('small', $dummyImageList);
        $this->assertArrayHasKey('medium', $dummyImageList);
        $this->assertArrayHasKey('large', $dummyImageList);
    }

    /**
     * @group UG
     * @group UG-1313
     * @group App/Http/Helpers/ApiHelper
     */
    public function testGetDummyImageUser()
    {
        $dummyImageList = ApiHelper::getDummyImageUser();
        $this->assertArrayHasKey('real', $dummyImageList);
        $this->assertArrayHasKey('small', $dummyImageList);
        $this->assertArrayHasKey('medium', $dummyImageList);
        $this->assertArrayHasKey('large', $dummyImageList);
    }
}
