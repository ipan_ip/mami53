<?php

namespace App\Http\Helpers;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Lpl\Criteria;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use DB;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use LplCriteriaSeeder;

class LplScoreHelperPartnerTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $partnerLevel;
    protected $room;

    protected function setUp()
    {
        parent::setUp();
        $this->seed(LplCriteriaSeeder::class);

        $this->room = factory(Room::class)->create();

        $this->partnerLevel = factory(KostLevel::class)->create([
            'name' => 'OYO',
            'is_partner' => 1,
        ]);

        factory(KostLevelMap::class)->create([
            'kost_id' => $this->room->song_id,
            'level_id' => $this->partnerLevel->id
        ]);
    }

    protected function tearDown(): void
    {
        DB::table('lpl_criteria')->truncate();
        parent::tearDown();
    }

    /** test criteria room_available */
    public function testGetCriteriaScoreForPartnerCriteriaRoomAvailable()
    {
        $this->room->room_available = 5;
        $this->room->save();

        $criteria = Criteria::query()->where('attribute', 'has_room')->first();
        $score = LplScoreHelper::getCriteriaScoreForPartner($this->room, $criteria);
        
        $this->assertEquals(0, $score);

        // update room to unavailable
        $this->room->room_available = 0;
        $this->room->save();

        $score = LplScoreHelper::getCriteriaScoreForPartner($this->room, $criteria);

        $expected = (-1) * $criteria->score;
        $this->assertEquals($expected, $score);
    }

    /** test criteria has_photo */
    public function testGetCriteriaScoreForPartnerCriteriaHasPhoto()
    {
        $criteria = Criteria::query()->where('attribute', 'has_photo')->first();
        $score = LplScoreHelper::getCriteriaScoreForPartner($this->room, $criteria);
        
        // assert dont have photo
        $expected = (-1) * $criteria->score;
        $this->assertEquals($expected, $score);
    }
}
