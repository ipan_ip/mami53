<?php

namespace App\Http\Helpers;

use App\Entities\Api\ErrorCode;
use App\Test\MamiKosTestCase;
use Exception;
use Mockery;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class ApiErrorHelperTest extends MamiKosTestCase
{
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGenerateErrorResponseCodeNotAvailable()
    {
        $res = ApiErrorHelper::generateErrorResponse(0);

        $this->assertSame($res['issue']['code'], ErrorCode::FALLBACK_CODE);
        $this->assertSame($res['issue']['message'], ErrorCode::MESSAGE_MAP[ErrorCode::FALLBACK_CODE]);
    }

    public function testGenerateErrorResponseCodeAvailable()
    {
        $res = ApiErrorHelper::generateErrorResponse(ErrorCode::SINGGAHSINI_REGISTRATION);

        $this->assertSame($res['issue']['code'], ErrorCode::SINGGAHSINI_REGISTRATION);
    }

    public function testGenerateErrorResponseErrorMessageAvailable()
    {
        $res = ApiErrorHelper::generateErrorResponse(ErrorCode::SINGGAHSINI_REGISTRATION);

        $this->assertSame($res['issue']['code'], ErrorCode::SINGGAHSINI_REGISTRATION);
        $this->assertSame($res['issue']['message'], ErrorCode::MESSAGE_MAP[ErrorCode::SINGGAHSINI_REGISTRATION]);
    }
}