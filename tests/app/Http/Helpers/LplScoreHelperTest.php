<?php

namespace App\Http\Helpers;

use App;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Lpl\Criteria;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use LplCriteriaSeeder;

class LplScoreHelperTest extends MamiKosTestCase
{
    use WithoutEvents;
    use WithoutMiddleware;

    private const UPLIFTED_ID_PRODUCTION = 201473;
    private const UPLIFTED_ID_2_PRODUCTION = 177726;
    private const UPLIFTED_ID_STAGING = 1000009248;
    private const NON_UPLIFTED_ID = 177725;
    private const FULL_SCORE = 4100;

    protected function setUp()
    {
        parent::setUp();

        $now = Carbon::now();

        factory(Criteria::class)->create(
            [
                'name' => 'Non-hostile Owner',
                'description' => 'Owner is NOT a competitor',
                'attribute' => 'is_not_hostile',
                'order' => 12,
                'score' => 2048,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Photo Availability',
                'description' => 'Kos has available photo',
                'attribute' => 'has_photo',
                'order' => 11,
                'score' => 1024,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Room Availability',
                'description' => 'Kos has available room',
                'attribute' => 'has_room',
                'order' => 10,
                'score' => 512,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Promo Ngebut',
                'description' => 'Kos is included in "Promo Ngebut" program',
                'attribute' => 'is_flash_sale',
                'order' => 9,
                'score' => 256,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Mamiroom',
                'description' => 'Kos is a Mamiroom typed',
                'attribute' => 'is_mamiroom',
                'order' => 8,
                'score' => 128,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Gold Plus 4',
                'description' => 'Kos has a GP 4 level',
                'attribute' => 'is_gp_4',
                'order' => 8,
                'score' => 128,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Gold Plus 3',
                'description' => 'Kos has a GP 3 level',
                'attribute' => 'is_gp_3',
                'order' => 7,
                'score' => 64,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Gold Plus 2',
                'description' => 'Kos has a GP 2 level',
                'attribute' => 'is_gp_2',
                'order' => 6,
                'score' => 32,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Premium: Active Saldo',
                'description' => 'Kos is a premium typed with allocated saldo',
                'attribute' => 'is_premium_saldo_on',
                'order' => 5,
                'score' => 16,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Gold Plus 1',
                'description' => 'Kos has a GP 1 level',
                'attribute' => 'is_gp_1',
                'order' => 4,
                'score' => 8,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Kos BBK',
                'description' => 'Kos has Booking capability',
                'attribute' => 'is_booking',
                'order' => 3,
                'score' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Money-back Guarantee',
                'description' => 'Kos has a Money-back guarantee',
                'attribute' => 'is_guaranteed',
                'order' => 2,
                'score' => 2,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'name' => 'Premium',
                'description' => 'Kos is a premium typed',
                'attribute' => 'is_premium',
                'order' => 1,
                'score' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(KostLevel::class)->states('regular')->create();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRecalculateRoomScoreWithNewScore()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'sort_score' => 128
                ]
            );

        LplScoreHelper::recalculateRoomScore($room);

        $this->assertEquals(2692, Room::orderBy('id', 'desc')->first()->sort_score);
    }

    public function testRecalculateRoomScoreWithSameNewScore()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'sort_score' => 2692
                ]
            );

        LplScoreHelper::recalculateRoomScore($room);

        $this->assertEquals($room->sort_score, Room::orderBy('id', 'desc')->first()->sort_score);
    }

    public function testGetScore()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $response = LplScoreHelper::getScore($room);
        $this->assertEquals(2692, $response);
    }

    public function testGetScoreReturnFullScore()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'id' => self::UPLIFTED_ID_PRODUCTION
                ]
            );

        $response = LplScoreHelper::getScore($room);
        $this->assertEquals(self::FULL_SCORE, $response);
    }

    public function testGetScoreReturnFullScoreVersion2()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'id' => self::UPLIFTED_ID_2_PRODUCTION
                ]
            );

        $response = LplScoreHelper::getScore($room);
        $this->assertEquals(self::FULL_SCORE, $response);
    }

    public function testGetScoreReturnNullScore()
    {
        DB::table('lpl_criteria')->truncate();

        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $response = LplScoreHelper::getScore($room);
        $this->assertEquals(0, $response);
    }

    public function testGetAvailableCriteria()
    {
        $response = LplScoreHelper::getAvailableCriteria();

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals(13, $response->count());
    }

    public function testIsEligibleForUpliftingInProduction()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $room = factory(Room::class)
            ->create(
                [
                    'id' => self::UPLIFTED_ID_PRODUCTION
                ]
            );

        $this->assertTrue(LplScoreHelper::isEligibleForUplifting($room));
    }

    public function testIsEligibleForUpliftingInStaging()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'staging';
            }
        );

        $room = factory(Room::class)
            ->create(
                [
                    'id' => self::UPLIFTED_ID_STAGING
                ]
            );

        $this->assertTrue(LplScoreHelper::isEligibleForUplifting($room));
    }

    public function testIsEligibleForUpliftingReturnFalse()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'room_available' => 0
                ]
            );
        $this->assertFalse(LplScoreHelper::isEligibleForUplifting($room));

        $room = factory(Room::class)
            ->create(
                [
                    'id' => self::NON_UPLIFTED_ID
                ]
            );
        $this->assertFalse(LplScoreHelper::isEligibleForUplifting($room));
    }

    public function testGetCriteriaForTracking()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $this->assertEquals(
            'AVAIL_ROOM | BBK | MAMIROOMS | NOT_HOSTILE',
            LplScoreHelper::getCriteriaForTracking($room)
        );
    }

    public function testGetCriteriaForTrackingReturnEmpty()
    {
        DB::table('lpl_criteria')->truncate();

        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $this->assertEmpty(LplScoreHelper::getCriteriaForTracking($room));
    }

    public function testGetFullScore()
    {
        $this->assertEquals(self::FULL_SCORE, LplScoreHelper::getFullScore());
    }

    public function testIsScoreShownForTesting()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'staging';
            }
        );

        $this->assertTrue(LplScoreHelper::isScoreShownForTesting());
    }

    public function testIsScoreShownForTestingReturnFalse()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $this->assertFalse(LplScoreHelper::isScoreShownForTesting());
    }

    public function testIsEligibleForVersion2()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'staging';
            }
        );

        $this->assertTrue(LplScoreHelper::isEligibleForVersion2());
    }

    public function testIsEligibleForVersion2WithProductionEnv()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $dateMock = Carbon::parse('2020-08-13 00:00:00');
        Carbon::setTestNow($dateMock);

        $this->assertTrue(LplScoreHelper::isEligibleForVersion2());
    }

    public function testIsEligibleForVersion2WithProductionEnvReturnFalse()
    {
        /* Faking production environment */
        $this->app->detectEnvironment(
            function () {
                return 'production';
            }
        );

        $dateMock = Carbon::parse('2020-08-12 23:59:58');
        Carbon::setTestNow($dateMock);

        $this->assertFalse(LplScoreHelper::isEligibleForVersion2());
    }

    public function testRecalculateRoomScoreWithDryRun()
    {
    }

    public function testIsCriteriaMet()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $criteria = Criteria::where('attribute', 'is_mamiroom')->first();
        $this->assertTrue(LplScoreHelper::isCriteriaMet($room, $criteria));

        $criteria = Criteria::where('attribute', 'is_booking')->first();
        $this->assertTrue(LplScoreHelper::isCriteriaMet($room, $criteria));

        $criteria = Criteria::where('attribute', 'is_not_hostile')->first();
        $this->assertTrue(LplScoreHelper::isCriteriaMet($room, $criteria));

        $criteria = Criteria::where('attribute', 'has_room')->first();
        $this->assertTrue(LplScoreHelper::isCriteriaMet($room, $criteria));
    }

    public function testIsCriteriaMetReturnFalse()
    {
        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create();

        $criteria = Criteria::where('attribute', 'has_photo')->first();
        $this->assertFalse(LplScoreHelper::isCriteriaMet($room, $criteria));

        $criteria = Criteria::where('attribute', 'is_gp_3')->first();
        $this->assertFalse(LplScoreHelper::isCriteriaMet($room, $criteria));

        $criteria = Criteria::where('attribute', 'is_flash_sale')->first();
        $this->assertFalse(LplScoreHelper::isCriteriaMet($room, $criteria));
    }

    public function testIsBasicKosForMamiroom()
    {
        $room = factory(Room::class)
            ->states([ 'active', 'mamirooms'])
            ->create();
        
        $result = LplScoreHelper::isBasicKos($room);
        $this->assertEquals(false, $result);
    }

    public function testIsBasicKosForGoldLevelKos()
    {
        $level = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class)
            ->states([ 'active'])
            ->create();

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $result = LplScoreHelper::isBasicKos($room);
        $this->assertEquals(false, $result);
    }

    public function testIsBasicKos()
    {
        $room = factory(Room::class)
            ->states([ 'active'])
            ->create();
        
        $result = LplScoreHelper::isBasicKos($room);
        $this->assertEquals(true, $result);
    }

    public function testIsHavingGoodBookingPerformance()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => now(),
        ]);

        $result = LplScoreHelper::isHavingGoodBookingPerformance($room);
        $this->assertEquals(true, $result);
    }

    public function testIsHavingGoodBookingPerfomanceShouldReturnFalse()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
        ]);

        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => Carbon::now()->subDays(30),
        ]);

        $result = LplScoreHelper::isHavingGoodBookingPerformance($room);
        $this->assertEquals(false, $result);
    }

    public function testIsHavingGoodBookingPerfomanceTrueOnCheck2()
    {
        $room = factory(Room::class)->create();

        // out of 14 days
        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => Carbon::now()->subDays(30),
        ]);

        // within of 14 days
        factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_FINISHED,
            'created_at' => Carbon::now()->subDays(10),
        ]);

        $result = LplScoreHelper::isHavingGoodBookingPerformance($room);
        $this->assertEquals(true, $result);
    }

    public function testIsHavingGoodBookingPerfomanceTrueOnCheck3()
    {
        $room = factory(Room::class)->create();

        // out of 14 days
        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => Carbon::now()->subDays(30),
        ]);

        // out of 14 days
        factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_FINISHED,
            'created_at' => Carbon::now()->subDays(30),
        ]);

        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE]
        );

        // within of 14 days
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'created_at' => Carbon::now()->subDays(10),
        ]);

        $result = LplScoreHelper::isHavingGoodBookingPerformance($room);
        $this->assertEquals(true, $result);
    }
}
