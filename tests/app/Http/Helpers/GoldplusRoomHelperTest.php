<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Http\Helpers\GoldplusRoomHelper;
use Exception;

class GoldplusRoomHelperTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-2624
     * @group App\Http\Helpers\GoldplusRoomHelper
     */
    public function testGenerateWebUrl()
    {
        $room = factory(Room::class)->make();
        $this->assertNotEmpty(GoldplusRoomHelper::getGoldplusStatisticWebUrl($room->song_id));
    }

    /**
     * @group UG
     * @group UG-4072
     * @group App\Http\Helpers\GoldplusRoomHelper
     */
    public function testFormatGoldplusAcquisitionStatusNull()
    {
        $this->expectException(Exception::class);
        GoldplusRoomHelper::formatGoldplusAcquisitionStatus(null);
        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-4072
     * @group App\Http\Helpers\GoldplusRoomHelper
     */
    public function testFormatGoldplusAcquisitionStatusEmptyLevel()
    {
        $room = factory(Room::class)->make();
        $this->assertEquals([
            'key' => '',
            'value' => ''
        ], GoldplusRoomHelper::formatGoldplusAcquisitionStatus($room));
    }
}
