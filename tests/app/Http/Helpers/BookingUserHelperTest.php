<?php

namespace App\Http\Helpers;

use App\Entities\Activity\Call;
use App\Entities\Activity\Question;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagMaxRenter;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\Booking\BookingSendBirdAutoChatQueue;
use App\Services\Consultant\BookingUserIndexService;
use App\Test\MamiKosTestCase;
use App\Transformers\Room\WebStrippedTransformer;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Mockery;
use Venturecraft\Revisionable\Revision;
use SendBird;
use App\Entities\Level\KostLevelMap;

class BookingUserHelperTest extends MamiKosTestCase
{
    const SENDBIRD_VALID_CHANNEL_URL_EXAMPLE = 'sendbird_group_channel_183150021_40c7dab9c983d4fe041a6ccd882aabed20eee3db';

    // platform interface for MoEngage
    const INTERFACE_DESKTOP = 'desktop';
    const INTERFACE_IOS     = 'mobile-ios';
    const PLATFORM_IOS      = 'ios';


    protected $withEvents = true;

    public function testGetOriginalPriceAtBookingHaveDiscountOriPrice()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);

        // create booking designer data related with room
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $room->id,]);

        // create booking user data related with booking designer
        $bookingUser = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesigner->id,
            'rent_count_type' => 'monthly',
            'created_at' => Carbon::now()->addDays(1)->format('Y-m-d H:i:s')
        ]);

        // create discount data
        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'created_at' => Carbon::now()->addDays(-5)->format('Y-m-d H:i:s'),
            'price' => 100000,
            'is_active' => true
        ]);

        sleep(1);

        // make revision data with update the data of booking
        $discount->price = 15000;
        $discount->save();

        $this->assertEquals($discount->price, BookingUserHelper::getOriginalPriceAtBooking($bookingUser));
    }

    public function testGetOriginalPriceAtBookingSuccess()
    {
        // create room data
        $room = factory(Room::class)->create(['price_monthly' => 1000000, 'address' => null]);

        // create booking designer data related with room
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $room->id,]);

        // create booking user data related with booking designer
        $bookingUser = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesigner->id,
            'rent_count_type' => 'monthly'
        ]);

        $this->assertEquals($room->price_monthly, BookingUserHelper::getOriginalPriceAtBooking($bookingUser));
    }

    public function testGetDiscountHaveOriPrice()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // create discount data
        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'created_at' => Carbon::now()->addDays(-5)->format('Y-m-d H:i:s'),
            'price' => 100000,
            'is_active' => true
        ]);

        sleep(1);

        // make revision data with update the data of booking
        $discount->price = 15000;
        $discount->save();

        $getDiscount = BookingUserHelper::getDiscount($room, Carbon::now()->format('Y-m-d H:i:s'), 'monthly');

        $this->assertEquals($discount->price, $getDiscount['ori_price']);
        $this->assertTrue($getDiscount['has_discount']);
    }

    public function testGetDiscountOriPriceIsFalse()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);

        // create discount data
        factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'created_at' => Carbon::now()->addDays(-5)->format('Y-m-d H:i:s'),
            'price' => 100000,
            'is_active' => true
        ]);

        $getDiscount = BookingUserHelper::getDiscount($room, Carbon::now()->format('Y-m-d H:i:s'), 'monthly');

        $this->assertFalse($getDiscount['ori_price']);
        $this->assertTrue($getDiscount['has_discount']);
    }

    public function testGetDiscountIsEmpty()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // run test
        $this->assertEmpty(BookingUserHelper::getDiscount($room, Carbon::now()->format('Y-m-d H:i:s'), 'monthly'));
    }

    public function testGetRoomPriceAtEntityCreatedDaily()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // run test
        $this->assertFalse(BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'monthly'));
    }

    public function testGetRoomPriceAtEntityCreatedWeekly()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // run test
        $this->assertFalse(BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'weekly'));
    }

    public function testGetRoomPriceAtEntityCreatedMonthly()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // run test
        $this->assertFalse(BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'monthly'));
    }

    public function testGetRoomPriceAtEntityCreatedYearly()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        sleep(1);

        // make revision
        $room->price_yearly = 10000000;
        $room->save();

        // run test
        $getRoomPrice = BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'yearly');

        $this->assertInstanceOf(Revision::class, $getRoomPrice);
        $this->assertEquals('price_yearly', $getRoomPrice->key);
        $this->assertEquals($room->price_yearly, $getRoomPrice->new_value);
        $this->assertEquals('App\Entities\Room\Room', $getRoomPrice->revisionable_type);
    }

    public function testGetRoomPriceAtEntityCreatedQuarterly()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);

         // make revision
         $room->price_quarterly = 10000000;
         $room->save();

        // run test
        $getRoomPrice = BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'quarterly');

        $this->assertInstanceOf(Revision::class, $getRoomPrice);
        $this->assertEquals('price_quarterly', $getRoomPrice->key);
        $this->assertEquals($room->price_quarterly, $getRoomPrice->new_value);
        $this->assertEquals('App\Entities\Room\Room', $getRoomPrice->revisionable_type);
    }

    public function testGetRoomPriceAtEntityCreatedSemiannually()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);

         // make revision
         $room->price_semiannually = 4000000;
         $room->save();

        // run test
        $getRoomPrice = BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), 'semiannually');

        $this->assertInstanceOf(Revision::class, $getRoomPrice);
        $this->assertEquals('price_semiannually', $getRoomPrice->key);
        $this->assertEquals($room->price_semiannually, $getRoomPrice->new_value);
        $this->assertEquals('App\Entities\Room\Room', $getRoomPrice->revisionable_type);
    }

    public function testGetRoomPriceAtEntityCreatedDefault()
    {
        // create room data
        $room = factory(Room::class)->create(['address' => null]);
        // run test
        $this->assertNull(BookingUserHelper::getRoomPriceAtEntityCreated($room, Carbon::now()->format('Y-m-d H:i:s'), ''));
    }
    
    public function testGetCheckoutDateSuccess()
    {
        // prepare variable
        $now        = Carbon::now()->format('Y-m-d');
        $duration   = 1;

        // added data
        $weekly         = Carbon::parse($now)->addWeeks($duration)->format('Y-m-d');
        $quarterly      = Carbon::parse($now)->addQuarters($duration)->format('Y-m-d');
        $semiannually   = Carbon::parse($now)->addMonths($duration * 6)->format('Y-m-d');
        $yearly         = Carbon::parse($now)->addYear($duration)->format('Y-m-d');
        $default        = Carbon::parse($now)->addMonths($duration)->format('Y-m-d');

        // run test
        $this->assertEquals($weekly, BookingUserHelper::getCheckoutDate('weekly', $now, $duration));
        $this->assertEquals($quarterly, BookingUserHelper::getCheckoutDate('quarterly', $now, $duration));
        $this->assertEquals($semiannually, BookingUserHelper::getCheckoutDate('semiannually', $now, $duration));
        $this->assertEquals($yearly, BookingUserHelper::getCheckoutDate('yearly', $now, $duration));
        $this->assertEquals($default, BookingUserHelper::getCheckoutDate('', $now, $duration));
    }

    public function testIsPremiumOwnerFalse()
    {
        // prepare variable
        $user = factory(User::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id
        ]);

        // run test
        $this->assertFalse(BookingUserHelper::isPremiumOwner($roomOwner));
    }

    public function testIsPremiumOwnerTrue()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(1)->format('Y-m-d')
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id
        ]);

        // run test
        $this->assertTrue(BookingUserHelper::isPremiumOwner($roomOwner));
    }

    public function testIsKostLevelGoldPlusFalseLevelIsEmpty(): void
    {
        // prepare data
        $room = factory(Room::class)->create(['address' => null]);

        // run test
        $this->assertFalse(BookingUserHelper::isKostLevelGoldPlus($room));
    }

    public function testIsKostLevelGoldPlusFalseLevelIsNotGoldPlus(): void
    {
        // prepare data
        $room = factory(Room::class)->create(['address' => null]);
        $level = factory(KostLevel::class)->create(['name' => 'Premium']);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $this->assertFalse(BookingUserHelper::isKostLevelGoldPlus($room));
    }

    public function testIsKostLevelGoldPlusTrue(): void
    {
        // prepare data
        $room = factory(Room::class)->create(['address' => null]);
        $level = factory(KostLevel::class)->create([
            'id' => config('kostlevel.id.goldplus1'),
            'name' => 'Mamikos Goldplus 1'
        ]);

        // add level to kost / room
        $room->changeLevel($level->id);

        // run test
        $this->assertTrue(BookingUserHelper::isKostLevelGoldPlus($room));
    }

    public function testMaxMonthCheckInPrebookOwnerPremiumFalse()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(-1)->format('Y-m-d')
        ]);
        $room = factory(Room::class)->create(['address' => null]);
        // create song id
        $room->song_id = $room->id;
        $room->save();

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        $data = BookingUserHelper::maxMonthCheckInPrebook($room);
        $this->assertEquals(config('booking.prebook_checkin_normal_month'), $data);
    }

    public function testMaxMonthCheckInPrebookOwnerPremiumFalseKostLevelNotGoldPlus()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(-1)->format('Y-m-d')
        ]);
        $room = factory(Room::class)->create(['address' => null]);
        // create song id
        $room->song_id = $room->id;
        $room->save();

        // level
        $level = factory(KostLevel::class)->create(['name' => 'Premium']);

        // add level to kost / room
        $room->changeLevel($level->id);

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        $data = BookingUserHelper::maxMonthCheckInPrebook($room);
        $this->assertEquals(config('booking.prebook_checkin_normal_month'), $data);
    }

    public function testMaxMonthCheckInPrebookOwnerPremiumTrue()
    {
        // prepare variable
        $user = factory(User::class)->create([
            'date_owner_limit' => Carbon::today()->addMonth(1)->format('Y-m-d')
        ]);
        $room = factory(Room::class)->create(['address' => null]);
        // create song id
        $room->song_id = $room->id;
        $room->save();

        // level
        $level = factory(KostLevel::class)->create([
            'id' => config('kostlevel.id.goldplus1'),
            'name' => 'Mamikos Goldplus 1'
        ]);

        // add level to kost / room
        $room->changeLevel
        ($level->id);

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => 'verified'
        ]);

        $data = BookingUserHelper::maxMonthCheckInPrebook($room);
        $this->assertEquals(config('booking.prebook_checkin_max_month'), $data);
    }

    public function testMaxMonthCheckInPrebookRoomIsMamirooms(): void
    {
        $room = factory(Room::class)->create([
            'is_mamirooms' => 1,
            'address' => null
        ]);
        // run test
        $data = BookingUserHelper::maxMonthCheckInPrebook($room);
        $this->assertEquals(config('booking.prebook_checkin_max_month'), $data);
    }

    public function testGetPriceSuccess()
    {
        // create data
        $room = factory(Room::class)->create(['price_monthly' => 300000, 'address' => null]);
        $roomPrice = $room->price();

        // run test
        $data = BookingUserHelper::getPrice($roomPrice, 'monthly', false);
        $this->assertEquals($room->price_monthly, $data['price']);
        $this->assertArrayHasKey('currency_symbol', $data);
        $this->assertArrayHasKey('price', $data);
        $this->assertArrayHasKey('rent_type_unit', $data);
    }

    public function testGetIsBookingWithCalendarSuccess()
    {
        // run test
        $data = BookingUserHelper::isBookingWithCalendar(null);
        $this->assertTrue($data);
    }

    public function testGetInterfaceForMoEngageReturnIsDesktop(): void
    {
        // by default function Tracker::getPlatform() will return to web-desktop
        $data = BookingUserHelper::getInterfaceForMoEngage();
        $this->assertEquals(self::INTERFACE_DESKTOP, $data);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetInterfaceForMoEngageReturnIsMobileIos(): void
    {
        // prepare data
        request()->os = self::PLATFORM_IOS;

        // run test
        $data = BookingUserHelper::getInterfaceForMoEngage();
        $this->assertEquals(self::INTERFACE_IOS, $data);
    }

    public function testGetOriginalPriceSuccess(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->make();

        // run test
        $result = BookingUserHelper::getOriginalPrice($roomEntity, 'monthly');
        $this->assertIsInt($result);
    }

    public function testGetMaxRenterByTagIdsEmpty()
    {
        // run test
        $data = BookingUserHelper::getMaxRenterByTagIds([]);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('max', $data);
    }

    public function testGetMaxRenterByTagIdsNotFound(): void
    {
        // run test
        $data = BookingUserHelper::getMaxRenterByTagIds([20201]);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('max', $data);
    }

    public function testGetMaxRenterByTagIdsSuccess()
    {
        // prepare data create tag
        $tagEntity = factory(Tag::class)->create([
            'name' => 'Sekamar berlima'
        ]);

        // create max renter data
        $tagMaxRenterEntity = factory(TagMaxRenter::class)->create([
            'tag_id' => $tagEntity->id,
            'max_renter' => 5
        ]);

        // get tag ids
        $tagIds = Tag::all()->pluck('id')->toArray();

        // run test
        $data = BookingUserHelper::getMaxRenterByTagIds($tagIds);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('max', $data);
        $this->assertEquals($tagEntity->name, $data['name']);
        $this->assertEquals($tagMaxRenterEntity->max_renter, $data['max']);

    }

}