<?php

namespace App\Http\Helpers;

use App\Entities\Device\UserDevice;
use App\Test\MamiKosTestCase;
use Illuminate\Http\JsonResponse;
use phpmock\MockBuilder;

class RatingHelperTest extends MamiKosTestCase
{
    
    protected $userDevice;

    public function testMethodFourToFiveStarScale()
    {
        $this->assertEquals(5, RatingHelper::fourToFiveStarScale(4));
        $this->assertEquals(2.5, RatingHelper::fourToFiveStarScale(2));
        $this->assertEquals(0, RatingHelper::fourToFiveStarScale(0));
        $this->assertEquals(1.25, RatingHelper::fourToFiveStarScale(1));
    }

    public function testRoomRatingToInt()
    {
        $rooms = [
            ['rating' => 3.5, 'rating_string' => '3'],
            ['rating' => 2.11, 'rating_string' => '2.0'],
            ['rating' => 4.5, 'rating_string' => '4.0'],
        ];
    
        $result = RatingHelper::roomsRatingToInt($rooms);

        $this->assertEquals([
            ['rating' => 3, 'rating_string' => '3'],
            ['rating' => 2, 'rating_string' => '2.0'],
            ['rating' => 4, 'rating_string' => '4.0'],
        ], $result);
    }

    public function testRoomRatingToIntWithEmptyArray()
    {
        $result = RatingHelper::roomsRatingToInt([]);
        $this->assertEquals([], $result);
    }

    public function testNormalizeRatingToInt()
    {
        $strData = '{"rooms":[{"_id":62041738,"rating":2.1,"rating_string":"2.0","review_count":4,"room-title":"Kost Rumah Kamang Residence Ragunan Pasar Minggu Jakarta Selatan"},{"_id":11840438,"rating":3.5,"rating_string":"3.0","review_count":5,"room-title":"Kost Chintia - Janson Jakarta Barat"}]}';
        $dummyData = json_decode($strData);

        $response = new JsonResponse;
        $response->setData($dummyData);

        $result = RatingHelper::normalizeRatingToInt($response);
        $jsonResult = $result->getData();

        $this->assertEquals(2, $jsonResult->rooms[0]->rating);
        $this->assertEquals(3, $jsonResult->rooms[1]->rating);
    }

    public function testNormalizeRatingToIntWithEmptyData()
    {
        $strData = '{"rooms":[]}';
        $dummyData = json_decode($strData);

        $response = new JsonResponse;
        $response->setData($dummyData);

        $result = RatingHelper::normalizeRatingToInt($response);
        $jsonResult = $result->getData();

        $this->assertEquals([], $jsonResult->rooms);
    }
    
    public function testAdjustValue()
    {
        $result = RatingHelper::adjustValue(3.04);
        $this->assertEquals(3.0, $result);

        $result = RatingHelper::adjustValue(3.05);
        $this->assertEquals(3.1, $result);

        $result = RatingHelper::adjustValue(3.15);
        $this->assertEquals(3.2, $result);

        $result = RatingHelper::adjustValue(3.56);
        $this->assertEquals(3.6, $result);

        $result = RatingHelper::adjustValue(3.55);
        $this->assertEquals(3.6, $result);

        $result = RatingHelper::adjustValue(3.96);
        $this->assertEquals(4, $result);
    }

    public function testNormalizedAvarageRatingBasedOnPlatformRatingScale4()
    {
        $this->user = factory(\App\User::class)->create();
        $this->userDevice = UserDevice::createDummy('device_token_dummy');
        $this->userDevice->platform = 'android';
        $this->app->user = $this->user;

        // Skenario 1
        // Set to unsupport version 
        $this->setDeviceVersion(UserDevice::MIN_RATING_5_APP_VERSION_ANDROID - 1000000);

        $avgRating = 4;
        $result = RatingHelper::normalizedAvarageRatingBasedOnPlatform($avgRating);
        $this->assertEquals(4, $result);

        // Skenario 2 
        // Set to support version 
        $this->setDeviceVersion(UserDevice::MIN_RATING_5_APP_VERSION_ANDROID + 1000000);

        $avgRating = 4;
        $result = RatingHelper::normalizedAvarageRatingBasedOnPlatform($avgRating);
        $this->assertEquals(5, $result);
    }

    /** 
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testNormalizedAvarageRatingBasedOnPlatformRatingScale5()
    {
        $this->user = factory(\App\User::class)->create();
        $this->userDevice = UserDevice::createDummy('device_token_dummy');
        $this->userDevice->platform = 'android';
        $this->app->user = $this->user;

        // start mock rating_scale = 5
        $mockScale = $this->mockForRatingScaleFive();
        $mockScale->enable();

        // Skenario 1
        // Set to unsupport version 
        $this->setDeviceVersion(UserDevice::MIN_RATING_5_APP_VERSION_ANDROID - 1000000);

        $avgRating = 5;
        $result = RatingHelper::normalizedAvarageRatingBasedOnPlatform($avgRating);
        $this->assertEquals(4, $result);

        // Skenario 2 
        // Set to support version 
        $this->setDeviceVersion(UserDevice::MIN_RATING_5_APP_VERSION_ANDROID + 1000000);

        $avgRating = 5;
        $result = RatingHelper::normalizedAvarageRatingBasedOnPlatform($avgRating);
        $this->assertEquals(5, $result);

        // end of mock
        $mockScale->disable();
    }
    
    private function mockForRatingScaleFive()
    {
        // mock for env RATING_SCALE=5
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Helpers')
            ->setName('config')
            ->setFunction( function ($key) { return 5; }
            );
        $mock = $builder->build();

        return $mock;
    }

    private function setDeviceVersion(string $version)
    {
        $this->userDevice->app_version_code = $version; 
        $this->app->device  = $this->userDevice;
    }

}
