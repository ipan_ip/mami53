<?php

namespace App\Http\Helpers;

use App\Test\MamiKosTestCase;

use \InvalidArgumentException;

class UtilityHelperTest extends MamikosTestCase
{
    //////////////////////////////////////////////////
    ////      UtilityHelper::checkValidEmail     ////
    ////////////////////////////////////////////////

    //Test Function Here

    /**
     * @dataProvider validEmailProvider
     */
    public function testCheckValidEmailReturnTrueIfEmailIsValid($email)
    {
        $this->assertTrue(UtilityHelper::checkValidEmail($email));    
    }

    /**
     * @dataProvider invalidEmailProvider
     */
    public function testCheckValidEmailReturnFalseIfEmailIsInvalid($email)
    {
        $this->assertFalse(UtilityHelper::checkValidEmail($email));    
    }

    //Test Case Data here

    public function validEmailProvider()
    {
        return [
            'normal $email'  => ['email@domain.com'],
            '$email with dot in  the user name'  => ['email.dot@domain.com'],
            '$email with subdomain'  => ['email@subdomain.domain.com'],
            '$email with dash in the user name'  => ['email-dash@domain.com'],
            '$email with dash in the domain'  => ['email@domain-dash.com'],
            '$email with plus in the user name'  => ['email+plus@domain.com'],
            '$email with bracket ip address instead of domain'  => ['email@[123.123.123.123]'],
            '$email with quote in user name'  => ['"email"@domain.com'],
            '$email with quoted special character in user name'  => ['"b(c)d,e:f;g<h>i[j\k]l"@domain.com'],
            '$email with weird top level domain'  => ['email.dot@domain.weird'],
            '$email with multi level top level domain'  => ['email.dot@domain.co.id'],
            '$email with dot in  the user name'  => ['email.dot@domain.com'],
            '$email with number in the user name ' => ['123123123@domain.com'],
            '$email with single digit user name'  => ['o@domain.com'],
        ];
    }

    public function invalidEmailProvider()
    {
        return [
            '$email is null' => [null],
            '$email is empty string' => [''],
            '$email space only' => [' '],
            '$email is plaintext' =>['thisisplaintext'],
            '$email with repeating dot '  => ['email..dotisrepeat@domain.com'],
            '$email with  dot in the end '  => ['email.@domain.com'],
            '$email with no @' => ['email.com'],
            '$email with no @' => ['email.domain.com'],
            '$email with more than one @' => ['email@@domain.com'],
            '$email with special character without quote' => ['b(c)d,e:f;g<h>i[j\k]l@domain.com'],
            '$email with user name more than 64 digit' => ['1234567890123456789012345678901234567890123456789012345678901234+x@domain.com'],
            '$email without user name' => ['@domain.com']
        ];
    }

    //////////////////////////////////////////////////
    ////      UtilityHelper::getCenterPoint     /////
    ////////////////////////////////////////////////


	/**
	 * @param $area
	 * @param $expected
	 *
	 * @dataProvider validAreaProvider
	 */
	public function testGetCenterPointExpectArrayWithValue($area, $expected)
	{
		$this->assertEquals($expected, UtilityHelper::getCenterPoint($area));
	}

	/**
	 * @dataProvider invalidAreaProvider
	 */
	public function testGetCenterPointExpectEmptyArray($area)
	{
		$this->assertEquals([], UtilityHelper::getCenterPoint($area));
	}

	/**
	 * @dataProvider validAreaProvider
	 */
	public function testGetIsValidAreaCoordinatesExpectTrue($area)
	{
		$this->assertTrue(UtilityHelper::getIsValidAreaCoordinates($area));
	}

	/**
	 * @dataProvider invalidAreaProvider
	 */
	public function testGetIsValidAreaCoordinatesExpectFalse($area)
	{
		$this->assertFalse(UtilityHelper::getIsValidAreaCoordinates($area));
	}

	//Test Case Data here

    public function validAreaProvider()
    {
        //Under the assumption the coordinate is 6 digit, default from google maps API
        return [
            '0 precision coordinate' => [
                [
                    [1, 2],
                    [3, 4]
                ],
                [2, 3]
            ],
            '1 precision coordinate' => [
                [
                    [-59.7, 157.6],
                    [42.2, -160.8]
                ],
                [-8.75, -1.6]
            ],
            '2 precision coordinate' => [
                [
                    [3.37, 162.02],
                    [68.76, 66.12]
                ],
                [36.065,114.07]
            ],
            '3 precision coordinate' => [
                [
                    [-47.615, 152.161],
                    [60.741, -80.854]
                ],
                [6.563, 35.6535]
            ],
            '4 precision cordinate' => [
                [
                    [4.3548, -1.4865],
                    [-1.5905, -4.2186]
                ],
                [1.38215, -2.85255]
            ],
            '5 precision coordinate' => [
                [
                    [4.18008, 7.83010],
                    [3.02643, 2.89931]
                ],
                [3.603255, 5.364705]
            ],
        ];
    }

    public function invalidAreaProvider()
    {
        return [
            'coordinates is null' => [
                [null, null],
                [null, null]
            ],
			'coordinate is zero' => [
				[
					[0, 0],
					[0, 0]
				],
				[0, 0]
			],
            'a coordinate is null' => [
                [null, null],
                [1, 2],
            ],
            'incomplete coordinate 1' => [
                [1],
                [2],
            ],
            'incomplete coordinate 2' => [
                [1, 2]
            ],
            '$area is null' => [null]
        ];
    }
}
