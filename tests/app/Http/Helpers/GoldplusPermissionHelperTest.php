<?php

namespace App\Http\Helpers;

use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;
use App\Enums\Goldplus\Permission\GoldplusPermission;

class GoldplusPermissionHelperTest extends MamiKosTestCase
{
    protected function setup()
    {
        parent::setUp();
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsAllowGP1ShouldHasAllPermission()
    {
        foreach (KostLevel::getGoldplusLevelIdsByLevel(1) as $gpId) {
            foreach (GoldplusPermission::getValues() as $permission) {
                $this->assertTrue(GoldplusPermissionHelper::isAllow($gpId, $permission), "GP1 (" . $gpId . ") Should has permission: " . $permission);
            }
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsAllowGP2ShouldHasAllPermission()
    {
        foreach (KostLevel::getGoldplusLevelIdsByLevel(2) as $gpId) {
            foreach (GoldplusPermission::getValues() as $permission) {
                $this->assertTrue(GoldplusPermissionHelper::isAllow($gpId, $permission), "GP2 (" . $gpId . ") Should has permission: " . $permission);
            }
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsAllowGP3ShouldHasLimitedPermission()
    {
        foreach (KostLevel::getGoldplusLevelIdsByLevel(3) as $gpId) {
            foreach (GoldplusPermission::getValues() as $permission) {
                $this->assertFalse(GoldplusPermissionHelper::isAllow($gpId, $permission), "GP3 (" . $gpId . ") Should not have permission: " . $permission);
            }
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsAllowGP4()
    {
        $allowedPermissions = [
            GoldplusPermission::ROOM_UNIT_READ,
            GoldplusPermission::ROOM_UNIT_UPDATE,
            GoldplusPermission::ROOM_UNIT_DELETE,
            GoldplusPermission::PRICE_READ,
            GoldplusPermission::PRICE_UPDATE,
        ];
        $this->assertIsAllow(4, $allowedPermissions);
        $this->assertIsDeny(4, array_diff(GoldplusPermission::getValues(), $allowedPermissions));
    }

    private function assertIsAllow(int $gpGroup, array $permissions)
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups($gpGroup) as $gpId) {
            foreach ($permissions as $permission) {
                $this->assertTrue(
                    GoldplusPermissionHelper::isAllow($gpId, $permission),
                    "GP" . $gpGroup . " (" . $gpId . ") Should have permission: " . $permission
                );
            }
        }
    }

    private function assertIsDeny(int $gpGroup, array $permissions)
    {
        foreach (KostLevel::getGoldplusLevelIdsByGroups($gpGroup) as $gpId) {
            foreach ($permissions as $permission) {
                $this->assertFalse(
                    GoldplusPermissionHelper::isAllow($gpId, $permission),
                    "GP" . $gpGroup . " (" . $gpId . ") Should not have permission: " . $permission
                );
            }
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testGetPermissionsInvalidGPIdShouldAlledAll()
    {
        $gpId = mt_rand(111111, 999999);
        $this->assertEqualsCanonicalizing(
            GoldplusPermission::getValues(),
            GoldplusPermissionHelper::getPermissions($gpId)
        );
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testGetPermissionsGP1ShouldHasPermissions()
    {
        foreach (KostLevel::getGoldplusLevelIdsByLevel(1) as $gpId) {
            $this->assertNotEquals([], GoldplusPermissionHelper::getPermissions($gpId));
        }
    }

    /**
     * @group UG
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testGetPermissionsGP2ShouldHasPermissions()
    {
        foreach (KostLevel::getGoldplusLevelIdsByLevel(2) as $gpId) {
            $this->assertNotEquals([], GoldplusPermissionHelper::getPermissions($gpId));
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowNonGPShouldBeAllowed()
    {
        $room = factory(Room::class)->create();
        $this->assertTrue(GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::getRandomValue()));
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowAnyNonGPShouldBeAllowed()
    {
        $room = factory(Room::class)->create();
        $this->assertTrue(GoldplusPermissionHelper::isRoomAllowAny($room, GoldplusPermission::getValues()));
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowAllNonGPShouldBeAllowed()
    {
        $room = factory(Room::class)->create();
        $this->assertTrue(GoldplusPermissionHelper::isRoomAllowAll($room, GoldplusPermission::getValues()));
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowGP1ShouldBeAllowed()
    {
        $room = factory(Room::class)->create();
        factory(KostLevel::class)->state('regular')->create();
        foreach (KostLevel::getGoldplusLevelIdsByLevel(1) as $gpId) {
            $room->level()->detach();
            $room->level()->attach(factory(KostLevel::class)->create(['id' => $gpId]));
            $this->assertTrue(GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::getRandomValue()));
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowGP3ShouldBeDenied()
    {
        $room = factory(Room::class)->create();
        factory(KostLevel::class)->state('regular')->create();
        foreach (KostLevel::getGoldplusLevelIdsByLevel(3) as $gpId) {
            $room->level()->detach();
            $room->level()->attach(factory(KostLevel::class)->create(['id' => $gpId]));
            $this->assertFalse(GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::getRandomValue()));
        }
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowAnyGP4()
    {
        $room = factory(Room::class)->create();
        factory(KostLevel::class)->state('regular')->create();
        foreach (KostLevel::getGoldplusLevelIdsByLevel(4) as $gpId) {
            $room->level()->detach();
            $room->level()->attach(factory(KostLevel::class)->create(['id' => $gpId]));
            $this->assertTrue(GoldplusPermissionHelper::isRoomAllowAny($room, GoldplusPermission::getValues()));
        }
    }

    /**
     * @group UG
     * @group UG-4072
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowAnyInvalidPermissions()
    {
        factory(KostLevel::class)->state('regular')->create();
        $gpId = factory(KostLevel::class)->create()->id;
        $room = factory(Room::class)->create();
        $room->level()->attach($gpId);
        Config::set('kostlevel.ids.goldplus1', $gpId);

        $invalidPermission = str_random();
        $this->assertFalse(GoldplusPermissionHelper::isRoomAllowAny($room, [$invalidPermission]));
    }

    /**
     * @group UG
     * @group UG-2671
     * @group UG-4216
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testIsRoomAllowAllGP4()
    {
        $room = factory(Room::class)->create();
        factory(KostLevel::class)->state('regular')->create();
        foreach (KostLevel::getGoldplusLevelIdsByLevel(4) as $gpId) {
            $room->level()->detach();
            $room->level()->attach(factory(KostLevel::class)->create(['id' => $gpId]));
            $this->assertFalse(GoldplusPermissionHelper::isRoomAllowAll($room, GoldplusPermission::getValues()));
        }
    }
    /**
     * @group UG
     * @group UG-4491
     * @group App\Http\Helpers\GoldplusPermissionHelper
     */
    public function testGetGoldplusLevelIdsWithDenyRoomUpdatePermission()
    {
        $expected = KostLevel::getGoldplusLevelIdsByGroups(3);
        $result = GoldplusPermissionHelper::getGoldplusLevelIdsWithDenyPermissions([
            GoldplusPermission::ROOM_UNIT_READ,
            GoldplusPermission::PRICE_READ,
        ]);
        $this->assertEqualsCanonicalizing($expected, $result);
    }
}
