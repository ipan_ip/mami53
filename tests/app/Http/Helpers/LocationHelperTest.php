<?php

namespace Http\Helpers;

use App\Http\Helpers\LocationHelper;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class LocationHelperTest extends MamiKosTestCase
{
    use WithFaker;

    public const COORDINATE = 106.5938186645508;

    public function testGetCenterLocationWithEmptyArray_ResultSuccess()
    {
        $location = [];

        $response = LocationHelper::getCenterLocationForTracker($location);

        $this->assertEquals("", $response);
    }

    public function testGetCenterLocationWithSingleArray_ResultSuccess()
    {
        $location = [
            [
                "110.29432441711427",
                "-8.009030676669463",
            ],
        ];

        $expectedLocation = floatval($location[0][0]) . "," . floatval($location[0][1]);

        $response = LocationHelper::getCenterLocationForTracker($location);

        $this->assertEquals($expectedLocation, $response);
    }

    public function testGetCenterLocationWithMultipleArray_ResultSuccess()
    {
        $location = [
            [
                "106.5938186645508",
                "-6.247088848170634",
            ],
            [
                "106.62815093994142",
                "-6.22981105782168",
            ],
        ];

        $expectedLocation = ((floatval($location[0][0]) + floatval($location[1][0])) / 2) . "," . ((floatval($location[0][1]) + floatval($location[1][1])) / 2);

        $response = LocationHelper::getCenterLocationForTracker($location);

        $this->assertEquals($expectedLocation, $response);
    }

    public function testGetGeoCoordinate()
    {
        $response = LocationHelper::getGeoCoordinate(self::COORDINATE);

        $this->assertIsNotFloat($response);
        $this->assertIsString($response);
    }

    public function testExpandRangeLocation()
    {
        $mockLocationData = $this->setupLocationData(false);
        $response = LocationHelper::expandRangeLocation($mockLocationData);

        $this->assertIsArray($response);
        $this->assertNotEmpty($response);
    }

    public function testExpandRangeLocation_WithCenterPointData()
    {
        $mockLocationData = $this->setupLocationData();
        $response = LocationHelper::expandRangeLocation($mockLocationData);

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    public function testExpandRangeLocation_WithEmptyData()
    {
        $mockLocationData = [];
        $response = LocationHelper::expandRangeLocation($mockLocationData);

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    public function testExpandRangeLocation_WithoutData()
    {
        $response = LocationHelper::expandRangeLocation();

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    private function setupLocationData(bool $isCenterPoint = true)
    {
        $initialLatitude = $this->faker->latitude;
        $initialLongitude = $this->faker->longitude;

        if ($isCenterPoint) {
            return [
                $initialLongitude,
                $initialLatitude
            ];
        } else {
            return [
                [
                    $initialLongitude,
                    $initialLatitude
                ],
                [
                    $initialLongitude + 0.1,
                    $initialLatitude + 0.1
                ]
            ];
        }
    }
}
