<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\ApiResponse;
use App\Test\MamiKosTestCase;
use Validator;

class ApiResponseTest extends MamiKosTestCase
{
    public function testResponseReturnsRightContent()
    {
        
        $data = array("status" => "not found");
        $resp = ApiResponse::response($data, 404);

        // response status must an instance of Illuminate\Http\JsonResponse
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $resp);

        // response status must return the right value
        $this->assertEquals(404, $resp->status());

        // response content must return the right content
        $this->assertEquals('{"status":"not found"}', $resp->content());
    }

    public function testResponseDataReturnsRightContent()
    {
        
        $data = array("name" => "Vinsmoke Sanji", "age" => 19);
        $resp = ApiResponse::responseData($data);

        // response status must an instance of Illuminate\Http\JsonResponse
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $resp);

        $origCont = $resp->getOriginalContent();
        $this->assertTrue($origCont["status"]);

        // check the match of original data
        $this->assertEquals("Vinsmoke Sanji", $origCont["name"]);
        $this->assertEquals(19, $origCont["age"]);
    }

    public function testResponseError()
    {
        $resp = ApiResponse::responseError("Room data could not be found!", false, 400);

        // response status must an instance of Illuminate\Http\JsonResponse
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $resp);

        // status code must match
        $this->assertEquals(400, $resp->status());

        $origCont = $resp->getOriginalContent();

        // original content must match
        $this->assertFalse($origCont["status"]);
        $this->assertEquals("Room data could not be found!", $origCont["meta"]["message"]);
        $this->assertEquals("Error", $origCont["meta"]["severity"]);
    }

    public function testResponseUserFailedEmptyParams()
    {
        //empty params
        $resp = ApiResponse::responseUserFailed();

        // response status must an instance of Illuminate\Http\JsonResponse
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $resp);

        // status code must be 200
        $this->assertEquals(200, $resp->status());
    }

    public function testResponseUserFailedWithParams()
    {
        $resp = ApiResponse::responseUserFailed(array("data" => array("name" => "Lisa Blackpink"), "meta" => array("message" => "No hp tidak sesuai format")));
        
        // response status must an instance of Illuminate\Http\JsonResponse
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $resp);

        // status code must be 200
        $this->assertEquals(200, $resp->status());

        $origCont = $resp->getOriginalContent();

        // data data must be match
        $this->assertEquals("Lisa Blackpink", $origCont["data"]["name"]);

        // meta must be match
        $this->assertEquals("No hp tidak sesuai format", $origCont["meta"]["message"]);
    }

    public function testValidationErrorsToString()
    {
        $vald = Validator::make(array('city' => 'Sleman'), array('latitude' => 'required'));
        $vald->errors()->add('field', 'Data tidak ditemukan');

        $str = ApiResponse::validationErrorsToString($vald->errors());

        $this->assertEquals("The latitude field is required., Data tidak ditemukan", $str);
    }
}