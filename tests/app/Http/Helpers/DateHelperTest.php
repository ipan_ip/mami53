<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\DateHelper;
use App\Test\MamiKosTestCase;

class DateHelperTest extends MamiKosTestCase
{
    public function testMonthIdnFormated()
    {
        // this number to string is copied from DateHelper directly
        // on April 06 2020
        // If there is a requirement change, then please also update
        // this test conciously.
        $months = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        ];

        // when number is in range, return the right value
        foreach ($months as $key => $value) {
            $this->assertEquals($value, DateHelper::monthIdnFormated($key));
        }

        // when number is out of range, return null
        $this->assertEquals(null, DateHelper::monthIdnFormated(0));
        $this->assertEquals(null, DateHelper::monthIdnFormated(13));
    }

}