<?php

namespace App\Http\Helpers;

use App\Entities\Booking\BookingAcceptanceRate;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Entities\Room\BookingOwnerRequest;
use App\Http\Helpers\BookingAcceptanceRateHelper;
use App\Entities\Booking\BookingOwnerRequestEnum;

class BookingAcceptanceRateHelperTest extends MamiKosTestCase
{
    private $helper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->helper = $this->app->make(BookingAcceptanceRateHelper::class);
    }

    public function testGet()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
            'is_booking' => 1,
            'is_active' => 1,
        ]);

        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => '2018-06-01 19:00:00',
        ]);

        $result = BookingAcceptanceRateHelper::get($room);

        $this->assertEquals($ownerRequest->created_at->toDateTimeString(), $result['active_from']);
        $this->assertEquals($bar->rate, $result['accepted_rate']);
        $this->assertEquals($bar->average_time/60, $result['average_time']);
    }

    public function testGetWhenRegularKos()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
            'is_booking' => 0,
            'is_active' => 1,
        ]);

        $result = BookingAcceptanceRateHelper::get($room);

        $this->assertEquals(null, $result['active_from']);
        $this->assertEquals(null, $result['accepted_rate']);
        $this->assertEquals(null, $result['average_time']);
    }

    public function testGetWhenStatusInactive()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
            'is_booking' => 1,
            'is_active' => 1,
        ]);

        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 0,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => '2018-06-01 19:00:00',
        ]);

        $result = BookingAcceptanceRateHelper::get($room);

        $this->assertEquals($ownerRequest->created_at->toDateTimeString(), $result['active_from']);
        $this->assertEquals(null, $result['accepted_rate']);
        $this->assertEquals(null, $result['average_time']);
    }

    public function testGetWhenNullData()
    {
        $room = factory(Room::class)->create([
            'owner_phone' => '08121312312',
            'owner_name' => 'John Doe',
            'is_booking' => 1,
            'is_active' => 1,
        ]);

        $ownerRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id' => $room->id,
            'status' => BookingOwnerRequestEnum::STATUS_APPROVE,
            'created_at' => null,
        ]);

        $result = BookingAcceptanceRateHelper::get($room);

        $this->assertEquals(null, $result['active_from']);
        $this->assertEquals(null, $result['accepted_rate']);
        $this->assertEquals(null, $result['average_time']);
    }

    public function testMinuteToHour()
    {
        $this->assertEquals(2, BookingAcceptanceRateHelper::minuteToHour(120));
        $this->assertEquals(1, BookingAcceptanceRateHelper::minuteToHour(60));
        $this->assertEquals(0, BookingAcceptanceRateHelper::minuteToHour(20));
        $this->assertEquals(0, BookingAcceptanceRateHelper::minuteToHour(59));
    }

    public function testExtractEachRowOnCancelled()
    {
        $data = [
            "id" => 302,
            "booking_code" => "MAMI0607000302",
            "reject_reason" => null,
            "booking_user_created" => "2019-06-27 13:40:41",
            "booking_status_created" => "2019-06-27 13:41:28",
            "booking_user_status" => "cancelled",
            "booking_status_status" => "cancelled",
            "process_time" => 0
        ];

        $method = $this->getPrivateMethod('extractEachRow');
        $result = $method->invokeArgs($this->helper, [$data]);

        $this->assertEquals(0, $result['value']);
        $this->assertEquals(true, $result['valid']);
    }

    public function testExtractEachRowOnSuccess()
    {
        $data = [
            "id" => 277,
            "booking_code" => "MAMI0606000277",
            "reject_reason" => null,
            "booking_user_created" => "2019-06-26 11:23:27",
            "booking_status_created" => "2019-06-26 12:16:41",
            "booking_user_status" => "terminated",
            "booking_status_status" => "confirmed",
            "process_time" => 53
        ];

        $method = $this->getPrivateMethod('extractEachRow');
        $result = $method->invokeArgs($this->helper, [$data]);

        $this->assertEquals(53, $result['value']);
        $this->assertEquals(true, $result['valid']);
    }

    public function testExtractEachRowOnRejected()
    {
        $data = [
            "id" => 330,
            "booking_code" => "MAMI0601000330",
            "reject_reason" => "xhcjbkxtg",
            "booking_user_created" => "2019-06-28 17:00:21",
            "booking_status_created" => null,
            "booking_user_status" => "rejected",
            "booking_status_status" => null,
            "process_time" => null
        ];

        $method = $this->getPrivateMethod('extractEachRow');
        $result = $method->invokeArgs($this->helper, [$data]);

        $this->assertEquals(0, $result['value']);
        $this->assertEquals(false, $result['valid']);
    }

    private function getPrivateMethod($key)
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingAcceptanceRateHelper::class,
            $key
        );
        return $method;
    }

    public function testGetAcceptanceRate()
    {
        $room = factory(Room::class)->create([
            'owner_name' => 'John Doe',
            'is_booking' => 1,
            'is_active' => 1,
        ]);

        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => $room->id,
            'is_active' => 1,
            'average_time' => 120,
            'rate' => 80,
        ]);

        $result = BookingAcceptanceRateHelper::getAcceptanceRate($room);
        $this->assertEquals($bar->rate, $result);
    }

    public function testGetAcceptanceRateReturnZero()
    {
        $room = factory(Room::class)->create([
            'owner_name' => 'Plam Doe',
            'is_active' => 1,
        ]);

        $result = BookingAcceptanceRateHelper::getAcceptanceRate($room);
        $this->assertEquals(0, $result);
    }
}
