<?php

namespace App\Http\Helpers;

use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelValue;
use App\Entities\Level\KostLevelValueMapping;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Artisan;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;

class KostLevelValueHelperTest extends MamiKosTestCase
{
    use WithFaker;

    protected $helperInstance;
    protected $defaultCounter;

    protected function setUp()
    {
        parent::setUp();

        $this->helperInstance = new KostLevelValueHelper();

        Artisan::call(
            'db:seed',
            [
                '--class' => 'KostLevelValueSeeder'
            ]
        );
    }

    public function testGetKostLevelValueMapping()
    {
        $response = $this->helperInstance->getKostLevelValueMapping();

        $this->assertIsArray($response);
        $this->assertCount(5, $response);
        $this->assertEquals(
            [
                0 => [1, 2, 3, 4, 5, 6, 7],
                1 => [1, 2, 3],
                2 => [1, 2, 3],
                3 => [1, 2, 3, 4, 5],
                4 => [1, 2, 3, 4, 5, 6]
            ],
            $response
        );
    }

    public function testGetAllKostLevelIDs()
    {
        $response = $this->helperInstance->getAllKostLevelIDs();

        $this->assertIsArray($response);
        $this->assertCount(4, $response);
    }

    public function testGetAllKostLevelIDs_WithMamirooms()
    {
        factory(KostLevel::class)->create(
            [
                'name' => 'Mamirooms'
            ]
        );

        $response = $this->helperInstance->getAllKostLevelIDs();

        $this->assertIsArray($response);
        $this->assertCount(5, $response);
    }

    public function testGetMappingByLevelName_ForGoldPlus1()
    {
        $mapping = $this->helperInstance->getKostLevelValueMapping();

        $response = $this->helperInstance->getMappingByLevelName('GP1');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[1], $response);

        $response = $this->helperInstance->getMappingByLevelName('Goldplus 1');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[1], $response);
    }

    public function testGetMappingByLevelName_ForGoldPlus2()
    {
        $mapping = $this->helperInstance->getKostLevelValueMapping();

        $response = $this->helperInstance->getMappingByLevelName('GP2');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[2], $response);

        $response = $this->helperInstance->getMappingByLevelName('Goldplus 2');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[2], $response);
    }

    public function testGetMappingByLevelName_ForGoldPlus3()
    {
        $mapping = $this->helperInstance->getKostLevelValueMapping();

        $response = $this->helperInstance->getMappingByLevelName('GP3');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[3], $response);

        $response = $this->helperInstance->getMappingByLevelName('Goldplus 3');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[3], $response);
    }

    public function testGetMappingByLevelName_ForGoldPlus4()
    {
        $mapping = $this->helperInstance->getKostLevelValueMapping();

        $response = $this->helperInstance->getMappingByLevelName('GP4');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[4], $response);

        $response = $this->helperInstance->getMappingByLevelName('Goldplus 4');
        $this->assertIsArray($response);
        $this->assertEquals($mapping[4], $response);
    }

    public function testGetMappingByLevelName_ReturnEmpty()
    {
        $response = $this->helperInstance->getMappingByLevelName('Non GP');

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    public function testGetMappingByLevelName_WithEmptyParameter()
    {
        $response = $this->helperInstance->getMappingByLevelName('');

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    public function testGetMappingByLevelName_WithNoParameter()
    {
        $response = $this->helperInstance->getMappingByLevelName();

        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    public function testGetGoldPlusIds()
    {
        $response = $this->helperInstance->getGoldPlusIds();

        $this->assertIsArray($response);
        $this->assertCount(4, $response);
        $this->assertEquals(config('kostlevel.id.goldplus1'), $response[1]);
        $this->assertEquals(config('kostlevel.id.goldplus2'), $response[2]);
        $this->assertEquals(config('kostlevel.id.goldplus3'), $response[3]);
        $this->assertEquals(config('kostlevel.id.goldplus4'), $response[4]);
    }

    public function testAssignValuesToKostLevel()
    {
        $level = $this->setupData();

        $targetIDs = KostLevelValue::all()
            ->pluck('id')
            ->toArray();

        $response = $this->helperInstance->assignValuesToKostLevel($level, $targetIDs);
        $this->assertTrue($response);

        $instance = KostLevel::with('values')->find($level->id);
        $this->assertEquals($this->defaultCounter, $instance->values->count());
    }

    public function testAssignValuesToKostLevel_WithEmptyTargetIDs()
    {
        $level = $this->setupData();

        $response = $this->helperInstance->assignValuesToKostLevel($level);
        $this->assertTrue($response);

        $instance = KostLevel::with('values')->find($level->id);
        $this->assertEquals(0, $instance->values->count());
    }

    /**
     * @return Model|mixed
     */
    private function setupData()
    {
        /* Cleaning Up */
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        if (KostLevel::query()->count()) {
            DB::table('kost_level')->truncate();
        }

        if (KostLevelValueMapping::query()->count()) {
            DB::table('kost_level_value_mapping')->truncate();
        }

        if (KostLevelValue::query()->count()) {
            DB::table('kost_level_value')->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        /* Create Kost Values */
        $this->defaultCounter = $this->faker->numberBetween(1, 3);
        factory(KostLevelValue::class, $this->defaultCounter)->create();

        /* Create Kost Level */
        return factory(KostLevel::class)->create();
    }

    public function testGetKostValueWithEmptyBenefit()
    {
        factory(KostLevel::class)->create(['name' => 'OYO']);
        $room = factory(Room::class)->create(['is_mamirooms' => false]);

        $result = KostLevelValueHelper::getKostValue($room);
        $this->assertEquals([], $result);
    }

    public function testGetKostValueWithoutId()
    {
        $room = factory(Room::class)->create(['is_mamirooms' => false]);
        $result = KostLevelValueHelper::getKostValueWithoutId($room);

        $this->assertEmpty($result["benefits"]);
        $this->assertEquals($room->song_id, $result["_id"]);
    }

    public function testGetKosBookingTransaction()
    {
        $room = factory(Room::class)->create();

        $contract = factory(MamipayContract::class)->create();

        factory(MamipayContractKost::class)->create(
            [
                'contract_id' => $contract->id,
                'designer_id' => $room->id
            ]
        );

        factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        factory(BookingUser::class)->create(
            [
                'contract_id' => $contract->id,
                'booking_designer_id' => $room->id
            ]
        );

        $result = KostLevelValueHelper::getKosBookingTransaction($room);
        $this->assertArrayHasKey('title', $result);
        $this->assertArrayHasKey('description', $result);
    }

    public function testGetKosBookingTransactionReturnEmpty()
    {
        $room = factory(Room::class)->create();
        $this->assertEmpty(KostLevelValueHelper::getKosBookingTransaction($room));
    }
}
