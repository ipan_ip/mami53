<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\BookingInvoiceHelper;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayAdditionalCost;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\File;
use Mockery;

class BookingInvoiceHelperTest extends MamiKosTestCase
{
    
    public function testPriceFormat()
    {
        $prcForm = BookingInvoiceHelper::priceFormat('Total', 19000);
        $this->assertEquals('Total', $prcForm['price_name']);
        $this->assertEquals('Total', $prcForm['price_label']);
        $this->assertEquals(19000.0, $prcForm['price_total']);
        $this->assertEquals('Rp19.000', $prcForm['price_total_string']);
        $this->assertEquals('', $prcForm['price_type']);
    }

    /** 
    * Author: Arifin
    * following test method will use mockery hard dependency trick
    * see: http://docs.mockery.io/en/latest/cookbook/mocking_hard_dependencies.html
    * 
    * about the runInSeparateProcess and preserveGlobalState anotations
    * see: https://phpunit.de/manual/6.5/en/appendixes.annotations.html#appendixes.annotations.runInSeparateProcess
    * trick from : https://benramsey.com/articles/mocking-hard-dependencies/
    *
    * @runInSeparateProcess
    * @preserveGlobalState disabled
    */
    public function testGetAmountByContractId() {
        // --Start Mocking--
        // MamipayApi get internal/contract response
        $f = File::get(storage_path('test/json/mamipay/internal_contract.json'));
        $jsonResp = json_decode($f);
        $mpContMock = Mockery::mock('overload:App\Entities\Mamipay\MamipayContract');
        $mpContMock->shouldReceive('getFromExternalMamipay')
            ->andReturn($jsonResp);
        $mpContMock->shouldReceive('select')
            ->andReturn($mpContMock);

        $mpContEntity = new MamipayContract;
        $mpContEntity->start_date = '2019-02-02';
        $mpContMock->shouldReceive('find')
            ->andReturn($mpContEntity);
        // --End Mocking--

        $contractId = 2147483647; // max int value.
        $amount = BookingInvoiceHelper::getAmountByContractId($contractId);
        // billing amount is 0
        $this->assertEquals('Base', $amount['base']['price_name']);
        $this->assertEquals('Base', $amount['base']['price_label']);
        $this->assertEquals(0, $amount['base']['price_total']);
        $this->assertEquals("", $amount['base']['price_type']);
        
        $this->assertArrayHasKey('other', $amount);

        $this->assertEquals('Total', $amount['total']['price_name']);
        $this->assertEquals('Total', $amount['total']['price_label']);
        $this->assertEquals(0, $amount['total']['price_total']);
        $this->assertEquals("", $amount['total']['price_type']);

        $this->assertArrayHasKey('fine', $amount);
    }

    public function testGetInvoiceCostByContractIdTypeOther()
    {
        $contractId = 2147483647; // max int value.
        factory(MamipayInvoice::class)
            ->create(['contract_id' => $contractId])
            ->each(function($inv){
                $inv->additional_costs()
                    ->save(factory(MamipayAdditionalCost::class)->make());
            });
        
        $invCost = BookingInvoiceHelper::getInvoiceCostByContractId($contractId);

        // costs element must be 1
        $this->assertCount(1, $invCost['costs']);
        $this->assertEquals([], $invCost['admin']);
        $this->assertEquals([], $invCost['vouchers']);
        $this->assertEquals(6500.0, $invCost['total']);

        // check the first element

        $expCostVal = array(
            "price_name" => "Makan",
            "price_label" => "Makan",
            "price_total" => 6500.0,
            "price_total_string" => 'Rp6.500',
            "price_type" => 'other'
        );
        $this->assertEquals($expCostVal, $invCost['costs'][0]);
    }

    public function testGetInvoiceCostByContractIdTypeAdmin()
    {
        $contractId = 2147483647; // max int value.
        factory(MamipayInvoice::class)
            ->create(['contract_id' => $contractId])
            ->each(function($inv){
                $inv->additional_costs()
                    ->save(
                        factory(MamipayAdditionalCost::class)
                            ->make([
                                'cost_type' => 'admin',
                                'cost_title' => 'Admin Fee',
                                'cost_value' => 10000.0,
                                ])
                    );
                $inv->additional_costs()
                    ->save(factory(MamipayAdditionalCost::class)->make());
            });
        
        $invCost = BookingInvoiceHelper::getInvoiceCostByContractId($contractId);

        // costs element must be 2
        $this->assertCount(2, $invCost['costs']);
        $expAdmCostVal = array(
            "price_name" => "Admin Fee",
            "price_label" => "Admin Fee",
            "price_total" => 10000.0,
            "price_total_string" => 'Rp10.000',
            "price_type" => 'admin'
        );
        $this->assertEquals($expAdmCostVal, $invCost['admin']);
        $this->assertEquals([], $invCost['vouchers']);
        $this->assertEquals(16500.0, $invCost['total']);

        $key = array_search('admin', array_column($invCost['costs'], 'price_type'));
        // admin key should be found
        $this->assertNotFalse($key);

        $key = array_search('other', array_column($invCost['costs'], 'price_type'));
        // other key should be found
        $this->assertNotFalse($key);
    }

    public function testGetInvoiceCostByContractIdNull() {
        $contractId = -2147483645;
        // this contract id should not exist
        $invCost = BookingInvoiceHelper::getInvoiceCostByContractId($contractId);
        
        $expVal = array(
            'costs' => [],
            'admin' => [],
            'total' => 0
        );
        $this->assertEquals($expVal, $invCost); 
    }

    // Author: Arifin
    public function testInvoice()
    {
        $invoices = factory(MamipayInvoice::class, 3)
            ->create(['contract_id' => 2147483647, 'scheduled_date'=>'2020-01-01'])
            ->each(function($inv){
                $con = factory(MamipayContract::class)->create();
                $inv->contract_id = $con->id;
                $inv->save();
            });
        $formattedInvs = BookingInvoiceHelper::invoice($invoices);
        $this->assertCount(3, $formattedInvs);
        $this->assertArrayHasKey('_id', $formattedInvs[0]);
        $this->assertArrayHasKey('contract_id', $formattedInvs[0]);
        $this->assertArrayHasKey('name', $formattedInvs[0]);
        $this->assertArrayHasKey('invoice_number', $formattedInvs[0]);
        $this->assertArrayHasKey('shortlink', $formattedInvs[0]);
        $this->assertArrayHasKey('scheduled_date', $formattedInvs[0]);
        $this->assertArrayHasKey('scheduled_date_second', $formattedInvs[0]);
        $this->assertArrayHasKey('paid_at', $formattedInvs[0]);
        $this->assertArrayHasKey('amount', $formattedInvs[0]);
        $this->assertArrayHasKey('amount_string', $formattedInvs[0]);
        $this->assertArrayHasKey('status', $formattedInvs[0]);
        $this->assertArrayHasKey('late_payment', $formattedInvs[0]);
        $this->assertArrayHasKey('status_payment', $formattedInvs[0]);
        $this->assertArrayHasKey('manual_reminder', $formattedInvs[0]);
        $this->assertArrayHasKey('last_payment', $formattedInvs[0]);

        // to: author of the function
        // please continue the the test to check if the value of each key
        // are valid.
    }
}