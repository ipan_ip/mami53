<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\GenderHelper;
use App\Test\MamiKosTestCase;

class GenderHelperTest extends MamiKosTestCase
{
    public function testGetUserGenderReplaceLakiLakiReturnMale()
    {
        $this->assertEquals('male', GenderHelper::getUserGenderReplace('laki-laki'));
    }

    public function testGetUserGenderReplacePerempuanReturnFemale()
    {
        $this->assertEquals('female', GenderHelper::getUserGenderReplace('perempuan'));
    }

    public function testGetUserGenderReplaceNotMappedReturnInput()
    {
        $input = 'definitely-not-gender';
        $this->assertEquals(
            $input, 
            GenderHelper::getUserGenderReplace($input)
        );
    }
}
