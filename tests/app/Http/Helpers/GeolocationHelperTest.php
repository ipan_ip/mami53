<?php

namespace App\Http\Helpers;

use App;
use App\Entities\Area\AreaGeolocation;
use App\Entities\Search\InputKeyword;
use App\Entities\Search\InputKeywordSuggestion;
use App\Test\MamiKosTestCase;
use DB;

class GeolocationHelperTest extends MamiKosTestCase
{
    /**
     * @var GeolocationHelper|mixed
     */
    private $geolocationHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->geolocationHelper = app()->make(GeolocationHelper::class);
    }

    public function testMapGeolocationReturnTrue()
    {
        [$suggestion, $areaGeolocation] = $this->setBandungInputProperty();
        DB::commit();

        $this->assertTrue($this->geolocationHelper->mapGeolocation($suggestion));
        $this->deleteTableRecords();
    }

    public function testMapGeolocationWithIgnoredAdministrativeTypeReturnFalse()
    {
        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
                'administrative_type' => 'bank' // ignored Administrative type
            ]
        );

        $this->assertFalse($this->geolocationHelper->mapGeolocation($suggestion));
    }

    public function testSetGeolocationWithEmptyAddressReturnFalse()
    {
        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'suggestion' => '',
                'area' => '',
            ]
        );

        $this->assertFalse($this->geolocationHelper->setGeolocation($suggestion));
    }

    public function testSetGeolocationReturnTrue()
    {
        [$suggestion, $areaGeolocation] = $this->setBandungInputProperty();
        DB::commit();

        $this->assertTrue($this->geolocationHelper->setGeolocation($suggestion));
        $this->deleteTableRecords();
    }

    public function testSetGeolocationWithNewLevelReturnTrue()
    {
        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        //Area Geolocation doesn't have city, so the function will use new level (Province)
        factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => null]);
        DB::commit();

        $this->assertTrue($this->geolocationHelper->setGeolocation($suggestion));
        $this->deleteTableRecords();
    }

    public function testGetFormattedAddressReturnData()
    {
        $address  = "Bandung, Jawa Barat, Indonesia";
        $response = $this->geolocationHelper->getFormattedAddress($address);

        $this->assertArrayHasKey('level', $response);
        $this->assertArrayHasKey('address', $response);
    }

    public function testGetFormattedAddressWithEmptyInputReturnEmptyArray()
    {
        $this->assertEmpty($this->geolocationHelper->getFormattedAddress(''));
    }

    public function testGetAddressMappingReturnData()
    {
        $address  = "Bandung, Jawa Barat, Indonesia";
        $this->assertEquals(
            ['Indonesia', 'Jawa Barat', 'Bandung'],
            $this->geolocationHelper->getAddressMapping($address)
        );
    }

    public function testGetAddressMappingWithEmptyInputReturnEmptyArray()
    {
        $this->assertEmpty($this->geolocationHelper->getAddressMapping(''));
    }

    public function testGetAddressMappingWithBlockedKeywordsInputReturnEmptyArray()
    {
        // 'Jalan' is one of blocked keywords
        $this->assertEmpty($this->geolocationHelper->getAddressMapping('Jalan'));
    }

    public function testGetAreaLevelReturnExpectedLevel()
    {
        $cityData = ['Indonesia', 'Jawa Barat', 'Bandung'];
        $this->assertEquals('city', $this->geolocationHelper->getAreaLevel($cityData));

        $provinceData = ['Indonesia', 'Jawa Barat'];
        $this->assertEquals('province', $this->geolocationHelper->getAreaLevel($provinceData));
    }

    public function testGetAreaLevelReturnEmpty()
    {
        $data = ['Indonesia', 'Jawa Barat', 'Bandung', 'Kelarik', 'Kelarik Utara', 'Desa Cira', 'RT 01'];
        $this->assertEmpty($this->geolocationHelper->getAreaLevel($data));
    }

    public function testPrepareAddressForQuery()
    {
        $data = ['Indonesia', 'Jawa Barat', 'Bandung'];
        $this->assertEquals(
            ['+Indonesia', '+Jawa +Barat', '-Kota +Bandung'],
            $this->geolocationHelper->prepareAddressForQuery($data)
        );
    }

    public function testContainsReturnTrue()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'contains'
        );
        $response = $method->invokeArgs($this->geolocationHelper, ['Jalan Setia Budi', ['Jalan']]);

        $this->assertTrue($response);
    }

    public function testContainsReturnFalse()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'contains'
        );
        $response = $method->invokeArgs($this->geolocationHelper, ['Jalan Setia Budi', ['Kota', 'Kabupaten']]);

        $this->assertFalse($response);
    }

    public function testRemoveIgnorableWords()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'removeIgnorableWords'
        );

        $response = $method->invokeArgs($this->geolocationHelper, ['kabupaten kota']);
        $this->assertEquals('kota', $response);
    }

    public function testFindSynonymWord()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'findSynonymWord'
        );

        $response = $method->invokeArgs($this->geolocationHelper, ['batam city']);
        $this->assertEquals('Kota Batam', $response);
    }

    public function testDoesContainBlockedWordsReturnTrue()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'doesContainBlockedWords'
        );

        $this->assertTrue($method->invokeArgs($this->geolocationHelper, ['Jalan']));
    }

    public function testDoesContainBlockedWordsReturnFalse()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'doesContainBlockedWords'
        );

        $this->assertFalse($method->invokeArgs($this->geolocationHelper, ['Bandung']));
    }

    public function testGetGeolocationIDReturnId()
    {
        [$suggestion, $areaGeolocation] = $this->setBandungInputProperty();
        DB::commit();

        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'getGeolocationID'
        );

        $response = $method->invokeArgs(
            $this->geolocationHelper,
            [
                'city',
                ['Indonesia', 'Jawa Barat', 'Bandung']
            ]
        );

        $this->assertEquals($areaGeolocation->id, $response);
        $this->deleteTableRecords();
    }

    public function testGetGeolocationIDReturnNull()
    {
        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'getGeolocationID'
        );

        $response = $method->invokeArgs(
            $this->geolocationHelper,
            [
                'city',
                ['Indonesia', 'Jawa Barat', 'Bandung']
            ]
        );

        $this->assertNull($response);
    }

    public function testSetConditionsWithZeroRetry()
    {
        $query = AreaGeolocation::query();
        $address = ['Indonesia', 'Jawa Barat', 'Bandung'];

        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'setConditions'
        );

        $response = $method->invokeArgs(
            $this->geolocationHelper,
            ['city', $query, $address, 0]
        );

        $expectedQuery = $query
            ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
            ->whereRaw("MATCH (`city`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)")
            ->getQuery()
            ->toSql();

        $this->assertSame($expectedQuery, $response->getQuery()->toSql());
    }

    public function testSetConditionsWithOneRetry()
    {
        $query = AreaGeolocation::query();
        $address = ['Indonesia', 'Jawa Barat', 'Bandung'];

        $method = $this->getNonPublicMethodFromClass(
            GeolocationHelper::class,
            'setConditions'
        );

        $response = $method->invokeArgs(
            $this->geolocationHelper,
            ['city', $query, $address, 1]
        );

        $expectedQuery = $query
            ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
            ->whereRaw("MATCH (`subdistrict`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)")
            ->getQuery()
            ->toSql();

        $this->assertSame($expectedQuery, $response->getQuery()->toSql());
    }

    private function setBandungInputProperty(): array
    {
        $inputKeyword = factory(InputKeyword::class)->create(['keyword' => 'Bandung']);

        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'input_id' => $inputKeyword->id,
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);

        return [
            $suggestion,
            $areaGeolocation
        ];
    }

    private function deleteTableRecords()
    {
        DB::table('area_geolocation_mapping')->delete();
        DB::table('area_geolocation')->delete();
        DB::table('search_input_keyword_suggestion')->delete();
        DB::table('search_input_keyword')->delete();
    }
}
