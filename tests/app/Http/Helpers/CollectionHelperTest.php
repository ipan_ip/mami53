<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class CollectionHelperTest extends MamiKosTestCase
{
    
    public function testGetFirstInstanceOfCollection()
    {
        $collection = $this->createRoomWithRelations();
        $owner = $collection->owners->first();
        $getFirstOwner = CollectionHelper::getFirstInstanceOfCollection($collection->owners, RoomOwner::class);
        // assert wrong instance
        $this->assertNull(CollectionHelper::getFirstInstanceOfCollection($collection->owners, User::class));
        // assert wrong collection
        $this->assertNull(CollectionHelper::getFirstInstanceOfCollection($collection->user, User::class));
        // assert success
        $this->assertEquals($owner->id, $getFirstOwner->id);
        // assert with empty data
        $this->assertNull(CollectionHelper::getFirstInstanceOfCollection([], User::class));
    }

    private function createRoomWithRelations()
    {
        $this->withoutEvents();

        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room
        ]);

        return $room;
    }
}