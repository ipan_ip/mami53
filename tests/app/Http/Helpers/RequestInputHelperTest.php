<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\RequestInputHelper;
use App\Test\MamiKosTestCase;

class RequestInputHelperTest extends MamiKosTestCase
{
    public function testGetNumericValueWithNull()
    {
        $input = null;
        $output = RequestInputHelper::getNumericValue($input);
        $this->assertEquals(0, $output);
    }
    public function testGetNumericValueWithDash()
    {
        $input = '-';
        $output = RequestInputHelper::getNumericValue($input);
        $this->assertEquals(0, $output);
    }
    public function testGetNumericValueWithWord()
    {
        $input = 'one';
        $output = RequestInputHelper::getNumericValue($input);
        $this->assertEquals(0, $output);
    }
    public function testGetNumericValueWithNumber()
    {
        $input = '345';
        $output = RequestInputHelper::getNumericValue($input);
        $this->assertEquals((int) $input, $output);
    }
}