<?php
declare(strict_types=1);

use App\Http\Helpers\RegexHelper;
use App\Test\MamiKosTestCase;

class RegexHelperTest extends MamikosTestCase
{
    public function testAlphaNumericNonUnicodeInput(){
        $this->assertRegExp(RegexHelper::alphanumericAndSpace(),'3nd4nk S03k4mt1');
        $this->assertRegExp(RegexHelper::alphanumericAndSpace(),'Endank Soekamti');
    }

    public function testAlphaNumericUnicodeInput(){
        $this->assertNotRegExp(RegexHelper::alphanumericAndSpace(),'심예슬');
        $this->assertNotRegExp(RegexHelper::alphanumericAndSpace(),'@ku s@y@n6 kamu!');
    }

    public function testAlphaOnlyeNonUnicodeInput(){
        $this->assertRegExp(RegexHelper::alphaOnlyAndSpace(),'Endank Soekamti');
    }

    public function testAlphaOnlyUnicodeInput(){
        $this->assertNotRegExp(RegexHelper::alphaOnlyAndSpace(),'심예슬');
        $this->assertNotRegExp(RegexHelper::alphaOnlyAndSpace(),'@ku s@y@n6 kamu!');
    }
}
