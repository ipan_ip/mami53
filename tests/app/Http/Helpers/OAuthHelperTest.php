<?php

namespace App\Http\Helpers;

use App\Http\Helpers\OAuthHelper;
use App\Libraries\Oauth\AuthorizationServer;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Stream\Stream;
use Illuminate\Support\Facades\Log;
use League\OAuth2\Server\AuthorizationServer as LeagueAuthorizationServer;
use Zend\Diactoros\Response as Psr7Response;
use Mockery;

class OAuthHelperTest extends MamiKosTestCase
{
    private $oauthHelper;

    public function testIssueTokenFromApiGuzzleException()
    {
        $expectedException = new Exception('expectedException');

        $mock = $this->mockAlternatively(Client::class);
        $mock->shouldReceive('request')
            ->andThrow($expectedException);
        Log::shouldReceive('warning')->with($expectedException)->once();
        Bugsnag::shouldReceive('notifyException')->once();

        $token = $this->oauthHelper->issueTokenFromApi('hanani@gmail.com', '080VCds51deexxxxxXXxxxxXxxxxxXx');

        $this->assertNull($token);
    }

    public function testIssueTokenFromApiSuccess()
    {
        $fakeResponseToken = (object) [
            'token_type'        => 'Bearer',
            'expires_in'        => 863999,
            'access_token'      => 'eyJ0eXAiOiJKV1QiLCJ',
            'refresh_token'     => 'def50200c3081c8af97',
        ];

        $mock = $this->mockAlternatively(Client::class);
        $mock->shouldReceive('request')
            ->andReturn(
                new Response(
                    200,
                    [],
                    Stream::factory(json_encode($fakeResponseToken))
                )
            );

        $token = $this->oauthHelper->issueTokenFromApi('hanani@gmail.com', '080VCds51deexxxxxXXxxxxXxxxxxXx');

        $this->assertEquals('Bearer', $token->token_type);
        $this->assertEquals($fakeResponseToken->access_token, $token->access_token);
        $this->assertEquals($fakeResponseToken->refresh_token, $token->refresh_token);
        $this->assertEquals($fakeResponseToken->expires_in, $token->expires_in);
    }

    public function testIssueTokenFromApiWithPhoneNumberSuccess()
    {
        $fakeResponseToken = (object) [
            'token_type'        => 'Bearer',
            'expires_in'        => 863999,
            'access_token'      => 'eyJ0eXAiOiJKV1QiLCJ',
            'refresh_token'     => 'def50200c3081c8af97',
        ];

        $mock = $this->mockAlternatively(Client::class);
        $mock->shouldReceive('request')
            ->andReturn(
                new Response(
                    200,
                    [],
                    Stream::factory(json_encode($fakeResponseToken))
                )
            );

        $token = $this->oauthHelper->issueTokenFromApi('081920933839', '08051deeabc72882b70c4bd4868940e8');

        $this->assertEquals('Bearer', $token->token_type);
        $this->assertNotEmpty($token->access_token);
        $this->assertNotEmpty($token->refresh_token);
    }

    public function testIssueTokenFromApiFailPasswordInvalid()
    {
        $fakeResponse = (object) [
            'error'             => 'invalid_credentials',
            'error_description' => 'The user credentials were incorrect.',
            'message'           => 'The user credentials were incorrect.',
        ];

        $mock = $this->mockAlternatively(Client::class);
        $mock->shouldReceive('request')
            ->andReturn(
                new Response(
                    401,
                    [],
                    Stream::factory(json_encode($fakeResponse))
                )
            );

        $token = $this->oauthHelper->issueTokenFromApi('hanani@gmail.com', 'invalid_password');

        $this->assertNull($token);
    }

    public function testIssueTokenForUserSuccess()
    {
        $userEntity =  factory(User::class)->create(['is_verify' => 1]);

        $expectedBody = [
            "token_type" => "Bearer",
            "expires_in" => 800,
            "access_token" => "the-access-token",
            "refresh_token" => "the-refresh-token"
        ];

        $expectedResponse = new Psr7Response();
        $expectedResponse->getBody()->write(
            json_encode($expectedBody)
        );

        $mockServer = $this->mockAlternatively(AuthorizationServer::class);
        $mockServer
            ->shouldReceive('respondAccessTokenForUser')
            ->once()
            ->andReturn($expectedResponse);

        $oauthHelper = new OAuthHelper($mockServer);

        $token = $oauthHelper->issueTokenFor($userEntity);

        $this->assertNotNull($token);
        $this->assertEquals($expectedBody, $token);
    }

    public function testIssueTokenNullWhenNotMamiServer()
    {
        $userEntity =  factory(User::class)->create(['is_verify' => 1]);

        $mockServer = $this->mockAlternatively(LeagueAuthorizationServer::class);
        $oauthHelper = new OAuthHelper($mockServer);

        $token = $oauthHelper->issueTokenFor($userEntity);

        $this->assertNull($token);
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->oauthHelper = app()->make(OAuthHelper::class);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }
}
