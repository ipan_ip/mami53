<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\MailHelper;
use App\Test\MamiKosTestCase;
use App\Entities\Notif\EmailBounce;

class MailHelperTest extends MamiKosTestCase
{
    
    public function testIsBlockedEmailByDomain()
    {
        // by the time this test is written, $blockedDomains in Mailhelper
        // contains 'facebook.com' and 'mailinator.com'
        // if at one point the requirement is changed that 'facebook.com' or 'mailinator.com'
        // is no longer a member of blockedDomains, please update this test.

        $blocked = MailHelper::isBlockedEmail('galgadot@facebook.com');
        $this->assertTrue($blocked);

        $blocked = MailHelper::isBlockedEmail('galgadot@mailinator.com');
        $this->assertTrue($blocked);
    }

    public function testIsBlockedEmailFalseByDomain()
    {
        $blocked = MailHelper::isBlockedEmail('galgadot@gmail.com');
        $this->assertFalse($blocked);

        $blocked = MailHelper::isBlockedEmail('galgadot@yahoo.com');
        $this->assertFalse($blocked);
    }

    public function testIsBlockedByEmailBounce()
    {
        factory(EmailBounce::class)->create(["email" => 'galgadot@gmail.com']);
        
        $blocked = MailHelper::isBlockedEmail('galgadot@gmail.com');
        $this->assertTrue($blocked);
    }
}