<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\OpenSslEncryptor;
use App\Test\MamiKosTestCase;

class OpenSslEncryptorTest extends MamiKosTestCase
{
    protected $key = '1234567890abcdefghijklmnopqrstuv';
    protected $iv = 'abcdef1234567890';
    protected $original = '{"value":"abcdef","count":35,"is_booking":true}';
    protected $correctlyEncrypted = 'XzksFaarcG0seT4webfq9/rd2MMI0/28VxX6FDr/pjc9uV4yXY3e2IXzmgSZYMT8';

    public function testEncryptReturnsEncryptedString() 
    {
        $encrypted = OpenSslEncryptor::encrypt($this->original, $this->key, $this->iv);
        $this->assertEquals($this->correctlyEncrypted, $encrypted, 'OpenSslEncryptor::encrypt does not correct encrypted string.');
    }

    public function testDecryptReturnsDecryptedString() {
        $decrypted = OpenSslEncryptor::decrypt($this->correctlyEncrypted, $this->key, $this->iv);
        $this->assertEquals($this->original, $decrypted, 'OpenSslEncryptor::decrypt does not correct decrypted string.');
    }
}