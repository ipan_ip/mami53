<?php

namespace App\Http\Helpers;

use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Http\Helpers\GoldplusConsultantHelper;
use Illuminate\Foundation\Testing\WithFaker;
use App\Test\Traits\GoldplusConsultantConfig;
use InvalidArgumentException;

class GoldplusConsultantHelperTest extends MamiKosTestCase
{
    use WithFaker;
    use GoldplusConsultantConfig;

    protected function setUp()
    {
        parent::setUp();
        $this->prepareAllGpLevels();
    }

    /**
     * @group UG
     * @group UG-4217
     * @group App\Http\Helpers\GoldplusConsultantHelper
     */
    public function testGetGoldplusConsultantIdsDefaultTestingEnvShouldReturnEmpty()
    {
        $this->assertEquals([], GoldplusConsultantHelper::getGoldplusConsultantIds());
    }

    /**
     * @group UG
     * @group UG-4217
     * @group App\Http\Helpers\GoldplusConsultantHelper
     */
    public function testGetGoldplusConsultantIdsConfiguredShouldNotReturnEmpty()
    {
        $expected = $this->createRandomNumbers(2);
        $this->mockConfigGpConsultantIds($expected);
        $this->assertEquals($expected, GoldplusConsultantHelper::getGoldplusConsultantIds());
    }

    /**
     * @group UG
     * @group UG-4217
     * @group App\Http\Helpers\GoldplusConsultantHelper
     */
    public function testGetRandomGoldplusConsultantIdSingleConfig()
    {
        $expected = $this->createRandomNumbers(1);
        $this->mockConfigGpConsultantIds($expected);
        $this->assertEquals($expected[0], GoldplusConsultantHelper::getRandomGoldplusConsultantId());
    }

    /**
     * @group UG
     * @group UG-4217
     * @group App\Http\Helpers\GoldplusConsultantHelper
     */
    public function testGetRandomGoldplusConsultantIdEmptyConfig()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->mockConfigGpConsultantIds([]);
        GoldplusConsultantHelper::getRandomGoldplusConsultantId();
        $this->assertTrue(false);
    }

    public function createRandomNumbers(int $count = 1): array
    {
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            // Set minimum to 1 as "0" will evaluated as empty using empty("0")
            // Hence it will be excluded when parsed from config.
            $result[] = $this->faker->unique()->numberBetween(1);
        }
        return $result;
    }
}
