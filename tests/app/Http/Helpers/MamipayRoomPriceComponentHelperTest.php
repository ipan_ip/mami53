<?php

namespace App\Http\Helpers;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;

class MamipayRoomPriceComponentHelperTest extends MamiKosTestCase
{
    public function testGetRoomEnableDownPaymentIsTrue(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $priceComponentEntity = factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $roomEntity->id,
            'component' => MamipayRoomPriceComponentType::DP,
            'is_active' => true
        ]);

        // run test
        $result = MamipayRoomPriceComponentHelper::getRoomEnableDownPayment($roomEntity);
        $this->assertTrue($result);
    }

    public function testGetRoomEnableDownPaymentIsFalse(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();

        // run test
        $result = MamipayRoomPriceComponentHelper::getRoomEnableDownPayment($roomEntity);
        $this->assertFalse($result);
    }

    public function testGetRoomEnableDownPaymentIsFalseWithRoomNull()
    {
        // run test
        $result = MamipayRoomPriceComponentHelper::getRoomEnableDownPayment(null);
        $this->assertFalse($result);
    }
}