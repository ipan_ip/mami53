<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\SendPushNotificationQueue;
use App\Jobs\SendSMSQueue;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag;
use Mockery;

/**
 *  Test BookingNotificationHelper
 *
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 *
 */
class BookingNotificationHelperTest extends MamiKosTestCase
{
    
    private const USER_ID = 1;
    private const BOOKING_ID = 1;
    private const IS_MAMIPAY_OWNER = false;
    private const TEMPLATE = [
        'title' => 'Yeay! Booking telah diteruskan ke pemilik!',
        'message' => 'Beri waktu pemilik untuk mengulas pesanan kamu atau hubungi melalui chatroom kami.',
        'scheme' => 'list_booking?id=1'
    ];
    private const CANCEL_REASON = 'Sudah dapat kost lainnya';

    protected $withEvents = true;

    protected function setUp() : void
    {
        parent::setUp();
        $this->mockAlternatively('overload:App\Libraries\SMSLibrary');
        // Spying bugsnag so it's not need to actually call bugsgnag api.
        // Speeding up local unit test run.
        Bugsnag::spy();
    }

    public function testSendNotifSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->sendNotif(self::USER_ID, self::TEMPLATE, self::IS_MAMIPAY_OWNER);
    }

    public function testNotifSuccessBookingToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifSuccessBookingToTenant();
    }

    public function testNotifSuccessBookingToTenantSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifSuccessBookingToTenant(self::USER_ID, self::BOOKING_ID));
    }

    public function testNotifCancelBookingToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifCancelBookingToTenant();
    }

    public function testNotifCancelBookingToTenantSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifCancelBookingToTenant(self::USER_ID, self::BOOKING_ID));
    }

    public function testNotifWaitingConfirmToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifWaitingConfirmToTenant();
    }

    public function testNotifWaitingConfirmToTenantSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifWaitingConfirmToTenant(self::USER_ID, self::BOOKING_ID));
    }

    public function testSmsWaitingConfirmToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->smsWaitingConfirmToTenant();
    }

    public function testSmsWaitingConfirmToTenantInvalidPhoneNumber()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(false);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmToTenant([], '8234ABS'));
    }

    public function testSmsWaitingConfirmToTenantSuccess()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(true);
        $this->mock->shouldReceive('send')->once()->andReturn(null);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmToTenant([], '082234167880'));
    }

    public function testNotifBookingHasPassedToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifBookingHasPassedToTenant();
    }

    public function testNotifBookingHasPassedToTenantSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifBookingHasPassedToTenant(self::USER_ID, self::BOOKING_ID));
    }

    public function testNotifSuccessBookingToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifSuccessBookingToOwner();
    }

    public function testNotifSuccessBookingToOwnerEmptyRoomOwner()
    {
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifSuccessBookingToOwner(null, self::BOOKING_ID));
    }

    public function testNotifSuccessBookingToOwnerSuccess()
    {
        $room = factory(Room::class)->create(['address' => null]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => self::USER_ID
        ]);

        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertTrue($notification->notifSuccessBookingToOwner($room, self::BOOKING_ID));
    }

    public function testSendSmsSuccessBookingToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->sendSmsSuccessBookingToOwner();
    }

    public function testSendSmsSuccessBookingToOwnerEmptyRoomOwner()
    {
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->sendSmsSuccessBookingToOwner(null));
    }

    public function testSendSmsSuccessBookingToOwnerInvalidUserPhoneNumber()
    {
        $room = factory(Room::class)->create(['address' => null]);
        $user = factory(User::class)->create(['phone_number' => '02394AB']);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->sendSmsSuccessBookingToOwner($room));
    }

    public function testSendSmsSuccessBookingToOwnerSuccess()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(true);

        $room = factory(Room::class)->create(['address' => null]);
        $user = factory(User::class)->create(['phone_number' => '082234167880']);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $this->expectsJobs(SendSMSQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->sendSmsSuccessBookingToOwner($room));
    }

    public function testNotifCancelBookingToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifCancelBookingToOwner();
    }

    public function testNotifCancelBookingToOwnerEmptyRoomOwner()
    {
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifCancelBookingToOwner(null, self::BOOKING_ID, self::CANCEL_REASON));
    }

    public function testNotifCancelBookingToOwnerSuccess()
    {
        $room = factory(Room::class)->create(['address' => null]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => self::USER_ID
        ]);

        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertTrue($notification->notifCancelBookingToOwner($room, self::BOOKING_ID, self::CANCEL_REASON));
    }

    public function testSendSmsCancelBookingToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->sendSmsCancelBookingToOwner();
    }

    public function testSendSmsCancelBookingToOwnerInvalidUserPhoneNumber()
    {
        $room = factory(Room::class)->create(['address' => null]);
        $user = factory(User::class)->create(['phone_number' => '02394AB']);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->sendSmsCancelBookingToOwner($room, self::CANCEL_REASON));
    }

    public function testSendSmsCancelBookingToOwnerSuccess()
    {
        $room = factory(Room::class)->create(['address' => null]);
        $user = factory(User::class)->create(['phone_number' => '082234167880']);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(true);
        $this->mock->shouldReceive('smsVerification')->once()->andReturn(true);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertTrue($notification->sendSmsCancelBookingToOwner($room, self::CANCEL_REASON));
    }

    public function testNotifWaitingConfirmToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifWaitingConfirmToOwner();
    }

    public function testNotifWaitingConfirmToOwnerEmptyRoomOwner()
    {
        $room = factory(Room::class)->create(['address' => null]);
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifWaitingConfirmToOwner($room, self::BOOKING_ID));
    }

    public function testNotifWaitingConfirmToOwnerSuccess()
    {
        $room = factory(Room::class)->create(['address' => null]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => self::USER_ID
        ]);

        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertTrue($notification->notifWaitingConfirmToOwner($room, self::BOOKING_ID));
    }

    public function testSmsWaitingConfirmToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->smsWaitingConfirmToOwner();
    }

    public function testSmsWaitingConfirmToOwnerInvalidPhoneNumber()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(false);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmToOwner([], '0283423AB'));
    }

    public function testSmsWaitingConfirmToOwnerSuccess()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(true);
        $this->mock->shouldReceive('send')->once()->andReturn(null);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmToOwner(null, '082234167880'));
    }

    public function testNotifWaitingConfirmTodayToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifWaitingConfirmTodayToOwner();
    }

    public function testNotifWaitingConfirmTodayToOwnerEmptyRoomOwner()
    {
        $room = factory(Room::class)->create(['address' => null]);
        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifWaitingConfirmTodayToOwner($room, self::BOOKING_ID));
    }

    public function testNotifWaitingConfirmTodayToOwnerSuccess()
    {
        $room = factory(Room::class)->create(['address' => null]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => self::USER_ID
        ]);

        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertTrue($notification->notifWaitingConfirmTodayToOwner($room, self::BOOKING_ID));
    }

    public function testSmsWaitingConfirmTodayToOwnerEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->smsWaitingConfirmTodayToOwner();
    }

    public function testSmsWaitingConfirmTodayToOwnerInvalidPhoneNumber()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(false);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmTodayToOwner([], '0283423AB'));
    }

    public function testSmsWaitingConfirmTodayToOwnerSuccess()
    {
        $this->mock->shouldReceive('validateIndonesianMobileNumber')->once()->andReturn(true);
        $this->mock->shouldReceive('send')->once()->andReturn(null);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->smsWaitingConfirmTodayToOwner(null, '082234167880'));
    }

    public function testNotifBookingFinsihedToTenantEmptyParams()
    {
        $this->expectException(\ArgumentCountError::class);

        $notification = BookingNotificationHelper::getInstance();
        $notification->notifBookingFinsihedToTenant();
    }

    public function testNotifBookingFinsihedToTenantSuccess()
    {
        $this->expectsJobs(SendPushNotificationQueue::class);

        $notification = BookingNotificationHelper::getInstance();
        $this->assertNull($notification->notifBookingFinsihedToTenant(self::USER_ID, [],self::BOOKING_ID));
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }
}