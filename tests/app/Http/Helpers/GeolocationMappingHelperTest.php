<?php

namespace App\Http\Helpers;

use App;
use App\Entities\Area\AreaGeolocation;
use App\Entities\Area\AreaGeolocationMapping;
use App\Entities\Room\Geolocation;
use App\Entities\Room\Room;
use App\Entities\Search\InputKeyword;
use App\Entities\Search\InputKeywordSuggestion;
use App\Test\MamiKosTestCase;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Model;

class GeolocationMappingHelperTest extends MamiKosTestCase
{
    /**
     * @var GeolocationMappingHelper|mixed
     */
    private $helper;

    protected function setUp()
    {
        parent::setUp();
        $this->helper = app()->make(GeolocationMappingHelper::class);
    }


    public function testSyncGeocodeWithDisabledTriggerReturnFalse()
    {
        $room = factory(Room::class)->create();

        $this->assertFalse($this->helper->syncGeocode($room, 'scheduler'));
        $this->assertFalse($this->helper->syncGeocode($room, 'observer'));
    }

    public function testSyncGeocodeWithNoGeolocationReturnFalse()
    {
        $room = factory(Room::class)->create();

        $this->assertFalse($this->helper->syncGeocode($room));
    }

    public function testSyncGeocodeWithNoGeocodeAreaIdsReturnFalse()
    {
        $room = $this->setBandungKosProperty();
        $this->helper->syncGeocode($room);

        $this->assertFalse($this->helper->syncGeocode($room));
    }

    public function testSyncGeocodeCreateNewRecord()
    {
        $room = $this->setBandungKosProperty();
        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);
        $this->helper->syncGeocode($room);

        $this->assertDatabaseHas('designer_geolocation_mapping',
            [
                'designer_id' => $room->id,
                'area_geolocation_id' => $areaGeolocation->id
            ]
        );
    }

    public function testRemoveGeocodeWithDisabledTriggerReturnFalse()
    {
        $room = factory(Room::class)->create();

        $this->assertFalse($this->helper->removeGeocode($room, 'scheduler'));
        $this->assertFalse($this->helper->removeGeocode($room, 'observer'));
    }

    public function testRemoveGeocodeWithNoGeolocationReturnFalse()
    {
        $room = factory(Room::class)->create();

        $this->assertFalse($this->helper->syncGeocode($room));
    }

    public function testRemoveGeocodeDeleteRecord()
    {
        $room = $this->setBandungKosProperty();
        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);

        // sync the data first to create new record
        $this->helper->syncGeocode($room);

        $this->assertDatabaseHas('designer_geolocation_mapping',
            [
                'designer_id' => $room->id,
                'area_geolocation_id' => $areaGeolocation->id
            ]
        );

        // remove the recently added record
        $this->helper->removeGeocode($room);

        $this->assertDatabaseMissing('designer_geolocation_mapping',
            [
                'designer_id' => $room->id,
                'area_geolocation_id' => $areaGeolocation->id
            ]
        );
    }

    public function testGetRoomIDsBySearchKeywordWithEmptyInputReturnEmpty()
    {
        $this->assertEmpty($this->helper->getRoomIDsBySearchKeyword(''));
    }

    public function testGetRoomIDsBySearchKeywordReturnEmpty()
    {
        $this->assertEmpty($this->helper->getRoomIDsBySearchKeyword('Test'));
    }

    public function testGetRoomIDsBySearchKeywordReturnData()
    {
        $room = $this->setBandungKosProperty();

        $this->setBandungAreaProperty();

        $response = $this->helper->getRoomIDsBySearchKeyword('Bandung');

        $this->assertEquals($room->id, $response[0]);
    }

    public function testGetRoomIDsByMultipleSearchKeyword()
    {
        $room = $this->setBandungKosProperty();

        $this->setBandungAreaProperty();

        $response = $this->helper->getRoomIDsByMultipleSearchKeyword(['Bandung']);

        $this->assertEquals($room->id, $response[0]);
    }

    public function testGetRoomIDWithinGeolocationBoundary()
    {
        $room = $this->setBandungKosProperty();

        [$suggestion, $areaGeolocation] = $this->setBandungAreaProperty();
        $response = $this->helper->getRoomIDWithinGeolocationBoundary($suggestion->geolocation->first());

        $this->assertEquals($room->id, $response[0]);
    }

    public function testGetRoomIDsBySuggestions()
    {
        $room = $this->setBandungKosProperty();

        [$suggestion, $areaGeolocation, $keyword] = $this->setBandungAreaProperty();

        $response = $this->helper->getRoomIDsBySuggestions($keyword->suggestions);
        $this->assertEquals($room->id, $response[0]);
    }

    public function testGetCoveringAreaGeocodeIDs()
    {
        $room = $this->setBandungKosProperty();
        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);

        $method = $this->getNonPublicMethodFromClass(
            GeolocationMappingHelper::class,
            'getCoveringAreaGeocodeIDs'
        );

        $response = $method->invokeArgs(
            $this->helper,
            [$room->geolocation->geolocation]
        );

        $this->assertEquals($areaGeolocation->id, $response[0]);
    }

    private function setBandungAreaProperty(): array
    {
        $inputKeyword = factory(InputKeyword::class)->create(['keyword' => 'Bandung']);

        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'input_id' => $inputKeyword->id,
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);

        factory(AreaGeolocationMapping::class)->create(
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        return [
            $suggestion,
            $areaGeolocation,
            $inputKeyword
        ];
    }

    private function setBandungKosProperty()
    {
        $room = factory(Room::class)->create();

        // the point is in bandung Area
        factory(Geolocation::class)->create(
            [
                'designer_id' => $room->id,
                'geolocation' => new Point(40.74894149554007, -73.98615270853044),
            ]
        );

        return $room;
    }
}
