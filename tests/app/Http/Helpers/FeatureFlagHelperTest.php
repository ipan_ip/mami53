<?php

namespace App\Http\Helpers;

use App\Test\MamiKosTestCase;
use App\Entities\Device\UserDevice;

class FeatureFlagHelperTest extends MamiKosTestCase
{
    protected $user;
    protected $userDevice;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(\App\User::class)->create();
        $this->userDevice = UserDevice::createDummy('device_token_dummy');
    }

    public function testIsFromWebRequest()
    {
        $this->app->user = $this->user;

        $result = FeatureFlagHelper::isFromApp();
        $this->assertFalse($result);
    }

    public function testIsFromAndroidRequest()
    {
        $this->userDevice->platform = 'android';

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isFromApp();
        $this->assertTrue($result);
    }

    public function testIsFromIosRequest()
    {
        $this->userDevice->platform = 'ios';

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isFromApp();
        $this->assertTrue($result);
    }

    public function testIsSupportFlashSaleAccessFromWeb()
    {
        $this->app->device = null;

        $result = FeatureFlagHelper::isSupportFlashSale();
        $this->assertTrue($result);
    }

    public function testIsSupportFlashSaleAccessFromIos()
    {
        $this->userDevice->platform = 'ios';

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportFlashSale();
        $this->assertFalse($result);
    }

    public function testIsSupportFlashSaleAccessFromAndroidLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID) - 1;

        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportFlashSale();
        $this->assertFalse($result);
    }

    public function testIsSupportFlashSaleAccessFromAndroidSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID);

        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportFlashSale();
        $this->assertTrue($result);
    }

    public function testIsSupportFlashSaleAccessFromAndroidHigherVersion()
    {
        $appVersion = ((int) UserDevice::MIN_FLASH_SALE_APP_VERSION_ANDROID) + 1;

        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportFlashSale();
        $this->assertTrue($result);
    }

    public function testIsFromApp()
    {
        $this->app->device = null;

        $result = FeatureFlagHelper::isFromApp();
        $this->assertFalse($result);
    }

    public function testIsSupportKosRuleFromWeb()
    {
        $this->app->device = null;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertTrue($result);
    }


    public function testIsSupportKosRuleFromAndroidLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID) - 1;

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertFalse($result);
    }

    public function testIsSupportKosRuleFromAndroidSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID);

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleFromAndroidHigherVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_ANDROID) + 1;

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleFromIosLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_IOS) - 1;

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertFalse($result);
    }

    public function testIsSupportKosRuleFromIosSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_IOS);

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleFromIosHigherVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_API_VERSION_IOS) + 1;

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRule();
        $this->assertTrue($result);
    }

    public function testIsSupportNewRecommendationCityFromWeb()
    {
        $this->app->device = null;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertTrue($result);
    }

    public function testIsSupportNewRecommendationCityFromAndroidLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID) - 1;

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertFalse($result);
    }

    public function testIsSupportNewRecommendationCityFromAndroidSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID);

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertTrue($result);
    }

    public function testIsSupportNewRecommendationCityFromAndroidHigherVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID) + 1;

        // override the value of platform and version from App
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertTrue($result);
    }

    public function testIsSupportNewRecommendationCityFromIosLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS) - 1;

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertFalse($result);
    }

    public function testIsSupportNewRecommendationCityFromIosSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS);

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertTrue($result);
    }

    public function testIsSupportNewRecommendationCityFromIosHigherVersion()
    {
        $appVersion = ((int) UserDevice::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS) + 1;

        // override the value version and make sure the platform is ios
        $this->userDevice->platform = 'ios';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportNewRecommendationCity();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleWithPhotoAndroidLowerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID) - 1;

        // override the value version and make sure the platform is android
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        $this->assertFalse($result);
    }
    
    public function testIsSupportKosRuleWithPhotoAndroidSameVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID);

        // override the value version and make sure the platform is android
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleWithPhotoAndroidNewerVersion()
    {
        $appVersion = ((int) UserDevice::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID) + 1;

        // override the value version and make sure the platform is android
        $this->userDevice->platform = 'android';
        $this->userDevice->app_version_code = $appVersion;

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleWithPhotoFromWeb()
    {
        $this->app->device = null;

        $result = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        $this->assertTrue($result);
    }

    public function testIsSupportKosRuleWithPhotoFromIos()
    {
        // override the value version and make sure the platform is android
        $this->userDevice->platform = 'ios';

        $this->app->user = $this->user;
        $this->app->device = $this->userDevice;

        $result = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        $this->assertTrue($result);
    }
}
