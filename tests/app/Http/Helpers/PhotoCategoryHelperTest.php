<?php

namespace App\Http\Helpers;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class PhotoCategoryHelperTest extends MamiKosTestCase
{

    public function testGetCategoryWithParamNull()
    {
        $testCase = PhotoCategoryHelper::getCategoryFromDescription(null);
        $this->assertEquals(PhotoCategoryHelper::OTHERS_CATEGORY, $testCase);
    }

    public function testGetCategory()
    {
        $testCase = PhotoCategoryHelper::getCategoryFromDescription('Photo kamar mandi dalam');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_KAMAR_MANDI, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('Photo kamar tidur dalam');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_KAMAR, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('foto lain lain ini');
        $this->assertEquals(PhotoCategoryHelper::OTHERS_CATEGORY, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('');
        $this->assertEquals(PhotoCategoryHelper::OTHERS_CATEGORY, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('Photokamartidurdalam');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_KAMAR, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('Photo-kamar-tidur-dalam');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_KAMAR, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('Photo-bangunan-dalam');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_BANGUNAN, $testCase);

        $testCase = PhotoCategoryHelper::getCategoryFromDescription('foto fasilitas-bersama');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_FASILITAS_BERSAMA, $testCase);

        // ignore uppercase
        $testCase = PhotoCategoryHelper::getCategoryFromDescription('PHOTO DAPUR DALAM');
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_FASILITAS_BERSAMA, $testCase);
    }

    public function testGetListCardWithCategory()
    {
        $room = factory(Room::class)->state('active')->create();
        $media = factory(Media::class)->create();

        factory(Card::class)->create(
            [
                'designer_id' => $room->id,
                'description' => 'foto kamar mandi dalam',
                'photo_id' => $media->id
            ]
        );

        $testCase = PhotoCategoryHelper::getListCardWithCategory($room);
        $this->assertEquals(PhotoCategoryHelper::CATEGORY_KAMAR_MANDI, $testCase[0]['category']);
        $this->assertEquals(
            'Foto ' . ucwords(PhotoCategoryHelper::CATEGORY_KAMAR_MANDI), 
            $testCase[0]['category_title']
        );
    }

    public function testGetListCardWithCategoryLainLain()
    {
        $room = factory(Room::class)->state('active')->create();
        $media = factory(Media::class)->create();

        factory(Card::class)->create(
            [
                'designer_id' => $room->id,
                'description' => '',
                'photo_id' => $media->id
            ]
        );

        $testCase = PhotoCategoryHelper::getListCardWithCategory($room);
        $this->assertEquals(PhotoCategoryHelper::OTHERS_CATEGORY, $testCase[0]['category']);
        $this->assertEquals(
            'Foto ' . ucwords(PhotoCategoryHelper::OTHERS_CATEGORY), 
            $testCase[0]['category_title']
        );
    }

    public function testPushIntoCategory()
    {
        $card1 = factory(Card::class)->create();
        $card1['category'] = PhotoCategoryHelper::CATEGORY_FASILITAS_BERSAMA;

        $card2 = factory(Card::class)->create();
        $card2['category'] = PhotoCategoryHelper::CATEGORY_BANGUNAN;

        $card3 = factory(Card::class)->create();

        $listCategories = [
            'building-card'   => [],
            'room-card'       => [],
            'bath-card'       => [],
            'share-card'      => [],
            'around-kos-card' => [],
            'other-card'      => []
        ];

       $listCategories = PhotoCategoryHelper::pushIntoCategory($listCategories, $card1);
       $listCategories = PhotoCategoryHelper::pushIntoCategory($listCategories, $card2);
       $listCategories = PhotoCategoryHelper::pushIntoCategory($listCategories, $card3);

       $this->assertNotEmpty($listCategories['share-card']);
       $this->assertNotEmpty($listCategories['building-card']);
       $this->assertNotEmpty($listCategories['other-card']);
       $this->assertEmpty($listCategories['bath-card']);
       $this->assertEmpty($listCategories['room-card']);
    }

}