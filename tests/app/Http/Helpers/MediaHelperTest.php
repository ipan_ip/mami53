<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\MediaHelper;
use App\Test\MamiKosTestCase;
use Config;
use Illuminate\Http\UploadedFile;

class MediaHelperTest extends MamiKosTestCase
{
    protected $pngFile;
    protected $jpegFile;
    protected $jpgFile;
    protected $gifFile;

    protected function setUp() : void
    {
        $this->pngFile  = UploadedFile::fake()->image('testFile.png', '1024', '768');
        $this->jpegFile = UploadedFile::fake()->image('testFile.jpeg', '1024', '768');
        $this->jpgFile  = UploadedFile::fake()->image('testFile.jpg', '1024', '768');
        $this->gifFile  = UploadedFile::fake()->image('testFile.gif', '60', '30');

        parent::setUp();
    }

    public function testSizeByTypeForUser()
    {
        $this->assertEquals(
            Config::get('api.media.size.user'),
            MediaHelper::sizeByType('user_photo')
        );
    }

    public function testSizeByTypeForStyle()
    {
        $this->assertEquals(
            Config::get('api.media.size.style'),
            MediaHelper::sizeByType('style_photo')
        );
    }

    public function testSizeByTypeForRoundStyle()
    {
        $this->assertEquals(
            Config::get('api.media.size.round_style'),
            MediaHelper::sizeByType('round_style_photo')
        );
    }

    public function testSizeByTypeForReview()
    {
        $this->assertEquals(
            Config::get('api.media.size.review'),
            MediaHelper::sizeByType('review_photo')
        );
    }

    public function testSizeByTypeForPhotobooth()
    {
        $this->assertEquals(
            Config::get('api.media.size.photobooth'),
            MediaHelper::sizeByType('photobooth')
        );
    }

    public function testSizeByTypeForCompany()
    {
        $this->assertEquals(
            Config::get('api.media.size.company'),
            MediaHelper::sizeByType('company_photo')
        );
    }

    public function testSizeByTypeForDefault()
    {
        $this->assertEquals(
            Config::get('api.media.size.style'),
            MediaHelper::sizeByType('test')
        );
    }

    public function testRealSizeByTypeForUser()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.user'),
            MediaHelper::realSizeByType('user_photo')
        );
    }

    public function testRealSizeByTypeForStyle()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.style'),
            MediaHelper::realSizeByType('style_photo')
        );
    }

    public function testRealSizeByTypeForRoundStyle()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.round_style'),
            MediaHelper::realSizeByType('round_style_photo')
        );
    }

    public function testRealSizeByTypeForReview()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.review'),
            MediaHelper::realSizeByType('review_photo')
        );
    }

    public function testRealSizeByTypeForPhotobooth()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.photobooth'),
            MediaHelper::realSizeByType('photobooth')
        );
    }

    public function testRealSizeByTypeForCompany()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.company'),
            MediaHelper::realSizeByType('company_photo')
        );
    }

    public function testRealSizeByTypeForDefault()
    {
        $this->assertEquals(
            Config::get('api.media.real_size.style'),
            MediaHelper::realSizeByType('testing')
        );
    }

    public function testStripExtension()
    {
        $jpg = str_replace('.jpg', '', $this->jpgFile);
        $response = MediaHelper::stripExtension($jpg);
        $this->assertEquals($jpg, $response);

        $png = str_replace('.png', '', $this->pngFile);
        $response = MediaHelper::stripExtension($png);
        $this->assertEquals($png, $response);

        $jpeg = str_replace('.jpeg', '', $this->jpegFile);
        $response = MediaHelper::stripExtension($jpeg);
        $this->assertEquals($jpeg, $response);

        $gif = str_replace('.gif', '', $this->gifFile);
        $response = MediaHelper::stripExtension($gif);
        $this->assertEquals($gif, $response);
    }

}