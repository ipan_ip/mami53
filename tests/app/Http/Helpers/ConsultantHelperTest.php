<?php

namespace App\Http\Helpers;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantMapping;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Cache;

class ConsultantHelperTest extends MamiKosTestCase
{
    
    public function testGetConsultantByAreaCityWithEmptyAreaCity()
    {
        $this->assertNull(ConsultantHelper::getConsultantByAreaCity('', 1));
    }

    public function testGetConsultantByAreaCityWithEmptyMapping()
    {
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn([]);

        $this->assertNull(ConsultantHelper::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetConsultantByAreaCityWithConsultantNotFound()
    {
        Cache::shouldReceive('get')
            ->once()
            ->with(ConsultantHelper::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Jakarta' => ['mamirooms' => '']]);

        $this->assertNull(ConsultantHelper::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testGetConsultantByAreaCityWithConsultantFound()
    {
        $consultant = factory(Consultant::class)->create();
        Cache::shouldReceive('get')
            ->once()
            ->with(ConsultantHelper::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn(['Surabaya' => ['mamirooms' => [$consultant->id]]]);

        $this->assertNotNull(ConsultantHelper::getConsultantByAreaCity('Surabaya', 1));
    }

    public function testRefreshCache(): void
    {
        $mapping = factory(ConsultantMapping::class)->create([
            'is_mamirooms' => true
        ]);
        $expected = [
            $mapping->area_city => [
                'mamirooms' => [$mapping->consultant_id],
                'non-mamirooms' => []
            ]
        ];
        Cache::shouldReceive('rememberForever')
            ->once()
            ->withAnyArgs();
        Cache::shouldReceive('get')
            ->once()
            ->with(Consultant::CACHE_KEY_CONSULTANT_MAPPING)
            ->andReturn($expected);
        $actual = ConsultantHelper::refreshCache();
        $this->assertEquals($expected, $actual);
    }
}
