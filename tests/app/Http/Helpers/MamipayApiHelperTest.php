<?php

namespace Test\App\Http\Helpers;

use App\Http\Helpers\MamipayApiHelper;
use App\Test\MamiKosTestCase;
use ErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery;
use stdClass;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MamipayApiHelperTest extends MamiKosTestCase
{
    const MEDIA_UPLOAD = 'media/upload';
    const NON_MEDIA_UPLOAD = 'nonmedia/upload';

    protected function setUp() : void
    {
        $this->clientMock = Mockery::mock('overload:' . Client::class);
        parent::setUp();
    }

    public function testMamipaySetAuthorizationHeader()
    {
        $authHead = MamipayApiHelper::mamipaySetAuthorizationHeader('GET', 'contract', 10);

        // the auth token is calculated using now() timestamp so it's hard to examine
        // the validity of the value. we just make sure that every keys are exists in requests.

        $this->assertArrayHasKey('X-GIT-PF', $authHead);
        $this->assertArrayHasKey('Authorization', $authHead);
        $this->assertArrayHasKey('X-GIT-Time', $authHead);
    }

    public function testMakeRequestWithNonMediaUploadNonPdfWithRequestDataAndSuccess()
    {
        $requestData = Mockery::mock(stdClass::class);
        $requestData->shouldReceive('all')->andReturn(['data' => 'data']);

        $this->clientMock->shouldReceive('post')->andReturn(
            new Response(
                $status = 200,
                $headers = [],
                $body = '{
                "status_code": "200",
                "status_message": "Success"
            }'
            )
        );
        $result = MamipayApiHelper::makeRequest(
            'post',
            $requestData,
            self::NON_MEDIA_UPLOAD,
            123
        );
        $this->assertNotEmpty($result);
    }

    public function testMakeRequestWithNonMediaUploadNonPdfWithRequestDataAllEmptyAndSuccess()
    {
        $requestData = Mockery::mock(stdClass::class);
        $requestData->shouldReceive('all')->andReturn(null);

        $this->clientMock->shouldReceive('post')->andReturn(
            new Response(
                $status = 200,
                $headers = [],
                $body = '{
                "status_code": "200",
                "status_message": "Success"
            }'
            )
        );
        $result = MamipayApiHelper::makeRequest(
            'post',
            $requestData,
            self::NON_MEDIA_UPLOAD,
            123
        );
        $this->assertNotEmpty($result);
    }

    public function testMakeRequestWithNonMediaUploadNonPdfWithoutRequestDataAndSuccess()
    {
        $this->clientMock->shouldReceive('post')->andReturn(
            new Response(
                $status = 200,
                $headers = [],
                $body = '{
                "status_code": "200",
                "status_message": "Success"
            }'
            )
        );
        $result = MamipayApiHelper::makeRequest(
            'post',
            null,
            self::NON_MEDIA_UPLOAD,
            123
        );
        $this->assertNotEmpty($result);
    }

    public function testMakeRequestWithNonMediaUploadPdfWithoutRequestDataAndSuccess()
    {
        $this->clientMock->shouldReceive('get')->andReturn(
            new Response(
                $status = 200,
                $headers = [
                    'Content-Type' => 'application/pdf'
                ],
                $body = random_bytes(10)
            )
        );
        $result = MamipayApiHelper::makeRequest(
            'get',
            null,
            self::NON_MEDIA_UPLOAD,
            123
        );
        $this->assertNull($result);
    }

    public function testMakeRequestWithMediaAndSuccess()
    {
        $mediaMock = Mockery::mock(stdClass::class);
        $mediaMock->shouldReceive('getPathName')->andReturn('/');
        $mediaMock->shouldReceive('getClientOriginalName')->andReturn('name');
        $requestData = Mockery::mock(stdClass::class);
        $requestData->shouldReceive('file')->andReturn($mediaMock);
        $requestData->shouldReceive('input')->andReturn('mock');

        $this->clientMock->shouldReceive('post')->andReturn(
            new Response(
                $status = 200,
                $headers = [],
                $body = '{
                "status_code": "200",
                "status_message": "Success"
            }'
            )
        );
        $result = MamipayApiHelper::makeRequest(
            'post',
            $requestData,
            self::MEDIA_UPLOAD,
            123
        );
        $this->assertNotEmpty($result);
    }
}
