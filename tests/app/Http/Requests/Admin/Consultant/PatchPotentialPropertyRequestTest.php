<?php
namespace App\Http\Requests\Admin\Consultant;

use App\Entities\Consultant\PotentialProperty;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Testing\WithFaker;

class PatchPotentialPropertyRequestTest extends MamiKosTestCase
{
    use WithFaker;

    protected $request;

    protected function setUp() : void
    {
        parent::setUp();
        $this->request = new PatchPotentialPropertyRequest();
    }

    public function testRulesWithValidProperty(): void
    {
        $folupStats = [
            PotentialProperty::FOLLOWUP_STATUS_NEW,
            PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
            PotentialProperty::FOLLOWUP_STATUS_PAID,
            PotentialProperty::FOLLOWUP_STATUS_APPROVED,
            PotentialProperty::FOLLOWUP_STATUS_REJECTED
        ];

        $offeredProd = [
            PotentialProperty::PRODUCT_GP1,
            PotentialProperty::PRODUCT_GP2,
            PotentialProperty::PRODUCT_GP3
        ];

        foreach ($folupStats as $stat) {
            foreach ($offeredProd as $offer) {
                $params = $this->getValidParams();
                $params['followup_status'] = $stat;
                $params['offered_product'] = [$offer];

                $validator = $this->makeValidator($params);

                $this->assertFalse($validator->fails());
                $this->assertEmpty($validator->errors()->all());
            }
        }
    }

    public function testRulesWithInvalidFollowupStatus(): void
    {
        $params = $this->getValidParams();
        $params['followup_status'] = 'good';
        $validator = $this->makeValidator($params);

        $this->assertTrue($validator->fails());
    }

    public function testRulesWithInvalidOfferedProduct(): void
    {
        $params = $this->getValidParams();
        $params['offered_product'] = ['nice'];
        $validator = $this->makeValidator($params);

        $this->assertTrue($validator->fails());
    }

    private function getValidParams()
    {
        return [
            'followup_status' => 'new',
            'is_priority' => $this->faker->boolean,
            'remark' => $this->faker->sentence(10),
            'offered_product' => ['gp1']
        ];
    }

    private function makeValidator(array $params)
    {
        return Validator::make(
            $params,
            $this->request->rules(),
            $this->request->messages()
        );
    }
}