<?php

namespace App\Http\Requests\Admin\AbTest;

use App\Test\MamiKosTestCase;
use App\User;

class ControlRequestTest extends MamiKosTestCase
{
    public function testAuthorizeWithNoUser()
    {
        $request = new ControlRequest([]);
        $actual = $request->authorize();
        $this->assertFalse($actual);
    }

    public function testAuthorize()
    {
        $user = $this->mock(User::class);
        $request = $this->mock(ControlRequest::class)->makePartial();
        $request->shouldReceive('user')->andReturn($user);
        $user->shouldReceive('can')->with('access-abtest')->andReturn(true);
        $actual = $request->authorize();
        $this->assertTrue($actual);
    }

    public function testRules()
    {
        $request = new ControlRequest([]);
        $this->assertEmpty($request->rules());
    }
}
