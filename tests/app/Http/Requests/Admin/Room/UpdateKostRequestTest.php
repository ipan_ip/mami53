<?php

namespace App\Http\Requests\Admin\Property;

use App\Http\Requests\Admin\Room\UpdateKostRequest;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Validator;

class UpdateKostRequestTest extends MamiKosTestCase
{
    use WithFaker;

    protected $request;

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = new UpdateKostRequest();
        $this->setUpFaker();
    }

    /**
    * @group PMS-495
    */    
    public function testRules()
    {
        $validator = Validator::make(
            [
                'name' => 'Kost Name',
                'gender' => 1,
                'building_year' => null,
                'description' => null,
                'remark' => null,
                'size' => '4x4',
                'room_count' => rand(1, 499),
                'room_available' => 0,
                'address' => 'Alamat bakso',
                'latitude' => rand(-90, 90),
                'longitude' => rand(-180, 180),
                'province' => 'DI Yogyakarta',
                'area_city' => 'Sleman',
                'area_subdistrict' => 'Depok',
                'area_big' => null,
                'guide' => null,
                'price_daily' => 0,
                'price_weekly' => 0,
                'price_monthly' => 10000,
                'price_yearly' => 0,
                'price_quarterly' => 0,
                'price_semiannually' => 0,
                'price_remark' => null,
                'min_payment' => null,
                'additional_cost' => [['name' => 'parkir', 'price' => 1000]],
                'deposit' => 0,
                'dp' => $this->faker->randomElement([0, 10, 20, 30, 40, 50]),
                'fine_price' => rand(0, 1000),
                'fine_type' => $this->faker->randomElement(['day', 'week', 'month']),
                'fine_length' => rand(1, 100),
                'facility_share_ids' => null,
                'facility_room_ids' => [1, 2, 3, 4],
                'facility_bath_ids' => null,
                'facility_park_ids' => null,
                'facility_other_ids' => null,
                'kos_rule_ids' => null,
                'kos_rule_photos' => null,
                'owner_name' => 'Owner 1',
                'owner_phone' => '0812345678',
                'manager_name' => 'Manager 1',
                'manager_phone' => '081111111',
                'property_id' => null,
                'unit_type' => null,
            ],
            $this->request->rules()
        );
        $this->assertEmpty($validator->errors()->toArray());
    }

    /**
    * @group PMS-495
    */    
    public function testFailedValidation()
    {
        $validator = Validator::make(
            [
                'name' => 'Kost Name',
                'gender' => 1,
                'building_year' => null,
                'description' => null,
                'remark' => null,
                'size' => '4x4',
                'room_count' => rand(1, 499),
                'room_available' => 0,
                'address' => 'Alamat bakso',
                'latitude' => rand(-90, 90),
                'longitude' => rand(-180, 180),
                'province' => 'DI Yogyakarta',
                'area_city' => 'Sleman',
                'area_subdistrict' => 'Depok',
                'area_big' => null,
                'guide' => null,
                'price_daily' => 0,
                'price_weekly' => 0,
                'price_monthly' => 10000,
                'price_yearly' => 0,
                'price_quarterly' => 0,
                'price_semiannually' => 0,
                'price_remark' => null,
                'min_payment' => null,
                'additional_cost' => [['name' => 'parkir', 'price' => 1000]],
                'deposit' => 0,
                'dp' => $this->faker->randomElement([0, 10, 20, 30, 40, 50]),
                'fine_price' => rand(0, 1000),
                'fine_type' => $this->faker->randomElement(['day', 'week', 'month']),
                'fine_length' => rand(1, 100),
                'facility_share_ids' => null,
                'facility_room_ids' => [1, 2, 3, 4],
                'facility_bath_ids' => null,
                'facility_park_ids' => null,
                'facility_other_ids' => null,
                'kos_rule_ids' => null,
                'kos_rule_photos' => null,
                'owner_name' => 'Owner 1',
                'owner_phone' => '0812345678',
                'manager_name' => 'Manager 1',
                'manager_phone' => '081111111',
                'property_id' => null,
                'unit_type' => null,
            ],
            $this->request->rules()
        );
        $reflection = new \ReflectionClass(get_class($this->request));
        $method = $reflection->getMethod('failedValidation');
        $method->setAccessible(true);
        $method->invokeArgs($this->request, [$validator]);

        $this->assertEquals($validator, $this->request->validator);
    }
}
