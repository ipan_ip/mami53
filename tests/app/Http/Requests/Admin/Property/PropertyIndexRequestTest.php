<?php

namespace App\Http\Requests\Admin\Property;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Validator;

class PropertyIndexRequestTest extends MamiKosTestCase
{
    protected $request;

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = new PropertyIndexRequest([]);
    }

    public function testAuthorize(): void
    {
        $this->assertTrue($this->request->authorize());
    }

    public function testRulesShouldAllowNullForAllFields(): void
    {
        $validator = Validator::make(
            [
                'property_name' => null,
                'owner_name' => null,
                'owner_phone_number' => null,
                'levels.*' => [null]
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertEmpty($validator->errors()->toArray());
    }
}
