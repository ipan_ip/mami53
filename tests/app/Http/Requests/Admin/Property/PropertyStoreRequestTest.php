<?php

namespace App\Http\Requests\Admin\Property;

use App\Repositories\UserDataRepository;
use App\Test\MamiKosTestCase;
use App\Repositories\UserDataRepositoryEloquent;
use App\User;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Support\Facades\Validator;

class PropertyStoreRequestTest extends MamiKosTestCase
{
    protected $request;

    protected function setUp(): void
    {
        parent::setUp();

        $userDataRepository = app()->make(UserDataRepositoryEloquent::class);
        $this->request = new PropertyStoreRequest($userDataRepository);
    }

    /**
    * @group PMS-495
    */
    public function testAuthorize(): void
    {
        $this->assertTrue($this->request->authorize());
    }

    /**
    * @group PMS-495
    */    
    public function testRulesShouldAllowNullForAllFields(): void
    {
        $phoneNumber = '08123456789';
        $owner = factory(User::class)->state('owner')->create([
            'phone_number' => $phoneNumber
        ]);
        $validator = Validator::make(
            [
                'property_name' => 'Properti 1',
                'owner_phone_number' => $phoneNumber,
                'owner_id' => $owner->id,
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertEmpty($validator->errors()->toArray());
    }

    /**
    * @group PMS-495
    */    
    public function testGetOwner()
    {
        $phoneNumber = '08123456789';
        $owner = factory(User::class)->state('owner')->create([
            'phone_number' => $phoneNumber
        ]);
        $this->request->owner_phone_number = $phoneNumber;

        $this->assertEquals($owner->id, $this->request->getOwner()->id);
    }

    /**
    * @group PMS-495
    */    
    public function testGetOwnerId()
    {
        $phoneNumber = '08123456789';
        $owner = factory(User::class)->state('owner')->create([
            'phone_number' => $phoneNumber
        ]);
        $this->request->owner_phone_number = $phoneNumber;

        $this->assertEquals($owner->id, $this->request->getOwnerId());
    }

    public function testPrepareForValidation()
    {
        $request = $this->mock(PropertyStoreRequest::class)->makePartial();

        $method = $this->getNonPublicMethodFromClass(PropertyStoreRequest::class, 'prepareForValidation');

        $request->shouldReceive('merge')->andReturn(null)->once();
        $request->shouldReceive('getOwnerId')->andReturn(null)->once();
        $method->invokeArgs($request, []);
    }

    public function testFailedValidation()
    {
        $request = $this->mock(PropertyStoreRequest::class)->makePartial();

        $method = $this->getNonPublicMethodFromClass(PropertyStoreRequest::class, 'failedValidation');

        $mock = $this->mock(ValidationValidator::class);
        $request->shouldNotReceive('anything')->andSet('validator', $mock);
        $method->invokeArgs($request, [$mock]);
    }
}
