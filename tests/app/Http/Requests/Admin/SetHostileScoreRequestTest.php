<?php

namespace App\Http\Requests\Admin;

use App\Test\MamiKosTestCase;
use App\User;

class SetHostileScoreRequestTest extends MamiKosTestCase
{
    public function testRulesShouldRejectInvalidOwnerId(): void
    {
        $request = new SetHostileScoreRequest(['id' => 0]);
        $this->validateFormRequest($request);

        $this->assertEquals('Owner tidak ditemukan! Coba lagi nanti', $request->failedValidator->errors()->first('id'));
    }

    public function testRulesShouldRejectNonOwnerId(): void
    {
        $user = factory(User::class)->state('tenant')->create();
        $request = new SetHostileScoreRequest(['id' => $user->id]);
        $this->validateFormRequest($request);

        $this->assertEquals('Owner tidak ditemukan! Coba lagi nanti', $request->failedValidator->errors()->first('id'));
    }

    public function testRulesShouldNotRejectOwnerId(): void
    {
        $user = factory(User::class)->state('owner')->create();
        $request = new SetHostileScoreRequest(['id' => $user->id]);
        $this->validateFormRequest($request);

        $this->assertEmpty($request->failedValidator->errors()->first('id'));
    }

    public function testRulesShouldRetrieveOwner(): void
    {
        $expected = factory(User::class)->state('owner')->create();
        $request = new SetHostileScoreRequest(['id' => $expected->id]);
        $this->validateFormRequest($request);

        $this->assertTrue(
            $request->owner->is($expected)
        );
    }
}
