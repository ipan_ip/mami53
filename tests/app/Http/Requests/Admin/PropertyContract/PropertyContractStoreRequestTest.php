<?php

namespace App\Http\Requests\Admin\PropertyContract;

use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class PropertyContractStoreRequestTest extends MamiKosTestCase
{
    protected $request;

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = new PropertyContractStoreRequest([]);
    }

    public function testAuthorize(): void
    {
        $this->assertTrue($this->request->authorize());
    }

    public function testRulesShouldRequire(): void
    {
        $validator = Validator::make([], $this->request->rules(), $this->request->messages());

        $this->assertEquals(
            [
                'property_level_id' => ['Property Level belum diisi'],
                'joined_at' => ['Waktu bergabung belum diisi'],
                'total_package' => ['Total package belum diisi']
            ],
            $validator->errors()->toArray()
        );
    }

    public function testPrepareForValidationShouldPass()
    {
        $request = new PropertyContractStoreRequest([
            'property_level_id' => 5,
            'total_package' => 5
        ]);
        $this->validateFormRequest($request);
        $this->assertEquals(date('Y-m-d H:i:s'), $request->joined_at);
    }
}
