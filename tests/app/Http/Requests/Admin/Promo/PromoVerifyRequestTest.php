<?php

namespace App\Http\Requests\Admin\Promo;

use App\Entities\Promoted\Promotion;
use App\Test\MamiKosTestCase;

class PromoVerifyRequestTest extends MamiKosTestCase
{
    public function testRules()
    {
        $request = new PromoVerifyRequest(['is_verify' => Promotion::VERIFY]);
        $this->validateFormRequest($request);

        $this->assertTrue($request->isValidationFailed());
        $this->assertNotEmpty($request->failedValidator->errors()->first());
        $this->assertNull($request->getPromo());
    }
}
