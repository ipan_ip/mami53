<?php

namespace App\Http\Requests\Admin\Premium;

use App\Entities\Consultant\Consultant;
use App\Entities\Premium\PremiumPlusUser;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PremiumPlusUserRequestTest extends MamiKosTestCase
{
    private function mockRequestHelper(PremiumPlusUserRequest $request)
    {
        $this->app->instance('request', $request);
    }

    private function validate(PremiumPlusUserRequest $request)
    {
        $this->mockRequestHelper($request);
        $this->validateFormRequest($request);
    }

    public function testGetConsultant()
    {
        $consultant = factory(Consultant::class)->create();
        $request = new PremiumPlusUserRequest(['consultant_id' => $consultant->id]);

        $this->mockRequestHelper($request);
        $this->assertTrue($request->getConsultant()->is($consultant));
    }

    public function testGetUserDataWithConsultant()
    {
        $user = factory(User::class)->create();
        $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);

        $request = new PremiumPlusUserRequest(['consultant_id' => $consultant->id]);
        $actual = $request->getUserData($user->phone_number, 'consultant');

        $this->assertTrue($actual->is($user));
    }

    public function testGetOwner()
    {
        $user = factory(User::class)->create();
        $consultant = factory(Consultant::class)->create(['user_id' => $user->id]);

        $request = new PremiumPlusUserRequest(['consultant_id' => $consultant->id, 'admin_phone' => $user->phone_number]);
        $actual = $request->getUserData($user->phone_number, 'consultant');

        $this->validate($request);
        $actual = $request->getOwner();

        $this->assertTrue($actual->is($user));
    }

    public function testGetRoom()
    {
        $kost = factory(Room::class)->create();
        $request = new PremiumPlusUserRequest(['designer_id' => $kost->id]);

        $this->mockRequestHelper($request);
        $this->assertTrue($request->getRoom()->is($kost));
    }

    public function testRulesWithInvalidConsultantId()
    {
        $request = new PremiumPlusUserRequest(['consultant_id' => 0]);

        $this->validate($request);

        $actual = $request->failedValidator->errors()->first('consultant_id');
        $expected = 'The selected consultant id is invalid.';
        $this->assertEquals($expected, $actual);
    }

    public function testRulesWithInvalidAdminPhone()
    {
        $request = new PremiumPlusUserRequest([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'admin_phone' => '0812345678'
        ]);

        $this->validate($request);

        $actual = $request->failedValidator->errors()->first('admin_phone');
        $expected = 'Owner phone not found';
        $this->assertEquals($expected, $actual);
    }

    public function testRulesWithInvalidDesignerId()
    {
        $request = new PremiumPlusUserRequest([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'admin_phone' => '0812345678',
            'designer_id' => factory(Room::class)->create(['deleted_at' => Carbon::now()])->id
        ]);

        $this->validate($request);

        $actual = $request->failedValidator->errors()->first('designer_id');
        $expected = 'Room not found';
        $this->assertEquals($expected, $actual);
    }

    public function testRulesWithInvalidGuaranteedRoom()
    {
        $request = new PremiumPlusUserRequest([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'admin_phone' => '0812345678',
            'designer_id' => factory(Room::class)->create(['deleted_at' => Carbon::now()])->id,
            'guaranteed_room' => 5,
            'registered_room' => 3
        ]);

        $this->validate($request);

        $actual = $request->failedValidator->errors()->first('guaranteed_room');
        $expected = 'Guaranteed room must less than Registered room';
        $this->assertEquals($expected, $actual);
    }
}
