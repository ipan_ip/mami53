<?php

namespace App\Http\Requests;

use App\Test\MamiKosTestCase;

class BookingAdminCheckinRequestTest extends MamiKosTestCase
{

    public function testAuthorize()
    {
        $bookingAdminCheckinRequest = new BookingAdminCheckinRequest();
        $this->assertTrue($bookingAdminCheckinRequest->authorize());
    }

    public function testRules()
    {
        $bookingAdminCheckinRequest = new BookingAdminCheckinRequest();
        $expected = [
            'datetime_checkin' => 'required|date:Y-m-d H:i:s',
            'booking_user_id' => 'required'
        ];

        $this->assertEquals($expected, $bookingAdminCheckinRequest->rules());
    }

    public function testGetBookingUser()
    {
        $bookingAdminCheckinRequest = new BookingAdminCheckinRequest();

        $this->assertNull($bookingAdminCheckinRequest->getBookingUser());
    }
}
