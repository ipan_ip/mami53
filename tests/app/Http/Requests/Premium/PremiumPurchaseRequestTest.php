<?php

namespace App\Http\Requests\Premium;

use App\Test\MamiKosTestCase;

class PremiumPurchaseRequestTest extends MamiKosTestCase
{
    public function testRulesWithPremiumPackageId()
    {
        $request = new PremiumPurchaseRequest(['premium_package_id' => '']);
        $this->instance('request', $request);
        $this->validateFormRequest($request);
        $this->assertEquals('Paket premium tidak ditemukan', $request->failedValidator->errors()->first('premium_package_id'));
    }
}
