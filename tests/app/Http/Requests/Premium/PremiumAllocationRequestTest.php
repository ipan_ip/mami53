<?php

namespace App\Http\Requests\Premium;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Validator;

class PremiumAllocationRequestTest extends MamiKosTestCase
{
    public function testAuthorize(): void
    {
        $request = new PremiumAllocationRequest();
        $this->assertTrue($request->authorize());
    }

    // Comment because check allocation minimum now in repository
    // not in validator
    // public function testBalanceAllocationUnderMinimum(): void
    // {
    //     $request = new PremiumAllocationRequest();
    //     $validator = Validator::make(
    //         [
    //             'allocation' => 1000
    //         ],
    //         $request->rules(),
    //         $request->messages()
    //     );

    //     $this->assertTrue($validator->fails());
    // }

    public function testBalanceAllocationEmptyString(): void
    {
        $request = new PremiumAllocationRequest();
        $validator = Validator::make(
            [
                'allocation' => ' '
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertTrue($validator->fails());
    }

    public function testBalanceAllocationString(): void
    {
        $request = new PremiumAllocationRequest();
        $validator = Validator::make(
            [
                'allocation' => 'abc'
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertTrue($validator->fails());
    }

    public function testBalanceAllocationSuccess(): void
    {
        $request = new PremiumAllocationRequest();
        $validator = Validator::make(
            [
                'allocation' => 5000
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertFalse($validator->fails());
    }

}