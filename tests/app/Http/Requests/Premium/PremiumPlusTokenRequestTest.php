<?php

namespace App\Http\Requests\Premium;

use App\Test\MamiKosTestCase;

class PremiumPlusTokenRequestTest extends MamiKosTestCase
{
    public function testAuthorize()
    {
        $request = new PremiumPlusTokenRequest([]);
        $this->assertTrue($request->authorize());
    }

    public function testRulesWithMissingInvoiceNumber()
    {
        $request = new PremiumPlusTokenRequest([]);
        $this->validateFormRequest($request);

        $this->assertEquals('Invoice tidak ditemukan', $request->failedValidator->errors()->first('invoice_number'));
    }
}
