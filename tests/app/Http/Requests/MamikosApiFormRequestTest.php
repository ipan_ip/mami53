<?php

namespace App\Http\Requests;

use App\Test\MamiKosTestCase;
use Mockery;
use stdClass;

class MamikosApiFormRequestTest extends MamiKosTestCase
{
    public function testIsValidationFailedShouldReturnTrue(): void
    {
        $validator = Mockery::mock(stdClass::class);
        $form = new MamikosApiFormRequest();
        $form->failedValidator = $validator;
        $validator->shouldReceive('fails')
            ->once()
            ->andReturn(true);

        $actual = $form->isValidationFailed();

        $this->assertTrue($actual);
    }

    public function testIsValidationFailedShouldReturnFalse(): void
    {
        $validator = Mockery::mock(stdClass::class);
        $form = new MamikosApiFormRequest();

        $actual = $form->isValidationFailed();

        $this->assertFalse($actual);
    }
}
