<?php
namespace App\Http\Requests;

use App\Test\MamiKosTestCase;
use App\Http\Requests\GoldplusStatisticRequest;
use Validator;

class GoldplusStatisticRequestTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Requests/GoldplusStatisticRequest
     */
    public function testValidationGoldplusStatisticRequestReturnFails()
    {
        $request = new GoldplusStatisticRequest();
        $data = [];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
    }
    
    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Requests/GoldplusStatisticRequest
     */
    public function testValidationGoldplusStatisticRequestReturnInvalidParam()
    {
        $request = new GoldplusStatisticRequest();
        $data = [
            'filter_list' => mt_rand()
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());
        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('goldplus-statistic.error.filter.filter_not_valid'), $error[0]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Http/Requests/GoldplusStatisticRequest
     */
    public function testValidationGoldplusStatisticRequestReturnInvalidParamFilter()
    {
        $request = new GoldplusStatisticRequest();
        $data = [
            'filter_list' => str_random()
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());
        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('goldplus-statistic.error.filter.selected_filter_not_valid'), $error[0]);
    }
}
