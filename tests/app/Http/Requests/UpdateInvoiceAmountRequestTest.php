<?php

namespace App\Http\Requests;

use App\Test\MamiKosTestCase;

class UpdateInvoiceAmountRequestTest extends MamiKosTestCase
{
    public function testAuthorize()
    {
        $request = new UpdateInvoiceAmountRequest([]);

        $this->assertTrue($request->authorize());
    }

    public function testRules()
    {
        $request = new UpdateInvoiceAmountRequest([]);

        $this->validateFormRequest($request);
        $this->assertEquals('The amount field is required.', $request->failedValidator->errors()->first('amount'));
    }
}
