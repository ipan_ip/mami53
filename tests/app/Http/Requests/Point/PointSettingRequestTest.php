<?php

namespace App\Http\Requests\Point;

use App\Entities\Point\PointActivity;
use App\Entities\Point\PointSetting;
use App\Http\Requests\Point\PointSettingRequest;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class PointSettingRequestTest extends MamiKosTestCase
{
    use WithFaker;

    public function testAuthorize(): void
    {
        $request = new PointSettingRequest();
        $this->assertTrue($request->authorize());
    }

    public function testValidation_WithFixEarnPointValue_ShouldWork()
    {
        $request = new PointSettingRequest([
            100 => [
                PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY => [
                    1 => [
                        'received_each' => $this->faker->numberBetween(1, 100),
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => $this->faker->numberBetween(1, 10000)
                    ]
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testValidation_WithPercentageEarnPointValue_Below100Percent_ShouldWork()
    {
        $request = new PointSettingRequest();
        $request->initialize([
            100 => [
                PointActivity::TENANT_PAID_BOOKING_ACTIVITY => [
                    1 => [
                        'received_each' => $this->faker->numberBetween(1, 100),
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => $this->faker->numberBetween(1, 10000)
                    ]
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testValidation_WithPercentageEarnPointValue_Exceed100Percent_ShouldWork()
    {
        $request = new PointSettingRequest();
        $request->initialize([
            100 => [
                PointActivity::TENANT_PAID_BOOKING_ACTIVITY => [
                    1 => [
                        'received_each' => $this->faker->numberBetween(1, 100),
                        'limit_type' => PointSetting::LIMIT_MONTHLY,
                        'limit' => $this->faker->numberBetween(1, 10000)
                    ]
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }
}