<?php

namespace App\Http\Requests;

use App\Entities\Consultant\PotentialTenantRemarks;
use App\Entities\Mamipay\MamipayContract;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Validator;

class PotentialTenantUpdateOrCreateRequestTest extends MamiKosTestCase
{
    use WithFaker;

    protected $request;

    protected function setUp() : void
    {
        parent::setUp();
        $this->request = new PotentialTenantUpdateOrCreateRequest();
    }

    public function testAuthorize(): void
    {
        $isAuthorized = $this->request->authorize();
        $this->assertTrue($isAuthorized);
    }

    public function testRulesWithMissingRemarksCategoryShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_note' => 'Heyo heyo',
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksMessageForAdminShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksHasActiveContractShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_HAS_ACTIVE_CONTRACT,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksHasBookingDataShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_HAS_BOOKING_DATA,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksHasPaidToOwnerShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_HAS_PAID_TO_OWNER,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksRejectPaymentToMamipayShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksDoesNotInhabitRoomShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_DOES_NOT_INHABIT_ROOM,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksWantToConfirmToOwnerShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_WANT_TO_CONFIRM_TO_OWNER,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksPhoneNumberUnreachableShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_PHONE_NUMBER_UNREACHABLE,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksChatUnresponsiveShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_CHAT_UNRESPONSIVE,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksOtherShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_OTHER,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithInvalidRemarksShouldFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => 'haihohu',
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );
    
        $this->assertTrue($validator->fails());
        $this->assertEquals('The selected remarks category is invalid.', $validator->errors()->first());
    }

    public function testRulesWithMissingRemarksNoteShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithRemarksNoteTooLongShouldFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_note' => $this->faker->regexify('\w{201}'),
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertTrue($validator->fails());
        $this->assertEquals('Maksimal 200 karakter', $validator->errors()->first());
    }

    public function testRulesWithDurationUnit3MonthShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnit6MonthShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_6MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnitDayShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_DAY,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnitMonthShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_MONTH,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnitWeekShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_WEEK,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnitYearShouldNotFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_YEAR,
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    public function testRulesWithDurationUnitInvalidShouldFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => 'test',
                'scheduled_date' => '2020-05-06'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertTrue($validator->fails());
        $this->assertEquals('The selected duration unit is invalid.', $validator->errors()->first());
    }

    public function testRulesWithMissingScheduledDateShouldFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_3MONTH,
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertTrue($validator->fails());
        $this->assertEquals('The scheduled date field is required.', $validator->errors()->first());
    }

    public function testRulesWithNonDateAsScheduledDateShouldFail(): void
    {
        $validator = Validator::make(
            [
                'remarks_category' => PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                'remarks_note' => 'Hello',
                'duration_unit' => MamipayContract::UNIT_3MONTH,
                'scheduled_date' => 'Heyo'
            ],
            $this->request->rules(),
            $this->request->messages()
        );

        $this->assertTrue($validator->fails());
        $this->assertEquals('The scheduled date is not a valid date.', $validator->errors()->first());
    }

    public function testMessages(): void
    {
        $this->assertEquals(
            [
                'remarks_note.max' => __('api.input.remarks_note.max')
            ],
            $this->request->messages()
        );
    }
}
