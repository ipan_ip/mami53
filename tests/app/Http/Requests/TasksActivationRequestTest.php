<?php

namespace App\Http\Requests;

use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\Consultant;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Auth;

class TasksActivationRequestTest extends MamiKosTestCase
{
    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();

        $this->consultant = factory(User::class)->create();
        factory(Consultant::class)->create(['user_id' => $this->consultant->id]);
        Auth::shouldReceive('user')
            ->andReturn($this->consultant);
    }

    public function testAuthorize(): void
    {
        $request = new TasksActivationRequest([]);
        $this->assertTrue($request->authorize());
    }

    public function testRulesShouldRequire(): void
    {
        $request = new TasksActivationRequest([]);
        $this->validateFormRequest($request);
        $this->assertEquals(
            [
                'funnel_id' => ['The funnel id field is required.'],
                'tasks' => ['The tasks field is required.']
            ],
            $request->failedValidator->errors()->toArray()
        );
    }

    public function testRulesWithInvalidFunnelId(): void
    {
        $request = new TasksActivationRequest(['funnel_id' => 5]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('funnel_id', $errors, 'The selected funnel id is invalid.');
    }

    public function testRulesWithInvalidContractTasksId(): void
    {
        $request = new TasksActivationRequest([
            'funnel_id' => factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT])->id,
            'tasks' => [5]
        ]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('tasks.*', $errors, 'Invalid task id');
    }

    public function testRulesWithInvalidDBETId(): void
    {
        $request = new TasksActivationRequest([
            'funnel_id' => factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET])->id,
            'tasks' => [5]
        ]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('tasks.*', $errors, 'Invalid task id');
    }

    public function testRulesWithInvalidKostId(): void
    {
        $request = new TasksActivationRequest([
            'funnel_id' => factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_PROPERTY])->id,
            'tasks' => [5]
        ]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('tasks.*', $errors, 'Invalid task id');
    }

    public function testRulesWithInvalidPotentialPropertyId(): void
    {
        $request = new TasksActivationRequest([
            'funnel_id' => factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY])->id,
            'tasks' => [5]
        ]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('tasks.*', $errors, 'Invalid task id');
    }

    public function testRulesWithInvalidPotentialOwnerId(): void
    {
        $request = new TasksActivationRequest([
            'funnel_id' => factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER])->id,
            'tasks' => [5]
        ]);
        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('tasks.*', $errors, 'Invalid task id');
    }
}
