<?php
namespace App\Http\Requests\Auth;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Enums\Activity\ActivationCodeVia;
use Illuminate\Foundation\Testing\WithFaker;

class PasswordResetRequestTest extends MamiKosTestCase
{
    use WithFaker;
    
    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Requests\PasswordResetRequest
     */
    public function testValidationWithCorrectPasswordShouldPass()
    {
        $user = factory(User::class)->states('owner')->create();
        $newPassword = $this->faker->password;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::OWNER_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);
        $request = new PasswordResetRequest();
        $data = [
            'for' => $activationCode->for,
            'via' => $activationCode->via->value,
            'destination' => $user->phone_number,
            'code' => $activationCode->code,
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Requests\PasswordResetRequest
     */
    public function testValidationWithoutPasswordShouldFail()
    {
        $user = factory(User::class)->create();

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);
        $request = new PasswordResetRequest();
        $data = [
            'for' => $activationCode->for,
            'via' => $activationCode->via->value,
            'destination' => $user->phone_number,
            'code' => $activationCode->code,
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.password.required'), $validator->errors()->first('password'));
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Requests\PasswordResetRequest
     */
    public function testValidationWithoutPasswordConfirmationShouldFail()
    {
        $user = factory(User::class)->create();
        $newPassword = $this->faker->password;

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);
        $request = new PasswordResetRequest();
        $data = [
            'for' => $activationCode->for,
            'via' => $activationCode->via->value,
            'destination' => $user->phone_number,
            'code' => $activationCode->code,
            'password' => $newPassword,
            'password_confirmation' => null
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.password.confirmed'), $validator->errors()->first('password'));
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Http\Requests\PasswordResetRequest
     */
    public function testValidationWithInvalidPasswordTypeShouldFail()
    {
        $user = factory(User::class)->create();

        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::TENANT_FORGET_PASSWORD,
            'code' => '' . mt_rand(1000, 9999),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $user->phone_number
        ]);
        $request = new PasswordResetRequest();
        $data = [
            'for' => $activationCode->for,
            'via' => $activationCode->via->value,
            'destination' => $user->phone_number,
            'code' => $activationCode->code,
            'password' => 123456789,
            'password_confirmation' => 123456789
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.password.string'), $validator->errors()->first('password'));
    }
}