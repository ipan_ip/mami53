<?php

namespace App\Http\Requests;

use App\Test\MamiKosTestCase;

class CreateOrUpdateTaskDataRequestTest extends MamiKosTestCase
{
    public function testAuthorize(): void
    {
        $request = new CreateOrUpdateTaskDataRequest([]);
        $this->assertTrue($request->authorize());
    }

    public function testRulesShouldRequire(): void
    {
        $request = new CreateOrUpdateTaskDataRequest([]);

        $this->validateFormRequest($request);
        $this->assertEquals(
            [
                'stage_id' => ['The stage id field is required.'],
                'inputs' => ['The inputs field is required.'],
                'taskId' => ['Task id could not be found']
            ],
            $request->failedValidator->errors()->toArray()
        );
    }

    public function testRulesShouldRequireValueAndMediaId(): void
    {
        $request = new CreateOrUpdateTaskDataRequest([
            'inputs' => [['id' => 5]]
        ]);

        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('inputs.0.media_id', $errors, 'The inputs.0.media_id field is required when inputs.0.value is not present.');
        $this->assertArrayHasKey('inputs.0.value', $errors, 'The inputs.0.value field is required when inputs.0.media_id is not present.');
    }

    public function testRulesWithInvalidStageId(): void
    {
        $request = new CreateOrUpdateTaskDataRequest([
            'stage_id' => 5
        ]);

        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('stage_id', $errors, 'The selected stage id is invalid.');
    }

    public function testRulesWithInvalidMediaId(): void
    {
        $request = new CreateOrUpdateTaskDataRequest([
            'inputs' => [['media_id' => 5]]
        ]);

        $this->validateFormRequest($request);
        $errors = $request->failedValidator->errors()->toArray();
        $this->assertArrayHasKey('inputs.0.media_id', $errors, 'The selected inputs.0.media_id is invalid.');
    }
}
