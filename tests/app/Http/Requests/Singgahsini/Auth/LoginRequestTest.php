<?php
namespace App\Http\Requests\Singgahsini\Auth;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Testing\WithFaker;

class LoginRequestTest extends MamiKosTestCase
{
    
    /**
     * @group SS
     * @group SS-20
     * @group App\Http\Requests\Singgahsini\Auth\LoginRequest
     */
    public function testValidationWithCorrectEmailAndPasswordShouldPass()
    {
        $request = new LoginRequest();
        $data = [
            'email' => 'admin@mail.com',
            'password' => 'password',
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    /**
     * @group SS
     * @group SS-20
     * @group App\Http\Requests\Singgahsini\Auth\LoginRequest
     */
    public function testValidationWithoutEmailShouldFail()
    {
        $request = new LoginRequest();
        $data = [
            'password' => 'password',
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.email.required'), $validator->errors()->first('email'));
    }

    /**
     * @group SS
     * @group SS-20
     * @group App\Http\Requests\Singgahsini\Auth\LoginRequest
     */
    public function testValidationWithInvalidEmailShouldFail()
    {
        $request = new LoginRequest();
        $data = [
            'email' => 'admin.mail.com',
            'password' => 'password',
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.email.email'), $validator->errors()->first('email'));
    }

    /**
     * @group SS
     * @group SS-20
     * @group App\Http\Requests\Singgahsini\Auth\LoginRequest
     */
    public function testValidationWithoutPasswordShouldFail()
    {
        $request = new LoginRequest();
        $data = [
            'email' => 'admin@mail.com',
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.password.required'), $validator->errors()->first('password'));
    }

    /**
     * @group SS
     * @group SS-20
     * @group App\Http\Requests\Singgahsini\Auth\LoginRequest
     */
    public function testValidationWithInvalidPasswordShouldFail()
    {
        $request = new LoginRequest();
        $data = [
            'email' => 'admin.mail.com',
            'password' => 12345678,
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.password.string'), $validator->errors()->first('password'));
    }
}