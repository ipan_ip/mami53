<?php

namespace App\Http\Requests\Booking;

use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Validator;

class BookingUserRequestTest extends MamiKosTestCase
{
    public function testValidationRequiredFails()
    {
        $request = new BookingUserRequest();
        $data = [];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Rent Count Type harus diisi', $validator->errors()->first('rent_count_type'));
        $this->assertEquals('Tanggal Checkin harus diisi', $validator->errors()->first('checkin'));
        $this->assertEquals('Tanggal Checkout harus diisi', $validator->errors()->first('checkout'));
        $this->assertEquals('Lama tinggal harus diisi', $validator->errors()->first('duration'));
        $this->assertEquals('Nama kontak harus diisi', $validator->errors()->first('contact_name'));
        $this->assertEquals('Nomor telepon kontak harus diisi', $validator->errors()->first('contact_phone'));
        $this->assertEquals('Profesi kontak harus diisi', $validator->errors()->first('contact_job'));
        $this->assertEquals('Jenis kelamin kontak harus diisi', $validator->errors()->first('contact_gender'));
    }

    public function testValidationCheckinDateFormatFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->format('d-m-Y')
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Format Tanggal Checkin salah', $validator->errors()->first('checkin'));
    }

    public function testValidationCheckinAfterYesterdayFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->addDays(-1)->format('Y-m-d')
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Tanggal Checkin harus lebih dari tanggal kemarin', $validator->errors()->first('checkin'));
    }

    public function testValidationCheckoutDateFormatFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' =>  Carbon::today()->format('d-m-Y')
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Format Tanggal Checkout salah', $validator->errors()->first('checkout'));
    }

    public function testValidationDurationNumericFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' =>  Carbon::today()->format('Y-m-d'),
            'duration' => 'satu'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Lama tinggal harus berupa angka', $validator->errors()->first('duration'));
    }

    public function testValidationContactNameRegexFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' =>  Carbon::today()->format('Y-m-d'),
            'duration' => 1,
            'contact_name' => 'fan-fandi'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Nama hanya boleh mengandung karakter alfabetis', $validator->errors()->first('contact_name'));
    }

    public function testValidationContactIntroductionRegexFails()
    {
        $request = new BookingUserRequest();
        $data = [
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' =>  Carbon::today()->format('Y-m-d'),
            'duration' => 1,
            'contact_name' => 'fandi',
            'contact_introduction' => 'lorem*ipsum-dolor'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertEquals('Pengenalan Diri hanya boleh mengandung karakter alfabetis', $validator->errors()->first('contact_introduction'));
    }


    public function testValidationSuccess()
    {
        $request = new BookingUserRequest();
        $data = [
            'rent_count_type' => 'quarterly',
            'checkin' => Carbon::today()->format('Y-m-d'),
            'checkout' =>  Carbon::today()->format('Y-m-d'),
            'duration' => 1,
            'contact_name' => 'fandi',
            'contact_introduction' => 'lorem ipsum dolor',
            'contact_phone' => '0822342342342',
            'contact_job' => 'kuliah',
            'contact_gender' => 'male'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());
        $this->assertFalse($validator->fails());
    }
}