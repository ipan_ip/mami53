<?php
namespace App\Http\Requests\Booking;

use App\Test\MamiKosTestCase;
use Validator;

class BookingUserDraftRequestTest extends MamiKosTestCase
{
    public function testValidationRequiredFails()
    {
        $request = new BookingUserDraftRequest();
        $data = [];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Kamar tidak boleh kosong', $validator->errors()->first('room_id'));
    }

    public function testValidationRoomNotExist()
    {
        $request = new BookingUserDraftRequest();
        $data = [
            'room_id' => 1
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Kamar tidak di temukan', $validator->errors()->first('room_id'));
    }

    public function testValidationCheckInDateNotValid()
    {
        $request = new BookingUserDraftRequest();
        $data = [
            'checkin' => '20 April 2020'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Format Tanggal Checkin salah', $validator->errors()->first('checkin'));
    }

    public function testValidationCheckOutDateNotValid()
    {
        $request = new BookingUserDraftRequest();
        $data = [
            'checkout' => '20 April 2020'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Format Tanggal Checkout salah', $validator->errors()->first('checkout'));
    }

    public function testValidationDurationNotValid()
    {
        $request = new BookingUserDraftRequest();
        $data = [
            'duration' => 'satu'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Lama tinggal harus berupa angka', $validator->errors()->first('duration'));
    }
}