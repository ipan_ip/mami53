<?php

namespace App\Http\Requests\Booking;

use App\Test\MamiKosTestCase;
use Validator;

class BookingUserCancelRequestTest extends MamiKosTestCase
{
    public function testValidationRequiredFails()
    {
        $request = new BookingUserCancelRequest();
        $data = [];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Alasan Batal harus diisi', $validator->errors()->first('cancel_reason'));
    }

    public function testValidationMinFails()
    {
        $request = new BookingUserCancelRequest();
        $data = [
            'cancel_reason' => 'no'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Alasan Batal minimal 4 karakter', $validator->errors()->first('cancel_reason'));
    }

    public function testValidationMaxFails()
    {
        $request = new BookingUserCancelRequest();
        $data = [
            'cancel_reason' => str_repeat('a', 201)
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals('Alasan Batal maksimal 200 karakter', $validator->errors()->first('cancel_reason'));
    }

    public function testValidationSuccess()
    {
        $request = new BookingUserCancelRequest();
        $data = [
            'cancel_reason' => 'Sudah dapat kos'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertFalse($validator->fails());
    }
}