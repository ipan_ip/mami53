<?php

namespace App\Http\Requests;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Routing\Redirector;

class UpdateBookingDataRequestTest extends MamiKosTestCase
{
    
    protected $room;

    protected function setUp()
    {
        parent::setUp();
        $this->room = factory(Room::class)->create();
    }

    public function testAuthorixe(): void
    {
        $request = new UpdateBookingDataRequest();
        $this->assertTrue($request->authorize());
    }

    public function testRulesWithIdrAndDaily(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_daily'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndMonthly(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_monthly'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndYearly(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_yearly'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndPrice3Month(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_3_month'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndPrice6Month(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_6_month'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndAllPricesShouldNotFail(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'price_daily'     => 1000,
            'price_weekly'     => 1000,
            'price_monthly'     => 1000,
            'price_yearly'     => 1000,
            'price_3_month'     => 1000,
            'price_6_month'     => 1000,
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithIdrAndNoPriceShouldFail(): void
    {
        $request = new UpdateBookingDataRequest([
            'available_room'  => '5',
            'id' => $this->room->song_id
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        $errors = $request->failedValidator->errors();
        $expected = 'Harga sewa harus diisi salah satu';
        $this->assertEquals($expected, $errors->first('price_daily'));
        $this->assertEquals($expected, $errors->first('price_monthly'));
        $this->assertEquals($expected, $errors->first('price_weekly'));
        $this->assertEquals($expected, $errors->first('price_yearly'));
        $this->assertEquals($expected, $errors->first('price_3_month'));
        $this->assertEquals($expected, $errors->first('price_6_month'));
    }
}
