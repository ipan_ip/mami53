<?php

namespace App\Http\Requests\Owner\Tenant;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OwnerTenantLinkRequestTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp()
    {
        parent::setUp();

        $this->repo = $this->mock(RoomRepository::class);
    }

    public function testRulesWithInvalidSongId()
    {
        $request = new OwnerTenantLinkRequest(['designer_id' => 0]);
        $this->repo
            ->shouldReceive('getRoomWithOwnerBySongId')
            ->with(0)
            ->andThrow(new ModelNotFoundException());

        $this->validateFormRequest($request);
        $this->assertEquals('Kos tidak ditemukan', $request->failedValidator->errors()->first('designer_id'));
    }

    public function testRulesWithNullRoom()
    {
        $request = new OwnerTenantLinkRequest(['designer_id' => 0]);
        $this->repo
            ->shouldReceive('getRoomWithOwnerBySongId')
            ->with(0)
            ->andReturn(null);

        $this->validateFormRequest($request);
        $this->assertEquals('Kos tidak ditemukan', $request->failedValidator->errors()->first('designer_id'));
    }

    public function testGetRoom()
    {
        $request = new OwnerTenantLinkRequest(['designer_id' => 0]);
        $kost = factory(Room::class)->create();
        $this->repo
            ->shouldReceive('getRoomWithOwnerBySongId')
            ->with(0)
            ->andReturn($kost);

        $this->validateFormRequest($request);
        $this->assertTrue($kost->is($request->getRoom()));
    }
}
