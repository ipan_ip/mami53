<?php

namespace App\Http\Requests\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use App\Repositories\Dbet\DbetLinkRepository;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ContractSubmissionRequestTest extends MamiKosTestCase
{
    public function testRulesWithInvalidDBETLinkId()
    {
        $request = new ContractSubmissionRequest(['dbet_link_id' => 0]);
        $repo = $this->mock(DbetLinkRepository::class);
        $this->instance(DbetLinkRepository::class, $repo);
        $repo->shouldReceive('find')->with(0)->andThrow(new ModelNotFoundException());

        $this->validateFormRequest($request);
        $this->assertEquals('DBET link data tidak ditemukan', $request->failedValidator->errors()->first('dbet_link_id'));
    }

    public function testRulesWithEmptyIdentityId()
    {
        $request = new ContractSubmissionRequest(['dbet_link_id' => 0]);
        $repo = $this->mock(DbetLinkRepository::class);
        $this->instance(DbetLinkRepository::class, $repo);
        $link = factory(DbetLink::class)->create(['is_required_identity' => true]);
        $repo->shouldReceive('find')->with(0)->andReturn($link);

        $this->validateFormRequest($request);
        $this->assertEquals('Identity tidak boleh kosong', $request->failedValidator->errors()->first('identity_id'));
    }

    public function testGetDbetLink()
    {
        $request = new ContractSubmissionRequest(['dbet_link_id' => 0]);
        $repo = $this->mock(DbetLinkRepository::class);
        $this->instance(DbetLinkRepository::class, $repo);
        $link = factory(DbetLink::class)->create(['is_required_identity' => true]);
        $repo->shouldReceive('find')->with(0)->andReturn($link);

        $this->validateFormRequest($request);
        $this->assertTrue($request->getDbetLink()->is($link));
    }
}
