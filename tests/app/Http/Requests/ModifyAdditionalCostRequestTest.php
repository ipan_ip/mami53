<?php

namespace App\Http\Requests;

use App\Test\MamiKosTestCase;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;

class ModifyAdditionalCostRequestTest extends MamiKosTestCase
{
    public function testAuthorize(): void
    {
        $request = new ModifyAdditionalCostRequest();
        $this->assertTrue($request->authorize());
    }

    public function testRulesWithFixedCostType(): void
    {
        $request = new ModifyAdditionalCostRequest();
        $validator = Validator::make(
            [
                'aditional_costs' => [
                    'cost_type' => 'fixed',
                    'action' => 'CREATE'
                ]
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertFalse($validator->fails());
    }

    public function testRulesWithOtherCostType(): void
    {
        $request = new ModifyAdditionalCostRequest();
        $validator = Validator::make(
            [
                'aditional_costs' => [
                    'cost_type' => 'other',
                    'action' => 'CREATE'
                ]
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertFalse($validator->fails());
    }

    public function testRulesWithUpdateAction(): void
    {
        $request = new ModifyAdditionalCostRequest();
        $validator = Validator::make(
            [
                'aditional_costs' => [
                    'cost_type' => 'fixed',
                    'action' => 'UPDATE'
                ]
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertFalse($validator->fails());
    }

    public function testRulesWithDeleteAction(): void
    {
        $request = new ModifyAdditionalCostRequest();
        $validator = Validator::make(
            [
                'aditional_costs' => [
                    'cost_type' => 'fixed',
                    'action' => 'DELETE'
                ]
            ],
            $request->rules(),
            $request->messages()
        );

        $this->assertFalse($validator->fails());
    }

    public function testWithValidatorWithCreateRequest(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'CREATE',
                    'invoice_id' => 1,
                    'cost_value' => 15000,
                    'cost_title' => 'Biaya Pecahin Kaca',
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNull($request->failedValidator);
    }

    public function testWithValidatorWithCreateRequestAndMissingInvoiceIdShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'CREATE',
                    'cost_value' => 15000,
                    'cost_title' => 'Biaya Pecahin Kaca',
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.invoice_id.required'), $request->failedValidator->errors()->first('invoice_id'));
    }

    public function testWithValidatorWithCreateRequestAndMissingCostValueShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'CREATE',
                    'invoice_id' => 1,
                    'cost_title' => 'Biaya Pecahin Kaca',
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.cost_value.required'), $request->failedValidator->errors()->first('cost_value'));
    }

    public function testWithValidatorWithCreateRequestAndMissingCostTitleShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'CREATE',
                    'invoice_id' => 1,
                    'cost_value' => 15000,
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.cost_title.required'), $request->failedValidator->errors()->first('cost_title'));
    }

    public function testWithValidatorWithCreateRequestAndMissingCostTypeShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'action' => 'CREATE',
                    'invoice_id' => 1,
                    'cost_value' => 15000,
                    'cost_title' => 'Biaya Pecahin Kaca',
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.cost_type.required'), $request->failedValidator->errors()->first('cost_type'));
    }

    public function testWithValidatorWithUpdateRequestShouldPass(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'UPDATE',
                    'additional_cost_id' => 1,
                    'cost_value' => 15000,
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNull($request->failedValidator);
    }

    public function testWithValidatorWithUpdateRequestAndMissingAdditionalCostIdShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'UPDATE',
                    'cost_value' => 15000,
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.input.additional_cost_id.required'), $request->failedValidator->errors()->first('additional_cost_id'));
    }

    public function testWithValidatorWithUpdateRequestAndMissingCostValueShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'UPDATE',
                    'additional_cost_id' => 1,
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.input.cost_value.required'), $request->failedValidator->errors()->first('cost_value'));
    }

    public function testWithValidatorWithDeleteRequestShouldPass(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'DELETE',
                    'additional_cost_id' => 1,
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNull($request->failedValidator);
    }

    public function testWithValidatorWithDeleteRequestAndMissingAdditionalCostIdShouldFail(): void
    {
        $request = new ModifyAdditionalCostRequest([
            'additional_costs' => [
                [
                    'cost_type' => 'fixed',
                    'action' => 'UPDATE',
                ]
            ]
        ]);

        $request->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(__('api.input.additional_cost_id.required'), $request->failedValidator->errors()->first('additional_cost_id'));
    }
}
