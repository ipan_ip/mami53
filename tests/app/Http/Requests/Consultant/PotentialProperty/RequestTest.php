<?php

namespace App\Http\Requests\Consultant\PotentialProperty;

use App\Entities\Media\Media;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class RequestTest extends MamiKosTestCase
{
    use WithFaker;

    public function testRulesShouldRequireName(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The name field is required.',
            $request->failedValidator->errors()->first('name')
        );
    }

    public function testRulesShouldEnforceNameAsString(): void
    {
        $request = new Request(['name' => 5]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The name must be a string.',
            $request->failedValidator->errors()->first('name')
        );
    }

    public function testRulesShouldEnforceNameLength(): void
    {
        $request = new Request(['name' => $this->faker->regexify('[a-z]{256}')]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The name may not be greater than 255 characters.',
            $request->failedValidator->errors()->first('name')
        );
    }

    public function testRulesShouldRequireAddress(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The address field is required.',
            $request->failedValidator->errors()->first('address')
        );
    }

    public function testRulesShouldEnforceAddressAsString(): void
    {
        $request = new Request(['address' => 5]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The address must be a string.',
            $request->failedValidator->errors()->first('address')
        );
    }

    public function testRulesShouldEnforceAddressLength(): void
    {
        $request = new Request(['address' => $this->faker->regexify('[a-z]{256}')]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The address may not be greater than 255 characters.',
            $request->failedValidator->errors()->first('address')
        );
    }

    public function testRulesShouldRequireAreaCity(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The area city field is required.',
            $request->failedValidator->errors()->first('area_city')
        );
    }

    public function testRulesShouldEnforceAreaCityAsString(): void
    {
        $request = new Request(['area_city' => 5]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The area city must be a string.',
            $request->failedValidator->errors()->first('area_city')
        );
    }

    public function testRulesShouldEnforceAreaCityLength(): void
    {
        $request = new Request(['area_city' => $this->faker->regexify('[a-z]{256}')]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The area city may not be greater than 255 characters.',
            $request->failedValidator->errors()->first('area_city')
        );
    }

    public function testRulesShouldRequireProvince(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The province field is required.',
            $request->failedValidator->errors()->first('province')
        );
    }

    public function testRulesShouldEnforceProvinceAsString(): void
    {
        $request = new Request(['province' => 5]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The province must be a string.',
            $request->failedValidator->errors()->first('province')
        );
    }

    public function testRulesShouldEnforceProvinceLength(): void
    {
        $request = new Request(['province' => $this->faker->regexify('[a-z]{256}')]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The province may not be greater than 255 characters.',
            $request->failedValidator->errors()->first('province')
        );
    }

    public function testRulesShouldAllowNullPhoto(): void
    {
        $request = new Request(['photo' => null]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('photo')
        );
    }

    public function testRulesShouldAllowWithoutPhoto(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('photo')
        );
    }

    public function testRulesShouldEnforceValidPhoto(): void
    {
        $request = new Request(['photo' => 0]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The selected photo is invalid.',
            $request->failedValidator->errors()->first('photo')
        );
    }

    public function testRulesShouldAllowNullTotalRoom(): void
    {
        $request = new Request(['total_room' => null]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('total_room')
        );
    }

    public function testRulesShouldAllowWithoutTotalRoom(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('total_room')
        );
    }

    public function testRulesShouldAllow0TotalRoom(): void
    {
        $request = new Request(['total_room' => 0]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('total_room')
        );
    }

    public function testRulesShouldEnforceTotalRoomAsInteger(): void
    {
        $request = new Request(['total_room' => 'test']);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The total room must be an integer.',
            $request->failedValidator->errors()->first('total_room')
        );
    }

    public function testRulesShouldEnforceRequiredProductOffered(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The product offered field is required.',
            $request->failedValidator->errors()->first('product_offered')
        );
    }

    public function testRulesShouldAllowGp1AsProductOffered(): void
    {
        $request = new Request(['product_offered' => 'gp1']);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('product_offered')
        );
    }

    public function testRulesShouldAllowGp2AsProductOffered(): void
    {
        $request = new Request(['product_offered' => 'gp2']);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('product_offered')
        );
    }

    public function testRulesShouldAllowGp3AsProductOffered(): void
    {
        $request = new Request(['product_offered' => 'gp3']);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('product_offered')
        );
    }

    public function testRulesShouldAllowGp4AsProductOffered(): void
    {
        $request = new Request(['product_offered' => 'gp4']);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('product_offered')
        );
    }

    public function testRulesShouldAllowNullPriority(): void
    {
        $request = new Request(['priority' => null]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('priority')
        );
    }

    public function testRulesShouldAllowWithoutPriority(): void
    {
        $request = new Request([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            '',
            $request->failedValidator->errors()->first('priority')
        );
    }

    public function testRulesShouldEnforceValidPriority(): void
    {
        $request = new Request(['priority' => 'test']);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The selected priority is invalid.',
            $request->failedValidator->errors()->first('priority')
        );
    }

    public function testRulesShouldAllowEmptyNotes(): void
    {
        $request = new Request(['remark' => '']);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('remark')
        );
    }

    public function testRulesShouldAllowNullNotes(): void
    {
        $request = new Request(['remark' => null]);

        $this->validateFormRequest($request);

        $this->assertEmpty(
            $request->failedValidator->errors()->first('remark')
        );
    }

    public function testInputsShouldSetIsPriorityAsHigh(): void
    {
        $request = new Request(['priority' => 'high']);

        $input = $request->inputs();

        $this->assertEquals(
            true,
            $input['is_priority']
        );
    }

    public function testInputsShouldSetIsPriorityAsLow(): void
    {
        $request = new Request(['priority' => 'low']);

        $input = $request->inputs();

        $this->assertEquals(
            false,
            $input['is_priority']
        );
    }

    public function testInputsShouldDefaultIsPriorityToHigh(): void
    {
        $request = new Request(['priority' => null]);

        $input = $request->inputs();

        $this->assertEquals(
            true,
            $input['is_priority']
        );

        $request = new Request([]);

        $input = $request->inputs();

        $this->assertEquals(
            true,
            $input['is_priority']
        );
    }

    public function testInputsDefaultTotalRoomTo0(): void
    {
        $request = new Request(['total_room' => null]);

        $input = $request->inputs();

        $this->assertEquals(
            0,
            $input['total_room']
        );

        $request = new Request([]);

        $input = $request->inputs();

        $this->assertEquals(
            0,
            $input['total_room']
        );
    }

    public function testInputsShouldNotSerializeProductOffered(): void
    {
        $request = new Request(['product_offered' => 'gp1']);

        $input = $request->inputs();

        $this->assertEquals(
            'gp1',
            $input['offered_product']
        );
    }

    public function testInputsShouldSetMediaId(): void
    {
        $photo = factory(Media::class)->create();
        $request = new Request(['photo' => $photo->id]);

        $input = $request->inputs();

        $this->assertEquals(
            $photo->id,
            $input['media_id']
        );
    }

    public function testInputsWithEmptyStringPhoto(): void
    {
        $request = new Request(['photo' => '']);

        $input = $request->inputs();

        $this->assertEquals(
            null,
            $input['media_id']
        );
    }

    public function testInputsWithoutPhoto(): void
    {
        $request = new Request();

        $input = $request->inputs();

        $this->assertEquals(
            null,
            $input['media_id']
        );
    }

    public function testInputsShouldSetNotesAsRemark(): void
    {
        $request = new Request(['remark' => 'test']);

        $input = $request->inputs();

        $this->assertEquals(
            'test',
            $input['remark']
        );
    }
}
