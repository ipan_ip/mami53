<?php

namespace App\Http\Requests\Consultant\PotentialProperty;

use App\Test\MamiKosTestCase;

class CreateRequestTest extends MamiKosTestCase
{
    public function testRulesShouldRequireOwnerId(): void
    {
        $request = new CreateRequest([]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The owner id field is required.',
            $request->failedValidator->errors()->first('owner_id')
        );
    }

    public function testRulesShouldEnforceOwnerIdAsInteger(): void
    {
        $request = new CreateRequest(['owner_id' => 'test']);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The owner id must be an integer.',
            $request->failedValidator->errors()->first('owner_id')
        );
    }

    public function testRulesShouldEnforceValidOwnerId(): void
    {
        $request = new CreateRequest(['owner_id' => 0]);

        $this->validateFormRequest($request);

        $this->assertEquals(
            'The selected owner id is invalid.',
            $request->failedValidator->errors()->first('owner_id')
        );
    }

    public function testInputsShouldReturnOwnerId(): void
    {
        $request = new CreateRequest(['owner_id' => 5]);

        $this->assertEquals(
            5,
            $request->inputs()['consultant_potential_owner_id']
        );
    }
}
