<?php

namespace App\Http\Requests;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;

class CreateContractRequestTest extends MamiKosTestCase
{
    use WithFaker;

    public function testAuthorize(): void
    {
        $request = new CreateContractRequest([]);
        $this->assertTrue($request->authorize());
    }

    public function testRulesWithPotentialTenantShouldPass(): void
    {
        $potentialTenant = factory(PotentialTenant::class)->create(['phone_number' => '0811111111']);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithNewTenant(): void
    {
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithExistingTenant(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0811111111']);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testRulesWithActiveContractShouldFail(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0811111111']);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id,
            'status' => MamipayContract::STATUS_ACTIVE
        ]);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(
            'Tenant already has active or booked contract',
            $request->failedValidator->errors()->first()
        );
    }

    public function testRulesWithBookedContractShouldFail(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0811111111']);
        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id,
            'status' => MamipayContract::STATUS_BOOKED
        ]);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(
            'Tenant already has active or booked contract',
            $request->failedValidator->errors()->first()
        );
    }

    public function testRulesWithMahasiswaAndMissingParentNameShouldFail(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0811111111']);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_phone_number' => '0811111111',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(
            'Nama tidak boleh kosong',
            $request->failedValidator->errors()->first()
        );
    }

    public function testRulesWithMahasiswaAndMissingParentPhoneNumberShouldFail(): void
    {
        $tenant = factory(MamipayTenant::class)->create(['phone_number' => '0811111111']);
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Mahasiswa',
            'marital_status' => 'Kawin',
            'parent_name' => 'Test Test',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNotNull($request->failedValidator);
        $this->assertEquals(
            'Mohon masukkan nomor',
            $request->failedValidator->errors()->first()
        );
    }

    public function testRulesWithKaryawanShouldPass(): void
    {
        $media = factory(MamipayMedia::class)->create();
        $room = factory(Room::class)->create();
        $request = new CreateContractRequest([
            'email' => $this->faker->email,
            'phone_number' => '0811111111',
            'name' => 'Test Test',
            'gender' => 'male',
            'occupation' => 'Karyawan',
            'marital_status' => 'Kawin',
            'photo_identifier_id' => $media->id,
            'photo_document_id' => $media->id,
            'room_id' => $room->song_id,
            'room_number' => '5A',
            'start_date' => '2020-04-06',
            'rent_type' => 'day',
            'amount' => '1500000',
            'duration' => 10,
            'fine_amount' => '150000',
            'fine_maximum_length' => 5,
            'fine_duration_type' => 'day',
            'additional_costs' => [
                [
                    'field_title' => 'Sekamar berdua',
                    'field_value' => '500000',
                    'cost_type'  => 'fixed'
                ],
                [
                    'field_title' => 'Denda',
                    'field_value' => '5000',
                    'cost_type' => 'other'
                ]
            ]
        ]);

        $this->validateFormRequest($request);
        $this->assertNull($request->failedValidator);
    }

    public function testMessages(): void
    {
        $request = new CreateContractRequest();
        $this->assertEquals(
            [
                'email.required' => __('api.input.email.required'),
                'email.email' => __('api.input.email.email'),
                'email.unique' => __('api.input.email.unique'),
                'phone_number.required' => __('api.input.phone_number.required'),
                'phone_number.min' => __('api.input.phone_number.min'),
                'phone_number.max' => __('api.input.phone_number.max'),
                'phone_number.unique' => __('api.input.phone_number.unique'),
                'phone_number.regex' => __('api.input.phone_number.regex'),
                'name.required' => __('api.input.name.required'),
                'name.string' => __('api.input.name.alpha'),
                'name.min' => __('api.input.name.min'),
                'name.max' => __('api.input.name.max'),
                'name.regex' => __('api.input.name.alpha'),
                'gender.required' => __('api.input.gender.required'),
                'gender.string' => __('api.input.gender.string'),
                'gender.in' => __('api.input.gender.in'),
                'occupation.required' => __('api.input.occupation.required'),
                'occupation.string' => __('api.input.occupation.string'),
                'occupation.in' => __('api.input.occupation.in'),
                'marital_status.required' => __('api.input.marital_status.required'),
                'marital_status.string' => __('api.input.marital_status.string'),
                'marital_status.in' => __('api.input.marital_status.in'),
                'parent_name.required_if' => __('api.input.parent_name.required_if'),
                'parent_name.string' => __('api.input.parent_name.string'),
                'parent_name.min' => __('api.input.parent_name.min'),
                'parent_name.max' => __('api.input.parent_name.max'),
                'parent_name.regex' => __('api.input.parent_name.regex'),
                'parent_phone_number.required_if' => __('api.input.parent_phone_number.required_if'),
                'parent_phone_number.min' => __('api.input.parent_phone_number.min'),
                'parent_phone_number.max' => __('api.input.parent_phone_number.max'),
                'parent_phone_number.regex' => __('api.input.parent_phone_number.regex'),
                'room_number.required' => __('api.input.room_number.required'),
                'room_number.string' => __('api.input.room_number.string'),
                'start_date.required' => __('api.input.start_date.required'),
                'start_date.date_format' => __('api.input.start_date.date_format'),
                'rent_type.required' => __('api.input.rent_type.required'),
                'rent_type.string' => __('api.input.rent_type.string'),
                'rent_type.in' =>  __('api.input.rent_type.in'),
                'amount.required' => __('api.input.amount.required'),
                'amount.numeric' => __('api.input.amount.numeric'),
                'amount.min' => __('api.input.amount.min'),
                'duration.required' => __('api.input.duration.required'),
                'duration.integer' => __('api.input.duration.integer'),
                'duration.min' => __('api.input.duration.min'),
                'fine_amount.numeric' => __('api.input.fine_amount.numeric'),
                'fine_maximum_length.integer' => __('api.input.fine_maximum_length.integer'),
                'fine_duration_type.string' => __('api.input.fine_duration_type.string'),
                'fine_duration_type.in' => __('api.input.fine_duration_type.in'),
                'additional_costs.array' => __('api.input.additional_costs.array'),
                'additional_costs.*.field_title.required' => __('api.input.additional_costs.*.field_title.required'),
                'additional_costs.*.field_title.string' => __('api.input.additional_costs.*.field_title.string'),
                'additional_costs.*.field_title.min' => __('api.input.additional_costs.*.field_title.min'),
                'additional_costs.*.field_title.max' => __('api.input.additional_costs.*.field_title.max'),
                'additional_costs.*.field_value.required' => __('api.input.additional_costs.*.field_value.required'),
                'additional_costs.*.field_value.numeric' => __('api.input.additional_costs.*.field_value.numeric'),
                'additional_costs.*.cost_type.sometimes' => __('api.input.additional_costs.*.cost_type.sometimes'),
                'additional_costs.*.cost_type.required' => __('api.input.additional_costs.*.cost_type.required'),
                'additional_costs.*.cost_type.string' => __('api.input.additional_costs.*.cost_type.string'),
                'additional_costs.*.cost_type.in' => __('api.input.additional_costs.*.cost_type.in')
            ],
            $request->messages()
        );
    }
}
