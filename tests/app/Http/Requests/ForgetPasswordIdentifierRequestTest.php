<?php
namespace App\Http\Requests;

use App\Test\MamiKosTestCase;
use App\Http\Requests\ForgetPasswordIdentifierRequest;
use Illuminate\Support\Facades\Validator;
use App\User;

class ForgetPasswordIdentifierRequestTest extends MamiKosTestCase
{
    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithCorrectPhoneNumberRequestShouldPass()
    {
        $owner = factory(User::class)->states('owner')->create();
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => $owner->phone_number
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertFalse($validator->fails());
        $this->assertEmpty($validator->errors()->all());
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithEmptyRequestReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $this->assertTrue($validator->fails());
        $this->assertIsArray($validator->errors()->all());
        $this->assertEquals(__('api.input.phone_number.required'), $validator->errors()->first('phone_number'));
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithMinPhoneNumberRequestReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => '0812312'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('api.input.phone_number.min', ['min' => 8]), $error[0]);
    }
    
    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithMaxPhoneNumberRequestReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => '081231154353462'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('api.input.phone_number.max', ['max' => 14]), $error[0]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithInvalidPhoneNumberFormatReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => '6281154353462'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('api.input.phone_number.regex'), $error[0]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithInvalidPhoneNumberTypeReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => 6281154353462
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('api.input.phone_number.invalid'), $error[0]);
    }

    /**
     * @group UG
     * @group UG-4743
     * @group App\Http\Requests\ForgetPasswordIdentifierRequest
     */
    public function testValidationWithUnregisteredPhoneNumberReturnFails()
    {
        $request = new ForgetPasswordIdentifierRequest();
        $data = [
            'phone_number' => '081154353462'
        ];
        $validator = Validator::make($data, $request->rules(), $request->messages());

        $error = $validator->errors()->all();
        $this->assertTrue($validator->fails());
        $this->assertIsArray($error);
        $this->assertEquals(__('api.input.phone_number.exists'), $error[0]);
    }
}
