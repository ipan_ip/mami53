<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Forum\ForumAnswerListTransformer;
use App\Transformers\Forum\ForumAnswerVoteTransformer;

class ForumAnswerPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ForumAnswerListTransformer::class;

        $presenter = new ForumAnswerPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithVote(): void
    {
        $expected = ForumAnswerVoteTransformer::class;

        $presenter = new ForumAnswerPresenter('vote');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
