<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Apartment\DetailTransformer;
use App\Transformers\Apartment\ListTransformer;

class ApartmentPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $transformer = ListTransformer::class;

        $presenter = new ApartmentPresenter('list');

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = DetailTransformer::class;

        $presenter = new ApartmentPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
