<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Giant\GiantListTransformer;
use App\Transformers\Room\AdminIndexBenefitTransformer;
use App\Transformers\Room\AdminIndexTransformer;
use App\Transformers\Room\AdminIndexTwoTransformer;
use App\Transformers\Room\AdminIndexTypeTransformer;
use App\Transformers\Room\AdminRevisionTransformer;
use App\Transformers\Room\AdminUcodeTransformer;
use App\Transformers\Room\AfterInputTransformer;
use App\Transformers\Room\DetailTransformer;
use App\Transformers\Room\EmailListRecommendationTransformer;
use App\Transformers\Room\ExistingInputTransformer;
use App\Transformers\Room\LineRecommendationTransformer;
use App\Transformers\Room\ListingOwnerPropertyTransformer;
use App\Transformers\Room\ListingOwnerTransformer;
use App\Transformers\Room\ListingOwnerUpdateTransformer;
use App\Transformers\Room\ListOwnerTransformer;
use App\Transformers\Room\ListTransformer;
use App\Transformers\Room\MetaTransformer;
use App\Transformers\Room\RecommendationTransformer;
use App\Transformers\Room\RoomActiveTransformer;
use App\Transformers\Room\RoomAttachmentIndexTransformer;
use App\Transformers\Room\RoomFacilityTransformer;
use App\Transformers\Room\RoomTypeFacilityTransformer;
use App\Transformers\Room\SimpleListTransformer;
use App\Transformers\Room\WebRoomFacilityTransformer;
use App\Transformers\Room\WebStrippedTransformer;
use App\Transformers\Room\WebTransformer;
use App\Transformers\Room\FinancialReport\RoomTransformer as RoomFinancialReport;
use App\Transformers\Room\GoldplusRoomTransformer;

class RoomPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList()
    {
        $expected = ListTransformer::class;
        $presenter = new RoomPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithGiantList()
    {
        $expected = GiantListTransformer::class;
        $presenter = new RoomPresenter('giant-list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithSimpleList()
    {
        $expected = SimpleListTransformer::class;
        $presenter = new RoomPresenter('simple-list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithIndex()
    {
        $expected = AdminIndexTransformer::class;
        $presenter = new RoomPresenter('index');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithType()
    {
        $expected = AdminIndexTypeTransformer::class;
        $presenter = new RoomPresenter('type');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithIndexTwo()
    {
        $expected = AdminIndexTwoTransformer::class;
        $presenter = new RoomPresenter('index-two');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithUcode()
    {
        $expected = AdminUcodeTransformer::class;
        $presenter = new RoomPresenter('ucode');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithVrtourIndex()
    {
        $expected = RoomAttachmentIndexTransformer::class;
        $presenter = new RoomPresenter('vrtour-index');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithWeb()
    {
        $expected = WebTransformer::class;
        $presenter = new RoomPresenter('web');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithWebStripped()
    {
        $expected = WebStrippedTransformer::class;
        $presenter = new RoomPresenter('web-stripped');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithAfterInput()
    {
        $expected = AfterInputTransformer::class;
        $presenter = new RoomPresenter('after-input');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRecommendation()
    {
        $expected = RecommendationTransformer::class;
        $presenter = new RoomPresenter('recommendation');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithOwner()
    {
        $expected = ListOwnerTransformer::class;
        $presenter = new RoomPresenter('owner');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListOwner()
    {
        $expected = ListingOwnerTransformer::class;
        $presenter = new RoomPresenter('list-owner');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListOwnerProperty()
    {
        $expected = ListingOwnerPropertyTransformer::class;
        $presenter = new RoomPresenter('list-owner-property');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithOwnerUpdate()
    {
        $expected = ListingOwnerUpdateTransformer::class;
        $presenter = new RoomPresenter('owner-update');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithLine()
    {
        $expected = LineRecommendationTransformer::class;
        $presenter = new RoomPresenter('line');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithExistingInput()
    {
        $expected = ExistingInputTransformer::class;
        $presenter = new RoomPresenter('existing-input');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithEmailRecommendation()
    {
        $expected = EmailListRecommendationTransformer::class;
        $presenter = new RoomPresenter('email-recommendation');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithFacilities()
    {
        $expected = RoomFacilityTransformer::class;
        $presenter = new RoomPresenter('facilities');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithFacilitiesType()
    {
        $expected = RoomTypeFacilityTransformer::class;
        $presenter = new RoomPresenter('facilities-type');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithWebFacilities()
    {
        $expected = WebRoomFacilityTransformer::class;
        $presenter = new RoomPresenter('web-facilities');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithMeta()
    {
        $expected = MetaTransformer::class;
        $presenter = new RoomPresenter('meta');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithActive()
    {
        $expected = RoomActiveTransformer::class;
        $presenter = new RoomPresenter('active');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRoomFinancialReport()
    {
        $expected = RoomFinancialReport::class;
        $presenter = new RoomPresenter('room-financial-report');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithGoldplusList()
    {
        $expected = GoldplusRoomTransformer::class;
        $presenter = new RoomPresenter('goldplus-list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRevisionIndex()
    {
        $expected = AdminRevisionTransformer::class;
        $presenter = new RoomPresenter(RoomPresenter::REVISION_INDEX);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithInvalidArgument(): void
    {
        $expected = DetailTransformer::class;
        $presenter = new RoomPresenter('test');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListKostBenefit(): void
    {
        $expected = AdminIndexBenefitTransformer::class;
        $presenter = new RoomPresenter('list-kost-benefit');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
