<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Jobs\JobsListTransformer;
use App\Transformers\Jobs\JobsOwnerListTransformer;
use App\Transformers\Jobs\JobsTransformer;

class JobsPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = JobsListTransformer::class;

        $presenter = new JobsPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListOwner(): void
    {
        $expected = JobsOwnerListTransformer::class;

        $presenter = new JobsPresenter('list-owner');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = JobsTransformer::class;

        $presenter = new JobsPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
