<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\BalanceRequestTransformer;
use App\Transformers\ConfirmationTransformer;
use App\Transformers\Premium\RequestHistoryTransformer;
use App\Transformers\PremiumRequestTransformer;

class PremiumRequestPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithBalance(): void
    {
        $expected = BalanceRequestTransformer::class;

        $presenter = new PremiumRequestPresenter('balance');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithConfirmation(): void
    {
        $expected = ConfirmationTransformer::class;

        $presenter = new PremiumRequestPresenter('confirmation');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRequestHistory(): void
    {
        $expected = RequestHistoryTransformer::class;

        $presenter = new PremiumRequestPresenter('request_history');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = PremiumRequestTransformer::class;

        $presenter = new PremiumRequestPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
