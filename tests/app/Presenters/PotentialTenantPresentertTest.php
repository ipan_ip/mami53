<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\Contract\PotentialTenantTransformer;

class PotentialTenantPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithContractManagement(): void
    {
        $presenter = new PotentialTenantPresenter('contract-management');
        $expected = PotentialTenantTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
