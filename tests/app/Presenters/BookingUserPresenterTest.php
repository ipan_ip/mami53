<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserListTransformer;
use App\Transformers\Booking\BookingMyRoomDetailTransformer;
use App\Transformers\Booking\BookingRoomUnitListTransformer;
use App\Transformers\Booking\NewBookingUserDetailTransformer;
use App\Transformers\Booking\BookingUserAdminIndexTransformer;
use App\Transformers\Booking\BookingUserStatusChangedTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingAndPropertyDetailTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingManagementTenantDetailTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingManagementContractDetailTransformer;

class BookingUserPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithInvalidTransformer(): void
    {
        $transformer = (new BookingUserPresenter('invalid'))->getTransformer();

        $this->assertNull($transformer);
    }

    public function testGetTransformerWithDetail(): void
    {
        $actual = (new BookingUserPresenter('detail'))->getTransformer();
        $expected = NewBookingUserDetailTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithList(): void
    {
        $actual = (new BookingUserPresenter('list'))->getTransformer();
        $expected = BookingUserListTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithMyRoom(): void
    {
        $actual = (new BookingUserPresenter('my-room'))->getTransformer();
        $expected = BookingMyRoomDetailTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithAdminIndex(): void
    {
        $actual = (new BookingUserPresenter('admin-index'))->getTransformer();
        $expected = BookingUserAdminIndexTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithDetailStatusList(): void
    {
        $actual = (new BookingUserPresenter('detail-status-list'))->getTransformer();
        $expected = BookingUserStatusChangedTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithRoomUnitList(): void
    {
        $actual = (new BookingUserPresenter('room-unit-list'))->getTransformer();
        $expected = BookingRoomUnitListTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithConsultantBookingManagementContractDetail(): void
    {
        $actual = (
            new BookingUserPresenter(
                BookingUserPresenter::CONSULTANT_BOOKING_MANAGEMENT_CONTRACT_DETAIL
            )
        )->getTransformer();
        $expected = BookingManagementContractDetailTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithConsultantBookingAndPropertyDetail(): void
    {
        $actual = (
        new BookingUserPresenter(
            BookingUserPresenter::CONSULTANT_BOOKING_AND_PROPERTY_DETAIL
        )
        )->getTransformer();
        $expected = BookingAndPropertyDetailTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithConsultantBookingTenantDetail(): void
    {
        $actual = (
        new BookingUserPresenter(
            BookingUserPresenter::CONSULTANT_BOOKING_MANAGEMENT_TENANT_DETAIL
        )
        )->getTransformer();
        $expected = BookingManagementTenantDetailTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }
}
