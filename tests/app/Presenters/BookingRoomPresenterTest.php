<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingRoomDetailTransformer;

class BookingRoomPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithDetail(): void
    {
        $expected = BookingRoomDetailTransformer::class;

        $presenter = new BookingRoomPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
