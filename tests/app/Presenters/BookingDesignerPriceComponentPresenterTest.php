<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDesignerPriceComponentListTransformer;

class BookingDesignerPriceComponentPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = BookingDesignerPriceComponentListTransformer::class;

        $presenter = new BookingDesignerPriceComponentPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
