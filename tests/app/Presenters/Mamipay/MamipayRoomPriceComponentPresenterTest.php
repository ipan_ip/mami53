<?php

namespace App\Presenters\Mamipay;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDraftPriceTransformer;
use App\Transformers\Mamipay\MamipayRoomPriceComponentTransformer;

class MamipayRoomPriceComponentPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerData(): void
    {
        $presenter = new MamipayRoomPriceComponentPresenter(
            MamipayRoomPriceComponentPresenter::DATA
        );

        $expected = MamipayRoomPriceComponentTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerDraftPrice(): void
    {
        $presenter = new MamipayRoomPriceComponentPresenter(
            MamipayRoomPriceComponentPresenter::DRAFT_PRICE
        );

        $expected = BookingDraftPriceTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}