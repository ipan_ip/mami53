<?php

namespace App\Presenters\Mamipay;

use App\Entities\Mamipay\MamipayInvoice;
use App\Test\MamiKosTestCase;
use App\Transformers\Mamipay\Invoice\InvoiceTransformer;

class MamipayInvoicePresenterTest extends MamiKosTestCase
{
    public function testGetTransformer()
    {
        $transformer = new class extends InvoiceTransformer {
            public function transform(MamipayInvoice $invoice): array
            {
                return [];
            }
        };

        $presenter = new MamipayInvoicePresenter($transformer);

        $this->assertSame($transformer, $presenter->getTransformer());
    }
}
