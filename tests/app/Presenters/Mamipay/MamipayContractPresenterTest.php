<?php

namespace App\Presenters\Mamipay;

use App\Test\MamiKosTestCase;
use App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer;

class MamipayContractPresenterTest extends MamiKosTestCase
{
    public function testGetTransformer(): void
    {
        $transformer = $this->mock(MamipayContractMyRoomDetailTransformer::class);

        $presenter = new MamipayContractPresenter($transformer);
        $this->assertSame($transformer, $presenter->getTransformer());
    }
}
