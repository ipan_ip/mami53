<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\Contract\MamipayTenantTransformer;
use App\Transformers\Mamipay\FinancialReport\IncomeTransformer;

class MamipayTenantPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithContractManagement(): void
    {
        $presenter = new MamipayTenantPresenter('contract-management');
        $expected = MamipayTenantTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithListIncomeFinancialReport(): void
    {
        $presenter = new MamipayTenantPresenter('list-income-financial-report');
        $expected = IncomeTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
