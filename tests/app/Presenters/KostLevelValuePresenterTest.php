<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\KostLevelValue\KostLevelValueTransformer;

class KostLevelValuePresenterTest extends MamiKosTestCase
{
    /**
     * @group BG-3000
     */
    public function testListPresenter(): void
    {
        $expected = KostLevelValueTransformer::class;
        $presenter = new KostLevelValuePresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    /**
     * @group BG-3000
     */
    public function testPresenterWithNoParam(): void
    {
        $expected = KostLevelValueTransformer::class;
        $presenter = new KostLevelValuePresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
