<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingPaymentDetailTransformer;

class BookingPaymentPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithDetail(): void
    {
        $expected = BookingPaymentDetailTransformer::class;

        $presenter = new BookingPaymentPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
