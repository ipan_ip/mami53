<?php

namespace App\Presenters;

use App\Entities\Consultant\PotentialProperty;
use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\PotentialProperty\DetailTransformer;
use App\Transformers\Consultant\PotentialProperty\ListTransformer;
use App\User;

class PotentialPropertyPresenterTest extends MamiKosTestCase
{
    public function testList(): void
    {
        $this->assertEquals('list', PotentialPropertyPresenter::LIST);
    }

    public function testDetail(): void
    {
        $this->assertEquals('detail', PotentialPropertyPresenter::DETAIL);
    }

    public function testGetTransformerWithList(): void
    {
        $presenter = new PotentialPropertyPresenter('list');
        $expected = ListTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithDetail(): void
    {
        $presenter = new PotentialPropertyPresenter('detail');
        $expected = DetailTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testPresentIndexes(): void
    {
        $user = factory(User::class)->create();
        $property = factory(PotentialProperty::class)->create(['is_priority' => true])->toArray();
        $property['owner_id'] = $property['consultant_potential_owner_id'];
        $property['creator_name'] = $user->name;
        $property['offered_product'] = ['bbk', 'gp'];

        $expected = [
            'total' => 1,
            'data' => [
                [
                    'id' => $property['id'],
                    'owner_id' => $property['owner_id'],
                    'name' => $property['name'],
                    'area_city' => $property['area_city'],
                    'province' => $property['province'],
                    'created_by' => $property['creator_name'],
                    'bbk_status' => $property['bbk_status'],
                    'followup_status' => $property['followup_status'],
                    'product_offered' => $property['offered_product'],
                    'priority' => 'high',
                    'created_at' => $property['created_at']
                ]
            ]
        ];

        $property = ['_source' => $property];
        $properties = [
            'hits' => [
                'total' => 1,
                'hits' => [
                    $property
                ]
            ]
        ];

        $presenter = new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST);
        $this->assertEquals($expected, $presenter->presentIndexes($properties));
    }
}
