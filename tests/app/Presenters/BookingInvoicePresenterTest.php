<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingInvoicesTransformer;

class BookingInvoicePresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $presenter = new BookingInvoicePresenter('list');
        $expected = BookingInvoicesTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
