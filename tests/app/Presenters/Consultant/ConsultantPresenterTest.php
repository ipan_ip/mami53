<?php

namespace App\Presenters\Consultant;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\Consultant\AdminBSETransformer;
use App\Transformers\Consultant\Consultant\SalesMotionListTransformer;

class ConsultantPresenterTest extends MamiKosTestCase
{
    public function testTransformWithSalesMotionList(): void
    {
        $presenter = new ConsultantPresenter(
            ConsultantPresenter::SALES_MOTION_LIST
        );

        $actual = $presenter->getTransformer();
        $this->assertInstanceOf(
            SalesMotionListTransformer::class,
            $actual
        );
    }

    public function testTransformerWithAdminBSE(): void
    {
        $presenter = new ConsultantPresenter(
            ConsultantPresenter::ADMIN_BSE
        );

        $actual = $presenter->getTransformer();
        $this->assertInstanceOf(
            AdminBSETransformer::class,
            $actual
        );
    }
}
