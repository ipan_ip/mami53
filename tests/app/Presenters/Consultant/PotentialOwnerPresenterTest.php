<?php

namespace App\Presenters\Consultant;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\PotentialOwner\AdminListTransformer;
use App\Transformers\Consultant\PotentialOwner\ConsultantListTransformer;
use App\Transformers\Consultant\PotentialOwner\ConsultantDetailTransformer;

class PotentialOwnerPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerAdminList(): void
    {
        $presenter = new PotentialOwnerPresenter(
            PotentialOwnerPresenter::ADMIN_LIST
        );

        $expected = AdminListTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerConsultantList(): void
    {
        $presenter = new PotentialOwnerPresenter(
            PotentialOwnerPresenter::CONSULTANT_LIST
        );

        $expected = ConsultantListTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerConsultantDetail(): void
    {
        $presenter = new PotentialOwnerPresenter(
            PotentialOwnerPresenter::CONSULTANT_DETAIL
        );

        $expected = ConsultantDetailTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
