<?php

namespace App\Presenters\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\ActivityManagement\ActivityProgress\ActivityProgressTransformer;

class ActivityProgressPresenterTest extends MamiKosTestCase
{
    public function testGetTransformer(): void
    {
        $expected = new class extends ActivityProgressTransformer {
            public function transform(ActivityProgress $progress): array
            {
                return [];
            }
        };

        $presenter = new ActivityProgressPresenter($expected);
        $actual = $presenter->getTransformer();

        $this->assertSame($expected, $actual);
    }
}
