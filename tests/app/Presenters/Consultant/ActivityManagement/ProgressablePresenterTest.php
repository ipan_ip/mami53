<?php

namespace App\Presenters\Consultant\ActivityManagement;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\ActivityManagement\FunnelSearchTransformer;
use Exception;

class ProgressablePresenterTest extends MamiKosTestCase
{
    public function testFunnelSearch(): void
    {
        $this->assertEquals('funnel_search', ProgressablePresenter::FUNNEL_SEARCH);
    }

    public function testSetStageCount(): void
    {
        $presenter = new ProgressablePresenter(
            ProgressablePresenter::FUNNEL_SEARCH
        );
        $actual = $presenter->setStageCount(0);
        $expected = ProgressablePresenter::class;

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithFunnelSearch(): void
    {
        $expected = FunnelSearchTransformer::class;
        $presenter = new ProgressablePresenter(
            ProgressablePresenter::FUNNEL_SEARCH
        );
        $presenter->setStageCount(0);
        $actual = $presenter->getTransformer();
        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithoutSetStageCountShouldThrowException(): void
    {
        $expected = Exception::class;
        $presenter = new ProgressablePresenter(
            ProgressablePresenter::FUNNEL_SEARCH
        );
        $this->expectException($expected);
        $actual = $presenter->getTransformer();
    }
}
