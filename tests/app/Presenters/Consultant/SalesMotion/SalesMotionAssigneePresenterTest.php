<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\SalesMotion\SalesMotionAssigneeAdminListTransformer;

class SalesMotionAssigneePresenterTest extends MamiKosTestCase
{
    public function testPresenterTypeAdminList(): void
    {
        $this->assertEquals('presenter-type-admin-list', SalesMotionAssigneePresenter::PRESENTER_TYPE_ADMIN_LIST);
    }

    public function testGetTransformer(): void
    {
        $presenter = new SalesMotionAssigneePresenter(SalesMotionAssigneePresenter::PRESENTER_TYPE_ADMIN_LIST);
        $actual = $presenter->getTransformer();
        $expected = SalesMotionAssigneeAdminListTransformer::class;

        $this->assertInstanceOf($expected, $actual);
    }
}
