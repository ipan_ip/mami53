<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\SalesMotion\DataAssociatedTransformer;
use App\Transformers\Consultant\SalesMotion\ProgressDetailTransformer;
use App\Transformers\Consultant\SalesMotion\ProgressListTransformer;

class SalesMotionProgressPresenterTest extends MamiKosTestCase
{
    public function testProgressList(): void
    {
        $this->assertEquals(
            'progress-list',
            SalesMotionProgressPresenter::PROGRESS_LIST
        );
    }

    public function testGetTransformerWithProgressList(): void
    {
        $presenter = new SalesMotionProgressPresenter(
            SalesMotionProgressPresenter::PROGRESS_LIST
        );

        $expected = ProgressListTransformer::class;
        $actual = $presenter->getTransformer();
        $this->assertInstanceOf(
            $expected,
            $actual
        );
    }

    public function testGetTransformerWithProgressDetail(): void
    {
        $presenter = new SalesMotionProgressPresenter(
            SalesMotionProgressPresenter::PROGRESS_DETAIL
        );

        $expected = ProgressDetailTransformer::class;
        $actual = $presenter->getTransformer();
        $this->assertInstanceOf(
            $expected,
            $actual
        );
    }

    public function testGetTransformerWithDataAssociated(): void
    {
        $presenter = new SalesMotionProgressPresenter(
            SalesMotionProgressPresenter::DATA_ASSOCIATED
        );

        $expected = DataAssociatedTransformer::class;
        $actual = $presenter->getTransformer();
        $this->assertInstanceOf(
            $expected,
            $actual
        );
    }
}
