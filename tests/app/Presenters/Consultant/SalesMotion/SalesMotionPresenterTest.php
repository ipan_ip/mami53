<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\SalesMotion\SalesMotionAdminDetailTransformer;
use App\Transformers\Consultant\SalesMotion\SalesMotionAdminListTransformer;
use App\Transformers\Consultant\SalesMotion\SalesMotionListTransformer;

class SalesMotionPresenterTest extends MamiKosTestCase
{
    public function testSalesMotionAdminList(): void
    {
        $this->assertEquals('admin-list', SalesMotionPresenter::PRESENTER_TYPE_ADMIN_LIST);
    }

    public function testSalesMotionAdminDetail(): void
    {
        $this->assertEquals('sales-motion-admin-detail', SalesMotionPresenter::PRESENTER_TYPE_ADMIN_DETAIL);
    }

    public function testGetTransformerWithAdminList(): void
    {
        $presenter = new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_ADMIN_LIST);

        $this->assertInstanceOf(SalesMotionAdminListTransformer::class, $presenter->getTransformer());
    }

    public function testGetTransformerWithConsultantList(): void
    {
        $presenter = new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_CONSULTANT_LIST);

        $this->assertInstanceOf(SalesMotionListTransformer::class, $presenter->getTransformer());
    }

    public function testGetTransformerWithAdminDetail(): void
    {
        $presenter = new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_ADMIN_DETAIL);

        $this->assertInstanceOf(SalesMotionAdminDetailTransformer::class, $presenter->getTransformer());
    }
}
