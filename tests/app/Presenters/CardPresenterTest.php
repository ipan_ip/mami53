<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Card\ListCardTransformer;

class CardPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ListCardTransformer::class;

        $presenter = new CardPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = ListCardTransformer::class;

        $presenter = new CardPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
