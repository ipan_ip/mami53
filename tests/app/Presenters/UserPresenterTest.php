<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Auth\UserLoginTransformer;

class UserPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithLogin(): void
    {
        $expected = UserLoginTransformer::class;

        $presenter = new UserPresenter('login');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
