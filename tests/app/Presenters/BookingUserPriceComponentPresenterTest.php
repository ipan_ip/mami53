<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserPriceComponentListTransformer;

class BookingUserPriceComponentPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = BookingUserPriceComponentListTransformer::class;

        $presenter = new BookingUserPriceComponentPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
