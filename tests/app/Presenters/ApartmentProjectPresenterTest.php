<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\ApartmentProject\ApartmentSuggestionTransformer;
use App\Transformers\ApartmentProject\DetailTransformer;
use App\Transformers\ApartmentProject\ListTransformer;
use App\Transformers\ApartmentProject\WebSimpleTransformer;

class ApartmentProjectPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithWebSimple(): void
    {
        $expected = WebSimpleTransformer::class;

        $presenter = new ApartmentProjectPresenter('web-simple');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithSuggestion(): void
    {
        $expected = ApartmentSuggestionTransformer::class;

        $presenter = new ApartmentProjectPresenter('suggestion');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithList(): void
    {
        $expected = ListTransformer::class;

        $presenter = new ApartmentProjectPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = DetailTransformer::class;

        $presenter = new ApartmentProjectPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
