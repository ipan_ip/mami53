<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\FlashSale\AdminTransformer;
use App\Transformers\FlashSale\AdminUpdateTransformer;
use App\Transformers\FlashSale\ApiIndexTransformer;
use App\Transformers\FlashSale\ApiSingleTransformer;
use App\Transformers\FlashSale\ApiSpecificTransformer;
use App\Transformers\FlashSale\WebIndexTransformer;

class FlashSalesPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithAdminUpdate(): void
    {
        $expected = AdminUpdateTransformer::class;

        $presenter = new FlashSalePresenter('admin-update');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithWebIndex(): void
    {
        $expected = WebIndexTransformer::class;

        $presenter = new FlashSalePresenter('web-index');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithApiIndex(): void
    {
        $expected = ApiIndexTransformer::class;

        $presenter = new FlashSalePresenter('api-index');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithApiSingle(): void
    {
        $expected = ApiSingleTransformer::class;

        $presenter = new FlashSalePresenter('api-single');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithApiSpecific(): void
    {
        $expected = ApiSpecificTransformer::class;

        $presenter = new FlashSalePresenter('api-specific');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = AdminTransformer::class;

        $presenter = new FlashSalePresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
