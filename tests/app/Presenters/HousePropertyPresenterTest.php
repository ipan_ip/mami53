<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\HouseProperty\HousePropertyListTransformer;
use App\Transformers\HouseProperty\HousePropertyTransformer;

class HousePropertyPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = HousePropertyListTransformer::class;

        $presenter = new HousePropertyPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = HousePropertyTransformer::class;

        $presenter = new HousePropertyPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
