<?php

namespace App\Presenters\Room;

use App\Test\MamiKosTestCase;
use App\Transformers\Room\RoomUnitAvailableTransformer;
use App\Transformers\Room\RoomUnitLevelListTransformer;
use App\Transformers\Room\RoomUnitWithTenantTransformer;
use App\Transformers\Room\RoomUnitTransformer;

class RoomUnitPresenterTest extends MamiKosTestCase
{
    public function testConstructWithNullTransformer(): void
    {
        $presenter = new RoomUnitPresenter();
        $this->assertInstanceOf(RoomUnitPresenter::class, $presenter);
    }

    public function testConstructWithConsultantAvailableRoomTransformer(): void
    {
        $presenter = new RoomUnitPresenter('consultant-available-room');
        $this->assertInstanceOf(RoomUnitPresenter::class, $presenter);
    }

    public function testGetTransformerWithNullTransformerShouldReturnDefaultTransformer(): void
    {
        $presenter = new RoomUnitPresenter();
        $this->assertInstanceOf(RoomUnitTransformer::class, $presenter->getTransformer());
    }

    public function testGetTransformerWithRoomAvailableTransformerShouldReturnCorrectTransformer(): void
    {
        $presenter = new RoomUnitPresenter('consultant-available-room');
        $this->assertInstanceOf(RoomUnitAvailableTransformer::class, $presenter->getTransformer());
    }
    public function testGetTransformerWithDetailTenantShouldReturnCorrectTransformer(): void
    {
        $presenter = new RoomUnitPresenter('detail-tenant');
        $this->assertInstanceOf(RoomUnitWithTenantTransformer::class, $presenter->getTransformer());
    }

    public function testGetTransformerWithAdminLevelListShouldReturnCorrectTransformer(): void
    {
        $presenter = new RoomUnitPresenter(
            RoomUnitPresenter::ADMIN_LEVEL_LIST
        );

        $this->assertInstanceOf(RoomUnitLevelListTransformer::class, $presenter->getTransformer());
    }
}
