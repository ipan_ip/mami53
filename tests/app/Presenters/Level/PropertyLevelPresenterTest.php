<?php

namespace App\Presenters\Level;

use App\Test\MamiKosTestCase;
use App\Transformers\Level\SelectLevelTransformer;

class PropertyLevelPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithSelectLevelDropdown(): void
    {
        $presenter = new PropertyLevelPresenter(PropertyLevelPresenter::SELECT_LEVEL_DROPDOWN);
        $expected = SelectLevelTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
