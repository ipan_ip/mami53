<?php

namespace App\Presenters\Owner;

use App\Test\MamiKosTestCase;
use App\Transformers\Owner\StatusHistory\ListTransformer;

class StatusHistoryPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $presenter = new StatusHistoryPresenter(StatusHistoryPresenter::LIST);

        $expected = ListTransformer::class;
        $actual = $presenter->getTransformer();
        $this->assertInstanceOf($expected, $actual);
    }
}
