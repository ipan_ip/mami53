<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Point\PointActivityListTransformer;
use App\Transformers\Point\PointExpiryDetailTransformer;
use App\Transformers\Point\PointHistoryListTransformer;

class PointPresenterTest extends MamiKosTestCase
{
    public function testResponseTypeActivityList(): void
    {
        $this->assertEquals('activity-list', PointPresenter::RESPONSE_TYPE_ACTIVITY_LIST);
    }

    public function testResponseTypeExpiryDetail(): void
    {
        $this->assertEquals('expiry-detail', PointPresenter::RESPONSE_TYPE_EXPIRY_DETAIL);
    }

    public function testResponseTypeHistoryList(): void
    {
        $this->assertEquals('history-list', PointPresenter::RESPONSE_TYPE_HISTORY_LIST);
    }

    public function testGetTransformerWithActivityList(): void
    {
        $transformer = PointActivityListTransformer::class;

        $presenter = new PointPresenter();
        $presenter->setResponseType(PointPresenter::RESPONSE_TYPE_ACTIVITY_LIST);

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }

    public function testGetTransformerWithExpiryDetail(): void
    {
        $transformer = PointExpiryDetailTransformer::class;

        $presenter = new PointPresenter();
        $presenter->setResponseType(PointPresenter::RESPONSE_TYPE_EXPIRY_DETAIL);

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }

    public function testGetTransformerWithHistoryList(): void
    {
        $transformer = PointHistoryListTransformer::class;

        $presenter = new PointPresenter();
        $presenter->setResponseType(PointPresenter::RESPONSE_TYPE_HISTORY_LIST);

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }

    public function testGetTransformerWithInvalidArgument(): void
    {
        $transformer = PointHistoryListTransformer::class;

        $presenter = new PointPresenter();
        $presenter->setResponseType('test');

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }
}
