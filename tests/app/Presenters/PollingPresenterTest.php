<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Polling\PollingDetailTransformer;

class PollingPresenterTest extends MamiKosTestCase
{
    public function testResponseTypePollingDetail(): void
    {
        $this->assertEquals('polling-detail', PollingPresenter::RESPONSE_TYPE_POLLING_DETAIL);
    }

    public function testGetTransformerWithPollingDetail(): void
    {
        $transformer = PollingDetailTransformer::class;

        $presenter = new PollingPresenter();
        $presenter->setResponseType(PollingPresenter::RESPONSE_TYPE_POLLING_DETAIL);

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }

    public function testGetTransformerWithInvalidArgument(): void
    {
        $transformer = PollingDetailTransformer::class;

        $presenter = new PollingPresenter();
        $presenter->setResponseType('test');

        $this->assertInstanceOf($transformer, $presenter->getTransformer());
    }
}
