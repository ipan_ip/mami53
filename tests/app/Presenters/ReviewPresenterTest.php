<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\ReviewTransformer;
use App\Transformers\Room\ListReviewTransformer;

class ReviewPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ListReviewTransformer::class;

        $presenter = new ReviewPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = ReviewTransformer::class;

        $presenter = new ReviewPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
