<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\CardPremium\ListCardPremiumTransformer;

class CardPremiumPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ListCardPremiumTransformer::class;

        $presenter = new CardPremiumPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = ListCardPremiumTransformer::class;

        $presenter = new CardPremiumPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
