<?php

namespace App\Presenters\Contract;

use App\Test\MamiKosTestCase;
use App\Transformers\Contract\ContractAdminListTransformer;
use App\Transformers\Contract\ContractUserDetailTransformer;
use App\Transformers\Contract\ContractUserTransformer;

class ContractPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithAdminList(): void
    {
        $presenter = new ContractPresenter('admin-list');

        $expected = ContractAdminListTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithUserList(): void
    {
        $presenter = new ContractPresenter('user-list');

        $expected = ContractUserTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithUserDetail(): void
    {
        $presenter = new ContractPresenter('user-detail');

        $expected = ContractUserDetailTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $presenter = new ContractPresenter();

        $this->assertNull($presenter->getTransformer());
    }
}
