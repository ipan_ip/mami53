<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\AreaTransformer;

class AreaPresenterTest extends MamiKosTestCase
{
    public function testGetTransformer(): void
    {
        $expected = AreaTransformer::class;

        $presenter = new AreaPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
