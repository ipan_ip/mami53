<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Marketplace\MarketplaceProductDetailTransformer;
use App\Transformers\Marketplace\MarketplaceProductListTransformer;
use App\Transformers\Marketplace\MarketplaceProductUpdateTransformer;

class MarketPlaceProductPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = MarketplaceProductListTransformer::class;

        $presenter = new MarketplaceProductPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithUpdate(): void
    {
        $expected = MarketplaceProductUpdateTransformer::class;

        $presenter = new MarketplaceProductPresenter('update');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = MarketplaceProductDetailTransformer::class;

        $presenter = new MarketplaceProductPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
