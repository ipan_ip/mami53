<?php

namespace App\Presenters\Booking;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingUserDraftDetailTransformer;
use App\Transformers\Booking\BookingUserDraftHomepageShortcutTransformer;
use App\Transformers\Booking\BookingUserDraftListTransformer;
use App\Transformers\Booking\BookingUserViewedRoomListTransformer;

class BookingUserDraftPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $presenter = new BookingUserDraftPresenter('list');

        $expected = BookingUserDraftListTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithDetail(): void
    {
        $presenter = new BookingUserDraftPresenter('detail');

        $expected = BookingUserDraftDetailTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithHomepageShortcut(): void
    {
        $presenter = new BookingUserDraftPresenter('homepage-shortcut');

        $expected = BookingUserDraftHomepageShortcutTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListRoomHistory(): void
    {
        $presenter = new BookingUserDraftPresenter('list-room-history');

        $expected = BookingUserViewedRoomListTransformer::class;

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $presenter = new BookingUserDraftPresenter();

        $this->assertNull($presenter->getTransformer());
    }
}
