<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingContractDetailTransformer;

class BookingContractPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithDetail(): void
    {
        $expected = BookingContractDetailTransformer::class;

        $presenter = new BookingContractPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
