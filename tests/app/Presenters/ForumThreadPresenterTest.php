<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Forum\ForumThreadDetailSimpleTransformer;
use App\Transformers\Forum\ForumThreadDetailTransformer;
use App\Transformers\Forum\ForumThreadListSimpleTransformer;
use App\Transformers\Forum\ForumThreadListTransformer;

class ForumThreadPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithDetailSimple(): void
    {
        $expected = ForumThreadDetailSimpleTransformer::class;

        $presenter = new ForumThreadPresenter('detail-simple');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithListSimple(): void
    {
        $expected = ForumThreadListSimpleTransformer::class;

        $presenter = new ForumThreadPresenter('list-simple');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithList(): void
    {
        $expected = ForumThreadListTransformer::class;

        $presenter = new ForumThreadPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithDetail(): void
    {
        $expected = ForumThreadDetailTransformer::class;

        $presenter = new ForumThreadPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
