<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Premium\PremiumFaqListTransformer;

class PremiumFaqPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = PremiumFaqListTransformer::class;

        $presenter = new PremiumFaqPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
