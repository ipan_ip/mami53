<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Premium\FinancialReport\PremiumExpenseTransformer;

class PremiumHistoryPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithListExpenseFinancialReport(): void
    {
        $expected = PremiumExpenseTransformer::class;

        $presenter = new PremiumHistoryPresenter('list-expenses-financial-report');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
