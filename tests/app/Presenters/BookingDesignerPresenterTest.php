<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Booking\BookingDesignerDetailTransformer;
use App\Transformers\Booking\BookingDesignerListTransformer;

class BookingDesignerPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithDetail(): void
    {
        $expected = BookingDesignerDetailTransformer::class;

        $presenter = new BookingDesignerPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithList(): void
    {
        $expected = BookingDesignerListTransformer::class;

        $presenter = new BookingDesignerPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
