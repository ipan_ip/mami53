<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Consultant\ActivityManagement\ShowAllTaskTransformer;
use App\Transformers\Consultant\ActivityManagement\StageStatusTransformer;
use App\Transformers\Consultant\ActivityManagement\TaskStagesTransformer;

class ActivityFormPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithShowAllTask(): void
    {
        $presenter = new ActivityFormPresenter('show-all-task', 5);
        $expected = ShowAllTaskTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithStageStatus(): void
    {
        $presenter = new ActivityFormPresenter('stage-status');
        $presenter->setCurrentStage(5);
        $expected = StageStatusTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithTaskStage(): void
    {
        $presenter = new ActivityFormPresenter('task-stages');
        $presenter->setCurrentStage(5);
        $expected = TaskStagesTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
