<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\UserVoucherDetailTransformer;
use App\Transformers\UserVoucherTransformer;

class UserVoucherPresenterTest extends MamiKosTestCase
{
    public function testResponseTypeList(): void
    {
        $this->assertEquals('list', UserVoucherPresenter::RESPONSE_TYPE_LIST);
    }

    public function testResponseTypeDetail(): void
    {
        $this->assertEquals('detail', UserVoucherPresenter::RESPONSE_TYPE_DETAIL);
    }

    public function testGetTransformerWithDetail(): void
    {
        $expected = UserVoucherDetailTransformer::class;

        $presenter = new UserVoucherPresenter();
        $presenter->setResponseType(UserVoucherPresenter::RESPONSE_TYPE_DETAIL);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithList(): void
    {
        $expected = UserVoucherTransformer::class;

        $presenter = new UserVoucherPresenter();
        $presenter->setResponseType(UserVoucherPresenter::RESPONSE_TYPE_LIST);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithInvalidArgument(): void
    {
        $expected = UserVoucherTransformer::class;

        $presenter = new UserVoucherPresenter();
        $presenter->setResponseType('test');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
