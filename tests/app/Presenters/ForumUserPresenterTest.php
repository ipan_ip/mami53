<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Forum\ForumUserRegisterTransformer;

class ForumUserPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithRegister(): void
    {
        $expected = ForumUserRegisterTransformer::class;

        $presenter = new ForumUserPresenter('register');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
