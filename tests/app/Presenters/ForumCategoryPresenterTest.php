<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Forum\ForumCategoryDetailTransformer;
use App\Transformers\Forum\ForumCategoryListTransformer;
use App\Transformers\Forum\ForumCategoryOptionTransformer;

class ForumCategoryPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ForumCategoryListTransformer::class;

        $presenter = new ForumCategoryPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithOption(): void
    {
        $expected = ForumCategoryOptionTransformer::class;

        $presenter = new ForumCategoryPresenter('option');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithDetail(): void
    {
        $expected = ForumCategoryDetailTransformer::class;

        $presenter = new ForumCategoryPresenter('detail');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
