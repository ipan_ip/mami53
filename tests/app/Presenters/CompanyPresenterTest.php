<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Jobs\Company\CompanyTransformer;
use App\Transformers\Jobs\Company\ProfileListTransformer;

class CompanyPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $expected = ProfileListTransformer::class;

        $presenter = new CompanyPresenter('list');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithoutArgument(): void
    {
        $expected = CompanyTransformer::class;

        $presenter = new CompanyPresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
