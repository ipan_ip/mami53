<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\Reward\RedeemDetailTransformer;
use App\Transformers\Reward\RedeemListTransformer;
use App\Transformers\Reward\RewardDetailTransformer;
use App\Transformers\Reward\RewardListTransformer;

class RewardPresenterTest extends MamiKosTestCase
{
    public function testResponseTypeRewardList(): void
    {
        $this->assertEquals('reward-list', RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
    }

    public function testResponseTypeRewardDetail(): void
    {
        $this->assertEquals('reward-detail', RewardPresenter::RESPONSE_TYPE_REWARD_DETAIL);
    }

    public function testResponseTypeRedeemList(): void
    {
        $this->assertEquals('redeem-list', RewardPresenter::RESPONSE_TYPE_REDEEM_LIST);
    }

    public function testResponseTypeRedeemDetail(): void
    {
        $this->assertEquals('redeem-detail', RewardPresenter::RESPONSE_TYPE_REDEEM_DETAIL);
    }

    public function testGetTransformerWithRewardList(): void
    {
        $expected = RewardListTransformer::class;

        $presenter = new RewardPresenter();
        $presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRewardDetail(): void
    {
        $expected = RewardDetailTransformer::class;

        $presenter = new RewardPresenter();
        $presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_DETAIL);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRedeemList(): void
    {
        $expected = RedeemListTransformer::class;

        $presenter = new RewardPresenter();
        $presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REDEEM_LIST);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithRedeemDetail(): void
    {
        $expected = RedeemDetailTransformer::class;

        $presenter = new RewardPresenter();
        $presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REDEEM_DETAIL);

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }

    public function testGetTransformerWithInvalidArgument(): void
    {
        $expected = RewardListTransformer::class;

        $presenter = new RewardPresenter();
        $presenter->setResponseType('test');

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
