<?php

namespace App\Presenters;

use App\Test\MamiKosTestCase;
use App\Transformers\ActivityLog\ListTransformer;

class ActivityLogPresenterTest extends MamiKosTestCase
{
    public function testGetTransformerWithList(): void
    {
        $presenter = new ActivityLogPresenter('list');
        $expected = ListTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }
}
