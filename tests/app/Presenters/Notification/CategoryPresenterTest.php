<?php

namespace App\Presenters\Notification;

use App\Test\MamiKosTestCase;
use App\Transformers\Notification\CategoryTransformer;

class CategoryPresenterTest extends MamiKosTestCase
{
    public function testGetTransformer(): void
    {
        $expected = CategoryTransformer::class;

        $presenter = new CategoryPresenter();
        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
