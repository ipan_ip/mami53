<?php

namespace App\Presenters\Notification;

use App\Test\MamiKosTestCase;
use App\Transformers\Notification\NotificationTransformer;

class NotificationPresenterTest extends MamiKosTestCase
{
    public function testGetTransformer(): void
    {
        $expected = NotificationTransformer::class;

        $presenter = new NotificationPresenter();
        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
