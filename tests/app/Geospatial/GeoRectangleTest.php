<?php

namespace App\Geospatial;

use App\Test\MamiKosTestCase;
use Exception;
use OutOfBoundsException;

class GeoRectangleTest extends MamiKosTestCase
{
    protected $withDbTransaction = false;

    public function testConstruct()
    {
        $rect = new GeoRectangle(10, 40, 30, 20);
        $this->assertEquals(10, $rect->left);
        $this->assertEquals(40, $rect->top);
        $this->assertEquals(30, $rect->right);
        $this->assertEquals(20, $rect->bottom);
    }

    public function testConstructShouldThrowExceptionWhenLongitudeReversed()
    {
        $this->expectException(Exception::class);
        $rect = new GeoRectangle(30, 40, 10, 20);
    }

    public function testConstructShouldThrowExceptionWhenLatitudeReversed()
    {
        $this->expectException(Exception::class);
        $rect = new GeoRectangle(10, 20, 30, 40);
    }

    public function testConstructShouldThrowExceptionWhenLeftLongitudeIsOutOfBoundWithNegativeValue()
    {
        $this->expectException(OutOfBoundsException::class);
        $rect = new GeoRectangle(-181, 40, 30, 20);
    }

    public function testConstructShouldThrowExceptionWhenRightLongitudeIsOutOfBoundWithPositiveValue()
    {
        $this->expectException(OutOfBoundsException::class);
        $rect = new GeoRectangle(10, 40, 181, 20);
    }

    public function testConstructShouldThrowExceptionWhenTopLongitudeIsOutOfBoundWithPositiveValue()
    {
        $this->expectException(OutOfBoundsException::class);
        $rect = new GeoRectangle(10, 91, 30, 20);
    }

    public function testConstructShouldThrowExceptionWhenBottomLongitudeIsOutOfBoundWithNegativeValue()
    {
        $this->expectException(OutOfBoundsException::class);
        $rect = new GeoRectangle(10, 40, 30, -91);
    }
}