<?php

namespace App\Listeners;

use App\Entities\Point\PointActivity;
use App\Entities\Point\PointSetting;
use App\Events\EligibleEarnPoint;
use App\Repositories\Point\PointRepository;
use App\Test\MamiKosTestCase;
use App\User;

class EarnPointListenerTest extends MamiKosTestCase
{
    private $class;
    private $repo;
    private $event;

    protected function setUp()
    {
        parent::setUp();

        $this->repo = $this->createMock(PointRepository::class);
        $this->event = $this->createMock(EligibleEarnPoint::class);
        $this->class = new EarnPointListener($this->repo);
    }

    public function testHandle_emptyActivity_returnNull()
    {
        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_WithJoinBookingActivity_RepoAddPointCalled()
    {
        $user = $this->createMock(User::class);
        $activity = $this->createMock(PointActivity::class);
        $activity->method('__get')->with('key')->willReturn('owner_join_bbk');
        $this->event->user = $user;
        $this->event->activity = $activity;
        $this->event->notes = ['notes1', 'notes2'];

        $this->repo->expects($this->once())
            ->method('setNotes');
        $this->repo->expects($this->once())
            ->method('addPoint')
            ->willReturn(true);
        $this->repo->expects($this->once())
            ->method('getPointAdded')
            ->willReturn(1);

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_WithJoinGoldplusActivity_RepoAddPointCalled()
    {
        $user = $this->createMock(User::class);
        $activity = $this->createMock(PointActivity::class);
        $activity->method('__get')->with('key')->willReturn('owner_join_goldplus');
        $this->event->user = $user;
        $this->event->activity = $activity;
        $this->event->notes = ['notes1', 'notes2'];

        $this->repo->expects($this->once())
            ->method('setNotes');
        $this->repo->expects($this->once())
            ->method('addPoint')
            ->willReturn(true);
        $this->repo->expects($this->once())
            ->method('getPointAdded')
            ->willReturn(1);

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_WithRoomJoinGoldplusActivity_RepoAddPointCalled()
    {
        $user = $this->createMock(User::class);
        $activity = $this->createMock(PointActivity::class);
        $activity->method('__get')->with('key')->willReturn('owner_room_join_goldplus');
        $this->event->user = $user;
        $this->event->activity = $activity;
        $this->event->notes = ['notes1', 'notes2'];

        $this->repo->expects($this->once())
            ->method('setNotes');
        $this->repo->expects($this->once())
            ->method('addPoint')
            ->willReturn(true);
        $this->repo->expects($this->once())
            ->method('getPointAdded')
            ->willReturn(1);

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_WithAdjustmentTopupActivity()
    {
        $user = $this->createMock(User::class);
        $user->method('__get')->with('id')->willReturn(1);
        $this->event->user = $user;
        $this->event->activity = null;
        $this->event->notes = ['point' => '1'];
        $this->event->adjustment = 'admin_adjustment_topup';

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_WithAdjustmentTopdownActivity()
    {
        $user = $this->createMock(User::class);
        $user->method('__get')->with('id')->willReturn(1);
        $this->event->user = $user;
        $this->event->activity = null;
        $this->event->notes = ['point' => '1'];
        $this->event->adjustment = 'admin_adjustment_topdown';

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }
}
