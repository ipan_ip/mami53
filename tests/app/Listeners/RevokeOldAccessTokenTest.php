<?php

namespace App\Listeners;

use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutEvents;
use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Token as AccessToken;

class RevokeOldAccessTokenTest extends MamiKosTestCase
{
    use WithoutEvents;

    public function testRevokeOldAccessTokenHandlerSuccess()
    {
        $createdCount = rand(3, 7);
        $user = factory(User::class)->create();
        $tokenCollection = factory(AccessToken::class, $createdCount)->make(['user_id' => $user->id, 'client_id' => 1]);

        [$expiredTokens, $revokedTokens] = $tokenCollection->chunk(ceil($createdCount/2));
        $expiredTokens->each(function (AccessToken $token) {
            $token->expires_at = Carbon::now()->subHour(rand(1, 5));
            $token->save();
        });
        $revokedTokens->each(function (AccessToken $token) {
            $token->revoke();
        });
        $tokenCount = AccessToken::where('user_id', $user->id)->count();

        $this->assertDatabaseHas('oauth_access_tokens', ['user_id' => $user->id]);
        $this->assertSame($createdCount, $tokenCount);

        $newToken = factory(AccessToken::class)->create(['user_id' => $user->id, 'client_id' => 1]);

        (new RevokeOldAccessToken)->handle(new AccessTokenCreated($newToken->id, $newToken->user_id, $newToken->client_id));
        
        $newTokenCount = AccessToken::where('user_id', $user->id)->count();
        $this->assertSame(1, $newTokenCount);
    }
}
