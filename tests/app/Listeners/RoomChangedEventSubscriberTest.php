<?php

namespace App\Listeners;

use App\Entities\Room\Room;
use App\Jobs\Room\RecalculateScoreById;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Queue;
use stdClass;

class RoomChangedEventSubscriberTest extends MamiKosTestCase
{
    use WithFaker;

    protected $withEvents = true;

    /**
     * @covers \App\Listeners\RoomChangedEventSubscriber
     */
    public function testChangeRoomAvailableWillAddAvailabilityTracker()
    {
        $room = $this->getDummyRoom();
        $this->assertDatabaseMissing(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );
        $beforeValue = $room->room_available;
        $room->room_available -= 1;
        $afterValue = $room->room_available;
        $room->save();
        $this->assertDatabaseHas(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );
        $room->load('room_available_trackers');
        $this->assertCount(1, $room->room_available_trackers);
        $tracker = $room->room_available_trackers->first();
        $this->assertSame($beforeValue, $tracker->before_value);
        $this->assertSame($afterValue, $tracker->after_value);
    }

    /**
     * @covers \App\Listeners\RoomChangedEventSubscriber
     */
    public function testChangeRoomAvailableAndPriceWillTrackPriceChanges()
    {
        $room = $this->getDummyRoom();
        $this->assertDatabaseMissing(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );

        $beforePrice = $room->price;
        $beforePriceDaily = $room->price_daily;
        $beforePriceWeekly = $room->price_weekly;
        $beforePriceMonthly = $room->price_monthly;
        $beforePriceYearly = $room->price_yearly;

        $room->room_available -= 1;
        $room->price -= 50000;
        $room->price_daily -= 50000;
        $room->price_weekly -= 50000;
        $room->price_monthly -= 50000;
        $room->price_yearly -= 50000;

        $afterPrice = $room->price;
        $afterPriceDaily = $room->price_daily;
        $afterPriceWeekly = $room->price_weekly;
        $afterPriceMonthly = $room->price_monthly;
        $afterPriceYearly = $room->price_yearly;
        $room->save();
        $this->assertDatabaseHas(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );
        $room->load('room_available_trackers');
        $this->assertCount(1, $room->room_available_trackers);
        $tracker = $room->room_available_trackers->first();

        $this->assertSame($beforePrice, $tracker->price_before);
        $this->assertSame($beforePriceDaily, $tracker->price_daily_before);
        $this->assertSame($beforePriceWeekly, $tracker->price_weekly_before);
        $this->assertSame($beforePriceMonthly, $tracker->price_monthly_before);
        $this->assertSame($beforePriceYearly, $tracker->price_yearly_before);

        $this->assertSame($afterPrice, $tracker->price_after);
        $this->assertSame($afterPriceDaily, $tracker->price_daily_after);
        $this->assertSame($afterPriceWeekly, $tracker->price_weekly_after);
        $this->assertSame($afterPriceMonthly, $tracker->price_monthly_after);
        $this->assertSame($afterPriceYearly, $tracker->price_yearly_after);
    }

    /**
     * @covers \App\Listeners\RoomChangedEventSubscriber
     */
    public function testChangePriceOnlyDontAddTracker()
    {
        $room = $this->getDummyRoom();
        $this->assertDatabaseMissing(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );

        $room->price -= 50000;
        $room->price_daily -= 50000;
        $room->price_weekly -= 50000;
        $room->price_monthly -= 50000;
        $room->price_yearly -= 50000;

        $room->save();
        $this->assertDatabaseMissing(
            'designer_availability_tracker',
            [
                'designer_id' => $room->id
            ]
        );
        $room->load('room_available_trackers');
        $this->assertCount(0, $room->room_available_trackers);
    }

    public function testRoomChangedExceptionOnSortScoreCaught()
    {
        $room = $this->getDummyRoom();
        
        $mockEvent = $this->mockAlternatively(stdClass::class);
        $mockEvent->shouldReceive('getTable')->andReturn('designer');
        $mockEvent->shouldReceive('getOriginal')->andReturn(['id' => $room->id]);
        $mockEvent->shouldReceive('getAttributes')->andReturn(['id' => $room->id]);
        
        $this->expectException(Exception::class);

        $bugSnagMock = $this->mockAlternatively('overload:' . Bugsnag::class);
        $bugSnagMock->shouldReceive('notifyException')->once();

        $roomMockSortScore = $this->mockAlternatively(stdClass::class);
        $roomMockSortScore->shouldReceive('updateSortScore')->andThrow(new Exception('Sort Score Failed'));

        $roomMock = $this->mockAlternatively('overload:' . Room::class);
        $roomMock->shouldReceive('find')->andReturn($roomMockSortScore);

        $mockEvent->disable();
        $bugSnagMock->disable();
        $roomMockSortScore->disable();
        $roomMock->disable();
    }

    public function testRoomChangedWithSortScoreField()
    {
        Queue::fake();

        // Create a Kos data
        factory(Room::class)->create(
            [
                'room_available' => 10,
                'sort_score' => 0,
                'address' => null
            ]
        );

        // Room availability change
        $room = Room::first();
        $room->room_available = 3;
        $room->save();

        Queue::assertPushed(RecalculateScoreById::class);

        $room->load('room_available_trackers');
        $this->assertCount(1, $room->room_available_trackers);
    }

    public function testRoomChangedWithoutSortScoreField()
    {
        Queue::fake();

        // Create a Kos data
        factory(Room::class)->create(
            [
                'room_available' => 10,
                'address' => null
            ]
        );

        // Room availability change
        $room = Room::first();
        $room->room_available = 3;
        $room->save();

        Queue::assertPushed(RecalculateScoreById::class);

        $room->load('room_available_trackers');
        $this->assertCount(1, $room->room_available_trackers);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

    private function getDummyRoom(): Room
    {
        return factory(Room::class)->create([
            'price' => 1000000,
            'price_daily' => 1000000,
            'price_weekly' => 1000000,
            'price_monthly' => 1000000,
            'price_yearly' => 1000000,
            'room_available' => 4,
            'room_count' => 4,
            'sort_score' => 0,
            'address' => null
        ]);
    }
}