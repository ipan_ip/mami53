<?php

namespace App\Listeners;

use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Events\RefreshTokenCreated;

class PruneOldRefreshTokenTest extends MamiKosTestCase
{
    
    public function testPruneOldRefreshTokensRunningSuccess()
    {
        $refreshTokenId = 'first-refresh-token-id';
        $accessTokenId = 'first-access-token-id';
        DB::table('oauth_refresh_tokens')->insert([
            'id' => $refreshTokenId,
            'access_token_id' => $accessTokenId,
            'revoked' => 0,
            'expires_at' => Carbon::now()->subDay(2)->format('Y-m-d H:i:s')
        ]);

        $this->assertDatabaseHas('oauth_refresh_tokens', ['id' => $refreshTokenId]);

        $newRefreshTokenId = 'definitely-not-the-old-refresh-token-id';
        DB::table('oauth_refresh_tokens')->insert([
            'id' => $newRefreshTokenId,
            'access_token_id' => $accessTokenId,
            'revoked' => 0,
            'expires_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        (new PruneOldRefreshToken)->handle(new RefreshTokenCreated($newRefreshTokenId, $accessTokenId));

        $this->assertDatabaseMissing('oauth_refresh_tokens', ['id' => $refreshTokenId]);
    }
}
