<?php
namespace App\Listeners;

use \App\Entities\Room\Element\RoomTag;
use \App\Events\RoomTagDeleted;
use \App\Listeners\RoomTagDeletedListener;

use \App\Test\MamiKosTestCase;
use Mockery;

class RoomTagDeletedListenerTest extends MamiKosTestCase
{
    public function testHandle()
    {
        $roomTag = factory(RoomTag::class)->make(['id' => 1]);
        $event = new RoomTagDeleted($roomTag);

        $mock = Mockery::mock('\App\Repositories\RoomTagHistoryRepository');
        $mock->shouldReceive('store')->once()->with($roomTag);

        $listener = (new RoomTagDeletedListener($mock))->handle($event);
    }
}
