<?php

namespace App\Listeners;

use App\Entities\User\UserNotificationCounter;
use App\Events\NotificationRead;
use App\Test\MamiKosTestCase;
use App\User;

class ClearNotificationCounterTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->event = $this->createMock(NotificationRead::class);
        $this->class = new ClearNotificationCounter();
    }

    public function testHandle_ForOwner_ShouldWork()
    {
        $user = factory(User::class)->create();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $this->event->type = 'owner';
        $this->event->user = $user;

        $this->class->handle($this->event);

        $this->assertDatabaseHas((new UserNotificationCounter())->getTable(), [
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 2,
        ]);
        $this->assertDatabaseHas((new UserNotificationCounter())->getTable(), [
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 0,
        ]);
    }

    public function testHandle_ForUser_ShouldWork()
    {
        $user = factory(User::class)->create();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 2
        ]);
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $this->event->type = 'user';
        $this->event->user = $user;

        $this->class->handle($this->event);

        $this->assertDatabaseHas((new UserNotificationCounter())->getTable(), [
            'notification_type' => 'recommendation',
            'user_id' => $user->id,
            'counter' => 0,
        ]);
        $this->assertDatabaseHas((new UserNotificationCounter())->getTable(), [
            'notification_type' => 'survey',
            'user_id' => $user->id,
            'counter' => 2,
        ]);
    }

    public function testHandle_ForOther_ShouldWork()
    {
        $user = factory(User::class)->create();
        factory(UserNotificationCounter::class)->create([
            'notification_type' => 'other',
            'user_id' => $user->id,
            'counter' => 2
        ]);

        $this->event->type = 'other';
        $this->event->user = $user;

        $this->class->handle($this->event);

        $this->assertDatabaseHas((new UserNotificationCounter())->getTable(), [
            'notification_type' => 'other',
            'user_id' => $user->id,
            'counter' => 0
        ]);
    }
}