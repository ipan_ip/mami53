<?php

namespace App\Listeners;

use App\Entities\Point\PointActivity;
use App\Entities\Point\PointSetting;
use App\Events\EligibleEarnPointTenant;
use App\Repositories\Point\PointRepository;
use App\Test\MamiKosTestCase;
use App\User;

class EarnPointTenantListenerTest extends MamiKosTestCase
{
    private $class;
    private $repo;
    private $event;

    protected function setUp()
    {
        parent::setUp();

        $this->repo = $this->createMock(PointRepository::class);
        $this->event = $this->createMock(EligibleEarnPointTenant::class);
        $this->class = new EarnPointTenantListener($this->repo);
    }

    public function testHandle_emptyActivity_returnNull()
    {
        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_withActivity_RepoAddPointCalled()
    {
        $user = $this->createMock(User::class);
        $user->method('__get')->with('id')->willReturn(1);
        $activity = $this->createMock(PointActivity::class);
        $activity->method('__get')->with('key')->willReturn('tenant_paid_booking');
        $this->event->user = $user;
        $this->event->activity = $activity;
        $this->event->notes = ['notes1', 'notes2'];
        $this->event->isMamirooms = true;
        $this->event->invoiceAmount = 1000;

        $this->repo->expects($this->once())
            ->method('setNotes');
        $this->repo->expects($this->once())
            ->method('addPointTenant')
            ->willReturn(true);
        $this->repo->expects($this->once())
            ->method('getPointAdded')
            ->willReturn(1);

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }

    public function testHandle_withActivity_withoutUser_RepoAddPointCalled()
    {
        $user = $this->createMock(User::class);
        $activity = $this->createMock(PointActivity::class);
        $activity->method('__get')->with('key')->willReturn('tenant_paid_booking');
        $this->event->user = $user;
        $this->event->activity = $activity;
        $this->event->notes = ['notes1', 'notes2'];
        $this->event->isMamirooms = true;
        $this->event->invoiceAmount = 1000;

        $this->repo->expects($this->once())
            ->method('setNotes');
        $this->repo->expects($this->once())
            ->method('addPointTenant')
            ->willReturn(true);
        $this->repo->expects($this->once())
            ->method('getPointAdded')
            ->willReturn(1);

        $return = $this->class->handle($this->event);
        $this->assertNull($return);
    }
}
