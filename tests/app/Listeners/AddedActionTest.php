<?php

namespace App\Listeners;

use App\Entities\User\Notification;
use App\Events\NotificationAdded;
use App\Test\MamiKosTestCase;
use App\User;

class AddedActionTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->event = $this->createMock(NotificationAdded::class);
        $this->class = new AddedAction();
    }

    public function testHandle_ShouldWork()
    {
        $this->event->data = [
            'type' => 'room_verif',
            'owner_id' => 123,
            'designer_id' => 100,
            'identifier' => 'room'
        ];

        $this->class->handle($this->event);

        $this->assertDatabaseHas((new Notification())->getTable(), [
            'type' => 'room_verif',
            'user_id' => 123,
            'designer_id' => 100,
            'read' => false,
            'identifier' => 'room'
        ]);
    }
}