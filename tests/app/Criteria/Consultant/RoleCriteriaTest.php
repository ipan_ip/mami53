<?php

namespace App\Criteria\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Repositories\Consultant\ConsultantRepository;
use App\Test\MamiKosTestCase;

class RoleCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(ConsultantRepository::class);

    }

    public function testApplyWithoutRoleShouldShowAllRole(): void
    {
        factory(ConsultantRole::class)->create([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'role' => ConsultantRole::ADMIN
        ]);

        factory(ConsultantRole::class)->create([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'role' => ConsultantRole::SUPPLY
        ]);

        factory(ConsultantRole::class)->create([
            'consultant_id' => factory(Consultant::class)->create()->id,
            'role' => ConsultantRole::DEMAND
        ]);

        $this->assertCount(
            3,
            $this->repository
                ->pushCriteria(new RoleCriteria())
                ->get()
        );
    }

    public function testApplyWithAdminRole(): void
    {
        $expected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $expected->id,
            'role' => ConsultantRole::ADMIN
        ]);

        $actual = $this->repository
            ->pushCriteria(
                new RoleCriteria(ConsultantRole::ADMIN)
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyWithDemandRole(): void
    {
        $expected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $expected->id,
            'role' => ConsultantRole::DEMAND
        ]);

        $actual = $this->repository
            ->pushCriteria(
                new RoleCriteria(ConsultantRole::DEMAND)
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyWithSupplyRole(): void
    {
        $expected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $expected->id,
            'role' => ConsultantRole::SUPPLY
        ]);

        $actual = $this->repository
            ->pushCriteria(
                new RoleCriteria(ConsultantRole::SUPPLY)
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyWithNonAssignedConsultant(): void
    {
        $expected = factory(Consultant::class)->create();

        $actual = $this->repository
            ->pushCriteria(new RoleCriteria())
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyShouldFilterDifferentRole(): void
    {
        $expected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $expected->id,
            'role' => ConsultantRole::SUPPLY
        ]);

        $actual = $this->repository
            ->pushCriteria(
                new RoleCriteria(ConsultantRole::ADMIN)
            )
            ->first();

        $this->assertNull($actual);
    }
}
