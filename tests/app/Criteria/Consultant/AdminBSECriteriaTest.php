<?php

namespace App\Criteria\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Repositories\Consultant\ConsultantRepository;
use App\Test\MamiKosTestCase;

class AdminBSECriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp()
    {
        parent::setUp();
        $this->repository = $this->app->make(ConsultantRepository::class);
    }

    public function testApply()
    {
        $expected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $expected->id,
            'role' => ConsultantRole::ADMIN_BSE
        ]);

        $notExpected = factory(Consultant::class)->create();
        factory(ConsultantRole::class)->create([
            'consultant_id' => $notExpected->id,
            'role' => ConsultantRole::ADMIN
        ]);

        $actual = $this->repository->pushCriteria(AdminBSECriteria::class)->get();

        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual->first()->is($expected)
        );
    }
}
