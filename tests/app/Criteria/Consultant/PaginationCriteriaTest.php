<?php

namespace App\Criteria\Consultant;

use App\MamikosModel;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class PaginationCriteriaTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    public function testDefaultLimit(): void
    {
        $this->assertEquals(10, PaginationCriteria::DEFAULT_LIMIT);
    }

    public function testDefaultOffset(): void
    {
        $this->assertEquals(0, PaginationCriteria::DEFAULT_OFFSET);
    }

    public function testApply(): void
    {
        $criteria = new PaginationCriteria(15, 15);
        $model = Mockery::mock(MamikosModel::class);
        $model->shouldReceive('offset')
            ->with(15)
            ->once()
            ->andReturn($model);
        $model->shouldReceive('limit')
            ->with(15)
            ->once()
            ->andReturn($model);
        $repo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $criteria->apply($model, $repo);
    }

    public function testApplyWithAllDefaultParameter(): void
    {
        $criteria = new PaginationCriteria();
        $model = Mockery::mock(MamikosModel::class);
        $model->shouldReceive('offset')
            ->with(0)
            ->once()
            ->andReturn($model);
        $model->shouldReceive('limit')
            ->with(10)
            ->once()
            ->andReturn($model);
        $repo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $criteria->apply($model, $repo);
    }

    public function testApplyWithDefaultOffsetParameter(): void
    {
        $criteria = new PaginationCriteria(15);
        $model = Mockery::mock(MamikosModel::class);
        $model->shouldReceive('offset')
            ->with(0)
            ->once()
            ->andReturn($model);
        $model->shouldReceive('limit')
            ->with(15)
            ->once()
            ->andReturn($model);
        $repo = Mockery::mock(ActivityProgressRepositoryEloquent::class);
        $criteria->apply($model, $repo);
    }
}
