<?php

namespace App\Criteria\Consultant;

use App\MamikosModel;
use App\Test\MamiKosTestCase;
use Mockery;
use Prettus\Repository\Eloquent\BaseRepository;

class SearchByNameCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $criteria = new SearchByNameCriteria('test');

        $model = Mockery::mock(MamikosModel::class);
        $model->shouldReceive('where')
            ->with('name', 'like', '%test%')
            ->once()
            ->andReturn($model);

        $repo = Mockery::mock(BaseRepository::class);

        $actual = $criteria->apply($model, $repo);
        $this->assertEquals($model, $actual);
    }

    public function testApplyWithEmptyArgument(): void
    {
        $criteria = new SearchByNameCriteria('');

        $model = Mockery::mock(MamikosModel::class);
        $model->shouldNotReceive('where');

        $repo = Mockery::mock(BaseRepository::class);

        $actual = $criteria->apply($model, $repo);
        $this->assertEquals($model, $actual);
    }

    public function testApplyWithNullArgument(): void
    {
        $criteria = new SearchByNameCriteria(null);

        $model = Mockery::mock(MamikosModel::class);
        $model->shouldNotReceive('where');

        $repo = Mockery::mock(BaseRepository::class);

        $actual = $criteria->apply($model, $repo);
        $this->assertEquals($model, $actual);
    }
}
