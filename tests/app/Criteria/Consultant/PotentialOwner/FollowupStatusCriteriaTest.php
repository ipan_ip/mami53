<?php

namespace App\Criteria\Consultant\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Test\MamiKosTestCase;

class FollowupStatusCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(PotentialOwnerRepository::class);
    }

    public function testApplyShouldFindCorrectFollowupStatus(): void
    {
        $expected = factory(PotentialOwner::class)->create(['followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW]);

        $actual = $this->repository
            ->pushCriteria(new FollowupStatusCriteria(PotentialOwner::FOLLOWUP_STATUS_NEW))
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyShouldNotFindIncorrectFollowupStatus(): void
    {
        $expected = factory(PotentialOwner::class)->create(['followup_status' => PotentialOwner::FOLLOWUP_STATUS_DONE]);

        $actual = $this->repository
            ->pushCriteria(new FollowupStatusCriteria(PotentialOwner::FOLLOWUP_STATUS_NEW))
            ->first();

        $this->assertFalse($expected->is($actual));
    }
}
