<?php

namespace App\Criteria\Consultant\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\MamikosModel;
use App\Test\MamiKosTestCase;
use Elasticsearch\Endpoints\Search;
use Mockery;
use Prettus\Repository\Eloquent\BaseRepository;

class SearchCriteriaTest extends MamiKosTestCase
{
    
    protected $model;
    protected $repo;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = Mockery::mock(BaseRepository::class);
    }

    public function testApplyWithQueryShouldFindMatchingId(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $query = PotentialOwner::query();

        $criteria = new SearchCriteria([$owner->id]);
        $actual = $criteria->apply($query, $this->repo);

        $this->assertTrue(
            $actual->first()->is($owner)
        );
    }
}
