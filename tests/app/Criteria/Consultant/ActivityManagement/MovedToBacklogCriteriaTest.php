<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class MovedToBacklogCriteriaTest extends MamiKosTestCase
{
    
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(ActivityProgressRepositoryEloquent::class);
    }

    public function testApply(): void
    {
        $movedToBacklog = factory(ActivityProgress::class)->create([
            'deleted_at' => Carbon::now()
        ]);
        $notMovedToBacklog = factory(ActivityProgress::class)->create();

        $this->repository->pushCriteria(MovedToBacklogCriteria::class);
        $response = $this->repository->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($movedToBacklog));
    }
}
