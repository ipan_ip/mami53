<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\Consultant;
use App\Entities\Mamipay\MamipayContract;
use App\MamikosModel;
use App\Test\MamiKosTestCase;
use Mockery;
use Prettus\Repository\Eloquent\BaseRepository;

class ToDoTaskCriteriaTest extends MamiKosTestCase
{
    
    protected $contract;
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->contract = factory(MamipayContract::class)->create();
        $this->repo = Mockery::mock(BaseRepository::class);
    }

    public function testApplyShouldFindToDoTask(): void
    {
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $this->contract->id,
            'current_stage' => 0,
            'consultant_id' => 1,
        ]);
        $criteria = new ToDoTaskCriteria(1, 1);
        $result = $criteria
            ->apply(MamipayContract::query(), $this->repo)
            ->first();

        $this->assertTrue($result->is($this->contract));
    }

    public function testApplyShouldNotFindOtherStageTask(): void
    {
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $this->contract->id,
            'current_stage' => 1,
            'consultant_id' => 1,
        ]);
        $criteria = new ToDoTaskCriteria(1, 1);
        $result = $criteria
            ->apply(MamipayContract::query(), $this->repo)
            ->first();

        $this->assertNull($result);
    }

    public function testApplyShouldNotFindOtherFunnel(): void
    {
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 0,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $this->contract->id,
            'current_stage' => 1,
            'consultant_id' => 1,
        ]);
        $criteria = new ToDoTaskCriteria(1, 1);
        $result = $criteria
            ->apply(MamipayContract::query(), $this->repo)
            ->first();

        $this->assertNull($result);
    }

    public function testApplyShouldNotFindOtherConsultant(): void
    {
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $this->contract->id,
            'current_stage' => 1,
            'consultant_id' => 0,
        ]);
        $criteria = new ToDoTaskCriteria(1, 1);
        $result = $criteria
            ->apply(MamipayContract::query(), $this->repo)
            ->first();

        $this->assertNull($result);
    }
}
