<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Repositories\Consultant\PotentialPropertyRepositoryEloquent;
use App\Repositories\Consultant\PotentialTenantRepositoryEloquent;
use App\Repositories\Contract\ContractRepositoryEloquent;
use App\Repositories\RoomRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class BacklogTaskCriteriaTest extends MamiKosTestCase
{
    
    protected $contract;
    protected $potentialTenant;
    protected $potentialProperty;
    protected $property;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contract = new ContractRepositoryEloquent(app());
        $this->potentialProperty = new PotentialPropertyRepositoryEloquent(app());
        $this->potentialTenant = new PotentialTenantRepositoryEloquent(app());
        $this->property = new RoomRepositoryEloquent(app());
    }

    public function testApplyWithContractRepository(): void
    {
        $withoutProgress = factory(MamipayContract::class)->create();
        $withProgress = factory(MamipayContract::class)->create();
        $withProgress->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0
            ])
        );

        $this->contract->pushCriteria(new BacklogTaskCriteria(0));
        $response = $this->contract->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($withoutProgress));
    }

    public function testApplyWithPotentialPropertyRepository(): void
    {
        $withoutProgress = factory(PotentialProperty::class)->create();
        $withProgress = factory(PotentialProperty::class)->create();
        $withProgress->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0
            ])
        );

        $this->potentialProperty->pushCriteria(new BacklogTaskCriteria(0));
        $response = $this->potentialProperty->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($withoutProgress));
    }

    public function testApplyWithPotentialTenantRepository(): void
    {
        $withoutProgress = factory(PotentialTenant::class)->create();
        $withProgress = factory(PotentialTenant::class)->create();
        $withProgress->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0
            ])
        );

        $this->potentialTenant->pushCriteria(new BacklogTaskCriteria(0));
        $response = $this->potentialTenant->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($withoutProgress));
    }

    public function testApplyWithPropertyRepository(): void
    {
        $withoutProgress = factory(Room::class)->create([
            'is_active' => false
        ]);
        $withProgress = factory(Room::class)->create();
        $withProgress->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0
            ])
        );

        $this->property
            ->popCriteria(app('App\Criteria\Room\ActiveCriteria'))
            ->pushCriteria(new BacklogTaskCriteria(0));
        $response = $this->property->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($withoutProgress));
    }

    public function testApplyWithConsultantId(): void
    {
        $withoutProgress = factory(Room::class)->create([
            'is_active' => false
        ]);
        $withProgress = factory(Room::class)->create();
        $withProgress->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0,
                'consultant_id' => 0
            ])
        );

        $this->property
            ->popCriteria(app('App\Criteria\Room\ActiveCriteria'))
            ->pushCriteria(new BacklogTaskCriteria(0, 0));
        $response = $this->property->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($withoutProgress));
    }

    public function testApplyWithSoftDeletedProgress(): void
    {
        $expected = factory(Room::class)->create();
        $expected->progress()->save(
            factory(ActivityProgress::class)->make([
                'consultant_activity_funnel_id' => 0,
                'consultant_id' => 0,
                'deleted_at' => Carbon::now()
            ])
        );

        $this->property
            ->popCriteria(app('App\Criteria\Room\ActiveCriteria'))
            ->pushCriteria(new BacklogTaskCriteria(0, 0));
        $response = $this->property->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($expected));
    }
}
