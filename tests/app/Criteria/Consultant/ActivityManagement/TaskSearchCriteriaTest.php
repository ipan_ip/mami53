<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Mockery;
use Prettus\Repository\Eloquent\BaseRepository;

class TaskSearchCriteriaTest extends MamiKosTestCase
{
    
    public function testApplyWithContractShouldFindMatchingName(): void
    {
        $tenant = factory(MamipayTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $expected = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_CONTRACT, 'es');
        $actual = $criteria->apply(MamipayContract::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithContractShouldFindMatchingPhoneNumber(): void
    {
        $tenant = factory(MamipayTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $expected = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_CONTRACT, '11');
        $actual = $criteria->apply(MamipayContract::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithContractShouldNotFindNonMatching(): void
    {
        $tenant = factory(MamipayTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $notExpected = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_CONTRACT, 'toast');
        $actual = $criteria->apply(MamipayContract::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertNull(
            $actual
        );
    }

    public function testApplyWithRoomShouldFindMatchingName(): void
    {
        $expected = factory(Room::class)->create([
            'name' => 'test',
            'owner_phone' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_PROPERTY, 'test');
        $actual = $criteria->apply(Room::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithRoomShouldFindMatchingOwnerPhone(): void
    {
        $expected = factory(Room::class)->create([
            'name' => 'test',
            'owner_phone' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_PROPERTY, '11');
        $actual = $criteria->apply(Room::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithRoomShouldNotFindNonMatching(): void
    {
        $notExpected = factory(Room::class)->create([
            'name' => 'test',
            'owner_phone' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_PROPERTY, 'toast');
        $actual = $criteria->apply(Room::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertNull(
            $actual
        );
    }

    public function testApplyWithPotentialPropertyShouldFindMatchingName(): void
    {
        $owner = factory(PotentialOwner::class)->create(['phone_number' => '0811111111']);
        $expected = factory(PotentialProperty::class)->create([
            'name' => 'test',
            'consultant_potential_owner_id' => $owner->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 'es');
        $actual = $criteria->apply(PotentialProperty::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialPropertyShouldFindMatchingOwnerPhone(): void
    {
        $owner = factory(PotentialOwner::class)->create(['phone_number' => '0811111111']);
        $expected = factory(PotentialProperty::class)->create([
            'name' => 'test',
            'consultant_potential_owner_id' => $owner->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, '11');
        $actual = $criteria->apply(PotentialProperty::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialPropertyShouldNotFindNonMatching(): void
    {
        $owner = factory(PotentialOwner::class)->create(['phone_number' => '0811111111']);
        factory(PotentialProperty::class)->create([
            'name' => 'test',
            'consultant_potential_owner_id' => $owner->id
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 'toast');
        $actual = $criteria->apply(PotentialProperty::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertNull(
            $actual
        );
    }

    public function testApplyWithPotentialTenantShouldFindMatchingName(): void
    {
        $expected = factory(PotentialTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_DBET, 'es');
        $actual = $criteria->apply(PotentialTenant::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialTenantShouldFindMatchingPhone(): void
    {
        $expected = factory(PotentialTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_DBET, '11');
        $actual = $criteria->apply(PotentialTenant::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialTenantShouldNotFindNonMatching(): void
    {
        $notExpected = factory(PotentialTenant::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_DBET, 'toast');
        $actual = $criteria->apply(PotentialTenant::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertNull(
            $actual
        );
    }

    public function testApplyWithPotentialOwnerShouldFindMatchingName(): void
    {
        $expected = factory(PotentialOwner::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, 'es');
        $actual = $criteria->apply(PotentialOwner::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialOwnerShouldFindMatchingPhone(): void
    {
        $expected = factory(PotentialOwner::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, '11');
        $actual = $criteria->apply(PotentialOwner::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithPotentialOwnerShouldNotFindNOnMatching(): void
    {
        factory(PotentialOwner::class)->create([
            'name' => 'test',
            'phone_number' => '0811111111'
        ]);
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, 'toast');
        $actual = $criteria->apply(PotentialOwner::query(), Mockery::mock(BaseRepository::class))->first();
        $this->assertEmpty(
            $actual
        );
    }

    public function testApplyWithEmptyKeywoardShouldNotChangeQuery(): void
    {
        $criteria = new TaskSearchCriteria(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, '');
        $actual = $criteria->apply(PotentialOwner::query(), Mockery::mock(BaseRepository::class));
        
        $this->assertEquals(
            PotentialOwner::query(), $actual
        );
    }
}
