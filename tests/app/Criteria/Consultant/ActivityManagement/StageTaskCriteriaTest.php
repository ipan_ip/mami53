<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Test\MamiKosTestCase;
use Mockery;
use Prettus\Repository\Eloquent\BaseRepository;

class StageTaskCriteriaTest extends MamiKosTestCase
{
    
    protected $repo;
    protected $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->model = MamipayContract::query();
        $this->repo = Mockery::mock(BaseRepository::class);
    }

    public function testApplyShouldFindStageTask(): void
    {
        $expected = factory(MamipayContract::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $criteria = new StageTaskCriteria($funnel->id, $form->id, 1);
        $actual = $criteria->apply(MamipayContract::query(), $this->repo)->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyShouldNotFindOtherStage(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $criteria = new StageTaskCriteria($funnel->id, 0, 1);
        $actual = $criteria->apply(MamipayContract::query(), $this->repo)->first();
        $this->assertNull($actual);
    }

    public function testApplyShouldNotFindOtherFunnel(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $criteria = new StageTaskCriteria(0, $form->id, 1);
        $actual = $criteria->apply(MamipayContract::query(), $this->repo)->first();
        $this->assertNull($actual);
    }

    public function testApplyShouldNotFindOtherConsultant(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $criteria = new StageTaskCriteria($funnel->id, $form->id, 0);
        $actual = $criteria->apply(MamipayContract::query(), $this->repo)->first();
        $this->assertNull($actual);
    }
}
