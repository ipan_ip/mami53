<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Test\MamiKosTestCase;

class AssignedToCriteriaTest extends MamiKosTestCase
{
    
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(ActivityProgressRepository::class);
    }

    public function testApply(): void
    {
        $expected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
        ]);

        $actual = $this
            ->repository
            ->pushCriteria(
                new AssignedToCriteria(1, 1)
            )
            ->first();

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithNullConsultantId(): void
    {
        $expected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
        ]);

        $actual = $this
            ->repository
            ->pushCriteria(
                new AssignedToCriteria(1, null)
            )
            ->first();

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testApplyWithoutConsultantId(): void
    {
        $expected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
        ]);

        $actual = $this
            ->repository
            ->pushCriteria(
                new AssignedToCriteria(1)
            )
            ->first();

        $this->assertTrue(
            $actual->is($expected)
        );
    }
}
