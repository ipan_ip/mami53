<?php

namespace App\Criteria\ApartmentProject;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class PaginationCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApply(): void
    {
        factory(Room::class)->create(['is_active' => true]);
        $expected = factory(Room::class)->create(['is_active' => true, 'created_at' => Carbon::now()->addMinute()]);

        $actual = $this->repository
            ->pushCriteria(
                new PaginationCriteria(1, 1)
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }
}
