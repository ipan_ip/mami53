<?php

namespace App\Criteria\Marketplace;

use App\Test\MamiKosTestCase;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class PaginationCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $query = $this->mock(QueryBuilder::class);

        $query->shouldReceive('take')->with(10)->once();
        $query->shouldReceive('skip')->with(15)->once()->andReturn($query);

        (new PaginationCriteria(10, 15))->apply($query, $this->mock(RepositoryInterface::class));
    }
}
