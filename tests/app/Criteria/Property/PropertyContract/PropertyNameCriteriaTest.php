<?php

namespace App\Criteria\Property\PropertyContract;

use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Repositories\Property\PropertyContractRepository;
use App\Test\MamiKosTestCase;

class PropertyNameCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(PropertyContractRepository::class);
    }

    protected function createData(): array
    {
        $contract = factory(PropertyContract::class)->create();
        $property = factory(Property::class)->create(['name' => 'Test']);
        factory(PropertyContractDetail::class)->create(['property_contract_id' => $contract->id, 'property_id' => $property->id]);
        return ['property' => $property, 'contract' => $contract];
    }

    public function testApply()
    {
        $expected = $this->createData();
        $actual = $this->repository
            ->pushCriteria(new PropertyNameCriteria('Test'))
            ->first();

        $this->assertTrue(
            $actual->is($expected['contract'])
        );
    }

    public function testQuery()
    {
        $expected = $this->createData();
        $actual = (new PropertyNameCriteria('Test'))->query(PropertyContract::query())
            ->first();

        $this->assertTrue(
            $actual->is($expected['contract'])
        );
    }
}
