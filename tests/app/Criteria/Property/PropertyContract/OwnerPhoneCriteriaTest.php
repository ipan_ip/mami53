<?php

namespace App\Criteria\Property\PropertyContract;

use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Repositories\Property\PropertyContractRepository;
use App\Test\MamiKosTestCase;
use App\User;

class OwnerPhoneCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(PropertyContractRepository::class);
    }

    protected function createData(): array
    {
        $contract = factory(PropertyContract::class)->create();
        $owner = factory(User::class)->create(['phone_number' => '08111111111', 'is_owner' => 'true']);
        $property = factory(Property::class)->create(['owner_user_id' => $owner->id]);
        factory(PropertyContractDetail::class)->create(['property_contract_id' => $contract->id, 'property_id' => $property->id]);
        return ['property' => $property, 'contract' => $contract, 'owner' => $owner];
    }

    public function testApply()
    {
        $expected = $this->createData();
        $actual = $this->repository
            ->pushCriteria(new OwnerPhoneCriteria('08111111111'))
            ->first();

        $this->assertTrue(
            $actual->is($expected['contract'])
        );
    }

    public function testQuery()
    {
        $expected = $this->createData();
        $actual = (new OwnerPhoneCriteria('08111111111'))->query(PropertyContract::query())
            ->first();

        $this->assertTrue(
            $actual->is($expected['contract'])
        );
    }
}
