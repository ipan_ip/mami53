<?php

namespace App\Criteria\Property\PropertyContract;

use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Repositories\Property\PropertyContractRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class OwnerNameCriteriaTest extends MamiKosTestCase
{
    public function testApply()
    {
        $criteria = $this->mock(OwnerNameCriteria::class)->makePartial();
        $criteria->shouldReceive('query')->withAnyArgs()->andReturn(collect([]))->once();

        $criteria->apply(PropertyContract::query(), $this->mock(RepositoryInterface::class));
    }

    public function testQuery()
    {
        $query = $this->mock(QueryBuilder::class);
        $query->shouldReceive('whereHas')
            ->withArgs(function ($arg1, $arg2) {
                return $arg1 === 'properties.owner_user';
            })
            ->once();
        $actual = (new OwnerNameCriteria('bill gate'))->query($query);
    }
}
