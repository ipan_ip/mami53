<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class RoomCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $kost = factory(Room::class)->create([
            'area_city' => 'Sleman',
            'apartment_project_id' => null,
            'price_monthly' => 1500000
        ]);

        $criteria = new RandomCriteria(['city' => 'Sleman']);
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();
        $this->assertTrue(
            $actual->is($kost)
        );
    }
}
