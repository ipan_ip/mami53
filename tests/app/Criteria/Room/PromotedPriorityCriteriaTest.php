<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class PromotedPriorityCriteriaTest extends MamiKosTestCase
{
    public function testApplyShouldSet(): void
    {
        $mock = $this->mock(QueryBuilder::class);
        $mock->shouldReceive('orderBy')->with('is_promoted', 'desc')->once();

        $this->mock(PromotedPriorityCriteria::class)->makePartial()->apply($mock, $this->mock(RepositoryInterface::class));
    }

    public function testApplyShouldNotOrderBy(): void
    {
        $mock = $this->mock(QueryBuilder::class);
        $mock->shouldNotReceive('orderBy');

        (new PromotedPriorityCriteria(['direction' => null]))->apply($mock, $this->mock(RepositoryInterface::class));
    }
}
