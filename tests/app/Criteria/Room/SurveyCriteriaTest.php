<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Entities\Room\Survey;
use App\Test\MamiKosTestCase;
use App\User;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class SurveyCriteriaTest extends MamiKosTestCase
{
    protected $user;
    protected $criteria;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->criteria = new SurveyCriteria($this->user);
    }

    public function testApplyWithEmptySurvey(): void
    {
        $mock = $this->mock(QueryBuilder::class);
        $mock->shouldReceive('where')->with('id', '<', 0)->once();

        $this->criteria->apply($mock, $this->mock(RepositoryInterface::class));
    }

    public function testApply(): void
    {
        $kost = factory(Room::class)->create();
        factory(Survey::class)->create([
            'user_id' => $this->user->id,
            'designer_id' => $kost->id
        ]);

        $actual = $this->criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();
        $this->assertTrue(
            $actual->is($kost)
        );
    }
}
