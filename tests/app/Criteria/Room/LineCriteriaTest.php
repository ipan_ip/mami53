<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class LineCriteriaTest extends MamiKosTestCase
{
    protected $expected;
    protected $notExpected;
    protected $query;

    /**
     *  @var array
     */
    protected $filter;

    /**
     *  @var LineCriteria
     */
    protected $criteria;

    protected function setUp(): void
    {
        parent::setUp();

        $this->expected = factory(Room::class)->create([
            'is_active' => 'true',
            'expired_phone' => '0',
            'area_city' => 'Sleman',
            'gender' => 0,
            'price_monthly' => 1000000,
            'apartment_project_id' => null,
            'room_available' => 1,
        ]);

        $this->notExpected = factory(Room::class)->create([
            'is_active' => 'false',
            'expired_phone' => '1',
            'area_city' => 'Bekasi',
            'gender' => 1,
            'price_monthly' => 1500000,
            'apartment_project_id' => 50,
            'room_available' => 0,
        ]);

        $this->filter = [
            'area' => 'Sleman',
            'gender' => [0],
            'price' => 1300000,
            'include_apartment' => false
        ];

        $this->query = Room::query();

        $this->criteria = new LineCriteria($this->filter);
    }

    private function assertOnlyExpected(Collection $actual): void
    {
        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual->first()->is($this->expected)
        );
    }

    public function testFilterActive(): void
    {
        $actual = $this->criteria->filterActive($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testFilterGender(): void
    {
        $actual = $this->criteria->filterGender($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testFilterPrice(): void
    {
        $actual = $this->criteria->filterPrice($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testFilterRoomAvailable(): void
    {
        $actual = $this->criteria->filterRoomAvailable($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testFilterIncludeApartment(): void
    {
        $actual = $this->criteria->filterIncludeApartment($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testSorting(): void
    {
        $mock = $this->mock(QueryBuilder::class);
        $mock->shouldReceive('inRandomOrder')->once();

        $this->criteria->sorting($mock);
    }

    public function testLimit(): void
    {
        $mock = $this->mock(QueryBuilder::class);
        $mock->shouldReceive('take')->with(5)->once();

        $this->criteria->limit($mock);
    }

    public function testDoFilter(): void
    {
        $actual = $this->criteria->doFilter($this->query)->get();

        $this->assertOnlyExpected($actual);
    }

    public function testApply(): void
    {
        $actual = $this->criteria->apply($this->query, $this->mock(RepositoryInterface::class))->get();

        $this->assertOnlyExpected($actual);
    }
}
