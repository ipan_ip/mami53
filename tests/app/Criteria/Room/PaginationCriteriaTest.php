<?php

namespace App\Criteria\Room;

use App\Test\MamiKosTestCase;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class PaginationCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $mock = $this->mock(QueryBuilder::class);

        $criteria = new PaginationCriteria(5, 10);

        $mock->shouldReceive('skip')->with(10)->once()->andReturn($mock);
        $mock->shouldReceive('take')->with(5)->once();

        $criteria->apply($mock, $this->mock(RepositoryInterface::class));
    }
}
