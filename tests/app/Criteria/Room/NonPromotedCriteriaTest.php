<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class NonPromotedCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyWithNonPromotedKost(): void
    {
        $room = factory(Room::class)->create(['is_promoted' => 'false']);

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(NonPromotedCriteria::class)
            ->first();

        $this->assertTrue(
            $result->is($room)
        );
    }

    public function testApplyWithPromotedKost(): void
    {
        factory(Room::class)->create(['is_promoted' => 'true']);

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(NonPromotedCriteria::class)
            ->first();

        $this->assertNull($result);
    }
}
