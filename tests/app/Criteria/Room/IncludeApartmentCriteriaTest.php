<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class IncludeApartmentCriteriaTest extends MamiKosTestCase
{
    public function testApplyWithIncludeApartment(): void
    {
        $apartment = factory(Room::class)->create(['apartment_project_id' => 1000]);
        $criteria = new IncludeApartmentCriteria(true);
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();

        $this->assertTrue(
            $actual->is($apartment)
        );
    }

    public function testApplyWithNotIncludeApartment(): void
    {
        factory(Room::class)->create(['apartment_project_id' => 1000]);
        $criteria = new IncludeApartmentCriteria(false);
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();

        $this->assertNull($actual);
    }
}
