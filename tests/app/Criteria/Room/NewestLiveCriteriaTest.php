<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class NewestLiveCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyWithRecentlyCreatedKost(): void
    {
        $kost = factory(Room::class)->create(['created_at' => Carbon::now()]);

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(NewestLiveCriteria::class)
            ->first();

        $this->assertTrue(
            $result->is($kost)
        );
    }

    public function testApplyWithNonRecentlyCreatedKost(): void
    {
        factory(Room::class)->create(['created_at' => Carbon::now()->subDays(8)]);

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(NewestLiveCriteria::class)
            ->first();

        $this->assertNull($result);
    }
}
