<?php

namespace App\Criteria\Room;

use App\Entities\Activity\Love;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;

class LoveCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $user = factory(User::class)->create();
        $kost = factory(Room::class)->create();
        factory(Love::class)->create(['user_id' => $user->id, 'designer_id' => $kost->id]);

        $actual = (new LoveCriteria($user))->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();

        $this->assertTrue(
            $actual->is($kost)
        );
    }
}
