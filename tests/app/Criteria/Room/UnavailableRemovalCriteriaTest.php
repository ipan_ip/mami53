<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class UnavailableRemovalCriteriaTest extends MamiKosTestCase
{
    public function testApplyWithRoomAvailable(): void
    {
        $kost = factory(Room::class)->create(['room_available' => 1]);
        $criteria = new UnavailableRemovalCriteria();
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();
        $this->assertTrue(
            $actual->is($kost)
        );
    }

    public function testApplyWithoutRoomAvailable(): void
    {
        $kost = factory(Room::class)->create(['room_available' => 0]);
        $criteria = new UnavailableRemovalCriteria();
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();
        $this->assertNull($actual);
    }
}
