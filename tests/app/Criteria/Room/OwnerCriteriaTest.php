<?php

namespace App\Criteria\Room;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomInputProgress;
use App\Repositories\RoomRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OwnerCriteriaTest extends MamiKosTestCase
{
    use WithoutMiddleware;

    protected $user;
    protected $repository;

    const USER_ID = 4582;

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'id' => self::USER_ID
        ]);

        $this->repository = new RoomRepositoryEloquent(app());
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('owner-update'));
    }

    public function testGetRoomWithoutCriteriaReturnCompletedRoomFromUser()
    {
        $room = $this->createRoom();
        $incompleteRoom = $this->createRoom();
        $this->createRoom(123);

        factory(RoomInputProgress::class)->create([ 
            'designer_id' => $incompleteRoom->id
        ]);

        $this->repository->pushCriteria(new OwnerCriteria($this->user));
        $itemListOwner = $this->repository->getItemListOwner();

        $this->AssertEquals(1, $itemListOwner['total']);
        $this->AssertEquals($room->song_id, $itemListOwner['rooms'][0]['_id']);
    }

    public function testGetRoomWithOnboardingTypeOptionAlsoReturnIncompleteRoom()
    {
        $incompleteRoom = $this->createRoom();

        factory(RoomInputProgress::class)->create([ 
            'designer_id' => $incompleteRoom->id
        ]);

        $this->repository->pushCriteria(new OwnerCriteria($this->user, 'kos', [], ['type' => 'on_boarding']));
        $itemListOwner = $this->repository->getItemListOwner();

        $this->AssertEquals(1, $itemListOwner['total']);
        $this->AssertEquals($incompleteRoom->song_id, $itemListOwner['rooms'][0]['_id']);
    }

    public function testGetRoomWithRoomTypeKosOnlyReturnRoomWithTypeKos()
    {
        $room = $this->createRoom();
        $this->createRoom(self::USER_ID, 'Pemilik Apartemen');

        $this->repository->pushCriteria(new OwnerCriteria($this->user, 'kos'));
        $itemListOwner = $this->repository->getItemListOwner();

        $this->AssertEquals(1, $itemListOwner['total']);
        $this->AssertEquals($room->song_id, $itemListOwner['rooms'][0]['_id']);
    }

    public function testGetRoomWithRoomTypeApartmentOnlyReturnRoomWithTypeApartment()
    {
        $this->createRoom();
        $room = $this->createRoom(self::USER_ID, 'Pemilik Apartemen');

        $this->repository->pushCriteria(new OwnerCriteria($this->user, 'apartment'));
        $itemListOwner = $this->repository->getItemListOwner();

        $this->AssertEquals(1, $itemListOwner['total']);
        $this->AssertEquals($room->song_id, $itemListOwner['rooms'][0]['_id']);
    }

    public function testGetRoomWithUnverifiedRoomStatusOnlyReturnUnverifiedRoom()
    {
        $unverifiedRoom = $this->createRoom(self::USER_ID, 'Pemilik Kos', 'unverified');
        $this->createRoom();
        $this->createRoom(self::USER_ID, 'Pemilik Kos', 'claim');
        $this->createRoom(self::USER_ID, 'Pemilik Kos', 'draft');

        $this->repository->pushCriteria(new OwnerCriteria($this->user, 'kos', ['unverified']));
        $itemListOwner = $this->repository->getItemListOwner();

        $this->AssertEquals(1, $itemListOwner['total']);
        $this->AssertEquals($unverifiedRoom->song_id, $itemListOwner['rooms'][0]['_id']);
    }

    /**
     * Create Room and RoomOwner
     *
     * @param int $userId
     * @param string $ownerStatus
     * @param string $status
     *
     * @return App\Entities\Room\Room
     */
    private function createRoom(int $userId = self::USER_ID, string $ownerStatus = 'Pemilik Kos', string $status = 'verified')
    {
        $room = factory(Room::class)->create([
            'is_active' => 'true'
        ]);

        factory(RoomOwner::class)->create([ 
            'designer_id'   => $room->id, 
            'user_id'       => $userId,
            'owner_status'  => $ownerStatus,
            'status'        => $status
        ]);

        return $room;
    }
}
