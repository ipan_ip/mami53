<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class PromotedCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyWithPromotedKost(): void
    {
        $room = factory(Room::class)->create(['is_active' => true, 'is_promoted' => 'true']);

        $result = $this->repository->pushCriteria(PromotedCriteria::class)->first();

        $this->assertTrue(
            $result->is($room)
        );
    }

    public function testApplyWithNonPromotedKost(): void
    {
        $room = factory(Room::class)->create(['is_active' => true, 'is_promoted' => 'false']);

        $result = $this->repository->pushCriteria(PromotedCriteria::class)->first();

        $this->assertNull($result);
    }
}
