<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class TopKostPrioritySortingTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $first = factory(Room::class)->create();
        $second = factory(Room::class)->create();

        $criteria = new TopKostPrioritySorting([$first->id]);
        $actual = $criteria->apply(Room::query(), $this->mock(RepositoryInterface::class))->get();
        

        $this->assertTrue(
            $actual[0]->is($first)
        );

        $this->assertTrue(
            $actual[1]->is($second)
        );
    }
}
