<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class ViewWebCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $expected = factory(Room::class)->create(['song_id' => 123]);
        $notExpected = factory(Room::class)->create(['song_id' => 124]);
        $actual = (new ViewWebCriteria('123'))->apply(Room::query(), $this->mock(RepositoryInterface::class))->get();

        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual->first()->is($expected)
        );
    }
}
