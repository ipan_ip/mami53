<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class ExcludedIdsCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApply(): void
    {
        $room = factory(Room::class)->create();

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(new ExcludedIdsCriteria([$room->id]))
            ->first();

        $this->assertNull($result);
    }
}
