<?php

namespace App\Criteria\Room;

use App\Test\MamiKosTestCase;
use Doctrine\DBAL\Query\QueryBuilder;
use Prettus\Repository\Contracts\RepositoryInterface;

class TopKostPriorityCriteriaTest extends MamiKosTestCase
{
    protected $query;

    protected function setUp(): void
    {
        parent::setUp();

        $this->query = $this->mock(QueryBuilder::class);
    }

    public function testApplyWithNotExclusive(): void
    {
        $criteria = new TopKostPriorityCriteria([], false);

        $this->query->shouldNotReceive('where');

        $criteria->apply($this->query, $this->mock(RepositoryInterface::class));
    }

    public function testApplyWithExclusive(): void
    {
        $criteria = new TopKostPriorityCriteria([], true);

        $this->query->shouldReceive('where')->with('id', '==', 0)->once();

        $criteria->apply($this->query, $this->mock(RepositoryInterface::class));
    }
}
