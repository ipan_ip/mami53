<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class KosOnlyCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApply(): void
    {
        factory(Room::class)->create(['apartment_project_id' => 5]);

        $result = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(new KosOnlyCriteria())
            ->first();

        $this->assertNull($result);
    }
}
