<?php

namespace App\Criteria\Room;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class BookableCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyShouldDoNothing(): void
    {
        $first = factory(Room::class)->create();
        $second = factory(Room::class)->create();

        $results = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(new BookableCriteria(['direction' => 'test']))
            ->get();

        $this->assertTrue(
            $results->first()->is($first)
        );
    }

    public function testApplyShouldSortByIsBooking(): void
    {
        $first = factory(Room::class)->create(['is_booking' => true]);
        $second = factory(Room::class)->create(['is_booking' => false]);

        $results = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(new BookableCriteria(['direction' => '-']))
            ->get();

        $this->assertTrue(
            $results->first()->is($first)
        );
    }

    public function testApplyShouldSortByRoomAvailable(): void
    {
        $first = factory(Room::class)->create(['room_available' => 0]);
        $second = factory(Room::class)->create(['room_available' => 1]);

        $results = $this->repository
            ->popCriteria(ActiveCriteria::class)
            ->pushCriteria(new BookableCriteria(['direction' => '-']))
            ->get();

        $this->assertTrue(
            $results->first()->is($second)
        );
    }
}
