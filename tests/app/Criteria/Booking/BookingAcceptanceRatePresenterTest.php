<?php

namespace App\Criteria\Room;

use App\Test\MamiKosTestCase;
use App\Presenters\Booking\BookingAcceptanceRatePresenter;
use App\Transformers\Booking\BookingAcceptanceRateAdminTransformer;

class BookingAcceptanceRatePresenterTest extends MamiKosTestCase
{
    public function testGetTransformer()
    {
        $presenter = new BookingAcceptanceRatePresenter('admin');
        $expected = BookingAcceptanceRateAdminTransformer::class;
        $actual = $presenter->getTransformer();

        $this->assertInstanceOf($expected, $actual);
    }

    public function testGetTransformerWithApiSingle(): void
    {
        $presenter = new BookingAcceptanceRatePresenter('api-single');

        $this->assertNull($presenter->getTransformer());
    }

    public function testGetTransformerWithNoArgument(): void
    {
        $expected = BookingAcceptanceRateAdminTransformer::class;

        $presenter = new BookingAcceptanceRatePresenter();

        $this->assertInstanceOf($expected, $presenter->getTransformer());
    }
}
