<?php

namespace App\Criteria\Apartment;

use App\Criteria\Room\ActiveCriteria;
use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class ApartmentUnitOnlyCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyShouldGetApartment(): void
    {
        $expected = factory(Room::class)->create(['apartment_project_id' => 1, 'is_active' => true]);

        $actual = $this->repository
            ->pushCriteria(
                new ApartmentUnitOnlyCriteria()
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }

    public function testApplyShouldNotGetKost(): void
    {
        $expected = factory(Room::class)->create(['apartment_project_id' => 1, 'is_active' => true]);

        $actual = $this->repository
            ->pushCriteria(
                new ApartmentUnitOnlyCriteria()
            )
            ->first();

        $this->assertTrue($actual->is($expected));
    }
}
