<?php

namespace App\Criteria\Apartment;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;

class UnitInProjectCriteriaTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(RoomRepository::class);
    }

    public function testApplyShouldFindApartment(): void
    {
        $expected = factory(Room::class)->create(['apartment_project_id' => 1, 'is_active' => true]);

        $actual = $this->repository
            ->pushCriteria(
                new UnitInProjectCriteria(1)
            )
            ->first();

        $this->assertTrue($expected->is($actual));
    }

    public function testApplyShouldNotFindDifferentId(): void
    {
        $expected = factory(Room::class)->create(['apartment_project_id' => 1, 'is_active' => true]);

        $actual = $this->repository
            ->pushCriteria(
                new UnitInProjectCriteria(2)
            )
            ->first();

        $this->assertFalse($expected->is($actual));
    }
}
