<?php

namespace App\Criteria\Apartment;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Prettus\Repository\Contracts\RepositoryInterface;

class JustApartmentCriteriaTest extends MamiKosTestCase
{
    public function testApply(): void
    {
        $expected = factory(Room::class)->create([
            'apartment_project_id' => 5,
            'area_city' => 'Sleman'
        ]);

        $actual = (new JustApartmenCriteria(['Sleman']))->apply(Room::query(), $this->mock(RepositoryInterface::class))->first();

        $this->assertTrue(
            $actual->is($expected)
        );
    }
}
