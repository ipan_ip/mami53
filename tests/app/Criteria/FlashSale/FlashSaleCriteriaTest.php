<?php

namespace App\Criteria\Room;

use App\Test\MamiKosTestCase;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;
use App\Criteria\FlashSale\ActiveCriteria;
use App\Entities\FlashSale\FlashSale;
use Carbon\Carbon;

class FlashSaleCriteriaTest extends MamiKosTestCase
{
    
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = new FlashSaleRepositoryEloquent(app());
    }

    public function testApplyCriteriaShouldReturnData()
    {
        $flashSaleName  = 'Flash Sale Mamikos'; 
        $flashSale      = factory(FlashSale::class)->create([
            'name'       => $flashSaleName,
            'start_time' => '2018-06-01 19:00:00',
            'end_time'   => '2020-06-01 19:00:00',
            'is_active'  => 1,
        ]);

        // mock time now()
        $mockDate = Carbon::create(2019, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $this->repository->pushCriteria(new ActiveCriteria());
        $response = $this->repository->findByField('name', $flashSaleName);

        $this->assertEquals(1, count($response));
        $this->assertEquals($flashSale->id, $response[0]->id);
    }

    public function testApplyCriteriaShouldReturnNullDueToInactivity()
    {
        $flashSaleName  = 'Flash Sale Mamikos Old'; 
        $flashSale      = factory(FlashSale::class)->create([
            'name'       => $flashSaleName,
            'start_time' => '2018-06-01 19:00:00',
            'end_time'   => '2019-06-01 19:00:00',
            'is_active'  => 0,
        ]);

        // mock time now()
        $mockDate = Carbon::create(2020, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $this->repository->pushCriteria(new ActiveCriteria());
        $response = $this->repository->findByField('name', $flashSaleName);

        $this->assertEquals(0, $response->count());
    }

    public function testApplyCriteriaShouldReturnNullDueToNotActive()
    {
        $flashSaleName  = 'Flash Sale Mamikos Team'; 
        $flashSale      = factory(FlashSale::class)->create([
            'name'       => $flashSaleName,
            'start_time' => '2018-06-01 19:00:00',
            'end_time'   => '2021-06-01 19:00:00',
            'is_active'  => 0,
        ]);

        // mock time now()
        $mockDate = Carbon::create(2020, 5, 21, 12);
        Carbon::setTestNow($mockDate);

        $this->repository->pushCriteria(new ActiveCriteria());
        $response = $this->repository->findByField('name', $flashSaleName);

        $this->assertEquals(0, $response->count());
    }
}
