<?php

namespace App\Criteria\Review;

use App\Entities\Room\Review;
use App\Entities\Room\Room;
use App\Repositories\ReviewRepository;
use App\Test\MamiKosTestCase;

class ReviewCriteriaTest extends MamiKosTestCase
{
    protected $repository;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(ReviewRepository::class);
        $this->user = factory(\App\User::class)->create();
    }

    public function testApplyShouldGetReview(): void
    {
        $room = factory(Room::class)->create();

        factory(Review::class)->create(
            [
                'designer_id' => $room->id,
                'status'      => 'live'
            ]
        );

        $response = $this->repository
            ->pushCriteria(new ReviewCriteria(
                $room->id,
                'new',
                null,
                10))
            ->first();

        $this->assertNotNull($response);
    }

    public function testApplyShouldNotGetReview(): void
    {
        $room = factory(Room::class)->create();

        $response = $this->repository
            ->pushCriteria(new ReviewCriteria(
                $room->id,
                'new',
                null,
                10))
            ->first();

        $this->assertNull($response);
    }
}
