<?php


namespace app\Services\Level;

use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Repositories\Level\PropertyLevelRepository;
use App\Services\Level\PropertyLevelService;
use App\Test\MamiKosTestCase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Entities\GoldPlus\Package;

class PropertyLevelServiceTest extends MamiKosTestCase
{
    private $service;
    private $propertyLevelRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->propertyLevelRepository = $this->mockAlternatively(PropertyLevelRepository::class);
        $this->service = app()->make(PropertyLevelService::class);
    }

    public function testGetListPaginateAndSuccess()
    {
        $this->propertyLevelRepository->shouldReceive('getListPaginate')
            ->andReturn(new LengthAwarePaginator(
                [factory(PropertyLevel::class)->make()],
                1,
                20
            ));
        $result = $this->service->getListPaginate([]);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testCreatePropertyLevelShouldCallRepositoryMethod(): void
    {
        $level = factory(KostLevel::class)->create();
        $params = [
            'name' => 'test',
            'max_rooms' => 5,
            'minimum_charging' => 50000,
            'kost_level_id' => $level->id
        ];

        $this->propertyLevelRepository
            ->shouldReceive('createPropertyLevel')
            ->with($params)
            ->once()
            ->andReturn(
                factory(PropertyLevel::class)->create()
            );

        $this->service->createPropertyLevel($params);
    }

    public function testCreatePropertyLevelAndSuccess()
    {
        $level = factory(KostLevel::class)->create();
        $params = [
            'name' => 'test',
            'max_rooms' => 5,
            'minimum_charging' => 50000,
            'kost_level_id' => $level->id
        ];

        $propertyLevel = factory(PropertyLevel::class)->create();
        $this->propertyLevelRepository
            ->shouldReceive('createPropertyLevel')
            ->andReturn($propertyLevel);

        $result = $this->service->createPropertyLevel($params);
        $this->assertTrue($result->is($propertyLevel));
    }

    public function testCreatePropertyLevelShouldUpdateForeignKeyOnKostLevel(): void
    {
        $level = factory(KostLevel::class)->create(['property_level_id' => null]);
        $params = [
            'name' => 'test',
            'max_rooms' => 5,
            'minimum_charging' => 50000,
            'kost_level_id' => $level->id
        ];

        $propertyLevel = factory(PropertyLevel::class)->create();
        $this->propertyLevelRepository
            ->shouldReceive('createPropertyLevel')
            ->andReturn($propertyLevel);

        $this->service->createPropertyLevel($params);

        $this->assertDatabaseHas(
            'kost_level',
            [
                'id' => $level->id,
                'property_level_id' => $propertyLevel->id
            ]
        );
    }

    public function testUpdatePropertyLevelAndSuccess()
    {
        $level = factory(KostLevel::class)->create(['property_level_id' => null]);
        $this->propertyLevelRepository->shouldReceive('updatePropertyLevel')
            ->andReturn(true);
        $result = $this->service->updatePropertyLevel(
            factory(PropertyLevel::class)->make(),
            ['kost_level_id' => $level->id]
        );
        $this->assertTrue($result);
    }

    public function testUpdatePropertyLevelShouldUpdateForeignKeyOnKostLevel(): void
    {
        $level = factory(KostLevel::class)->create(['property_level_id' => null]);
        $propertyLevel = factory(PropertyLevel::class)->create();

        $this->propertyLevelRepository->shouldReceive('updatePropertyLevel')
            ->andReturn(true);
        $result = $this->service->updatePropertyLevel(
            $propertyLevel,
            ['kost_level_id' => $level->id]
        );
        $this->assertTrue($result);

        $this->assertDatabaseHas(
            'kost_level',
            [
                'id' => $level->id,
                'property_level_id' => $propertyLevel->id
            ]
        );
    }

    public function testUpdatePropertyLevelShouldUpdateOldLevelForeignKey(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $oldLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $newLevel = factory(KostLevel::class)->create(['property_level_id' => null]);

        $this->propertyLevelRepository->shouldReceive('updatePropertyLevel')
            ->andReturn(true);
        $result = $this->service->updatePropertyLevel(
            $propertyLevel,
            ['kost_level_id' => $newLevel->id]
        );
        $this->assertTrue($result);

        $this->assertDatabaseHas(
            'kost_level',
            [
                'id' => $oldLevel->id,
                'property_level_id' => null
            ]
        );

        $this->assertDatabaseHas(
            'kost_level',
            [
                'id' => $newLevel->id,
                'property_level_id' => $propertyLevel->id
            ]
        );
    }

    public function testDestroyAndSuccess()
    {
        $propertyLevel = factory(PropertyLevel::class)->make();
        $this->propertyLevelRepository->shouldReceive('destroy')->andReturn(true);
        $result = $this->service->destroy($propertyLevel);
        $this->assertTrue($result);
    }

    public function testGetLevelDropdownAndSuccess()
    {
        $this->propertyLevelRepository->shouldReceive('getLevelDropdown')
            ->andReturn(collect([]));
        $result = $this->service->getLevelDropdown();
        $this->assertInstanceOf(Collection::class, $result);
    }

    /**
     * @group UG
     * @group UG-4704
     * @group app/Services/Level
     */
    public function testUpdatePropertyLevelIdShouldReturnOk(): void
    {
        $package = factory(Package::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create();

        $expectedResult =  1;
        $this->propertyLevelRepository->shouldReceive('updatePropertyLevelId')
            ->andReturn($expectedResult);
        
        $result = $this->service->updatePropertyLevelId($propertyLevel, $package->id);
        
        $this->assertEquals($result, $expectedResult);
    }

    /**
     * @group UG
     * @group UG-4704
     * @group app/Services/Level
     */
    public function testUpdatePropertyLevelIdShouldExpectRuntimeException(): void
    {
        $package = factory(Package::class)->create(['id' => 0]);
        $propertyLevel = factory(PropertyLevel::class)->create();

        $expectedResult =  1;
        $this->propertyLevelRepository->shouldReceive('updatePropertyLevelId')
            ->andReturn($expectedResult);
        
        $this->expectException(\RuntimeException::class);
        $result = $this->service->updatePropertyLevelId($propertyLevel, null);
    }
}
