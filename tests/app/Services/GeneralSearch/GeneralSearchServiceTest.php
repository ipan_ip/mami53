<?php

namespace App\Services\GeneralSearch;

use App\Entities\Landing\Landing;
use App\Entities\Search\GeneralSearch;
use App\Test\MamiKosTestCase;

class GeneralSearchServiceTest extends MamiKosTestCase
{
    protected $service;

    private const STRING_FOR_LANDING_SLUG = 'kost-malang-murah';
    private const INCORRECT_STRING_FOR_LANDING_SLUG = 'kost-walang-murah';
    private const STRING_FOR_SEARCH_KEYWORD = 'malang';
    private const INCORRECT_STRING_FOR_SEARCH_KEYWORD = 'walang';
    private const STRING_FOR_SEARCH_SLUG = 'malang-kota-malang-jawa-timur-indonesia';
    private const INCORRECT_STRING_FOR_SEARCH_SLUG = 'walang-kota-malang-jawa-timur-indonesia';

    protected function setUp(): void
    {
        parent::setUp();

        factory(GeneralSearch::class)->create([
            'slug' => 'malang/all/bulanan/0-10000000',
            'keywords' => 'malang',
            'gender' => null,
            'rent_type' => 2,
            'price_min' => 0,
            'price_max' => 10000000,
            'longitude' => 112.627,
            'latitude' => -7.98027
        ]);

        factory(GeneralSearch::class)->create([
            'slug' => 'malang-kota-malang-jawa-timur-indonesia/all/bulanan/0-10000000',
            'keywords' => 'Malang, Kota Malang, Jawa Timur, Indonesia',
            'gender' => null,
            'rent_type' => 2,
            'price_min' => 0,
            'price_max' => 10000000,
            'longitude' => 112.633,
            'latitude' => -7.96662
        ]);

        factory(Landing::class)->create([
            'slug' => 'kost-malang-murah',
            'type' => 'area',
            'parent_id' => null,
            'latitude_1' => -8.016620399999999,
            'longitude_1' => 112.58263210000003,
            'latitude_2' => -7.9166203999999984,
            'longitude_2' => 112.68263210000002,
            'price_min' => 0,
            'price_max' => 10000000,
            'gender' => null,
            'rent_type' => 2,
            'place' => null
        ]);

        $this->service = $this->app->make(GeneralSearchService::class);
    }

    public function testGetGeoLocationGetLanding()
    {
        $response = $this->service->getGeoLocation(self::STRING_FOR_LANDING_SLUG);
        $landing = Landing::where('slug', self::STRING_FOR_LANDING_SLUG)->first();

        $this->assertEquals($landing->location, $response['location']);
        $this->assertEquals($landing->filters, $response['filters']);
    }

    public function testGetGeoLocationUsingKeywordGetGeneralSearch()
    {
        $response = $this->service->getGeoLocation(self::STRING_FOR_SEARCH_KEYWORD);
        $search = GeneralSearch::where('keywords', self::STRING_FOR_SEARCH_KEYWORD)
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->where('price_max', '!=', 0)
            ->orderBy('total_view', 'desc')
            ->first();

        $this->assertEquals($search->filters, $response['filters']);
    }

    public function testGetGeoLocationUsingSlugGetGeneralSearch()
    {
        $response = $this->service->getGeoLocation(self::STRING_FOR_SEARCH_SLUG);
        $search = GeneralSearch::where('slug', 'like', self::STRING_FOR_SEARCH_SLUG.'%')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->where('price_max', '!=', 0)
            ->orderBy('total_view', 'desc')
            ->first();

        $this->assertEquals($search->filters, $response['filters']);
    }

    public function testGetGeoLocationWithIncorrectInputReturnNull()
    {
        $response = $this->service->getGeoLocation(self::INCORRECT_STRING_FOR_SEARCH_KEYWORD);

        $this->assertNull($response);
    }

    public function testGetLandingWithSpecificSlug()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getLandingWithSpecificSlug');

        $response = $method->invokeArgs(
            $this->service,
            [self::STRING_FOR_LANDING_SLUG]
        );

        $this->assertNotNull($response);
    }

    public function testGetLandingWithSpecificSlugReturnNull()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getLandingWithSpecificSlug');

        $response = $method->invokeArgs(
            $this->service,
            [self::INCORRECT_STRING_FOR_LANDING_SLUG]
        );

        $this->assertNull($response);
    }

    public function testGetGeneralSearchWithKeywords()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getGeneralSearchWithKeywords');

        $response = $method->invokeArgs(
            $this->service,
            [self::STRING_FOR_SEARCH_KEYWORD]
        );

        $this->assertNotNull($response);
    }

    public function testGetGeneralSearchWithKeywordsReturnNull()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getGeneralSearchWithKeywords');

        $response = $method->invokeArgs(
            $this->service,
            [self::INCORRECT_STRING_FOR_SEARCH_KEYWORD]
        );

        $this->assertNull($response);
    }

    public function testGetGeneralSearchWithSlug()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getGeneralSearchWithSlug');

        $response = $method->invokeArgs(
            $this->service,
            [self::STRING_FOR_SEARCH_SLUG]
        );

        $this->assertNotNull($response);
    }

    public function testGetGeneralSearchWithSlugReturnNull()
    {
        $method = $this->getNonPublicMethodFromClass(GeneralSearchService::class, 'getGeneralSearchWithSlug');

        $response = $method->invokeArgs(
            $this->service,
            [self::INCORRECT_STRING_FOR_SEARCH_SLUG]
        );

        $this->assertNull($response);
    }

}
