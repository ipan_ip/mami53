<?php

namespace app\Services\RoomUnit;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Services\RoomUnit\RoomUnitService;
use App\Entities\Room\Room;
use App\Exceptions\RoomAllotmentException;
use App\Repositories\Room\RoomUnitRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Mockery;

class RoomUnitServiceTest extends MamiKosTestCase
{
    
    private $service;

    public function setUp() : void
    {
        parent::setUp();
        $this->service = App::make(RoomUnitService::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testAddSingleRoomUnit()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 0]);
        factory(RoomUnit::class, 2)->create(['occupied' => true, 'designer_id' => $room->id]);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);

        $data = [
            'song_id' => $room->song_id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => false
        ];

        $this->assertEquals(2, $room->room_count);
        $this->assertEquals(0, $room->room_available);
        $unit = $this->service->addRoomUnit($data);
        $room->refresh();
        $this->assertEquals(3, $room->room_count);
        $this->assertEquals(1, $room->room_available);
        $this->assertEquals($mockTime, $room->kost_updated_date);

        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
    }

    public function testAddSingleRoomUnitAndRoomNotFound()
    {
        $this->expectException(ModelNotFoundException::class);
        $data = [
            'song_id' => -1,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];
        $this->assertNull($this->service->addRoomUnit($data));
    }

    public function testAddSingleRoomUnitNamaIsUsed()
    {
        $this->expectException(RoomAllotmentException::class);
        $unitName = 'nama unit';
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 1]);
        factory(RoomUnit::class)->create(['designer_id' => $room->id, 'name' => $unitName, 'occupied' => false]);
        $data = [
            'song_id' => $room->song_id,
            'name' => $unitName,
            'floor' => '1',
            'occupied' => false
        ];

        $this->assertNull($this->service->addRoomUnit($data));
    }

    public function testAddSingleRoomUnitMaxUnitCountThrowException()
    {
        $this->expectException(RoomAllotmentException::class);
        $room = factory(Room::class)->create(['room_count' => 500, 'room_available' => 0]);

        $data = [
            'song_id' => $room->song_id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => false
        ];

        $this->assertNull($this->service->addRoomUnit($data));
    }

    public function testUpdateSingleRoomUnitToOccupied()
    {
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 1]);
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id, 'occupied' => false]);
        $data = [
            'id' => $roomUnit->id,
            'song_id' => $room->song_id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);

        $this->assertEquals(1, $room->room_available, 'before updated room available is 1');
        $unit = $this->service->updateRoomUnit($roomUnit->id, $data);
        $room->refresh();
        $this->assertEquals(0, $room->room_available, 'after updated room available is 0');
        $this->assertEquals($mockTime, $room->kost_updated_date);

        $this->assertEquals($unit->id, $data['id']);
        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
    }

    public function testUpdateSingleRoomUnitToAvailable()
    {
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 0]);
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id, 'occupied' => true]);
        $data = [
            'id' => $roomUnit->id,
            'song_id' => $room->song_id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => false
        ];
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);

        $this->assertEquals(0, $room->room_available, 'before updated room available is 0');
        $unit = $this->service->updateRoomUnit($roomUnit->id, $data);
        $room->refresh();
        $this->assertEquals(1, $room->room_available, 'after updated room available is 1');
        $this->assertEquals($mockTime, $room->kost_updated_date);

        $this->assertEquals($unit->id, $data['id']);
        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
    }

    public function testUpdateSingleRoomUnitAndRoomNotFound()
    {
        $this->expectException(ModelNotFoundException::class);
        $data = [
            'song_id' => 0,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];
        $this->service->updateRoomUnit(0, $data);
    }

    public function testUpdateSingleRoomUnitAndUnitNotFound()
    {
        $this->expectException(ModelNotFoundException::class);
        $room = factory(Room::class)->create();
        $data = [
            'id' => 888,
            'song_id' => $room->song_id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];

        $this->service->updateRoomUnit(888, $data);
    }

    public function testDeleteSingleRoomUnit()
    {
        $room = factory(Room::class)->create(['room_count' => 3, 'room_available' => 3]);
        $roomUnits = factory(RoomUnit::class, 3)->create(['designer_id' => $room->id, 'occupied' => false]);
        $roomUnit = $roomUnits->first();

        $this->service->deleteRoomUnit($roomUnit->id);

        $this->assertDatabaseMissing('designer_room', $roomUnit->toArray());
    }

    public function testDeleteSingleRoomUnitValidateContractOnOccupied()
    {
        $room = factory(Room::class)->create(['room_count' => 3, 'room_available' => 2]);
        $roomUnits = factory(RoomUnit::class, 3)->create(['designer_id' => $room->id, 'occupied' => false]);
        $roomUnit = $roomUnits->first();
        $roomUnit->occupied = true;
        $roomUnit->save();

        $this->service->deleteRoomUnit($roomUnit->id);

        $this->assertDatabaseMissing('designer_room', $roomUnit->toArray());
    }

    public function testDeleteSingleRoomUnitAndRoomUnitNotFound()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service->deleteRoomUnit(0);
    }

    public function testUpdateBulkNotFoundThrowException()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service->updateBulk(0, [], [], []);
    }

    public function testUpdateBulkSuccess()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $units = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $toUpdateUnit = $units->shift();
        $toDeleteUnit = $units->shift();

        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        $this->service->updateBulk(
            $room->song_id,
            [
                [
                    'name' => 'kamar A',
                    'floor' => 1,
                    'occupied' => false
                ],
                [
                    'name' => 'kamar B',
                    'floor' => 1,
                    'occupied' => true
                ]
            ],
            [
                [
                    'id' => $toUpdateUnit->id,
                    'name' => 'kamar C',
                    'floor' => 1,
                    'occupied' => true
                ],
            ],
            [
                $toDeleteUnit->id,
            ]
        );

        $room->refresh();

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertEquals($roomCount + 1, $room->room_count);
        $this->assertEquals($roomCount - 1, $room->room_available);
        $this->assertEquals($roomCount + 1, $room->room_unit->count());

        $this->assertEquals(
            [
                "kamar C",
                "kamar B",
                "kamar A",
                "5",
                "4",
                "3"
            ],
            $room->room_unit->pluck('name')->toArray()
        );
    }

    public function testUpdateBulkOneFailed()
    {
        $roomCount = 7;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $units = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $toUpdateUnit = $units->shift();
        $toUpdateUnit2 = $units->shift();
        $toDeleteUnit = $units->shift();
        $existUnit = $units->shift();

        $result = $this->service->updateBulk(
            $room->song_id,
            [
                [
                    'name' => 'kamar A',
                    'floor' => 1,
                    'occupied' => false
                ],
                [
                    'name' => 'kamar B',
                    'floor' => 1,
                    'occupied' => true
                ]
            ],
            [
                [
                    'id' => $toUpdateUnit->id,
                    'name' => 'kamar C',
                    'floor' => 1,
                    'occupied' => true
                ],
                [
                    'id' => $toUpdateUnit2->id,
                    'name' => $existUnit->name,
                    'floor' => 1,
                    'occupied' => true
                ]
            ],
            [
                $toDeleteUnit->id,
            ]
        );

        $room->refresh();

        $this->assertEquals($roomCount + 1, $room->room_count);
        $this->assertEquals($roomCount - 1, $room->room_available);
        $this->assertEquals($roomCount + 1, $room->room_unit->count());
        $this->assertNotEmpty($result['failed']);
    }

    public function testUpdateBulkThrowException()
    {
        $roomCount = 7;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $units = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $toUpdateUnit = $units->shift();
        $toUpdateUnit2 = $units->shift();
        $toDeleteUnit = $units->shift();
        $existUnit = $units->shift();

        $failedUpdate = [
            'id' => $toUpdateUnit2->id,
            'name' => $existUnit->name,
            'floor' => 1,
            'occupied' => true
        ];
        DB::shouldReceive('beginTransaction')->once()->andThrow(RoomAllotmentException::class);

        $this->expectException(RoomAllotmentException::class);
        $result = $this->service->updateBulk(
            $room->song_id,
            [
                [
                    'name' => 'kamar A',
                    'floor' => 1,
                    'occupied' => false
                ],
                [
                    'name' => 'kamar B',
                    'floor' => 1,
                    'occupied' => true
                ]
            ],
            [
                [
                    'id' => $toUpdateUnit->id,
                    'name' => 'kamar C',
                    'floor' => 1,
                    'occupied' => true
                ],
                $failedUpdate
            ],
            [
                $toDeleteUnit->id,
            ]
        );

        $room->refresh();

        $this->assertEquals($roomCount + 1, $room->room_count);
        $this->assertEquals($roomCount - 1, $room->room_available);
        $this->assertEquals($roomCount + 1, $room->room_unit->count());
    }

    public function testProcessAddSuccess()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $toAdd = collect([
            [
                'name' => 'kamar A',
                'floor' => 1,
                'occupied' => false
            ],
            [
                'name' => 'kamar B',
                'floor' => 1,
                'occupied' => true
            ],
        ]);


        $toAddCount = $toAdd->count();
        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processAdd');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        [$successList, $failedList] = $method->invokeArgs(
            $service,
            [
                $room,
                $toAdd->toArray(),
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('addRoomUnit')->times($toAddCount);

        $room->refresh();
        $pluckedExpectedArray = $room->room_unit->pluck('id')->toArray();

        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $pluckedExpectedArray));
        }

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNotEquals($roomCount, $room->room_count);
        $this->assertEquals($roomCount + $toAddCount, $room->room_count);
        $this->assertEquals($roomCount + 1, $room->room_available);
    }

    public function testProcessAddNameUsedFailed()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $failedItem = [
            'name' => 1,
            'floor' => 1,
            'occupied' => true
        ];
        $failedItem2 = [
            'name' => 3,
            'floor' => 1,
            'occupied' => false
        ];

        $successItem = [
            'name' => 'kamar A',
            'floor' => 1,
            'occupied' => false
        ];

        $toAdd = collect([
            $successItem,
            $failedItem,
            $failedItem2
        ]);


        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processAdd');
        $method->setAccessible(true);

        [$successList, $failedList] = $method->invokeArgs(
            $service,
            [
                $room,
                $toAdd->toArray(),
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('addRoomUnit')->atLeast()->once();
        $room->refresh();

        $this->assertNotEquals($roomCount, $room->room_count);
        $this->assertEquals($roomCount + 1, $room->room_count);
        $this->assertEquals($roomCount + 1, $room->room_available);
        $this->assertEquals($failedItem, array_shift($failedList));
    }

    public function testProcessUpdateUnitNotFoundException()
    {
        $roomCount = 3;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processUpdate');
        $method->setAccessible(true);
        try {
            $method->invokeArgs(
                $service,
                [
                    $room,
                    [
                        [
                            'id' => 888,
                            'name' => 'modified'
                        ]
                    ],
                    [],
                    []
                ]
            );
        } catch (RoomAllotmentException $exception) {
            $this->assertSame(
                "Room 888 not found",
                $exception->getMessage()
            );
        }

        $room->refresh();
        $this->assertEquals($roomCount, $room->room_available);
    }

    public function testProcessUpdateSuccess()
    {
        $roomCount = 8;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $unitToBeUpdated = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false])->nth(2, 1);
        $toBeUpdated = $unitToBeUpdated->map(function ($item) {
            return [
                'id' => $item->id,
                'occupied' => true,
                'name' => $item->name . ' modified',
                'floor' => 1
            ];
        });

        $toBeUpdatedCount = $unitToBeUpdated->count();
        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processUpdate');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        [$successList, $failedList] = $method->invokeArgs(
            $service,
            [
                $room,
                $toBeUpdated->toArray(),
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('updateRoomUnit')->times($toBeUpdatedCount);

        $pluckedExpectedArray = $toBeUpdated->pluck('id')->toArray();
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $pluckedExpectedArray));
        }
        $room->refresh();
        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNotEquals($roomCount, $room->room_available);
        $this->assertEquals($roomCount - $toBeUpdatedCount, $room->room_available);
    }

    public function testProcessUpdateWithContractSuccess()
    {
        $roomCount = 8;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount - 2 ]);
        $units = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $withContractUnit = $units->nth(2, 0)->take(2)->each(function ($unit) use ($room) {
            $unit->occupied = true;
            $unit->save();

            $contract = factory(MamipayContract::class)->create([
                'type' => 'kost',
                'status' => MamipayContract::STATUS_BOOKED,
                'end_date' => Carbon::now()->addMonth()
            ]);

            factory(MamipayContractKost::class)->create([
                'contract_id' => $contract->id,
                'designer_id' => $room->id,
                'designer_room_id' => $unit->id
            ]);
        });

        $unitToBeUpdated = $units->nth(2, 1)->prepend($withContractUnit->first());
        $toBeUpdated = $unitToBeUpdated->map(function ($item) {
            return [
                'id' => $item->id,
                'occupied' => true,
                'name' => $item->name . ' modified',
                'floor' => 1
            ];
        });

        $toBeUpdatedCount = $unitToBeUpdated->count();
        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processUpdate');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        [$successList, $failedList] = $method->invokeArgs(
            $service,
            [
                $room,
                $toBeUpdated->toArray(),
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('updateRoomUnit')->times($toBeUpdatedCount);

        $pluckedExpectedArray = $toBeUpdated->pluck('id')->toArray();
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $pluckedExpectedArray));
        }
        $room->refresh();
        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNotEquals($roomCount, $room->room_available);
        $this->assertEquals(2, $room->room_available);
    }

    public function testProcessUpdateWithContractFailed()
    {
        $roomCount = 8;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount - 2 ]);
        $units = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false]);
        $withContractUnit = $units->nth(2, 0)->take(2)->each(function ($unit) use ($room) {
            $unit->occupied = true;
            $unit->save();

            $contract = factory(MamipayContract::class)->create([
                'type' => 'kost',
                'status' => MamipayContract::STATUS_BOOKED,
                'end_date' => Carbon::now()->addMonth()
            ]);

            factory(MamipayContractKost::class)->create([
                'contract_id' => $contract->id,
                'designer_id' => $room->id,
                'designer_room_id' => $unit->id
            ]);
        });

        $unitToBeUpdated = $units->nth(2, 1);
        $firstWithContract = $withContractUnit->first();
        $toBeUpdated = $unitToBeUpdated->map(function ($item) {
            return [
                'id' => $item->id,
                'occupied' => true,
                'name' => $item->name . ' modified',
                'floor' => 1
            ];
        })->prepend([
            'id' => $firstWithContract->id,
            'occupied' => false,
            'name' => $firstWithContract->name . ' modified',
            'floor' => 1
        ]);

        $toBeUpdatedCount = $unitToBeUpdated->count();
        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processUpdate');
        $method->setAccessible(true);
        $this->expectException(RoomAllotmentException::class);
        [$successList, $failedList] = $method->invokeArgs(
            $service,
            [
                $room,
                $toBeUpdated->toArray(),
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('updateRoomUnit')->atLeast()->times($toBeUpdatedCount - 1);

        $pluckedExpectedArray = $toBeUpdated->pluck('id')->toArray();
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $pluckedExpectedArray));
        }
        $room->refresh();
        $this->assertNotEquals($roomCount, $room->room_available);
        $this->assertEquals(2, $room->room_available);
    }

    public function testProcessUpdateNonUniqueNameThrowException()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $unitToBeUpdated = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false])->nth(2, 1);
        $toBeUpdated = $unitToBeUpdated->map(function ($item) {
            return [
                'id' => $item->id,
                'occupied' => true,
                'name' => 1,
                'floor' => 1
            ];
        });
        $toBeUpdatedCount = $unitToBeUpdated->count();
        $room->load('room_unit');

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processUpdate');
        $method->setAccessible(true);

        try {
            $method->invokeArgs(
                $service,
                [
                    $room,
                    $toBeUpdated->toArray(),
                    [],
                    []
                ]
            );
        } catch (RoomAllotmentException $exception) {
            $this->assertSame(
                "Nama sudah pernah digunakan",
                $exception->getMessage()
            );
        }

        $room->refresh();
        $this->assertEquals($roomCount, $room->room_available);
    }

    public function testProcessDeleteSuccess()
    {
        $roomCount = 8;
        $roomAvailable = $roomCount - 1;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomAvailable]);
        $unitToBeDeleted = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false])->nth(2, 1);
        $occupiedUnit = $unitToBeDeleted->first();
        $occupiedUnit->occupied = true;
        $occupiedUnit->save();

        $tobeDeletedId = $unitToBeDeleted->pluck('id')->toArray();
        $tobeDeletedCount = $unitToBeDeleted->count();

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processDelete');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        [$successList] = $method->invokeArgs(
            $service,
            [
                $room,
                $tobeDeletedId,
                [],
                []
            ]
        );

        $spyRepo->shouldHaveReceived('deleteRoomUnit')->times($tobeDeletedCount);
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $tobeDeletedId));
        }
        $room->refresh();
        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNotEquals($roomCount, $room->room_count);
        $this->assertEquals($roomCount - $tobeDeletedCount, $room->room_count);
    }

    public function testProcessDeleteFailedOnLast()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $unitToBeDeleted = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false])->nth(2, 1);
        $tobeDeletedId = array_merge($unitToBeDeleted->pluck('id')->toArray(), [888]);
        $tobeDeletedCount = count($tobeDeletedId);

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processDelete');
        $method->setAccessible(true);
        $successList = $failedList = [];
        try {
            [$successList, $failedList] = $method->invokeArgs(
                $service,
                [
                    $room,
                    $tobeDeletedId,
                    [],
                    []
                ]
            );
        } catch (RoomAllotmentException $exception) {
            $this->assertInstanceOf(JsonResponse::class, $exception->render());
        }

        $spyRepo->shouldHaveReceived('deleteRoomUnit')->atLeast()->once();
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $tobeDeletedId));
        }
        $room->refresh();
        $this->assertNotEquals($roomCount, $room->room_count);
        $this->assertNotEquals($roomCount - $tobeDeletedCount, $room->room_count);
        $this->assertEquals($roomCount - ($tobeDeletedCount - 1), $room->room_count);
        $this->assertTrue(in_array(888, $failedList));
    }

    public function testProcessDeleteFailedOnFirst()
    {
        $roomCount = 5;
        $room = factory(Room::class)->create(['room_count' => $roomCount, 'room_available' => $roomCount]);
        $unitToBeDeleted = factory(RoomUnit::class, $roomCount)->create(['designer_id' => $room->id, 'occupied' => false])->nth(2, 1);
        $tobeDeletedId = array_merge([888], $unitToBeDeleted->pluck('id')->toArray());
        $tobeDeletedCount = count($tobeDeletedId);

        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('processDelete');
        $method->setAccessible(true);
        $successList = $failedList = [];
        try {
            [$successList, $failedList] = $method->invokeArgs(
                $service,
                [
                    $room,
                    $tobeDeletedId,
                    [],
                    []
                ]
            );
        } catch (RoomAllotmentException $exception) {
            $this->assertInstanceOf(JsonResponse::class, $exception->render());
        }

        $spyRepo->shouldHaveReceived('deleteRoomUnit')->atLeast()->once();
        foreach ($successList as $item) {
            $this->assertTrue(in_array($item['unit_id'], $tobeDeletedId));
        }
        $room->refresh();
        $this->assertNotEquals($roomCount, $room->room_count);
        $this->assertNotEquals($roomCount - $tobeDeletedCount, $room->room_count);
        $this->assertEquals($roomCount - ($tobeDeletedCount - 1), $room->room_count);
        $this->assertTrue(in_array(888, $failedList));
    }

    public function testGenerateSuccessResponseReturnArray()
    {
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 0]);
        $unit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);

        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('generateSuccessResponse');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$unit]
        );

        $this->assertEquals(['unit_id', 'designer_id'], array_keys($result));
    }

    public function testValidateMaxUnitRoomCountAlreadyMaxThrowException()
    {
        $this->expectException(RoomAllotmentException::class);
        $room = factory(Room::class)->create(['room_count' => 450, 'room_available' => 0]);

        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateMaxUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room, 51]
        );

        $this->assertNull($result);
    }

    public function testValidateMaxUnitRoomUnitCountAlreadyMaxThrowException()
    {
        $this->expectException(RoomAllotmentException::class);
        $room = factory(Room::class)->create(['room_count' => 440, 'room_available' => 0]);
        $room->room_unit = collect(range(0, 490));

        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateMaxUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room, 50]
        );

        $this->assertNull($result);
    }

    public function testValidateOccupiedUnitSuccess()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 0]);
        $unit = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => true])->first();

        $contract = factory(MamipayContract::class)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id,
            'designer_room_id' => $unit->id
        ]);

        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateOccupiedUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$unit]
        );

        $this->assertNull($result);
    }

    public function testValidateOccupiedUnitThrowExceptionWhenUnitWithContractIsGoingToBeChanged()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 0]);
        $occupiedUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => true]);
        $firstUnit = $occupiedUnits->first();

        factory(MamipayContract::class, 2)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ])->each(
            function ($contract) use ($room, &$occupiedUnits) {
                factory(MamipayContractKost::class)->create([
                    'contract_id' => $contract->id,
                    'designer_id' => $room->id,
                    'designer_room_id' => $occupiedUnits->shift()->id
                ]);
            }
        );

        $firstUnit->occupied = false;
        $firstUnit->save();

        $this->expectException(RoomAllotmentException::class);
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateOccupiedUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$firstUnit]
        );

        $this->assertNull($result);
    }

    public function testValidateOccupiedUnitThrowExceptionWhenContractLowerThanOccupied()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 0]);
        $occupiedUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => true]);
        $firstUnit = $occupiedUnits->first();

        factory(MamipayContract::class, 2)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ])->each(
            function ($contract) use ($room, &$occupiedUnits) {
                factory(MamipayContractKost::class)->create([
                    'contract_id' => $contract->id,
                    'designer_id' => $room->id,
                    'designer_room_id' => $occupiedUnits->shift()->id
                ]);
            }
        );

        $firstUnit->occupied = false;
        $firstUnit->save();

        $this->expectException(RoomAllotmentException::class);
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateOccupiedUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$firstUnit]
        );

        $this->assertNull($result);
    }

    public function testReduceRoomCountOccupied()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 0]);
        $unit = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => true])->first();
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('reduceRoomCount');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        $result = $method->invokeArgs(
            $service,
            [$room, $unit]
        );

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNull($result);
        $room->refresh();
        $this->assertEquals(1, $room->room_count);
        $this->assertEquals(0, $room->room_available);
    }

    public function testReduceRoomCountEmpty()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 2]);
        $unit = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => false])->first();
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('reduceRoomCount');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        $result = $method->invokeArgs(
            $service,
            [$room, $unit]
        );

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNull($result);
        $room->refresh();
        $this->assertEquals(1, $room->room_count);
        $this->assertEquals(1, $room->room_available);
    }

    public function testAddRoomCountOccupied()
    {
        $room = factory(Room::class)->create(['room_count' => 0, 'room_available' => 0]);
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('addRoomCount');
        $method->setAccessible(true);
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        $result = $method->invokeArgs(
            $service,
            [$room, true]
        );

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNull($result);
        $room->refresh();
        $this->assertEquals(1, $room->room_count);
        $this->assertEquals(0, $room->room_available);
    }

    public function testAddRoomCountEmpty()
    {
        $room = factory(Room::class)->create(['room_count' => 0, 'room_available' => 0]);
        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('addRoomCount');
        $mockTime = Carbon::create(2020, 8, 27, 14, 14);
        Carbon::setTestNow($mockTime);
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room, false]
        );

        $this->assertEquals($mockTime, $room->kost_updated_date);
        $this->assertNull($result);
        $room->refresh();
        $this->assertEquals(1, $room->room_count);
        $this->assertEquals(1, $room->room_available);
    }

    public function testBackfillUnitNotRunningOnZeroRoomCount()
    {
        $room = factory(Room::class)->create(['room_count' => 0, 'room_available' => 0]);
        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('backfillUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room]
        );

        $this->assertNull($result);
        $spyRepo->shouldNotReceive('backFillRoomUnit');
    }

    public function testBackfillUnitNotRunningOnHavingRoomUnit()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 2]);
        factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => false]);
        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('backfillUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room]
        );

        $this->assertNull($result);
        $spyRepo->shouldNotReceive('backFillRoomUnit');
    }

    public function testBackfillUnitRunningOnNotHavingRoomUnit()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 2]);
        $spyRepo = Mockery::spy(RoomUnitRepositoryEloquent::class)->makePartial();
        $service = new RoomUnitService($spyRepo);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('backfillUnit');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $service,
            [$room]
        );

        $this->assertNull($result);
        $spyRepo->shouldReceive('backFillRoomUnit');
    }

    public function testValidateUnitNotAssociatedWithContracts()
    {
        $room = factory(Room::class)->create(['room_count' => 2, 'room_available' => 2]);
        $occupiedUnits = factory(RoomUnit::class, 2)->create(['designer_id' => $room->id, 'occupied' => false]);
        $firstUnit = $occupiedUnits->first();

        factory(MamipayContract::class, 2)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ])->each(
            function ($contract) use ($room, &$occupiedUnits) {
                factory(MamipayContractKost::class)->create([
                    'contract_id' => $contract->id,
                    'designer_id' => $room->id,
                    'designer_room_id' => $occupiedUnits->shift()->id
                ]);
            }
        );

        $service = app()->make(RoomUnitService::class);
        $serviceReflection = new \ReflectionClass(RoomUnitService::class);
        $method = $serviceReflection->getMethod('validateUnitNotAssociatedWithContracts');
        $method->setAccessible(true);

        $this->expectException(RoomAllotmentException::class);

        $result = $method->invokeArgs(
            $service,
            [$firstUnit]
        );

        $this->assertNull($result);
    }
}
