<?php

namespace app\Services\RoomUnit;

use App\Services\RoomUnit\AutoAssignRoomLevelService;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\App;
use RuntimeException;

class AutoAssignRoomLevelServiceTest extends MamiKosTestCase
{
    private $service;
    private const CSV_FILE = 'kost_list_example.csv';

    public function setUp() : void
    {
        parent::setUp();
        $this->service = App::make(AutoAssignRoomLevelService::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-4602
     * @group app/Services/RoomUnit/AutoAssignRoomLevelService
     */
    public function testBuildArrayOfIdsWithFileNotFoundExpectException()
    {
        $csvFile = str_random().DIRECTORY_SEPARATOR.str_random();
        $this->expectException(RuntimeException::class);

        $this->service->buildArrayOfIds($csvFile);
    }

    /**
     * @group UG
     * @group UG-4602
     * @group app/Services/RoomUnit/AutoAssignRoomLevelService
     */
    public function testBuildArrayOfIdsWithFileNotValidExpectException()
    {
        $csvFile = __FILE__;
        $this->expectException(RuntimeException::class);

        $this->service->buildArrayOfIds($csvFile);
    }

    /**
     * @group UG
     * @group UG-4602
     * @group app/Services/RoomUnit/AutoAssignRoomLevelService
     */
    public function testBuildArrayOfBookingRequestIdSuccess()
    {
        $csvFile = self::CSV_FILE;
        $resultExpectedArray = $this->service->buildArrayOfIds($csvFile);
        $resultArray = ['123', '234', '345'];

        $this->assertEquals($resultArray, $resultExpectedArray);
    }
}