<?php

namespace app\Services\RoomUnit;

use App\Services\RoomUnit\RoomUnitContractService;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Collection;
use RuntimeException;

class RoomUnitContractServiceTest extends MamiKosTestCase
{
    
    private const CSV_FILE = 'room_allotment_contract_not_sync.csv';
    private const TOTAL_CSV_FILE_RECORD = 206;

    public function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testBuildCollectionOfRoomUnitDataFileIsEmpty(): void
    {
        // prepare data
        $csvFile = time() . 'file-not-found.csv';

        // expect
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('File not found');

        // run test
        RoomUnitContractService::buildCollectionOfRoomUnitData($csvFile);
    }

    public function testBuildCollectionOfRoomUnitDataSuccess(): void
    {
        // prepare data
        $csvFile = self::CSV_FILE;
        $totalOfCsvFileRecord = self::TOTAL_CSV_FILE_RECORD;
        $resultExpectedCollection = RoomUnitContractService::buildCollectionOfRoomUnitData($csvFile);

        // run test
        $this->assertInstanceOf(Collection::class, $resultExpectedCollection);
        $this->assertEquals($resultExpectedCollection->count(), $totalOfCsvFileRecord);
    }

    public function testIsCsvFileNotValid(): void
    {
        // prepare data
        $csvFile = storage_path('csv-room-allotment') . DIRECTORY_SEPARATOR . 'room_allotment_contract_not_sync.exe';
        $service = $this->app->make(RoomUnitContractService::class);
        $result = $this->getPublicIsCsvFile()->invokeArgs(
            $service, [$csvFile]
        );

        // run test
        $this->assertFalse($result);
    }

    public function testIsCsvFileValid(): void
    {
        // prepare data
        $csvFile = storage_path('csv-room-allotment') . DIRECTORY_SEPARATOR . self::CSV_FILE;
        $service = $this->app->make(RoomUnitContractService::class);
        $result = $this->getPublicIsCsvFile()->invokeArgs(
            $service, [$csvFile]
        );

        // run test
        $this->assertTrue($result);
    }

    /**
     * Handle private function from RoomUnitContractService
     * @return \ReflectionMethod
     */
    private function getPublicIsCsvFile(): \ReflectionMethod
    {
        $method = $this->getNonPublicMethodFromClass(
            RoomUnitContractService::class,
            'isCsvFile'
        );
        return $method;
    }
}