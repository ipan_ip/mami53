<?php

namespace app\Services\Booking;

use App;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Room\Room;
use Illuminate\Foundation\Testing\WithoutEvents;
use App\Services\Booking\BookingDraftService;

class BookingDraftServiceTest extends MamiKosTestCase
{
    use WithoutEvents;

    protected $service;

    protected function setUp(): void
    {
        $this->service = new BookingDraftService();
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testSaveRecentRoomViewedReturnNull(): void
    {
        // run test
        $response = $this->service->saveRecentRoomViewed(1, null);
        $this->assertNull($response);
    }

    public function testSaveRecentRoomViewedReturnNullWithUserNull(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create(['song_id' => 900002, 'is_booking' => 1]);

        // run test
        $response = $this->service->saveRecentRoomViewed($roomEntity->song_id, null);
        $this->assertNull($response);
    }

    public function testSaveRecentRoomViewedSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create(['song_id' => 900001, 'is_booking' => 1]);

        // run test
        $response = $this->service->saveRecentRoomViewed($roomEntity->song_id, $userEntity);
        $this->assertNotNull($response);
        $this->assertEquals($userEntity->id, $response->user_id);
        $this->assertEquals($roomEntity->id, $response->designer_id);
    }
}