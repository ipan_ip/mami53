<?php

namespace app\Services\Booking;

use App;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutEvents;
use Mockery;
use Illuminate\Support\Facades\Event;
use App\Entities\Dbet\DbetLinkRegistered;
use App\Services\Booking\ContractSubmissionService;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\UniqueCode;

class ContractSubmissionServiceTest extends MamiKosTestCase
{
    use WithoutEvents;

    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
        Mockery::close();
    }

    public function testOwnerRejectNotificationIsSuccess(): void
    {
        // service
        $service = new ContractSubmissionService();

        // disable event
        Event::fake();

        // prepare data
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        // run test
        $response = $service->ownerRejectNotification($data);
        $this->assertTrue($response);
    }

    public function testOwnerConfirmNotificationIsSuccess(): void
    {
        // service
        $service = new ContractSubmissionService();

        // disable event
        Event::fake();

        // prepare data
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        // run test
        $response = $service->ownerConfirmNotification($data, 'original', true);
        $this->assertTrue($response);
    }

    public function testTenantRequestNotificationIsSuccess(): void
    {
        // service
        $service = new ContractSubmissionService();

        // disable event
        Event::fake();

        // prepare data
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);
        $data = factory(DbetLinkRegistered::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id
        ]);

        // run test
        $response = $service->tenantRequestNotification($userEntity, $data);
        $this->assertTrue($response);
    }

    public function testReminderOwnerRequestIsSuccessWithEmptyData(): void
    {
        // service
        $service = new ContractSubmissionService();

        // disable event
        Event::fake();

        // run test
        $response = $service->reminderOwnerRequest();
        $this->assertTrue($response);
    }

    public function testReminderOwnerRequestIsSuccessWithData(): void
    {
        // service
        $service = new ContractSubmissionService();

        // disable event
        Event::fake();

        // prepare data
        $roomEntity = factory(Room::class)->create();
        $userEntity = factory(User::class)->create();
        $ownerEntity = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $ownerEntity->id,
            'status' => 'verified'
        ]);
        factory(DbetLinkRegistered::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $userEntity->id,
            'status' => DbetLinkRegistered::STATUS_PENDING
        ]);
        factory(UniqueCode::class)->create([
            'designer_id' => $roomEntity->id
        ]);

        // run test
        $response = $service->reminderOwnerRequest();
        $this->assertTrue($response);
    }
}