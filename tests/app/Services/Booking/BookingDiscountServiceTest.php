<?php

namespace App\Services\Booking;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Services\Booking\BookingDiscountService;

class BookingDiscountServiceTest extends MamiKosTestCase
{

    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(BookingDiscountService::class);
    }
    
    public function testRemoveNotFoundDiscount()
    {
        $result = $this->service->remove(9999);
        $this->assertEquals([
            'status' => false,
            'message' => 'invalid data discount',
        ], $result);
    }

    public function testRemoveInActiveDiscount()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 100000,
            'is_booking' => 1,
            'is_active' => true
        ]);

        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'is_active' => true
        ]);

        $result = $this->service->remove($discount->id);
        $this->assertEquals(['status' => true, 'message' => 'success'], $result);
    }

    public function testSetToOriginalPrice()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 100000,
            'is_booking' => 1,
            'is_active' => true
        ]);

        // booking set to inactive
        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price_type' => 'monthly',
            'is_active' => false
        ]);

        // price change
        $room->price_monthly = 4000;
        $room->save();

        $result = $this->service->remove($discount->id);
        $this->assertEquals(['status' => true, 'message' => 'success'], $result);

        // check db price must be correct
        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'price_monthly' => 4000
        ]);
    }

    public function testSetToOriginalPriceNoRoomData()
    {
        // booking set to inactive
        $discount = factory(BookingDiscount::class)->create([
            'designer_id' => 999,
            'price_type' => 'monthly',
            'is_active' => true
        ]);

        $result = $this->service->setToOriginalPrice($discount);
        $this->assertEquals(false, $result);
    }
}
