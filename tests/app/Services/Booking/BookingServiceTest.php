<?php

namespace app\Services\Booking;

use App;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\RoomOwner;
use App\Jobs\SendPushNotificationQueue;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithoutEvents;
use App\Services\Booking\BookingService;
use App\Entities\Room\Room;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\QueueFake;
use App\Entities\Mamipay\MamipayInvoice;
use App\Test\Traits\GoldplusLevelConfig;
use App\Entities\Activity\Call;
use Bugsnag;
use Mockery;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class BookingServiceTest extends MamiKosTestCase
{
    use WithoutEvents;
    use GoldplusLevelConfig;

    const REMINDER_TYPE_PUSH_NOTIFICATION = 'push-notification';

    protected $service;
    protected $bugSnag;

    protected function setUp(): void
    {
        parent::setUp();

        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->grpChnlSrv = Mockery::mock('App\Services\Sendbird\GroupChannelService');
        $this->service = new BookingService($this->grpChnlSrv);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
        Mockery::close();
    }

    public function testOwnerHaveEmptyRoomNotificationSuccess(): void
    {
        // disable event
        Event::fake();

        $ownerEntity = factory(User::class)->create();

        // run test
        $response = $this->service->ownerHaveEmptyRoomNotification($ownerEntity, null);
        $this->assertTrue($response);
    }

    public function testGetReminderNotificationOwnerAcceptBookingSuccess(): void
    {
        // mock repository data
        $mockBookingUserRepo = Mockery::mock('App\Repositories\Booking\BookingUserRepository');
        $mockBookingUserRepo->shouldReceive('getReminderNotification')
            ->andReturn(null);

        // run test
        $this->assertNull($this->service->getReminderNotificationOwnerAcceptBooking());
    }

    public function testSendReminderAcceptBookingSuccess(): void
    {
        $this->withoutJobs();
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $bookingDesignerEntity = factory(BookingDesigner::class)->create([
            'designer_id' => $roomEntity->id
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesignerEntity
        ]);
        $ownerUserEntity = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $roomEntity->id,
            'user_id' => $ownerUserEntity->id,
            'status' => 'verified'
        ]);

        $reminders = config('booking.scheduler_owner_confirm_booking_notification');

        // run test
        $result = $this->service->sendReminderAcceptBooking($bookingUserEntity, $reminders[0], self::REMINDER_TYPE_PUSH_NOTIFICATION);
        $this->assertNull($result);
    }

    public function testSendReminderAcceptBookingExceptionSuccess(): void
    {
        $exception = new \Exception('test');
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $bookingUserEntity = factory(BookingUser::class)->create();
        $reminders = config('booking.scheduler_owner_confirm_booking_notification');

        // run test
        $result = $this->service->sendReminderAcceptBooking($bookingUserEntity, $reminders[0], self::REMINDER_TYPE_PUSH_NOTIFICATION);
        $this->assertNull($result);
    }

    public function testSendEmailWhenSubmitBookingToOwnerSuccess(): void
    {
        // disable event
        Event::fake();

        // skip jobs
        $this->withoutJobs();

        // prepare data
        $ownerEntity = factory(User::class)->create();
        $bookingUserEntity = factory(BookingUser::class)->create();

        $response = $this->service->sendEmailWhenSubmitBookingToOwner($ownerEntity, $bookingUserEntity, 'Rumah Cemar');
        $this->assertTrue($response);
    }

    /**
     * @group UG-4432
     */
    public function testSendEmailWhenSubmitBookingToOwnerLimitedGpFailed(): void
    {
        // disable event
        Event::fake();

        $this->prepareAllGpLevels();

        // prepare data
        $ownerEntity = factory(User::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => factory(Room::class)->state('gp3-random')->create(),
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesigner,
        ]);
        
        $response = $this->service->sendEmailWhenSubmitBookingToOwner($ownerEntity, $bookingUserEntity, 'Rumah Cemar');
        $this->assertFalse($response);
    }

    public function testSendReminderNearExpiredBookingSuccess(): void
    {
        $this->withoutJobs();
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $bookingDesignerEntity = factory(BookingDesigner::class)->create([
            'designer_id' => $roomEntity->id
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'user_id' => 1,
            'booking_designer_id' => $bookingDesignerEntity
        ]);

        // run test
        $result = $this->service->sendReminderNearExpiredBooking($bookingUserEntity, 60);
        $this->assertNull($result);
    }

    public function testSendReminderNearExpiredBookingExceptionSuccess(): void
    {
        $exception = new \Exception('test');
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $bookingUserEntity = factory(BookingUser::class)->create([
            'user_id' => 1
        ]);
        $reminders = config('booking.scheduler_owner_confirm_booking_notification');

        // run test
        $result = $this->service->sendReminderNearExpiredBooking($bookingUserEntity, 60);
        $this->assertNull($result);
    }

    /**
     * @runTestsInSeparateProcesses
     * @preserveGlobalState disabled
     */
    public function testGetInvoiceByContractIdFromMamipaySuccess(): void
    {
        // prepare data
        $invoices = factory(MamipayInvoice::class, 2)->make();

        // mock data response
        $obj = new \stdClass();
        $obj->status = true;
        $obj->data = $invoices;

        // mock mamipay api data
        $mamipayApi = Mockery::mock('overload:App\Libraries\MamipayApi');
        $mamipayApi->shouldReceive('get')->andReturn($obj);

        // run test
        $response = $this->service->getInvoiceByContractIdFromMamipay(1);
        $this->assertEquals(2, count($response));
    }

    public function testSendReminderInvoiceExpiryIsTrue()
    {
        // prepare data
        $contract = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE
        ]);
        $room = factory(Room::class)->create([
            'song_id' => '123',
            'id' => '456'
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contract->id,
            'designer_id' => $room->id
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID
        ]);

        $result = $this->service->sendReminderInvoiceExpiry();
        $this->assertTrue($result);
    }

    public function testOwnerEnableDbetLinkIsTrue(): void
    {
        $userEntity = factory(User::class)->create([
            'id' => 100001
        ]);
        // run test
        $result = $this->service->ownerEnableDbetLink($userEntity);
        $this->assertTrue($result);
    }

    public function testOwnerEnableDbetLinkIsForceTrue(): void
    {
        $userEntity = factory(User::class)->create([
            'id' => 100000
        ]);
        // run test
        $result = $this->service->ownerEnableDbetLink($userEntity);
        $this->assertTrue($result);
    }

    public function testSendChatAfterBookingSuccess()
    {
        $job = Mockery::mock('overload:App\Jobs\Booking\BookingSendBirdAutoChatQueue');
        $job->shouldReceive('dispatch')
            ->once()
            ->andReturn(null);

        $channel = new \App\Services\Sendbird\GroupChannel\Channel;
        $channel->channel_url = 'someChannelUrl';
        $this->grpChnlSrv
            ->shouldReceive('findOrCreateChannelForTenantAndRoom')
            ->andReturn($channel);

        $room    = factory(Room::class)->make();
        $user    = factory(User::class)->make();
        $booking = factory(BookingUser::class)->make();

        $channelUrl = $this->service->sendChatAfterBookingSuccess($room, $user, $booking);
        $this->assertEquals($channel->channel_url, $channelUrl);
    }
}