<?php
namespace App\Services\Chat;

use App\Test\MamiKosTestCase;
use App\Entities\Chat\Channel;
use App\Entities\Chat\Archive;

use App\Services\Chat\Exceptions\EmptyChannelException;
use App\Services\Chat\Exceptions\EmptyMessageException;
use App\Services\Chat\Exceptions\EmptyMemberException;

class ChannelExpiratorTest extends MamiKosTestCase
{
    protected $vendor;
    protected $channelExpirator;

    public function setUp(): void
    {
        parent::setUp();
        $this->makeVendorMock();
        $this->makeChannelMock();
        $this->makeChannelExpirator();
    }

    public function testExpireShouldNotRaiseError()
    {
        $this->assertNull($this->channelExpirator->expire());
    }

    public function testExpireShouldCallSetExpireOnChannel()
    {
        $this->channelMock->shouldReceive('setExpired')->once();
        $this->channelExpirator->expire();
    }

    public function testExpireWithEmptyChannel()
    {
        $this->vendorMock->shouldReceive('getChannel')
            ->with($this->channelMock->global_id)
            ->andReturn([]);

        $this->expectException(EmptyChannelException::class);
        $this->channelExpirator->expire();
    }

    public function testExpireWithEmptyMessage()
    {
        $this->vendorMock->shouldReceive('getMessages')
            ->with($this->channelMock->global_id)
            ->andReturn([]);

        $this->expectException(EmptyMessageException::class);
        $this->channelExpirator->expire();
    }

    public function testExpireWithEmptyMembers()
    {
        $this->vendorMock->shouldReceive('getMembers')
            ->with($this->channelMock->global_id)
            ->andReturn([]);

        $this->expectException(EmptyMemberException::class);
        $this->channelExpirator->expire();
    }

    public function testVendorShouldReceiveArchive()
    {
        $this->vendorMock->shouldReceive('archive')
            ->with($this->channelMock->global_id)
            ->once();
        $this->channelExpirator->expire();
    }

    public function testArchiveShouldBeSavedAndHaveTheRightValue()
    {
        $this->channelExpirator->expire();
        $archive = $this->channelExpirator->getArchive();

        $globalId = $this->channelMock->global_id;
        $this->assertEquals($this->vendorMock->getName(), $archive->vendor_name);
        $this->assertEquals($globalId, $archive->global_id);
        $this->assertEquals($this->vendorMock->getChannel($globalId), $archive->channel);
        $this->assertEquals($this->vendorMock->getMembers($globalId), $archive->members);
        $this->assertEquals($this->vendorMock->getMessages($globalId), $archive->messages);
        $this->assertNotNull($archive->expired_at);
        $this->assertNotNull($archive->id);
    }

    // ====== test setup ====== //

    protected function makeVendorMock()
    {
        $this->vendorMock = \Mockery::mock(VendorInterface::class);
        $this->vendorMock->shouldReceive('archive')
            ->byDefault();
        $this->vendorMock->shouldReceive('getChannel')
            ->andReturn(SendBirdResponseMocker::getChannel())
            ->byDefault();
        $this->vendorMock->shouldReceive('getMessages')
            ->andReturn(SendBirdResponseMocker::getMessages()['messages'])
            ->byDefault();
        $this->vendorMock->shouldReceive('getMembers')
            ->andReturn(SendBirdResponseMocker::getMembers())
            ->byDefault();
        $this->vendorMock->shouldReceive('getName')
            ->andReturn('Sendcat')
            ->byDefault();
    }

    protected function makeChannelMock()
    {
        $this->channelMock = \Mockery::mock(Channel::class);
        $this->channelMock->shouldReceive('getAttribute')
            ->with('global_id')
            ->andReturn('some_global_id')
            ->byDefault();
        $this->channelMock->shouldReceive('getAttribute')
            ->with('expired_at')
            ->andReturn('2020-02-02 10:10:10')
            ->byDefault();
        $this->channelMock->shouldReceive('setExpired')
            ->andReturn(null)
            ->byDefault();
    }

    protected function makeChannelExpirator()
    {
        $this->channelExpirator = new ChannelExpirator($this->channelMock);
        $this->channelExpirator->setVendor($this->vendorMock);
    }
}