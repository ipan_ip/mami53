<?php
namespace App\Services\Chat;

class SendBirdResponseMocker
{
    public static function getChannel()
    {
        return [
            "custom_type" => "",
            "is_ephemeral" => false,
            "freeze" => false,
            "disappearing_message" => [
              "message_survival_seconds" => -1,
              "is_triggered_by_message_read" => false,
            ],
            "member_count" => 3,
            "created_by" => null,
            "is_broadcast" => false,
            "last_message" => null,
            "unread_mention_count" => 0,
            "sms_fallback" => [
              "wait_seconds" => -1,
              "exclude_user_ids" => [],
            ],
            "is_discoverable" => false,
            "ignore_profanity_filter" => false,
            "channel_url" => "sendbird_group_channel_275226760_4646871e46a42a3bae27321d887267a966017b6e",
            "channel" => [
              "name" => "Arifin Arifin : Kost Murba 4 Cibeunying Kaler Bandung",
              "member_count" => 3,
              "custom_type" => "",
              "channel_url" => "sendbird_group_channel_275226760_4646871e46a42a3bae27321d887267a966017b6e",
              "created_at" => 1614591130,
              "cover_url" => "https://static8.kerupux.com/uploads/cache/data/style/2019-05-14/fz7nkDLx-240x320.jpg",
              "max_length_message" => 5000,
              "data" => '{"room_id":18235313}',
            ],
            "message_survival_seconds" => -1,
            "unread_message_count" => 0,
            "is_distinct" => false,
            "operators" => [],
            "cover_url" => "https://static8.kerupux.com/uploads/cache/data/style/2019-05-14/fz7nkDLx-240x320.jpg",
            "members" => [
              [
                "is_muted" => false,
                "is_active" => true,
                "is_online" => false,
                "require_auth_for_profile_image" => false,
                "nickname" => "Shinta Yuliana",
                "user_id" => "642217",
                "state" => "joined",
                "role" => "",
                "last_seen_at" => 0,
                "profile_url" => "",
                "metadata" => [],
              ],
              [
                "is_muted" => false,
                "is_active" => true,
                "is_online" => false,
                "require_auth_for_profile_image" => false,
                "nickname" => "CS Mamikos Ruri",
                "user_id" => "184923",
                "state" => "joined",
                "role" => "",
                "last_seen_at" => 1591762023519,
                "profile_url" => "",
                "metadata" => [
                  "role" => "admin",
                ],
              ],
              [
                "is_muted" => false,
                "is_active" => true,
                "is_online" => false,
                "require_auth_for_profile_image" => false,
                "nickname" => "Arifin Arifin",
                "user_id" => "99447241",
                "state" => "joined",
                "role" => "",
                "last_seen_at" => 1614591131509,
                "profile_url" => "",
                "metadata" => [],
              ],
            ],
            "is_public" => false,
            "data" => '{"room_id":18235313}',
            "joined_member_count" => 3,
            "is_super" => false,
            "name" => "Arifin Arifin : Kost Murba 4 Cibeunying Kaler Bandung",
            "created_at" => 1614591130,
            "is_access_code_required" => false,
            "max_length_message" => 5000,
        ];
    }

    public static function getMembers()
    {
        return self::getChannel()['members'];
    }

    public static function getMessages()
    {
        return[
            "messages" => [
                [
                "message_survival_seconds" => -1,
                "custom_type" => "",
                "mentioned_users" => [],
                "translations" => [],
                "updated_at" => 0,
                "is_op_msg" => false,
                "is_removed" => false,
                "user" => [
                    "require_auth_for_profile_image" => false,
                    "is_active" => true,
                    "role" => "",
                    "user_id" => "99447241",
                    "nickname" => "Arifin Arifin",
                    "profile_url" => "",
                    "metadata" => [],
                ],
                "file" => [],
                "message" => "Alamat kos di mana?",
                "data" => '{"room_id":18235313}',
                "silent" => false,
                "type" => "MESG",
                "created_at" => 1614592654740,
                "req_id" => "1614592646355",
                "mention_type" => "users",
                "channel_url" => "sendbird_group_channel_275226760_4646871e46a42a3bae27321d887267a966017b6e",
                "message_id" => 1169050743,
                ],
                [
                "message_survival_seconds" => -1,
                "custom_type" => "",
                "mentioned_users" => [],
                "translations" => [],
                "updated_at" => 0,
                "og_tag" => [
                    "og:url" => "https://maps.google.com/maps?z=7&q=-6.9050923446747",
                    "og:image" => [
                    "url" => "https://maps.google.com/maps/api/staticmap?center=1.31400005%2C103.84425005&zoom=11&size=256x256&language=en&sensor=false&client=google-maps-frontend&signature=S0fwOvFyYFlkdL3il86dgNU5iTQ",
                    "width" => 256,
                    "height" => 256,
                    ],
                    "og:description" => "Find local businesses, view maps and get driving directions in Google Maps.",
                    "og:title" => "Google Maps",
                ],
                "is_op_msg" => false,
                "is_removed" => false,
                "user" => [
                    "require_auth_for_profile_image" => false,
                    "is_active" => true,
                    "role" => "",
                    "user_id" => "184923",
                    "nickname" => "CS Mamikos Ruri",
                    "profile_url" => "",
                    "metadata" => [
                    "role" => "admin",
                    ],
                ],
                "file" => [],
                "message" => "\"
                    Kost Murba 4 Cibeunying Kaler Bandung beralamat di: jl. muararajeun baru IV no 9 bandung . https://maps.google.com/maps?z=7&q=-6.9050923446747,107.63008475304 *** Mohon hati-hati, Mamikos tidak bertanggung jawab atas semua transaksi di luar platform Mamikos. ***\r\n
                    \r\n
                    Balas di Mamikos - https://mamikos.com
                    \"",
                "data" => "",
                "silent" => false,
                "type" => "MESG",
                "created_at" => 1614592655466,
                "mention_type" => "users",
                "channel_url" => "sendbird_group_channel_275226760_4646871e46a42a3bae27321d887267a966017b6e",
                "message_id" => 1169050790,
                ],
                [
                "message_survival_seconds" => -1,
                "custom_type" => "",
                "mentioned_users" => [],
                "translations" => [],
                "updated_at" => 0,
                "is_op_msg" => false,
                "is_removed" => false,
                "user" => [
                    "require_auth_for_profile_image" => false,
                    "is_active" => true,
                    "role" => "",
                    "user_id" => "99447241",
                    "nickname" => "Arifin Arifin",
                    "profile_url" => "",
                    "metadata" => [],
                ],
                "file" => [],
                "message" => "kalu gitu oke la kalo gitu",
                "data" => '{"room_id":18235313}',
                "silent" => false,
                "type" => "MESG",
                "created_at" => 1614592674820,
                "req_id" => "1614592646358",
                "mention_type" => "users",
                "channel_url" => "sendbird_group_channel_275226760_4646871e46a42a3bae27321d887267a966017b6e",
                "message_id" => 1169052287,
                ],
            ],
        ];
    }
}