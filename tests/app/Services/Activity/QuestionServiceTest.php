<?php

namespace App\Services\Activity;

use App\Entities\Chat\ChatCondition;
use App\Entities\Chat\Criteria;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class QuestionServiceTest extends MamiKosTestCase
{
    private $goldPlus1;
    private $goldPlus2;
    private $goldPlus3;
    private $goldPlus4;

    protected function setUp(): void
    {
        parent::setUp();

        $now = Carbon::now();

        /*
         * ----- Chat Criteria Factory ------
         */

        factory(Criteria::class)->create(
            [
                'id' => 1,
                'name' => 'gp1',
                'attribute' => 'is_gp1',
                'description' => 'Kos is Mamikos Goldplus 1',
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 2,
                'name' => 'gp2',
                'attribute' => 'is_gp2',
                'description' => 'Kos is Mamikos Goldplus 2',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 3,
                'name' => 'gp3',
                'attribute' => 'is_gp3',
                'description' => 'Kos is Mamikos Goldplus 3',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 4,
                'name' => 'gp4',
                'attribute' => 'is_gp4',
                'description' => 'Kos is Mamikos Goldplus 4',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 5,
                'name' => 'Mamirooms',
                'attribute' => 'is_mamirooms',
                'description' => 'Kos is a Mamiroom',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 6,
                'name' => 'FreeListing',
                'attribute' => 'is_free_listing',
                'description' => 'Kos is not Premium and not BBK',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 7,
                'name' => 'PremiumSaldoOn',
                'attribute' => 'is_premium_saldo_on',
                'description' => 'Owner is premium and has saldo',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 8,
                'name' => 'PremiumSaldoOff',
                'attribute' => 'is_premium_saldo_off',
                'description' => 'Owner is premium and not has saldo',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 9,
                'name' => 'BBK',
                'attribute' => 'is_bbk',
                'description' => 'Kos can be booked',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 10,
                'name' => 'NotBBK',
                'attribute' => 'is_not_bbk',
                'description' => 'Kos cant be booked',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 11,
                'name' => 'RoomAvailable',
                'attribute' => 'is_available',
                'description' => 'Kos has available room',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        factory(Criteria::class)->create(
            [
                'id' => 12,
                'name' => 'RoomNotAvailable',
                'attribute' => 'is_not_available',
                'description' => 'Kos has no available room',
                'created_at' => $now,
                'updated_at' => $now
            ]
        );

        /*
         * ----- End of Chat Criteria Factory -----
         */

        /*
         * ----- Chat Condition Factory ------
         */

        // Condition is GoldPlus 1,2,3 (impossible to have)
        Factory(ChatCondition::class)->create(
            [
                'id' => 1,
                'chat_question_id' => 5,
                'chat_criteria_ids' => "1,2,3",
                "reply" => 'first',
                "order" => 1
            ]
        );

        // Condition is GoldPlus3, RoomNotAvailable and PremiumSaldoOn
        Factory(ChatCondition::class)->create(
            [
                'id' => 2,
                'chat_question_id' => 5,
                'chat_criteria_ids' => "3,12,7",
                "reply" => 'second',
                "order" => 2
            ]
        );

        // Condition is GoldPlus3 and RoomNotAvailable
        Factory(ChatCondition::class)->create(
            [
                'id' => 3,
                'chat_question_id' => 5,
                'chat_criteria_ids' => "3,12",
                "reply" => 'third',
                "order" => 3
            ]
        );

        // Condition is GoldPlus3, RoomAvailable and PremiumSaldoOn
        Factory(ChatCondition::class)->create(
            [
                'id' => 4,
                'chat_question_id' => 5,
                'chat_criteria_ids' => "3,11,7",
                "reply" => 'fourth',
                "order" => 4
            ]
        );

        // Condition is GoldPlus3 and RoomAvailable
        Factory(ChatCondition::class)->create(
            [
                'id' => 5,
                'chat_question_id' => 5,
                'chat_criteria_ids' => "3,11",
                "reply" => 'fifth',
                "order" => 5
            ]
        );

        /*
         * ----- End of Chat Condition Factory -----
         */

        /*
         * ----- Generate Kost Level -----
         */
        factory(KostLevel::class)->create(
            [
                'name' => 'regular',
                'is_regular' => 1,
                'is_hidden' => 0
            ]
        );

        $this->goldPlus1 = factory(KostLevel::class)->create(
            [
                'name' => 'Mamikos Goldplus 1',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        $this->goldPlus2 = factory(KostLevel::class)->create(
            [
                'name' => 'Mamikos Goldplus 2',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        $this->goldPlus3 = factory(KostLevel::class)->create(
            [
                'name' => 'Mamikos Goldplus 3',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );

        $this->goldPlus4 = factory(KostLevel::class)->create(
            [
                'name' => 'Mamikos Goldplus 4',
                'is_regular' => 0,
                'is_hidden' => 0
            ]
        );
    }

    public function testGetAnswerFromConditionGiveResponseSecond()
    {
        $room = factory(Room::class)
            ->state('premium')
            ->create(
                [
                    'room_available' => 0
                ]
            );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus3->id
            ]
        );

        $conditionCollection = $this->getConditionsOfQuestionId5();

        $response = (new QuestionService($room, $conditionCollection))->getAnswerFromCondition();

        $this->assertEquals('second', $response);
    }

    public function testGetAnswerFromConditionGiveResponseThird()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'room_available' => 0
                ]
            );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus3->id
            ]
        );

        $conditionCollection = $this->getConditionsOfQuestionId5();

        $response = (new QuestionService($room, $conditionCollection))->getAnswerFromCondition();

        $this->assertEquals('third', $response);
    }

    public function testGetAnswerFromConditionGiveResponseFourth()
    {
        $room = factory(Room::class)
            ->state('premium')
            ->create(
                [
                    'room_available' => 5
                ]
            );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus3->id
            ]
        );

        $conditionCollection = $this->getConditionsOfQuestionId5();

        $response = (new QuestionService($room, $conditionCollection))->getAnswerFromCondition();

        $this->assertEquals('fourth', $response);
    }

    public function testGetAnswerFromConditionGiveResponseFifth()
    {
        $room = factory(Room::class)
            ->create(
                [
                    'room_available' => 5
                ]
            );

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus3->id
            ]
        );

        $conditionCollection = $this->getConditionsOfQuestionId5();

        $response = (new QuestionService($room, $conditionCollection))->getAnswerFromCondition();

        $this->assertEquals('fifth', $response);
    }

    public function testGetAnswerFromConditionGiveResponseNull()
    {
        $room = factory(Room::class)->create();

        $conditionCollection = $this->getConditionsOfQuestionId5();

        $response = (new QuestionService($room, $conditionCollection))->getAnswerFromCondition();

        $this->assertNull($response);
    }

    public function testIsGoldPlus1ReturnTrue()
    {
        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus1->id
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus1');

        $this->assertTrue($response);
    }

    public function testIsGoldPlus1ReturnFalse()
    {
        $room = factory(Room::class)->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus1');

        $this->assertFalse($response);
    }

    public function testIsGoldPlus2ReturnTrue()
    {
        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus2->id
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus2');

        $this->assertTrue($response);
    }

    public function testIsGoldPlus2ReturnFalse()
    {
        $room = factory(Room::class)->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus2');

        $this->assertFalse($response);
    }

    public function testIsGoldPlus3ReturnTrue()
    {
        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus3->id
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus3');

        $this->assertTrue($response);
    }

    public function testIsGoldPlus3ReturnFalse()
    {
        $room = factory(Room::class)->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus3');

        $this->assertFalse($response);
    }

    public function testIsGoldPlus4ReturnTrue()
    {
        $room = factory(Room::class)->create();

        factory(KostLevelMap::class)->create(
            [
                'kost_id' => $room->song_id,
                'level_id' => $this->goldPlus4->id
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus4');

        $this->assertTrue($response);
    }

    public function testIsGoldPlus4ReturnFalse()
    {
        $room = factory(Room::class)->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isGoldPlus4');

        $this->assertFalse($response);
    }

    public function testIsMamiRoomsReturnTrue()
    {
        $room = factory(Room::class)
            ->state('mamirooms')
            ->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isMamiRooms');

        $this->assertTrue($response);
    }

    public function testIsMamiRoomsReturnFalse()
    {
        $room = factory(Room::class)
            ->state('notMamirooms')
            ->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isMamiRooms');

        $this->assertFalse($response);
    }

    public function testIsFreeListingReturnTrue()
    {
        $room = factory(Room::class)
            ->state('notAvailableBooking')
            ->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isFreeListing');

        $this->assertTrue($response);
    }

    public function testIsFreeListingReturnFalse()
    {
        $owner = factory(User::class)->create(
            [
                'date_owner_limit' => Carbon::today()->addDay(1),
                'is_owner' => 'true'
            ]
        );

        $room = factory(Room::class)
            ->state('availableBooking')
            ->create();

        factory(RoomOwner::class)->create(
            [
                "designer_id" => $room->id,
                "user_id" => $owner->id,
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isFreeListing');

        $this->assertFalse($response);
    }

    public function testIsPremiumSaldoOnReturnTrue()
    {
        $room = factory(Room::class)
            ->state('premium')
            ->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isPremiumSaldoOn');

        $this->assertTrue($response);
    }

    public function testIsPremiumSaldoOnReturnFalse()
    {
        $room = factory(Room::class)
            ->state('notPremium')
            ->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isPremiumSaldoOn');

        $this->assertFalse($response);
    }

    public function testIsPremiumSaldoOffReturnTrue()
    {
        $owner = factory(User::class)->create(
            [
                'date_owner_limit' => Carbon::today()->addDays(1),
                'is_owner' => 'true'
            ]
        );

        $room = factory(Room::class)->create();

        factory(RoomOwner::class)->create(
            [
                "designer_id" => $room->id,
                "user_id" => $owner->id,
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isPremiumSaldoOff');

        $this->assertTrue($response);
    }

    public function testIsPremiumSaldoOffReturnFalse()
    {
        $owner = factory(User::class)->create(
            [
                'date_owner_limit' => Carbon::today()->subDay(1),
                'is_owner' => 'true'
            ]
        );

        $room = factory(Room::class)->create();

        factory(RoomOwner::class)->create(
            [
                "designer_id" => $room->id,
                "user_id" => $owner->id,
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isPremiumSaldoOff');

        $this->assertFalse($response);
    }

    public function testIsBBKReturnTrue()
    {
        $room = factory(Room::class)->state('availableBooking')->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isBBK');

        $this->assertTrue($response);
    }

    public function testIsBBKReturnFalse()
    {
        $room = factory(Room::class)->state('notAvailableBooking')->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isBBK');

        $this->assertFalse($response);
    }

    public function testIsNotBBKReturnTrue()
    {
        $room = factory(Room::class)->state('notAvailableBooking')->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isNotBBK');

        $this->assertTrue($response);
    }

    public function testIsNotBBKReturnFalse()
    {
        $room = factory(Room::class)->state('availableBooking')->create();

        $response = $this->getResponseFromPrivateMethod($room, 'isNotBBK');

        $this->assertFalse($response);
    }

    public function testIsAvailableReturnTrue()
    {
        $room = factory(Room::class)->create(
            [
                'room_available' => 4
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isAvailable');

        $this->assertTrue($response);
    }

    public function testIsAvailableReturnFalse()
    {
        $room = factory(Room::class)->create(
            [
                'room_available' => 0
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isAvailable');

        $this->assertFalse($response);
    }

    public function testIsNotAvailableReturnTrue()
    {
        $room = factory(Room::class)->create(
            [
                'room_available' => 0
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isNotAvailable');

        $this->assertTrue($response);
    }

    public function testIsNotAvailableReturnFalse()
    {
        $room = factory(Room::class)->create(
            [
                'room_available' => 4
            ]
        );

        $response = $this->getResponseFromPrivateMethod($room, 'isNotAvailable');

        $this->assertFalse($response);
    }

    private function getConditionsOfQuestionId5()
    {
        return ChatCondition::where('chat_question_id', 5)->get();
    }

    /**
     *  Get the response of private method from Question Service
     *
     * @param $room
     * @param $methodName
     * @return mixed
     */
    private function getResponseFromPrivateMethod($room, $methodName)
    {
        $service = (new QuestionService($room, collect()));

        $method = $this->getNonPublicMethodFromClass(QuestionService::class, $methodName);

        return $method->invokeArgs($service, []);
    }
}
