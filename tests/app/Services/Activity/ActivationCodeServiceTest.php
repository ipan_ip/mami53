<?php

namespace App\Services\Activity;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeDeliveryReport;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Enums\Activity\ActivationCodeVia;
use App\Jobs\Activity\ActivationCodeDeliveryReportFetchQueue;
use App\Jobs\ProcessNotificationDispatch;
use App\Libraries\Activity\ActivationCodeLibrary;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use App\Services\Activity\ActivationCodeService;
use App\Services\Activity\Request\SendSmsActivationCodeRequest;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use RuntimeException;
use WhatsAppBusiness;
use Illuminate\Support\Facades\Bus;

/**
 * Test App\Services\Activity\ActivationCodeService
 * @property \Mockery\MockInterface|ActivationCodeService $activationCodeService
 * @property \Mockery\MockInterface|\Mockery\LegacyMockInterface $activationCodeLibrary
 */
class ActivationCodeServiceTest extends MamiKosTestCase
{
    use WithFaker;

    private $activationCodeService;
    private $activationCodeLibrary;

    private const ASSERTABLE_FIELD_ON_UPDATE = ['network', 'status', 'reason', 'send_at', 'done_at'];

    protected function setUp(): void
    {
        parent::setUp();
        $this->activationCodeLibrary = $this->mock(ActivationCodeLibrary::class);
        $this->activationCodeService = Mockery::mock(
            ActivationCodeService::class,
            [
                $this->activationCodeLibrary
            ]
        )->makePartial();
        WhatsAppBusiness::spy();
        Bus::fake();
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testGetActivationCodeByIdDatumFound()
    {
        $activationCode = factory(ActivationCode::class)->create();

        $result = $this->activationCodeService->getActivationCodeWithTrashedById($activationCode->id);

        $this->assertNotNull($result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testGetActivationCodeByIdDatumDeleted()
    {
        /** @var ActivationCode */
        $activationCode = factory(ActivationCode::class)->create();
        $activationCode->delete();

        $result = $this->activationCodeService->getActivationCodeWithTrashedById($activationCode->id);

        $this->assertNotNull($result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testGetActivationCodeByIdDatumNotFound()
    {
        $result = $this->activationCodeService->getActivationCodeWithTrashedById(rand());

        $this->assertNull($result);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testUpdateDeliveryReportWithoutPreviousReport()
    {
        $activationCode = factory(ActivationCode::class)->create();
        $activationCodeDeliveryReport = factory(ActivationCodeDeliveryReport::class)->make();

        $result = $this->activationCodeService->updateDeliveryReport($activationCode, $activationCodeDeliveryReport);

        $activationCode->refresh();

        $expectedResult = $activationCodeDeliveryReport->only(self::ASSERTABLE_FIELD_ON_UPDATE);

        $this->assertEquals($expectedResult, $result->only(self::ASSERTABLE_FIELD_ON_UPDATE));
        $this->assertEquals($expectedResult, $activationCode->delivery_report->only(self::ASSERTABLE_FIELD_ON_UPDATE));
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testUpdateDeliveryReportWithPreviousReport()
    {
        $activationCode = factory(ActivationCode::class)->create();
        $activationCode->delivery_report()->create([
            'network' => 'Network',
        ]);
        $activationCodeDeliveryReport = factory(ActivationCodeDeliveryReport::class)->make();

        $result = $this->activationCodeService->updateDeliveryReport($activationCode, $activationCodeDeliveryReport);

        $activationCode->refresh();

        $expectedResult = [
            'network' => $activationCodeDeliveryReport->network,
            'status' => $activationCodeDeliveryReport->status,
            'reason' => $activationCodeDeliveryReport->reason,
            'send_at' => $activationCodeDeliveryReport->send_at,
            'done_at' => $activationCodeDeliveryReport->done_at,
        ];

        $this->assertEquals($expectedResult, $result->only(self::ASSERTABLE_FIELD_ON_UPDATE));
        $this->assertEquals($expectedResult, $activationCode->delivery_report->only(self::ASSERTABLE_FIELD_ON_UPDATE));
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testUpdateDeliveryReportPrevouslyMadeShouldNotBeRepacledWithNullValue()
    {
        $activationCode = factory(ActivationCode::class)->create();
        $activationCode->delivery_report()->create([
            'network' => 'Expected Network',
        ]);
        $activationCodeDeliveryReport = factory(ActivationCodeDeliveryReport::class)->make([
            'network' => null,
        ]);

        $result = $this->activationCodeService->updateDeliveryReport($activationCode, $activationCodeDeliveryReport);

        $activationCode->refresh();

        $expectedResult = [
            'network' => 'Expected Network',
            'status' => $activationCodeDeliveryReport->status,
            'reason' => $activationCodeDeliveryReport->reason,
            'send_at' => $activationCodeDeliveryReport->send_at,
            'done_at' => $activationCodeDeliveryReport->done_at,
        ];

        $this->assertEquals($expectedResult, $result->only(self::ASSERTABLE_FIELD_ON_UPDATE));
        $this->assertEquals($expectedResult, $activationCode->delivery_report->only(self::ASSERTABLE_FIELD_ON_UPDATE));
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testFetchDeliveryReportFoundReport()
    {
        $providerDeliveryReport = $this->getFakeSuccessProviderDeliveryReport();
        $this->activationCodeLibrary->shouldReceive('fetchDeliveryReport')->andReturn($providerDeliveryReport);

        $result = $this->activationCodeService->fetchDeliveryReport(str_random(10));

        $this->assertNotNull($result);
        $this->assertEquals($providerDeliveryReport->status, $result->status);
        $this->assertEquals($providerDeliveryReport->reason, $result->reason);
        $this->assertEquals($providerDeliveryReport->sendAt, $result->send_at);
        $this->assertEquals($providerDeliveryReport->doneAt, $result->done_at);
    }

    /**
     * @group UG
     * @group UG-1204
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testFetchDeliveryReportNullReport()
    {
        $this->expectException(RuntimeException::class);
        $this->activationCodeLibrary->shouldReceive('fetchDeliveryReport')->andReturn(Mockery::mock(ProviderActivationCodeDeliveryReport::class));

        $result = $this->activationCodeService->fetchDeliveryReport(str_random(10));
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendSmsActivationCodeWithSuccessSendSms()
    {
        $providerSendResult = new ProviderSendActivationCodeResult();
        $providerSendResult->success = true;
        $this->mockLibrarySendSmsActivationCode($providerSendResult);

        $request = new SendSmsActivationCodeRequest();
        $request->activationCode = factory(ActivationCode::class)->create();
        $request->message = str_random(10);
        $request->phoneNumber = '0819999999';
        $this->activationCodeService->sendSmsActivationCode($request);

        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendSmsActivationCodeWithFailedAndHasReasonSendSmsResultShouldUpdateDeliveryReportsReason()
    {
        $providerSendResult = new ProviderSendActivationCodeResult();
        $providerSendResult->success = false;
        $providerSendResult->message = $this->faker()->realText();
        $this->mockLibrarySendSmsActivationCode($providerSendResult);

        /** @var ActivationCode $activationCode  */
        $activationCode = factory(ActivationCode::class)->create();

        $request = new SendSmsActivationCodeRequest();
        $request->activationCode = $activationCode;
        $request->message = str_random(10);
        $request->phoneNumber = '0819999999';
        $this->activationCodeService->sendSmsActivationCode($request);

        $this->assertTrue(true);
        $activationCode->refresh();
        $this->assertEquals($providerSendResult->message, $activationCode->delivery_report->reason);
    }

    /**
     * @group UG
     * @group UG-1270
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendSmsActivationCodeWithNetworkSendSmsResultShouldUpdateDeliveryReportsNetwork()
    {
        $providerSendResult = new ProviderSendActivationCodeResult();
        $providerSendResult->network = $this->faker()->company;
        $this->mockLibrarySendSmsActivationCode($providerSendResult);

        /** @var ActivationCode $activationCode  */
        $activationCode = factory(ActivationCode::class)->create();

        $request = new SendSmsActivationCodeRequest();
        $request->activationCode = $activationCode;
        $request->message = str_random(10);
        $request->phoneNumber = '0819999999';
        $this->activationCodeService->sendSmsActivationCode($request);

        $this->assertTrue(true);
        $activationCode->refresh();
        $this->assertEquals($providerSendResult->network, $request->activationCode->delivery_report->network);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testGetActivationCodeNotFound()
    {
        $activationCode = $this->activationCodeService->getActivationCode(
            ActivationCodeType::getRandomInstance(),
            str_random(),
            ActivationCodeVia::getRandomInstance()
        );
        $this->assertNull($activationCode);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testCreateActivationCode()
    {
        $activationCode = $this->activationCodeService->createActivationCode(
            ActivationCodeType::getRandomInstance(),
            str_random(),
            ActivationCodeVia::getRandomInstance()
        );
        $this->assertNotNull($activationCode);
        $this->assertNotNull($activationCode->code);
        $this->assertIsNumeric($activationCode->code);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendActivationCodeViaSms()
    {
        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $this->faker->phoneNumber,
        ]);

        // Should sent the sms through library
        $this->activationCodeLibrary->shouldReceive('sendSmsActivationCode');

        $this->activationCodeService->sendActivationCode($activationCode);

        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendActivationCodeViaNull()
    {
        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => null,
            'destination' => $this->faker->phoneNumber,
        ]);

        // Should sent the sms through library
        $this->activationCodeLibrary->shouldReceive('sendSmsActivationCode');

        $this->activationCodeService->sendActivationCode($activationCode);

        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendActivationCodeViaWhatsapp()
    {
        $activationCode = factory(ActivationCode::class)->make([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::WHATSAPP(),
            'destination' => $this->faker->phoneNumber,
        ]);

        factory(NotificationWhatsappTemplate::class)->states('active', 'otp-wa')->create();

        $this->activationCodeService->sendActivationCode($activationCode);

        Bus::assertDispatched(ProcessNotificationDispatch::class);
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testSendActivationCodeViaWhatsappNoTemplateYet()
    {
        $this->expectException(Exception::class);
        $activationCode = factory(ActivationCode::class)->make([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::WHATSAPP(),
            'destination' => $this->faker->phoneNumber,
        ]);

        $this->activationCodeService->sendActivationCode($activationCode);

        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testGetAllowedResendTime()
    {
        $activationCode = factory(ActivationCode::class)->make([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::WHATSAPP(),
            'destination' => $this->faker->phoneNumber,
        ]);

        $resendTime = $this->activationCodeService->getAllowedResendTime($activationCode);

        $this->assertNotNull($resendTime);
        $this->assertTrue($resendTime->isAfter($activationCode->created_at));
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testResendActivationCodeNotYetAllowed()
    {
        $this->expectException(Exception::class);

        $activationCode = factory(ActivationCode::class)->make([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::WHATSAPP(),
            'destination' => $this->faker->phoneNumber,
        ]);

        // Its only beeen 4 second
        Carbon::setTestNow($activationCode->created_at->addSecond(4));

        $this->activationCodeService->shouldNotReceive('sendActivationCode');

        $this->activationCodeService->resendActivationCode($activationCode);

        $this->assertTrue(false);
    }

    /**
     * @group UG
     * @group UG-4760
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testResendActivationCodeAllowed()
    {
        $activationCode = factory(ActivationCode::class)->make([
            'for' => ActivationCodeType::getRandomInstance(),
            'via' => ActivationCodeVia::SMS(),
            'destination' => $this->faker->phoneNumber,
        ]);

        // Its been 4 hours
        Carbon::setTestNow($activationCode->created_at->addHour(4));

        $this->activationCodeService->shouldReceive('sendActivationCode')->andReturn();

        $this->activationCodeService->resendActivationCode($activationCode);

        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testDeleteLatestActivationCodeReturnTrue()
    {
        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::getRandomInstance()->value,
            'via' => ActivationCodeVia::WHATSAPP()->value,
            'destination' => $this->faker->phoneNumber,
        ]);

        $delete = $this->activationCodeService->deleteLatestActivationCode(
            ActivationCodeType::fromValue($activationCode->for),
            $activationCode->via,
            $activationCode->destination
        );

        $this->assertTrue($delete);
        $this->assertSoftDeleted('user_verification_code', ['id' => $activationCode->id]);
    }

    /**
     * @group UG
     * @group UG-4802
     * @group App\Services\Activity\ActivationCodeService
     */
    public function testDeleteLatestActivationCodeRetrunFalse()
    {
        $activationCode = factory(ActivationCode::class)->create([
            'for' => ActivationCodeType::getRandomInstance()->value,
            'via' => ActivationCodeVia::WHATSAPP()->value,
            'destination' => $this->faker->phoneNumber,
        ]);

        $delete = $this->activationCodeService->deleteLatestActivationCode(
            ActivationCodeType::getRandomInstance(),
            ActivationCodeVia::SMS(),
            $activationCode->destination
        );

        $this->assertFalse($delete);
        $this->assertDatabaseHas(
            'user_verification_code', 
            [
                'id' => $activationCode->id, 
                'deleted_at' => null
            ]
        );
    }

    private function mockLibrarySendSmsActivationCode(ProviderSendActivationCodeResult $result): void
    {
        $this->activationCodeLibrary->shouldReceive('sendSmsActivationCode')->andReturn($result);
    }

    private function getFakeSuccessProviderDeliveryReport()
    {
        $providerDeliveryReport = new ProviderActivationCodeDeliveryReport();
        $providerDeliveryReport->success = true;
        $providerDeliveryReport->status = $this->faker()->randomElement(ActivationCodeDeliveryReportStatus::getValues());
        $providerDeliveryReport->reason = $this->faker()->realText();
        $providerDeliveryReport->doneAt = Carbon::now()->addDay($this->faker()->numberBetween(1, 3));
        $providerDeliveryReport->sendAt = Carbon::now()->addDay($this->faker()->numberBetween(1, 3));
        return $providerDeliveryReport;
    }
}
