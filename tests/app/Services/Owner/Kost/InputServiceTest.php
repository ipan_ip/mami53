<?php

namespace App\Services\Owner\Kost;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Media\Media;
use App\Entities\Property\Property;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Exceptions\Owner\ResourceNotFoundException;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Jobs\Owner\PropertyUpdateTracker;
use App\Jobs\Room\MarkReadyToVerify;
use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Queue\NullQueue;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class InputServiceTest extends MamiKosTestCase
{
    private $user;
    private $service;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->state('owner')->create();
        $this->service = app()->make(InputService::class);
    }

    /**
     * @group PMS-495
     */    
    public function testGetDataNotFoundThrowException()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service->getData(888, $this->user);
    }

    /**
     * @group PMS-495
     */    
    public function testGetDataNotOwnedThrowException()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => 888
        ]);

        $this->expectException(ResourceNotOwnedException::class);
        $this->service->getData($room->song_id, $this->user);
    }

    /**
     * @group PMS-495
     */    
    public function testGetDataOwnedSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        $roomFromService = $this->service->getData($room->song_id, $this->user);

        $this->assertSame($roomFromService->id, $room->id);
    }

    /**
     * @group PMS-495
     */    
    public function testCreateKostSuccess()
    {
        $alamatKos = [
            'address' => 'Jl. Test adalah ninjaku',
            'longitude' => 888,
            'latitude' => 888,
            'area_city' => 'East Blue'
        ];
        $this->assertDatabaseMissing((new Room())->getTable(), $alamatKos);
        $room = $this->service->createKost(
            $alamatKos,
            $this->user
        );
        $this->assertDatabaseHas((new Room())->getTable(), $alamatKos);
        $this->assertDatabaseHas(
            (new RoomOwner())->getTable(),
            [
                'designer_id' => $room->id,
                'user_id' => $this->user->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testCreateKostOverloadSaveSuccess()
    {
        $roomTable = (new Room())->getTable();

        $mockRoom = $this->mockPartialAlternatively(Room::class);
        $mockRoom->shouldReceive('generateSongId')->andSet('exists', false)->andReturn(null);

        $alamatKos = [
            'address' => 'Jl. Test adalah ninjaku',
            'longitude' => 888,
            'latitude' => 888,
            'area_city' => 'East Blue'
        ];
        $this->assertDatabaseMissing($roomTable, $alamatKos);
        $room = $this->service->createKost(
            $alamatKos,
            $this->user
        );
        $this->assertDatabaseHas($roomTable, $alamatKos);
        $this->assertDatabaseHas(
            (new RoomOwner())->getTable(),
            [
                'designer_id' => $room->id,
                'user_id' => $this->user->id
            ]
        );
    }

    /**
     * @group PMS-495
     */    
    public function testUpdateKost()
    {
        $room = factory(Room::class)->create(['address' => 'Jl. Original Test']);
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);
        $targetAddress = ['address' => 'Jl. Target Test'];

        Queue::fake();
        $this->assertDatabaseMissing((new Room())->getTable(), $targetAddress);
        $room = $this->service->updateKost($room, $targetAddress);
        $this->assertDatabaseHas((new Room())->getTable(), $targetAddress);
        Queue::assertPushed(
            PropertyUpdateTracker::class,
            function ($job) use ($room) {
                return $job->roomId === $room->id;
            }
        );
    }

    /**
     * @group PMS-495
     */    
    public function testSaveAddressNote()
    {
        $room = factory(Room::class)->create();

        $province = factory(Province::class)->create(['name' => 'Jawa Timur']);
        $city = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kota Malang']);
        $subdistrict = factory(Subdistrict::class)->create(['area_city_id' => $city->id, 'name' => 'Lowokwaru']);

        $note = $this->service->saveAddressNote($room->id, $province->name, $city->name, $subdistrict->name, "Dekat wartel");

        $room->load('address_note');

        $this->assertSame($note->id, $room->address_note->id);
        $this->assertSame($province->id, $note->area_province_id);
        $this->assertSame($city->id, $note->area_city_id);
        $this->assertSame($subdistrict->id, $note->area_subdistrict_id);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveAddressNoteOnExistingRoomWithNote()
    {
        $room = factory(Room::class)->state('with-address-note')->create();

        $province = factory(Province::class)->create(['name' => 'Jawa Timur']);
        $city = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kota Malang']);
        $subdistrict = factory(Subdistrict::class)->create(['area_city_id' => $city->id, 'name' => 'Lowokwaru']);

        $room->load('address_note');
        $originNote = $room->address_note->note;
        $originNoteId = $room->address_note->id;
        $note = $this->service->saveAddressNote(
            $room->id,
            $province->name,
            $city->name,
            $subdistrict->name,
            "Seberang SPLU belok kiri"
        );

        $room->load('address_note');
        $this->assertNotEquals($originNote, $room->address_note->note);
        $this->assertEquals($originNoteId, $room->address_note->id);
        $this->assertSame($note->id, $room->address_note->id);
        $this->assertEquals("Seberang SPLU belok kiri", $room->address_note->note);
        $this->assertSame($province->id, $note->area_province_id);
        $this->assertSame($city->id, $note->area_city_id);
        $this->assertSame($subdistrict->id, $note->area_subdistrict_id);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveAddressNoteOnMultipleKabupatenKotaCase()
    {
        $province = factory(Province::class)->create(['name' => 'Jawa Timur']);
        $city1 = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kota Malang']);
        $subdistrict1 = factory(Subdistrict::class)->create(['area_city_id' => $city1->id, 'name' => 'Lowokwaru']);
        $city2 = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kabupaten Malang']);
        $subdistrict2 = factory(Subdistrict::class)->create(['area_city_id' => $city2->id, 'name' => 'Turen']);

        $note1 = $this->service->saveAddressNote(18, $province->name, 'Malang', 'Lowokwaru', "Dekat wartel");
        $note2 = $this->service->saveAddressNote(19, $province->name, 'Malang', 'Turen', "Sebrang SPLU");

        
        $this->assertSame($province->id, $note1->area_province_id);
        
        $this->assertSame($city1->id, $note1->area_city_id);
        $this->assertSame($city2->id, $note2->area_city_id);
        
        $this->assertSame($subdistrict1->id, $note1->area_subdistrict_id);
        $this->assertSame($subdistrict2->id, $note2->area_subdistrict_id);

        $this->assertNotEquals($note1->area_city_id, $note2->area_city_id);
        $this->assertNotEquals($note1->area_subdistrict_id, $note2->area_subdistrict_id);
    }

    /**
     * @group PMS-495
     */    
    public function testGetCampaignSourceValueFromReferer()
    {
        $request =  Request::createFromBase(
            SymfonyRequest::create(
                '/',
                'POST',
                [
                    'city' => 'Kota',
                    'address' => "Alamat dengan Emoticon🤤🤤🤤",
                    'longitude' => 89,
                    'latitude' => 98,
                    'subdistrict' => 'kecamatan',
                ],
                [],
                [],
                [
                    'HTTP_REFERER' => 'https://test.mamikos.com/room?utm_source=facebook'
                ]
            )
        );

        $result = $this->service->getCampaignSourceValue($request);

        $this->assertSame('facebook', $result);
    }

    /**
     * @group PMS-495
     */    
    public function testGetCampaignSourceValueFromCookie()
    {
        $request =  Request::createFromBase(
            SymfonyRequest::create(
                '/',
                'POST',
                [
                    'city' => 'Kota',
                    'address' => "Alamat dengan Emoticon🤤🤤🤤",
                    'longitude' => 89,
                    'latitude' => 98,
                    'subdistrict' => 'kecamatan',
                ],
                [
                    'campaign_source' => 'google'
                ]
            )
        );

        $result = $this->service->getCampaignSourceValue($request);

        $this->assertSame('google', $result);
    }

    /**
     * @group PMS-495
     */    
    public function testGetCampaignSourceNoValue()
    {
        $request =  Request::createFromBase(
            SymfonyRequest::create(
                '/',
                'POST',
                [
                    'city' => 'Kota',
                    'address' => "Alamat dengan Emoticon🤤🤤🤤",
                    'longitude' => 89,
                    'latitude' => 98,
                    'subdistrict' => 'kecamatan',
                ]
            )
        );

        $result = $this->service->getCampaignSourceValue($request);

        $this->assertSame('', $result);
    }

    /**
     * @group PMS-495
     */    
    public function testFilterOldCardsByGroup()
    {
        $types = collect([
            Group::BUILDING_FRONT,
            Group::INSIDE_ROOM,
            Group::BATHROOM,
            Group::OTHER,
        ]);

        $expectation = $types->mapWithKeys(function ($type) {
            return [
                $type => []
            ];
        })->toArray();

        factory(Card::class, 25)->make([
            'designer_id' => 1
        ])->each(function ($card, $key) use ($types, &$expectation) {
            $type = $types->random();
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->save();

            $expectation[$type][] = $card;
        });

        $cards = Card::all();

        $types->each(function ($type) use ($cards, $expectation) {
            $result = $this->service->filterCardsByGroup($cards, $type);
            $this->assertEquals(
                count($expectation[$type]),
                $result->count()
            );
            $this->assertEquals(
                collect($expectation[$type])->pluck('id')->sort()->toArray(),
                $result->pluck('id')->sort()->toArray()
            );
        });

        $this->expectException(Exception::class);
        $this->service->filterCardsByGroup($cards, 'No-Exist Group');
    }

    /**
     * @group PMS-495
     */    
    public function testFilterNewCardsByGroup()
    {
        $types = collect([
            Group::BUILDING_FRONT,
            Group::INSIDE_BUILDING,
            Group::ROADVIEW_BUILDING,
            Group::ROOM_FRONT,
            Group::INSIDE_ROOM,
            Group::BATHROOM,
            Group::OTHER,
        ]);

        $expectation = $types->mapWithKeys(function ($type) {
            return [
                $type => []
            ];
        })->toArray();

        factory(Card::class, 25)->make([
            'designer_id' => 1
        ])->each(function ($card, $key) use ($types, &$expectation) {
            $type = $types->random();
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->group = $type;
            $card->save();

            $expectation[$type][] = $card;
        });

        $cards = Card::all();

        $types->each(function ($type) use ($cards, $expectation) {
            $result = $this->service->filterCardsByGroup($cards, $type);
            $this->assertEquals(
                count($expectation[$type]),
                $result->count()
            );
            $this->assertEquals(
                collect($expectation[$type])->pluck('id')->sort()->toArray(),
                $result->pluck('id')->sort()->toArray()
            );
        });

        $this->expectException(Exception::class);
        $this->service->filterCardsByGroup($cards, 'No-Exist Group');
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCardsNewEntry()
    {
        $room = factory(Room::class)->states([
            'active',
            'attribute-complete-state',
            'with-address',
            'with-small-rooms'
        ])->create();

        $this->service->saveCards(
            $room,
            [1,2,3,4],
            Group::INSIDE_BUILDING
        );

        $room = Room::with('cards')->find($room->id);

        $this->assertEquals(4, $room->cards->count());
        $this->assertContains(
            Card::DESCRIPTION_PREFIX[Group::INSIDE_BUILDING],
            $room->cards[0]->description
        );
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCoverCardsNoOldCoverSuccess()
    {
        $room = factory(Room::class)->create([
            'photo_id' => 123
        ]);

        $this->service->saveCards(
            $room,
            [123],
            Group::INSIDE_ROOM
        );
        $this->service->setCoverPhoto($room, $room->photo_id);

        $room = Room::with('cards')->find($room->id);

        $this->assertEquals(1, $room->cards->count());
        $this->assertContains(Card::DESCRIPTION_PREFIX['cover'], $room->cards[0]->description);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCoverCardsHaveOldCoverChangedSuccessfully()
    {
        $room = factory(Room::class)->create();

        $card = factory(Card::class)->create([
            'designer_id' => $room->id,
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM,
            'description' => 'dalam-kamar-cover',
            'photo_id' => 345
        ]);

        $room->photo_id = $card->photo_id;
        $room->save();

        $this->service->saveCards(
            $room,
            [123],
            Group::INSIDE_ROOM
        );
        $this->service->setCoverPhoto($room, 123);

        $room = Room::with('cards')->find($room->id);

        $this->assertEquals(1, $room->cards->count());
        $this->assertContains(Card::DESCRIPTION_PREFIX['cover'], $room->cards[0]->description);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCardsRemoveCards()
    {
        $room = factory(Room::class)->create();
        $types = collect([
            Group::INSIDE_BUILDING,
            Group::INSIDE_ROOM,
            Group::BATHROOM,
            Group::OTHER,
        ]);

        $expectation = [];
        $photoId = 1;

        factory(Card::class, 4)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$expectation, &$photoId) {
            $type = $types[$key];
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $expectation[$type][] = $card->photo_id;
        });
        factory(Card::class, 4)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$expectation, &$photoId) {
            $type = $types[$key];
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $expectation[$type][] = $card->photo_id;
        });

        factory(Card::class, 20)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$expectation, &$photoId) {
            $type = $types->random();
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $expectation[$type][] = $card->photo_id;
        });

        $room->load('cards');

        $types->each(function ($type) use ($room, $expectation) {

            [$expected, $removed] = collect($expectation[$type])->shuffle()->split(2)->toArray();

            $room = $this->service->saveCards(
                $room,
                $expected,
                $type
            );

            $currentResult = $this->service->filterCardsByGroup($room->cards, $type);

            $this->assertNotEquals(
                count($expectation[$type]),
                $currentResult->count(),
                "$type cards is not equals to original count"
            );
            $this->assertEquals(
                count($expected),
                $currentResult->count(),
                "$type cards is equals to expected count"
            );
            $this->assertEmpty(
                array_intersect(
                    $currentResult->pluck('photo_id')->toArray(),
                    $removed
                ),
                "$type removed cards is not exist on current result"
            );
        });
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCardsOnRoomWithoutCards()
    {
        $room = factory(Room::class)->create();
        $room = $this->service->saveCards(
            $room,
            [],
            Group::INSIDE_ROOM
        );

        $room->load('cards');
        $this->assertTrue($room->cards->isEmpty());
    }

    /**
     * @group PMS-495
     */    
    public function testSaveCardsMixAddAndRemoveCards()
    {
        $room = factory(Room::class)->create();
        $types = collect([
            Group::INSIDE_BUILDING,
            Group::INSIDE_ROOM,
            Group::BATHROOM,
            Group::OTHER,
        ]);

        $currentCondition = [];
        $photoId = 1;

        factory(Card::class, 4)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$currentCondition, &$photoId) {
            $type = $types[$key];
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $currentCondition[$type][] = $card->photo_id;
        });

        factory(Card::class, 4)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$currentCondition, &$photoId) {
            $type = $types[$key];
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $currentCondition[$type][] = $card->photo_id;
        });

        factory(Card::class, 12)->make([
            'designer_id' => $room->id
        ])->each(function ($card, $key) use ($types, &$currentCondition, &$photoId) {
            $type = $types->random();
            $card->description = Card::DESCRIPTION_PREFIX[$type] . '-' . $key;
            $card->photo_id = $photoId++;
            $card->save();

            $currentCondition[$type][] = $card->photo_id;
        });

        $sourceArray = range($photoId+1, $photoId + 14);
        shuffle($sourceArray);
        [
            $add[Group::INSIDE_BUILDING],
            $add[Group::INSIDE_ROOM],
            $add[Group::BATHROOM],
            $add[Group::OTHER]
        ] = array_chunk($sourceArray, 4);

        $types->each(function ($type) use ($room, $currentCondition, $add) {
            [$expected, $removed] = collect($currentCondition[$type])->shuffle()->split(2)->toArray();
            $expected = array_merge($expected, $add[$type]);

            $room = $this->service->saveCards(
                $room,
                $expected,
                $type
            );
            $result = $this->service
                ->filterCardsByGroup($room->cards, $type)
                ->pluck('photo_id')
                ->sort()
                ->toArray();

            $this->assertEquals(
                count($expected),
                count($result),
                $type . ' amount correct'
            );
            $this->assertEmpty(
                array_diff($expected, $result),
                $type . ' id MATCH'
            );

            $this->assertEmpty(
                array_intersect(
                    $result,
                    $removed
                ),
                "$type removed cards is not exist on current result"
            );
        });
    }

    /**
     * @group PMS-495
     */    
    public function testSyncRulePhotosAddPhoto()
    {
        $room = factory(Room::class)->create();
        $medias = factory(Media::class, 2)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[0]->id
        ]);

        $result = $this->service->syncRoomTermPhotos(
            [
                ['id' => $medias[0]->id],
                ['id' => $medias[1]->id]
            ],
            $room
        );

        $this->assertEquals($result->room_term->room_term_document[0]->media_id, $medias[0]->id);
        $this->assertEquals($result->room_term->room_term_document[1]->media_id, $medias[1]->id);
    }

    /**
     * @group PMS-495
     */    
    public function testSyncRulePhotosReplacePhoto()
    {
        $room = factory(Room::class)->create();
        $medias = factory(Media::class, 2)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[0]->id
        ]);

        $result = $this->service->syncRoomTermPhotos(
            [
                ['id' => $medias[1]->id]
            ],
            $room
        );

        $this->assertEquals($result->room_term->room_term_document[0]->media_id, $medias[1]->id);
    }

    /**
     * @group PMS-495
     */    
    public function testSyncRulePhotosRemovePhoto()
    {
        $room = factory(Room::class)->create();
        $medias = factory(Media::class, 2)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[0]->id
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => $medias[1]->id
        ]);

        $result = $this->service->syncRoomTermPhotos([], $room);

        $this->assertEmpty($result->room_term->room_term_document);
    }

    /**
     * @group PMS-495
     */    
    public function testSyncFacilitiesAddSuccess()
    {
        $room = factory(Room::class)->create();

        $tags = $this->prepareTags();

        $room = $this->service->syncFacilities($room, $tags->pluck('id')->toArray());
        $room->load('tags.types');

        $this->assertEquals(6, $room->tags->count());
        $this->assertEquals(6, RoomTag::where('designer_id', $room->id)->count());
    }

    /**
     * @group PMS-495
     */    
    public function testSyncFacilitiesRemoveSuccess()
    {
        $room = factory(Room::class)->create();
        $tags = $this->prepareTags();
        $room->tags()->attach($tags->pluck('id')->toArray());

        $room = $this->service->syncFacilities($room, []);
        $room->load('tags.types');

        $this->assertEquals(0, $room->tags->count());
        $this->assertEquals(0, RoomTag::where('designer_id', $room->id)->count());
    }

    /**
     * @group PMS-495
     */
    public function testSyncFacilitiesMixedSuccess()
    {
        $room = factory(Room::class)->create();
        $tags = $this->prepareTags();
        $room->tags()->attach($tags->pluck('id')->toArray());

        $newTags = $this->prepareTags(['Dispenser', 'Kulkas']);

        $room = $this->service->syncFacilities($room, $newTags->concat($tags->random(2))->pluck('id')->toArray());
        $room->load('tags.types');

        $this->assertEquals(4, $room->tags->count());
        $this->assertEquals(4, RoomTag::where('designer_id', $room->id)->count());
    }

    /**
     * @group PMS-495
     */    
    public function testSubmitFinalDataSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $this->user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_EARLY_EDIT_STATUS,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ]);

        $this->service->submitFinalData($room, $this->user->id);

        $room = Room::with(['owners', 'booking_owner_requests'])->find($room->id);

        $this->assertEquals(RoomOwner::ROOM_ADD_STATUS, $room->owners->first()->status);
        $this->assertEquals(BookingOwnerRequest::BOOKING_WAITING, $room->booking_owner_requests->first()->status);
    }

    /**
     * @group PMS-495
     */
    public function testUpdateKostStatusVerifiedSuccessMarkReady()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        Queue::fake();
        $room = $this->service->updateKost($room, []);
        Queue::assertPushed(MarkReadyToVerify::class);
    }

    /**
     * @group PMS-495
     */
    public function testUpdateKostStatusEditedSuccessMarkReady()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id,
            'status' => RoomOwner::ROOM_EDIT_STATUS
        ]);

        Queue::fake();
        $room = $this->service->updateKost($room, []);
        Queue::assertPushed(MarkReadyToVerify::class);
    }

    /**
     * @group PMS-495
     */
    public function testUpdateKostStatusUnverifiedSuccessMarkReady()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id,
            'status' => RoomOwner::ROOM_UNVERIFY_STATUS
        ]);

        Queue::fake();
        $room = $this->service->updateKost($room, []);
        Queue::assertPushed(MarkReadyToVerify::class);
    }

    /**
     * @group PMS-495
     */
    public function testUpdateKostStatusDraftNotMarkReady()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id,
            'status' => RoomOwner::ROOM_EARLY_EDIT_STATUS
        ]);

        Queue::fake();
        $room = $this->service->updateKost($room, []);
        Queue::assertNotPushed(MarkReadyToVerify::class);
    }

    /**
     * @group PMS-495
     */
    public function testGetNamePatternPropertyWithoutTypeNameSuccess()
    {
        $property = factory(Property::class)->create([
            'name' => 'Kost Epic',
            'has_multiple_type' => true
        ]);
        $expected = 'Kost Epic Jemursari Surabaya';
        $this->assertEquals(
            $expected,
            $this->service->getNamePattern(
                $property,
                null,
                'Jemursari',
                'Surabaya'
            )
        );

        $this->assertEquals(
            $expected,
            $this->service->getNamePattern(
                $property,
                '',
                'Jemursari',
                'Surabaya'
            )
        );
    }

    /**
    * @group PMS-495
    */
    public function testUnsetCoverWithGroupIsNull()
    {
        $card = factory(Card::class)->create([
            'is_cover' => true,
            'group' => null,
            'description' => 'dalam-kamar-cover',
        ]);
        $reflection = new \ReflectionClass(get_class($this->service));
        $method = $reflection->getMethod('unsetCover');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->service, [$card]);
        $this->assertFalse($result->is_cover);
        $this->assertEquals($result->description, 'dalam-kamar');
    }

    /**
    * @group PMS-495
    */
    public function testUnsetCoverWithGroupIsNotNull()
    {
        $card = factory(Card::class)->create([
            'is_cover' => true,
            'group' => Group::INSIDE_ROOM,
            'description' => 'dalam-kamar-cover',
        ]);
        $reflection = new \ReflectionClass(get_class($this->service));
        $method = $reflection->getMethod('unsetCover');
        $method->setAccessible(true);
        $result = $method->invokeArgs($this->service, [$card]);
        $this->assertFalse($result->is_cover);
        $this->assertEquals($result->description, 'dalam-kamar');
    }

    /**
    * @group PMS-495
    */
    public function testHandlePropertyDataNullNotCreatingAnything()
    {
        $this->assertNull(
            $this->service->handleProperty(
                null,
                $this->user
            )
        );
    }

    /**
    * @group PMS-495
    */
    public function testHandlePropertyResourceNotFoundThrowResourceNotFoundException()
    {
        $this->expectException(ResourceNotFoundException::class);
        $this->service->handleProperty(
            [
                'id' => 'abc'
            ],
            $this->user
        );
    }

    /**
    * @group PMS-495
    */
    public function testHandlePropertyResourceNotOwnedThrowResourceNotOwnedException()
    {
        $this->expectException(ResourceNotOwnedException::class);
        $property = factory(Property::class)->create([
            'name' => 'Kost Epic',
            'has_multiple_type' => true,
            'owner_user_id' => $this->user->id + 88
        ]);
        
        $this->service->handleProperty(
            [
                'id' => $property->id,
                'name' => $property->name,
                'has_multiple_type' => $property->has_multiple_type
            ],
            $this->user
        );
    }

    /**
    * @group PMS-495
    */
    public function testHandlePropertyChangeMultipleTypeFromFalse()
    {
        $property = factory(Property::class)->create([
            'name' => 'Kost Epic',
            'has_multiple_type' => false,
            'owner_user_id' => $this->user->id
        ]);
        
        $resultProperty = $this->service->handleProperty(
            [
                'id' => $property->id,
                'name' => $property->name . ' Edited',
                'allow_multiple' => true
            ],
            $this->user
        );

        $this->assertEquals(
            $property->name . ' Edited',
            $resultProperty->name
        );

        $this->assertTrue($resultProperty->has_multiple_type);
    }

    private function prepareTags(?array $items = null) {
        return collect(
            $items ??
            [
                'AC',
                'Lemari',
                'Meja',
                'Kloset',
                'Air Panas',
                'Kursi'
            ]
        )->map(function ($name) {
            $tag = factory(Tag::class)->create([
                'name' => $name
            ]);

            factory(TagType::class)->create([
                'tag_id' => $tag->id,
                'name' => 'fac_room'
            ]);

            return $tag;
        });
    }

}
