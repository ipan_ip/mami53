<?php

namespace App\Services\Owner\Kost;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;

class PriceServiceTest extends MamiKosTestCase
{
    use WithFaker;

    private $user;
    private $service;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->state('owner')->create();
        $this->service = app()->make(PriceService::class);
        $this->setUpFaker();
    }

    /**
     * @group PMS-495
     */    
    public function testSaveAdditionalCostData()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        $additionalCostData = [
            [
                'name' => 'Iuran Sampah',
                'price' => 10000,
                'is_active' => true
            ]
        ];

        $this->service->saveAdditionalCostData($room, $additionalCostData);
        $room->refresh();
        $this->assertEquals(10000, $room->mamipay_price_components[0]->price);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveDepositFeeData()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        $depositFeeData = [
            'price' => 50000,
            'is_active' => true
        ];

        $this->service->savePriceComponent($room, $depositFeeData, MamipayRoomPriceComponentType::DEPOSIT);
        $room->refresh();
        $this->assertEquals(50000, $room->mamipay_price_components[0]->price);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveDownPaymentData()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        $downPaymentData = [
            'percentage' => 20,
            'is_active' => true
        ];

        $this->service->savePriceComponent($room, $downPaymentData, MamipayRoomPriceComponentType::DP);
        $room->refresh();
        $this->assertEquals(20, $room->mamipay_price_components[0]->price_component_additionals[0]->value);
    }

    /**
     * @group PMS-495
     */    
    public function testSaveFineData()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        $fineData = [
            'price' => 30000,
            'duration_type' => 'day',
            'length' => 2,
            'is_active' => true
        ];

        $this->service->savePriceComponent($room, $fineData, MamipayRoomPriceComponentType::FINE);
        $room->refresh();
        $this->assertEquals(30000, $room->mamipay_price_components[0]->price);
    }

    /**
     * @group PMS-495
     */   
    public function testSavePriceComponentWithPriceComponentIsExistAndPriceComponentDataIsNull()
    {
        $room = factory(Room::class)->create();
        $component = $this->faker->randomElement(['additional', 'fine', 'deposit', 'dp']);
        factory(MamipayRoomPriceComponent::class)->create([
            'designer_id' => $room->id,
            'component' => $component,
        ]);
        $result = $this->service->savePriceComponent($room, null, $component);
        $this->assertNull($result);
    }
}
