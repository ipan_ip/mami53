<?php

namespace App\Services\Owner\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RuntimeException;

class DataStageServiceTest extends MamiKosTestCase
{
    use DatabaseTransactions;

    private $service;

    protected function setUp()
    {
        parent::setUp();
        $this->service = app()->make(DataStageService::class);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesNullRoomDefaultReturn()
    {
        $stages = $this->service->getInputStages(null, null);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $stages->each(function (DataStage $stage) {
            $this->assertNull($stage->id);
        });
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesUnsavedRoomDefaultReturn()
    {
        $stages = $this->service->getInputStages(
            factory(Room::class)->make(),
            null
        );

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $stages->each(function (DataStage $stage) {
            $this->assertNull($stage->id);
        });
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesNoDataStagesSuccess()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id
            ],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::INSIDE_ROOM,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER
            ]
        );
        
        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());

        $stages->each(function (DataStage $stage) {
            $this->assertTrue($stage->complete);
        });
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesFullSuccess()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id
            ],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::INSIDE_ROOM,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER
            ]
        );

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        collect(DataStage::AVAILABLE_STAGES)->map(function ($stageName) use ($room) {
            factory(DataStage::class)->state('complete')->create([
                'designer_id' => $room->id,
                'name' => $stageName
            ]);
        });

        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());

        $stages->each(function (DataStage $stage) {
            $this->assertNotNull($stage->id);
            $this->assertTrue($stage->complete);
        });
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesPartialStagesSuccess()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'active',
                'availableBooking',
                'notTesting',
                'with-unique-code',
            ],
            [
                'photo_id' => $media->id
            ],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::INSIDE_ROOM,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER
            ]
        );
        $completeStages = collect([
            DataStage::STAGE_DATA,
            DataStage::STAGE_KOS_PHOTO,
            DataStage::STAGE_ROOM_PHOTO,
            DataStage::STAGE_ROOM_ALLOTMENT
        ]);
        $completeStages->map(function ($stageName) use ($room) {
            return factory(DataStage::class)->state('complete')->create([
                'designer_id' => $room->id,
                'name' => $stageName
            ]);
        });
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $stages->each(function (DataStage $stage) use ($completeStages) {
            if (in_array($stage->name, $completeStages->toArray())) {
                $this->assertNotNull($stage->id, "{$stage->name} is saved");
                $this->assertTrue($stage->complete, "{$stage->name} is complete");
            } else {
                $this->assertFalse($stage->complete, "{$stage->name} is not complete");
            }
        });
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesOnlyAddressState()
    {
        $room = factory(Room::class)->states(['with-address', 'with-address-note'])->create();

        $stages = $this->service->getInputStages($room);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_ADDRESS]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesAddressWithoutProvinceFail()
    {
        $room = factory(Room::class)->states(['with-address'])->create();
        $stages = $this->service->getInputStages($room);
        $this->assertFalse(
            $stages[DataStage::STAGE_ADDRESS]->complete,
            "no province id, old data case"
        );
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithKosPhotoState()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
            ],
            [
                'photo_id' => $media->id,
                'photo_round_id' => $media->id,
            ],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING
            ],
            3
        );

        $stages = $this->service->getInputStages($room);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_KOS_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomPhotoStateNoCoverPhoto()
    {
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
            ],
            [],
            [
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM
            ],
            2
        );
        $stages = $this->service->getInputStages($room);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertFalse($stages[DataStage::STAGE_ROOM_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithKosPhotoStateNoRoadviewPhoto()
    {
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
            ],
            [],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
            ],
            2
        );
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertFalse($stages[DataStage::STAGE_KOS_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomPhotoStateTrue()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id,
            ],
            [
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::BATHROOM,
            ]
        );
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_ROOM_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomPhotoStateNotHavingRoomFrontPhotosFalse()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id,
            ],
            [
                Group::INSIDE_ROOM,
                Group::BATHROOM,
                Group::OTHER
            ]
        );
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertFalse($stages[DataStage::STAGE_ROOM_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomPhotoStateNotHavingBathroomPhotosTrue()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id,
            ],
            [
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::OTHER
            ]
        );
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_ROOM_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomPhotoStateNotHavingOtherPhotosTrue()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id,
            ],
            [
                Group::ROOM_FRONT,
                Group::INSIDE_ROOM,
                Group::BATHROOM
            ]
        );
        $stages = $this->service->getInputStages($room);
        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_ROOM_PHOTO]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesNoPriceData()
    {
        $media = factory(Media::class)->create();

        $room = factory(Room::class)->states([
            'with-address',
            'with-owner-collumn',
            'with-all-prices',
            'with-address-note'
        ])->create([
            'photo_id' => $media->id,
            'photo_round_id' => $media->id,
        ]);

        $stages = $this->service->getInputStages($room);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_PRICE]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testGetInputStagesWithRoomAllotment()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'with-address',
                'with-owner-collumn',
                'with-all-prices',
                'with-room-unit',
                'with-address-note'
            ],
            [
                'photo_id' => $media->id,
                'photo_round_id' => $media->id,
            ],
            [Group::INSIDE_BUILDING, Group::INSIDE_ROOM, Group::BATHROOM, Group::OTHER],
            4
        );
        $stages = $this->service->getInputStages($room);

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $stages->count());
        $this->assertTrue($stages[DataStage::STAGE_PRICE]->complete);
    }

    /**
     * @group PMS-495
     */    
    public function testRegisterDataStageStepNotDefinedThrowException()
    {
        $this->expectException(RuntimeException::class);
        $this->service->registerDataStage(
            factory(Room::class)->make(),
            "definitely-not-exist-stage"
        );
    }

    /**
     * @group PMS-495
     */    
    public function testRegisterDataStageStageExistReturnStatus()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
            ],
            [
                'photo_id' => $media->id
            ],
            [Group::INSIDE_BUILDING, Group::INSIDE_ROOM, Group::BATHROOM, Group::OTHER]
        );

        $stage = factory(DataStage::class)->create([
            'designer_id' => $room->id,
            'name' => DataStage::STAGE_PRICE,
            'complete' => false
        ]);

        $room->load('data_stage');
        
        $this->assertNotNull($room->data_stage->where('name', DataStage::STAGE_PRICE));

        $result = $this->service->registerDataStage(
            $room,
            DataStage::STAGE_PRICE
        );

        $this->assertEquals(DataStage::STAGE_PRICE, $result->name);
        $this->assertTrue($result->complete);
        $this->assertEquals($stage->id, $result->id);
    }

    /**
     * @group PMS-495
     */    
    public function testRegisterDataStageStageNotExistReturnStatus()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
            ],
            [
                'photo_id' => $media->id
            ],
            [Group::INSIDE_BUILDING, Group::INSIDE_ROOM, Group::BATHROOM, Group::OTHER]
        );

        $room->load('data_stage');
        
        $this->assertNull($room->data_stage->first(function ($stage) {
            return $stage->name === DataStage::STAGE_PRICE;
        }));

        $result = $this->service->registerDataStage(
            $room,
            DataStage::STAGE_PRICE
        );

        $this->assertEquals(DataStage::STAGE_PRICE, $result->name);
        $this->assertTrue($result->complete);

        $room->load('data_stage');
        $this->assertNotNull($room->data_stage->where('name', DataStage::STAGE_PRICE));
    }

    /**
     * @group PMS-495
     */    
    public function testIsCompleteFalse()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
            ],
            [
                'photo_id' => $media->id
            ],
            [
                Group::OTHER
            ]
        );

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->assertFalse($this->service->isComplete($room));
    }

    /**
     * @group PMS-495
     */    
    public function testIsCompleteTrue()
    {
        $media = factory(Media::class)->create();
        $room = $this->prepareRoom(
            [
                'attribute-complete-state',
                'active',
                'availableBooking',
                'notTesting',
                'with-all-prices',
                'with-small-rooms',
                'with-unique-code',
            ],
            [
                'photo_id' => $media->id
            ],
            [
                Group::BUILDING_FRONT,
                Group::INSIDE_BUILDING,
                Group::ROADVIEW_BUILDING,
                Group::INSIDE_ROOM,
                Group::ROOM_FRONT,
                Group::BATHROOM,
                Group::OTHER
            ]
        );

        $tags = $this->prepareTags();
        $room->tags()->sync(collect(array_flatten($tags))->pluck('id'));

        $this->assertTrue($this->service->isComplete($room));
    }

    private function prepareRoom(
        array $roomStates,
        array $roomAttribute,
        array $types,
        int $mediaAmmount = 12
    ) {
        $room = factory(Room::class)->states($roomStates)->create($roomAttribute);
        $types = collect($types);
        factory(Media::class, $mediaAmmount)
            ->create()
            ->split($types->count())
            ->mapWithKeys(
                function ($medias) use ($types, $room) {
                    $type = $types->shift();
                    return [
                        $type => $medias->each(function ($media, $key) use ($room, $type) {
                            factory(Card::class)->create([
                                'photo_id' => $media->id,
                                'designer_id' => $room->id,
                                'description' => Card::DESCRIPTION_PREFIX[$type] . '-' . $key,
                                'group' => $type,
                            ]);
                        })
                    ];
                }
            );

        return $room;
    }

    private function prepareTags(
        ?array $source = null
    ): array {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }
}
