<?php

namespace App\Services\Owner\Kost;

use App\Entities\Room\Element\Tag;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Room\Element\Tag\GroupType;
use App\Entities\Room\Element\TagType;
use Illuminate\Foundation\Testing\WithFaker;

class FacilitiesServiceTest extends MamiKosTestCase
{
    use WithFaker;

    private $user;
    private $service;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->state('owner')->create();
        $this->service = app()->make(FacilitiesService::class);
        $this->setUpFaker();
    }

    /**
    * @group PMS-495
    */    
    public function testGroupedFacilitiesFromTags()
    {
        $tags = factory(Tag::class, 10)->make([
            'name' => GroupType::BEDROOM_TAG_TYPE
        ])->each(function ($tag) {
            $typeName = $this->faker->randomElement([
                GroupType::BEDROOM_TAG_TYPE,
                GroupType::BATHROOM_TAG_TYPE,
                GroupType::PARKING_TAG_TYPE,
                GroupType::NEAR_TAG_TYPE,
                GroupType::SHARE_TAG_TYPE,
            ]);

            $tag->name = $typeName;
            $tag->save();

            factory(TagType::class)->create([
                'name' => $typeName,
                'tag_id' => $tag->id
            ]);
        });

        $kosRuleTags = factory(Tag::class, 2)
        ->create([
            'name' => GroupType::RULE_TAG_TYPE
        ])->each(function ($tag) {
            factory(TagType::class)->create([
                'name' => GroupType::RULE_TAG_TYPE,
                'tag_id' => $tag->id
            ]);
        });

        $mappedFacilities = $this->service->getGroupedFacilitiesFromTags(
            $tags->concat($kosRuleTags),
            function ($tag) {
                return $tag;
            }
        );

        // assert kos_rule tags not included
        foreach (GroupType::FACILITIES_MAPPING as $key => $value) {
            $tagsGroup = $tags->filter(function ($tag) use ($key) {
                return GroupType::FACILITIES_MAPPING[$tag->types->last()->name] == GroupType::FACILITIES_MAPPING[$key];
            })->each(function ($tag) {
                return $tag->toArray();
            })->toArray();

            $orderTagGroup = [];
            foreach ($tagsGroup as $tag) {
                $orderTagGroup[] = $tag;
            }

            if (isset($mappedFacilities[$value])) {
                $this->assertEquals($orderTagGroup, $mappedFacilities[$value]->toArray());
            };
        }
    }
}
