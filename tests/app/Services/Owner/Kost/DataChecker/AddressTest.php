<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class AddressTest extends MamiKosTestCase
{

    public function testShouldValid()
    {
        $room = factory(Room::class)->states(['active', 'with-address', 'with-address-note'])->create();
        $checker = app()->make(Address::class);

        $this->assertTrue($checker->check($room));
    }

    public function testShouldBeOkayOnZeroLongLat()
    {
        $room = factory(Room::class)->states(['active', 'with-address', 'with-address-note'])->create([
            'longitude' => 0,
            'latitude' => 0
        ]);
        $checker = app()->make(Address::class);

        $this->assertTrue($checker->check($room));
    }

    public function testShouldFailOnOutOfBoundariesLongLat()
    {
        $room = factory(Room::class)->states(['active', 'with-address', 'with-address-note'])->create([
            'longitude' => 200,
            'latitude' => 150
        ]);
        $checker = app()->make(Address::class);

        $this->assertFalse($checker->check($room));
    }

    public function testShouldFailOnNoAddressNoteProvinceFK()
    {
        $room = factory(Room::class)->states(['active', 'with-address'])->create([
            'longitude' => 200,
            'latitude' => 150
        ]);
        $checker = app()->make(Address::class);

        $this->assertFalse($checker->check($room));
    }
}
