<?php

namespace App\Services\Room;

use App\Entities\Booking\BookingDiscount;
use App\Entities\FlashSale\FlashSale;
use App\Entities\FlashSale\FlashSaleArea;
use App\Entities\FlashSale\FlashSaleAreaLanding;
use App\Entities\Landing\Landing;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Entities\Room\TraitFieldGeneratorTest;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class PriceFilterServiceTest extends MamiKosTestCase
{
    protected $room;
    protected $service;

    protected function setUp(): void
    {
        Parent::setUp();
        \DB::table('designer_price_filter')->truncate();
        $this->room = factory(Room::class)->create(
            [
                'name' => 'Kose Putra Automation'
            ]
        );

        $this->setRoomWithDiscounts();
        $this->setRunningFlashSaleData('2');
        $this->service = app()->make(PriceFilterService::class);
    }

    public function testUpdatePriceFilterCreateNewData()
    {
        $this->service->updatePriceFilter($this->room);
        $this->assertCount(1, PriceFilter::get());
    }

    public function testGetPriceFromRoomForMonthlyRentType()
    {
        $method = $this->getNonPublicMethodFromClass(PriceFilterService::class, 'getPriceFromRoom');
        $response = $method->invokeArgs(
            $this->service,
            [
                PriceFilterService::MONTHLY_RENT_TYPE,
                $this->room
            ]
        );

        $this->assertEquals($this->room->price_monthly, $response);
    }

    public function testGetPriceFromRoomForYearlyRentType()
    {
        $method = $this->getNonPublicMethodFromClass(PriceFilterService::class, 'getPriceFromRoom');
        $response = $method->invokeArgs(
            $this->service,
            [
                PriceFilterService::YEARLY_RENT_TYPE,
                $this->room
            ]
        );

        $this->assertEquals($this->room->price_yearly, $response);
    }

    public function testGetPriceFromRoomReturnZero()
    {
        $method = $this->getNonPublicMethodFromClass(PriceFilterService::class, 'getPriceFromRoom');
        $response = $method->invokeArgs(
            $this->service,
            [
                PriceFilterService::QUARTERLY_RENT_TYPE,
                $this->room
            ]
        );

        // price_quarterly is null
        $this->assertEquals(0, $response);
    }

    /*
     * Private methods
     */
    private function setRoomWithDiscounts(): void
    {
        // Geolocation
        $this->room->latitude = TraitFieldGeneratorTest::ROOM_LATITUDE;
        $this->room->longitude = TraitFieldGeneratorTest::ROOM_LONGITUDE;

        // Price elements
        $this->room->price_daily = 0;
        $this->room->price_weekly = 0;
        $this->room->price_monthly = 2400000;
        $this->room->price_yearly = 0;
        $this->room->price_quarterly = null;

        // Room status
        $this->room->is_active = 1;
        $this->room->is_booking = 1;

        $this->room->save();

        // Quarterly discount
        $this->room->discounts()->save(
            factory(BookingDiscount::class)
                ->state('monthly')
                ->make(
                    [
                        'price' => 3000000,
                        'markup_type' => 'nominal',
                        'markup_value' => 0,
                        'discount_type' => 'percentage',
                        'discount_value' => 20,
                        'discount_type_mamikos' => 'percentage',
                        'discount_value_mamikos' => 10,
                        'discount_type_owner' => 'percentage',
                        'discount_value_owner' => 10,
                        'is_active' => 1
                    ]
                )
        );
    }

    private function setRunningFlashSaleData($rentType): void
    {
        $landingData = $this->setLandingData($rentType);

        $flashSaleData = factory(FlashSale::class)
            ->create(
                [
                    "start_time" => Carbon::now()->subDay()->toDateTimeString(),
                    "end_time" => Carbon::now()->addDay()->toDateTimeString()
                ]
            );

        $flashSaleAreaData = factory(FlashSaleArea::class)
            ->create(
                [
                    "flash_sale_id" => $flashSaleData->id
                ]
            );

        factory(FlashSaleAreaLanding::class)->create(
            [
                "flash_sale_area_id" => $flashSaleAreaData->id,
                "landing_id" => $landingData->id
            ]
        );
    }

    private function setLandingData($rentType)
    {
        $parentLandingData = factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => null,
                "rent_type" => $rentType
            ]
        );

        return factory(Landing::class)->create(
            [
                "type" => 'area',
                "parent_id" => $parentLandingData->id,
                "latitude_1" => -8.020135563600139,
                "longitude_1" => 110.27492523193361,
                "latitude_2" => -7.976957752572763,
                "longitude_2" => 110.3350067138672,
                "price_min" => 0,
                "price_max" => 15000000
            ]
        );
    }
}
