<?php


namespace app\Services\Room;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Services\Room\RoomService;
use App\Test\MamiKosTestCase;
use Illuminate\Pagination\LengthAwarePaginator;

class RoomServiceTest extends MamiKosTestCase
{
    private $service;
    private $roomRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->roomRepository = $this->mockAlternatively(RoomRepository::class);
        $this->service = app()->make(RoomService::class);
    }

    public function testGetRoomsByPropertyAndSuccess()
    {
        $property = factory(Property::class)->make(['owner_user_id' => 123]);
        $this->roomRepository->shouldReceive('getActiveRoomWithProperty')
            ->andReturn(new LengthAwarePaginator(
                [factory(Room::class)->make()],
                1,
                20
            ));
        $result = $this->service->getRoomsByProperty($property);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testTogglePropertyAssignmentWithAssigningAndSuccess()
    {
        $property = factory(Property::class)->make();
        $room = factory(Room::class)->make();
        $this->roomRepository->shouldReceive('assignRoomProperty')->andReturn(true);
        $result = $this->service->togglePropertyAssignment($property, $room);
        $this->assertTrue($result);
    }

    public function testTogglePropertyAssignmentWithUnassigningAndSuccess()
    {
        $property = factory(Property::class)->make();
        $room = factory(Room::class)->make(['property_id' => 123]);
        $this->roomRepository->shouldReceive('unassignRoomProperty')->andReturn(true);
        $result = $this->service->togglePropertyAssignment($property, $room);
        $this->assertTrue($result);
    }
}
