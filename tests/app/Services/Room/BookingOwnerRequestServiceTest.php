<?php

namespace app\Services\Room;

use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Services\Room\BookingOwnerRequestService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\App;

class BookingOwnerRequestServiceTest extends MamiKosTestCase
{
    private $service;

    public function setUp()
    {
        parent::setUp();
        $this->service = App::make(BookingOwnerRequestService::class);
    }

    public function testRequestNewBbk()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        $data = [
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
            'requested_by'  => 'owner',
            'registered_by' => BookingOwnerRequestEnum::SYSTEM
        ];

        $requestBbk = $this->service->requestNewBbk($data);

        $this->assertEquals($room->id, $requestBbk->designer_id);
        $this->assertEquals($user->id, $requestBbk->user_id);
        $this->assertEquals('waiting', $requestBbk->status);
    }

    public function testRequestBbk()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $requestBbk = factory(BookingOwnerRequest::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_REJECT,
        ]);

        $requestBbk = $this->service->requestBbk($requestBbk);

        $this->assertEquals($room->id, $requestBbk->designer_id);
        $this->assertEquals($user->id, $requestBbk->user_id);
        $this->assertEquals('waiting', $requestBbk->status);
    }

    public function testRequestBulkBbk()
    {
        $user = factory(User::class)->create();
        $rooms = factory(Room::class, 2)->create();
        factory(RoomOwner::class)->create([
            'designer_id'   => $rooms[0]->id,
            'user_id'       => $user->id,
            'status'        => RoomOwner::ROOM_ADD_STATUS,
            'owner_status'  => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $rooms[1]->id,
            'user_id'       => $user->id,
            'status'        => RoomOwner::ROOM_ADD_STATUS,
            'owner_status'  => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ]);
        factory(BookingOwnerRequest::class)->create([
            'designer_id'   => $rooms[1]->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_REJECT,
        ]);

        $this->service->requestBulkBbk($user->id);

        $bbkRequests = BookingOwnerRequest::where('user_id', $user->id)->get();

        $bbkRequests->each(function ($bbkRequest) use ($user) {
            $this->assertEquals($user->id, $bbkRequest->user_id);
            $this->assertEquals('waiting', $bbkRequest->status);
        });
    }

    public function testBulkReject()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(BookingOwnerRequest::class, 2)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
        ]);
        
        $content = 'Mohon pastikan nama pemilik rekening sama dengan nama lengkap Anda.';
        $this->service->bulkReject($user->id, 'Mohon pastikan nama pemilik rekening sama dengan nama lengkap Anda.');

        $requestsBbk = BookingOwnerRequest::where('user_id', $user->id);

        foreach ($requestsBbk as $requestBbk) {
            $this->assertEquals(BookingOwnerRequest::BOOKING_REJECT, $requestBbk->status);
            $expectedResult = [
                'from'       => 'bbk',
                'listing_id' => $requestBbk->designer_id,
                'content'    => $content,
            ];
            $this->assertDatabaseHas('scraping_listing_reason',$expectedResult);
        }
    }

    public function testBulkApprove()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(BookingOwnerRequest::class, 2)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
        ]);
        $this->actingAs($user);
        
        $this->service->bulkApprove($user->id);

        $requestsBbk = BookingOwnerRequest::where('user_id', $user->id);

        foreach ($requestsBbk as $requestBbk) {
            $this->assertEquals(BookingOwnerRequest::BOOKING_APPROVE, $requestBbk->status);
        }
    }

    public function testSingleApprove()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(BookingOwnerRequest::class, 2)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
        ]);
        $this->actingAs($user);

        $this->service->singleApprove($user->id, $room->id);

        $requestsBbk = BookingOwnerRequest::where('user_id', $user->id);

        foreach ($requestsBbk as $requestBbk) {
            $this->assertEquals(BookingOwnerRequest::BOOKING_APPROVE, $requestBbk->status);
        }
    }

    public function testSingleApproveWithKostRoomLevel()
    {
        factory(KostLevel::class)->state('regular')->create();
        factory(RoomLevel::class)->create();
        
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(BookingOwnerRequest::class, 2)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
        ]);

        $this->actingAs($user);
        
        $this->service->singleApprove($user->id, $room->id);

        $requestsBbk = BookingOwnerRequest::where('user_id', $user->id);

        foreach ($requestsBbk as $requestBbk) {
            $this->assertEquals(BookingOwnerRequest::BOOKING_APPROVE, $requestBbk->status);
        }
    }

    public function testSingleReject()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
        ]);
        $bbkRequest = factory(BookingOwnerRequest::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'status'        => BookingOwnerRequest::BOOKING_WAITING,
        ]);
        
        $this->actingAs($user);
        $rejectRemark = 'reject';
        $this->service->singleReject($room->id,$rejectRemark);
        
        $expectedResult = [
            'from' => 'bbk',
            'listing_id' => $bbkRequest->designer_id,
            'content'    => $rejectRemark,
            'user_id'    => $user->id,
        ];
        $requestBbk = BookingOwnerRequest::find($bbkRequest->id);
        $this->assertEquals(BookingOwnerRequest::BOOKING_REJECT, $requestBbk->status);
        $this->assertDatabaseHas('scraping_listing_reason',$expectedResult);
    }

}
