<?php

namespace App\Services\Room;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\ListingRejectReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use DatabaseSeeder;
use RejectReasonSeeder;

class RejectServiceTest extends MamiKosTestCase
{
    private $service;

    protected function setUp()
    {
        parent::setUp();

        $this->service = app()->make(RejectService::class);
        app(DatabaseSeeder::class)->call(RejectReasonSeeder::class);
    }

    public function testReject()
    {
        $owner = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $rejectReason = factory(RejectReason::class)->create([
            'group' => RejectReason::ADDRESS,
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);
        $rejectReasonOther = factory(RejectReason::class)->create([
            'group' => RejectReason::BUILDING_PHOTO,
            'content' => null,
        ]);
        $reasonIds = [$rejectReason->id];
        $reasonOthers = [
            $rejectReasonOther->id => 'Foto Blur'
        ];

        $listingReason = $this->service->reject($room, $reasonIds, $reasonOthers);
        
        $this->assertDatabaseHas((new ListingReason())->getTable(), ['listing_id' => $room->id]);
        $this->assertDatabaseHas((new ListingRejectReason())->getTable(), ['reject_reason_id' => $rejectReasonOther->id]);
        $this->assertEquals($listingReason->content, 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah). Foto Bangunan: Foto Blur. ');
    }

    public function testGetRejectRemark()
    {
        $rejectReason = factory(RejectReason::class)->create([
            'group' => RejectReason::ADDRESS,
            'content' => 'Kurang lengkap (isi RT/RW dan nomor rumah)',
        ]);
        $rejectReasonOther = factory(RejectReason::class)->create([
            'group' => RejectReason::BUILDING_PHOTO,
            'content' => null,
        ]);
        $reasonIds = [$rejectReason->id];
        $reasonOthers = [
            $rejectReasonOther->id => 'Foto Blur'
        ];

        $rejectRemark = $this->service->getRejectRemark($reasonIds, $reasonOthers);

        $this->assertEquals($rejectRemark, 'Alamat Kos: Kurang lengkap (isi RT/RW dan nomor rumah). Foto Bangunan: Foto Blur. ');
    }
}
