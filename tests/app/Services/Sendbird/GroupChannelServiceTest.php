<?php
namespace App\Services\Sendbird;

use App\Entities\Room\Room;
use App\Libraries\SendBird;
use App\Services\Sendbird\GroupChannel\Channel;
use App\Services\Sendbird\GroupChannel\ChannelUrlFinder;
use App\Services\Sendbird\GroupChannel\GoldplusChannelAdjuster;
use App\Services\Sendbird\GroupChannel\TenantRoomChannelCreator;
use App\Services\Sendbird\GroupChannelService;
use App\Test\MamiKosTestCase;
use App\User;

include_once 'GroupChannel/SendbirdResponseFactory.php';
use App\Services\Sendbird\GroupChannel\SendbirdResponseFactory;

class GroupChannelServiceTest extends MamiKosTestCase
{
    public function testfindOrCreateChannelForTenantAndRoom()
    {
        $channel = $this->service->findOrCreateChannelForTenantAndRoom(
            $this->tenant,
            $this->room
        );

        $this->assertNotNull($channel);
    }

    public function testPutMetadataToGroupChannelShouldCallClient()
    {
        $this->client->shouldReceive('putChannelMetadata')
            ->once();
        $this->service->putMetadataToGroupChannel('some-channel-url', $this->room);
    }

    public function testAdjustChannelToGpRule()
    {
        $this->setChannel();
        $this->gpChannelAdjuster->shouldReceive('adjust')
            ->once();
        $this->service->adjustChannelToGpRule($this->channel, $this->room);
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setService();

        $this->setTenant();
        $this->setRoom();
    }

    protected function setService()
    {
        $this->setClient();
        $this->setUrlFinder();
        $this->setGoldplusChannelAdjuster();
        $this->setChannelCreator();

        $this->service = new GroupChannelService(
            $this->client,
            $this->urlFinder,
            $this->gpChannelAdjuster,
            $this->channelCreator
        );
    }

    protected function setClient()
    {
        $response = ['channels' => [
            SendbirdResponseFactory::getArray()
        ]];
        $client = \Mockery::mock(SendBird::class);
        $client->shouldReceive('getUserGroupChannels')
            ->andReturns($response)
            ->byDefault();

        $client->shouldReceive('putChannelMetadata')
            ->byDefault();

        $this->client = $client;
    }

    protected function setChannel()
    {
        $this->channel = new Channel();
    }

    protected function setUrlFinder()
    {
        $urlFinder = \Mockery::mock(ChannelUrlFinder::class);
        $urlFinder->shouldReceive('find')
            ->andReturns('some-channel-url')
            ->byDefault();
        $this->urlFinder = $urlFinder;
    }

    protected function setGoldplusChannelAdjuster()
    {
        $gpChannelAdjuster = \Mockery::mock(GoldplusChannelAdjuster::class);
        $this->gpChannelAdjuster = $gpChannelAdjuster;
    }

    protected function setChannelCreator()
    {
        $channelCreator = \Mockery::mock(TenantRoomChannelCreator::class);
        $this->channelCreator = $channelCreator;
    }

    protected function setTenant()
    {
        $tenant = $this->mockPartialAlternatively(User::class);
        $tenant->id = rand(1, 1000);
        $this->tenant = $tenant;
    }

    protected function setRoom()
    {
        $room = $this->mockPartialAlternatively(Room::class);
        $room->id = rand(1, 1000);
        $room->shouldReceive('getChatCoverUrl')
            ->andReturns('https://some.cover.url/somewhere')
            ->byDefault();
        $this->room = $room;
    }

    private function prepareClientMock($userId, $criteria, $returnValue = null)
    {
        if (is_null($returnValue)) {
            $returnValue = $this->defaultClientReturnValue();
        }

        $this->clientMock
            ->shouldReceive('getUserGroupChannels')
            ->andReturn($returnValue);

        $this->clientMock
            ->shouldReceive('createGroupChannel')
            ->andReturn($returnValue['channels'][0]);
    }
}
