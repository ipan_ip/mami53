<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'SendbirdResponseFactory.php';

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class TenantRoomChannelCreatorTest extends MamiKosTestCase
{
    public function testCreate()
    {
        $this->channelCreator->shouldReceive('create')
            ->andReturns($this->channelResult)
            ->once();

        $channel = $this->creator->create($this->tenant, $this->room);
        $this->assertEquals($this->channelResult, $channel);
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setCreator();
        $this->setTenant();
        $this->setRoom();
    }

    protected function setCreator()
    {
        $this->setChannelCreator();
        $this->setGpQuerist();
        $this->creator = new TenantRoomChannelCreator($this->channelCreator, $this->gpQuerist);
    }

    protected function setChannelCreator()
    {
        $this->setChannelResult();
        $channelCreator = \Mockery::mock(ChannelCreator::class);
        $channelCreator->shouldReceive('create')
            ->andReturns($this->channelResult)
            ->byDefault();
        $this->channelCreator = $channelCreator;
    }

    protected function setGpQuerist()
    {
        $gpQuerist = \Mockery::mock(GoldplusQuerist::class);
        $gpQuerist->shouldReceive('getCustomType')
            ->andReturns('')
            ->byDefault();
        $gpQuerist->shouldReceive('isOwnerAllowedAsMember')
            ->andReturns(true)
            ->byDefault();
        $gpQuerist->shouldReceive('isRoomGp3')
            ->andReturns(true)
            ->byDefault();
        $this->gpQuerist = $gpQuerist;
    }

    protected function setTenant()
    {
        $this->tenant = factory(User::class)->create();
    }

    protected function setRoom()
    {
        $this->room = factory(Room::class)->create();
        $owner = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $this->room->id,
            'user_id' => $owner->id
        ]);
    }

    protected function setChannelResult()
    {
        $this->channelResult = Channel::makeFromSendbirdArray(SendbirdResponseFactory::getArray());
    }
}