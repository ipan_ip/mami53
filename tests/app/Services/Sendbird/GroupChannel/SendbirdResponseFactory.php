<?php
namespace App\Services\Sendbird\GroupChannel;

class SendbirdResponseFactory
{
    public static function getArray()
    {
        return [
            'channel_url' => 'sendbird_group_channel_178477233_7ef313bce1302db5a313f30f9c49c331fea76dfa',
            'custom_type' => '',
            'data' => '{"room_id":57732013}',
            'name' => 'Supono: Kost Akika Mawar Kencana',
            'members' => [
                [
                    'user_id' => rand(1, 100),
                    'nickname' => 'supono',
                ],
                [
                    'user_id' => rand(101, 200),
                    'nickname' => 'sutejo',
                ],
                [
                    'user_id' => rand(201, 300),
                    'nickname' => 'sutarman',
                ]
            ]
        ];
    }
}