<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'SendbirdResponseFactory.php';

use App\Test\MamiKosTestCase;
use Faker\Generator as Faker;

class ChannelTest extends MamiKosTestCase
{
    public function testMakeFromSendbirdArray()
    {
        $channel = Channel::makeFromSendbirdArray($this->sendbirdArray);
        $this->assertArray($channel);
    }

    public function testAssignPropertiesFromSendbirdArray()
    {
        $channel = new Channel();
        $channel->assignPropertiesFromSendbirdArray($this->sendbirdArray);
        $this->assertArray($channel);
    }

    protected function assertArray($channel)
    {
        $this->assertEquals($this->sendbirdArray['channel_url'], $channel->channel_url);
        $this->assertEquals($this->sendbirdArray['custom_type'], $channel->custom_type);
        $this->assertEquals($this->sendbirdArray['data'], $channel->data);
        $this->assertEquals(count($this->sendbirdArray['members']), count($channel->members));
        $this->assertInstanceOf(Member::class, $channel->members[0]);
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setSendbirdArray();
    }

    public function setSendbirdArray()
    {
        $this->sendbirdArray = SendbirdResponseFactory::getArray();
    }
}