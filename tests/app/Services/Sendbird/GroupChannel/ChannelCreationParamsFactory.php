<?php
namespace App\Services\Sendbird\GroupChannel;

class ChannelCreationParamsFactory
{
    public static function make() : ChannelCreationParams
    {
        $members = [];
        for ($i = 1; $i <= 3; $i++) {
            $member = new Member();
            $member->user_id = rand(1, 1000);
            $member->nickname = "name" . $i;
            $members[] = $member;
        }

        $params = new ChannelCreationParams();
        $params->cover_url = 'https://some.url.for/a/picture';
        $params->custom_type = '';
        $params->data = [];
        $params->is_distinct = false;
        $params->name = 'ahaha';
        $params->room_id = rand(1, 10000);
        $params->members = $members;
        return $params;
    }
}