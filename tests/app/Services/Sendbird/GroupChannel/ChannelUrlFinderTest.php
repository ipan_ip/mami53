<?php
namespace App\Services\Sendbird\GroupChannel;

use App\Test\MamiKosTestCase;

use App\Entities\Chat\Call;
use App\Entities\Chat\Channel;
use App\User;

use App\Repositories\CallRepository;
use App\Repositories\UserDataRepository;


class ChannelUrlFinderTest extends MamiKosTestCase
{
    public function testFindIsFoundOnChatChannel()
    {
        $url = $this->finder->find($this->params);
        $this->assertEquals($this->channel->global_id, $url);
    }

    public function testFindIsFoundOnCall()
    {
        $this->userChatChannelMock
            ->shouldReceive('first')
            ->andReturns(null);

        $url = $this->finder->find($this->params);
        $this->assertEquals($this->call->chat_group_id, $url);
    }

    public function testFindIsNotFoundAnywhere()
    {
        $this->userChatChannelMock
            ->shouldReceive('first')
            ->andReturns(null);

        $this->callRepoMock
            ->shouldReceive('first')
            ->andReturns(null);

        $url = $this->finder->find($this->params);
        $this->assertEquals('', $url);
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setupFinder();
        $this->setupParams();
    }

    private function setupParams() {
        $params = new ChannelUrlFinderParams();
        $params->user_id = rand(1, 1000);
        $params->room_id = rand(1, 1000);

        $this->params = $params;
    }

    private function setupFinder() {
        $this->setUserDataRepoMock();
        $this->setCallRepoMock();
        $this->finder = new ChannelUrlFinder(
            $this->userRepoMock,
            $this->callRepoMock
        );
    }

    private function setUserDataRepoMock()
    {
        $this->setUserMock();
        $this->userRepoMock = \Mockery::mock(UserDataRepository::class);
        $this->userRepoMock
            ->shouldReceive('find')
            ->andReturns($this->userMock)
            ->byDefault();
    }

    private function setCallRepoMock()
    {
        $this->callRepoMock = \Mockery::mock(CallRepository::class);
        $this->callRepoMock 
            ->shouldReceive('where')
            ->andReturns($this->callRepoMock)
            ->byDefault();

        $this->callRepoMock 
            ->shouldReceive('orderBy')
            ->andReturns($this->callRepoMock)
            ->byDefault();

        $this->setCall();
        $this->callRepoMock 
            ->shouldReceive('first')
            ->andReturns($this->call)
            ->byDefault();
    }

    private function setUserMock()
    {
        $this->setUserChatChannelMock();
        $this->userMock = \Mockery::mock(User::class);
        $this->userMock
            ->shouldReceive('chat_channel')
            ->andReturns($this->userChatChannelMock)
            ->byDefault();
    }

    private function setUserChatChannelMock()
    {
        $this->userChatChannelMock = \Mockery::mock('UserChatChannels');
        $this->userChatChannelMock
            ->shouldReceive('wherePivot')
            ->andReturns($this->userChatChannelMock)
            ->byDefault();

        $this->setChannel();
        $this->userChatChannelMock
            ->shouldReceive('first')
            ->andReturns($this->channel)
            ->byDefault();
    }

    private function setChannel()
    {
        $this->channel = factory(Channel::class)->make();
    }

    private function setCall()
    {
        $this->call = factory(Call::class)->make();
    }
}