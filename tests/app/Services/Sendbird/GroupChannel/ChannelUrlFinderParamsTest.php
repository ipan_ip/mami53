<?php
namespace App\Services\Sendbird\GroupChannel;

use App\Test\MamiKosTestCase;

class ChannelUrlFinderParamsTest extends MamiKosTestCase
{
    public function testValidate()
    {
        $this->assertNull($this->params->validate());
    }

    public function testValidateInvalidUserIdShouldRaiseError()
    {
        $this->expectException('Exception');
        $this->params->user_id = 'one';
        $this->params->validate();
    }

    public function testValidateInvalidRoomIdShouldRaiseError()
    {
        $this->expectException('Exception');
        $this->params->room_id = 'mawar';
        $this->params->validate();
    }

    // setup

    public function setUp() : void
    {
        parent::setUp();
        $this->setParams();
    }

    protected function setParams()
    {
        $params = new ChannelUrlFinderParams();
        $params->user_id = rand(1, 1000);
        $params->room_id = rand(1, 1000);
        $this->params = $params;
    }
}