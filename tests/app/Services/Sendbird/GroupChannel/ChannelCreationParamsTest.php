<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'ChannelCreationParamsFactory.php';

use App\Test\MamiKosTestCase;

class ChannelCreationParamsTest extends MamiKosTestCase
{
    public function testValidateWithNoError()
    {
        $this->assertNull($this->params->validate());
    }

    public function testValidateWithCoverUrlError()
    {
        $this->expectException('Exception');
        $this->params->cover_url = 1;
        $this->params->validate();
    }

    public function testValidateWithCustomTypeError()
    {
        $this->expectException('Exception');
        $this->params->custom_type = 1;
        $this->params->validate();
    }

    public function testValidateWithDataError()
    {
        $this->expectException('Exception');
        $this->params->data = 1;
        $this->params->validate();
    }

    public function testValidateWithIsDistinctError()
    {
        $this->expectException('Exception');
        $this->params->is_distinct = 'false';
        $this->params->validate();
    }

    public function testValidateWithNameError()
    {
        $this->expectException('Exception');
        $this->params->name = '';
        $this->params->validate();
    }

    public function testValidateWithRoomIdError()
    {
        $this->expectException('Exception');
        $this->params->room_id = '';
        $this->params->validate();
    }

    public function testValidateWithMembersError()
    {
        $this->expectException('Exception');
        $this->params->members = [];
        $this->params->validate();
    }
    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setParams();
    }

    protected function setParams()
    {
        $this->params = ChannelCreationParamsFactory::make();
    }
}