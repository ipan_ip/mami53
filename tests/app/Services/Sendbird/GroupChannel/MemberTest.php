<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'SendbirdResponseFactory.php';

use App\Test\MamiKosTestCase;

class MemberTest extends MamiKosTestCase
{
    public function testAssignPropertiesFromSendbirdArray()
    {
        $member_a = $this->sendbirdArray['members'][0];
        $this->member->assignPropertiesFromSendbirdArray($member_a);
        $this->assertIsInt($this->member->user_id);
        $this->assertIsString($this->member->nickname);
        $this->assertEquals($member_a['user_id'], $this->member->user_id);
        $this->assertEquals($member_a['nickname'], $this->member->nickname);
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMember();
        $this->setupSendbirdArray();
    }

    protected function setupMember()
    {
        $this->member = new Member();
    }

    protected function setupSendbirdArray()
    {
        $this->sendbirdArray = SendbirdResponseFactory::getArray();
    }
}