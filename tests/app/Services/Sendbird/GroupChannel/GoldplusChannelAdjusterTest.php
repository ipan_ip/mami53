<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'SendbirdResponseFactory.php';

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\Libraries\SendBird;

class GoldplusChannelAdjusterTest extends MamiKosTestCase
{
    public function testAdjust()
    {
        $this->assertNull($this->adjuster->adjust($this->channel, $this->room));
    }

    public function testAdjustShouldUpdateCustomTypeWhenItsNotMatch()
    {
        $this->client->shouldReceive('updateGroupChannel')->once();
        $this->gpQuerist->shouldReceive('getCustomType')
            ->andReturns('gp3');
        $this->assertNull($this->adjuster->adjust($this->channel, $this->room));
    }

    public function testAdjustShouldAddOwnerToChannelWhenItsAllowedButNotIncluded()
    {
        $this->client->shouldReceive('addUserToGroupChannel')->once();
        $this->room->shouldReceive('getLatestOwnerUserId')
            ->andReturns(rand(1000000, 9999999));
        $this->assertNull($this->adjuster->adjust($this->channel, $this->room));
    }

    public function testAdjustShouldRemoveOwnerFromChannelWhenItsUnallowedButIncluded()
    {
        $this->client->shouldReceive('leaveChannel')->once();
        $this->gpQuerist->shouldReceive('isOwnerAllowedAsMember')
            ->andReturns(false);
        $this->assertNull($this->adjuster->adjust($this->channel, $this->room));
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setAdjuster();
        $this->setChannel();
        $this->setRoom();
    }

    protected function setAdjuster()
    {
        $this->setSendbirdClient();
        $this->setGoldplusQueriest();
        $this->adjuster = new GoldplusChannelAdjuster(
            $this->client,
            $this->gpQuerist
        );
    }

    protected function setSendbirdClient()
    {
        $this->client = \Mockery::mock(SendBird::class);
    }

    protected function setGoldplusQueriest()
    {
        $gpQuerist = \Mockery::mock(GoldplusQuerist::class);
        $gpQuerist->shouldReceive('isRoomGp3')
            ->andReturns(false)
            ->byDefault();

        $gpQuerist->shouldReceive('getCustomType')
            ->andReturns('')
            ->byDefault();
        
        $gpQuerist->shouldReceive('isOwnerAllowedAsMember')
            ->andReturns(true)
            ->byDefault();

        $this->gpQuerist = $gpQuerist;
    }

    protected function setChannel()
    {
        $channel = new Channel;

        $response = SendbirdResponseFactory::getArray();
        $channel->assignPropertiesFromSendbirdArray($response);
        $this->channel = $channel;
    }

    protected function setRoom()
    {
        $room = \Mockery::mock(Room::class);
        $room->shouldReceive('getLatestOwnerUserId')
            ->andReturns($this->channel->members[0]->user_id)
            ->byDefault();

        $this->room = $room;
    }
}