<?php
namespace App\Services\Sendbird\GroupChannel;

include_once 'SendbirdResponseFactory.php';
include_once 'ChannelCreationParamsFactory.php';

use App\Libraries\SendBird;
use App\Test\MamiKosTestCase;
use App\User;

class ChannelCreatorTest extends MamiKosTestCase
{
    public function testCreate()
    {
        $channel = $this->creator->create($this->params);
        $this->assertNotNull($channel->channel_url);
        $this->assertNotNull($channel->data);

        foreach ($this->users as $user) {
            $this->assertNotEmpty($user->chat_channel->count());
        }
    }

    public function testCreateWithInvalidParamsShouldRaiseException()
    {
        $this->expectException('Exception');
        $this->params->room_id = 0;
        $this->creator->create($this->params);
    }

    // setup

    public function setUp(): void
    {
        parent::setUp();
        $this->setCreator();
        $this->setUsers();
        $this->setParams();
    }

    protected function setCreator()
    {
        $this->setClientMock();
        $this->creator = new ChannelCreator($this->clientMock);
    }

    protected function setParams()
    {
        $this->params = ChannelCreationParamsFactory::make();

        foreach ($this->params->members as $i => $member) {
            $member->user_id = $this->users[$i]->id;
            $params->members[$i] = $member;
        }
    }

    protected function setClientMock()
    {
        $response = SendbirdResponseFactory::getArray();
        $this->clientMock = \Mockery::mock(SendBird::class);
        $this->clientMock
            ->shouldReceive('createGroupChannel')
            ->andReturns($response)
            ->byDefault();
    }

    protected function setUsers()
    {
        $users = [];
        for ($i = 0; $i < 3; $i++) {
            $users[] = factory(User::class)->create();
        }
        $this->users = $users;
    }
}