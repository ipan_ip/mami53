<?php

namespace App\Services\AbTest;

use App\Channel\ABTest\DashboardAPIClient;
use App\Test\MamiKosTestCase;
use Illuminate\Http\Request;

class DashboardAPIServiceTest extends MamiKosTestCase
{
    public function testPost()
    {
        // Preparation start
        $respBody = '{"id" : 2}';
        $resp = \Mockery::mock('response');
        $resp->shouldReceive('getBody')->andReturn($respBody);

        $params = ['mode' => 'list_experiment'];
        $body = '{}';

        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('query')->andReturn($params);
        $request->shouldReceive('getContent')->andReturn($body);

        $apiClient = \Mockery::mock(DashboardAPIClient::class);
        $apiClient->shouldReceive('postWithRawBody')
            ->with($params, $body)
            ->andReturns($resp);
        // Preparation end
        
        // Test start
        $service = new DashboardAPIService($apiClient);
        $result = $service->post($request);
        $this->assertEquals($respBody, $result);
    }
}