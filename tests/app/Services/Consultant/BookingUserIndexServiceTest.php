<?php

namespace App\Services\Consultant;

use App\Entities\Booking\BookingDesignerRoom;
use App\Entities\Booking\BookingOwner;
use App\Entities\Booking\BookingStatus;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Consultant\Elasticsearch\BookingUserIndexQuery;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\SLA\SLAConfiguration;
use App\Entities\SLA\SLAVersion;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class BookingUserIndexServiceTest extends MamiKosTestCase
{
    protected $service;
    protected $elasticsearch;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(BookingUserIndexService::class);
        $this->elasticsearch = $this->mock('alias:Elasticsearch');
    }

    public function testIndex(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithoutKostLevel(): void
    {
        $room = factory(Room::class)->create();

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => null,
                    'kost_level_name' => null,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithNoInstantBooking(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => false]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => false,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithoutBookingOwner(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => false,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithMultipleOwnerAndInstantBooking(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => factory(User::class)->create()->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithMultipleOwnerAndNoInstantBooking(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => factory(User::class)->create()->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => false]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => false,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithMultipleOwnerAndNoBookingOwner(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => factory(User::class)->create()->id,
            'designer_id' => $room->id
        ]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => false,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithNoUserPhoto(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => null]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => null,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithBookedStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_BOOKED]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_TWO, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $booking->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $booking->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithOtherStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CANCELLED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED, 'booking_user_id' => $booking->id]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_TWO,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_THREE,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => null
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithRoomName(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_CONFIRMED]);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_CONFIRMED, 'booking_user_id' => $booking->id]);

        $sla = factory(SLAVersion::class)->create(['sla_type' => SLAVersion::SLA_VERSION_THREE, 'created_at' => $booking->created_at->subHour()->toDateTimeString()]);
        factory(SLAConfiguration::class)->create(['sla_version_id' => $sla->id, 'case' => SLAConfiguration::CASE_EQUAL, 'value' => 60]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date->toDateString(),
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->created_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => $confirmed->created_at->addMinutes(60)
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithRejectedByAdminStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_REJECTED, 'created_at' => Carbon::now()->addHour()->toDateTimeString()]);
        $bookingStatus = factory(BookingStatus::class)->create(['booking_user_id' => $booking->id, 'changed_by_role' => 'admin']);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED, 'booking_user_id' => $booking->id]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_TWO,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_THREE,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date,
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, 'admin'),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => null
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithRejectedByConsultantStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_REJECTED, 'created_at' => Carbon::now()->addHour()->toDateTimeString()]);
        $bookingStatus = factory(BookingStatus::class)->create(['booking_user_id' => $booking->id, 'changed_by_role' => 'consultant']);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED, 'booking_user_id' => $booking->id]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_TWO,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_THREE,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date,
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, 'consultant'),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => null
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithRejectedByOwnerStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_REJECTED, 'created_at' => Carbon::now()->addHour()->toDateTimeString()]);
        $bookingStatus = factory(BookingStatus::class)->create(['booking_user_id' => $booking->id, 'changed_by_role' => 'owner']);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED, 'booking_user_id' => $booking->id]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_TWO,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_THREE,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date,
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, 'owner'),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => null
                ]
            ]);

        $this->service->index($booking);
    }

    public function testIndexWithRejectedBySystemStatus(): void
    {
        $room = factory(Room::class)->create();
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);

        $owner = factory(User::class)->create(['is_owner' => 'true']);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);

        $photo = factory(Media::class)->create();
        $tenant = factory(User::class)->create(['is_owner' => 'false', 'photo_id' => $photo->id]);

        $booking = factory(BookingUser::class)->create(['designer_id' => $room->song_id, 'user_id' => $tenant->id, 'status' => BookingUser::BOOKING_STATUS_REJECTED, 'created_at' => Carbon::now()->addHour()->toDateTimeString()]);
        $bookingStatus = factory(BookingStatus::class)->create(['booking_user_id' => $booking->id, 'changed_by_role' => 'system']);
        $confirmed = factory(BookingStatus::class)->create(['status' => BookingUser::BOOKING_STATUS_BOOKED, 'booking_user_id' => $booking->id]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_TWO,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        factory(SLAConfiguration::class)->create([
            'sla_version_id' => factory(SLAVersion::class)->create([
                'sla_type' => SLAVersion::SLA_VERSION_THREE,
                'created_at' => $booking->created_at->subHour()->toDateTimeString()
            ])->id,
            'case' => SLAConfiguration::CASE_EQUAL,
            'value' => 60
        ]);

        $this->elasticsearch->shouldReceive('index')
            ->with([
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date,
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, 'system'),
                    'booking_created' => $booking->created_at,
                    'booking_confirmed' => $confirmed->updated_at,
                    'kost_name' => $room->name,
                    'room_number' => $booking->contact_room_number,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'song_id' => $room->song_id,
                    'user_photo_id' => $tenant->photo_id,
                    'kost_level_id' => $level->id,
                    'kost_level_name' => $level->name,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'is_instant_booking' => true,
                    'due_date' => null
                ]
            ]);

        $this->service->index($booking);
    }
}
