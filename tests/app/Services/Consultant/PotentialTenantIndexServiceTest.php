<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class PotentialTenantIndexServiceTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $service;
    protected $elasticsearch;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(PotentialTenantIndexService::class);
        $this->elasticsearch = $this->mock('alias:Elasticsearch');
    }

    public function testIndex(): void
    {
        $kost = factory(Room::class)->create();
        $user = factory(User::class)->create(['phone_number' => '081234', 'is_owner' => 'false', 'role' => UserRole::User]);
        $tenant = factory(PotentialTenant::class)->create(['phone_number' => '081234', 'designer_id' => $kost->id, 'scheduled_date' => '2020-10-09', 'updated_at' => '2020-10-09 17:00:00']);
        $note = factory(ConsultantNote::class)->create(['noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT, 'noteable_id' => $tenant->id]);

        $expected = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'id' => $tenant->id,
            'body' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'email' => $tenant->email,
                'phone_number' => $tenant->phone_number,
                'kost_name' => $kost->name,
                'area_city' => $kost->area_city,
                'is_registered' => true,
                'song_id' => $kost->song_id,
                'note_content' => $note->content,
                'note_activity' => $note->activity,
                'scheduled_date' => '2020-10-09',
                'duration_unit' => $tenant->duration_unit,
                'designer_id' => $tenant->designer_id,
                'created_by_consultant' => $tenant->consultant_id,
                'updated_at' => '2020-10-09 17:00:00'
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($expected)
            ->once();

        $this->service
            ->index($tenant);
    }

    public function testIndexWithMissingRelations(): void
    {
        $tenant = factory(PotentialTenant::class)->create(['phone_number' => '081234', 'scheduled_date' => null, 'updated_at' => null]);

        $expected = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'id' => $tenant->id,
            'body' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'email' => $tenant->email,
                'phone_number' => $tenant->phone_number,
                'kost_name' => $tenant->property_name,
                'area_city' => '',
                'is_registered' => false,
                'song_id' => null,
                'note_content' => null,
                'note_activity' => null,
                'scheduled_date' => null,
                'duration_unit' => $tenant->duration_unit,
                'designer_id' => $tenant->designer_id,
                'created_by_consultant' => $tenant->consultant_id,
                'updated_at' => null
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($expected)
            ->once();

        $this->service
            ->index($tenant);
    }

    public function testRemove(): void
    {
        $tenant = factory(PotentialTenant::class)->create();

        $expected = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'id' => $tenant->id
        ];

        $this->elasticsearch
            ->shouldReceive('delete')
            ->with($expected)
            ->once();

        $this->service->remove($tenant);
    }

    public function testIndexNote(): void
    {
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
        ]);

        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'type' => '_doc',
            'id' => $note->noteable_id,
            'body' => [
                'doc' => [
                    'note_content' => $note->content,
                    'note_activity' => $note->activity
                ]
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('update')
            ->with($params)
            ->once();

        $this->service->indexNote($note);
    }

    public function testIndexNoteWithOtherNoteType(): void
    {
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
        ]);

        $this->elasticsearch->shouldNotReceive('update');

        $this->service->indexNote($note);
    }

    public function testRemoveNoteIndex(): void
    {
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_POTENTIAL_TENANT,
        ]);

        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'type' => '_doc',
            'id' => $note->noteable_id,
            'body' => [
                'doc' => [
                    'note_content' => null,
                    'note_activity' => null
                ]
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('update')
            ->with($params)
            ->once();

        $this->service->removeNoteIndex($note);
    }

    public function testRemoveNoteIndexWithOtherNoteType(): void
    {
        $note = factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
        ]);

        $this->elasticsearch->shouldNotReceive('update');

        $this->service->removeNoteIndex($note);
    }
}
