<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialProperty;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Config;

class PotentialPropertyIndexServiceTest extends MamiKosTestCase
{
    protected $elasticsearch;
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->elasticsearch = $this->mock('alias:Elasticsearch');
        $this->service = $this->app->make(PotentialPropertyIndexService::class);
    }

    public function testIndexWithOwner(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => true,
        ]);

        $property->offered_product = 'a:2:{i:0;s:2:"gp";i:1;s:3:"bbk";}';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => true,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp', 'bbk'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithConsultant(): void
    {
        $consultant = factory(User::class)->create();
        factory(Consultant::class)->create(['user_id' => $consultant->id]);

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $consultant->id,
            'is_priority' => true,
        ]);

        $property->offered_product = 'a:1:{i:0;s:3:"bbk";}';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'consultant',
                    'creator_name' => $consultant->name,
                    'is_priority' => true,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['bbk'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithNonPriority(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => false,
        ]);

        $property->offered_product = 'a:1:{i:0;s:2:"gp";}';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => false,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithGP1(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => false,
        ]);

        $property->offered_product = 'gp1';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => false,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp1'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithGP2(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => false,
        ]);

        $property->offered_product = 'gp2';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => false,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp2'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithGP3(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => false,
        ]);

        $property->offered_product = 'gp3';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => false,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp3'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testIndexWithGP4(): void
    {
        $owner = factory(User::class)->create();

        $property = factory(PotentialProperty::class)->create([
            'created_by' => $owner->id,
            'is_priority' => false,
        ]);

        $property->offered_product = 'gp4';

        $this->elasticsearch->shouldReceive('index')
            ->once()
            ->with([
                'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
                'id' => $property->id,
                'body' => [
                    'id' => $property->id,
                    'name' => $property->name,
                    'area_city' => $property->area_city,
                    'province' => $property->province,
                    'created_by' => 'owner',
                    'creator_name' => $owner->name,
                    'is_priority' => false,
                    'owner_id' => $property->consultant_potential_owner_id,
                    'offered_product' => ['gp4'],
                    'bbk_status' => $property->bbk_status,
                    'followup_status' => $property->followup_status,
                    'created_at' => $property->created_at->format('Y-m-d H:m:s'),
                    'updated_at' => $property->updated_at->format('Y-m-d H:m:s')
                ]
            ]);

        $this->service->index($property);
    }

    public function testRemove(): void
    {
        $potentialProperty = factory(PotentialProperty::class)->create();

        $params = [
            'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
            'id' => $potentialProperty->id
        ];

        $this->elasticsearch->shouldReceive('delete')->with($params)->once();
        $this->service->remove($potentialProperty);
    }
}
