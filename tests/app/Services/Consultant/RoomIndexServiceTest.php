<?php

namespace App\Services\Consultant;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;

class RoomIndexServiceTest extends MamiKosTestCase
{
    protected $elasticsearch;
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->elasticsearch = $this->mock('alias:Elasticsearch');
        $this->service = $this->app->make(RoomIndexService::class);
    }

    public function testSearch(): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => ['title' => 'exclusive'],
                        ],
                        'filter' => [
                            'term' => ['type' => 'kos']
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with($params)
            ->andReturn([]);

        $this->service->search('exclusive');
    }

    public function testSearchWithConsultantId(): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'body' => [
                'query' => [
                    'bool' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ['consultant_ids' => 1]],
                                    ['term' => ['type' => 'kos']],
                                    ['match' => ['title' => 'exclusive']]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with($params)
            ->andReturn([]);

        $this->service->search('exclusive', 1);
    }

    public function testSearchKostWithActiveMamipayWithoutConsultantId(): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => ['title' => 'exclusive'],
                        ],
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ['type' => 'kos']],
                                    ['term' => ['is_mamipay_active' => true]]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with($params)
            ->andReturn([]);

        $this->service->searchKostWithActiveMamipay('exclusive');
    }

    public function testSearchKostWithActiveMamipayWithNullConsultantId(): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => ['title' => 'exclusive'],
                        ],
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ['type' => 'kos']],
                                    ['term' => ['is_mamipay_active' => true]]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with($params)
            ->andReturn([]);

        $this->service->searchKostWithActiveMamipay('exclusive', null);
    }

    public function testSearchKostWithActiveMamipayWithConsultantId(): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => ['title' => 'exclusive'],
                        ],
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    ['term' => ['type' => 'kos']],
                                    ['term' => ['is_mamipay_active' => true]],
                                    ['term' => ['consultant_ids' => 1]]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')
            ->once()
            ->with($params)
            ->andReturn([]);

        $this->service->searchKostWithActiveMamipay('exclusive', 1);
    }
}
