<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\Document\DocumentType;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ConsultantDocumentServiceTest extends MamiKosTestCase
{
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(ConsultantDocumentService::class);
        Storage::fake('s3');
    }

    protected function tearDown(): void
    {
        Storage::fake('s3');
        parent::tearDown();
    }

    public function testStoreShouldStoreIntoDatabse(): void
    {
        $file = UploadedFile::fake()->create('document.pdf', 1500);

        $this->service->store($file, DocumentType::SALES_MOTION_PROGRESS());

        $this->assertDatabaseHas('consultant_document', [
            'file_name' => $file->hashName(),
            'file_path' => (config('consultant.document.folder_location') . '/' . DocumentType::SALES_MOTION_PROGRESS()),
            'documentable_type' => null,
            'documentable_id' => null
        ]);
    }

    public function testStoreShouldSaveFile(): void
    {
        $file = UploadedFile::fake()->create('document.pdf', 1500);

        $this->service->store($file, DocumentType::SALES_MOTION_PROGRESS());

        Storage::assertExists(
            config('consultant.document.folder_location') . '/' . DocumentType::SALES_MOTION_PROGRESS() . '/' . $file->hashName()
        );
    }

    public function testDeleteShouldDeleteDatabaseRecord(): void
    {
        $file = UploadedFile::fake()->create('document.pdf', 1500);
        $document = factory(ConsultantDocument::class)->create([
            'file_path' => '/var',
            'file_name' => $file->hashName()
        ]);
        $file->store('/var');

        $this->service->delete($document->id);

        $this->assertSoftDeleted('consultant_document', ['id' => $document->id]);
    }

    public function testDeleteShouldDeleteFileInStorage(): void
    {
        $file = UploadedFile::fake()->create('document.pdf', 1500);
        $document = factory(ConsultantDocument::class)->create([
            'file_path' => '/var',
            'file_name' => $file->hashName()
        ]);
        $file->store('/var');

        $this->service->delete($document->id);

        Storage::assertMissing('/var' . '/' . $file->hashName());
    }

    public function testDeleteWithInvalidIdShouldThrow(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service->delete(0);
    }

    public function testAttachShouldUpdateDatabase(): void
    {
        $document = factory(ConsultantDocument::class)->create([
            'documentable_type' => null,
            'documentable_id' => null
        ]);
        $progress = factory(SalesMotionProgress::class)->create();
        $this->service->attach($document->id, $progress);

        $this->assertDatabaseHas('consultant_document', [
            'documentable_type' => 'sales_motion_progress',
            'documentable_id' => $progress->id
        ]);
    }

    public function testAttachWithInvalidIdShouldThrow(): void
    {
        $progress = factory(SalesMotionProgress::class)->create();

        $this->expectException(ModelNotFoundException::class);

        $this->service->attach(0, $progress);
    }
}
