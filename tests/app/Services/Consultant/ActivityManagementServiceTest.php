<?php

namespace App\Services\Consultant;

use App\Criteria\Consultant\ActivityManagement\BacklogTaskCriteria;
use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Services\Consultant\ActivityManagementService;
use App\Test\MamiKosTestCase;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\Consultant\PotentialTenantRepository;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\RoomRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery;

class ActivityManagementServiceTest extends MamiKosTestCase
{
    protected $service;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(ActivityManagementService::class);
        $this->user = factory(User::class)->create();
        $this->user->consultant()->save(
            factory(Consultant::class)->make()
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testGetProgressableRepositoryWithContract()
    {
        $actual = $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', [ActivityProgress::MORPH_TYPE_CONTRACT]);

        $this->assertInstanceOf(ContractRepository::class, $actual);
    }

    public function testGetProgressableRepositoryWithRoom()
    {
        $actual = $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', [ActivityProgress::MORPH_TYPE_PROPERTY]);

        $this->assertInstanceOf(RoomRepository::class, $actual);
    }

    public function testGetProgressableRepositoryWithPotentialTenant()
    {
        $actual = $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', [ActivityProgress::MORPH_TYPE_DBET]);

        $this->assertInstanceOf(PotentialTenantRepository::class, $actual);
    }

    public function testGetProgressableRepositoryWithPotentialProperty()
    {
        $actual = $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', [ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY]);

        $this->assertInstanceOf(PotentialPropertyRepository::class, $actual);
    }

    public function testGetProgressableRepositoryWithPotentialOwner()
    {
        $actual = $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', [ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER]);

        $this->assertInstanceOf(PotentialOwnerRepository::class, $actual);
    }

    public function testGetProgressableRepositoryWithInvalid()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->callNonPublicMethod(ActivityManagementService::class, 'getProgressableRepository', ['test']);
    }

    public function testGetBacklogProgressable()
    {
        $repo = $this->mock(RoomRepository::class);

        $this->instance(RoomRepository::class, $repo);
        $repo->shouldReceive('pushCriteria')
            ->withArgs(function ($arg) {
                return $arg instanceof BacklogTaskCriteria;
            });

        $this->service->getBacklogProgressable(ActivityProgress::MORPH_TYPE_PROPERTY, 0, 0);
    }

    public function testCreateNewTasksWithContract(): void
    {
        $contract = factory(MamipayContract::class)->create();
        $response = $this->service->createNewTasks(
            new Collection([$contract]),
            1,
            1
        );

        $this->assertCount(1, $response);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $contract->id
        ]);
    }

    public function testCreateNewTasksWithRoom(): void
    {
        $room = factory(Room::class)->create();
        $response = $this->service->createNewTasks(
            new Collection([$room]),
            1,
            1
        );

        $this->assertCount(1, $response);
        $this->assertDatabaseHas('consultant_activity_progress', [
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $room->id
        ]);
    }

    public function testRestoreBackloggedTasks(): void
    {
        $softDeletedTask = factory(ActivityProgress::class)->create([
            'deleted_at' => Carbon::now(),
            'progressable_id' => 1,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1
        ]);
        $response = $this->service->restoreBackloggedTasks(1, [1], 1);
        $this->assertTrue($response[0]->is($softDeletedTask));
        $this->assertDatabaseHas('consultant_activity_progress', [
            'id' => $softDeletedTask->id,
            'deleted_at' => null
        ]);
    }

    public function testGetToDoProgressableWithContract(): void
    {
        $expected = factory(MamipayContract::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_CONTRACT, 1, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithRoom(): void
    {
        $expected = factory(Room::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_PROPERTY, 1, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithPotentialTenant(): void
    {
        $expected = factory(PotentialTenant::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_DBET, 1, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithPotentialProperty(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithPotentialOwner(): void
    {
        $expected = factory(PotentialOwner::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, 1, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithNullConsultantIdShouldFindAnyConsultantTask(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, null)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableWithoutConsultantIdShouldFindAnyConsultantTask(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $expected->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetToDoProgressableShouldNotFindOtherStage(): void
    {
        $room = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 1
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first();
        $this->assertNull($actual);
    }

    public function testGetToDoProgressableShouldNotFindOtherFunnel(): void
    {
        $room = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 0,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first();
        $this->assertNull($actual);
    }

    public function testGetToDoProgressableShouldNotFindOtherConsultant(): void
    {
        $room = factory(PotentialProperty::class)->create();
        $task = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 0,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first();
        $this->assertNull($actual);
    }

    public function testGetToDoProgressableShouldNotEagerLoadOtherConsultant(): void
    {
        $room = factory(PotentialProperty::class)->create();
        $expected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);
        $notExpected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 0,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first()
            ->progress;
        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual[0]->is($expected)
        );
    }

    public function testGetToDoProgressableShouldNotEagerLoadOtherFunnel(): void
    {
        $room = factory(PotentialProperty::class)->create();
        $expected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 1,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);
        $notExpected = factory(ActivityProgress::class)->create([
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $room->id,
            'consultant_activity_funnel_id' => 0,
            'consultant_id' => 1,
            'current_stage' => 0
        ]);

        $actual = $this->service
            ->getToDoProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, 1, 1)
            ->first()
            ->progress;
        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual[0]->is($expected)
        );
    }

    public function testGetToDoProgressableWithInvalidProgressableShouldThrow(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service
            ->getToDoProgressable('test', 1, 1)
            ->first();
    }

    public function testGetStageProgressableWithContract(): void
    {
        $expected = factory(MamipayContract::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_CONTRACT, $funnel->id, $form->id, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetStageProgressableWithRoom(): void
    {
        $expected = factory(Room::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_PROPERTY, $funnel->id, $form->id, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetStageProgressableWithPotentialTenant(): void
    {
        $expected = factory(PotentialTenant::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_DBET, $funnel->id, $form->id, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetStageProgressableWithPotentialProperty(): void
    {
        $expected = factory(PotentialProperty::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY, $funnel->id, $form->id, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetStageProgressableWithPotentialOwner(): void
    {
        $expected = factory(PotentialOwner::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $task = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $expected->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, $funnel->id, $form->id, 1)
            ->first();
        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetStageProgressableShouldNotEagerLoadOtherConsultant(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $expected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owner->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);
        $notExpected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owner->id,
            'current_stage' => $form->stage,
            'consultant_id' => 2
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, $funnel->id, $form->id, 1)
            ->first()
            ->progress;

        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual[0]->is($expected)
        );
        $this->assertFalse(
            $actual[0]->is($notExpected)
        );
    }

    public function testGetStageProgressableShouldNotEagerLoadOtherFunnel(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);
        $expected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owner->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);
        $notExpected = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 0,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
            'progressable_id' => $owner->id,
            'current_stage' => $form->stage,
            'consultant_id' => 1
        ]);

        $actual = $this->service
            ->getStageProgressable(ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER, $funnel->id, $form->id, 1)
            ->first()
            ->progress;

        $this->assertCount(1, $actual);
        $this->assertTrue(
            $actual[0]->is($expected)
        );
        $this->assertFalse(
            $actual[0]->is($notExpected)
        );
    }

    public function testGetBacklogTasksWithPotentialTenant(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertTrue($backlog->progress[0]->progressable->is($tenant));
    }

    public function testGetBacklogTasksWithPotentialTenantShouldReturnMaximumTenData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class, 11)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(10, $backlog->progress);
    }

    public function testGetBacklogTasksWithPotentialTenantShouldSortByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $secondTenant = factory(PotentialTenant::class)->create();
        $firstTenant = factory(PotentialTenant::class)->create(['updated_at' => Carbon::now()->addHour()]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertTrue($backlog->progress[0]->progressable->is($firstTenant));
        $this->assertTrue($backlog->progress[1]->progressable->is($secondTenant));
    }

    public function testGetBacklogTasksWithPotentialTenantShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
            'progressable_id' => factory(PotentialTenant::class)->create()->id,
            'consultant_id' => $this->user->consultant->id
        ]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($tenant));
    }

    public function testGetBacklogTasksWithPotentialTenantShouldNotReturnOtherFunnel(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class)->create();
        factory(MamipayContract::class)->create();
        factory(Room::class)->create();
        factory(PotentialProperty::class)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($tenant));
    }

    public function testGetBacklogTasksWithRoomShouldReturnMaximumTenData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $room = factory(Room::class, 11)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(10, $backlog->progress);
    }

    public function testGetBacklogTasksWithRoomShouldSortByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $secondRoom = factory(Room::class)->create();
        $firstRoom = factory(Room::class)->create(['updated_at' => Carbon::now()->addHour()]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertTrue($backlog->progress[0]->progressable->is($firstRoom));
        $this->assertTrue($backlog->progress[1]->progressable->is($secondRoom));
    }

    public function testGetBacklogTasksWithPotentialProperty(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);
        $potentialProperty = factory(PotentialProperty::class)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($potentialProperty));
    }

    public function testGetBacklogTasksWithPotentialPropertyShouldReturnMaximumTenData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);
        $potentialProperty = factory(PotentialProperty::class, 11)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(10, $backlog->progress);
    }

    public function testGetBacklogTasksWithPotentialPropertyShouldSortByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);
        $secondPotentialProperty = factory(PotentialProperty::class)->create();
        $firstPotentialProperty = factory(PotentialProperty::class)->create(['updated_at' => Carbon::now()->addHour()]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertTrue($backlog->progress[0]->progressable->is($firstPotentialProperty));
        $this->assertTrue($backlog->progress[1]->progressable->is($secondPotentialProperty));
    }

    public function testGetBacklogTasksWithPotentialPropertyShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);
        $potentialProperty = factory(PotentialProperty::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => factory(PotentialProperty::class)->create()->id,
            'consultant_id' => $this->user->consultant->id
        ]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($potentialProperty));
    }

    public function testGetBacklogTasksWithPotentialPropertyShouldNotReturnOtherFunnel(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);
        $potentialProperty = factory(PotentialProperty::class)->create();
        factory(Room::class)->create();
        factory(MamipayContract::class)->create();
        factory(PotentialTenant::class)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($potentialProperty));
    }

    public function testGetBacklogTasksWithMamipayContract(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $contract = factory(MamipayContract::class)
            ->create(['tenant_id' => factory(MamipayTenant::class)->create()->id]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($contract));
    }

    public function testGetBacklogTasksWithMamipayContractShouldReturnMaximumTenData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $contract = factory(MamipayContract::class, 11)
            ->create(['tenant_id' => factory(MamipayTenant::class)->create()->id]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(10, $backlog->progress);
    }

    public function testGetBacklogTasksWithMamipayContractShouldSortByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $secondMamipayContract = factory(MamipayContract::class)
            ->create(['tenant_id' => factory(MamipayTenant::class)->create()->id]);
        $firstMamipayContract = factory(MamipayContract::class)
            ->create([
                'tenant_id' => factory(MamipayTenant::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertTrue($backlog->progress[0]->progressable->is($firstMamipayContract));
        $this->assertTrue($backlog->progress[1]->progressable->is($secondMamipayContract));
    }

    public function testGetBacklogTasksWithMamipayContractShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $contract = factory(MamipayContract::class)
            ->create(['tenant_id' => factory(MamipayTenant::class)->create()->id]);
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
            'progressable_id' => factory(MamipayContract::class)->create()->id,
            'consultant_id' => $this->user->consultant->id
        ]);

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id, $this->user->consultant->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($contract));
    }

    public function testGetBacklogTasksWithMamipayContractShouldNotReturnOtherFunnel(): void
    {
        $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $contract = factory(MamipayContract::class)
            ->create(['tenant_id' => factory(MamipayTenant::class)->create()->id]);
        factory(Room::class)->create();
        factory(PotentialProperty::class)->create();
        factory(PotentialTenant::class)->create();

        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [$funnel->id]);
        $this->assertInstanceOf(BacklogStage::class, $backlog);
        $this->assertCount(1, $backlog->progress);
        $this->assertTrue($backlog->progress[0]->progressable->is($contract));
    }

    public function testGetBacklogTasksWithInvalidId(): void
    {
        $backlog = $this->callNonPublicMethod(ActivityManagementService::class, 'getBacklogTasks', [0]);
        $this->assertEmpty($backlog->progress);
    }

    public function testGetToDoTasks(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);

        $toDo = $this->callNonPublicMethod(ActivityManagementService::class, 'getToDoTasks', [$funnel->id, $this->user->consultant->id]);

        $this->assertInstanceOf(ToDoStage::class, $toDo);
        $this->assertTrue($toDo->progress[0]->is($progress));
    }

    public function testGetToDoTasksShouldReturnTenTasks(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        factory(ActivityProgress::class, 11)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);

        $toDo = $this->callNonPublicMethod(ActivityManagementService::class, 'getToDoTasks', [$funnel->id, $this->user->consultant->id]);

        $this->assertInstanceOf(ToDoStage::class, $toDo);
        $this->assertCount(10, $toDo->progress);
    }

    public function testGetToDoTasksShouldSortByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $secondProgress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);
        $firstProgress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'updated_at' => Carbon::now()->addHour(),
            'consultant_id' => $this->user->consultant->id
        ]);

        $toDo = $this->callNonPublicMethod(ActivityManagementService::class, 'getToDoTasks', [$funnel->id, $this->user->consultant->id]);

        $this->assertInstanceOf(ToDoStage::class, $toDo);
        $this->assertTrue($toDo->progress[0]->is($firstProgress));
        $this->assertTrue($toDo->progress[1]->is($secondProgress));
    }

    public function testGetToDoTasksShouldNotReturnOtherFunnelTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => 0,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);

        $toDo = $this->callNonPublicMethod(ActivityManagementService::class, 'getToDoTasks', [$funnel->id, $this->user->consultant->id]);

        $this->assertInstanceOf(ToDoStage::class, $toDo);
        $this->assertCount(1, $toDo->progress);
        $this->assertTrue($toDo->progress[0]->is($progress));
    }

    public function testGetToDoTasksShouldNotReturnOtherStage(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $progress = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 0,
            'consultant_id' => $this->user->consultant->id
        ]);
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'current_stage' => 1,
            'consultant_id' => $this->user->consultant->id
        ]);

        $toDo = $this->callNonPublicMethod(ActivityManagementService::class, 'getToDoTasks', [$funnel->id, $this->user->consultant->id]);

        $this->assertInstanceOf(ToDoStage::class, $toDo);
        $this->assertCount(1, $toDo->progress);
        $this->assertTrue($toDo->progress[0]->is($progress));
    }

    public function testGetStagesWithTasksWithInvalidId(): void
    {
        $stages = $this->service->getStagesWithTasks(0);
        $this->assertSame(0, $stages['stage_count']);
        $this->assertEmpty($stages['stages']);
    }

    public function testGetStagesWithTasksShouldReturnCorrectStageOrder(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        $first = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        $second = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);

        $stages = $this->service->getStagesWithTasks($funnel->id);
        $this->assertInstanceOf(BacklogStage::class, $stages['stages'][0]);
        $this->assertInstanceOf(ToDoStage::class, $stages['stages'][1]);
        $this->assertTrue($stages['stages'][2]->is($first));
        $this->assertTrue($stages['stages'][3]->is($second));
        $this->assertSame(2, $stages['stage_count']);
    }

    public function testGetStagesWithTasksWithOnlyBacklogTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);
        $property = factory(PotentialProperty::class)->create();
        $stages = $this->service->getStagesWithTasks($funnel->id);
        $this->assertTrue($stages['stages'][0]->progress[0]->progressable->is($property));
        $this->assertEmpty($stages['stages'][1]->progress);
        $this->assertEmpty($stages['stages'][2]->progress);
        $this->assertEmpty($stages['stages'][3]->progress);
        $this->assertSame(2, $stages['stage_count']);
    }

    public function testGetStagesWithTasksWithOnlyToDoTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);
        $property = factory(PotentialProperty::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $property->id,
            'consultant_id' => $this->user->consultant->id,
            'current_stage' => 0
        ]);
        $stages = $this->service->getStagesWithTasks($funnel->id, $this->user->consultant->id);
        $this->assertEmpty($stages['stages'][0]->progress);
        $this->assertTrue($stages['stages'][1]->progress[0]->progressable->is($property));
        $this->assertEmpty($stages['stages'][2]->progress);
        $this->assertEmpty($stages['stages'][3]->progress);
        $this->assertSame(2, $stages['stage_count']);
    }

    public function testGetStagesWithTasksWithOtherStageTask(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 2
        ]);
        $property = factory(PotentialProperty::class)->create();
        factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => $property->id,
            'consultant_id' => $this->user->consultant->id,
            'current_stage' => 1
        ]);
        $stages = $this->service->getStagesWithTasks($funnel->id, $this->user->consultant->id);
        $this->assertEmpty($stages['stages'][0]->progress);
        $this->assertEmpty($stages['stages'][1]->progress);
        $this->assertTrue($stages['stages'][2]->progress[0]->progressable->is($property));
        $this->assertEmpty($stages['stages'][3]->progress);
        $this->assertSame(2, $stages['stage_count']);
    }

    public function testGetStagesWithTasksWithOtherStageTaskShouldReturnMaximumTenData(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);
        factory(ActivityProgress::class, 11)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => factory(PotentialProperty::class)->create()->id,
            'current_stage' => 1
        ]);
        $stages = $this->service->getStagesWithTasks($funnel->id);
        $this->assertCount(10, $stages['stages'][2]->progress);
        $this->assertSame(1, $stages['stage_count']);
    }

    public function testGetStageWithTaskWithOtherStageTaskShouldSortyByUpdatedAtDesc(): void
    {
        $funnel = factory(ActivityFunnel::class)->create([
            'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY
        ]);
        factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'stage' => 1
        ]);

        $second = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => factory(PotentialProperty::class)->create()->id,
            'current_stage' => 1
        ]);
        $first = factory(ActivityProgress::class)->create([
            'consultant_activity_funnel_id' => $funnel->id,
            'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
            'progressable_id' => factory(PotentialProperty::class)->create()->id,
            'current_stage' => 1,
            'updated_at' => Carbon::now()->addHour()
        ]);
        $stages = $this->service->getStagesWithTasks($funnel->id);
        $this->assertTrue($stages['stages'][2]->progress[0]->is($first));
        $this->assertTrue($stages['stages'][2]->progress[1]->is($second));
        $this->assertSame(1, $stages['stage_count']);
    }
}
