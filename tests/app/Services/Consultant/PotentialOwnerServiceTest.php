<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\PotentialOwner;
use App\Services\PotentialOwner\SearchInElasticSearch;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use InvalidArgumentException;

class PotentialOwnerServiceTest extends MamiKosTestCase
{
    protected $searchService;
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->searchService = $this->mockAlternatively(SearchInElasticSearch::class);
        $this->service = $this->app->make(PotentialOwnerService::class);
    }

    public function testCheckRequiredOptionsWithMissingNameShouldThrow(): void
    {
        $this->expectException(InvalidArgumentException::class, 'Missing argument name');
        $this->callNonPublicMethod(PotentialOwnerService::class, 'checkRequiredOptions', [['phone_number' => '0811111111']]);
    }

    public function testCheckRequiredOptionsWithMissingPhoneNumberShouldThrow(): void
    {
        $this->expectException(InvalidArgumentException::class, 'Missing argument phone_number');
        $this->callNonPublicMethod(PotentialOwnerService::class, 'checkRequiredOptions', [['name' => 'John Doe']]);
    }

    public function testCheckRequiredOptionsShouldNotThrow(): void
    {
        $this->callNonPublicMethod(
            PotentialOwnerService::class,
            'checkRequiredOptions',
            [
                [
                    'phone_number' => '0811111111',
                    'name' => 'John Doe',
                    'user_id' => 1
                ]
            ]
        );
    }

    public function testMakeSurePhoneNumberAndEmailUniqueUniqueWithNonUniquePhoneNumber(): void
    {
        factory(PotentialOwner::class)->create(['phone_number' => '0811111111']);
        $this->expectException(InvalidArgumentException::class, 'Phone number or email is not unique');
        $this->callNonPublicMethod(PotentialOwnerService::class, 'makeSurePhoneNumberAndEmailUnique', ['0811111111', 'John Doe']);
    }

    public function testMakeSurePhoneNumberAndEmailUniqueUniqueWithNonUniqueEmail(): void
    {
        factory(PotentialOwner::class)->create(['email' => 'john@doe.com']);
        $this->expectException(InvalidArgumentException::class, 'Phone number or email is not unique');
        $this->callNonPublicMethod(PotentialOwnerService::class, 'makeSurePhoneNumberAndEmailUnique', ['0811111111', 'john@doe.com']);
    }

    public function testMakeSurePhoneNumberAndEmailUniqueUniqueShouldNotThrow(): void
    {
        factory(PotentialOwner::class)->create([
            'phone_number' => '0811111111',
            'email' => 'jane@doe.com'
        ]);
        $this->callNonPublicMethod(PotentialOwnerService::class, 'makeSurePhoneNumberAndEmailUnique', ['0822222222', 'john@doe.com']);
    }

    public function testMakeSurePhoneNumberAndEmailUniqueWithEmptyEmail(): void
    {
        factory(PotentialOwner::class)->create([
            'phone_number' => '0811111111',
            'email' => ''
        ]);
        $this->callNonPublicMethod(PotentialOwnerService::class, 'makeSurePhoneNumberAndEmailUnique', ['0822222222', '']);
    }

    public function testMakeSurePhoneNumberAndEmailUniqueWithNullEmail(): void
    {
        factory(PotentialOwner::class)->create([
            'phone_number' => '0811111111',
            'email' => null
        ]);
        $this->callNonPublicMethod(PotentialOwnerService::class, 'makeSurePhoneNumberAndEmailUnique', ['0822222222', null]);
    }

    public function testStoreShouldCreateNewPotentialOwner(): void
    {
        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => 'jane@doe.com'
            ],
            1
        );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => '811111111',
            'email' => 'jane@doe.com',
            'deleted_at' => null,
            'created_by' => 1
        ]);
    }
    
    public function testStoreShouldPreventDuplicateEmail(): void
    {
        factory(PotentialOwner::class)->create([
            'phone_number' => '0811111112',
            'email' => 'jane@doe.com',
        ]);

        $this->expectException(InvalidArgumentException::class, 'Phone number or email is not unique');
        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => 'jane@doe.com'
            ],
            1
        );
    }

    public function testStoreShouldPreventDuplicatePhoneNumber(): void
    {
        factory(PotentialOwner::class)->create([
            'phone_number' => '0811111111',
            'email' => 'jane@doe.cot',
        ]);

        $this->expectException(InvalidArgumentException::class, 'Phone number or email is not unique');
        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => 'jane@doe.cot'
            ],
            1
        );
    }

    public function testStoreWithNullEmailShouldSaveCorrectly(): void
    {
        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => null
            ],
            1
        );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => '811111111',
            'email' => null,
            'created_by' => 1
        ]);
    }

    public function testStoreWithNullDuplicateEmailShouldSaveCorrectly(): void
    {
        factory(PotentialOwner::class)->create([
            'name' => 'John Doe',
            'phone_number' => '82222222222',
            'email' => null
        ]);

        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => null
            ],
            1
        );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => '811111111',
            'email' => null,
            'created_by' => 1
        ]);
    }

    public function testStoreWithEmptyEmailShouldSaveCorrecly(): void
    {
        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => ''
            ],
            1
        );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => '811111111',
            'email' => null,
            'created_by' => 1
        ]);
    }

    public function testStoreWithEmptyDuplicateEmailShouldSaveCorrectly(): void
    {
        factory(PotentialOwner::class)->create([
            'name' => 'John Doe',
            'phone_number' => '82222222222',
            'email' => ''
        ]);

        $this->service->store(
            [
                'name' => 'Jane Doe',
                'phone_number' => '0811111111',
                'email' => ''
            ],
            1
        );

        $this->assertDatabaseHas('consultant_potential_owner', [
            'name' => 'Jane Doe',
            'phone_number' => '811111111',
            'email' => null,
            'created_by' => 1
        ]);
    }

    public function testSearch(): void
    {
        $params = [];
        $this->searchService->shouldReceive('search')->with($params)->once();
        $this->service->search($params);
    }
}
