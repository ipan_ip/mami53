<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use InvalidArgumentException;
use Mockery;
use ReflectionClass;

class ConsultantRoomIndexServiceTest extends MamiKosTestCase
{
    protected $service;
    protected $elasticsearch;
    protected $repository;
    protected $mapping;

    protected function setUp(): void
    {
        parent::setUp();
        $this->elasticsearch = $this->mockAlternatively('alias:Elasticsearch');
        $this->repository = $this->mockAlternatively(RoomRepository::class);
        $this->service = $this->app->make(ConsultantRoomIndexService::class);
        $this->mapping = factory(ConsultantRoom::class)->make(['designer_id' => 155]);
    }

    private function repositoryShouldReturn(Room $room): void
    {
        $this->repository
            ->shouldReceive('find')
            ->with($room->id)
            ->once()
            ->andReturn($room);
    }

    private function mockActiveKost(?bool $getFromElasticsearch): Room
    {
        $room = $this->mock(Room::class);
        $room->shouldReceive('getAttribute')->with('is_test_data')->andReturn(false);
        $room->shouldReceive('getAttribute')->with('is_active')->andReturn('true');
        $room->shouldReceive('getAttribute')->with('expired_phone')->andReturn(0);
        $room->shouldReceive('getAttribute')->with('deleted_at')->andReturn(null);
        $room->shouldReceive('getAttribute')->with('song_id')->andReturn(122);
        $room->shouldReceive('getAttribute')->with('id')->andReturn(155);
        $room->shouldReceive('getFromElasticsearch')->once()->andReturn($getFromElasticsearch);

        return $room;
    }

    private function mockSoftDeletedKost(): Room
    {
        $room = $this->mock(Room::class);
        $room->shouldReceive('getAttribute')->with('is_test_data')->andReturn(false);
        $room->shouldReceive('getAttribute')->with('is_active')->andReturn('true');
        $room->shouldReceive('getAttribute')->with('expired_phone')->andReturn(0);
        $room->shouldReceive('getAttribute')->with('deleted_at')->andReturn(Carbon::now());
        $room->shouldReceive('getAttribute')->with('song_id')->andReturn(122);
        $room->shouldReceive('getAttribute')->with('id')->andReturn(155);

        return $room;
    }

    private function mockExpiredPhoneKost(): Room
    {
        $room = $this->mock(Room::class);

        $room->shouldReceive('getAttribute')->with('is_test_data')->andReturn(false);
        $room->shouldReceive('getAttribute')->with('is_active')->andReturn('true');
        $room->shouldReceive('getAttribute')->with('expired_phone')->andReturn(1);
        $room->shouldReceive('getAttribute')->with('deleted_at')->andReturn(null);
        $room->shouldReceive('getAttribute')->with('song_id')->andReturn(122);
        $room->shouldReceive('getAttribute')->with('id')->andReturn(155);

        return $room;
    }

    private function mockNonActiveKost(): Room
    {
        $room = $this->mock(Room::class);

        $room->shouldReceive('getAttribute')->with('is_test_data')->andReturn(false);
        $room->shouldReceive('getAttribute')->with('is_active')->andReturn('false');
        $room->shouldReceive('getAttribute')->with('expired_phone')->andReturn(0);
        $room->shouldReceive('getAttribute')->with('deleted_at')->andReturn(null);
        $room->shouldReceive('getAttribute')->with('song_id')->andReturn(122);
        $room->shouldReceive('getAttribute')->with('id')->andReturn(155);

        return $room;
    }

    private function assertElasticAddMapping(int $designerId, int $consultantId, int $songId): void
    {
        $this->elasticsearch->shouldReceive('update')
            ->withArgs(function ($arg) use ($designerId, $consultantId, $songId) {
                return ($arg['id'] === $designerId) && ($arg['body']['script']['params']['consultant_id'] === $consultantId) && ($arg['body']['script']['params']['song_id'] === $songId);
            })
            ->once();
    }

    private function assertBugsnagNotify(string $exception): void
    {
        Bugsnag::shouldReceive('notifyException')
            ->withArgs(function ($argc) use ($exception) {
                return $argc instanceof $exception;
            })
            ->once();
    }

    public function testCreateWithSoftDeletedMappingShouldThrow(): void
    {
        $this->assertBugsnagNotify(InvalidArgumentException::class);

        $mapping = factory(ConsultantRoom::class)->make(['deleted_at' => Carbon::now()]);
        $this->service->create($mapping);
    }

    public function testCreateWithSoftDeletedRoom(): void
    {
        $room = $this->mockSoftDeletedKost();

        $this->repositoryShouldReturn($room);
        $this->assertBugsnagNotify(InvalidArgumentException::class);
        $this->service->create($this->mapping);
    }

    public function testCreateWithExpiredPhone(): void
    {
        $room = $this->mockExpiredPhoneKost();

        $this->repositoryShouldReturn($room);
        $this->assertBugsnagNotify(InvalidArgumentException::class);
        $this->service->create($this->mapping);
    }

    public function testCreateWithNonActiveRoom(): void
    {
        $room = $this->mockNonActiveKost();

        $this->repositoryShouldReturn($room);
        $this->assertBugsnagNotify(InvalidArgumentException::class);
        $this->service->create($this->mapping);
    }

    public function testCreateWithUnindexedRoom(): void
    {
        $room = $this->mockActiveKost(null);
        $room->shouldReceive('addToElasticsearch')->once();

        $this->repositoryShouldReturn($room);
        $this->assertElasticAddMapping($this->mapping->designer_id, $this->mapping->consultant_id, $room->song_id);

        $this->service->create($this->mapping);
    }

    public function testCreateWithIndexedRoom(): void
    {
        $room = $this->mockActiveKost(true);
        $room->shouldNotReceive('addToElasticsearch');

        $this->repositoryShouldReturn($room);
        $this->assertElasticAddMapping($this->mapping->designer_id, $this->mapping->consultant_id, $room->song_id);

        $this->service->create($this->mapping);
    }

    public function testCreateWithTestData(): void
    {
        $room = $this->mock(Room::class);
        $room->shouldReceive('getAttribute')->with('is_test_data')->andReturn(true);

        $this->elasticsearch->shouldNotReceive('update');
        $this->service->create($this->mapping);
    }

    public function testDeleteShouldNotThrowNotFound(): void
    {
        $mapping = factory(ConsultantRoom::class)->make(['designer_id' => 0]);
        $this->service->delete($mapping);
    }

    public function testDeleteWithUnIndexedRoom(): void
    {
        $room = $this->mockActiveKost(null);

        $this->repositoryShouldReturn($room);
        $this->elasticsearch->shouldNotReceive('update');

        $this->service->delete($this->mapping);
    }

    public function testDeleteShouldCatchError(): void
    {
        $exception = new Exception();
        $this->repository->shouldReceive('find')->with(155)->once()->andThrow($exception);
        $this->assertBugsnagNotify(Exception::class);
        $this->service->delete($this->mapping);
    }

    public function testDelete(): void
    {
        $room = $this->mockActiveKost(true);

        $this->repositoryShouldReturn($room);
        $this->elasticsearch->shouldReceive('update')
            ->withArgs(function ($arg) {
                return ($arg['id'] === $this->mapping->designer_id) && ($arg['body']['script']['params']['consultant_id'] === $this->mapping->consultant_id);
            })
            ->once();

        $this->service->delete($this->mapping);
    }

    public function testBulkDeleteShouldCatchError(): void
    {
        $exception = new Exception();
        $this->repository->shouldReceive('find')->with(155)->andThrow($exception);
        $this->assertBugsnagNotify(Exception::class);

        $this->service->bulkDelete(1, [155]);
    }
}
