<?php

namespace App\Services;

use App\Entities\User\UserVerificationAccount;
use App\Test\MamiKosTestCase;
use App\User;

class UserServiceTest extends MamiKosTestCase
{
    
    protected $premiumService;
    protected $mockedPremiumRequestRepo;
    protected $mockedUser;
    protected $type;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'phone_number' => '08123456789',
            'is_tester' => false
        ]);
        $this->userVerificationAccount = factory(UserVerificationAccount::class)->create([
            'user_id' => $this->user->id,
            'is_verify_phone_number' => 1
        ]);

        $this->userService = $this->app->make(UserService::class);

    }

    public function testGetUsersFromAdmin()
    {
        $result = $this->userService->getUsersFromAdmin(['id' => $this->user->id]);
        $this->assertTrue($result->contains($this->user));
    }

    
    public function testRecyclePhoneNumber()
    {
        $result = $this->userService->recyclePhoneNumber($this->user);
        $this->user->fresh();
        $this->assertTrue($result);
        $this->assertEquals(0, $this->user->user_verification_account->is_verify_phone_number);
        $this->assertEquals(null, $this->user->phone_number);
    }
    
    public function testSetAsTester()
    {
        $result = $this->userService->setAsTester($this->user, true);
        $this->user->fresh();
        $this->assertTrue($result);
        $this->assertEquals(1, $this->user->is_tester);
    }


}
