<?php

namespace App\Services\Polling;

use App\Entities\GoldPlus\Package;
use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Entities\Polling\UserPolling;
use App\Entities\Property\PropertyContractOrder;
use App\Entities\User\Notification;
use App\Services\Polling\PollingService;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PollingServiceTest extends MamiKosTestCase
{
    private $service;

    private $user;
    private $polling;

    protected function setUp()
    {
        parent::setUp();
        
        $this->service = app()->make(PollingService::class);

        $this->user = factory(User::class)->state('owner')->create();

        $this->polling = factory(Polling::class)->create(['is_active' => true]);
    }

    public function testHasSubmittedPolling_ShouldSuccess()
    {
        $result = $this->service->hasSubmittedPolling('abcdef', $this->user);

        $this->assertFalse($result);
    }

    public function testHasSubmittedPolling_BeforeSubmitExitGoldPlus_ShouldSuccess()
    {
        $exitGoldplusPolling = factory(Polling::class)->create([
            'key'       => 'exit_goldplus',
            'is_active' => true
        ]);

        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $exitGoldplusPolling->id
        ]);

        factory(UserPolling::class)->create([
            'user_id'       => $this->user->id,
            'question_id'   => $question->id,
            'created_at'    => Carbon::now()->subDays(1)
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $result = $this->service->hasSubmittedPolling($exitGoldplusPolling->key, $this->user);

        $this->assertFalse($result);
    }

    public function testHasSubmittedPolling_AfterSubmitExitGoldPlus_ShouldSuccess()
    {
        $exitGoldplusPolling = factory(Polling::class)->create([
            'key'       => 'exit_goldplus',
            'is_active' => true
        ]);

        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $exitGoldplusPolling->id
        ]);

        factory(UserPolling::class)->create([
            'user_id'       => $this->user->id,
            'question_id'   => $question->id,
            'created_at'    => Carbon::now()
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $result = $this->service->hasSubmittedPolling($exitGoldplusPolling->key, $this->user);

        $this->assertTrue($result);
    }

    public function testGetPollingDetail_WithInvalidPolling_ShouldFail()
    {
        $result = $this->service->getPollingDetail('abcdef')['data'];
        
        $this->assertNull($result);
    }

    public function testGetPollingDetail_ShouldSuccess()
    {
        $nonOptionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'      => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => true
        ]);
        $optionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $options = factory(PollingQuestionOption::class, 2)->create([
            'question_id'   => $optionQuestion->id
        ]);
        $otherOption = factory(PollingQuestionOption::class)->create([
            'question_id'   => $optionQuestion->id,
            'setting'       => json_encode(['is_other' => 1])
        ]);

        $result = $this->service->getPollingDetail($this->polling->key)['data'];

        $this->assertEquals($this->polling->id, $result['id']);
        $this->assertEquals($this->polling->descr, $result['description']);
        $this->assertEquals(2, count($result['questions']));
    }

    public function testSubmitPolling_WithInvalidPolling_ShouldFail()
    {
        $result = $this->service->submitPolling('abcdef', $this->user, []);

        $this->assertFalse($result['status']);
        $this->assertEquals('Polling does not exist', $result['message']);
        $this->assertEquals('error', $result['action_code']);
    }

    public function testSubmitPolling_WithInvalidQuestion_ShouldSuccess()
    {
        $nonOptionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'      => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => true
        ]);
        $optionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $options = factory(PollingQuestionOption::class, 2)->create([
            'question_id'   => $optionQuestion->id
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $data = [
            'answers' => [
                [
                    'id' => 0,
                    'value' => 'Aku sih YES!'
                ],
                [
                    'id' => $optionQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $options[0]->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $options[1]->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($this->polling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $nonOptionQuestion->id,
            'answer' => 'Aku sih YES!'
        ]);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[0]->id,
            'answer' => false
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[1]->id,
            'answer' => 'Tidak ada comment lain!'
        ]);
    }

    public function testSubmitPolling_WithoutExpiredPropertyContractOrder_ShouldSuccess()
    {
        $nonOptionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'      => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => true
        ]);
        $optionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $options = factory(PollingQuestionOption::class, 2)->create([
            'question_id'   => $optionQuestion->id
        ]);

        $data = [
            'answers' => [
                [
                    'id' => 0,
                    'value' => 'Aku sih YES!'
                ],
                [
                    'id' => $optionQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $options[0]->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $options[1]->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($this->polling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $nonOptionQuestion->id,
            'answer' => 'Aku sih YES!'
        ]);

        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[0]->id,
            'answer' => false
        ]);
        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[1]->id,
            'answer' => 'Tidak ada comment lain!'
        ]);
    }

    public function testSubmitPolling_ShouldSuccess()
    {
        $nonOptionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'      => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => true
        ]);
        $optionQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $this->polling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $options = factory(PollingQuestionOption::class, 2)->create([
            'question_id'   => $optionQuestion->id
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $data = [
            'answers' => [
                [
                    'id' => $nonOptionQuestion->id,
                    'value' => 'Aku sih YES!'
                ],
                [
                    'id' => $optionQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $options[0]->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $options[1]->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($this->polling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $nonOptionQuestion->id,
            'answer' => 'Aku sih YES!'
        ]);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[0]->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[1]->id,
            'answer' => 'Tidak ada comment lain!'
        ]);

        // Re-submit polling
        $data = [
            'answers' => [
                [
                    'id' => $nonOptionQuestion->id,
                    'value' => 'Aku sih NO!'
                ],
                [
                    'id' => $optionQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $options[0]->id,
                            'value' => 'Y'
                        ],
                        [
                            'id' => $options[1]->id,
                            'value' => 'N'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($this->polling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $nonOptionQuestion->id,
            'answer' => 'Aku sih NO!'
        ]);

        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[0]->id,
            'answer' => 'Y'
        ]);
        $this->assertDatabaseMissing((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $optionQuestion->id,
            'option_id' => $options[1]->id,
            'answer' => 'N'
        ]);
    }

    public function testSubmitPolling_ExitGoldplus_ForGp1_WithChooseOtherGpSelected_ShouldSuccess()
    {
        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'package_type'      => Package::CODE_GP1,
            'owner_id'          => $this->user->id
        ]);

        $exitGoldplusPolling = factory(Polling::class)->create([
            'key'       => 'exit_goldplus',
            'is_active' => true
        ]);
        
        $exitGoldplusQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $exitGoldplusPolling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $option1 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option2 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id,
            'setting'       => json_encode([ 'action_code' => 'choose_other_gp' ])
        ]);
        $option3 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option4 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $data = [
            'answers' => [
                [
                    'id' => $exitGoldplusQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $option1->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $option2->id,
                            'value' => 'Y'
                        ],
                        [
                            'id' => $option3->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $option4->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($exitGoldplusPolling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option1->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option2->id,
            'answer' => 'Y'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option3->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option4->id,
            'answer' => 'Tidak ada comment lain!'
        ]);
    }

    public function testSubmitPolling_ExitGoldplus_FotOtherGp_WithChooseOtherGpSelected_ShouldSuccess()
    {
        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'package_type'      => Package::CODE_GP2,
            'owner_id'          => $this->user->id
        ]);

        $exitGoldplusPolling = factory(Polling::class)->create([
            'key'       => 'exit_goldplus',
            'is_active' => true
        ]);
        
        $exitGoldplusQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $exitGoldplusPolling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $option1 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option2 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id,
            'setting'       => json_encode([ 'action_code' => 'choose_other_gp' ])
        ]);
        $option3 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option4 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $data = [
            'answers' => [
                [
                    'id' => $exitGoldplusQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $option1->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $option2->id,
                            'value' => 'Y'
                        ],
                        [
                            'id' => $option3->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $option4->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($exitGoldplusPolling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('choose_other_gp', $result['action_code']);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option1->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option2->id,
            'answer' => 'Y'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option3->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option4->id,
            'answer' => 'Tidak ada comment lain!'
        ]);
    }

    public function testSubmitPolling_ExitGoldplus_WithoutChooseOtherGpSelected_ShouldSuccess()
    {
        $exitGoldplusPolling = factory(Polling::class)->create([
            'key'       => 'exit_goldplus',
            'is_active' => true
        ]);
        
        $exitGoldplusQuestion = factory(PollingQuestion::class)->create([
            'polling_id'    => $exitGoldplusPolling->id,
            'type'          => PollingQuestion::TYPE_CHECKLIST,
            'is_active'     => true
        ]);
        $option1 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option2 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id,
            'setting'       => json_encode([ 'action_code' => 'choose_other_gp' ])
        ]);
        $option3 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);
        $option4 = factory(PollingQuestionOption::class)->create([
            'question_id'   => $exitGoldplusQuestion->id
        ]);

        $order = factory(PropertyContractOrder::class)->create([
            'status'            => 'expired',
            'scheduled_date'    => Carbon::now(),
            'owner_id'          => $this->user->id
        ]);

        factory(Notification::class)->create([
            'user_id'           => $this->user->id,
            'type'              => 'unsubscribe_goldplus',
            'identifier_type'   => 'property_contract',
            'identifier'        => $order->id,
            'created_at'        => $order->scheduled_date
        ]);

        $data = [
            'answers' => [
                [
                    'id' => $exitGoldplusQuestion->id,
                    'value' => '',
                    'options' => [
                        [
                            'id' => $option1->id,
                            'value' => 'Y'
                        ],
                        [
                            'id' => $option2->id,
                            'value' => 'N'
                        ],
                        [
                            'id' => $option3->id,
                            'value' => 'Y'
                        ],
                        [
                            'id' => $option4->id,
                            'value' => 'Tidak ada comment lain!'
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->service->submitPolling($exitGoldplusPolling->key, $this->user, $data);

        $this->assertTrue($result['status']);
        $this->assertEquals('User Polling has been successfully submitted', $result['message']);
        $this->assertEquals('exit', $result['action_code']);

        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option1->id,
            'answer' => 'Y'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option2->id,
            'answer' => 'N'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option3->id,
            'answer' => 'Y'
        ]);
        $this->assertDatabaseHas((new UserPolling)->getTable(), [
            'user_id' => $this->user->id,
            'question_id' => $exitGoldplusQuestion->id,
            'option_id' => $option4->id,
            'answer' => 'Tidak ada comment lain!'
        ]);
    }
}