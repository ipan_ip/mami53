<?php

namespace App\Services\PotentialOwner;

use Mockery;
use App\Test\MamiKosTestCase;
use App\Entities\Consultant\PotentialOwner;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use App\Libraries\Contracts\ElasticSearchClient;

class RemoveFromElasticSearchTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $service;
    protected $elasticsearch;

    protected function setUp(): void
    {
        parent::setUp();

        $this->elasticsearch = Mockery::mock(ElasticsearchClient::class);
        $this->app->instance(ElasticsearchClient::class, $this->elasticsearch);

        $this->service = $this->app->make(RemoveFromElasticSearch::class);       
    }

    public function testRemoveIndex()
    {
        $potOwner = factory(PotentialOwner::class)->create();

        $expected = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'id' => $potOwner->id,
        ];

        $this->elasticsearch
            ->shouldReceive('delete')
            ->with($expected)
            ->once();

        $this->service
            ->process($potOwner);
    }
}