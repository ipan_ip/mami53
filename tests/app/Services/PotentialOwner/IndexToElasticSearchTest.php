<?php

namespace App\Services\PotentialOwner;

use App\Entities\Consultant\Consultant;
use Mockery;
use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Consultant\PotentialOwner;
use App\Libraries\Contracts\ElasticSearchClient;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class IndexToElasticSearchTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $indexService;
    protected $elasticsearch;

    protected function setUp(): void
    {
        parent::setUp();

        $this->elasticsearch = Mockery::mock(ElasticsearchClient::class);
        $this->app->instance(ElasticsearchClient::class, $this->elasticsearch);

        $this->indexService = $this->app->make(IndexToElasticSearch::class);
    }

    public function testIndexCreatedByOwner(): void
    {
        $creator = factory(User::class)->create(['is_owner' => 'true']);
        $potOwner = factory(PotentialOwner::class)->create([
            'phone_number' => '081234',
            'created_by' => $creator->id
        ]);

        $expected = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'id' => $potOwner->id,
            'body' => [
                'id' => $potOwner->id,
                'name' => $potOwner->name,
                'phone_number' => $potOwner->phone_number,
                'email' => $potOwner->email,
                'date_to_visit' => $potOwner->date_to_visit,
                'total_property' => $potOwner->total_property,
                'total_room' => $potOwner->total_room,
                'bbk_status' => $potOwner->bbk_status,
                'followup_status' => $potOwner->followup_status,
                'creator_name' => $creator->name,
                'created_by' => 'owner',
                'updated_at' => $potOwner->updated_at->toDateTimeString(),
                'last_updater_name' => $creator->name
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($expected)
            ->once();

        $this->indexService
            ->process($potOwner);
    }

    public function testIndexCreatedByConsultant(): void
    {
        $creator = factory(User::class)->create();
        factory(Consultant::class)->create(['user_id' => $creator->id]);

        $updater = factory(User::class)->create();
        $potOwner = factory(PotentialOwner::class)->create([
            'created_by' => $creator->id,
            'updated_by' => $updater->id
        ]);

        $expected = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'id' => $potOwner->id,
            'body' => [
                'id' => $potOwner->id,
                'name' => $potOwner->name,
                'phone_number' => $potOwner->phone_number,
                'email' => $potOwner->email,
                'date_to_visit' => $potOwner->date_to_visit,
                'total_property' => $potOwner->total_property,
                'total_room' => $potOwner->total_room,
                'bbk_status' => $potOwner->bbk_status,
                'followup_status' => $potOwner->followup_status,
                'creator_name' => $creator->name,
                'created_by' => 'consultant',
                'updated_at' => $potOwner->updated_at->toDateTimeString(),
                'last_updater_name' => $updater->name
            ]
        ];

        $this->elasticsearch
            ->shouldReceive('index')
            ->with($expected)
            ->once();

        $this->indexService
            ->process($potOwner);
    }

    public function testProcessShouldThrowError(): void
    {
        $potOwner = $this->mock(PotentialOwner::class);

        $potOwner->shouldReceive('offsetExists')
            ->with('id')
            ->andReturn(null);

        $this->expectException(IdEmptyError::class);

        $this->indexService->process($potOwner);
    }
}
