<?php

namespace app\Services\Thanos;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Services\Thanos\ThanosService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Request;

class ThanosServiceTest extends MamiKosTestCase
{
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRoomIdNotFound()
    {
        $this->expectException(\Exception::class);

        $helper = new ThanosService();
        $helper->snap(0, "test", Request::ip());
    }

    public function testSnapActiveRoom()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->state('active')->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $helper = new ThanosService();
        $thanos = $helper->snap($room->id, "test", Request::ip());

        $this->assertNotNull($thanos);

        //assert room
        $this->assertDatabaseHas('designer', [
            "id" => $thanos->room->id,
            "is_active" => 'false'
        ]);

        // assert thanos data
        $this->assertDatabaseHas('designer_hidden_thanos', [
            "designer_id" => $thanos->room->id,
            "snapped" => true
        ]);

        // assert notification
        $this->assertDatabaseHas('notification', [
            "designer_id" => $thanos->room->id,
            "user_id" => $user->id,
            "type" => "thanos_activate_reminder"
        ]);
    }

    public function testSnapInactiveRoom()
    {
        $this->expectException(\Exception::class);

        $room = factory(Room::class)->create();

        $helper = new ThanosService();
        $thanos = $helper->snap($room->id, "test", Request::ip());
    }

    public function testRevertActiveRoom()
    {
        $this->expectException(\Exception::class);

        $room = factory(Room::class)->state('active')->create();

        $helper = new ThanosService();
        $thanos = $helper->revert($room->id, "test", Request::ip());
    }

    public function testRevertRoomAlreadyInactive()
    {
        $this->expectException(\Exception::class);
        $room = factory(Room::class)->create();

        $helper = new ThanosService();
        $thanos = $helper->revert($room->id, "test", Request::ip());
    }

    public function testRevertRoomGetThanosSnap()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->state('active')->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $helper = new ThanosService();
        $snap = $helper->snap($room->id, "test", Request::ip());
        $this->assertNotNull($snap);

        $revert = $helper->revert($room->id, "test", Request::ip());
        $this->assertNotNull($revert);
        $this->assertDatabaseHas('designer', [
            "id" => $revert->room->id,
            "is_active" => 'true'
        ]);
        // assert thanos data
        $this->assertDatabaseMissing('designer_hidden_thanos', [
            "designer_id" => $revert->room->id,
            "snapped" => true
        ]);
        $this->assertDatabaseHas('designer_hidden_thanos', [
            "designer_id" => $revert->room->id,
            "snapped" => false
        ]);
        // assert notification
        $this->assertDatabaseHas('notification', [
            "designer_id" => $snap->room->id,
            "user_id" => $user->id,
            "type" => "thanos_activate_reminder"
        ]);
    }

    public function testNotifyThanosToApp()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->state('active')->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $thanosHidden = factory(ThanosHidden::class)->create([
            'designer_id' => $room->id
        ]);

        $helper = new ThanosService();
        $this->assertTrue($helper->notifyThanosToApp($thanosHidden));

        // assert notification
        $this->assertDatabaseHas('notification', [
            "designer_id" => $thanosHidden->room->id,
            "user_id" => $user->id,
            "type" => "thanos_activate_reminder"
        ]);
    }

    public function testNotifyAppByDate()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->state('active')->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);
        $thanosHidden = factory(ThanosHidden::class)->create([
            'designer_id' => $room->id
        ]);

        $helper = new ThanosService();
        $helper->notifyAppByDate(1);

        $this->assertDatabaseHas('notification', [
            "designer_id" => $thanosHidden->room->id,
            "user_id" => $user->id,
            "type" => "thanos_activate_reminder"
        ]);
    }
}
