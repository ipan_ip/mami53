<?php
namespace app\Services\Thanos;

use App\Entities\Log\ThanosBookingRequestLog;
use App\Test\MamiKosTestCase;
use App\Services\Thanos\ThanosBookingRequestService;
use RuntimeException;

/**
 * Class ThanosBookingRequestServiceTest
 * This class purposed for unittest 
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class ThanosBookingRequestServiceTest extends MamiKosTestCase
{
    
    private const CSV_FILE = 'booking_owner_request_waiting.csv';
    private const CSV_COLUMN_SEPARATOR = ',';

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testBuildArrayOfBookingRequestIdExpectArgumentCountErrorException()
    {
        $csvFile = str_random().DIRECTORY_SEPARATOR.str_random();
        $this->expectException(\ArgumentCountError::class);

        ThanosBookingRequestService::buildArrayOfBookingRequestId();
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testBuildArrayOfBookingRequestIdExpectRuntimeException()
    {
        $csvFile = str_random().DIRECTORY_SEPARATOR.str_random();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage(__('service.thanos-request-booking.error.runtime-exception.file-not-found'));

        ThanosBookingRequestService::buildArrayOfBookingRequestId($csvFile);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testBuildArrayOfBookingRequestIdExpectRuntimeExceptionInvalidCsv()
    {
        $csvFile = __FILE__;
        $this->expectException(RuntimeException::class);

        ThanosBookingRequestService::buildArrayOfBookingRequestId($csvFile);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testBuildArrayOfBookingRequestIdSuccess()
    {
        $csvFile = self::CSV_FILE;
        $resultExpectedArray = ThanosBookingRequestService::buildArrayOfBookingRequestId($csvFile);
        $resultArray = $this->readCsvFile($csvFile);

        $this->assertEquals($resultArray, $resultExpectedArray);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testSaveToLogExpectRuntimeException()
    {
        $data = [];
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage(__('service.thanos-request-booking.error.empty-param-data'));
        ThanosBookingRequestService::saveToLog($data);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testGetThanosBookingRequestLogCountSuccess()
    {
        $countExpected = factory(ThanosBookingRequestLog::class)->create([]);
        $this->assertNotEmpty($countExpected);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testIsCsvFileReturnTrue()
    {
        $csvFile = storage_path("csv-booking").DIRECTORY_SEPARATOR.self::CSV_FILE;
        $result = $this->getNonPublicMethodFromClass(ThanosBookingRequestService::class, 'isCsvFile')
            ->invokeArgs(new ThanosBookingRequestService(), [$csvFile]);
        
        $this->assertTrue($result);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Services/Thanos/ThanosBookingRequestService
     */
    public function testIsCsvFileReturnFalse()
    {
        $csvFile = __FILE__;
        $result = $this->getNonPublicMethodFromClass(ThanosBookingRequestService::class, 'isCsvFile')
            ->invokeArgs(new ThanosBookingRequestService(), [$csvFile]);
        
        $this->assertFalse($result);
    }

    /**
     * Read csv file
     * 
     * @param string $file
     * @return array
     */
    private function readCsvFile(string $file): array
    {
        $csvFile = storage_path("csv-booking").DIRECTORY_SEPARATOR.$file;
        //Initialize bookingRequestId
        $bookingRequestId = [];

        //Open the file
        $fp = fopen($csvFile, 'r');

        //Do read file line by line -> for effectiveness 
        while (!feof($fp)) {
            $oneLine = fgets($fp);
            $parts = explode(self::CSV_COLUMN_SEPARATOR, trim($oneLine));
            
            //Make the id and status as array elements
            $id = (!empty($parts[0])) ? str_replace(',', '', $parts[0]) : -1;
            $status = (!empty($parts[1])) ? $parts[1] : '---';

            //Define var
            $bookingRequestId[$id] = [
                'id' => (int) $id,
                'status' => (string) $status,
            ];
        }

        //close the resources and delete unused memory
        fclose($fp);
        unset($fp);

        //Return the result
        return $bookingRequestId;
    }
}
