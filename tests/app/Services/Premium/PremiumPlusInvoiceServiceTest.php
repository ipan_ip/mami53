<?php
namespace App\Services\Premium;

use App;
use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPlusInvoice;
use App\Entities\Premium\PremiumPlusUser;
use App\Services\Premium\PremiumPlusInvoiceService;
use App\User;
use App\Veritrans\Midtrans;
use GuzzleHttp\Psr7\Response;
use App\Entities\Premium\Payment;

class PremiumPlusInvoiceTest extends MamiKosTestCase
{
    
    protected $service;
    public function setUp(): void
    {
        parent::setUp();
        $this->service = App::make(PremiumPlusInvoiceService::class);
    }

    public function testExpiredTime()
    {
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'due_date' => date('Y-m-d H:i:s')
        ]);

        $expiredDate = $this->service->expiredDate($premiumPlusInvoice);
        $this->assertEquals("day", $expiredDate['unit']);
    }

    public function testGetOwnerDetail()
    {
        $email = 'saya@mail.com';
        $premiumPlusUser = factory(PremiumPlusUser::class)->create([
            'name' => 'Nama saya',
            'email' => $email,
        ]);

        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);

        $owner = $this->service->ownerDetail($premiumPlusInvoice);
        $this->assertEquals($email, $owner['email']);
    }

    public function testGetTransactionDetail()
    {
        $amount = 1000;
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'amount' => $amount,
            'name' => 'Pembayaran bulan 1',
            'invoice_number' => 'USHNUEJD',
        ]);

        $transactionDetail = $this->service->transactionDetail($premiumPlusInvoice);
        $this->assertEquals($amount, $transactionDetail['detail']['price']);
    }

    public function testChangeStatusPayment()
    {
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id'  => $premiumPlusUser->id
        ]);
        $payment = factory(Payment::class)->create([
            'source_id' => $premiumPlusInvoice->id,
            'source' => Payment::PAYMENT_SOURCE_GP4
        ]);
        $midtransData = [
            'payment_type' => 'bank_transfer',
            'transaction_status' => 'settlement'
        ];

        $result = $this->service->changeStatusPayment($payment, $midtransData, Payment::MIDTRANS_STATUS_SUCCESS, PremiumPlusInvoice::INVOICE_STATUS_PAID);
        $this->assertEquals('paid', $result->status);
    }

}