<?php
namespace App\Services\Premium;

use App;
use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPlusUser;

class PremiumPlusUserServiceTest extends MamiKosTestCase
{
    
    protected $service;
    public function setUp(): void
    {
        parent::setUp();
        $this->service = App::make(PremiumPlusUserService::class);
    }

    public function testPremiumPlusUserSaveSuccess()
    {
        $data = [
            'name' => 'Jono',
            'phone_number' => '087839439484',
            'email' => 'me@mail.com',
            'consultant_id' => 1,
            'user_id' => 1,
            'designer_id' => 1,
            'request_date' => date('Y-m-d H:i:s'),
            'activation_date' => date('Y-m-d H:i:s'),
            'start_date' => date('Y-m-d H:i:s'),
            'end_date' => date('Y-m-d H:i:s', strtotime("+60 days")),
            'premium_rate' => 10,
            'static_rate' => 1000000,
            'registered_room' => 10,
            'guaranteed_room' => 5,
            'grace_period' => 6
        ];

        $premiumPlusUser = $this->service->save(new PremiumPlusUser(), $data);
        $this->assertInstanceOf(PremiumPlusUser::class, $premiumPlusUser);
    }

}