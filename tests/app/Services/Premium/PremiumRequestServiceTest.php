<?php

namespace App\Services\Premium;

use App;
use App\Services\Premium\PremiumRequestService;
use App\Entities\Premium\PremiumRequest;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\AccountConfirmation;

class PremiumRequestServiceTest extends MamiKosTestCase
{
    
    protected $service;
    public function setUp(): void
    {
        parent::setUp();
        $this->service = App::make(PremiumRequestService::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testPremiumPackageRequestWhenPackageNotFound()
    {
        $user = factory(User::class)->make();
        $request = $this->service->premiumPackageRequest([
            'package_id' => 0,
        ], $user);

        $this->assertFalse($request['status']);
    }

    public function testPremiumRequestWhenOwnerAlreadyRequest()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'premium_package_id' => $premiumPackage->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING,
            'expired_date' => date('Y-m-d'),
            'expired_status' => PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED
        ]);

        $request = $this->service->premiumPackageRequest([
            'package_id' => $premiumPackage->id,
        ], $user);
        
        $this->assertTrue($request['status']);
    }

    public function testChangePremiumPackage()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'premium_package_id' => $premiumPackage->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING,
            'expired_date' => date('Y-m-d'),
            'expired_status' => PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED
        ]);

        $request = $this->service->premiumPackageRequest([
            'package_id' => $premiumPackage->id,
        ], $user);
        
        $lastRequest = PremiumRequest::find($premiumRequest->id);
        $this->assertEquals(null, $lastRequest);
        $this->assertTrue($request['status']);
    }

    public function testPremiumRequestSuccess()
    {
        $user = factory(User::class)->create();
        
        $premiumPackage = factory(PremiumPackage::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'premium_package_id' => $premiumPackage->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            'expired_date' => null
        ]);

        $request = $this->service->premiumPackageRequest([
            'package_id' => $premiumPackage->id,
        ], $user);

        $this->assertTrue($request['status']);
    }

    public function testpremiumBalanceRequestFailed()
    {
        $user = factory(User::class)->create();

        $request = $this->service->premiumBalanceRequest([
            'package_id' => 0
        ], $user);

        $this->assertFalse($request['action']);
    }

    public function testBalanceResponse()
    {
        $response = $this->service->balanceResponse();
        $this->assertFalse($response['action']);
    }

    public function testPackageResponse()
    {
        $response = $this->service->packageResponse();
        $this->assertFalse($response['status']);
    }
}