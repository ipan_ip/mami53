<?php
namespace App\Services\Premium;

use App;
use App\Test\MamiKosTestCase;
use App\Entities\Premium\AbTestPremium;
use App\Services\Premium\AbTestPremiumService;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use Config;
use App\Enums\ABTest\ExperimentValue;

class AbTestPremiumServiceTest extends MamiKosTestCase
{
    
    protected $service;
    public function setUp(): void
    {
        parent::setUp();
        $this->service = App::make(AbTestPremiumService::class);
    }

    public function testGetScenarioVariantA()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::BALANCE_TYPE
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $premiumPackage->id,
            'user_id' => $user->id
        ]);

        $response = $this->service->checkAbTestScenario($user);
        return $this->assertInstanceOf(PremiumPackage::class, $response->premium_package);
    }

    public function testGetScenarioControl()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create([
            'for' => PremiumPackage::PACKAGE_TYPE
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'premium_package_id' => $premiumPackage->id,
            'user_id' => $user->id
        ]);

        $response = $this->service->checkAbTestScenario($user);
        return $this->assertEquals($response, null);
    }

    public function testHandleReponse()
    {
        $params = [
            'user_id' => 1,
            'experiment_id' => 1,
            'experiment_value' => ExperimentValue::VARIANT_A_VALUE,
            'is_active' => false,
        ];

        $response = $this->service->handleResponse($params);
        $this->assertEquals($response['experiment_value'], ExperimentValue::VARIANT_A_VALUE);
    }

    public function testExperimentIdNull()
    {
        $user = factory(User::class)->create();
        $response = $this->service->getUserData($user);
        $this->assertFalse($response['is_active']);
    }

    public function testIsScenarioB()
    {
        $user = factory(User::class)->create();
        $response = $this->service->isScenarioB($user);
        $this->assertFalse($response);
    }
}