<?php
namespace App\Services\GoldPlus;

use App\User;
use App\Entities\GoldPlus\Package;
use App\Entities\GoldPlus\Submission;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;

use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\GoldPlus\PackageRepository;
use App\Repositories\Room\BookingOwnerRequestRepository;
use App\Repositories\RoomRepository;

use Illuminate\Database\Eloquent\Collection;
use App\Entities\Level\GoldplusLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Property\PropertyContract;
use App\Services\GoldPlus\GoldPlusSubmissionError;
use App\Services\GoldPlus\CreateNewSubmission;
use App\Entities\GoldPlus\Enums\SubmissionStatus;

class CreateNewSubmissionTest extends MamiKosTestCase
{
    protected $packages;
    protected $roomIds;
    protected $songIds;
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupGoldplusLevel();
    }

    private function setupGoldplusLevel()
    {
        factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1_PROMO]);
        factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_2_PROMO]);
    }

    public function testSubmitWithValidValueOnAllUnitType()
    {
        $code = 'gp1';
        $service = $this->prepareService([], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code);

        $this->assertInstanceOf(Submission::class, $res['submission']);
        $this->assertEquals($this->package->id, $res['submission']->package_id);
        $this->assertEquals($this->user->id, $res['submission']->user_id);

        $this->assertInstanceOf(PotentialOwner::class, $res['potential_owner']);
        $this->assertCount(3, $res['potential_properties']);
        $this->assertInstanceOf(PotentialProperty::class, $res['potential_properties'][0]);
        $this->assertCount(3, $res['booking_owner_requests']);
        $this->assertInstanceOf(BookingOwnerRequest::class, $res['booking_owner_requests'][0]);
    }

    public function testSubmitWithValidValueOnSingleUnitType()
    {
        $code = 'gp3';
        $service = $this->prepareService([], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code, $this->songIds);

        $this->assertInstanceOf(Submission::class, $res['submission']);
        $this->assertEquals($this->user->id, $res['submission']->user_id);
        $this->assertEquals($this->package->id, $res['submission']->package_id);
        $this->assertEquals(implode(',', $this->roomIds), $res['submission']->listing_ids);

        $this->assertInstanceOf(PotentialOwner::class, $res['potential_owner']);
        $this->assertCount(3, $res['potential_properties']);
        $this->assertInstanceOf(PotentialProperty::class, $res['potential_properties'][0]);
        $this->assertCount(3, $res['booking_owner_requests']);
        $this->assertInstanceOf(BookingOwnerRequest::class, $res['booking_owner_requests'][0]);
    }

    public function testSubmitWithOneRoomAlreadyBbk()
    {
        $code = 'gp1';
        $service = $this->prepareService(['oneRoomBbk'], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code);

        $this->assertInstanceOf(Submission::class, $res['submission']);
        $this->assertEquals($this->package->id, $res['submission']->package_id);
        $this->assertEquals($this->user->id, $res['submission']->user_id);

        $this->assertInstanceOf(PotentialOwner::class, $res['potential_owner']);
        $this->assertCount(4, $res['potential_properties']);
        $this->assertInstanceOf(PotentialProperty::class, $res['potential_properties'][0]);
        $this->assertCount(3, $res['booking_owner_requests']);
        $this->assertInstanceOf(BookingOwnerRequest::class, $res['booking_owner_requests'][0]);
    }

    public function testSubmitWithAllRoomAlreadyBbkShouldNotSentBbkRequest()
    {
        $code = 'gp1';
        $service = $this->prepareService(['allRoomBbk'], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code);

        $this->assertInstanceOf(Submission::class, $res['submission']);
        $this->assertEquals($this->package->id, $res['submission']->package_id);
        $this->assertEquals($this->user->id, $res['submission']->user_id);

        $this->assertInstanceOf(PotentialOwner::class, $res['potential_owner']);
        $this->assertCount(3, $res['potential_properties']);
        $this->assertInstanceOf(PotentialProperty::class, $res['potential_properties'][0]);
        $this->assertEmpty($res['booking_owner_requests']);
    }

    public function testSubmitWithNullPackage()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['packageNull']);
        $res = $service->submit($this->user, 'gp1');
    }

    public function testSubmitWithInactivePackage()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['packageInactive']);
        $res = $service->submit($this->user, 'gp1');
    }

    public function testSubmitWithEmptyPropIdsToSingleUnitTypePackage()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['packageInactive']);
        $res = $service->submit($this->user, 'gp3', [12, 34, 56]);
    }

    public function testSubmitWithNotFoundPropertyId()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['noProperty']);
        $res = $service->submit($this->user, 'gp3', [12, 34, 56]);
    }

    public function testSubmitWithPropertyDoesntBelongToUser()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['noBelongingProperty']);
        $res = $service->submit($this->user, 'gp3', [12, 34, 56]);
    }

    public function testSubmitWithPropertyInactive()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['inactiveProperty']);
        $res = $service->submit($this->user, 'gp3', [12, 34, 56]);
    }

    /**
     * @group KOS-15935
     */
    public function testSubmitWithMultipleUserWithEmptyEmail()
    {
        $code = 'gp1';
        $service = $this->prepareService([
            'emptyEmail'
        ], [
            'currentPackageCode' => $code,
        ]);
        $user1 = $this->user;
        $this->prepareService([
            'emptyEmail',
            'packageNull',
        ], []);
        $user2 = $this->user;

        $res1 = $service->submit($user1, $code);
        $res2 = $service->submit($user2, $code);

        $this->basicAssertResponse($res1);
        $this->basicAssertResponse($res2);
    }

    public function testSubmitWithGP1ShouldCreateContract()
    {
        $code = 'gp1';
        $service = $this->prepareService([], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code);

        $this->assertInstanceOf(PropertyContract::class, $res['property_contract']);
        $this->assertEquals(GoldplusLevel::GOLDPLUS_1_PROMO, $res['property_contract']->property_level->name);
    }

    public function testSubmitWithGP2ShouldCreateContract()
    {
        $code = 'gp2';
        $service = $this->prepareService([], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code);

        $this->assertInstanceOf(PropertyContract::class, $res['property_contract']);
        $this->assertEquals(GoldplusLevel::GOLDPLUS_2_PROMO, $res['property_contract']->property_level->name);
    }

    public function testSubmitWithGP3ShouldNotCreateContract()
    {
        $code = 'gp3';
        $service = $this->prepareService([], ['currentPackageCode' => $code]);

        $res = $service->submit($this->user, $code, $this->songIds);

        $this->assertNull($res['property_contract']);
    }

    /**
     * @group KOS-15935
     */
    public function testSubmitWithIncompleteUserDataShouldNotReturnError()
    {
        $code = 'gp1';
        $service = $this->prepareService([
            'emptyEmail'
        ], [
            'currentPackageCode' => $code,
        ]);
        $user = $this->user;

        $res = $service->submit($user, $code);

        $this->basicAssertResponse($res);
    }

    /**
     * @group KOS-15934
     */
    public function testSubmitWithRoomHasNullAreaBig()
    {
        $code = 'gp1';
        $service = $this->prepareService([
            'nullAreaBig'
        ], [
            'currentPackageCode' => $code,
        ]);

        $res = $service->submit($this->user, $code);

        $this->basicAssertResponse($res);
    }

    /**
     * @group KOS-15934
     */
    public function testSubmitWithIncompleteRoomDataShouldNotReturnError()
    {
        $code = 'gp1';
        $service = $this->prepareService([
            'nullAreaBig',
            'nullAddress',
            'nullAreaCity',
            'nullRoomCount'
        ], [
            'currentPackageCode' => $code,
        ]);

        $res = $service->submit($this->user, $code);

        $this->basicAssertResponse($res);
        $this->assertNotNull($res['potential_owner']->email);
    }

    /**
     * @group UG
     * @group UG-4334
     */
    public function testSubmitWithDuplicatedEmailShouldNotReturnError()
    {
        $code = 'gp1';
        $service = $this->prepareService([
            'duplicatedEmailPotentialOwner',
        ], [
            'currentPackageCode' => $code,
        ]);

        $res = $service->submit($this->user, $code);

        $this->basicAssertResponse($res);
        $this->assertNull($res['potential_owner']->email);
    }

     /**
     * @group UG
     * @group UG-4657
     * @group App/Services/GoldPlus/CreateNewSubmission
     */
    public function testSubmitGpSubmissionWithNullableUserData()
    {
        $this->expectException(\RuntimeException::class);
        $code = 'gp1';
        $service = $this->prepareService(['nullUser'], [
            'currentPackageCode' => $code,
        ]);
        $user = $this->user;

        $res = $service->submit($user, $code);

        $this->basicAssertResponse($res);
    }

     /**
     * @group UG
     * @group UG-4657
     * @group App/Services/GoldPlus/CreateNewSubmission
     */
    public function testSubmitGpSubmissionWithBookingOwnerRequest()
    {
        $code = 'gp1';
        $service = $this->prepareService([], [
            'currentPackageCode' => $code,
        ]);
        $user = $this->user;

        $res = $service->submit($user, $code);

        $this->basicAssertResponse($res);
    }

    /**
     * @group UG
     * @group UG-4803
     * @group App/Services/GoldPlus/CreateNewSubmission
     */
    public function testSubmitWithAlreadyHasActiveSubmission()
    {
        $this->expectException(GoldPlusSubmissionError::class);

        $service = $this->prepareService(['hasActiveSubmission']);
        $service->submit($this->user, 'gp1');
    }

    private function basicAssertResponse(array $res)
    {
        $this->assertNotNull($res);
        $this->assertNotNull($res['submission']);
        $this->assertNotNull($res['potential_owner']);
        $this->assertNotNull($res['potential_properties']);
        $this->assertNotNull($res['booking_owner_requests']);
        $this->assertInstanceOf(Submission::class, $res['submission']);
        $this->assertInstanceOf(PotentialOwner::class, $res['potential_owner']);
        $this->assertInstanceOf(PotentialProperty::class, $res['potential_properties'][0]);
        $this->assertInstanceOf(BookingOwnerRequest::class, $res['booking_owner_requests'][0]);
    }

    private function prepareService($options = [], $config = []): CreateNewSubmission
    {
        if (!in_array('packageNull', $options)) {
            $packageArgs = [
                ['code' => 'gp1'],
                ['code' => 'gp2'],
                ['code' => 'gp3', 'unit_type' => 'single']
            ];

            if (in_array('packageInactive', $options)) {
                foreach($packageArgs as $k => $v) {
                    $packageArgs[$k]['active'] = false;
                }
            }

            $this->packages = [];
            foreach($packageArgs as $k => $v) {
                $this->packages[] = factory(Package::class)->create($v);
            }

            if (isset($config['currentPackageCode'])) {
                $code = $config['currentPackageCode'];
                $this->package = array_values(array_filter($this->packages,
                    function($p) use ($code) {
                        return $p->code == $code;
                    }))[0];
            }
        }

        $this->roomIds = [];
        $userAttributes = [];
        if (in_array('emptyEmail', $options)) {
            $userAttributes['email'] = '';
        }
        if (in_array('nullName', $options)) {
            $userAttributes['name'] = null;
        }

        if (in_array('nullUser', $options)) {
            $this->user = null;
        } else {
            $this->user = factory(User::class)->create($userAttributes);
        
            if(!in_array('noProperty', $options)) {
                $roomAttributes = [
                    'is_active' => !in_array('inactiveProperty', $options) ? 'true' : 'false',
                ];
                if (in_array('nullAreaBig', $options)) {
                    $roomAttributes['area_big'] = null;
                }
                if (in_array('nullAddress', $options)) {
                    $roomAttributes['address'] = null;
                }
                if (in_array('nullAreaCity', $options)) {
                    $roomAttributes['area_city'] = null;
                }
                if (in_array('nullName', $options)) {
                    $roomAttributes['name'] = null;
                }
                if (in_array('nullRoomCount', $options)) {
                    $roomAttributes['room_count'] = null;
                }
                if (in_array('allRoomBbk', $options)) {
                    $roomAttributes['is_booking'] = 1;
                }

                $rooms = factory(Room::class, 3)->create($roomAttributes);

                if (in_array('oneRoomBbk', $options)) {
                    $roomAttributes['is_booking'] = 1;
                    $room = factory(Room::class)->create($roomAttributes);
                    $rooms->push($room);
                }

                $this->roomIds = array_map(function($r){ return $r['id']; }, $rooms->toArray());
                $this->songIds = array_map(function($r){ return $r['song_id']; }, $rooms->toArray());

                $userId = $this->user->id;
                if(in_array('noBelongingProperty', $options)) {
                    $userId += 1;
                }

                foreach ($this->roomIds as $id) {
                    $roomOwner = factory(RoomOwner::class)->create([
                        'user_id' => $this->user->id,
                        'designer_id' => $id,
                        'status' => RoomOwner::ROOM_VERIFY_STATUS
                    ]);
                }
            }

            if (in_array('potentialOwnerExists', $options)) {
                factory(PotentialOwner::class)->create(['user_id' => $this->user->id]);
            }
        }

        if (in_array('duplicatedEmailPotentialOwner', $options)) {
            factory(PotentialOwner::class)->create(['email' => $this->user->email]);
        }

        if (in_array('hasActiveSubmission', $options)) {
            factory(Submission::class)->create([
                'user_id' => $this->user->id,
                'status' => SubmissionStatus::NEW
            ]);
        }

        return app()->make(CreateNewSubmission::class);
    }

}