<?php

namespace app\Services\Property;

use App\Entities\Property\Property;
use App\Repositories\Property\PropertyRepository;
use App\Services\Property\PropertyService;
use App\Test\MamiKosTestCase;
use App\User;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;

class PropertyServiceTest extends MamiKosTestCase
{
    private $service;
    private $propertyRepository;
    private $roomRepository;
    private $propertyContractRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->propertyRepository = $this->mockAlternatively(PropertyRepository::class);
        $this->roomRepository = $this->mockAlternatively(RoomRepository::class);
        $this->propertyContractRepository = $this->mockAlternatively(PropertyContractRepository::class);
        $this->service = app()->make(PropertyService::class);
    }

    public function testGetListPaginateAndSuccess()
    {
        $this->propertyRepository->shouldReceive('getListPaginate')
            ->andReturn(new LengthAwarePaginator(
                [factory(Property::class)->make()],
                1,
                20
            ));
        $result = $this->service->getListPaginate([]);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testCreatePropertyAndSuccess()
    {
        $this->propertyRepository->shouldReceive('createProperty')
            ->andReturn(factory(Property::class)->make());
        $result = $this->service->createProperty('name', factory(User::class)->make());
        $this->assertNotEmpty($result);
    }

    public function testUpdatePropertyAndSuccess()
    {
        $this->propertyRepository->shouldReceive('updateProperty')
            ->andReturn(true);
        $result = $this->service->updateProperty(
            factory(Property::class)->make(),
            'name',
            factory(User::class)->make()
        );
        $this->assertTrue($result);
    }

    // public function testDeletePropertyAndSuccess()
    // {
    //     $this->roomRepository->shouldReceive('resetRoomWithPropertyId')->andReturn(true);
    //     $this->propertyContractRepository->shouldReceive('deleteContractByPropertyId')
    //         ->andReturn(true);
    //     $this->propertyRepository->shouldReceive('deleteProperty')->andReturn(true);
    //     $result = $this->service->deleteProperty(
    //         factory(Property::class)->make(['id' => 12])
    //     );
    //     $this->assertTrue($result);
    // }

    public function testDeletePropertyAndFailed()
    {
        $this->roomRepository->shouldReceive('resetRoomWithPropertyId')
            ->andThrow(new Exception('error'));
        $result = $this->service->deleteProperty(
            factory(Property::class)->make(['id' => 12])
        );
        $this->assertFalse($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsPropertyNameExist()
    {
        $owner = factory(User::class)->create();
        $duplicateName = 'duplicate name';
        $this->propertyRepository->shouldReceive('isPropertyNameExist')->once()->withArgs([$duplicateName, $owner])
            ->andReturn(true);

        $result = $this->service->isPropertyNameExist($duplicateName, $owner);
        $this->assertTrue($result);
    }
}
