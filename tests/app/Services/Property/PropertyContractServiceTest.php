<?php

namespace app\Services\Property;

use App\Entities\Consultant\PotentialOwner;
use App\Entities\Level\GoldplusLevel;
use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\Property\UpgradePropertyContract;
use App\Repositories\Property\PropertyContractOrderRepository;
use App\Repositories\Property\PropertyContractRepository;
use App\Services\Property\PropertyContractService;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Queue;
use Mockery;

class PropertyContractServiceTest extends MamiKosTestCase
{
    /**
     *  @var PropertyContractService
     */
    private $service;

    /**
     *  @var PropertyContractRepository
     */
    private $propertyContractRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->propertyContractRepository = $this->mockAlternatively(
            PropertyContractRepository::class
        );
        $this->propertyContractOrderRepository = $this->mockAlternatively(
            PropertyContractOrderRepository::class
        );
        $this->service = app()->make(PropertyContractService::class);
    }

    public function testCreatePackage(): void
    {
        $user = factory(User::class)->create();
        $params = [
            'assigned_by' => $user->id
        ];
        $propertyLevel = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);
        $this->propertyContractRepository
            ->shouldReceive('createContract')
            ->with($params)
            ->once()
            ->andReturn($contract);
        $this->propertyContractOrderRepository->shouldReceive('createOrder');
        $this->service->createPackage($params, $user);
    }

    public function testUpdatePackage(): void
    {
        $params = [
            'joined_at' => '2020-09-30 17:57:18',
            'is_autocount' => false,
            'total_package' => 4
        ];
        $propertyLevel = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);
        $this->propertyContractRepository
            ->shouldReceive('updateContract')
            ->with($contract, $params)
            ->once()
            ->andReturn($contract);

        $this->service->updatePackage($contract, $params);
    }

    public function testCreateContractAndSuccess()
    {
        $user = factory(User::class)->state('admin')->make();
        $property = factory(Property::class)->make();
        $this->propertyContractRepository->shouldReceive('createContract')
            ->andReturn(factory(PropertyContract::class)->make());
        $this->propertyContractOrderRepository->shouldReceive('createOrder');
        $result = $this->service->createContract($user, $property, []);
        $this->assertInstanceOf(PropertyContract::class, $result);
    }

    public function testCalculateTotalRoomAndPackageWithoutKostLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 0, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageWithoutRoomLevel(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 0, 'total_package' => 2]);
    }

    public function testSetSubmissionIdAndSuccess()
    {
        $contract = factory(PropertyContract::class)->make();
        $this->propertyContractRepository->shouldReceive('setSubmissionId')->andReturn(
            $contract
        );
        $result = $this->service->setSubmissionId($contract, 123);
        $this->assertNotEmpty($result);
    }

    private function createContractDetail(PropertyContract $contract): array
    {
        $property = factory(Property::class)->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $contract->property_level->id]);
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);
        $room = factory(RoomUnit::class)->create(['designer_id' => $kost->id, 'room_level_id' => $roomLevel->id]);
        $contractDetail = factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $contract->id
        ]);

        return [
            'property' => $property,
            'kost' => $kost,
            'kostLevel' => $kost,
            'roomLevel' => $roomLevel,
            'room' => $room,
            'contractDetail' => $contractDetail
        ];
    }

    public function testCalculateTotalRoomAndPackageWithoutRoom(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);

        $property = factory(Property::class)->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $roomLevel = factory(RoomLevel::class)->create(['kost_level_id' => $kostLevel->id]);

        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $property->id,
            'property_contract_id' => $contract->id
        ]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 0, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldUpdateTotalRoomPackage(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $nonCountedRoom = factory(RoomUnit::class)->create(['designer_id' => factory(Room::class)->create()->id, 'room_level_id' => factory(RoomLevel::class)->create()->id]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $this->createContractDetail($contract);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 1, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldCalculateOtherKostRooms(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $this->createContractDetail($contract);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 1, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldNotCountDifferentLevelRoom(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $created = $this->createContractDetail($contract);

        $nonCountedRoom = factory(RoomUnit::class)->create(['designer_id' => $created['kost']->id, 'room_level_id' => factory(RoomLevel::class)->create()->id]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 1, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldNotCountOtherKostRoom(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $created = $this->createContractDetail($contract);

        $nonCountedRoom = factory(RoomUnit::class)->create(['designer_id' => factory(Room::class)->create()->id, 'room_level_id' => $created['kostLevel']->id]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 1, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldCalculateOtherPropertiesRoom(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $created = $this->createContractDetail($contract);

        $property = factory(Property::class)->create();
        $kost = factory(Room::class)->create(['property_id' => $property->id]);
        $room = factory(RoomUnit::class)->create(['designer_id' => $kost->id, 'room_level_id' => $created['roomLevel']->id]);
        factory(PropertyContractDetail::class)->create([
            'property_contract_id' => $contract,
            'property_id' => $property->id
        ]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 2, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageShouldCalculateOtherKost(): void
    {
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => false,
            'total_room_package' => 0,
            'total_package' => 2
        ]);

        $created = $this->createContractDetail($contract);

        $kost = factory(Room::class)->create(['property_id' => $created['property']->id]);
        $room = factory(RoomUnit::class)->create(['designer_id' => $kost->id, 'room_level_id' => $created['roomLevel']->id]);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => false, 'total_room_package' => 2, 'total_package' => 2]);
    }

    public function testCalculateTotalRoomAndPackageWithGP1ShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP3TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP1PromoShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1_PROMO]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP3TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP2ShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_2]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP3TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP2PromoShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_2_PROMO]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP3TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP3ShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_3]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP3TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP1TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP3PromoShouldCallMethod(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_3_PROMO]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $this->service->shouldReceive('calculateGP3TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->shouldNotHaveReceived('calculateGP1TotalPackage');

        $this->service->calculateTotalRoomAndPackage($contract);
    }

    public function testCalculateTotalRoomAndPackageWithGP1ShouldSaveCorrectTotalPackage(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_1]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $contract->total_package = 1;
        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => true, 'total_room_package' => 1, 'total_package' => 1]);
    }

    public function testCalculateTotalRoomAndPackageWithGP2ShouldSaveCorrectTotalPackage(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_2]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $contract->total_package = 1;
        $this->service->shouldReceive('calculateGP1TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => true, 'total_room_package' => 1, 'total_package' => 1]);
    }

    public function testCalculateTotalRoomAndPackageWithGP3ShouldSaveCorrectTotalPackage(): void
    {
        $this->service = Mockery::mock(PropertyContractService::class)->makePartial();

        $propertyLevel = factory(PropertyLevel::class)->create(['name' => PropertyLevel::GOLDPLUS_3]);
        $contract = factory(PropertyContract::class)->create([
            'property_level_id' => $propertyLevel->id,
            'is_autocount' => true,
            'total_room_package' => 0,
            'total_package' => 0
        ]);

        $this->createContractDetail($contract);

        $contract->total_package = 1;
        $this->service->shouldReceive('calculateGP3TotalPackage')
            ->withArgs(function ($arg) use ($contract) {
                return $arg->is($contract);
            })
            ->once()
            ->andReturn($contract);

        $this->service->calculateTotalRoomAndPackage($contract);

        $this->assertDatabaseHas('property_contracts', ['id' => $contract->id, 'is_autocount' => true, 'total_room_package' => 1, 'total_package' => 1]);
    }

    public function testCalculateGP1TotalPackage(): void
    {
        $contract = factory(PropertyContract::class)->create(['total_package' => 0]);
        $firstProperty = factory(Property::class)->create(['owner_user_id' => 1]);
        $secondProperty = factory(Property::class)->create(['owner_user_id' => 1]);
        $thirdProperty = factory(Property::class)->create(['owner_user_id' => 2]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $firstProperty->id,
            'property_contract_id' => $contract->id
        ]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $secondProperty->id,
            'property_contract_id' => $contract->id
        ]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $thirdProperty->id,
            'property_contract_id' => $contract->id
        ]);

        $contract = $this->service->calculateGP1TotalPackage($contract);
        $this->assertEquals(2, $contract->total_package);
    }

    public function testCalculateGP3TotalPackage(): void
    {
        $contract = factory(PropertyContract::class)->create(['total_package' => 0]);
        $firstProperty = factory(Property::class)->create(['owner_user_id' => 1]);
        $secondProperty = factory(Property::class)->create(['owner_user_id' => 1]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $firstProperty->id,
            'property_contract_id' => $contract->id
        ]);

        factory(PropertyContractDetail::class)->create([
            'property_id' => $secondProperty->id,
            'property_contract_id' => $contract->id
        ]);

        $contract = $this->service->calculateGP3TotalPackage($contract);
        $this->assertEquals(2, $contract->total_package);
    }

    public function testTerminateContractAndSuccess()
    {
        $property = $this->mockPartialAlternatively(Property::class);
        $property->shouldReceive('getAttribute')
            ->with('property_active_contract')
            ->once()
            ->andReturn(factory(PropertyContract::class)->make());
        $this->propertyContractRepository->shouldReceive('terminateContract')
            ->andReturn(factory(PropertyContract::class)->make());
        $result = $this->service->terminateContract(
            factory(User::class)->make(['id' => 123]),
            $property
        );
        $this->assertInstanceOf(PropertyContract::class, $result);
    }

    public function testAutoCreateShouldCreateProperty(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1]);
        $this->service->autoCreate($potentialOwner, GoldplusLevel::GOLDPLUS_1());

        $this->assertDatabaseHas(
            'properties',
            [
                'owner_user_id' => $potentialOwner->user_id
            ]
        );
    }

    public function testAutoCreateShouldAssignAllKostsToProperty(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1]);

        $firstKost = factory(Room::class)->create();
        factory(RoomOwner::class)->create(['designer_id' => $firstKost->id, 'user_id' => $potentialOwner->user_id, 'status' => RoomOwner::ROOM_VERIFY_STATUS]);
        $secondKost = factory(Room::class)->create();
        factory(RoomOwner::class)->create(['designer_id' => $secondKost->id, 'user_id' => $potentialOwner->user_id, 'status' => RoomOwner::ROOM_VERIFY_STATUS]);

        $this->service->autoCreate($potentialOwner, GoldplusLevel::GOLDPLUS_1());

        $property = Property::where('owner_user_id', $potentialOwner->user_id)->first();

        $this->assertDatabaseHas('designer', ['id' => $firstKost->id, 'property_id' => $property->id]);
        $this->assertDatabaseHas('designer', ['id' => $secondKost->id, 'property_id' => $property->id]);
    }

    public function testAutoCreateShouldCreateContract(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1]);
        $this->service->autoCreate($potentialOwner, GoldplusLevel::GOLDPLUS_1());

        $this->assertDatabaseHas(
            'property_contracts',
            [
                'property_level_id' => $propertyLevel->id,
                'status' => PropertyContract::STATUS_INACTIVE,
                'is_autocount' => true,
                'assigned_by' => 0
            ]
        );
    }

    public function testAutoCreateShouldAddNewPropertyIntoContract(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1]);

        $contract = $this->service->autoCreate($potentialOwner, GoldplusLevel::GOLDPLUS_1());

        $property = Property::where('owner_user_id', $potentialOwner->user_id)->first();

        $this->assertDatabaseHas('property_contract_detail', ['property_id' => $property->id, 'property_contract_id' => $contract->id]);
    }

    public function testAutoCreateShouldAddOwnerPropertiesIntoOneContract(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create();
        $propertyLevel = factory(PropertyLevel::class)->create(['name' => GoldplusLevel::GOLDPLUS_1]);
        $firstProperty = factory(Property::class)->create(['owner_user_id' => $potentialOwner->user_id]);
        $secondProperty = factory(Property::class)->create(['owner_user_id' => $potentialOwner->user_id]);

        $contract = $this->service->autoCreate($potentialOwner, GoldplusLevel::GOLDPLUS_1());

        $this->assertDatabaseHas('property_contract_detail', ['property_id' => $firstProperty->id, 'property_contract_id' => $contract->id]);
        $this->assertDatabaseHas('property_contract_detail', ['property_id' => $secondProperty->id, 'property_contract_id' => $contract->id]);
    }

    public function testUpgradeToGoldplus2WithGoldplus1PromoContract(): void
    {
        $gp2 = factory(PropertyLevel::class)->create([
            'name' => PropertyLevel::GOLDPLUS_2_PROMO
        ]);

        factory(RoomLevel::class)->create([
            'kost_level_id' => factory(KostLevel::class)->create(['property_level_id' => $gp2->id])->id
        ]);

        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => factory(PropertyLevel::class)->create([
                'name' => PropertyLevel::GOLDPLUS_1_PROMO
            ])
        ]);

        Queue::fake();

        $this->service->upgradeToGoldplus2($propertyContract, factory(User::class)->create());

        Queue::assertPushed(UpgradePropertyContract::class);

        $this->assertDatabaseHas('property_contracts', ['id' => $propertyContract->id, 'property_level_id' => $gp2->id]);
    }

    public function testUpgradeToGoldplus2WithGoldplus3Contract(): void
    {
        $gp2 = factory(PropertyLevel::class)->create([
            'name' => PropertyLevel::GOLDPLUS_2_PROMO
        ]);

        factory(RoomLevel::class)->create([
            'kost_level_id' => factory(KostLevel::class)->create(['property_level_id' => $gp2->id])->id
        ]);

        $propertyContract = factory(PropertyContract::class)->create([
            'property_level_id' => factory(PropertyLevel::class)->create([
                'name' => PropertyLevel::GOLDPLUS_3
            ])
        ]);

        Queue::fake();

        $this->expectExceptionMessage('Tried to upgrade non goldplus 1 contract');

        $this->service->upgradeToGoldplus2($propertyContract, factory(User::class)->create());

        Queue::assertNotPushed(UpgradePropertyContract::class);

        $this->assertDatabaseMissing('property_contracts', ['id' => $propertyContract->id, 'property_level_id' => $gp2->id]);
    }
}
