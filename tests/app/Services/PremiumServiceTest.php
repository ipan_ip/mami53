<?php

namespace App\Services;

use App;
use Mockery;

use App\Test\MamiKosTestCase;
use App\Services\PremiumService;
use App\Repositories\Premium\PremiumRequestRepository;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\User;


class PremiumServiceTest extends MamiKosTestCase
{
    
    protected $premiumService;
    protected $mockedPremiumRequestRepo;
    protected $mockedUser;
    protected $type;

    private function generatePremiumRequestCollection(int $view = 10000, int $used = 10000)
    {
        $premiumRequests = collect();
        $premiumRequests->push(factory(PremiumRequest::class)->make([
            'view' => $view,
            'used' => $used
        ]));
        return $premiumRequests;
    }

    public function setUp() : void
    {
        parent::setUp();

        $this->mockedPremiumRequestRepo = $this->mockPartialAlternatively(PremiumRequestRepository::class);
        $this->mockedUser = $this->mockPartialAlternatively(User::class);

        $this->premiumService = App::make(PremiumService::class);
        $this->type = 'package';

        // seeding normal packages
        factory(PremiumPackage::class, 2)->create();
        // seeding trial package
        factory(PremiumPackage::class)->states('trial')->create();
        // seeding extension package
        factory(PremiumPackage::class)->states('extension')->create();
    }

    public function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

    /** @test */
    public function it_returns_empty_array_if_wrong_type()
    {
        $this->type = 'click';
        $output = $this->premiumService->getPremiumPackages($this->type, $this->mockedUser);
        $this->assertEquals([], $output);
    }

    /** @test */
    public function it_returns_normal_packages_for_user_empty_balance()
    {
        // prepare mocked data
        $this->mockedUser->id = 1;
        $premiumRequests = $this->generatePremiumRequestCollection(10000, 10000);
        $this->mockedPremiumRequestRepo->shouldReceive('getUserPaid')
            ->with($this->mockedUser->id, 1)->andReturn($premiumRequests);
        $this->mockedUser->shouldReceive('hasEverPremium')->andReturn(true);

        // assertion
        $output = $this->premiumService->getPremiumPackages($this->type, $this->mockedUser);
        $this->assertTrue(is_null($output['trial']));
        $this->assertTrue(count($output['packages']) > 0);
    }

    /** @test */
    public function it_returns_extension_packages_for_user_not_empty_balance()
    {
        $this->mockedUser->id = 1;
        $this->mockedPremiumRequestRepo->shouldReceive('getUserPaid')
            ->with($this->mockedUser->id, 1)->andReturn(collect());
        $this->mockedUser->shouldReceive('hasEverPremium')->andReturn(true);
        $output = $this->premiumService->getPremiumPackages($this->type, $this->mockedUser);
        $this->assertTrue(is_null($output['trial']));
        $this->assertTrue(count($output['packages']) > 0);
    }

    /** @test */
    public function it_returns_packages_and_trial_for_non_login_user()
    {
        $output = $this->premiumService->getPremiumPackages($this->type, null);
        $this->assertTrue(count($output['trial']) > 0);
        $this->assertTrue(count($output['packages']) > 0);
    }
}
