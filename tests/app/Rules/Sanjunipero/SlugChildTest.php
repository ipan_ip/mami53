<?php
namespace App\Rules\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;

class SlugChildTest extends MamikosTestCase 
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testPassesSave_False()
    {
        $parentSlug = $this->faker->slug;
        $childSlug = $this->faker->slug;

        $parent = factory(DynamicLandingParent::class)->states('active')->create([
                'slug' => $parentSlug
            ]);
        factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childSlug,
                'parent_id' => $parent->id
            ]);

        $res = (new SlugChild($parent->id, null, 'save'))->passes(
            'slug',
            $childSlug
        );
        $this->assertFalse($res);
    }

    public function testPassesSave_True()
    {
        $parentSlug = $this->faker->slug;
        $childExistSlug = $this->faker->slug;
        $childNewSlug = $this->faker->slug;

        $parent = factory(DynamicLandingParent::class)->states('active')->create([
                'slug' => $parentSlug
            ]);
        factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childExistSlug,
                'parent_id' => $parent->id
            ]);

        $res = (new SlugChild($parent->id, null, 'save'))->passes(
            'slug',
            $childNewSlug
        );
        $this->assertTrue($res);
    }

    public function testPassesUpdate_False()
    {
        $parentSlug = $this->faker->slug;
        $childExistSlug = $this->faker->slug;
        $childNewSlug = $childExistSlug;

        $parent = factory(DynamicLandingParent::class)->states('active')->create([
                'slug' => $parentSlug
            ]);
        $existedChild = factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childExistSlug,
                'parent_id' => $parent->id
            ]);
        $updatedChild = factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childNewSlug,
                'parent_id' => $parent->id
            ]);

        $res = (new SlugChild($parent->id, $updatedChild->id, 'update'))->passes(
            'slug',
            $childNewSlug
        );
        $this->assertFalse($res);
    }

    public function testPassesUpdate_True()
    {
        $parentSlug = $this->faker->slug;
        $childExistSlug = $this->faker->slug;
        $childNewSlug = $this->faker->slug;

        $parent = factory(DynamicLandingParent::class)->states('active')->create([
                'slug' => $parentSlug
            ]);
        $existedChild = factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childExistSlug,
                'parent_id' => $parent->id
            ]);
        $updatedChild = factory(DynamicLandingChild::class)->states('active')->create([
                'slug' => $childNewSlug,
                'parent_id' => $parent->id
            ]);

        $res = (new SlugChild($parent->id, $updatedChild->id, 'update'))->passes(
            'slug',
            $childNewSlug
        );
        $this->assertTrue($res);
    }
}