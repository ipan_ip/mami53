<?php
namespace App\Rules\Sanjunipero;

use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class ImageHeaderTest extends MamikosTestCase 
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testPasses_BothEmptyTrue()
    {
        $request = $this->createFakeRequest(
            '',
            'POST',
            [
                'desktop_header_media_id' => 0,
                'mobile_header_media_id' => 0,
            ]
        );

        $res = (new ImageHeader($request))->passes(
            '',
            ''
        );

        $this->assertTrue($res);
    }

    public function testPasses_BothFilledTrue()
    {
        $request = $this->createFakeRequest(
            '',
            'POST',
            [
                'desktop_header_media_id' => mt_rand(1,9),
                'mobile_header_media_id' => mt_rand(1,9)
            ]
        );

        $res = (new ImageHeader($request))->passes(
            '',
            ''
        );

        $this->assertTrue($res);
    }

    public function testPasses_DesktopEmptyFalse()
    {
        $request = $this->createFakeRequest(
            '',
            'POST',
            [
                'desktop_header_media_id' => 0,
                'mobile_header_media_id' => mt_rand(1,9)
            ]
        );

        $imageHeader = new ImageHeader($request);
        $res = $imageHeader->passes(
            '',
            ''
        );
        $message = $imageHeader->message();

        $attribute = $this->getNonPublicAttributeFromClass('attribute', $imageHeader);

        $this->assertSame($attribute, 'Desktop header image');
        $this->assertFalse($res);
        $this->assertIsString($message);
        $this->assertSame($message, 'Desktop header image must be filled.');
    }

    public function testPasses_MobileEmptyFalse()
    {
        $request = $this->createFakeRequest(
            '',
            'POST',
            [
                'desktop_header_media_id' => mt_rand(1,9),
                'mobile_header_media_id' => 0
            ]
        );

        $imageHeader = new ImageHeader($request);
        $res = $imageHeader->passes(
            '',
            ''
        );
        $message = $imageHeader->message();

        $attribute = $this->getNonPublicAttributeFromClass('attribute', $imageHeader);

        $this->assertSame($attribute, 'Mobile header image');
        $this->assertFalse($res);
        $this->assertIsString($message);
        $this->assertSame($message, 'Mobile header image must be filled.');
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}