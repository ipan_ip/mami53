<?php
namespace App\Rules\Sanjunipero;

use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;

class EmptyArrayTest extends MamikosTestCase 
{
    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function testPasses_True()
    {
        $res = (new EmptyArray())->passes(
            'faq_question',
            [$this->faker->sentences(3)]
        );
        $this->assertTrue($res);
    }

    public function testPasses_False()
    {
        $res = (new EmptyArray())->passes(
            'faq_question',
            ['']
        );
        $this->assertFalse($res);
    }

    public function testMessage_Show()
    {
        $attribute = 'faq_question';
        $emptyArray = new EmptyArray();
        $emptyArray->passes(
            $attribute,
            ['']
        );
        $res = $emptyArray->message();

        $this->assertIsString($res);
        $this->assertSame($res, ucfirst(str_replace('_', ' ',$attribute)).' must be filled.');
    }
}