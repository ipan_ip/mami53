<?php

namespace App\Rules;

use App\Rules\PriceProperty;
use App\Test\MamiKosTestCase;

class PricePropertyTest extends MamiKosTestCase
{
    protected $rule;

    public function setUp() : void
    {
        parent::setUp();

        $this->rule = new PriceProperty();
    }

    public function testPricePassesValid()
    {
        $this->assertTrue($this->rule->passes('test', 0));
        $this->assertTrue($this->rule->passes('test', 10000));
        $this->assertTrue($this->rule->passes('test', 100000000));
    }

    public function testPriceNotPassesDueToInvalidAmount()
    {
        $this->assertFalse($this->rule->passes('test', 213));
    }

    public function testPriceSuccessRetrieveCustomMessage()
    {
        $rule = new PriceProperty('Harga Bulanan harus lebih besar dari 10000');
        $this->assertEquals($rule->message(), 'Harga Bulanan harus lebih besar dari 10000');
    }

    public function testPriceSuccessRetrieveDefaultMessage()
    {
        $this->assertEquals($this->rule->message(), ':attribute must be 0 or greater than 10000');
    }

}