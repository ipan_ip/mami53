<?php

namespace App\Rules;

use App\Test\MamiKosTestCase;

class CoordinateRuleTest extends MamiKosTestCase
{
    protected $rule;

    public function setUp() : void
    {
        parent::setUp();

        $this->rule = new CoordinateRule();
    }

    /**
     * @group PMS-495
     */    
    public function testCoordinateRulePasses()
    {
        // real staging kost Coordinate
        $this->assertEquals(1, $this->rule->passes('longitude', 106.43444444435356));
        $this->assertEquals(1, $this->rule->passes('latitude',  -6.190752959106575));

        // test the maximum of each attribute
        $this->assertEquals(1, $this->rule->passes('longitude', 180.00000));
        $this->assertEquals(1, $this->rule->passes('latitude',  -90.00000));

        // test the invalid input
        $this->assertEquals(0, $this->rule->passes('longitude', 180.0001));
        $this->assertEquals(0, $this->rule->passes('latitude',  90.0001));
        $this->assertEquals(0, $this->rule->passes('longitude', -180.0001));
        $this->assertEquals(0, $this->rule->passes('latitude',  -90.0001));

        $this->assertEquals(1, $this->rule->passes('longitude', 106.43444444435356));
        $this->assertEquals(1, $this->rule->passes('latitude',  -6.190752959106575));

        // test valid coordinate
        $this->assertEquals(1, $this->rule->passes('coordinate', "89.9999999, -179.9999999"));
        $this->assertEquals(1, $this->rule->passes('coordinate', "-89.9999999, -179.9999999"));
        $this->assertEquals(1, $this->rule->passes('coordinate', "89.9999999, 179.9999999"));
        $this->assertEquals(1, $this->rule->passes('coordinate', "90.000000, -180.0000000"));
        $this->assertEquals(1, $this->rule->passes('coordinate', "-90.000000, -180.0000000"));
        $this->assertEquals(1, $this->rule->passes('coordinate', "90.000000, 180.0000000"));

        // test invalid coordinate
        $this->assertEquals(0, $this->rule->passes('coordinate', "891.9999999, -179.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "891.9999999, -1791.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "-891.9999999, -179.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "-89.9999999, -1791.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "891.9999999, 179.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "89.9999999, 1791.9999999"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "90.000001, -180.0000000"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "90.000000, -180.0000001"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "-90.000001, -180.0000000"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "-90.000000, -180.0000001"));
        $this->assertEquals(0, $this->rule->passes('coordinate', "90.000001, 180.0000000"));    
        $this->assertEquals(0, $this->rule->passes('coordinate', "90.000000, 180.0000001"));    
    }

    /**
     * @group PMS-495
     */    
    public function testCoordinateRuleMessage()
    {
        $this->assertEquals('The coordinate value is not a valid coordinate.', $this->rule->message());
    }
}