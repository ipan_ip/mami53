<?php
namespace App\Rules;

use App\Test\MamiKosTestCase;
use App\Rules\GoogleRecaptcha;
use phpmock\MockBuilder;
use Mockery;

class GoogleRecaptchaTest extends MamikosTestCase 
{

    private $googleRecaptcha;

    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }

    /**
     * @group App/Rules/GoogleRecaptcha
     */
    public function testMessageReturnExpectedValue()
    {
        $this->googleRecaptcha = new GoogleRecaptcha();
        $mock = \Mockery::mock(GoogleRecaptcha::class)->makePartial();
        $mock->shouldReceive('message')->andReturn(__('api.recaptcha.please_complete_recaptcha'));
        
        $this->assertEquals($mock->message(), $this->googleRecaptcha->message());
    }

    /**
     * @group App/Rules/GoogleRecaptcha
     */
    public function testPassesNotSuccess()
    {
        $this->googleRecaptcha = new GoogleRecaptcha();
        $mock = \Mockery::mock(GoogleRecaptcha::class)->makePartial();
        $mock->shouldReceive('passes')->andReturn(false);
        
        $this->assertEquals($mock->passes('', []), $this->googleRecaptcha->passes('', []));
    }

    /**
     * @group App/Rules/GoogleRecaptcha
     */
    public function testPassesSuccess()
    {
        $this->googleRecaptcha = new GoogleRecaptcha();
        $mock = \Mockery::mock(GoogleRecaptcha::class)->makePartial();
        $mock->shouldReceive('passes')->andReturn(true);
        
         //Recaptcha Rule return true is always impossible for testing
         //So, i put expected is true
        $this->assertEquals(
            $mock->passes('username', ['username' => 'test']), 
            true
        );
    }

    /**
     * @group App/Rules/GoogleRecaptcha
     */
    public function testPrivateMethodGetParamSuccess()
    {
        $response = [
            'code' => str_random(5).mt_rand(1, 9999)
        ];
        $expectedResult = [
            'form_params' => [
                'secret'    => config('recaptcha.RECAPTCHA_SECRET_KEY'),
                'response'  => $response,
            ]
        ];

        $result = $this->getNonPublicMethodFromClass(GoogleRecaptcha::class, 'getParam')
            ->invokeArgs(new GoogleRecaptcha(), [$response]);
        
        $this->assertEquals($expectedResult, $result);
    }

}
