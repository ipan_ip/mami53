<?php

namespace App\Observers\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Repositories\Sanjunipero\DynamicLandingChildRepositoryEloquent;
use App\Test\MamiKosTestCase;

class DynamicLandingParentObserverTest extends MamiKosTestCase
{
    protected $repo, $observer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repo = $this->mockAlternatively(DynamicLandingChildRepositoryEloquent::class);
        $this->observer = app()->make(DynamicLandingParentObserver::class);
    }

    public function testUpdated_ActiveParent()
    {
        $childLength = 2;
        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        factory(DynamicLandingChild::class, $childLength)->states('inactive')->create([
            'parent_id' => $parent->id
        ]);

        $this->repo->shouldReceive('activate')->atLeast()->times($childLength);
        $this->observer->updated($parent);
    }

    public function testUpdated_Inactivate()
    {
        $childLength = 2;
        $parent = factory(DynamicLandingParent::class)->states('inactive')->create();
        factory(DynamicLandingChild::class, $childLength)->states('active')->create([
            'parent_id' => $parent->id
        ]);

        $this->repo->shouldReceive('deactivate')->atLeast()->times($childLength);
        $this->observer->updated($parent);
    }

    public function testActivated()
    {
        $childLength = 2;
        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        factory(DynamicLandingChild::class, $childLength)->states('inactive')->create([
            'parent_id' => $parent->id
        ]);

        $this->repo->shouldReceive('activate')->atLeast()->times($childLength);
        $this->observer->activated($parent);
    }

    public function testDeactivated()
    {
        $childLength = 2;
        $parent = factory(DynamicLandingParent::class)->states('inactive')->create();
        factory(DynamicLandingChild::class, $childLength)->states('active')->create([
            'parent_id' => $parent->id
        ]);

        $this->repo->shouldReceive('deactivate')->atLeast()->times($childLength);
        $this->observer->deactivated($parent);
    }
}