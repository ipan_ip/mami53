<?php

namespace App\Observers;

use App\Entities\Booking\BookingUser;
use App\Entities\Room\Booking;
use App\Repositories\Booking\BookingStatusChangeRepository;
use App\Repositories\Booking\BookingUserRepository;
use App\Services\Consultant\BookingUserIndexService;
use App\Test\MamiKosTestCase;
use App\User;
use Mockery;

class BookingUserObserverTest extends MamiKosTestCase
{
    /**
     *  Mocked instance of BookingUserIndexService
     *
     *  @var BookingUserIndexService
     */
    protected $indexerService;

    /**
     *  Instance of BookingUserObserver
     *
     *  @var BookingUserObserver
     */
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->indexerService = $this->mockAlternatively(BookingUserIndexService::class);
        $this->observer = $this->app->make(BookingUserObserver::class);
    }

    public function testCreated(): void
    {
        // create booking data
        $bookingUserEntity = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        // Make sure that indexer is called to create the index
        $this->indexerService->shouldReceive('index')
            ->withArgs(function ($arg) use ($bookingUserEntity) {
                return $arg->is($bookingUserEntity);
            })
            ->once();

        // testing observer data
        $this->observer->created($bookingUserEntity);

        // get booking status data
        $bookingStatus = $bookingUserEntity->statuses;

        $this->assertEquals(1, $this->count($bookingStatus));
        $this->assertEquals($bookingUserEntity->status, $bookingStatus->first()->status);
    }

    public function testUpdated(): void
    {
        // create booking data
        $bookingUserEntity = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $bookingUserEntity->status = BookingUser::BOOKING_STATUS_CONFIRMED;
        $bookingUserEntity->save();

        $this->indexerService->shouldReceive('index')
            ->withArgs(function ($arg) use ($bookingUserEntity) {
                return $arg->is($bookingUserEntity);
            })
            ->once();

        // testing observer data
        $this->assertNull($this->observer->updated($bookingUserEntity));
    }

    public function testUpdatedShouldGetUserData(): void
    {
        $booking = $this->mock(BookingUser::class);

        $booking->shouldReceive('getOriginal')
            ->andReturn([
                'status' => BookingUser::BOOKING_STATUS_BOOKED
            ]);

        $booking->shouldReceive('getAttribute')
            ->with('status')
            ->andReturn(BookingUser::BOOKING_STATUS_CANCELLED);

        $repository = $this->mockAlternatively(BookingStatusChangeRepository::class);
        $this->observer = $this->app->make(BookingUserObserver::class);

        $repository->shouldReceive('addChange')
            ->with($booking, BookingUser::BOOKING_STATUS_CANCELLED, null, null)
            ->once();

        $this->indexerService->shouldReceive('index')
            ->with($booking)
            ->once();
        $this->observer->updated($booking);
    }

    public function testGetUserDataIsNull(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingUserObserver::class,
            'getUserData'
        );

        // run test
        $result = $method->invokeArgs(
            $this->observer, []
        );

        $this->assertNull($result);
    }

    public function testGetUserDataIsNotNull(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingUserObserver::class,
            'getUserData'
        );

        // create user session
        $user = factory(User::class)->create();
        $this->actingAs($user);

        // run test
        $result = $method->invokeArgs(
            $this->observer, []
        );

        $this->assertNotNull($result);
        $this->assertEquals($user->id, $result->id);
    }
}
