<?php

namespace App\Observers\Room\Element;

use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\RoomUnit\SaveLogRevisionRoomUnit;
use App\User;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Support\Facades\Queue;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class RoomUnitObserverTest extends MamikosTestCase
{
    use MockeryPHPUnitIntegration;

    public function testCreated()
    {
        Queue::fake();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
        ]);

        $observer = app()->make(RoomUnitObserver::class);
        $observer->created($roomUnit);

        Queue::assertPushed(SaveLogRevisionRoomUnit::class);
    }

    public function testCreatedWithExceptionThrown(): void
    {
        $exception = new Exception();

        $roomUnit = $this->mock(RoomUnit::class);
        $roomUnit->shouldReceive('toArray')
            ->withNoArgs()
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $observer = app()->make(RoomUnitObserver::class);
        $observer->created($roomUnit);
    }

    public function testUpdated()
    {
        Queue::fake();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
        ]);

        $observer = app()->make(RoomUnitObserver::class);
        $observer->updated($roomUnit);

        Queue::assertPushed(SaveLogRevisionRoomUnit::class);
    }

    public function testUpdatedWithExceptionThrown(): void
    {
        $exception = new Exception();

        $roomUnit = $this->mock(RoomUnit::class);
        $roomUnit->shouldReceive('getOriginal')
            ->withNoArgs()
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $observer = app()->make(RoomUnitObserver::class);
        $observer->updated($roomUnit);
    }

    public function testDeleted()
    {
        Queue::fake();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
        ]);
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
        ]);

        $observer = app()->make(RoomUnitObserver::class);
        $observer->deleted($roomUnit);

        Queue::assertPushed(SaveLogRevisionRoomUnit::class);
    }

    public function testDeletedWithExceptionThrown(): void
    {
        $exception = new Exception();

        $roomUnit = $this->mock(RoomUnit::class);
        $roomUnit->shouldReceive('getOriginal')
            ->withNoArgs()
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $observer = app()->make(RoomUnitObserver::class);
        $observer->deleted($roomUnit);
    }

    public function testgetUserAuthWithAppUserIsNotNull()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $observer = app()->make(RoomUnitObserver::class);

        $reflection = new \ReflectionClass(RoomUnitObserver::class);
        $method = $reflection->getMethod('getUserAuth');
        $method->setAccessible(true);
        $result = $method->invokeArgs($observer, []);
        $this->assertEquals($result,$user);
    }

    public function testgetUserAuthWithAppUserIsNull()
    {
        $user = factory(User::class)->create();
        app()->user = null;
        $this->actingAs($user);
        $observer = app()->make(RoomUnitObserver::class);

        $reflection = new \ReflectionClass(RoomUnitObserver::class);
        $method = $reflection->getMethod('getUserAuth');
        $method->setAccessible(true);
        $result = $method->invokeArgs($observer, []);
        $this->assertEquals($result,$user);
    }

    public function testgetUserAuthWithErrorException()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $observer = app()->make(RoomUnitObserver::class);

        $reflection = new \ReflectionClass(RoomUnitObserver::class);
        $method = $reflection->getMethod('getUserAuth');
        $method->setAccessible(true);
        $result = $method->invokeArgs($observer, []);
        $this->assertEquals($result,$user);
    }

    public function testgetUserWithAppUserIsNotNullAndNoAuthUserShouldReturnNull()
    {
        app()->user = null;
        $observer = app()->make(RoomUnitObserver::class);

        $reflection = new \ReflectionClass(RoomUnitObserver::class);
        $method = $reflection->getMethod('getUserAuth');
        $method->setAccessible(true);
        $result = $method->invokeArgs($observer, []);
        $this->assertNull($result);
    }
}
