<?php

namespace App\Observers\Room;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Observers\RoomObserver;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class RoomOwnerObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $observer;
    protected $owner;
    protected $room;

    protected function setUp(): void
    {
        parent::setUp();
        $this->observer = $this->app->make(RoomOwnerObserver::class);
        $this->room = Mockery::mock(Room::class);
        $this->owner = Mockery::mock(RoomOwner::class);
    }

    public function testCreatedWithOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')
            ->once();

        $this->observer->created($this->owner);
    }

    public function testCreatedWithOldOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER_OLD);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')
            ->once();

        $this->observer->created($this->owner);
    }

    public function testCreatedWithNonActiveKost()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER_OLD);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(false);

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->created($this->owner);
    }

    public function testCreatedWithNonOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_APARTMENT_AGENT);

        $this->owner->shouldNotReceive('getAttribute');

        $this->room->shouldNotReceive('isActive');

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->created($this->owner);
    }

    public function testCreatedWithExceptionThrown(): void
    {
        $exception = new Exception();

        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->created($this->owner);
    }

    public function testUpdatedWithOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')
            ->once();

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithOldOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER_OLD);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')
            ->once();

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithNonActiveKost()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_KOS_OWNER_OLD);

        $this->owner->shouldReceive('getAttribute')
            ->with('room')
            ->once()
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(false);

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithNonOwner()
    {
        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andReturn(RoomOwner::STATUS_TYPE_APARTMENT_AGENT);

        $this->owner->shouldNotReceive('getAttribute');

        $this->room->shouldNotReceive('isActive');

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithExceptionThrown(): void
    {
        $exception = new Exception();

        $this->owner->shouldReceive('getAttribute')
            ->with('owner_status')
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->updated($this->owner);
    }
}
