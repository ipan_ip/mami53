<?php

namespace App\Observers\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Services\Room\PriceFilterService;
use App\Test\MamiKosTestCase;
use DB;
use Mockery;

class MamipayRoomPriceComponentObserverTest extends MamiKosTestCase
{
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();
        DB::table('designer_price_filter')->truncate();
        $this->observer = app()->make(MamipayRoomPriceComponentObserver::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testCreatedShouldUpdatePriceFilter()
    {
        $service = $this->mockPartialAlternatively(PriceFilterService::class);

        $room = $this->generateKos();

        $this->generatePriceFilter($room);

        $additionalPrice = $this->generateAdditionalPrice($room);

        $service->shouldReceive('updatePriceFilter')
            ->withArgs(function ($arg) use ($room) {
                return $arg->is($room);
            })
            ->once();

        $observer = app()->make(MamipayRoomPriceComponentObserver::class);
        $this->assertNull($observer->created($additionalPrice));
    }

    public function testUpdatedShouldUpdatePriceFilter()
    {
        $service = $this->mockPartialAlternatively(PriceFilterService::class);

        $room = $this->generateKos();

        $this->generatePriceFilter($room);

        $additionalPrice = $this->generateAdditionalPrice($room);

        $additionalPrice->price = 600000;
        $additionalPrice->save();

        $service->shouldReceive('updatePriceFilter')
            ->withArgs(function ($arg) use ($room) {
                return $arg->is($room);
            })
            ->once();

        $observer = app()->make(MamipayRoomPriceComponentObserver::class);
        $this->assertNull($observer->updated($additionalPrice));
    }

    public function testDeletedShouldUpdatePriceFilter()
    {
        $service = $this->mockPartialAlternatively(PriceFilterService::class);

        $room = $this->generateKos();

        $this->generatePriceFilter($room);

        $additionalPrice = $this->generateAdditionalPrice($room);

        $additionalPrice->delete();

        $service->shouldReceive('updatePriceFilter')
            ->withArgs(function ($arg) use ($room) {
                return $arg->is($room);
            })
            ->once();

        $observer = app()->make(MamipayRoomPriceComponentObserver::class);
        $this->assertNull($observer->deleted($additionalPrice));
    }

    public function testUpdatedShouldChangeThePrice()
    {
        // dont mock service to make observer work
        $room = $this->generateKos();

        $this->generatePriceFilter($room);

        $additionalPrice = $this->generateAdditionalPrice($room);

        $additionalPrice->price = 600000;
        $additionalPrice->save();

        $observer = app()->make(MamipayRoomPriceComponentObserver::class);
        $this->assertNull($observer->updated($additionalPrice));

        //get the updated price filter
        $priceFilter = PriceFilter::first();

        $this->assertEquals(($room->price_monthly + 600000), $priceFilter->final_price_monthly);
        $this->assertEquals(($room->price_yearly + (12*600000)), $priceFilter->final_price_yearly);
    }

    public function testDeletedShouldChangeThePrice()
    {
        // dont mock service to make observer work
        $room = $this->generateKos();

        $this->generatePriceFilter($room);

        $additionalPrice = $this->generateAdditionalPrice($room);

        $additionalPrice->delete();

        $observer = app()->make(MamipayRoomPriceComponentObserver::class);
        $this->assertNull($observer->updated($additionalPrice));

        //get the updated price filter
        $priceFilter = PriceFilter::first();

        $this->assertEquals(($room->price_monthly), $priceFilter->final_price_monthly);
        $this->assertEquals(($room->price_yearly), $priceFilter->final_price_yearly);
    }

    public function testUpdatePriceFilterWhenCriteriaFulfilledCallUpdatePriceFilter()
    {
        $room = $this->generateKos();

        $additionalPrice = $this->generateAdditionalPrice($room);

        $method = $this->getNonPublicMethodFromClass(
            MamipayRoomPriceComponentObserver::class,
            'updatePriceFilterWhenCriteriaFulfilled'
        );

        $method->invokeArgs($this->observer, [$additionalPrice]);

        $priceFilter = PriceFilter::first();

        $this->assertEquals(
            ($room->price_monthly + $additionalPrice->price),
            $priceFilter->final_price_monthly
        );
    }

    public function testUpdatePriceFilterWhenCriteriaFulfilledNotCallUpdatePriceFilter()
    {
        $room = $this->generateKos();

        $notAdditionalPrice = $this->generateAdditionalPrice($room, false);

        $method = $this->getNonPublicMethodFromClass(
            MamipayRoomPriceComponentObserver::class,
            'updatePriceFilterWhenCriteriaFulfilled'
        );

        $method->invokeArgs($this->observer, [$notAdditionalPrice]);

        // because the criteria is not fulfilled, Price Filter should be null
        $priceFilter = PriceFilter::first();

        $this->assertNull($priceFilter);
    }

    private function generateKos()
    {
        return factory(Room::class)->create(
            [
                'price_daily' => 10000,
                'price_weekly' => 100000,
                'price_monthly' => 1000000,
                'price_quarterly' => 3000000,
                'price_semiannually' => 6000000,
                'price_yearly' => 12000000
            ]
        );
    }

    private function generatePriceFilter($room)
    {
        return factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_daily' => $room->price_daily,
                'final_price_weekly' => $room->price_weekly,
                'final_price_monthly' => $room->price_monthly,
                'final_price_quarterly' => $room->price_quarterly,
                'final_price_semiannually' => $room->price_semiannually,
                'final_price_yearly' => $room->price_yearly
            ]
        );
    }

    private function generateAdditionalPrice($room, $isAdditional = true)
    {
        $component = MamipayRoomPriceComponentType::ADDITIONAL;

        if (!$isAdditional) {
            $component = MamipayRoomPriceComponentType::FINE;
        }

        return factory(MamipayRoomPriceComponent::class)->create(
            [
                'designer_id' => $room->id,
                'component' => $component,
                'price' => 500000,
                'is_active' => 1,
            ]
        );
    }
}
