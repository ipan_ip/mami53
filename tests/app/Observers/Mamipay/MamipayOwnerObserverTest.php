<?php

namespace App\Observers\Mamipay;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class MamipayOwnerObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $owner;
    protected $user;
    protected $roomOwner;
    protected $room;
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->owner = Mockery::mock(MamipayOwner::class);
        $this->user = Mockery::mock(User::class);
        $this->roomOwner = Mockery::mock(RoomOwner::class);
        $this->room = Mockery::mock(Room::class);

        $this->observer = $this->app->make(MamipayOwnerObserver::class);
    }

    public function testCreatedWithMissingUser(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->once()
            ->andReturn(null);

        $this->observer->created($this->owner);
    }

    public function testCreatedWithMissingOwnerMapping(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->once()
            ->andReturn(null);

        $this->observer->created($this->owner);
    }

    public function testCreatedWithNoKost(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([]));

        $this->observer->created($this->owner);
    }

    public function testCreatedWithNonActiveKost(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([
                $this->roomOwner
            ]));

        $this->roomOwner->shouldReceive('getAttribute')
            ->with('room')
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(false);

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->created($this->owner);
    }

    public function testCreatedWithError(): void
    {
        $exception = new Exception();
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->created($this->owner);
    }

    public function testCreated(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([
                $this->roomOwner
            ]));

        $this->roomOwner->shouldReceive('getAttribute')
            ->with('room')
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')->once();

        $this->observer->created($this->owner);
    }

    public function testUpdatedWithMissingUser(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->once()
            ->andReturn(null);

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithMissingOwnerMapping(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->once()
            ->andReturn(null);

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithNoKost(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([]));

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithNonActiveKost(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([
                $this->roomOwner
            ]));

        $this->roomOwner->shouldReceive('getAttribute')
            ->with('room')
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(false);

        $this->room->shouldNotReceive('addToElasticsearch');

        $this->observer->updated($this->owner);
    }

    public function testUpdated(): void
    {
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->once();

        $this->owner->shouldReceive('getAttribute')
            ->with('user')
            ->andReturn($this->user);

        $this->user->shouldReceive('getAttribute')
            ->with('room_owner')
            ->andReturn(collect([
                $this->roomOwner
            ]));

        $this->roomOwner->shouldReceive('getAttribute')
            ->with('room')
            ->andReturn($this->room);

        $this->room->shouldReceive('isActive')
            ->once()
            ->andReturn(true);

        $this->room->shouldReceive('addToElasticsearch')->once();

        $this->observer->updated($this->owner);
    }

    public function testUpdatedWithExceptionThrown(): void
    {
        $exception = new Exception();
        $this->owner->shouldReceive('load')
            ->with(['user', 'user.room_owner', 'user.room_owner.room'])
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->updated($this->owner);
    }
}
