<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepository;
use App\Services\Consultant\PotentialPropertyIndexService;
use App\Test\MamiKosTestCase;

class PotentialPropertyObserverTest extends MamiKosTestCase
{
    protected $service;
    protected $observer;
    protected $property;
    protected $owner;
    protected $propertyFollowupHistoryRepo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->propertyFollowupHistoryRepo = $this->mock(PotentialPropertyFollowupHistoryRepository::class);
        $this->service = $this->mock(PotentialPropertyIndexService::class);
        $this->observer = $this->app->make(PotentialPropertyObserver::class);
        $this->property = $this->mock(PotentialProperty::class);
        $this->owner = $this->mock(PotentialOwner::class);
    }

    private function followUpStatusReturn(?string $toReturn): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('followup_status')
            ->andReturn($toReturn);
    }

    private function potentialOwnerIdReturn(int $toReturn): void
    {
        $this->owner
            ->shouldReceive('getAttribute')
            ->with('id')
            ->andReturn($toReturn);
    }

    private function assertIndexingProcess(PotentialProperty $toIndex): void
    {
        $this->service
            ->shouldReceive('index')
            ->with($toIndex)
            ->once();
    }

    private function assertSetFolUpStatToNew(): void
    {
        $this->owner
            ->shouldReceive('setFolUpStatToNew')
            ->once();
    }

    private function assertSetFolUpStatToOnGoing(): void
    {
        $this->owner
            ->shouldReceive('setFolUpStatToOnGoing')
            ->once();
    }

    private function assertCreateFollowupHistory(PotentialProperty $prop): void
    {
        $this->propertyFollowupHistoryRepo
            ->shouldReceive('createHistory')
            ->with($prop)
            ->once();
    }

    private function assertUpdateFollowupHistory(PotentialProperty $prop): void
    {
        $this->propertyFollowupHistoryRepo
            ->shouldReceive('updateHistory')
            ->with($prop)
            ->once();
    }

    public function testSaved(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn(null)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->observer->saved($this->property);
    }

    public function testSavedWithOwnerAndFollowUpStatusNew(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->followUpStatusReturn(PotentialProperty::FOLLOWUP_STATUS_NEW);
        $this->assertSetFolUpStatToNew();

        $this->observer->saved($this->property);
    }

    public function testSavedWithOwnerAndOtherPropertyFollowUpStatusNew(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->followUpStatusReturn(null);
        $this->potentialOwnerIdReturn(5);

        factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $this->assertSetFolUpStatToNew();
        $this->observer->saved($this->property);
    }

    public function testSavedWithOwnerFollowUpStatusInProgress(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->potentialOwnerIdReturn(5);
        $this->followUpStatusReturn(PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS);
        $this->assertSetFolUpStatToOnGoing();

        $this->observer->saved($this->property);
    }

    public function testSavedWithOtherPropertyFollowUpStatusInProgress(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->followUpStatusReturn(null);
        $this->potentialOwnerIdReturn(5);

        factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $this->assertSetFolUpStatToOnGoing();
        $this->observer->saved($this->property);
    }

    public function testSavedWithOwnerFollowUpStatusPaid(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->potentialOwnerIdReturn(5);
        $this->followUpStatusReturn(PotentialProperty::FOLLOWUP_STATUS_PAID);
        $this->assertSetFolUpStatToOnGoing();

        $this->observer->saved($this->property);
    }

    public function testSavedWithOtherPropertyFollowUpStatusPaid(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->followUpStatusReturn(null);
        $this->potentialOwnerIdReturn(5);

        factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_PAID
        ]);

        $this->assertSetFolUpStatToOnGoing();
        $this->observer->saved($this->property);
    }

    public function testSavedWithOwnerFollowUpStatusDone(): void
    {
        $this->property
            ->shouldReceive('getAttribute')
            ->with('potential_owner')
            ->andReturn($this->owner)
            ->once();

        $this->assertIndexingProcess($this->property);
        $this->followUpStatusReturn(null);
        $this->potentialOwnerIdReturn(5);

        $this->owner
            ->shouldReceive('setFolUpStatToDone')
            ->once();
        $this->observer->saved($this->property);
    }

    public function testDeleted(): void
    {
        $this->service
            ->shouldReceive('remove')
            ->with($this->property)
            ->once();

        $this->observer->deleted($this->property);
    }

    public function testCreatedPotentialProperty(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $this->assertCreateFollowupHistory($property);

        $this->observer->created($property);
    }

    public function testUpdatedFollowUpStatusShouldUpdateFollowupHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $property->update([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $this->assertUpdateFollowupHistory($property);

        $this->observer->updated($property);
    }

    public function testUpdatedWithFollowUpStatusReturnBackToNewShouldCreateNewFollowupHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $property->update([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $this->assertCreateFollowupHistory($property);

        $this->observer->updated($property);
    }
}
