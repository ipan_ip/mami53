<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\PotentialTenant;
use App\Services\Consultant\PotentialTenantIndexService;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class PotentialTenantObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    /**
     *  @var PotentialTenantIndexService
     */
    protected $service;

    /**
     *  @var PotentialTenantObserver
     */
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->mockAlternatively(PotentialTenantIndexService::class);
        $this->observer = $this->app->make(PotentialTenantObserver::class);
    }

    public function testSaved(): void
    {
        $tenant = factory(PotentialTenant::class)->create();

        $this->service
            ->shouldReceive('index')
            ->withArgs(function ($arg) use ($tenant) {
                return $arg->is($tenant);
            })
            ->once();

        $this->observer->saved($tenant);
    }

    public function testSavedShouldCatchError(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $exception = new Exception();

        $this->service
            ->shouldReceive('index')
            ->withArgs(function ($arg) use ($tenant) {
                return $arg->is($tenant);
            })
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->saved($tenant);
    }

    public function testDeleted(): void
    {
        $tenant = factory(PotentialTenant::class)->create();

        $this->service
            ->shouldReceive('remove')
            ->withArgs(function ($arg) use ($tenant) {
                return $arg->is($tenant);
            })
            ->once();

        $this->observer->deleted($tenant);
    }

    public function testDeletedShouldCatchError(): void
    {
        $tenant = factory(PotentialTenant::class)->create();
        $exception = new Exception();

        $this->service
            ->shouldReceive('remove')
            ->withArgs(function ($arg) use ($tenant) {
                return $arg->is($tenant);
            })
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->deleted($tenant);
    }
}
