<?php

namespace App\Observers\Consultant;

use Exception;
use App\Test\MamiKosTestCase;
use App\Entities\Consultant\PotentialOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Services\PotentialOwner\IndexToElasticSearch;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use App\Services\PotentialOwner\RemoveFromElasticSearch;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepository;

class PotentialOwnerObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    /**
     *  @var PotentialOwnerFollowupHistoryRepository
     */
    protected $followupHistoryRepo;

    /**
     *  @var IndexToElasticSearch
     */
    protected $indexService;

    /**
     *  @var RemoveFromElasticSearch
     */
    protected $deleteService;

    /**
     *  @var PotentialOwnerObserver
     */
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->followupHistoryRepo = $this->mock(PotentialOwnerFollowupHistoryRepository::class);
        $this->indexService = $this->mockAlternatively(IndexToElasticSearch::class);
        $this->deleteService = $this->mockAlternatively(RemoveFromElasticSearch::class);
        $this->observer = $this->app->make(PotentialOwnerObserver::class);
    }

    public function testSaved(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $this->indexService
            ->shouldReceive('process')
            ->withArgs(function ($arg) use ($owner) {
                return $arg->is($owner);
            })
            ->once();

        $this->observer->saved($owner);
    }

    public function testSavedShouldCatchError(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $exception = new Exception();

        $this->indexService
            ->shouldReceive('process')
            ->withArgs(function ($arg) use ($owner) {
                return $arg->is($owner);
            })
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->saved($owner);
    }

    public function testDeleted(): void
    {
        $owner = factory(PotentialOwner::class)->create();

        $this->deleteService
            ->shouldReceive('process')
            ->withArgs(function ($arg) use ($owner) {
                return $arg->is($owner);
            })
            ->once();

        $this->observer->deleted($owner);
    }

    public function testDeletedShouldCatchError(): void
    {
        $owner = factory(PotentialOwner::class)->create();
        $exception = new Exception();

        $this->deleteService
            ->shouldReceive('process')
            ->withArgs(function ($arg) use ($owner) {
                return $arg->is($owner);
            })
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->with($exception)
            ->once();

        $this->observer->deleted($owner);
    }

    public function testCreatedPotentialOwner(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $this->followupHistoryRepo
            ->shouldReceive('createHistory')
            ->with($owner)
            ->once();

        $this->observer->created($owner);
    }

    public function testUpdatedFollowUpStatusShouldUpdateFollowupHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $owner->setFolUpStatToOnGoing();

        $this->followupHistoryRepo
            ->shouldReceive('updateHistory')
            ->with($owner)
            ->once();

        $this->observer->updated($owner);
    }

    public function testUpdatedWithFollowUpStatusReturnBackToNewShouldCreateNewFollowupHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_ONGOING
        ]);

        $owner->setFolUpStatToNew();

        $this->followupHistoryRepo
            ->shouldReceive('createHistory')
            ->with($owner)
            ->once();

        $this->observer->updated($owner);
    }
}
