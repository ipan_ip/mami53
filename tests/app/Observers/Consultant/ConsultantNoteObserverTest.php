<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\ConsultantNote;
use App\Services\Consultant\PotentialTenantIndexService;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class ConsultantNoteObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    protected $observer;
    protected $potentialTenantIndexService;
    protected $note;

    protected function setUp(): void
    {
        parent::setUp();

        $this->potentialTenantIndexService = $this->mockAlternatively(PotentialTenantIndexService::class);
        $this->observer = $this->app->make(ConsultantNoteObserver::class);
        $this->note = factory(ConsultantNote::class)->create();
    }

    private function assertBugsnag(Exception $e): void
    {
        Bugsnag::shouldReceive('notifyException')
            ->with($e)
            ->once();
    }

    public function testSaved(): void
    {
        $this->potentialTenantIndexService->shouldReceive('indexNote')
            ->withArgs(function ($arg) {
                return $arg->is($this->note);
            })
            ->once();

        $this->observer->saved($this->note);
    }

    public function testSavedShouldCatchError(): void
    {
        $exception = new Exception();

        $this->potentialTenantIndexService
            ->shouldReceive('indexNote')
            ->withArgs(function ($arg) {
                return $arg->is($this->note);
            })
            ->andThrow($exception);

        $this->assertBugsnag($exception);

        $this->observer->saved($this->note);
    }

    public function testDeleted(): void
    {
        $this->potentialTenantIndexService->shouldReceive('removeNoteIndex')
            ->withArgs(function ($arg) {
                return $arg->is($this->note);
            })
            ->once();

        $this->observer->deleted($this->note);
    }

    public function testDeletedShouldCatchError(): void
    {
        $exception = new Exception();

        $this->potentialTenantIndexService->shouldReceive('removeNoteIndex')
            ->withArgs(function ($arg) {
                return $arg->is($this->note);
            })
            ->andThrow($exception);

        $this->assertBugsnag($exception);

        $this->observer->deleted($this->note);
    }
}
