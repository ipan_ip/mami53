<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\ConsultantRoom;
use App\Services\Consultant\ConsultantRoomIndexService;
use App\Test\MamiKosTestCase;

class ConsultantRoomObserverTest extends MamiKosTestCase
{
    protected $service;
    protected $observer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->mock(ConsultantRoomIndexService::class);
        $this->observer = $this->app->make(ConsultantRoomObserver::class);
    }

    public function testCreate(): void
    {
        $mapping = factory(ConsultantRoom::class)->make();

        $this->service->shouldReceive('create')
            ->withArgs(function ($args) use ($mapping) {
                return (($args->designer_id === $mapping->designer_id) && ($args->consultant_id === $mapping->consultant_id));
            });

        $this->observer->created($mapping);
    }

    public function testDelete(): void
    {
        $mapping = factory(ConsultantRoom::class)->make();

        $this->service->shouldReceive('delete')
            ->withArgs(function ($args) use ($mapping) {
                return (($args->designer_id === $mapping->designer_id) && ($args->consultant_id === $mapping->consultant_id));
            });

        $this->observer->deleted($mapping);
    }
}
