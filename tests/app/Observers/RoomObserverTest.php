<?php

namespace App\Observers;

use App\Entities\Room\Room;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\Room\PriceFilterService;
use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Bugsnag\Report;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Bus;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class RoomObserverTest extends MamiKosTestCase
{
    use MockeryPHPUnitIntegration;

    private $room;
    private $roomId;

    protected function setUp(): void
    {
        parent::setUp();
        $this->roomId = 1;
        $this->room = $this->mockPartialAlternatively(Room::class);
        $this->room->id = $this->roomId;
        $this->room->shouldReceive('fresh')->andReturn($this->room);
        $this->room->shouldReceive('generateGeolocation')->andReturn(null);
        
        $this->roomRepository = $this->mockPartialAlternatively(RoomUnitRepository::class);
        $this->roomRepository->shouldReceive('backFillRoomUnit')->andReturn(collect([]));
        $this->priceFilterService = $this->mockPartialAlternatively(PriceFilterService::class);
        $this->priceFilterService->shouldReceive('updatePriceFilter')
            ->with($this->room)
            ->andReturn(null);
        Bus::fake();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testAfterCommitCreated_Success()
    {
        $this->room->shouldReceive('addToElasticsearch')->once();
        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitCreated($this->room));
    }

    public function testAfterCommitUpdatedRoomActiveShouldAddToElasticsearch(): void
    {
        $this->room->is_active = 'true';
        $this->room->expired_phone = 0;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(null);
        $this->room->shouldReceive('addToElasticsearch')->once();

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomActive_Success()
    {
        $this->room->is_active = 'true';
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomActive_ExceptionThrown()
    {
        $exception = new \Exception();
        $this->room->is_active = 'true';
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->andThrow($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomInactive_Success()
    {
        $this->room->is_active = 'false';
        $this->room->shouldReceive('deleteFromElasticsearch')->once();

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomBookingActive_Success()
    {
        $this->room->is_booking = 1;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();
        
        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomBookingActive_ExceptionThrown()
    {
        $exception = new \Exception();
        $this->room->is_booking = 1;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once()->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomBookingInactive_Success()
    {
        $this->room->is_booking = 0;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdated_RoomBookingInactive_ExceptionThrown()
    {
        $exception = new \Exception();
        $this->room->is_booking = 0;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once()->andThrow($exception);
        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomCount_Success()
    {
        $this->room->is_active = 'true';
        $this->room->room_count = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();
        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(true);
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_count')->andReturn(3);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(2);
        $this->roomRepository->shouldReceive('recalculateRoomUnit')->with($this->room, 3, 2)->andReturn(null);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomCountWithNullValue_Success()
    {
        $this->room->is_active = 'true';
        $this->room->room_count = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        
        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(true);
        $this->room->shouldReceive('isDirty')->with('room_available')->andReturn(true);
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_count')->andReturn(null);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(null);
        $this->roomRepository->shouldReceive('recalculateRoomUnit')->with($this->room, 0, 0)->andReturn(null);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomAvailable_Success()
    {
        $this->room->is_active = 'true';
        $this->room->room_available = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(false);
        $this->room->shouldReceive('isDirty')->with('room_available')->andReturn(true);
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId, 'occupied' => false])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(1);
        $this->roomRepository->shouldReceive('adjustRoomUnitAvailability')->with($this->room, 2)->andReturn(null);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomAvailableThrowQueryException()
    {
        $this->room->is_active = 'true';
        $this->room->room_available = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(false);
        $this->room->shouldReceive('isDirty')->with('room_available')->andReturn(true);
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId, 'occupied' => false])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(1);

        $exception = new QueryException("the query :bind", ['bind' => 'binding'], new Exception(""));
        $this->roomRepository
            ->shouldReceive('adjustRoomUnitAvailability')
            ->andThrow(
                $exception
            );
        Bugsnag::shouldReceive('notifyError');
        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomAvailableThrowGeneralException()
    {
        $this->room->is_active = 'true';
        $this->room->room_available = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(false);
        $this->room->shouldReceive('isDirty')->with('room_available')->andReturn(true);
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId, 'occupied' => false])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(1);


        $exception = new Exception("the exception");
        $this->roomRepository
            ->shouldReceive('adjustRoomUnitAvailability')
            ->andThrow(
                $exception
            );
        Bugsnag::shouldReceive('notifyException');
        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedRoomAndChangeRoomAvailableNullValue_Success()
    {
        $this->room->is_active = 'true';
        $this->room->room_available = 3;
        $this->room->shouldReceive('getFromElasticsearch')->andReturn(true);
        $this->room->shouldReceive('updateElasticsearch')->once();

        $this->room->shouldReceive('isDirty')->with('room_count')->andReturn(false);
        $this->room->shouldReceive('isDirty')->with('room_available')->andReturn(true);        
        $this->roomRepository->shouldReceive('count')->with(['designer_id' => $this->roomId, 'occupied' => false])->andReturn(0);
        $this->room->shouldReceive('getOriginal')->with('room_available')->andReturn(1);

        $this->roomRepository->shouldReceive('adjustRoomUnitAvailability')->with($this->room, 0)->andReturn(null);

        $observer = app()->make(RoomObserver::class);
        $this->assertNull($observer->afterCommitUpdated($this->room));
    }

    public function testAfterCommitUpdatedWithExceptionThrownOnNormalizeCity(): void
    {
        $exception = new Exception();

        $this->room->shouldReceive('isDirty')->andReturn(true);
        $this->room->shouldReceive('generateGeolocation')->andReturn(true);
        $this->room->shouldReceive('normalizeCity')
            ->withNoArgs()
            ->once()
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException');

        $observer = app()->make(RoomObserver::class);
        $observer->afterCommitUpdated($this->room);
    }

    public function testGetBugsnagQueryExceptionCallBackReportClosure()
    {
        $exception = new QueryException("the query :bind", ['bind' => 'binding'], new Exception(""));
        $params = [$exception];
        $repoMock = $this->createMock(RoomUnitRepository::class);
        $serviceMock = $this->createMock(PriceFilterService::class);
        $observer = new RoomObserver($repoMock, $serviceMock);
        $observerReflection = new \ReflectionClass(RoomObserver::class);
        $method = $observerReflection->getMethod('getBugsnagQueryExceptionCallBackReport');
        $method->setAccessible(true);
        $metaReporter = $method->invokeArgs(
            $observer,
            $params
        );

        $this->assertIsCallable($metaReporter);
        $reportMock = $this->createMock(Report::class);
        $reportMock->method('setSeverity')->with('info')->willReturn($this->anything());
        $reportMock->method('setMetaData')->willReturn($this->anything());
        $metaReporter($reportMock);
    }
}
