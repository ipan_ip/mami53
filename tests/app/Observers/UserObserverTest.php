<?php

namespace App\Observers;

use App\User;
use App\Test\MamiKosTestCase;

class UserObserverTest extends MamikosTestCase
{
    private $user;

    protected function setUp() : void
    {
        $this->user = $this->createMock(User::class);
    }

    public function testCreated_Success()
    {
        $this->user->expects($this->once())->method('tryCreateChatUser');

        $observer = new UserObserver();
        $this->assertNull($observer->created($this->user));
    }
}
