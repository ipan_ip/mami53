<?php

namespace App;

use App\Entities\Activity\Call;
use App\Entities\Config\AppConfig;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Entities\Feature\Feature;
use App\Entities\Mamipay\MamipayOwnerAgreement;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Room\Review;
use App\Entities\Room\ReviewReply;
use App\Entities\Room\Survey;
use App\Entities\User\Hostility;
use App\Entities\User\UserSocial;
use AppleSignIn\ASPayload;
use WhitelistFeature;
use Mockery;
use RuntimeException;
use ASDecoder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Entities\User\UserVerificationAccount;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class UserTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithFaker;

    private const APPLE_SOCIAL_TOKEN = 'eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLmdpdC5hcHBsZXNpZ25pbi50ZXN0aW5nIiwiZXhwIjoxNTk5MTg5OTg1LCJpYXQiOjE1OTkxMDM1ODUsInN1YiI6IjAwMTQ5MC4yNjQ5YTFiNjc0M2I0ZTM0YjBlMmIwMmM5MTAwMzY0Zi4xNTE0IiwiYXRfaGFzaCI6Ild4bWQtamw1NUw2emJJN0t0MGdFSGciLCJlbWFpbCI6InRpYXJhLnRlbmF0MUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6InRydWUiLCJhdXRoX3RpbWUiOjE1OTkxMDM1ODIsIm5vbmNlX3N1cHBvcnRlZCI6dHJ1ZX0.kXp0ZDwmy3DMJS91cIXhOYVb34YeUJUx70vJhnOCkGMSQgBl818yKYIuhbqIoDooX5wGEd_XVZQq1WIe8tMaZ3ZD0QF19SRbcD-fhZxOba8TzVHnQEA6z7ILrnX44krXUUJATiUUrEH0II6qCZRGGQRe3LUvEZVuZTbVLcHoIF7gvulVTv08M9ekUJF_gwdRYcovTUVr1NryXFpmDiU7frsY3Mfn9TyZO5_NVZiVhuVmusMee8hGUDbT-8kWxUKWTd9xj6Y4W3v2mNK4tAC78RnxvlQA0AyXFaVnA2K-QqVEe8yaXtmqA83GtIR6kePCOMjzU0DKwofL3l9iFUi6mg';
    private const APPLE_SOCIAL_ID = '001490.2649a1b6743b4e34b0e2b02c9100364f.1514';
    private const APPLE_SOCIAL_TOKEN_ISSUED_TIMESTAMP = 1599103585;

    ///////////////////////////////////////////////////////////////////
    // User::makeSafeUserName
    ///////////////////////////////////////////////////////////////////
    
    public function testGetSafeUserNameReturnsSafeNameIfNullOrEmpty(): void
    {
        // null
        $this->assertEquals(User::makeSafeUserName(null), '');

        // empty
        $this->assertEquals(User::makeSafeUserName(''), '');

        // spaces
        $this->assertEquals(User::makeSafeUserName(' '), '');
        $this->assertEquals(User::makeSafeUserName('  '), '');

        // line breaks
        $this->assertEquals(User::makeSafeUserName("\n"), '');
        $this->assertEquals(User::makeSafeUserName("\r"), '');
        $this->assertEquals(User::makeSafeUserName("\n\r"), '');
        $this->assertEquals(User::makeSafeUserName("abc\ndef"), 'abcdef');
        $this->assertEquals(User::makeSafeUserName("abc\rdef"), 'abcdef');
        $this->assertEquals(User::makeSafeUserName("abc\n\rdef"), 'abcdef');
    }

    public function testGetSafeUserNameReturnsSafeNameIfNormal(): void
    {
        $this->assertEquals(User::makeSafeUserName('a'), 'a');
        $this->assertEquals(User::makeSafeUserName('a1'), 'a1');

        // 49 letters
        $this->assertEquals(User::makeSafeUserName('1234567890123456789012345678901234567890123456789'), '1234567890123456789012345678901234567890123456789');

    }

    public function testGetSafeUserNameReturns50CharactersIfLongerThan50Characters(): void
    {
        // longer than 50 characters (60 letters)
        $this->assertEquals(User::makeSafeUserName('123456789012345678901234567890123456789012345678901234567890'), '12345678901234567890123456789012345678901234567890');

        // 51 letters including spaces
        $this->assertEquals(User::makeSafeUserName('  1234567890123456789012345678901234567890123456789'), '1234567890123456789012345678901234567890123456789');
    }

    public function testGetSafeUserNameReturnsSafeNameForUnicodeName(): void
    {
        // 60 bytes characters but 20 letters keep its name
        $this->assertEquals(User::makeSafeUserName('日本のホットセックスプロジェクトチャネル'), '日本のホットセックスプロジェクトチャネル');

        // 180 bytes characters but 60 letters should become 50 letters
        $this->assertEquals(User::makeSafeUserName('日本のホットセックスプロジェクトチャネル日本のホットセックスプロジェクトチャネル日本のホットセックスプロジェクトチャネル'), 
            '日本のホットセックスプロジェクトチャネル日本のホットセックスプロジェクトチャネル日本のホットセックス');
    }

    public function testUpdateAllPropertyNotUpdateUnverifiedRoom()
    {
        $date = "2020-03-31 10:07:36";

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = factory(Room::class)->create([
            'kost_updated_date' => $date
        ]);

        factory(RoomOwner::class)->create([ 
            "designer_id" => $room->id, 
            "user_id" => $user->id
        ]);

        $user->updateAllProperty();

        $this->assertDatabaseHas('designer', [
            'kost_updated_date' => $date
        ]);
    }

    public function testUpdateAllPropertySuccessfullyUpdateRoom()
    {
        $date = "2020-03-31 10:07:36";

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $room = factory(Room::class)->create([
            'kost_updated_date' => $date
        ]);

        factory(RoomOwner::class)->create([ 
            "designer_id" => $room->id, 
            "user_id" => $user->id,
            'status' => 'verified'
        ]);

        $user->updateAllProperty();

        $this->assertDatabaseMissing('designer', [
            'kost_updated_date' => $date
        ]);
    }

    public function testUpdateAllPropertySuccessfullyUpdateMultipleRoom()
    {
        $date = "2020-03-31 10:07:36";

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $rooms = factory(Room::class, 5)->create([
            'kost_updated_date' => $date
        ]);

        foreach ($rooms as $room) {
            factory(RoomOwner::class, 3)->create([ 
                "designer_id" => $room->id, 
                "user_id" => $user->id,
                'status' => 'verified'
            ]);
        } 

        $user->updateAllProperty();

        $this->assertDatabaseMissing('designer', [
            'kost_updated_date' => $date
        ]);
    }

    public function testSkipPremiumAutoAllocation()
    {
        $user = factory(User::class)->make([
            'id' => 3
        ]);
        WhitelistFeature::shouldReceive('isEligible')
            ->with(Feature::PREMIUM_SKIP_AUTO_ALLOCATION, $user->id)
            ->andReturn(true);
        $this->assertTrue($user->skipPremiumAutoAllocation());
    }

    /**
     * @group UG
     * @group UG-1352
     * @group App/User
     */
    public function testLoginWithAppleShouldReturnSuccess(): void
    {
        $mock = Mockery::mock(User::class)->makePartial();
        $expectedEmail  = str_random().'@'.str_random().'.com';
        $expectedUser   = str_random(15);
        $expectedResult = [
            'success'   => true,
            'data'      => [
                'email' => $expectedEmail,
                'user'  => $expectedUser,
            ]   
            ];

        $mock->shouldReceive('loginWithApple')
            ->andReturn([
                'success'   => true,
                'data'      => [
                    'email' => $expectedEmail,
                    'user'  => $expectedUser,
                ]   
            ]);
        $this->assertEquals(
            $mock->loginWithApple($expectedEmail, $expectedUser), 
            $expectedResult
        );
    }

    /**
     * @group UG
     * @group UG-1352
     * @group App/User
     */
    public function testLoginWithAppleShouldExpectRuntimeException(): void
    {
        $this->expectException(RuntimeException::class);
        User::loginWithApple('', '');
    }

    /**
     * @group UG
     * @group UG-1352
     * @group App/User
     */
    public function testLoginWithAppleShouldReturnPayloadWithNullInstance(): void
    {
    $mock       = Mockery::mock(User::class)->makePartial();
        $appleMock  = ASDecoder::spy();

        $mockPayload = $appleMock->shouldReceive('getAppleSignInPayload')->andReturn(null);
        $this->assertEquals($appleMock->getAppleSignInPayload(str_random()), null);
        
        $mock ->shouldReceive('loginWithApple')->andReturnUsing(function($clientUser, array $identityToken) {
            if ($clientuser === '' || $identityToken === '') {
                return RuntimeException(__('api.apple-auth.exception.empty-client-or-identity-token'));
            }
        });
    }

    public function testMamipayOwnerAgreementRelation()
    {
        $user = factory(User::class)->create();
        factory(MamipayOwnerAgreement::class)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(MamipayOwnerAgreement::class, $user->mamipay_owner_agreement);
    }

    /**
     * @group UG
     */
    public function testGetJobFormattedKuliah()
    {
        $user = factory(User::class)->make([
            'job' => 'kuliah',
        ]);
        $this->assertEquals('Mahasiswa', $user->job_formatted);
    }

    /**
     * @group UG
     */
    public function testGetJobFormattedOther()
    {
        $user = factory(User::class)->make();
        $this->assertEquals($user->job, $user->job_formatted);
    }

    /**
     * @group UG
     */
    public function testGetGenderFormattedMale()
    {
        $user = factory(User::class)->make([
            'gender' => 'male', 
        ]);
        $this->assertEquals('Laki-Laki', $user->gender_formatted);
    }

    /**
     * @group UG
     */
    public function testGetGenderFormattedFemale()
    {
        $user = factory(User::class)->make([
            'gender' => 'female', 
        ]);
        $this->assertEquals('Perempuan', $user->gender_formatted);
    }

    /**
     * @group UG
     */
    public function testGetGenderFormattedOther()
    {
        $user = factory(User::class)->make([
            'gender' => str_random(), 
        ]);
        $this->assertEquals('-', $user->gender_formatted);
    }

    /**
     * @group UG
     */
    public function testGetHostileStatusFalse()
    {
        $user = factory(User::class)->make([
            'hostility' => 0, 
        ]);
        $this->assertEquals('FALSE', $user->hostile_status);
    }

    /**
     * @group UG
     */
    public function testGetHostileStatusTrue()
    {
        $user = factory(User::class)->make([
            'hostility' => 50, 
        ]);
        $this->assertEquals("TRUE ($user->hostility)", $user->hostile_status);
    }

    /**
     * @group UG
     */
    public function testIsOwnerEmailRegisteredExist()
    {
        $user = factory(User::class)->states('owner')->create();
        $this->assertTrue(User::isOwnerEmailRegistered($user->email));
    }

    /**
     * @group UG
     */
    public function testLoginWithAppleExpiredToken()
    {
        $this->expectException(\Firebase\JWT\ExpiredException::class);
        ASDecoder::spy()
            ->shouldReceive('getAppleSignInPayload')
            ->andThrow(\Firebase\JWT\ExpiredException::class);
        User::loginWithApple(self::APPLE_SOCIAL_TOKEN, self::APPLE_SOCIAL_ID);
        $this->assertTrue(false);
    }

    /**
     * @group UG
     */
    public function testLoginWithAppleNotMatchClientId()
    {
        ASDecoder::spy()
            ->shouldReceive('getAppleSignInPayload')
            ->andReturn(new ASPayload((object) [
                'sub' => self::APPLE_SOCIAL_ID,
                'email' => $this->faker->email,
            ]));
        $result = User::loginWithApple(self::APPLE_SOCIAL_TOKEN, str_random());
        $this->assertEquals([], $result);
    }

    /**
     * @group UG
     */
    public function testLoginWithAppleMatchClientId()
    {
        ASDecoder::spy()
            ->shouldReceive('getAppleSignInPayload')
            ->andReturn(new ASPayload((object) [
                'sub' => self::APPLE_SOCIAL_ID,
                'email' => $this->faker->email,
            ]));
        $result = User::loginWithApple(self::APPLE_SOCIAL_TOKEN, self::APPLE_SOCIAL_ID);
        $this->assertNotEquals([], $result);
        $this->assertTrue($result['success']);
    }

    /**
     * @group UG
     */
    public function testGetDefaultUserNameOwner()
    {
        $user = factory(User::class)->state('owner')->make();
        $this->assertEquals(User::OWNER_DEFAULT_NAME, $user->getDefaultUserName());
    }

    /**
     * @group UG
     */
    public function testGetDefaultUserNameTenant()
    {
        $user = factory(User::class)->state('tenant')->make();
        $this->assertEquals(User::TENANT_DEFAULT_NAME, $user->getDefaultUserName());
    }

    public function testSynchronizeHostility()
    {
        $roomsCount = $this->faker->numberBetween(1, 5);
        $userID = $this->setupOwnerAndOwnershipData($roomsCount, 0);

        // Execute synchronization
        $newHostilityScore = $this->faker->numberBetween(10, 100);
        $owner = User::find($userID);
        $owner->synchronizeHostility($newHostilityScore);

        $this->assertSame($newHostilityScore, $owner->hostility);
        $this->assertEquals($roomsCount, $owner->room_owner()->count());

        foreach ($owner->room_owner as $ownership) {
            $this->assertEquals($newHostilityScore, $ownership->room->hostility);
        }
    }

    public function testSynchronizeHostilityWithSameScore()
    {
        $hostilityScore = $this->faker->numberBetween(10, 100);
        $roomsCount = $this->faker->numberBetween(1, 5);
        $userID = $this->setupOwnerAndOwnershipData($roomsCount, $hostilityScore);

        // Execute synchronization
        $owner = User::find($userID);
        $owner->synchronizeHostility($hostilityScore);

        $this->assertSame($hostilityScore, $owner->hostility);
        $this->assertEquals($roomsCount, $owner->room_owner()->count());

        foreach ($owner->room_owner as $ownership) {
            $this->assertEquals($hostilityScore, $ownership->room->hostility);
        }
    }

    public function testOwnerSurvey(): void
    {
        $user = factory(User::class)->create();
        $survey = factory(SurveySatisfaction::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $user->owner_survey->first()->is($survey)
        );
    }

    public function testReview(): void
    {
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $user->review->first()->is($review)
        );
    }

    public function testSurvey(): void
    {
        $user = factory(User::class)->create();
        $survey = factory(Survey::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $user->survey->first()->is($survey)
        );
    }

    public function testCall(): void
    {
        $user = factory(User::class)->create();
        $call = factory(Call::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $user->call->first()->is($call)
        );
    }

    public function testReviewReply(): void
    {
        $user = factory(User::class)->create();
        $reply = factory(ReviewReply::class)->create(['user_id' => $user->id]);

        $this->assertTrue(
            $user->review_reply->first()->is($reply)
        );
    }

    /**
     * @param int $roomsCount
     * @param int $hostilityScore
     * @return int
     */
    private function setupOwnerAndOwnershipData(int $roomsCount, int $hostilityScore): int
    {
        // Setup owner data
        $user = factory(User::class)
            ->state('owner')
            ->create(
                [
                    'hostility' => $hostilityScore
                ]
            );

        // Setup room ownership data
        factory(RoomOwner::class, $roomsCount)
            ->state('owner')
            ->create(
                [
                    'user_id' => $user->id,
                    'designer_id' => function() use ($hostilityScore) {
                        return factory(Room::class)
                            ->states(
                                [
                                    'active',
                                    'notTesting',
                                    'availablePriceMonthly'
                                ]
                            )
                            ->create(
                                [
                                    'hostility' => $hostilityScore
                                ]
                            )
                            ->id;
                    }
                ]
            );

        return $user->id;
    }

    public function testGetSafeUserEmailIsValid()
    {
        $user = factory(User::class)->create([
            'email' => 'maudyayundaforever@maudy.com'
        ]);

        factory(AppConfig::class)->create([
            'name' => AppConfig::EMAIL_BLACKLISTED_CONFIG_NAME,
            'value' => 'tenant@mamikos.com,dummy@mail.dummy'
        ]);

        $result = $user->getSafeUserEmail();
        $this->assertNotNull($result);
        $this->assertEquals('maudyayundaforever@maudy.com', $result);
    }

    public function testGetSafeUserEmailIsNotValid()
    {
        $user = factory(User::class)->create([
            'email' => 'dummy@mail.dummy'
        ]);

        factory(AppConfig::class)->create([
            'name' => AppConfig::EMAIL_BLACKLISTED_CONFIG_NAME,
            'value' => 'tenant@mamikos.com,dummy@mail.dummy'
        ]);

        $result = $user->getSafeUserEmail();
        $this->assertNull($result);
    }

    public function testTracking()
    {
        $user = factory(User::class)->create();
        $tracking = factory(Entities\Activity\Tracking::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->tracking->first()->is($tracking));
    }

    public function testUserMedia()
    {
        $user = factory(User::class)->create();
        $userMedia = factory(Entities\User\UserMedia::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->user_medias->first()->is($userMedia));
    }

    public function testCheckerRole()
    {
        $user = factory(User::class)->create();
        $checker = factory(Entities\User\UserChecker::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->checker_role->is($checker));
    }

    public function testVacancyApply()
    {
        $user = factory(User::class)->create();
        $vacancyApply = factory(Entities\Vacancy\VacancyApply::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->vacancy_apply->first()->is($vacancyApply));
    }

    public function testMamipayMedia()
    {
        $user = factory(User::class)->create();
        $media = factory(Entities\Mamipay\MamipayMedia::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->mamipay_media->first()->is($media));
    }

    public function testLevelHistory()
    {
        $user = factory(User::class)->create();
        $history = factory(Entities\Level\LevelHistory::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->level_history->first()->is($history));
    }

    public function testRoomLevelHistory()
    {
        $user = factory(User::class)->create();
        $history = factory(Entities\Level\RoomLevelHistory::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->room_level_histories->first()->is($history));
    }

    public function testReward()
    {
        $user = factory(User::class)->create();
        $reward = factory(Entities\Reward\Reward::class)->create();
        $user->reward()->attach($reward);
        
        $this->assertTrue($user->reward->first()->is($reward));
    }

    public function testRewardRedeem()
    {
        $user = factory(User::class)->create();
        $rewardRedeem = factory(Entities\Reward\RewardRedeem::class)->create([
            'user_id' => $user->id
        ]);

        $rewardRedeemStatusHistory = factory(Entities\Reward\RewardRedeemStatusHistory::class)->create([
            'user_id' => $user->id
        ]);
        
        $this->assertTrue($user->reward_redeem->first()->is($rewardRedeem));
        $this->assertTrue($user->reward_redeem_status_history->first()->is($rewardRedeemStatusHistory));
    }

    public function testDesignerViewHistory()
    {
        $user = factory(User::class)->create();
        $designerView = factory(Entities\User\UserDesignerViewHistory::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertEquals($designerView->user_id, $user->designer_view_history->first()->user_id);
    }

    public function testRegisterWithSocial()
    {
        $user = factory(User::class)->create();
        factory(UserSocial::class)->create([
            'user_id' => $user->id,
            'type' => 'facebook'
        ]);
        $this->assertEquals('facebook', $user->registerWith());
    }

    public function testRegisterWithEmail()
    {
        $user = factory(User::class)->create([
            'email' => 'dummy@email.com'
        ]);
        $this->assertEquals('email', $user->registerWith());
    }

    public function testRegisterWithPhoneNumber()
    {
        $user = factory(User::class)->create([
            'email' => null,
            'phone_number' => '08123456789'
        ]);
        $this->assertEquals('phone number', $user->registerWith());
    }

    public function testRecyclePhoneNumber()
    {
        $user = factory(User::class)->create([
            'phone_number' => '08123456789'
        ]);
        $user->recyclePhoneNumber();
        $this->assertEquals(null, $user->fresh()->phone_number);
    }

    public function testSetAsTester()
    {
        $user = factory(User::class)->create([
            'is_tester' => false
        ]);
        $user->setAsTester(true);
        $this->assertEquals(true, $user->fresh()->is_tester);
    }

    public function testHostility()
    {
        $owner = factory(User::class)->state('owner')->create();
        $expected = factory(Hostility::class)->create(['user_id' => $owner->id]);

        $this->assertTrue(
            $owner->hostility_reason->is($expected)
        );
    }

    public function testSettingNotif()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->setting_notif());
    }

    public function testNotificationLog()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->notification_log());
    }

    public function testUserKos()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->user_kos());
    }

    public function testReferrer()
    {
        $this->assertInstanceOf(HasOne::class, (new User)->referrer());
    }

    public function testReferralTracking()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->referral_tracking());
    }

    public function testForumThreads()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->forum_threads());
    }

    public function testForumAnswers()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->forum_answers());
    }

    public function testForumThreadVotes()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->forum_thread_votes());
    }

    public function testForumAnswerVotes()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->forum_answer_votes());
    }

    public function testForumUser()
    {
        $this->assertInstanceOf(HasOne::class, (new User)->forum_user());
    }

    public function testForumPoints()
    {
        $this->assertInstanceOf(HasMany::class, (new User)->forum_points());
    }

    public function testForumCategories()
    {
        $this->assertInstanceOf(BelongsToMany::class, (new User)->forum_categories());
    }

    public function testAutoVerifyEmailTenantSocial()
    {
        $user = factory(User::class)->states('tenant')->create();
        $userVerify = factory(UserVerificationAccount::class)->create([
            'user_id' => $user->id,
            'is_verify_email' => 0
        ]);

        (new User)->autoVerifyEmailTenantSocial($user->email);
        $res = UserVerificationAccount::find($userVerify->id);
        
        $this->assertSame($res->is_verify_email, 1);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testTryCreateChatUserSendBirdDataAccessTokenNotNull()
    {
        $userFactory = factory(User::class)->create();
        $mockUser = $this->mockPartialAlternatively('\App\User');
        $mockUser->id = $userFactory->id;
        $mockUser->chat_access_token = 'access_token_user';
        $mockUser->shouldReceive('save')->andReturn(true);

        $mockSendBird = $this->mockPartialAlternatively('alias:\App\Facades\SendBirdFacade');
        $mockSendBird->shouldReceive('getUserDetail')
            ->with($mockUser->id)
            ->andReturn([
                'access_token' => 'access_token'
            ]);

        $this->assertNull($mockUser->tryCreateChatUser());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testTryCreateChatUserSendBirdDataAccessTokenNull()
    {
        $userFactory = factory(User::class)->create();
        $mockUser = $this->mockPartialAlternatively('\App\User');
        $mockUser->id = $userFactory->id;
        $mockUser->chat_access_token = 'access_token_user';
        $mockUser->shouldReceive('save')->andReturn(true);

        $mockSendBird = $this->mockPartialAlternatively('alias:\App\Facades\SendBirdFacade');
        $mockSendBird->shouldReceive('getUserDetail')
            ->with($mockUser->id)
            ->andReturn([
                'access_token' => ''
            ]);
        $mockSendBird->shouldReceive('revokeAccessToken')->with($mockUser->id);

        $this->assertNull($mockUser->tryCreateChatUser());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testTryCreateChatUserSendBirdDataNotNull()
    {
        $mockUser = $this->mockPartialAlternatively('\App\User');
        $mockUser->shouldReceive('getSafeUserNameOrDefault')->andReturn('nickname');
        $mockUser->shouldReceive('save')->andReturn(true);

        $mockSendBird = $this->mockPartialAlternatively('alias:\App\Facades\SendBirdFacade');
        $mockSendBird->shouldReceive('getUserDetail')->andReturn(null);
        $mockSendBird->shouldReceive('createUserWithAccessToken')->andReturn([
            'access_token' => 'access_token'
        ]);

        $mockUser->tryCreateChatUser();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testTryCreateChatUserReturnException()
    {
        $exception = new \Exception();
        $mockUser = $this->mockPartialAlternatively('\App\User');
        $mockUser->shouldReceive('getSafeUserNameOrDefault')->andReturn('nickname');
        $mockUser->shouldReceive('save')->andThrow($exception);

        $mockSendBird = $this->mockPartialAlternatively('alias:\App\Facades\SendBirdFacade');
        $mockSendBird->shouldReceive('getUserDetail')->andReturn(null);
        $mockSendBird->shouldReceive('createUserWithAccessToken')->andReturn([
            'access_token' => 'access_token'
        ]);

        Bugsnag::shouldReceive('notifyException')->once()->with($exception);

        $mockUser->tryCreateChatUser();
    }
}
