<?php

namespace App\Imports;

use App\Test\MamiKosTestCase;

class RevertUpdateConsultantImportTest extends MamiKosTestCase
{

    public function testArray()
    {
        $data = [
            [1,2,3,4,5],
            [6,7,8,9,0]
        ];

        $importClass = new RevertUpdateConsultantImport();
        $result = $importClass->array($data);

        $expected = [
            [
                'call_id' => 1,
                'chat_group_id' => 2,
                'designer_id' => 3,
                'old_admin_id' => 4,
                'new_admin_id' => 5
            ],
            [
                'call_id' => 6,
                'chat_group_id' => 7,
                'designer_id' => 8,
                'old_admin_id' => 9,
                'new_admin_id' => 0
            ]
        ];

        $this->assertEquals($expected,$result);
    }
}
