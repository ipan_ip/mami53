<?php

namespace app\Facades;

use App\Test\MamiKosTestCase;
use App\Facades\ABTestClientFacade;

class TestABTestClientFacade extends ABTestClientFacade
{
    public static function getProtectedMethod()
    {
        return parent::getFacadeAccessor();
    }
}

class ABTestClientFacadeTest extends MamiKosTestCase
{

    public function testGetFacadeAccessor()
    {
        $expected = 'abtestclient';

        $this->assertEquals($expected, TestABTestClientFacade::getProtectedMethod());
    }
}
