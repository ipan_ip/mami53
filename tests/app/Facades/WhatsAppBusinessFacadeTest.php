<?php

namespace App\Facades;

use App\Test\MamiKosTestCase;

class WhatsAppBusinessFacadeTest extends MamiKosTestCase
{
    public function testGetFacadeAccessor(): void
    {
        $expected = 'whatsappbusiness';
        $actual = $this->callNonPublicMethod(WhatsAppBusinessFacade::class, 'getFacadeAccessor', []);
        $this->assertEquals($expected, $actual);
    }
}
