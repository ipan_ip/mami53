<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Notif\MamiKosPushNotifData;
use Mockery;
use Notification;
use App\Test\MamiKosTestCase;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NewThreadForSubscriberTest extends MamikosTestCase
{
    private $user;
    private $thread;
    private $notifThread;
    private $pushNotifData;

    protected function setUp() : void
    {
        $this->user = Mockery::mock('alias:App\User');
        $this->user->shouldReceive('find')->andReturn($this->user);
        $this->thread = Mockery::mock('alias:App\Entities\Forum\ForumThread');
        $this->thread->shouldReceive('with')->andReturn($this->thread);
        $this->thread->shouldReceive('find')->andReturn($this->thread);
        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->pushNotifData->user_id = 1;
        $this->pushNotifData->params = json_encode(['thread_id' => 100]);
        $this->notifThread = Mockery::mock('overload:App\Notifications\Forum\NewThreadForSubscriber');
        $this->notifThread->shouldReceive('setPushNotifData')->once()->with($this->pushNotifData);
    }

    public function testhandle()
    {
        $notification = Mockery::mock('alias:Notification');
        $notification->shouldReceive('send')->once();

        $handler = new NewThreadForSubscriber();
        $result = $handler->handle($this->pushNotifData);
        $this->assertNull($result);
    }
}
