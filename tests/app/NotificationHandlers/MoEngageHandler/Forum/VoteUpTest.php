<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Notif\MamiKosPushNotifData;
use Mockery;
use Notification;
use App\Test\MamiKosTestCase;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class VoteUpTest extends MamikosTestCase
{
    private $user;
    private $answer;
    private $vote;
    private $pushNotifData;

    protected function setUp() : void
    {
        $this->user = Mockery::mock('alias:App\User');
        $this->user->shouldReceive('find')->andReturn($this->user);
        $this->answer = Mockery::mock('alias:App\Entities\Forum\ForumAnswer');
        $this->answer->shouldReceive('with')->andReturn($this->answer);
        $this->answer->shouldReceive('find')->andReturn($this->answer);
        $this->vote = Mockery::mock('alias:App\Entities\Forum\ForumVote');
        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->pushNotifData->user_id = 1;
        $this->pushNotifData->params = json_encode(['thread_id' => 100]);
        $this->notifThread = Mockery::mock('overload:App\Notifications\Forum\VoteUp');
        $this->notifThread->shouldReceive('setPushNotifData')->once()->with($this->pushNotifData);
    }

    public function testhandle_voteFound()
    {
        $this->vote->shouldReceive('with')->andReturn($this->vote);
        $this->vote->shouldReceive('find')->andReturn($this->vote);
        $notification = Mockery::mock('alias:Notification');
        $notification->shouldReceive('send')->once();

        $handler = new VoteUp();
        $result = $handler->handle($this->pushNotifData);
        $this->assertNull($result);
    }

    public function testhandle_voteNotFound()
    {
        $this->vote->shouldReceive('with')->andReturn($this->vote);
        $this->vote->shouldReceive('find')->andReturn(null);
        $this->pushNotifData->expects($this->once())->method('save')->willReturn($this->anything());

        $handler = new VoteUp();
        $result = $handler->handle($this->pushNotifData);
        $this->assertNull($result);
    }
}
