<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Notif\MamiKosPushNotifData;
use Mockery;
use Notification;
use App\Test\MamiKosTestCase;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class NewAnswerForJuniorTest extends MamikosTestCase
{
    private $user;
    private $answer;
    private $notifAnswer;
    private $pushNotifData;

    protected function setUp() : void
    {
        $this->user = Mockery::mock('alias:App\User');
        $this->user->shouldReceive('find')->andReturn($this->user);
        $this->answer = Mockery::mock('alias:App\Entities\Forum\ForumAnswer');
        $this->answer->shouldReceive('with')->andReturn($this->answer);
        $this->answer->shouldReceive('find')->andReturn($this->answer);
        $this->pushNotifData = $this->createMock(MamiKosPushNotifData::class);
        $this->pushNotifData->user_id = 1;
        $this->pushNotifData->params = json_encode(['thread_id' => 100]);
        $this->notifAnswer = Mockery::mock('overload:App\Notifications\Forum\NewAnswerForJunior');
        $this->notifAnswer->shouldReceive('setPushNotifData')->once()->with($this->pushNotifData);
    }

    public function testhandle()
    {
        $notification = Mockery::mock('alias:Notification');
        $notification->shouldReceive('send')->once();

        $handler = new NewAnswerForJunior();
        $result = $handler->handle($this->pushNotifData);
        $this->assertNull($result);
    }
}
