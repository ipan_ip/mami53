<?php

namespace App\Channel\InfoBip;

use App\Test\MamiKosTestCase;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class WhatsAppBusinessClientTest extends MamiKosTestCase
{
    private $client;

    protected function setUp() : void
    {
        parent::setUp();
        $response = $this->createMock(ResponseInterface::class);
        $this->client = $this->createMock(Client::class);
        $this->client->method('__call')->with($this->isType('string'), $this->isType('array'))->willReturn($response);
    }

    public function testAsync()
    {
        $waBusiness = $this->createWABusiness();
        $waBusiness->async();
        $this->assertSame(true, $this->getNonPublicAttributeFromClass('isAsyncronous', $waBusiness));
    }

    public function testCallback()
    {
        $callback = function() {
            return true;
        };
        $waBusiness = $this->createWABusiness();
        $waBusiness->callback($callback);
        $this->assertSame($callback, $this->getNonPublicAttributeFromClass('requestCallback', $waBusiness));
    }

    public function testSetParams()
    {
        $params = ['user_id' => 123];
        $waBusiness = $this->createWABusiness();
        $waBusiness->setParams($params);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $waBusiness));
    }

    public function testAddParam()
    {
        $key = 'user_id';
        $value = 123;
        $params = [$key => $value];
        $waBusiness = $this->createWABusiness();
        $waBusiness->addParam($key, $value);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $waBusiness));
    }

    public function testClearParams()
    {
        $params = ['user_id' => 123];
        $waBusiness = $this->createWABusiness();
        $waBusiness->setParams($params);
        $waBusiness->clearParams();
        $this->assertSame([], $this->getNonPublicAttributeFromClass('additionalParams', $waBusiness));
    }

    public function testGet()
    {
        $waBusiness = $this->createWABusiness();
        $result = $waBusiness->get('/');
        $headers = ['content-type' => 'application/json', 'accept' => 'application/json', 'authorization' => 'whatsapp_business.auth_header'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $waBusiness));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPost_NotAsync()
    {
        $params = ['user_id' => 123];
        $waBusiness = $this->createWABusiness();
        $result = $waBusiness->post('/', $params);
        $headers = ['content-type' => 'application/json', 'accept' => 'application/json', 'authorization' => 'whatsapp_business.auth_header'];
        $body = json_encode($params);
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $waBusiness));
        $this->assertContains($body, $this->getNonPublicAttributeFromClass('args', $waBusiness));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPost_Async()
    {
        $waBusiness = $this->createWABusiness();
        $waBusiness->async();
        $result = $waBusiness->post('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPut_NotAsync()
    {
        $waBusiness = $this->createWABusiness();
        $result = $waBusiness->put('/');
        $headers = ['content-type' => 'application/json', 'accept' => 'application/json', 'authorization' => 'whatsapp_business.auth_header'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $waBusiness));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testPut_Async()
    {
        $waBusiness = $this->createWABusiness();
        $waBusiness->async();
        $result = $waBusiness->put('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testDelete_NotAsync()
    {
        $waBusiness = $this->createWABusiness();
        $result = $waBusiness->delete('/');
        $headers = ['content-type' => 'application/json', 'accept' => 'application/json', 'authorization' => 'whatsapp_business.auth_header'];
        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $waBusiness));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testDelete_Async()
    {
        $waBusiness = $this->createWABusiness();
        $waBusiness->async();
        $result = $waBusiness->delete('/');
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    private function createWABusiness()
    {
        return new WhatsAppBusinessClient($this->client);
    }
}

function config($key)
{
    return $key;
}
