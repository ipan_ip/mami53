<?php

namespace App\Channel\ABTest;

use App\Test\MamiKosTestCase;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use App\User;

class ABTestClientTest extends MamiKosTestCase
{

    private $client;

    public function setUp(): void
    {
        parent::setUp();
        $response = $this->createMock(ResponseInterface::class);
        $this->client = $this->createMock(Client::class);
        $this->client->method('__call')->with($this->isType('string'), $this->isType('array'))->willReturn($response);
    }

    private function createABTest()
    {
        return new ABTestClient($this->client);
    }

    public function testPostClient()
    {
        $user = factory(User::class)->create();
        $abTest = $this->createABTest();
        $params = [
            'user_id'       => $user->id,
            'device_id'     => null,
            'session_id'    => null,
            'experiment_id' => 0
        ];

        
        $result = $abTest->post('/', $params);
        $body = json_encode($params);
        
        $headers = [
            "Content-Type" => "application/json",
            "Authorization" => "abtest.authorization",
            "x-api-key" => "abtest.api_key"
        ];

        $this->assertContains($headers, $this->getNonPublicAttributeFromClass('args', $abTest));
        $this->assertContains($body, $this->getNonPublicAttributeFromClass('args', $abTest));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

}

function config($key)
{
    return $key;
}