<?php

namespace App\Channel\ABTest;

use App\Test\MamiKosTestCase;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class DashboardAPIClientTest extends MamiKosTestCase
{
    public function tearDown(): void
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testPostWithRawBody()
    {
        $httpClient = \Mockery::mock(Client::class);
        $httpClient->shouldReceive('post')
            ->with(
                config('abtest.base_url') . config('abtest.base_path.dashboard_api'),
                \Mockery::any()
                )
            ->once()
            ->andReturn([]);

        $apiClient= new DashboardAPIClient($httpClient);
        $result = $apiClient->postWithRawBody(['mode' => 'list-experiment'], '{}');
        // assert that it returns anything that http client give.
        $this->assertEquals([], $result);
    }
}