<?php

namespace App\Channel\MoEngage;

use App\Test\MamiKosTestCase;
use Psr\Http\Message\ResponseInterface;
use phpmock\MockBuilder;
use Mockery;
use stdClass;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MoEngageUserPropertiesApiTest extends MamiKosTestCase
{
    private $userId;
    private $userPropertiesIsInstantBookingOnly;
    private $userPropertiesFirstNameOnly;
    private $userPropertiesFirstNameOnlyEmpty;
    private $httpClient;
    private $bugSnag;

    protected function setUp() : void
    {
        $this->userId = '21312312';
        $this->userPropertiesIsInstantBookingOnly = ['is_instant_booking' => true];
        $this->userPropertiesFirstNameOnly = ['first_name' => 'lala'];
        $this->userPropertiesFirstNameOnlyEmail = ['first_name' => 'test@mamikos.com'];
        $this->userPropertiesFirstNameOnlyNumber = ['first_name' => 3012930121];
        $this->userPropertiesFirstNameOnlyEmpty = ['first_name' => ''];
        $this->httpClient = Mockery::mock('overload:GuzzleHttp\Client');
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testReturnBuildPostBodyIsInstantBookingOnly_Array(): void
    {
        $expectedArray = [
            'customer_id' => (string) $this->userId,
            'attributes' => $this->userPropertiesIsInstantBookingOnly
        ];

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('buildPostBody');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $moEngageUserPropertiesApi, 
            [
                $this->userId,
                $this->userPropertiesIsInstantBookingOnly
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame($result, $expectedArray);
    }

    public function testReturnBuildPostBodyFirstNameOnly_Array(): void
    {
        $expectedArray = [
            'customer_id' => (string) $this->userId,
            'attributes' => $this->userPropertiesFirstNameOnly
        ];

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('buildPostBody');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $moEngageUserPropertiesApi, 
            [
                $this->userId,
                $this->userPropertiesFirstNameOnly
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame($result, $expectedArray);
    }

    public function testAddNewUserPropertiesIsInstantBookingOnly_Success(): void
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesIsInstantBookingOnly);
        $this->assertEquals($body, $response);
    }

    public function testAddNewUserPropertiesIsInstantBookingOnly_Fail(): void
    {
        $body = new stdClass();
        $body->status = 'failed';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);
        $this->bugSnag->shouldReceive('notifyError')->once();

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesIsInstantBookingOnly);

        $this->assertEquals($body, $response);
    }

    public function testAddNewUserPropertiesFirstNameOnly_Success(): void
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesFirstNameOnly);
        $this->assertEquals($body, $response);
    }

    public function testAddNewUserPropertiesFirstNameOnly_Fail(): void
    {
        $body = new stdClass();
        $body->status = 'failed';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);
        $this->bugSnag->shouldReceive('notifyError')->once();

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesFirstNameOnly);
        $this->assertEquals($body, $response);
    }

    public function testAddNewUserPropertiesFirstNameOnlyEmpty_Fail(): void
    {
        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesFirstNameOnlyEmpty);
        $this->assertEquals(null, $response);
    }

    public function testAddNewUserProperties_ExceptionThrown(): void
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();
        $response = $moEngageUserPropertiesApi->addNewUserProperties($this->userId, $this->userPropertiesFirstNameOnly);
        $this->assertNull($response);
    }

    public function testFilterFirstName_NonEmpty(): void
    {
        $expectedArray =  $this->userPropertiesFirstNameOnly;
        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('filterFirstName');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $moEngageUserPropertiesApi, 
            [
                $this->userPropertiesFirstNameOnly
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame($result, $expectedArray);
    }

    public function testFilterFirstName_Email(): void
    {
        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('filterFirstName');
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $moEngageUserPropertiesApi, 
            [
                $this->userPropertiesFirstNameOnlyEmail
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame($result, []);
    }

    public function testFilterFirstName_Number(): void
    {
        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('filterFirstName');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $moEngageUserPropertiesApi, 
            [
                $this->userPropertiesFirstNameOnlyNumber
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame($result, []);
    }

    public function testFilterFirstName_Empty(): void
    {
        $moEngageUserPropertiesApi = new MoEngageUserPropertiesApi();

        $moEngageUserPropertiesApiReflector = new \ReflectionClass(MoEngageUserPropertiesApi::class);
        $method = $moEngageUserPropertiesApiReflector->getMethod('filterFirstName');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $moEngageUserPropertiesApi, 
            [
                $this->userPropertiesFirstNameOnlyEmpty
            ]
        );

        $this->assertIsArray($result);
        $this->assertSame([], $result);
    }
}