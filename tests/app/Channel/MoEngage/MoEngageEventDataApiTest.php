<?php
namespace App\Channel\MoEngage;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Booking\BookingUser;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Element\Facility;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\UserVerificationAccount;
use App\Http\Helpers\BookingUserHelper;
use App\Test\MamiKosTestCase;
use App\User;

use Illuminate\Support\Collection;
use Mockery;
use phpmock\MockBuilder;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use stdClass;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MoEngageEventDataApiTest extends MamiKosTestCase
{
    private $httpClient;
    private $mock;

    protected function setUp() : void
    {
        $this->bugSnag = Mockery::mock('overload:Bugsnag\BugsnagLaravel\Facades\Bugsnag');
        $this->httpClient = Mockery::mock('overload:GuzzleHttp\Client');
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    return $key;
                }
            );
        $this->mock = $builder->build();
        $this->mock->enable();
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        $this->mock->disable();
    }

    public function testReportBookingActivated_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $kost = $this->createMock(Room::class);
        $kost->price_quarterly = 3000000;
        $kost->price_semiannually = 6000000;
        $kost->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $bookingOwnerRequest = $this->createMock(BookingOwnerRequest::class);
        $bookingOwnerRequest->method('__get')->will($this->returnCallback(function ($key) use ($kost) {
            if ($key == 'room') {
                return $kost;
            }
            return 123;
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportBookingActivated($bookingOwnerRequest);
        $this->assertEquals($body, $response);
    }

    public function testReportBookingActivated_ReturnNotSuccess()
    {
        $body = new stdClass();
        $body->status = 'failed';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $kost = $this->createMock(Room::class);
        $kost->price_quarterly = 3000000;
        $kost->price_semiannually = 6000000;
        $kost->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $bookingOwnerRequest = $this->createMock(BookingOwnerRequest::class);
        $bookingOwnerRequest->method('__get')->will($this->returnCallback(function ($key) use ($kost) {
            if ($key == 'room') {
                return $kost;
            }
            return 123;
        }));
        $this->bugSnag->shouldReceive('notifyError')->once();

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportBookingActivated($bookingOwnerRequest);
        $this->assertEquals($body, $response);
    }

    public function testReportBookingActivated_NoConfigAppId()
    {
        $this->mock->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == 'moengage.app_id') {
                        return '';
                    }
                    return $key;
                }
            );
        $mock = $builder->build();
        $mock->enable();

        $this->bugSnag->shouldReceive('notifyException')->once();
        $eventData = new MoEngageEventDataApi();
        $this->assertSame('', $this->getNonPublicAttributeFromClass('appId', $eventData));

        $mock->disable();
        $this->mock->enable();
    }

    public function testReportBookingActivated_NoConfigApiBaseUrl()
    {
        $this->mock->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == 'moengage.api_base_url') {
                        return '';
                    }
                    return $key;
                }
            );
        $mock = $builder->build();
        $mock->enable();

        $this->bugSnag->shouldReceive('notifyException')->once();
        $eventData = new MoEngageEventDataApi();
        $this->assertSame('', $this->getNonPublicAttributeFromClass('apiBaseUrl', $eventData));

        $mock->disable();
        $this->mock->enable();
    }

    public function testReportBookingActivated_NoConfigDataApiId()
    {
        $this->mock->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == 'moengage.data_api_id') {
                        return '';
                    }
                    return $key;
                }
            );
        $mock = $builder->build();
        $mock->enable();

        $this->bugSnag->shouldReceive('notifyException')->once();
        $eventData = new MoEngageEventDataApi();
        $this->assertSame(null, $this->getNonPublicAttributeFromClass('authentication', $eventData));

        $mock->disable();
        $this->mock->enable();
    }

    public function testReportBookingActivated_NoConfigDataApiKey()
    {
        $this->mock->disable();
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) {
                    if ($key == 'moengage.data_api_key') {
                        return '';
                    }
                    return $key;
                }
            );
        $mock = $builder->build();
        $mock->enable();

        $this->bugSnag->shouldReceive('notifyException')->once();
        $eventData = new MoEngageEventDataApi();
        $this->assertSame(null, $this->getNonPublicAttributeFromClass('authentication', $eventData));

        $mock->disable();
        $this->mock->enable();
    }

    public function testReportBookingActivated_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $kost = $this->createMock(Room::class);
        $kost->price_quarterly = 3000000;
        $kost->price_semiannually = 6000000;
        $kost->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $bookingOwnerRequest = $this->createMock(BookingOwnerRequest::class);
        $bookingOwnerRequest->method('__get')->will($this->returnCallback(function ($key) use ($kost) {
            if ($key == 'room') {
                return $kost;
            }
            return 123;
        }));
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportBookingActivated($bookingOwnerRequest);
        $this->assertNull($response);
    }

    public function testReportPackagePurchaseConfirmed_PremiumRequestSet_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $premiumPack = $this->createMock(PremiumPackage::class);
        $premiumPack->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $premiumReq = $this->createMock(PremiumRequest::class);
        $premiumReq->method('__get')->will($this->returnCallback(function ($key) use ($premiumPack) {
            if ($key == 'premium_package') {
                return $premiumPack;
            }
            return 'anything';
        }));
        $accountConfirmation = $this->createMock(AccountConfirmation::class);
        $user = $this->createMock(User::class);
        $accountConfirmation->method('__get')->will($this->returnCallback(function ($key) use ($premiumReq, $user) {
            if ($key == 'premium_request') {
                return $premiumReq;
            }
            if ($key == 'user') {
                return $user;
            }
            if ($key == 'updated_at' || $key == 'created_at') {
                return null;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPackagePurchaseConfirmed($accountConfirmation);
        $this->assertEquals($body, $response);
    }

    public function testReportPackagePurchaseConfirmed_ViewBalanceSet_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $premiumPack = $this->createMock(PremiumPackage::class);
        $premiumPack->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));

        $premiumRequest = $this->createMock(PremiumRequest::class);
        $balanceReq = $this->createMock(BalanceRequest::class);
        
        $balanceReq->method('__get')->will($this->returnCallback(function ($key) use ($premiumPack, $premiumRequest) {
            if ($key == 'premium_package') {
                return $premiumPack;
            }
            if ($key == 'premium_request') {
                return $premiumRequest;
            }
            return 1000000;
        }));
        
        $accountConfirmation = $this->createMock(AccountConfirmation::class);
        $accountConfirmation->method('__get')->will($this->returnCallback(function ($key) use ($balanceReq) {
            if ($key == 'premium_request') {
                return null;
            }
            if ($key == 'view_balance') {
                return $balanceReq;
            }
            if ($key == 'user') {
                return null;
            }
            if ($key == 'updated_at' || $key == 'created_at') {
                return null;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPackagePurchaseConfirmed($accountConfirmation);
        $this->assertEquals($body, $response);
    }

    public function testReportPackagePurchaseConfirmed_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $premiumPack = $this->createMock(PremiumPackage::class);
        $premiumPack->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $premiumReq = $this->createMock(PremiumRequest::class);
        $premiumReq->method('__get')->will($this->returnCallback(function ($key) use ($premiumPack) {
            if ($key == 'premium_package') {
                return $premiumPack;
            }
            return 'anything';
        }));
        $accountConfirmation = $this->createMock(AccountConfirmation::class);
        $accountConfirmation->method('__get')->will($this->returnCallback(function ($key) use ($premiumReq) {
            if ($key == 'premium_request') {
                return $premiumReq;
            }
            if ($key == 'user') {
                return null;
            }
            if ($key == 'updated_at' || $key == 'created_at') {
                return null;
            }
            return 'anything';
        }));
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPackagePurchaseConfirmed($accountConfirmation);
        $this->assertEquals($exception, $response);
    }

    public function testReportAddingKostConfirmed_OwnerExist_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collection = $this->createMock(Collection::class);
        $collection->method('count')->willReturn(1);
        $collection->method('first')->willReturn($owner);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));
        $fac = $this->createMock(Facility::class);
        $fac->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $room->method('facility')->willReturn($fac);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingKostConfirmed($room);
        $this->assertEquals($body, $response);
    }

    public function testReportAddingKostConfirmed_OwnerNotExist_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $collection = $this->createMock(Collection::class);
        $collection->method('count')->willReturn(0);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingKostConfirmed($room);
        $this->assertNull($response);
    }

    public function testReportAddingKostConfirmed_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collection = $this->createMock(Collection::class);
        $collection->method('count')->willReturn(1);
        $collection->method('first')->willReturn($owner);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));
        $fac = $this->createMock(Facility::class);
        $fac->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $room->method('facility')->willReturn($fac);
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingKostConfirmed($room);
        $this->assertNull($response);
    }

    public function testReportAddingApartmentConfirmed_OwnerExist_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $apartmentType = $this->createMock(Type::class);
        $apartmentType->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collectionApartmentType = $this->createMock(Collection::class);
        $collectionApartmentType->method('last')->willReturn($apartmentType);
        $collectionApartmentTag = $this->createMock(Collection::class);
        $collectionApartmentTag->method('pluck')->willReturn($collectionApartmentTag);
        $collectionApartmentTag->method('toArray')->willReturn([1,2,3]);
        $apartment = $this->createMock(ApartmentProject::class);
        $apartment->method('__get')->will($this->returnCallback(function ($key) use ($collectionApartmentType, $collectionApartmentTag) {
            if ($key == 'types') {
                return $collectionApartmentType;
            }
            if ($key == 'tags') {
                return $collectionApartmentTag;
            }
            return 'anything';
        }));

        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collectionOwner = $this->createMock(Collection::class);
        $collectionOwner->method('count')->willReturn(1);
        $collectionOwner->method('first')->willReturn($owner);
        $collectionRoomTag = $this->createMock(Collection::class);
        $collectionRoomTag->method('pluck')->willReturn($collectionRoomTag);
        $collectionRoomTag->method('toArray')->willReturn([1,2,3]);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collectionOwner, $collectionRoomTag, $apartment) {
            if ($key == 'owners') {
                return $collectionOwner;
            }
            if ($key == 'tags') {
                return $collectionRoomTag;
            }
            if ($key == 'apartment_project') {
                return $apartment;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingApartmentConfirmed($room);
        $this->assertEquals($body, $response);
    }

    public function testReportAddingApartmentConfirmed_OwnerNotExist_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $collection = $this->createMock(Collection::class);
        $collection->method('count')->willReturn(0);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingApartmentConfirmed($room);
        $this->assertNull($response);
    }

    public function testReportAddingApartmentConfirmed_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $apartmentType = $this->createMock(Type::class);
        $apartmentType->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collectionApartmentType = $this->createMock(Collection::class);
        $collectionApartmentType->method('last')->willReturn($apartmentType);
        $collectionApartmentTag = $this->createMock(Collection::class);
        $collectionApartmentTag->method('pluck')->willReturn($collectionApartmentTag);
        $collectionApartmentTag->method('toArray')->willReturn([1,2,3]);
        $apartment = $this->createMock(ApartmentProject::class);
        $apartment->method('__get')->will($this->returnCallback(function ($key) use ($collectionApartmentType, $collectionApartmentTag) {
            if ($key == 'types') {
                return $collectionApartmentType;
            }
            if ($key == 'tags') {
                return $collectionApartmentTag;
            }
            return 'anything';
        }));

        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) {
            return 123;
        }));
        $collectionOwner = $this->createMock(Collection::class);
        $collectionOwner->method('count')->willReturn(1);
        $collectionOwner->method('first')->willReturn($owner);
        $collectionRoomTag = $this->createMock(Collection::class);
        $collectionRoomTag->method('pluck')->willReturn($collectionRoomTag);
        $collectionRoomTag->method('toArray')->willReturn([1,2,3]);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collectionOwner, $collectionRoomTag, $apartment) {
            if ($key == 'owners') {
                return $collectionOwner;
            }
            if ($key == 'tags') {
                return $collectionRoomTag;
            }
            if ($key == 'apartment_project') {
                return $apartment;
            }
            return 'anything';
        }));

        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportAddingApartmentConfirmed($room);
        $this->assertNull($response);
    }

    public function testReportPropertyBooked_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $bookingUser = factory(BookingUser::class)->make();
        $bookingUser->designer = factory(Room::class)->make();
        $bookingUser->designer->owners = factory(RoomOwner::class, 3)->make()->each(function($item) {
            $item->user = factory(User::class)->make([
                'id' => rand(1,1000)
            ]);
        });

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPropertyBooked($bookingUser);
        $this->assertEquals($body, $response);
    }

    public function testReportPropertyBooked_Null()
    {
        $bookingUser = factory(BookingUser::class)->make();
        $bookingUser->designer = factory(Room::class)->make();
        $bookingUser->designer->owners = [];

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPropertyBooked($bookingUser);
        $this->assertEquals(null, $response);
    }

    public function testReportPropertyBooked_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $bookingUser = factory(BookingUser::class)->make();
        $bookingUser->designer = factory(Room::class)->make();
        $bookingUser->designer->owners = factory(RoomOwner::class, 3)->make()->each(function($item) {
            $item->user = factory(User::class)->make([
                'id' => rand(1,1000)
            ]);
        });
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportPropertyBooked($bookingUser);
        $this->assertNull($response);
    }

    public function testReportBookingConfirmed_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $userOwner = $this->createMock(User::class);
        $userOwner->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) use ($userOwner) {
            return $userOwner;
        }));
        $collection = $this->createMock(Collection::class);
        $collection->method('first')->willReturn($owner);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));
        $userTenant = $this->createMock(User::class);
        $userTenant->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $bookingUser = $this->createMock(BookingUser::class);
        $bookingUser->method('__get')->will($this->returnCallback(function ($key) use ($room, $userTenant) {
            if ($key == 'designer') {
                return $room;
            }
            if ($key == 'user') {
                return $userTenant;
            }
            return 'anything';
        }));

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportBookingConfirmed($bookingUser);
        $this->assertEquals($body, $response);
    }

    public function testReportBookingConfirmed_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        $userOwner = $this->createMock(User::class);
        $userOwner->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $owner = $this->createMock(RoomOwner::class);
        $owner->method('__get')->will($this->returnCallback(function ($key) use ($userOwner) {
            return $userOwner;
        }));
        $collection = $this->createMock(Collection::class);
        $collection->method('first')->willReturn($owner);
        $room = $this->createMock(Room::class);
        $room->method('__get')->will($this->returnCallback(function ($key) use ($collection) {
            if ($key == 'owners') {
                return $collection;
            }
            return 'anything';
        }));
        $userTenant = $this->createMock(User::class);
        $userTenant->method('__get')->will($this->returnCallback(function ($key) {
            return 'anything';
        }));
        $bookingUser = $this->createMock(BookingUser::class);
        $bookingUser->method('__get')->will($this->returnCallback(function ($key) use ($room, $userTenant) {
            if ($key == 'designer') {
                return $room;
            }
            if ($key == 'user') {
                return $userTenant;
            }
            return 'anything';
        }));
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportBookingConfirmed($bookingUser);
        $this->assertNull($response);
    }

    public function testReportEmailVerification_Success()
    {
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        $user = factory(\App\User::class)->make([
            'id' => 1
        ]);
        $eventData = new MoEngageEventDataApi();
        $result = $eventData->reportEmailVerification($user, ['interface' => 'Android']);
        $this->assertEquals($body, $result);
    }

    public function testReportEmailVerification_ExceptionThrown()
    {
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $user = factory(\App\User::class)->make([
            'id' => 1
        ]);
        $eventData = new MoEngageEventDataApi();
        $result = $eventData->reportEmailVerification($user, ['interface' => 'Android']);
        $this->assertEquals((object)[],$result);
    }

    public function testReportEmailVerification_Abort404()
    {
        try {
            $user = factory(\App\User::class)->make([
                'id' => 1
            ]);
            $eventData = new MoEngageEventDataApi();
            $eventData->reportEmailVerification($user, []);
        } catch (\Throwable $e) {
        }

        $this->assertEquals(404, $e->getStatusCode());
        $this->assertEquals(new NotFoundHttpException(), $e);
    }

    public function testReportEmailVerificationUserIdNull_Abort404()
    {
        try {
            $user = factory(\App\User::class)->make();
            $eventData = new MoEngageEventDataApi();
            $eventData->reportEmailVerification($user, ['interface' => 'Android']);
        } catch (\Throwable $e) {
        }

        $this->assertEquals(404, $e->getStatusCode());
        $this->assertEquals(new NotFoundHttpException(), $e);
    }

    public function testReportUserBookingSubmittedSuccess(): void
    {
        // mock data http
        $body = new stdClass();
        $body->status = 'success';
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode($body));
        $this->httpClient->shouldReceive('post')->andReturn($response);

        // prepare data user
        $user = factory(User::class)->make();
        $user->user_verification_account = factory(UserVerificationAccount::class)->make();

        // data booking user
        $bookingUser = factory(BookingUser::class)->make();
        $bookingUser->designer = factory(Room::class)->make();
        $bookingUser->user = $user;

        $interface = BookingUserHelper::INTERFACE_IOS;

        // run test
        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportUserBookingSubmitted($bookingUser, $interface);
        $this->assertEquals($body, $response);
    }

    public function testReportUserBookingSubmittedExceptionThrown(): void
    {
        $this->markTestSkipped();
        // mock http
        $exception = new \Exception;
        $this->httpClient->shouldReceive('post')->andThrow($exception);

        // data booking user
        $bookingUser = factory(BookingUser::class)->make();
        $bookingUser->designer = factory(Room::class)->make();

        // mock bugSnag
        $this->bugSnag->shouldReceive('notifyException')->once()->with($exception);

        $eventData = new MoEngageEventDataApi();
        $response = $eventData->reportUserBookingSubmitted($bookingUser, null);
        $this->assertNull($response);
    }
}
