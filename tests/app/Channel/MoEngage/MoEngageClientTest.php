<?php

namespace App\Channel\MoEngage;

use App\Test\MamiKosTestCase;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class MoEngageClientTest extends MamiKosTestCase
{
    private $client;

    protected function setUp() : void
    {
        parent::setUp();
        $response = $this->createMock(ResponseInterface::class);
        $this->client = $this->createMock(Client::class);
        $this->client->method('__call')->with($this->isType('string'), $this->isType('array'))->willReturn($response);
    }

    public function testAsync()
    {
        $channel = new MoEngageClient($this->client);
        $channel->async();
        $this->assertSame(true, $this->getNonPublicAttributeFromClass('requestAsync', $channel));
    }

    public function testCallback()
    {
        $callback = function() {
            return true;
        };
        $channel = new MoEngageClient($this->client);
        $channel->callback($callback);
        $this->assertSame($callback, $this->getNonPublicAttributeFromClass('requestCallback', $channel));
    }

    public function testAddParams()
    {
        $params = ['appSecret' => 'xyz'];
        $channel = new MoEngageClient($this->client);
        $channel->addParams($params);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $channel));
    }

    public function testSetParam()
    {
        $key = 'appSecret';
        $value = 'xyz';
        $params = [$key => $value];
        $channel = new MoEngageClient($this->client);
        $channel->setParam($key, $value);
        $this->assertSame($params, $this->getNonPublicAttributeFromClass('additionalParams', $channel));
    }

    public function testSendNotification_NotAsync()
    {
        $params = ['appSecret' => 'xyz'];
        $channel = new MoEngageClient($this->client);
        $result = $channel->sendNotification($params);
        $this->assertContains(['Content-Type' => 'application/json'], $this->getNonPublicAttributeFromClass('headers', $channel));
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    public function testSendNotification_Async()
    {
        $params = ['appSecret' => 'xyz'];
        $channel = new MoEngageClient($this->client);
        $channel->async();
        $result = $channel->sendNotification($params);
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }
}

function config($key)
{
    return $key;
}
