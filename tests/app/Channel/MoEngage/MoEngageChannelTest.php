<?php

namespace App\Channel\MoEngage;

use App\Notifications\MoEngageSimpleNotification;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Bugsnag\Report;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mockery;
use Psr\Http\Message\ResponseInterface;

class MoEngageChannelTest extends MamiKosTestCase
{
    private $client;
    private $user;
    private $notification;

    protected function setUp() : void
    {
        parent::setUp();
        $this->client = $this->createMock(MoEngageClient::class);
        $this->user = $this->createMock(User::class);
        $this->notification = $this->createMock(MoEngageSimpleNotification::class);
    }

    public function testSend_NoRouteForMoEngage()
    {
        $this->user->method('routeNotificationFor')->willReturn(null);

        $channel = new MoEngageChannel($this->client);
        $result = $channel->send($this->user, $this->notification);
        $this->assertNull($result);
    }

    public function testSend_Success()
    {
        $this->user->method('routeNotificationFor')->willReturn(1);
        $this->notification->method('toMoEngage')->willReturn($this->anything());
        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')->willReturn(json_encode(['status' => 'Success']));
        $this->client->method('sendNotification')->willReturn($response);
        $this->notification
            ->expects($this->once())
            ->method('updatePushNotifData')
            ->willReturn($this->anything());

        $channel = new MoEngageChannel($this->client);
        $result = $channel->send($this->user, $this->notification);
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }

    /**
     * @covers \App\Channel\MoEngage\MoEngageChannel::send
     */
    public function testSend_ExceptionThrown()
    {
        $this->user->method('routeNotificationFor')->willReturn(1);
        $this->notification->method('toMoEngage')->willReturn($this->anything());
        $exception = new ServerException(
            'exception',
            new Request('get', 'http://example.com'),
            new Response()
        );
        $this->client->method('sendNotification')->willThrowException($exception);
        Bugsnag::shouldReceive('notifyException')->once();
        $this->notification
            ->expects($this->once())
            ->method('updatePushNotifData')
            ->willReturn($this->anything());

        $channel = new MoEngageChannel($this->client);
        $result = $channel->send($this->user, $this->notification);
        $this->assertNull($result);
    }

    public function testBugsnagRequestMetaLogClosure()
    {
        $channel = new MoEngageChannel($this->client);
        $exception = new ServerException(
            'exception',
            new Request('get', 'http://example.com'),
            new Response()
        );
        $moeEngageChannel = new \ReflectionClass(MoEngageChannel::class);
        $method = $moeEngageChannel->getMethod('bugsnagRequestMetaLogClosure');
        $method->setAccessible(true);
        $metaReporter = $method->invokeArgs( 
            $channel, 
            [
                $exception
            ]
        );

        $this->assertIsCallable($metaReporter);
        $reportMock = $this->createMock(Report::class);
        $reportMock->method('setMetaData')->willReturn($this->anything());
        $metaReporter($reportMock);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }
}
