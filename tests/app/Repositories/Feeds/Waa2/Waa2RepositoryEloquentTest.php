<?php

namespace App\Repositories\Feeds\Waa2;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;

class Waa2RepositoryEloquentTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp()
    {
        parent::setUp();

        $this->repo = $this->app->make(Waa2RepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    public function testGetPremium()
    {
        $res = $this->repo->getPremium();
        
        $this->assertInstanceof(Builder::class, $res);
    }

    public function testGetReguler()
    {
        $res = $this->repo->getReguler();
        
        $this->assertInstanceof(Builder::class, $res);
    }
}