<?php

namespace App\Repositories\Feeds\Google;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use phpmock\MockBuilder;

class GoogleRepositoryEloquentTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp()
    {
        parent::setUp();

        $this->repo = $this->app->make(GoogleRepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    public function testGetIdGoldPlusOn()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->getIdGoldPlusOn(1));
    }

    public function testGetIdMamiroomsOn()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->getIdMamiroomsOn());
    }

    public function testGetIdBisaBookingOn()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->getIdBisaBookingOn());
    }

    public function testGetIdRegulerOn()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->getIdRegulerOn());
    }

    public function testVerifyRoomOwnerOn()
    {
        $roomOwner = [];
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $level = factory(KostLevel::class)->states('gp1', 'gp_level_1')->create();
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        $room = factory(Room::class, $length)->states(
                'availableBooking',
                'notMamirooms',
                'active',
                'notTesting',
                'availablePriceMonthly',
                'notPremium'
            )
            ->create()
            ->each(function($item) use($level) {
                factory(KostLevelMap::class)->create([
                    'kost_id' => $item->song_id,
                    'level_id' => $level->id
                ]);
            });
        $roomIds = $room->pluck('id')->toArray();

        $userArray = $user->toArray();
        foreach($roomIds as $key => $val) {
            $roomOwner[] = factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $userArray[$key]['id'],
                    'designer_id' => $val
                ]);
        };

        $res = $this->repo->verifyRoomOwnerOn($roomIds);
        $first = $res->first();

        $this->assertInstanceOf(Builder::class, $res);
        $this->assertContains($first->designer_id, $roomIds);
        $this->assertSame($first->room->level->first()->id, $level->id);
    }

    public function testGetIdForTrialError()
    {
        $length = 5;
        $room = factory(Room::class, $length)->states(
                'availableBooking',
                'notMamirooms',
                'active',
                'notTesting',
                'availablePriceMonthly',
                'notPremium'
            )
            ->create();

        $res = $this->repo->getIdForTrialError($room->pluck('id')->toArray());

        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetIdGoldPlusOnInputArray()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->getIdGoldPlusOnInputArray([1,2,3]));
    }

    public function testSetIsMamiroomsBasedOnDesigner()
    {
        $res = $this->callNonPublicMethod(
            GoogleRepositoryEloquent::class,
            'setIsMamiroomsBasedOnDesigner',
            []
        );

        $this->assertIsString($res);
        $this->assertIsInt(strpos($res, 'is_mamirooms'));
    }

    public function testSetIsMamiroomsBasedOnKostLevelConfigEmpty()
    {
        $this->assertEmpty(
            $this->callNonPublicMethod(
                GoogleRepositoryEloquent::class,
                'setIsMamiroomsBasedOnKostLevel',
                []
            )
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetIsMamiroomsBasedOnKostLevelConfigFilled()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function () {
                    return mt_rand(1, 100);
                }
            );
        $this->mockConfig = $builder->build();
        $this->mockConfig->enable();

        $this->assertNotEmpty(
            $this->callNonPublicMethod(
                GoogleRepositoryEloquent::class,
                'setIsMamiroomsBasedOnKostLevel',
                []
            )
        );

        $this->mockConfig->disable();
    }

    public function testSetIsMamiroomsBasedDesignerAndKostLevelMamiroomsFail()
    {
        $expectedString = "(is_mamirooms = 1)";

        $res = $this->callNonPublicMethod(
            GoogleRepositoryEloquent::class,
            'setIsMamiroomsBasedDesignerAndKostLevel',
            []
        );

        $this->assertSame($expectedString, $res);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetIsMamiroomsBasedDesignerAndKostLevelMamiroomsSuccess()
    {
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function () {
                    return mt_rand(1, 100);
                }
            );
        $this->mockConfig = $builder->build();
        $this->mockConfig->enable();

        $res = $this->callNonPublicMethod(
            GoogleRepositoryEloquent::class,
            'setIsMamiroomsBasedDesignerAndKostLevel',
            []
        );

        $this->assertIsInt(strpos($res, 'exists'));

        $this->mockConfig->disable();
    }
}