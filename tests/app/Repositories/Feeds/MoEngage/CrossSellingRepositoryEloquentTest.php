<?php

namespace App\Repositories\Feeds\MoEngage;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CrossSellingRepositoryEloquentTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->app->make(CrossSellingRepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    public function testGetReferenceRooms()
    {
        $res = $this->repo->getReferenceRooms();
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testGetReferenceRoomsById()
    {
        $room = factory(Room::class)->create();
        $res = $this->repo->getReferenceRoomsById([$room->id]);
        $this->assertInstanceOf(Builder::class, $res);
    }

    public function testGetCountRecommendedGoldPlusRooms()
    {
        $res = $this->repo->getCountRecommendedGoldPlusRooms('','');
        $this->assertIsInt($res);
    }

    public function testGetRecommendedGoldPlusRooms()
    {
        $res = $this->repo->getRecommendedGoldPlusRooms('','');
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetCountRecommendedMamiRooms()
    {
        $res = $this->repo->getCountRecommendedMamiRooms('','');
        $this->assertIsInt($res);
    }

    public function testGetRecommendedMamiRooms()
    {
        $res = $this->repo->getRecommendedMamiRooms('','');
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetReferenceRoomsByArea()
    {
        $res = $this->repo->getReferenceRoomsByArea();
        $this->assertInstanceOf(Builder::class, $res);
    }
}