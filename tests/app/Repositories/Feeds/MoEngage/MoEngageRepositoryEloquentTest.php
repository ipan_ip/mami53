<?php

namespace App\Repositories\Feeds\MoEngage;

use App\Entities\Level\{
    KostLevel,
    KostLevelMap
};
use App\Entities\Room\{
    Room,
    RoomOwner
};
use App\Test\MamiKosTestCase;
use App\User;
use DateTime;
use Illuminate\Support\Collection;

class MoEngageRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repo, $level;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->app->make(MoEngageRepositoryEloquent::class);
        $this->level = factory(KostLevel::class)->states('goldplus')->create();
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    protected function setRecordsGoldPlus(int $length, DateTime $datetime, int $levelId)
    {
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        factory(Room::class, $length)->states(
                'active',
                'notTesting',
                'availablePriceMonthly'
            )
            ->create([
                'kost_updated_date' => $datetime
            ])
            ->each(function($item, $key) use($user, $levelId) {
                factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $user[$key]->id,
                    'designer_id' => $item->id
                ]);
                factory(KostLevelMap::class)->create([
                    'kost_id' => $item->id,
                    'level_id' => $levelId
                ]);
            });
    }

    protected function setRecordsMamiroomsPremiumOn(int $length, DateTime $datetime)
    {
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        $room = factory(Room::class, $length)->states(
                'mamirooms',
                'active',
                'notTesting',
                'availablePriceMonthly'
            )
            ->create();

        $roomOwner = collect();
        $roomArray = $room->toArray();
        $userArray = $user->toArray();
        foreach($roomArray as $key => $val) {
            $roomOwner[] = factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $userArray[$key]['id'],
                    'designer_id' => $val['id']
                ]);
        };
    }

    protected function setRecordsBisaBookingPremiumOn(int $length, DateTime $datetime)
    {
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        $room = factory(Room::class, $length)->states(
                'availableBooking',
                'active',
                'notTesting',
                'availablePriceMonthly'
            )
            ->create();

        $roomOwner = collect();
        $roomArray = $room->toArray();
        $userArray = $user->toArray();
        foreach($roomArray as $key => $val) {
            $roomOwner[] = factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $userArray[$key]['id'],
                    'designer_id' => $val['id']
                ]);
        };
    }

    protected function setRecordsNonMamiroomsNonBisaBookingPremiumOn(int $length, DateTime $datetime)
    {
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        $room = factory(Room::class, $length)->states(
                'notAvailableBooking',
                'notMamirooms',
                'active',
                'notTesting',
                'availablePriceMonthly'
            )
            ->create();

        $roomOwner = collect();
        $roomArray = $room->toArray();
        $userArray = $user->toArray();
        foreach($roomArray as $key => $val) {
            $roomOwner[] = factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $userArray[$key]['id'],
                    'designer_id' => $val['id']
                ]);
        };
    }

    protected function setRecordsReguler(int $length, DateTime $datetime)
    {
        $user = factory(User::class, $length)->create([
            'date_owner_limit' => $datetime->format('Y-m-d')
        ]);
        $room = factory(Room::class, $length)->states(
                'notAvailableBooking',
                'notMamirooms',
                'active',
                'notTesting',
                'availablePriceMonthly',
                'notPremium'
            )
            ->create();
            /* ->create([
                'name' => 'lala',
                'is_promoted' => 'false'
            ]); */

        $roomOwner = collect();
        $roomArray = $room->toArray();
        $userArray = $user->toArray();
        foreach($roomArray as $key => $val) {
            $roomOwner[] = factory(RoomOwner::class)->states(
                    'owner'
                )
                ->create([
                    'user_id' => $userArray[$key]['id'],
                    'designer_id' => $val['id']
                ]);
        };
    }

    public function testGetCountGoldPlus()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsGoldPlus($length, $datetime, $this->level->id);
        $res = $this->repo->getCountGoldPlus();

        $this->assertIsInt($res);
    }

    public function testGetGoldPlus()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsGoldPlus($length, $datetime, $this->level->id);
        $res = $this->repo->getGoldPlus(0, 10);

        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetCountMamirooms()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsMamiroomsPremiumOn($length, $datetime);
        $res = $this->repo->getCountMamirooms();
        
        $this->assertGreaterThanOrEqual($length, $res);
        $this->assertIsInt($res);
    }

    public function testGetMamirooms()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsMamiroomsPremiumOn($length, $datetime);
        $res = $this->repo->getMamirooms(0, 10);
        
        $this->assertGreaterThanOrEqual($length, $res->count());
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetCountBisaBooking()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsBisaBookingPremiumOn($length, $datetime);
        $res = $this->repo->getCountBisaBooking();
        
        $this->assertGreaterThanOrEqual($length, $res);
        $this->assertIsInt($res);
    }

    public function testGetBisaBooking()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsBisaBookingPremiumOn($length, $datetime);
        $res = $this->repo->getBisaBooking(0, 10);

        $this->assertGreaterThanOrEqual($length, $res->count());
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetCountRegulerPremium()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsNonMamiroomsNonBisaBookingPremiumOn($length, $datetime);
        $res = $this->repo->getCountRegulerPremium();
        
        $this->assertGreaterThanOrEqual($length, $res);
        $this->assertIsInt($res);
    }

    public function testGetRegulerPremium()
    {
        $length = 10;
        $datetime = new \DateTime('tomorrow');

        $this->setRecordsNonMamiroomsNonBisaBookingPremiumOn($length, $datetime);
        $res = $this->repo->getRegulerPremium(0, 10);
        
        $this->assertGreaterThanOrEqual($length, $res->count());
        $this->assertInstanceOf(Collection::class, $res);
    }

    public function testGetCountReguler()
    {
        $length = 10;
        $datetime = new \DateTime('yesterday');

        $this->setRecordsReguler($length, $datetime);
        $res = $this->repo->getCountReguler();
        
        $this->assertGreaterThanOrEqual($length, $res);
        $this->assertIsInt($res);
    }

    public function testGetReguler()
    {
        $length = 10;
        $datetime = new \DateTime('yesterday');

        $this->setRecordsReguler($length, $datetime);
        $res = $this->repo->getReguler(0, 10);

        $this->assertInstanceOf(Collection::class, $res);
    }
}