<?php

namespace App\Repositories\Feeds\Flatfy;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use App\Entities\Room\Room;

class FlatfyRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->app->make(FlatfyRepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repository->model());
    }

    public function testGetPremiumCount()
    {
        factory(Room::class, 3)->create([
            'is_active' => 'true',
            'expired_phone' => 0,
            'price_monthly' => 100000,
            'room_available' => 2,
            'room_count' => 2,
            'is_promoted' => true,
            'area_city' => 'Tester'
        ]);
        $count = $this->repository->getPremiumCount();
        $this->assertIsInt($count);
    }

    public function testGetPremium()
    {
        factory(Room::class, 3)->create([
            'is_active' => 'true',
            'expired_phone' => 0,
            'price_monthly' => 100000,
            'room_available' => 2,
            'room_count' => 2,
            'is_promoted' => true,
            'area_city' => 'Tester'
        ]);
        $result = $this->repository->getPremium(0, 3);
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testGetRegulerCount()
    {
        factory(Room::class, 3)->create([
            'is_active' => 'true',
            'area_city' => 'Tester',
            'room_available' => 2,
            'room_count' => 2,
            'expired_phone' => 0,
            'price_monthly' => 100000
        ]);
        $count = $this->repository->getRegulerCount('Tester');
        $this->assertIsInt($count);
    }

    public function testGetReguler()
    {
        factory(Room::class, 10)->create([
            'is_active' => 'true',
            'area_city' => 'Tester',
            'room_available' => 2,
            'room_count' => 2,
            'expired_phone' => 0,
            'price_monthly' => 100000
        ]);
        $result = $this->repository->getReguler('Tester', 0, 3);
        $this->assertInstanceOf(Collection::class, $result);
    }
}
