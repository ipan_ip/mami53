<?php

namespace App\Repositories\Feeds\Facebook;

use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use App\Entities\Room\Room;
use Carbon\Carbon;

class FacebookRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->app->make(FacebookRepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repository->model());
    }

    public function testGetCount()
    {
        $count = $this->repository->getCount();
        $this->assertIsInt($count);
    }

    public function testGetData()
    {
        $result = $this->repository->getData();
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testGetDataWithOffsetLimit()
    {
        $rooms = factory(Room::class, 5)->states(
            'active',
            'notTesting',
            'availablePriceMonthly',
            'with-small-rooms'
            )
            ->create([
                'kost_updated_date' => Carbon::now()
            ]);
        $ids = $rooms->pluck('id')->toArray();
        $res = $this->repository->getDataWithOffsetLimit(0, 5)
            ->get()
            ->pluck('id')
            ->toArray();

        $this->assertSame($ids, $res);
    }
}
