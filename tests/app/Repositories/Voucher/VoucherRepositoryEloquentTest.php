<?php

namespace App\Repositories\Voucher;

use App;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Entities\Mamipay\MamipayVoucherPrefix;
use App\Entities\Mamipay\MamipayVoucherPublicCampaign;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Entities\Mamipay\MamipayVoucherUsage;
use App\Test\MamiKosTestCase;
use App\Presenters\UserVoucherPresenter;
use Carbon\Carbon;

class VoucherRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = App::make(VoucherRepositoryEloquent::class);
    }

    public function testGetById_GetTransformedResult()
    {
        $voucherId = 1;
        factory(MamipayVoucher::class)->create(['id' => $voucherId]);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getById($voucherId);

        $this->assertEquals($voucherId, $result['data']['id']);
    }

    public function testGetById_ShouldReturnNullWhenIdNotFound()
    {
        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getById(99);

        $this->assertEquals(null, $result);
    }

    public function testCountActiveUserVoucher_GetCounter()
    {
        $email = 'arif@mamiteam.com';
        $domain = 'mamiteam.com';
        $kostId = 1;
        $kostCity = 'Surabaya';
        $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
        $workplace = 'Universitas Mamikos';
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN, $domain);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_KOST_ID, $kostId);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_CITY, $kostCity);
        $voucherMahasiswa = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_UNIVERSITY,
            $workplace,
            $voucherMahasiswa->id
        );

        $result = $this->class->countActiveUserVoucher(null, $email, $kostId, $kostCity, $occupation, $workplace);

        $this->assertEquals(5, $result);
    }

    public function testCountActiveUserVoucher_UsedVoucher()
    {
        $email = 'arif@mamiteam.com';
        $domain = 'mamiteam.com';
        $kostId = 1;
        $kostCity = 'Surabaya';
        $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
        $workplace = 'Universitas Mamikos';

        $tenant = factory(MamipayTenant::class)->create(['email' => $email]);

        $total = 1;
        $limit = 2;
        $param = [
            'user_limit' => $limit,
            'limit' => $limit,
            'is_active' => 1
        ];
        $voucher = $this->createVoucher($param);

        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email, $voucher->id);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN, $domain, $voucher->id);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_KOST_ID, $kostId, $voucher->id);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_CITY, $kostCity, $voucher->id);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_PROFESSION, $occupation, $voucher->id);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_UNIVERSITY, $workplace, $voucher->id);

        $this->createVoucherUsage(['id' => $voucher->id, 'tenant_id' => $tenant->id]);

        $result = $this->class->countActiveUserVoucher($tenant->id, $email, $kostId, $kostCity, $occupation, $workplace);

        $this->assertEquals(1, $result);
    }

    public function testGetActiveUserVoucherList_GetPaginatedResult()
    {
        $limit = 5;
        $email = 'arif@mamiteam.com';
        $domain = 'mamiteam.com';
        $kostId = 1;
        $kostCity = 'Surabaya';
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN, $domain);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_KOST_ID, $kostId);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_CITY, $kostCity);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList($limit, null, $email, $kostId, $kostCity);

        $this->assertEquals(4, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }
    
    public function testGetActiveUserVoucherList_IncludeRule_OnNullInformationUser()
    {
        $limit = 5;
        $email = 'antoni@mamiteam.com';
        $kostId = null;
        $city = null;
        $occupation = null;
        $workplace = null;

        // 1. Voucher Valid Email
        $this->generateVoucher([
            'prefix' => false,
            'voucher_code' => 'voucher_email',
        ], [
            'type' => MamipayVoucher::IDENTIFIER_EMAIL,
            'identifier' => $email
        ]);

        // 2. Voucher invalid City
        $this->generateVoucher([
            'prefix' => false,
            'voucher_code' => 'voucher_city_invalid',
        ], [
            'type' => MamipayVoucher::IDENTIFIER_CITY,
            'identifier' => 'Surabaya'
        ]);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(
            $limit,
            null,
            $email,
            $kostId,
            $city,
            $occupation,
            $workplace
        );

        $this->assertEquals(1, count($result['data']));
    }

    public function testGetActiveUserVoucherList_ExcludeRuleNotReturned_GetPaginatedResult()
    {
        $limit = 5;
        $excludeEmail = 'arif@mamiteam.com';
        $domain = 'mamiteam.com';
        $excludeKostId = 1;
        $kostCity = 'Surabaya';
        $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
        $excludeWorkplace = 'Universitas Mamikos';
        $this->createActiveVoucherWithExcludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $excludeEmail);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN, $domain);
        $this->createActiveVoucherWithExcludeRule(MamipayVoucher::IDENTIFIER_KOST_ID, $excludeKostId);
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_CITY, $kostCity);
        $voucherMahasiswa = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );
        $this->createActiveVoucherWithExcludeRule(
            MamipayVoucher::IDENTIFIER_UNIVERSITY,
            $excludeWorkplace,
            $voucherMahasiswa->id
        );

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList($limit, null, $excludeEmail, $excludeKostId, $kostCity, $occupation, $excludeWorkplace);

        $this->assertEquals(2, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_UsedVoucherLimitAvailable_GetPaginatedResult()
    {
        $total = 1;
        $limit = 2;
        $email = 'arif@mamiteam.com';
        $tenantId = $this->createUsedVoucher($total, $email, $limit);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList($limit, $tenantId, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_LimitDailyExceed_ShouldNotShown()
    {
        $total = 1;
        $limit = 2;
        $limitDaily = 1;
        $email = 'antoni@mamiteam.com';
        $tenantId = $this->createUsedVoucher($total, $email, $limit, $limitDaily);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList($limit, $tenantId, $email);

        $this->assertEquals(0, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_LimitDailyAvailable_ShouldShown()
    {
        $total = 1;
        $limit = 2;
        $limitDaily = 2;
        $email = 'antoni@mamiteam.com';
        $tenantId = $this->createUsedVoucher($total, $email, $limit, $limitDaily);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList($limit, $tenantId, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_OccupationMahasiswa_GetPaginatedResult()
    {
        $limit = 5;
        $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
        $workplace = 'Universitas Mamikos';
        $voucherMahasiswaA = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_UNIVERSITY,
            $workplace,
            $voucherMahasiswaA->id
        );
        $voucherMahasiswaB = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );

        $this->class->setPresenter(UserVoucherPresenter::class);

        $result = $this->class->getActiveUserVoucherList($limit, null, null, null, null, $occupation, $workplace);
        $this->assertEquals(2, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);

        $result = $this->class->getActiveUserVoucherList($limit, null, null, null, null, $occupation);
        $this->assertEquals(2, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_OccupationKaryawan_GetPaginatedResult()
    {
        $limit = 5;
        $occupation = MamipayVoucher::PROFESSION_KARYAWAN;
        $workplace = 'PT Mamikos';
        $voucherKaryawanA = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_COMPANY,
            $workplace,
            $voucherKaryawanA->id
        );
        $voucherKaryawanB = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_PROFESSION,
            $occupation
        );

        $this->class->setPresenter(UserVoucherPresenter::class);

        $result = $this->class->getActiveUserVoucherList($limit, null, null, null, null, $occupation, $workplace);
        $this->assertEquals(2, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);

        $result = $this->class->getActiveUserVoucherList($limit, null, null, null, null, $occupation);
        $this->assertEquals(2, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetActiveUserVoucherList_ForSingleVoucherWithoutEmail_ShouldSuccess()
    {
        $limit = 10;
        $email = 'antoni@mamiteam.com';
        $city = 'Surabaya';
        $kostId = 123;

        // 1. Voucher Valid Email
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_email',
        ], [
            'type' => MamipayVoucher::IDENTIFIER_EMAIL,
            'identifier' => $email
        ]);

        // 2. Voucher Valid City
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_city_valid',
        ], [
            'type' => MamipayVoucher::IDENTIFIER_CITY,
            'identifier' => $city
        ]);

        // 3. Voucher Invalid City
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_city_invalid',
        ], [
            'type' => MamipayVoucher::IDENTIFIER_CITY,
            'identifier' => 'Tuban'
        ]);

        // 4. Voucher Invalid Email but Valid city
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_city_email_invalid',
        ], [
            [
                'type' => MamipayVoucher::IDENTIFIER_EMAIL,
                'identifier' => 'other@example.com'
            ],
            [
                'type' => MamipayVoucher::IDENTIFIER_CITY,
                'identifier' => $city
            ],
        ]);

        // 5. Voucher Valid Email but Invalid city
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_city_email_valid',
        ], [
            [
                'type' => MamipayVoucher::IDENTIFIER_EMAIL,
                'identifier' => $email
            ],
            [
                'type' => MamipayVoucher::IDENTIFIER_CITY,
                'identifier' => 'Tuban'
            ],
        ]);
        
        // 6. Voucher Invalid Email but Valid KostId
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_kostid_email_invalid',
        ], [
            [
                'type' => MamipayVoucher::IDENTIFIER_EMAIL,
                'identifier' => 'sobirin@gmail.com'
            ],
            [
                'type' => MamipayVoucher::IDENTIFIER_KOST_ID,
                'identifier' => $kostId
            ],
        ]);
        
        // 7. Voucher Valid Email but Invalid KostId
        $this->generateVoucher([
            'prefix' => true,
            'voucher_code' => 'voucher_kostid_email_valid',
        ], [
            [
                'type' => MamipayVoucher::IDENTIFIER_EMAIL,
                'identifier' => $email
            ],
            [
                'type' => MamipayVoucher::IDENTIFIER_KOST_ID,
                'identifier' => $kostId
            ],
        ]);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(
            $limit,
            null, // tenantId
            $email, // email
            $kostId, // kostId
            $city, // kostCity
            null, // occupation
            null // workplace
        );
        $this->assertEquals(4, count($result['data']));
    }

    public function testGetActiveUserVoucherList_TargetNewUser_ShouldSeeVoucher()
    {
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER,
            1
        );

        $tenant = factory(MamipayTenant::class)->create(['email' => 'antoni@mamiteam.com']);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(10, $tenant->id);
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetActiveUserVoucherList_TargetNewUser_ShouldNotSeeVoucher()
    {
        $email = 'antoni@mamiteam.com';
        $tenantId = $this->createUsedVoucher(1, $email, 5);

        $voucherForNewUser = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER,
            1
        );
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email, $voucherForNewUser->id);
        
        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(10, $tenantId, $email);
        
        $this->assertEquals(1, count($result['data']));
        $this->assertNotEquals($result['data'][0]['id'], $voucherForNewUser->id);
    }

    public function testGetActiveUserVoucherList_TargetNewUserVoucher_ShouldSeeVoucher()
    {
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER_VOUCHER,
            69696969
        );

        $tenant = factory(MamipayTenant::class)->create(['email' => 'antoni@mamiteam.com']);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(10, $tenant->id);
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetActiveUserVoucherList_TargetNewUserVoucher_ShouldNotSeeVoucher()
    {
        $email = 'antoni@mamiteam.com';
        $tenantId = $this->createUsedVoucher(1, $email, 5);
        $usedVoucher = MamipayVoucher::first();

        $voucherForNewUserVoucher = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER_VOUCHER,
            $usedVoucher->id
        );
        $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email, $voucherForNewUserVoucher->id);
        
        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(10, $tenantId, $email);
        
        $this->assertEquals(1, count($result['data']));
        $this->assertNotEquals($result['data'][0]['id'], $voucherForNewUserVoucher->id);
    }

    public function testGetActiveUserVoucherList_TargetNewUserOnMultipleVoucher_ShouldNotSeeVoucher()
    {
        $email = 'antoni@mamiteam.com';
        $tenantId = $this->createUsedVoucher(1, $email, 5);
        $usedVoucher = MamipayVoucher::first();

        // Create Voucher that exclude usedVoucher
        $voucherForNewUserVoucher = $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER_VOUCHER,
            999999999
        );
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_NEW_USER_VOUCHER,
            $usedVoucher->id,
            $voucherForNewUserVoucher->id
        );
        $this->createActiveVoucherWithIncludeRule(
            MamipayVoucher::IDENTIFIER_EMAIL,
            $email,
            $voucherForNewUserVoucher->id
        );
        // End Create Voucher
        
        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getActiveUserVoucherList(10, $tenantId, $email);
        
        $this->assertEquals(1, count($result['data']));
        $this->assertNotEquals($result['data'][0]['id'], $voucherForNewUserVoucher->id);
    }

    public function testGetExpiredUserVoucherList_GetPaginatedResult()
    {
        $total = 5;
        $email = 'arif@mamiteam.com';
        $this->createExpiredVoucher($total, $email);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getExpiredUserVoucherList($total, null, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetExpiredUserVoucherList_ByTenant_GetPaginatedResult()
    {
        $total = 5;
        $email = 'arif@mamiteam.com';
        $this->createExpiredVoucher($total, $email);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getExpiredUserVoucherList($total, 1, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetUsedUserVoucherList_GetPaginatedResult()
    {
        $total = 5;
        $email = 'arif@mamiteam.com';
        $tenantId = $this->createUsedVoucher($total, $email);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getUsedUserVoucherList($total, $tenantId, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetUsedUserVoucherList_LimitDailyExceed_ShouldStillShown()
    {
        $total = 2;
        $email = 'antoni@mamiteam.com';
        $limit = 3;
        $limitDaily = 1;
        $tenantId = $this->createUsedVoucher($total, $email, $limit, $limitDaily);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getUsedUserVoucherList($total, $tenantId, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    public function testGetUsedUserVoucherList_UsedVoucherLimitAvailableExpired_GetPaginatedResult()
    {
        $total = 1;
        $limit = 2;
        $endDate = now()->subDay();
        $email = 'arif@mamiteam.com';
        $tenantId = $this->createUsedVoucher($total, $email, $limit, 0, $endDate);

        $this->class->setPresenter(UserVoucherPresenter::class);
        $result = $this->class->getUsedUserVoucherList($limit, $tenantId, $email);

        $this->assertEquals($total, count($result['data']));
        $this->assertIsArray($result['meta']['pagination']);
    }

    private function createActiveVoucherWithIncludeRule(string $type, string $identifier, ?int $id = null)
    {
        $param = [
            'end_date' => null,
            'user_limit' => 0,
            'limit' => 1,
            'is_active' => 1
        ];
        if (!is_null($id)) {
            $param['id'] = $id;
        }
        return $this->createVoucherIncludeRule($param, $type, $identifier);
    }

    private function createActiveVoucherWithExcludeRule(string $type, string $identifier, ?int $id = null)
    {
        $param = [
            'end_date' => null,
            'user_limit' => 0,
            'limit' => 1,
            'is_active' => 1
        ];
        if (!is_null($id)) {
            $param['id'] = $id;
        }
        return $this->createVoucherExcludeRule($param, $type, $identifier);
    }

    private function createExpiredVoucher(int $total, string $email)
    {
        $param = [
            'start_date' => now()->subDay(),
            'end_date' => now(),
            'user_limit' => 0,
            'limit' => 1,
            'is_active' => 1
        ];
        for ($i = 0; $i < $total; $i++) {
            $voucher = $this->createVoucher($param);
            $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email, $voucher->id);
        }
    }

    private function createUsedVoucher(int $total, string $email, ?int $limit = 1, ?int $limitDaily = 0, ?Carbon $endDate = null)
    {
        $param = [
            'end_date' => $endDate,
            'user_limit' => $limit,
            'limit' => $limit,
            'limit_daily' => $limitDaily,
            'is_active' => 1
        ];
        $tenant = factory(MamipayTenant::class)->create(['email' => $email]);
        for ($i = 0; $i < $total; $i++) {
            $voucher = $this->createVoucher($param);
            $this->createActiveVoucherWithIncludeRule(MamipayVoucher::IDENTIFIER_EMAIL, $email, $voucher->id);
            $this->createVoucherUsage(['id' => $voucher->id, 'tenant_id' => $tenant->id]);
        }
        return $tenant->id;
    }

    private function createVoucherIncludeRule(array $param, string $type, string $identifier)
    {
        if (!isset($param['id'])) {
            $voucher = $this->createVoucher($param);
            $voucherId = $voucher->id;
        } else {
            $voucherId = $param['id'];
        }
        factory(MamipayVoucherTargetIdentifier::class)->create(
            [
                'voucher_id' => $voucherId,
                'type' => $type,
                'identifier' => $identifier
            ]
        );
        return $voucher ?? null;
    }

    private function createVoucherExcludeRule(array $param, string $type, string $identifier)
    {
        if (!isset($param['id'])) {
            $voucher = $this->createVoucher($param);
            $voucherId = $voucher->id;
        } else {
            $voucherId = $param['id'];
        }
        factory(MamipayVoucherExcludeIdentifier::class)->create(
            [
                'voucher_id' => $voucherId,
                'type' => $type,
                'identifier' => $identifier
            ]
        );
        return $voucher ?? null;
    }

    private function createVoucherUsage(array $param)
    {
        if (!isset($param['id'])) {
            $voucher = $this->createVoucher($param);
            $voucherId = $voucher->id;
        } else {
            $voucherId = $param['id'];
        }

        $contractParam = [];
        if (isset($param['tenant_id'])) { 
            $contractParam['tenant_id'] = $param['tenant_id'];
        }
        $contract = factory(MamipayContract::class)->create($contractParam);
        $invoice = factory(MamipayInvoice::class)->create(
            [
                'contract_id' => $contract->id,
                'status' => MamipayInvoice::STATUS_PAID
            ]
        );

        return factory(MamipayVoucherUsage::class)->create(
            [
                'voucher_id' => $voucherId,
                'source_id' => $invoice->id
            ]
        );
    }

    private function createVoucher(array $param)
    {
        $publicCampaign = factory(MamipayVoucherPublicCampaign::class)->create([
            'is_published' => 1,
        ]);
        $voucher = factory(MamipayVoucher::class)->create(array_merge($param, [
            'public_campaign_id' => $publicCampaign->id
        ]));

        return $voucher;
    }

    private function generateVoucher(array $voucherData, array $incData = [], array $excData = [])
    {
        $voucherData = array_merge([
            'end_date' => null,
            'user_limit' => 0,
            'limit' => 1,
            'is_active' => 1
        ], $voucherData);

        // Create prefix if needed
        if (isset($voucherData['prefix']) && $voucherData['prefix']) {
            $prefix = factory(MamipayVoucherPrefix::class)->create();
            $voucherData['prefix_id'] = $prefix->id;
        }
        unset($voucherData['prefix']);
        
        // Create Public Campaign
        $publicCampaign = factory(MamipayVoucherPublicCampaign::class)->create([
            'is_published' => 1,
        ]);
        $voucherData['public_campaign_id'] = $publicCampaign->id;
        
        // Create Voucher
        $voucher = factory(MamipayVoucher::class)->create($voucherData);

        // Create Include Identifiers
        if (!empty($incData)) {
            $this->generateVoucherIdentifier('include', $voucher->id, $incData);
            $voucher->load('include_identifiers');
        }

        // Create Exclude Identifiers
        if (!empty($excData)) {
            $this->generateVoucherIdentifier('exclude', $voucher->id, $excData);
            $voucher->load('exclude_identifiers');
        }

        return $voucher;
    }

    private function generateVoucherIdentifier($type, $voucherId, $data = [])
    {
        if ($type == 'include') {
            $type = MamipayVoucherTargetIdentifier::class;
        } elseif ($type == 'exclude') {
            $type = MamipayVoucherExcludeIdentifier::class;
        } else {
            throw new \Exception("Error - Parameter Type should be: `include` or `exclude`", 1);
        }

        $result = [];

        // if multiple data
        if (!empty($data[0])) {
            foreach ($data as $row) {
                array_push(
                    $result,
                    factory($type)->create(array_merge($row, [
                        'voucher_id' => $voucherId
                    ]))
                );
            }
        } else {
            $result = factory($type)->create(array_merge($data, [
                'voucher_id' => $voucherId
            ]));
        }

        return $result;
    }
}
