<?php

namespace App\Repositories;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectSlug;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Faker\Factory;
use Illuminate\Support\Collection;

class ApartmentProjectRepositoryEloquentTest extends MamiKosTestCase
{
    const APARTMENT_NAME = 'Apartment Mamitest 123';
    const APARTMENT_SLUG = 'apartment-mamitest-123-jakarta-selatan-jakarta';
    const APARTMENT_CODE = "MAMI123";
    const APARTMENT_AREA_CITY = 'jakarta selatan';

    protected $repository;
    protected $faker;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = new ApartmentProjectRepositoryEloquent(app(), Collection::make());

        // Create Faker instance
        $this->faker = Factory::create();
    }

    public function testModel()
    {
        $this->assertEquals(ApartmentProject::class, $this->repository->model());
    }

    public function testGetSuggestions()
    {
        $name = self::APARTMENT_NAME;
        $this->setupSingleApartmentProjectData(true, $name);

        $otherDatas = $this->faker->numberBetween(1, 5);
        $this->setupApartmentProjectData(true, $otherDatas);

        $response = $this->repository->getSuggestions('Mamitest');

        $this->assertEquals(1, $response->count());
        $this->assertSame($name, $response->first()->name);
    }

    public function testGetByCityAndSlug()
    {
        $this->setupSingleApartmentProjectData(
            true,
            self::APARTMENT_NAME,
            self::APARTMENT_SLUG,
            self::APARTMENT_AREA_CITY
        );

        $response = $this->repository->getByCityAndSlug(self::APARTMENT_AREA_CITY, self::APARTMENT_SLUG);

        $this->assertNotNull($response);
        $this->assertSame(self::APARTMENT_NAME, $response->name);
    }

    public function testGetFromSlugHistory()
    {
        $this->setupSingleApartmentProjectData(true, self::APARTMENT_NAME);

        // Create apartment slug history
        factory(ApartmentProjectSlug::class)
            ->create(
                [
                    'apartment_project_id' => ApartmentProject::first()->id,
                    'slug' => self::APARTMENT_SLUG,
                    'area_city' => self::APARTMENT_AREA_CITY
                ]
            );

        $response = $this->repository->getFromSlugHistory('JAKARTA SELATAN', self::APARTMENT_SLUG);

        $this->assertNotNull($response);
        $this->assertSame(self::APARTMENT_NAME, $response->name);
    }

    public function testGetFromSlugHistoryReturnNull()
    {
        $slug = self::APARTMENT_SLUG;

        $response = $this->repository->getFromSlugHistory('JAKARTA SELATAN', $slug);

        $this->assertNull($response);
    }

    public function testGetByIdAndCode()
    {
        $this->setupSingleApartmentProjectData(true, self::APARTMENT_NAME, null, null, self::APARTMENT_CODE);

        $response = $this->repository->getByIdAndCode(ApartmentProject::first()->id, self::APARTMENT_CODE);

        $this->assertNotNull($response);
        $this->assertSame(self::APARTMENT_NAME, $response->name);
    }

    public function testActivate()
    {
        $this->setupSingleApartmentProjectData(false, self::APARTMENT_NAME);

        $inactiveApartment = ApartmentProject::first();
        $this->assertSame(0, $inactiveApartment->is_active);

        $this->repository->activate($inactiveApartment);

        $activatedApartment = ApartmentProject::first();
        $this->assertSame(1, $activatedApartment->is_active);
    }

    public function testDeactivate()
    {
        $this->setupSingleApartmentProjectData(true, self::APARTMENT_NAME);

        $activeApartment = ApartmentProject::first();
        $this->assertSame(1, $activeApartment->is_active);

        $this->repository->activate($activeApartment);

        $deactivatedApartment = ApartmentProject::first();
        $this->assertSame(1, $deactivatedApartment->is_active);
    }

    public function testGetUnitPriceRange()
    {
        $this->setupSingleApartmentProjectData(true, self::APARTMENT_NAME);

        $apartment = ApartmentProject::first();
        factory(Room::class)
            ->states(
                [
                    'with-all-prices',
                    'active'
                ]
            )
            ->create(
                [
                    'apartment_project_id' => $apartment->id
                ]
            );

        $response = $this->repository->getUnitPriceRange($apartment)->toArray();

        $this->assertNotEmpty($response);
        $this->assertArrayHasKey('min_price', $response);
        $this->assertTrue($response['min_price'] > 0);
        $this->assertArrayHasKey('max_price', $response);
        $this->assertTrue($response['max_price'] > 0);
    }

    private function setupSingleApartmentProjectData(
        bool $isActive,
        string $name,
        ?string $slug = '',
        ?string $areaCity = '',
        string $code = ''
    ): void {
        $factory = factory(ApartmentProject::class);

        if ($isActive) {
            $factory->state('active');
        } else {
            $factory->state('inactive');
        }

        $factory->create(
            [
                'name' => $name,
                'slug' => $slug,
                'area_city' => $areaCity,
                'project_code' => $code
            ]
        );
    }

    private function setupApartmentProjectData(bool $isActive, int $amount): void
    {
        if ($isActive) {
            factory(ApartmentProject::class, $amount)
                ->state('active')
                ->create();
        } else {
            factory(ApartmentProject::class, $amount)
                ->state('inactive')
                ->create();
        }
    }
}
