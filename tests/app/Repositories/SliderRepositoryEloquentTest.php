<?php

namespace App\Repositories;

use App\Entities\Media\Media;
use App\Entities\Slider\SliderPage;
use App\Test\MamiKosTestCase;
use App\Entities\Slider\Slider;

use App\User;
use Mockery;

use function factory;

class SliderRepositoryEloquentTest extends MamiKosTestCase
{

    const SLIDER_NAME = 'Test Slide';
    const SLIDER_ENDPOINT = 'testing';

    private $repository;

    public function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(SliderRepositoryEloquent::class);
    }

    public function testGetSliderListGiveResponse()
    {
        $slider = factory(Slider::class)->create(["is_active" => 1]);

        $response = $this->repository->getSliderList();

        $this->assertEquals($slider->name, $response[0]["name"]);
        $this->assertEquals('/slider/' . $slider->endpoint, $response[0]["endpoint"]);
    }

    public function testGetSliderListGiveEmptyArrayIfThereIsNoActiveSlider()
    {
        factory(Slider::class)->create(["is_active" => 0]);

        $response = $this->repository->getSliderList();

        $this->assertEquals([], $response);
    }

    public function testGetSliderListGiveEmptyArrayIfThereisNoDataInDB()
    {
        $response = $this->repository->getSliderList();

        $this->assertEquals([], $response);
    }

    public function testGetSlider()
    {
        $this->setupSliderData(true);

        $response = $this->repository->getSlider('testing');

        $this->assertNotEmpty($response);
        $this->assertEquals(self::SLIDER_NAME, $response["name"]);
        $this->assertEquals('/slider/' . self::SLIDER_ENDPOINT, $response["endpoint"]);

        $this->assertNotEmpty($response['pages']);
    }

    public function testGetSliderWithNonActiveData()
    {
        $this->setupSliderData(false);

        $response = $this->repository->getSlider('testing');

        $this->assertEmpty($response);
    }

    public function testGetSliderWithoutPages()
    {
        $this->setupSliderData(true, false);

        $response = $this->repository->getSlider('testing');

        $this->assertNotEmpty($response);
        $this->assertEquals(self::SLIDER_NAME, $response["name"]);
        $this->assertEquals('/slider/' . self::SLIDER_ENDPOINT, $response["endpoint"]);

        $this->assertEmpty($response['pages']);
    }

    public function testCompileRedirectObjectReturnResponse()
    {
        $sliderPage = factory(SliderPage::class)->create();

        $response = $this->repository->compileRedirectObject($sliderPage);

        $this->assertEquals($sliderPage->redirect_label, $response["label"]);
        $this->assertEquals($sliderPage->redirect_link, $response["url"]);
    }

    public function testCompileRedirectObjectReturnNull()
    {
        $sliderPage = factory(SliderPage::class)->create(
            [
                'redirect_label' => null,
                'redirect_link' => null
            ]
        );

        $response = $this->repository->compileRedirectObject($sliderPage);

        $this->assertNull($response);
    }

    private function setupSliderData(bool $isActive, bool $isPageEnabled = true): void
    {
        $user = factory(User::class)
            ->state('admin')
            ->create();

        $slider = factory(Slider::class)->create(
            [
                "name" => self::SLIDER_NAME,
                "is_active" => $isActive ? 1 : 0,
                "endpoint" => self::SLIDER_ENDPOINT,
                'user_id' => $user->id
            ]
        );

        if ($isPageEnabled) {
            $media = factory(Media::class)->create();

            factory(SliderPage::class)->create(
                [
                    "slider_id" => $slider->id,
                    "user_id" => $user->id,
                    "media_id" => $media->id,
                    "is_active" => 1
                ]
            );
        }
    }
}
