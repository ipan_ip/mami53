<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\Document\ConsultantDocument;
use App\Test\MamiKosTestCase;

class ConsultantDocumentRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(ConsultantDocumentRepository::class);
    }

    public function testModel(): void
    {
        $this->assertEquals(
            ConsultantDocument::class,
            $this->repository->model()
        );
    }
}
