<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class SalesMotionProgressRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = $this->app->make(SalesMotionProgressRepository::class);
    }

    public function testModel(): void
    {
        $this->assertEquals($this->repo->model(), SalesMotionProgress::class);
    }

    public function testGetDataAssociatedWithUnknownTypeShouldReturnNull()
    {
        $input = ['type' => 'test', 'search' => 'test'];

        $result = $this->repo->getDataAssociated($input);

        $this->assertNull($result);
    }

    public function testGetDataAssociatedWithTypePropertyShouldReturnRoomData()
    {
        $room = factory(Room::class)->create(['name' => 'kost testing']);
        $input = ['type' => 'property', 'search' => 'test'];

        $result = $this->repo->getDataAssociated($input);

        $this->assertEquals(1, count($result));
        $this->assertInstanceOf(Room::class, $result[0]);
        $this->assertEquals($room->id, $result[0]->id);
    }

    public function testGetDataAssociatedWithTypeDbetShouldReturnPotentialTenantData()
    {
        $tenant = factory(PotentialTenant::class)->create(['name' => 'tenant test']);
        $input = ['type' => 'dbet', 'search' => 'test'];

        $result = $this->repo->getDataAssociated($input);

        $this->assertEquals(1, count($result));
        $this->assertInstanceOf(PotentialTenant::class, $result[0]);
        $this->assertEquals($tenant->id, $result[0]->id);
    }

    public function testGetDataAssociatedWithTypeContractShouldReturnActiveTenantData()
    {
        $tenant = factory(MamipayTenant::class)->create(['name' => 'tenant test']);
        factory(MamipayContract::class)->create(['tenant_id' => $tenant->id, 'status' => 'active']);
        $input = ['type' => 'contract', 'search' => 'test'];

        $result = $this->repo->getDataAssociated($input);

        $this->assertEquals(1, count($result));
        $this->assertInstanceOf(MamipayTenant::class, $result[0]);
        $this->assertEquals($tenant->id, $result[0]->id);
    }
}
