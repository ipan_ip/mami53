<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Test\MamiKosTestCase;

class SalesMotionAssigneeRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = $this->app->make(SalesMotionAssigneeRepository::class);
    }

    public function testModel(): void
    {
        $this->assertEquals($this->repo->model(), SalesMotionAssignee::class);
    }
}
