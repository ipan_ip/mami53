<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Test\MamiKosTestCase;
use App\User;

class SalesMotionRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;
    protected $consultant;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = $this->app->make(SalesMotionRepository::class);

        $this->consultant = factory(User::class)->create();
    }

    public function testModel(): void
    {
        $this->assertEquals($this->repo->model(), SalesMotion::class);
    }

    public function testActivateSalesMotion()
    {
        $salesMotion = factory(SalesMotion::class)->create();

        $input['start_date'] = '2020-07-21';
        $input['end_date'] = '2020-07-30';

        $this->actingAs($this->consultant);

        $this->repo->activateSalesMotion($salesMotion, $input);

        $updatedData = SalesMotion::find($salesMotion->id);

        $this->assertEquals(true, $updatedData->is_active);
        $this->assertEquals($input['start_date'], $updatedData->start_date);
        $this->assertEquals($input['end_date'], $updatedData->end_date);
        $this->assertEquals($this->consultant->id, $updatedData->updated_by);
    }

    public function testDeactivateSalesMotion()
    {
        $salesMotion = factory(SalesMotion::class)->create(['is_active' => true]);

        $this->actingAs($this->consultant);

        $this->repo->deactivateSalesMotion($salesMotion);

        $updatedData = SalesMotion::find($salesMotion->id);

        $this->assertEquals(false, $updatedData->is_active);
        $this->assertEquals($this->consultant->id, $updatedData->updated_by);
    }

    public function testGetConsultantListWithConsultantShouldReturnOnlyHisCountedProgress()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList($consultant, $input);

        $this->assertEquals(1, $actual['total']);
        $this->assertEquals(1, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(0, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithoutConsultantShouldReturnAllCountedProgress()
    {
        $salesMotion = factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(1, $actual['total']);
        $this->assertEquals(2, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithoutConsultantShouldReturnAllSalesMotion()
    {
        $this->markTestSkipped();
        $salesMotion = factory(SalesMotion::class)->create();
        factory(SalesMotion::class)->create();
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(2, $actual['total']);
        $this->assertEquals(2, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithIsActiveParamShouldReturnCorrectData()
    {
        $salesMotion = factory(SalesMotion::class)->create(['is_active' => false]);
        factory(SalesMotion::class)->create(['is_active' => true]);
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'is_active' => 1,
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(1, $actual['total']);
        $this->assertEquals(0, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(0, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(0, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithTypeParamShouldReturnCorrectData()
    {
        $salesMotion = factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PROMOTION]
        );
        factory(SalesMotion::class)->create(['is_active' => true, 'type' => SalesMotion::TYPE_PRODUCT]);
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'is_active' => 1,
            'type' => [SalesMotion::TYPE_PROMOTION, SalesMotion::TYPE_FEATURE],
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(1, $actual['total']);
        $this->assertEquals(2, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithTypeParamCountIs4ShouldReturnCorrectData()
    {
        $salesMotion = factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PROMOTION]
        );
        factory(SalesMotion::class)->create(['is_active' => true, 'type' => SalesMotion::TYPE_PRODUCT]);
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'sort_by' => 'updated_at',
            'is_active' => 1,
            'type' => [SalesMotion::TYPE_PROMOTION, SalesMotion::TYPE_FEATURE, SalesMotion::TYPE_PRODUCT, SalesMotion::TYPE_CAMPAIGN],
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(2, $actual['total']);
        $this->assertEquals(2, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_not_interested_count);
    }

    public function testGetConsultantListWithSearchParamShouldReturnCorrectData()
    {
        $salesMotion = factory(SalesMotion::class)->create(
            ['is_active' => true, 'type' => SalesMotion::TYPE_PROMOTION,'name'=>'test sales motion']
        );
        factory(SalesMotion::class)->create(['is_active' => true, 'type' => SalesMotion::TYPE_PRODUCT, 'name'=>'coba coba aja']);
        $consultant = factory(Consultant::class)->create(['id' => 10]);

        $salesMotion->consultants()->sync([10]);
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_INTERESTED,
                'consultant_id' => 10,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_DEAL,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );
        factory(SalesMotionProgress::class)->create(
            [
                'status' => SalesMotionProgress::STATUS_NOT_INTERESTED,
                'consultant_id' => 20,
                'consultant_sales_motion_id' => $salesMotion->id
            ]
        );

        $input = [
            'limit' => 10,
            'is_active' => 1,
            'search'=>'sales_motion',
            'offset'=>0
        ];

        $actual = $this->repo->getConsultantList(null, $input);

        $this->assertEquals(1, $actual['total']);
        $this->assertEquals(2, $actual['list'][0]->progress_status_deal_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_interested_count);
        $this->assertEquals(1, $actual['list'][0]->progress_status_not_interested_count);
    }
}
