<?php

namespace App\Repositories\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class ActivityFormRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $user;
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->user->consultant()->save(
            factory(Consultant::class)->make()
        );
        $this->repository = $this->app->make(ActivityFormRepositoryEloquent::class);
    }

    public function testStoreStagingData(): void
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'total_stage' => 0]);
        $input = [
            'name' => 'Test',
            'detail' => 'Coba Bikin Staging',
            'stage' => 1
        ];

        $this->actingAs($this->user);
        $this->repository->storeStagingData(1, $input);

        $form = ActivityForm::where('name', $input['name']);
        $this->assertEquals(1, $form->count());

        $form = $form->first();
        $this->assertEquals($input['name'], $form->name);
        $this->assertEquals($input['detail'], $form->detail);
        $this->assertEquals($input['stage'], $form->stage);
        $this->assertEquals($this->user->id, $form->created_by);

        $funnel = ActivityFunnel::find(1);
        $this->assertEquals(1, $funnel->total_stage);
    }

    public function testUpdateStagingDataFailed(): void
    {
        $input = [
            'name' => 'Test',
            'detail' => 'Coba Bikin Staging',
            'stage' => 1
        ];

        $this->actingAs($this->user);
        $response = $this->repository->updateStagingData($input, 100);

        $this->assertNull($response);
    }

    public function testUpdateStagingDataSuccess(): void
    {
        factory(ActivityForm::class)->create(['id' => 1, 'form_element' => '']);
        $input = [
            'name' => 'Test',
            'detail' => 'Coba Bikin Staging',
            'stage' => 1
        ];

        $this->actingAs($this->user);
        $response = $this->repository->updateStagingData($input, 1);

        $form = ActivityForm::find(1);
        $this->assertEquals($input['name'], $form->name);
        $this->assertEquals($input['detail'], $form->detail);
        $this->assertEquals($input['stage'], $form->stage);
        $this->assertEquals($this->user->id, $form->updated_by);
    }

    public function testDeleteStagingFailed(): void
    {
        $this->actingAs($this->user);
        $response = $this->repository->deleteStaging(1);

        $this->assertFalse($response);
    }

    public function testDeleteStagingSuccess(): void
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'total_stage' => 1]);
        factory(ActivityForm::class)->create(['id' => 1, 'form_element' => '', 'consultant_activity_funnel_id' => 1]);

        $this->actingAs($this->user);
        $response = $this->repository->deleteStaging(1);

        $this->assertTrue($response);

        $funnel = ActivityFunnel::find(1);
        $this->assertEquals(0, $funnel->total_stage);
        $this->assertEquals($this->user->id, $funnel->updated_by);

        $form = ActivityForm::find(1);
        $this->assertNull($form);

        $form = ActivityForm::where('id', 1)->withTrashed()->first();
        $this->assertEquals($this->user->id, $form->deleted_by);
    }

    public function testDeleteFormInputFailed(): void
    {
        $this->actingAs($this->user);
        $response = $this->repository->deleteFormInput(1, 1);

        $this->assertFalse($response);
    }

    public function testDeleteFormInputSuccessRemoveFormInput(): void
    {
        $formElements = [
            [
                "id" => 1,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 1
            ],
            [
                "id" => 5,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 2
            ]
        ];

        factory(ActivityForm::class)->create(
            [
                'id' => 1,
                'form_element' => $formElements,
                'consultant_activity_funnel_id' => 1
            ]
        );

        $this->actingAs($this->user);
        $response = $this->repository->deleteFormInput(1, 5);

        $this->assertTrue($response);

        $form = ActivityForm::find(1);
        $this->assertEquals($this->user->id, $form->updated_by);
        $this->assertEquals(1, count($form->form_element));

        $expectedElements = [
            [
                "id" => 1,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 1
            ]
        ];
        $this->assertEquals($expectedElements, $form->form_element);
    }

    public function testDeleteFormInputIdNotFoundShouldNotUpdate()
    {
        $formElements = [
            [
                "id" => 1,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 1
            ],
            [
                "id" => 5,
                "type" => "text",
                "label" => "test",
                "placeholder" => "test",
                "values" => [],
                "order" => 2
            ]
        ];

        factory(ActivityForm::class)->create(
            [
                'id' => 1,
                'form_element' => $formElements,
                'consultant_activity_funnel_id' => 1,
                'updated_by' => $this->user->id + 1
            ]
        );

        $this->actingAs($this->user);
        $response = $this->repository->deleteFormInput(1, 10);

        $this->assertTrue($response);

        $form = ActivityForm::find(1);
        $this->assertEquals($this->user->id + 1, $form->updated_by);
        $this->assertEquals(2, count($form->form_element));

        $this->assertEquals($formElements, $form->form_element);
    }

    public function testGetFunnelStages(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);

        $response = $this->repository->getFunnelStages($funnel->id);

        $this->assertCount(2, $response);
        $this->assertSame($form->id, $response[1]->id);
        $this->assertSame('Tugas Aktif', $response[0]->getName());
    }

    public function testGetTaskStages(): void
    {
        $funnel = factory(ActivityFunnel::class)->create();
        $form = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $funnel->id
        ]);

        $response = $this->repository->getTaskStages($funnel->id);

        $this->assertCount(3, $response);
        $this->assertSame('Daftar Tugas', $response[0]->getName());
        $this->assertSame('Tugas Aktif', $response[1]->getName());
        $this->assertSame($form->id, $response[2]->id);
    }

    public function testGetCurrentStageWithoutTaskSouldReturnBacklogStage(): void
    {
        $response = $this->repository->getCurrentStage(0);

        $this->assertInstanceOf(BacklogStage::class, $response);
    }

    public function testGetCurrentStageWithToDoStageSouldReturnToDoStage(): void
    {
        $task = factory(ActivityProgress::class)->create(['current_stage' => ToDoStage::STAGE_NUMBER]);

        $response = $this->repository->getCurrentStage($task->id);

        $this->assertInstanceOf(ToDoStage::class, $response);
    }

    public function testGetCurrentStageWithOtherStageSouldReturnActivityForm(): void
    {
        $task = factory(ActivityProgress::class)->create(['current_stage' => 1]);
        $stage = factory(ActivityForm::class)->create([
            'consultant_activity_funnel_id' => $task->consultant_activity_funnel_id,
            'stage' => $task->current_stage
        ]);

        $currentStage = $this->repository->getCurrentStage($task->id);

        $this->assertEquals($stage->id, $currentStage->id);
    }
}
