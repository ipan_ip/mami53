<?php

namespace App\Repositories\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class ActivityFunnelRepositoryEloquentTest extends MamiKosTestCase
{

    protected $user;
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->repository = $this->app->make(ActivityFunnelRepositoryEloquent::class);
    }

    public function testStore()
    {
        $input = [
            'name' => 'Funnel',
            'role' => 'admin',
            'relatedTable' => ActivityFunnel::TABLE_DBET
        ];

        $this->actingAs($this->user);
        $this->repository->store($input);

        $funnel = ActivityFunnel::where('name', 'Funnel');
        $this->assertEquals(1, $funnel->count());

        $funnel = $funnel->first();
        $this->assertEquals($input['name'], $funnel->name);
        $this->assertEquals($input['role'], $funnel->consultant_role);
        $this->assertEquals($input['relatedTable'], $funnel->related_table);
        $this->assertEquals(0, $funnel->total_stage);
        $this->assertEquals($this->user->id, $funnel->created_by);
    }

    public function testUpdateFailed()
    {
        $input = [
            'name' => 'Funnel',
            'role' => 'admin',
            'relatedTable' => ActivityFunnel::TABLE_DBET
        ];

        $this->actingAs($this->user);
        $response = $this->repository->update($input, 100);

        $this->assertNull($response);
    }

    public function testUpdateSuccess()
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);

        $input = [
            'name' => 'Funnel',
            'role' => 'admin',
            'relatedTable' => ActivityFunnel::TABLE_DBET
        ];

        $this->actingAs($this->user);
        $response = $this->repository->update($input, 1);

        $this->assertEquals($input['name'], $response->name);
        $this->assertEquals($input['relatedTable'], $response->related_table);
        $this->assertEquals($input['role'], $response->consultant_role);
        $this->assertEquals($this->user->id, $response->updated_by);
    }

    public function testDestroyFailed()
    {
        $this->actingAs($this->user);
        $response = $this->repository->destroy(1);

        $this->assertFalse($response);
    }

    public function testDestroySuccess()
    {
        factory(ActivityFunnel::class)->create(['id' => 1]);
        factory(ActivityForm::class)->create(['id' => 10, 'consultant_activity_funnel_id' => 1]);

        $this->actingAs($this->user);
        $response = $this->repository->destroy(1);

        $this->assertTrue($response);

        $funnel = ActivityFunnel::find(1);
        $this->assertNull($funnel);

        $funnel = ActivityFunnel::where('id', 1)->withTrashed()->first();
        $this->assertEquals($this->user->id, $funnel->deleted_by);

        $forms = $funnel->forms()->withTrashed()->get();
        $this->assertEquals($this->user->id, $forms[0]->deleted_by);
    }

    public function testGetRelatedTableDataPotentialTenantRoomHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class)->create(['id' => 1, 'designer_id' => 1]);
        $room = factory(Room::class)->create(['id' => 1]);
        factory(RoomOwner::class)->create(['designer_id' => 1, 'user_id' => $this->user->id]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals($tenant->name, $response['name']);
        $this->assertEquals($tenant->phone_number, $response['phone_number']);
        $this->assertEquals($tenant->gender, $response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertEquals($this->user->name, $response['property_owner_name']);
        $this->assertEquals($this->user->phone_number, $response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataPotentialTenantRoomDontHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET]);
        $tenant = factory(PotentialTenant::class)->create(['id' => 1, 'designer_id' => 1]);
        $room = factory(Room::class)->create(['id' => 1]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals($tenant->name, $response['name']);
        $this->assertEquals($tenant->phone_number, $response['phone_number']);
        $this->assertEquals($tenant->gender, $response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertNull($response['property_owner_name']);
        $this->assertNull($response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataContractRoomHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_CONTRACT]);
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => 1]);
        factory(MamipayContractKost::class)->create(['contract_id' => 1, 'designer_id' => 1]);
        $tenant = factory(MamipayTenant::class)->create(['id' => 1]);
        $room = factory(Room::class)->create(['id' => 1]);
        factory(RoomOwner::class)->create(['designer_id' => 1, 'user_id' => $this->user->id]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals($tenant->name, $response['name']);
        $this->assertEquals($tenant->phone_number, $response['phone_number']);
        $this->assertEquals($tenant->gender, $response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertEquals($this->user->name, $response['property_owner_name']);
        $this->assertEquals($this->user->phone_number, $response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataContractRoomDontHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_CONTRACT]);
        factory(MamipayContract::class)->create(['id' => 1, 'tenant_id' => 1]);
        factory(MamipayContractKost::class)->create(['contract_id' => 1, 'designer_id' => 1]);
        $tenant = factory(MamipayTenant::class)->create(['id' => 1]);
        $room = factory(Room::class)->create(['id' => 1]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals($tenant->name, $response['name']);
        $this->assertEquals($tenant->phone_number, $response['phone_number']);
        $this->assertEquals($tenant->gender, $response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertNull($response['property_owner_name']);
        $this->assertNull($response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataDesignerHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $room = factory(Room::class)->create(['id' => 1]);
        factory(RoomOwner::class)->create(['designer_id' => 1, 'user_id' => $this->user->id]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertNull($response['name']);
        $this->assertNull($response['phone_number']);
        $this->assertNull($response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertEquals($this->user->name, $response['property_owner_name']);
        $this->assertEquals($this->user->phone_number, $response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataDesignerDontHaveOwner()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $room = factory(Room::class)->create(['id' => 1]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertNull($response['name']);
        $this->assertNull($response['phone_number']);
        $this->assertNull($response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertNull($response['property_owner_name']);
        $this->assertNull($response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataActivityFunnelNotFound()
    {
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
    }

    public function testGetRelatedTableDataPotentialTenantNotFound()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_DBET]);
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
    }

    public function testGetRelatedTableDataContractNotFound()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_CONTRACT]);
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
    }

    public function testGetRelatedTableDataDesignerNotFound()
    {
        factory(ActivityFunnel::class)->create(['id' => 1, 'related_table' => ActivityFunnel::TABLE_PROPERTY]);
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
    }

    public function testGetRelatedTableDataPotentialPropertyFound()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]
        );

        factory(PotentialOwner::class)->create(['id' => 10]);
        $room = factory(PotentialProperty::class)->create(['id' => 1, 'consultant_potential_owner_id' => 10]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertNull($response['name']);
        $this->assertNull($response['phone_number']);
        $this->assertNull($response['gender']);
        $this->assertEquals($room->name, $response['property_name']);
        $this->assertEquals($room->potential_owner->name, $response['property_owner_name']);
        $this->assertEquals($room->potential_owner->phone_number, $response['property_owner_phone_number']);
    }

    public function testGetRelatedTableDataPotentialPropertyNotFound()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]
        );
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
    }

    public function testGetRelatedTableDataPotentialOwnerNotFoundShouldReturnEmptyArray()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER]
        );
        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertEquals(0, count($response));
        $this->assertEquals([], $response);
    }

    public function testGetRelatedTableDataPotentialOwnerFound()
    {
        factory(ActivityFunnel::class)->create(
            ['id' => 1, 'related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER]
        );

        $owner = factory(PotentialOwner::class)->create(['id' => 1]);

        $response = $this->repository->getRelatedTableData(1, 1);

        $this->assertNull($response['name']);
        $this->assertNull($response['phone_number']);
        $this->assertNull($response['gender']);
        $this->assertNull($response['property_name']);
        $this->assertEquals($owner->name, $response['property_owner_name']);
        $this->assertEquals($owner->phone_number, $response['property_owner_phone_number']);
        $this->assertEquals($owner->email, $response['property_owner_email']);
    }
}
