<?php

namespace App\Repositories\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityProgressDetail;
use App\Entities\Consultant\ActivityManagement\InputType;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ActivityProgressRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $repository;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->repository = $this->app->make(ActivityProgressRepositoryEloquent::class);
    }

    public function testGetCurrentStageValueIdNotFoundShouldReturnEmptyArray()
    {
        $value = $this->repository->getCurrentStageValue(10);

        $this->assertEquals([], $value);
    }

    public function testGetCurrentStageValueCurrentStage0ShouldReturnEmptyArray()
    {
        factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 0, 'consultant_activity_funnel_id' => 2]
        );

        $value = $this->repository->getCurrentStageValue(10);

        $this->assertEquals([], $value);
    }

    public function testGetCurrentStageValueNotHaveDetailShouldReturnEmptyArray()
    {
        factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 2, 'consultant_activity_funnel_id' => 2]
        );
        factory(ActivityForm::class)->create(
            ['id' => 2, 'consultant_activity_funnel_id' => 2, 'stage' => 2, 'form_element' => []]
        );

        $value = $this->repository->getCurrentStageValue(10);

        $this->assertEquals([], $value);
    }

    public function testGetCurrentStageValueShouldReturnCorrectValue()
    {
        factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 2, 'consultant_activity_funnel_id' => 2]
        );
        factory(ActivityForm::class)->create(
            [
                'id' => 2,
                'consultant_activity_funnel_id' => 2,
                'stage' => 2,
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        $detail = factory(ActivityProgressDetail::class)->create(
            [
                'consultant_activity_progress_id' => 10,
                'consultant_activity_form_id' => 2,
                'value' => [['id' => 1, 'value' => ['test value']]]
            ]
        );

        $value = $this->repository->getCurrentStageValue(10);

        $this->assertEquals(
            [['id' => 1, 'label' => 'test', 'type' => InputType::TEXT, 'value' => 'test value']],
            $value
        );
    }

    public function testGetCurrentStageValueForInputFileShouldReturnMediaUrl()
    {
        factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 2, 'consultant_activity_funnel_id' => 2]
        );
        factory(ActivityForm::class)->create(
            [
                'id' => 2,
                'consultant_activity_funnel_id' => 2,
                'stage' => 2,
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => InputType::UPLOAD_BUTTON,
                        'label' => 'test',
                        'placeholder' => 'test',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        factory(Media::class)->create(['id' => 10]);
        $detail = factory(ActivityProgressDetail::class)->create(
            [
                'consultant_activity_progress_id' => 10,
                'consultant_activity_form_id' => 2,
                'value' => [['id' => 1, 'value' => 10]]
            ]
        );

        $value = $this->repository->getCurrentStageValue(10);
        $media = Media::find(10);

        $this->assertEquals(
            [
                [
                    'id' => 1,
                    'label' => 'test',
                    'type' => InputType::UPLOAD_BUTTON,
                    'value' => $media->getMediaUrl(),
                    'media_id' => 10
                ]
            ],
            $value
        );
    }

    public function testGetHistoryLogProgressNotFoundShouldReturnEmptyArray()
    {
        $log = $this->repository->getHistoryLog(10);

        $this->assertEquals([], $log);
    }

    public function testMoveToBacklog(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $status = $this->repository->moveToBacklog($task->id);

        $this->assertSoftDeleted(
            'consultant_activity_progress',
            [
                'id' => $task->id
            ]
        );
    }

    public function testMoveToBacklogWithInvalidId(): void
    {
        $task = factory(ActivityProgress::class)->create();

        $this->expectException(ModelNotFoundException::class);
        $status = $this->repository->moveToBacklog(0);

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'deleted_at' => null
            ]
        );

        $this->assertFalse($status);
    }

    public function testGetHistoryForProgressThatDontHaveDetailsShouldReturnJustCreatedDate()
    {
        $progress = factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 0, 'consultant_activity_funnel_id' => 2]
        );

        $log = $this->repository->getHistoryLog(10);

        $this->assertEquals([['status_name' => 'Created to "Tugas Aktif"', 'date' => $progress->created_at]], $log);
    }

    public function testGetHistoryForMultipleDetailsShouldReturnCorrectOrder()
    {
        $progress = factory(ActivityProgress::class)->create(
            ['id' => 10, 'current_stage' => 2, 'consultant_activity_funnel_id' => 2]
        );
        factory(ActivityForm::class)->create(
            [
                'id' => 2,
                'consultant_activity_funnel_id' => 2,
                'stage' => 1,
                'name' => 'Test Stage',
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test2',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        factory(ActivityForm::class)->create(
            [
                'id' => 3,
                'consultant_activity_funnel_id' => 2,
                'stage' => 2,
                'name' => 'Test Stage 2',
                'form_element' => [
                    [
                        'id' => 1,
                        'type' => InputType::TEXT,
                        'label' => 'test',
                        'placeholder' => 'test2',
                        'values' => [],
                        'order' => 1
                    ]
                ]
            ]
        );
        $detail1 = factory(ActivityProgressDetail::class)->create(
            [
                'consultant_activity_progress_id' => 10,
                'consultant_activity_form_id' => 2,
                'value' => [1 => 'test value']
            ]
        );
        $detail2 = factory(ActivityProgressDetail::class)->create(
            [
                'consultant_activity_progress_id' => 10,
                'consultant_activity_form_id' => 3,
                'value' => [1 => 'test value new']
            ]
        );

        $log = $this->repository->getHistoryLog(10);

        $expectedValue = [
            ['status_name' => 'Moved to "Test Stage 2"', 'date' => $detail2->created_at],
            ['status_name' => 'Moved to "Test Stage"', 'date' => $detail1->created_at],
            ['status_name' => 'Created to "Tugas Aktif"', 'date' => $progress->created_at],
        ];

        $this->assertEquals($expectedValue, $log);
    }

    public function testWhereTaskInShouldNotReturnOtherFunnel(): void
    {
        $differentFunnel = factory(ActivityProgress::class)->create();
        $response = $this->repository->whereTaskIn(
            ($differentFunnel->consultant_activity_funnel_id + 1),
            [$differentFunnel->progressable_id]
        )->get();

        $this->assertCount(0, $response);
    }

    public function testWhereTaskInShouldNotReturnOtherId(): void
    {
        $differentId = factory(ActivityProgress::class)->create();
        $response = $this->repository->whereTaskIn(
            $differentId->consultant_activity_funnel_id,
            [$differentId->progressable_id + 1]
        )->get();

        $this->assertCount(0, $response);
    }

    public function testWhereTaskInShouldReturnCorrectFunnelIdAndId(): void
    {
        $correctData = factory(ActivityProgress::class)->create();

        $response = $this->repository->whereTaskIn(
            $correctData->consultant_activity_funnel_id,
            [$correctData->progressable_id]
        )->get();

        $this->assertCount(1, $response);
        $this->assertTrue($response[0]->is($correctData));
    }

    public function testCreateWithContract(): void
    {
        $progressable = factory(MamipayContract::class)->create();
        $newModel = $this->repository->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable' => $progressable
            ]
        );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
                'progressable_id' => $progressable->id,
                'current_stage' => 0
            ]
        );
    }

    public function testCreateWithRoom(): void
    {
        $progressable = factory(Room::class)->create();
        $newModel = $this->repository->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable' => $progressable
            ]
        );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
                'progressable_id' => $progressable->id,
                'current_stage' => 0
            ]
        );
    }

    public function testCreateWithPotentialProperty(): void
    {
        $progressable = factory(PotentialProperty::class)->create();
        $newModel = $this->repository->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable' => $progressable
            ]
        );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                'progressable_id' => $progressable->id,
                'current_stage' => 0
            ]
        );
    }

    public function testCreateWithPotentialTenant(): void
    {
        $progressable = factory(PotentialTenant::class)->create();
        $newModel = $this->repository->create(
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable' => $progressable
            ]
        );

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'consultant_activity_funnel_id' => 1,
                'consultant_id' => 1,
                'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
                'progressable_id' => $progressable->id,
                'current_stage' => 0
            ]
        );
    }

    public function testMoveToToDo(): void
    {
        $task = factory(ActivityProgress::class)->create([
            'current_stage' => 5
        ]);
        $this->repository->moveToToDo($task->id);

        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'deleted_at' => null,
                'current_stage' => 0
            ]
        );
    }

    public function testMoveToToDoWithInvalidId(): void
    {
        $task = factory(ActivityProgress::class)->create([
            'current_stage' => 5
        ]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->moveToToDo(1);
        $this->assertDatabaseHas(
            'consultant_activity_progress',
            [
                'id' => $task->id,
                'deleted_at' => null,
                'current_stage' => 5
            ]
        );
    }

    public function testListWithDBET(): void
    {
        $progress = factory(ActivityProgress::class)->states('withDBET', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->repository->list()->first();
        $expected = [
            'id' => $progress->id,
            'current_stage' => $progress->current_stage,
            'funnel_id' => $progress->consultant_activity_funnel_id,
            'total_stage' => $progress->funnel->total_stage,
            'stage_name' => $form->name,
            'related_table' => $progress->funnel->related_table,
            'progressable_type' => $progress->progressable_type,
            'progressable_id' => $progress->progressable_id,
            'potential_tenant_id' => $progress->progressable->id,
            'potential_tenant_name' => $progress->progressable->name,
            'kost_id' => null,
            'kost_name' => null,
            'tenant_id' => null,
            'tenant_name' => null,
            'potential_property_id' => null,
            'potential_property_name' => null,
            'potential_owner_id' => null,
            'potential_owner_name' => null
        ];

        $this->assertEquals($expected, $actual->toArray());
    }

    public function testListWithProperty(): void
    {
        $progress = factory(ActivityProgress::class)->states('withProperty', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->repository->list()->first();
        $expected = [
            'id' => $progress->id,
            'current_stage' => $progress->current_stage,
            'funnel_id' => $progress->consultant_activity_funnel_id,
            'total_stage' => $progress->funnel->total_stage,
            'stage_name' => $form->name,
            'related_table' => $progress->funnel->related_table,
            'progressable_type' => $progress->progressable_type,
            'progressable_id' => $progress->progressable_id,
            'potential_tenant_id' => null,
            'potential_tenant_name' => null,
            'kost_id' => $progress->progressable->id,
            'kost_name' => $progress->progressable->name,
            'tenant_id' => null,
            'tenant_name' => null,
            'potential_property_id' => null,
            'potential_property_name' => null,
            'potential_owner_id' => null,
            'potential_owner_name' => null
        ];

        $this->assertEquals($expected, $actual->toArray());
    }

    public function testListWithContract(): void
    {
        $progress = factory(ActivityProgress::class)->states('withContract', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->repository->list()->first();
        $expected = [
            'id' => $progress->id,
            'current_stage' => $progress->current_stage,
            'funnel_id' => $progress->consultant_activity_funnel_id,
            'total_stage' => $progress->funnel->total_stage,
            'stage_name' => $form->name,
            'related_table' => $progress->funnel->related_table,
            'progressable_type' => $progress->progressable_type,
            'progressable_id' => $progress->progressable_id,
            'potential_tenant_id' => null,
            'potential_tenant_name' => null,
            'kost_id' => null,
            'kost_name' => null,
            'tenant_id' => $progress->progressable->tenant->id,
            'tenant_name' => $progress->progressable->tenant->name,
            'potential_property_id' => null,
            'potential_property_name' => null,
            'potential_owner_id' => null,
            'potential_owner_name' => null
        ];

        $this->assertEquals($expected, $actual->toArray());
    }

    public function testListWithPotentialProperty(): void
    {
        $progress = factory(ActivityProgress::class)->states('withPotentialProperty', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->repository->list()->first();
        $expected = [
            'id' => $progress->id,
            'current_stage' => $progress->current_stage,
            'funnel_id' => $progress->consultant_activity_funnel_id,
            'total_stage' => $progress->funnel->total_stage,
            'stage_name' => $form->name,
            'related_table' => $progress->funnel->related_table,
            'progressable_type' => $progress->progressable_type,
            'progressable_id' => $progress->progressable_id,
            'potential_tenant_id' => null,
            'potential_tenant_name' => null,
            'kost_id' => null,
            'kost_name' => null,
            'tenant_id' => null,
            'tenant_name' => null,
            'potential_property_id' => $progress->progressable->id,
            'potential_property_name' => $progress->progressable->name,
            'potential_owner_id' => null,
            'potential_owner_name' => null
        ];

        $this->assertEquals($expected, $actual->toArray());
    }

    public function testListWithPotentialOwner(): void
    {
        $progress = factory(ActivityProgress::class)->states('withPotentialOwner', 'withForm')->create();
        $form = ActivityForm::where('consultant_activity_funnel_id', $progress->consultant_activity_funnel_id)
            ->where('stage', $progress->current_stage)
            ->first();

        $actual = $this->repository->list()->first();
        $expected = [
            'id' => $progress->id,
            'current_stage' => $progress->current_stage,
            'funnel_id' => $progress->consultant_activity_funnel_id,
            'total_stage' => $progress->funnel->total_stage,
            'stage_name' => $form->name,
            'related_table' => $progress->funnel->related_table,
            'progressable_type' => $progress->progressable_type,
            'progressable_id' => $progress->progressable_id,
            'potential_tenant_id' => null,
            'potential_tenant_name' => null,
            'kost_id' => null,
            'kost_name' => null,
            'tenant_id' => null,
            'tenant_name' => null,
            'potential_property_id' => null,
            'potential_property_name' => null,
            'potential_owner_id' => $progress->progressable->id,
            'potential_owner_name' => $progress->progressable->name
        ];

        $this->assertEquals($expected, $actual->toArray());
    }
}
