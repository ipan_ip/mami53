<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Media\Media;
use App\Http\Helpers\PhoneNumberHelper;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use App\Entities\Property\PropertyContract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Property\Property;

class PotentialPropertyRepositoryEloquentTest extends MamiKosTestCase
{
    
    /**
     *  The repository to test
     *
     * @var PotentialPropertyRepositoryEloquent
     */
    protected $repository;

    protected $consultant;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new PotentialPropertyRepositoryEloquent(app());
        $this->consultant = factory(Consultant::class)->create(
            [
                'user_id' => factory(User::class)->create()->id
            ]
        );
    }

    public function testModel(): void
    {
        $this->assertSame($this->repository->model(), PotentialProperty::class);
    }

    public function testGetList(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(1, 0);
        $this->assertEquals($property->name, $properties[0]->name);
        $this->assertEquals($property->owner_name, $properties[0]->owner_name);
        $this->assertEquals($property->owner_phone, $properties[0]->owner_phone);
        $this->assertEquals($property->media_id, $properties[0]->media_id);
        $this->assertEquals($property->created_at, $properties[0]->created_at);
    }

    public function testGetListWithLimitShouldReturnCorrectCount(): void
    {
        factory(PotentialProperty::class)->create(
            [
                'media_id' => factory(Media::class)->create()->id
            ]
        );

        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(1, 0);
        $this->assertCount(1, $properties);
        $this->assertTrue($properties[0]->is($property));
        $this->assertTrue($properties[0]->photo->is($photo));
    }

    public function testGetListWithOffsetShouldReturnCorrectData(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(1, 1);
        $this->assertCount(1, $properties);
        $this->assertTrue($properties[0]->is($property));
        $this->assertTrue($properties[0]->photo->is($photo));
    }

    public function testGetListShouldReturnCorrectOrder(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
            ]
        );

        $otherPhoto = factory(Media::class)->create();
        $otherProperty = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(2, 0);
        $this->assertTrue($properties[0]->is($otherProperty));
        $this->assertTrue($properties[1]->is($property));
    }

    public function testGetListWithConsultantShouldNotDisplayOther(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'consultant_id' => 0,
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(2, 0, $this->consultant->id);
        $this->assertCount(1, $properties);
        $this->assertTrue($properties[0]->is($property));
        $this->assertTrue($properties[0]->photo->is($photo));
    }

    public function testGetListWithNullConsultantIdShouldDisplayAll(): void
    {
        $photo = factory(Media::class)->create();
        $property = factory(PotentialProperty::class)->create(
            [
                'media_id' => $photo->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'consultant_id' => 0,
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $properties = $this->repository->getList(2, 0, null);
        $this->assertCount(2, $properties);
    }

    public function testCountCreatedByConsultantWithConsultantId(): void
    {
        factory(PotentialProperty::class)->create(
            [
                'media_id' => factory(Media::class)->create()->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'consultant_id' => 0,
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $count = $this->repository->countCreatedByConsultant($this->consultant->id);
        $this->assertSame(1, $count);
    }

    public function testCountCreateByConsultantWithNullConsultantId(): void
    {
        factory(PotentialProperty::class)->create(
            [
                'media_id' => factory(Media::class)->create()->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'consultant_id' => 0,
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $count = $this->repository->countCreatedByConsultant(null);
        $this->assertSame(2, $count);
    }

    public function testCountCreatedByConsultant(): void
    {
        factory(PotentialProperty::class)->create(
            [
                'media_id' => factory(Media::class)->create()->id,
                'consultant_id' => $this->consultant->id
            ]
        );

        factory(PotentialProperty::class)->create(
            [
                'consultant_id' => 0,
                'media_id' => factory(Media::class)->create()->id,
                'updated_at' => Carbon::now()->addHour()
            ]
        );

        $count = $this->repository->countCreatedByConsultant();
        $this->assertSame(2, $count);
    }

    /**
     * @group UG
     * @group UG-4412
     * @group App\Repositories\Consultant\PotentialPropertyRepositoryEloquent
     */
    public function testUpdateFollowupStatusByContract():void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $property = factory(Property::class)->create();
        $room = $this->createRoomAndMapToProperty($owner, $property);

        $potentialProperty = factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1]);
        $contract->properties()->attach($property);

        $updated = $this->repository
            ->updateFollowupStatusByContract($contract);

        $this->assertSame(1, $updated);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS, $potentialProperty->refresh()->followup_status);
    }

    public function testUpdateFollowupStatusByContractWithActiveContract(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $property = factory(Property::class)->create();
        $room = $this->createRoomAndMapToProperty($owner, $property);

        $potentialProperty = factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_ACTIVE]);
        $contract->properties()->attach($property);

        $updated = $this->repository
            ->updateFollowupStatusByContract($contract);

        $this->assertSame(1, $updated);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_APPROVED, $potentialProperty->refresh()->followup_status);
    }

    public function testUpdateFollowupStatusByContractWithTerminatedContract(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $property = factory(Property::class)->create();
        $room = $this->createRoomAndMapToProperty($owner, $property);

        $potentialProperty = factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_TERMINATED]);
        $contract->properties()->attach($property);

        $updated = $this->repository
            ->updateFollowupStatusByContract($contract);

        $this->assertSame(1, $updated);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_REJECTED, $potentialProperty->refresh()->followup_status);
    }

    /**
     * @group UG
     * @group UG-4412
     * @group App\Repositories\Consultant\PotentialPropertyRepositoryEloquent
     */
    public function testUpdateFollowupStatusByContractWithMultipleProperties():void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $property1 = factory(Property::class)->create();
        $property2 = factory(Property::class)->create();

        $room1 = $this->createRoomAndMapToProperty($owner, $property1);
        $room2 = $this->createRoomAndMapToProperty($owner, $property2);
        $room3 = $this->createRoomAndMapToProperty($owner, $property2);

        $potentialProperty1 = factory(PotentialProperty::class)->create([
            'designer_id' => $room1->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $potentialProperty2 = factory(PotentialProperty::class)->create([
            'designer_id' => $room2->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);
        $potentialProperty3 = factory(PotentialProperty::class)->create([
            'designer_id' => $room3->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => 'active']);
        $contract->properties()->attach($property1);
        $contract->properties()->attach($property2);

        $updated = $this->repository
            ->updateFollowupStatusByContract($contract, PotentialProperty::FOLLOWUP_STATUS_APPROVED);

        $this->assertSame(3, $updated);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_APPROVED, $potentialProperty1->refresh()->followup_status);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_APPROVED, $potentialProperty2->refresh()->followup_status);
        $this->assertEquals(PotentialProperty::FOLLOWUP_STATUS_APPROVED, $potentialProperty3->refresh()->followup_status);
    }

    private function createRoomAndMapToProperty(User $owner, Property $property): Room
    {
        $room = factory(Room::class)->create(['property_id' => $property->id]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);
        return $room;
    }

    // public function testCreateWithRemarkParamShouldCreateNewPotentialPropertyWithNote()
    // {
    //     $input = [
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'remark' => 'note',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->create($input);

    //     $property = PotentialProperty::where('name', 'test kost')->get();
    //     $this->assertNotNull($property);
    //     $this->assertEquals($input['name'], $property[0]->name);
    //     $this->assertEquals($input['owner_name'], $property[0]->owner_name);
    //     $this->assertEquals($input['owner_email'], $property[0]->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property[0]->owner_phone);
    //     $this->assertEquals($input['media'], $property[0]->media_id);
    //     $this->assertEquals($input['role'], $property[0]->role);
    //     $this->assertEquals($this->consultant->id, $property[0]->consultant_id);
    //     $this->assertNotNull($property[0]->consultant_note);
    //     $this->assertEquals($input['remark'], $property[0]->consultant_note->content);
    // }

    // public function testCreateWithoutRemarkParamShouldCreateNewPotentialPropertyWithoutNote()
    // {
    //     $input = [
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->create($input);

    //     $property = PotentialProperty::where('name', 'test kost')->get();
    //     $this->assertNotNull($property);
    //     $this->assertEquals($input['name'], $property[0]->name);
    //     $this->assertEquals($input['owner_name'], $property[0]->owner_name);
    //     $this->assertEquals($input['owner_email'], $property[0]->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property[0]->owner_phone);
    //     $this->assertEquals($input['media'], $property[0]->media_id);
    //     $this->assertEquals($input['role'], $property[0]->role);
    //     $this->assertEquals($this->consultant->id, $property[0]->consultant_id);
    //     $this->assertNull($property[0]->consultant_note);
    // }

    // public function testCreateWithEmptyRemarkShouldCreateNewPotentialPropertyWithoutNote()
    // {
    //     $input = [
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'remark' => '',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->create($input);

    //     $property = PotentialProperty::where('name', 'test kost')->get();
    //     $this->assertNotNull($property);
    //     $this->assertEquals($input['name'], $property[0]->name);
    //     $this->assertEquals($input['owner_name'], $property[0]->owner_name);
    //     $this->assertEquals($input['owner_email'], $property[0]->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property[0]->owner_phone);
    //     $this->assertEquals($input['media'], $property[0]->media_id);
    //     $this->assertEquals($input['role'], $property[0]->role);
    //     $this->assertEquals($this->consultant->id, $property[0]->consultant_id);
    //     $this->assertNull($property[0]->consultant_note);
    // }

    // public function testUpdateWithoutRemarkParamShouldNotDeleteExistingNote()
    // {
    //     $propertyId = factory(PotentialProperty::class)->create(
    //         [
    //             'consultant_id' => $this->consultant->id
    //         ]
    //     )->id;
    //     $note = factory(ConsultantNote::class)->create(
    //         [
    //             'noteable_type' => 'potential_property',
    //             'noteable_id' => $propertyId
    //         ]
    //     );

    //     $input = [
    //         'property_id' => $propertyId,
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->update($input, $propertyId);

    //     $property = PotentialProperty::find($propertyId);
    //     $this->assertEquals($input['name'], $property->name);
    //     $this->assertEquals($input['owner_name'], $property->owner_name);
    //     $this->assertEquals($input['owner_email'], $property->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property->owner_phone);
    //     $this->assertEquals($input['media'], $property->media_id);
    //     $this->assertEquals($input['role'], $property->role);
    //     $this->assertEquals($this->consultant->id, $property->consultant_id);
    //     $this->assertTrue($property->consultant_note->is($note));
    // }

    // public function testUpdateWithEmptyRemarkParamShouldNotDeleteExistingNote()
    // {
    //     $propertyId = factory(PotentialProperty::class)->create(
    //         [
    //             'consultant_id' => $this->consultant->id
    //         ]
    //     )->id;
    //     $note = factory(ConsultantNote::class)->create(
    //         [
    //             'noteable_type' => 'potential_property',
    //             'noteable_id' => $propertyId
    //         ]
    //     );

    //     $input = [
    //         'property_id' => $propertyId,
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'remark' => '',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->update($input, $propertyId);

    //     $property = PotentialProperty::find($propertyId);
    //     $this->assertEquals($input['name'], $property->name);
    //     $this->assertEquals($input['owner_name'], $property->owner_name);
    //     $this->assertEquals($input['owner_email'], $property->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property->owner_phone);
    //     $this->assertEquals($input['media'], $property->media_id);
    //     $this->assertEquals($input['role'], $property->role);
    //     $this->assertEquals($this->consultant->id, $property->consultant_id);
    //     $this->assertTrue($property->consultant_note->is($note));
    // }

    // public function testUpdateWithNotEmptyRemarkParamShouldDeleteExistingNote()
    // {
    //     $propertyId = factory(PotentialProperty::class)->create(
    //         [
    //             'consultant_id' => $this->consultant->id
    //         ]
    //     )->id;
    //     $note = factory(ConsultantNote::class)->create(
    //         [
    //             'noteable_type' => 'potential_property',
    //             'noteable_id' => $propertyId
    //         ]
    //     );

    //     $input = [
    //         'property_id' => $propertyId,
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'remark' => 'new note',
    //         'consultant_id' => $this->consultant->id
    //     ];
    //     $this->repository->update($input, $propertyId);

    //     $property = PotentialProperty::find($propertyId);
    //     $this->assertEquals($input['name'], $property->name);
    //     $this->assertEquals($input['owner_name'], $property->owner_name);
    //     $this->assertEquals($input['owner_email'], $property->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property->owner_phone);
    //     $this->assertEquals($input['media'], $property->media_id);
    //     $this->assertEquals($input['role'], $property->role);
    //     $this->assertEquals($this->consultant->id, $property->consultant_id);
    //     $this->assertFalse($property->consultant_note->is($note));
    //     $deletedNote = $property->consultant_note()->withTrashed()->whereNotNull('deleted_at')->get();
    //     $this->assertEquals(2, count($property->consultant_note()->withTrashed()->get()));
    //     $this->assertEquals($note->id, $deletedNote[0]->id);
    //     $this->assertEquals($note->content, $deletedNote[0]->content);
    //     $this->assertEquals($input['remark'], $property->consultant_note->content);
    // }

    // public function testUpdateWithoutNoteShouldCreateNewNote()
    // {
    //     $property = factory(PotentialProperty::class)->create(
    //         [
    //             'consultant_id' => $this->consultant->id
    //         ]
    //     );

    //     $input = [
    //         'property_id' => $property->id,
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'media' => 10,
    //         'role' => 'supply',
    //         'remark' => 'new note',
    //         'consultant_id' => $this->consultant->id
    //     ];

    //     $this->assertNull($property->consultant_note);
    //     $this->repository->update($input, $property->id);

    //     $property = PotentialProperty::find($property->id);
    //     $this->assertEquals($input['name'], $property->name);
    //     $this->assertEquals($input['owner_name'], $property->owner_name);
    //     $this->assertEquals($input['owner_email'], $property->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property->owner_phone);
    //     $this->assertEquals($input['media'], $property->media_id);
    //     $this->assertEquals($input['role'], $property->role);
    //     $this->assertEquals($this->consultant->id, $property->consultant_id);
    //     $this->assertEquals(1, count($property->consultant_note()->withTrashed()->get()));
    //     $this->assertEquals($input['remark'], $property->consultant_note->content);
    // }

    // public function testUpdateWithoutMediaParamShouldNotDeleteOldMedia()
    // {
    //     $property = factory(PotentialProperty::class)->create(
    //         [
    //             'consultant_id' => $this->consultant->id
    //         ]
    //     );

    //     $input = [
    //         'property_id' => $property->id,
    //         'name' => 'test kost',
    //         'owner_name' => 'test owner',
    //         'owner_phone' => '081111111',
    //         'owner_email' => 'email@email.com',
    //         'role' => 'supply',
    //         'remark' => 'new note',
    //         'consultant_id' => $this->consultant->id
    //     ];

    //     $this->assertNull($property->consultant_note);
    //     $this->repository->update($input, $property->id);

    //     $property = PotentialProperty::find($property->id);
    //     $this->assertEquals($input['name'], $property->name);
    //     $this->assertEquals($input['owner_name'], $property->owner_name);
    //     $this->assertEquals($input['owner_email'], $property->owner_email);
    //     $this->assertEquals($input['owner_phone'], $property->owner_phone);
    //     $this->assertEquals($property->media_id, $property->media_id);
    //     $this->assertEquals($input['role'], $property->role);
    //     $this->assertEquals($this->consultant->id, $property->consultant_id);
    //     $this->assertEquals(1, count($property->consultant_note()->withTrashed()->get()));
    //     $this->assertEquals($input['remark'], $property->consultant_note->content);
    // }
}
