<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\ConsultantRole;
use App\Test\MamiKosTestCase;

class ConsultantRoleRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = $this->app->make(ConsultantRoleRepository::class);
    }

    public function testModel(): void
    {
        $this->assertEquals($this->repo->model(), ConsultantRole::class);
    }
}
