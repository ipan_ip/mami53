<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class ConsultantRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = $this->app->make(ConsultantRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals(Consultant::class, $this->repo->model());
    }
}
