<?php

namespace App\Repositories\Consultant\ActivityManagement;

use App\Entities\Consultant\PotentialTenant;
use App\Repositories\Consultant\PotentialTenantRepositoryEloquent;
use App\Test\MamiKosTestCase;

class PotentialTenantRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = new PotentialTenantRepositoryEloquent(app());
    }

    public function testModelShouldReturnCorrectModel(): void
    {
        $this->assertEquals($this->repository->model(), PotentialTenant::class);
    }
}
