<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\PotentialOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use App\Entities\Property\PropertyContract;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Entities\Property\Property;

class PotentialOwnerRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = $this->app->make(PotentialOwnerRepository::class);
        $this->user = factory(User::class)->create();
    }

    public function testModel(): void
    {
        $this->assertEquals(PotentialOwner::class, $this->repo->model());
    }

    public function testFindByUserId(): void
    {
        $potentialOwner = factory(PotentialOwner::class)->create([
            'user_id' => 1
        ]);

        $this->assertEquals($potentialOwner->id, $this->repo->findByUserId(1)->id);
    }

    /**
     * @group UG
     * @group UG-4412
     * @group App\Repositories\Consultant\PotentialOwnerRepositoryEloquent
     */
    public function testUpdateFollowupStatusByContractWithOngoingSuccess():void
    {
        $property = factory(Property::class)->create(['owner_user_id' => $this->user->id]);

        $potentialOwner = factory(PotentialOwner::class)->create([
            'user_id' => $this->user->id,
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_INACTIVE]);
        $contract->properties()->attach($property);

        $result = $this->repo->updateFollowupStatusByContract($contract);

        $this->assertInstanceOf(PotentialOwner::class, $result);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_ONGOING, $result->followup_status);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_ONGOING, $potentialOwner->refresh()->followup_status);
    }

    /**
     * @group UG
     * @group UG-4412
     * @group App\Repositories\Consultant\PotentialOwnerRepositoryEloquent
     */
    public function testUpdateFollowupStatusByContractWithDoneSuccess():void
    {
        $property = factory(Property::class)->create(['owner_user_id' => $this->user->id]);

        $potentialOwner = factory(PotentialOwner::class)->create([
            'user_id' => $this->user->id,
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_ONGOING
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_ACTIVE]);
        $contract->properties()->attach($property);

        $result = $this->repo->updateFollowupStatusByContract($contract);

        $this->assertInstanceOf(PotentialOwner::class, $result);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_DONE, $result->followup_status);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_DONE, $potentialOwner->refresh()->followup_status);
    }

    public function testUpdateFollowupStatusByContractWithTerminatedContract(): void
    {
        $property = factory(Property::class)->create(['owner_user_id' => $this->user->id]);

        $potentialOwner = factory(PotentialOwner::class)->create([
            'user_id' => $this->user->id,
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_ONGOING
        ]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_TERMINATED]);
        $contract->properties()->attach($property);

        $result = $this->repo->updateFollowupStatusByContract($contract);

        $this->assertInstanceOf(PotentialOwner::class, $result);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_DONE, $result->followup_status);
        $this->assertEquals(PotentialOwner::FOLLOWUP_STATUS_DONE, $potentialOwner->refresh()->followup_status);
    }

    /**
     * @group UG
     * @group UG-4412
     * @group App\Repositories\Consultant\PotentialOwnerRepositoryEloquent
     */
    public function testUpdateFollowupStatusByContractWithoutOwnerShouldReturnNull():void
    {
        $property = factory(Property::class)->create(['owner_user_id' => $this->user->id]);

        $contract = factory(PropertyContract::class)->create(['property_level_id' => 1, 'status' => PropertyContract::STATUS_INACTIVE]);
        $contract->properties()->attach($property);

        $result = $this->repo->updateFollowupStatusByContract($contract);

        $this->assertNull($result);
    }
}
