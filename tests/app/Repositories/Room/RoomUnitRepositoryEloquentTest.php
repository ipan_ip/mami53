<?php

namespace app\Repositories\Room;

use App\Criteria\Room\RoomUnitCriteria;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;

class RoomUnitRepositoryEloquentTest extends MamiKosTestCase
{
    use WithFaker;

    private $repo;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repo = app()->make(RoomUnitRepository::class);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testBackFillRoomUnitThrowException()
    {
        $room = factory(Room::class)->create([
            'room_count' => 2,
            'room_available' => 1
        ]);
        factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 1
        ]);

        Bugsnag::shouldReceive('notifyException')->once();

        $roomUnit = $this->repo->backFillRoomUnit($room);

        $this->assertNull($roomUnit);
    }

    public function testBackFillRoomUnitWithZeroRoomCount()
    {
        $room = factory(Room::class)->create([
            'room_count' => 0,
            'room_available' => 0
        ]);

        $roomUnit = $this->repo->backFillRoomUnit($room);

        $this->assertNull($roomUnit);
    }

    public function testBackFillRoomUnitWithLessThanZeroRoomAvailable()
    {
        $roomTotal = 4;
        $roomAvailable = - 4;
        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $roomUnit = $this->repo->backFillRoomUnit(
            $room,
            function () use ($room) {
                return RoomUnit::where('designer_id', $room->id)->get();
            }
        );

        $this->assertEquals($roomTotal, $roomUnit->count(), 'room total match');
        $this->assertEquals(0, $roomUnit->where('occupied', false)->count(), 'empty room match');
    }

    public function testBackFillRoomUnitRoomAllotmentCreated()
    {
        $roomTotal = 8;
        $roomAvailable = 5;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal, count($room->room_unit), 'room total match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testBackFillRoomUnitRoomAllotmentCreatedFull()
    {
        $roomTotal = 8;
        $roomAvailable = 0;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal, count($room->room_unit), 'room total match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testBackFillRoomUnitRoomAllotmentCreatedEmpty()
    {
        $roomTotal = 8;
        $roomAvailable = 8;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal, count($room->room_unit), 'room total match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testBackFillRoomUnitRoomAllotmentCreatedTotalLessThanEmpty()
    {
        $roomTotal = 8;
        $roomAvailable = 10;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal + $roomAvailable, count($room->room_unit), 'occupied match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testBackFillRoomUnitRoomAllotmentCreatedInSuccession()
    {
        $roomTotal = 8;
        $roomAvailable = 3;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal, count($room->room_unit), 'room total match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');

        $roomTotal = 8;
        $roomAvailable = 7;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty', 'room_unit'],
            true
        );

        $this->assertEquals($roomTotal, count($room->room_unit), 'room total match');
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testAddSingleRoomUnit()
    {
        $room = factory(Room::class)->create();
        $data = [
            'designer_id' => $room->id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];

        $unit = $this->repo->addRoomUnit($data, $room);

        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
        $this->assertEquals($unit->is_charge_by_room, false);
    }

    public function testAddSingleRoomUnitWithSpace()
    {
        $room = factory(Room::class)->create();
        $data = [
            'designer_id' => $room->id,
            'name' => ' unit testing  ',
            'floor' => ' 1 ',
            'occupied' => true
        ];

        $unit = $this->repo->addRoomUnit($data, $room);

        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, trim($data['name']));
        $this->assertEquals($unit->floor, trim($data['floor']));
        $this->assertEquals($unit->occupied, $data['occupied']);
        $this->assertEquals($unit->is_charge_by_room, false);
    }

    public function testAddAnotherSingleRoomUnit()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'name' => 'old room',
            'floor' => '1',
            'occupied' => true,
            'is_charge_by_room' => true,
        ]);

        $data = [
            'designer_id' => $room->id,
            'name' => 'new room',
            'floor' => '1',
            'occupied' => false
        ];

        $unit = $this->repo->addRoomUnit($data, $room);

        $this->assertEquals($unit->designer_id, $room->id);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
        $this->assertEquals($unit->is_charge_by_room, true);
    }

    public function testUpdateSingleRoomUnit()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);
        $data = [
            'id' => $roomUnit->id,
            'designer_id' => $room->id,
            'name' => 'unit testing',
            'floor' => '1',
            'occupied' => true
        ];

        $unit = $this->repo->updateRoomUnit($roomUnit, $data);

        $this->assertEquals($unit->id, $data['id']);
        $this->assertEquals($unit->designer_id, $data['designer_id']);
        $this->assertEquals($unit->name, $data['name']);
        $this->assertEquals($unit->floor, $data['floor']);
        $this->assertEquals($unit->occupied, $data['occupied']);
    }

    public function testUpdateSingleRoomUnitWithSpace()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);
        $data = [
            'id' => $roomUnit->id,
            'designer_id' => $room->id,
            'name' => '  unit testing  ',
            'floor' => '  1  ',
            'occupied' => true
        ];

        $unit = $this->repo->updateRoomUnit($roomUnit, $data);

        $this->assertEquals($unit->id, $data['id']);
        $this->assertEquals($unit->designer_id, $data['designer_id']);
        $this->assertEquals($unit->name, trim($data['name']));
        $this->assertEquals($unit->floor, trim($data['floor']));
        $this->assertEquals($unit->occupied, $data['occupied']);
    }

    public function testDeleteSingleRoomUnit()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);

        $this->repo->deleteRoomUnit($roomUnit);

        $this->assertDatabaseMissing('designer_room', $roomUnit->toArray());
    }


    public function testDecreaseRoomAvailableSpecificUnitId()
    {
        $roomTotal = 8;
        $roomAvailable = 6;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty'],
            false
        );

        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');

        $emptyUnit = $room->room_unit_empty->random();
        $this->assertTrue($emptyUnit->isEmpty());

        $this->repo->decreaseRoomAvailable($room, $emptyUnit->id);
        $emptyUnit->refresh();
        $this->assertTrue($emptyUnit->isOccupied());
    }

    public function testDecreaseRoomAvailableUnSpecificUnitId()
    {
        $roomTotal = 8;
        $roomAvailable = 5;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty'],
            false
        );

        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');

        $this->repo->decreaseRoomAvailable($room);
        $room->load(['room_unit_empty']);

        $this->assertTrue($roomAvailable > count($room->room_unit_empty));
    }

    public function testDecreaseRoomAvailableUndefinedUnitIdNoChange()
    {
        $roomTotal = 8;
        $roomAvailable = 4;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty'],
            false
        );

        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');

        $emptyUnit = $room->room_unit_empty->random();
        $this->repo->decreaseRoomAvailable($room, $emptyUnit->id + 888);
        $room->load(['room_unit_empty']);
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testDecreaseRoomAvailableFullKos()
    {
        $roomTotal = 8;
        $roomAvailable = 0;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_empty'],
            false
        );

        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');

        $this->repo->decreaseRoomAvailable($room);
        $room->load(['room_unit_empty']);

        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
    }

    public function testIncreaseRoomAvailableSpecificUnitId()
    {
        $roomTotal = 8;
        $roomAvailable = 5;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_occupied'],
            false
        );

        $this->assertEquals($roomTotal - $roomAvailable, count($room->room_unit_occupied));

        $occupiedUnit = $room->room_unit_occupied->random();
        $this->assertTrue($occupiedUnit->isOccupied());

        $this->repo->increaseRoomAvailable($room, $occupiedUnit->id);
        $occupiedUnit->refresh();
        $this->assertTrue($occupiedUnit->isEmpty());
    }

    public function testIncreaseRoomAvailableUnSpecificUnitId()
    {
        $roomTotal = 8;
        $roomAvailable = 4;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_occupied'],
            false
        );

        $this->assertEquals($roomTotal - $roomAvailable, count($room->room_unit_occupied));

        $this->repo->increaseRoomAvailable($room);
        $room->load(['room_unit_occupied']);

        $this->assertTrue($roomTotal - $roomAvailable > count($room->room_unit_occupied));
    }

    public function testIncreaseRoomAvailableUndefinedUnitIdNoChange()
    {
        $roomTotal = 8;
        $roomAvailable = 3;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_occupied'],
            false
        );

        $this->assertEquals($roomTotal - $roomAvailable, count($room->room_unit_occupied));

        $occupiedUnit = $room->room_unit_occupied->random();
        $this->repo->increaseRoomAvailable($room, $occupiedUnit->id + 888);
        $room->load(['room_unit_occupied']);
        $this->assertEquals($roomTotal - $roomAvailable, count($room->room_unit_occupied));
    }

    public function testIncreaseRoomAvailableEmptyKos()
    {
        $roomTotal = 8;
        $roomAvailable = $roomTotal;

        $room = $this->prepareModels(
            $roomTotal,
            $roomAvailable,
            ['room_unit_occupied', 'room_unit_empty'],
            false
        );

        $this->assertEquals(0, count($room->room_unit_occupied));
        $this->assertEquals($roomTotal, count($room->room_unit_empty));

        $this->repo->increaseRoomAvailable($room);
        $room->load(['room_unit_occupied']);

        $this->assertEquals(0, count($room->room_unit_occupied));
    }

    public function testAdjustRoomUnitAvailability()
    {
        $roomTotal = rand(8, 16);
        $roomAvailable = $roomTotal - rand(0, 7);
        $roomAvailablenew = rand(0, 8);

        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $this->repo->backFillRoomUnit($room);

        $room->room_available = $roomAvailablenew;
        $room->save();
        
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
        $this->repo->adjustRoomUnitAvailability($room, $roomAvailable);

        $room->load('room_unit_empty');
        $this->assertEquals($roomAvailablenew, count($room->room_unit_empty));
    }

    public function testAdjustRoomUnitAvailabilityOldAvailableLessThanUnitAvailable()
    {
        $roomTotal = rand(8, 16);
        $roomAvailable = $roomTotal - rand(1, 7);
        $roomAvailablenew = $roomAvailable + 1;

        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $this->repo->backFillRoomUnit($room);

        $room->room_available = $roomAvailablenew;
        $room->save();
        
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
        $this->repo->adjustRoomUnitAvailability($room, $roomAvailable);

        $room->load('room_unit_empty');
        $this->assertEquals($roomAvailablenew, count($room->room_unit_empty));
    }

    public function testAdjustRoomUnitAvailabilityZeroCount()
    {
        $roomTotal = rand(8, 16);
        $roomAvailable = $roomTotal - rand(0, 7);
        $roomAvailablenew = rand(0, 8);

        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $room->room_available = $roomAvailablenew;
        $room->save();
        
        $this->repo->adjustRoomUnitAvailability($room, $roomAvailable);

        $room->load('room_unit_empty');
        $this->assertEquals($roomAvailablenew, count($room->room_unit_empty));
    }

    public function testRecalculateRoomUnitCaseTotalIncreaseAvailableIncrease()
    {
        $this->assertRecalculateRoomUnit(8, 4, 12, 7);
    }

    public function testRecalculateRoomUnitCaseTotalIncreaseAvailableDecrease()
    {
        $this->assertRecalculateRoomUnit(8, 4, 12, 2);
    }

    public function testRecalculateRoomUnitCaseTotalDecreaseAvailableIncrease()
    {
        $this->assertRecalculateRoomUnit(9, 4, 6, 5);
    }

    public function testRecalculateRoomUnitCaseTotalDecreaseAvailableDecrease()
    {
        $this->assertRecalculateRoomUnit(10, 4, 4, 2);
    }

    private function assertRecalculateRoomUnit(
        int $roomTotal,
        int $roomAvailable,
        int $roomTotalNew,
        int $roomAvailableNew
    ) {
        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $this->repo->backFillRoomUnit($room);

        $room->room_available = $roomAvailableNew;
        $room->room_count = $roomTotalNew;
        $room->save();
        
        $this->assertEquals($roomAvailable, count($room->room_unit_empty), 'empty room match');
        $this->assertEquals($roomTotal - $roomAvailable, count($room->room_unit_occupied));
        $this->repo->recalculateRoomUnit($room, $roomTotal, $roomAvailable);

        $room->load(['room_unit_occupied', 'room_unit_empty']);
        $this->assertEquals($roomAvailableNew, count($room->room_unit_empty));
        $this->assertEquals($roomTotalNew - $roomAvailableNew, count($room->room_unit_occupied));
    }

    public function testRecalculateRoomUnitNoUnitsYet()
    {
        $roomTotal = rand(8, 16);
        $roomAvailable = rand(0, 8);
        $roomTotalNew = rand(17, 30);
        $roomAvailableNew = rand(0, 17);

        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        $room->room_available = $roomAvailableNew;
        $room->room_count = $roomTotalNew;
        $room->save();
        
        $this->repo->recalculateRoomUnit($room, $roomTotal, $roomAvailable);

        $room->load(['room_unit_occupied', 'room_unit_empty']);
        $this->assertEquals($roomAvailableNew, count($room->room_unit_empty));
        $this->assertEquals($roomTotalNew - $roomAvailableNew, count($room->room_unit_occupied));
    }


    private function prepareModels(
        int $roomTotal = 8,
        int $roomAvailable = 4,
        array $relationLoaded = ['room_unit_empty', 'room_unit'],
        bool $assertDatabase = false
    ): Room {

        $room = factory(Room::class)->create([
            'room_count' => $roomTotal,
            'room_available' => $roomAvailable
        ]);

        if ($assertDatabase) {
            $this->assertDatabaseMissing(
                'designer_room',
                [
                    'designer_id' => $room->id
                ]
            );
        }

        $this->repo->backFillRoomUnit($room);

        if ($assertDatabase) {
            $this->assertDatabaseHas(
                'designer_room',
                [
                    'designer_id' => $room->id
                ]
            );
        }

        $room->load($relationLoaded);

        return $room;
    }

    public function testGetRoomUnitListShouldAppliCriteria()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 2)->create([
            'designer_id' => $room->id
        ]);
        $this->repo->pushCriteria(new RoomUnitCriteria(1, 0));
        $response = $this->repo->getRoomUnitList($room->id);
        $this->assertCount(1, $response);
    }

    public function testGetRoomUnitListPaginated_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $result = $this->repo->getRoomUnitListPaginated($room->id);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithInvalidRoomId_ShouldFail()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $result = $this->repo->getRoomUnitListPaginated(999);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(0, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithLimitPagination_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $result = $this->repo->getRoomUnitListPaginated($room->id, [], 3);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(5, $result->total());
        $this->assertEquals(3, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithFilterRoomName_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        factory(RoomUnit::class, 1)->create([
            'designer_id' => $room->id,
            'name' => 'Executive',
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $result = $this->repo->getRoomUnitListPaginated($room->id, [ 'room-name' => 'Executive' ]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithFilterLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $result = $this->repo->getRoomUnitListPaginated($room->id, [ 'level-id' => $regularLevel->id ]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(3, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithFilterAllLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $result = $this->repo->getRoomUnitListPaginated($room->id, [ 'level-id' => 0 ]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(6, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomUnitListPaginated_WithFilterUnassignedLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();

        factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);
        
        $this->createRoomUnitAtLevel($room, $regularLevel, 3);
        $this->createRoomUnitAtLevel($room, $standardLevel, 3);

        $result = $this->repo->getRoomUnitListPaginated($room->id, [ 'level-id' => -1 ]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(3, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testUpdateRoomLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $result = $this->repo->updateRoomLevel($roomUnit, $regularLevel);
        $this->assertNotNull($result);
        $this->assertEquals($regularLevel->id, $result->room_level_id);
        $this->assertEquals($roomUnit->is_charge_by_room, $result->is_charge_by_room);

        $levelHistory = $roomUnit->level_histories()->orderBy('id', 'DESC')->first();
        $this->assertEquals($regularLevel->id, $levelHistory->room_level_id);
        $this->assertEquals($roomUnit->id, $levelHistory->room_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistory->action);
        
        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        $result = $this->repo->updateRoomLevel($roomUnit, $standardLevel);
        $this->assertNotNull($result);
        $this->assertEquals($standardLevel->id, $result->room_level_id);
        $this->assertEquals($roomUnit->is_charge_by_room, $result->is_charge_by_room);

        $levelHistory = $roomUnit->level_histories()->orderBy('id', 'DESC')->first();
        $this->assertEquals($standardLevel->id, $levelHistory->room_level_id);
        $this->assertEquals($roomUnit->id, $levelHistory->room_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistory->action);
    }

    public function testAssignRoomLevelToAllUnits_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnits = factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false
        ]);

        $regularLevel = factory(RoomLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'order' => 1,
        ]);

        $this->repo->assignRoomLevelToAllUnits($room->id, $regularLevel);
        $roomUnits = RoomUnit::whereIn('id', $roomUnits->pluck('id')->toArray())->get();

        foreach ($roomUnits as $roomUnit) { 
            $this->assertEquals($regularLevel->id, $roomUnit->room_level_id);
            $this->assertEquals(false, $roomUnit->is_charge_by_room);
            
            $levelHistory = $roomUnit->level_histories()->orderBy('id', 'DESC')->first();
            $this->assertEquals($regularLevel->id, $levelHistory->room_level_id);
            $this->assertEquals($roomUnit->id, $levelHistory->room_id);
            $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistory->action);
        }

        $standardLevel = factory(RoomLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'order' => 2,
        ]);

        $result = $this->repo->assignRoomLevelToAllUnits($room->id, $standardLevel);
        $roomUnits = RoomUnit::whereIn('id', $roomUnits->pluck('id')->toArray())->get();

        foreach ($roomUnits as $roomUnit) { 
            $this->assertEquals($standardLevel->id, $roomUnit->room_level_id);
            $this->assertEquals(false, $roomUnit->is_charge_by_room);

            $levelHistory = $roomUnit->level_histories()->orderBy('id', 'DESC')->first();
            $this->assertEquals($standardLevel->id, $levelHistory->room_level_id);
            $this->assertEquals($roomUnit->id, $levelHistory->room_id);
            $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistory->action);
        }
    }
    
    public function testUpdateRoomUnitChargeType_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnits = factory(RoomUnit::class, 3)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false
        ]);

        $this->repo->updateRoomUnitChargeType($room->id, true);
        $roomUnits = RoomUnit::whereIn('id', $roomUnits->pluck('id')->toArray())->get();
        
        foreach ($roomUnits as $roomUnit) { 
            $this->assertEquals($room->id, $roomUnit->designer_id);
            $this->assertEquals(true, $roomUnit->is_charge_by_room);
        }
    }

    public function testGetRoomUnitCountShouldReturnCorrectCount()
    {
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 2)->create([
            'designer_id' => $room->id
        ]);
        $response = $this->repo->getRoomUnitCount($room->id);
        $this->assertEquals(2, $response);
    }

    public function testGetRoomUnitCountShouldNotCountOtherKost()
    {
        $otherRoom = factory(Room::class)->create();
        factory(RoomUnit::class, 2)->create([
            'designer_id' => $otherRoom->id
        ]);
        $room = factory(Room::class)->create();
        factory(RoomUnit::class, 2)->create([
            'designer_id' => $room->id
        ]);
        $response = $this->repo->getRoomUnitCount($room->id);
        $this->assertEquals(2, $response);
    }

    public function testAssignNewLevelToAllUnits(): void
    {
        $kost = factory(Room::class)->create();
        $gp1 = factory(RoomLevel::class)->create();
        $gp2 = factory(RoomLevel::class)->create();
        $roomUnit1 = factory(RoomUnit::class)->create([
            'designer_id' => $kost->id,
            'room_level_id' => $gp1->id,
        ]);
        $roomUnit2 = factory(RoomUnit::class)->create([
            'designer_id' => $kost->id,
            'room_level_id' => $gp1->id,
        ]);

        $this->repo->assignNewLevelToAllUnits($kost->id, $gp2, factory(User::class)->create());
        $this->assertDatabaseHas(
            'designer_room',
            [
                'id' => $roomUnit1->id,
                'room_level_id' => $gp2->id
            ]
        );
        $this->assertDatabaseHas(
            'designer_room',
            [
                'id' => $roomUnit2->id,
                'room_level_id' => $gp2->id
            ]
        );
    }

    private function createRoomUnitAtLevel(Room $room, RoomLevel $roomLevel, int $count = 1)
    {
        for ($i=0; $i < $count; $i++) { 
            factory(RoomUnit::class)->create([
                'designer_id' => $room->id,
                'room_level_id' => $roomLevel->id,
            ]);
        }
    }

}
