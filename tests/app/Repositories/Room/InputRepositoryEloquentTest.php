<?php

namespace App\Repositories\Room;

use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Element\TypeCard;
use App\Entities\Room\Element\Unit;
use App\Entities\Room\Price;
use App\Entities\Room\PriceAdditional;
use App\Entities\Room\Room;
use App\Entities\Room\RoomInputProgress;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Test\MamiKosTestCase;
use App\User;

class InputRepositoryEloquentTest extends MamiKosTestCase
{

    private $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = app()->make(InputRepository::class);
    }

    public function testModelFunctionReturnRoom()
    {
        $this->assertEquals(Room::class, $this->repo->model());
    }

    public function testBootInstanceNothingHappen()
    {
        $this->assertNull($this->repo->boot());
    }

    public function testSaveLocationCreateARoomSuccess()
    {
        $data = [
            'address' => 'alamat test',
            'latitude' => 0,
            'longitude' => 0,
            'fac_near' => 'none',
            'city' => 'kota',
            'subdistrict' => 'kecamatan',
        ];

        $this->assertDatabaseMissing('designer', ['address' => 'alamat test']);
        $this->repo->saveLocation($data);
        $this->assertDatabaseHas('designer', ['address' => 'alamat test']);
    }

    public function testUpdateLocationSuccess()
    {
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 1, 'address' => 'alamat asli']);
        $expectedAddress = 'alamat test';
        $data = [
            'room_id' => $room->song_id,
            'address' => $expectedAddress,
            'latitude' => 0,
            'longitude' => 0,
            'fac_near' => 'none',
            'city' => 'kota',
            'subdistrict' => 'kecamatan',
        ];
        $this->assertNotEquals($expectedAddress, $room->address);
        $result = $this->repo->updateLocation($data);
        $this->assertInstanceOf(Room::class, $result);
        $this->assertEquals($expectedAddress, $result->address);
        $this->assertEquals($room->id, $result->id);
    }

    public function testUpdateLocationNullOnNotFound()
    {
        $data = [
            'room_id' => 888,
            'address' => 'alamat palsu',
            'latitude' => 0,
            'longitude' => 0,
            'fac_near' => 'none',
            'city' => 'kota',
            'subdistrict' => 'kecamatan',
        ];
        $this->assertNull($this->repo->updateLocation($data));
    }

    public function testOwnerClaimCreateRelationBetweenRoomAndUser()
    {
        $user = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create(['room_count' => 1, 'room_available' => 1, 'address' => 'alamat asli']);

        $this->assertDatabaseMissing('designer_owner', ['user_id' => $user->id]);
        $this->assertDatabaseMissing('designer_owner', ['designer_id' => $room->id]);

        $this->repo->ownerClaim($room, $user);

        $this->assertDatabaseHas('designer_owner', ['user_id' => $user->id]);
        $this->assertDatabaseHas('designer_owner', ['designer_id' => $room->id]);
    }

    public function testSaveInformationUpdateTheRoom()
    {
        $room = factory(Room::class)->create([
            'name' => 'Nama asli',
            'room_count' => 1,
            'room_available' => 1,
            'address' => 'alamat asli',
            'gender' => 0,
            'description' => '',
            'building_year' => '2010',
        ]);
        $data = [
            'name' => 'name test',
            'gender' => 1,
            'description' => 'new desc',
            'building_year' => '2012',
        ];

        $this->assertNotEquals($data['name'], $room->name);
        $this->assertNotEquals($data['gender'], $room->gender);
        $this->repo->saveInformation($room, $data);
        $this->assertEquals($data['name'], $room->name);
        $this->assertEquals($data['gender'], $room->gender);
    }

    public function testaddRoomPhotos()
    {
        $room = factory(Room::class)->create([]);
        $card = factory(CardPremium::class)->create([
            'designer_id' => $room->id
        ]);

        $data = [
            'kos_photos' => [
                [
                    'card_id' => $card->id
                ],
                [
                    'description' => 'description',
                    'photo_id' => rand(1, 100)
                ]
            ]
        ];

        $expectedResult = [
            'designer_id' => $room->id,
            'type' => 'image',
            'source' => 'new_owner',
            'description' => $data['kos_photos'][1]['description'],
            'photo_id' => $data['kos_photos'][1]['photo_id'],
        ];
        $this->repo->addRoomPhotos($room, $data);
        $this->assertDatabaseHas('designer_style_premium', $expectedResult);
    }

    public function testaddRoomTypePhotos()
    {
        $room = factory(Room::class)->create([]);
        $roomType = new Type;
        $roomType->designer_id = $room->id;
        $roomType->save();

        $card  = new TypeCard;
        $card->designer_type_id = $roomType->id;
        $card->save();

        $data = [
            'kos_type_photos' => [
                [
                    'card_id' => $card->id
                ],
                [
                    'description' => 'description',
                    'photo_id' => rand(1, 100)
                ]
            ]
        ];

        $expectedResult = [
            'designer_type_id' => $roomType->id,
            'type' => 'image',
            'source' => 'new_owner',
            'description' => $data['kos_type_photos'][1]['description'],
            'photo_id' => $data['kos_type_photos'][1]['photo_id'],
        ];
        $this->repo->addRoomTypePhotos($roomType, $data);
        $this->assertDatabaseHas('designer_type_style', $expectedResult);
    }

    public function testsaveOrUpdateInputProcessWithExsitingInputProgressShouldUpdate()
    {
        $room = factory(Room::class)->create([]);
        $inputProcess = factory(RoomInputProgress::class)->create([
            'designer_id' => $room->id,
            'on_step' => 1
        ]);

        $step = 12;
        $this->repo->saveOrUpdateInputProcess($room, $step);

        $expectedResult = [
            'id' => $inputProcess->id,
            'designer_id' => $room->id,
            'on_step' => $step,
            'is_completed' => RoomInputProgress::INPUT_PROGRESS_TOTAL == $step ? true : false
        ];

        $this->assertDatabaseHas('designer_input_progress', $expectedResult);
    }

    public function testsaveOrUpdateInputProcessWithRoomOwner()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);
        $inputProcess = factory(RoomInputProgress::class)->create([
            'designer_id' => $room->id,
            'on_step' => 1
        ]);

        $step = 12;
        $this->repo->saveOrUpdateInputProcess($room, $step);

        $expectedResult = [
            'id' => $inputProcess->id,
            'designer_id' => $room->id,
            'on_step' => $step,
            'is_completed' => RoomInputProgress::INPUT_PROGRESS_TOTAL == $step ? true : false
        ];

        $this->assertDatabaseHas('designer_input_progress', $expectedResult);
    }

    public function testsaveOrUpdateInputProcessShoudlCreateNewInputProgress()
    {
        $room = factory(Room::class)->create([]);
        $step = 1;
        $this->repo->saveOrUpdateInputProcess($room, $step);

        $expectedResult = [
            'designer_id' => $room->id,
            'on_step' => $step,
            'is_completed' => false
        ];

        $this->assertDatabaseHas('designer_input_progress', $expectedResult);
    }

    public function testsaveFacilitySetting()
    {
        $room = factory(Room::class)->create([]);
        $data = [
            'categories' => [
                [
                    'sub_categories' => [
                        [
                            'is_selected' => true,
                            'sub_category_id' => 1
                        ],
                        [
                            'is_selected' => true,
                            'sub_category_id' => 2
                        ]
                    ]
                ]
            ]
        ];

        $this->repo->saveFacilitySetting($room, $data);
        $tags = [1, 2];
        $tags = trim(json_encode($tags), '[]');

        $expectedResult = [
            'designer_id' => $room->id,
            'active_tags' => $tags
        ];

        $this->assertDatabaseHas('designer_facility_setting', $expectedResult);
    }

    public function testsaveFacilityTypeSetting()
    {
        $room = factory(Room::class)->create([]);
        $roomType = new Type;
        $roomType->designer_id = $room->id;
        $roomType->save();

        $data = [
            'categories' => [
                [
                    'sub_categories' => [
                        [
                            'is_selected' => true,
                            'sub_category_id' => 1
                        ],
                        [
                            'is_selected' => true,
                            'sub_category_id' => 2
                        ]
                    ]
                ]
            ]
        ];

        $result = $this->repo->saveFacilityTypeSetting($roomType, $data);
        $tags = [1, 2];
        $tags = trim(json_encode($tags), '[]');

        $expectedResult = [
            'designer_type_id' => $roomType->id,
            'active_tags' => $tags
        ];

        $this->assertDatabaseHas('designer_type_facility_setting', $expectedResult);
        $this->assertEquals($result, $data['categories']);
    }

    public function testsaveRoomTypeShouldCreateNewRecord()
    {
        $room = factory(Room::class)->create([]);
        $data = [
            'name_room_type' => 'type 1',
            'maximum_occupancy' => 10,
            'tenant_type' => [1, 2, 3],
            'room_size' => '25'
        ];

        $result = $this->repo->saveRoomType($room, $data);

        $expectedResult = [
            'designer_id' => $room->id,
            'name' => $data['name_room_type'],
            'maximum_occupancy' => $data['maximum_occupancy'],
            'tenant_type' => implode(',', $data['tenant_type']),
            'room_size' => $data['room_size']
        ];
        $result = array_only($result->toArray(), ['designer_id', 'name', 'maximum_occupancy', 'tenant_type', 'room_size']);
        $this->assertDatabaseHas('designer_type', $expectedResult);
        $this->assertEquals($result, $expectedResult);
    }

    public function testsaveRoomTypeWithExistingRoomTypeShouldUpdate()
    {
        $room = factory(Room::class)->create([]);

        $roomType = factory(Type::class)->create([
            'designer_id' => $room->id
        ]);

        $data = [
            'room_type_id' => $roomType->id,
            'name_room_type' => 'type 1',
            'maximum_occupancy' => 10,
            'tenant_type' => [1, 2, 3],
            'room_size' => '25'
        ];

        $result = $this->repo->saveRoomType($room, $data);

        $expectedResult = [
            'designer_id' => $room->id,
            'name' => $data['name_room_type'],
            'maximum_occupancy' => $data['maximum_occupancy'],
            'tenant_type' => implode(',', $data['tenant_type']),
            'room_size' => $data['room_size']
        ];
        $result = array_only($result->toArray(), ['designer_id', 'name', 'maximum_occupancy', 'tenant_type', 'room_size']);
        $this->assertDatabaseHas('designer_type', $expectedResult);
        $this->assertEquals($result, $expectedResult);
    }

    public function testsaveRoomUnitsWithInvalidRoomTypeIdShouldReturnFalse()
    {
        $room = factory(Room::class)->create([]);

        $data = ['room_type_id' => rand(1, 100)];
        $result = $this->repo->saveRoomUnits($room, $data);

        $this->assertFalse($result);
    }

    public function testsaveRoomUnitsWithValidType()
    {
        $room = factory(Room::class)->create([]);

        $roomType = factory(Type::class)->create([
            'designer_id' => $room->id
        ]);
        [$roomUnit1, $roomUnit2] = factory(Unit::class, 2)->create();
        $data = [
            'room_type_id' => $roomType->id,
            'total_room' => rand(1, 10),
            'available_room_units' => [
                [
                    'room_unit_id' => $roomUnit1->id,
                    'designer_type_id' => $roomType->id,
                    'room_name' => 'name unit 1',
                    'floor_name' => '1',
                    'is_active' => true,
                ],
                [
                    'room_unit_id' => $roomUnit2->id,
                    'is_deleted' => true,
                ],
                [
                    'room_name' => 'name unit 3',
                    'floor_name' => '3',
                    'is_active' => true,
                ],
                [
                    'room_unit_id' => 999,
                    'room_name' => 'invalid',
                    'floor_name' => '3',
                    'is_active' => true,
                ]
            ]
        ];
        $result = $this->repo->saveRoomUnits($room, $data);

        $expectedResult = [
            [
                'floor_name'   => $data['available_room_units'][0]['floor_name'],
                'room_name'    => $data['available_room_units'][0]['room_name'],
                'is_active'    => $data['available_room_units'][0]['is_active'] == 1,
            ],
            [
                'floor_name'   => $data['available_room_units'][2]['floor_name'],
                'room_name'    => $data['available_room_units'][2]['room_name'],
                'is_active'    => $data['available_room_units'][2]['is_active'] == 1,
            ]
        ];

        $unitDeleted = [
            'id' => $data['available_room_units'][1]['room_unit_id'],
        ];

        $unit1 = [
            'floor'   => $data['available_room_units'][0]['floor_name'],
            'name'    => $data['available_room_units'][0]['room_name'],
            'is_active'    => $data['available_room_units'][0]['is_active'] == 1,
        ];

        $unit3 = [
            'floor'   => $data['available_room_units'][2]['floor_name'],
            'name'    => $data['available_room_units'][2]['room_name'],
            'is_active'    => $data['available_room_units'][2]['is_active'] == 1,
        ];

        unset($result[0]['room_unit_id']);
        unset($result[1]['room_unit_id']);
        $this->assertEquals($result, $expectedResult);
        $this->assertDatabaseHas('designer_unit', $unit1);
        $this->assertDatabaseHas('designer_unit', $unit3);
        $this->assertDatabaseMissing('designer_unit', $unitDeleted);
    }

    public function testsaveRoomUnitsSingleWithoutRoomType()
    {
        $room = factory(Room::class)->create([]);

        $roomType = factory(Type::class)->create([
            'designer_id' => $room->id
        ]);
        [$roomUnit1, $roomUnit2] = factory(Unit::class, 2)->create();

        $data = [
            'total_room' => rand(1, 10),
            'available_room_units' => [
                [
                    'room_unit_id' => $roomUnit1->id,
                    'designer_type_id' => $roomType->id,
                    'room_name' => 'name unit 1',
                    'floor_name' => '1',
                    'is_active' => true,
                ],
                [
                    'room_unit_id' => $roomUnit2->id,
                    'is_deleted' => true,
                ],
                [
                    'room_name' => 'name unit 3',
                    'floor_name' => '3',
                    'is_active' => true,
                ]
            ]
        ];
        $result = $this->repo->saveRoomUnits($room, $data);

        $expectedResult = [
            [
                'floor_name'   => $data['available_room_units'][0]['floor_name'],
                'room_name'    => $data['available_room_units'][0]['room_name'],
                'is_active'    => $data['available_room_units'][0]['is_active'] == 1,
            ],
            [
                'floor_name'   => $data['available_room_units'][2]['floor_name'],
                'room_name'    => $data['available_room_units'][2]['room_name'],
                'is_active'    => $data['available_room_units'][2]['is_active'] == 1,
            ]
        ];

        $unitDeleted = [
            'id' => $data['available_room_units'][1]['room_unit_id'],
        ];

        $unit1 = [
            'floor'   => $data['available_room_units'][0]['floor_name'],
            'name'    => $data['available_room_units'][0]['room_name'],
            'is_active'    => $data['available_room_units'][0]['is_active'] == 1,
        ];

        $unit3 = [
            'floor'   => $data['available_room_units'][2]['floor_name'],
            'name'    => $data['available_room_units'][2]['room_name'],
            'is_active'    => $data['available_room_units'][2]['is_active'] == 1,
        ];

        unset($result[0]['room_unit_id']);
        unset($result[1]['room_unit_id']);
        $this->assertEquals($result, $expectedResult);
        $this->assertDatabaseHas('designer_unit', $unit1);
        $this->assertDatabaseHas('designer_unit', $unit3);
        $this->assertDatabaseMissing('designer_unit', $unitDeleted);
    }

    public function testsaveBankAccount()
    {
        $user = factory(User::class)->create();

        $bankData = [
            'bank_name' => 'bank name',
            'bank_account_number' => '0000000000',
            'bank_account_name' => 'bank account name'
        ];

        $this->repo->saveBankAccount($user, $bankData);

        $expectedResult = [
            'user_id' => $user->id,
            'bank_name' => $bankData['bank_name'],
            'bank_account_number' => $bankData['bank_account_number'],
            'bank_account_owner' => $bankData['bank_account_name']
        ];

        $this->assertDatabaseHas('mamipay_owner_profile', $expectedResult);
    }

    public function testsavePriceWithRoomTypeId()
    {
        $room = factory(Room::class)->create();

        $options = [
            'room_type_id' => rand(1, 100),
        ];

        $priceData = [
            'type 1' => 1000000,
            'type 2' => 1500000
        ];

        $this->repo->savePrice($room, $priceData, $options);

        foreach ($priceData as $key => $value) {
            $expectedResult = [
                'reference_id' => $options['room_type_id'],
                'type' => $key,
                'nominal' => $value,
            ];

            $this->assertDatabaseHas('price', $expectedResult);
        }
    }

    public function testsavePriceWithoutRoomTypeId()
    {
        $room = factory(Room::class)->create();

        $options = [];

        $priceData = [
            'type 1' => 1000000,
            'type 2' => 1500000
        ];

        $this->repo->savePrice($room, $priceData, $options);

        foreach ($priceData as $key => $value) {
            $expectedResult = [
                'type' => $key,
                'nominal' => $value,
            ];

            $this->assertDatabaseHas('price', $expectedResult);
        }
    }

    public function testsaveAdditionalCost()
    {
        $room = factory(Room::class)->create();
        $roomTypeId = rand(1, 100);
        factory(PriceAdditional::class)->create([
            'designer_id' => $room->id,
            'type' => PriceAdditional::ADDITIONAL_COST,
            'reference' => Price::ROOM_TYPE_REFERENCE,
            'reference_id' => $roomTypeId,
        ]);
        $additionalCost = [
            'room_type_id' => $roomTypeId,
            'deposit_amount' => 100000,
            'down_payment' => 100000,
            'billing_date' => 100000,
            'fine_amount' => 100000,
            'due_date' => 100000,
            'price_remark' => 200000,
            'additional_cost' => [
                [
                    'price_name' => 'another additional cost',
                    'price_value' => 10000,
                ],
            ],
        ];

        $this->repo->saveAdditionalCost($room, $additionalCost);
    }

    public function testSaveRulePhotoNew()
    {
        $room = factory(Room::class)->create();
        $ruleData = [
            'rule' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
            'document' => [
                [
                    'id' => 432,
                    'file_id' => 123,
                    'type' => 'image',
                    'file_name' => 'image.jpg'
                ]
            ]
        ];

        $this->repo->saveRule($room, $ruleData);
        $room->loadMissing('room_term.room_term_document');
        $rtd = $room->room_term->room_term_document->first();

        $this->assertEquals($rtd->media_id, 123);
    }

    public function testSaveRuleDocumentNew()
    {
        $room = factory(Room::class)->create();
        $ruleData = [
            'rule' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
            'document' => [
                [
                    'file_id' => 321,
                    'type' => 'document',
                    'file_name' => 'pdf file.pdf'
                ]
            ]
        ];

        $this->repo->saveRule($room, $ruleData);
        $room->loadMissing('room_term.room_term_document');
        $rtd = $room->room_term->room_term_document->first();

        $this->assertEquals($rtd->document_id, 321);
    }

    public function testSaveRulePhotoReplace()
    {
        $room = factory(Room::class)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id,
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => 456,
        ]);

        $ruleData = [
            'rule' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
            'document' => [
                [
                    'file_id' => 123,
                    'type' => 'image',
                    'file_name' => 'image.jpg'
                ]
            ]
        ];

        $this->repo->saveRule($room, $ruleData);
        $room->loadMissing('room_term.room_term_document');
        $rtd = $room->room_term->room_term_document->first();

        $this->assertEquals($rtd->media_id, 123);
    }

    public function testSaveRuleWithEmptyDocumentData()
    {
        $room = factory(Room::class)->create();
        $roomTerm = factory(RoomTerm::class)->create([
            'designer_id' => $room->id,
        ]);
        factory(RoomTermDocument::class)->create([
            'designer_term_id' => $roomTerm->id,
            'media_id' => 456,
        ]);

        $ruleData = [
            'rule' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
            'document' => []
        ];

        $this->repo->saveRule($room, $ruleData);
        $room->loadMissing('room_term.room_term_document');
        $rtd = $room->room_term->room_term_document->first();

        $this->assertNull($rtd);
    }

    public function testAddRoomTypeFacilityPhotos()
    {
        $room = factory(Room::class)->create();
        $type = factory(Type::class)->create([
            'designer_id' => $room->id
        ]);
        $data = [
            'user_id' => 123,
            'facilities' => [
                [
                    'types' => [
                        [
                            'type_id' => 321,
                            'photos' => [145]
                        ]
                    ]
                ]
            ]
        ];

        $this->repo->addRoomTypeFacilityPhotos($type, $data);
        $type->loadMissing('facilities');
        
        $this->assertEquals(145, $type->facilities->first()->photo_id);
    }

    public function testDeleteRoomTypeFacilityPhotos()
    {
        $room = factory(Room::class)->create();
        $type = factory(Type::class)->create([
            'designer_id' => $room->id
        ]);
        $data = [
            'user_id' => 123,
            'facilities' => [
                [
                    'types' => [
                        [
                            'type_id' => 321,
                            'photos' => []
                        ]
                    ]
                ]
            ]
        ];

        $this->repo->addRoomTypeFacilityPhotos($type, $data);
        $type->loadMissing('facilities');
        
        $this->assertNull($type->facilities->first());
    }
}
