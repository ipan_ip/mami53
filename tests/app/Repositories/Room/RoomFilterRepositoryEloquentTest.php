<?php

namespace App\Repositories\Room;

use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class RoomFilterRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repo = app()->make(RoomFilterRepositoryEloquent::class);
    }

    public function testModelFunctionReturnRoom()
    {
        $this->assertEquals(Room::class, $this->repo->model());
    }

    public function testCountHomeListWithValidRoomAddCount()
    {
        $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->countHomeList();

        $this->assertEquals(1, $response);
    }

    public function testCountHomeListWithInvalidRoomGiveZeroCount()
    {
        // the month price should have value
        $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 0
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->countHomeList();

        $this->assertEquals(0, $response);
    }

    public function testCountItemListWithValidRoomAddCount()
    {
        $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->countItemList();

        $this->assertEquals(1, $response);
    }

    public function testCountItemListWithInvalidRoomGiveZeroCount()
    {
        // the month price should have value
       $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 0
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->countItemList();

        $this->assertEquals(0, $response);
    }

    public function testGetDataCountWithValidRoomAddCount()
    {
        $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 1000000
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->getDataCount();

        $this->assertEquals(1, $response);
    }

    public function testGetDataCountWithInvalidRoomGiveZeroCount()
    {
        // the month price should have value
        $room = factory(Room::class)->create(
            [
                "name" => "Kos Juragan 123",
                "location_id" => 52595569,
                "latitude" => -7.772256,
                "longitude" => 110.370937,
                "is_active" => 'true',
                "expired_phone" => 0,
                "slug" => "kos-juragan-123",
                "gender" => 1,
                "is_mamirooms" => 1,
                "room_available" => 10,
                "status" => 1,
                "price_monthly" => 0
            ]
        );

        $this->generatePriceFilter($room);

        $response = $this->repo->getDataCount();

        $this->assertEquals(0, $response);
    }

    private function generatePriceFilter($room)
    {
        return factory(PriceFilter::class)->create(
            [
                'designer_id' => $room->id,
                'final_price_monthly' => $room->price_monthly
            ]
        );
    }

}
