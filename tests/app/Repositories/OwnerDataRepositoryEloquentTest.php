<?php

namespace App\Repositories;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Booking\BookingOwner;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Level\KostLevelMap;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Premium\AdHistory;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\Card;
use App\Entities\Media\Media;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\Payment;
use App\Entities\Promoted\ViewPromote;
use App\Entities\User\Notification;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Level\KostLevel;
use App\Entities\Owner\Activity;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use stdClass;
use Faker\Factory as Faker;
use App\Entities\Owner\Loyalty;
use App\Entities\Point\PointUser;
use App\Entities\User\UserVerificationAccount;
use App\Repositories\OwnerDataRepositoryEloquent;
use TypeError;
use App\Enums\Premium\PremiumRequestStatus;
use Config;

/**
 * @property OwnerDataRepositoryEloquent $repository
 */
class OwnerDataRepositoryEloquentTest extends MamiKosTestCase
{

    const KAMAR_KEYWORD = ['kamar ', 'Kamar ', 'kamar', 'Kamar'];
    private $repository;
    private $premiumPackage;

    protected function createRecordForGetListTenants(
        Room $room,
        MamipayTenant $tenant,
        int $amount,
        string $statusPayment,
        $contactRoomNumber = null,
        $number = null,
        $floor = null
    ) {
        $kamarKeyword = self::KAMAR_KEYWORD[array_rand(self::KAMAR_KEYWORD)];

        $contract = factory(MamipayContract::class)->create([
            'tenant_id' => $tenant->id
        ]);
        $bookingUser = factory(BookingUser::class)->create([
            'contract_id' => $contract->id
        ]);
        factory(BookingUserRoom::class)->create([
            'booking_user_id' => $bookingUser->id,
            'number' => $kamarKeyword.$number,
            'floor' => $floor
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-01',
            'transfer_amount' => $amount,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED,
            'transfer_reference' => $statusPayment
        ]);
        factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract->id,
            'room_number' => is_null($contactRoomNumber) ? '' : $kamarKeyword.$contactRoomNumber
        ]);
    }

    public function testGetListPremiumExpensesSpecificKost_Success()
    {
        $length = 5;
        $media = factory(Media::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => $media->id
        ]);
        factory(Card::class, 3)->create([
            'designer_id' => $room->id,
            'photo_id' => $media->id,
            'description' => $this->faker->slug.'-'.ImageFacebook::TYPE_DALAM_KAMAR.'-cover'
        ]);

        for ($x=0; $x<$length; $x++) {
            factory(AdHistory::class)->create([
                'designer_id' => $room->id,
                'total' => mt_rand(11111, 999999),
                'created_at' => '2020-05-01 00:00:00',
                'updated_at' => '2020-05-02 00:00:00'
            ]);
        }

        $result = $this->repository->getListPremiumExpensesSpecificKost($room->song_id, 0, $length);

        $this->assertEquals(count($result), $length);
    }

    public function testGetListPremiumExpensesSpecificKost_Fail()
    {
        $result = $this->repository->getListPremiumExpensesSpecificKost(mt_rand(11111, 999999), 0, 4);

        $this->assertEquals(count($result), 0);
    }

    public function testGetListTenants_SuccessWithMamipayMedia()
    {
        $length = 3;
        $room = factory(Room::class)->create();
        $tenantPhotoId_1 = factory(MamipayMedia::class)->create();
        $mamipayTenant_1 = factory(MamipayTenant::class)->create([
            'photo_id' => $tenantPhotoId_1
        ]);
        $tenantPhotoId_2 = factory(MamipayMedia::class)->create();
        $mamipayTenant_2 = factory(MamipayTenant::class)->create([
            'photo_id' => $tenantPhotoId_2
        ]);

        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', 'sasa', 'number', 'floor');
        $this->createRecordForGetListTenants($room, $mamipayTenant_1, 500000, 'not_in_mamipay', 'lala', null, null);
        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', null, null, null);

        $result = $this->repository->getListTenants($room->song_id, 0, $length);

        $this->assertEquals(3, count($result));
        $this->assertEquals(
            array_keys(reset($result)),
            [
                "title",
                "total_amount",
                "type",
                "tenant_name",
                "tenant_photo",
                "room_name"
            ]
        );
    }

    public function testGetListTenants_SuccessWithUserMedia()
    {
        $length = 3;
        $room = factory(Room::class)->create();
        $user1 = factory(User::class)->create();
        $mamipayTenant_1 = factory(MamipayTenant::class)->create([
            'user_id' => $user1
        ]);
        $user2 = factory(User::class)->create();
        $mamipayTenant_2 = factory(MamipayTenant::class)->create([
            'user_id' => $user2
        ]);

        $this->createRecordForGetListTenants($room, $mamipayTenant_1, 500000, 'not_in_mamipay', 'lala', null, null);
        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', 'sasa', 'number', 'floor');
        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', '', null, null);

        $result = $this->repository->getListTenants($room->song_id, 0, $length);

        $this->assertEquals(3, count($result));
        $this->assertEquals(
            array_keys(reset($result)),
            [
                "title",
                "total_amount",
                "type",
                "tenant_name",
                "tenant_photo",
                "room_name"
            ]
        );
    }

    public function testGetListTenants_SuccessWithEmptyMedia()
    {
        $length = 3;
        $room = factory(Room::class)->create();
        $mamipayTenant_1 = factory(MamipayTenant::class)->create();
        $mamipayTenant_2 = factory(MamipayTenant::class)->create();

        $this->createRecordForGetListTenants($room, $mamipayTenant_1, 500000, 'not_in_mamipay', 'lala', null, null);
        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', '', 'number', 'floor');
        $this->createRecordForGetListTenants($room, $mamipayTenant_2, 750000, '8291849 (flip)', '', null, null);

        $result = $this->repository->getListTenants($room->song_id, 0, $length);

        $this->assertEquals(3, count($result));
        $this->assertEquals(
            array_keys(reset($result)),
            [
                "title",
                "total_amount",
                "type",
                "tenant_name",
                "tenant_photo",
                "room_name"
            ]
        );
    }

    public function testGetListTenants_EmptyArray()
    {
        $result = $this->repository->getListTenants(mt_rand(111111, 9999999999), 0, 10);

        $this->assertEquals(0, count($result));
        $this->assertEquals($result, []);
    }

    public function testGetListTenants_Fail()
    {
        $room = factory(Room::class)->create();
        $result = $this->repository->getListTenants(mt_rand(1111111, 999999999), 0, 3);

        //mock expense
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);

        $result = $this->repository->getFinanceSpecificKost($room->song_id, 5, 2020);

        $this->assertEquals(array_keys($result), ['total_amount', 'total_expense', 'room']);
    }

    public function testGetFinanceSpecificKost_Success()
    {
        $room = factory(Room::class)->create();

        //mock income
        $contract = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayContractKost = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract->id
        ]);

        //mock expense
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);

        $result = $this->repository->getFinanceSpecificKost($room->song_id, 5, 2020);

        $this->assertEquals(array_keys($result), ['total_amount', 'total_expense', 'room']);
    }

    public function testGetFinanceSpecificKostSemester_Success()
    {
        $room = factory(Room::class)->create();

        //mock income
        $contract = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayContractKost = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract->id
        ]);

        //mock expense
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);

        $result = $this->repository->getFinanceSpecificKostSemester($room->song_id, 1, 2020);

        $this->assertEquals(array_keys($result), ['total_amount', 'total_expense', 'room']);
    }

    public function testGetIncomeSpecificKostById_MonthYear()
    {
        $room = factory(Room::class)->create();
        $contract = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract->id,
            'scheduled_date' => '2020-05-02',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract->id
        ]);

        $result = $this->repository->getIncomeSpecificKostById($room->id, 5, 2020);
        $this->assertEquals($result, 1000000);
    }

    public function testGetIncomeSpecificKostById_Year()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostById($room->id, null, 2020);
        $this->assertEquals($result, 1500000);
    }

    public function testGetIncomeSpecificKostById_AllTime()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostById($room->id, null, null);
        $this->assertEquals($result, 2000000);
    }

    public function testGetIncomeSpecificKostById_ReturnNol()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostById($room->id, 7, 2020);
        $this->assertEquals($result, 0);
    }

    public function testGetIncomeSpecificKostByIdSemester_Ganjil()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostByIdSemester($room->id, 1, 2020);
        $this->assertEquals($result, 1000000);
    }

    public function testGetIncomeSpecificKostByIdSemester_Genap()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 750000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostByIdSemester($room->id, 2, 2020);
        $this->assertEquals($result, 750000);
    }

    public function testGetIncomeSpecificKostByIdSemester_SemesterNull()
    {
        $room = factory(Room::class)->create();
        $contract_1 = factory(MamipayContract::class)->create();
        $contract_2 = factory(MamipayContract::class)->create();
        $mamipayInvoice_1 = factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-06-01',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_2 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-01-03',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_3 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_1->id,
            'scheduled_date' => '2020-12-02',
            'transfer_amount' => 750000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        $mamipayInvoice_4 =factory(MamipayInvoice::class)->create([
            'contract_id' => $contract_2->id,
            'scheduled_date' => '2019-12-31',
            'transfer_amount' => 500000,
            'status' => MamipayInvoice::STATUS_PAID,
            'transfer_status' => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);
        
        $mamipayContractKost_1 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_1->id
        ]);
        $mamipayContractKost_2 = factory(MamipayContractKost::class)->create([
            'designer_id' => $room->id,
            'contract_id' => $contract_2->id
        ]);

        $result = $this->repository->getIncomeSpecificKostByIdSemester($room->id, 3, 2020);
        $this->assertEquals($result, 2250000);
    }

    public function testGetExpensePremiumSpecificKostById_MonthYear()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-05-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostById($room->id, 5, 2020);
        $this->assertEquals($result, 25000);
    }

    public function testGetExpensePremiumSpecificKostById_Year()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-05-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostById($room->id, null, 2020);
        $this->assertEquals($result, 35000);
    }

    public function testGetExpensePremiumSpecificKostById_AllTime()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2018-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2010-05-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostById($room->id, null, null);
        $this->assertEquals($result, 35000);
    }

    public function testGetExpensePremiumSpecificKostByIdSemester_Ganjil()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2010-05-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostByIdSemester($room->id, 1, 2020);
        $this->assertEquals($result, 25000);
    }

    public function testGetExpensePremiumSpecificKostByIdSemester_Genap()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 1000,
            'created_at' => '2010-12-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostByIdSemester($room->id, 2, 2010);
        $this->assertEquals($result, 1000);
    }

    public function testGetExpensePremiumSpecificKostByIdSemester_ReturnNol()
    {
        $room = factory(Room::class)->create();
        $adHistory_1 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 15000,
            'created_at' => '2020-05-01 00:00:00'
        ]);
        $adHistory_2 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 10000,
            'created_at' => '2020-04-01 00:00:00'
        ]);
        $adHistory_3 = factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total' => 1000,
            'created_at' => '2010-12-30 00:00:00'
        ]);

        $result = $this->repository->getExpensePremiumSpecificKostByIdSemester($room->id, 7, 2010);
        $this->assertEquals($result, 0);
    }

    public function testVerifyKostWithItsOwnerBySongId_True()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id
        ]);

        $result = $this->repository->verifyKostWithItsOwnerBySongId($owner->id, $room->song_id);
        $this->assertTrue($result);
    }

    public function testVerifyKostWithItsOwnerBySongId_False()
    {
        $owner = factory(User::class)->states('owner')->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id
        ]);

        $result = $this->repository->verifyKostWithItsOwnerBySongId(mt_rand(111111, 999999), $room->song_id);
        $this->assertFalse($result);
    }

    public function testGetTotalPremiumExpense_UserNotFound()
    {
        $result = $this->repository->getTotalPremiumExpense(mt_rand(1111111, 999999999));
        $this->assertEquals(0, $result);
    }

    public function testGetTotalPremiumExpense_UserNotFoundWithMonthYear()
    {
        $result = $this->repository->getTotalPremiumExpense(mt_rand(1111111, 999999999), 5, 2020);
        $this->assertEquals(0, $result);
    }

    protected function createPremiumRequest(
        int $userId,
        string $status,
        string $updatedDate,
        int $used
    ) {
        return factory(PremiumRequest::class)->create([
            'status' => $status,
            'expired_date' => null,
            'expired_status' => null,
            'user_id' => $userId,
            'updated_at' => $updatedDate,
            'used' => $used,
            'premium_package_id' => factory(PremiumPackage::class)->create([
                'for' => 'trial',
            ]),
            'deleted_at' => null,
            'auto_upgrade' => null
        ]);
    }

    protected function createViewPromote(
        int $premiumRequestId,
        int $used
    ) {
        return factory(ViewPromote::class)->create([
            'premium_request_id' => $premiumRequestId,
            'used' => $used,
        ]);
    }

    public function testGetTotalPremiumExpense_AllTime()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class, 3)->create()->map(function ($item) use ($user) {
            factory(AdHistory::class)->create([
                'designer_id' => $item->id,
                'total' => 500000,
                'created_at' => '2020-05-01 00:00:00',
                'updated_at' => '2020-05-02 00:00:00'
            ]);

            factory(RoomOwner::class)->create([ 
                "designer_id" => $item->id, 
                "user_id" => $user->id
            ]);
        });

        $result = $this->repository->getTotalPremiumExpense($user->id);
        $this->assertSame(1500000, $result);
    }

    public function testGetTotalPremiumExpense_MonthYear()
    {
        $user = factory(User::class)->create();

        $room = factory(Room::class, 5)->create()->map(function ($item) use ($user) {
            factory(AdHistory::class)->create([
                'designer_id' => $item->id,
                'total' => 250000,
                'created_at' => '2020-05-01 00:00:00',
                'updated_at' => '2020-05-02 00:00:00'
            ]);

            factory(RoomOwner::class)->create([ 
                "designer_id" => $item->id, 
                "user_id" => $user->id
            ]);
        });

        $result = $this->repository->getTotalPremiumExpense($user->id, 5, 2020);
        $this->assertSame(1250000, $result);
    }

    public function testGetTotalPremiumExpense_Year()
    {
        $user = factory(User::class)->create();

        $room = factory(Room::class, 5)->create()->map(function ($item) use ($user) {
            factory(AdHistory::class)->create([
                'designer_id' => $item->id,
                'total' => 100000,
                'created_at' => '2010-05-01 00:00:00',
                'updated_at' => '2010-05-02 00:00:00'
            ]);

            factory(RoomOwner::class)->create([ 
                "designer_id" => $item->id, 
                "user_id" => $user->id
            ]);
        });

        $result = $this->repository->getTotalPremiumExpense($user->id, null, 2010);
        $this->assertSame(500000, $result);
    }

    public function testGetTotalExpenseSemester_UserNotFound()
    {
        $result = $this->repository->getTotalPremiumExpenseSemester(mt_rand(111111111, 999999999999), 1, 2020);
        $this->assertEquals(0, $result);
    }

    public function testGetTotalExpenseSemester_Ganjil()
    {
        $user = factory(User::class)->create();

        $room = factory(Room::class, 5)->create()->map(function ($item) use ($user) {
            factory(AdHistory::class)->create([
                'designer_id' => $item->id,
                'total' => 300000,
                'created_at' => '2020-05-01 00:00:00',
                'updated_at' => '2020-05-02 00:00:00'
            ]);

            factory(RoomOwner::class)->create([ 
                "designer_id" => $item->id, 
                "user_id" => $user->id
            ]);
        });

        $result = $this->repository->getTotalPremiumExpenseSemester($user->id, 1, 2020);
        $this->assertEquals(1500000, $result);
    }

    public function testGetTotalExpenseSemester_Genap()
    {
        $user = factory(User::class)->create();

        $room = factory(Room::class, 5)->create()->map(function ($item) use ($user) {
            factory(AdHistory::class)->create([
                'designer_id' => $item->id,
                'total' => 200000,
                'created_at' => '2020-08-01 00:00:00',
                'updated_at' => '2020-08-02 00:00:00'
            ]);

            factory(RoomOwner::class)->create([ 
                "designer_id" => $item->id, 
                "user_id" => $user->id
            ]);
        });

        $result = $this->repository->getTotalPremiumExpenseSemester($user->id, 2, 2020);
        $this->assertEquals(1000000, $result);
    }

    public function testGetTotalExpenseSemester_UnvalidSemester()
    {
        $user = factory(User::class)->create();

        $this->createPremiumRequest($user->id, '1', '2020-01-01 00:00:00', 1000000);
        $this->createPremiumRequest($user->id, '1', '2020-12-01 00:00:00', 1000000);
        $this->createPremiumRequest($user->id, '1', '2020-06-14 00:00:00', 500000);

        $result = $this->repository->getTotalPremiumExpenseSemester($user->id, 3, 2020);
        $this->assertEquals(0, $result);
    }

    public function testGetTotalIncomeSemester_UserNotFound()
    {
        $result = $this->repository->getTotalIncomeSemester(987654321, 1, 2020);
        $this->assertEquals(0, $result);
    }

    public function testGetTotalIncomeSemester_ZeroUserNotFoundAndWithMonthAndYear()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $result = $this->repository->getTotalIncomeSemester(987654321, 1, 2020);
        $this->assertEquals(0, $result);
    }

    public function testGetTotalIncomeSemester_ParamValidGanjil()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 1;
        $ownerId = 2;

        // dumping data to db testing
        $this->createMamipayContract($contractId, $ownerId);
        $this->createMamipayInvoice($contractId, 1000000, '2020-01-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-02-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-03-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-04-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-05-01');

        $result = $this->repository->getTotalIncomeSemester(2, 1, 2020);

        $this->assertEquals(5000000, $result);
    }

    public function testGetTotalIncomeSemester_ParamValidGenap()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 1;
        $ownerId = 2;

        // dumping data to db testing
        $this->createMamipayContract($contractId, $ownerId);
        $this->createMamipayInvoice($contractId, 1000000, '2020-01-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-02-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-03-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-04-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-12-01');

        $result = $this->repository->getTotalIncomeSemester(2, 2, 2020);

        $this->assertEquals(1000000, $result);
    }

    public function testGetTotalIncomeSemester_ParamSemesterUnvalid()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 1;
        $ownerId = 2;

        // dumping data to db testing
        $this->createMamipayContract($contractId, $ownerId);
        $this->createMamipayInvoice($contractId, 1000000, '2020-01-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-02-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-03-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-04-01');
        $this->createMamipayInvoice($contractId, 1000000, '2020-12-01');

        $result = $this->repository->getTotalIncomeSemester(2, 3, 2020);

        $this->assertEquals(0, $result);
    }

    public function testGetUserRoomOwner_Success()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $response = $this->repository->getUserRoomOwner($user->id, 0, 3);

        $this->assertInstanceOf(User::class, $response);
    }

    public function testGetFinanceListKosts_Success()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $owner = factory(User::class)->create([
            'date_owner_limit' => $expiredDate,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $owner->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $mamipayContract = factory(MamipayContract::class)->create([
            'owner_id'  => $owner->id
        ]);

        factory(MamipayInvoice::class)->create([
            'contract_id'        => (int) $mamipayContract->id,
            'status'             => MamipayInvoice::PAYMENT_STATUS_PAID,
            'amount'             => (int) 1000000,
            'scheduled_date'     => '2019-01-08',
            'transfer_amount'    => (int) 1000000,
            'total_paid_amount'  => (int) 1000000,
            'transfer_reference' => null,
            'transfer_status'    => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract->id,
            'designer_id' => $room->id,
        ]);

        $response = $this->repository->getFinanceListKosts($owner->id, 0, 3);

        $this->assertSame(
            array_keys(reset($response)),
            [
                'total_amount',
                'total_expense',
                'room'
            ]
        );
    }

    public function testSetRoomForFinanceListKosts_Success()
    {
        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        $response = $this->repository->setRoomForFinanceListKosts($room->id);

        $this->assertSame(
            array_keys($response),
            [
                'id',
                'name',
                'address',
                'gender',
                'room_available',
                'price_monthly',
                'image'
            ]
        );
    }

    public function testGetIncomeSpecificKostById_Zero()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $owner = factory(User::class)->create([
            'date_owner_limit' => $expiredDate,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        $mamipayContract = factory(MamipayContract::class)->create([
            'owner_id'  => $owner->id
        ]);

        factory(MamipayInvoice::class)->create([
            'contract_id'        => (int) $mamipayContract->id,
            'status'             => MamipayInvoice::PAYMENT_STATUS_PAID,
            'amount'             => (int) 1000000,
            'scheduled_date'     => '2019-01-08',
            'transfer_amount'    => (int) 1000000,
            'total_paid_amount'  => (int) 1000000,
            'transfer_reference' => null,
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract->id,
            'designer_id' => $room->id,
        ]);

        $response = $this->repository->getIncomeSpecificKostById($room->id);

        $this->assertEquals($response, 0);
    }

    public function testGetIncomeSpecificKostById_NotZero()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $owner = factory(User::class)->create([
            'date_owner_limit' => $expiredDate,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        $mamipayContract = factory(MamipayContract::class)->create([
            'owner_id'  => $owner->id
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id'        => (int) $mamipayContract->id,
            'status'             => MamipayInvoice::PAYMENT_STATUS_PAID,
            'amount'             => (int) 1000000,
            'scheduled_date'     => '2019-01-08',
            'transfer_amount'    => (int) 1000000,
            'total_paid_amount'  => (int) 1000000,
            'transfer_reference' => null,
            'transfer_status'    => MamipayInvoice::TRANSFER_STATUS_TRANSFERRED
        ]);

        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract->id,
            'designer_id' => $room->id,
        ]);

        $response = $this->repository->getIncomeSpecificKostById($room->id);

        $this->assertIsInt($response);
    }

    public function testGetFinanceKostFromName_Success()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $owner = factory(User::class)->create([
            'date_owner_limit' => $expiredDate,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'name' => 'xxx',
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $owner->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $response = $this->repository->getFinanceKostFromName($owner->id, $room->name);

        $this->assertInternalType('array', $response);
    }

    public function testGetFinanceKostFromName_EmptyArray()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $owner = factory(User::class)->create([
            'date_owner_limit' => $expiredDate,
            'is_owner' => 'true'
        ]);

        $room = factory(Room::class)->create([
            'name' => 'xxx',
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $owner->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $response = $this->repository->getFinanceKostFromName($owner->id, 'aaaa');

        $this->assertInternalType('array', $response);
        $this->assertSame([], $response);
    }
    
    public function getListTenants_Success()
    {
        $room = factory(Room::class)->create();

        $result = $this->repository->getListTenants($room->song_id, 0, 1);
        dd($result);
    }

    public function testUserIncomeIsZeroWhenUserNotFound()
    {
        $result = $this->repository->getTotalIncome(99999);
        $this->assertEquals(0, $result);
    }

    public function testUserIncomeIsZeroWhenUserNotFoundAndWithMonthAndYear()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $result = $this->repository->getTotalIncome(99999, 2, 2020);
        $this->assertEquals(0, $result);
    }

    public function testUserHasAllTimeIncomeWhenParamValid()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 1;
        $ownerId = 2;

        // dumping data to db testing
        $this->createMamipayContract($contractId, $ownerId);
        $this->createMamipayInvoice($contractId, 1400000);
        $this->createMamipayInvoice($contractId, 2000000);

        $result = $this->repository->getTotalIncome($ownerId);

        $this->assertEquals(3400000, $result);
    }

    public function testUserHasIncomeByMonthAndYearWhenParamValid()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 3;
        $ownerId = 4;

        // dumping data to db testing
        $this->createMamipayContract($contractId, $ownerId);
        $this->createMamipayInvoice($contractId, 2400000, '2020-01-01');
        $this->createMamipayInvoice($contractId, 4000000, '2020-01-01');
        $this->createMamipayInvoice($contractId, 2000000, '2019-05-05');

        $result = $this->repository->getTotalIncome($ownerId, 1, 2020);

        $this->assertEquals(6400000, $result);
    }

    public function testUserHasValidAllTimeIncome()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 10;
        $ownerId = 10;

        // dumping data
        $this->createMamipayContract($contractId, $ownerId);

        // createMamipayInvoice($contractId, $amount, $scheduleDate, $transferRef, $paidAmount, $tfAmount)
        $this->createMamipayInvoice($contractId, 1300000, '2020-03-15', '1484XXXX (flip)', 1250000, 1300000);
        $this->createMamipayInvoice($contractId, 1250000, '2020-03-13', 'not_in_mamipay', 1250000, 1250000);
        $this->createMamipayInvoice($contractId, 1170000, '2020-01-03', '1484XXXX (flip)', 1120000, 1170000);
        $this->createMamipayInvoice($contractId, 130000, '2019-12-21', '1484XXXX (flip)', 136500, 130000);

        $result = $this->repository->getTotalIncome($ownerId);

        $this->assertEquals(3850000, $result);
    }

    public function testUserHasValidMonthIncome()
    {
        $this->skipIfColomnOnTableNotExist('mamipay_contract_invoice', 'transfer_reference');

        $contractId = 11;
        $ownerId = 11;

        // dumping data
        $this->createMamipayContract($contractId, $ownerId);

        // createMamipayInvoice($contractId, $amount, $scheduleDate, $transferRef, $paidAmount, $tfAmount)
        $this->createMamipayInvoice($contractId, 1300000, '2020-03-15', '1484XXXX (flip)', 1250000, 1300000);
        $this->createMamipayInvoice($contractId, 1250000, '2020-03-13', 'not_in_mamipay', 1250000, 1250000);
        $this->createMamipayInvoice($contractId, 1170000, '2020-01-03', '1484XXXX (flip)', 1120000, 1170000);
        $this->createMamipayInvoice($contractId, 130000, '2019-12-21', '1484XXXX (flip)', 136500, 130000);

        $result = $this->repository->getTotalIncome($ownerId, 3, 2020);

        $this->assertEquals(2550000, $result);
    }

    public function testGetUserDataSuccess()
    {
        $user = factory(User::class)->create();
        Cache::shouldReceive('has')->with('showPopupDailyAllocation:'.$user->id)->andReturn(true)->once();
        Cache::shouldReceive('forget')->with('showPopupDailyAllocation:'.$user->id)->andReturn(true)->once();

        $result = $this->repository->userData($user);
        $this->assertEquals($user->id, $result['user_id']);
    }

    public function testGetUserDataPremiumStillActive()
    {
        $user = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(2)]);
        $result = $this->repository->userData($user);

        $this->assertFalse($result['show_popup_upgrade']);
    }

    public function testGetUserDataUnValidEmail()
    {
        $user = factory(User::class)->create(['email' => 'not-valid-email']);
        $result = $this->repository->userData($user);

        $this->assertFalse($result['valid_email']);
    }

    public function testCheckUserTrialUserCanTrialSuccess()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => null
        ]);

        factory(PremiumPackage::class)->create([
            "for" => 'trial',
            'is_active' => '1',
            'start_date' => date('Y-m-d', strtotime('-7 days')),
            'sale_limit_date' => date('Y-m-d', strtotime('+60 days')),
        ]);

        $result = $this->repository->checkUserTrial($user);
        $this->assertTrue($result['can_trial']);
    }

    public function testCheckUserTrialUserCanTrialPeringatan()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => null
        ]);

        $this->createRoom($user->id, RoomOwner::ROOM_UNVERIFY_STATUS, 3, 10);
        $this->createRoom($user->id, RoomOwner::ROOM_UNVERIFY_STATUS, 6, 13);

        factory(PremiumPackage::class)->create([
            "for" => 'trial',
            'is_active' => '1',
            'start_date' => Carbon::now()->subDays(7),
            'sale_limit_date' => Carbon::now()->addDays(60),
        ]);

        $user->load(['room_owner_verified', 'room_owner']);
        $user->room_owner_verified_count = count($user->room_owner_verified);
        $user->room_owner_count = count($user->room_owner);
        $user->owners = collect([]);

        $result = $this->repository->checkUserTrial($user);
        $this->assertTrue($result['can_trial']);
        $this->assertSame('Peringatan', $result['popup_can_trial_text']['title']);
    }

    public function testCheckUserTrialUserCanNotTrialSuccess()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);

        $result = $this->repository->checkUserTrial($user);
        $this->assertFalse($result['can_trial']);
    }

    public function testCheckUserTrialUserDontHaveVerifiedRoom()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);

        factory(PremiumPackage::class)->create([
            "for" => 'trial',
            'is_active' => '1',
            'start_date' => date('Y-m-d', strtotime('-7 days')),
            'sale_limit_date' => date('Y-m-d', strtotime('+60 days')),
        ]);

        $user->load(['room_owner_verified', 'room_owner']);
        $user->room_owner_verified_count = count($user->room_owner_verified);
        $user->room_owner_count = count($user->room_owner);

        $result = $this->repository->checkUserTrial($user);
        $this->assertFalse($result['can_trial']);
        $this->assertTrue($result['popup_can_trial']);
        $this->assertArrayHasKey('title', $result['popup_can_trial_text']);
    }

    public function testCheckUserTrialUserHaveApartment()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);

        $this->createRoom($user->id, RoomOwner::ROOM_UNVERIFY_STATUS, 3, 10);
        $this->createRoom($user->id, RoomOwner::ROOM_UNVERIFY_STATUS, 6, 13);

        factory(PremiumPackage::class)->create([
            "for" => 'trial',
            'is_active' => '1',
            'start_date' => date('Y-m-d', strtotime('-7 days')),
            'sale_limit_date' => date('Y-m-d', strtotime('+60 days')),
        ]);

        $user->load(['room_owner_verified', 'room_owner']);
        $user->room_owner_verified_count = count($user->room_owner_verified);
        $user->room_owner_count = count($user->room_owner);

        $result = $this->repository->checkUserTrial($user);
        $this->assertFalse($result['can_trial']);
        $this->assertTrue($result['popup_can_trial']);
        $this->assertArrayHasKey('title', $result['popup_can_trial_text']);
    }

    public function testGetFirstDataWithInvalidRoom()
    {
        $request = [
            'room_id' => rand(1,100)
        ];
        $result = $this->repository->getFirstData($request);
        $this->assertNull($result);
    }

    public function testGetFirstDataForRoom()
    {
        $room = factory(Room::class)->create();
        $request = [
            'room_id' => $room->song_id
        ];
        $result = $this->repository->getFirstData($request);
        $keys = [
                "_id","apartment_project_id","price_title_time","price_title_time_usd",
                "price_type","price_title","room_title","address","share_url",
                "has_round_photo", "gender", "status", "status-title", "available_room",
                "status_kost", "incomplete", "min_month", "photo_url", "keyword",
                "status_kos", "owner_phone", "is_booking", "rating", "review_count",
                "owner_phone_array", "survey_count", "chat_count", "love_count",
                "view_count", "click_count", "is_premium_owner", "label", "label_array",
                "is_promoted", "has_video", "promo_title", "click_price", "chat_price",
                "unit_type", "unit_type_rooms", "floor", "furnished_status", "size",
                "reject_remark", "not_updated", "price_daily", "price_weekly",
                "price_monthly", "price_yearly", "price_daily_usd", "price_weekly_usd",
                "price_monthly_usd", "price_yearly_usd", "price_3_month", "price_6_month",
                "unique_code", "input_progress", "level_info", "is_editable",
                "permissions"
            ];
        
        $this->assertEquals($result['_id'],$room->song_id);
        foreach($keys as $key){
            $this->assertArrayHasKey($key,$result);
        } 
    }

    public function testGetFirstDataForUpdate()
    {
        $room = factory(Room::class)->create();
        $request = [
            'room_id' => $room->song_id
        ];
        $result = $this->repository->getFirstData($request,'update');

        $keys = [
                "_id", "is_apartment", "price_type", "room_title", "price_daily",
                "price_weekly", "price_monthly", "price_yearly", "price_3_month",
                "price_6_month", "room_available", "size", "room_total",
                "inside_bathroom", "outside_bathroom", "with_listrik", "without_listrik",
                "photo_url", "gender", "min_month", "status", "status-title",
                "available_room", "warning_message", "is_price_flash_sale"
          ];
        
        $this->assertEquals($result['_id'],$room->song_id);
        foreach($keys as $key){
            $this->assertArrayHasKey($key,$result);
        } 
    }

    public function testGetTrialMembershipStatusSuccess()
    {
        $user = factory(\App\User::class)->create([
            "id" => 100000,
            "date_owner_limit" => null
        ]);

        factory(PremiumPackage::class)->create([
            "id" => 10,
            "for" => 'trial',
            'is_active' => '1',
            'start_date' => date('Y-m-d', strtotime('-7 days')),
            'sale_limit_date' => date('Y-m-d', strtotime('+60 days')),
        ]);

        $result = $this->repository->getMembership($user, 2);
        $this->assertEquals("Coba Trial", $result['status']);
    }

    public function testGetPaymentConfirmationMembershipStatusSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '0',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () {
                return factory(User::class)->create();
            }
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Konfirmasi Pembayaran", $result['status']);
    }

    public function testGetMembershipPaymentConfirmationWithPaymentMidtrans()
    {
        $expiredDate = Carbon::now()->addDay();

        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '0',
            'premium_package_id' => $this->premiumPackage->id,
            'expired_date' => null,
            'expired_status' => 'false',
            'user_id' => $user->id
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'expired_at' => $expiredDate,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING
        ]);

        $user->load('premium_request');

        $result = $this->repository->getMembership($user, 2);
        $this->assertEquals("Konfirmasi Pembayaran", $result['status']);
        $this->assertTrue($result['countdown'] > 0);
    }

    public function testGetMembershipPaymentConfirmationWithBSD()
    {
        $expiredDate = Carbon::now()->addDay();

        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '0',
            'premium_package_id' => $this->premiumPackage->id,
            'expired_date' => null,
            'expired_status' => 'false',
            'user_id' => $user->id
        ]);

        factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'expired_date' => null,
            'expired_status' => 'false',
            'user_id' => $user->id
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'expired_at' => $expiredDate,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING
        ]);

        $user->load('premium_request');

        $result = $this->repository->getMembership($user, 2);
        $this->assertEquals("Konfirmasi Pembayaran", $result['status']);
        $this->assertTrue($result['countdown'] > 0);
    }

    public function testGetProcessVerificationMembershipStatusSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '0',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () {
                return factory(User::class)->create();
            }
        ]);

        $premiumConfirmation = factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '0'
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals(PremiumRequestStatus::CONFIRMATION, $result['status']);
    }

    public function testGetMembershipStatusZeroView()
    {
        $expiredDate = date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 days'));

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'view' => 0,
            'user_id' => function () use ($expiredDate) {
                return factory(User::class)->create([
                    'date_owner_limit' => $expiredDate
                ]);
            }
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '1'
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Premium", $result['status']);
    }

    public function testGetPremiumMembershipStatusSuccess()
    {
        $expiredDate = date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 days'));

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () use ($expiredDate) {
                return factory(User::class)->create([
                    'date_owner_limit' => $expiredDate
                ]);
            }
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '1'
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Premium", $result['status']);
    }

    public function testGetMembershipStatusSuggestUpgrade()
    {
        $expiredDate = date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 days'));

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () use ($expiredDate) {
                return factory(User::class)->create([
                    'date_owner_limit' => $expiredDate
                ]);
            }
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '0'
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Premium", $result['status']);
    }

    public function testGetMembershipWithViewBalanceRequest()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => $user->id
        ]);

        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'price' => 8888,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => $balanceRequest->id,
            'is_confirm' => '1'
        ]);

        $room = factory(Room::class)->create([
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        factory(MamipayOwner::class)->create([
            'user_id' => $user->id,
            'status' => MamipayOwner::STATUS_APPROVED
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);

        $this->assertEquals("Premium", $result['status']);
        $this->assertEquals(8888, $result['balance_price_number']);

        $this->assertEquals(ChatAdmin::getDefaultAdminIdForBooking(), $result['cs_id']);
    }

    public function testGetMembershipAsMamiroomsOwner()
    {
        $expiredDate = Carbon::now()->addDays(1);
        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => $user->id
        ]);

        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'price' => 8888,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => $balanceRequest->id,
            'is_confirm' => '1'
        ]);

        $room = factory(Room::class)->create([
            'name' => 'TestAs mamirooms',
            'room_available' => 5,
            'room_count' => 2,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);

        $this->assertEquals("Premium", $result['status']);
        $this->assertEquals(8888, $result['balance_price_number']);

        $this->assertEquals(ChatAdmin::getDefaultAdminIdForBooking(), $result['cs_id']);
    }

    public function testGetDataMembershipWithViewBalanceRequest()
    {
        $expiredDate = Carbon::now()->addDays(4);
        $user = factory(User::class)->create([
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => $user->id
        ]);

        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'price' => 8888,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => $balanceRequest->id,
            'is_confirm' => '1'
        ]);

        $result = $this->repository->getDataMembership($user);

        $this->assertEquals("Premium", $result['status']);
        $this->assertEquals(8888, $result['balance_price_number']);
    }

    public function testGetMembershipWithViewPromoteActive()
    {
        $expiredDate = Carbon::now()->addDays(7);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () use ($expiredDate) {
                return factory(User::class)->create([
                    'date_owner_limit' => $expiredDate
                ]);
            }
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '1'
        ]);

        factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        $viewPromote = factory(ViewPromote::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'is_active' => 1
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Premium", $result['status']);
        $this->assertSame(
            $viewPromote->total - ($viewPromote->used + $viewPromote->history),
            $result['total_can_views']
        );
    }

    public function testGetMembershipWithViewPromoteInActive()
    {
        $expiredDate = Carbon::now()->addDays(7);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '1',
            'premium_package_id' => $this->premiumPackage->id,
            'user_id' => function () use ($expiredDate) {
                return factory(User::class)->create([
                    'date_owner_limit' => $expiredDate
                ]);
            }
        ]);

        factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'is_confirm' => '1'
        ]);

        factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        $viewPromote = factory(ViewPromote::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'is_active' => 0
        ]);

        $result = $this->repository->getMembership($premiumRequest->user, 2);
        $this->assertEquals("Premium", $result['status']);
        $this->assertSame(
            $viewPromote->total - $viewPromote->history,
            $result['total_can_views']
        );
    }

    protected function createMamipayInvoice(
        $contractId,
        $amount = 0,
        $scheduleDate = null,
        $tfRef = null,
        $paidAmount = 0,
        $tfAmount = 0
    ) {
        $paidAmount = ($paidAmount === 0) ? $amount : $paidAmount;
        $tfAmount = ($tfAmount === 0) ? $amount : $tfAmount;

        return factory(MamipayInvoice::class)->create([
            'contract_id'       => (int) $contractId,
            'status'            => MamipayInvoice::PAYMENT_STATUS_PAID,
            'amount'            => (int) $amount,
            'scheduled_date'    => $scheduleDate ? $scheduleDate : '2019-01-08',
            'transfer_amount'   => (int) $tfAmount,
            'total_paid_amount' => (int) $paidAmount,
            'transfer_reference' => $tfRef,
        ]);
    }

    protected function createMamipayContract($contractId, $ownerId)
    {
        return factory(MamipayContract::class)->create([
            'id'        => (int) $contractId,
            'owner_id'  => (int) $ownerId
        ]);
    }

    public function testTopUpRequestStatusExpiredStatusFalse()
    {
        $topup = factory(BalanceRequest::class)->create([
            'premium_request_id' => 1,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'expired_status' => 'false',
        ]);

        $result = $this->repository->topUpRequestStatus($topup);
        $this->assertEquals($topup->id, $result['id']);
        $this->assertEquals('add', $result['status']);
    }

    public function testTopUpRequestStatusExpiredStatusBalanceRequestSuccess()
    {
        $topup = factory(BalanceRequest::class)->create([
            'premium_request_id' => 1,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => BalanceRequest::TOPUP_SUCCESS,
            'expired_status' => 'false',
        ]);

        $result = $this->repository->topUpRequestStatus($topup);
        $this->assertEquals('verif', $result['status']);
    }

    public function testTopUpRequestStatusExpiredStatusBalanceRequestWaiting()
    {
        $topup = factory(BalanceRequest::class)->create([
            'premium_request_id' => 1,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => BalanceRequest::TOPUP_WAITING,
            'expired_status' => 'false',
        ]);

        $result = $this->repository->topUpRequestStatus($topup);
        $this->assertEquals('add', $result['status']);
    }

    public function testTopUpRequestStatusExpiredStatusBalanceRequestWaitingWithNoExpiredStatus()
    {
        $topup = factory(BalanceRequest::class)->create([
            'premium_request_id' => 1,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => BalanceRequest::TOPUP_WAITING,
            'expired_status' => null,
        ]);

        $result = $this->repository->topUpRequestStatus($topup);
        $this->assertEquals('waiting', $result['status']);
    }

    public function testTopUpRequestStatusVerif()
    {
        $topup = factory(BalanceRequest::class)->create([
            'id' => 1,
            'premium_request_id' => 1,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'expired_status' => false,
        ]);

        $result = $this->repository->topUpRequestStatus($topup);
        $this->assertEquals(1, $result['id']);
        $this->assertEquals('verif', $result['status']);
    }

    /**
     * Skip if colomn X on table Y doesnt exist coz it will make test fail
     *
     * @param string $tableName Y
     * @param string $colomnNdame X
     */
    private function skipIfColomnOnTableNotExist(string $tableName, string $colomnName)
    {
        if (!Schema::hasColumn($tableName, $colomnName)) {
            $this->markTestSkipped('Skipped due to colomn ' . $tableName . '.' . $colomnName . ' not exist');
        }

        return;
    }

    public function testProfileFunctionCallSignature()
    {
        $expiredDate = Carbon::now()->addDays(8);

        $user = factory(User::class)->create([
            'is_verify' => '1',
            'is_owner' => 'true',
            'date_owner_limit' => $expiredDate
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => '0',
            'premium_package_id' => $this->premiumPackage->id,
            'expired_date' => null,
            'expired_status' => 'false',
            'user_id' => $user->id
        ]);

        factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $this->premiumPackage->id,
            'status' => 0,
            'price' => 0,
            'expired_status' => 'false',
            'expired_date' => $expiredDate
        ]);

        $this->createRoom($user->id, RoomOwner::ROOM_UNVERIFY_STATUS, 3, 10);
        $this->createRoom($user->id, RoomOwner::ROOM_ADD_STATUS, 6, 13);
        $this->createRoom($user->id, RoomOwner::ROOM_EARLY_EDIT_STATUS, 9, 21);
        $this->createRoom($user->id, RoomOwner::ROOM_VERIFY_STATUS, 2, 7);
        $this->createRoom($user->id, RoomOwner::ROOM_VERIFY_STATUS, 5, 11);

        $result = $this->repository->profile($user->id, null);

        $this->assertEquals($this->profileFunctionExpectedContracts(), array_keys($result));
    }

    public function testGetProfileDataRoomCountAndRoomAvailabilitySuccess()
    {
        $user = factory(User::class)->create([
            'is_verify' => '1',
            'is_owner' => 'true',
        ]);

        $this->createRoom($user->id, 'unverified', 3, 10);
        $this->createRoom($user->id, 'add', 6, 13);
        $this->createRoom($user->id, 'draft', 9, 21);
        $this->createRoom($user->id, 'verified', 2, 7);
        $this->createRoom($user->id, 'verified', 5, 11);

        $result = $this->repository->profile($user->id, null);

        $this->assertEquals(2, $result['room_count']);
        $this->assertEquals(3, $result['room_count_unverified']);
        $this->assertEquals(7, $result['room_availability']['available']);
        $this->assertEquals(11, $result['room_availability']['filled']);
        $this->assertEquals(18, $result['room_availability']['total']);
    }

    public function testGetProfileDataHasLoyaltyData()
    {
        $user = factory(User::class)->state('owner')->create();

        factory(Loyalty::class)->create([
            'owner_phone_number' => $user->phone_number,
        ]);

        $result = $this->repository->profile($user->id, null);

        $this->assertNotNull($result['loyalty']);
    }

    public function testGetProfileDataHasPointDataBlacklisted()
    {
        $user = factory(User::class)->state('owner')->create();

        factory(PointUser::class)->create([
            'user_id' => $user,
            'is_blacklisted' => true,
        ]);

        $result = $this->repository->profile($user->id, null);

        $this->assertTrue($result['is_blacklist_mamipoin']);
    }

    public function testCalculateUserProfileBadgeCounterNullVerificationAccount()
    {
        $result = $this->executeCalculateUserProfileCounter(null);

        $this->assertEquals(1, $result);
    }

    public function testCalculateUserProfileBadgeCounterFullyVerifiedAccount()
    {
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED,
        ]);
        $result = $this->executeCalculateUserProfileCounter($userVerificationAccount);

        $this->assertEquals(0, $result);
    }

    public function testCalculateUserProfileBadgeCounterWaitingVerifiedAccount()
    {
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING,
        ]);
        $result = $this->executeCalculateUserProfileCounter($userVerificationAccount);

        $this->assertEquals(0, $result);

        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
        ]);
        $result = $this->executeCalculateUserProfileCounter($userVerificationAccount);

        $this->assertEquals(1, $result);
    }

    public function testCalculateUserProfileBadgeCounterRejectedVerifiedAccount()
    {
        /** @var UserVerificationAccount */
        $userVerificationAccount = factory(UserVerificationAccount::class)->make([
            'identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED,
        ]);
        $result = $this->executeCalculateUserProfileCounter($userVerificationAccount);

        $this->assertEquals(1, $result);
    }

    public function testGetProfileBadgeCounterNullOwner()
    {
        $this->expectException(TypeError::class);
        $result = $this->executeGetProfileBadgeCounter(null);
        $this->assertTrue(false); // should not reach here
    }

    public function testGetProfileBadgeCounterOwnerWithoutMamipayProfile()
    {
        /** @var User */
        $owner = factory(User::class)->state('owner')->make();
        $result = $this->executeGetProfileBadgeCounter($owner);

        $expected = [
            'user' => 1,
            'payment' => 1,
        ];

        $this->assertArray($expected, $result);
    }

    public function testGetProfileBadgeCounterOwnerWithMamipayProfileButMissingBankAccountInfo()
    {
        /** @var User */
        $owner = factory(User::class)->state('owner')->create();
        factory(MamipayOwner::class)->make([
            'user_id' => $owner,
            'bank_account_city' => null,
        ]);
        $result = $this->executeGetProfileBadgeCounter($owner);

        $expected = [
            'user' => 1,
            'payment' => 1,
        ];

        $this->assertArray($expected, $result);
    }

    public function testGetProfileBadgeCounterOwnerWithMamipayProfileCompleteBankAccountInfo()
    {
        /** @var User */
        $owner = factory(User::class)->state('owner')->create();
        factory(MamipayOwner::class)->create([
            'user_id' => $owner,
            'bank_account_city' => str_random(),
            'bank_account_number' => str_random(),
            'bank_account_owner' => str_random(),
        ]);
        $result = $this->executeGetProfileBadgeCounter($owner);

        $expected = [
            'user' => 1,
            'payment' => 0,
        ];

        $this->assertArray($expected, $result);
    }

    public function testGetRoomAvailabilityEmptyCollection()
    {
        $result = $this->repository->getRoomAvailability(collect([]));

        $this->assertEquals(0, $result['available']);
        $this->assertEquals(0, $result['filled']);
        $this->assertEquals(0, $result['total']);
    }

    public function testGetRoomAvailabilityNull()
    {
        $result = $this->repository->getRoomAvailability(null);

        $this->assertEquals(0, $result['available']);
        $this->assertEquals(0, $result['filled']);
        $this->assertEquals(0, $result['total']);
    }

    public function testGetRoomAvailabilitySuccess()
    {
        $user = factory(User::class)->create([
            'is_verify' => '1',
            'is_owner' => 'true',
        ]);

        $this->createRoom($user->id, RoomOwner::ROOM_VERIFY_STATUS, 3, 10);
        $this->createRoom($user->id, RoomOwner::ROOM_VERIFY_STATUS, 6, 13);
        $this->createRoom($user->id, RoomOwner::ROOM_VERIFY_STATUS, 9, 21);

        $userData = User::with([
            'room_owner_verified' => function ($query) {
                $query->with(['room' => function ($query) {
                    $query->with(['booking_designers' => function ($query) {
                        $query->withCount([
                            'booking_user as booking_total_count' => function ($query) {
                                $query->checkinFromToday()->notExpired()->newRequest();
                            },
                            'booking_user as booking_paid_count' => function ($query) {
                                $query->checkinFromToday()->notExpired()->paid();
                            },
                        ]);
                    }]);
                }]);
            }
        ])
            ->withCount([
                'room_owner_verified'
            ])
            ->where('is_owner', 'true')
            ->find($user->id);

        $result = $this->repository->getRoomAvailability($userData->room_owner_verified);

        $this->assertEquals(18, $result['available']);
        $this->assertEquals(26, $result['filled']);
        $this->assertEquals(44, $result['total']);
    }

    public function testCalculateBookingReportNullParam()
    {
        $publicCalculateBookingReport = $this->getNonPublicMethodFromClass(
            OwnerDataRepositoryEloquent::class,
            'calculateBookingReport'
        );
        $result = $publicCalculateBookingReport->invokeArgs(
            $this->repository,
            [null]
        );

        $this->assertEquals(0, $result['total']);
        $this->assertEquals(0, $result['paid']);
    }

    public function testCalculateBookingReportZeroVerified()
    {
        $publicCalculateBookingReport = $this->getNonPublicMethodFromClass(
            OwnerDataRepositoryEloquent::class,
            'calculateBookingReport'
        );
        $result = $publicCalculateBookingReport->invokeArgs(
            $this->repository,
            [collect([])]
        );

        $this->assertEquals(0, $result['total']);
        $this->assertEquals(0, $result['paid']);
    }

    public function testcalculateBookingReportSuccess()
    {
        $expectedTotalBooking = 0;
        $expectedTotalPaidBooking = 0;

        $bookingMockGenerator = static function () use (&$expectedTotalBooking, &$expectedTotalPaidBooking): stdClass {
            $totalBooking = rand(4, 9);
            $expectedTotalBooking += $totalBooking;
            $totalPaidBooking = $totalBooking - rand(0, $totalBooking);
            $expectedTotalPaidBooking += $totalPaidBooking;

            return (object) [
                'booking_total_count' => $totalBooking,
                'booking_paid_count' => $totalPaidBooking
            ];
        };
        $bookingDesignersMockGenerator = static function () use ($bookingMockGenerator): Collection {
            return collect(
                array_map(
                    static function () use ($bookingMockGenerator) {
                        return $bookingMockGenerator();
                    },
                    range(0, rand(2, 9))
                )
            );
        };

        $roomMockGenerator = static function () use ($bookingDesignersMockGenerator): stdClass {
            return (object) [
                'room' => (object) [
                    'booking_designers' => $bookingDesignersMockGenerator()
                ]
            ];
        };

        $roomOwnersMockGenerator = function () use ($roomMockGenerator): Collection {
            return collect(
                array_map(
                    static function () use ($roomMockGenerator) {
                        return $roomMockGenerator();
                    },
                    range(0, rand(2, 9))
                )
            );
        };
        $mockVerifiedRooms = $roomOwnersMockGenerator();

        $publicCalculateBookingReport = $this->getNonPublicMethodFromClass(
            OwnerDataRepositoryEloquent::class,
            'calculateBookingReport'
        );

        $result = $publicCalculateBookingReport->invokeArgs(
            $this->repository,
            [$mockVerifiedRooms]
        );


        $this->assertEquals($expectedTotalBooking, $result['total']);
        $this->assertEquals($expectedTotalPaidBooking, $result['paid']);
    }

    public function testGetInstantBookingSuccess()
    {
        $rentCounts = ['weekly', 'monthly'];
        $additionalPrices = [[
            'is_checked' => false,
            'is_enable_update' => true,
            'key' => 'air',
            'value' => 500,
        ]];

        $user = factory(User::class)->create();
        factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'rent_counts' => serialize($rentCounts),
            'additional_prices' => serialize($additionalPrices),
            'is_instant_booking' => false
        ]);

        $user->load('booking_owner');

        $result = $this->repository->getInstantBooking($user);

        $this->assertTrue($result['is_active']);
        $this->assertFalse($result['is_instant_booking']);
        $this->assertSame($rentCounts, $result['rent_counts']);
        $this->assertSame($additionalPrices, $result['additional_prices']);
    }

    public function testPopupAllocatedNoCacheReturnNull()
    {
        $roomEntity = factory(Room::class)->create();
        Cache::shouldReceive('has')->andReturn(false);
        $this->assertNull($this->repository->popupAllocated($roomEntity->song_id));
    }

    public function testPopupAllocatedWithCacheReturnObject()
    {
        $roomEntity = factory(Room::class)->create();
        Cache::shouldReceive('has')->with('popupallocated:' . $roomEntity->song_id)->andReturn(true);
        $result = (array) $this->repository->popupAllocated($roomEntity->song_id);
        $this->assertArrayHasKey('title', $result);
        $this->assertArrayHasKey('content', $result);
    }

    public function testAllocatePopupDeprecatedReturnNull()
    {
        $this->assertNull($this->repository->allocatePopup());
    }

    public function testSurveyDataNoPremium()
    {
        $userEntity = factory(User::class)->create();

        $this->assertNull($this->repository->surveyData($userEntity, ['expired' => false]));
    }

    public function testSurveyDataWithExpiredPremium()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->subDay(2)]);

        $this->assertNull(
            $this->repository->surveyData(
                $userEntity,
                ['expired' => true]
            )
        );
    }

    public function testSurveyDataWithActivePremiumAnySurvey()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(12)]);
        factory(SurveySatisfaction::class, 4)->create([
            'for' => 'premium_expired',
            'identifier' => 888,
            'user_id' => $userEntity->id
        ]);
        $this->assertNull(
            $this->repository->surveyData(
                $userEntity,
                [
                    'expired' => true,
                    'active_package' => 888
                ]
            )
        );
    }

    public function testSurveyDataWithActivePremiumMembershipKonfirmasiPembayaran()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(12)]);
        $this->assertNull(
            $this->repository->surveyData(
                $userEntity,
                [
                    'expired' => true,
                    'status' => "Konfirmasi Pembayaran",
                    'active_package' => 0
                ]
            )
        );
    }

    public function testSurveyDataWithActivePremiumMembershipProsesVerifikasi()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(12)]);
        $this->assertNull(
            $this->repository->surveyData(
                $userEntity,
                [
                    'expired' => true,
                    'status' => "Proses Verifikasi",
                    'active_package' => 0
                ]
            )
        );
    }

    public function testSurveyDataWithActiveReturnPremiumSurvey()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(12)]);

        $this->assertSame(
            SurveySatisfaction::SURVEY_PREMIUM,
            $this->repository->surveyData(
                $userEntity,
                [
                    'expired' => true,
                    'status' => 'definitely-not-the-status-in-if-selections',
                    'active_package' => 0
                ]
            )
        );
    }

    public function testSurveyDataWithActiveAndDeviceTrackedPlatformReturnPremiumSurvey()
    {
        $userEntity = factory(User::class)->create(['date_owner_limit' => Carbon::now()->addDay(12)]);

        $this->assertSame(
            SurveySatisfaction::SURVEY_PREMIUM,
            $this->repository->surveyData(
                $userEntity,
                [
                    'expired' => true,
                    'status' => 'definitely-not-the-status-in-if-selections',
                    'active_package' => 0
                ]
            )
        );
    }

    public function testSurveyPremiumOwnerHaveSurvey()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);

        factory(SurveySatisfaction::class, 4)->create([
            'for' => 'premium_expired',
            'identifier' => 888,
            'user_id' => $user->id
        ]);


        $result = $this->repository->surveyPremiumOwner(
            $user,
            [
                'data_for' => 'premium_expired',
                'premium_id' => 888,
                'reason' => [
                    'not',
                    'selected',
                    'in_array'
                ]
            ]
        );

        $this->assertTrue($result['status']);
        $this->assertFalse($result['room_survey']);
    }

    public function testSurveyPremiumOwnerDontHaveSurvey()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d')
        ]);

        $result = $this->repository->surveyPremiumOwner(
            $user,
            [
                'data_for' => 'premium_expired',
                'premium_id' => 888,
                'reason' => [
                    SurveySatisfaction::SURVEY_PREMIUM[1],
                    SurveySatisfaction::SURVEY_PREMIUM[0]
                ]
            ]
        );

        $this->assertTrue($result['room_survey']);
        $this->assertDatabaseHas('owner_survey', ['user_id' => $user->id]);
    }

    public function testListAllCanPromoteNewUser()
    {
        $user = factory(User::class)->create();

        $result = $this->repository->listAllCanPromote($user);
        $this->assertArrayHasKey('membership', $result);
    }

    public function testGetDateTimeDiffNullEmptyReturn()
    {
        $this->assertEquals("", $this->repository->getDateTimeDiff(null));
    }

    public function testGetDateTimeDiffToday()
    {
        $this->assertEquals("Hari ini", $this->repository->getDateTimeDiff(Carbon::now()));
    }

    public function testGetDateTimeDiffYesterday()
    {
        $this->assertEquals("1 Hari yang lalu", $this->repository->getDateTimeDiff(Carbon::now()->subDay()));
    }

    public function testGetDateTimeDiffFormatted()
    {
        $date = Carbon::now()->subWeek();

        $this->assertEquals(
            $date->format("d M H:i"),
            $this->repository->getDateTimeDiff($date)
        );
    }

    public function testGetTotalApartmentActive()
    {
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'status' => 'verified'
        ]);
        
        $result = $this->repository->userData($user);
        $this->assertEquals(1, $result['apartment_total_active']);
    }

    /**
     * @group UG
     * @group UG-4091
     */
    public function testGetGoldPlusFlagOldGp()
    {
        $this->mockGpKostLevelConfiguration();
        $user = factory(User::class)->create();
        $this->createRoomWithStates($user, ['gp-random']);
        $this->assertTrue($this->repository->getGoldPlusFlag($user->id));
    }

    /**
     * @group UG
     * @group UG-4091
     */
    public function testGetGoldPlusFlagNewGp()
    {
        $this->mockGpKostLevelConfiguration();
        $user = factory(User::class)->create();
        $this->createRoomWithStates($user, ['new-gp-random']);
        $this->assertTrue($this->repository->getGoldPlusFlag($user->id));
    }

    /**
     * @group UG
     * @group UG-4091
     */
    public function testGetGoldPlusFlagNonGp()
    {
        $this->mockGpKostLevelConfiguration();
        $user = factory(User::class)->create();
        $this->createRoomWithStates($user, []);
        $this->assertFalse($this->repository->getGoldPlusFlag($user->id));
    }

    private function createRoomWithStates(User $user, array $states): Room
    {
        $room = factory(Room::class)->states($states)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user,
            'designer_id' => $room,
            'status' => 'verified'
        ]);
        return $room;
    }

    public function mockGpKostLevelConfiguration()
    {
        factory(KostLevel::class)->state('regular')->create();
        for ($gpGroup = 1; $gpGroup <= KostLevel::GP_MAX_GROUP; $gpGroup++) {
            $id = factory(KostLevel::class)->create()->id;
            
            Config::set('kostlevel.id.goldplus' . $gpGroup, $id);
            Config::set('kostlevel.ids.goldplus' . $gpGroup, $id);
        }
    }

    private function executeCalculateUserProfileCounter(?UserVerificationAccount $userVerificationAccount)
    {
        return $this->getNonPublicMethodFromClass(OwnerDataRepositoryEloquent::class, 'calculateUserProfileCounter')
            ->invoke($this->repository, $userVerificationAccount);
    }

    private function executeGetProfileBadgeCounter(User $user)
    {
        return $this->getNonPublicMethodFromClass(OwnerDataRepositoryEloquent::class, 'getProfileBadgeCounter')
            ->invoke($this->repository, $user);
    }

    public function testNotificationOwner_Mamipoin()
    {
        $user = factory(User::class)->create();
        $notifOwners = factory(Notification::class, 5)->create([
            'user_id' => $user->id,
            'type' => Notification::IDENTIFIER_TYPE_MAMIPOIN,
            'url' => Notification::WEB_SCHEME[Notification::IDENTIFIER_TYPE_MAMIPOIN]
        ]);
        
        $result = $this->repository->notificationOwner($user, "web", []);
        $this->assertNotNull($result);
        $this->assertEquals(5, count($result['data']));
        foreach ($result['data'] as $key => $item) {
            $this->assertEquals(Notification::IDENTIFIER_TYPE_MAMIPOIN, $item['type']);
            $this->assertEquals(config('owner.dashboard_url').Notification::WEB_SCHEME[Notification::IDENTIFIER_TYPE_MAMIPOIN], $item['url']);
            $this->assertEquals($user->id, $item['user_id']);
        }
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(OwnerDataRepositoryEloquent::class);
        $this->premiumPackage = factory(PremiumPackage::class)->create([
            //'id' => 1,
            'is_active' => '1',
            'price' => 1000,
            'view' => 2000
        ]);

        $this->faker = Faker::create();
    }

    /**
     * Create Room
     *
     * @param int $userId
     * @param string $roomStatus
     * @param int $roomAvailable
     * @param int $roomCount
     */
    private function createRoom(int $userId, string $roomStatus, int $roomAvailable, int $roomCount)
    {
        $room = factory(Room::class)->create([
            'room_available' => $roomAvailable,
            'room_count' => $roomCount,
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $userId,
            'status' => $roomStatus,
        ]);
    }

    private function assertArray(array $expected, array $result): void
    {
        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $result, "Expected: " . json_encode($expected) . "\nResult :" . json_encode($result));
            $resultValue = $result[$key];
            if (is_array($value)) {
                $this->assertArray($value, $resultValue);
            } else {
                $this->assertEquals($value, $resultValue, "Expected: " . json_encode($expected) . "\nResult :" . json_encode($result). "\nKey: " . $key);
            }
        }
    }

    /**
     * @group UG
     * @group UG-4491
     */
    public function testGetListDataUpdateShouldExcludeGpLimited()
    {
        $user = factory(User::class)->create();

        $this->mockGpKostLevelConfiguration();
        $nonLimitedGpGroup = collect([1, 2, 4])->random();
        $nonLimitedGpId = collect(KostLevel::getGoldplusLevelIdsByGroups($nonLimitedGpGroup))->random();

        $limitedGpGroup = collect([3])->random();
        $limitedGpId = collect(KostLevel::getGoldplusLevelIdsByGroups($limitedGpGroup))->random();

        $room = $this->createActiveVerifiedRoomWithLevel($user, $nonLimitedGpId);
        $this->createActiveVerifiedRoomWithLevel($user, $limitedGpId);

        $result = $this->repository->getListDataUpdate($user, []);

        $this->assertEquals(1, $result['total']);
        $this->assertEquals($room->song_id, $result['rooms'][0]['_id']);
    }

    /**
     * @group UG
     * @group UG-4491
     */
    public function testGetListDataUpdateShouldIncludeNonGp()
    {
        $user = factory(User::class)->create();

        $this->mockGpKostLevelConfiguration();
        $nonLimitedGpGroup = collect([1, 2, 4])->random();
        $nonLimitedGpId = collect(KostLevel::getGoldplusLevelIdsByGroups($nonLimitedGpGroup))->random();

        $this->createActiveVerifiedRoomWithLevel($user, $nonLimitedGpId);
        $this->createActiveVerifiedRoomWithLevel($user, null);

        $result = $this->repository->getListDataUpdate($user, []);

        $this->assertEquals(2, $result['total']);
    }

    public function testIsShowingActivityPopover()
    {
        $user = factory(User::class)->create();
        factory(Activity::class)->create([
            'user_id' => $user->id,
            'logged_in_at' => Carbon::now()->subDays(4),
            'updated_property_at' => Carbon::now()->subDays(4),
        ]);

        $repository = app()->make(OwnerDataRepositoryEloquent::class);
        $method = $this->getNonPublicMethodFromClass(OwnerDataRepositoryEloquent::class, 'isShowingActivityPopover');

        config(['owner.notification.update_kost_popover_active' => false]);
        $result = $method->invokeArgs(
            $repository,
            [$user->id, 5]
        );
        $this->assertFalse($result);

        config(['owner.notification.update_kost_popover_active' => true]);
        $result = $method->invokeArgs(
            $repository,
            [$user->id, 5]
        );
        $this->assertTrue($result);

    }

    private function createActiveVerifiedRoomWithLevel(User $user, ?int $kostLevelId): Room
    {
        $room = factory(Room::class)->create([
            'is_active' => true,
        ]);
        if (!is_null($kostLevelId)) {
            $room->level()->attach($kostLevelId);
        }
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => 'verified',
        ]);
        return $room;
    }

    private function profileFunctionExpectedContracts()
    {
        return [
            "user",
            "membership",
            "promo",
            "survey",
            "detail_popover",
            "detail_popover_id",
            "profile_popover",
            "profilepopover_data",
            "popup",
            "loyalty",
            "activity_popover",
            "room_count",
            "room_count_unverified",
            "room_availability",
            "bookings",
            "instant_booking",
            "is_booking_all_room",
            "is_blacklist_mamipoin",
            "is_mamirooms",
            "total_mamipoin",
            "badge_counter",
            "gp_owner",
            "user_token",
            "gp_status"
        ];
    }

    public function testGetOwnerGPKostIsNotNul()
    {
        $owner = factory(User::class)->create([
            'is_verify' => '1',
            'is_owner' => 'true',
        ]);

        $gpLevelIds = KostLevel::getGoldplusLevelIdsByLevel();

        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $owner->id
        ]);
        $kostLevel = factory(KostLevel::class)->create([
            'id' => $gpLevelIds[0]
        ]);
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $gpLevelIds[0]
        ]);

        $result = $this->repository->getOwnerGPKost($owner->id);
        $this->assertNotNull($result);
        $this->assertIsString($result);
        $this->assertEquals($kostLevel->name, $result);
    }


    public function testGetOwnerGPKostIsNull()
    {
        $owner = factory(User::class)->create([
            'is_verify' => '1',
            'is_owner' => 'true',
        ]);

        $result = $this->repository->getOwnerGPKost($owner->id);
        $this->assertNotNull($result);
        $this->assertIsString($result);
        $this->assertEquals('reguler', $result);
    }

}
