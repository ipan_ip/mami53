<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;
use App\Repositories\Property\PropertyContractRepository;
use App\Test\MamiKosTestCase;

class PropertyContractRepositoryTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyContractRepository::class);
    }

    // public function testCreateContractAndSuccess()
    // {
    //     $result = $this->repository->createContract([
    //         'property_id' => 123123,
    //         'property_level_id' => 123123,
    //         'joined_at' => date('Y-m-d H:i:s'),
    //         'assigned_by' => 123132,
    //         'total_package' => 10
    //     ]);
    //     $this->assertInstanceOf(PropertyContract::class, $result);
    // }

    public function testTerminateContractActiveAndSuccess()
    {
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_ACTIVE
        ]);
        $result = $this->repository->terminateContract($contract, 123);
        $this->assertEquals(PropertyContract::STATUS_TERMINATED, $result->status);
    }

    public function testTerminateContractInactiveAndUnchanged()
    {
        $endedBy = 123;
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_INACTIVE,
            'ended_at' => date('Y-m-d H:i:s'),
            'ended_by' => $endedBy
        ]);
        $result = $this->repository->terminateContract($contract, 456);
        $this->assertEquals(PropertyContract::STATUS_TERMINATED, $result->status);
        $this->assertEquals(456, $result->ended_by);
    }

    // public function testDeleteContractByPropertyIdAndSuccess()
    // {
    //     $propertyId = 1;
    //     factory(PropertyContract::class)->create(['property_id' => $propertyId]);
    //     $result = $this->repository->deleteContractByPropertyId($propertyId);
    //     $this->assertTrue($result);
    // }
}
