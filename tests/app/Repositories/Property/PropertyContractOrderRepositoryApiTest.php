<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractOrderConnector;
use App\Test\MamiKosTestCase;

class PropertyContractOrderRepositoryApiTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyContractOrderRepositoryApi::class);
    }

    public function testCreateOrderAndSuccess()
    {
        $mockConnector = $this->mockAlternatively(PropertyContractOrderConnector::class);
        $mockConnector->shouldReceive('createOrder');
        $this->repository->createOrder(factory(PropertyContract::class)->make());
        $this->assertTrue(true);
    }
}
