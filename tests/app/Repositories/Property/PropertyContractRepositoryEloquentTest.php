<?php

namespace App\Repositories\Property;

use App\User;
use App\Test\MamiKosTestCase;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repositories\Property\PropertyContractRepositoryEloquent;
use App\Entities\Property\Property;

class PropertyContractRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyContractRepositoryEloquent::class);
    }

    public function testCreateContractAndSuccess()
    {
        $result = $this->repository->createContract([
            'property_id' => 123123,
            'property_level_id' => 123123,
            'joined_at' => date('Y-m-d H:i:s'),
            'assigned_by' => 123132,
            'total_package' => 10
        ]);
        $this->assertInstanceOf(PropertyContract::class, $result);
    }

    public function testCreateContractWithoutNewPropertyPackageFieldsShouldSaveIntoDatabase(): void
    {
        $params = [
            'property_id' => 123123,
            'property_level_id' => 123123,
            'joined_at' => date('Y-m-d H:i:s'),
            'assigned_by' => 123132,
            'total_package' => 10,
        ];

        $this->repository->createContract($params);

        unset($params['property_id']);

        $params['is_autocount'] = true;
        $params['total_room_package'] = 0;
        $this->assertDatabaseHas('property_contracts', $params);
        $this->assertDatabaseHas('property_contract_detail', ['property_id' => 123123]);
    }

    public function testCreateContractWithNewPropertyPackageFieldsShouldSaveIntoDatabase(): void
    {
        $params = [
            'property_id' => 123123,
            'property_level_id' => 123123,
            'joined_at' => date('Y-m-d H:i:s'),
            'assigned_by' => 123132,
            'total_package' => 10,
            'is_autocount' => true,
            'total_room_package' => 10
        ];

        $this->repository->createContract($params);

        unset($params['property_id']);
        $this->assertDatabaseHas('property_contracts', $params);
        $this->assertDatabaseHas('property_contract_detail', ['property_id' => 123123]);
    }

    public function testUpdateContractJoinedAt()
    {
        $updater = factory(User::class)->create();
        $contract = factory(PropertyContract::class)->create(['joined_at' => '2019-09-30 17:57:18']);
        $params = [
            'joined_at' => '2020-09-30 17:57:18',
            'is_autocount' => false,
            'total_package' => 20,
            'updated_by' => $updater->id
        ];
        $result = $this->repository->updateContract($contract, $params);
        $this->assertInstanceOf(PropertyContract::class, $result);
        $this->assertEquals($params['joined_at'], $result->joined_at);
        $this->assertEquals($params['updated_by'], $result->updated_by);
    }

    public function testUpdateContractWithAutocount()
    {
        $contract = factory(PropertyContract::class)->create(['total_package' => 2]);
        $params = [
            'joined_at' => '2020-09-30 17:57:18',
            'is_autocount' => true,
            'total_package' => null,
            'updated_by' => 99
        ];
        $result = $this->repository->updateContract($contract, $params);
        $this->assertInstanceOf(PropertyContract::class, $result);
        $this->assertEquals(2, $result->total_package);
    }

    public function testUpdateContractWithTotalPackage()
    {
        $contract = factory(PropertyContract::class)->create(['total_package' => 2]);
        $params = [
            'joined_at' => '2020-09-30 17:57:18',
            'is_autocount' => false,
            'total_package' => 4,
            'updated_by' => 99
        ];
        $result = $this->repository->updateContract($contract, $params);
        $this->assertInstanceOf(PropertyContract::class, $result);
        $this->assertEquals($params['total_package'], $result->total_package);
    }

    public function testTerminateContractActiveAndSuccess()
    {
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_ACTIVE
        ]);
        $result = $this->repository->terminateContract($contract, 123);
        $this->assertEquals(PropertyContract::STATUS_TERMINATED, $result->status);
    }

    public function testTerminateContractInactiveAndUnchanged()
    {
        $endedBy = 123;
        $contract = factory(PropertyContract::class)->create([
            'status' => PropertyContract::STATUS_INACTIVE,
            'ended_at' => date('Y-m-d H:i:s'),
            'ended_by' => $endedBy
        ]);
        $result = $this->repository->terminateContract($contract, 456);
        $this->assertEquals(PropertyContract::STATUS_TERMINATED, $result->status);
        $this->assertEquals(456, $result->ended_by);
    }

    public function testDeleteContractByPropertyIdAndSuccess()
    {
        $propertyId = 1;
        $contract = factory(PropertyContract::class)->create();
        $detail = factory(PropertyContractDetail::class)->create([
            'property_contract_id' => $contract->id,
            'property_id' => $propertyId
        ]);
        $result = $this->repository->deleteContractByPropertyId($propertyId);
        $this->assertTrue($result);
    }

    public function testSetSubmissionIdAndSuccess()
    {
        $contract = factory(PropertyContract::class)->create();
        $submissionId = 123;
        $result = $this->repository->setSubmissionId($contract, $submissionId);
        $this->assertEquals($submissionId, $result->submission_id);
    }

    public function testSetSubmissionIdAndReturnNull()
    {
        $submissionId = 123;
        $result = $this->repository->setSubmissionId(null, $submissionId);
        $this->assertEmpty($result);
    }
    
    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Property\PropertyContractRepositoryEloquent
     */
    public function testFindUserOwnerByContractReturnUserEntity()
    {
        $user = factory(User::class)->create(['is_owner' => 'true']);
        $property = factory(Property::class)->create(['owner_user_id' => $user]);
        $contract = factory(PropertyContract::class)->create();
        $contract->properties()->attach($property);

        $result = $this->repository->findUserOwnerByContract($contract);

        $this->assertInstanceOf(User::class, $result);
        $this->assertEquals($user->id, $result->id);
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Property\PropertyContractRepositoryEloquent
     */
    public function testFindUserOwnerByContractWithInvalidContractShouldReturnNull()
    {
        $contract = factory(PropertyContract::class)->create();

        $result = $this->repository->findUserOwnerByContract($contract);

        $this->assertNull($result);
    }
}
