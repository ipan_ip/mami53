<?php

namespace App\Repositories\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\Repositories\Property\PropertyRepository;
use App\Test\MamiKosTestCase;
use App\User;

class PropertyRepositoryTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyRepository::class);
    }

    public function testCreatePropertyAndSuccess()
    {
        $owner = factory(User::class)->state('owner')->create();
        $result = $this->repository->createProperty('name', $owner);
        $this->assertNotEmpty($result);
        $this->assertInstanceOf(Property::class, $result);
    }

    public function testCreatePropertyAndReturnNull()
    {
        $owner = factory(User::class)->state('tenant')->create();
        $result = $this->repository->createProperty('name', $owner);
        $this->assertEmpty($result);
    }

    public function testUpdatePropertyAndSuccess()
    {
        $name = 'name';
        $nameUpdated = 'nameUpdated';
        $owner = factory(User::class)->state('owner')->create();
        $property = factory(Property::class)->create(['name' => $name]);
        $result = $this->repository->updateProperty($property, $nameUpdated, $owner);
        $this->assertTrue($result);
    }

    public function testUpdatePropertyAndReturnFalse()
    {
        $name = 'name';
        $nameUpdated = 'nameUpdated';
        $owner = factory(User::class)->state('tenant')->create();
        $property = factory(Property::class)->create(['name' => $name]);
        $result = $this->repository->updateProperty($property, $nameUpdated, $owner);
        $this->assertFalse($result);
    }

    public function testGetListPaginateAndSuccess()
    {
        factory(Property::class)->create();
        $result = $this->repository->getListPaginate(new PropertySearchQueryBuilder([], []));
        $this->assertNotEmpty($result);
    }

    public function testDeletePropertyAndSuccess()
    {
        $property = factory(Property::class)->create();
        $result = $this->repository->deleteProperty($property);
        $this->assertTrue($result);
    }
}
