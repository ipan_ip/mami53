<?php

namespace App\Repositories\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\Entities\Room\Room;
use App\Repositories\Property\PropertyRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;

class PropertyRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyRepositoryEloquent::class);
    }

    public function testCreatePropertyAndSuccess()
    {
        $owner = factory(User::class)->state('owner')->create();
        $result = $this->repository->createProperty('name', $owner);
        $this->assertNotEmpty($result);
        $this->assertInstanceOf(Property::class, $result);
    }

    public function testCreatePropertyAndReturnNull()
    {
        $owner = factory(User::class)->state('tenant')->create();
        $result = $this->repository->createProperty('name', $owner);
        $this->assertEmpty($result);
    }

    public function testUpdatePropertyAndSuccess()
    {
        $name = 'name';
        $nameUpdated = 'nameUpdated';
        $owner = factory(User::class)->state('owner')->create();
        $property = factory(Property::class)->create(['name' => $name]);
        $result = $this->repository->updateProperty($property, $nameUpdated, $owner);
        $this->assertTrue($result);
    }

    public function testUpdatePropertyAndReturnFalse()
    {
        $name = 'name';
        $nameUpdated = 'nameUpdated';
        $owner = factory(User::class)->state('tenant')->create();
        $property = factory(Property::class)->create(['name' => $name]);
        $result = $this->repository->updateProperty($property, $nameUpdated, $owner);
        $this->assertFalse($result);
    }

    public function testGetListPaginateAndSuccess()
    {
        factory(Property::class)->create();
        $result = $this->repository->getListPaginate(new PropertySearchQueryBuilder([], []));
        $this->assertNotEmpty($result);
    }

    public function testDeletePropertyAndSuccess()
    {
        $property = factory(Property::class)->create();
        $result = $this->repository->deleteProperty($property);
        $this->assertTrue($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsPropertyNameExistWithSameName()
    {
        $owner = factory(User::class)->create();

        $duplicateName = 'test duplicate name';
        factory(Property::class)->create([
            'name' => $duplicateName,
            'owner_user_id' => $owner->id
        ]);

        $result = $this->repository->isPropertyNameExist($duplicateName, $owner);
        $this->assertTrue($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsPropertyNameExistWithDifferentName()
    {
        $owner = factory(User::class)->create();

        $duplicateName = 'test duplicate name';
        factory(Property::class)->create([
            'name' => $duplicateName,
            'owner_user_id' => $owner->id
        ]);

        $result = $this->repository->isPropertyNameExist('test name', $owner);
        $this->assertFalse($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsPropertyExistWithOwnerDoesntOwnTheProperty()
    {
        $owner = factory(User::class)->create();
        $property = factory(Property::class)->create();

        $result = $this->repository->isPropertyExist($property->id, $owner);
        $this->assertFalse($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsPropertyExistWithOwnerOwnTheProperty()
    {
        $owner = factory(User::class)->create();
        $property = factory(Property::class)->create([
            'owner_user_id' => $owner->id
        ]);

        $result = $this->repository->isPropertyExist($property->id, $owner);
        $this->assertTrue($result);
    }

}
