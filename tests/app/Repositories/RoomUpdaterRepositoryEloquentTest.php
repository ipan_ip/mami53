<?php

namespace App\Repositories;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\Call;
use App\Test\MamiKosTestCase;
use App\User;
use App\Presenters\RoomPresenter;
use App\Criteria\Room\OwnerCriteria;
use App\Entities\Media\Media;
use App\Entities\Room\Element\ResponseMessage;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Room\Element\Card;
use App\Http\Requests\Consultant\PotentialProperty\Request;

class RoomUpdaterRepositoryEloquentTest extends MamiKosTestCase
{
    use WithFaker;

    protected $user;
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->repository = new RoomUpdaterRepositoryEloquent(app());
        $this->repository->pushCriteria(new OwnerCriteria($this->user, 'kos', [], []));
    }

    public function testcheckOwnershipWithoutRoom()
    {
        $result = $this->repository->checkOwnership(null, $this->user);

        $this->assertFalse($result);
    }

    public function testcheckOwnershipWhenUserIsNotRoomOwner()
    {
        $room = $this->createRoom();
        $ordinaryUser = factory(User::class)->create();

        $result = $this->repository->checkOwnership($room, $ordinaryUser);

        $this->assertFalse($result);
    }

    public function testcheckUpdateKostShouldReturnSuccess()
    {
        $room = $this->createRoom();
        $data = factory(Room::class)->make()->toArray();
        $params = array_merge($data, ['user_id' => $this->user->id]);

        // Check database before update
        $this->assertDatabaseMissing('designer', $data);

        $result = $this->repository->updateKost($room, $params);

        // Check database after update
        $expectedResult = [
            'status' => true,
            'message' => ResponseMessage::SUCCESS_UPDATE_KOST,
        ];
        $this->assertDatabaseHas('designer', $data);
        $this->assertEquals($result, $expectedResult);
    }

    public function testcheckOwnershipForValidOwnerRoom()
    {
        $room = $this->createRoom();

        $result = $this->repository->checkOwnership($room, $this->user);

        $this->assertTrue($result);
    }

    public function testGetPriceRoomShouldReturnDailyWeeklyMonthlyYearlyPrice()
    {
        $room = $this->createRoom();

        $expectedResult = [
            'daily' => $room->price_daily,
            'weekly' => $room->price_weekly,
            'monthly' => $room->price_monthly,
            'yearly' => $room->price_yearly
        ];

        $result = $this->repository->getPriceRoom($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetOtherInformationShouldReturnRemarkDescriptionPriceRemark()
    {
        $room = $this->createRoom();

        $expectedResult = [
            'remarks' => $room->remark,
            'description' => $room->description,
            'price_remark' => $room->price_remark
        ];

        $result = $this->repository->getOtherInformation($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetFacNear()
    {
        $room = $this->createRoom();

        $tag = factory(Tag::class, 1)->create([
            'type' => 'fac_near',
            'id' => 1,
            'name' => 'facility 1'
        ])->toArray()[0];

        RoomTag::create(['designer_id' => $room->id, 'tag_id' => $tag['id']]);

        $data[] = [
            'id' => $tag['id'],
            'name' => $tag['name'],
            'selected' => true
        ];

        $expectedResult = [
            'fac_near_other' => $room->fac_near_other,
            'fac_near' => $data
        ];

        $result = $this->repository->getFacNear($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetParking()
    {
        $room = $this->createRoom();

        $tag = factory(Tag::class, 1)->create([
            'type' => 'fac_park',
            'id' => 1,
            'name' => 'facility 1'
        ])->toArray()[0];

        RoomTag::create(['designer_id' => $room->id, 'tag_id' => $tag['id']]);

        $data[] = [
            'id' => $tag['id'],
            'name' => $tag['name'],
            'selected' => true
        ];

        $expectedResult = [
            'fac_park' => $data
        ];

        $result = $this->repository->getParking($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetFacShare()
    {
        $room = $this->createRoom();

        $tag = factory(Tag::class, 1)->create([
            'type' => 'fac_share',
            'id' => 1,
            'name' => 'facility 1'
        ])->toArray()[0];

        RoomTag::create(['designer_id' => $room->id, 'tag_id' => $tag['id']]);

        $data[] = [
            'id' => $tag['id'],
            'name' => $tag['name'],
            'selected' => true
        ];

        $expectedResult = [
            'fac_share_other' => $room->fac_share_other,
            'fac_share' => $data
        ];

        $result = $this->repository->getFacShare($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetFacRoom()
    {
        $room = $this->createRoom();

        $tag = factory(Tag::class, 1)->create([
            'type' => 'fac_room',
            'id' => 1,
            'name' => 'facility 1'
        ])->toArray()[0];

        RoomTag::create(['designer_id' => $room->id, 'tag_id' => $tag['id']]);

        $data[] = [
            'id' => $tag['id'],
            'name' => $tag['name'],
            'selected' => true
        ];

        $expectedResult = [
            'fac_room_other' => $room->fac_room_other,
            'fac_room' => $data
        ];

        $result = $this->repository->getFacRoom($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetFacBath()
    {
        $room = $this->createRoom();

        $tag = factory(Tag::class, 1)->create([
            'type' => 'fac_bath',
            'id' => 1,
            'name' => 'facility 1'
        ])->toArray()[0];

        RoomTag::create(['designer_id' => $room->id, 'tag_id' => $tag['id']]);

        $data[] = [
            'id' => $tag['id'],
            'name' => $tag['name'],
            'selected' => true
        ];

        $expectedResult = [
            'fac_bath_other' => $room->fac_bath_other,
            'fac_bath' => $data
        ];

        $result = $this->repository->getFacBath($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testcheckUpdateCardsShouldReturnSuccess()
    {
        $room = $this->createRoom();
        $data = [];

        $currentCardRoomPhoto = $this->faker->numberBetween(10000, 99999);
        $currentCardRoomWillRemovedPhoto = $this->faker->numberBetween(10000, 99999);
        $newCardRoomPhoto = $this->faker->numberBetween(10000, 99999);

        $currentCardRoom = factory(Card::class, 1)->create([
            'designer_id' => $room->id,
            'photo_id' => $currentCardRoomPhoto
        ])->toArray()[0];

        $currentCardRoomWillRemoved = factory(Card::class, 1)->create([
            'designer_id' => $room->id,
            'photo_id' => $currentCardRoomWillRemovedPhoto
        ])->toArray()[0];

        $newCardRoom = factory(Card::class, 1)->make([
            'designer_id' => null,
            'photo_id' => $newCardRoomPhoto,
        ])->toArray()[0];

        // Check card is exists
        $this->assertDatabaseHas('designer_style', $currentCardRoom);
        $this->assertDatabaseHas('designer_style', $currentCardRoomWillRemoved);

        // Update single card newCarRoom
        $currentCardRoom['photo'] = $currentCardRoomPhoto;
        $currentCardRoom['ordering'] = 1;
        $newCardRoom['photo'] = $newCardRoomPhoto;
        $newCardRoom['ordering'] = 3;

        $data[] = $currentCardRoom;
        $data[] = $newCardRoom;

        $result = $this->repository->updateCards($data, $room->id);

        // Check updated table

        $expectedResult = [
            'status' => true,
            'message' => ResponseMessage::SUCCESS_UPDATE_KOST,
        ];

        $this->assertDatabaseHas('designer_style', [
            'id' => $currentCardRoom['id'],
            'designer_id' => $room->id,
            'photo_id' => $currentCardRoomPhoto
        ]);

        $this->assertDatabaseHas('designer_style', [
            'designer_id' => $room->id,
            'photo_id' => $newCardRoomPhoto,
        ]);

        $this->assertEquals($result, $expectedResult);

        $this->assertSoftDeleted('designer_style', [
            'designer_id' => $room->id,
            'photo_id' => $currentCardRoomWillRemovedPhoto
        ]);
    }

    public function testcheckUpdateSingleCardsShouldSuccess()
    {
        $room = $this->createRoom();
        $data = [];

        $photoId = $this->faker->numberBetween(10000, 99999);

        $card = factory(Card::class, 1)->create([
            'designer_id' => $room->id,
            'photo_id' => $photoId
        ])->toArray()[0];

        // Check card is exists
        $this->assertDatabaseHas('designer_style', [
            'id' => $card['id'],
            'designer_id' => $room->id,
            'photo_id' => $photoId
        ]);

        // Update single card
        $data = [
            'photo' => $photoId,
            'description' => 'Update decription',
            'ordering' => 'Update ordering',
        ];

        $this->repository->updateSingleCards($data, $room->id);

        // Check updated table
        $this->assertDatabaseHas('designer_style', [
            'id' => $card['id'],
            'designer_id' => $room->id,
            'photo_id' => $photoId,
            'description' => 'Update decription',
            'ordering' => 'Update ordering'
        ]);
    }

    public function testcheckUpdateOtherInformationShouldReturnSuccess()
    {
        $room = $this->createRoom();
        $data = [];
        $data['description'] = 'update description';
        $data['price_remark'] = 'update price_remark';
        $data['remarks'] = 'update remark';
        $data['user_id'] = $this->user->id;

        $expectedResult = [
            'status' => true,
            'message' => ResponseMessage::SUCCESS_UPDATE_KOST,
        ];

        $result = $this->repository->updateOtherInformation($room, $data);
        $this->assertDatabaseHas('designer', [
            'id' => $room->id,
            'description' => $data['description'],
            'price_remark' => $data['price_remark'],
            'remark' => $data['remarks']
        ]);
        $this->assertEquals($result, $expectedResult);
    }

    public function testGetRoomNameTypeShouldReturnNameAndType()
    {
        $room = $this->createRoom();

        $expectedResult = [
            'name' => $room->name,
            'gender' => $room->gender
        ];

        $result = $this->repository->getRoomNameType($room);

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetKosOwnerItemWithoutParam()
    {
        $room = $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem();

        $this->assertEquals(1, count($data));
        $this->assertEquals($room->song_id, $data[0]['_id']);
    }

    public function testGetKosOwnerItemWithLimit()
    {
        $this->createRoom();
        $this->createRoom();
        $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem(2);
        $this->assertEquals(2, count($data));
    }

    public function testGetKosOwnerItemWithOffset()
    {
        $this->createRoom();
        $this->createRoom();
        $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem(2, 2);

        $this->assertEquals(1, count($data));
    }

    public function testGetKosOwnerItemNotIncludingSongId()
    {
        $room = $this->createRoom();
        $this->createRoom();
        $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem(2, 0, $room->song_id);

        $this->assertEquals(2, count($data));
        $this->assertNull(collect($data)->first(function ($item) use ($room) {
            return $item['_id'] == $room->song_id;
        }));
    }

    public function testGetKosOwnerItemWithAllParam()
    {
        $this->createRoom();
        $room = $this->createRoom();
        $this->createRoom();
        $this->createRoom();
        $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem(2, 2, $room->song_id);

        $this->assertEquals(2, count($data));
    }

    public function testGetKosOwnerItemWithZeroCountCall()
    {
        $this->createRoom();

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem();

        $this->assertEquals(0, $data[0]['chat_count']);
    }

    public function testGetKosOwnerItemWithOneCountCall()
    {
        $room = $this->createRoom();
        factory(Call::class)->create([
            'designer_id' => $room->id
        ]);

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem();

        $this->assertEquals(1, $data[0]['chat_count']);
    }

    public function testGetKosOwnerItemWithFiveCountCall()
    {
        $room = $this->createRoom();
        factory(Call::class, 5)->create([
            'designer_id' => $room->id
        ]);

        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getKosOwnerItem();

        $this->assertEquals(5, $data[0]['chat_count']);
    }

    public function testGetOwnerKosBySongIdNullOnNotFound()
    {
        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getOwnerKosBySongId(888);
        $this->assertEmpty($data);
    }

    public function testGetOwnerKosBySongIdSuccess()
    {
        $this->createRoom();
        $this->createRoom();
        $room = $this->createRoom();
        $this->repository->setPresenter(new RoomPresenter('list-owner-property'));
        $data = $this->repository->getOwnerKosBySongId($room->song_id);
        $this->assertEquals($room->song_id, $data['_id']);
    }

    public function testCheckOwnershipTrue()
    {
        $room = $this->createRoom();

        $this->assertTrue($this->repository->checkOwnership($room, $this->user));
    }

    public function testCheckOwnershipFalse()
    {
        $notOwner = factory(User::class)->create();
        $room = $this->createRoom();

        $this->assertFalse($this->repository->checkOwnership($room, $notOwner));
    }

    public function testUpdateKostTrue()
    {
        $room = $this->createRoom();
        $data = [
            'gender' => 1,
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateKost($room, $data);

        $room = Room::find($room->id);
        $this->assertEquals($room->gender, 1);
        $this->assertTrue($response['status']);
    }

    public function testUpdateCard()
    {
        $medias = factory(Media::class, 3)->create();
        $room = $this->createRoom();
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $medias[0]->id,
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'photo_id' => $medias[2]->id,
        ]);

        $cards = [
            [
                'photo' => $medias[0]->id,
                'description' => null,
                'ordering' => null,
            ],
            [
                'photo' => $medias[1]->id,
                'description' => null,
                'ordering' => null,
            ],
        ];

        $response = $this->repository->updateCards($cards, $room->id);

        $room = Room::with('cards')->find($room->id);
        $this->assertTrue($response['status']);
        $this->assertContains($room->cards[0]->photo_id, [$medias[0]->id, $medias[1]->id]);
        $this->assertContains($room->cards[1]->photo_id, [$medias[0]->id, $medias[1]->id]);
    }

    public function testUpdateBathroom()
    {
        $room = $this->createRoom();
        $data = [
            'fac_bath' => [13],
            'fac_bath_other' => 'gantungan baju',
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateBathroom($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
        $this->assertEquals($room->fac_bath_other, 'gantungan baju');
    }

    public function testUpdateFacRoom()
    {
        $room = $this->createRoom();
        $data = [
            'fac_room' => [14],
            'fac_room_other' => 'gantungan baju',
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateFacRoom($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
        $this->assertEquals($room->fac_room_other, 'gantungan baju');
    }

    public function testUpdateFacShare()
    {
        $room = $this->createRoom();
        $data = [
            'fac_share' => [15],
            'fac_share_other' => 'gantungan baju',
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateFacShare($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
        $this->assertEquals($room->fac_share_other, 'gantungan baju');
    }

    public function testUpdateParking()
    {
        $room = $this->createRoom();
        $data = [
            'fac_park' => [16],
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateParking($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
    }

    public function testUpdateFacNear()
    {
        $room = $this->createRoom();
        $data = [
            'fac_near' => [17],
            'fac_near_other' => 'taman',
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateFacNear($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
        $this->assertEquals($room->fac_near_other, 'taman');
    }

    public function testUpdateOtherInformation()
    {
        $room = $this->createRoom();
        $data = [
            'description' => null,
            'price_remark' => null,
            'remarks' => 'test remark',
            'user_id' => $this->user->id,
        ];

        $response = $this->repository->updateOtherInformation($room, $data);

        $room = Room::find($room->id);
        $this->assertTrue($response['status']);
        $this->assertEquals($room->remark, 'test remark');
    }

    public function testGetRoomNameType()
    {
        $room = factory(Room::class)->create([
            'name' => 'Kost unit test',
            'gender' => 1,
        ]);

        $response = $this->repository->getRoomNameType($room);

        $this->assertEquals($room->name, $response['name']);
    }

    public function testGetPriceRoom()
    {
        $room = factory(Room::class)->create([
            'price_daily' => 100000,
        ]);

        $response = $this->repository->getPriceRoom($room);

        $this->assertEquals($room->price_daily, $response['daily']);
    }

    public function testGetOtherInformation()
    {
        $room = factory(Room::class)->create([
            'remark' => 'test remark',
        ]);

        $response = $this->repository->getOtherInformation($room);

        $this->assertEquals($room->remark, $response['remarks']);
    }

    public function testGetOwnerTotalKos()
    {
        $this->createRoom();

        $this->assertEquals(1, $this->repository->getOwnerTotalKos());
    }

    public function testGetItemListOwner()
    {
        $room = $this->createRoom();

        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list-owner'));
        $this->repository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($this->user, 'kos', [], null));
        $response = $this->repository->getItemListOwner();

        $this->assertEquals(1, $response['page']);
        $this->assertEquals($room->song_id, $response['rooms'][0]['_id']);
    }

    public function testGetItemListOwnerWithoutOffsetAndLimitParam()
    {
        $this->createRoom();
        $this->createRoom();
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list-owner'));

        $result = $this->repository->getItemListOwner(false);
        $expectedResult = [
            'page' => 1,
            'next-page' => 2,
            'limit' => 20,
            'offset' => 0,
            'has-more' => false,
            'total' => 2,
            'total-str' => '2',
            'toast' => null,
        ];
        array_splice($result, 8, 1);
        $this->assertEquals($result, $expectedResult);
    }

    public function testGetItemListOwnerWitOffsetAndLimitParam()
    {
        $this->createRoom();
        $this->createRoom();
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list-owner'));

        request()->merge(['offset' => 0]);
        request()->merge(['limit' => 1]);
        $result = $this->repository->getItemListOwner(false);
        $totalDataInOnePage = count($result['rooms']);
        $expectedResult = [
            'page' => 1,
            'next-page' => 2,
            'limit' => 1,
            'offset' => 0,
            'has-more' => true,
            'total' => 2,
            'total-str' => '2',
            'toast' => null
        ];
        array_splice($result, 8, 1);
        $this->assertEquals($result, $expectedResult);
        $this->assertEquals($totalDataInOnePage, request()->input('limit'));
    }

    public function testsecurePaginateWithOffsetIsNotZeroLimitIsNotNull()
    {
        $reflection = new \ReflectionClass(RoomUpdaterRepositoryEloquent::class);
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);
        request()->merge(['offset' => rand(1, 5)]);
        request()->merge(['limit' => rand(8, 10)]);

        $page = $method->invokeArgs($this->repository, [true]);

        $expectedResult = ceil(request()->input('offset') / request()->input('limit')) + 1;
        $this->assertEquals($page, $expectedResult);
    }

    public function testsecurePaginateWithPageGreaterThan10AndShouldLimitPageIsTrue()
    {
        $reflection = new \ReflectionClass(RoomUpdaterRepositoryEloquent::class);
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);
        request()->merge(['page' => 11]);

        $result = $method->invokeArgs($this->repository, [true]);

        $expectedResult = 10;
        $this->assertEquals($result->page, $expectedResult);
    }

    public function testsecurePaginateWithOffsetIsZero()
    {
        $reflection = new \ReflectionClass(RoomUpdaterRepositoryEloquent::class);
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);
        request()->merge(['offset' => 0]);
        request()->merge(['limit' => rand(8, 10)]);

        $page = $method->invokeArgs($this->repository, [true]);

        $expectedResult = ceil(request()->input('offset') / request()->input('limit')) + 1;
        $this->assertEquals($page, $expectedResult);
    }

    /**
     * Create Room and RoomOwner
     *
     * @return App\Entities\Room\Room
     */
    private function createRoom()
    {
        $room = factory(Room::class)->create([
            'is_active' => 'true'
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $this->user->id
        ]);

        return $room;
    }
}
