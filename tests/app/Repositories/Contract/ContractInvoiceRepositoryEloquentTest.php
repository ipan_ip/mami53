<?php

namespace App\Repositories;

use App\Entities\Mamipay\MamipayInvoice;
use App\Repositories\Contract\ContractInvoiceRepositoryEloquent;
use App\Test\MamiKosTestCase;

class ContractInvoiceRepositoryEloquentTest extends MamiKosTestCase
{
    
    public function testGetDownPaymentInvoiceByContractId()
    {
        $invoice = factory(MamipayInvoice::class)->create(['invoice_number' => 'DP/2123/2323/22', 'contract_id' => 999]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getDownPaymentInvoiceByContractId($invoice->contract_id);
        $this->assertEquals($invoice->id, $result->id);
    }

    public function testGetSettlementInvoiceByContractId()
    {
        $invoice = factory(MamipayInvoice::class)->create(['invoice_number' => 'ST/2123/2323/22', 'contract_id' => 998]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getSettlementInvoiceByContractId($invoice->contract_id);
        $this->assertEquals($invoice->id, $result->id);
    }

    public function testGetFirstInvoiceByContractId()
    {
        $invoice = factory(MamipayInvoice::class)->create(['contract_id' => 997]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getFirstInvoiceByContractId($invoice->contract_id);
        $this->assertEquals($invoice->id, $result->id);
    }

    public function testGetLatestUnpaidBookingInvoiceAndGetUnpaidInvoice()
    {
        $invoice = factory(MamipayInvoice::class)->create([
            'contract_id' => 996,
            'status' => MamipayInvoice::STATUS_UNPAID
            ]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getLatestUnpaidBookingInvoice($invoice->contract_id);
        $this->assertEquals($invoice->id, $result->id);
    }

    public function testGetLatestUnpaidBookingInvoiceAndGetUnpaidDp()
    {
        $invoice = factory(MamipayInvoice::class)->create([
            'contract_id' => 995,
            'status' => MamipayInvoice::STATUS_UNPAID,
            'invoice_number' => 'DP/2123/2323/44'
            ]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getLatestUnpaidBookingInvoice($invoice->contract_id);
        $this->assertEquals($invoice->id, $result->id);
    }

    public function testGetLatestUnpaidBookingInvoiceAndGetUnpaidSettlement()
    {
        $invoiceDp = factory(MamipayInvoice::class)->create([
            'contract_id' => 994,
            'status' => MamipayInvoice::STATUS_PAID,
            'invoice_number' => 'DP/2123/2323/44'
            ]);
        $invoiceSt = factory(MamipayInvoice::class)->create([
                'contract_id' => $invoiceDp->contract_id,
                'status' => MamipayInvoice::STATUS_UNPAID,
                'invoice_number' => 'ST/2123/2323/44'
                ]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getLatestUnpaidBookingInvoice($invoiceDp->contract_id);
        $this->assertEquals($invoiceSt->id, $result->id);
    }

    public function testGetLatestUnpaidBookingInvoiceAndGetNull()
    {
        $invoiceDp = factory(MamipayInvoice::class)->create([
            'contract_id' => 993,
            'status' => MamipayInvoice::STATUS_PAID,
            'invoice_number' => 'DP/2123/2323/44'
            ]);
        $invoiceSt = factory(MamipayInvoice::class)->create([
                'contract_id' => $invoiceDp->contract_id,
                'status' => MamipayInvoice::STATUS_PAID,
                'invoice_number' => 'ST/2123/2323/44'
                ]);
        $repo = new ContractInvoiceRepositoryEloquent($this->app);
        $result = $repo->getLatestUnpaidBookingInvoice($invoiceDp->contract_id);
        $this->assertNull($result);
    }
}
