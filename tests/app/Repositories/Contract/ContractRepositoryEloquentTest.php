<?php

namespace App\Repositories;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\Contract\ContractRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class ContractRepositoryEloquentTest extends MamiKosTestCase
{
    
    /** @var ContractRepository */
    private $contractRepo;

    protected function setUp() : void
    {
        parent::setUp();
        $this->contractRepo = $this->app->make(ContractRepository::class);
    }

    public function testIsFullyPaidWithDpAndSettlementPaid()
    {
        $invoiceDP = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'DP/2123/2323/22',
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID,
            'contract_id' => 123
        ]);
        factory(MamipayInvoice::class)->create([
            'invoice_number' => 'ST/2123/2323/22',
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID,
            'contract_id' => $invoiceDP->contract_id
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoiceDP->contract_id);
        $this->assertTrue($result);
    }

    public function testIsFullyPaidWithDpAndSettlementUnpaid()
    {
        $invoiceDP = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'DP/2123/2323/23',
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID,
            'contract_id' => 321
        ]);
        factory(MamipayInvoice::class)->create([
            'invoice_number' => 'ST/2123/2323/23',
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID,
            'contract_id' => $invoiceDP->contract_id
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoiceDP->contract_id);
        $this->assertFalse($result);
    }

    public function testIsFullyPaidWithDpUnpaid()
    {
        $invoiceDP = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'DP/2123/2323/23',
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID,
            'contract_id' => 323
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoiceDP->contract_id);
        $this->assertFalse($result);
    }

    public function testIsFullyPaidWithSettlementUngenerated()
    {
        $invoiceDP = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'DP/2123/2323/23',
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID,
            'contract_id' => 323
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoiceDP->contract_id);
        $this->assertFalse($result);
    }

    public function testIsFullyPaidWithInvoicePaid()
    {
        $invoice = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'INV/2123/2323/22',
            'status' => MamipayInvoice::PAYMENT_STATUS_PAID,
            'contract_id' => 231
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoice->contract_id);
        $this->assertTrue($result);
    }

    public function testIsFullyPaidWithInvoiceUnpaid()
    {
        $invoice = factory(MamipayInvoice::class)->create([
            'invoice_number' => 'INV/2123/2323/23',
            'status' => MamipayInvoice::PAYMENT_STATUS_UNPAID,
            'contract_id' => 213
        ]);
        $repo = new ContractRepositoryEloquent($this->app);
        $result = $repo->isFullyPaid($invoice->contract_id);
        $this->assertFalse($result);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Repositories\ContractRepositoryEloquent
     */
    public function testGetMyRoomHasActiveContract()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        $mamipayContract = $this->createFullValidContractForMyRoom(MamipayContract::STATUS_ACTIVE, $user, $room);

        $contract = $this->contractRepo->getMyRoomContract($user->id);
        $this->assertNotNull($contract);
        $this->assertNotNull($contract->kost);
        $this->assertNotNull($contract->kost->room);
        $this->assertNotNull($contract->kost->room->owners);
        $this->assertNotEmpty($contract->kost->room->owners);
        $this->assertNotNull($contract->invoices);
        $this->assertNotEmpty($contract->invoices);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Repositories\ContractRepositoryEloquent
     */
    public function testGetMyRoomDoesntHaveActiveContract()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        $mamipayContract = $this->createFullValidContractForMyRoom(MamipayContract::STATUS_BOOKED, $user, $room);

        $contract = $this->contractRepo->getMyRoomContract($user->id);
        $this->assertNull($contract);
    }

    /**
     * @group UG
     * @group UG-1219
     * @group App\Repositories\ContractRepositoryEloquent
     *
     * Not sure wether this is possible case or not
     */
    public function testGetMyRoomHasMultipleActiveContract()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        $notLatestContract = $this->createFullValidContractForMyRoom(MamipayContract::STATUS_ACTIVE, $user, $room);
        $latestContract = $this->createFullValidContractForMyRoom(MamipayContract::STATUS_ACTIVE, $user, $room);

        $contract = $this->contractRepo->getMyRoomContract($user->id);
        $this->assertNotNull($contract);
        $this->assertEquals($latestContract->id, $contract->id);
    }

    public function testGetContractListAdminSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();

        // run test
        $contract = $this->contractRepo->getContractListAdmin(null, null, null, null, null, 1);
        $this->assertInstanceOf(LengthAwarePaginator::class, $contract);
    }

    public function testGetContractInvoicesSuccess(): void
    {
        // prepare data
        $contractEntity = factory(MamipayContract::class)->create();
        $invoiceEntity = factory(MamipayInvoice::class)->create([
            'contract_id' => $contractEntity->id
        ]);

        // run test
        $invoices = $this->contractRepo->getContractInvoices($contractEntity->id);
        $firstInvoice = $invoices->first();
        $this->assertInstanceOf(MamipayInvoice::class, $firstInvoice);
    }

    public function testGetContractListUserSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $tenantEntity = factory(MamipayTenant::class)->create([
            'user_id' => $userEntity->id
        ]);
        $contractEntity = factory(MamipayContract::class)->create([
            'tenant_id' => $tenantEntity->id
        ]);

        // run test
        $contract = $this->contractRepo->getContractListUser($userEntity, []);
        $this->assertInstanceOf(LengthAwarePaginator::class, $contract);
    }

    public function testGetContractDetailUserSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $tenantEntity = factory(MamipayTenant::class)->create([
            'user_id' => $userEntity->id
        ]);
        $contractEntity = factory(MamipayContract::class)->create([
            'tenant_id' => $tenantEntity->id
        ]);

        // run test
        $contract = $this->contractRepo->getContractDetailUser($userEntity, $contractEntity->id);
        $this->assertInstanceOf(MamipayContract::class, $contract);
    }

    public function testCountOwnerCurrentMonthUnpaidInvoicesSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $contractEntity = factory(MamipayContract::class)->create([
            'status' => MamipayContract::STATUS_ACTIVE,
            'owner_id' => $userEntity->id,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $contractEntity->id,
            'scheduled_date' => Carbon::now()->format('Y-m-10'),
            'status' => MamipayInvoice::STATUS_UNPAID
        ]);

        // run test
        $data = $this->contractRepo->countOwnerCurrentMonthUnpaidInvoices($userEntity->id);
        $this->assertNotSame(0, $data);
    }

    private function createFullValidContractForMyRoom(string $status, User $user, Room $room): MamipayContract
    {
        $mamipayContract = factory(MamipayContract::class)->create([
            'status' => $status,
            'tenant_id' => factory(MamipayTenant::class)->create([
                'user_id' => $user,
            ])
        ]);
        $room = factory(Room::class)->create([
            'photo_id' => factory(Media::class)->create(),
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => factory(User::class)->create(),
        ]);
        factory(MamipayContractKost::class)->create([
            'contract_id' => $mamipayContract,
            'designer_id' => $room,
        ]);
        factory(MamipayInvoice::class)->create([
            'contract_id' => $mamipayContract,
        ]);
        return $mamipayContract;
    }
}
