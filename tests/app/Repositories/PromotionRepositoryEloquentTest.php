<?php
namespace App\Repositories;

use App;
use App\Test\MamiKosTestCase;
use App\Repositories\PromotionRepositoryEloquent;
use Illuminate\Support\Collection;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\Room;

class PromotionRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = App::make(PromotionRepositoryEloquent::class);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testModel()
    {
        $this->assertEquals(Promotion::class, $this->repository->model());
    }

    public function testUpdatePromoSuccess()
    {
        $promo = factory(Promotion::class)->create([
            "id" => 1,
            "designer_id" => factory(Room::class)->create([
                "id" => 1
            ]),
            "start_date" => date("Y-m-d"),
            "finish_date" => date("Y-m-d"),
            "title" => "New promo",
            "is_show" => "true",
            "verification" => "true",
            "content" => "Content",
        ]);
        
        $endOfPromo = date('Y-m-d', strtotime('+1 day'));
        $updatePromo = $this->repository->updatePromo([
            "designer_id" => 1,
            "title" => "Update promo",
            "content" => "Content",
            "start_date" => date("Y-m-d"),
            "finish_date" => $endOfPromo,
            "verification" => "true",
            "is_show" => "true",
        ]);

        $this->assertEquals($endOfPromo, $updatePromo->finish_date);
    }

    public function testGetPromotionOwnerEmptyData()
    {
        $room = factory(Room::class)->create([
            "id" => 2,
            "song_id" => 2
        ]);

        $ownerPromo = $this->repository->getPromotionOwner($room->song_id);
        
        $promoTotal = 0;
        $this->assertEquals($promoTotal, $ownerPromo['count']);
    }

    public function testGetPromotionOwnerSuccess()
    {   
        $room = factory(Room::class)->create([
            "id" => 3,
            "song_id" => 3
        ]);
        
        $promo = factory(Promotion::class)->create([
            "id" => 2,
            "designer_id" => $room->id,
            "title" => "Promo owner",
            "content" => "Content",
            "start_date" => date("Y-m-d"),
            "finish_date" => date('Y-m-d', strtotime('+1 day')),
            "verification" => "true",
            "is_show" => "true",
        ]);

        $ownerPromo = $this->repository->getPromotionOwner($room->song_id);
        
        $promoTotal = 1;
        $this->assertEquals($promoTotal, $ownerPromo['count']);
    }

    public function testGetPromotionOwnerWhenRoomNotFound()
    {
        $result = $this->repository->getPromotionOwner(0);
        $this->assertEquals(0, $result['count']);
    }

    public function testPromoKostTotal()
    {
        $room = factory(Room::class)->create([
            "id" => 4,
        ]);

        $promoTotal = $this->repository->promoKost($room->id);
        $this->assertEquals(0, $promoTotal);
    }

}