<?php

namespace App\Repositories;

use App\Entities\Testimonial\Testimonial;
use App\Test\MamiKosTestCase;
use Mockery;

use function factory;

class TestimonialRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(TestimonialRepositoryEloquent::class);
    }

    public function testModel()
    {
        $this->assertEquals(Testimonial::class, $this->repository->model());
    }

    public function testGetListTestimonyWithActiveData()
    {
        $testimony1 = factory(Testimonial::class)->create(["is_active" => 1, "id" => 1]);
        $testimony2 = factory(Testimonial::class)->create(["is_active" => 0, "id" => 2]);
        $testimony3 = factory(Testimonial::class)->create(["is_active" => 1, "id" => 3]);

        $response = $this->repository->getListTestimony();

        $this->assertCount(2, $response);
    }

    public function testGetListTestimonyWithNonActiveData()
    {
        factory(Testimonial::class)->create(["is_active" => 0]);

        $response = $this->repository->getListTestimony();

        $this->assertEquals([], $response);
    }

}