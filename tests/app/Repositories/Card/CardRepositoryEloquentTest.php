<?php

namespace App\Repositories;

use App\Test\MamiKosTestCase;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Repositories\Card\CardRepositoryEloquent;

class CardRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(CardRepositoryEloquent::class);
    }
    
    public function testModel()
    {
        $this->assertEquals('App\Entities\Room\Element\Card', $this->repository->model());
    }

    public function testGetModel()
    {
        $result = $this->repository->getModel();
        $this->assertInstanceOf('App\Entities\Room\Element\Card', $result);
    }

    public function testGetListCard()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id'  => $photo->id,
            'is_active' => true
        ]);
        $card   = factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => 'image',
            'photo_id'    => $photo->id,
        ]);

        $response = $this->repository->getListCard($room->id);

        $this->assertEquals($room->id, $response[0]['designer_id']);
        $this->assertEquals('image', $response[0]['type']);
        $this->assertEquals($photo->id, $response[0]['photo_id']);
    }

    public function testGetListCardWithTypeEmpty()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id'  => $photo->id,
            'is_active' => true
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => '',
            'photo_id'    => $photo->id,
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => 'image',
            'photo_id'    => $photo->id,
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => 'video',
            'photo_id'    => $photo->id,
        ]);

        $response = $this->repository->getListCard($room->id);

        $this->assertEquals(2, $response->count());
        $this->assertEquals($room->id, $response[0]['designer_id']);
        $this->assertEquals('', $response[0]['type']);
        $this->assertEquals($photo->id, $response[0]['photo_id']);
    }

    public function testGetListCardExpectGetEmptyArray()
    {
        $photo  = factory(Media::class)->create();
        $room   = factory(Room::class)->create([
            'photo_id'  => $photo->id,
            'is_active' => true
        ]);
        factory(Card::class)->create([
            'designer_id' => $room->id,
            'type'        => 'text',
            'photo_id'    => $photo->id,
        ]);

        $response = $this->repository->getListCard($room->id);
        $this->assertEquals(0, $response->count());
    }
}
