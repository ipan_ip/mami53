<?php 
namespace App\Repositories;

use \App\Entities\Room\Element\RoomTag;
use \App\Entities\Room\RoomTagHistory;
use \App\Repositories\RoomTagHistoryRepositoryEloquent;
use \App\Test\MamiKosTestCase;

class RoomTagHistoryRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setup() : void
    {
        parent::setUp();
        $this->repository = new RoomTagHistoryRepositoryEloquent(app());
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        $this->repository->truncate();
    }

    public function testStore()
    {
        $roomTag = factory(RoomTag::class)->make(['id' => 1]);

        $this->repository->store($roomTag);

        $this->assertNotNull($this->repository->where('id', $roomTag->id)->first());
    }
}
