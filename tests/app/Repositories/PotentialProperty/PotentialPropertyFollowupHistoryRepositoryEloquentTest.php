<?php

namespace App\Repositories\PotentialProperty;

use App\Entities\PotentialProperty\PotentialPropertyFollowupHistory;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use App\Entities\Consultant\PotentialProperty;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class PotentialPropertyFollowupHistoryRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new PotentialPropertyFollowupHistoryRepositoryEloquent(app());
        $this->followupHistoryModel = $this->mock(PotentialOwnerFollowupHistory::class);
    }

    public function testModel(): void
    {
        $this->assertSame($this->repository->model(), PotentialPropertyFollowupHistory::class);
    }

    public function testCreateHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $followupHistory = $this->repository->createHistory($property);
        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNotNull($followupHistory->new_at);
    }

    public function testUpdateHistoryInProgressWithExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $followupHistory = $this->repository->createHistory($property);

        $property->update([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNotNull($followupHistory->new_at);
        $this->assertNotNull($followupHistory->in_progress_at);
    }

    public function testUpdateHistoryInProgressWithoutExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNotNull($followupHistory->in_progress_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryPaidWithoutExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_PAID
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNull($followupHistory->in_progress_at);
        $this->assertNotNull($followupHistory->paid_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryApprovedWithoutExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_APPROVED
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNull($followupHistory->in_progress_at);
        $this->assertNotNull($followupHistory->approved_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryRejectedWithoutExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_REJECTED
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNull($followupHistory->in_progress_at);
        $this->assertNotNull($followupHistory->rejected_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryWithInvalidStatus(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => ''
        ]);

        $followupHistory = $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistory->potential_property_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNull($followupHistory->in_progress_at);
        $this->assertNull($followupHistory->rejected_at);
    }

    public function testUpdateHistoryInProgressWithMultipleExistingHistory(): void
    {
        $property = factory(PotentialProperty::class)->create([
            'consultant_potential_owner_id' => 5,
            'designer_id' => 10,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_REJECTED
        ]);

        $followupHistoryFirst = factory(PotentialPropertyFollowupHistory::class)->create([
            'potential_property_id' => $property->id,
            'created_at' => now()->subMinute()
        ]);

        $followupHistoryLatest = factory(PotentialPropertyFollowupHistory::class)->create([
            'potential_property_id' => $property->id,
        ]);

        $property->update([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
        ]);

        $this->repository->updateHistory($property);

        $this->assertEquals($property->id, $followupHistoryLatest->potential_property_id);

        $this->assertNull($followupHistoryFirst->fresh()->in_progress_at);

        $this->assertNotNull($followupHistoryLatest->fresh()->in_progress_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testCreateHistoryShouldCatchError(): void
    {
        $exception = new Exception();
        $property = factory(PotentialProperty::class)->create([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $history = factory(PotentialPropertyFollowupHistory::class)->make()->toArray();

        $this->followupHistoryModel->shouldReceive('create')
            ->with($history)
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->once();

        $result = $this->repository->createHistory($property);
        $this->assertNull($result);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryShouldCatchError(): void
    {
        $exception = new Exception();
        $property = factory(PotentialProperty::class)->create([
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);

        $history = factory(PotentialPropertyFollowupHistory::class)->make()->toArray();

        $this->followupHistoryModel->shouldReceive('update')
            ->with($history)
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->once();

        $result = $this->repository->updateHistory($property);
        $this->assertNull($result);
    }
}