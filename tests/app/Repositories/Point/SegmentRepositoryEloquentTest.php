<?php

namespace App\Repositories\Point;

use Illuminate\Contracts\Pagination\Paginator;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Point\Point;
use App\Test\MamiKosTestCase;
use App\Repositories\Point\SegmentRepository;

class SegmentRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $segmentRepository;
    
    private $nonSegmentOwner;
    private $nonSegmentTenant;
    private $mamiroomsTenant;
    private $pointSegmentOwner;
    private $pointSegmentTenant;

    private $kostLevel;
    private $roomLevel;

    protected function setup()
    {
        parent::setUp();

        $this->segmentRepository = app()->make(SegmentRepository::class);

        // Non-segment
        $this->nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $this->nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);
        $this->mamiroomsTenant = factory(Point::class)->create([ 'type' => Point::MAMIROOMS_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $this->pointSegmentOwner = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);
        $this->pointSegmentTenant = factory(Point::class)->create([ 'target' => Point::TARGET_TENANT ]);

        $this->kostLevel = factory(KostLevel::class)->create();
        $this->roomLevel = factory(RoomLevel::class)->create();
    }

    public function testGetById_ShouldNotFound()
    {
        $result = $this->segmentRepository->getById(999);

        $this->assertNull($result);
    }

    public function testGetById_ShouldSuccess()
    {
        $result = $this->segmentRepository->getById($this->pointSegmentOwner->id);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($this->pointSegmentOwner->id, $result->id);
        $this->assertEquals($this->pointSegmentOwner->type, $result->type);
        $this->assertEquals($this->pointSegmentOwner->title, $result->title);
        $this->assertEquals($this->pointSegmentOwner->kost_level_id, $result->kost_level_id);
        $this->assertEquals($this->pointSegmentOwner->room_level_id, $result->room_level_id);
    }

    public function testGetByType_ShouldNotFound()
    {
        $result = $this->segmentRepository->getByType('xyz');

        $this->assertNull($result);
    }

    public function testGetByType_ShouldSuccess()
    {
        $result = $this->segmentRepository->getByType($this->pointSegmentOwner->type);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($this->pointSegmentOwner->id, $result->id);
        $this->assertEquals($this->pointSegmentOwner->type, $result->type);
        $this->assertEquals($this->pointSegmentOwner->title, $result->title);
        $this->assertEquals($this->pointSegmentOwner->kost_level_id, $result->kost_level_id);
        $this->assertEquals($this->pointSegmentOwner->room_level_id, $result->room_level_id);
    }

    public function testGetSegmentListPaginated_ShouldSuccess()
    {
        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $result = $this->segmentRepository->getSegmentListPaginated();
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(6, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetSegmentListPaginated_WithTypeSearch_ShouldSuccess()
    {
        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $result = $this->segmentRepository->getSegmentListPaginated(['type' => $this->pointSegmentOwner->type]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());

        $this->assertEquals($result[0]->type, $this->pointSegmentOwner->type);
    }

    public function testGetSegmentListPaginated_WithTargetSearch_ShouldSuccess()
    {
        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $result = $this->segmentRepository->getSegmentListPaginated(['target' => Point::TARGET_OWNER]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetSegmentListPaginated_WithLimitPagination_ShouldSuccess()
    {
        factory(Point::class, 4)->create([ 'target' => Point::TARGET_OWNER ]);

        $result = $this->segmentRepository->getSegmentListPaginated([], 3);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(6, $result->total());
        $this->assertEquals(3, $result->perPage());
    }

    public function testCreateSegmentOwner_ShouldSuccess()
    {
        $data = factory(Point::class)->make([
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'target' => Point::TARGET_OWNER
        ])->toArray();
        
        $result = $this->segmentRepository->createSegment($data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['target'], $result->target);
        $this->assertEquals($data['kost_level_id'], $result->kost_level_id);
        $this->assertEquals($data['room_level_id'], $result->room_level_id);
        $this->assertEquals($this->nonSegmentOwner->value, $result->value);
        $this->assertEquals($this->nonSegmentOwner->expiry_value, $result->expiry_value);
        $this->assertEquals($this->nonSegmentOwner->expiry_unit, $result->expiry_unit);
        $this->assertEquals($this->nonSegmentOwner->tnc, $result->tnc);
    }

    public function testCreateSegmentTenant_ShouldSuccess()
    {
        $data = factory(Point::class)->make([
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'target' => Point::TARGET_TENANT
        ])->toArray();
        
        $result = $this->segmentRepository->createSegment($data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['target'], $result->target);
        $this->assertEquals($data['kost_level_id'], $result->kost_level_id);
        $this->assertEquals($data['room_level_id'], $result->room_level_id);
        $this->assertEquals($this->nonSegmentTenant->value, $result->value);
        $this->assertEquals($this->nonSegmentTenant->expiry_value, $result->expiry_value);
        $this->assertEquals($this->nonSegmentTenant->expiry_unit, $result->expiry_unit);
        $this->assertEquals($this->nonSegmentTenant->tnc, $result->tnc);
    }

    public function testUpdateSegmentOwner_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_OWNER,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $result = $this->segmentRepository->updateSegment($this->pointSegmentOwner, $data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['target'], $result->target);
        $this->assertEquals($data['kost_level_id'], $result->kost_level_id);
        $this->assertEquals($data['room_level_id'], $result->room_level_id);
    }

    public function testUpdateSegmentTenant_ShouldSuccess()
    {
        $data = [
            'type' => 'goldplus_1',
            'title' => 'GoldPlus 1',
            'target' => Point::TARGET_TENANT,
            'kost_level_id' => $this->kostLevel->id,
            'room_level_id' => $this->roomLevel->id,
            'is_published' => 1
        ];

        $result = $this->segmentRepository->updateSegment($this->pointSegmentTenant, $data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Point::class, $result);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['target'], $result->target);
        $this->assertEquals($data['kost_level_id'], $result->kost_level_id);
        $this->assertEquals($data['room_level_id'], $result->room_level_id);
    }
}