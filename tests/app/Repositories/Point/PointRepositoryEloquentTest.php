<?php

namespace App\Repositories\Point;

use App;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Point\Point;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointBlacklistConfig;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Entities\Point\PointUser;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomUnit;
use App\Repositories\OwnerDataRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;

class PointRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $class;
    private $ownerRepo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->ownerRepo = $this->createMock(OwnerDataRepository::class);
        $this->app->instance(OwnerDataRepository::class, $this->ownerRepo);
        $this->class = App::make(PointRepositoryEloquent::class);
    }

    public function testGetUserPointPaginated_CanFilterable()
    {
        $users = factory(User::class, 3)->create([
            'is_owner' => 'true',
        ])->each(function ($user) {
            $user->point_user()->save(factory(PointUser::class)->make([
                'is_blacklisted' => 0,
            ]));
        });

        $paginated = $this->class->getUserPointPaginated(20, [
            'keyword' => $users->first()->name,
            'status' => 'whitelist',
            'user' => 'owner',
            'sortOrder' => 'notAscAndDesc',
        ]);

        $paginatedBlacklisted = $this->class->getUserPointPaginated(20, [
            'keyword' => $users->first()->name,
            'status' => 'blacklist',
            'user' => 'tenant',
            'sortBy' => 'total_point',
            'sortOrder' => 'notAscAndDesc',
        ]);

        $this->assertInstanceOf(Paginator::class, $paginated);
        $this->assertEquals(1, $paginated->total());
        
        $this->assertEquals(0, $paginatedBlacklisted->total());
    }

    public function testAdjustPointUp_ForOwner_ShouldWorks()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $this->class->adjustPoint($pointUser, 10, $notes);
        $pointHistory = $pointUser->user->point_history->first();
        
        $this->assertEquals(15, $pointUser->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointUser->expired_date);
        $this->assertEquals(15, $pointHistory->balance);
        $this->assertEquals(10, $pointHistory->value);
        $this->assertEquals($notes, $pointHistory->notes);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointHistory->expired_date);
    }

    public function testAdjustPointUp_ForTenant_ShouldWorks()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('tenant')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $this->class->adjustPoint($pointUser, 10, $notes);
        $pointHistory = $pointUser->user->point_history->first();
        
        $this->assertEquals(15, $pointUser->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(2)->endOfMonth(), $pointUser->expired_date);
        $this->assertEquals(15, $pointHistory->balance);
        $this->assertEquals(10, $pointHistory->value);
        $this->assertEquals($notes, $pointHistory->notes);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(2)->endOfMonth(), $pointHistory->expired_date);
    }

    public function testAdjustPointUp_ForOwner_UserIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, 10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointUp_ForOwner_MamiroomIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => true
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'value' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, 10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointUp_ForOwner_OwnerLevelIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => false
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_KOST_LEVEL,
            'value' => $kostLevels[0]->id,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, 10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointUp_ForTenant_UserIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('tenant')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, 10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointDown_ForOwner_ShouldWorks()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $this->class->adjustPoint($pointUser, -10, $notes);
        $pointHistory = $pointUser->user->point_history->first();
        
        $this->assertEquals(0, $pointUser->total);
        $this->assertEquals(null, $pointUser->expired_date);
        $this->assertEquals(0, $pointHistory->balance);
        $this->assertEquals(-5, $pointHistory->value);
        $this->assertEquals($notes, $pointHistory->notes);
        $this->assertEquals(null, $pointHistory->expired_date);
    }

    public function testAdjustPointDown_ForTenant_ShouldWorks()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('tenant')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $this->class->adjustPoint($pointUser, -10, $notes);
        $pointHistory = $pointUser->user->point_history->first();
        
        $this->assertEquals(0, $pointUser->total);
        $this->assertEquals(null, $pointUser->expired_date);
        $this->assertEquals(0, $pointHistory->balance);
        $this->assertEquals(-5, $pointHistory->value);
        $this->assertEquals($notes, $pointHistory->notes);
        $this->assertEquals(null, $pointHistory->expired_date);
    }

    public function testAdjustPointDown_ForOwner_UserIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, -10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointDown_ForOwner_MamiroomIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => true
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'value' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, -10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointDown_ForOwner_OwnerLevelIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('owner')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 0,
        ]);

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => false
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_KOST_LEVEL,
            'value' => $kostLevels[0]->id,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, -10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testAdjustPointDown_ForTenant_UserIsBlacklisted_ReturnFalse()
    {
        $notes = 'test note prend';
        $user = factory(User::class)->state('tenant')->create();
        $point = factory(Point::class)->create(['expiry_value' => 1, 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER]);
        $point = factory(Point::class)->create(['expiry_value' => 2, 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT]);
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 5,
            'is_blacklisted' => 1,
        ]);

        $result = $this->class->adjustPoint($pointUser, -10, $notes);
        $this->assertFalse($result);

        $pointHistory = $pointUser->user->point_history->first();
        $this->assertNull($pointHistory);

        $this->assertEquals(5, $pointUser->total);
    }

    public function testToggleBlacklistPointUser_ShouldWorks()
    {
        $user = factory(User::class)->create();
        $pointUser = $user->point_user()->create(factory(PointUser::class)->make([
            'is_blacklisted' => 0,
        ])->toArray());

        $this->class->toggleBlacklistPointUser($pointUser);

        $this->assertEquals(1, $pointUser->is_blacklisted);
    }

    public function testGetActivityListByUserType_ShouldWorks()
    {
        factory(PointActivity::class, 1)->create([
            'target' => 'owner',
            'is_active' => 1
        ]);
        factory(PointActivity::class, 1)->create([
            'target' => 'owner',
            'title' => '',
        ]);

        $result = $this->class->getActivityListByUserType('owner');

        $this->assertEquals(1, count($result));
    }

    public function testGetSettingList_ShouldWorks()
    {
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner'
        ]);

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => true
                ]);
            }
        }

        $result = $this->class->getSettingList();

        $this->assertEquals(6, count($result));
    }

    public function testGetSettingList_WithoutSettings_ShouldWorks()
    {
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivity = factory(PointActivity::class, 3)->create([
            'target' => 'owner'
        ]);

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);

        $result = $this->class->getSettingList();

        $this->assertEquals(0, count($result));
    }

    public function testGetSettingList_WithInactiveSettings_ShouldWorks()
    {
        $ownerRoomGroups = factory(PointOwnerRoomGroup::class, 2)->create();
        $pointActivities = factory(PointActivity::class, 3)->create([
            'target' => 'owner'
        ]);

        // Non-segment
        $nonSegmentOwner = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE, 'target' => Point::TARGET_OWNER ]);
        $nonSegmentTenant = factory(Point::class)->create([ 'type' => Point::DEFAULT_TYPE_TENANT, 'target' => Point::TARGET_TENANT ]);

        // Segment
        $pointSegment = factory(Point::class)->create([ 'target' => Point::TARGET_OWNER ]);

        foreach ($pointActivities as $pointActivity) { 
            foreach ($ownerRoomGroups as $ownerRoomGroup) { 
                // Non-segment
                factory(PointSetting::class)->create([
                    'point_id' => $nonSegmentOwner->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 4,
                    'limit' => 12,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
        
                // Segment
                factory(PointSetting::class)->create([
                    'point_id' => $pointSegment->id,
                    'activity_id' => $pointActivity->id,
                    'room_group_id' => $ownerRoomGroup->id,
                    'received_each' => 8,
                    'limit' => 24,
                    'limit_type' => PointSetting::LIMIT_MONTHLY,
                    'is_active' => false
                ]);
            }
        }

        $result = $this->class->getSettingList();

        $this->assertEquals(0, count($result));
    }

    public function testGetPointHistoryByUser_ShouldWorks()
    {
        $user = factory(User::class)->create();
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => 1,
            'value' => 1,
            'created_at' => Carbon::now()->startOfDay()->subMonthsNoOverflow(2)
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => 1,
            'value' => 1,
            'created_at' => Carbon::now()->subYear()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'redeem_id' => 1,
            'value' => -1,
            'notes' => null,
            'created_at' => Carbon::now()->startOfDay()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'redeem_id' => 1,
            'value' => -1,
            'notes' => null,
            'created_at' => Carbon::now()->subYear()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'redeemable_type' => 'room_contract',
            'redeemable_id' => 1,
            'redeem_value' => 2,
            'value' => -2,
            'created_at' => Carbon::now()->startOfDay()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'redeemable_type' => 'room_contract',
            'redeemable_id' => 1,
            'redeem_value' => 2,
            'value' => -2,
            'created_at' => Carbon::now()->subYear()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'value' => 1,
            'created_at' => Carbon::now()->startOfDay()->subMonthsNoOverflow(2)
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'value' => -1,
            'created_at' => Carbon::now()->startOfDay()
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'value' => -1,
            'notes' => 'Poin Kedaluwarsa',
            'created_at' => Carbon::now()->startOfDay()
        ]);

        $result = $this->class->getPointHistoryByUser($user);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(6, $result->total());
        $this->assertEquals(1, $result[0]->is_recent_of_month);
        $this->assertEquals(1, $result[4]->is_recent_of_month);

        $result = $this->class->getPointHistoryByUser($user, 10, PointHistory::HISTORY_TYPE_EARN);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(2, $result->total());
        $this->assertEquals(1, $result[0]->is_recent_of_month);
        $this->assertEquals(0, $result[1]->is_recent_of_month);

        $result = $this->class->getPointHistoryByUser($user, 10, PointHistory::HISTORY_TYPE_REDEEM);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(2, $result->total());
        $this->assertEquals(1, $result[0]->is_recent_of_month);
        $this->assertEquals(0, $result[1]->is_recent_of_month);

        $result = $this->class->getPointHistoryByUser($user, 10, PointHistory::HISTORY_TYPE_EXPIRED);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(1, $result[0]->is_recent_of_month);
    }

    public function testGetUserPointCountByDate_WithoutHistory_ShouldWorks()
    {
        $user = factory(User::class)->create();
        $pointUser = $user->point_user()->create(factory(PointUser::class)->make([
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ])->toArray());
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);

        $result = $this->class->getUserPointCountByDate($user, $expiredDateCarbon);

        $this->assertNull($result->first());
    }

    public function testGetUserPointCountByDate_WithoutPointUsed_ShouldWorks()
    {
        $user = factory(User::class)->create();
        $pointUser = $user->point_user()->create(factory(PointUser::class)->make([
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ])->toArray());
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 1,
            'expired_date' => $expiredDateCarbon
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 2,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 3,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);

        $result = $this->class->getUserPointCountByDate($user);

        $this->assertEquals(2, count($result));
        $this->assertEquals(2, $result[0]->total_point);
        $this->assertEquals($expiredDateCarbon, $result[0]->expired_date);
    }

    public function testGetUserPointCountByDate_WithPointUsed_ShouldWorks()
    {
        $user = factory(User::class)->create();
        $pointUser = $user->point_user()->create(factory(PointUser::class)->make([
            'is_blacklisted' => 0,
            'expired_date' => Carbon::now()->endOfMonth()
        ])->toArray());
        $expiredDateCarbon = Carbon::createFromTimeString($pointUser->expired_date);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 1,
            'expired_date' => $expiredDateCarbon
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 2,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => 3,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth()
        ]);
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => -4,
            'expired_date' => null
        ]);

        $result = $this->class->getUserPointCountByDate($user);

        $this->assertEquals(1, count($result));
        $this->assertEquals(4, $result[0]->total_point);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $result[0]->expired_date);
    }

    public function testSetNotes_NotesSet()
    {
        $notes = 'test notes';

        $this->class->setNotes($notes);

        $this->assertEquals($notes, $this->getNonPublicAttributeFromClass('notes', $this->class));
    }

    public function testAddPoint_DifferentTarget_ReturnFalse()
    {
        $user = factory(User::class)->state('owner')->make();
        $activity = factory(PointActivity::class)->make(['target' => 'tenant']);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_NoRoomGroup_ReturnFalse()
    {
        $userRoom = 100;
        $user = factory(User::class)->state('owner')->make();
        $activity = factory(PointActivity::class)->make(['target' => 'owner']);
        factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => 10]);

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_NoSettingFound_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        factory(PointSetting::class)->create(
            [
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'is_active' => false
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedOnceLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_ONCE,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedDailyLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedWeeklyLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedMonthlyLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedThreeMonthLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedSixMonthLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_ExceedYearlyLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_NoHistory_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPoint_WithCount_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, null, 3);

        $this->assertTrue($return);
        $this->assertEquals(3, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(3, $this->class->getPointAdded());
    }

    public function testAddPoint_WithCount_PartiallyExceedLimit_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, null, 4);

        $this->assertTrue($return);
        $this->assertEquals(3, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(3, $this->class->getPointAdded());
    }

    public function testAddPoint_WithCount_FullyExceedLimit_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 3,
            'balance' => 3,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, null, 4);

        $this->assertFalse($return);
    }

    public function testAddPoint_WithUpdateHistory_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, null, 1, true);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPoint_PointUserBlacklisted_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $pointUser = factory(PointUser::class)->create(['user_id' => $user->id, 'is_blacklisted' => true]);
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPoint_MamiroomIsBlacklisted_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => true
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'value' => PointBlacklistConfig::USERTYPE_MAMIROOMS,
            'is_blacklisted' => 1,
        ]);

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);
        $this->assertFalse($return);
    }

    public function testAddPoint_OwnerLevelIsBlacklisted_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $kostLevels = factory(KostLevel::class, 2)->create();
        $rooms = factory(Room::class, 2)->create([
            'user_id' => $user->id,
            'is_mamirooms' => false
        ]);

        for ($i=0; $i < 2; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => RoomOwner::ROOM_VERIFY_STATUS
            ]);

            $rooms[$i]->level()->attach($kostLevels[$i]->id);
        }

        factory(PointBlacklistConfig::class)->create([
            'usertype' => PointBlacklistConfig::USERTYPE_KOST_LEVEL,
            'value' => $kostLevels[0]->id,
            'is_blacklisted' => 1,
        ]);

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);
            
        $return = $this->class->addPoint($user, $activity);
        $this->assertFalse($return);
    }

    public function testAddPoint_WithManyTimesReceived_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 3,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->exactly(3))
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(0, $this->class->getPointAdded());

        $return = $this->class->addPoint($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(0, $this->class->getPointAdded());
            
        $return = $this->class->addPoint($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointTenant_DifferentTarget_ReturnFalse()
    {
        $user = factory(User::class)->state('owner')->make();
        $activity = factory(PointActivity::class)->make(['target' => 'tenant']);

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_NoSettingFound_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        factory(PointSetting::class)->create(
            [
                'activity_id' => $activity->id,
                'is_active' => false
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedOnceLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_ONCE,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedDailyLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedWeeklyLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedMonthlyLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedThreeMonthLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedSixMonthLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_ExceedYearlyLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_WithPercentage_ExceedLimit_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000);

        $this->assertTrue($return);
        $this->assertEquals(10, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(10, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WithPercentage_PartiallyExceedLimit_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 6,
            'balance' => 6,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000);

        $this->assertTrue($return);
        $this->assertEquals(4, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(4, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WithPercentage_FullyExceedLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 10,
            'balance' => 10,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_NoHistory_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WhenOnlyCalculating_PointCalculated()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 0, true);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertNull(PointHistory::where('user_id', $user->id)->first());
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WhenOnlyCalculating_ExceedLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 1,
            'balance' => 1,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 0, true);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_WhenOnlyCalculating_WithPercentage_ExceedLimit_PointCalculated()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000, true);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertNull(PointHistory::where('user_id', $user->id)->first());
        $this->assertEquals(10, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WhenOnlyCalculating_WithPercentage_PartiallyExceedLimit_PointCalculated()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 6,
            'balance' => 6,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000, true);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(1, PointHistory::where('user_id', $user->id)->count());
        $this->assertEquals(4, $this->class->getPointAdded());
    }

    public function testAddPointTenant_WhenOnlyCalculating_WithPercentage_FullyExceedLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant', 'key' => PointActivity::TENANT_PAID_BOOKING_ACTIVITY]);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
            'value' => 10,
            'balance' => 10,
            'created_at' => Carbon::now()
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, false, 1000, true);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_PointUserBlacklisted_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $pointUser = factory(PointUser::class)->create(['user_id' => $user->id, 'is_blacklisted' => true]);
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertFalse($return);
    }

    public function testAddPointTenant_WithManyTimesReceived_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $point = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $point->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 3,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 10,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(0, $this->class->getPointAdded());

        $return = $this->class->addPointTenant($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(0, $this->class->getPointAdded());
            
        $return = $this->class->addPointTenant($user, $activity);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->latest()->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->latest()->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointSegment_WithNonGoldplus_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel->id);
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostLevel);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointSegment_WithNonGoldplus_ExceedLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel->id);
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 3,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostLevel);

        $this->assertFalse($return);
    }

    public function testAddPointSegment_NoSegmentSettingFound_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel1->id);
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel3->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointSegment_ExceedLimit_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel1->id);
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel1->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertFalse($return);
    }

    public function testAddPointSegment_WithSingleOwnerActivePackage_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel1->id);
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel1->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertTrue($return);
        $this->assertEquals(3, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(3, $this->class->getPointAdded());
    }

    public function testAddPointSegment_WithMultiOwnerActivePackage_NoSegmentSettingFound_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = $i + 1;
            ${'kostGpLevel'.($i+1)}->save();
        }
        
        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel3->id,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => PointSetting::LIMIT_ROOM_COUNT,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => PointSetting::LIMIT_ROOM_COUNT,
                'is_active' => true
            ]
        );

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddPointSegment_WithMultiOwnerActivePackage_ExceedLimitFromLowerLevelOrder_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            ${'kostGpLevel'.($i+1)}->order = $i + 1;
            ${'kostGpLevel'.($i+1)}->save();
        }

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 2; $i++) { 
            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_OWNER,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'room_group_id' => $roomGroup->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'limit' => $i,
                    'is_active' => true
                ]
            );
        }

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertFalse($return);
    }

    public function testAddPointSegment_WithMultiOwnerActivePackage_ExceedLimitFromHigherLevelOrder_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_OWNER,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'room_group_id' => $roomGroup->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'limit' => $i,
                    'is_active' => true
                ]
            );

            ${'kostGpLevel'.($i+1)}->order = $i + 1;
            ${'kostGpLevel'.($i+1)}->save();
        }

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertFalse($return);
    }

    public function testAddPointSegment_WithMultiOwnerActivePackage_ExceedLimitFromHighestLevelOrder_ReturnFalse()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($kostGpLevel1->id);

            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_OWNER,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'room_group_id' => $roomGroup->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'limit' => $i,
                    'is_active' => true
                ]
            );

            ${'kostGpLevel'.($i+1)}->order = $i + 1;
            ${'kostGpLevel'.($i+1)}->save();
        }

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertFalse($return);
    }

    public function testAddPointSegment_WithMultiOwnerActivePackage_PointAdded()
    {
        $userRoom = 10;
        $user = factory(User::class)->state('owner')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        $activity = factory(PointActivity::class)->create(['target' => 'owner']);
        $roomGroup = factory(PointOwnerRoomGroup::class)->create(['floor' => 0, 'ceil' => $userRoom]);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'room_group_id' => $roomGroup->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach(${'kostGpLevel'.($i+1)}->id);

            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_OWNER,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'room_group_id' => $roomGroup->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_WEEKLY,
                    'limit' => 3 + $i,
                    'is_active' => true
                ]
            );

            ${'kostGpLevel'.($i+1)}->order = $i + 1;
            ${'kostGpLevel'.($i+1)}->save();
        }

        $this->ownerRepo->expects($this->once())
            ->method('getRoomAvailability')
            ->willReturn(['total' => $userRoom]);

        $return = $this->class->addPoint($user, $activity, $kostGpLevel1);

        $this->assertTrue($return);
        $this->assertEquals(3, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(3, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_WithMamirooms_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 2,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, true);

        $this->assertTrue($return);
        $this->assertEquals(2, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(2, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_WhenOnlyCalculating_WithMamirooms_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 2,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_WEEKLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, null, true, 0, true);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertNull(PointHistory::where('user_id', $user->id)->first());
        $this->assertEquals(2, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_WithNonGoldplus_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 2,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_MONTHLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, $kostLevel);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_WithNonGoldplus_ExceedLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostLevel = factory(KostLevel::class)->create();
        $kostGpLevel = factory(KostLevel::class)->state('gp_level_1')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel->id,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 2,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_YEARLY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, $kostLevel);

        $this->assertFalse($return);
    }

    public function testAddTenantPointSegment_NoSettingFound_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $segment = factory(Point::class)->create([
            'kost_level_id' => $kostGpLevel1->id,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 2,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $segment->id,
                'activity_id' => $activity->id,
                'received_each' => 3,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_DAILY,
                'limit' => 3,
                'is_active' => true
            ]
        );

        $return = $this->class->addPointTenant($user, $activity, $kostGpLevel2);

        $this->assertTrue($return);
        $this->assertEquals(1, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(1, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_ExeedLimit_ReturnFalse()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_3_MONTH,
                'limit' => 2,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 2; $i++) { 
            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_TENANT,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_3_MONTH,
                    'limit' => 3,
                    'is_active' => true
                ]
            );
        }

        $return = $this->class->addPointTenant($user, $activity, $kostGpLevel2);

        $this->assertFalse($return);
    }

    public function testAddTenantPointSegment_PointAdded()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 2,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 2; $i++) { 
            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_TENANT,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_6_MONTH,
                    'limit' => 3 + $i,
                    'is_active' => true
                ]
            );
        }

        $return = $this->class->addPointTenant($user, $activity, $kostGpLevel2);

        $this->assertTrue($return);
        $this->assertEquals(4, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), PointUser::where('user_id', $user->id)->first()->expired_date);
        $this->assertEquals(4, $this->class->getPointAdded());
    }

    public function testAddTenantPointSegment_WhenOnlyCalculating_PointCalculated()
    {
        $user = factory(User::class)->state('tenant')->create();
        $kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();

        $activity = factory(PointActivity::class)->create(['target' => 'tenant']);
        $nonSegment = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        $mamirooms = factory(Point::class)->create([
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT,
            'expiry_value' => 1
        ]);
        
        factory(PointSetting::class)->create(
            [
                'point_id' => $nonSegment->id,
                'activity_id' => $activity->id,
                'received_each' => 1,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 1,
                'is_active' => true
            ]
        );
        factory(PointSetting::class)->create(
            [
                'point_id' => $mamirooms->id,
                'activity_id' => $activity->id,
                'received_each' => 2,
                'times_to_received' => 1,
                'limit_type' => PointSetting::LIMIT_6_MONTH,
                'limit' => 2,
                'is_active' => true
            ]
        );

        for ($i=0; $i < 2; $i++) { 
            $segment = factory(Point::class)->create([
                'kost_level_id' => ${'kostGpLevel'.($i+1)}->id,
                'target' => Point::TARGET_TENANT,
                'expiry_value' => 1
            ]);

            factory(PointSetting::class)->create(
                [
                    'point_id' => $segment->id,
                    'activity_id' => $activity->id,
                    'received_each' => 3 + $i,
                    'times_to_received' => 1,
                    'limit_type' => PointSetting::LIMIT_6_MONTH,
                    'limit' => 3 + $i,
                    'is_active' => true
                ]
            );
        }

        $return = $this->class->addPointTenant($user, $activity, $kostGpLevel2, false, 0, true);

        $this->assertTrue($return);
        $this->assertEquals(0, PointUser::where('user_id', $user->id)->first()->total);
        $this->assertNull(PointHistory::where('user_id', $user->id)->first());
        $this->assertEquals(4, $this->class->getPointAdded());
    }
}
