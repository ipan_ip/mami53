<?php

namespace App\Repositories\Mamipay;

use App\Entities\GoldPlus\Package;
use App\Test\MamiKosTestCase;
use App\Repositories\Mamipay\InvoiceRepositoryEloquent;
use App\User;
use App\Entities\Property\PropertyContractOrder;
use App\Entities\Mamipay\Invoice;

class InvoiceRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(InvoiceRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals('App\Entities\Mamipay\Invoice', $this->repository->model());
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Repositories\Mamipay\InvoiceRepositoryEloquent
     */
    public function testGetActiveGoldplusInvoiceIdSuccess(): void
    {
        $user = factory(User::class)->create();
        $contractOrder = factory(PropertyContractOrder::class)->create([
            'owner_id' => $user->id,
            'package_type' => Package::CODE_GP2
        ]);

        $invoice = factory(Invoice::class)->create([
            'order_type' => PropertyContractOrder::ORDER_TYPE,
            'order_id' => $contractOrder->id
        ]);

        $result = $this->repository->getActiveGoldplusInvoiceId($user->id);
        $this->assertEquals($invoice->id, $result);
    }

    /**
     * @group UG
     * @group UG-4913
     * @group App\Repositories\Mamipay\InvoiceRepositoryEloquent
     */
    public function testGetActiveGoldplusInvoiceIdReturnNull(): void
    {
        $user = factory(User::class)->create();

        $result = $this->repository->getActiveGoldplusInvoiceId($user->id);
        $this->assertNull($result);
    }
}