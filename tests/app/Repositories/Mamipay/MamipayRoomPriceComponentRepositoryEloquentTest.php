<?php

namespace App\Repositories\Mamipay;

use App\Test\MamiKosTestCase;
use App;
use Illuminate\Database\Eloquent\Collection;

class MamipayRoomPriceComponentRepositoryEloquentTest extends MamiKosTestCase
{

    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(MamipayRoomPriceComponentRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals('App\Entities\Mamipay\MamipayRoomPriceComponent', $this->repository->model());
    }

    public function testGetListByRoomId(): void
    {
        $result = $this->repository->getListByRoomId(1);

        // run test
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testUpdateSwitchActive(): void
    {
        $result = $this->repository->updateSwitchActive(1, 1, App\Enums\Mamipay\MamipayRoomPriceComponentType::ADDITIONAL);

        // run test
        $this->assertNotNull($result);
    }
}