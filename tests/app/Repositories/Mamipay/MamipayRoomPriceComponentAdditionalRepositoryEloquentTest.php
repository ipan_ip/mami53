<?php

namespace App\Repositories\Mamipay;

use App\Test\MamiKosTestCase;
use App;

class MamipayRoomPriceComponentAdditionalRepositoryEloquentTest extends MamiKosTestCase
{

    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(MamipayRoomPriceComponentAdditionalRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals('App\Entities\Mamipay\MamipayRoomPriceComponentAdditional', $this->repository->model());
    }
}