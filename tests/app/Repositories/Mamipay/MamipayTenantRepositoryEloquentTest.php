<?php

namespace App\Repositories\Mamipay;

use App\Entities\Mamipay\MamipayTenant;
use App\Repositories\Mamipay\MamipayTenantRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App\User;
use RuntimeException;

class MamipayTenantRepositoryEloquentTest extends MamiKosTestCase
{
    
    /** @var MamipayTenantRepositoryEloquent */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(MamipayTenantRepositoryEloquent::class);
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Repositories/Mamipay/MamipayTenantRepositoryEloquent
     */
    public function testSyncMamipayTenantWithUserTenantNull()
    {
        $this->expectException(RuntimeException::class);
        $this->repository->syncMamipayTenantWithUserTenant(null);
        $this->assertTrue(false); // should not reach this
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Repositories/Mamipay/MamipayTenantRepositoryEloquent
     */
    public function testSyncMamipayTenantWithUserTenantEmptyPhoneNumber()
    {
        $this->repository->syncMamipayTenantWithUserTenant(
            factory(User::class)->make([
                'phone_number' => null,
            ])
        );
        $this->assertTrue(true);
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Repositories/Mamipay/MamipayTenantRepositoryEloquent
     */
    public function testSyncMamipayTenantWithUserTenantNoMamipayTenantDataFound()
    {
        /** @var User */
        $tenant = factory(User::class)->create();
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => null,
            'phone_number' => $tenant->phone_number . '12312312',
        ]);
        $this->repository->syncMamipayTenantWithUserTenant($tenant);

        $mamipayTenant->refresh();
        $this->assertNull($mamipayTenant->user);
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Repositories/Mamipay/MamipayTenantRepositoryEloquent
     */
    public function testSyncMamipayTenantWithUserTenantFoundMatchedMamipayTenantExactMatchPhoneNumber()
    {
        /** @var User */
        $tenant = factory(User::class)->create([
            'phone_number' => '08123456789',
        ]);
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => null,
            'phone_number' => $tenant->phone_number,
        ]);
        $this->repository->syncMamipayTenantWithUserTenant($tenant);

        $mamipayTenant->refresh();
        $this->assertNotNull($mamipayTenant->user);
        $this->assertEquals($tenant->id, $mamipayTenant->user->id);
    }

    /**
     * @group UG
     * @group UG-1340
     * @group App/Repositories/Mamipay/MamipayTenantRepositoryEloquent
     */
    public function testSyncMamipayTenantWithUserTenantFoundMatchedMamipayTenantMatchPhoneNumberDifferentPrefix()
    {
        /** @var User */
        $tenant = factory(User::class)->create([
            'phone_number' => '08123456789',
        ]);
        /** @var MamipayTenant */
        $mamipayTenant = factory(MamipayTenant::class)->create([
            'user_id' => null,
            'phone_number' => '+628123456789',
        ]);
        $this->repository->syncMamipayTenantWithUserTenant($tenant);

        $mamipayTenant->refresh();
        $this->assertNotNull($mamipayTenant->user);
        $this->assertEquals($tenant->id, $mamipayTenant->user->id);
    }
}
