<?php

namespace App\Repositories\Goldplus;

use App\Test\MamiKosTestCase;
use App\User;
use App\Repositories\GoldPlus\PackageRepositoryEloquent;
use App\Entities\GoldPlus\Package;
use App\Entities\Level\PropertyLevel;

class PackageRepositoryEloquentTest extends MamikosTestCase
{

    private $packageRepository;
    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->packageRepository = $this->app->make(PackageRepositoryEloquent::class);
        $this->user = factory(User::class)->create(['is_owner' => 'true']);
    }

    /**
     * @group UG
     * @group UG-4714
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testModelShouldReturnValidReturnValue(): void
    {
        $this->assertEquals($this->packageRepository->model(), Package::class);
    }

    /**
     * @group UG
     * @group UG-4714
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testFindByCodeShouldReturnValid(): void
    {
        $code = (string) mt_rand(111, 999);
        $package = factory(Package::class)->create(['code' => $code]);
        $expected = $this->packageRepository->findByCode($code);
        $this->assertEquals($package->first(), $expected->first());
    }

    /**
     * @group UG
     * @group UG-4714
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testFindByCodeShouldReturnNull(): void
    {
        $code = (string) mt_rand(111, 999);
        $package = null;
        $expected = $this->packageRepository->findByCode($code);
        $this->assertEquals($package, $expected);
    }

    /**
     * @group UG
     * @group UG-4714
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testUpdatePropertyLevelByIdShouldReturnOk(): void
    {
        $packageId          = mt_rand();
        $propertyLevelId    = mt_rand();

        $propertyLevel = factory(PropertyLevel::class)->create(['id' => $propertyLevelId]);
        $package = factory(Package::class)->create(['id'   => $packageId, 'property_level_id' => $propertyLevelId]);

        $updatedPropertyLevelId = $propertyLevelId + mt_rand();
        $expected = $this->packageRepository->updatePropertyLevelById($packageId, $updatedPropertyLevelId);
        
        $propertyLevelData = $this->packageRepository->where('property_level_id', $updatedPropertyLevelId)->first();

        $this->assertGreaterThan(0, $expected);
        $this->assertEquals(
            $propertyLevelData->property_level_id, 
            $updatedPropertyLevelId
        );
    }

    /**
     * @group UG
     * @group UG-4714
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testUpdatePropertyLevelByIdExpectRuntimeException(): void
    {
        $packageId          = mt_rand();
        $propertyLevelId    = mt_rand();

        $propertyLevel = factory(PropertyLevel::class)->create(['id' => $propertyLevelId]);
        $package = factory(Package::class)->create(['id'   => $packageId, 'property_level_id' => $propertyLevelId]);

        $this->expectException(\RuntimeException::class, 'User id is invalid.');
        $updatedPropertyLevelId = $propertyLevelId + mt_rand();
        $this->packageRepository->updatePropertyLevelById(null, $updatedPropertyLevelId);
    }

    /**
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testUpdatePriceByIdShouldReturnOk(): void
    {
        $packageId  = mt_rand();
        $price      = mt_rand();

        factory(Package::class)->create(['id' => $packageId, 'price' => $price]);

        $updatedPrice = $price + mt_rand();
        $expected = $this->packageRepository->updatePriceById($packageId, $updatedPrice);
        
        $updatedPackage = $this->packageRepository->find($packageId);

        $this->assertGreaterThan(0, $expected);
        $this->assertEquals($updatedPackage->price, $updatedPrice);
    }

    /**
     * @group App/Repositories/Goldplus/PackageRepositoryEloquent
     */
    public function testUpdatePriceByIdExpectRuntimeException(): void
    {
        $packageId  = mt_rand();
        $price      = mt_rand();

        factory(Package::class)->create(['id' => $packageId, 'price' => $price]);

        $this->expectException(\RuntimeException::class, 'GoldPlus package ID is invalid.');
        $updatedPrice = $price + mt_rand();
        $this->packageRepository->updatePropertyLevelById(null, $updatedPrice);
    }
}