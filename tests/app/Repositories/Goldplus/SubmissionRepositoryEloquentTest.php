<?php

namespace App\Repositories\Goldplus;

use App\Entities\GoldPlus\Submission;
use App\Repositories\GoldPlus\SubmissionRepository;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\GoldPlus\Enums\SubmissionStatus;

class SubmissionRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repo;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = $this->app->make(SubmissionRepository::class);
        $this->user = factory(User::class)->create(['is_owner' => 'true']);
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Goldplus\SubmissionRepositoryEloquentTest
     */
    public function testModel(): void
    {
        $this->assertEquals(Submission::class, $this->repo->model());
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Goldplus\SubmissionRepositoryEloquent
     */
    public function testUpdateSubmissionStatusByUserId(): void
    {
        $submission = factory(Submission::class)->create([
            'user_id' => $this->user->id,
            'status' => SubmissionStatus::NEW
        ]);

        $updated = $this->repo->updateSubmissionStatusByUserId($this->user->id, SubmissionStatus::DONE);

        $this->assertEquals(1, $updated);
        $this->assertDatabaseHas('goldplus_submission', [
            'id' => $submission->id,
            'status' => SubmissionStatus::DONE
        ]);
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Goldplus\SubmissionRepositoryEloquent
     */
    public function testUpdateSubmissionStatusByUserIdWithMultipleSubmissions(): void
    {
        $submission = factory(Submission::class)->create([
            'user_id' => $this->user->id,
            'status' => SubmissionStatus::NEW
        ]);
        $submission2 = factory(Submission::class)->create([
            'user_id' => $this->user->id,
            'status' => SubmissionStatus::NEW
        ]);
        $submission3 = factory(Submission::class)->create(['status' => SubmissionStatus::NEW]);

        $updated = $this->repo->updateSubmissionStatusByUserId($this->user->id, SubmissionStatus::DONE);
        
        $this->assertEquals(2, $updated);
        $this->assertEquals(SubmissionStatus::DONE, $submission->refresh()->status);
        $this->assertEquals(SubmissionStatus::DONE, $submission2->refresh()->status);
        $this->assertEquals(SubmissionStatus::NEW, $submission3->refresh()->status);
    }

    /**
     * @group UG
     * @group UG-4411
     * @group App\Repositories\Goldplus\SubmissionRepositoryEloquent
     */
    public function testUpdateSubmissionStatusByUserIdWithInvalidUserId(): void
    {
        $submission = factory(Submission::class)->create(['status' => SubmissionStatus::NEW]);

        $updated = $this->repo->updateSubmissionStatusByUserId($this->user->id, SubmissionStatus::DONE);
        
        $this->assertEquals(0, $updated);
        $this->assertEquals(SubmissionStatus::NEW, $submission->refresh()->status);
    }
}