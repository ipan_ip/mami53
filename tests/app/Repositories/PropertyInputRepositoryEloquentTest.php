<?php
namespace App\Repositories;

use Illuminate\Support\Collection;
use App\Test\MamiKosTestCase;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Media\PhotoCompleteQueue;
use App\Entities\Refer\Referrer;
use App\Entities\Room\Element\InputTracker;
use App\Entities\Room\Element\RoomPriceComponent;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Review;
use App\User;
use App\Repositories\PropertyInputRepositoryEloquent;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\UserScoreboard;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\RequestInputHelper;
use App\Libraries\AreaFormatter;
use Illuminate\Support\Facades\Hash;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Entities\Consultant\Consultant;
use App\Entities\Room\DataStage;
use phpDocumentor\Reflection\Types\This;
use phpDocumentor\Reflection\Types\Void_;
use App\Entities\Room\Element\Card;

class PropertyInputRepositoryEloquentTest extends MamikosTestCase 
{
    
    private $repository;
    private $owner;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = new PropertyInputRepositoryEloquent(app(), Collection::make());
    }

    /**
     * @group UG
     * @group UG-490
     * @group App/Repositories/PropertyInputRepositoryEloquentTest
     */
    public function testAddRequestBisaBookingSuccess()
    {
        $owner =  factory(User::class)->state('owner')
            ->create([
                "is_verify" => "1",
                "is_owner" => "true"
        ]);

        $designerId = mt_rand(111111, 999999);

        $room = factory(Room::class)->create([
            'id' => $designerId
        ]);

        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $designerId,
            'user_id' => $owner->id,
        ]);
        
        $params = [
            'name'          => $room->name,
            'price_daily'   => mt_rand(11111, 99999),
            'price_monthly' => 0,
            'room_available' => mt_rand(1, 5),
        ];
        $result = $this->repository->addRequestBisaBooking($params, $designerId, $owner);
        
        $this->assertTrue($result);
    }


    /**
     * @group UG
     * @group UG-490
     * @group App/Repositories/PropertyInputRepositoryEloquentTest
     */
    public function testAddRequestBisaBookingWhichUserIsNull()
    {
        $owner =  null;

        $designerId = mt_rand(111111, 999999);

        $room = factory(Room::class)->create([
            'id' => $designerId
        ]);

        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $designerId,
        ]);

        $params = [
            'name'          => $room->name,
            'price_daily'   => mt_rand(11111, 99999),
            'price_monthly' => 0,
            'room_available' => mt_rand(1, 5),
        ];

        $this->expectException(\RuntimeException::class);

        $result = $this->repository->addRequestBisaBooking($params, $designerId, $owner);
    }

    /**
     * @group UG
     * @group UG-490
     * @group App/Repositories/PropertyInputRepositoryEloquentTest
     */
    public function testAddRequestBisaBookingReturnFalse()
    {
        $owner =  factory(User::class)->state('owner')
            ->create([
                "is_verify" => "1",
                "is_owner" => "true"
        ]);

        $designerId = mt_rand(111111, 999999);

        $room = factory(Room::class)->create([
            'id' => $designerId
        ]);

        factory(BookingOwnerRequest::class)->create([
            'designer_id' => $designerId,
            'user_id' => $owner->id.mt_rand(1, 10),
        ]);

        $params = [
            'name'          => $room->name,
            'price_daily'   => mt_rand(11111, 99999),
            'price_monthly' => 0,
            'room_available' => mt_rand(1, 5),
        ];
        $result = $this->repository->addRequestBisaBooking($params, $designerId, $owner);
        
        $this->assertTrue($result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewKostAsPemilikKosAndUpdateOwner()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $newOwner = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $params = factory(Room::class)->make([
            'owner_name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'agent_status' => 'Pemilik Kos',
            'hostility' => $owner->hostility,
            'size' => '3x3',
            'photo_id' => rand(1,99),
            'photo_round_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_remark' => '0',
            'remark' => '0',
            'description' => 'description',
            'manager_name' => $owner->name,
            'manager_phone' => $owner->phone_number,
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'price_quarterly' => 0,
            'price_semiannually' => 0,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_email' => 'agent@mail.com',
            'add_from' => '',
            'building_year' => '2020',
            'agent_status' => 'Pemilik Kos',
            'campaign_source' => ''
        ])->toArray();
        $params['input_as'] = $params['agent_status'];
        $params['room_size'] = $params['size'];
        $params['photos']=[
            'cover' => [$params['photo_id']],
            '360' => $params['photo_round_id'],
        ];
        $id = '95ZLrpmYEVc';
        $link = "https://www.youtube.com/watch?v=$id";
        $params['youtube_link'] = $link;
        $params['autobbk'] = 1;
        $params['fac_property'] = [$tags[0]->id];
        $params['fac_room'] = [$tags[1]->id];
        $params['fac_bath'] = [$tags[2]->id];
        $params['city'] =$params['area_city'];
        $params['subdistrict'] =$params['area_subdistrict'];
        $params['remarks'] =$params['remark'];
        $params['room_size'] =$params['size'];
        $params['owner_email'] =$newOwner->email;        
        $params['password'] ='password';

        $result = $this->repository->saveNewKost($owner, $params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'is_indexed'     => 0,
            'is_promoted'    => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'expired_phone' => 0,
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'agent_status'  => $params['input_as'],
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'][0],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'agent_name'  => $params['agent_name'],
            'agent_phone' => $params['agent_phone'],
            'agent_email' => $params['agent_email'], 
            'add_from' => $params['add_from'],
            'photo_count' => 1,
            'view_count' => 0,
            'campaign_source' => $params['campaign_source'],
            'owner_name'  => $params['owner_name'],
			'owner_phone' => $owner->phone_number,            
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
        $this->assertTrue(Hash::check($params['password'],$owner->password));
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewKostAsPengelolaKos()
    {
        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $params = factory(Room::class)->make([
            'owner_name' => $user->name,
            'phone_number' => $user->phone_number,
            'agent_status' => 'Pemilik Kos',
            'hostility' => $user->hostility,
            'size' => '3x3',
            'photo_id' => rand(1,99),
            'photo_round_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_remark' => '0',
            'remark' => '0',
            'description' => 'description',
            'manager_name' => $user->name,
            'manager_phone' => $user->phone_number,
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'price_quarterly' => 0,
            'price_semiannually' => 0,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_email' => 'agent@mail.com',
            'add_from' => '',
            'building_year' => '2020',
            'campaign_source' => ''
        ])->toArray();
        $params['input_as'] = 'Pengelola Kos';
        $params['room_size'] = $params['size'];
        $params['photos']=[
            'cover' => $params['photo_id'],
            '360' => $params['photo_round_id'],
        ];
        $id = '95ZLrpmYEVc';
        $link = "https://www.youtube.com/watch?v=$id";
        $params['youtube_link'] = $link;
        $params['autobbk'] = 1;
        $params['fac_property'] = [$tags[0]->id];
        $params['fac_room'] = [$tags[1]->id];
        $params['fac_bath'] = [$tags[2]->id];
        $params['password'] =null;
        $params['city'] =$params['area_city'];
        $params['subdistrict'] =$params['area_subdistrict'];
        $params['remarks'] =$params['remark'];
        $params['room_size'] =$params['size'];
        $params['input_source'] ='' ;
        
        $result = $this->repository->saveNewKost($user, $params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'is_indexed'     => 0,
            'is_promoted'    => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'expired_phone' => 0,
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'agent_status'  => $params['input_as'],
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'agent_name'  => $params['agent_name'],
            'agent_phone' => $params['agent_phone'],
            'agent_email' => $params['agent_email'], 
            'add_from' => $params['add_from'],
            'photo_count' => 1,
            'view_count' => 0,
            'campaign_source' => $params['campaign_source'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewKostAsAgen()
    {

        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $params = factory(Room::class)->make([
            'owner_name' => $user->name,
            'phone_number' => $user->phone_number,
            'agent_status' => 'Pemilik Kos',
            'hostility' => $user->hostility,
            'size' => '3x3',
            'photo_id' => rand(1,99),
            'photo_round_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_remark' => '0',
            'remark' => '0',
            'description' => 'description',
            'manager_name' => $user->name,
            'manager_phone' => $user->phone_number,
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'price_quarterly' => 0,
            'price_semiannually' => 0,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_email' => 'agent@mail.com',
            'add_from' => '',
            'building_year' => '2020',
            'campaign_source' => ''
        ])->toArray();
        $params['input_as'] = 'Agen';
        $params['room_size'] = $params['size'];
        $params['photos']=[
            'cover' => $params['photo_id'],
            '360' => $params['photo_round_id'],
        ];
        $id = '95ZLrpmYEVc';
        $link = "https://www.youtube.com/watch?v=$id";
        $params['youtube_link'] = $link;
        $params['autobbk'] = 1;
        $params['fac_property'] = [$tags[0]->id];
        $params['fac_room'] = [$tags[1]->id];
        $params['fac_bath'] = [$tags[2]->id];
        $params['password'] =null;
        $params['city'] =$params['area_city'];
        $params['subdistrict'] =$params['area_subdistrict'];
        $params['remarks'] =$params['remark'];
        $params['room_size'] =$params['size'];
        $params['owner_phone'] =$params['phone_number'];
        $params['input_source'] ='' ;
        
        $result = $this->repository->saveNewKost($user, $params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'is_indexed'     => 0,
            'is_promoted'    => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'expired_phone' => 0,
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'agent_status'  => $params['input_as'],
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'agent_name'  => $params['agent_name'],
            'agent_phone' => $params['agent_phone'],
            'agent_email' => $params['agent_email'], 
            'add_from' => $params['add_from'],
            'photo_count' => 1,
            'view_count' => 0,
            'campaign_source' => $params['campaign_source'],
            'is_visited_kost' => 1,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewKostAsAnakKosWithInputScoreboard()
    {
        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $params = factory(Room::class)->make([
            'owner_name' => $user->name,
            'phone_number' => $user->phone_number,
            'agent_status' => 'Pemilik Kos',
            'hostility' => $user->hostility,
            'size' => '3x3',
            'photo_id' => rand(1,99),
            'photo_round_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_remark' => '0',
            'remark' => '0',
            'description' => 'description',
            'manager_name' => $user->name,
            'manager_phone' => $user->phone_number,
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'price_quarterly' => 0,
            'price_semiannually' => 0,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_email' => 'agent@mail.com',
            'add_from' => '',
            'building_year' => '2020',
            'campaign_source' => ''
        ])->toArray();
        $params['input_as'] = 'Anak Kos';
        $params['room_size'] = $params['size'];
        $params['photos']=[
            'cover' => $params['photo_id'],
            '360' => $params['photo_round_id'],
        ];
        $id = '95ZLrpmYEVc';
        $link = "https://www.youtube.com/watch?v=$id";
        $params['youtube_link'] = $link;
        $params['autobbk'] = 1;
        $params['fac_property'] = [$tags[0]->id];
        $params['fac_room'] = [$tags[1]->id];
        $params['fac_bath'] = [$tags[2]->id];
        $params['password'] =null;
        $params['city'] =$params['area_city'];
        $params['subdistrict'] =$params['area_subdistrict'];
        $params['remarks'] =$params['remark'];
        $params['room_size'] =$params['size'];
        $params['owner_phone'] =$params['phone_number'];
        $params['input_source'] ='input-scoreboard' ;
        
        $result = $this->repository->saveNewKost($user, $params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'is_indexed'     => 0,
            'is_promoted'    => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'expired_phone' => 0,
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'agent_status'  => $params['input_as'],
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'agent_name'  => $params['agent_name'],
            'agent_phone' => $params['agent_phone'],
            'agent_email' => $params['agent_email'], 
            'add_from' => $params['add_from'],
            'photo_count' => 1,
            'view_count' => 0,
            'campaign_source' => $params['campaign_source'],
        ];

        $scoreBoard = [
            'type' => UserScoreboard::TYPE_INPUT,
            'user_id' => !is_null($user) ? $user->id : nulll,
            'identifier' => $result->id,
            'description' => 'Input ' . ApiHelper::removeEmoji($params['name']),
            'status' => UserScoreboard::STATUS_NEW,
            'point' => UserScoreboard::POINT_NEW,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
        $this->assertDatabaseHas('user_scoreboard',$scoreBoard);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateKostByOwnerAsPemilikKosAndUpdateOwner()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $newOwner = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $id = 'U3gBa4-LF6k';
        $params = [
            'name' => 'update name',
            'address' => 'update address',
            'latitude' => 1.1,
            'longitude' => 1.1,
            'gender' => 1,
            'room_available' => 3,
            'room_count' => 5,
            'city' => 'update area',
            'subdistrict' => 'update subdistrict',
            'price_daily' => '10000',
            'price_weekly' => '700000',
            'price_monthly' => '3000000',
            'price_yearly' => '36000000',
            'price_quarterly' => '9000000',
            'price_semiannually' => '18000000',
            'room_size' => '5x5',    
            'price_remark' => '20000',    
            'description' => 'update description',    
            'remarks' => 'update remarks',
            'owner_name' => $newOwner->name,
            'owner_phone' => $newOwner->phone_number,
            'manager_name' => $newOwner->name,
            'manager_phone' => $newOwner->phone_number,
            'building_year' =>  '2021',
            'photos' => [
                    'cover' => [1],
                    '360' => 2,    
            ],
            'youtube_link' => "https://youtu.be/$id",
            'fac_property' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'fac_bath' => [$tags[2]->id],
            'concern_ids' => [$tags[3]->id], 
        ];

        collect(DataStage::AVAILABLE_STAGES)->map(function ($stageName) use ($room) {
            factory(DataStage::class)->state('complete')->create([
                'designer_id' => $room->id,
                'name' => $stageName
            ]);
        });

        $secondRoom = factory(Room::class)->states([
            'attribute-complete-state',
            'active',
            'availableBooking',
            'notTesting',
            'with-all-prices',
            'with-small-rooms',
            'with-unique-code',
            'with-address-note'
        ])->create();

        collect(DataStage::AVAILABLE_STAGES)->map(function ($stageName) use ($secondRoom) {
            factory(DataStage::class)->state('complete')->create([
                'designer_id' => $secondRoom->id,
                'name' => $stageName
            ]);
        });

        $this->assertEquals(count(DataStage::AVAILABLE_STAGES), $room->data_stage->count());

        $result = $this->repository->updateKostByOwner($room,$roomOwner, $params);

        $room->refresh();
        $this->assertEquals(0, $room->data_stage->count(), 'Database records deleted');
        $this->assertDatabaseHas(
            (new DataStage())->getTable(),
            ['designer_id' => $secondRoom->id]
        );
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'][0],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'photo_count' => 1,
            'owner_name'  => $params['owner_name'],
			'owner_phone' => $newOwner->phone_number,            
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateKostByOwnerAsPemilikKosAndUpdateOwnerWithout360photoAndPhotoCoverIsNotArray()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $newOwner = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $id = 'U3gBa4-LF6k';
        $params = [
            'name' => 'update name',
            'address' => 'update address',
            'latitude' => 1.1,
            'longitude' => 1.1,
            'gender' => 1,
            'room_available' => 3,
            'room_count' => 5,
            'city' => 'update area',
            'subdistrict' => 'update subdistrict',
            'price_daily' => '10000',
            'price_weekly' => '700000',
            'price_monthly' => '3000000',
            'price_yearly' => '36000000',
            'price_quarterly' => '9000000',
            'price_semiannually' => '18000000',
            'room_size' => '5x5',    
            'price_remark' => '20000',    
            'description' => 'update description',    
            'remarks' => 'update remarks',
            'owner_name' => $newOwner->name,
            'owner_phone' => $newOwner->phone_number,
            'manager_name' => $newOwner->name,
            'manager_phone' => $newOwner->phone_number,
            'building_year' =>  '2021',
            'photos' => [
                    'cover' => 1,
                    '360' => '',    
            ],
            'youtube_link' => "https://youtu.be/$id",
            'fac_property' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'fac_bath' => [$tags[2]->id],
            'concern_ids' => [$tags[3]->id], 
        ];

        $result = $this->repository->updateKostByOwner($room,$roomOwner, $params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'],
            'photo_round_id' => $params['photos']['360'] ?? null,
            'youtube_id' => $id,
            'photo_count' => 1,
            'owner_name'  => $params['owner_name'],
			'owner_phone' => $newOwner->phone_number,            
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    public function testsaveNewKostSimple()
    {       
        $owner = factory(User::class)->create([]);

        $params = factory(Room::class)->make([
            'owner_name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'agent_status' => 'Pemilik Kos',
            'photo_id' => rand(1,99),
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_status' => 'Pemilik Kos',
        ])->toArray();
        $params['input_as'] = $params['agent_status'];
        $params['input_source'] = 'source';
        $params['owner_phone'] = $params['phone_number'];
        $params['photos']=[
            'cover' => [$params['photo_id']],
        ];

        $result = $this->repository->saveNewKostSimple($params);
        
        $expectedResult= [
            'name'         => ApiHelper::removeEmoji($params['name']),
            'address'      => ApiHelper::removeEmoji($params['address']),
            'owner_phone' 	=> $params['owner_phone'],    
            'agent_status' => $params['input_as'],
            'agent_name' => $params['agent_name'],
            'agent_phone' => $params['agent_phone'],
            'photo_id' => $params['photos']['cover'][0],          
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
    
        $this->assertDatabaseHas('designer',$expectedResult);
        $this->assertEquals($expectedResult,$result);
    }

    public function testsaveNewKostReferral()
    {   
        
        $user = factory(User::class)->create([]);
        $referrer = factory(Referrer::class)->create(['user_id'=>$user->id]);
        $owner = factory(User::class)->create([]);

        $params = factory(Room::class)->make([
            'owner_name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'photo_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_status' => 'Pengelola Kos',
        ])->toArray();
        $params['owner_phone'] = $params['phone_number'];
        $params['price'] = $params['price_monthly'];
        $params['input_as'] = $params['agent_status'];
        $params['city'] = $params['area_city'];
        $params['subdistrict'] = $params['area_subdistrict'];
        $params['photos']=[
            'cover' => $params['photo_id'],
        ];
        $params['review'] = 'review';
        $params['referrer'] = $referrer;
        $params['user'] = $user;

        $result = $this->repository->saveNewKostReferral($params);
        
        $expectedResult= [
            'name'         => ApiHelper::removeEmoji($params['name']),
            'address'      => ApiHelper::removeEmoji($params['address']),
            'owner_phone' 	=> $params['owner_phone'],
            'price_monthly' => $params['price'],
            'latitude'     => $params['latitude'],
            'longitude'    => $params['longitude'],
            'agent_status' => $params['input_as'],
            'agent_name'   => $params['agent_name'],
            'agent_phone'  => $params['agent_phone'],
            'area_city'    => $params['city'],
            'area_subdistrict' => $params['subdistrict'],
            'price_daily'  => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly'=> $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
			'photo_id' => $params['photos']['cover'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
    
        $this->assertDatabaseHas('designer',$expectedResult);
        $this->assertEquals($expectedResult,$result);
    }

    public function testsaveNewKostGeneral()
    {   
        
        $owner = factory(User::class)->create([]);

        $params = factory(Room::class)->make([
            'owner_name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'photo_id' => rand(1,99),
            'latitude' => '0',
            'longitude' => '0',
            'area_city' => 'Jakarta',
            'area_subdistrict' => 'subdistrict',
            'price_daily' => 200000,
            'price_weekly' => 1400000,
            'price_monthly' => 60000000,
            'price_yearly' => 720000000,
            'agent_name' => 'agent 1',
            'agent_phone' => '0811111111',
            'agent_status' => 'Pengelola Kos',
        ])->toArray();
        $params['owner_phone'] = $params['phone_number'];
        $params['price'] = $params['price_monthly'];
        $params['input_as'] = $params['agent_status'];
        $params['city'] = $params['area_city'];
        $params['subdistrict'] = $params['area_subdistrict'];
        $params['photos']=[
            'cover' => $params['photo_id'],
        ];
        $params['input_source'] = 'source';

        $result = $this->repository->saveNewKostGeneral($owner,$params);
        
        $expectedResult= [
            'name'         => ApiHelper::removeEmoji($params['name']),
            'address'      => ApiHelper::removeEmoji($params['address']),
            'owner_phone' 	=> $params['owner_phone'],
            'latitude'     => $params['latitude'],
            'longitude'    => $params['longitude'],
            'agent_status' => $params['input_as'],
            'agent_name'   => $params['agent_name'],
            'agent_phone'  => $params['agent_phone'],
            'area_city'    => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_daily'  => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly'=> $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
			'photo_id' => $params['photos']['cover'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
    
        $this->assertDatabaseHas('designer',$expectedResult);
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitAsPemilikApartemenWithProjectNameParamAndOwnerUpdate()
    {
        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        
        $youtubeId='abJasha7';
        $params = [
            'project_name' => 'apartment project',
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'input_as' => 'Pemilik Apartemen',
            'owner_name' => $user->name,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'description' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
            'owner_email' => $user->email,
            'password' => 'password',
            'input_source' => 'source',
        ];

        $result = $this->repository->saveNewApartmentUnit($user,$params);

        $expectedApartmentProject= [
			'name' => $params['project_name'],
			'is_active' => 0,
        ];

        $expectedRoomOwner= [
			'designer_id' => $result->id,
            'user_id' => $user->id,
            'owner_status' => $params['input_as'],
            'status' => 'add',
            'promoted_status' => 'false',
        ];

        $expectedUser = [
            'id' => $user->id,
            'email' => $params['owner_email'],
        ];

        $expectedResult = [
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'owner_name' => $params['owner_name'],
            'price_daily' => is_numeric($params['price_daily']) ? $params['price_daily'] : 0,
            'price_weekly' => is_numeric($params['price_weekly']) ? $params['price_weekly'] : 0,
            'price_monthly' => is_numeric($params['price_monthly']) ? $params['price_monthly'] : 0,
            'price_yearly' => is_numeric($params['price_yearly']) ? $params['price_yearly'] : 0,    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,    
            'payment_duration' => !is_null($params['price_shown']) ? $params['price_shown'] : null,
            'description' => $params['description'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
			'photo_id' => $params['photos']['cover'],
			'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $youtubeId,
            'add_from' => $params['add_from'],
            'campaign_source' => $params['campaign_source'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('apartment_project',$expectedApartmentProject);
        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertDatabaseHas('user',$expectedUser);
        $this->assertTrue(Hash::check($params['password'],$user->password));
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitAsPemilikApartemenWithProjectIdParam()
    {
        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData();

        $apartmentProject = factory(ApartmentProject::class)->create();
                
        $youtubeId='abJasha7';
        $params = [
            'project_name' => '',
            'project_id' => $apartmentProject->id,
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'input_as' => 'Pemilik Apartemen',
            'owner_name' => $user->name,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'description' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
            'owner_email' => $user->email,
            'password' => 'password',
            'input_source' => 'source',
        ];

        $result = $this->repository->saveNewApartmentUnit($user,$params);

        $expectedRoomOwner= [
			'designer_id' => $result->id,
            'user_id' => $user->id,
            'owner_status' => $params['input_as'],
            'status' => 'add',
            'promoted_status' => 'false',
        ];

        $expectedUser = [
            'id' => $user->id,
            'email' => $params['owner_email'],
        ];

        $expectedResult = [
            'apartment_project_id' => $apartmentProject->id,
            'address' => $apartmentProject->address,
            'area_city' => AreaFormatter::format($apartmentProject->area_city),
            'area_subdistrict' => AreaFormatter::format($apartmentProject->area_subdistrict),
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'owner_name' => $params['owner_name'],
            'price_daily' => is_numeric($params['price_daily']) ? $params['price_daily'] : 0,
            'price_weekly' => is_numeric($params['price_weekly']) ? $params['price_weekly'] : 0,
            'price_monthly' => is_numeric($params['price_monthly']) ? $params['price_monthly'] : 0,
            'price_yearly' => is_numeric($params['price_yearly']) ? $params['price_yearly'] : 0,    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,    
            'payment_duration' => !is_null($params['price_shown']) ? $params['price_shown'] : null,
            'description' => $params['description'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
			'photo_id' => $params['photos']['cover'],
			'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $youtubeId,
            'add_from' => $params['add_from'],
            'campaign_source' => $params['campaign_source'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertDatabaseHas('user',$expectedUser);
        $this->assertTrue(Hash::check($params['password'],$user->password));
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitAsPengelolaApartemenWithProjectNameParam()
    {

        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
                
        $youtubeId='abJasha7';
        $params = [
            'project_name' => 'apartment project',
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'manager_phone' => $user->phone,
			'agent_status' => 'Pengelola Apartemen',
            'input_as' => 'Pengelola Apartemen',
            'owner_name' => $user->name,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'description' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
            'owner_email' => $user->email,
            'password' => '',
            'input_source' => 'source',
        ];

        $result = $this->repository->saveNewApartmentUnit($user,$params);

        $expectedApartmentProject= [
			'name' => $params['project_name'],
			'is_active' => 0,
        ];

        $expectedRoomOwner= [
			'designer_id' => $result->id,
            'owner_status' => $params['input_as'],
            'status' => 'add',
            'promoted_status' => 'false',
        ];

        $expectedResult = [
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'price_daily' => is_numeric($params['price_daily']) ? $params['price_daily'] : 0,
            'price_weekly' => is_numeric($params['price_weekly']) ? $params['price_weekly'] : 0,
            'price_monthly' => is_numeric($params['price_monthly']) ? $params['price_monthly'] : 0,
            'price_yearly' => is_numeric($params['price_yearly']) ? $params['price_yearly'] : 0,    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,    
            'payment_duration' => !is_null($params['price_shown']) ? $params['price_shown'] : null,
            'description' => $params['description'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
			'photo_id' => $params['photos']['cover'],
			'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $youtubeId,
            'add_from' => $params['add_from'],
            'campaign_source' => $params['campaign_source'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('apartment_project',$expectedApartmentProject);
        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitAsAgenApartemenWithProjectNameParam()
    {
        [$user,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        
        $youtubeId='abJasha7';
        $params = [
            'project_name' => 'apartment project',
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'agent_phone' => $user->phone,
            'agent_status' => 'Agen Apartemen',
            'agent_email' => $user->email,
            'input_as' => 'Agen Apartemen',
            'owner_name' => $user->name,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'description' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
            'owner_email' => $user->email,
            'password' => '',
            'input_source' => 'source',
        ];

        $result = $this->repository->saveNewApartmentUnit($user,$params);

        $expectedApartmentProject= [
			'name' => $params['project_name'],
			'is_active' => 0,
        ];

        $expectedRoomOwner= [
			'designer_id' => $result->id,
            'owner_status' => $params['input_as'],
            'status' => 'add',
            'promoted_status' => 'false',
        ];

        $expectedResult = [
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'price_daily' => is_numeric($params['price_daily']) ? $params['price_daily'] : 0,
            'price_weekly' => is_numeric($params['price_weekly']) ? $params['price_weekly'] : 0,
            'price_monthly' => is_numeric($params['price_monthly']) ? $params['price_monthly'] : 0,
            'price_yearly' => is_numeric($params['price_yearly']) ? $params['price_yearly'] : 0,    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,    
            'payment_duration' => !is_null($params['price_shown']) ? $params['price_shown'] : null,
            'description' => $params['description'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
			'photo_id' => $params['photos']['cover'],
			'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $youtubeId,
            'add_from' => $params['add_from'],
            'campaign_source' => $params['campaign_source'],
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('apartment_project',$expectedApartmentProject);
        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitByAdminWithIsAdminIsTrue()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        $apartmentProject = factory(ApartmentProject::class)->create();
        
        $youtubeId='abJasha7';
        $params = [
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'input_as' => 'Pengelola Apartemen',
            'input_name' => 'Agent Name',
            'owner_name' => $owner->name,
            'owner_phone' => $owner->phone,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'description' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'is_indexed' => 1,
            'description' => 'description',
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'is_admin' => true,
            'facilities' => [$tags[0]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $this->repository->saveNewApartmentUnitByAdmin($apartmentProject,$params);

        $expectedResult = [
            'apartment_project_id' => $apartmentProject->id,
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'address' => $apartmentProject->address,
            'latitude' => $apartmentProject->latitude,
            'longitude' => $apartmentProject->longitude,
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'gender' => 0, 
            'room_available' => 1, 
            'room_count' => 1,
            'status' => 0,
            'is_active' => 'false',
            'is_promoted' => 'false',
            'area_city' => AreaFormatter::format($apartmentProject->area_city),
            'area_subdistrict' => AreaFormatter::format($apartmentProject->area_subdistrict),  
            'owner_name' => $params['owner_name'],
            'owner_phone' => $params['owner_phone'],
            'agent_status' => $params['input_as'],
            'agent_name' => $params['input_name'],    
            'expired_phone' => 0,
            'is_booking' => 0,    
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
            'is_indexed' => $params['is_indexed'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover']
        ];

        $this->assertDatabaseHas('designer',$expectedResult);
        $this->assertDatabaseHas('designer_tag',[
            'tag_id' => $tags[0]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'tag_id' => $tags[4]->id,
        ]);
        
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testsaveNewApartmentUnitByAdmin()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        $apartmentProject = factory(ApartmentProject::class)->create();
        
        $youtubeId='abJasha7';
        $params = [
            'name' => 'room_name',
            'unit_number' => '000',
            'floor' => '5',
            'unit_type' => 'unit type',
            'unit_type_name' => 'unit type name',
            'input_as' => 'Pengelola Apartemen',
            'input_name' => 'Agent Name',
            'owner_name' => $owner->name,
            'owner_phone' => $owner->phone,
            'price_daily' => 0,
            'price_weekly' => 0,
            'price_monthly' => 0,
            'price_yearly' => 0,
            'price_daily_usd' => 0,
            'price_weekly_usd' => 0,
            'price_monthly_usd' => 0,
            'price_yearly_usd' => 0,
            'price_shown' => 0,
            'is_furnished' => 0,
            'unit_size' => 0,
            'is_indexed' => 1,
            'description' => 'description',
            'photos' => [
                'cover' => 1,
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'add_from' => 'add form',
            'campaign_source' => 'campaign source',
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'facilities' => [$tags[0]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $this->repository->saveNewApartmentUnitByAdmin($apartmentProject,$params);

        $expectedResult = [
            'apartment_project_id' => $apartmentProject->id,
            'name' => ApiHelper::removeEmoji($params['name']),
            'unit_number' => $params['unit_number'],
            'address' => $apartmentProject->address,
            'latitude' => $apartmentProject->latitude,
            'longitude' => $apartmentProject->longitude,
            'floor' => $params['floor'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'gender' => 0, 
            'room_available' => 1, 
            'room_count' => 1,
            'status' => 0,
            'is_active' => 'false',
            'is_promoted' => 'false',
            'area_city' => AreaFormatter::format($apartmentProject->area_city),
            'area_subdistrict' => AreaFormatter::format($apartmentProject->area_subdistrict),  
            'owner_name' => $params['owner_name'],
            'owner_phone' => $params['owner_phone'],
            'agent_status' => $params['input_as'],
            'agent_name' => $params['input_name'],    
            'expired_phone' => 0,
            'is_booking' => 0,    
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'furnished' => (int)$params['is_furnished'],
            'size' => $params['unit_size'],
            'is_indexed' => $params['is_indexed'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover']
        ];

        $this->assertDatabaseHas('designer',$expectedResult);
        $this->assertDatabaseHas('designer_tag',[
            'tag_id' => $tags[4]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'tag_id' => $tags[0]->id,
        ]);
        
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateApartmentUnitByOwnerWithPhotosCoverIsArray()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        
        $youtubeId='abJasha7';
        $params = [
            'name' => 'update name',
            'unit_number' => '000',
            'price_daily' => 999,
            'price_weekly' => 999,
            'price_monthly' => 999,
            'price_yearly' => 999,
            'price_daily_usd' => 999,
            'price_weekly_usd' => 999,
            'price_monthly_usd' => 999,
            'price_yearly_usd' => 999,
            'price_shown' => 999,
            'floor' => '5',
            'unit_size' => '5 x 5',
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'Unit lainnya',
            'is_furnished' => 0,
            'description' => 'update description',
            'photos' => [
                'cover' => [1],
                '360' =>2
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $result = $this->repository->updateApartmentUnitByOwner($room,$roomOwner, $params);


        $expectedRoomOwner= [
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'draft2',
        ];

        $expectedResult = [
            'name' => $params['name'],
            'unit_number' => $params['unit_number'],
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'floor' => $params['floor'],
            'is_active' => 'false',
            'size' => $params['unit_size'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'furnished' => (int)$params['is_furnished'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover'][0],
            'photo_round_id' => $params['photos']['360'],
			'youtube_id' => $youtubeId,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);

        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[4]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[1]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[0]->id,
        ]);

    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateApartmentUnitByOwnerWithPhotosCoverIsNotArrayAndPhotos360IsNull()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 

        $youtubeId='abJasha7';
        $params = [
            'name' => 'update name',
            'unit_number' => '000',
            'price_daily' => 999,
            'price_weekly' => 999,
            'price_monthly' => 999,
            'price_yearly' => 999,
            'price_daily_usd' => 999,
            'price_weekly_usd' => 999,
            'price_monthly_usd' => 999,
            'price_yearly_usd' => 999,
            'price_shown' => 999,
            'floor' => '5',
            'unit_size' => '5 x 5',
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'Unit lainnya',
            'is_furnished' => 0,
            'description' => 'update description',
            'photos' => [
                'cover' => 1,
                '360' =>''
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $result = $this->repository->updateApartmentUnitByOwner($room,$roomOwner, $params);


        $expectedRoomOwner= [
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'draft2',
        ];

        $expectedResult = [
            'name' => $params['name'],
            'unit_number' => $params['unit_number'],
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'floor' => $params['floor'],
            'is_active' => 'false',
            'size' => $params['unit_size'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'furnished' => (int)$params['is_furnished'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover'],
            'photo_round_id' => null,
			'youtube_id' => $youtubeId,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);

        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[4]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[1]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[0]->id,
        ]);

    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateApartmentUnitByOwnerWithExistingRoomPriceParkingAndMaintenance()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 
        
        $this->createRoomPriceComponent($room->id);
        
        $youtubeId='abJasha7';
        $params = [
            'name' => 'update name',
            'unit_number' => '000',
            'price_daily' => 999,
            'price_weekly' => 999,
            'price_monthly' => 999,
            'price_yearly' => 999,
            'price_daily_usd' => 999,
            'price_weekly_usd' => 999,
            'price_monthly_usd' => 999,
            'price_yearly_usd' => 999,
            'price_shown' => 999,
            'floor' => '5',
            'unit_size' => '5 x 5',
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'Unit lainnya',
            'is_furnished' => 0,
            'description' => 'update description',
            'photos' => [
                'cover' => 1,
                '360' =>''
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'price_type' => 'usd',
            'maintenance_price' => 200000,
            'parking_price' => 50000,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $result = $this->repository->updateApartmentUnitByOwner($room,$roomOwner, $params);

        $expectedRoomOwner= [
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'draft2',
        ];

        $expectedResult = [
            'name' => $params['name'],
            'unit_number' => $params['unit_number'],
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'floor' => $params['floor'],
            'is_active' => 'false',
            'size' => $params['unit_size'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'furnished' => (int)$params['is_furnished'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover'],
            'photo_round_id' => null,
			'youtube_id' => $youtubeId,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);

        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[4]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[1]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[0]->id,
        ]);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateApartmentUnitByOwnerWithWithMaintenancePriceAndParkingPriceIsNull()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(); 

        $this->createRoomPriceComponent($room->id);
        
        $youtubeId='abJasha7';
        $params = [
            'name' => 'update name',
            'unit_number' => '000',
            'price_daily' => 999,
            'price_weekly' => 999,
            'price_monthly' => 999,
            'price_yearly' => 999,
            'price_daily_usd' => 999,
            'price_weekly_usd' => 999,
            'price_monthly_usd' => 999,
            'price_yearly_usd' => 999,
            'price_shown' => 999,
            'floor' => '5',
            'unit_size' => '5 x 5',
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'Unit lainnya',
            'is_furnished' => 0,
            'description' => 'update description',
            'photos' => [
                'cover' => 1,
                '360' =>''
            ],
            'youtube_link' => "https://youtu.be/$youtubeId",
            'price_type' => 'usd',
            'maintenance_price' => null,
            'parking_price' => null,
            'fac_unit' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'min_payment' => [$tags[4]->id],
        ];

        $result = $this->repository->updateApartmentUnitByOwner($room,$roomOwner, $params);

        $expectedRoomOwner= [
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'draft2',
        ];

        $expectedResult = [
            'name' => $params['name'],
            'unit_number' => $params['unit_number'],
            'price_daily' => $params['price_daily'],
            'price_weekly' => $params['price_weekly'],
            'price_monthly' => $params['price_monthly'],
            'price_yearly' => $params['price_yearly'],
    
            'price_daily_usd' => is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0,
            'price_weekly_usd' => is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0,
            'price_monthly_usd' => is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0,
            'price_yearly_usd' => is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0,
            'payment_duration' => $params['price_shown'],
            'floor' => $params['floor'],
            'is_active' => 'false',
            'size' => $params['unit_size'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'furnished' => (int)$params['is_furnished'],
            'description' => $params['description'],
			'photo_id' => $params['photos']['cover'],
            'photo_round_id' => null,
			'youtube_id' => $youtubeId,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);

        $this->assertDatabaseHas('designer_owner',$expectedRoomOwner);
        $this->assertEquals($expectedResult,$result);

        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[4]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[1]->id,
        ]);
        $this->assertDatabaseHas('designer_tag',[
            'designer_id' => $room->id,
            'tag_id' => $tags[0]->id,
        ]);
    }

    public function testSavePhotos()
    {
        $room = factory(Room::class)->create();
        $photos = [
			'cover'=>[1,2,3,4,5],
			'main'=>[6,7,8,9,null],
			'bedroom'=> null,
			'bath'=> 10,
            '360' => null
        ];
        $photos['other'] = [
            [
                'id' => 11,
                'description' => 1,
            ],
        ];
        
        $this->repository->savePhotos($photos,$room);
        
        $descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
        ];
        
        $expectedPhotos = [
            'designer_id' => $room->id,
        ];
        $expectedRoom = [
            'id' => $room->id,
            'photo_count' => 0
        ];
        foreach($photos as $key => $photo) {
            if($key == '360') {
                continue;
            }

            if(is_array($photo)) {
                foreach($photo as $keyPhoto => $photoId) {
                    if(is_null($photoId)) {
                        continue;
                    }

                    if($key != 'other') {
                        $expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . '-' . ($keyPhoto + 1);
                        $expectedPhotos['photo_id'] = $photoId;
                    } else {
                        $expectedPhotos['description'] = $room->name . '-' . $photoId['description'] . '-' . ($keyPhoto + 1);
                        $expectedPhotos['photo_id'] = $photoId['id'];
                    }
                    
                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
                }
            } else {
                if(!is_null($photo)) {
                    $expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
                    $expectedPhotos['photo_id'] = $photo;
                    
                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
                }
            }

        }

        $this->assertDatabaseHas('designer',$expectedRoom);
    }

    public function testSavePhotosFromAdmin()
    {
        $room = factory(Room::class)->create();
        $photos = [
			'cover'=>[1,2,3,4,5],
			'main'=>[6,7,8,9,null],
			'bedroom'=> null,
			'bath'=> 10,
            'other' => [11],
            '360' => null
        ];
        
        $this->repository->savePhotosFromAdmin($photos,$room);
        
        $descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
        ];
        
        $expectedPhotos = [
            'designer_id' => $room->id,
        ];
        $expectedRoom = [
            'id' => $room->id,
            'photo_count' => 0
        ];
        foreach($photos as $key => $photo) {
            if($key == '360') {
                continue;
            }

            if(is_array($photo)) {
                foreach($photo as $keyPhoto => $photoId) {
                    if(is_null($photoId)) {
                        continue;
                    }

                    $expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . '-' . ($keyPhoto + 1);
                    $expectedPhotos['photo_id'] = $photoId;
                    
                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
                }
            } else {
                if(!is_null($photo)) {
                    $expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
                    $expectedPhotos['photo_id'] = $photo;
                    
                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
                }
            }

        }

        $this->assertDatabaseHas('designer',$expectedRoom);
    }

    public function testupdatePhotosWithExistingRecord()
    {
        $room = factory(Room::class)->create();
        $oldPhotoId = 999 ;
        $odlCard = factory(Card::class,10)->create([
            'designer_id' => $room->id,
            'photo_id' => $oldPhotoId,
        ]);
        $photos = [
			'cover'=>[1,2,3,4,5],
			'main'=>[6,7,8,9,null],
			'bedroom'=> null,
			'bath'=> 10,
            '360' => null
        ];
        $photos['other'] = [
            [
                'id' => 11,
                'description' => 1,
            ],
        ];
        
        $this->repository->updatePhotos($photos,$room);
        
        $descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
        ];
        
        $expectedPhotos = [
            'designer_id' => $room->id,
        ];
        $expectedRoom = [
            'id' => $room->id,
            'photo_count' => 0
        ];
        $unexpectedCard = [
            'designer_id' => $room->id,
            'photo_id' => $oldPhotoId,
        ];
		foreach($photos as $key => $photo) {
        	if($key == '360') {
        		continue;
        	}

        	if(is_array($photo)) {
        		if($key != 'other') {
        			foreach($photo as $keyPhoto => $photoId) {

						$expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
                        $expectedPhotos['photo_id'] = $photoId;

                        $expectedRoom['photo_count']++;
                        $this->assertDatabaseHas('designer_style',$expectedPhotos);                            
        			}
        		} else {
        			foreach($photo as $keyPhoto => $photoId) {
                        
	        			$expectedPhotos['description'] = $room->name . '-' . $photoId['description'] . '-' . ($keyPhoto + 1);
						$expectedPhotos['photo_id'] = $photoId['id'];
                        
                        $expectedRoom['photo_count']++;
                        $this->assertDatabaseHas('designer_style',$expectedPhotos);
        			}
        		}
        	} else {

					$expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
					$expectedPhotos['photo_id'] = $photo;

                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
        	}
        }


        $this->assertDatabaseHas('designer',$expectedRoom);
        $this->assertSoftDeleted('designer_style',$unexpectedCard);
    }

    public function testupdatePhotosWithNoExistingRecord()
    {
        $room = factory(Room::class)->create();
        $photos = [
			'cover'=>[1],
			'main'=>[2,null],
			'bedroom'=> null,
			'bath'=> 3,
            '360' => null
        ];
        $photos['other'] = [
            [
                'id' => 11,
                'description' => 1,
            ],
        ];
        
        $this->repository->updatePhotos($photos,$room);
        
        $descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
        ];
        
        $expectedPhotos = [
            'designer_id' => $room->id,
        ];
        $expectedRoom = [
            'id' => $room->id,
            'photo_count' => 0
        ];

		foreach($photos as $key => $photo) {
        	if($key == '360') {
        		continue;
        	}

        	if(is_array($photo)) {
        		if($key != 'other') {
        			foreach($photo as $keyPhoto => $photoId) {

						$expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
                        $expectedPhotos['photo_id'] = $photoId;

                        $expectedRoom['photo_count']++;
                        $this->assertDatabaseHas('designer_style',$expectedPhotos);                            
        			}
        		} else {
        			foreach($photo as $keyPhoto => $photoId) {
                        
	        			$expectedPhotos['description'] = $room->name . '-' . $photoId['description'] . '-' . ($keyPhoto + 1);
						$expectedPhotos['photo_id'] = $photoId['id'];
                        
                        $expectedRoom['photo_count']++;
                        $this->assertDatabaseHas('designer_style',$expectedPhotos);
        			}
        		}
        	} else {

					$expectedPhotos['description'] = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
					$expectedPhotos['photo_id'] = $photo;

                    $expectedRoom['photo_count']++;
                    $this->assertDatabaseHas('designer_style',$expectedPhotos);
        	}
        }

        $this->assertDatabaseHas('designer',$expectedRoom);
    }

    public function testtrackingReferralData()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();
        $referrer = factory(Referrer::class)->create(['user_id'=>$user->id]);

        $result = $this->repository->trackingReferralData($referrer,$user,$room);
        
        $this->assertDatabaseHas('user_referrer',$referrer->toArray());
        $this->assertTrue($result);
    }

    public function testreviewInputReferral()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create();

        $content = 'review';
        $result = $this->repository->reviewInputReferral($content,$user,$room);
        $expectedResult = [
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'content' => $content
        ];

        $this->assertDatabaseHas('designer_review',$expectedResult);
        $this->assertTrue($result);
    }

    public function testqueueCompletePhotoNotification()
    {
        $room = factory(Room::class)->create();

        $this->repository->queueCompletePhotoNotification($room);

        $expectedResult = [
            'designer_id' => $room->id,
            'status' => PhotoCompleteQueue::STATUS_WAITING
        ];

        $this->assertDatabaseHas('media_photo_complete_queue',$expectedResult);
    }

    public function testsavePriceComponent()
    {
        $room = factory(Room::class)->create();

        $priceName = 'price name';
        $priceLabel = 'price label';
        $priceRegular = 0.00;
        $priceSale = 0.00;
        $result = $this->repository->savePriceComponent($room,$priceName,$priceLabel,$priceRegular,$priceSale);

        $expectedResult = [
            'price_name' => $priceName,
            'price_label' => $priceLabel,
            'designer_id' => $room->id,
            'price_reguler' => $priceRegular,
            'price_sale' => $priceSale,
        ];
        $result = array_only($result->toArray(),['price_name','price_label','designer_id','price_reguler','price_sale']);
        $this->assertDatabaseHas('designer_price_component',$expectedResult);
        $this->assertEquals($result,$expectedResult);
    }

    public function testupdatePriceComponent()
    {
        factory(Room::class)->create();
        $priceComponent = factory(RoomPriceComponent::class)->create();
        $oldPriceComponent = $priceComponent->toArray();
        $this->assertDatabaseHas('designer_price_component',$oldPriceComponent);
       
        $params['price_name'] = 'update price name';
        $params['price_label'] = 'update price label';
        $params['price_reguler'] = 0.00;
        // $params['sale_price'] = 0.00;
        
        $result = $this->repository->updatePriceComponent($priceComponent,$params);

        $expectedResult = [
            'price_name' => $params['price_name'],
            'price_label' => $params['price_label'],
            'price_reguler' => $params['price_reguler'],
            // 'price_sale' => $params['sale_price'],
        ];

        $result = array_only($result->toArray(),['price_name','price_label','price_reguler']);

        $this->assertDatabaseMissing('designer_price_component',$oldPriceComponent);
        $this->assertDatabaseHas('designer_price_component',$expectedResult);
        $this->assertEquals($result,$expectedResult);
    }

    public function testremovePriceComponent()
    {
        $priceComponent = factory(RoomPriceComponent::class)->create();

        $this->assertDatabaseHas('designer_price_component',$priceComponent->toArray());
        $this->repository->removePriceComponent($priceComponent);

        $this->assertSoftDeleted('designer_price_component',['id' => $priceComponent->id]);
    }

    // UT test based on the script not on requirement
    public function testgroupPriceComponents()
    {
        $group=[
            'price_name_1',
            'price_name_2',
            'price_name_3',
        ]; 
        $priceComponents[] = factory(RoomPriceComponent::class)->create(['price_name' =>$group[0]]);
        $priceComponents[] = factory(RoomPriceComponent::class)->create(['price_name' =>$group[1]]);
        $priceComponents[] = factory(RoomPriceComponent::class)->create(['price_name' =>$group[2]]);

        $priceKey = 'price_name';
        $result = $this->repository->groupPriceComponents($priceComponents,$priceKey);
        $this->assertEquals($result[$group[0]]->id,$priceComponents[0]->id);
        $this->assertEquals($result[$group[1]]->id,$priceComponents[1]->id);
        $this->assertEquals($result[$group[2]]->id,$priceComponents[2]->id);
    }

    public function testSaveNewApartmentProjectByAdmin()
    {

        $tagsId = factory(Tag::class,5)->create([])->pluck('id');

        $params = [
            'name' => 'apartment project name',
            'address' => 'apartment project address',
            'description' => 'description',
            'latitude' => '0.0',
            'longitude' => '0.0',
            'area_city' => 'area city',
            'area_subdistrict' => 'area subdistrict',
            'phone_number_building' => '0819123456789',
            'phone_number_marketing' => '081111111111',
            'phone_number_other' => '082222222222',
            'developer' => 'developer',
            'unit_count' => 1,
            'floors' => '5',
            'size' => '5 x 5',
            'tags' => $tagsId,
            'photo_cover' => 1,
            'photo_other' => [2,3,4],
        ];
        $result = $this->repository->saveNewApartmentProjectByAdmin($params);

        $apartmentProject = ApartmentProject::find($result);

        $expectedResult = [
            'name' => $params['name'],
            'address' => $params['address'],
            'description' => $params['description'],
            'latitude' => $params['latitude'],
            'longitude' => $params['longitude'],
            'area_city' => AreaFormatter::format($params['area_city']),
            'area_subdistrict' => AreaFormatter::format($params['area_subdistrict']),
            'phone_number_building' => $params['phone_number_building'],
            'phone_number_marketing' => $params['phone_number_marketing'],
            'phone_number_other' => $params['phone_number_other'],
            'developer' => $params['developer'],
            'unit_count' => $params['unit_count'],
            'floor' => $params['floors'],
            'size' => $params['size'],
        ];
        


        $expectedPhotoOther = [
            'apartment_project_id' => $apartmentProject->id
        ];

        $expectedTags = [
            'apartment_project_id' => $apartmentProject->id
        ];

        $this->assertDatabaseHas('apartment_project',$expectedResult);

        foreach($params['photo_other'] as $photoOther) {

            $expectedPhotoOther['photo_id'] = $photoOther;
            $expectedPhotoOther['title'] = $apartmentProject->name;

            $this->assertDatabaseHas('apartment_project_style',$expectedPhotoOther);
        }
        
        foreach($params['tags'] as $tag) {

            $expectedTags['tag_id'] = $tag;
            $this->assertDatabaseHas('apartment_project_tag',$expectedTags);
        }
    }

    public function testUpdateApartmentProjectByAdmin()
    {

        $tagsId = factory(Tag::class,5)->create([])->pluck('id');
        
        $apartmentProject = factory(ApartmentProject::class)->create();
        
        $room = factory(Room::class)->create([
            'apartment_project_id' => $apartmentProject->id
            ]);
            
            
        $usedPhoto = [5,6];
        $unusedPhoto = [7,8,9];

        foreach(array_merge($usedPhoto,$unusedPhoto) as $photo){
            factory(ApartmentProjectStyle::class)->create([
                'apartment_project_id' => $apartmentProject->id,
                'photo_id' => $photo,
                ]);
            }
                
        $usedPhoto = [5,6,10];
        
        $params = [
            'name' => 'update name',
            'address' => 'update address',
            'description' => 'update description',
            'latitude' => '1.1',
            'longitude' => '1.1',
            'area_city' => 'update area city',
            'area_subdistrict' => 'update area city',
            'phone_number_building' => '0815555555',
            'phone_number_marketing' => '0866666666',
            'phone_number_other' => '087777777777',
            'developer' => 'update developer',
            'unit_count' => 10,
            'floors' => '10',
            'size' => '10 x 10',
            'tags' => $tagsId,
            'photo_cover' => 10,
            'photo_other' => $usedPhoto,
        ];

        $this->repository->updateApartmentProjectByAdmin($apartmentProject,$params);
        $apartmentProject = ApartmentProject::find($apartmentProject->id);
        $expectedResult = [
            'name' => $params['name'],
            'address' => $params['address'],
            'description' => $params['description'],
            'latitude' => $params['latitude'],
            'longitude' => $params['longitude'],
            'area_city' => AreaFormatter::format($params['area_city']),
            'area_subdistrict' => AreaFormatter::format($params['area_subdistrict']),
            'phone_number_building' => $params['phone_number_building'],
            'phone_number_marketing' => $params['phone_number_marketing'],
            'phone_number_other' => $params['phone_number_other'],
            'developer' => $params['developer'],
            'unit_count' => $params['unit_count'],
            'floor' => $params['floors'],
            'size' => $params['size'],
        ];

        $expectedPhotoOther = [
            'apartment_project_id' => $apartmentProject->id
        ];

        $expectedUnusedPhoto = [
            'apartment_project_id' => $apartmentProject->id
        ];

        $expectedTags = [
            'apartment_project_id' => $apartmentProject->id
        ];

        $expectedRoom = [
			'address' => $apartmentProject->address,
			'latitude' => $apartmentProject->latitude,
			'longitude' => $apartmentProject->longitude,
			'area_city' => $apartmentProject->area_city,
			'area_subdistrict' => $apartmentProject->area_subdistrict,
        ];

        $this->assertDatabaseHas('apartment_project',$expectedResult);

        foreach($params['tags'] as $tag) {

            $expectedTags['tag_id'] = $tag;
            $this->assertDatabaseHas('apartment_project_tag',$expectedTags);
        }

        $this->assertDatabaseHas('apartment_project_style',[
            'apartment_project_id' => $apartmentProject->id,
            'photo_id' => $params['photo_cover'],
            'title' => $apartmentProject->name . '-cover',
        ]);

        foreach($params['photo_other'] as $photo) {

            $expectedPhotoOther['photo_id'] = $photo;

            $this->assertDatabaseHas('apartment_project_style',$expectedPhotoOther);
        }

        foreach($unusedPhoto as $photo) {

            $expectedUnusedPhoto['photo_id'] = $photo;

            $this->assertSoftDeleted('apartment_project_style',$expectedUnusedPhoto);
        }

        $this->assertDatabaseHas('designer',$expectedRoom);
        
    }

    public function testUpdateApartmentUnitByAdminWithSaveMaintenancePriceAndParkingPrice()
    {
        [$apartmentProject,$newApartmentProject,$apartmentUnit] = $this->getSetApartment();

        $oldApartmentUnit = Room::find($apartmentUnit->id)->toArray();
        $params = [
            'apartment_project_id' => $newApartmentProject->id,
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'update type name',
            'floor' => '5',
            'unit_number' => '1',
            'is_furnished' => 0,
            'price_shown' => 0,
            'maintenance_price' => 200000,
            'parking_price' => 300000,
        ];

        $this->repository->updateApartmentUnitByAdmin($apartmentUnit,$params);
        $expectedResult = [
            'apartment_project_id' => $params['apartment_project_id'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'floor' => $params['floor'],
            'unit_number' => $params['unit_number'],
            'furnished' => (int)$params['is_furnished'],
            'payment_duration' => $params['price_shown'],
            'address' => $newApartmentProject->address,
			'area_city' => AreaFormatter::format($newApartmentProject->area_city),
			'area_subdistrict' => AreaFormatter::format($newApartmentProject->area_subdistrict),
        ];
        $unexpectedResult = [
            'apartment_project_id' => $oldApartmentUnit['apartment_project_id'],
            'unit_type' => $oldApartmentUnit['unit_type'],
            'floor' => $oldApartmentUnit['floor'],
            'unit_number' => $oldApartmentUnit['unit_number'],
            'furnished' => (int)$oldApartmentUnit['furnished'],
            'payment_duration' => $oldApartmentUnit['payment_duration'],
            'address' => $oldApartmentUnit['address'],
			'area_city' => $oldApartmentUnit['area_city'],
			'area_subdistrict' => $oldApartmentUnit['area_subdistrict'],
        ];

        $this->assertDatabaseHas('designer',$expectedResult);        
        $this->assertDatabaseMissing('designer',$unexpectedResult);        
    }

    public function testUpdateApartmentUnitByAdminWithUpdateMaintenancePriceAndParkingPrice()
    {
        [$apartmentProject,$newApartmentProject,$apartmentUnit] = $this->getSetApartment();
        $oldApartmentUnit = Room::find($apartmentUnit->id)->toArray();

        $this->createRoomPriceComponent($apartmentUnit->id);

        $params = [
            'apartment_project_id' => $newApartmentProject->id,
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'update type name',
            'floor' => '5',
            'unit_number' => '1',
            'is_furnished' => 0,
            'price_shown' => 0,
            'maintenance_price' => 200000,
            'parking_price' => 300000,
        ];

        $this->repository->updateApartmentUnitByAdmin($apartmentUnit,$params);
        $expectedResult = [
            'apartment_project_id' => $params['apartment_project_id'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'floor' => $params['floor'],
            'unit_number' => $params['unit_number'],
            'furnished' => (int)$params['is_furnished'],
            'payment_duration' => $params['price_shown'],
            'address' => $newApartmentProject->address,
			'area_city' => AreaFormatter::format($newApartmentProject->area_city),
			'area_subdistrict' => AreaFormatter::format($newApartmentProject->area_subdistrict),
        ];
        $unexpectedResult = [
            'apartment_project_id' => $oldApartmentUnit['apartment_project_id'],
            'unit_type' => $oldApartmentUnit['unit_type'],
            'floor' => $oldApartmentUnit['floor'],
            'unit_number' => $oldApartmentUnit['unit_number'],
            'furnished' => (int)$oldApartmentUnit['furnished'],
            'payment_duration' => $oldApartmentUnit['payment_duration'],
            'address' => $oldApartmentUnit['address'],
			'area_city' => $oldApartmentUnit['area_city'],
			'area_subdistrict' => $oldApartmentUnit['area_subdistrict'],
        ];

        $this->assertDatabaseHas('designer',$expectedResult);        
        $this->assertDatabaseMissing('designer',$unexpectedResult);        
    }

    public function testUpdateApartmentUnitByAdminWithDeleteMaintenancePriceAndParkingPrice()
    {
        [$apartmentProject,$newApartmentProject,$apartmentUnit] = $this->getSetApartment();
        $oldApartmentUnit = Room::find($apartmentUnit->id)->toArray();

        $this->createRoomPriceComponent($apartmentUnit->id);

        $params = [
            'apartment_project_id' => $newApartmentProject->id,
            'unit_type' => 'Lainnya',
            'unit_type_name' => 'update type name',
            'floor' => '5',
            'unit_number' => '1',
            'is_furnished' => 0,
            'price_shown' => 0,
            'maintenance_price' => 0,
            'parking_price' => 0,
        ];

        $this->repository->updateApartmentUnitByAdmin($apartmentUnit,$params);
        $expectedResult = [
            'apartment_project_id' => $params['apartment_project_id'],
            'unit_type' => $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'],
            'floor' => $params['floor'],
            'unit_number' => $params['unit_number'],
            'furnished' => (int)$params['is_furnished'],
            'payment_duration' => $params['price_shown'],
            'address' => $newApartmentProject->address,
			'area_city' => AreaFormatter::format($newApartmentProject->area_city),
			'area_subdistrict' => AreaFormatter::format($newApartmentProject->area_subdistrict),
        ];
        $unexpectedResult = [
            'apartment_project_id' => $oldApartmentUnit['apartment_project_id'],
            'unit_type' => $oldApartmentUnit['unit_type'],
            'floor' => $oldApartmentUnit['floor'],
            'unit_number' => $oldApartmentUnit['unit_number'],
            'furnished' => (int)$oldApartmentUnit['furnished'],
            'payment_duration' => $oldApartmentUnit['payment_duration'],
            'address' => $oldApartmentUnit['address'],
			'area_city' => $oldApartmentUnit['area_city'],
			'area_subdistrict' => $oldApartmentUnit['area_subdistrict'],
        ];

        $this->assertDatabaseHas('designer',$expectedResult);        
        $this->assertDatabaseMissing('designer',$unexpectedResult);        
    }

    public function testtrackInput()
    {
        $inputTracket = factory(InputTracker::class)->make();
        $params = [
            'user_id' => $inputTracket->user_id,
            'designer_id' => $inputTracket->designer_id,
            'input_source' => $inputTracket->input_source,
        ];

        $this->repository->trackInput($params);

        $this->assertDatabaseHas('input_tracker',$params);
    }

    public function testGetYoutubeIdWithLinkIsEmptyShouldReturnNull()
    {
        $result = $this->repository->getYoutubeId('');
        $this->assertNull($result);
    }

    public function testGetYoutubeIdWithInvalidLinkShouldReturnNull()
    {
        $result = $this->repository->getYoutubeId('https://');
        $this->assertNull($result);
    }

    //https://www.youtube.com/watch?v=95ZLrpmYEVc

    public function testGetYoutubeIdWithNormalLink()
    {   
        $id = '95ZLrpmYEVc';
        $link = "https://www.youtube.com/watch?v=$id";
        $result = $this->repository->getYoutubeId($link);
        $this->assertEquals($result,$id);
    }

    public function testGetYoutubeIdWithShortenLink()
    {   
        $id = '95ZLrpmYEVc';
        $link = "https://youtu.be/$id";
        $result = $this->repository->getYoutubeId($link);
        $this->assertEquals($result,$id);
    }

    public function testGetYoutubeIdWithShortsLink()
    {   
        $id = 'ErWDmZ9GWQ0';
        $link = "https://youtube.com/shorts/$id";
        $result = $this->repository->getYoutubeId($link);
        $this->assertEquals($result,$id);
    }

    public function testGetYoutubeIdNotYoutubeReturnNull()
    {   
        $id = 'ErWDmZ9GWQ0';
        $link = "https://videos.com/watch?v=$id";
        $result = $this->repository->getYoutubeId($link);
        $this->assertNull($result,$id);
    }

	public function testgetExistingInputOwnerPhoneOrManagerPhone()
	{
        $room1 = factory(Room::class)->create([
			'owner_phone' => '0822222222',
			'manager_phone' => '08123456789',
		]);

        $room2 = factory(Room::class)->create([
			'owner_phone' => '01090909090',
			'manager_phone' => '08111111111',
        ]);
        
        factory(Room::class,20)->create([
			'owner_phone' => '0566666666',
			'manager_phone' => '01111111111',
		]);

        $result1 = $this->repository->getExistingInput('08');
        $result2 = $this->repository->getExistingInput('05');
        
        $this->assertEquals(2,$result1->count());
        $this->assertEquals($room1->id,$result1[0]->id);
        $this->assertEquals($room2->id,$result1[1]->id);

        $this->assertLessThanOrEqual(10,$result2->count());
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateKostByConsultantWithPhotosCoverIsArray()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 

        $user = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $this->actingAs($user);
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id
        ]);

        $id = 'U3gBa4-LF6k';
        $params = [
            'name' => 'update name',
            'address' => 'update address',
            'latitude' => 1.1,
            'longitude' => 1.1,
            'gender' => 1,
            'room_available' => 3,
            'room_count' => 5,
            'city' => 'update area',
            'subdistrict' => 'update subdistrict',
            'price_daily' => '10000',
            'price_weekly' => '700000',
            'price_monthly' => '3000000',
            'price_yearly' => '36000000',
            'price_quarterly' => '9000000',
            'price_semiannually' => '18000000',
            'room_size' => '5x5',    
            'price_remark' => '20000',    
            'description' => 'update description',    
            'remarks' => 'update remarks',
            'owner_name' => $owner->name,
            'owner_phone' => $owner->phone_number,
            'manager_name' => $owner->name,
            'manager_phone' => $owner->phone_number,
            'building_year' =>  '2021',
            'photos' => [
                    'cover' => [1],
                    '360' => 2,    
            ],
            'youtube_link' => "https://youtu.be/$id",
            'fac_property' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'fac_bath' => [$tags[2]->id],
            'concern_ids' => [$tags[3]->id], 
        ];

        $result = $this->repository->updateKostByConsultant($room,$params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'][0],
            'photo_round_id' => $params['photos']['360'],
            'youtube_id' => $id,
            'photo_count' => 1,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    // Test implemented as is the script with the current flow. Not updated using multiple types yet.
    public function testupdateKostByConsultantWithPhotos360IsNull()
    {
        [$owner,$room,$roomOwner,$tags] = $this->getSetRoomData(
            ['password' => Hash::make('password')]
        ); 
        
        $user = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $this->actingAs($user);
        $consultant = factory(Consultant::class)->create([
            'user_id' => $user->id
        ]);

        $id = 'U3gBa4-LF6k';
        $params = [
            'name' => 'update name',
            'address' => 'update address',
            'latitude' => 1.1,
            'longitude' => 1.1,
            'gender' => 1,
            'room_available' => 3,
            'room_count' => 5,
            'city' => 'update area',
            'subdistrict' => 'update subdistrict',
            'price_daily' => '10000',
            'price_weekly' => '700000',
            'price_monthly' => '3000000',
            'price_yearly' => '36000000',
            'price_quarterly' => '9000000',
            'price_semiannually' => '18000000',
            'room_size' => '5x5',    
            'price_remark' => '20000',    
            'description' => 'update description',    
            'remarks' => 'update remarks',
            'owner_name' => $owner->name,
            'owner_phone' => $owner->phone_number,
            'manager_name' => $owner->name,
            'manager_phone' => $owner->phone_number,
            'building_year' =>  '2021',
            'photos' => [
                    'cover' => 1,
                    '360' => ''
            ],
            'youtube_link' => "https://youtu.be/$id",
            'fac_property' => [$tags[0]->id],
            'fac_room' => [$tags[1]->id],
            'fac_bath' => [$tags[2]->id],
            'concern_ids' => [$tags[3]->id], 
        ];

        $result = $this->repository->updateKostByConsultant($room,$params);
        
        $expectedResult= [
            'name'           => ApiHelper::removeEmoji($params['name']),
            'address'        => ApiHelper::removeEmoji($params['address']),
            'latitude'       => $params['latitude'],
            'longitude'      => $params['longitude'],
            'gender'         => $params['gender'],
            'room_available' => $params['room_available'],
            'room_count'     => $params['room_count'],
            'status'         => $params['room_available'] > 0 ? 0 : 2,
            'is_active'      => 'false',
            'area_city'      => AreaFormatter::format($params['city']),
            'area_subdistrict' => AreaFormatter::format($params['subdistrict']),
            'price_remark'     => ApiHelper::removeEmoji($params['price_remark']),
            'remark'          	=> ApiHelper::removeEmoji($params['remarks']),
            'description'      => ApiHelper::removeEmoji($params['description']),
            'is_booking'    => 0,
            'price_daily'   => RequestInputHelper::getNumericValue($params['price_daily']),
            'price_weekly'  => RequestInputHelper::getNumericValue($params['price_weekly']),
            'price_monthly' => RequestInputHelper::getNumericValue($params['price_monthly']),
            'price_yearly'  => RequestInputHelper::getNumericValue($params['price_yearly']),
            'price_quarterly'  => RequestInputHelper::getNumericValue($params['price_quarterly']),
            'price_semiannually'  => RequestInputHelper::getNumericValue($params['price_semiannually']),
            'size' => $params['room_size'],
            'photo_id' => $params['photos']['cover'],
            'youtube_id' => $id,
            'photo_count' => 1,
        ];

        $keys = array_keys($expectedResult);
        $result = array_only($result->toArray(),$keys);
        
        $this->assertEquals($expectedResult,$result);
    }

    private function getSetRoomData(array $ownerData=[],array $roomData=[],array $user=[]):array
    {
        $owner = factory(User::class)->create($ownerData);
        $room = factory(Room::class)->create($roomData);
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $tags = [];
        $tags[] = factory(Tag::class)->create([
            'type' => 'fac_share',
        ]);
        $tags[] = factory(Tag::class)->create([
            'type' => 'fac_room'
        ]);
        $tags[] = factory(Tag::class)->create([
            'type' => 'fac_bath'
        ]);
        $tags[] = factory(Tag::class)->create([
            'type' => 'concern'
        ]);
        $tags[] = factory(Tag::class)->create([
            'type' => 'fac_price'
        ]);
        return [$owner,$room,$roomOwner,$tags];
    }

    private function createRoomPriceComponent($roomId){
        factory(RoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'price_name' => RoomPriceComponent::PRICE_NAME_MAINTENANCE,
        ]);

        factory(RoomPriceComponent::class)->create([
            'designer_id' => $roomId,
            'price_name' => RoomPriceComponent::PRICE_NAME_PARKING,
        ]);
    }

    private function getSetApartment()
    {
        $apartmentProject = factory(ApartmentProject::class)->create();
        $newApartmentProject = factory(ApartmentProject::class)->create();
        
        $apartmentUnit = factory(Room::class)->create([
            'apartment_project_id' => $apartmentProject->id,
            'unit_type' => 'type name',
        ]);
        
        return [$apartmentProject,$newApartmentProject,$apartmentUnit];
    }
}
