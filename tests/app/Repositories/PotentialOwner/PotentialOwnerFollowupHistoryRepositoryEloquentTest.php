<?php

namespace App\Repositories\PotentialOwner;

use App\Entities\PotentialOwner\PotentialOwnerFollowupHistory;
use App\Test\MamiKosTestCase;
use App\Entities\Consultant\PotentialOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class PotentialOwnerFollowupHistoryRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new PotentialOwnerFollowupHistoryRepositoryEloquent(app());
        $this->followupHistoryModel = $this->mock(PotentialOwnerFollowupHistory::class);
    }

    public function testModel(): void
    {
        $this->assertSame($this->repository->model(), PotentialOwnerFollowupHistory::class);
    }

    public function testCreateHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $followupHistory = $this->repository->createHistory($owner);
        $this->assertEquals($owner->id, $followupHistory->potential_owner_id);
        $this->assertNotNull($followupHistory->new_at);
    }

    public function testUpdateHistoryInProgressWithExistingHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $followupHistory = $this->repository->createHistory($owner);
        
        $owner->setFolUpStatToOnGoing();

        $followupHistory = $this->repository->updateHistory($owner);

        $this->assertEquals($owner->id, $followupHistory->potential_owner_id);
        $this->assertNotNull($followupHistory->new_at);
        $this->assertNotNull($followupHistory->ongoing_at);
    }

    public function testUpdateHistoryInProgressWithoutExistingHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_ONGOING
        ]);

        $followupHistory = $this->repository->updateHistory($owner);

        $this->assertEquals($owner->id, $followupHistory->potential_owner_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNotNull($followupHistory->ongoing_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryDoneWithoutExistingHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_DONE
        ]);

        $followupHistory = $this->repository->updateHistory($owner);

        $this->assertEquals($owner->id, $followupHistory->potential_owner_id);
        $this->assertNull($followupHistory->new_at);
        $this->assertNull($followupHistory->ongoing_at);
        $this->assertNotNull($followupHistory->done_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryWithInvalidStatusShouldNotError(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => ''
        ]);

        $followupHistory = $this->repository->updateHistory($owner);

        $this->assertEquals($owner->id, $followupHistory->potential_owner_id);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryInProgressWithMultipleExistingHistory(): void
    {
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_DONE
        ]);

        $followupHistoryFirst = factory(PotentialOwnerFollowupHistory::class)->create([
            'potential_owner_id' => $owner->id,
            'created_at' => now()->subMinute()
        ]);

        $followupHistoryLatest = factory(PotentialOwnerFollowupHistory::class)->create([
            'potential_owner_id' => $owner->id,
        ]);

        $owner->setFolUpStatToOnGoing();

        $this->repository->updateHistory($owner);

        $this->assertEquals($owner->id, $followupHistoryLatest->potential_owner_id);

        $this->assertNull($followupHistoryFirst->fresh()->ongoing_at);

        $this->assertNotNull($followupHistoryLatest->fresh()->ongoing_at);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent
     */
    public function testCreateHistoryShouldCatchError(): void
    {
        $exception = new Exception();
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $history = factory(PotentialOwnerFollowupHistory::class)->make()->toArray();

        $this->followupHistoryModel->shouldReceive('create')
            ->with($history)
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->once();

        $result = $this->repository->createHistory($owner);
        $this->assertNull($result);
    }

    /**
     * @group UG
     * @group UG-4705
     * @group App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent
     */
    public function testUpdateHistoryShouldCatchError(): void
    {
        $exception = new Exception();
        $owner = factory(PotentialOwner::class)->create([
            'followup_status' => PotentialOwner::FOLLOWUP_STATUS_NEW
        ]);

        $history = factory(PotentialOwnerFollowupHistory::class)->make()->toArray();

        $this->followupHistoryModel->shouldReceive('update')
            ->with($history)
            ->andThrow($exception);

        Bugsnag::shouldReceive('notifyException')
            ->once();

        $result = $this->repository->updateHistory($owner);
        $this->assertNull($result);
    }
}