<?php

namespace App\Repositories\Jobs;

use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Activity\Tracking;
use Faker\Factory as Faker;

class JobsRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(JobsRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function testModel()
    {
        $this->assertSame(Vacancy::class, $this->repository->model());
    }

    public function testInsertNewJobs_Success()
    {
        $status = true;

        $mockedRepo = $this->mockPartialAlternatively(JobsRepositoryEloquent::class);
        $mockedRepo->shouldReceive('insertVacancy')->once()->andReturn($status);

        $this->assertSame(['status' => $status], $mockedRepo->insertNewJobs('', ''));
    }

    public function testDeactiveJobsOwner_Success()
    {
        $slug = $this->faker->slug;

        $user = factory(User::class)->create();
        factory(Vacancy::class)->create([
                'slug' => $slug,
                'user_id' => $user->id,
            ]);
        
        $this->assertTrue($this->repository->deactiveJobsOwner($slug, $user));
    }

    public function testDeactiveJobsOwner_Fail()
    {
        $slug = $this->faker->slug;

        $user = factory(User::class)->create();
        
        $this->assertFalse($this->repository->deactiveJobsOwner($slug, $user));
    }

    public function testDeleteJobsOwner_Success()
    {
        $slug = $this->faker->slug;

        $user = factory(User::class)->create();
        factory(Vacancy::class)->create([
                'slug' => $slug,
                'user_id' => $user->id,
            ]);
        
        $this->assertTrue($this->repository->deleteJobsOwner($slug, $user));
    }

    public function testDeleteJobsOwner_Fail()
    {
        $slug = $this->faker->slug;

        $user = factory(User::class)->create();
        
        $this->assertFalse($this->repository->deleteJobsOwner($slug, $user));
    }

    public function testDeleteFromOwner_TidakDiterima()
    {
        $expected = [
            "status" => false,
            "messages" => [
                JobsRepositoryEloquent::VERIFIKASI_TIDAK_DITERIMA
            ]
        ];

        $request = [
                'verification_status' => false
            ];

        $this->assertSame($this->repository->deleteFromOwner($request), $expected);
    }

    public function testDeleteFromOwner_LowonganTidakDitemukan()
    {
        $expected = [
            "status" => false,
            "messages" => [
                JobsRepositoryEloquent::LOWONGAN_TIDAK_DITEMUKAN
            ]
        ];

        $request = [
                'verification_status' => true,
                'verification_for' => base64_encode('randomId,randomDataInput')
            ];

        $this->assertSame($this->repository->deleteFromOwner($request), $expected);
    }

    public function testDeleteFromOwner_Success()
    {
        $expected = [
            "status" => true,
            "messages" => [
                JobsRepositoryEloquent::BERHASIL_VERIFIKASI
            ]
        ];

        $dataInput = 'randomDataInput';
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        $vacancy = factory(Vacancy::class)->states('active')
            ->create([
                'slug' => $slug,
                'user_id' => $user->id,
                'data_input' => $dataInput
            ]);

        $request = [
                'verification_status' => true,
                'verification_for' => base64_encode( $vacancy->id.','.$dataInput )
            ];

        $this->assertSame($this->repository->deleteFromOwner($request), $expected);
    }

    public function testActiveJobsOwner_ReturnNull()
    {
        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        
        $this->assertFalse($this->repository->activeJobsOwner($slug, $user));
    }

    public function testActiveJobsOwner_ReturnKadaluarsa()
    {
        $expected = [
            "status" => false,
            "message" => JobsRepositoryEloquent::LOWONGAN_KADALUARSA.date("d M Y", strtotime("yesterday"))
        ];

        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        factory(Vacancy::class)->states('active')
            ->create([
                'slug' => $slug,
                'user_id' => $user->id,
                'expired_date' => date("Y-m-d", strtotime("yesterday"))
            ]);

        $this->assertSame($expected, $this->repository->activeJobsOwner($slug, $user));
    }

    public function testActiveJobsOwner_ReturnSuccess()
    {
        $expected = [
            "status" => true,
            "message" => JobsRepositoryEloquent::SUKSES_UBAH_STATUS_LOWONGAN
        ];

        $slug = $this->faker->slug;
        $user = factory(User::class)->create();
        factory(Vacancy::class)->states('active')
            ->create([
                'slug' => $slug,
                'user_id' => $user->id,
                'expired_date' => date("Y-m-d", strtotime("tomorrow"))
            ]);

        $this->assertSame($expected, $this->repository->activeJobsOwner($slug, $user));
    }

    public function testUpdateUserProfile()
    {
        $user = factory(User::class)->create();
        $request = [
            'user' => 'randomUser',
            'address' => 'randomAddress',
            'education' => 'randomEducation'
        ];

        $this->assertTrue($this->repository->updateUserProfile($request, $user));
    }

    public function testJobsApply()
    {
        DB::shouldReceive('beginTransaction')->once();
        DB::shouldReceive('commit')->once();
        $mockAgent = $this->mockPartialAlternatively(Agent::class);
        $this->mockPartialAlternatively(Tracking::class)
            ->shouldReceive('checkDevice')
            ->withArgs([$mockAgent])
            ->andReturn( collect("android", "tablet", "iphone", "desktop", "robot")->random() );

        $user = factory(User::class)->create();
        $vacancy = factory(Vacancy::class)->create();
        $request = [
            'user' => 'randomUser',
            'address' => 'randomAddress',
            'education' => 'randomEducation',
            'identifier' => $vacancy->id,
            'user' => 'randomUser',
            'skill' => 'randomSkill',
            'job_experience' => 'randomJobExperience',
            'last_salary' => mt_rand(11111, 999999),
            'expectation_salary' => mt_rand(11111, 999999),
            'phone' => mt_rand(11111, 999999),
            'cv' => 'randomCv.pdf',
            'workplace_status' => mt_rand(0,1),
        ];

        $result = $this->repository->jobsApply($request, $user, $vacancy);

        $this->assertSame(['status', 'meta', 'data'], array_keys($result));
    }
}