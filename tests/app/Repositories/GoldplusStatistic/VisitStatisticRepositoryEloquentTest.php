<?php
namespace App\Repositories\GoldplusStatistic;

use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use App\Repositories\GoldplusStatistic\VisitStatisticRepositoryEloquent;
use Illuminate\Database\Query\Builder;
use App\Entities\Read\Read;
use App\Entities\Read\ReadTemp3;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\KostLevel;
use Mockery;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use Carbon\CarbonPeriod;
use Carbon\Carbon;

//TODO: Angga Bayu Sejati
class VisitStatisticRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(VisitStatisticRepositoryEloquent::class);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testModelShouldBeValid(): void
    {
        $this->assertSame(Read::class, $this->repository->model());
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildVisitStatisticArrShouldReturnValid(): void
    {   
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);
        factory(Read::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);

        $repository = $this->repository->buildVisitStatisticArr()->first();
        $this->assertEquals($repository->designer_id, $room->id);
    }

    /**
     * @group UG
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildVisitStatisticArrDailyShouldReturnValid(): void
    {   
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged',
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);

        $repository = $this->repository->buildVisitStatisticArrForDaily()->first();
        $this->assertEquals($repository->designer_id, $room->id);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3859
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildVisitStatisticArrShouldReturnNull(): void
    {   
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => mt_rand(111111, 999999)
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);
        factory(Read::class)->create([
            'designer_id' => $room->id . mt_rand(111111, 999999),
            'user_id'       => $owner->id . mt_rand(111111, 999999),
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0] + mt_rand(1, 10);
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id . mt_rand(111111, 999999),
            'level_id'  => $levelId . mt_rand(111111, 999999),
        ]);

        $repository = $this->repository->buildVisitStatisticArr()->first();
        $this->assertNull($repository);
    }

    /**
     * @group UG
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildVisitStatisticArrDailyShouldReturnNull(): void
    {   
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => mt_rand(111111, 999999)
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id . mt_rand(111111, 999999),
            'user_id'       => $owner->id . mt_rand(111111, 999999),
            'status'        => 'logged'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0] + mt_rand(1, 10);
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id . mt_rand(111111, 999999),
            'level_id'  => $levelId . mt_rand(111111, 999999),
        ]);

        $repository = $this->repository->buildVisitStatisticArr()->first();
        $this->assertNull($repository);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticDaily(): void 
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id
        ]);
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 01:00:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 07:01:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 13:01:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 15:01:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 07:01:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 20:01:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeToday.' 22:30:00',
            'count'         => mt_rand(1, 10),
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);

        $repository = $this->repository->buildFilteredGpStatisticDaily($oneDayBeforeToday, $room->id)->first();
       
        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->value_0_6);
        $this->assertIsInt((int) $repository->value_6_10);
        $this->assertIsInt((int) $repository->value_10_14);
        $this->assertIsInt((int) $repository->value_14_18);
        $this->assertIsInt((int) $repository->value_18_22);
        $this->assertIsInt((int) $repository->value_22_24);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticYesterday(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        //Statistic var
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');
        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();

        //Baseline var
        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("9 days")
        ), 'Y-m-d');
        $baselineDateGp = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("7 days")
        ), 'Y-m-d');
        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Feeding for statistic
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged'
            ]
        );

        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[0]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[1]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[2]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[3]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[4]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[5]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );

        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $twoDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArrForDaily();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticYesterday($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticYesterdayExpectException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        //Statistic var
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');
        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();

        //Baseline var
        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("9 days")
        ), 'Y-m-d');
        $baselineDateGp = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("7 days")
        ), 'Y-m-d');
        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Feeding for statistic
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged'
            ]
        );

        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[0]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[1]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[2]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[3]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[4]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[5]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );

        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $twoDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArrForDaily();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticYesterday(null);
    }

     /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastSevenDays(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        //Statistic var
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');
        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();

        //Baseline var
        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("9 days")
        ), 'Y-m-d');
        $baselineDateGp = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("7 days")
        ), 'Y-m-d');
        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Feeding for statistic
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged'
            ]
        );

        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[0]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[1]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[2]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[3]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[4]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[5]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );

        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $twoDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $repository = $this->repository->buildFilteredGpStatisticLastSevenDays(
            $period, 
            $room->id,
            $baselineDateGp,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->date_1);
        $this->assertIsInt((int) $repository->date_2);
        $this->assertIsInt((int) $repository->date_3);
        $this->assertIsInt((int) $repository->date_4);
        $this->assertIsInt((int) $repository->date_5);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticSevenLastDays(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        //Statistic var
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');
        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();

        //Baseline var
        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("9 days")
        ), 'Y-m-d');
        $baselineDateGp = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("7 days")
        ), 'Y-m-d');
        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Feeding for statistic
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged'
            ]
        );

        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[0]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[1]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[2]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[3]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[4]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[5]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );

        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $twoDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArrForDaily();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticSevenLastDays($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group UG-4225
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticSevenLastDaysExpectRuntimeException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        //Statistic var
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');
        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();

        //Baseline var
        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("9 days")
        ), 'Y-m-d');
        $baselineDateGp = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("7 days")
        ), 'Y-m-d');
        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Feeding for statistic
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'status'        => 'logged'
            ]
        );

        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[0]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id' => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[1]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[2]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[3]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[4]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $period[5]->format('Y-m-d').' 00:00:00',
            'status'        => 'logged'
            ]
        );

        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $oneDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        //Feeding data for baseline
        factory(ReadTemp3::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'created_at'    => $twoDayBeforeGpCreated.' 00:00:00',
            'status'        => 'logged'
            ]
        );
        
        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArrForDaily();
        $this->assertNotNull($chatData->get()[0]);
        $this->repository->buildGpStatisticSevenLastDays($chatData->get()[0]);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastThirtyDays(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');
        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        //Date for seeding baseline value
        $dateBeforeGpCreated1 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated2 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("3 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated3 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("8 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding data
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek5.' 00:00:00'
            ]
        );

        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated3.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $repository = $this->repository->buildFilteredGpStatisticLastThirtyDays(
            $oneDayBeforeToday,
            $thirtyDaysBeforeToday,
            $room->id,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->week_1);
        $this->assertIsInt((int) $repository->week_2);
        $this->assertIsInt((int) $repository->week_3);
        $this->assertIsInt((int) $repository->week_4);
        $this->assertIsInt((int) $repository->week_5);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastThirtyDays(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');
        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        //Date for seeding baseline value
        $dateBeforeGpCreated1 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated2 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("3 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated3 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("8 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding data
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek5.' 00:00:00'
            ]
        );

        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated3.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticLastThirtyDays($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastThirtyDaysExpectRuntimeException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');
        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        //Date for seeding baseline value
        $dateBeforeGpCreated1 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("2 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated2 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("3 days")
        ), 'Y-m-d');
        $dateBeforeGpCreated3 = date_format(date_sub(
            date_create($thirtyDaysBeforeGpCreated), date_interval_create_from_date_string("8 days")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding data
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $endDateWeek5.' 00:00:00'
            ]
        );

        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $dateBeforeGpCreated3.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticLastThirtyDays(null);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastTwoMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $twoMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $repository = $this->repository->buildFilteredGpStatisticLastTwoMonths(
            $oneDayBeforeToday,
            $twoMonthsBeforeToday,
            $room->id,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->month_1);
        $this->assertIsInt((int) $repository->month_2);
        $this->assertIsInt((int) $repository->month_3);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastTwoMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $twoMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticLastTwoMonths($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastTwoMonthsExpectRuntimeException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $twoMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticLastTwoMonths(null);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastThreeMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $threeMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $repository = $this->repository->buildFilteredGpStatisticLastThreeMonths(
            $oneDayBeforeToday,
            $threeMonthsBeforeToday,
            $room->id,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->month_1);
        $this->assertIsInt((int) $repository->month_2);
        $this->assertIsInt((int) $repository->month_3);
        $this->assertIsInt((int) $repository->month_4);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastThreeMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $threeMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticLastThreeMonths($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastThreeMonthsExpectRuntimeException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $threeMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticLastThreeMonths(null);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastFourMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $repository = $this->repository->buildFilteredGpStatisticLastFourMonths(
            $oneDayBeforeToday,
            $fourMonthsBeforeToday,
            $room->id,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->month_1);
        $this->assertIsInt((int) $repository->month_2);
        $this->assertIsInt((int) $repository->month_3);
        $this->assertIsInt((int) $repository->month_4);
        $this->assertIsInt((int) $repository->month_5);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastFourMonths()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticLastFourMonths($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastFourMonthsExpectRuntimeException()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticLastFourMonths(null);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildFilteredGpStatisticLastFiveMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month6.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $repository = $this->repository->buildFilteredGpStatisticLastFiveMonths(
            $oneDayBeforeToday,
            $fourMonthsBeforeToday,
            $room->id,
            $gpCreatedAt
        )->first();

        $this->assertEquals($repository->designer_id, $room->id);
        $this->assertEquals($repository->name, $room->name);
        $this->assertEquals($repository->level_id, $levelId);
        $this->assertIsInt((int) $repository->month_1);
        $this->assertIsInt((int) $repository->month_2);
        $this->assertIsInt((int) $repository->month_3);
        $this->assertIsInt((int) $repository->month_4);
        $this->assertIsInt((int) $repository->month_5);
        $this->assertIsInt((int) $repository->month_6);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3977
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastFiveMonths(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month6.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $expected = $this->repository->buildGpStatisticLastFiveMonths($chatData->get()[0]);
        $this->assertNull($expected);
    }

    /**
     * @group UG
     * @group UG-4703
     * @group App/Repositories/GoldplusStatistic/VisitStatisticRepositoryEloquent
     */
    public function testBuildGpStatisticLastFiveMonthsExpectRuntimeException(): void
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $oneMonthsBeforeGpCreated= date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $room = factory(Room::class)->create([
            'apartment_project_id' => null,
            'is_testing'    => 0
        ]);
        factory(RoomOwner::class)->create(['designer_id' => $room->id]);

        //Seeding for statistic
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month1.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month2.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month3.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month4.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month5.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $month6.' 00:00:00'
            ]
        );

        $oneDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoDaysBefore= date_format(date_sub(
            date_create($oneMonthsBeforeGpCreated), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');

        //Seeding for baseline value
        factory(Read::class)->create(
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $oneDaysBefore.' 00:00:00'
            ],
            [
                'designer_id'   => $room->id,
                'user_id'       => $owner->id,
                'created_at'    => $twoDaysBefore.' 00:00:00'
            ]
        );

        factory(KostLevel::class)->create();
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(null)[0];
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
            'created_at'    => $gpCreatedAt,
            'updated_at'    => $gpCreatedAt,
        ]);

        $chatData = $this->repository->buildVisitStatisticArr();
        $this->assertNotNull($chatData->get()[0]);
        $this->expectException(\RuntimeException::class, 'Source data was null');
        $this->repository->buildGpStatisticLastFiveMonths(null);
    }
}
