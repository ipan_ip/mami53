<?php
namespace App\Repositories\GoldplusStatistic;

use App\Test\MamiKosTestCase;
use App\User;
use Faker\Factory as Faker;
use App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepositoryEloquent;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\BaselineType;
use Carbon\Carbon;
use App\Entities\Media\Media;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;

class OwnerGoldplusStatisticRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(OwnerGoldplusStatisticRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testModelShouldBeValid(): void
    {
        $this->assertSame(OwnerGoldplusStatistic::class, $this->repository->model());
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetAvailableFilterStatistic()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode([
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ], ',');

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        );

        $filterArr = $this->transformKeyValueOfFilter(
            GoldplusStatisticReportType::YESTERDAY, 
            $gpCreatedAt,
            date('Y-m-d')
        );
        $expected = [
            GoldplusStatisticReportType::YESTERDAY => $filterArr
        ];
        $result = $this->repository->getAvailableFilterStatistic($owner->id, GoldplusStatisticType::FAVORITE, $songId);
        $this->assertEquals($expected['yesterday'], $result['yesterday']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKost()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'id' => env('GOLDPLUS3_LEVEL_ID'),
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $key = array_rand(KostLevel::getGoldplusLevelIdsByLevel(3));
        $levelId = KostLevel::getGoldplusLevelIdsByLevel(3)[$key];
        $kostLevel = factory(KostLevel::class)->states('rand-gp3')->create([
            'id'    => $levelId,
            'is_regular' => true,
        ]);
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);
        $result     = $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
        
        $expected   = [
            'id'                => $room->song_id,
            'room_title'        => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'           => $room->address,
        ];
       
        $this->assertEquals($result['id'], $expected['id']);
        $this->assertEquals($result['room_title'], $expected['room_title']);
        $this->assertEquals($result['area_formatted'], $expected['area_formatted']);
        $this->assertEquals($result['address'], $expected['address']);
    }

    /**
     * @group UG
     * @group UG-4196
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKostShouldReturnValidGp1Label()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'id' => env('GOLDPLUS3_LEVEL_ID'),
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $kostLevel = factory(KostLevel::class)->states('gp2-with-config')->create();
        $levelId = $kostLevel->id;
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);
        $result     = $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
        
        $expected   = [
            'id'                => $room->song_id,
            'room_title'        => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'           => $room->address,
        ];
       
        $this->assertEquals($result['id'], $expected['id']);
        $this->assertEquals($result['room_title'], $expected['room_title']);
        $this->assertEquals($result['area_formatted'], $expected['area_formatted']);
        $this->assertEquals($result['address'], $expected['address']);
        $this->assertEquals($result['gp_status']['value'], KostLevel::getGoldplusNameByLevelId($levelId));
    }

    /**
     * @group UG
     * @group UG-4196
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKostShouldReturnValidGp2Label()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'id' => env('GOLDPLUS3_LEVEL_ID'),
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $kostLevel = factory(KostLevel::class)->states('gp2-with-config')->create();
        $levelId = $kostLevel->id;
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);
        $result     = $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
        
        $expected   = [
            'id'                => $room->song_id,
            'room_title'        => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'           => $room->address,
        ];
       
        $this->assertEquals($result['id'], $expected['id']);
        $this->assertEquals($result['room_title'], $expected['room_title']);
        $this->assertEquals($result['area_formatted'], $expected['area_formatted']);
        $this->assertEquals($result['address'], $expected['address']);
        $this->assertEquals($result['gp_status']['value'], KostLevel::getGoldplusNameByLevelId($levelId));
    }

    /**
     * @group UG
     * @group UG-4196
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKostShouldReturnValidGp3Label()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'id' => env('GOLDPLUS3_LEVEL_ID'),
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $kostLevel = factory(KostLevel::class)->states('gp3-with-config')->create();
        $levelId = $kostLevel->id;
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);
        $result     = $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
        
        $expected   = [
            'id'                => $room->song_id,
            'room_title'        => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'           => $room->address,
        ];
       
        $this->assertEquals($result['id'], $expected['id']);
        $this->assertEquals($result['room_title'], $expected['room_title']);
        $this->assertEquals($result['area_formatted'], $expected['area_formatted']);
        $this->assertEquals($result['address'], $expected['address']);
        $this->assertEquals($result['gp_status']['value'], KostLevel::getGoldplusNameByLevelId($levelId));
    }

    /**
     * @group UG
     * @group UG-4196
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKostShouldReturnValidGp4Label()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'id' => env('GOLDPLUS3_LEVEL_ID'),
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $kostLevel = factory(KostLevel::class)->states('gp4-with-config')->create();
        $levelId = $kostLevel->id;
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->save();
        
        factory(KostLevelMap::class)->create([
            'kost_id'   => $room->song_id,
            'level_id'  => $levelId,
        ]);
        $result     = $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
        
        $expected   = [
            'id'                => $room->song_id,
            'room_title'        => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'           => $room->address,
        ];
       
        $this->assertEquals($result['id'], $expected['id']);
        $this->assertEquals($result['room_title'], $expected['room_title']);
        $this->assertEquals($result['area_formatted'], $expected['area_formatted']);
        $this->assertEquals($result['address'], $expected['address']);
        $this->assertEquals($result['gp_status']['value'], KostLevel::getGoldplusNameByLevelId($levelId));
    }

    /**
     * @group UG
     * @group UG-2587
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetDetailOfGoldplusKostReturnRuntimeExceptinon()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $kostLevel = factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);
        
        $address =  'Jalan '.str_random();
        $room = factory(Room::class)->create([
            'address' => $address,
        ]);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id'   => $room->id.mt_rand(1, 10),
            'user_id'       => $owner->id,
            'owner_status'  => 'add',
            'promoted_status'   => 'false',
        ]);
        $photo = factory(Media::class)->create();
        $room->photo()->associate($photo);
        $room->changeLevel($kostLevel->id);
        $room->save();

        $this->expectException(\RuntimeException::class);
        $this->repository->getDetailOfGoldplusKost($owner->id, $room->song_id);
    }

     /**
     * @group UG
     * @group UG-2587
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetStatisticData()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode([
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ], ',');

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue
                )
        )->toArray();

        $result = $this->repository->getStatisticData(
            GoldplusStatisticType::FAVORITE,
            GoldplusStatisticReportType::YESTERDAY,
            $songId,
            $owner->id
        );

        $this->assertEquals($result['id'], $gpData['room']['song_id']);
        $this->assertEquals($result['room_title'], $gpData['room']['name']);
        $this->assertNotNull($result['statistic']);
        $this->assertNotNull($result['chart_label']);
        $this->assertNotNull($result['chart']);
    }

    /**
     * @group UG
     * @group UG-2587
     * @group UG-3885
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetStatisticDataForChatReport()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode([
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ], ',');

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $gpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $growthValue
                )
        )->toArray();

        $result = $this->repository->getStatisticData(
            GoldplusStatisticType::CHAT,
            GoldplusStatisticReportType::YESTERDAY,
            $songId,
            $owner->id
        );

        $this->assertEquals($result['id'], $gpData['room']['song_id']);
        $this->assertEquals($result['room_title'], $gpData['room']['name']);
        $this->assertNotNull($result['statistic']);
        $this->assertNotNull($result['chart_label']);
        $this->assertNotNull($result['chart']);
    }

    /**
     * Transform key value of filter
     * 
     * @param string $key
     * @param string $gpCreatedAt
     * 
     * @return array
     */
    private function transformKeyValueOfFilter(
        string $key, 
        string $gpCreatedAt, 
        string $date
    ): array
    {
        switch ($key) {
            case GoldplusStatisticReportType::YESTERDAY:
                $oneDayBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("1 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $oneDayBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_SEVEN_DAYS:
                $sevenDaysBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("7 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $sevenDaysBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_THIRTY_DAYS:
                $thirtyDaysBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("30 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $thirtyDaysBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_TWO_MONTHS:
                $twoMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("2 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_TWO_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $twoMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_THREE_MONTHS:
                $threeMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("3 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_THREE_MONTHS,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $threeMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_FOUR_MONTHS:
                $fourMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("4 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_FOUR_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $fourMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_FIVE_MONTHS:
                $fiveMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("5 months")
                ), 'Y-m-d');
                return [
                    'key'           => $key,
                    'value'         => GoldplusStatisticReportType::LAST_FIVE_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $fiveMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            default :
                throw new RuntimeException('Invalid key value filter statistic!');
        }
    }

    /**
     * Build format periode description
     * 
     * @param string $startPeriodeStr
     * @param string $endDate
     * 
     * @return string
     */
    private function buildFormatPeriodeDescription(
        string $startDate, 
        string $endDate
    ): string
    {
        $monthIndonesian = [
            '01'    => 'Jan',
            '02'    => 'Feb',
            '03'    => 'Mar',
            '04'    => 'Apr',
            '05'    => 'Mei',
            '06'    => 'Jun',
            '07'    => 'Jul',
            '08'    => 'Agt',
            '09'    => 'Sep',
            '10'    => 'Okt',
            '11'    => 'Nov',
            '12'    => 'Des'
        ];

        $strtoTimeStartDate = strtotime($startDate);
        $strtoTimeEndDate   = strtotime($endDate);
        $roomToGpDiff       = abs(
            Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate))
        );

        if ($roomToGpDiff === 1) {
            return date('d', $strtoTimeStartDate).' '
                .$monthIndonesian[date('m', $strtoTimeStartDate)]
                . ' '.date('Y', $strtoTimeStartDate);
        }

        $startPeriodeStr = date('d', $strtoTimeStartDate).' '
            .$monthIndonesian[date('m', $strtoTimeStartDate)]
            . ' '.date('Y', $strtoTimeStartDate);
        $endPeriodStr   = date('d', $strtoTimeEndDate).' '
            .$monthIndonesian[date('m', $strtoTimeEndDate)]
            . ' '.date('Y', $strtoTimeEndDate);

        unset($monthIndonesian);

        return $startPeriodeStr.' - '.$endPeriodStr;
    }

     /**
     * Payload for test
     * 
     * @return array
     */
    private function payloadForStatisticType(
        int $userId, 
        Room $room, 
        string $availableReportType,
        string $gpCreatedAt,
        string $filterType,
        int $growthValue,
        string $reportType = GoldplusStatisticReportType::YESTERDAY
        ): array
    {
        $totalLastTwoMonthsVisit = mt_rand(1, 1000);
        $growthType     = GrowthType::UP;
        $baselineType   = BaselineType::GROWTH;

        return [
            'room'  => [
                'id'        => $room->id,
                'song_id'   => $room->song_id,
                'name'      => $room->name,
                'gender'    => $room->gender,
                'address'   => $room->address,
                'area'      => $room->area,
                'is_active' => $room->is_active,
            ],
            'owner' => [
                'user_id'   => $userId,
            ],
            'statistic' => [
                'key'       => $filterType,
                'total'     => $totalLastTwoMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => mt_rand(1, 100),
                    'currdate_to_gp_diff'   => mt_rand(1, 100),
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => mt_rand(1, 100),
                'start_baseline_date_range_value'   => date('Y-m-d'),
                'available_report_type'             => $availableReportType,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => $reportType,
            ],
            'gp'    => [
                'level_id'      => 11,
                'gp_created_at' => $gpCreatedAt,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Agt',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Sep',
                        'y' => mt_rand(1, 10),
                    ],
                    [
                        'x' =>  'Okt',
                        'y' => mt_rand(1, 10),
                    ],
                ]   
            ]
        ];
    }

     /**
     * @group LOY
     * @group LOY-2528
     * @group App/Repositories/GoldplusStatistic/FavoriteStatisticRepositoryEloquent
     */
    public function testGetBestKostStatisticData()
    {
        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $designerId     = mt_rand(111111, 999999);
        $songId         = mt_rand(111111, 999999);
        $name           = \str_random();
        $gender         = 0;
        $address        = \str_random();
        $area           = \str_random();
        $isActive       = 1;

        $room = factory(Room::class)->create([
            'id'        => $designerId,
            'song_id'   => $songId,
            'name'      => $name,
            'gender'    => $gender,
            'address'   => $address,
            'area'      => $area,
            'is_active' => $isActive,
        ]);

        $availableReportTypeForLastThirtyDays = implode([
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS
        ], ',');

        $gpCreatedAt = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        (int) $growthValue = mt_rand(1, 1000);
        $favGpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::FAVORITE,
                    $growthValue,
                    GoldplusStatisticReportType::LAST_THIRTY_DAYS
                )
        )->toArray();

        (int) $growthValue = mt_rand(1, 1000);
        $chatGpData = factory(OwnerGoldplusStatistic::class)
            ->create(
                $this->payloadForStatisticType(
                    $owner->id, 
                    $room, 
                    $availableReportTypeForLastThirtyDays,
                    $gpCreatedAt,
                    GoldplusStatisticType::CHAT,
                    $growthValue,
                    GoldplusStatisticReportType::LAST_THIRTY_DAYS
                )
        )->toArray();

        $result = $this->repository->getBestKostStatisticData(
            $owner->id
        );

        $this->assertNotEmpty($result);
        $this->assertEquals($result['id'], $favGpData['room']['song_id']);
        $this->assertEquals($result['room_title'], $favGpData['room']['name']);

        $this->assertNotNull($result['statistic']);
        $this->assertEquals(2, count($result['statistic']));

        $this->assertNotNull($result['statistic']['favorite']);
        $this->assertNotNull($result['statistic']['chat']);
    }
}

