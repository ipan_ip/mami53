<?php

namespace App\Repositories;

use App\Test\MamiKosTestCase;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;

class FlashSaleRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(FlashSaleRepositoryEloquent::class);
    }

    public function testModel()
    {
        $this->assertEquals('App\Entities\FlashSale\FlashSale', $this->repository->model());
    }

    public function testCompileHistoryData()
    {
        $data = (object) [
            "id"            => 45,
            "name"          => "jujun",
            "start_time"    => "2020-06-01 19:03:00",
            "end_time"      => "2020-06-07 19:05:00",
            "photo_id"      => 1662236,
            "is_active"     => 1,
            "created_by"    => "administrator :: Maya Sari Samosir",
            "created_at"    => "2020-06-01 14:30:25",
            "updated_at"    => "2020-06-01 14:55:19",
            "deleted_at"    => null,
            "histories" => [
                (object)[
                    "id"              => 219,
                    "flash_sale_id"   => 45,
                    "original_value"  => '{"id":45,"name":"jujun","start_time":null,"end_time":null,"photo_id":1662236,"is_active":0,"created_by":"administrator :: Maya Sari Samosir","created_at":"2020- ▶',
                    "modified_value"  => '{"start_time":"2020-06-02 14:54:25","end_time":"2020-06-09 14:54:25","is_active":1,"updated_at":"2020-06-01 14:55:19"}',
                    "triggered_by"    => "administrator :: Maya Sari Samosir",
                    "created_at"      => "2020-06-01 14:55:19",
                    "updated_at"      => "2020-06-01 14:55:19",
                ],
                (object)[
                    "id"              => 216,
                    "flash_sale_id"   => 45,
                    "original_value"  => '{}',
                    "modified_value"  => '{"name":"bantul","added_by":"administrator :: Bayu BE","flash_sale_id":45,"updated_at":"2020-06-01 14:52:54","created_at":"2020-06-01 14:52:54","id":24}',
                    "triggered_by"    => "administrator :: Bayu BE",
                    "created_at"      => "2020-06-01 14:55:19",
                    "updated_at"      => "2020-06-01 14:55:19",
                ],
                (object)[
                    "id"              => 218,
                    "flash_sale_id"   => 45,
                    "original_value"  => '{"id":45,"name":"jujun","start_time":null,"end_time":null,"photo_id":null,"is_active":0,"created_by":"administrator :: Maya Sari Samosir","created_at":"2020-06-01 14:30:25","updated_at":"2020-06-01 14:30:25","deleted_at":null}',
                    "modified_value"  => '{"photo_id":"1662236","updated_at":"2020-06-01 14:52:54"}',
                    "triggered_by"    => "administrator :: Bayu BE",
                    "created_at"      => "2020-06-01 14:55:19",
                    "updated_at"      => "2020-06-01 14:55:19",
                ],
            ],
        ];

        $result = $this->repository->compileHistoryData($data);

        $this->assertEquals(3, count($result));
        $this->assertEquals(
            [
                "date" => "01 Jun 2020 at 14:55:19",
                "event" => [
                    "type" => "add",
                    "message" => 'Maya Sari Samosir created flash sale "JUJUN"' 
                ]
            ], 
            $result[0]
        );
        $this->assertEquals(
            [
                "date" => "01 Jun 2020 at 14:55:19",
                "event" => [
                    "type" => "add",
                    "message" => 'Bayu BE added area "Bantul" in Flash Sale "JUJUN"'
                ]
            ], 
            $result[1]
        );
        $this->assertEquals(
            [
                "date" => "01 Jun 2020 at 14:55:19",
                "event" => [
                    "type" => "update",
                    "message" => 'Bayu BE set banner image with ID "1662236" for Flash Sale "JUJUN"'
                ]
            ], 
            $result[2]
        );
    }

    public function testUpdateWithRelationsFlashSaleNotFound()
    {
        $result = $this->repository->updatewithRelations(['id'=> 999]);
        $this->assertFalse($result);
    }
}
