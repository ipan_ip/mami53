<?php

namespace App\Repositories\Notification;

use App;
use App\Entities\Notif\Category;
use App\Entities\User\Notification;
use App\Test\MamiKosTestCase;
use App\Presenters\Notification\CategoryPresenter;
use App\Presenters\Notification\NotificationPresenter;
use App\User;
use Prettus\Repository\Exceptions\RepositoryException;

class NotificationRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = App::make(NotificationRepositoryEloquent::class);
    }

    public function testSetModel_GivenNotModel_ExceptionThrown()
    {
        $this->expectException(RepositoryException::class);
        $this->class->setModel(CategoryPresenter::class);
    }

    public function testCategoryList_GetPaginatedResult()
    {
        $limit = 5;
        $totalPage = 2;
        factory(Category::class, $limit * $totalPage)->create();

        $this->class->setPresenter(CategoryPresenter::class);
        $result = $this->class->categoryList($limit);

        $this->assertEquals($limit, count($result['data']));
        $this->assertEquals($totalPage, $result['meta']['pagination']['total_pages']);
    }

    public function testGetByUser_GetPaginatedResult()
    {
        $limit = 5;
        $totalPage = 2;

        $user = factory(User::class)->create(['is_owner' => false]);
        $category = factory(Category::class)->create();
        factory(Notification::class, $limit * $totalPage)->create(
            [
                'user_id' => $user->id,
                'category_id' => $category->id
            ]
        );

        $this->class->setPresenter(NotificationPresenter::class);
        $result = $this->class->getByUser($user, $category->id, $limit);

        $this->assertEquals($limit, count($result['data']));
        $this->assertEquals($totalPage, $result['meta']['pagination']['total_pages']);
    }

    public function testGetUserUnreadCount_GetUnreadCountGroupByCategory()
    {
        $countCategoryA = 5;
        $countCategoryB = 10;
        $user = factory(User::class)->create(['is_owner' => false]);
        $categoryA = factory(Category::class)->create();
        $categoryB = factory(Category::class)->create();
        factory(Notification::class, $countCategoryA)->create(
            [
                'user_id' => $user->id,
                'category_id' => $categoryA->id
            ]
        );
        factory(Notification::class, $countCategoryB)->create(
            [
                'user_id' => $user->id,
                'category_id' => $categoryB->id
            ]
        );

        $result = $this->class->getUserUnreadCount($user);
        $this->assertEquals([
            ['category_id' => $categoryA->id, 'unread' => $countCategoryA],
            ['category_id' => $categoryB->id, 'unread' => $countCategoryB]
        ], $result->toArray());
    }

    public function testSetAsRead_NotificationIsRead()
    {
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();

        $notif = factory(Notification::class)->create([
            'user_id' => $user->id, 
            'category_id' => $category->id
        ]);

        $result = $this->class->setAsRead($user, $category->id);
        $this->assertEquals('true', Notification::find($notif->id)->read);
    }
}
