<?php

namespace App\Repositories;

use App\Repositories\WhitelistFeatureRepositoryEloquent;
use App\Test\MamiKosTestCase;
use App;
use App\Entities\Feature\WhitelistFeature;

class WhitelistFeatureRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;
    private $user;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = App::make(WhitelistFeatureRepositoryEloquent::class);
    }

    public function testFindByNameUserId()
    {
        $name = 'feature_a';
        $userId = 3;
        factory(WhitelistFeature::class)->create([
            'name' => $name,
            'user_id' => $userId
        ]);

        $result = $this->repository->findByNameUserId($name, $userId);
        $this->assertEquals($name, $result->name);
        $this->assertEquals($userId, $result->user_id);
    }

    public function testIsEligibleWithNoSetFeature()
    {
        $result = $this->repository->isEligible('feature_b', 4);
        $this->assertFalse($result);
    }

    public function testIsEligibleWithSetFeature()
    {
        $name = 'feature_b';
        $userId = 4;
        factory(WhitelistFeature::class)->create([
            'name' => $name,
            'user_id' => $userId
        ]);
        $result = $this->repository->isEligible($name, $userId);
        $this->assertTrue($result);
    }
    
    public function testSaveDuplicated()
    {
        $name = 'feature_b';
        $userId = 4;
        factory(WhitelistFeature::class)->create([
            'name' => $name,
            'user_id' => $userId
        ]);
        $whitelistFeature = new WhitelistFeature;
        $whitelistFeature->name = $name;
        $whitelistFeature->user_id = $userId;
        $result = $this->repository->save($whitelistFeature);
        $this->assertFalse($result[0]);
        $this->assertEquals($this->repository::ERROR_DUPLICATED_ENTRY, $result[1]);
    }

    public function testSave()
    {
        $name = 'feature_b';
        $userId = 4;
        $whitelistFeature = new WhitelistFeature;
        $whitelistFeature->name = $name;
        $whitelistFeature->user_id = $userId;
        $result = $this->repository->save($whitelistFeature);
        $this->assertTrue($result[0]);
        
        $insertedFeature = WhitelistFeature::where('name' , $name)->where('user_id', $userId)->first();
        $this->assertEquals($name, $insertedFeature->name);
        $this->assertEquals($userId, $insertedFeature->user_id);
    }
}
