<?php

namespace App\Repositories;

use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Collection;
use App\User;
use App\Entities\Premium\Bank;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\Payment;
use App\Entities\Mamipay\MamipayInvoices;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Enums\Premium\PremiumRequestStatus;
use App\Http\Helpers\MamipayApiHelper;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Notification;
use Mockery;

class PremiumRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = new PremiumRepositoryEloquent(app(), Collection::make());
    }

    public function testGetModel()
    {
        $model = $this->repository->model();
        $this->assertEquals(PremiumRequest::class, $model);
    }

    public function testGetBankListEmptyData()
    {
        $bank = factory(Bank::class)->create([
            "is_active" => 0
        ]);
        $result = $this->repository->getBankAccount();
        $this->assertEquals(null, $result);
    }

    public function testGetBankListSuccess()
    {
        $bank = factory(Bank::class)->create([
            "is_active" => 1
        ]);

        $result = $this->repository->getBankAccount();
        $this->assertEquals(1, count($result));
    }

    public function testListPackageSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "price" => 10000,
            "sale_price" => 10000,
            "start_date" => date("Y-m-d"),
            "sale_limit_date" => date("Y-m-d h:i:s"),
            "total_day" => 100,
            "special_price" => 0
        ]);
        $specialPackage = factory(PremiumPackage::class)->create([
            'special_price' => 1000
        ]);

        $result = $this->repository->getDesignListPackage($premiumPackage);
        $resultSpecialPackage = $this->repository->getDesignListPackage($specialPackage);
        $this->assertEquals($premiumPackage->id, $result['id']);
        $this->assertEquals("Rp 5.000 + Rp 1.000", $resultSpecialPackage['sale_price']);
    }

    public function testGetOldPremiumPackageListSuccess()
    {
        $user = factory(User::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create([
                'name' => 'Paket test',
                'is_active' => '1',
                'price' => 500,
                'sale_price' => 0,
                'special_price' => null,
                'start_date' => date('Y-m-d'),
                'sale_limit_date' => date('Y-m-d h:i:s'),
                'view' => 500
        ]);

        $result = $this->repository->getPremiumPackage("package", $user);
        $this->assertEquals(1, count($result['packages']));
    }

    public function testGetPremiumConfirmationTotalSuccess()
    {
        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => 88,
            "premium_request_id" => 93,
            "is_confirm" => "0",
        ]);

        $result = $this->repository->checkConfirmExist($confirmation->user_id, [
            "for" => "premium_request_id",
            "idconfirm" => 93
        ]);

        $this->assertEquals(1, $result);
    }

    public function testPremiumRequestAlreadyConfirmed()
    {
        $user = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_date" => date('Y-m-d'),
            "expired_status" => 'false',
            "user_id" => $user->id
        ]);
        
        $data = [
            "premium_id" => $premiumRequest->id,
            "buy_balance_id" => 0
        ];

        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => $user->id,
            "premium_request_id" => $premiumRequest->id,
            "is_confirm" => "0",
        ]);

        $result = $this->repository->confirm($data, $user->id);
        $this->assertTrue($result);
    }

    public function testPremiumRequestConfirmFailed()
    {
        $user = factory(User::class)->create();
        
        $data = [
            "premium_id" => 989,
            "buy_balance_id" => 0
        ];

        $result = $this->repository->confirm($data, $user->id);
        $this->assertFalse($result);
    }

    public function testPremiumRequesConfirmed()
    {
        $user = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_date" => date('Y-m-d'),
            "expired_status" => 'false',
            "user_id" => $user->id
        ]);
        
        $data = [
            "premium_id" => $premiumRequest->id,
            "buy_balance_id" => 0
        ];

        $result = $this->repository->confirm($data, $user->id);
        $this->assertTrue($result);
    }

    public function testUpdateExpiredPremiumRequestSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_date" => date('Y-m-d'),
            "expired_status" => 'false'
        ]);

        $result = $this->repository->hideExpired($premiumRequest);
        $this->assertEquals(null, $result->expired_date);
    }

    public function testGetPackageFeaturesSuccess()
    {
        $result = $this->repository->packageFeatures();
        $this->assertEquals(3, count($result));
    }

    public function testCalculatePremiumPackagePrice()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'name' => 'Paket test',
            'is_active' => '1',
            'price' => 500,
            'sale_price' => 500,
            'sale_price' => 0,
            'special_price' => 0,
            'special_price' => null,
            'start_date' => date('Y-m-d'),
            'sale_limit_date' => date('Y-m-d h:i:s'),
            'view' => 500
        ]);

        $result = $this->repository->sumPayTotal($premiumPackage->id);
        $this->assertEquals(500, $result);
    }

    public function testCheckTotalPremiumRequestByOwner()
    {
        $user = factory(User::class)->create();

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "0",
            "expired_status" => "false"
        ]);

        $result = $this->repository->checkRequestOwner($user->id);
        $this->assertEquals(1, $result);
    }

    public function testUpgradePremiumRequestSuccess()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        factory(PremiumRequest::class)->create([
            "status" => "0",
            "user_id" => $user->id,
        ]);
        
        $data = [
            "user_id" => $user->id,
            "premium_package_id" => 991,
            "view" => 1000,
            "total" => 1000,
            "auto_upgrade" => 1,
        ];

        $data2 = $data;
        $data2['user_id'] = $user2->id;

        $result = $this->repository->upgradePremiumRequest($data);
        $result2 = $this->repository->upgradePremiumRequest($data2);
        $this->assertTrue($result instanceof PremiumRequest);
        $this->assertTrue($result2 instanceof PremiumRequest);
    }

    public function testDeleteConfirmationSuccess()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create();

        $confirmation = factory(AccountConfirmation::class)-> create([
            "premium_request_id" => $premiumRequest->id,
            "user_id" => $user->id
        ]);

        $result = $this->repository->deleteAccountConfirmation($user->id, $premiumRequest->id);
        $this->assertEquals(1, $result);
    }

    public function testPremiumRequestPackageNotFound()
    {
        $user = factory(User::class)->create();
        
        $data = [
            "premium_package_id" => 0
        ];

        $result = $this->repository->prosesRequestOwner($data, $user, "web");
        $this->assertEquals(null, $result['active_package']);
    }

    public function testPremiumRequestForExpiredOwnerSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "for" => "package",
            "view" => 1000
        ]);

        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d', strtotime('-7 days'))
        ]);

        $data = [
            "premium_package_id" => $premiumPackage->id,
            "user_id" => $user->id,
            "total" => 1000,
            "view" => $premiumPackage->view,
            "is_cancel" => false
        ];

        $result = $this->repository->prosesRequestOwner($data, $user, "web");
        $this->assertTrue($result['action']);
    }

    public function testPremiumRequestAlreadyExist()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "for" => "package",
            "view" => 1000
        ]);

        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d')
        ]);

        factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "0",
            "expired_status" => "false"
        ]);

        $data = [
            "premium_package_id" => $premiumPackage->id,
            "user_id" => $user->id,
            "total" => 1000,
            "view" => $premiumPackage->view,
            "is_cancel" => false,
            "is_upgrade" => false
        ];

        $result = $this->repository->prosesRequestOwner($data, $user, "web");
        $this->assertFalse($result['action']);
    }

    public function testPremiumRequestWhenOwnerNotExpiredSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "for" => "package",
            "view" => 1000
        ]);

        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d')
        ]);

        $data = [
            "premium_package_id" => $premiumPackage->id,
            "user_id" => $user->id,
            "total" => 1000,
            "view" => $premiumPackage->view,
            "is_cancel" => false,
            "is_upgrade" => false
        ];

        $result = $this->repository->prosesRequestOwner($data, $user, "web");
        $this->assertTrue($result['action']);
    }

    public function testNotifAfterPayingFailed()
    {
        $user = factory(User::class)->create();

        $bank = factory(Bank::class)->create([
            "is_active" => false
        ]);
        
        $data = [
            "user_id" => $user->id,
            "bank_id" => $bank->id
        ];

        $result = $this->repository->confirmationNotif($user, $data);
        $this->assertFalse($result['status']);
    }

    public function testNotifAfterPayingSuccess()
    {
        Notification::shouldReceive('send');
        
        $user = factory(User::class)->create();

        $bank = factory(Bank::class)->create([
            "is_active" => true,
            "name" => "Bank BL",
            "number" => "72328738232",
            "account_name" => "Supeno"
        ]);

        factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "total" => 1000,
            "expired_status" => "false"
        ]);
        
        $data = [
            "user_id" => $user->id,
            "bank_id" => $bank->id
        ];

        $result = $this->repository->confirmationNotif($user, $data);
        $this->assertTrue($result['status']);
    }

    public function testAutoApproveTrialFailed()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => 'false',
            "user_id" => $user->id
        ]);

        $result = $this->repository->autoApproveTrial($user, []);
        $this->assertFalse($result['status']);
    }

    public function testGetPremiumPackageSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create();

        $result = $this->repository->checkPremiumPackage($premiumPackage->id);
        $this->assertTrue($result instanceof PremiumPackage);
    }

    public function testGetConfirmationStatusSuccess()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => null,
            'is_verify' => '1'
        ]);
        
        $this->mockOwnerRepo = $this->mockAlternatively('App\Repositories\OwnerDataRepositoryEloquent');
        $this->mockOwnerRepo->shouldReceive('profile')->andReturn(['membership' => []]);

        $result = $this->repository->getConfirmation($user);
        $this->assertEquals(2, count($result));
    }

    public function testPremiumTopUpProcessFailed()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => null
        ]);

        $result = $this->repository->processBuyBalance([
            "user" => $user
        ]);

        $this->assertFalse($result['action']);
    }

    public function testPremiumTopUpProcessSuccess()
    {
        Notification::shouldReceive('send');

        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d', strtotime('+7 days')),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => "1",
            "expired_date" => null,
            "view" => 1000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id
        ]);

        $premiumPackage = factory(PremiumPackage::class)->create([
            "view" => 1000,
            "price" => 10000,
            "sale_price" => 10000,
            "total_day" => 30
        ]);

        $bank = factory(Bank::class)->create([
            "name" => "Bank BRI",
            "is_active" => 1,
            "number" => "302392839283982"
        ]);

        $result = $this->repository->processBuyBalance([
            "user" => $user,
            "premium_request_id" => $premiumRequest->id,
            "premium_package_id" => $premiumPackage->id
        ]);
        $this->assertTrue($result['action']);
    }

    public function testExistTopUpProcessSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d', strtotime('+4 days')),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "status" => "1",
            "expired_date" => null,
            "view" => 1000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id
        ]);

        $topUpRequest = factory(BalanceRequest::class)->create([
            "premium_request_id" => $premiumRequest->id,
            "expired_status" => "false",
            "status" => 0

        ]);

        $premiumPackage = factory(PremiumPackage::class)->create([
            "view" => 1000,
            "price" => 10000,
            "sale_price" => 10000,
            "total_day" => 30
        ]);

        $bank = factory(Bank::class)->create([
            "name" => "Bank BRI",
            "is_active" => 1,
            "number" => "302392839283982"
        ]);

        $result = $this->repository->processBuyBalance([
            "user" => $user,
            "premium_request_id" => $premiumRequest->id,
            "premium_package_id" => $premiumPackage->id,
            "is_cancel" => true
        ]);
        $this->assertTrue($result['action']);
        $this->assertEquals($topUpRequest->id, $result['balance_id']);
    }

    public function testSaveTopUpRequestSuccess()
    {
        Notification::shouldReceive('send');

        $user = factory(User::class)->create();

        $data = array(
            "view" => 1000,
            "price" => 1000,
            "premium_package_id" => 900,
            "premium_request_id" => 900,
            "expired_date"       => null,
            "expired_status"     => null,
            "status" => 0,
            "user" => $user,
        );

        $bank = factory(Bank::class)->create([
            "name" => "Bank BRI",
            "is_active" => 1,
            "number" => "302392839283982"
        ]);

        $result = $this->repository->topUpRequestSave($data, 10000);
        
        $this->assertTrue($result instanceof BalanceRequest);
    }

    public function testGetPremiumPackageListSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => null
        ]);
        
        $premiumPackage = factory(PremiumPackage::class)->create([
            "is_active" => "1",
            "start_date" => date('Y-m-d', strtotime('-2 days')),
            "sale_limit_date" => date('Y-m-d', strtotime('+4 days')),
            "for" => "package",
            "view" => 100
        ]);

        $result = $this->repository->getPremiumPackageVersion("package", $user, [
            "v" => 2
        ]);

        $this->assertEquals(3, count($result));
    }

    public function testGetPremiumPackageListFailed()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d')
        ]);

        $result = $this->repository->getPremiumPackageVersion("package", $user, [
            "v" => 2
        ]);
        
        $this->assertEquals(3, count($result));
    }

    public function testGetPremiumPackageDetailSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => null
        ]);
        
        $premiumPackage = factory(PremiumPackage::class)->create([
            "is_active" => "1",
            "start_date" => date('Y-m-d', strtotime('-2 days')),
            "sale_limit_date" => date('Y-m-d', strtotime('+4 days')),
            "for" => "package",
            "view" => 100
        ]);

        $result = $this->repository->getPremiumPackageVersion("package", $user, [
            "v" => 2,
            "package_id" => $premiumPackage->id
        ]);

        $this->assertEquals(1, count($result['packages']));
    }

    public function testGetPremiumRequestHistoryEmptyData()
    {
        $user = factory(User::class)->create();
        $result = $this->repository->getHistory($user);
        $this->assertEquals(0, $result['total']);
        $this->assertEquals([], $result['history']);
    }

    public function testGetPremiumRequestHistoryWithData()
    {
        $user = factory(User::class)->create();
        $premiumPackage = $this->createPremiumPackage();
        $this->createPremiumRequest($premiumPackage, $user->id);
        $result = $this->repository->getHistory($user);
        $this->assertEquals(1, $result['total']);
        $this->assertEquals(1, $result['page']);
        $this->assertEquals(false, $result['has-more']);
    }

    public function testPremiumRequestWhenWhenLastRequestIsTrial()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            "for" => "package",
            "view" => 0
        ]);

        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d')
        ]);

        factory(PremiumRequest::class)->create([
            "status" => "1",
            "expired_date" => null,
            "user_id" => $user->id,
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "for" => "trial"
            ])->id
        ]);

        $data = [
            "premium_package_id" => $premiumPackage->id,
            "user_id" => $user->id,
            "total" => 1000,
            "view" => $premiumPackage->view,
            "is_cancel" => false,
            "is_upgrade" => false
        ];

        $result = $this->repository->prosesRequestOwner($data, $user, "web");
        $this->assertEquals(null, $result['action']);
        $this->assertEquals(0, $result['jumlah']);
    }

    public function testGetDetailInvoiceShouldNotFound()
    {
        $user = factory(User::class)->create();
        factory(PremiumRequest::class)->create([
            "expired_status" => "true",
            "user_id"   => $user->id
        ]);
        
        $invoice = $this->repository->getDetailInvoice($user);
        
        $this->assertEquals(404, $invoice['meta']['code']);
        $this->assertEquals('Invoice tidak ditemukan', $invoice['meta']['message']);
    }

    public function testGetDetailInvoicePremiumRequest()
    {
        $user = factory(User::class)->create();
        $package = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING,
            'payment_type' => 'bank_transfer bca',
            'total' => null
        ]);

        factory(MamipayInvoices::class)->create([
            'order_id' => $premiumRequest->id,
            'order_type' => PremiumRequest::ORDER_TYPE
        ]);

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertTrue($invoice['status']);
        $this->assertEquals($invoice['invoice']['selected_payment'], 'Bank BCA');
        $this->assertEquals($invoice['invoice']['detail_package']['id'], $package->id);
    }

    public function testGetDetailInvoiceBalanceRequest()
    {
        $user = factory(User::class)->create();
        $package = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_package_id' => $package->id,
            'premium_request_id' => $premiumRequest->id,
            'status' => BalanceRequest::TOPUP_WAITING,
            'expired_status' => null
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => 0,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING,
            'payment_type' => 'echannel',
            'biller_code' => 70012,
            'total' => 1000980,
            'source' => Payment::PAYMENT_SOURCE_BALANCE_REQUEST,
            'source_id' => $balanceRequest->id
        ]);

        factory(MamipayInvoices::class)->create([
            'order_id' => $balanceRequest->id,
            'order_type' => BalanceRequest::ORDER_TYPE
        ]);

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertTrue($invoice['status']);
        $this->assertEquals($invoice['invoice']['selected_payment'], 'Bank Mandiri');
        $this->assertEquals($invoice['invoice']['detail_package']['id'], $package->id);
    }

    public function testGetDetailInvoicePaymentFromIndomaret()
    {
        $user = factory(User::class)->create();
        $package = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING,
            'payment_type' => 'cstore indomaret',
            'total' => null
        ]);

        factory(MamipayInvoices::class)->create([
            'order_id' => $premiumRequest->id,
            'order_type' => PremiumRequest::ORDER_TYPE
        ]);

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertTrue($invoice['status']);
        $this->assertEquals($invoice['invoice']['selected_payment'], 'Indomaret');
        $this->assertEquals($invoice['invoice']['detail_package']['id'], $package->id);
    }

    public function testGetDetailInvoiceWithEmptyPayment()
    {
        $user = factory(User::class)->create();
        $package = factory(PremiumPackage::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);

        factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'transaction_status' => Payment::MIDTRANS_STATUS_PENDING,
            'payment_type' => null,
            'total' => null
        ]);

        factory(MamipayInvoices::class)->create([
            'order_id' => $premiumRequest->id,
            'order_type' => PremiumRequest::ORDER_TYPE
        ]);

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertTrue($invoice['status']);
        $this->assertNull($invoice['invoice']['checkout']);
        $this->assertEquals($invoice['invoice']['detail_package']['id'], $package->id);
    }

    public function testGetEmptyInvoiceWithStatusPremium()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('+2 days'))
        ]);

        factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            "user_id"   => $user->id
        ]);

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertEquals($invoice['invoice']['status'], PremiumRequestStatus::PREMIUM);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetIntegrateInvoiceUniversal()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('-2 days'))
        ]);
        $package = factory(PremiumPackage::class)->create();
        factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);

        $helperMock = Mockery::mock('overload:' . MamipayApiHelper::class);

        $helperMock->shouldReceive('makeRequest')
            ->andReturnUsing(function() {
                $data = new \StdClass;
                $data->id = 1;
                $data->expiry_time = date('Y-m-d H:i:s', strtotime('+2 hours'));
                $data->invoice_number = 'CP/20210224/12/34567';

                $resp = new \StdClass;
                $resp->data = $data;

                return $resp;
            });

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertNotNull($invoice['invoice']['invoice_url']);

    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetIntegrateInvoiceUniversalWithException()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('-2 days'))
        ]);
        $package = factory(PremiumPackage::class)->create();
        factory(PremiumRequest::class)->create([
            "expired_status" => null,
            "status" => PremiumRequest::PREMIUM_REQUEST_WAITING,
            "user_id"   => $user->id,
            "premium_package_id" => $package->id
        ]);

        $helperMock = Mockery::mock('overload:' . MamipayApiHelper::class);

        $helperMock->shouldReceive('makeRequest')
            ->andReturnUsing(function() {

                $resp = new \StdClass;
                $resp->status_code = 140021;

                return $resp;
            });

        $bugsnagMock = Mockery::mock('overload:' . BugSnag::class);
        $bugsnagMock->shouldReceive('notifyException')->once();

        $invoice = $this->repository->getDetailInvoice($user);

        $this->assertEmpty($invoice['invoice']['invoice_url']);
    }

    protected function createPremiumRequest($premiumPackage, $userId)
    {
        return factory(PremiumRequest::class)->create([
            'user_id' => $userId,
            'premium_package_id' => $premiumPackage->id,
            'status' => "1",
            'total' => $premiumPackage->price,
            'view' => $premiumPackage->view,
            'used' => 0,
            'allocated' => 0,
            'expired_date' => null,
            'expired_status' => null
        ]);
    }

    protected function createPremiumPackage()
    {
        return factory(PremiumPackage::class)->create([
            'name' => 'Paket test',
            'is_active' => '1',
            'price' => 500,
            'sale_price' => 0,
            'special_price' => null,
            'sale_limit_date' => '2019-01-01',
            'view' => 500
        ]);
    }

    public function testSendWhatsAppNotificationFail()
    {
        $user = factory(User::class)->create();
        $params = [
            'fullname' => 'Alucard'
        ];
        $templateName = '';
        $response = $this->repository->sendWhatsAppNotification($user, $params, $templateName);
        $this->assertFalse($response);
    }

    public function testSendWhatsappNotificationSuccess()
    {
        $user = factory(User::class)->create();
        factory(NotificationWhatsappTemplate::class)->create([
            'is_active' => true,
            'name' => 'dummy'
        ]);

        $response = $this->repository->sendWhatsAppNotification($user, [], 'dummy');
        $this->assertTrue($response);

    }

    public function testPaymentInformation()
    {
        $payment = factory(Payment::class)->create();
        $data = [
            'payment_code' => '123',
            'payment_number' => 123,
            'amount' => 12000,
            'admin_fee' => 0,
            'method' => 'bank_transfer',
        ];

        $response = $this->repository->paymentInformation($payment, $data, 5000);
        $this->assertEquals(count($response), 2);
    }

}