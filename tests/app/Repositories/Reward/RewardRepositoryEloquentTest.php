<?php

namespace App\Repositories\Reward;

use App\Entities\Level\KostLevel;
use App\Entities\Media\Media;
use App\Entities\Point\Point;
use App\Entities\Point\PointBlacklistConfig;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use App\Entities\Reward\RewardTargetConfig;
use App\Entities\Reward\RewardType;
use App\Entities\Revision\Revision;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification AS NotifOwner;
use App\Presenters\RewardPresenter;
use App\Repositories\Reward\RewardRepository;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;

class RewardRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repo;

    private $presenter;

    private $user;
    
    private $reward;

    private $pointOwner;

    private $pointTenant;

    private $kostGpLevel1;
    private $kostGpLevel2;
    private $kostGpLevel3;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = app()->make(RewardRepository::class);

        $this->presenter = new RewardPresenter();

        $this->user = factory(User::class)->state('owner')->create();
        app()->user = $this->user;

        $media = factory(Media::class)->create();
        $type = factory(RewardType::class)->create();
        $this->reward = factory(Reward::class)->create([
            'media_id' => $media->id,
            'type_id' => $type->id,
            'redeem_value' => 25,
            'user_target' => Reward::ALL_REWARD_USER_TARGET,
            'is_active' => 1,
            'is_published' => 1,
            'is_testing' => 0,
            'redeem_currency' => 'point',
        ]);
        $this->reward->target()->attach($this->user->id);

        $this->reward->quotas()->createMany([
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_TOTAL_USER, 'value' => 10])->toArray(),
            factory(RewardQuota::class)->make(['type' => RewardQuota::TYPE_DAILY_USER, 'value' => 10])->toArray(),
        ]);
        $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $this->user->id,
            'status' => RewardRedeem::STATUS_FAILED
        ]));

        $this->pointOwner = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER
        ]);
        $this->pointTenant = factory(Point::class)->create([
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT
        ]);
        $this->kostGpLevel1 = factory(KostLevel::class)->state('gp_level_1')->create();
        $this->kostGpLevel2 = factory(KostLevel::class)->state('gp_level_2')->create();
        $this->kostGpLevel3 = factory(KostLevel::class)->state('gp_level_3')->create();
    }

    public function testGetListForAdmin_ShouldSuccess()
    {
        $result = $this->repo->getListForAdmin(2, [
            'keyword' => '',
            'start_date' => '',
            'end_date' => '',
            'status' => '',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetListForAdmin_WithSortBySequence_ShouldSuccess()
    {
        $result = $this->repo->getListForAdmin(2, [
            'keyword' => '',
            'start_date' => '',
            'end_date' => '',
            'status' => '',
            'user_target' => '',
            'sortBy' => 'sequence',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetListForAdmin_WithSortByRemainingQuota_ShouldSuccess()
    {
        $result = $this->repo->getListForAdmin(2, [
            'keyword' => '',
            'start_date' => '',
            'end_date' => '',
            'status' => '',
            'user_target' => '',
            'sortBy' => 'remaining_quota',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetListForAdmin_WithSearchAttributes_ForNonTestingRecord_ShouldSuccess()
    {
        $result = $this->repo->getListForAdmin(2, [
            'keyword' => $this->reward->name,
            'start_date' => Carbon::now()->startOfDay()->subMonths(3),
            'end_date' => Carbon::now()->endOfDay()->addMonths(3),
            'status' => $this->reward->is_active,
            'user_target' => $this->reward->user_target,
            'sortBy' => 'redeem_value',
            'show_testing' => '0',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetListForAdmin_WithSearchAttributes_ForTestingRecord_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->is_testing = 1;
        $reward->save();

        $result = $this->repo->getListForAdmin(2, [
            'keyword' => $this->reward->name,
            'start_date' => Carbon::now()->startOfDay()->subMonths(3),
            'end_date' => Carbon::now()->endOfDay()->addMonths(3),
            'status' => $this->reward->is_active,
            'user_target' => $this->reward->user_target,
            'sortBy' => 'redeem_value',
            'show_testing' => '1',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetListForAdmin_WithSearchAttributes_ForAllRecord_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->is_testing = 1;
        $reward->save();

        $result = $this->repo->getListForAdmin(2, [
            'keyword' => $this->reward->name,
            'start_date' => Carbon::now()->startOfDay()->subMonths(3),
            'end_date' => Carbon::now()->endOfDay()->addMonths(3),
            'status' => $this->reward->is_active,
            'user_target' => $this->reward->user_target,
            'sortBy' => 'redeem_value',
            'show_testing' => '2',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetRewardRevisions_WithoutRevision_ShouldSuccess()
    {
        $reward = $this->reward;
        
        $result = $this->repo->getRewardRevisions($reward);

        $this->assertEquals(0, count($result['data']));
    }

    public function testGetRewardRevisions_WithRevision_ShouldSuccess()
    {
        $reward = $this->reward;
        $user = $this->user;

        factory(Revision::class)->create([
            'revisionable_type' => Reward::class,
            'revisionable_id' => $reward->id,
            'user_id' => $user->id,
            'key' => 'created_at',
            'created_at' => Carbon::now()->subMinutes(3)
        ]);
        factory(Revision::class)->create([
            'revisionable_type' => Reward::class,
            'revisionable_id' => $reward->id,
            'user_id' => $user->id,
            'key' => 'is_active',
            'new_value' => 1,
            'created_at' => Carbon::now()->subMinutes(2)
        ]);
        factory(Revision::class)->create([
            'revisionable_type' => Reward::class,
            'revisionable_id' => $reward->id,
            'user_id' => $user->id,
            'key' => 'name',
            'created_at' => Carbon::now()->subMinutes(1)
        ]);

        $result = $this->repo->getRewardRevisions($reward);

        $this->assertEquals(3, count($result['data']));
    }

    public function testGetListByDate_WithoutStartEndDate_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);
        
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetListByDate_WithoutStartEndDate_NotEffectiveByToday_ShouldFail()
    {
        $reward = $this->reward;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(2);
        $reward->end_date = Carbon::now()->endOfDay()->subDays(1);
        $reward->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);

        $this->assertEquals(0, count($result['data']));
    }

    public function testGetListByDate_WithStartEndDate_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user, Carbon::now()->startOfDay()->subDays(1), Carbon::now()->endOfDay()->addDays(1));
        
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetListByDate_WithStartEndDate_NotEffectiveByTomorrow_ShouldFail()
    {
        $reward = $this->reward;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(2);
        $reward->end_date = Carbon::now()->endOfDay()->subDays(1);
        $reward->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user, Carbon::now()->startOfDay()->addDays(1), Carbon::now()->endOfDay()->addDays(2));
        
        $this->assertEquals(0, count($result['data']));
    }

    public function testGetListByDate_WithInvalidOwnerUserTarget_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->user->is_owner = false;
        $this->user->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);
        
        $this->assertEquals(0, count($result['data']));
    }

    public function testGetListByDate_WithValidOwnerUserTarget_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);
        
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetListByDate_WithInvalidTenantUserTarget_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);
        
        $this->assertEquals(0, count($result['data']));
    }

    public function testGetListByDate_WithValidTenantUserTarget_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $this->user->is_owner = false;
        $this->user->save();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($this->user);
        
        $this->assertEquals(1, count($result['data']));
    }

    public function testGetListByDate_WithUntargetedUser_ShouldFail()
    {
        $reward = $this->reward;
        $reward->start_date = Carbon::now()->startOfDay()->subDays(1);
        $reward->end_date = Carbon::now()->endOfDay()->addDays(1);
        $reward->save();

        $user = factory(User::class)->create();

        $this->repo->setPresenter($this->presenter);
        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $result = $this->repo->getListByDate($user);
        
        $this->assertEquals(0, count($result['data']));
    }

    public function testGetDetail_WithAllUserTarget_ShouldSuccess()
    {
        $result = $this->repo->getDetail($this->reward->id, $this->user);

        $this->assertInstanceOf(Reward::class, $result);
    }

    public function testGetDetail_WithInvalidOwnerUserTarget_ShouldFail()
    {
        $this->reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $this->reward->save();

        $this->user->is_owner = false;
        $this->user->save();

        $result = $this->repo->getDetail($this->reward->id, $this->user);

        $this->assertNull($result);
    }

    public function testGetDetail_WithValidOwnerUserTarget_ShouldSuccess()
    {
        $this->reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $this->reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $result = $this->repo->getDetail($this->reward->id, $this->user);

        $this->assertInstanceOf(Reward::class, $result);
    }

    public function testGetDetail_WithInvalidTenantUserTarget_ShouldFail()
    {
        $this->reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $this->reward->save();

        $this->user->is_owner = true;
        $this->user->save();

        $result = $this->repo->getDetail($this->reward->id, $this->user);

        $this->assertNull($result);
    }

    public function testGetDetail_WithValidTenantUserTarget_ShouldSuccess()
    {
        $this->reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $this->reward->save();

        $this->user->is_owner = false;
        $this->user->save();

        $result = $this->repo->getDetail($this->reward->id, $this->user);

        $this->assertInstanceOf(Reward::class, $result);
    }

    public function testGetDetail_WithUntargetedUser_ShouldFail()
    {
        $user = factory(User::class)->create();

        $result = $this->repo->getDetail($this->reward->id, $user);

        $this->assertNull($result);
    }

    public function testGetRewardRedeemList_ShouldSuccess()
    {
        $result = $this->repo->getRewardRedeemList($this->reward, 2, [
            'keyword' => '',
            'start_date' => '',
            'end_date' => '',
            'user' => '',
            'status' => '',
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testGetRewardRedeemList_WithSearchAttributes_ShouldSuccess()
    {
        $result = $this->repo->getRewardRedeemList($this->reward, 2, [
            'keyword' => $this->user->name,
            'start_date' => Carbon::now()->startOfDay(),
            'end_date' => Carbon::now()->endOfDay(),
            'user' => 'owner',
            'status' => $this->reward->redeems->first()->status,
        ]);

        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
    }

    public function testUpdateRedeemStatus_ShouldSuccess()
    {
        $redeem = $this->reward->redeems->first();
        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_SUCCESS, $this->user, 'admin notes');

        $this->assertTrue($result);
        $this->assertEquals(1, $redeem->status_history->count());
    }

    public function testUpdateRedeemStatus_OnlyEditNotesButSameStatus_ShouldSuccess()
    {
        $updatedNotes = 'admin notes updated';
        $redeem = $this->reward->redeems->first();
        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_FAILED, $this->user, $updatedNotes);

        $this->assertTrue($result);
        $this->assertEquals($updatedNotes, $redeem->notes);
        
        // Update redeem status with same status value
        // history should not be added
        $this->assertEquals(0, $redeem->status_history->count());
    }

    public function testUpdateRedeemStatus_ByRejectRedeem_ShouldSuccess()
    {
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $this->user->id,
            'is_blacklisted' => 0,
            'total' => 10
        ]);

        $redeem = $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $this->user->id,
            'status' => RewardRedeem::STATUS_ONPROCESS
        ]));

        $redeem->status_history()->save(factory(RewardRedeemStatusHistory::class)->make([
            'user_id' => $this->user->id,
            'action' => RewardRedeem::STATUS_REDEEMED
        ]));

        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_FAILED, $this->user, 'admin notes');

        $this->assertTrue($result);
        $redeem = $this->reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->reward->id, $redeem->reward_id);
        $this->assertEquals($this->user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_FAILED, $redeem->status);
        $this->assertEquals('admin notes', $redeem->notes);
        
        $redeemHistory = $redeem->status_history()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_FAILED, $redeemHistory->action);

        $pointUser = PointUser::where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
        $this->assertEquals(10 + $this->reward->redeem_value, $pointUser->total);

        $pointHistory = PointHistory::where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
        $this->assertEquals($redeem->id, $pointHistory->redeem_id);
        $this->assertEquals($this->reward->redeem_value, $pointHistory->value);
        $this->assertEquals($pointUser->total, $pointHistory->balance);
        $this->assertEquals('Return from rejected redeem', $pointHistory->notes);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($this->pointOwner->expiry_value)->endOfMonth(), $pointHistory->expired_date);
    }

    public function testUpdateRedeemStatus_ByRejectRedeem_WithInvalidUser_ShouldFail()
    {
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $this->user->id,
            'is_blacklisted' => 0,
            'total' => 10
        ]);

        $redeem = $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => 999,
            'status' => RewardRedeem::STATUS_ONPROCESS
        ]));

        $redeem->status_history()->save(factory(RewardRedeemStatusHistory::class)->make([
            'user_id' => 999,
            'action' => RewardRedeem::STATUS_REDEEMED
        ]));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Reward user is invalid.');

        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_FAILED, $this->user, 'admin notes');

        $this->assertFalse($result);
        $redeem = $this->reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->reward->id, $redeem->reward_id);
        $this->assertEquals(999, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);
        
        $redeemHistory = $redeem->status_history()->orderBy('id', 'DESC')->first();
        $this->assertEquals(999, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);

        $pointUser = PointUser::where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
        $this->assertEquals(0, $pointUser->is_blacklisted);
        $this->assertEquals(10, $pointUser->total);
    }

    public function testUpdateRedeemStatus_ByRejectRedeem_WithInvalidPointUser_ShouldFail()
    {
        $redeem = $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $this->user->id,
            'status' => RewardRedeem::STATUS_ONPROCESS
        ]));

        $redeem->status_history()->save(factory(RewardRedeemStatusHistory::class)->make([
            'user_id' => $this->user->id,
            'action' => RewardRedeem::STATUS_REDEEMED
        ]));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Point user does not exist.');

        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_FAILED, $this->user, 'admin notes');

        $this->assertFalse($result);
        $redeem = $this->reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->reward->id, $redeem->reward_id);
        $this->assertEquals($this->user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);
        
        $redeemHistory = $redeem->status_history()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);

        $pointUser = PointUser::where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
        $this->assertNull($pointUser);
    }

    public function testUpdateRedeemStatus_ByRejectRedeem_WithBlacklistedPoint_ShouldSuccess()
    {
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $this->user->id,
            'is_blacklisted' => 1,
            'total' => 10
        ]);

        $redeem = $this->reward->redeems()->save(factory(RewardRedeem::class)->make([
            'user_id' => $this->user->id,
            'status' => RewardRedeem::STATUS_ONPROCESS
        ]));

        $redeem->status_history()->save(factory(RewardRedeemStatusHistory::class)->make([
            'user_id' => $this->user->id,
            'action' => RewardRedeem::STATUS_REDEEMED
        ]));

        $result = $this->repo->updateRedeemStatus($this->reward, $redeem, RewardRedeem::STATUS_FAILED, $this->user, 'admin notes');

        $this->assertTrue($result);
        $redeem = $this->reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->reward->id, $redeem->reward_id);
        $this->assertEquals($this->user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_FAILED, $redeem->status);
        
        $redeemHistory = $redeem->status_history()->orderBy('id', 'DESC')->first();
        $this->assertEquals($this->user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_FAILED, $redeemHistory->action);

        $pointUser = PointUser::where('user_id', $this->user->id)->orderBy('id', 'DESC')->first();
        $this->assertEquals(1, $pointUser->is_blacklisted);
        $this->assertEquals(35, $pointUser->total);
    }

    public function testCreateReward_ShouldSuccess()
    {
        $reward = $this->repo->createReward($this->rewardForm());

        $this->assertInstanceOf(Reward::class, $reward);
        $this->assertDatabaseHas((new Reward)->getTable(), [
            'name' => 'Dummy name',
            'sequence' => 6,
        ]);
    }

    public function testCreateReward_WithTargetConfigs_ShouldSuccess()
    {
        $reward = $this->repo->createReward($this->rewardForm([
            'owner_segment' => [
                'goldplus1' => 1
            ]
        ]));

        $this->assertInstanceOf(Reward::class, $reward);
        $this->assertDatabaseHas((new Reward)->getTable(), [
            'name' => 'Dummy name'
        ]);
        $this->assertDatabaseHas((new RewardTargetConfig)->getTable(), [
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]);
    }

    public function testUpdateReward_WithException_ShouldFailed()
    {
        $reward = $this->mockPartialAlternatively(Reward::class);
        $reward->shouldReceive('fill')->andThrow(new \Exception);

        $this->expectException(\Exception::class);

        $reward = $this->repo->updateReward($reward, []);
    }
    
    public function testUpdateReward_ShouldSuccess()
    {
        $reward = $this->repo->updateReward($this->reward, $this->rewardForm());

        $this->assertInstanceOf(Reward::class, $reward);
        $this->assertDatabaseHas((new Reward)->getTable(), [
            'name' => 'Dummy name',
            'sequence' => 6,
        ]);
    }

    public function testUpdateReward_WithTargetConfigs_ShouldSuccess()
    {
        $this->reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        $this->reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel3->id
        ]));

        $reward = $this->repo->updateReward($this->reward, $this->rewardForm([
            'owner_segment' => [
                'goldplus1' => 0,
                'goldplus2' => 1
            ]
        ]));

        $this->assertInstanceOf(Reward::class, $reward);
        $this->assertDatabaseHas((new Reward)->getTable(), [
            'name' => 'Dummy name'
        ]);
        $this->assertDatabaseHas((new RewardTargetConfig)->getTable(), [
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel2->id
        ]);
    }

    protected function rewardForm(array $data = [])
    {
        return array_merge([
            'type_id' => 1,
            'name' => 'Dummy name',
            'description' => 'Dummy description',
            'start_date' => '2020-06-01 00:00:00',
            'end_date' => '2020-06-20 00:00:00',
            'redeem_value' => 10,
            'redeem_currency' => 'point',
            'tnc' => 'Dummy tnc',
            'howto' => 'Dummy howto',
            'sequence' => 6,
            'user_target' => Reward::OWNER_REWARD_USER_TARGET,
            'quota' => [
                'total' => 15
            ],
            'target_user_ids' => [
                $this->user->id
            ],
            'is_active' => 0,
            'is_published' => 0,
        ], $data);
    }

    public function testGetUserRedeemList_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);
        
        $result = $this->repo->getUserRedeemList($user);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetUserRedeemList_WithBlacklistPointUser_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 1,
            'total' => 500
        ]);
        
        $result = $this->repo->getUserRedeemList($user, RewardRedeem::STATUS_ONPROCESS);
        $this->assertNull($result);
    }

    public function testGetUserRedeemList_WithOnProcessStatus_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);
        
        $result = $this->repo->getUserRedeemList($user, RewardRedeem::STATUS_ONPROCESS);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(0, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetUserRedeemList_WithSpecificStatus_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);
        
        $result = $this->repo->getUserRedeemList($user, RewardRedeem::STATUS_FAILED);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetUserRedeemDetail_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);

        $result = $this->repo->getUserRedeemDetail($user, $redeem->public_id);

        $this->assertInstanceOf(RewardRedeem::class, $result);
        $this->assertEquals($result->id, $redeem->id);
        $this->assertEquals($result->public_id, $redeem->public_id);
        $this->assertEquals($result->status, $redeem->status);
        $this->assertEquals($result->notes, $redeem->notes);
        $this->assertDatabaseHas((new Reward)->getTable(), [
            'id' => $redeem->reward->id,
            'name' => $redeem->reward->name,
            'redeem_value' => $redeem->reward->redeem_value,
        ]);
    }

    public function testGetUserRedeemDetail_WithBlacklistPointUser_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 1,
            'total' => 500
        ]);

        $result = $this->repo->getUserRedeemDetail($user, $redeem->public_id);

        $this->assertNull($result);
    }

    public function testGetUserRedeemDetail_WithOtherUser_ShouldSuccess()
    {
        $reward = $this->reward;
        $redeem = $reward->redeems->first();
        $user = factory(User::class)->state('owner')->create();
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 500
        ]);

        $result = $this->repo->getUserRedeemDetail($user, $redeem->public_id);

        $this->assertNull($result);
    }

    public function testRedeem_WithBlacklistedPoint_ShouldFail()
    {
        $reward = $this->reward;
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 1
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Your point has been blacklisted');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithPointBlacklistConfig_ShouldFail()
    {
        $reward = $this->reward;
        $user = $this->user;
        $pointUser = factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0
        ]);
        $kostLevelGoldplus1 = factory(KostLevel::class)->states('goldplus')->create();
        $room = factory(Room::class)->create();
        $room->level()->attach($kostLevelGoldplus1);
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        factory(PointBlacklistConfig::class)->create([
            'usertype' => 'kost_level',
            'value' => $kostLevelGoldplus1->id,
            'is_blacklisted' => 1,
        ]);

        $this->expectExceptionCode(400);
        $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithInvalidReward_ShouldFail()
    {
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(404);
        $this->expectExceptionMessage('Reward not found or doesnt exists');

        $result = $this->repo->redeem(999, $user);
    }

    public function testRedeem_WithInvalidTargetUser_ShouldFail()
    {
        $reward = $this->reward;
        $user = factory(User::class)->create();
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(404);
        $this->expectExceptionMessage('Reward not found or doesnt exists');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithInvalidRewardQuota_ShouldFail()
    {
        $reward = $this->reward;
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0
        ]);

        $quota = $reward->quotas()->first();
        $quota->value = 0;
        $quota->save();

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Reward quota is empty or not available');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithInsufficientPoint_ShouldFail()
    {
        $reward = $this->reward;
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 10
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Your current point is not enough to redeem this reward');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithInvalidOwnerTarget_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = false;
        $user->save();

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(404);
        $this->expectExceptionMessage('Reward not found or doesnt exists');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithValidOwnerTarget_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);
    }

    public function testRedeem_WithTargetedOwnerSegment_WithNonGoldPlusOwner_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);
        }

        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Reward is not targeted to this user segment');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithTargetedOwnerSegment_WithUntargetedGoldPlusOwner_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->kostGpLevel2->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Reward is not targeted to this user segment');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithTargetedOwnerSegment_WithTargetedGoldPlusOwner_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->kostGpLevel1->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);
    }

    public function testRedeem_WithTargetedOwnerSegment_WithMultiTargetedGoldPlusOwner_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);

            $rooms[$i]->level()->attach($this->{'kostGpLevel'.($i+1)}->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);
    }

    public function testRedeem_WithMultiTargetedOwnerSegment_WithTargetedGoldPlusOwner_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::OWNER_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();

        $rooms = factory(Room::class, 3)->create([
            'user_id' => $user->id,
        ]);

        for ($i=0; $i < 3; $i++) { 
            factory(RoomOwner::class)->create([
                'user_id' => $user->id,
                'designer_id' => $rooms[$i]->id,
                'status' => 'verified'
            ]);
            
            $rooms[$i]->level()->attach($this->kostGpLevel3->id);
        }
        
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel1->id
        ]));
        $reward->target_configs()->save(factory(RewardTargetConfig::class)->make([
            'type' => RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE,
            'value' => $this->kostGpLevel3->id
        ]));
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);
    }

    public function testRedeem_WithInvalidTenantTarget_ShouldFail()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = true;
        $user->save();
        
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(404);
        $this->expectExceptionMessage('Reward not found or doesnt exists');

        $result = $this->repo->redeem($reward->id, $user);
    }

    public function testRedeem_WithValidTenantTarget_ShouldSuccess()
    {
        $reward = $this->reward;
        $reward->user_target = Reward::TENANT_REWARD_USER_TARGET;
        $reward->save();

        $user = $this->user;
        $user->is_owner = false;
        $user->save();
        
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);
    }

    public function testRedeem_ShouldSuccess()
    {
        $reward = $this->reward;
        $user = $this->user;
        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'is_blacklisted' => 0,
            'total' => 25
        ]);

        $result = $this->repo->redeem($reward->id, $user);
        $this->assertTrue(true, $result);
        $redeem = $reward->redeems()->orderBy('id', 'DESC')->first();
        $this->assertEquals($reward->id, $redeem->reward_id);
        $this->assertEquals($user->id, $redeem->user_id);
        $this->assertEquals(RewardRedeem::STATUS_ONPROCESS, $redeem->status);

        $redeemHistory = $redeem->status_history()->first();
        $this->assertEquals($user->id, $redeemHistory->user_id);
        $this->assertEquals(RewardRedeem::STATUS_REDEEMED, $redeemHistory->action);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '25 poin berhasil ditukarkan'));
    }
}
