<?php

namespace App\Repositories\Dbet;

use App\Test\MamiKosTestCase;
use App;
use App\Entities\Room\Room;
use App\Entities\Dbet\DbetLink;
use Illuminate\Pagination\LengthAwarePaginator;

class DbetLinkRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(DbetLinkRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals('App\Entities\Dbet\DbetLink', $this->repository->model());
    }

    public function testFindByDesignerIdIsNull(): void
    {
        $result = $this->repository->findByDesignerId(1);

        // run test
        $this->assertNull($result);
    }

    public function testFindByDesignerIdIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        factory(DbetLink::class)->create([
            'designer_id' => $room->id,
        ]);
        $result = $this->repository->findByDesignerId($room->id);

        // run test
        $this->assertNotNull($result);
    }

    public function testGetListAdminSuccess(): void
    {
        // prepare data
        factory(DbetLink::class)->create();

        // run test
        $result = $this->repository->getListAdmin(null, null, 20);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
    }

    public function testFindByCodeIsNull(): void
    {
        // prepare data
        $code = str_random(5);
        $result = $this->repository->findByCode($code);

        // run test
        $this->assertNull($result);
    }

    public function testFindByCodeIsSuccess(): void
    {
        // prepare data
        $room = factory(Room::class)->create();
        $code = str_random(5);
        factory(DbetLink::class)->create([
            'code' => $code,
            'designer_id' => $room->id,
        ]);
        $result = $this->repository->findByCode($code);

        // run test
        $this->assertNotNull($result);
    }
}