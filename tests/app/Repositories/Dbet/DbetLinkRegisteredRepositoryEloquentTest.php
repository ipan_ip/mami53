<?php

namespace App\Repositories\Dbet;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App;
use App\Entities\Dbet\DbetLinkRegistered;
use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class DbetLinkRegisteredRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(DbetLinkRegisteredRepositoryEloquent::class);
    }

    public function testModel(): void
    {
        $this->assertEquals('App\Entities\Dbet\DbetLinkRegistered', $this->repository->model());
    }

    public function testGetListContractSubmissionSuccess(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        factory(DbetLinkRegistered::class)->create([
            'designer_id' => $roomEntity->id
        ]);

        $params = [];
        $params['limit'] = 1;

        // run test
        $data = $this->repository->getListContractSubmission($params);
        $this->assertInstanceOf(LengthAwarePaginator::class, $data);
        $this->assertIsArray($data->items());
        $this->assertEquals($params['limit'], $data->perPage());
    }

    public function testGetHomepageShortcutDataIsNull(): void
    {
        // run test
        $data = $this->repository->getHomepageShortcutData();
        $this->assertNull($data);
    }

    public function testGetHomepageShortcutDataSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(DbetLinkRegistered::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => DbetLinkRegistered::STATUS_PENDING
        ]);

        // run test
        $data = $this->repository->getHomepageShortcutData($userEntity->id);
        $this->assertInstanceOf(DbetLinkRegistered::class, $data);
    }

    public function testGetReminderPendingContractSubmissionSuccess(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(DbetLinkRegistered::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => DbetLinkRegistered::STATUS_PENDING
        ]);
        // run test
        $data = $this->repository->getReminderPendingContractSubmission();
        $this->assertInstanceOf(Collection::class, $data);
    }
}