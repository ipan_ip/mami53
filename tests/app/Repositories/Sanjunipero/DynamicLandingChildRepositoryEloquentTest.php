<?php

namespace App\Repositories\Sanjunipero;

use App\Test\MamiKosTestCase;
use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class DynamicLandingChildRepositoryEloquentTest extends MamiKosTestCase
{
    private const SAVE = 'admin/sanjunipero/child/save';
    private const EDIT = 'admin/sanjunipero/child/update';

    private $parentRepo, $childRepo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->parentRepo = $this->app->make(DynamicLandingParentRepositoryEloquent::class);
        $this->childRepo = $this->app->make(DynamicLandingChildRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function testModel_Success()
    {
        $this->assertSame(DynamicLandingChild::class, $this->childRepo->model());
    }

    public function testIndex_Success()
    {
        $this->assertInstanceOf(LengthAwarePaginator::class, $this->childRepo->index());
    }

    public function testSave_Success()
    {
        $parent = $this->createParentRecord();
        $fakeRequest = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'parent_id' => $parent->id,
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->childRepo->save($fakeRequest));
    }

    public function testFindById_Success()
    {
        $parent = factory(DynamicLandingParent::class)->create();
        $child = factory(DynamicLandingChild::class)->create(['parent_id' => $parent->id]);
        $this->assertInstanceOf(DynamicLandingChild::class, $this->childRepo->findById($child->id));
    }

    public function testFindById_Null()
    {
        $this->assertNull($this->childRepo->findById(mt_rand(0, 99)));
    }

    public function testEdit_Success()
    {
        $parent = $this->createParentRecord();
        $child = factory(DynamicLandingChild::class)->create(['parent_id' => $parent->id]);
        $fakeRequest = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'parent_id' => $parent->id,
                'area_type' => DynamicLandingChild::AREA_TYPE,
                'area_name' => $this->faker->state(),
                'slug' => $this->faker->slug(),
                'coordinate_1' => $this->faker->latitude().','.$this->faker->longitude(),
                'coordinate_2' => $this->faker->latitude().','.$this->faker->longitude(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'is_active' => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->childRepo->edit($fakeRequest, $child->id));
    }

    public function testActivate_Success()
    {
        $parent = $this->createParentRecord();
        $child = factory(DynamicLandingChild::class)->states('inactive')->create(['parent_id' => $parent->id]);
        $res = $this->childRepo->activate($child->id);
        $this->assertTrue($res->is_active);
    }

    public function testDeactivate_Success()
    {
        $parent = $this->createParentRecord();
        $child = factory(DynamicLandingChild::class)->states('active')->create(['parent_id' => $parent->id]);
        $res = $this->childRepo->deactivate($child->id);
        $this->assertFalse($res->is_active);
    }

    private function createParentRecord()
    {
        return factory(DynamicLandingParent::class)->create();
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}
