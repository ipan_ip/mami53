<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class RoomRepositoryEloquentTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->app->make(RoomRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function testModel_Success()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    public function testBase_Success()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->base());
    }

    public function testAttribute_Success()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->attribute($this->repo->base()));
    }

    public function testShow_EmptyQueryString()
    {
        $length = 5;
        $rooms = factory(Room::class, $length)
            ->states('active', 'notTesting', 'with-room-unit')
            ->create([
                'updated_at' => Carbon::now()->toDateTimeString(),
                'price_monthly' => 9999999999,
                'sort_score' => 5000
            ]);
        $roomsId = $rooms->pluck('id')->toArray();

        $res = $this->repo->show(
                $this->repo->attribute($this->repo->base()),
                '',
                'price',
                2,
                'DESC',
                0,
                5
            );
        $resId = $res->pluck('id')->toArray();

        $this->assertInstanceof(Collection::class, $res);
        $this->assertEquals($length, count(array_intersect($roomsId, $resId)));
    }

    public function testShow_NotEmptyQueryString()
    {
        $rooms = factory(Room::class, 5)
            ->states('active', 'mamirooms', 'notTesting', 'with-room-unit')
            ->create([
                'updated_at' => Carbon::now()->toDateTimeString(),
                'sort_score' => 5000
            ]);
        $roomsId = $rooms->pluck('id');

        $res = $this->repo->show(
                $this->repo->attribute($this->repo->base()),
                '((is_mamirooms = 1))',
                'price',
                2,
                '-',
                0,
                5
            );

        $this->assertInstanceof(Collection::class, $res);
        $this->assertFalse(
            empty(array_intersect(
                $roomsId->toArray(),
                $res->pluck('id')->toArray()
            ))
        );
    }

    public function testGetCount_EmptyQueryString()
    {
        factory(Room::class, 5)
            ->states('active', 'notTesting', 'with-room-unit')
            ->create([
                'updated_at' => Carbon::now()->toDateTimeString(),
                'sort_score' => 5000
            ]);

        $res = $this->repo->getCount(
            $this->repo->base(),
            ''
        );

        $this->assertIsInt($res);
    }

    public function testGetCount_NotEmptyQueryString()
    {
        factory(Room::class, 5)
            ->states('active', 'mamirooms', 'notTesting', 'with-room-unit')
            ->create([
                'updated_at' => Carbon::now()->toDateTimeString(),
                'sort_score' => 5000
            ]);

        $res = $this->repo->getCount(
            $this->repo->base(),
            '((is_mamirooms = 1))'
        );

        $this->assertIsInt($res);
    }

    public function testFiltering_Success()
    {   
        $this->assertIsArray($this->repo->filtering([
            'level_info' => "9, 10, 7",
            'mamirooms' => true,
            'include_promoted' => true,
            'mamichecker' => true,
            'virtual_tour' => true
        ]));
    }

    public function testFilterByLevel()
    {
        $this->assertIsString($this->repo->filterByLevel(["9, 10, 7"]));
    }

    public function testFilterMamirooms()
    {
        $this->assertIsString($this->repo->filterMamirooms(true));
    }

    public function testFilterPromoted()
    {
        $this->assertIsString($this->repo->filterPromoted('true'));
    }

    public function testFilterMamiChecker()
    {
        $this->assertIsString($this->repo->filterMamiChecker(true));
    }

    public function testFilterVirtualTour()
    {
        $this->assertIsString($this->repo->filterVirtualTour(true));
    }

    public function testFilterLocation()
    {
        $this->assertInstanceOf(
            Builder::class,
            $this->repo->filterLocation($this->repo->base(), 110.0, -6.00, 110.0, -6.00)
        );
    }

    public function testFilterByGender()
    {
        $this->assertInstanceOf(
            Builder::class,
            $this->repo->filterByGender($this->repo->base(), [0, 1, 2])
        );
    }

    public function testFilterByPriceRange()
    {
        $this->assertInstanceOf(
            Builder::class,
            $this->repo->filterByPriceRange(
                $this->repo->base(),
                '2',
                '1,10000'
            )
        );
    }
}