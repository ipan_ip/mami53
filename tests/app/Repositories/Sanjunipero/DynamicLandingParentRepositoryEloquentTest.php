<?php

namespace App\Repositories\Sanjunipero;

use App\Test\MamiKosTestCase;
use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class DynamicLandingParentRepositoryEloquentTest extends MamiKosTestCase
{
    private const SAVE = 'admin/sanjunipero/parent/save';
    private const EDIT = 'admin/sanjunipero/parent/update';

    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(DynamicLandingParentRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function testModel_Success()
    {
        $this->assertSame(DynamicLandingParent::class, $this->repository->model());
    }

    public function testIndex_Success()
    {
        $this->assertInstanceOf(LengthAwarePaginator::class, $this->repository->index());
    }

    public function testSave_Success()
    {
        $fakeRequest = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->repository->save($fakeRequest));
    }

    public function testFindById_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->create();
        $this->assertInstanceOf(DynamicLandingParent::class, $this->repository->findById($dynamicLandingParent->id));
    }

    public function testFindById_Null()
    {
        $this->assertNull($this->repository->findById(mt_rand(0, 99)));
    }

    public function testFindWithChildByActiveSlug()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $res = $this->repository->findWithChildByActiveSlug($dynamicLandingParent->slug);
        $this->assertInstanceOf(Collection::class, $res->children);
        $this->assertEquals($dynamicLandingChild->id, $res->children->first()->id);
    }

    public function testFindWithSpecificChildByActiveSlug()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $dynamicLandingChild = factory(DynamicLandingChild::class)->states('active')->create([
            'parent_id' => $dynamicLandingParent->id
        ]);

        $res = $this->repository->findWithSpecificChildByActiveSlug(
            $dynamicLandingParent->slug,
            $dynamicLandingChild->slug
        );
        $this->assertInstanceOf(DynamicLandingChild::class, $res->children->first());
        $this->assertEquals($dynamicLandingChild->id, $res->children->first()->id);
    }

    public function testEdit_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->create();
        $fakeRequest = $this->createFakeRequest(
            self::EDIT,
            'POST',
            [
                'slug' => $this->faker->slug(),
                'type_kost' => [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO],
                'title_tag' => $this->faker->domainName(),
                'title_header' => $this->faker->domainName(),
                'subtitle_header' => $this->faker->domainName(),
                'meta_desc' => $this->faker->sentence(),
                'meta_keywords' => $this->faker->sentence(),
                'faq_question' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'faq_answer' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'image_url' => $this->faker->imageUrl(640, 480),
                'is_active' => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->repository->edit($fakeRequest, $dynamicLandingParent->id));
    }

    public function testActivate_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('inactive')->create();
        $res = $this->repository->activate($dynamicLandingParent->id);
        $this->assertTrue($res->is_active);
    }

    public function testDeactivate_Success()
    {
        $dynamicLandingParent = factory(DynamicLandingParent::class)->states('active')->create();
        $res = $this->repository->deactivate($dynamicLandingParent->id);
        $this->assertFalse($res->is_active);
    }

    public function testGetWithChildById_Success()
    {
        $parent = factory(DynamicLandingParent::class)->states('active')->create();
        $child = factory(DynamicLandingChild::class)->create(['parent_id' => $parent->id]);
        $parent->children = $child;
        $res = $this->repository->getWithChildById($parent->id);
        
        $this->assertInstanceOf(DynamicLandingParent::class, $res);
        $this->assertInstanceOf(Collection::class, $res->children);
        $this->assertSame($parent->id, $res->id);
    }

    public function testGetWithChildById_Fail()
    {
        $res = $this->repository->getWithChildById(mt_rand(0, 999999));
        
        $this->assertNull($res);
    }

    public function testGetIdTitleHeaderSlug_Success()
    {
        $length = 5;
        $parent = factory(DynamicLandingParent::class, $length)->states('active')->create();

        $res = $this->repository->getIdTitleHeaderSlug();

        $this->assertInstanceOf(Collection::class, $res);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}
