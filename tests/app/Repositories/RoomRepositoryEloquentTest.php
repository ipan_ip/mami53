<?php

namespace App\Repositories;

use App\Entities\Activity\View;
use App\Entities\Device\UserDevice;
use App\Entities\Landing\HomeStaticLanding;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Entities\Room\HistorySlug;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use App\Entities\Room\RoomOwner;
use App\Entities\Chat\Call;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Event\EventModel;
use App\Entities\Landing\Landing;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Notif\SettingNotif;
use App\Entities\Premium\AdHistory;
use App\Entities\Property\Property;
use App\Entities\Premium\AdsInteractionTracker;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\HistoryPromote;
use App\Entities\Promoted\Promoted;
use App\Entities\Promoted\PromotedList;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Room\ContactUser;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Element\Unit;
use App\Entities\Room\RoomTerm;
use App\Libraries\SMSLibrary;
use App\Presenters\RoomPresenter;
use App\Test\MamiKosTestCase;
use App\User;
use DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Entities\Room\Review;
use RuntimeException;

class RoomRepositoryEloquentTest extends MamiKosTestCase
{
    use WithoutEvents;
    use WithFaker;

    private const TESTING_KOS_NAME = 'Kos Testing 123';

    protected $room;
    protected $repository;

    /**
     * @return array|array[]
     */
    protected function cardsArrayProvider(): array
    {
        return [
            [
                'photo' => 1,
                'description' => 'Testing Photo 1',
                'ordering' => 0
            ],
            [
                'photo' => 2,
                'description' => 'Testing Photo 2',
                'ordering' => 1
            ],
            [
                'photo' => 3,
                'description' => 'Testing Photo 3',
                'ordering' => 2
            ]
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new RoomRepositoryEloquent(app(), Collection::make());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testGetDataCount()
    {
        $rooms = factory(Room::class, 3)
            ->states(
                [
                    'active',
                    'availablePriceMonthly'
                ]
            )
            ->create();

        foreach ($rooms as $room) {
            factory(PriceFilter::class)->create(
                [
                    'designer_id' => $room->id,
                    'final_price_monthly' => $room->price_monthly
                ]
            );
        }

        $filter = new RoomFilter(
            [
                'is_active' => 'true'
            ]
        );

        $response = $this->repository->getDataCount($filter);

        $this->assertEquals(3, $response);
    }

    public function testGetItemListWithSugesstionLimit()
    {
        $this->setupRoomData();

        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;

        $user = factory(User::class)->make();
        $this->app->user = $user;

        $filters['suggestion_limit'] = true;
        $this->repository->setPresenter(new RoomPresenter('list'));
        $result = $this->repository->getItemList(new RoomFilter($filters));

        $expectedResult = [
            'page' => 1,
            'next-page' => 2,
            'limit' => 10,
            'offset' => 0,
            'has-more' => false,
            'total' => 1,
            'total-str' => '1',
            'toast' => null,
            'filter-str' => '',
            'promoted_total' => 0,
        ];

        $this->assertEquals($this->room->song_id, $result['rooms'][0]['_id']);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($result, $expectedResult);
    }

    public function testGetItemListWithoutSugesstionLimit()
    {
        $this->setupRoomData();

        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;

        $user = factory(User::class)->make();
        $this->app->user = $user;

        $this->repository->setPresenter(new RoomPresenter('list'));
        $result = $this->repository->getItemList(new RoomFilter());

        $expectedResult = [
            'page' => 1,
            'next-page' => 2,
            'limit' => 20,
            'offset' => 0,
            'has-more' => false,
            'total' => 1,
            'total-str' => '1',
            'toast' => null,
            'filter-str' => '',
            'promoted_total' => 0,
        ];

        $this->assertEquals($this->room->song_id, $result['rooms'][0]['_id']);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($result, $expectedResult);
    }

    public function testGetItemListWithoutLimitAndOffset()
    {
        $this->setupRoomData();

        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;

        $user = factory(User::class)->make();
        $this->app->user = $user;

        $limit = 5;
        $offset = 0;
        request()->merge(['limit' => $limit, 'offset' => $offset]);

        $this->repository->setPresenter(new RoomPresenter('list'));
        $result = $this->repository->getItemList(new RoomFilter());

        $expectedResult = [
            'page' => $offset + 1,
            'next-page' => $offset + 2,
            'limit' => $limit,
            'offset' => $limit * $offset,
            'has-more' => false,
            'total' => 1,
            'total-str' => '1',
            'toast' => null,
            'filter-str' => '',
            'promoted_total' => 0,
        ];

        $this->assertEquals($this->room->song_id, $result['rooms'][0]['_id']);
        $keys = array_keys($expectedResult);
        $result = array_only($result, $keys);
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetLocationByLocationIdWithValidId()
    {
        $this->setupRoomData();

        $locationId = 52595569;
        $response = $this->repository->getLocationByLocationId($locationId);

        $this->assertArraySubset(
            [
                'status' => true
            ],
            $response
        );
    }

    public function testGetLocationByLocationIdWithInvalidId()
    {
        $locationId = 123;
        $response = $this->repository->getLocationByLocationId($locationId);

        $this->assertArraySubset(
            [
                'status' => false,
                'location' => [
                    'latitude' => '',
                    'longitude' => ''
                ]
            ],
            $response
        );
    }

    public function testAddFavoriteWithInvalidSongIdAndInvalidUser()
    {
        $this->assertNull($this->repository->addFavorite(999, null));
    }

    public function testAddFavoriteWithInvalidSongIdAndValidUser()
    {
        $user = factory(User::class)->create();

        $this->assertNull($this->repository->addFavorite(999, $user));
    }

    public function testAddFavoriteWithValidSongIdAndInvalidUser()
    {
        $this->setupRoomData();

        $this->assertNull($this->repository->addFavorite($this->room->song_id));
    }

    /**
     * @runTestsInSeparateProcesses
     * @preserveGlobalState disabled
     */
    public function testAddFavoriteWithValidSongIdAndValidUser()
    {
        $this->setupRoomData();

        $user = factory(User::class)->create();

        // check on tracker that data doesn't exists
        $this->assertDatabaseMissing('designer_ads_interaction_tracker', [
            'designer_id' => $this->room->id,
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);

        // add favorite
        $result = $this->repository->addFavorite($this->room->song_id, $user);

        $this->assertEquals('love', $result);
        $this->assertDatabaseHas('designer_ads_interaction_tracker', [
            'designer_id' => $this->room->id,
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);

        // remove favorite
        $result = $this->repository->addFavorite($this->room->song_id, $user);

        $this->assertEquals('unlove', $result);
        $this->assertDatabaseMissing('designer_ads_interaction_tracker', [
            'designer_id' => $this->room->id,
            'user_id' => $user->id,
            'type' => AdsInteractionTracker::LIKE_TRACKER_TYPE
        ]);
    }

    public function testFindRoomBySlugWithValidSlug()
    {
        $this->setupRoomData();

        $this->assertInstanceOf(
            Room::class,
            $this->repository->findRoomBySlug('this-is-the-slug-please-test-me')
        );
    }

    public function testFindRoomBySlugWithFindingInSlugHistory()
    {
        $this->setupRoomData();

        $slug = 'this-is-slug-for-history';

        factory(HistorySlug::class)->create(
            [
                'designer_id' => $this->room->id,
                'slug' => $slug
            ]
        );

        $this->assertInstanceOf(
            Room::class,
            $this->repository->findRoomBySlug($slug)
        );
    }

    public function testFindRoomBySlugWithFindingInSimilarSlugInHistoryResultNull()
    {
        $slug = 'this-is-fake-slug-do-not-rely-on-this';

        $this->assertNull($this->repository->findRoomBySlug($slug));
    }

    public function testFindRoomBySlugWithFindingInSimilarSlugInHistory()
    {
        $this->setupRoomData();

        // This slug is similar with inside the Factory, but with extra suffix
        $slug = 'this-is-the-slug-please-test-me-1';

        factory(HistorySlug::class)->create(
            [
                'designer_id' => $this->room->id
            ]
        );

        $this->assertInstanceOf(
            Room::class,
            $this->repository->findRoomBySlug($slug)
        );
    }

    public function testGetPinnedPropertyWithoutPinnedType()
    {
        $tomorrow = Carbon::tomorrow();

        $room = factory(Room::class)->create([
            'longitude' => 10.00,
            'latitude' => 50.00,
        ]);
        factory(PromotedList::class)->create([
            'designer_id' => $room->id,
            'period_end' => $tomorrow
        ]);

        $filters = [
            'location' => [
                [10, 50],
                [10, 50]
            ]
        ];

        $result = $this->repository->getPinnedProperty($filters);
        $this->assertContains($room->id, $result);
    }

    public function testGetPinnedPropertyWithPinnedType()
    {
        $tomorrow = Carbon::tomorrow();

        $room = factory(Room::class)->create([
            'longitude' => 10.00,
            'latitude' => 50.00,
        ]);
        factory(PromotedList::class)->create([
            'designer_id' => $room->id,
            'period_end' => $tomorrow,
            'type' => 'type',
        ]);

        $filters = [
            'location' => [
                [10, 50],
                [10, 50]
            ],
            'pinned_type' => 'type'
        ];

        $result = $this->repository->getPinnedProperty($filters);
        $this->assertContains($room->id, $result);
    }

    public function testAddViewWithInvalidRoom()
    {
        $result = $this->repository->addView(rand(1, 100), null, null, null, null);
        $this->assertFalse($result);
    }

    public function testAddViewWithViewForIsClick()
    {
        $room = factory(Room::class)->create([
            'chat_count' => 0,
            'view_count' => 0,
        ]);

        $viewCount = $room->view_count;
        $chatCount = $room->chat_count;
        $user = factory(User::class)->create();
        $device = factory(UserDevice::class)->create([
            'user_id' => $user->id
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ]);
        factory(ViewPromote::class)->create([
            'designer_id' => $room->id
        ]);

        $result = $this->repository->addView($room->song_id, $user, $device, false, ViewPromote::CLICK);
        $expectedView = [
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'device_id' => $device->id,
            'type' => View::SOURCE_DEFAULT,
        ];
        $expectedRoom = [
            'id' => $room->id,
            'view_count' => $viewCount + 1,
            'chat_count' => $chatCount,
        ];
        $this->assertTrue($result);
        $this->assertDatabaseHas('designer', $expectedRoom);
        $this->assertDatabaseHas('read_temp_3', $expectedView);
    }

    // skipped
    // public function testAddViewWithViewForIsNotClick()
    // {
    //     $room = factory(Room::class)->create([
    //         'chat_count' => 0,
    //         'view_count' => 0,
    //     ]);

    //     $viewCount = $room->view_count;
    //     $chatCount = $room->chat_count;
    //     $user = factory(User::class)->create();
    //     $device = factory(UserDevice::class)->create([
    //         'user_id' => $user->id
    //     ]);
    //     factory(RoomOwner::class)->create([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
    //     ]);
    //     factory(ViewPromote::class)->create([
    //         'designer_id' => $room->id
    //     ]);

    //     $result = $this->repository->addView($room->song_id, $user, $device, false, '');
    //     $expectedView = [
    //         'designer_id' => $room->id,
    //         'user_id' => $user->id,
    //         'device_id' => $device->id,
    //         'type' => View::SOURCE_DEFAULT,
    //     ];
    //     $expectedRoom = [
    //         'id' => $room->id,
    //         'view_count' => $viewCount,
    //         'chat_count' => $chatCount + 1,
    //     ];
    //     $this->assertTrue($result);
    //     $this->assertDatabaseHas('designer', $expectedRoom);
    //     $this->assertDatabaseHas('chat_count', $expectedView);
    // }

    public function testCheckViewPromoteClickingWithValidViewPromote()
    {
        $room = factory(Room::class)->create();

        factory(ViewPromote::class)->create([
            'designer_id' => $room->id,
            'for' => 'clicking'
        ]);
        $result = $this->repository->checkViewPromoteClicking($room, 'clicking');
        $this->assertTrue($result);
    }

    public function testCheckViewPromoteClickingWithViewoForNotEqualsToViewPromoteFor()
    {
        $room = factory(Room::class)->create();

        factory(ViewPromote::class)->create([
            'designer_id' => $room->id,
            'for' => 'clicking'
        ]);
        $result = $this->repository->checkViewPromoteClicking($room, '');
        $this->assertFalse($result);
    }

    public function testCheckViewPromoteClickingWithInvalidViewPromote()
    {
        $room = factory(Room::class)->create();

        $result = $this->repository->checkViewPromoteClicking($room, 'clicking');
        $this->assertFalse($result);
    }

    public function testPostTelp()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();

        $data = [
            'song_id' => $this->room->song_id,
            'user_id' => $user->id,
        ];
        $result = $this->repository->postTelp($data);

        $expectedContactUser = [
            'user_id' => $user->id,
            'designer_id' => $this->room->id
        ];
        $this->assertTrue($result['contact']);
        $this->assertDatabaseHas('designer_contact', $expectedContactUser);
    }

    public function testFinishTelpWithInvalidContactUser()
    {
        $this->setupRoomData();

        $data = [
            'song_id' => $this->room->song_id,
            'status_call' => true,
            'user_id' => rand(1, 100),
        ];

        $result = $this->repository->finishTelp($data);
        $this->assertFalse($result);
    }

    public function testFinishTelpWithStatusCallIsFalse()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();

        factory(ContactUser::class)->create([
            'user_id' => $user->id,
            'designer_id' => $this->room->id,
            'status_call' => null,
        ]);

        $this->mockAlternatively(ContactUser::class)->shouldReceive('notifToOwner');

        $data = [
            'song_id' => $this->room->song_id,
            'status_call' => false,
            'user_id' => $user->id,
        ];

        $result = $this->repository->finishTelp($data);
        $this->assertTrue($result);
    }

    public function testFinishTelpWithStatusCallIsTrue()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();

        factory(ContactUser::class)->create([
            'user_id' => $user->id,
            'designer_id' => $this->room->id,
            'status_call' => null,
        ]);

        $this->mockAlternatively(ContactUser::class)->shouldReceive('notifOwnerFirstTime');

        $data = [
            'song_id' => $this->room->song_id,
            'status_call' => true,
            'user_id' => $user->id,
        ];

        $result = $this->repository->finishTelp($data);
        $this->assertTrue($result);
    }

    public function testGetRelated()
    {
        $this->setupRoomData();

        $this->getMockBuilder(RoomPresenter::class)
            ->setMockClassName('RoomPresenter')
            ->setConstructorArgs(
                [
                    'list'
                ]
            )
            ->setMethods(
                [
                    'present'
                ]
            )
            ->getMock();

        $response = $this->repository->getRelated($this->room);
        $data = $response['data'];

        $this->assertIsArray($data);
    }

    public function testOwnerRegister()
    {
        $data = [
            'email' => 'owner@mail.com',
            'phone_number' => '0812345678',
        ];

        $result = $this->repository->ownerRegister($data);
        $this->assertTrue($result);
        $this->assertDatabaseHas('owner', $data);
    }

    public function testSaveFilterToAlarmWithUserAndUserDeviceIsNotNull()
    {
        $user = factory(User::class)->create();
        $device = factory(UserDevice::class)->create([
            'user_id' => $user->id
        ]);
        $settingotification = factory(SettingNotif::class)->create([
            'for' => 'user',
            'identifier' => $user->id,
            'key' => 'alarm_email',
            'value' => 'false',
        ]);
        $alarmDetail = [
            'area' => 'area',
            'job' => 'job',
            'location' => [0.0, 1.0],
            'phone' => '0812345678910',
            'email' => 'user@mail.com',
        ];

        $alarm = $this->repository->saveFilterToAlarm($user, $device, $alarmDetail);

        $alarmDetail['filters']['job'] = $alarmDetail['job'];
        $alarmDetail['filters']['area'] = $alarmDetail['area'];

        $expectedAlarm = [
            'user_id' => $user->id,
            'device_id' => $device->id,
            'phone' => $alarmDetail['phone'],
            'email' => $alarmDetail['email'],
            'filters' => json_encode($alarmDetail['filters']),
            'location' => json_encode($alarmDetail['location']),
            'type' => 'subscribe'
        ];

        $expectedNotificaiton = [
            'id' => $settingotification->id,
            'identifier' => $user->id,
            'value' => 'true'
        ];

        $this->assertEquals($alarm->user_id, $expectedAlarm['user_id']);
        $this->assertEquals($alarm->device_id, $expectedAlarm['device_id']);
        $this->assertEquals($alarm->phone, $expectedAlarm['phone']);
        $this->assertEquals($alarm->email, $expectedAlarm['email']);
        $this->assertEquals($alarm->location, $expectedAlarm['location']);
        $this->assertEquals($alarm->type, $expectedAlarm['type']);

        $this->assertDatabaseHas('setting_notification', $expectedNotificaiton);
    }

    public function testUnPinnedWithInalidRoomOwner()
    {
        $room = factory(Room::class)->create([
            'is_promoted' => 'true'
        ]);
        $owner = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $result = $this->repository->unPinned($user->id);

        $expectedRoom = [
            'id' => $room->id,
            'is_promoted' => 'false',
        ];
        $this->assertFalse($result);
        $this->assertDatabaseMissing('designer', $expectedRoom);
    }

    public function testUnPinnedWithValidRoomOwner()
    {
        $room = factory(Room::class)->create([
            'is_promoted' => 'true'
        ]);
        $owner = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);
        $result = $this->repository->unPinned($owner->id);

        $expectedRoom = [
            'id' => $room->id,
            'is_promoted' => 'false',
        ];
        $this->assertTrue($result);
        $this->assertDatabaseHas('designer', $expectedRoom);
    }

    public function testInsertViewPromoteWithoutExistingRecord()
    {
        $roomId = rand(1, 100);
        $PremiumRequestId = rand(1, 100);
        $views = rand(1, 100);

        $result = $this->repository->insertViewPromote($roomId, $PremiumRequestId, $views);

        $this->assertEquals($roomId, $result->designer_id);
        $this->assertEquals($PremiumRequestId, $result->premium_request_id);
        $this->assertEquals($views, $result->total);
    }

    public function testCanPinnedWithInvalidDesignerIdOrViewPromoteIsNull()
    {
        $designerId = rand(1, 100);

        $result = $this->repository->canPinned(null, null, $designerId);

        $this->assertTrue($result);
    }

    public function testCanPinnedWithStatusIs1AndViewPromoteIsActiveIs1AndPremiumRequestUserIdEqualsToValidUserId()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id
        ]);
        factory(ViewPromote::class, 10)->create([
            'premium_request_id' => $premiumRequest->id,
            'designer_id' => $this->room->id,
            'is_active' => 1,
        ]);
        $data = [
            'status' => 1
        ];

        $result = $this->repository->canPinned($user->id, $data, $this->room->id);

        $this->assertTrue($result);
    }

    public function testCanPinnedWithStatusIs1AndViewPromoteIsActiveIs1AndPremiumRequestUserIdNotEqualsToValidUserId()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => rand(1, 100)
        ]);
        factory(ViewPromote::class, 10)->create([
            'premium_request_id' => $premiumRequest->id,
            'designer_id' => $this->room->id,
            'is_active' => 1,
        ]);
        $data = [
            'status' => 1
        ];

        $result = $this->repository->canPinned($user->id, $data, $this->room->id);

        $this->assertFalse($result);
    }

    public function testCanPinnedWithStatusIs0AndPremiumRequestUserIdEqualsToValidUserId()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id
        ]);
        factory(ViewPromote::class, 10)->create([
            'premium_request_id' => $premiumRequest->id,
            'designer_id' => $this->room->id,
        ]);
        $data = [
            'status' => 0
        ];

        $result = $this->repository->canPinned($user->id, $data, $this->room->id);

        $this->assertTrue($result);
    }

    public function testCanPinnedWithStatusIs0AndPremiumRequestUserIdNotEqualsToValidUserId()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => rand(1, 100)
        ]);
        factory(ViewPromote::class, 10)->create([
            'premium_request_id' => $premiumRequest->id,
            'designer_id' => $this->room->id,
        ]);
        $data = [
            'status' => 0
        ];

        $result = $this->repository->canPinned($user->id, $data, $this->room->id);

        $this->assertFalse($result);
    }

    public function testGetHistoryPromoteWithStatusIs1()
    {
        $historyPromote = factory(HistoryPromote::class)->create();
        $result = $this->repository->getHistoryPromote($historyPromote, 1);
        $expectedResult = $historyPromote->history + $historyPromote->used;
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetHistoryPromoteWithStatusIs0()
    {
        $historyPromote = factory(HistoryPromote::class)->create();
        $result = $this->repository->getHistoryPromote($historyPromote, 0);
        $expectedResult = $historyPromote->history;
        $this->assertEquals($expectedResult, $result);
    }

    public function testLastHistory()
    {
        $this->setupRoomData();
        $factories = factory(ViewPromote::class)->create([
            'designer_id' => $this->room->id,
            'created_at' => Carbon::now()->addDay(10),
        ]);

        factory(ViewPromote::class, 10)->create([
            'designer_id' => $this->room->id,
            'created_at' => now(),
        ]);
        $result = $this->repository->lastHistory($this->room->id);

        $this->assertEquals($factories->id, $result->id);
    }

    public function testUpdateEndHistory()
    {
        $viewPromote = factory(ViewPromote::class)->create();
        $historyPromote = factory(HistoryPromote::class)->create([
            'view_promote_id' => $viewPromote->id,
            'session_id' => $viewPromote->session_id,
        ]);

        $result = $this->repository->updateEndHistory($viewPromote);
        $expectedResult = [
            'end_view' => $viewPromote->total - $viewPromote->used,
            'for' => $viewPromote->for,
            'used_view' => $viewPromote->used,
        ];

        $this->assertTrue($result);
        $this->assertDatabaseHas('premium_promote_history', $expectedResult);
    }

    public function testGetHistoryUsedView()
    {
        $viewPromote = factory(ViewPromote::class)->create();
        $historyPromote = factory(HistoryPromote::class)->create([
            'view_promote_id' => $viewPromote->id,
        ]);

        $result = $this->repository->getHistoryUsedView($viewPromote->id);
        $this->assertCount(1, $result);
        $this->assertEquals($historyPromote->id, $result[0]->id);
    }

    public function testEndViewPromoteWithInvalidRoomId()
    {
        $viewPromote = factory(ViewPromote::class)->create([
            'is_active' => 1
        ]);
        $room = factory(Room::class)->create();
        $result = $this->repository->endViewPromote($room->id);
        $this->assertNull($result);
    }

    public function testEndViewPromoteWithIsActive0()
    {
        $this->setupRoomData();
        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id' => $this->room->id,
            'is_active' => 0
        ]);
        $result = $this->repository->endViewPromote($this->room->id);
        $this->assertEquals(0, $result->is_active);
        $this->assertEquals($viewPromote->id, $result->id);
    }

    public function testEndViewPromote()
    {
        $this->setupRoomData();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'allocated' => 100
        ]);
        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id' => $this->room->id,
            'is_active' => 1,
            'premium_request_id' => $premiumRequest->id
        ]);
        $historyPromote = factory(HistoryPromote::class)->create([
            'view_promote_id' => $viewPromote->id,
            'session_id' => $viewPromote->session_id,
        ]);

        $history = $viewPromote->history + $viewPromote->used;
        $result = $this->repository->endViewPromote($this->room->id);

        $this->assertEquals($viewPromote->id, $result->id);
        $this->assertEquals(0, $result->is_active);
        $this->assertEquals($history, $result->history);
    }

    public function testEndAllocatedViews()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'allocated' => 100
        ]);
        $allocated = $premiumRequest->allocated;
        $total = 10;
        $result = $this->repository->endAllocatedViews($premiumRequest, $total);
        $expectedResult = [
            'allocated' => $allocated - $total
        ];
        $this->assertDatabaseHas('premium_request', $expectedResult);
        $this->assertTrue($result);
    }

    public function testendViewPromoteTwoWithInvalidRoomId()
    {
        $viewPromote = factory(ViewPromote::class)->create([
            'is_active' => 1
        ]);
        $roomID = rand(1, 100);
        $result = $this->repository->endViewPromoteTwo($roomID);
        $this->assertNull($result);
    }

    public function testEndViewPromoteTwo()
    {
        $this->setupRoomData();
        $viewPromote = factory(ViewPromote::class)->create([
            'designer_id' => $this->room->id,
            'is_active' => 1
        ]);
        $result = $this->repository->endViewPromoteTwo($this->room->id);
        $this->assertEquals(0, $result->is_active);
    }

    public function testPinnedByOwnerWithSongIdIs0()
    {
        $data = [
            'song_id' => 0
        ];
        $result = $this->repository->pinnedByOwner(null, $data);
        $expectedResult = [
            'data' => [], 'status' => false, 'message' => 'Data iklan belum siap di promosikan.'
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testPinnedByOwnerWithCanPinnedIsFalse()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => rand(1, 100)
        ]);
        factory(ViewPromote::class, 10)->create([
            'premium_request_id' => $premiumRequest->id,
            'designer_id' => $this->room->id,
            'is_active' => 1,
        ]);
        $data = [
            'song_id' => 0,
            'status' => 1
        ];

        $result = $this->repository->pinnedByOwner($user->id, $data);
        $expectedResult = [
            'data' => [], 'status' => false, 'message' => 'Data iklan belum siap di promosikan.'
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testPinnedByOwnerWithendViewPromoteIsNull()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();

        $data = [
            'song_id' => $this->room->song_id,
            'status' => 0
        ];

        $result = $this->repository->pinnedByOwner($user->id, $data);
        $expectedResult = [
            'data' => null, 'status' => false, 'message' => 'Gagal nonaktifkan promosi data iklan.'
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testToHistoryPromote()
    {
        $viewPromote = factory(ViewPromote::class)->create(['for' => null]);
        $result = $this->repository->toHistoryPromote($viewPromote);
        $this->assertEquals($viewPromote->total, $result->start_view);
        $this->assertEquals($viewPromote->id, $result->view_promote_id);
        $this->assertEquals($viewPromote->session_id, $result->session_id);
        $this->assertEquals($viewPromote->for, $result->for);
    }

    public function testSanitizeCityOrDistrictName()
    {
        $word = 'Kabupaten Kota Kecamatan';
        $data = [
            'Kabupaten' => 'Bantul',
            'Kota' => 'Bantul',
            'Kecamatan' => 'Jetis',
        ];
        $result = $this->repository->sanitizeCityOrDistrictName($word, $data);
        $expectedResult = [
            $data['Kabupaten'],
            $data['Kota'],
            $data['Kecamatan'],
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetListHomeWithUserIsNotNullAndIsOwnerIsTrueAndActionUrlIsNotNullAndForHomeAndIsAppIsTrue()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $photo = factory(Media::class)->create();
        $eventModel = factory(EventModel::class)->create([
            'is_published' => 1,
            'is_owner' => '1',
            'photo_id' => $photo->id,
            'scheme_url' => 'http://schemeurl.com'
        ]);

        $result = $this->repository->getListHome($user, true);
        $this->assertEquals($result['posters'][0]['poster_id'], $eventModel->id);
        $this->assertEquals($result['posters'][0]['title'], $eventModel->title);
        $this->assertEquals($result['posters'][0]['description'], $eventModel->description);
        $this->assertEquals($result['posters'][0]['redirect_browser'], $eventModel->redirect_browser);
        $this->assertEquals($result['posters'][0]['scheme'], $eventModel->scheme_url);
        $this->assertNotNull($result['posters'][0]['photo_url']['real']);
        $this->assertNotNull($result['posters'][0]['photo_url']['small']);
        $this->assertNotNull($result['posters'][0]['photo_url']['medium']);
        $this->assertNotNull($result['posters'][0]['photo_url']['large']);
        $this->assertEquals($result['poster_count'], 1);
        $this->assertEquals($result['top_count'], 0);
        $this->assertEquals($result['recommendation_kost'], []);
    }

    public function testGetNewsOwner()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $photo = factory(Media::class)->create();
        $eventModel = factory(EventModel::class)->create([
            'is_published' => 1,
            'is_owner' => '1',
            'photo_id' => $photo->id,
            'scheme_url' => 'http://schemeurl.com'
        ]);

        $result = $this->repository->getNewsOwner($user);
        $this->assertEquals($result['event'][0]['poster_id'], $eventModel->id);
        $this->assertEquals($result['event'][0]['title'], $eventModel->title);
        $this->assertEquals($result['event'][0]['description'], $eventModel->description);
        $this->assertEquals($result['event'][0]['redirect_browser'], $eventModel->redirect_browser);
        $this->assertEquals($result['event'][0]['scheme'], $eventModel->scheme_url);
        $this->assertNotNull($result['event'][0]['photo_url']['real']);
        $this->assertNotNull($result['event'][0]['photo_url']['small']);
        $this->assertNotNull($result['event'][0]['photo_url']['medium']);
        $this->assertNotNull($result['event'][0]['photo_url']['large']);
    }

    public function testGetListEventWithUserIsNotNullAndIsOwnerIsTrueAndActionUrlIsNotNullAndForHomeAndIsAppIsTrue()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'true'
        ]);

        $photo = factory(Media::class)->create();
        $eventModel = factory(EventModel::class)->create([
            'is_published' => 1,
            'is_owner' => '1',
            'photo_id' => $photo->id,
            'scheme_url' => 'http://schemeurl.com'
        ]);

        $result = $this->repository->getListEvent($user, 'home', true);
        $this->assertEquals($result[0]['poster_id'], $eventModel->id);
        $this->assertEquals($result[0]['title'], $eventModel->title);
        $this->assertEquals($result[0]['description'], $eventModel->description);
        $this->assertEquals($result[0]['redirect_browser'], $eventModel->redirect_browser);
        $this->assertEquals($result[0]['scheme'], $eventModel->scheme_url);
        $this->assertNotNull($result[0]['photo_url']['real']);
        $this->assertNotNull($result[0]['photo_url']['small']);
        $this->assertNotNull($result[0]['photo_url']['medium']);
        $this->assertNotNull($result[0]['photo_url']['large']);
    }

    public function testGetListEventWithUserIsNotNullAndIsOwnerIsFalseAndActionUrlIsNotNullAndIsAppIsFalseWithoutPhoto()
    {
        $user = factory(User::class)->create([
            'is_owner' => 'false'
        ]);

        $eventModel = factory(EventModel::class)->create([
            'is_published' => 1,
            'is_owner' => '0',
        ]);

        $result = $this->repository->getListEvent($user, '', false);
        $this->assertEquals($result[0]['poster_id'], $eventModel->id);
        $this->assertEquals($result[0]['title'], $eventModel->title);
        $this->assertEquals($result[0]['description'], $eventModel->description);
        $this->assertEquals($result[0]['redirect_browser'], $eventModel->redirect_browser);
        $this->assertEquals($result[0]['scheme'], $eventModel->action_url);
        $this->assertNull($result[0]['photo_url']['real']);
        $this->assertNull($result[0]['photo_url']['small']);
        $this->assertNull($result[0]['photo_url']['medium']);
        $this->assertNull($result[0]['photo_url']['large']);
    }

    public function testGetListEventWithUserNullAndActionUrlIsNotNullAndIsAppIsFalseWithoutPhoto()
    {
        $eventModel = factory(EventModel::class)->create([
            'is_published' => 1,
            'is_owner' => '0',
        ]);

        $result = $this->repository->getListEvent(null, '', false);
        $this->assertEquals($result[0]['poster_id'], $eventModel->id);
        $this->assertEquals($result[0]['title'], $eventModel->title);
        $this->assertEquals($result[0]['description'], $eventModel->description);
        $this->assertEquals($result[0]['redirect_browser'], $eventModel->redirect_browser);
        $this->assertEquals($result[0]['scheme'], $eventModel->action_url);
        $this->assertNull($result[0]['photo_url']['real']);
        $this->assertNull($result[0]['photo_url']['small']);
        $this->assertNull($result[0]['photo_url']['medium']);
        $this->assertNull($result[0]['photo_url']['large']);
    }

    public function testGetListPromotedWithPhotoRound()
    {
        $photo = factory(Media::class)->create([
        ]);
        $room = factory(Room::class)->state('active')->create([
            'photo_id' => $photo->id,
            'photo_round_id' => rand(1, 100),
        ]);
        $promoted = factory(Promoted::class)->create([
            'is_published' => 1
        ]);
        $promotedList = factory(PromotedList::class)->create([
            'designer_id' => $room->id,
            'list_id' => $promoted->id
        ]);

        $result = $this->repository->getListPromoted();
        $this->assertEquals($result[0]['rec_id'], $promoted->id);
        $this->assertEquals($result[0]['rec_name'], $promoted->name);

        $this->assertEquals($result[0]['kost'][0]['_id'], $room->song_id);
        $this->assertEquals($result[0]['kost'][0]['room-title'], $room->name);
        $this->assertEquals($result[0]['kost'][0]['has_round_photo'], true);
    }

    public function testGetListPromotedWithoutPhotoRound()
    {
        $photo = factory(Media::class)->create([
        ]);
        $room = factory(Room::class)->state('active')->create([
            'photo_id' => $photo->id,
            'photo_round_id' => null,
        ]);
        $promoted = factory(Promoted::class)->create([
            'is_published' => 1
        ]);
        $promotedList = factory(PromotedList::class)->create([
            'designer_id' => $room->id,
            'list_id' => $promoted->id
        ]);

        $result = $this->repository->getListPromoted();
        $this->assertEquals($result[0]['rec_id'], $promoted->id);
        $this->assertEquals($result[0]['rec_name'], $promoted->name);

        $this->assertEquals($result[0]['kost'][0]['_id'], $room->song_id);
        $this->assertEquals($result[0]['kost'][0]['room-title'], $room->name);
        $this->assertEquals($result[0]['kost'][0]['has_round_photo'], false);
    }

    public function testUpdateJson()
    {
        // TODO: This method needs refactoring before this test case
        self::assertTrue(true);
    }

    public function testEditJson()
    {
        // TODO: This method needs refactoring before this test case
        self::assertTrue(true);
    }

    public function testSaveJson()
    {
        // TODO: This method needs refactoring before this test case
        self::assertTrue(true);
    }

    public function testAddCardsWithNoPhotoKey()
    {
        $this->setupRoomData();

        $cards = [
            [
                'description' => 'Testing Photo 1 Non Key',
                'ordering' => 0
            ]
        ];

        $this->assertEquals(0, $this->repository->addCards($cards, $this->room->id));
    }

    public function testAddCardsWithPhotoKey()
    {
        $this->setupRoomData();

        $this->assertEquals(3, $this->repository->addCards($this->cardsArrayProvider(), $this->room->id));
    }

    public function testUpdateCardsWithExistingCardsDeletion()
    {
        $this->setupRoomData();

        factory(Card::class)->create(
            [
                'description' => 'This card will be missing',
                'designer_id' => $this->room->id
            ]
        );

        $this->repository->updateCards($this->cardsArrayProvider(), $this->room->id);

        $this->assertSoftDeleted(
            'designer_style',
            [
                'description' => 'This card will be missing',
            ]
        );
    }

    public function testUpdateCardsWithNewCardsInsertion()
    {
        $this->setupRoomData();

        $this->repository->updateCards($this->cardsArrayProvider(), $this->room->id);

        $this->assertDatabaseHas(
            'designer_style',
            [
                'description' => 'Testing Photo 1'
            ]
        );
    }

    public function testUpdateCards()
    {
        $this->setupRoomData();

        $card = factory(Card::class)->create();

        $cardAboutToBeUpdated = [
            [
                'photo' => $card->id,
                'description' => 'This is NOT a testing photo',
                'ordering' => 1
            ],
        ];

        $this->repository->updateCards($cardAboutToBeUpdated, $this->room->id);

        $this->assertDatabaseHas(
            'designer_style',
            [
                'description' => 'This is NOT a testing photo'
            ]
        );
    }

    public function testUpdateCardsWithUpdateSingleCard()
    {
        $this->deleteRoomData();
        $this->setupRoomData();

        $photo = factory(Media::class)->create();
        $card = factory(Card::class)->create([
            'designer_id' => $this->room->id,
            'photo_id' => $photo->id
        ]);

        $cards = [
            [
                'photo' => $photo->id,
                'description' => 'Testing Update Cards with single card',
                'ordering' => 0
            ]
        ];

        $this->repository->updateCards($cards, $this->room->id);

        $this->assertDatabaseHas(
            'designer_style',
            [
                'description' => $cards[0]['description']
            ]
        );
    }

    public function testUpdateSingleCard()
    {
        $this->setupRoomData();

        $media = factory(Media::class)->create();
        factory(Card::class)->create(
            [
                'photo_id' => $media->id,
                'designer_id' => $this->room->id,
                'description' => 'This is a description'
            ]
        );

        $cardAboutToBeUpdated = [
            'photo' => $media->id,
            'designer_id' => $this->room->id,
            'description' => 'This is another description',
            'ordering' => 0
        ];

        $this->repository->updateSingleCard($cardAboutToBeUpdated, $this->room->id);

        $this->assertDatabaseHas(
            'designer_style',
            [
                'description' => 'This is another description'
            ]
        );
    }

    public function testAutoReplyChatWithInvalidRoom()
    {
        $this->assertNull($this->repository->autoReplyChat(null, []));
    }

    public function testAutoReplyChatWithValidRoom()
    {
        // TODO: Class "Chat" constructor needs heavy refactoring before this class
        $this->assertTrue(true);
    }

    public function testGetItemListOwnerWithoutLimitAndOffsetShouldReturnValidStructure()
    {
        $this->setupRoomData();
        $user = factory(User::class)->state('owner')->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $this->room->id,
            'user_id' => $user->id,
        ]);
        factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'status' => '1',
        ]);

        $result = $this->repository->getItemListOwner();
        $keys = [
            'page',
            'next-page',
            'limit',
            'offset',
            'has-more',
            'total',
            'total-str',
            'toast',
            'rooms',
        ];
        $structures = array_keys($result);
        $this->assertEquals($structures, $keys);
    }

    public function testGetItemListOwnerWithLimitAndOffsetShouldReturnValidStructure()
    {
        $this->setupRoomData();
        $user = factory(User::class)->state('owner')->create();

        factory(RoomOwner::class)->state('owner')->create([
            'designer_id' => $this->room->id,
            'user_id' => $user->id,
        ]);
        factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'status' => '1',
        ]);

        request()->merge([
            'limit' => 10,
            'offset' => 1
        ]);

        $result = $this->repository->getItemListOwner();
        $keys = [
            'page',
            'next-page',
            'limit',
            'offset',
            'has-more',
            'total',
            'total-str',
            'toast',
            'rooms',
        ];
        $structures = array_keys($result);
        $this->assertEquals($structures, $keys);
    }

    public function testGetRoomReportWithValidStructure()
    {
        $room = factory(Room::class)->create();
        $result = $this->repository->getRoomReport($room, '');

        $keys = [
            'id', 'type', 'love_count', 'view_count', 'view_ads_count', 'survey_count', 'availability_count', 'telp_count',
            'message_count', 'review_count', 'used_balance'
        ];
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }

        $this->assertEquals($result['id'], $room->song_id);
    }

    public function testGetPhoneOwnerWithInvalidRoom()
    {
        $songId = rand(1, 99999);
        $result = $this->repository->getPhoneOwner($songId);
        $this->assertNull($result['phone']);
        $this->assertEquals('No telp tidak di temukan.', $result['message']);
    }

    public function testGetPhoneOwnerWithouthRoomPhoneNumberAndInvalidOwner()
    {
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'owner_phone' => '',
            'manager_phone' => '',
        ]);
        $result = $this->repository->getPhoneOwner($room->song_id);
        $this->assertNull($result['phone']);
        $this->assertEquals('No telp tidak di temukan.', $result['message']);
    }

    public function testGetPhoneOwnerWithOwnerPhoneNumber()
    {
        $owner = factory(User::class)->create([
            'phone_number' => '0812345678910'
        ]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'owner_phone' => '',
            'manager_phone' => '',
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER
        ]);

        $result = $this->repository->getPhoneOwner($room->song_id);
        $phone = '+62' . SMSLibrary::phoneNumberCleaning($owner->phone_number);
        $this->assertEquals($phone, $result['phone']);
        $this->assertEquals('No telp di temukan.', $result['message']);
    }

    public function testGetPhoneOwnerWithValidRoomOwnerPhone()
    {
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'owner_phone' => '0812345678910',
            'manager_phone' => '',
        ]);

        $result = $this->repository->getPhoneOwner($room->song_id);
        $phone = '+62' . SMSLibrary::phoneNumberCleaning($room->owner_phone);
        $this->assertEquals($phone, $result['phone']);
        $this->assertEquals('No telp di temukan.', $result['message']);
    }

    public function testGetPhoneOwnerWithValidRoomManagerPhone()
    {
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'owner_phone' => '',
            'manager_phone' => '081111111111',
        ]);

        $result = $this->repository->getPhoneOwner($room->song_id);
        $phone = '+62' . SMSLibrary::phoneNumberCleaning($room->manager_phone);
        $this->assertEquals($phone, $result['phone']);
        $this->assertEquals('No telp di temukan.', $result['message']);
    }

    public function testGetPhoneExistWithEmptyPhones()
    {
        $phones = [];
        $result = $this->repository->getPhoneExist($phones);
        $this->assertEquals('-', $result);
    }

    public function testGetPhoneExist()
    {
        $phones = ['08123456789'];
        $result = $this->repository->getPhoneExist($phones);
        $this->assertEquals($phones[0], $result);
    }

    public function testGetRoomTypeNumberWithInvalidRoomType()
    {
        $result = $this->repository->getRoomTypeNumber(rand(1, 99));
        $this->assertNull($result);
    }

    public function testGetRoomTypeNumberWithoutUnit()
    {
        $type = factory(Type::class)->create([
            'name' => 'Room Type'
        ]);
        $result = $this->repository->getRoomTypeNumber($type->id);
        $this->assertEmpty($result);
    }

    public function testGetRoomTypeNumberWithUnit()
    {
        $type = factory(Type::class)->create([
            'name' => 'Room Type'
        ]);

        $unit = factory(Unit::class)->create([
            'designer_type_id' => $type->id
        ]);
        $result = $this->repository->getRoomTypeNumber($type->id);
        $this->assertContains($unit->name, $result);
    }

    public function testGetCityReturnEmptyArray()
    {
        $this->assertEmpty($this->repository->getCity());
    }

    public function testGetCityReturnValidArray()
    {
        factory(HomeStaticLanding::class, 10)->create();

        $this->assertNotEmpty($this->repository->getCity());
    }

    public function testGetCityWithLandingKost()
    {
        $landing = factory(Landing::class)->create([]);
        $homeStatic = factory(HomeStaticLanding::class)->create([
            'parent_id' => null,
            'landing_id' => $landing->id
        ]);

        $result = $this->repository->getCity();
        $this->assertEquals($landing->getLocationAttribute(), $result[0]['location']);
        $this->assertEquals($landing->slug, $result[0]['landing_source']);
    }

    public function testGetRoomBySongIdWithValidSongId()
    {
        $this->setupRoomData();

        $result = $this->repository->getRoomBySongId(20);
        $this->assertInstanceOf(Room::class, $result);
    }

    public function testGetRoomBySongIdWithInvalidSongId()
    {
        $result = $this->repository->getRoomBySongId(999);
        $this->assertNull($result);
    }

    public function testGetRoomsByNameOrIdWithEmptyKeywordString()
    {
        $this->setupRoomData();

        // Commit the DB transaction so function MATCH..AGAINST could work
        DB::commit();

        $result = $this->repository->getRoomsByNameOrId('');

        $this->assertEquals(1, $result->count());
    }

    public function testGetRoomsByNameOrIdWithInvalidKeywordString()
    {
        $this->setupRoomData();

        // Commit the DB transaction so function MATCH..AGAINST could work
        DB::commit();

        $result = $this->repository->getRoomsByNameOrId('Kos Non Existence');

        $this->assertTrue($result->isEmpty());
    }

    public function testGetRoomsByNameOrIdWithValidKeywordString()
    {
        $this->setupRoomData();

        // Commit the DB transaction so function MATCH..AGAINST could work
        DB::commit();

        $result = $this->repository->getRoomsByNameOrId(self::TESTING_KOS_NAME);

        $this->assertTrue($result->isNotEmpty());
        $this->assertEquals(self::TESTING_KOS_NAME, $result->items()[0]['name']);
    }

    public function testGetRoomsByNameOrIdWithValidKeywordId()
    {
        $this->setupRoomData();

        // Commit the DB transaction so function MATCH..AGAINST could work
        DB::commit();

        $result = $this->repository->getRoomsByNameOrId($this->room->id);

        $this->assertTrue($result->isNotEmpty());
        $this->assertEquals($this->room->id, $result->items()[0]['id']);
    }

    public function testGetRoomForChatMetaWithInvalidSongId()
    {
        $result = $this->repository->getRoomBySongId(999);
        $this->assertNull($result);
    }

    public function testGetRoomForChatMetaWithValidSongId()
    {
        $this->setupRoomData();

        $result = $this->repository->getRoomForChatMeta($this->room->song_id);
        $this->assertInstanceOf(Room::class, $result);
        $this->assertEquals($result->id, $this->room->id);
    }

    public function testModel()
    {
        $this->assertEquals('App\Entities\Room\Room', $this->repository->model());
    }

    public function testGetModel()
    {
        $result = $this->repository->getModel();
        $this->assertInstanceOf(Room::class, $result);
    }

    public function testValidator()
    {
        $this->assertEquals('App\Validators\RoomValidator', $this->repository->validator());
    }

    public function testGetItemListWithoutPagination()
    {
        $this->setupRoomData();

        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;

        $user = factory(User::class)->make();
        $this->app->user = $user;

        $this->repository->setPresenter(new RoomPresenter('list'));
        $result = $this->repository->getItemListWithoutPagination();

        $this->assertEquals(1, count($result));
        $this->assertEquals($this->room->name, $result[0]['room-title']);
    }

    public function testGetItemListWithoutPaginationWithRoomsIsNull()
    {
        $this->deleteRoomData();
        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;

        $user = factory(User::class)->make();
        $this->app->user = $user;

        $this->repository->setPresenter(new RoomPresenter('list'));
        $result = $this->repository->getItemListWithoutPagination();

        $this->assertNull($result);
    }

    public function testGetItemListSimple()
    {
        $this->setupRoomData();

        $this->repository->setPresenter(new RoomPresenter('simple-list'));
        $result = $this->repository->getItemListSimple(null);

        $this->assertEquals(1, count($result['rooms']));
        $this->assertEquals(
            $this->room->name,
            $result['rooms'][0]['room_title']
        );
        $this->assertEquals(
            'https://mamikos.com/room/this-is-the-slug-please-test-me',
            $result['rooms'][0]['share_url']
        );
    }

    public function testSecurePaginateWithOffsetIs0()
    {
        $reflection = new \ReflectionClass(get_class($this->repository));
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);

        request()->merge(['offset' => 0]);
        request()->merge(['limit' => 1]);

        $page = $method->invokeArgs($this->repository, []);

        $this->assertEquals($page, 1);
    }

    public function testSecurePaginateWithOffsetIsNot0()
    {
        $reflection = new \ReflectionClass(get_class($this->repository));
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);

        request()->merge(['offset' => 10]);
        request()->merge(['limit' => 5]);

        $page = $method->invokeArgs($this->repository, []);

        $this->assertEquals($page, 3);
    }

    public function testSecurePaginateWithPageIsGreaterThan10()
    {
        $reflection = new \ReflectionClass(get_class($this->repository));
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);

        request()->merge(['page' => rand(11, 99)]);

        $method->invokeArgs($this->repository, []);

        $page = request()->input('page', 1);
        $this->assertEquals($page, 10);
    }

    public function testGetClusterListResponseStructure()
    {
        $this->setupRoomData();

        $filter = [
            'location' => [
                [0, 0], [0, 0]
            ],
        ];

        $result = $this->repository->getClusterList($filter);

        $keys = [
            'page', 'next-page', 'limit', 'offset', 'has-more', 'total', 'total-str',
            'grid_length', 'toast', 'filter-str', 'location', 'discounted_total', 'flash_sale_id', 'rooms'
        ];

        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetRoomWithOwnerBySongIdWithUnregisteredSongId()
    {
        $result = $this->repository->getRoomWithOwnerBySongId(123);

        $this->assertEquals(null, $result);
    }

    public function testGetRoomWithOwnerBySongIdSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $result = $this->repository->getRoomWithOwnerBySongId($room->song_id);

        $this->assertEquals($user->id, $result->owners->first()->user_id);
    }

    public function testGetAdHistoryStatsShouldSuccess()
    {
        $now = Carbon::now();

        $room = factory(Room::class)->create([
            'longitude' => $this->faker()->longitude,
            'latitude' => $this->faker()->latitude,
            'is_testing' => 0,
            'is_active' => 'true'
        ]);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 2,
            'date' => $now->startOfMonth()->addDays(4)
        ]);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 1,
            'date' => $now->startOfMonth()->addDays(20)
        ]);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 1,
            'date' => $now->subMonth()
        ]);

        $historyStats = $this->repository->getAdHistoryStats($room, 3000);

        $this->assertCount(12, $historyStats);
        $this->assertEquals(0, $historyStats[6]['value']);
        $this->assertEquals(3, $historyStats[11]['value']);
    }

    public function testGetAdHistoryStatsRoomWithNoHistory()
    {
        $room = factory(Room::class)->create();

        $historyStats = $this->repository->getAdHistoryStats($room, 3000);

        $this->assertCount(12, $historyStats);
        $this->assertEquals(0, $historyStats[6]['value']);
    }

    public function testGetActiveRoomByOwnerUserIdAndSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => 'true']);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $result = $this->repository->getActiveRoomByOwnerUserId($user->id);
        $this->assertCount(1, $result);
    }

    public function testGetActiveRoomByOwnerUserIdAndReturnNull()
    {
        $room = factory(Room::class)->create(['is_active' => 'true']);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $result = $this->repository->getActiveRoomByOwnerUserId($user->id + 5);
        $this->assertCount(0, $result);
    }

    public function testAssignRoomPropertyAndSucccess()
    {
        $room = factory(Room::class)->create(['is_active' => 'true']);
        $property = factory(Property::class)->create();
        $result = $this->repository->assignRoomProperty($room, $property);
        $this->assertTrue($result);
        $this->assertNotNull($room->property_id);
    }

    public function testUnassignRoomPropertyAndSucccess()
    {
        $property = factory(Property::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'property_id' => $property->id
        ]);
        $result = $this->repository->unassignRoomProperty($room);
        $this->assertTrue($result);
        $this->assertNull($room->property_id);
    }

    public function testResetRoomWithPropertyIdAndSuccess()
    {
        $propertyId = 1;
        factory(Room::class)->create(['property_id' => $propertyId]);
        $result = $this->repository->resetRoomWithPropertyId($propertyId);
        $this->assertTrue($result);
    }

    public function testGetActiveRoomWithPropertyWithNoRoomPropertyIdAndSuccess()
    {
        $room = factory(Room::class)->create(['is_active' => 'true']);
        $user = factory(User::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $property = factory(Property::class)->create(['owner_user_id' => $user->id]);
        $result = $this->repository->getActiveRoomWithProperty($property);
        $this->assertCount(1, $result);
    }

    public function testGetActiveRoomWithPropertyWithRoomPropertyIdAndSuccess()
    {
        $user = factory(User::class)->create();
        $property = factory(Property::class)->create(['owner_user_id' => $user->id]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'property_id' => $property->id
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $result = $this->repository->getActiveRoomWithProperty($property);
        $this->assertCount(1, $result);
    }

    public function testGetActiveRoomWithPropertyWithDifferentPropertyIdAndReturnEmpty()
    {
        $user = factory(User::class)->create();
        $property = factory(Property::class)->create(['owner_user_id' => $user->id]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'property_id' => $property->id + 999
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $result = $this->repository->getActiveRoomWithProperty($property);
        $this->assertEmpty($result);
    }

    public function testGetGoldplusKostsWithGoldplusLevelIdIsEmpty()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);
        $gp1 = factory(KostLevel::class)->create([
            'name' => 'goldplus 1'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $gp1,
            'kost_id' => $room->song_id,
        ]);
        Config::set('kostlevel.ids.goldplus1', $gp1->id);

        $result = $this->repository->getGoldplusKosts($owner->id, []);
        $result = $result->toArray();
        $keys = $this->getRoomPaginateStructure();

        $this->assertEquals($result['data'][0]['id'], $room->id);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetGoldplusKostsWithGoldplusLevelIdIsNotEmpty()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);
        $gp1 = factory(KostLevel::class)->create([
            'name' => 'goldplus 1'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $gp1,
            'kost_id' => $room->song_id,
        ]);

        $result = $this->repository->getGoldplusKosts($owner->id, [$gp1->id]);
        $result = $result->toArray();
        $keys = $this->getRoomPaginateStructure();

        $this->assertEquals($result['data'][0]['id'], $room->id);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetGoldplusAcquisitionKostsValidStructure()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 0);
        $result = $result->toArray();

        $keys = $this->getRoomPaginateStructure();

        $this->assertEmpty($result['data']);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetGoldplusAcquisitionKostsWithFilterOptionIs0()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 0);
        $result = $result->toArray();
        $keys = $this->getRoomPaginateStructure();

        $this->assertEquals($result['data'][0]['id'], $room->id);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetGoldplusAcquisitionKostsWithFilterOptionIs1()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);
        $gp1 = factory(KostLevel::class)->create([
            'name' => 'goldplus 1'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $gp1,
            'kost_id' => $room->song_id,
        ]);
        Config::set('kostlevel.id.new_goldplus1', $gp1->id);

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $result = $result->toArray();
        $keys = $this->getRoomPaginateStructure();

        $this->assertEquals($result['data'][0]['id'], $room->id);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetGoldplusAcquisitionKostsWithFilterOptionIs2()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);
        $gp1 = factory(KostLevel::class)->create([
            'name' => 'goldplus 1'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $gp1,
            'kost_id' => $room->song_id,
        ]);

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $result = $result->toArray();
        $keys = $this->getRoomPaginateStructure();

        $this->assertEquals($result['data'][0]['id'], $room->id);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }

    public function testGetButtonSubmitGPWithPotentialPropertyOnReviewShouldReturnFalse()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);
        factory(PotentialProperty::class)->create([
            'designer_id' => $room->id,
            'followup_status' => PotentialProperty::FOLLOWUP_STATUS_NEW
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);

        $result = $this->repository->getButtonSubmitGP($owner->id);
        $this->assertFalse($result);
    }

    public function testGetButtonSubmitGPWithGoldplusShouldReturnFalse()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        $gp1 = factory(KostLevel::class)->create([
            'name' => 'goldplus 1'
        ]);
        factory(KostLevelMap::class)->create([
            'level_id' => $gp1,
            'kost_id' => $room->song_id,
        ]);
        Config::set('kostlevel.id.new_goldplus1', $gp1->id);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);

        $result = $this->repository->getButtonSubmitGP($owner->id);
        $this->assertFalse($result);
    }

    public function testGetButtonSubmitGPWithoutGPorPotentialProvertyOnReviewShouldReturnTrue()
    {
        $owner = factory(User::class)->create([]);
        $room = factory(Room::class)->create([
            'is_active' => 'true',
            'apartment_project_id' => null
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => 'verified'
        ]);

        $result = $this->repository->getButtonSubmitGP($owner->id);
        $this->assertTrue($result);
    }

    public function testGetRoomByUserId()
    {
        $owner = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => 'true',
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id
        ]);

        $result = $this->repository->getRoomsByUserId($owner->id);
        $this->assertEquals($room->id, $result[0]->id);
    }

    public function testGetRoomsByUserIdAndIsBooking()
    {
        $owner = factory(User::class)->create([]);

        $room = factory(Room::class)->create([
            'is_booking' => true,
            'is_active' => true
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_ADD_STATUS
        ]);

        $result = $this->repository->getRoomsByUserIdAndIsBooking($owner->id, $room->is_booking);
        $this->assertInstanceOf(Collection::class, $result);
        $this->assertEquals($room->id, $result[0]->id);
    }

    public function testGetCollectionOfActiveRoomByUserId()
    {
        $this->setupRoomData();
        $user = factory(User::class)->create();

        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $this->room->id,
        ]);
        $result = $this->repository->getCollectionOfActiveRoomsByUserId($user->id);
        $this->assertEquals($result[0]->id, $this->room->id);
    }

    public function testFindByUserIdAndIsBooking()
    {
        $this->setupRoomData();
        $this->room->is_booking = true;
        $this->room->save();

        $user = factory(User::class)->create();

        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $this->room->id,
        ]);

        $isBooking = $this->room->is_booking ;
        $result = $this->repository->findByUserIdAndIsBooking($user->id, $isBooking);
        $this->assertEquals($result->id, $this->room->id);
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsOldGP(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        
        $room = $this->createRoomAndRoomOwner($owner->id);
        $oldGP1Level = $this->setupGpLevel();
        $room->changeLevel($oldGP1Level->id);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(1, $result->total());
        $this->assertEquals($room->id, $result->items()[0]->id);

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals(0, $result->total());
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsOldGPWithPotentialPropertyNew(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);

        $room = $this->createRoomAndRoomOwner($owner->id);
        $oldGP1Level = $this->setupGpLevel();
        $room->changeLevel($oldGP1Level->id);

        $this->createPotentialProperty($room->id, PotentialProperty::FOLLOWUP_STATUS_NEW);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals($room->id, $result->items()[0]->id);
        $this->assertEquals(1, $result->total());

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 3);
        $this->assertEquals(0, $result->total());
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsOldGPWithPotentialPropertyInProgress(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);

        $room = $this->createRoomAndRoomOwner($owner->id);
        $oldGP1Level = $this->setupGpLevel();
        $room->changeLevel($oldGP1Level->id);

        $this->createPotentialProperty($room->id, PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 3);
        $this->assertEquals(1, $result->total());
        $this->assertEquals($room->id, $result->items()[0]->id);
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsWithPotentialPropertyNewWithoutLevel(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $room = $this->createRoomAndRoomOwner($owner->id);
        $this->createPotentialProperty($room->id, PotentialProperty::FOLLOWUP_STATUS_NEW);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals(1, $result->total());
        $this->assertEquals($room->id, $result->items()[0]->id);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 3);
        $this->assertEquals(0, $result->total());
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsWithPotentialPropertyInProgressWithoutLevel(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);
        $room = $this->createRoomAndRoomOwner($owner->id);
        $this->createPotentialProperty($room->id, PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals(0, $result->total());
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 3);
        $this->assertEquals(1, $result->total());
        $this->assertEquals($room->id, $result->items()[0]->id);
    }

    /**
     * @group UG
     * @group UG-4437
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetGoldplusAcquisitionKostsWithNewGPShouldActive(): void
    {
        $owner = factory(User::class)->create(['is_owner' => 'true']);

        $room = $this->createRoomAndRoomOwner($owner->id);
        $newGP1Level = $this->setupNewGpLevel();
        $room->changeLevel($newGP1Level->id);

        $this->createPotentialProperty($room->id, PotentialProperty::FOLLOWUP_STATUS_NEW);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 1);
        $this->assertEquals(1, $result->total());
        $this->assertEquals($room->id, $result->items()[0]->id);
        
        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 2);
        $this->assertEquals(0, $result->total());

        $result = $this->repository->getGoldplusAcquisitionKosts($owner->id, 3);
        $this->assertEquals(0, $result->total());
    }

    private function setupGpLevel(): KostLevel
    {
        $oldGP1Level = factory(KostLevel::class)->create();
        Config::set('kostlevel.ids.goldplus1', "$oldGP1Level->id");
        return $oldGP1Level;
    }

    private function setupNewGpLevel(): KostLevel
    {
        $newGP1Level = factory(KostLevel::class)->create();
        Config::set('kostlevel.id.new_goldplus1', $newGP1Level->id);
        return $newGP1Level;
    }

    private function createRoomAndRoomOwner(int $userId): Room
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $userId,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);
        return $room;
    }

    private function createPotentialProperty(int $roomId, string $followupStatus): PotentialProperty
    {
        return factory(PotentialProperty::class)->create([
            'designer_id' => $roomId,
            'offered_product' => 'gp1',
            'followup_status' => $followupStatus,
        ]);
    }

    /**
     * Set Room data for testing
     */
    private function setupRoomData(): void
    {
        if (Room::query()->count()) {
            DB::table('designer')->delete();
        }

        $this->room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'premium',
                    'with-slug',
                    'with-latlong',
                    'notTesting',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'name' => self::TESTING_KOS_NAME,
                    'song_id' => 20,
                    'location_id' => 52595569
                ]
            );
    }

    private function deleteRoomData(): void
    {
        if (Room::query()->count()) {
            DB::table('designer')->delete();
        }
    }

    private function getRoomPaginateStructure()
    {
        return  [
            'current_page',
            'data',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total'
        ];
    }

    /**
     * @group UG
     * @group UG-4089
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testaddCall(): void
    {
        $userId = factory(User::class)->create();
        $adminId = factory(User::class)->create();

        $owner = factory(User::class)->states('owner')->create();
        $this->actingAs($owner);

        $userIds = [$userId, $adminId, $owner->id];

        $songId = mt_rand(111111, 999999);
        $room = factory(Room::class)->create([
            'user_id' => $userId,
            'song_id' => $songId
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $userId
        ]);
        factory(Call::class)->create(
            [
                'designer_id' => $room->id,
                'user_id' => $owner->id,
            ]
        );

        $levelId = config('kostlevel.id.goldplus4');
        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $levelId,
        ]);

        $callData = [
            [
                'phone' => '0856' . mt_rand(111111, 999999),
                'email' => 'test@email.com',
                'note' => str_random(),
                'schedule' => str_random(0)
            ],
            'add_from' => str_random(),
            'chat_admin_id' => mt_rand(111, 999999),
            'group_channel_url' => 'sendbird_' . str_random()
        ];

        $result = $this->repository->addCall($songId, $callData, $owner, null);

        $this->assertFalse($result);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetRoomsWithReview()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create(['photo_id' => factory(Media::class)]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room,
            'user_id' => $user,
            'status' => RoomOwner::ROOM_VERIFY_STATUS
        ]);

        factory(Review::class)->create([
            'designer_id' => $room->id,
            'status' => 'live'
        ]);

        $result = $this->repository->GetRoomsWithReview($user->id);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(1, $result->items()[0]->avg_review_count);
        $this->assertEquals($room->id, $result->items()[0]->id);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetRoomsWithReviewOrderByReviewCount()
    {
        $user = factory(User::class)->create();
        
        $room1 = $this->createRoomAndRoomOwner($user->id);
        factory(Review::class)->create(['designer_id' => $room1->id]);

        $room2 = $this->createRoomAndRoomOwner($user->id);
        factory(Review::class)->create(['designer_id' => $room2->id]);
        factory(Review::class)->create(['designer_id' => $room2->id]);

        $room3 = $this->createRoomAndRoomOwner($user->id);
        factory(Review::class)->create(['designer_id' => $room3->id]);

        $result = $this->repository->GetRoomsWithReview($user->id);
        $this->assertEquals(3, $result->total());
        $this->assertEquals($room2->id, $result->items()[0]->id);
        $this->assertEquals(2, $result->items()[0]->avg_review_count);
    }

    /**
     * @group UG
     * @group UG-5047
     * @group App/Repositories/RoomRepositoryEloquent
     */
    public function testGetRoomsWithReviewWithInvalidIdShouldThrowException()
    {
        $this->expectException(RuntimeException::class);

        $this->repository->GetRoomsWithReview(0);
    }

    public function testGetKosRulesData()
    {
        $room = factory(Room::class)->create();

        $response = $this->repository->getKosRulesData($room->song_id);
        $this->assertArrayHasKey('rules', $response);
        $this->assertArrayHasKey('photos', $response);
    }

    public function testGetKosRulesDataReturnEmpty()
    {
        // random song id 144
        $response = $this->repository->getKosRulesData(144);
        $this->assertEmpty($response);
    }

    public function testGetKosRuleFacility()
    {
        $room = factory(Room::class)->create();
        $tagIcon = factory(Media::class)->create();
        $smallTagIcon = factory(Media::class)->create();

        $tag  = factory(Tag::class)->create(
            [
                'name' => 'Akses Kunci 24 jam',
                'photo_id' => $tagIcon->id,
                'small_photo_id' => $smallTagIcon->id
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'kos_rule'
            ]
        );

        factory(RoomTag::class)->create(
            [
                'designer_id' => $room->id,
                'tag_id' => $tag->id
            ]
        );

        $response = $this->repository->getKosRuleFacility($room);
        $this->assertNotEmpty($response);
        $this->assertEquals($tag->id, $response[0]['id']);
    }

    public function testGetKosRuleFacilityReturnEmpty()
    {
        $room = factory(Room::class)->create();
        $response = $this->repository->getKosRuleFacility($room);
        $this->assertEmpty($response);
    }

    public function testGetKosRulePhotosByOwnerAlwaysReturnEmpty()
    {
        $room      = factory(Room::class)->create();
        $response  = $this->repository->getKosRulePhotosByOwner($room);
        $this->assertEmpty($response);
    }

    public function testGetKosRulePhotosByOwner()
    {
        $room      = factory(Room::class)->create();
        $termPhoto = factory(Media::class)->create();
        $roomTerm  = factory(RoomTerm::class)->create(['designer_id' => $room->id]);

        factory(RoomTermDocument::class)->create(
            [
                'designer_term_id' => $roomTerm->id,
                'media_id' => $termPhoto->id
            ]
        );

        $response = $this->repository->getKosRulePhotosByOwner($room);
        $this->assertNotEmpty($response);
    }

    public function testGetKosRulePhotosByOwnerWithoutTermReturnEmpty()
    {
        $room     = factory(Room::class)->create();
        $response = $this->repository->getKosRulePhotosByOwner($room);
        $this->assertEmpty($response);
    }

    public function testGetKosRulePhotosByOwnerWithoutDocumentReturnEmpty()
    {
        $room = factory(Room::class)->create();
        factory(RoomTerm::class)->create(['designer_id' => $room->id]);
        $response = $this->repository->getKosRulePhotosByOwner($room);
        $this->assertEmpty($response);
    }

    /**
    * @group PMS-495
    */    
    public function testCreateOwnerAndRoomOwnerWithValidRoom()
    {
        $room = factory(Room::class)->create();
        $result = $this->repository->createOwnerAndRoomOwner($room);
        $expectedResultOwner = [
            'is_owner' => 'true',
            'role' => 'user',
            'name' => $room->owner_name,
            'phone_number' => $room->owner_phone,
            'is_verify' => '1',
        ];

        $this->assertInstanceOf(RoomOwner::class, $result);
        $this->assertInstanceOf(User::class, $result->user);
        $this->assertDatabaseHas('user', $expectedResultOwner);
    }

    /**
    * @group PMS-495
    */    
    public function testCreateOwnerAndRoomOwnerWithSameOwnerPhoneNumber()
    {
        $room1 = factory(Room::class)->create();
        $poneNumber = '08123456789';
        $user = factory(User::class)->state('owner')->create([
            'phone_number' => $poneNumber
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room1->id,
            'user_id' => $user->id,
        ]);
        $room2 = factory(Room::class)->create([
            'owner_phone' => $poneNumber
        ]);
        $result = $this->repository->createOwnerAndRoomOwner($room2);
        $expectedRoomOwner = [
            'user_id' => $user->id,
            'designer_id' => $room2->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
        ];

        $this->assertInstanceOf(RoomOwner::class, $result);
        $this->assertInstanceOf(User::class, $result->user);
        $this->assertDatabaseHas('designer_owner', $expectedRoomOwner);
    }

    /**
    * @group PMS-495
    */    
    public function testCreateOwnerAndRoomOwnerWithInvalidRoom()
    {
        $room = new Room;
        $result = $this->repository->createOwnerAndRoomOwner($room);

        $this->assertNull($result);
    }

    /**
    * @group PMS-495
    */    
    public function testCreateRoomOwnerWithValidRoomAndOwner()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create([
            'is_owner' => 'true',
        ]);

        $roomOwner = $this->repository->createRoomOwner($room, $owner);
        $expecredResult = [
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER
        ];

        $this->assertInstanceOf(RoomOwner::class, $roomOwner);
        $this->assertEquals($room->owners()->first()->user->id, $owner->id);
        $this->assertDatabaseHas('designer_owner', $expecredResult);
    }

    /**
    * @group PMS-495
    */    
    public function testCreateRoomOwnerWithUserIsNotOwner()
    {
        $room = factory(Room::class)->create();
        $owner = factory(User::class)->create([
            'is_owner' => 'false',
        ]);

        $roomOwner = $this->repository->createRoomOwner($room, $owner);
        $expecredResult = [
            'designer_id' => $room->id,
            'user_id' => $owner->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER
        ];

        $this->assertNull($roomOwner);
        $this->assertDatabaseMissing('designer_owner', $expecredResult);
    }

    /**
    * @group PMS-495
    */    
    public function testIsUnitTypeExistWithSameUnitTypeIgnoreCaseSensitiveAndSamePropertyId()
    {
        $duplicateName = 'test duplicate unit type';
        $property = factory(Property::class)->create([
            'name' => 'test name',
        ]);

        $owner = factory(User::class)->create();
        
        $room = factory(Room::class)->create([
            'unit_type' => null,
            'property_id' => $property->id
        ]);

        $room1 = factory(Room::class)->create([
            'unit_type' => $duplicateName,
            'property_id' => $property->id
        ]);
        
        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $result = $this->repository->isUnitTypeExist($property->id, strtoupper($duplicateName), $room->id);
        $this->assertTrue($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsUnitTypeExistWithSameUnitTypeIgnoreCaseSensitiveAndDifferentPropertyId()
    {
        $owner = factory(User::class)->create();
        $duplicateName = 'test duplicate unit type';

        $property1 = factory(Property::class)->create([
            'name' => 'test name 1',
        ]);

        $room = factory(Room::class)->create([
            'unit_type' => null,
            'property_id' => $property1->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $room1 = factory(Room::class)->create([
            'unit_type' => $duplicateName,
            'property_id' => $property1->id
        ]);
        
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room1->id,
        ]);
        
        $property2 = factory(Property::class)->create([
            'name' => 'test name 1',
        ]);

        $room2 = factory(Room::class)->create([
            'unit_type' => 'different property id',
            'property_id' => $property2->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room2->id,
        ]);

        $result = $this->repository->isUnitTypeExist($property2->id, strtoupper($duplicateName), $room->id);
        $this->assertFalse($result);
    }

    /**
    * @group PMS-495
    */    
    public function testIsUnitTypeExistWithSameRoomId()
    {
        $owner = factory(User::class)->create();
        $duplicateName = 'test duplicate unit type';

        $property1 = factory(Property::class)->create([
            'name' => 'test name 1',
        ]);

        $room = factory(Room::class)->create([
            'unit_type' => $duplicateName,
            'property_id' => $property1->id
        ]);

        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
        ]);

        $result = $this->repository->isUnitTypeExist($property1->id, strtoupper($duplicateName), $room->id);
        $this->assertFalse($result);
    }
}
