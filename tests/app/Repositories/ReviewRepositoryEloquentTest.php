<?php

namespace App\Repositories;

use App\Entities\Room\Review;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Builder;
use App\Entities\Device\UserDevice;
use Illuminate\Http\Request;
use phpmock\MockBuilder;

class ReviewRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $repository;

    protected $user;

    protected $room;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = app()->make(ReviewRepositoryEloquent::class);

        $this->user = factory(\App\User::class)->create();
        $this->room = factory(\App\Entities\Room\Room::class)->create();

        $userDevice = UserDevice::createDummy('device_token_dummy');
        $this->app->device = $userDevice;
        $this->app->user = $this->user;
    }

    public function testGetRatingAvgAndScaleValid()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'user_id' => 1,
            'designer_id' => $room->id,
            'price' => 2,
            'safe' => 1,
            'room_facility' => 3,
        ]);
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'user_id' => 2,
            'designer_id' => $room->id,
            'price' => 2,
            'safe' => 0,
            'room_facility' => 1,
        ]);
        factory(\App\Entities\Room\Review::class)->create([
            'status' => 'live',
            'user_id' => 3,
            'designer_id' => $room->id,
            'price' => 5,
            'safe' => 2,
            'room_facility' => 2,
        ]);

        $result = $this->repository->getRatingAvgAndScale($room);
        $this->assertEquals(3, $result['rating']->ratingPrice);
        $this->assertEquals(1, $result['rating']->ratingSafe);
        $this->assertEquals(2, $result['rating']->ratingRoom);
    }

    public function testGetRatingAvgAndScaleWillReturnCorrectScaleOn4()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'user_id'           => 1,
            'designer_id'       => $room->id,
            'price'             => 2,
            'safe'              => 1,
            'room_facility'     => 3,
            'scale'             => 4,
        ]);

        $result = $this->repository->getRatingAvgAndScale($room);
        $this->assertEquals(4, $result['scale']);
    }

    /**
     * @runInSeparateProcess 
     * @preserveGlobalState disabled
     */
    public function testGetRatingAvgAndScaleWillReturnCorrectScaleOn5()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'user_id'           => 1,
            'designer_id'       => $room->id,
            'price'             => 2,
            'safe'              => 1,
            'room_facility'     => 3,
            'scale'             => 4,
        ]);

        // mock for env RATING_SCALE=5
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Helpers')
            ->setName('config')
            ->setFunction( function ($key) { return 5; }
            );
        $this->mock = $builder->build();
        $this->mock->enable();

        // call test function
        $result = $this->repository->getRatingAvgAndScale($room);
        $this->assertEquals(5, $result['scale']);

        // disable mock RATING_SCALE
        $this->mock->disable();
    }

    public function testConvertRatingToScaleFrom4To5()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $dumpReview = factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'designer_id'       => $room->id,
            'scale'             => 4,
            'cleanliness'       => 2,
            'price'             => 2,
            'safe'              => 2,
            'comfort'           => 2,
            'room_facility'     => 2,
            'public_facility'   => 2,
        ]);

        $review = Review::where('id', $dumpReview->id)->first();

        // call converter
        $this->repository->convertRatingToScale(5, $review);

        $resultReview = Review::where('id', $dumpReview->id)->first()->toArray();

        $this->assertEquals(5, $resultReview['scale']);
        
        $this->assertEquals(2.5, $resultReview['cleanliness']);
        $this->assertEquals(2.5, $resultReview['price']);
        $this->assertEquals(2.5, $resultReview['safe']);
        $this->assertEquals(2.5, $resultReview['comfort']);
        $this->assertEquals(2.5, $resultReview['room_facility']);
        $this->assertEquals(2.5, $resultReview['public_facility']);
    }

    public function testConvertRatingToScaleFrom5To4()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $dumpReview = factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'designer_id'       => $room->id,
            'scale'             => 5,
            'cleanliness'       => 2.5,
            'price'             => 2.5,
            'safe'              => 2.5,
            'comfort'           => 2.5,
            'room_facility'     => 2.5,
            'public_facility'   => 2.5,
        ]);

        $review = Review::where('id', $dumpReview->id)->first();

        // call converter
        $this->repository->convertRatingToScale(4, $review);

        $resultReview = Review::where('id', $dumpReview->id)->first()->toArray();

        $this->assertEquals(4, $resultReview['scale']);
        
        $this->assertEquals(2, $resultReview['cleanliness']);
        $this->assertEquals(2, $resultReview['price']);
        $this->assertEquals(2, $resultReview['safe']);
        $this->assertEquals(2, $resultReview['comfort']);
        $this->assertEquals(2, $resultReview['room_facility']);
        $this->assertEquals(2, $resultReview['public_facility']);
    }

    public function testConvertRatingToScaleFrom5To4PerfectValue()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $dumpReview = factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'designer_id'       => $room->id,
            'scale'             => 5,
            'cleanliness'       => 5,
            'price'             => 5,
            'safe'              => 5,
            'comfort'           => 5,
            'room_facility'     => 5,
            'public_facility'   => 5,
        ]);

        $review = Review::where('id', $dumpReview->id)->first();

        // call converter
        $this->repository->convertRatingToScale(4, $review);

        $resultReview = Review::where('id', $dumpReview->id)->first()->toArray();

        $this->assertEquals(4, $resultReview['scale']);
        
        $this->assertEquals(4, $resultReview['cleanliness']);
        $this->assertEquals(4, $resultReview['price']);
        $this->assertEquals(4, $resultReview['safe']);
        $this->assertEquals(4, $resultReview['comfort']);
        $this->assertEquals(4, $resultReview['room_facility']);
        $this->assertEquals(4, $resultReview['public_facility']);
    }

    public function testConvertRatingToScaleFrom4To5PerfectValue()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $dumpReview = factory(\App\Entities\Room\Review::class)->create([
            'status'            => 'live',
            'designer_id'       => $room->id,
            'scale'             => 4,
            'cleanliness'       => 4,
            'price'             => 4,
            'safe'              => 4,
            'comfort'           => 4,
            'room_facility'     => 4,
            'public_facility'   => 4,
        ]);

        $review = Review::where('id', $dumpReview->id)->first();

        // call converter
        $this->repository->convertRatingToScale(5, $review);

        $resultReview = Review::where('id', $dumpReview->id)->first()->toArray();

        $this->assertEquals(5, $resultReview['scale']);
        
        $this->assertEquals(5, $resultReview['cleanliness']);
        $this->assertEquals(5, $resultReview['price']);
        $this->assertEquals(5, $resultReview['safe']);
        $this->assertEquals(5, $resultReview['comfort']);
        $this->assertEquals(5, $resultReview['room_facility']);
        $this->assertEquals(5, $resultReview['public_facility']);
    }

    public function testModel()
    {
        $this->assertEquals('App\Entities\Room\Review', $this->repository->model());
    }

    public function testGetModel()
    {
        $result = $this->repository->getModel();
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testCheckCountReview()
    {
        $user = factory(\App\User::class)->create();
        $room = factory(\App\Entities\Room\Room::class)->create();
        factory(\App\Entities\Room\Review::class)->create([
            'user_id'           => $user->id,
            'designer_id'       => $room->id,
        ]);
        factory(\App\Entities\Room\Review::class)->create([
            'user_id'           => $user->id,
            'designer_id'       => $room->id,
        ]);
        
        $result = $this->repository->checkCountReview($user, $room->id);

        $this->assertEquals($result, 2);
    }

    public function testInsertReviewOnScale4WithRequestScale4()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $data = [
            'clean'              => 0,
            'happy'              => 4,
            'safe'               => 4,
            'pricing'            => 1,
            'room_facilities'    => 4,
            'public_facilities'  => 2,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'photo'              => [],
        ];

        $result = $this->repository->insertReview($data, $this->user, $room->id);
        $this->assertDatabaseHas('designer_review', [
            'id'                => $result['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => $data['clean'],
            'comfort'           => $data['happy'],
            'safe'              => $data['safe'],
            'price'             => $data['pricing'],
            'room_facility'     => $data['room_facilities'],
            'public_facility'   => $data['public_facilities'],
            'content'           => $data['content'],
            'is_anonim'         => $data['anonim'],
            'status'            => 'waiting',
            'scale'             => 4,
        ]);
    }

    public function testInsertReviewOnScale4WithRequestScale5()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $data = [
            'clean'              => 0,
            'happy'              => 5,
            'safe'               => 5,
            'pricing'            => 2,
            'room_facilities'    => 5,
            'public_facilities'  => 0,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'photo'              => [],
            'scale'              => 5,
        ];

        $result = $this->repository->insertReview($data, $this->user, $room->id);
        $this->assertDatabaseHas('designer_review', [
            'id'                => $result['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => 0,
            'comfort'           => 4,
            'safe'              => 4,
            'price'             => 1.6,
            'room_facility'     => 4,
            'public_facility'   => 0,
            'content'           => $data['content'],
            'is_anonim'         => $data['anonim'],
            'status'            => 'waiting',
            'scale'             => 4,
        ]);
    }

    /**
     * @runInSeparateProcess 
     * @preserveGlobalState disabled
     */
    public function testInsertReviewOnScale5WithRequestScale5()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        // mock for env RATING_SCALE=5
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Helpers')
            ->setName('config')
            ->setFunction( function ($key) { return 5; }
            );
        $this->mock = $builder->build();
        $this->mock->enable();

        $data = [
            'clean'              => 5,
            'happy'              => 2,
            'safe'               => 5,
            'pricing'            => 0,
            'room_facilities'    => 4,
            'public_facilities'  => 2,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'photo'              => [],
            'scale'              => 5,
        ];

        $result = $this->repository->insertReview($data, $this->user, $room->id);

        // disable mock RATING_SCALE
        $this->mock->disable();

        $this->assertDatabaseHas('designer_review', [
            'id'                => $result['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => 5,
            'comfort'           => 2,
            'safe'              => 5,
            'price'             => 0,
            'room_facility'     => 4,
            'public_facility'   => 2,
            'content'           => $data['content'],
            'is_anonim'         => $data['anonim'],
            'status'            => 'waiting',
            'scale'             => 5,
        ]);
    }

    /**
     * @runInSeparateProcess 
     * @preserveGlobalState disabled
     */
    public function testInsertReviewOnScale5WithRequestScale4()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();

        // mock for env RATING_SCALE=5
        $builder = new MockBuilder();
        $builder->setNamespace('App\Http\Helpers')
            ->setName('config')
            ->setFunction( function ($key) { return 5; }
            );
        $this->mock = $builder->build();
        $this->mock->enable();

        $data = [
            'clean'              => 4,
            'happy'              => 2,
            'safe'               => 4,
            'pricing'            => 0,
            'room_facilities'    => 4,
            'public_facilities'  => 2,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'photo'              => [],
            'scale'              => 4,
        ];

        $result = $this->repository->insertReview($data, $this->user, $room->id);

        // disable mock RATING_SCALE
        $this->mock->disable();

        $this->assertDatabaseHas('designer_review', [
            'id'                => $result['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => 5,
            'comfort'           => 2.5,
            'safe'              => 5,
            'price'             => 0,
            'room_facility'     => 5,
            'public_facility'   => 2.5,
            'content'           => $data['content'],
            'is_anonim'         => $data['anonim'],
            'status'            => 'waiting',
            'scale'             => 5,
        ]);
    }

    public function testAdjustInputReview()
    {
        $data = [
            'clean'              => 4,
            'happy'              => 2.5,
            'safe'               => 4,
            'pricing'            => 0,
            'room_facilities'    => -1,
            'public_facilities'  => 7,
        ];

        $ratingNames = ['clean', 'happy', 'safe', 'pricing', 'room_facility', 'public_facility', 'room_facilities', 'public_facilities'];

        $repositoryEloquent = $this->app->make(ReviewRepositoryEloquent::class);
        $method = $this->getNonPublicMethodFromClass(ReviewRepositoryEloquent::class, 'adjustInputReview');
        $result = $method->invokeArgs(
            $repositoryEloquent,
            [
                $data,
                $ratingNames
            ]
        );

        $this->assertEquals([
            'clean'              => 4,
            'happy'              => 2,
            'safe'               => 4,
            'pricing'            => 0,
            'room_facilities'    => 0,
            'public_facilities'  => 5,
        ], $result);
    }

    public function testUpdateReviewOnScale4WithRequest4()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $data = [
            'clean'              => 0,
            'happy'              => 4,
            'safe'               => 4,
            'pricing'            => 1,
            'room_facilities'    => 4,
            'public_facilities'  => 2,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'photo'              => [],
        ];
        $this->repository->insertReview($data, $this->user, $room->id);
        
        // Update scenario
        $newData = [
            'clean'              => 0,
            'happy'              => 1,
            'safe'               => 1,
            'pricing'            => 4,
            'room_facilities'    => 3,
            'public_facilities'  => 2,
            'content'            => 'Review Content',
            'anonim'             => 0,
            'photo'              => [],
        ];
        $resultAfterUpdate = $this->repository->insertReview($newData, $this->user, $room->id);
        $this->assertDatabaseHas('designer_review', [
            'id'                => $resultAfterUpdate['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => $newData['clean'],
            'comfort'           => $newData['happy'],
            'safe'              => $newData['safe'],
            'price'             => $newData['pricing'],
            'room_facility'     => $newData['room_facilities'],
            'public_facility'   => $newData['public_facilities'],
            'content'           => $newData['content'],
            'is_anonim'         => $newData['anonim'],
            'status'            => 'waiting',
            'scale'             => 4,
        ]);
    }

    public function testUpdateReviewOnScale4WithRequest5()
    {
        $room = factory(\App\Entities\Room\Room::class)->create();
        $data = [
            'clean'              => 1,
            'happy'              => 1,
            'safe'               => 1,
            'pricing'            => 1,
            'room_facilities'    => 1,
            'public_facilities'  => 1,
            'content'            => 'Review Content',
            'anonim'             => 1,
            'scale'              => 5,
            'photo'              => [],
        ];
        $this->repository->insertReview($data, $this->user, $room->id);
        
        // Update scenario
        $newData = [
            'clean'              => 5,
            'happy'              => 5,
            'safe'               => 5,
            'pricing'            => 5,
            'room_facilities'    => 5,
            'public_facilities'  => 5,
            'content'            => 'Review Content',
            'anonim'             => 0,
            'scale'              => 5,
            'photo'              => [],
        ];
        $resultAfterUpdate = $this->repository->insertReview($newData, $this->user, $room->id);
        $this->assertDatabaseHas('designer_review', [
            'id'                => $resultAfterUpdate['review_id'],
            'user_id'           => $this->user->id,
            'cleanliness'       => 4,
            'comfort'           => 4,
            'safe'              => 4,
            'price'             => 4,
            'room_facility'     => 4,
            'public_facility'   => 4,
            'content'           => $newData['content'],
            'is_anonim'         => $newData['anonim'],
            'status'            => 'waiting',
            'scale'             => 4,
        ]);
    }

    public function testGetReviewsByKosIDReturnEmpty()
    {
        $request = new Request();
        $response = $this->repository->getReviewsByKosID(100, $request, $this->user);

        $this->assertEmpty($response);
    }

    public function testGetReviewsByKosIDWithoutReview()
    {
        $room = factory(Room::class)->create(['song_id' => 5]);
        $request = new Request();

        $expectedResponse = [
            "rating" => "0.0",
            "all_rating" => [
                "clean" => 0,
                "happy" => 0,
                "safe" => 0,
                "pricing" => 0,
                "room_facilities" => 0,
                "public_facilities" => 0
            ],
            "owner" => false,
            "has_more" => false,
            "data_count" => 0,
            "data" => []
        ];

        $response = $this->repository->getReviewsByKosID($room->song_id, $request, $this->user);
        $this->assertEquals($expectedResponse, $response);
    }

    public function testGetReviewsByKosIDWithReview()
    {
        $room = factory(Room::class)->create(['song_id' => 5]);

        $request = new Request();

        factory(Review::class, 3)->create([
            'status'          => 'live',
            'designer_id'     => $room->id,
            'price'           => 2,
            'safe'            => 1,
            'room_facility'   => 3,
        ]);

        $response = $this->repository->getReviewsByKosID($room->song_id, $request, $this->user);
        $this->assertCount(3, $response['data']);
    }

    public function testGetAllAndDetailRatingReturnInteger()
    {
        $room = factory(Room::class)->create(['song_id' => 5]);

        factory(Review::class, 3)->create([
            'status'          => 'live',
            'designer_id'     => $room->id,
            'price'           => 3,
            'safe'            => 4.3,
            'room_facility'   => 3.2,
        ]);

        $response = $this->repository->getAllAndDetailRating($room);
        $this->assertIsInt($response['all_rating']['safe']);
        $this->assertNotEmpty($response['rating']);
    }

    public function testGetAllAndDetailRatingReturnFloat()
    {
        $room = factory(Room::class)->create(['song_id' => 5]);

        factory(Review::class, 3)->create([
            'status'          => 'live',
            'designer_id'     => $room->id,
            'price'           => 3,
            'safe'            => 4.3,
            'room_facility'   => 3.2,
        ]);

        // override old app and make the new app that support float rating
        $this->app->device = $this->createDummyDevice();

        $response = $this->repository->getAllAndDetailRating($room);
        $this->assertIsFloat($response['all_rating']['safe']);
        $this->assertNotEmpty($response['rating']);
    }

    public function testSetLimitForGettingReviewWithSortingByNewest()
    {
        $request = new Request(
            [
                'sort'  => 'new',
                'limit' => 10
            ]
        );

        $method = $this->getNonPublicMethodFromClass($this->repository, 'setLimitForGettingReview');
        $response = $method->invokeArgs($this->repository, [$request]);
        $this->assertEquals(11, $response);
    }

    public function testSetLimitForGettingReviewWithSortingByBest()
    {
        $request = new Request(
            [
                'sort'  => 'Best',
                'limit' => 10
            ]
        );

        $method = $this->getNonPublicMethodFromClass($this->repository, 'setLimitForGettingReview');
        $response = $method->invokeArgs($this->repository, [$request]);
        $this->assertEquals(21, $response);
    }

    private function createDummyDevice()
    {
        $randomKey = UserDevice::max('id') + 1 . rand(10000, 99999);

        $deviceDetail = array(
            'identifier'            => "user_device_" . $randomKey ,
            'uuid'                  => "2381023818371312938129" . $randomKey,
            'platform'              => 'android',
            'platform_version_code' => 22,
            'model'                 => 'Samsung SM-A510F',
            'email'                 => 'user_device_' . $randomKey . '@songyoung.kang',
            'app_version_code'      =>  UserDevice::MIN_RATING_5_APP_VERSION_ANDROID
        );

        $userDevice = UserDevice::insertOrUpdate($deviceDetail, 0);
        $userDevice->device_token = 'device_token_dummy';
        $userDevice->save();

        return $userDevice;
    }
}
