<?php

namespace App\Repositories\SLA;

use App\Entities\Booking\BookingUser;
use App\Entities\SLA\SLAVersion;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class SLAVersionRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(SLAVersionRepositoryEloquent::class);
    }

    public function testGetSLAVersionShouldReturnVersionTwo(): void
    {
        $expected = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => Carbon::now()
        ]);
        $booking = factory(BookingUser::class)->make([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'created_at' => Carbon::now()->addMinute()
        ]);

        $actual = $this->repository->getSLAVersion($booking->status, $booking->created_at);

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetSLAVersionShouldReturnVersionThree(): void
    {
        $expected = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => Carbon::now()
        ]);
        $booking = factory(BookingUser::class)->make([
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'created_at' => Carbon::now()->addMinute()
        ]);

        $actual = $this->repository->getSLAVersion($booking->status, $booking->created_at);

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetSLAVersionShouldReturnNull(): void
    {
        $booking = factory(BookingUser::class)->make([
            'status' => BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
            'created_at' => Carbon::now()->addMinute()
        ]);

        $actual = $this->repository->getSLAVersion($booking->status, $booking->created_at);

        $this->assertNull($actual);
    }

    public function testGetSLAVersionWithVersionTwoShouldReturnNewestVersion(): void
    {
        factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'version_number' => 1,
            'created_at' => Carbon::now()
        ]);
        $expected = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'version_number' => 2,
            'created_at' => Carbon::now()->addMinute()
        ]);
        $booking = factory(BookingUser::class)->make([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'created_at' => Carbon::now()->addMinutes(2)
        ]);

        $actual = $this->repository->getSLAVersion($booking->status, $booking->created_at);

        $this->assertTrue(
            $actual->is($expected)
        );
    }

    public function testGetSLAVersionWithVersionThreeShouldReturnNewestVersion(): void
    {
        factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'version_number' => 1,
            'created_at' => Carbon::now()
        ]);
        $expected = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'version_number' => 2,
            'created_at' => Carbon::now()->addMinute()
        ]);
        $booking = factory(BookingUser::class)->make([
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'created_at' => Carbon::now()->addMinutes(2)
        ]);

        $actual = $this->repository->getSLAVersion($booking->status, $booking->created_at);

        $this->assertTrue(
            $actual->is($expected)
        );
    }
}
