<?php

namespace App\Repositories\Landing;

use App\Entities\Activity\ViewTemp3;
use App\Entities\Landing\HomeStaticLanding;
use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use App\User;

class HomeLandingRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;
    protected $user;
    protected $city;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(HomeStaticLandingRepository::class);
        $this->user = factory(User::class)->create();

        $landing1 = factory(Landing::class)->create(
            [
                'id' => 73,
                'slug' => 'kost-jakarta-murah',
                'latitude_1' => -6.211892748573704,
                'longitude_1' => 106.80856704711914,
                'latitude_2' => -6.147040448664959,
                'longitude_2' => 106.85045242309572
            ]
        );

        $landing2 = factory(Landing::class)->create(
            [
                'id' => 43,
                'slug' => 'kost-jogja-murah',
                'latitude_1' => -7.847056947123799,
                'longitude_1' => 110.32470703125001,
                'latitude_2' => -7.687175708432646,
                'longitude_2' => 110.42907714843751
            ]
        );

        factory(HomeStaticLanding::class)->create(
            [
                'id' => 104,
                'parent_id' => null,
                'landing_id' => $landing1->id,
                'type' => 'kost',
                'ordinal' => 1,
                'name' => 'Jakarta'
            ]
        );

        factory(HomeStaticLanding::class)->create(
            [
                'id' => 86,
                'parent_id' => null,
                'landing_id' => $landing2->id,
                'type' => 'kost',
                'ordinal' => 1,
                'name' => 'Jogja'
            ]
        );

        $this->city = HomeStaticLanding::with('landing_kost')
            ->whereNull('parent_id')
            ->inRandomOrder()
            ->get();
    }

    public function testModel()
    {
        $this->assertEquals(HomeStaticLanding::class, $this->repository->model());
    }

    public function testGetCitiesWithLandingMatch()
    {
        $room = $this->generateKosInJogjaCity();

        factory(ViewTemp3::class)->create(
            [
                'user_id' => $this->user->id,
                'designer_id' => $room->id,
                'status' => 'logged'
            ]
        );

        $response = $this->repository->getCities($this->user);

        $this->assertEquals('Jogja', $response[0]['name']);
    }

    public function testGetCitiesWithNoLandingMatchReturnEmpty()
    {
        $room1 = $this->generateKosWhichIsNotOnLanding();

        factory(ViewTemp3::class)->create(
            [
                'user_id' => $this->user->id,
                'designer_id' => $room1->id,
                'status' => 'logged'
            ]
        );

        $response = $this->repository->getCities($this->user);

        $this->assertEmpty($response);
    }

    public function testGetCitiesWithNoLandingMatchAndThereIsRoomAvailableInJakarta()
    {
        $room1 = $this->generateKosWhichIsNotOnLanding();

        $this->generateKosInJakartaCity();

        factory(ViewTemp3::class)->create(
            [
                'user_id' => $this->user->id,
                'designer_id' => $room1->id,
                'status' => 'logged'
            ]
        );

        $response = $this->repository->getCities($this->user);

        $this->assertEquals('Jakarta', $response[0]['name']);
    }


    public function testGetFirstCityWithMatchLanding()
    {
        // this room is in jogja city
        $room = $this->generateKosInJogjaCity();

        factory(ViewTemp3::class)->create(
            [
                'user_id' => $this->user->id,
                'designer_id' => $room->id,
                'status' => 'logged'
            ]
        );

        $response = $this->repository->getFirstCity($this->city, $this->user);
        $this->assertEquals('Jogja', $response['name']);
        $this->assertEquals(86, $response['id']);
    }

    public function testGetFirstCityWithNoLandingMatch()
    {
        // this room is not in any landing
        $room = $this->generateKosWhichIsNotOnLanding();

        factory(ViewTemp3::class)->create(
            [
                'user_id' => $this->user->id,
                'designer_id' => $room->id,
                'status' => 'logged'
            ]
        );

        $response = $this->repository->getFirstCity($this->city, $this->user);
        $this->assertNull($response);
    }

    public function testGetFirstCityWithNonLoginUser()
    {
        $response = $this->repository->getFirstCity($this->city);

        $this->assertEquals($this->city->first(), $response);
    }

    public function testGetPrioritizedCityReturnLanding()
    {
        $this->generateKosInJogjaCity();

        $response = $this->repository->getPrioritizedCity($this->city);
        $this->assertEquals(86, $response['id']);
        $this->assertEquals('Jogja', $response['name']);
    }

    public function testGetPrioritizedCityReturnPrioritizedLanding()
    {
        $this->generateKosInJogjaCity();

        $this->generateKosInJakartaCity();

        // Based on order, Jakarta should come first
        $response = $this->repository->getPrioritizedCity($this->city);
        $this->assertEquals(104, $response['id']);
        $this->assertEquals('Jakarta', $response['name']);
    }

    public function testGetPrioritizedCityReturnNull()
    {
        $response = $this->repository->getPrioritizedCity($this->city);
        $this->assertNull($response);
    }

    public function testBetweenReturnTrue()
    {
        $low = -7.857056947123799;
        $high = -7.827056947123799;
        $subject = -7.837056947123799;

        $method = $this->getNonPublicMethodFromClass($this->repository, 'between');
        $response = $method->invokeArgs(
            $this->repository,
            [
                $low,
                $high,
                $subject
            ]
        );

        $this->assertTrue($response);
    }

    public function testBetweenReturnFalse()
    {
        $low = -6.857056947123799;
        $high = -7.827056947123799;
        $subject = -7.837056947123799;

        $method = $this->getNonPublicMethodFromClass($this->repository, 'between');
        $response = $method->invokeArgs(
            $this->repository,
            [
                $low,
                $high,
                $subject
            ]
        );

        $this->assertFalse($response);
    }

    public function testIsRoomExistOnLandingReturnTrue()
    {
        $landing = factory(Landing::class)->create(
            [
                'slug' => 'kost-murah',
                'latitude_1' => -7.847056947123799,
                'longitude_1' => 110.32470703125001,
                'latitude_2' => -7.687175708432646,
                'longitude_2' => 110.42907714843751
            ]
        );

        $this->generateKosInJogjaCity();

        $method = $this->getNonPublicMethodFromClass($this->repository, 'isRoomExistOnLanding');
        $response = $method->invokeArgs($this->repository, [$landing]);

        $this->assertTrue($response);
    }

    public function testIsRoomExistOnLandingReturnFalse()
    {
        $landing = factory(Landing::class)->create(
            [
                'slug' => 'kost-murah',
                'latitude_1' => -7.847056947123799,
                'longitude_1' => 110.32470703125001,
                'latitude_2' => -7.687175708432646,
                'longitude_2' => 110.42907714843751
            ]
        );

        $method = $this->getNonPublicMethodFromClass($this->repository, 'isRoomExistOnLanding');
        $response = $method->invokeArgs($this->repository, [$landing]);

        $this->assertFalse($response);
    }

    private function generateKosInJogjaCity()
    {
        return Factory(Room::class)->create(
            [
                'latitude' => -7.837056947123799,
                'longitude' => 110.33470703125001,
                'room_available' => 3,
                'price_monthly' => 100000
            ]
        );
    }

    private function generateKosInJakartaCity()
    {
        return Factory(Room::class)->create(
            [
                'latitude' => -6.201892748573704,
                'longitude' => 106.81856704711914,
                'price_monthly' => 1000000,
                'room_available' => 3
            ]
        );
    }

    private function generateKosWhichIsNotOnLanding()
    {
        return Factory(Room::class)->create(
            [
                'latitude' => -1.837056947123799,
                'longitude' => 10.33470703125001,
                'price_monthly' => 1000000,
                'room_available' => 3
            ]
        );
    }

}
