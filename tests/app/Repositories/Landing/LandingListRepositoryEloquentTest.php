<?php

namespace App\Repositories\Landing;

use App\Test\MamiKosTestCase;
use App\Entities\Landing\LandingList;
use Illuminate\Database\Eloquent\Collection;

class LandingListRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;
    private $landingBookingSpecific;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(LandingListRepositoryEloquent::class);

        $this->landingBookingSpecifics = factory(LandingList::class, 3)
            ->states(LandingList::BOOKING_SPECIFIC)
            ->create([
                'is_active' => 1
            ]);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testModel_Success()
    {
        $this->assertSame(LandingList::class, $this->repository->model());
    }

    public function testGetLandingBookingSpecificById_Success()
    {
        $dummyId = $this->landingBookingSpecifics->first()->id;
        $landing = $this->repository->getLandingBookingSpecificById($dummyId);

        $this->assertInstanceOf(LandingList::class, $landing);
        $this->assertSame($dummyId, $landing->id);
    }

    public function testGetAllBookingLanding_BookingSpecific()
    {
        $landings = $this->repository->getAllBookingLanding([LandingList::BOOKING_SPECIFIC]);

        $this->assertSame($landings->last()->type, LandingList::BOOKING_SPECIFIC);
        $this->assertInstanceOf(Collection::class, $landings);
    }

    public function testGetCitiesAjax_Success()
    {
        $cities = $this->repository->getCitiesAjax('is_booking', 1);

        $this->assertInternalType("array", $cities);
    }
}
