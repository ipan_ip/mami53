<?php

namespace App\Repositories\Landing;

use App\Test\MamiKosTestCase;
use App\Entities\Landing\LandingMetaOg;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class LandingMetaOgRepositoryEloquentTest extends MamiKosTestCase
{
    private const SAVE = 'admin/landing-meta/save';
    private const EDIT = 'admin/landing-meta/update';

    private $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(LandingMetaOgRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testModel_Success()
    {
        $this->assertSame(LandingMetaOg::class, $this->repository->model());
    }

    public function testIndex_Success()
    {
        $this->assertInstanceOf(LengthAwarePaginator::class, $this->repository->index());
    }

    public function testSave_Success()
    {
        $fakeRequest = $this->createFakeRequest(
            self::SAVE,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->repository->save($fakeRequest));
    }

    public function testFindById_Success()
    {
        $landingMetaOg = factory(LandingMetaOg::class)->create();
        $this->assertInstanceOf(LandingMetaOg::class, $this->repository->findById($landingMetaOg->id));
    }

    public function testEdit_Success()
    {
        $landingMetaOg = factory(LandingMetaOg::class)->create();
        $fakeRequest = $this->createFakeRequest(
            self::EDIT,
            'POST',
            [
                'page_type'   => LandingMetaOg::PAGE_TYPES[mt_rand(0,3)],
                'title'       => $this->faker->catchPhrase(),
                'description' => $this->faker->paragraph(3),
                'keywords'    => $this->faker->paragraph(3),
                'image'       => $this->faker->imageUrl(640, 480),
                'is_active'   => mt_rand(0, 1)
            ]
        );
        $this->assertTrue($this->repository->edit($fakeRequest, $landingMetaOg->id));
    }

    public function testGetFirstActiveLastUpdated_Null()
    {
        $this->assertNull($this->repository->getFirstActiveLastUpdated(''));
    }

    public function testGetFirstActiveLastUpdated_Homepage()
    {
        factory(LandingMetaOg::class)->states('active', LandingMetaOg::HOMEPAGE)->create([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->assertInstanceOf(LandingMetaOg::class, $this->repository->getFirstActiveLastUpdated(LandingMetaOg::HOMEPAGE));
    }

    public function testGetFirstActiveLastUpdated_LandingBookingOwner()
    {
        factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_BOOKING_OWNER)->create([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->assertInstanceOf(LandingMetaOg::class, $this->repository->getFirstActiveLastUpdated(LandingMetaOg::LANDING_BOOKING_OWNER));
    }

    public function testGetFirstActiveLastUpdated_LandingAre()
    {
        factory(LandingMetaOg::class)->states('active', LandingMetaOg::LANDING_AREA)->create([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->assertInstanceOf(LandingMetaOg::class, $this->repository->getFirstActiveLastUpdated(LandingMetaOg::LANDING_AREA));
    }

    public function testGetFirstActiveLastUpdated_DetailKost()
    {
        factory(LandingMetaOg::class)->states('active', LandingMetaOg::DETAIL_KOST)->create([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        $this->assertInstanceOf(LandingMetaOg::class, $this->repository->getFirstActiveLastUpdated(LandingMetaOg::DETAIL_KOST));
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}