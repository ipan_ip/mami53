<?php

namespace App\Repositories\Landing;

use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Entities\Component\ListCampus;
use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use Faker\Factory as Faker;
use Illuminate\Container\Container as Application;

class LandingRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;
    private $landings;
    private $faker;

    const SUB_DISTRICT = 'testing tester';
    const MIN_ROOMS = 10;
    const MAX_ROOMS = 50;
    const LATITUDE_1 = -6.171647;
    const LATITUDE_2 = -6.155562;
    const LONGITUDE_1 = 106.756354;
    const LONGITUDE_2 = 106.773043;
    const TAG_WIFI = 15;
    const TAG_KAMAR_MANDI_DALAM = 1;
    const TAG_BEBAS_24_JAM = 59;
    const TAG_PASUTRI =60;
    const COUNT_ROOMS = 3;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(LandingRepositoryEloquent::class);

        $this->landings = factory(Landing::class, 3)->create([
            'area_subdistrict' => self::SUB_DISTRICT,
            'latitude_1' => self::LATITUDE_1,
            'latitude_2' => self::LATITUDE_2,
            'longitude_1' => self::LONGITUDE_1,
            'longitude_2' => self::LONGITUDE_2,
        ]);

        $this->faker = Faker::create();
    }

    public function testModel_Success()
    {
        $this->assertSame(Landing::class, $this->repository->model());
    }

    public function testGetFirstLandingBySlugAreaSubdistrict()
    {
        $slug = $this->landings->first()->slug;
        $landing = $this->repository->getFirstLandingBySlugAreaSubdistrict($slug);

        $this->assertSame($landing->slug, $slug);
        $this->assertInstanceOf(Landing::class, $landing);
    }

    public function testGetLandingDetailBySlug()
    {
        $slug = $this->landings->first()->slug;
        $landing = $this->repository->getLandingDetailBySlug($slug);

        $this->assertSame($landing->slug, $slug);
        $this->assertInstanceOf(Landing::class, $landing);
    }

    public function testGetCheapestDaily_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=5; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'price_daily' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $price_dailys = collect($rooms)->pluck('price_daily')->toArray();
        $cheapestDaily = $this->repository->getCheapestDaily($landingDetail);

        $this->assertTrue(in_array($cheapestDaily->price_daily, $price_dailys));
        $this->assertTrue(in_array($cheapestDaily->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $cheapestDaily);
    }

    public function testGetCheapestMonthly_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=5; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'price_monthly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $price_monthlys = collect($rooms)->pluck('price_monthly')->toArray();
        $cheapestMonthly = $this->repository->getCheapestMonthly($landingDetail);

        $this->assertTrue(in_array($cheapestMonthly->price_monthly, $price_monthlys));
        $this->assertTrue(in_array($cheapestMonthly->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $cheapestMonthly);
    }

    public function testGetCheapestWeekly_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'price_weekly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $price_weeklys = collect($rooms)->pluck('price_weekly')->toArray();
        $cheapestWeekly = $this->repository->getCheapestWeekly($landingDetail);

        $this->assertTrue(in_array($cheapestWeekly->price_weekly, $price_weeklys));
        $this->assertTrue(in_array($cheapestWeekly->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $cheapestWeekly);
    }

    public function testGetCheapestYearly_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'price_yearly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $price_yearlys = collect($rooms)->pluck('price_yearly')->toArray();
        $cheapestYearly = $this->repository->getCheapestYearly($landingDetail);

        $this->assertTrue(in_array($cheapestYearly->price_yearly, $price_yearlys));
        $this->assertTrue(in_array($cheapestYearly->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $cheapestYearly);
    }

    public function testGetAvailableBisaBooking_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'is_booking' => true,
                'price_monthly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $bisaBookings = collect($rooms)->pluck('price_monthly')->toArray();
        $bisaBooking = $this->repository->getAvailableBisaBooking($landingDetail);

        $this->assertTrue(in_array($bisaBooking->price_monthly, $bisaBookings));
        $this->assertTrue(in_array($bisaBooking->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $bisaBooking);
    }

    public function testGetCheapestPutri_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'gender' => 2,
                'price_monthly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $putris = collect($rooms)->pluck('price_monthly')->toArray();
        $putri = $this->repository->getCheapestPutri($landingDetail);

        $this->assertTrue(in_array($putri->price_monthly, $putris));
        $this->assertTrue(in_array($putri->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $putri);
    }

    public function testGetCheapestPutra_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'gender' => 1,
                'price_monthly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $putras = collect($rooms)->pluck('price_monthly')->toArray();
        $putra = $this->repository->getCheapestPutra($landingDetail);

        $this->assertTrue(in_array($putra->price_monthly, $putras));
        $this->assertTrue(in_array($putra->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $putra);
    }

    public function testGetCheapestCampur_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        for ($x=1; $x<=self::COUNT_ROOMS; $x++) {
            $rooms[] = factory(Room::class)->create([
                'is_active' => 'true',
                'room_available' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'room_count' => self::MAX_ROOMS,
                'expired_phone' => 0,
                'is_testing' => 0,
                'latitude' => ($landingDetail->latitude_1 + $landingDetail->latitude_2) / 2,
                'longitude' => ($landingDetail->longitude_1 + $landingDetail->longitude_2) / 2,
                'gender' => 0,
                'price_monthly' => $this->faker->numberBetween(self::MIN_ROOMS, self::MAX_ROOMS),
                'area_city' => $this->faker->city
            ]);
        }

        $songIds = collect($rooms)->pluck('song_id')->toArray();
        $campurs = collect($rooms)->pluck('price_monthly')->toArray();
        $campur = $this->repository->getCheapestCampur($landingDetail);

        $this->assertTrue(in_array($campur->price_monthly, $campurs));
        $this->assertTrue(in_array($campur->song_id, $songIds));
        $this->assertInstanceOf(Room::class, $campur);
    }

    public function testSetRoom_Success()
    {
        $slug = $this->landings->first()->slug;
        $landingDetail = $this->repository->getLandingDetailBySlug($slug);

        $landingRepositoryEloquent = new LandingRepositoryEloquent(new Application);

        $landingRepositoryEloquentReflector = new \ReflectionClass(LandingRepositoryEloquent::class);
        $method = $landingRepositoryEloquentReflector->getMethod('setRoom');
        $method->setAccessible(true);
        $result = $method->invokeArgs( 
            $landingRepositoryEloquent, 
            [
                $landingDetail,
            ]
        );

        $this->assertInstanceOf(Builder::class, $result);
    }

    public function testGetCampusType_EmptyArray()
    {
        $landings = factory(Landing::class)->states('campus')->create([
                'keyword' => 'tester'
            ]);
        $keyword = $landings->keyword;

        $res = $this->repository->getCampusType([])
            ->pluck('keyword')
            ->first(function($item) use($keyword){
                if ($item === $keyword ) {
                    return $item;
                }
            });

        $this->assertSame($res, $keyword);
    }

    public function testGetCampusType_NonEmptyArray()
    {
        $slug = $this->faker->slug;
        $landings = factory(Landing::class)->states('campus')->create([
                'keyword' => 'tester',
                'slug' => $slug
            ]);
        $keyword = $landings->keyword;

        $res = $this->repository->getCampusType([])
            ->pluck('keyword')
            ->first(function($item) use($keyword){
                if ($item === $keyword ) {
                    return $item;
                }
            });

        $this->assertSame($res, $keyword);
    }
}