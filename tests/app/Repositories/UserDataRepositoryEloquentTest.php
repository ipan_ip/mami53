<?php

namespace App\Repositories;

use App\Test\MamiKosTestCase;
use App\Entities\User\UserSocial;
use App\User;

class UserDataRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = app()->make(UserDataRepositoryEloquent::class);
    }

    public function testGetUserByIdWillReturnValidData()
    {
        $user = factory(\App\User::class)->create(['is_verify' => 1]);

        $result = $this->repository->getUserById($user->id);

        $this->assertEquals($user->id, $result->id);
        $this->assertEquals($user->email, $result->email);
        $this->assertEquals($user->phone_number, $result->phone_number);
        $this->assertEquals($user->name, $result->name);
        $this->assertEquals($user->gender, $result->gender);
        $this->assertEquals($user->job, $result->job);
        $this->assertEquals($user->role, $result->role);
        $this->assertEquals($user->is_verify, $result->is_verify);
    }

    public function testGetUserByIdWillReturnNull()
    {
        $result = $this->repository->getUserById(122);
        $this->assertNull($result);
    }

    /**
     * @group UG
     * @group App/Repositories
     * @group App/Repositories/UserDataRepositoryEloquentTest
     */
    public function testIsUserSocialReturnTrue()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->create(['id' => $userId]);
        factory(UserSocial::class)->create(['user_id' => $userId]);

        $expected = $this->repository->isUserSocial($userId);
        $this->assertEquals($expected, true);
    }

    /**
     * @group UG
     * @group App/Repositories
     * @group App/Repositories/UserDataRepositoryEloquentTest
     */
    public function testIsUserSocialReturnFalse()
    {
        $userId = mt_rand(1, 99999);
        $user = factory(\App\User::class)->make(['id' => $userId.'1']);
        factory(UserSocial::class)->make(['user_id' => $userId.mt_rand(1, 10)]);

        $expected = $this->repository->isUserSocial($user->id);
        $this->assertEquals($expected, false);
    }

    public function testGetOwnerByPhoneNumberAndReturnNull()
    {
        $result = $this->repository->getOwnerByPhoneNumber(null);
        $this->assertEmpty($result);
    }

    public function testGetOwnerByPhoneNumberAndReturnSuccess()
    {
        $phone = '08213123123123';
        $owner = factory(User::class)->create([
            'is_owner' => 'true',
            'phone_number' => $phone
            ]);
        $result = $this->repository->getOwnerByPhoneNumber($phone);
        $this->assertEquals($owner->id, $result->id);
    }
}
