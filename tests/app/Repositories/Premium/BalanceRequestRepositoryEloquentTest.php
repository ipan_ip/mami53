<?php

namespace App\Repositories;

use App;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\BalanceRequestRepositoryEloquent;

class BalanceRequestRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $balanceRequestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->balanceRequestRepo = App::make(BalanceRequestRepositoryEloquent::class);
    }

    public function testGetUnpaidReturnEmpty()
    {
        $expiredDate = date('Y-m-d', strtotime('+3 days'));
        factory(BalanceRequest::class)->create([
            'expired_date' => $expiredDate,
            'expired_status' => 'false'
        ]);
        $tomorrowDate = date('Y-m-d', strtotime('+1 day'));
        $balanceRequests = $this->balanceRequestRepo->getUnpaid($tomorrowDate);
        $this->assertEquals(count($balanceRequests), 0);
    }

    public function testGetUnpaidReturnResult()
    {
        $expiredDate = date('Y-m-d', strtotime('+1 days'));
        factory(BalanceRequest::class)->create([
            'expired_date' => $expiredDate,
            'expired_status' => 'false'
        ]);
        $balanceRequests = $this->balanceRequestRepo->getUnpaid($expiredDate);
        $this->assertEquals(count($balanceRequests), 1);
    }
}
