<?php

namespace App\Repositories;

use App;

use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\PaymentRepositoryEloquent;

class PaymentRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $paymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paymentRepo = App::make(PaymentRepositoryEloquent::class);
    }

    public function testGetByPremiumRequestIds()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $payment = factory(Payment::class)->create([
            'premium_request_id' => $premiumRequest->id
        ]);
        $payments = $this->paymentRepo->getByPremiumRequestIds([$premiumRequest->id]);
        $this->assertEquals(count($payments), 1);
    }

    public function testGetUnpaidByExpiredAtReturnEmpty()
    {
        $expiredTime = date('Y-m-d H:i:s', strtotime('+3 hour'));
        factory(Payment::class)->create([
            'expired_at' => $expiredTime,
            'transaction_status' => 'pending'
        ]);
        $nextHourTime = strtotime('+1 hour');
        $payments = $this->paymentRepo->getUnpaidByExpiredAt($nextHourTime);
        $this->assertEquals(count($payments), 0);
    }

    public function testGetUnpaidByExpiredAtReturnResult()
    {
        $expiredTime = date('Y-m-d H:i:s', strtotime('+1 hour'));
        factory(Payment::class)->create([
            'expired_at' => $expiredTime,
            'transaction_status' => 'pending'
        ]);
        $nextHourTime = strtotime('+1 hour');
        $payments = $this->paymentRepo->getUnpaidByExpiredAt($nextHourTime);
        $this->assertEquals(count($payments), 1);
    }
}
