<?php

namespace App\Repositories\Premium;

use App;
use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumCashbackHistory;

class PremiumCashbackHistoryRepositoryEloquentTest extends MamiKosTestCase
{

    protected $repository;

    public function setUp(): void
    {
        parent::setUp();

        $this->repository = App::make(PremiumCashbackHistoryRepository::class);
    }

    public function testModel()
    {
        $this->assertEquals(PremiumCashbackHistory::class, $this->repository->model());
    }

    public function testSaveCashbackHistoryShouldSuccess()
    {
        $data = array(
            'user_id' => 1,
            'reference_id' => 2,
            'amount' => 10,
            'type' => PremiumCashbackHistory::CASHBACK_TYPE_TOPUP,
        );

        $this->repository->save($data);

        $this->assertDatabaseHas('premium_cashback_history', ['user_id' => 1, 'reference_id' => 2, 'amount' => 10]);
    }

}