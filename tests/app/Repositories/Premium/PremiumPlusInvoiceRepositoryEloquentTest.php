<?php

namespace App\Repositories\Premium;

use App;
use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumPlusUser;
use App\Entities\Premium\PremiumPlusInvoice;
use App\Entities\Room\Room;
use App\User;
use App\Repositories\Premium\PremiumPlusInvoiceRepository;
use Carbon\Carbon;
use App\Entities\Premium\Payment;
use Facades\Illuminate\Support\Str;

class PremiumPlusInvoiceRepositoryEloquentTest extends MamiKosTestCase
{
 
    protected $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = App::make(PremiumPlusInvoiceRepository::class);
    }

    public function testModel()
    {
        $this->assertEquals(PremiumPlusInvoice::class, $this->repository->model());
    }

    public function testGetInvoicesByPremiumPlusUser()
    {
        $user = factory(PremiumPlusUser::class)->create();
        
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user
        ]);
        
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user
        ]);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($user->id);

        $this->assertEquals(2, count($invoices));
    }

    public function testSaveInvoicesShouldSuccess()
    {
        $user = factory(PremiumPlusUser::class)->create([
            'active_period'             => 6
        ]);
        $data = array(
            'total_monthly_premium'     => 1000000,
            'active_period'             => 6,
            'premium_plus_user_id'      => $user->id,
            'guaranteed_room'           => 5,
            'old_total_monthly_premium' => 0,
            'old_guaranteed_room'       => 0,
            'activation_date'           => now(),
            'is_notif_sent'             => false,
            'is_active_user'            => false,
            'designer_id'               => 9,
        );
        
        $this->repository->save($user, $data);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($user->id);

        $this->assertEquals(6, count($invoices));
    }

    public function testUpdateInvoicesShouldSuccess()
    {
        $user = factory(PremiumPlusUser::class)->create([
            'active_period'     => 6,
            'start_date'        => now()
        ]);
        
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);

        $data = array(
            'total_monthly_premium'     => 1200000,
            'premium_plus_user_id'      => $user->id,
            'guaranteed_room'           => 6,
            'old_total_monthly_premium' => 10000000,
            'old_guaranteed_room'       => 5,
            'active_period'             => 5,
            'activation_date'           => now(),
            'is_notif_sent'             => true,
            'is_active_user'            => true
        );

        $this->repository->save($user, $data);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($user->id);

        $this->assertEquals(6, count($invoices));
    }

    public function testCalculatePremiumMonthly()
    {
        $user = factory(PremiumPlusUser::class)->create([
            'active_period'     => 6,
            'start_date'        => now()
        ]);
        $invoice = factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);
        $invoice2 = factory(PremiumPlusInvoice::class)->create([
            "premium_plus_user_id"  => $user->id
        ]);

        $data = array(
            'total_monthly_premium'     => 1200000,
            'premium_plus_user_id'      => $user->id,
            'guaranteed_room'           => 6,
            'old_total_monthly_premium' => 10000000,
            'old_guaranteed_room'       => 5,
            'active_period'             => 5,
            'activation_date'           => now(),
            'is_notif_sent'             => true,
            'is_active_user'            => true
        );

        $month1 = $this->repository->calculatePremiumMonthly($invoice, $data, 0, 0, 1);
        $month2 = $this->repository->calculatePremiumMonthly($invoice2, $data, 1, 2, 3);

        $this->assertEquals($data['old_total_monthly_premium'] * 2, $month1);
        $this->assertEquals($data['old_total_monthly_premium'], $month2);
    }

    public function testGetDetailInvoice()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create([
            'is_owner'  => 'true'
        ]);
        $user2 = factory(User::class)->create();
        $userGp4 = factory(PremiumPlusUser::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'phone_number'  => $user->phone_number
        ]);
        $invoices = factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id'  => $userGp4->id,
            'amount'                => 1440000,
            'status'                => PremiumPlusInvoice::INVOICE_STATUS_UNPAID
        ]);

        $result = $this->repository->getDetail($user, $invoices->shortlink);
        $result2 = $this->repository->getDetail($user2, $invoices->shortlink);
        $this->assertEquals("Rp1.440.000", $result['invoice']['total_month_str']);
        $this->assertEquals(false, $result2['status']);
        $this->assertEquals('Invoice tidak ditemukan', $result2['message']);
    }

    public function testGetDetailExpiredInvoice()
    {
        $mockDate = Carbon::create(2020, 5, 4);
        Carbon::setTestNow($mockDate);

        $now = Carbon::now();

        $room = factory(Room::class)->create();
        $user = factory(User::class)->create([
            'is_owner'  => 'true'
        ]);
        $userGp4 = factory(PremiumPlusUser::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'phone_number'  => $user->phone_number
        ]);
        $invoices = factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id'  => $userGp4->id,
            'amount'                => 1440000,
            'due_date'              => $now->copy()->subDays(2),
            'status'                => PremiumPlusInvoice::INVOICE_STATUS_UNPAID
        ]);

        $result = $this->repository->getDetail($user, $invoices->shortlink);
        $this->assertEquals("Rp1.440.000", $result['invoice']['total_month_str']);
        $this->assertEquals(PremiumPlusInvoice::INVOICE_STATUS_EXPIRED, $result['invoice']['status']);
    }

    public function testGetDetailInvoiceShouldFail()
    {
        $user = factory(User::class)->create();

        $result = $this->repository->getDetail($user, 0);

        $this->assertFalse($result['status']); // Invoice ID not found
    }

    public function testMidtransNotificationPaymentNotfound()
    {
        $payment = factory(Payment::class)->create();
        $result = $this->repository->midtransNotification($payment, []);
        $this->assertFalse($result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationSettlementStatus()
    {
        $orderId = 98434;
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_UNPAID,
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);
        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive('status')
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::MIDTRANS_STATUS_SETTLEMENT,
                        "payment_type" => "bank_transfer",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);
        $result = $this->repository->midtransNotification($payment, ['order_id' => $orderId]);

        $this->assertTrue($result);
    }

    public function testUpdateInvoiceWhenReducedActivePeriod()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create([
            'is_owner'  => 'true'
        ]);
        $userGP4 = factory(PremiumPlusUser::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'phone_number'  => $user->phone_number,
            'active_period' => 3
        ]);

        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-1'
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-2'
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-3'
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-4'
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-5'
        ]);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($userGP4->id);

        $this->assertEquals(5, count($invoices));

        $oldGuaranteedRoom = $userGP4->guaranteed_room;
        $oldTotalMonthlyPremium = $userGP4->total_monthly_premium;

        $userGP4->active_period = 2;
        $userGP4->save();

        $data = [
            'total_monthly_premium' => $userGP4->total_monthly_premium,
            'premium_plus_user_id'  => $userGP4->id,
            'old_guaranteed_room'   => $oldGuaranteedRoom,
            'old_total_monthly_premium' => $oldTotalMonthlyPremium,
            'activation_date'       => $userGP4->activation_date,
            'is_active_user'        => $userGP4->isActive(),
            'is_notif_sent'         => $userGP4->isInvoiceSend(),
            'guaranteed_room'       => $userGP4->guaranteed_room,
            'active_period'         => $userGP4->active_period
        ];

        $this->repository->save($userGP4, $data);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($userGP4->id);

        $this->assertEquals(2, count($invoices));
    }

    public function testUpdateInvoiceWhenIncreaseActivePeriod()
    {
        $room = factory(Room::class)->create();
        $user = factory(User::class)->create([
            'is_owner'  => 'true'
        ]);
        $userGP4 = factory(PremiumPlusUser::class)->create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'phone_number'  => $user->phone_number,
            'active_period' => 3
        ]);

        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-1'
        ]);
        factory(PremiumPlusInvoice::class)->create([
            'premium_plus_user_id' => $userGP4->id,
            'name'                 => 'Pembayaran bulan ke-2'
        ]);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($userGP4->id);

        $this->assertEquals(2, count($invoices));

        $oldGuaranteedRoom = $userGP4->guaranteed_room;
        $oldTotalMonthlyPremium = $userGP4->total_monthly_premium;

        $userGP4->active_period = 5;
        $userGP4->save();

        $data = [
            'total_monthly_premium' => $userGP4->total_monthly_premium,
            'premium_plus_user_id'  => $userGP4->id,
            'old_guaranteed_room'   => $oldGuaranteedRoom,
            'old_total_monthly_premium' => $oldTotalMonthlyPremium,
            'activation_date'       => $userGP4->activation_date,
            'is_active_user'        => $userGP4->isActive(),
            'is_notif_sent'         => $userGP4->isInvoiceSend(),
            'guaranteed_room'       => $userGP4->guaranteed_room,
            'active_period'         => $userGP4->active_period
        ];

        $this->repository->save($userGP4, $data);

        $invoices = $this->repository->getInvoicesByPremiumPlusUser($userGP4->id);

        $this->assertEquals(5, count($invoices));
    }

    public function testGenerateShortlinkShouldSuccess()
    {
        $firstShortlink = 'abcde';
        $secondShortlink = 'fghij';
        Str::shouldReceive('random')->andReturn($firstShortlink, $secondShortlink);

        factory(PremiumPlusInvoice::class)->create([
            'shortlink' => $firstShortlink
        ]);
        $responseData = $this->repository->generateShortlink();
        $this->assertEquals(5, strlen($responseData));
        $this->assertEquals($secondShortlink, $responseData);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationOnPaidTrx()
    {
        $orderId = 98392;
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_PAID
        ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);

        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::TRANSACTION_STATUS_SUCCESS,
                        "payment_type" => "bank_transfer",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);

        $paymentStatus = $this->repository->midtransNotification($payment, ['order_id' => $orderId]);
        $this->assertEquals($paymentStatus->id, $payment->id);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationDenyTrx()
    {
        $orderId = 98392;
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_UNPAID,
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);

        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::MIDTRANS_STATUS_DENY,
                        "payment_type" => "bank_transfer",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);

        $paymentStatus = $this->repository->midtransNotification($payment, ['order_id' => $orderId]);
        $this->assertTrue($paymentStatus);
    }
    
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationOnPendingTrx()
    {
        $orderId = 98392;
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_UNPAID,
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);

        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::TRANSACTION_STATUS_PENDING,
                        "payment_type" => "bank_transfer",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);
        $data = [
            'order_id' => $orderId,
            'va_numbers' => true,
            'payment_type' => 'bank_transfer',
            'biller_code' => "3456"
        ];

        $paymentStatus = $this->repository->midtransNotification($payment, $data);
        $this->assertTrue($paymentStatus);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationOnPendingTrxWithCreditCard()
    {
        $orderId = 98392;
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_UNPAID,
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);

        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::MIDTRANS_STATUS_CAPTURE,
                        "payment_type" => "credit_card",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);

        $data = [
            'order_id' => $orderId,
            'va_numbers' => false,
            'payment_type' => 'credit_card',
            'biller_code' => "3456"
        ];

        $paymentStatus = $this->repository->midtransNotification($payment, $data);
        $this->assertTrue($paymentStatus);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testMidtransNotificationOnPendingTrxWithGoPay()
    {
        $orderId = 98392;
        $premiumPlusUser = factory(PremiumPlusUser::class)->create();
        $premiumPlusInvoice = factory(PremiumPlusInvoice::class)->create([
            'status' => PremiumPlusInvoice::INVOICE_STATUS_UNPAID,
            'premium_plus_user_id' => $premiumPlusUser->id
        ]);

        $payment = factory(Payment::class)->create([
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'order_id' => $orderId
        ]);

        $midtransMock = $this->mockAlternatively("overload:App\Veritrans\Veritrans");
        $midtransMock->shouldReceive("status")
                    ->with($orderId)
                    ->andReturn((object) [
                        "transaction_status" => Payment::MIDTRANS_STATUS_PENDING,
                        "payment_type" => "gopay",
                        "order_id" => $orderId,
                        "gross_amount" => 12000,
                        "fraud_status" => "accept",
                        "bill_key" => 1234,
                        "biller_code" => "3456"
                    ]);

        $data = [
            'order_id' => $orderId,
            'payment_type' => 'gopay',
            'biller_code' => "3456"
        ];

        $paymentStatus = $this->repository->midtransNotification($payment, $data);
        $this->assertTrue($paymentStatus);
    }
}