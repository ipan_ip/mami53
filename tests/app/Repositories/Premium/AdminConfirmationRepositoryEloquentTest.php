<?php

namespace App\Repositories\Premium;
use App;
use App\Test\MamiKosTestCase;
use App\Repositories\Premium\AdminConfirmationRepositoryEloquent;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\AccountConfirmation;
use App\User;
use App\Entities\Premium\BalanceRequest;
use Mockery;
use App\Notifications\Premium\TopUpConfirmation;
use Illuminate\Support\Facades\Notification;

class AdminConfirmationRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = App::make(AdminConfirmationRepositoryEloquent::class);
    }

    public function testPremiumBookingPackageAlreadyPurchasedSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);

        $bookingPackage = factory(PremiumPackage::class)->create([
            "total_day" => 30
        ]);

        $premiumRequestBooking = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => $bookingPackage->id,
        ]);

        $otherPremiumRequest = factory(PremiumRequest::class)->create([
            "status" => "1",
            "user_id" => $user->id,
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "total_day" => 10
            ])
        ]);

        factory(AccountConfirmation::class)->create([
            "premium_request_id" => $otherPremiumRequest->id,
            "transfer_date" => date('Y-m-d', strtotime("20120-04-15 -30 days"))
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "total_day" => 20
            ])->id
        ]);

        $result = $this->repository->premiumBookingPackageAlreadyPurchased($bookingPackage, $premiumRequest);
        $dateOnwerLimitExpected = date('Y-m-d', strtotime(date("Y-m-d")." +20 days"));
        $this->assertEquals($dateOnwerLimitExpected, $result);
    }

    public function testSetConfirmationSuccess()
    {
        $premiumConfirmation = factory(AccountConfirmation::class)->create([
            "is_confirm" => "0"
        ]);
                                    
        $data = [
            "is_confirm" => "1",
            "bank_name" => "Bank BL",
            "account_name" => "PT BL",
            "total" => 1000,
            "transfer_date" => date("Y-m-d")
        ];

        $result = $this->repository->setConfirmation($premiumConfirmation, $data);
        $this->assertEquals($premiumConfirmation->id, $result->id);
    }

    public function testSetConfirmationWithPeId()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "request_user_id" => null
        ]);

        $premiumConfirmation = factory(AccountConfirmation::class)->create([
            "premium_request_id" => $premiumRequest->id,
            "is_confirm" => "1"
        ]);
                                    
        $data = [
            "is_confirm" => "1",
            "bank_name" => "Bank BL",
            "account_name" => "PT BL",
            "total" => 1000,
            "transfer_date" => date("Y-m-d"),
            "pe_id" => 4
        ];

        $result = $this->repository->setConfirmation($premiumConfirmation, $data);
        $premiumRequest = PremiumRequest::find($premiumRequest->id);
        $this->assertEquals(4, $data['pe_id']);
    }

    public function testUpdatePremiumSaldoSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "expired_date" => null,
            "view" => 1000,
        ]);
        
        $topUpRequest = factory(BalanceRequest::class)->create([
            "premium_request_id" => $premiumRequest->id,
            "view" => 500,
        ]);

        $result = $this->repository->updateSaldo($topUpRequest);
        $this->assertTrue($result);
    }

    public function testPremiumTopUpFailed()
    {
        $result = $this->repository->premiumTopUp(null);
        $this->assertFalse($result);
    }

    public function testPremiumTopUpSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "expired_date" => null,
            "view" => 1000,
        ]);
        
        $topUpRequest = factory(BalanceRequest::class)->create([
            "premium_request_id" => $premiumRequest->id,
            "view" => 500,
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        $result = $this->repository->premiumTopUp($topUpRequest);
        $this->assertTrue($result);
    }

    public function testExtentDueDateUserSuccess()
    {
        $package = factory(PremiumPackage::class)->create([
            "view" => 100,
            "total_day" => 30
        ]);
        
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "status" => "1",
            "expired_date" => null,
            "view" => 1000,
            "premium_package_id" => $package->id,
        ]);

        $result = $this->repository->extendDueDateUser($premiumRequest);
        $this->assertEquals($user->id, $result->id);
    }

    public function testConfirmationForTopUpSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);
        
        $package = factory(PremiumPackage::class)->create([
            "for" => "balance",
            "view" => 1000
        ]);

        $topUpRequest = factory(BalanceRequest::class)->create([
            "price" => 1000,
            "status" => 1,
            "view" => 1000,
            "premium_package_id" => $package->id,
            "premium_request_id" => factory(PremiumRequest::class)->create([
                "user_id" => $user->id,
                "view" => 1000,
                "used" => 0,
                "premium_package_id" => $package->id,
                "allocated" => 0,
                "status" => "1"
            ])
        ]);

        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => $user->id,
            "premium_request_id" => 0,
            "view_balance_request_id" => $topUpRequest->id
        ]);

        Notification::fake();
        Notification::assertNothingSent();
        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $result = $this->repository->premiumConfirmation($confirmation);
        $this->assertTrue($result);
    }

    public function testConfirmationForTopUpBalanceNotFound()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);
        
        $package = factory(PremiumPackage::class)->create([
            "for" => "balance",
            "view" => 1000
        ]);

        $topUpRequest = factory(BalanceRequest::class)->create([
            "price" => 1000,
            "status" => 1,
            "view" => 1000,
            "premium_package_id" => $package->id,
            "premium_request_id" => factory(PremiumRequest::class)->create([
                "user_id" => $user->id,
                "view" => 1000,
                "used" => 0,
                "premium_package_id" => $package->id,
                "allocated" => 0,
                "status" => "1"
            ]),
            "deleted_at" => date('Y-m-d H:i:s')
        ]);

        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => $user->id,
            "premium_request_id" => 0,
            "view_balance_request_id" => $topUpRequest->id
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $result = $this->repository->premiumConfirmation($confirmation);
        $this->assertFalse($result);
    }

    public function testPremiumRequestAlreadyConfirmed()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date("Y-m-d")
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "user_id" => $user->id,
            "view" => 1000,
            "used" => 0,
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "view" => 1000
            ]),
            "allocated" => 0,
            "status" => "1"
        ]);

        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => $user->id,
            "is_confirm" => "1",
            "premium_request_id" => $premiumRequest->id,
            "view_balance_request_id" => 0
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $result = $this->repository->premiumConfirmation($confirmation);
        $this->assertFalse($result);
    }

    public function testPremiumRequestConfirmationSuccess()
    {
        $user = factory(User::class)->create([
            "date_owner_limit" => date('Y-m-d')
        ]);
        $premiumPackage = factory(PremiumPackage::class)->create([
            "total_day" => 30,
            "for" => "package"
        ]);

        $confirmation = factory(AccountConfirmation::class)->create([
            "user_id" => $user->id,
            "is_confirm" => "0",
            "premium_request_id" => factory(PremiumRequest::class)->create([
                "user_id" => $user->id,
                "view" => 1000,
                "used" => 0,
                "premium_package_id" => $premiumPackage->id,
                "allocated" => 0,
                "status" => "0"
            ]),
            "view_balance_request_id" => 0
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        $moengageMock = $this->mockAlternatively('App\Channel\MoEngage\MoEngageEventDataApi');
        $moengageMock->shouldReceive('reportPackagePurchaseConfirmed');

        $result = $this->repository->premiumConfirmation($confirmation);
        $this->assertTrue($result);
    }

    public function testUpdatePremiumExecutiveUser()
    {
        $user = factory(User::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create([
            'request_user_id' => null
        ]);

        $result = $this->repository->updatePremiumExecutiveUser($premiumRequest, $user->id);
        $this->assertEquals($user->id, $result->request_user_id);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        Mockery::close();
    }

}