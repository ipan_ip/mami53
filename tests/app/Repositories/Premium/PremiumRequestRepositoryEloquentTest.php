<?php

namespace App\Repositories;

use App;

use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\PremiumRequestRepository;
use App\Test\MamiKosTestCase;
use App\User;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;

class PremiumRequestRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $premiumRequestRepo;
    protected $userID;
    const TESTING_USER_ID = 10;

    public function setUp() : void
    {
        parent::setUp();

        $this->premiumRequestRepo = App::make(PremiumRequestRepository::class);
        $this->userID = $this::TESTING_USER_ID;
        // seeding 2 premium requests
        factory(PremiumRequest::class, 2)->create([
            'user_id' => $this::TESTING_USER_ID
        ]);
        // seeding 1 expired premium request
        factory(PremiumRequest::class)->create([
            'user_id' => $this::TESTING_USER_ID,
            'expired_status' => 'true'
        ]);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    /** @test */
    public function it_gets_all_user_paid_premium_requests()
    {
        // seeding 1 more premium request
        factory(PremiumRequest::class)->create([
            'user_id' => $this::TESTING_USER_ID
        ]);

        $premiumRequests = $this->premiumRequestRepo->getUserPaid($this->userID);
        // assert expected row counts
        $this->assertEquals(count($premiumRequests), 3);
        // assert all row doesn't have true expired_status
        $allExpiredStatus = $premiumRequests->pluck('expired_status')->values()->all();
        $this->assertFalse(in_array('true', $allExpiredStatus));
    }

    /** @test */
    public function it_gets_user_paid_premium_request_limit_1()
    {
        // seeding 1 more premium request
        $latestPremiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $this::TESTING_USER_ID,
        ]);

        $premiumRequests = $this->premiumRequestRepo->getUserPaid($this->userID, 1);
        // assert expected row counts
        $this->assertEquals(count($premiumRequests), 1);
        // assert all row doesn't have true expired_status
        $allExpiredStatus = $premiumRequests->pluck('expired_status')->values()->all();
        $this->assertFalse(in_array('true', $allExpiredStatus));
        // assert correct latest premium request
        $this->assertEquals($latestPremiumRequest->id, $premiumRequests[0]->id);
    }

    public function testSavePremiumPackageRequestSuccess()
    {
        $data = [
            'user_id' => 1,
            'total' => 1200,
            'pe_id' => 2
        ];
        
        $package = factory(PremiumPackage::class)->create([
            'id' => 1959,
            'view' => 1200,
            'price' => 1200,
            'sale_price' => 0
        ]);

        $result = $this->premiumRequestRepo->savePremiumPackageRequest($data, $package);
        
        $expectedTotal = 1200;
        $this->assertEquals($expectedTotal, $result->total);
    }

    public function testSavePremiumTopupRequestSuccess()
    {
        $premiumPackage = factory(PremiumPackage::class)->create([
            'id' => 1995,
            'view' => 1200,
            'price' => 1200,
            'sale_price' => 0
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'id' => 1995,
            'user_id' => 1995,
            'premium_package_id' => $premiumPackage->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_SUCCESS
        ]);

        $request = $this->premiumRequestRepo->savePremiumBalanceRequest($premiumPackage, $premiumRequest);
        $this->assertTrue($request instanceof BalanceRequest);
    }

    public function testGetUnpaidPremiumRequest()
    {
        $expiredDate = date('Y-m-d', strtotime('+1 day'));

        factory(PremiumRequest::class)->create([
            'expired_status' => PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED,
            'expired_date' => $expiredDate
        ]);

        $premiumRequest = $this->premiumRequestRepo->getUnpaidByExpiredDate($expiredDate);

        $this->assertEquals(1, $premiumRequest->count());
    }
}
