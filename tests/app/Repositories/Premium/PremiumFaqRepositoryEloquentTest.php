<?php

namespace App\Repositories;

use App;
use App\Entities\Premium\PremiumFaq;
use App\Repositories\Premium\PremiumFaqRepository;
use App\Test\MamiKosTestCase;

class PremiumFaqRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $premiumFaqRepo;
    protected $premiumFaq;
    protected $faqData;

    public function setUp() : void
    {
        parent::setUp();
        $this->faqData = [
            'question' => 'question',
            'answer' => 'answer',
            'is_active' => 1,
            'id' => 1
        ];

        $this->premiumFaqRepo = App::make(PremiumFaqRepository::class);
        $this->premiumFaq = factory(PremiumFaq::class)->create($this->faqData);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    /** @test */
    public function get_premium_faq_list_sucess()
    {
        $result = $this->premiumFaqRepo->getPremiumFaq(20);
        $this->assertEquals(1, $result['total']);
        $this->assertEquals(1, $result['page']);
        $this->assertEquals(false, $result['has-more']);
    }

    /** @test */
    public function add_new_premium_faq_success()
    {
        $faq = $this->premiumFaqRepo->store($this->faqData);
        $this->assertEquals($faq->id, $faq->id);
        $this->assertEquals(1, $faq->is_active);
    }

    /** @test */
    public function edit_premium_faq_success()
    {
        $editFaq = $this->premiumFaqRepo->edit($this->faqData, $this->premiumFaq->id);
        $this->assertEquals($editFaq->id, $editFaq->id);
    }

    /** @test */
    public function delete_premium_faq_success()
    {
        $premiumFaq = $this->premiumFaqRepo->destroy($this->premiumFaq->id);
        $this->assertEquals($premiumFaq->id, $premiumFaq->id);
    }

    /** @test */
    public function delete_premium_faq_failed()
    {
        $premiumFaq = $this->premiumFaqRepo->destroy(0);
        $this->assertEquals(null, $premiumFaq);
    }
}