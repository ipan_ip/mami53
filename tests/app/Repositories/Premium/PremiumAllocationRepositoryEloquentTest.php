<?php

namespace App\Repositories;

use App;
use App\Repositories\Premium\PremiumAllocationRepository;
use App\Test\MamiKosTestCase;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use Illuminate\Support\Facades\Notification;
use Exception;

class PremiumAllocationRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $repository;

    public function setUp() : void
    {
        parent::setUp();
        $this->repository = App::make(PremiumAllocationRepository::class);
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testModel()
    {
        $this->assertEquals(ViewPromote::class, $this->repository->model());
    }

    public function testGetPremiumPromoteSuccess()
    {
        $room = factory(Room::class)->create();
        $premiumRequest = factory(PremiumRequest::class)->create();
        $premiumPromote = factory(ViewPromote::class)->create([
            "designer_id" => $room,
            "premium_request_id" => $premiumRequest->id
        ]);

        $result = $this->repository->getPremiumPromote($room->id, $premiumRequest);
        $this->assertTrue($result instanceOf ViewPromote);
    }

    public function testGetPremiumPromoteNull()
    {
        $premiumRequest = factory(PremiumRequest::class)->make();
        $result = $this->repository->getPremiumPromote(1002, $premiumRequest);
        $this->assertEquals(null, $result);
    }

    public function testDeactivatePremiumAllocationSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "allocated" => 500,
            "used" => 200,
            "view" => 500
        ]);

        $premiumPromote = factory(ViewPromote::class)->create([
            "is_active" => 1,
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "true"
            ]),
            "premium_request_id" => $premiumRequest->id,
            "total" => 500,
            "used" => 200,
            "history" => 0
        ]);

        $result = $this->repository->deactivatePremiumPromote($premiumPromote);

        $this->assertEquals(200, $result->total);
        $this->assertEquals(200, $result->history);
        $this->assertEquals(0, $result->is_active);
    }

    public function testDeactivatePremiumAllocationWhenActiveFalseShouldSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "allocated" => 500,
            "used" => 200,
            "view" => 500
        ]);

        $premiumPromote = factory(ViewPromote::class)->create([
            "is_active" => 0,
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "true"
            ]),
            "premium_request_id" => $premiumRequest->id,
            "total" => 500,
            "used" => 200,
            "history" => 0
        ]);

        $result = $this->repository->deactivatePremiumPromote($premiumPromote);

        $this->assertEquals($result, $premiumPromote);
    }

    public function testGetAvailableSaldoSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "view" => 1000,
            "allocated" => 100,
        ]);
        
        $result = $this->repository->availableBalance($premiumRequest);
        $this->assertEquals(900, $result);
    }

    public function testGetAvailableSaldoShould0()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            "view" => 1000,
            "allocated" => 1000,
        ]);
        
        $result = $this->repository->availableBalance($premiumRequest);
        $this->assertEquals(0, $result);
    }

    public function testSavePremiumPromoteHistory()
    {
        $premiumPromote = factory(ViewPromote::class)->create([
            "session_id" => 3,
            "total" => 1000,
            "for" => "click"
        ]);

        $result = $this->repository->saveAllocationHistory($premiumPromote);
        $this->assertEquals($premiumPromote->total, $result->start_view);
    }

    public function testActivateRoomStatusPremium()
    {
        $room = factory(Room::class)->create();

        $result = $this->repository->activateRoomStatus($room);
        $this->assertEquals('true', $result->is_promoted);
    }

    public function testPremiumAllocationSuccess()
    {
        $data = [
            "allocation" => 1000
        ];

        $premiumRequest = factory(PremiumRequest::class)->create([
            "view" => 1000,
            "user_id" => factory(User::class)->create(),
            "used" => 0,
            "allocated" => 0,
        ]);

        $room = factory(Room::class)->create([
            "is_promoted" => "false"
        ]);

        $premiumPromote = factory(ViewPromote::class)->create([
            "designer_id" => $room->id,
            "premium_request_id" => $premiumRequest->id,
            "total" => 0,
            "used" => 0,
            "history" => 0,
            "session_id" => 1,
            "is_active" => 0
        ]);

        Notification::fake();
        Notification::assertNothingSent();
        
        $result = $this->repository->allocation($premiumPromote, $data);
        
        //Allocation total
        $this->assertEquals(1000, $result->premium_request->allocated);
        
        //Premium room status
        $this->assertEquals('true', $result->room->is_promoted);

        //Premium allocation status
        $this->assertEquals(1000, $result->total);
    }

    public function testFirstAllocationSuccess()
    {
        $data = [
            "allocation" => 1000,
        ];

        $premiumRequest = factory(PremiumRequest::class)->create([
            "view" => 1000,
            "used" => 0,
            "allocated" => 0,
        ]);

        $room = factory(Room::class)->create([
            "is_promoted" => "false"
        ]);

        $result = $this->repository->firstAllocation($room, $premiumRequest, $data);
        
        //Allocation total
        $this->assertEquals(1000, $result->premium_request->allocated);
        
        //Premium room status
        $this->assertEquals('true', $result->room->is_promoted);

        //Premium allocation status
        $this->assertEquals(1000, $result->total);
    }

    public function testPremiumAllocationWhenRequestNotFound()
    {
        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create(),
            "user_id" => factory(User::class)->create(),
        ]);

        $result = $this->repository->premiumAllocation([], $roomOwner);
        $this->assertFalse($result['status']);
    }

    public function testPremiumAllocationWhenAllocationBiggerThanView()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create(),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 1000,
            "allocated" => 2000,
            "user_id" => $user->id,
        ]);

        $result = $this->repository->premiumAllocation([], $roomOwner);
        $this->assertFalse($result['status']);
    }

    public function testPremiumAllocationForFirstAllocationSuccess()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 6000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);
        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertTrue($result['status']);
    }

    public function testPremiumAllocationForFirstAllocationSaldo()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 6000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);
        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertTrue($result['status']);
    }

    public function testPremiumAllocationSaldoOnAlreadyAllocated()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 6000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);

        factory(ViewPromote::class)->create([
            "total" => 1000,
            "used" => 0,
            "history" => 1000,
            "is_active" => 1,
        ]);

        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertTrue($result['status']);
    }

    public function testPremiumAllocationSaldoNotAvailableForFirstAllocation()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 1000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);

        $result = $this->repository->premiumAllocation(["allocation" => 1500], $roomOwner);
        $this->assertFalse($result['status']);
    }

    public function testPremiumAllocationSaldoNotAvailableForExistData()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 7000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);

        factory(ViewPromote::class)->create([
            "total" => 1000,
            "used" => 0,
            "history" => 1000,
            "is_active" => 0,
        ]);

        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertTrue($result['status']);
    }

    public function testPremiumAllocationWhenPromoteIsActive()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 7000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);

        factory(ViewPromote::class)->create([
            "total" => 1000,
            "daily_budget" => 0,
            "used" => 200,
            "history" => 700,
            "is_active" => 0,
        ]);

        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertTrue($result['status']);
    }

    public function testDeactivateRoomStatus()
    {
        $room = factory(Room::class)->create([
            "is_promoted" => "true",
        ]);

        $result = $this->repository->deactivateRoomStatus($room);

        $this->assertEquals('false', $result->is_promoted);
    }

    public function testAllocationWhenBalanceLessThanAllocationMinimum()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 4000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);
        $result = $this->repository->premiumAllocation(["allocation" => 5000], $roomOwner);
        $this->assertFalse($result['status']);
    }

    public function testAllocationWhenAllocatedBalanceLessThanAllocationMinimum()
    {
        $user = factory(User::class)->create();

        $roomOwner = factory(RoomOwner::class)->create([
            "designer_id" => factory(Room::class)->create([
                "is_promoted" => "false"
            ]),
            "user_id" => $user->id,
        ]);

        factory(PremiumRequest::class)->create([
            "status" => '1',
            "view" => 6000,
            "used" => 0,
            "allocated" => 0,
            "user_id" => $user->id,
        ]);
        $result = $this->repository->premiumAllocation(["allocation" => 4990], $roomOwner);
        $this->assertFalse($result['status']);
    }

}