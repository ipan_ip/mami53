<?php

namespace App\Repositories;

use App;

use App\Entities\Premium\PremiumPackage;
use App\Repositories\Premium\PremiumPackageRepository;
use App\Test\MamiKosTestCase;

class PremiumPackageRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $premiumPackageRepo;
    protected $date;

    public function setUp() : void
    {
        parent::setUp();

        $this->premiumPackageRepo = App::make(PremiumPackageRepository::class);
        $this->date = date('Y-m-d');
        // seeding normal packages
        factory(PremiumPackage::class, 2)->create();
        // seeding trial package
        factory(PremiumPackage::class)->states('trial')->create();
        // seeding extension package
        factory(PremiumPackage::class)->states('extension')->create();
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    private function assertTrialPackage($premiumPackages, bool $exist = true)
    {
        $premiumPackageTypes = array_unique($premiumPackages->pluck('for')->unique()->values()->all());
        if ($exist) {
            return $this->assertTrue(in_array('trial', $premiumPackageTypes));
        }
        return $this->assertFalse(in_array('trial', $premiumPackageTypes));
    }

    private function assertExtensionPackage($premiumPackages, bool $exist = true)
    {
        $premiumPackageViews = array_unique($premiumPackages->pluck('view')->unique()->values()->all());
        if ($exist) {
            return $this->assertTrue(in_array(0, $premiumPackageViews));
        }
        return $this->assertFalse(in_array(0, $premiumPackageViews));
    }

    /** @test */
    public function it_shows_normal_packages_only()
    {
        $premiumPackages = $this->premiumPackageRepo->getPackages($this->date, false, false);
        // assert no trial package
        $this->assertTrialPackage($premiumPackages, false);
        // assert no extension package
        $this->assertExtensionPackage($premiumPackages, false);
    }

    /** @test */
    public function it_shows_packages_and_trial()
    {
        $premiumPackages = $this->premiumPackageRepo->getPackages($this->date, true, false);
        // assert trial package
        $this->assertTrialPackage($premiumPackages, true);
        // assert no extension package
        $this->assertExtensionPackage($premiumPackages, false);
    }

    /** @test */
    public function it_shows_packages_and_extension()
    {
        $premiumPackages = $this->premiumPackageRepo->getPackages($this->date, false, true);
        // assert no trial package
        $this->assertTrialPackage($premiumPackages, false);
        // assert no extension package
        $this->assertExtensionPackage($premiumPackages, true);
    }

     /** @test */
     public function it_shows_packages_and_trial_and_extension()
     {
         $premiumPackages = $this->premiumPackageRepo->getPackages($this->date, true, true);
         $premiumPackageTypes = array_unique($premiumPackages->pluck('for')->unique()->values()->all());
         // assert trial package
         $this->assertTrialPackage($premiumPackages, true);
         // assert extension package
         $this->assertExtensionPackage($premiumPackages, true);
     }
}
