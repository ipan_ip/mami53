<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Test\MamiKosTestCase;
use Illuminate\Contracts\Pagination\Paginator;

class PollingOptionRepositoryEloquentTest extends MamiKosTestCase
{
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = app(PollingOptionRepositoryEloquent::class);
    }

    public function testGetOptionPaginated_CanFilterable_ShouldSuccess()
    {
        $option = factory(PollingQuestionOption::class)->create([
            'option' => 'option-asdf',
            'sequence' => 1,
        ]);
        factory(PollingQuestionOption::class, 3)->create();

        $paginated = $this->class->getOptionPaginated(10, [
            'keyword' => 'asdf',
            'status' => 1,
            'question' => $option->question_id
        ]);

        $this->assertInstanceOf(Paginator::class, $paginated);
        $this->assertEquals(1, $paginated->total());
    }

    public function testCreatePollingOption_ShouldSuccess()
    {
        $formData = [
            'question_id' => 1,
            'option' => 'option-dummy',
            'sequence' => 1
        ];

        $this->class->createOption($formData);
        
        $this->assertDatabaseHas((new PollingQuestionOption)->getTable(), $formData);
    }

    public function testUpdatePollingOption_ShouldSuccess()
    {
        $formData = [
            'question_id' => 1,
            'option' => 'option-dummy',
            'sequence' => 1
        ];
        $option = factory(PollingQuestionOption::class)->create([
            'question_id' => 99,
            'option' => 'asdf',
            'sequence' => 99,
        ]);

        $this->class->updateOption($option->id, $formData);
        
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'asdf']);
        $this->assertDatabaseHas((new PollingQuestionOption)->getTable(), $formData);
    }

    public function testDeletePollingOption_ShouldSuccess()
    {
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $option = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $this->class->delete($option->id);

        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
