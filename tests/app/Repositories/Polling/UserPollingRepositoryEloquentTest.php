<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\UserPolling;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Contracts\Pagination\Paginator;

class UserPollingRepositoryEloquentTest extends MamiKosTestCase
{
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = app(UserPollingRepositoryEloquent::class);
    }

    public function testGetUserPollingPaginated_CanFilterable_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $polling = factory(UserPolling::class)->create([
            'user_id' => $user->id,
            'answer' => 'oke gaes'
        ]);
        factory(UserPolling::class, 2)->create();

        $paginated = $this->class->getUserPaginated(10, [
            'keyword' => 'oke',
            'question' => $polling->question_id,
            'user' => $user->id
        ]);

        $this->assertInstanceOf(Paginator::class, $paginated);
        $this->assertEquals(1, $paginated->total());
    }
}
