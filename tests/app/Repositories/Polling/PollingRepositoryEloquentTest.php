<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Test\MamiKosTestCase;
use Illuminate\Contracts\Pagination\Paginator;

class PollingRepositoryEloquentTest extends MamiKosTestCase
{
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = app(PollingRepositoryEloquent::class);
    }

    public function testGetPollingPaginated_CanFilterable()
    {
        $polling = factory(Polling::class)->create([
            'key' => 'key-asdf',
            'is_active' => 1
        ]);
        factory(Polling::class, 3)->create();

        $paginated = $this->class->getPollingPaginated(10, [
            'keyword' => 'asdf',
            'status' => 1,
        ]);

        $this->assertInstanceOf(Paginator::class, $paginated);
        $this->assertEquals(1, $paginated->total());
    }

    public function testCreatePolling_ShouldSuccess()
    {
        $formData = [
            'key' => 'key-dummy',
            'descr' => 'descr-dummy',
            'is_active' => 1
        ];

        $this->class->createPolling($formData);
        
        $this->assertDatabaseHas((new Polling)->getTable(), $formData);
    }

    public function testUpdatePolling_ShouldSuccess()
    {
        $formData = [
            'key' => 'key-dummy',
            'descr' => 'descr-dummy',
            'is_active' => 1
        ];
        $polling = factory(Polling::class)->create([
            'key' => 'asdf',
            'descr' => 'lorem',
            'is_active' => 0,
        ]);

        $this->class->updatePolling($polling->id, $formData);
        
        $this->assertDatabaseMissing((new Polling)->getTable(), ['key' => 'asdf']);
        $this->assertDatabaseHas((new Polling)->getTable(), $formData);
    }

    public function testDeletePolling_ShouldSuccess()
    {
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $questionOption = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $this->class->deletePolling($polling->id);

        $this->assertDatabaseMissing((new Polling)->getTable(), ['key' => 'ini-key', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'ini-question', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
