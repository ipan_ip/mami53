<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Test\MamiKosTestCase;
use Illuminate\Contracts\Pagination\Paginator;

class PollingQuestionRepositoryEloquentTest extends MamiKosTestCase
{
    private $class;

    protected function setUp() : void
    {
        parent::setUp();

        $this->class = app(PollingQuestionRepositoryEloquent::class);
    }

    public function testGetQuestionPaginated_CanFilterable_ShouldSuccess()
    {
        $question = factory(PollingQuestion::class)->create([
            'question' => 'key-asdf',
            'is_active' => 1,
            'type' => PollingQuestion::TYPE_ALPHANUMERIC
        ]);
        factory(PollingQuestion::class, 3)->create();

        $paginated = $this->class->getQuestionPaginated(10, [
            'keyword' => 'asdf',
            'status' => 1,
            'type' => PollingQuestion::TYPE_ALPHANUMERIC,
            'polling' => $question->polling_id,
        ]);

        $this->assertInstanceOf(Paginator::class, $paginated);
        $this->assertEquals(1, $paginated->total());
    }

    public function testCreatePollingQuestion_ShouldSuccess()
    {
        $formData = [
            'polling_id' => 1,
            'question' => 'question-dummy',
            'type' => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => 1
        ];

        $this->class->createQuestion($formData);
        
        $this->assertDatabaseHas((new PollingQuestion)->getTable(), $formData);
    }

    public function testUpdatePollingQuestion_ShouldSuccess()
    {
        $formData = [
            'polling_id' => 1,
            'question' => 'question-dummy',
            'type' => PollingQuestion::TYPE_ALPHANUMERIC,
            'is_active' => 1
        ];
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => 99,
            'question' => 'asdf',
            'type' => PollingQuestion::TYPE_BOOLEAN,
            'is_active' => 0,
        ]);

        $this->class->updateQuestion($question->id, $formData);
        
        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'asdf']);
        $this->assertDatabaseHas((new PollingQuestion)->getTable(), $formData);
    }

    public function testDeletePollingQuestion_ShouldSuccess()
    {
        $polling = factory(Polling::class)->create([
            'key' => 'ini-key',
        ]);
        $question = factory(PollingQuestion::class)->create([
            'polling_id' => $polling->id,
            'question' => 'ini-question',
            'type' => 'selectionlist'
        ]);
        $questionOption = factory(PollingQuestionOption::class)->create([
            'question_id' => $question->id,
            'option' => 'ini-option'
        ]);

        $this->class->deleteQuestion($question->id);

        $this->assertDatabaseMissing((new PollingQuestion)->getTable(), ['question' => 'ini-question', 'deleted_at' => null]);
        $this->assertDatabaseMissing((new PollingQuestionOption)->getTable(), ['option' => 'ini-option', 'deleted_at' => null]);
    }
}
