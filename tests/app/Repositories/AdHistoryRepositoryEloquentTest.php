<?php

namespace App\Repositories;

use App;
use App\User;
use App\Entities\Premium\AdHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Repositories\Premium\AdHistoryRepository;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use StatsLib;

class AdHistoryRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $adHistoryRepo;
    protected $type;
    protected $clickPrice;
    protected $roomId;
    protected $date;

    public function setUp() : void
    {
        parent::setUp();

        $this->adHistoryRepo = App::make(AdHistoryRepository::class);
        $this->type = 'click';
        $this->clickPrice = 200;
        $this->roomId = 1;
        $this->date = date('Y-m-d');

        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        // Reset test carbon mock
        Carbon::setTestNow();
    }

    protected function getExistingAdHistory()
    {
        return AdHistory::where('designer_id', $this->roomId)
            ->where('type', $this->type)
            ->whereDate('date', $this->date)
            ->first();
    }

    /** @test */
    public function it_inserts_new_data_if_not_exists()
    {
        $output = $this->adHistoryRepo->insertOrUpdate($this->type, $this->clickPrice, $this->roomId, $this->date);
        $this->assertTrue($output);
        $adHistory = $this->getExistingAdHistory();
        $this->assertEquals(200, $adHistory->total);
        $this->assertEquals(1, $adHistory->total_click);
    }

    /** @test */
    public function it_updates_exising_data_if_exists()
    {
        factory(AdHistory::class)->create(
            [
                'designer_id' => $this->roomId,
                'total_click' => 4,
                'total' => 800
            ]
        );
        $output = $this->adHistoryRepo->insertOrUpdate($this->type, $this->clickPrice, $this->roomId, $this->date);
        $this->assertTrue($output);

        $adHistory = $this->getExistingAdHistory();
        $this->assertEquals(1000, $adHistory->total);
        $this->assertEquals(5, $adHistory->total_click);
    }

    public function testGetModel()
    {
        $model = $this->adHistoryRepo->model();
        $this->assertEquals(AdHistory::class, $model);
    }

    private function generateAdHistory(Room $room, $createdTime = null, $click = 1, $price = null)
    {
        $now = Carbon::now();
        $historyData = [
            'designer_id'   => $room->id,
            'type'          => AdHistory::TYPE_CLICK,
            'total_click'   => $click,
            'created_at'    => (is_null($createdTime)) ? $now : $createdTime,
            'updated_at'    => (is_null($createdTime)) ? $now : $createdTime,
        ];

        if (!is_null($price)) {
            $historyData['total'] = $price;
        }

        factory(AdHistory::class)->create($historyData);

        return true;
    }

    public function testGetClickStatsAllKosRangeToday()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $startDate = $now->copy()->startOfDay();
        $date23PM = $startDate->copy()->subHour();
        $date3AM = $startDate->copy()->addHours(3);
        $date345AM = $startDate->copy()->addHours(3)->addMinutes(45);
        $validateDate23PM = $date23PM->format('Y-m-d H');
        $validateDate3AM = $date3AM->format('Y-m-d H');
        $validateDate1AM = $startDate->copy()->addHour()->format('Y-m-d H');

        // It will counted
        $this->generateAdHistory($room, $date3AM, 4);
        $this->generateAdHistory($room, $date345AM, 4);
        $this->generateAdHistory($room, $date23PM, 4);
        $this->generateAdHistory($room, $date23PM->addMinutes(30), 4);

        // not counted
        $this->generateAdHistory($room, $startDate->copy()->subHours(2), 1);

        $statsOn12AM = $this->adHistoryRepo->getRoomsClickStatistic($user, null, null, $startDate);
        $statsOn1AM = $this->adHistoryRepo->getRoomsClickStatistic($user, null, null, $startDate->copy()->addHour());
        $stats = $this->adHistoryRepo->getRoomsClickStatistic($user, null);

        $currentHour = $now->hour;
        if ($currentHour > 3) {
            $key = array_search($validateDate3AM, array_column($stats, 'date'));
            $this->assertEquals(8, $stats[$key]['value']);
        }

        // it will show until current hour
        if ($currentHour <= 23) {
            $this->assertEquals($currentHour + 1, count($stats));
        }
        // if current hour is 12 AM then stats only show 2 statistics, on 11 PM - 00 AM
        $this->assertEquals(2, count($statsOn12AM));
        $this->assertEquals($validateDate23PM, $statsOn12AM[0]['date']);
        // if current hour is 1 AM then stats only show 2 statistics, on 00 AM - 01 AM
        $this->assertEquals(2, count($statsOn1AM));
        $this->assertEquals($validateDate1AM, $statsOn1AM[1]['date']);
    }

    public function testGetClickStatsAllKosRangeYesterday()
    {
        $now = Carbon::now();
        $yesterday = $now->copy()->subDay();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        $validateDate9AM = $yesterday->copy()->startOfDay()->addHours(9);

        // not counted
        $this->generateAdHistory($room);

        // It will counted
        $this->generateAdHistory($room, $validateDate9AM, 4);
        $this->generateAdHistory($room, $validateDate9AM->addMinutes(23), 4);

        $stats = $this->adHistoryRepo->getRoomsClickStatistic($user, StatsLib::RangeYesterday);

        $key = array_search($validateDate9AM->format('Y-m-d H'), array_column($stats, 'date'));
        $this->assertEquals(8, $stats[$key]['value']);
    }

    public function testGetClickStatsAllKosRange7Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdHistory($room);

        // It will counted
        $this->generateAdHistory($room, $now->copy()->subDays(2), 4);
        $this->generateAdHistory($room, $now->copy()->subDays(2)->startOfDay()->addHours(22), 4);

        $stats = $this->adHistoryRepo->getRoomsClickStatistic($user, StatsLib::Range7Days);

        $this->assertEquals(8, $stats[4]['value']);
    }

    public function testGetClickStatsAllKosRange30Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdHistory($room);

        // It will counted
        $this->generateAdHistory($room, $now->copy()->subDays(12), 4);
        $this->generateAdHistory($room, $now->copy()->subDays(12)->startOfDay()->addHours(3), 4);

        $stats = $this->adHistoryRepo->getRoomsClickStatistic($user, StatsLib::Range30Days);

        $this->assertEquals(8, $stats[17]['value']);
    }

    public function testGetClickStatsAllKosShouldReturnEmpty()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        // not counted
        $this->generateAdHistory($room);

        $stats = $this->adHistoryRepo->getRoomsClickStatistic($user, null);

        $this->assertEquals(0, count($stats));
    }

    public function testGetClickStatsAllKosSummary()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // It will counted
        $this->generateAdHistory($room, $now->subDays(12), 4);

        $stats = $this->adHistoryRepo->getRoomsClickStatisticSummary($user, StatsLib::Range30Days);

        $this->assertEquals(4, $stats['value']);   
    }

    public function testGetClickStatsAllKosSummaryShouldReturnEmpty()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        // It will counted
        $this->generateAdHistory($room, $now->subDays(12), 4);

        $stats = $this->adHistoryRepo->getRoomsClickStatisticSummary($user, StatsLib::Range30Days);

        $this->assertEquals(0, $stats['value']); 
    }

    public function testGetBurningBalanceAllKosRangeToday()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // It will counted
        $this->generateAdHistory($room, $now->copy()->startOfDay()->addHours(9), 4, 3600);

        // not counted
        $this->generateAdHistory($room, $now->copy()->subDays(2), 1, 900);

        $stats = $this->adHistoryRepo->getBurningBalanceStats($user, null);
        
        $currentHour = $now->hour;
        if ($currentHour == 9) {
            $this->assertEquals(3600, $stats[9]['value']);
        } else if ($currentHour < 2) {
            $this->assertEquals(2, count($stats));
        } else {
            $this->assertEquals($currentHour + 1, count($stats));
        }

    }

    public function testGetBurningBalanceAllKosRangeYesterday()
    {
        $now = Carbon::now();
        $yesterday = $now->copy()->subDay();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        $validateDate9AM = $yesterday->copy()->startOfDay()->addHours(9);

        // not counted
        $this->generateAdHistory($room, null, 1, 900);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 1,
            'total' => 900
        ]);

        // It will counted
        $this->generateAdHistory($room, $validateDate9AM, 4, 3600);

        $stats = $this->adHistoryRepo->getBurningBalanceStats($user, StatsLib::RangeYesterday);

        $key = array_search($validateDate9AM->format('Y-m-d H'), array_column($stats, 'date'));
        $this->assertEquals(3600, $stats[$key]['value']);
    }

    public function testGetBurningBalanceAllKosRange7Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdHistory($room, null, 1, 900);

        // It will counted
        $this->generateAdHistory($room, $now->subDays(2), 4, 3600);

        $stats = $this->adHistoryRepo->getBurningBalanceStats($user, StatsLib::Range7Days);

        $this->assertEquals(3600, $stats[4]['value']);
    }

    public function testGetBurningBalanceAllKosRange30Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $last12Days = $now->copy()->subDays(12);
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdHistory($room, null, 1, 900);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 1,
            'total' => 900
        ]);

        // It will counted
        $this->generateAdHistory($room, $last12Days, 4, 3600);

        $stats = $this->adHistoryRepo->getBurningBalanceStats($user, StatsLib::Range30Days);

        $this->assertEquals(3600, $stats[17]['value']);
    }

    public function testGetBurningBalanceAllKosShouldReturnEmpty()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        // not counted
        $this->generateAdHistory($room, null, 1, 900);
        factory(AdHistory::class)->create([
            'designer_id' => $room->id,
            'total_click' => 1,
            'total' => 900
        ]);

        $stats = $this->adHistoryRepo->getBurningBalanceStats($user, null);

        $this->assertEquals(0, count($stats));
    }

    public function testGetBurningBalanceSummary()
    {
        $now = Carbon::now();
        
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // It will counted
        $yesterday = $now->subHours(9);
        $adHistory = app(AdHistory::class);
        $adHistory->designer_id = $room->id;
        $adHistory->total_click = 4;
        $adHistory->total = 3600;
        $adHistory->created_at = $yesterday;
        $adHistory->updated_at = $yesterday;
        $adHistory->save();

        // It will counted
        $yesterday = $now->subHours(9);
        $adHistory = app(AdHistory::class);
        $adHistory->designer_id = $room->id;
        $adHistory->total_click = 4;
        $adHistory->total = 3600;
        $adHistory->created_at = $yesterday;
        $adHistory->updated_at = $yesterday;
        $adHistory->save();

        $stats = $this->adHistoryRepo->getBurningBalanceStatsSummary($user, StatsLib::Range30Days);

        $this->assertEquals(7200, $stats['value']);
    }

    public function testGetBurningBalanceSummaryShouldReturnEmpty()
    {
        $now = Carbon::now();
        
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        // It will counted
        $yesterday = $now->subHours(9);
        $adHistory = app(AdHistory::class);
        $adHistory->designer_id = $room->id;
        $adHistory->total_click = 4;
        $adHistory->total = 3600;
        $adHistory->created_at = $yesterday;
        $adHistory->updated_at = $yesterday;
        $adHistory->save();

        $stats = $this->adHistoryRepo->getBurningBalanceStatsSummary($user, StatsLib::Range30Days);

        $this->assertEquals(0, $stats['value']);
    }

    public function testGetAdsCostsStatsToday()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        $startDate = $now->copy()->startOfDay();
        $date23PM = $startDate->copy()->subHour();
        $date3AM = $startDate->copy()->addHours(3);
        $date345AM = $startDate->copy()->addHours(3)->addMinutes(45);
        $validateDate23PM = $date23PM->format('Y-m-d H');
        $validateDate3AM = $date3AM->format('Y-m-d H');
        $validateDate1AM = $startDate->copy()->addHour()->format('Y-m-d H');
        $currentHour = $now->hour;

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $date23PM, 1, 900);

        $this->generateAdHistory($room1, $date3AM, 1, 900);
        $this->generateAdHistory($room2, $date3AM, 1, 800);
        $this->generateAdHistory($room2, $date345AM, 1, 800);

        $this->generateAdHistory($room1, $startDate->copy()->addHours(2), 1, 900);
        $this->generateAdHistory($room2, $startDate->copy()->addHours(2), 1, 800);
        $this->generateAdHistory($room3, $startDate->copy()->addHours(2), 1, 1000);

        $stats = $this->adHistoryRepo->getAdsCostsStats($user, null);
        $statsOn12AM = $this->adHistoryRepo->getAdsCostsStats($user, null, null, $startDate);
        $statsOn1AM = $this->adHistoryRepo->getAdsCostsStats($user, null, null, $startDate->copy()->addHour());

        if ($currentHour > 3) {
            $key = array_search($validateDate3AM, array_column($stats, 'date'));
            $this->assertEquals(833, $stats[$key]['value']);
        }

        // it will show until current hour
        if ($currentHour <= 23) {
            $this->assertEquals($currentHour + 1, count($stats));
        }
        // if current hour is 12 AM then stats only show 2 statistics, on 11 PM - 00 AM
        $this->assertEquals(2, count($statsOn12AM));
        $this->assertEquals($validateDate23PM, $statsOn12AM[0]['date']);
        // if current hour is 1 AM then stats only show 2 statistics, on 00 AM - 01 AM
        $this->assertEquals(2, count($statsOn1AM));
        $this->assertEquals($validateDate1AM, $statsOn1AM[1]['date']);

    }

    public function testGetAdsCostsStatsYesterday()
    {
        $now = Carbon::now();
        $yesterday = $now->copy()->subDay();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();
        $time6AM = $yesterday->copy()->startOfDay()->addHours(6);
        $time2AM = $yesterday->copy()->startOfDay()->addHours(2);

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $time6AM, 1, 900);
        $this->generateAdHistory($room2, $time6AM, 1, 800);
        $this->generateAdHistory($room2, $time6AM, 1, 800);

        $this->generateAdHistory($room1, $time2AM, 1, 900);
        $this->generateAdHistory($room2, $time2AM, 1, 800);
        $this->generateAdHistory($room3, $time2AM, 1, 1000);

        $stats = $this->adHistoryRepo->getAdsCostsStats($user, StatsLib::RangeYesterday);

        $key6AM = array_search($time6AM->format('Y-m-d H'), array_column($stats, 'date'));
        $key2AM = array_search($time2AM->format('Y-m-d H'), array_column($stats, 'date'));
        $this->assertEquals(900, $stats[$key2AM]['value']);
        $this->assertEquals(833, $stats[$key6AM]['value']);
    }

    public function testGetAdsCostsStats7Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $threeDaysBehind = $now->copy()->subDays(3)->startOfDay();
        $fourDaysBehind = $now->copy()->subDays(4)->startOfDay();

        $this->generateAdHistory($room1, $threeDaysBehind->copy()->addHour(6), 1, 900);
        $this->generateAdHistory($room2, $threeDaysBehind->copy()->addHour(6), 1, 800);
        $this->generateAdHistory($room2, $threeDaysBehind->copy()->addHour(9), 1, 800);

        $this->generateAdHistory($room1, $fourDaysBehind->copy()->addHour(2), 1, 900);
        $this->generateAdHistory($room2, $fourDaysBehind->copy()->addHour(2), 1, 800);
        $this->generateAdHistory($room3, $fourDaysBehind->copy()->addHour(2), 1, 1000);

        $stats = $this->adHistoryRepo->getAdsCostsStats($user, StatsLib::Range7Days);

        $this->assertEquals(900, $stats[2]['value']);
        $this->assertEquals(833, $stats[3]['value']);
    }

    public function testGetAdsCostsStats30Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $now->copy()->subDays(23)->subHour(6), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(23)->subHour(6), 1, 800);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(9), 1, 800);

        $this->generateAdHistory($room1, $now->copy()->subDays(14)->subHour(2), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(14)->subHour(2), 1, 800);
        $this->generateAdHistory($room3, $now->copy()->subDays(14)->subHour(2), 1, 1000);

        $stats = $this->adHistoryRepo->getAdsCostsStats($user, StatsLib::Range30Days);

        $this->assertEquals(900, $stats[15]['value']);
        $this->assertEquals(850, $stats[6]['value']);
        $this->assertEquals(800, $stats[25]['value']);
    }

    public function testGetAdsCostsStatsReturnEmpty()
    {
        $now = Carbon::now();

        $user2 = factory(User::class)->create();
        $user1 = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user1->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user1->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user1->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $now->copy()->subDays(23)->subHour(6), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(23)->subHour(6), 1, 800);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(9), 1, 800);

        $this->generateAdHistory($room1, $now->copy()->subDays(14)->subHour(2), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(14)->subHour(2), 1, 800);
        $this->generateAdHistory($room3, $now->copy()->subDays(14)->subHour(2), 1, 1000);

        $stats = $this->adHistoryRepo->getAdsCostsStats($user2, StatsLib::Range30Days);

        $this->assertEquals(0, count($stats));
    }

    public function testGetAdsCostsStatsWithPpcChangeOnSameDay()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room11 = factory(Room::class)->create();
        $room22 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room11->id,
            "user_id" => $user2->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room22->id,
            "user_id" => $user2->id,
            "status" => "verified"
        ]);
        
        /**
         * Case 1
         * Each room got 2 click on yesterday
         * PPC room1 = 800 and room2 = 700
         * Ads Cost should be = 750
         */

        // PPC Room1 is 800.
        $this->generateAdHistory($room1, $now->copy()->subday()->addHour(9), 1, 800);
        $this->generateAdHistory($room1, $now->copy()->subday()->addHour(9), 1, 800);

        // PPC Room2 is 700
        $this->generateAdHistory($room2, $now->copy()->subDay()->addHour(9), 1, 700);
        $this->generateAdHistory($room2, $now->copy()->subDay()->addHour(9), 1, 700);

        $statsCase1 = $this->adHistoryRepo->getAdsCostsStats($user, StatsLib::Range7Days);

        /**
         * Case 2
         * Room1 got 1 click, 1 click on 9AM then 11AM got 1 click
         * but on 11AM ppc on room1 is increased to 1000
         * Room2 got 2 click
         * Ads cost should be = 800 -> 800 + 1000 + 700 + 700 = 3200 / 4 (click)
         */
        $twoDaysBehind = $now->copy()->subDays(2)->startOfDay();
        // Room1
        $this->generateAdHistory($room11, $twoDaysBehind->copy()->addHour(9), 1, 800);
        $this->generateAdHistory($room11, $twoDaysBehind->copy()->addHour(11), 1, 1000);

        // Room2
        $this->generateAdHistory($room22, $twoDaysBehind->copy()->addHour(13), 1, 700);
        $this->generateAdHistory($room22, $twoDaysBehind->copy()->addHour(13), 1, 700);

        $statsCase2 = $this->adHistoryRepo->getAdsCostsStats($user2, StatsLib::Range7Days);

        $this->assertEquals(750, $statsCase1[5]['value']);
        $this->assertEquals(800, $statsCase2[4]['value']);
    }

    public function testGetAdsCostsStatsSummary()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $now->copy()->subDays(3)->subHour(6), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(6), 1, 800);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(9), 1, 900);

        $this->generateAdHistory($room1, $now->copy()->subDays(4)->subHour(2), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(4)->subHour(2), 1, 800);
        $this->generateAdHistory($room3, $now->copy()->subDays(4)->subHour(2), 1, 1000);

        /**
         * $room1 => 900
         * $room2 => 800
         * $room3 => 1000
         * result should be  = 2700 / 3
         */

        $stats = $this->adHistoryRepo->getAdsCostsStatsSummary($user, StatsLib::Range7Days);

        $this->assertEquals(883, $stats['value']);
    }

    public function testGetRoomsMostClick()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 9
        ]);
        $room2 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 99
        ]);
        $room3 = factory(Room::class)->create([
            'expired_phone' => 0,
            'is_active'     => 'true',
            'view_count'    => 98
        ]);

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room2->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        
        factory(RoomOwner::class)->create([
            "designer_id" => $room3->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $now, 1);
        $this->generateAdHistory($room2, $now, 1);
        $this->generateAdHistory($room1, $now->copy()->addHour(), 1);
        $this->generateAdHistory($room3, $now->copy()->addHours(2), 1);

        $rooms = $this->adHistoryRepo->getRoomsMostClick($user);

        $this->assertEquals($room2->song_id, $rooms[0]['_id']);
    }

    public function testGetRoomsMostClickShouldReturnNull()
    {
        $user = factory(User::class)->create();

        $rooms = $this->adHistoryRepo->getRoomsMostClick($user);

        $this->assertEquals(0, count($rooms));
    }

    public function testGetAdsCostsStatsSummaryShouldReturnEmpty()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();
        $room2 = factory(Room::class)->create();
        $room3 = factory(Room::class)->create();

        $this->generateAdHistory($room1, $now->copy()->subDays(3)->subHour(6), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(6), 1, 800);
        $this->generateAdHistory($room2, $now->copy()->subDays(3)->subHour(9), 1, 900);

        $this->generateAdHistory($room1, $now->copy()->subDays(4)->subHour(2), 1, 900);
        $this->generateAdHistory($room2, $now->copy()->subDays(4)->subHour(2), 1, 800);
        $this->generateAdHistory($room3, $now->copy()->subDays(4)->subHour(2), 1, 1000);

        /**
         * $room1 => 900
         * $room2 => 800
         * $room3 => 1000
         * result should be  = 2700 / 3
         */

        $stats = $this->adHistoryRepo->getAdsCostsStatsSummary($user, StatsLib::Range7Days);

        $this->assertEquals(0, $stats['value']);
    }

    function testGetAdsRoomStatsSummaryFromUserWithoutVerifiedRoom()
    {
        $user = factory(User::class)->create();

        $stats = (array) $this->adHistoryRepo->getAdsRoomStatsSummary($user, StatsLib::Range7Days);

        $this->assertEquals(3, count($stats));
        $this->assertEquals(0, $stats['ads_cost']);
        $this->assertEquals(0, $stats['click']);
        $this->assertEquals(0, $stats['burning_balance']);
    }

    function testGetAdsRoomStatsSummaryShouldSuccess()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room1 = factory(Room::class)->create();

        factory(RoomOwner::class)->create([
            "designer_id" => $room1->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $this->generateAdHistory($room1, $now->copy()->subDays(4)->subHour(2), 1, 1000);

        $stats = (array) $this->adHistoryRepo->getAdsRoomStatsSummary($user, StatsLib::Range7Days);
        
        $this->assertEquals(1, $stats['click']);
        $this->assertEquals(1000, $stats['ads_cost']);
        $this->assertEquals(1000, $stats['burning_balance']);
        
    }

}
