<?php

namespace App\Repositories\Level;

use Illuminate\Contracts\Pagination\Paginator;
use App\Test\MamiKosTestCase;
use App\Entities\Level\RoomLevel;
use App\Repositories\Level\RoomLevelRepository;

class RoomLevelRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repo;
    
    private $roomLevel;

    protected function setup()
    {
        parent::setUp();

        $this->repo = app()->make(RoomLevelRepository::class);

        $this->roomLevel = factory(RoomLevel::class)->create([
            'name' => 'Executive',
            'is_regular' => false,
        ]);
    }

    public function testGetById_ShouldNotFound()
    {
        $result = $this->repo->getById(999);

        $this->assertNull($result);
    }

    public function testGetById_ShouldSuccess()
    {
        $result = $this->repo->getById($this->roomLevel->id);

        $this->assertNotNull($result);
        $this->assertInstanceOf(RoomLevel::class, $result);
        $this->assertEquals($this->roomLevel->id, $result->id);
        $this->assertEquals($this->roomLevel->name, $result->name);
    }

    public function testGetRoomLevelList_ShouldSuccess()
    {
        factory(RoomLevel::class, 4)->create();

        $result = $this->repo->getRoomLevelList();
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(5, $result->total());
        $this->assertEquals(20, $result->perPage());
    }

    public function testGetRoomLevelList_WithKeySearch_ShouldSuccess()
    {
        factory(RoomLevel::class, 4)->create();

        $result = $this->repo->getRoomLevelList($this->roomLevel->name);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(1, $result->total());
        $this->assertEquals(20, $result->perPage());

        $this->assertEquals($result[0]->name, $this->roomLevel->name);
    }

    public function testGetRoomLevelList_WithLimitPagination_ShouldSuccess()
    {
        factory(RoomLevel::class, 4)->create();

        $result = $this->repo->getRoomLevelList('', 3);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Paginator::class, $result);
        $this->assertEquals(5, $result->total());
        $this->assertEquals(3, $result->perPage());
    }

    public function testCreateRoomLevel_ShouldSuccess()
    {
        $data = factory(RoomLevel::class)->make()->toArray();
        
        $result = $this->repo->createRoomLevel($data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(RoomLevel::class, $result);
        $this->assertEquals($data['name'], $result->name);
    }

    public function testUpdateRoomLevel_ShouldSuccess()
    {
        $data = $this->roomLevelForm();

        $result = $this->repo->updateRoomLevel($this->roomLevel, $data);

        $this->assertNotNull($result);
        $this->assertInstanceOf(RoomLevel::class, $result);
        $this->assertEquals($data['name'], $result->name);
    }

    public function testGetAvailableOrders_ShouldSuccess()
    {
        $result = $this->repo->getAvailableOrders();

        $availableOrders = array_values(array_diff(range(1, 100), [$this->roomLevel->order]));

        $this->assertSame($availableOrders, $result);
    }

    public function testHasRegularLevel_ShouldSuccess()
    {
        $result = $this->repo->hasRegularLevel();

        $this->assertEquals(false, $result);

        $data = factory(RoomLevel::class)->create([
            'is_regular' => true
        ]);

        $result = $this->repo->hasRegularLevel();

        $this->assertEquals(true, $result);
    }

    private function roomLevelForm()
    {
        return [
            "name" => "Reguler",
            "order" => 1,
            "is_regular" => true,
            "is_hidden" => false,
            "notes" => "Qui explicabo autem placeat.",
            "charging_fee" => 10,
            "charging_type" => RoomLevel::CHARGING_TYPE_AMOUNT,
            "charge_for_owner" => false,
            "charge_for_consultant" => true,
            "charge_for_booking" => true,
            "charge_invoice_type" => RoomLevel::CHARGE_INVOICE_TYPE_ALL,
        ];
    }
}
