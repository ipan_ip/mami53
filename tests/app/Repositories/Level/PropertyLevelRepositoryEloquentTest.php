<?php

namespace App\Repositories\Reward;

use App\Entities\Level\KostLevel;
use App\Entities\Level\PropertyLevel;
use App\Repositories\Level\PropertyLevelRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class PropertyLevelRepositoryEloquentTest extends MamiKosTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->repository = app()->make(PropertyLevelRepositoryEloquent::class);
    }

    public function testCreatePropertyLevelAndSuccess()
    {
        $result = $this->repository->createPropertyLevel([
            'max_rooms' => 123,
            'minimum_charging' => 1000.00
        ]);
        $this->assertNotEmpty($result);
        $this->assertInstanceOf(PropertyLevel::class, $result);
    }

    public function testCreatePropertyLevelWithoutMaxRoomsAndReturnNull()
    {
        $result = $this->repository->createPropertyLevel([
            'minimum_charging' => 1000.00
        ]);
        $this->assertEmpty($result);
    }

    public function testCreatePropertyLevelWithoutMinChargingAndReturnNull()
    {
        $result = $this->repository->createPropertyLevel([
            'max_rooms' => 123,
        ]);
        $this->assertEmpty($result);
    }

    public function testUpdatePropertyLevelAndSuccess()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $result = $this->repository->updatePropertyLevel(
            $propertyLevel,
            [
                'max_rooms' => 123,
                'minimum_charging' => 1000.00
            ]
        );
        $this->assertTrue($result);
    }

    public function testUpdatePropertyLevelWithoutMaxRoomsAndReturnFalse()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $result = $this->repository->updatePropertyLevel(
            $propertyLevel,
            [
                'minimum_charging' => 1000.00
            ]
        );
        $this->assertFalse($result);
    }

    public function testUpdatePropertyLevelWithoutMinChargingAndReturnFalse()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $result = $this->repository->updatePropertyLevel(
            $propertyLevel,
            [
                'max_rooms' => 123,
            ]
        );
        $this->assertFalse($result);
    }

    public function testGetListPaginateWithoutNameParams()
    {
        factory(PropertyLevel::class)->create();
        $result = $this->repository->getListPaginate([], []);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testGetListPaginateWithNameParams()
    {
        $name = 'Hehehe';
        factory(PropertyLevel::class)->create();
        factory(PropertyLevel::class)->create(['name' => $name]);
        $result = $this->repository->getListPaginate([
            'name' => $name
        ], []);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result);
    }

    public function testDestroyAndSuccess()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $result = $this->repository->destroy($propertyLevel);

        $this->assertTrue($result);
    }

    public function testDestroyShouldRemoveKostLevelRelationToPropertyLevel()
    {
        $propertyLevel = factory(PropertyLevel::class)->create();
        $kostLevel = factory(KostLevel::class)->create(['property_level_id' => $propertyLevel->id]);
        $result = $this->repository->destroy($propertyLevel);

        $this->assertNull($kostLevel->property_level);
        $this->assertTrue($result);
    }

    public function testGetLevelDropdown()
    {
        factory(PropertyLevel::class)->create();
        $result = $this->repository->getLevelDropdown();
        $this->assertInstanceOf(Collection::class, $result);
    }
}
