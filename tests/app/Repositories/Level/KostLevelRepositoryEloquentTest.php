<?php

namespace App\Repositories\Level;

use App\Repositories\Level\KostLevelRepository;
use App\Entities\Level\KostLevel;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Level\LevelFaq;
use App\Entities\Level\LevelHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomUnit;
use App\Test\MamiKosTestCase;
use App\User;

class KostLevelRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repo;

    private $regularLevel;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repo = app()->make(KostLevelRepository::class);

        $this->regularLevel = factory(KostLevel::class)->create([
            'name' => 'Regular',
            'is_regular' => true,
            'is_hidden' => false,
            'order' => 5,
        ]);
    }

    public function testGetAll_ShouldSuccess()
    {
        $result = $this->repo->getAll();
        $this->assertNotNull($result);
        $this->assertTrue($result->count() > 0);
    }
    
    public function testChangeLevel_RegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $this->repo->changeLevel($room, $this->regularLevel, null, false);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $this->regularLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = $levelResult->histories->where('level_id', $this->regularLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
    }

    public function testChangeLevel_NonRegularLevelAssignment_UpgradeLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->changeLevel($room, $standardLevel, null, false);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $standardLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($standardLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = $levelResult->histories->where('level_id', $standardLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
    }

    public function testChangeLevel_NonRegularLevelAssignment_DowngradeLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 4,
        ]);

        $this->repo->changeLevel($room, $standardLevel, null, false);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $standardLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($standardLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories->where('level_id', $standardLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
    }

    public function testChangeLevel_NonRegularLevelAssignment_NoRegularLevel_ShouldSuccess()
    {
        $this->markTestSkipped('must be revisited.');

        $this->regularLevel->is_regular = false;
        $this->regularLevel->save();

        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 4,
        ]);

        $this->repo->changeLevel($room, $standardLevel, null, false);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $standardLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($standardLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = $levelResult->histories->where('level_id', $standardLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_UPGRADE, $levelHistoryResult->action);
    }

    public function testChangeLevel_GoldplusLevelAssignment_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);
        factory(RoomOwner::class)->create(
            [
                'user_id' => $user->id,
                'designer_id' => $room->id,
                'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
                'status' => 'verified'
            ]
        );

        $goldplusLevel = factory(KostLevel::class)->state('gp_level_1')->create([
            'name' => 'Goldplus',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->changeLevel($room, $goldplusLevel, null, false);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $goldplusLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($goldplusLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = $levelResult->histories->where('level_id', $goldplusLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($goldplusLevel->id, $levelHistoryResult->level_id);
    }

    public function testFlagLevel_RegularLevelAssignment_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $this->repo->flagLevel($room, $this->regularLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);
    }

    public function testFlagLevel_NonRegularLevelAssignment_HiddenLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => true,
            'order' => 6,
        ]);

        $this->repo->flagLevel($room, $standardLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);
    }

    public function testFlagLevel_NonRegularLevelAssignment_UpgradeLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->flagLevel($room, $standardLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelHistoryResult = LevelHistory::where([
                'kost_id' => $room->song_id, 
                'level_id' => $standardLevel->id
            ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertNull($levelHistoryResult->action);
    }

    public function testFlagLevel_NonRegularLevelAssignment_DowngradeLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 4,
        ]);

        $this->repo->flagLevel($room, $standardLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelHistoryResult = LevelHistory::where([
                'kost_id' => $room->song_id, 
                'level_id' => $standardLevel->id
            ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertNull($levelHistoryResult->action);
    }

    public function testFlagLevel_NonRegularLevelAssignment_DowngradeFlagLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 4,
        ]);
        $okeLevel = factory(KostLevel::class)->create([
            'name' => 'OKE',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 3,
        ]);

        $this->repo->flagLevel($room, $standardLevel, null);

        $room = Room::with(['level'])->where('id', $room->id)->first();
        $this->assertNotNull($room);

        $this->repo->flagLevel($room, $okeLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelHistoryResult = LevelHistory::where([
                'kost_id' => $room->song_id, 
                'level_id' => $standardLevel->id
            ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertNull($levelHistoryResult->action);

        $levelHistoryResult = LevelHistory::where([
            'kost_id' => $room->song_id, 
            'level_id' => $okeLevel->id
        ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($okeLevel->id, $levelHistoryResult->level_id);
        $this->assertNull($levelHistoryResult->action);
    }

    public function testFlagLevel_NonRegularLevelAssignment_NoRegularLevel_ShouldSuccess()
    {
        $this->markTestSkipped('must be revisited.');

        $this->regularLevel->is_regular = false;
        $this->regularLevel->save();

        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->flagLevel($room, $standardLevel, null);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->first();
        $this->assertNull($levelResult);

        $levelHistoryResult = LevelHistory::where([
            'kost_id' => $room->song_id, 
            'level_id' => $standardLevel->id
        ])->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(FlagEnum::FLAG_UPGRADE, $levelHistoryResult->flag);
        $this->assertNull($levelHistoryResult->action);
    }

    public function testApproveLevel_NoCurrentLevel_ShouldFail()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->approveUpgrade($room, $user);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->first();
        $this->assertNull($levelResult);
    }

    public function testApproveLevel_ShouldSuccess()
    {
        $this->markTestSkipped('must be revisited.');

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 6,
        ]);

        $this->repo->flagLevel($room, $standardLevel, $user);
        
        $room = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($room);

        $this->repo->approveUpgrade($room, $user);
        
        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($standardLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = LevelHistory::where([
                'kost_id' => $room->song_id, 
                'level_id' => $standardLevel->id
            ])->orderBy('id', 'DESC')->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($standardLevel->id, $levelHistoryResult->level_id);
        $this->assertEquals(HistoryActionEnum::ACTION_APPROVE, $levelHistoryResult->action);
        $this->assertNull($levelHistoryResult->flag);
    }

    public function testRemoveLevel_NoCurrentLevel_ShouldFail()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $room = Room::with(['level'])->where('id', $room->id)->first();
        $this->repo->removeFlag($room);

        $result = Room::with(['level'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->first();
        $this->assertNull($levelResult);
    }

    public function testRemoveLevel_ShouldSuccess()
    {
        $room = factory(Room::class)->create();
        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $this->repo->changeLevel($room, $this->regularLevel, null, false);
        
        $room = Room::with(['level'])->where('id', $room->id)->first();
        $room->changeFlagLevel(FlagEnum::FLAG_UPGRADE, $standardLevel->id);

        $room = Room::with(['level'])->where('id', $room->id)->first();
        $this->repo->removeFlag($room);

        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $this->regularLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelPivotResult = $levelResult->pivot->first();
        $this->assertNull($levelPivotResult->flag_level_id);
        $this->assertNull($levelPivotResult->flag);

        $levelHistoryResult = $levelResult->histories->where('level_id', $this->regularLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
    }

    public function testGetFaq_ShouldSuccess()
    {
        factory(LevelFaq::class, 3)->create();

        $result = $this->repo->getFaq();
        $this->assertNotNull($result);
        $this->assertEquals(3, $result->count());
    }

    public function testCountOwnerActiveKostList_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true
        ]);

        factory(RoomOwner::class)->create(
            [
                'user_id' => $user->id,
                'designer_id' => $room->id,
                'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
                'status' => 'verified'
            ]
        );

        $this->repo->changeLevel($room, $this->regularLevel, $user, false);
        
        $result = $this->repo->getOwnerActiveKostList($user, $room->name, $this->regularLevel->id);
        $this->assertNotNull($result);
        $this->assertEquals(1, $result->count());
    }

    public function testCountOwnerFlaggedKost_ShouldSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'is_active' => true
        ]);

        factory(RoomOwner::class)->create(
            [
                'user_id' => $user->id,
                'designer_id' => $room->id,
                'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
                'status' => 'verified'
            ]
        );

        $roomUnit = factory(RoomUnit::class)->create([
            'designer_id' => $room->id,
            'is_charge_by_room' => false,
        ]);

        $standardLevel = factory(KostLevel::class)->create([
            'name' => 'Standard',
            'is_regular' => false,
            'is_hidden' => false,
            'order' => 2,
        ]);

        $this->repo->changeLevel($room, $this->regularLevel, $user, false);
        
        $room = Room::with(['level'])->where('id', $room->id)->first();
        $room->changeFlagLevel(FlagEnum::FLAG_UPGRADE, $standardLevel->id);

        $result = $this->repo->countOwnerFlaggedKost($user);
        $this->assertEquals(1, $result);

        $result = Room::with(['room_unit', 'level', 'level.histories'])->where('id', $room->id)->first();
        $this->assertNotNull($result);

        $roomUnitResult = $result->room_unit->first();
        $this->assertNotNull($roomUnitResult);
        $this->assertEquals($roomUnit->id, $roomUnitResult->id);
        $this->assertEquals(false, (bool) $roomUnitResult->is_charge_by_room);

        $levelResult = $result->level->where('id', $this->regularLevel->id)->first();
        $this->assertNotNull($levelResult);
        $this->assertEquals($this->regularLevel->id, $levelResult->id);

        $levelHistoryResult = $levelResult->histories->where('level_id', $this->regularLevel->id)->first();
        $this->assertEquals($room->song_id, $levelHistoryResult->kost_id);
        $this->assertEquals($this->regularLevel->id, $levelHistoryResult->level_id);
    }
}
