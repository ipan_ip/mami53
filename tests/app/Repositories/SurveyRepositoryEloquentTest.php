<?php

namespace App\Repositories;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Room\Survey;
use App\Entities\Room\RoomOwner;
use App\Libraries\SMSLibrary;
use App\User;
use App\Entities\Room\Room;
use Carbon\Carbon;

class SurveyRepositoryEloquentTest extends MamiKosTestCase
{
    use WithFaker;

    protected $user;
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = new SurveyRepositoryEloquent(app());
    }

    public function testModelSurvey()
    {
        $model = $this->repository->model();
        $this->assertEquals(Survey::class, $model);
    }

    public function testgetModelSurvey()
    {
        $model = $this->repository->getModel();
        $this->assertInstanceOf(Survey::class, $model);
    }

    // Error count
    // Happen on Mamikos Web 1.9.0 upgrade
    // public function testaddSurveyWithPremiumOwnerShouldAddToDatabase()
    // {
    //     $now = now();

    //     $user = $this->createUserEntity();
    //     [$owner,$room,$roomOwner] = $this->createSetRoomOwner(
    //         ['is_owner' => 'true',
    //             'date_owner_limit' => date('Y-m-d H:i:s', strtotime($now . ' + 6 weeks'))
    //         ],
    //         [],
    //         [
    //             'owner_status' => RoomOwner::OWNER_TYPES[1],
    //         ]
    //     );

    //     $survey = factory(Survey::class)->make([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'birthday' => '1999-09-09',
    //         'time' => date('Y-m-d H:i:s', strtotime('2020-09-09 09:00:00')),
    //         'gender' => 'male',
    //         'job' => 'kerja',
    //         'forward' => 1
    //     ])->toArray();

    //     $data = [
    //         'is_me' => true,
    //         'phone' => '+6281111111111',
    //         'birthday' => '2000-01-01',
    //         'time' => $survey['time'],
    //         'designer_id' => $survey['designer_id'],
    //         'user_id' => $survey['user_id'],
    //         'name' => $survey['name'],
    //         'gender' => $survey['gender'],
    //         'birthday' => $survey['birthday'],
    //         'work_place' => $survey['work_place'],
    //         'position' => $survey['position'],
    //         'semester' => $survey['semester'],
    //         'kost_time' => $survey['kost_time'],
    //         'forward' => $survey['forward'],
    //         'serious' => 1,
    //         'chat_group_id' => 1,
    //         'admin_id' => '1',
    //         'jobs' => $survey['job'],
    //         'serious' => $survey['serious'],
    //         'song_id' => $room->song_id,
    //     ];

    //     $expectedResult = [
    //         'status' => true,
    //         'survey' => true,
    //         'is_premium' => true
    //     ];
    //     $result = $this->repository->addSurvey($user, $data);

    //     // check add survey success
    //     $this->assertDatabaseHas('survey', [
    //         'time' => $data['time'],
    //         'designer_id' => $data['designer_id'],
    //         'user_id' => $data['user_id'],
    //         'name' => $data['name'],
    //         'gender' => $data['gender'],
    //         'birthday' => $data['birthday'],
    //         'work_place' => $data['work_place'],
    //         'position' => $data['position'],
    //         'semester' => $data['semester'],
    //         'kost_time' => $data['kost_time'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //         'chat_group_id' => $data['chat_group_id'],
    //         'admin_id' => $data['admin_id'],
    //         'job' => $data['jobs'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //     ]);
    //     unset($result['message_survey']);
    //     $this->assertEquals($result, $expectedResult);
    // }

    // public function testaddSurveyWithNonPremiumOwnerShouldAddToDatabase()
    // {
    //     $now = now();

    //     $user = $this->createUserEntity();

    //     [$owner,$room,$roomOwner] = $this->createSetRoomOwner(
    //         [
    //             'is_owner' => 'true',
    //         ],
    //         [],
    //         [
    //             'owner_status' => RoomOwner::OWNER_TYPES[1],
    //         ]
    //     );

    //     $survey = factory(Survey::class)->make([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'birthday' => '1999-09-09',
    //         'time' => date('Y-m-d H:i:s', strtotime('2020-09-09 09:00:00')),
    //         'gender' => 'male',
    //         'job' => 'kuliah',
    //         'forward' => 1
    //     ])->toArray();

    //     $data = [
    //         'is_me' => true,
    //         'phone' => '+6281111111111',
    //         'birthday' => '2000-01-01',
    //         'time' => $survey['time'],
    //         'designer_id' => $survey['designer_id'],
    //         'user_id' => $survey['user_id'],
    //         'name' => $survey['name'],
    //         'gender' => $survey['gender'],
    //         'birthday' => $survey['birthday'],
    //         'work_place' => $survey['work_place'],
    //         'position' => $survey['position'],
    //         'semester' => $survey['semester'],
    //         'kost_time' => $survey['kost_time'],
    //         'forward' => $survey['forward'],
    //         'serious' => 1,
    //         'chat_group_id' => 1,
    //         'admin_id' => '1',
    //         'jobs' => $survey['job'],
    //         'serious' => $survey['serious'],
    //         'song_id' => $room->song_id,
    //     ];

    //     $expectedResult = [
    //         'status' => true,
    //         'survey' => true,
    //         'is_premium' => false
    //     ];
    //     $result = $this->repository->addSurvey($user, $data);

    //     // check add survey success
    //     $this->assertDatabaseHas('survey', [
    //         'time' => $data['time'],
    //         'designer_id' => $data['designer_id'],
    //         'user_id' => $data['user_id'],
    //         'name' => $data['name'],
    //         'gender' => $data['gender'],
    //         'birthday' => $data['birthday'],
    //         'work_place' => $data['work_place'],
    //         'position' => $data['position'],
    //         'semester' => $data['semester'],
    //         'kost_time' => $data['kost_time'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //         'chat_group_id' => $data['chat_group_id'],
    //         'admin_id' => $data['admin_id'],
    //         'job' => $data['jobs'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //     ]);
    //     unset($result['message_survey']);
    //     $this->assertEquals($result, $expectedResult);
    // }

    // public function testaddSurveyWithExistingSurveyAndNonPremuimUserShouldUpdateTheRecord()
    // {
    //     $now = now();

    //     $user = $this->createUserEntity();
    //     [$owner,$room,$roomOwner] = $this->createSetRoomOwner(
    //         [
    //             'is_owner' => 'true',
    //             'date_owner_limit' => date('Y-m-d H:i:s', strtotime($now . ' + 6 weeks'))
    //         ],
    //         [],
    //         [
    //             'owner_status' => RoomOwner::OWNER_TYPES[1],
    //         ]
    //     );

    //     $survey = factory(Survey::class)->create([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'birthday' => '1999-09-09',
    //         'time' => date('Y-m-d H:i:s', strtotime('2020-09-09 09:00:00')),
    //         'gender' => 'male',
    //         'job' => 'kuliah',
    //         'forward' => 1
    //     ])->toArray();

    //     $data = [
    //         'is_me' => true,
    //         'phone' => '+6281111111111',
    //         'birthday' => '2000-01-01',
    //         'time' => $survey['time'],
    //         'designer_id' => $survey['designer_id'],
    //         'user_id' => $survey['user_id'],
    //         'name' => 'update name',
    //         'gender' => $survey['gender'],
    //         'birthday' => $survey['birthday'],
    //         'work_place' => 'update work place',
    //         'position' => 'update positon',
    //         'semester' => 'update semester',
    //         'kost_time' => $survey['kost_time'],
    //         'forward' => $survey['forward'],
    //         'serious' => 1,
    //         'chat_group_id' => 1,
    //         'admin_id' => '1',
    //         'jobs' => $survey['job'],
    //         'serious' => $survey['serious'],
    //         'song_id' => $room->song_id,
    //         'notif_time' => date('Y-m-d H:i:s', strtotime($now . ' + 1 hours')),
    //         'notif' => '0'
    //     ];

    //     $expectedResult = [
    //         'status' => true,
    //         'survey' => true,
    //         'is_premium' => true
    //     ];
    //     $result = $this->repository->addSurvey($user, $data);

    //     // check add survey success
    //     $this->assertDatabaseHas('survey', [
    //         'time' => $data['time'],
    //         'designer_id' => $data['designer_id'],
    //         'user_id' => $data['user_id'],
    //         'name' => $data['name'],
    //         'gender' => $data['gender'],
    //         'birthday' => $data['birthday'],
    //         'work_place' => $data['work_place'],
    //         'position' => $data['position'],
    //         'semester' => $data['semester'],
    //         'kost_time' => $data['kost_time'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //     ]);
    //     unset($result['message_survey']);
    //     $this->assertEquals($result, $expectedResult);
    // }

    // public function testaddSurveyWithExistingSurveyAndPremuimUserShouldUpdateTheRecord()
    // {
    //     $now = now();

    //     $user = $this->createUserEntity();
    //     [$owner,$room,$roomOwner] = $this->createSetRoomOwner(
    //         [
    //             'is_owner' => 'true',
    //         ],
    //         [],
    //         [
    //             'owner_status' => RoomOwner::OWNER_TYPES[1],
    //         ]
    //     );

    //     $survey = factory(Survey::class)->create([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'birthday' => '1999-09-09',
    //         'time' => date('Y-m-d H:i:s', strtotime('2020-09-09 09:00:00')),
    //         'gender' => 'male',
    //         'job' => 'kuliah',
    //         'forward' => 1
    //     ])->toArray();

    //     $data = [
    //         'is_me' => true,
    //         'phone' => '+6281111111111',
    //         'birthday' => '2000-01-01',
    //         'time' => $survey['time'],
    //         'designer_id' => $survey['designer_id'],
    //         'user_id' => $survey['user_id'],
    //         'name' => 'update name',
    //         'gender' => $survey['gender'],
    //         'birthday' => $survey['birthday'],
    //         'work_place' => 'update work place',
    //         'position' => 'update positon',
    //         'semester' => 'update semester',
    //         'kost_time' => $survey['kost_time'],
    //         'forward' => $survey['forward'],
    //         'serious' => 1,
    //         'chat_group_id' => 1,
    //         'admin_id' => '1',
    //         'jobs' => $survey['job'],
    //         'serious' => $survey['serious'],
    //         'song_id' => $room->song_id,
    //         'notif_time' => date('Y-m-d H:i:s', strtotime($now . ' + 1 hours')),
    //         'notif' => '0'
    //     ];

    //     $expectedResult = [
    //         'status' => true,
    //         'survey' => true,
    //         'is_premium' => false
    //     ];
    //     $result = $this->repository->addSurvey($user, $data);

    //     // check add survey success
    //     $this->assertDatabaseHas('survey', [
    //         'time' => $data['time'],
    //         'designer_id' => $data['designer_id'],
    //         'user_id' => $data['user_id'],
    //         'name' => $data['name'],
    //         'gender' => $data['gender'],
    //         'birthday' => $data['birthday'],
    //         'work_place' => $data['work_place'],
    //         'position' => $data['position'],
    //         'semester' => $data['semester'],
    //         'kost_time' => $data['kost_time'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //         'forward' => $data['forward'],
    //         'serious' => $data['serious'],
    //     ]);
    //     unset($result['message_survey']);
    //     $this->assertEquals($result, $expectedResult);
    // }

    // public function testaddSurveyWithTimeGreateThanMaxAndaLessThanMinTime()
    // {
    //     $now = now();

    //     $user = $this->createUserEntity();
    //     [$owner,$room,$roomOwner] = $this->createSetRoomOwner(
    //         [
    //             'is_owner' => 'true',
    //             'date_owner_limit' => date('Y-m-d H:i:s', strtotime($now . ' + 6 weeks'))
    //         ],
    //         [],
    //         [
    //             'owner_status' => RoomOwner::OWNER_TYPES[1],
    //         ]
    //     );

    //     $survey = factory(Survey::class)->make([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id,
    //         'birthday' => '1999-09-09',
    //         'time' => date('Y-m-d H:i:s', strtotime('2020-20-09 09:00:00')),
    //         'gender' => 'male',
    //         'job' => 'kerja',
    //         'forward' => 1
    //     ])->toArray();

    //     $data = [
    //         'is_me' => true,
    //         'phone' => '+6281111111111',
    //         'birthday' => '2000-01-01',
    //         'time' => $survey['time'],
    //         'designer_id' => $survey['designer_id'],
    //         'user_id' => $survey['user_id'],
    //         'name' => $survey['name'],
    //         'gender' => $survey['gender'],
    //         'birthday' => $survey['birthday'],
    //         'work_place' => $survey['work_place'],
    //         'position' => $survey['position'],
    //         'semester' => $survey['semester'],
    //         'kost_time' => $survey['kost_time'],
    //         'forward' => $survey['forward'],
    //         'serious' => 1,
    //         'chat_group_id' => 1,
    //         'admin_id' => '1',
    //         'jobs' => $survey['job'],
    //         'serious' => $survey['serious'],
    //         'song_id' => $room->song_id,
    //     ];

    //     $expectedResult = [
    //         'status' => false,
    //         'survey' => false,
    //         'message' => 'Survey hanya bisa dilakukan antara jam 8 pagi sampai jam 8 malam.'
    //     ];
    //     $result = $this->repository->addSurvey($user, $data);

    //     $this->assertEquals($result, $expectedResult);
    // }

    public function testcheckOwnerKost()
    {
        $owner = $this->createUserEntity();
        $room = $this->createRoomEntity();
        factory(RoomOwner::class)->create([
            'user_id' => $owner->id,
            'designer_id' => $room->id,
            'owner_status' => 'Pemilik Kost'
        ]);

        $result = $this->repository->checkOwnerKost($room->id);
        $this->assertContains($owner->id, $result);
    }

    public function testupdatePhoneSurveyShouldReturnTrue()
    {
        $user = $this->createUserEntity(['phone_number' => '+6281111111111']);

        $this->assertDatabaseHas('user', [
            'id' => $user->id,
            'phone_number' => '+6281111111111'
        ]);

        $newPhone = '+6281234567890';
        $result = $this->repository->updatePhoneSurvey(
            $user,
            $newPhone
        );

        $this->assertDatabaseHas('user', [
            'id' => $user->id,
            'phone_number' => $newPhone
        ]);
        $this->assertTrue($result);
    }

    public function testupdatePhoneSurveyShouldReturnFalse()
    {
        $user = $this->createUserEntity(['phone_number' => '+6281111111111']);

        $this->assertDatabaseHas('user', [
            'id' => $user->id,
            'phone_number' => '+6281111111111'
        ]);

        $newPhone = '+0071234567890';
        $result = $this->repository->updatePhoneSurvey(
            $user,
            $newPhone
        );

        $this->assertDatabaseMissing('user', [
            'id' => $user->id,
            'phone_number' => $newPhone
        ]);
        $this->assertFalse($result);
    }

    public function testgetPhoneExistShouldReturnPhoneNumber()
    {
        $phones = ['+6281111111111'];
        $result = $this->repository->getPhoneExist($phones);
        $this->assertEquals($result, $phones[0]);
    }

    public function testgetPhoneExistShouldReturnDash()
    {
        $phones = ['+0071111111111'];
        $result = $this->repository->getPhoneExist($phones);
        $this->assertEquals($result, '-');
    }

    public function testupdateSurveyShouldReturnTrue()
    {
        $time = now();

        $user = $this->createUserEntity();
        $room = $this->createRoomEntity();
        $survey = factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'name' => 'survey',
            'gender' => 'male',
            'birthday' => date('Y-m-d', strtotime($time . ' - 10 years')),
            'job' => 'job',
            'work_place' => 'work place',
            'position' => 'position',
            'semester' => 'semester',
            'kost_time' => null,
            'time' => date('Y-m-d H:i:s', strtotime($time . ' - 6 hours')),
            'notification_time' => date('Y-m-d H:i:s', strtotime($time . ' - 6 minutes')),
            'notification' => '0',
            'forward' => 1,
            'serious' => 1,
            'chat_group_id' => 1
        ]);

        $this->assertDatabaseHas('survey', [
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'name' => 'survey',
            'gender' => 'male',
            'birthday' => $survey->birthday,
            'job' => 'job',
            'work_place' => 'work place',
            'position' => 'position',
            'semester' => 'semester',
            'kost_time' => null,
            'time' => $survey->time,
            'notification_time' => $survey->notification_time,
            'notification' => '0',
            'forward' => 1,
            'serious' => 1,
            'chat_group_id' => 1
        ]);

        $data = [
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'name' => 'survey update',
            'gender' => 'female',
            'birthday' => date('Y-m-d', strtotime($time . ' - 5 years')),
            'jobs' => 'job update',
            'work_place' => 'work place update',
            'position' => 'position update',
            'semester' => 'semester update',
            'kost_time' => 'kost-time update',
            'time' => date('Y-m-d H:i:s', strtotime($time . ' - 5 hours')),
            'notif_time' => date('Y-m-d H:i:s', strtotime($time . ' + 5 minutes')),
            'notif' => '1',
            'forward' => 2,
            'serious' => 2,
            'chat_group_id' => null
        ];

        $result = $this->repository->updateSurvey($data);

        $expectedResult = [
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'name' => 'survey update',
            'gender' => 'female',
            'birthday' => $data['birthday'],
            'job' => 'job update',
            'work_place' => 'work place update',
            'position' => 'position update',
            'semester' => 'semester update',
            'kost_time' => 'kost-time update',
            'time' => $data['time'],
            'notification_time' => $data['notif_time'],
            'notification' => '1',
            'forward' => 2,
            'serious' => 2,
        ];

        $this->assertDatabaseHas('survey', $expectedResult);
        $this->assertTrue($result);
    }

    public function testcheckAvailUserSurvey()
    {
        $room = $this->createRoomEntity();
        $user = $this->createUserEntity();

        $survey = factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        $result = $this->repository->checkAvailUserSurvey($user->id, $room->id);
        $this->assertEquals($result->id, $survey->id);
    }

    // Error count
    // Happen on Mamikos Web 1.9.0 upgrade
    // public function testgetSurveyShouldReturnSurvey()
    // {
    //     $room = $this->createRoomEntity();
    //     $user = $this->createUserEntity();
    //     $survey = factory(Survey::class)->create([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id
    //     ]);

    //     $result = $this->repository->getSurvey($user->id, $room->song_id);
    //     $this->assertEquals($result['survey']->id, $survey->id);
    // }

    // public function testgetSurveyWithWrongSongIdShouldReturnEmptyArray()
    // {
    //     $room = $this->createRoomEntity();
    //     $user = $this->createUserEntity();
    //     $survey = factory(Survey::class)->create([
    //         'user_id' => $user->id,
    //         'designer_id' => $room->id
    //     ]);

    //     $result = $this->repository->getSurvey($user->id, null);
    //     $this->assertEquals($result['survey'], []);
    // }

    // public function testgetSurveyWhenSurveyNotExistShouldReturnEmptyArray()
    // {
    //     $room = $this->createRoomEntity();
    //     $user = $this->createUserEntity();

    //     $result = $this->repository->getSurvey($user->id, $room->id);
    //     $this->assertEquals($result['survey'], []);
    // }

    public function testGetRoomData()
    {
        $room = $this->createRoomEntity();
        $result = $this->repository->getRoomData($room->song_id);
        $this->assertEquals($room->id, $result->id);
    }

    public function testsendSmsShouldReturnTrue()
    {
        $mockSmsLibrary = $this->mockAlternatively(SMSLibrary::class);
        $mockSmsLibrary->shouldReceive('send')->withAnyArgs()->andReturn(true);
        app()->instance(SMSLibrary::class, $mockSmsLibrary);

        $message = 'unit test';
        $phone_number = '+6281111111111';
        $result = $this->repository->sendSms($message, $phone_number);
        $this->assertTrue($result);
    }

    public function testgetListSurveyOwner()
    {
        $this->markTestSkipped("flaky test");

        [$owner,$room,$roomOwner] = $this->createSetRoomOwner();

        $user = $this->createUserEntity([
            'date_owner_limit' => date('Y-m-d', strtotime(now() . ' - 5 days')),
        ]);

        $survey1 = factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'job' => 'kuliah',
            'created_at' => date('Y-m-d', strtotime(now() . ' - 5 days'))
        ]);
        $survey2 = factory(Survey::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'job' => '',
        ]);

        $semester = ' , Semester ' . $survey1->semester;
        $phoneNumber = '+62' . SMSLibrary::phoneNumberCleaning($user->phone_number);
        $phone_number = SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? $phoneNumber : 'Nomor tidak valid';
        $phone_number = SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? substr($phoneNumber, 0, 7) . 'xxxxx' : 'Nomor tidak valid';

        $data[] = [
            'id' => $survey1->id,
            'user_id' => $survey1->user_id,
            'name' => $survey1->name,
            'name_social' => $survey1->user->name,
            'gender' => request()->filled('os') && request()->get('os') == 'ios' ? $survey1->gender :
            ($survey1->gender == 'male' ? 'laki-laki' : 'perempuan'),
            'kost_time' => $survey1->kost_time,
            'phone_number' => $phone_number,
            'time' => date('d M Y', strtotime($survey1->time)),
            'hour' => date('H:s', strtotime($survey1->time)),
            'birthday' => Carbon::parse($survey1->birthday)->age . ' Tahun',
            'jobs' => ucwords($survey1->job . ', ' . $survey1->work_place . ', ' . $survey1->position . $semester),
            'chat_group_id' => is_null($survey1->chat_group_id) ? 0 : (int) $survey1->chat_group_id,
            'group_channel_url' => (string) $survey1->chat_group_id,
            'button_click' => 'Perlihatkan',
            'is_new' => $survey1->created_at->format('Y-m-d') == date('Y-m-d') ? true : false
        ];

        $semester = '';
        $phoneNumber = '+62' . SMSLibrary::phoneNumberCleaning($user->phone_number);
        $phone_number = SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? $phoneNumber : 'Nomor tidak valid';

        $data[] = [
            'id' => $survey2->id,
            'user_id' => $survey2->user_id,
            'name' => $survey2->name,
            'name_social' => $survey2->user->name,
            'gender' => request()->filled('os') && request()->get('os') == 'ios' ? $survey2->gender :
            ($survey2->gender == 'male' ? 'laki-laki' : 'perempuan'),
            'kost_time' => $survey2->kost_time,
            'phone_number' => $phone_number,
            'time' => date('d M Y', strtotime($survey2->time)),
            'hour' => date('H:s', strtotime($survey2->time)),
            'birthday' => Carbon::parse($survey2->birthday)->age . ' Tahun',
            'jobs' => ucwords($survey2->job . ', ' . $survey2->work_place . ', ' . $survey2->position . $semester),
            'chat_group_id' => is_null($survey2->chat_group_id) ? 0 : (int) $survey2->chat_group_id,
            'group_channel_url' => (string) $survey2->chat_group_id,
            'button_click' => null,
            'is_new' => $survey2->created_at->format('Y-m-d') == date('Y-m-d') ? true : false
        ];

        $expectedResult = [
            'data' => $data,
            'clickable' => 'Untuk melihat nomor hp anda harus premium',
            'feature' => 'list histori survey dan chat dengan user , silakan upgrade akun anda menjadi premium.',
            'count' => 2,
            'page_total' => 1,
            'email' => true
        ];

        $result = $this->repository->getListSurveyOwner($room->song_id, $user);
        unset($result['data'][0]['admin_id'], $result['data'][1]['admin_id']);
        $this->assertEquals($result, $expectedResult);

        $user->email = 'not valid';
        $expectedResult['email'] = false;

        $result = $this->repository->getListSurveyOwner($room->song_id, $user);
        unset($result['data'][0]['admin_id'], $result['data'][1]['admin_id']);
        $this->assertEquals($result, $expectedResult);
    }

    public function testCountSurvey()
    {
        $room = $this->createRoomEntity();
        factory(Survey::class, 5)->create([
            'designer_id' => $room->id
        ]);
        $result = $this->repository->countSurvey($room->song_id);
        $this->assertEquals(5, $result);
    }

    // Error count
    // Happen on Mamikos Web 1.9.0 upgrade
    // public function testgetEditSurveyShouldReturnSurvey()
    // {
    //     $survey = factory(Survey::class)->create([]);

    //     $result = $this->repository->getEditSurvey($survey->id);

    //     $this->assertEquals($result->id, $survey->id);
    // }

    // public function testupdateSurveyOwnerShouldReturnEmptyArray()
    // {
    //     factory(Survey::class)->create([]);

    //     $data = ['id' => null];
    //     $result = $this->repository->updateSurveyOwner($data);

    //     $this->assertEquals($result, []);
    // }
    // 
    // public function testgetEditSurveyShouldReturnEmptyArray()
    // {
    //     $result = $this->repository->getEditSurvey(null);

    //     $this->assertEquals($result, []);
    // }
    // 
    // public function testupdateSurveyOwnerShouldReturnTrue()
    // {
    //     $time = now();
    //     $survey = factory(Survey::class)->create([
    //         'time' => $time,
    //         'notification_time' => $time,
    //         'notification' => '1',
    //     ]);

    //     $this->assertDatabaseHas('survey', [
    //         'time' => $survey->time,
    //         'notification_time' => $survey->notification_time,
    //         'notification' => '1',
    //     ]);

    //     $expectedResult = [
    //         'id' => $survey->id,
    //         'time' => date('Y-m-d H:i:s', strtotime($time)),
    //         'notification_time' => date('Y-m-d H:i:s', strtotime($time . ' - 6 hours')),
    //         'notification' => '0'
    //     ];

    //     $result = $this->repository->updateSurveyOwner($expectedResult);

    //     $this->assertDatabaseHas('survey', $expectedResult);

    //     $this->assertTrue($result);
    // }


    public function testgetSurveyList()
    {
        $user = $this->createUserEntity();
        $survey1 = factory(Survey::class)->create(['user_id' => $user->id, 'job' => 'kuliah']);
        $survey2 = factory(Survey::class)->create(['user_id' => $user->id, 'job' => '']);

        $result = $this->repository->getSurveyList($user->id);

        $semester = ' , Semester ' . $survey1->semester;
        $expectedResult[] = [
            'id' => $survey1->id,
            'user_id' => $survey1->user_id,
            'name' => $survey1->name,
            'name_social' => $survey1->user->name,
            'gender' => $survey1->gender,
            'kost_time' => $survey1->kost_time,
            'time' => date('d M Y', strtotime($survey1->time)),
            'birthday' => Carbon::parse($survey1->birthday)->age . ' Tahun',
            'jobs' => ucwords($survey1->job . ', ' . $survey1->work_place . ', ' . $survey1->position . $semester),
            'chat_group_id' => (int) $survey1->chat_group_id,
            'group_channel_url' => (string) $survey1->chat_group_id,
        ];

        $semester = '';
        $expectedResult[] = [
            'id' => $survey2->id,
            'user_id' => $survey2->user_id,
            'name' => $survey2->name,
            'name_social' => $survey2->user->name,
            'gender' => $survey2->gender,
            'kost_time' => $survey2->kost_time,
            'time' => date('d M Y', strtotime($survey2->time)),
            'birthday' => Carbon::parse($survey2->birthday)->age . ' Tahun',
            'jobs' => ucwords($survey2->job . ', ' . $survey2->work_place . ', ' . $survey2->position . $semester),
            'chat_group_id' => (int) $survey2->chat_group_id,
            'group_channel_url' => (string) $survey2->chat_group_id,
        ];

        unset($result['survey'][0]['admin_id'], $result['survey'][1]['admin_id']);

        $this->assertEquals($expectedResult[0], $result['survey'][0]);
        $this->assertEquals($expectedResult[1], $result['survey'][1]);
    }

    public function testsecurePaginateWithPageParamLessThan10ShouldSuccess()
    {
        $reflection = new \ReflectionClass(SurveyRepositoryEloquent::class);
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);

        request()->merge(['page' => 9]);

        $method->invokeArgs($this->repository, []);

        $page = request()->input('page', 1);
        $this->assertEquals($page, 9);
    }

    public function testsecurePaginateWithPageParamGreaterThan10ShouldSuccess()
    {
        $reflection = new \ReflectionClass(SurveyRepositoryEloquent::class);
        $method = $reflection->getMethod('securePaginate');
        $method->setAccessible(true);

        request()->merge(['page' => 20]);

        $method->invokeArgs($this->repository, []);

        $page = request()->input('page', 1);
        $this->assertEquals($page, 10);
    }

    private function createUserEntity(array $override = [])
    {
        $user = factory(User::class)->create($override);
        return $user;
    }

    private function createRoomEntity(array $override = [])
    {
        $room = factory(Room::class)->create($override);
        return $room;
    }

    private function createRoomOwnerEntity(array $override = [])
    {
        $roomOwner = factory(RoomOwner::class)->create($override);
        return $roomOwner;
    }

    private function createSetRoomOwner(array $overrideUser = [], array $overrideRoom = [], array $overrideRoomOwner = [])
    {
        $owner = $this->createUserEntity($overrideUser);
        $room = $this->createRoomEntity($overrideRoom);

        $overrideRoom['owner_phone'] = $overrideRoom['owner_phone'] ?? $owner->phone_number;

        $overrideRoomOwner['designer_id'] = $overrideRoomOwner['designer_id'] ?? $room->id;
        $overrideRoomOwner['user_id'] = $overrideRoomOwner['user_id'] ?? $owner->id;

        $roomOwner = $this->createRoomOwnerEntity($overrideRoomOwner);
        return [$owner, $room, $roomOwner];
    }
}
