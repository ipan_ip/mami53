<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Test\MamiKosTestCase;
use App\Repositories\Booking\BookingDesignerRoomRepositoryEloquent;

class BookingDesignerRoomRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingDesignerRoomRepositoryEloquent::class);
    }

    public function testGenerateBookingDesignerRoomsSuccess() : void
    {
        // create add Booking Designer
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'available_room' => 2
        ]);

        // make empty array
        $bookingDesignerRooms = [];

        // check data array
        $this->assertEquals(0, count($bookingDesignerRooms));

        // check data to get available room
        $bookingDesignerRooms = $this->repository->generateBookingDesignerRooms($bookingDesigner);

        // check data array
        $this->assertEquals(2, count($bookingDesignerRooms));
    }

    public function testUpdateBookingDesignerRoomsLessThanUpdatedRooms() : void
    {
        // create add Booking Designer
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'available_room' => 2
        ]);

        // create add Booking Designer Updated
        $bookingDesignerUpdated = factory(BookingDesigner::class)->create([
            'available_room' => 4
        ]);
        // make empty array
        $bookingDesignerRooms = [];

        // check data array
        $this->assertEquals(0, count($bookingDesignerRooms));

        // check data to get available room and updated room
        $bookingDesignerRooms = $this->repository->updateBookingDesignerRooms($bookingDesigner, $bookingDesignerUpdated);

        // check data array
        $this->assertEquals(4, count($bookingDesignerRooms));
    }

    public function testUpdateBookingDesignerRoomsBiggerThanUpdatedRooms() : void
    {
        // create add Booking Designer
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'available_room' => 3
        ]);

        // create add Booking Designer Updated
        $bookingDesignerUpdated = factory(BookingDesigner::class)->create([
            'available_room' => 1
        ]);
        // make empty array
        $bookingDesignerRooms = [];

        // check data array
        $this->assertEquals(0, count($bookingDesignerRooms));

        // check data to get available room and updated room
        $bookingDesignerRooms = $this->repository->updateBookingDesignerRooms($bookingDesigner, $bookingDesignerUpdated);

        // check data array
        $this->assertEquals(0, count($bookingDesignerRooms));
    }
}
