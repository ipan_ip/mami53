<?php

namespace App\Repositories\Booking;

use App\Test\MamiKosTestCase;
use App\Repositories\Booking\BookingDiscountRepositoryEloquent;

class BookingDiscountRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingDiscountRepositoryEloquent::class);
    }

    public function testGetFormattedValueOnNominal()
    {
        $discount = (object) [
            'discount_type'   => 'nominal',
            'discount_value'  => 200000,
        ];

        $method = $this->getPublicFormattedDataMethod();
        $result = $method->invokeArgs(
            $this->repository, 
            [$discount, 'discount_type', 'discount_value']
        );

        $this->assertEquals("200,000", $result);
    }

    public function testGetFormattedValueOnPercentage()
    {
        $discount = (object) [
            'discount_type'   => 'percentage',
            'discount_value'  => 20,
        ];

        $method = $this->getPublicFormattedDataMethod();
        $result = $method->invokeArgs(
            $this->repository, 
            [$discount, 'discount_type', 'discount_value']
        );

        $this->assertEquals("20%", $result);
    }

    public function testGetPercentageFromNominal()
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingDiscountRepositoryEloquent::class,
            'getPercentageFromNominal'
        );

        $result = $method->invokeArgs($this->repository, [50, 100]);
        $this->assertEquals(50, $result);

        $result = $method->invokeArgs($this->repository, [5, 100]);
        $this->assertEquals(5, $result);
    }

    public function testGetNominalFromPercentage()
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingDiscountRepositoryEloquent::class,
            'getNominalFromPercentage'
        );

        $result = $method->invokeArgs($this->repository, [10, 100000]);
        $this->assertEquals(10000, $result);

        $result = $method->invokeArgs($this->repository, [50, 100000]);
        $this->assertEquals(50000, $result);
    }

    public function testGetDiscountProportion()
    {
        $discount = (object) [
            "price"                   => 100000,
            "markup_type"             => "percentage",
            "markup_value"            => 0,
            "discount_source"         => "mamikos",
            "discount_type_mamikos"   => "percentage",
            "discount_value_mamikos"  => 10,
            "discount_type_owner"     => "percentage",
            "discount_value_owner"    => 0,
        ];

        $method = $this->getPublicGetDiscountProportion();
        $result = $method->invokeArgs(
            $this->repository, 
            [$discount]
        );

        $this->assertEquals(10, $result['mamikos']['percentage']);
        $this->assertEquals(10000, $result['mamikos']['nominal']);
        $this->assertEquals(0, $result['owner']['percentage']);
        $this->assertEquals(0, $result['owner']['nominal']);
    }

    public function testGetDiscountProportionMamikosAndOwner()
    {
        $discount = (object) [
            "price"                   => 100000,
            "markup_type"             => "percentage",
            "markup_value"            => 0,
            "discount_source"         => "mamikos_and_owner",
            "discount_type_mamikos"   => "percentage",
            "discount_value_mamikos"  => 10,
            "discount_type_owner"     => "percentage",
            "discount_value_owner"    => 40,
        ];

        $method = $this->getPublicGetDiscountProportion();
        $result = $method->invokeArgs(
            $this->repository, 
            [$discount]
        );

        $this->assertEquals(10, $result['mamikos']['percentage']);
        $this->assertEquals(10000, $result['mamikos']['nominal']);
        $this->assertEquals(40, $result['owner']['percentage']);
        $this->assertEquals(40000, $result['owner']['nominal']);
    }

    private function getPublicFormattedDataMethod()
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingDiscountRepositoryEloquent::class,
            'getFormattedValue'
        );

        return $method;
    }

    private function getPublicGetDiscountProportion()
    {
        $method = $this->getNonPublicMethodFromClass(
            BookingDiscountRepositoryEloquent::class,
            'getDiscountProportion'
        );

        return $method;
    }
}
