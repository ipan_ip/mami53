<?php
namespace App\Repositories\Booking;

use App\Entities\Booking\BookingReject;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\StatusChangedBy;
use App\Entities\Consultant\Consultant;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Test\MamiKosTestCase;
use App\User;

class BookingStatusChangeRepositoryEloquentTest extends MamiKosTestCase
{
    
    /**
     * @var BookingStatusChangeRepositoryEloquent
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingStatusChangeRepositoryEloquent::class);
    }

    public function testAddChangeSuccess(): void
    {
        // create booking user data
        $bookingUserEntity = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        // added historical data
        $this->repository->addChange($bookingUserEntity, $bookingUserEntity->status, null, null);

        // get status
        $bookingStatus = $bookingUserEntity->statuses;

        // test
        $this->assertEquals(1, count($bookingStatus));
        $this->assertEquals($bookingUserEntity->status, $bookingStatus->first()->status);
    }

    public function testAddChangeHandleExistDataSuccess(): void
    {
        // create booking user data
        $bookingUserEntity = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        // added historical data
        $this->repository->addChange($bookingUserEntity, $bookingUserEntity->status, null, null);
        $this->repository->addChange($bookingUserEntity, $bookingUserEntity->status, null, null);

        // get status
        $bookingStatus = $bookingUserEntity->statuses;
        $this->assertEquals(1, count($bookingStatus));
        $this->assertEquals($bookingUserEntity->status, $bookingStatus->first()->status);
    }

    public function testGetChangedRoleIsTenant(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingStatusChangeRepositoryEloquent::class,
            'getChangedRole'
        );

        // create user data
        $user = factory(User::class)->create();

        // run test
        $result = $method->invokeArgs(
            $this->repository,
            [$user]
        );

        $this->assertEquals(StatusChangedBy::TENANT, $result);
    }

    public function testGetChangedRoleIsConsultant(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingStatusChangeRepositoryEloquent::class,
            'getChangedRole'
        );

        // create user & consultant data
        $userEntity = factory(User::class)->create();
        $consultantEntity = factory(Consultant::class)->create(['user_id' => $userEntity->id]);

        // run test
        $result = $method->invokeArgs(
            $this->repository,
            [$userEntity]
        );

        $this->assertEquals(StatusChangedBy::CONSULTANT, $result);
    }

    public function testGetChangedRoleIsOwner(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingStatusChangeRepositoryEloquent::class,
            'getChangedRole'
        );

        // create user data
        $userEntity = factory(User::class)->create(['is_owner' => true]);

        // run test
        $result = $method->invokeArgs(
            $this->repository,
            [$userEntity]
        );

        $this->assertEquals(StatusChangedBy::OWNER, $result);
    }

    public function testGetChangedRoleIsAdministrator(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingStatusChangeRepositoryEloquent::class,
            'getChangedRole'
        );

        // create user data
        $userEntity = factory(User::class)->create(['role' => UserRole::Administrator]);

        // run test
        $result = $method->invokeArgs(
            $this->repository,
            [$userEntity]
        );

        $this->assertEquals(StatusChangedBy::ADMIN, $result);
    }

    public function testGetChangedRoleIsSystem(): void
    {
        // get private function
        $method = $this->getNonPublicMethodFromClass(
            BookingStatusChangeRepositoryEloquent::class,
            'getChangedRole'
        );

        // run test
        $result = $method->invokeArgs(
            $this->repository,
            [null]
        );

        $this->assertEquals(StatusChangedBy::SYSTEM, $result);
    }

    /**
     * When a trx have booking_reject_reason_id = null 
     * then it will be counted
     * 
     * @group BAR-Total
     * @group BG-2972
     */
    public function testGetTotalBookingBySongId()
    {
        $room = factory(Room::class)->create();
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_code' => 'TRANSACTION_ONE',
            'reject_reason' => 'Saya gak suka dengan penyewa ini',
        ]);
        $trx2 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
            'booking_code' => 'TRANSACTION_TWO',
            'reject_reason' => 'Saya lagi menutup sementara kos',
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);
        $this->repository->addChange($trx2, $trx2->status, null, null);

        $result = $this->repository->getTotalBookingBySongId($room->song_id);
        $this->assertEquals(2, $result);
    }
    
    /**
     * When a trx have reject_reason = null 
     * then it will be counted
     * 
     * @group BAR-Total
     * @group BG-2972
     */
    public function testGetBookingBySongIdWithNullRejectReason()
    {
        $room = factory(Room::class)->create();
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'reject_reason' => null,
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);

        $result = $this->repository->getTotalBookingBySongId($room->song_id);
        $this->assertEquals(1, $result);
    }

    /**
     * When a trx have booking_reject_reason_id = 1 (any valid number as relation)
     * then it will be check booking_reject_reason table
     * if booking_reject_reason.is_active === 1
     * and booking_reject_reason.is_affecting_acceptance === 0
     * then it will NOT be counted
     * 
     * @group BAR-Total
     * @group BG-2972
     */
    public function testGetTotalBookingBySongIdWithRejectReasonKostPenuh()
    {
        $room = factory(Room::class)->create();

        // create reject reason
        $rejectReason = factory(BookingReject::class)->create([
            'description' => 'Kost penuh',
            'is_active' => 1,
            'is_affecting_acceptance' => 0,
        ]);

        // create booking transaction
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_reject_reason_id' => $rejectReason->id,
            'reject_reason' => 'Kost penuh',
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);

        $result = $this->repository->getTotalBookingBySongId($room->song_id);
        $this->assertEquals(0, $result);
    }

    /**
     * When a trx have booking_reject_reason_id = 1 (any valid number as relation)
     * then it will be check booking_reject_reason table
     * if booking_reject_reason.is_active === 1
     * and booking_reject_reason.is_affecting_acceptance === 1
     * then it will be counted
     * 
     * @group BAR-Total
     * @group BG-2972
     */
    public function testGetTotalBookingBySongIdWithRejectReasonOwnerReject()
    {
        $room = factory(Room::class)->create();

        // create reject reason
        $rejectReason = factory(BookingReject::class)->create([
            'description' => 'alasan penolakan yang tidak bisa di jelaskan',
            'is_active' => 1,
            'is_affecting_acceptance' => 1,
        ]);

        // create booking transaction
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_reject_reason_id' => $rejectReason->id,
            'reject_reason' => 'alasan penolakan yang tidak bisa di jelaskan',
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);

        $result = $this->repository->getTotalBookingBySongId($room->song_id);
        $this->assertEquals(1, $result);
    }

    /**
     * When trx status is already confirmed that means booking_reject_reason_id = null
     * then it will be counted as booking accepted
     * 
     * @group BAR-Accepted
     * @group BG-2972
     */
    public function testGetTotalAcceptedBookingBySongId()
    {
        $room = factory(Room::class)->create();

        // create reject reason
        $rejectReason = factory(BookingReject::class)->create([
            'description' => 'alasan penolakan yang tidak bisa di jelaskan',
            'is_active' => 1,
            'is_affecting_acceptance' => 1,
        ]);

        // create transaction
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'booking_reject_reason_id' => null,
        ]);

        // create transaction 2
        $trx2 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_EXPIRED,
            'booking_reject_reason_id' => $rejectReason->id,
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);
        $this->repository->addChange($trx2, $trx2->status, null, null);

        $result = $this->repository->getTotalAcceptedBookingBySongId($room->song_id);
        $this->assertEquals(1, $result);
    }

    /**
     * When trx status is already confirmed that means booking_reject_reason_id = null
     * then it will be counted as booking accepted
     * 
     * @group BAR-Accepted
     * @group BG-2972
     */
    public function testGetTotalBookingBySongIdWithCancelByAdmin()
    {
        $room = factory(Room::class)->create();

        // create reject reason
        $rejectReason = factory(BookingReject::class)->create([
            'description' => 'Kost penuh',
            'is_active' => 1,
            'is_affecting_acceptance' => 0,
        ]);

        // create transaction
        $trx1 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
            'booking_code' => 'TRANSACTION_ABCD',
            'booking_reject_reason_id' => null,
        ]);

        $trx2 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'booking_code' => 'TRANSACTION_CDEF',
            'booking_reject_reason_id' => null,
        ]);

        $trx3 = factory(BookingUser::class)->create([
            'designer_id' => $room->song_id,
            'status' => BookingUser::BOOKING_STATUS_CANCELLED,
            'booking_code' => 'TRANSACTION_XYZ',
            'booking_reject_reason_id' => $rejectReason->id,
        ]);

        // added historical data
        $this->repository->addChange($trx1, $trx1->status, null, null);
        $this->repository->addChange($trx2, $trx2->status, null, null);
        $this->repository->addChange($trx3, $trx3->status, null, null);

        $result = $this->repository->getTotalBookingBySongId($room->song_id);
        $this->assertEquals(1, $result);
    }
}
