<?php

namespace App\Repositories\Booking\Owner;

use App\Entities\Booking\BookingOwner;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class OwnerBookingRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(OwnerBookingRepository::class);
    }

    public function testGetListInstantBooking()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        $roomVerifiedOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);

        $bookingOwner = factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => true,
        ]);

        $keywords = [
            'room_name' => '',
            'phone_number' => '',
            'created_at' => '',
            'sort_by' => '',
        ];

        $result = $this->repository->getListInstantBooking($keywords);
        $this->assertEquals($user->id, $result[0]->owner_id);
        $this->assertEquals($bookingOwner->owner_id, $result[0]->owner_id);
        $this->assertEquals($roomVerifiedOwner->user_id, $result[0]->owner_id);
        $this->assertEquals($bookingOwner->is_instant_booking, $result[0]->is_instant_booking);
        $this->assertEquals($bookingOwner->is_active, $result[0]->is_active);
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $result);
        $this->assertNotNull($result);
    }

    public function testStoreInstantBookIsFailed()
    {
        $user = factory(User::class)->create([
            'id' => '123'
        ]);

        $bookingOwner = factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => true,
        ]);

        $result = $this->repository->storeInstantBook($user->id, $this->defaultInstantBookingParams());
        $this->assertIsArray($result);
        $this->assertEquals(false, $result['status']);
        $this->assertEquals('Owner is already registered', $result['message']);
        $this->assertEquals($bookingOwner->owner_id, $result['data']->owner_id);
        $this->assertNotNull($result['data']);
    }

    public function testStoreInstantBookIsSuccess()
    {
        $user = factory(User::class)->create([
            'id' => '123'
        ]);

        $result = $this->repository->storeInstantBook($user->id, $this->defaultInstantBookingParams());
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Data owner successfully added', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals($user->id, $result['data']->owner_id);
    }

    public function testSetStatusBookingOwnerNull()
    {
        $user = factory(User::class)->create([
            'id' => '123'
        ]);

        factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => false,
        ]);

        $result = $this->repository->setStatus(1234, true);
        $this->assertIsArray($result);
        $this->assertEquals(false, $result['status']);
        $this->assertEquals('Data not found', $result['message']);
        $this->assertNull($result['data']);
    }

    public function testSetStatusActive()
    {
        $user = factory(User::class)->create([
            'id' => '123'
        ]);

        $bookingOwner = factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => false,
        ]);

        $result = $this->repository->setStatus($bookingOwner->id, true);
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Change status owner successfully', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals($bookingOwner->id, $result['data']->id);
        $this->assertEquals(true, $result['data']->is_instant_booking);
    }

    public function testSetStatusDeactive()
    {
        $user = factory(User::class)->create([
            'id' => '123'
        ]);

        $bookingOwner = factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => true,
        ]);

        $result = $this->repository->setStatus($bookingOwner->id, false);
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Change status owner successfully', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals($bookingOwner->id, $result['data']->id);
        $this->assertEquals(false, $result['data']->is_instant_booking);
    }

    public function defaultInstantBookingParams()
    {
        return [
            'rent_count' => [
                'weekly',
                'quarterly',
                'semiannually'
            ],
            'is_instant_booking' => true,
            'additional_prices' => [
                [
                    'key' => 'listrik',
                    'value' => '7000'
                ],
                [
                    'key' => 'air',
                    'value' => '3500'
                ],
            ]
        ];
    }

    public function testGetByIdIsNotNull()
    {
        $user = $this->mockUser();
        $bookingOwner = $this->mockBookingOwner($user);

        $result = $this->repository->getById($bookingOwner->id);
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Data found', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals($user->id, $result['data']->owner_id);
    }

    public function testGetByIdIsNull()
    {
        $result = $this->repository->getById(0);
        $this->assertIsArray($result);
        $this->assertEquals(false, $result['status']);
        $this->assertEquals('Data not found', $result['message']);
        $this->assertNull($result['data']);
    }

    public function testUpdateInstantBook()
    {
        $user = $this->mockUser();
        $bookingOwner = $this->mockBookingOwner($user);

        $result = $this->repository->updateInstantBook($bookingOwner->id, $this->defaultInstantBookingParams());
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Data owner successfully updated', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals($user->id, $result['data']->owner_id);
    }

    public function testGetRentCountsActiveRooms()
    {
        $owner = factory(User::class)->create([
            'id' => '123',
            'is_owner' => true,
        ]);

        $result = $this->repository->getRentCountsActiveRooms($owner->id);
        $this->assertIsArray($result);
        $this->assertEquals(true, $result['status']);
        $this->assertEquals('Data found', $result['message']);
        $this->assertNotNull($result['data']);
        $this->assertEquals(true, in_array('monthly', $result['data']));;
    }

    public function mockUser()
    {
        return factory(User::class)->create([
            'id' => 1
        ]);
    }

    public function mockBookingOwner($user)
    {
        return factory(BookingOwner::class)->create([
            'owner_id' => $user->id,
            'is_active' => true,
            'is_instant_booking' => true,
            'id' => 123
        ]);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

}