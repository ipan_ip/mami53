<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Booking\BookingStatus;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\SLA\SLAConfiguration;
use App\Entities\SLA\SLAVersion;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App;
use App\Entities\Booking\BookingUser;
use App\User;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingOwner;
use App\Entities\Booking\BookingReject;
use Mockery;
use App\Entities\Room\Room;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Level\KostLevel;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Level\KostLevelMap;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Element\RoomUnit;
use Google\Cloud\Vision\Annotation\Face;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class BookingUserRepositoryEloquentTest extends MamiKosTestCase
{

    protected $repository;
    protected $rentCountHelper;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository =  $this->app->make(App\Repositories\Booking\BookingUserRepositoryEloquent::class);
        $this->rentCountHelper = Mockery::mock('overload:App\Http\Helpers\RentCountHelper');
    }

    public function testModel()
    {
        $this->assertEquals('App\Entities\Booking\BookingUser', $this->repository->model());
    }

    public function testNewBookRoomSuccess()
    {
        $room = factory(Room::class)->create([
            'price_monthly' => 500000,
            'price_yearly' => 500000
        ]);
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_DAILY
        ]);

        $user = factory(User::class)->create();
        factory(MamipayTenant::class)->create([
            'user_id' => $user->id,
            'phone_number' => $user->phone_number,
            'parent_name' => 'Andrea',
            'parent_phone_number' => $user->phone_number
        ]);

        $params = [
            'checkin' => Carbon::now()->format('Y-m-d'),
            'checkout' => Carbon::now()->addMonth(1)->format('Y-m-d'),
            'duration' => 1,
            'rent_count_type' => 'yearly',
            'contact_parent_name' => 'xxxx',
            'contact_parent_phone' => 'xxxx',
            'contact_identity' => 345323,
            'contact_name' => 'Fandi',
            'contact_phone' => '08234234',
            'contact_job' => 'mahasiswa',
            'contact_email' => 'syifandi@mamiteam.com',
            'contact_gender' => 'male',
            'song_id' => $room->id,
            'session_id' => '98ec0b79-c357-471c-b833-fdd4f0d819b5'
        ];

        $this->rentCountHelper->shouldReceive('getFormatRentCount')
            ->andReturn('1 Tahun');

        $this->mockAlternatively('App\Repositories\Booking\BookingStatusChangeRepositoryEloquent')
            ->shouldReceive('addChange')
            ->andReturn(true);

        $this->mockAlternatively('overload:App\Channel\MoEngage\MoEngageEventDataApi')
            ->shouldReceive('reportPropertyBooked')
            ->andReturn(true);

        $output = $this->repository->newBookRoom($bookingDesigner, $params, $user, $room);

        $this->assertEquals($user->id, $output->user_id);
        $this->assertEquals($bookingDesigner->id, $output->booking_designer_id);
    }

    public function testNewConfirmAvailabilityDpMorethanFirstAmount()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_DAILY
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $params = [
            'room_number' => 1,
            'price' => 10000,
            'down_payment' => 100000,
            'first_amount' => 50000,
        ];

        $this->rentCountHelper->shouldReceive('getFormatRentTypeMamipay')
            ->andReturn('month');

        $output = $this->repository->newConfirmAvailability($bookingUser, $params);
        $this->assertEquals([
            'status' => false,
            'message' => 'Dp tidak bisa lebih besar dari Biaya Prorata Sewa Pertama'
        ], $output);
    }

    public function testNewConfirmAvailabilityInvalidReqMamipay()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_DAILY
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $params = [
            'room_number' => 1,
            'price' => 10000,
            'down_payment' => 50000,
            'first_amount' => 100000,
        ];

        $this->rentCountHelper->shouldReceive('getFormatRentTypeMamipay')
            ->andReturn('month');

        $mamipay = $this->mockAlternatively('overload:App\Entities\Mamipay\MamipayTenant');
        $mamipay->shouldReceive('createTenantViaMamipay')
            ->andReturnUsing(function (){

                $meta = new \StdClass;;
                $meta->message = 'Invalid mamipay request';

                $resp = new \StdClass;
                $resp->status = false;
                $resp->meta = $meta;

                return $resp;
            });

        $output = $this->repository->newConfirmAvailability($bookingUser, $params);

        $this->assertEquals([
            'status' => false,
            'message' => 'Invalid mamipay request'
        ], $output);
    }

    public function testNewConfirmAvailabilitySuccess()
    {
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_DAILY
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id
        ]);

        $params = [
            'room_number' => 1,
            'price' => 10000,
            'down_payment' => 50000,
            'first_amount' => 100000,
        ];

        $this->rentCountHelper->shouldReceive('getFormatRentTypeMamipay')
            ->andReturn('month');

        $mamipay = $this->mockAlternatively('overload:App\Entities\Mamipay\MamipayTenant');
        $mamipay->shouldReceive('createTenantViaMamipay')
            ->andReturnUsing(function (){

                $contract = new \StdClass;;
                $contract->id = 1;

                $data = new \StdClass;;
                $data->contract = $contract;

                $resp = new \StdClass;
                $resp->status = true;
                $resp->data = $data;

                return $resp;
            });

        $this->mockAlternatively('overload:App\Channel\MoEngage\MoEngageEventDataApi')
            ->shouldReceive('reportBookingConfirmed')
            ->andReturn(true);

        $output = $this->repository->newConfirmAvailability($bookingUser, $params);

        $this->assertEquals($bookingUser->id, $output->id);
        $this->assertEquals(1, $output->contract_id);
    }

    public function testUpdateBookingIsNullBookingDesigner()
    {
        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
        ]);

        $this->mockAlternatively('overload:App\Http\Helpers\BookingUserHelper')
            ->shouldReceive('getCheckoutDate')
            ->andReturn(Carbon::now()->addWeek(1)->format('Y-m-d'));

        $this->rentCountHelper->shouldReceive('getFormatRentCount')
            ->andReturn('1 Tahun');

        $output = $this->repository->updateBooking($bookingUser, $this->defaultParamsUpdateBooking());
        $this->assertNull($output);
    }

    public function testUpdateBookingSuccess()
    {
        $room = factory(Room::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_DAILY
        ]);

        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id
        ]);

        factory(BookingUserRoom::class)->create([
            'booking_user_id' => $bookingUser->id
        ]);

        // mock booking user helper
        $bookingUserHelper = $this->mockAlternatively('overload:App\Http\Helpers\BookingUserHelper');
        $bookingUserHelper->shouldReceive('getCheckoutDate')->andReturn(Carbon::now()->addWeek(1)->format('Y-m-d'));
        $bookingUserHelper->shouldReceive('getPrice')->andReturn(500000);

        $this->rentCountHelper->shouldReceive('getFormatRentCount')
            ->andReturn('1 Tahun');

        $output = $this->repository->updateBooking($bookingUser, $this->defaultParamsUpdateBooking());

        $this->assertEquals($this->defaultParamsUpdateBooking()['checkin'], $output->checkin_date);
        $this->assertEquals(Carbon::now()->addWeek(1)->format('Y-m-d'), $output->checkout_date);

    }

    public function testCancelBookingByAdminSuccess()
    {
        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
        ]);

        $cancelReason = factory(BookingReject::class)->create([
            'id' => 1
        ]);

        $params = [
            'booking_reject_reason_id' => $cancelReason->id,
            'cancel_reason' => $cancelReason->description
        ];

        $this->mockAlternatively('App\Repositories\Booking\BookingStatusChangeRepositoryEloquent')
            ->shouldReceive('addChange')
            ->andReturn(true);

        $this->mockAlternatively('overload:App\Entities\Notif\Category')
            ->shouldReceive('getNotificationCategory')
            ->andReturn(null);

        $output = $this->repository->cancelBookingByAdmin($bookingUser, $params);
        $this->assertNull($output);
    }

    public function testRejectBookingByAdminSuccess()
    {
        $bookingUser = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_PAID,
        ]);

        $rejectReason = factory(BookingReject::class)->create([
            'id' => 1
        ]);

        $params = [
            'booking_reject_reason_id' => $rejectReason->id,
            'reject_reason' => $rejectReason->description
        ];

        $this->mockAlternatively('App\Repositories\Booking\BookingStatusChangeRepositoryEloquent')
            ->shouldReceive('addChange')
            ->andReturn(true);

        $this->mockAlternatively('overload:App\Entities\Notif\Category')
            ->shouldReceive('getNotificationCategory')
            ->andReturn(null);

        $output = $this->repository->rejectBookingByAdmin($bookingUser, $params);
        $this->assertNull($output);
    }

    public function testCancelBookingTypeNotMonthly()
    {
        $user = factory(User::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['type' => BookingDesigner::BOOKING_TYPE_DAILY]);
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id
        ]);
        $output = $this->repository->cancelBooking($bookingUser, '');
        $this->assertFalse($output);
    }

    public function testCancelBookingInvalidCheckinDate()
    {
        $user = factory(User::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['type' => BookingDesigner::BOOKING_TYPE_MONTHLY]);
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id,
            'checkin_date' => Carbon::now()->addMonth(-2)->format('Y-m-d H:i:s')
        ]);
        $output = $this->repository->cancelBooking($bookingUser, '');
        $this->assertFalse($output);
    }

    public function testCancelBookingSuccess()
    {
        $user = factory(User::class)->create();
        $bookingDesigner = factory(BookingDesigner::class)->create(['type' => BookingDesigner::BOOKING_TYPE_MONTHLY]);
        $contract = factory(MamipayContract::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'contract_id' => $contract->id,
            'status' => BookingUser::BOOKING_STATUS_PAID,
            'booking_designer_id' => $bookingDesigner->id,
            'checkin_date' => Carbon::now()->addWeek(1)->format('Y-m-d H:i:s')
        ]);
        $params = [
            'reason' => 'Sudah dapat yang lebih baik',
            'reason_id' => 15
        ];
        $output = $this->repository->cancelBooking($bookingUser, $params);
        $this->assertTrue($output);
    }

    public function testGetHistoryIsEmpty()
    {
        $user = factory(User::class)->create();
        $params = [
            'limit' => 10,
            'offset' => 0,
        ];
        $output = $this->repository->getHistory($user, $params);

        $this->assertEquals(0, $output['bookingUsersTotal']);
        $this->assertIsNotArray($output['bookingUsers']);
    }

    public function testGetHistorySuccess()
    {
        $user = factory(User::class)->create();
        factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_PAID
        ]);

        $params = [
            'limit' => 10,
            'offset' => 0,
        ];
        $output = $this->repository->getHistory($user, $params);
        $this->assertEquals(1, $output['bookingUsersTotal']);
        $this->assertIsNotArray($output['bookingUsers']);
    }

    public function testcrawlNotificationWaitingIsNull()
    {
        $output = $this->repository->crawlNotificationWaiting();
        $this->assertNull($output);
    }

    public function testCrawlNotificationWaitingTodayIsNull()
    {
        $output = $this->repository->crawlNotificationWaitingToday();
        $this->assertNull($output);
    }

    public function testCrawlNotificationBookingHasPassedSuccess()
    {
        factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_EXPIRED_DATE,
        ]);

        $this->mockAlternatively('overload:App\Entities\Notif\Category')
            ->shouldReceive('getNotificationCategory')
            ->andReturn(null);

        $this->mockAlternatively('App\Http\Helpers\BookingNotificationHelper')
            ->shouldReceive('notifBookingHasPassedToTenant')
            ->andReturn(true);

        $output = $this->repository->crawlNotificationBookingHasPassed();
        $this->assertNull($output);
    }

    public function testCrawlNotificationBookingHasFinishedSuccess()
    {
        factory(BookingUser::class)->create([
            'is_expired' => 0,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'checkout_date' => Carbon::now()->addDay(-5)->format('Y-m-d')
        ]);

        $this->mockAlternatively('App\Http\Helpers\BookingNotificationHelper')
            ->shouldReceive('notifBookingFinsihedToTenant')
            ->andReturn(true);

        $this->mockAlternatively('App\Repositories\Booking\BookingStatusChangeRepositoryEloquent')
            ->shouldReceive('addChange')
            ->andReturn(true);

        $output = $this->repository->crawlNotificationBookingHasFinished();
        $this->assertNull($output);
    }

    public function testCrawlBookingExpirationWithoutDataSuccess()
    {
        $output = $this->repository->crawlBookingExpiration([5]);
        $this->assertNull($output);
    }

    public function testExtendWaitingTimeSuccess()
    {
        $smLib = $this->mockAlternatively('overload:App\Libraries\SMSLibrary');

        $smLib->shouldReceive('send')
            ->andReturn(true);

        $smLib->shouldReceive('phoneNumberCleaning')
            ->andReturn('08223345353');

        $user = factory(User::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id
        ]);
        $output = $this->repository->extendWaitingTime($bookingUser);
        $this->assertNull($output);
    }

    public function testCounterSuccess()
    {
        $user = factory(User::class)->create();
        factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_PAID
        ]);
        $output = $this->repository->counter($user);
        $this->assertEquals(1, $output);
    }

    public function testCheckInAvailibilityFalse()
    {
        $user = factory(User::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_PAID
        ]);

        $output = $this->repository->checkIn($bookingUser, '12:00', $user);
        $this->assertFalse($output);
    }

    public function testGetMyRoomSuccess()
    {
        $user = factory(User::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_VERIFIED
        ]);
        $output = $this->repository->getMyRoom($user->id);

        $this->assertEquals($bookingUser->id, $output->id);
        $this->assertEquals($bookingUser->booking_designer_id, $output->booking_designer_id);
        $this->assertEquals($bookingUser->user_id, $output->user_id);
        $this->assertEquals($bookingUser->status, $output->status);
        $this->assertEquals($bookingUser->is_expired, $output->is_expired);
    }

    private function defaultParamsUpdateBooking()
    {
        return [
            'rent_count_type' => 'monthly',
            'checkin' => Carbon::now()->addDay(2)->format('Y-m-d'),
            'duration' => 1,
            'contact_name' => 'Fandi',
            'contact_phone' => '08221343245',
            'contact_job' => 'mahasiswa',
            'contact_email' => 'syifandi@mamiteam.com',
            'contact_gender' => 'male'
        ];
    }

    public function testGetListBookingFromAdminPageWithoutDownload()
    {
        $user = factory(User::class)->create();
        factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request();
        $result = $this->repository->getListBookingFromAdminPage($request, false);
        $resultToArray = $result->toArray();

        $this->assertNotNull($result);
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $result);
        $this->assertIsArray($resultToArray);
        $this->assertEquals(BookingUser::BOOKING_STATUS_BOOKED, $resultToArray['data'][0]['status']);
        $this->assertArrayHasKey('data', $resultToArray);
    }

    public function testGetListBookingFromAdminPageWithDownload()
    {
        $user = factory(User::class)->create();
        factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request();
        $result = $this->repository->getListBookingFromAdminPage($request, true);
        $resultToArray = $result->toArray();

        $this->assertNotNull($result);
        $this->assertIsArray($resultToArray);
        $this->assertEquals(BookingUser::BOOKING_STATUS_BOOKED, $resultToArray[0]['status']);
    }

    public function testGetListBookingFromAdminPageWithNoteCategoryShouldShowMatching(): void
    {
        $user = factory(User::class)->create();
        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $booking->id,
            'activity' => 'test'
        ]);

        $request = new Request(['note_category' => 'test']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertNotNull($result->first());
        $this->assertTrue($result->first()->is($booking));
    }

    public function testGetListBookingFromAdminPageWithNoteCategoryShouldNotShowNonMatching(): void
    {
        $user = factory(User::class)->create();
        $booking = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        factory(ConsultantNote::class)->create([
            'noteable_type' => ConsultantNote::NOTE_BOOKING_USER,
            'noteable_id' => $booking->id,
            'activity' => 'est'
        ]);

        $request = new Request(['note_category' => 'test']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertEmpty($result);
    }

    public function testGetListBookingFromAdminPageShouldSortByCheckoutDateDesc(): void
    {
        $user = factory(User::class)->create();
        $first = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'checkout_date' => Carbon::now()->addDay(),
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        $second = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'checkout_date' => Carbon::now(),
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['sort' => 5]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($first));
    }

    public function testGetListBookingFromAdminPageShouldSortByCheckoutDateAsc(): void
    {
        $user = factory(User::class)->create();
        $first = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'checkout_date' => Carbon::now()->addDay(),
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        $second = factory(BookingUser::class)->create([
            'user_id' => $user->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'checkout_date' => Carbon::now(),
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['sort' => 4]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($second));
    }

    protected function createBookingWithInstantBookingOwner(): BookingUser
    {
        $kost = factory(Room::class)->create(['is_testing' => false]);
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $booking = factory(BookingUser::class)->create(['designer_id' => $kost->song_id, 'booking_designer_id' => $bookingDesigner->id]);
        $owner = factory(User::class)->create();
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => true]);
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => $owner->id]);

        return $booking;
    }

    protected function createBookingWithNonInstantBookingOwner(): BookingUser
    {
        $kost = factory(Room::class)->create(['is_testing' => false]);
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $booking = factory(BookingUser::class)->create(['designer_id' => $kost->song_id, 'booking_designer_id' => $bookingDesigner->id]);
        $owner = factory(User::class)->create();
        factory(BookingOwner::class)->create(['owner_id' => $owner->id, 'is_instant_booking' => false]);
        factory(RoomOwner::class)->create(['designer_id' => $kost->id, 'user_id' => $owner->id]);

        return $booking;
    }

    public function testGetListBookingFromAdminPageShouldFilterBookingWithInstantBookingStatus(): void
    {
        $expected = $this->createBookingWithInstantBookingOwner();
        $notExpected = $this->createBookingWithNonInstantBookingOwner();

        $request = new Request(['instant_booking' => 'true']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(1, $result);
        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterBookingWithoutInstantBookingStatus(): void
    {
        $notExpected = $this->createBookingWithInstantBookingOwner();
        $expected = $this->createBookingWithNonInstantBookingOwner();

        $request = new Request(['instant_booking' => 'false']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(1, $result);
        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldNotFilterByInstantBookingStatus(): void
    {
        $notExpected = $this->createBookingWithInstantBookingOwner();
        $expected = $this->createBookingWithNonInstantBookingOwner();

        $request = new Request(['instant_booking' => null]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(2, $result);
    }

    public function testGetListBookingFromAdminPageShouldFilterByContactPhone(): void
    {
        $expected = factory(BookingUser::class)->create([
            'contact_phone' => '08123456789',
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['phone' => '08123456789']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByContactName(): void
    {
        $expected = factory(BookingUser::class)->create([
            'contact_name' => 'guy',
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['contact_name' => 'guy']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByKostName(): void
    {
        $kost = factory(Room::class)->create(['name' => 'test', 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $request = new Request(['kost_name' => 'test']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByAreaCity(): void
    {
        $kost = factory(Room::class)->create(['area_city' => 'Bekasi', 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $request = new Request(['area_city' => ['Bekasi']]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByOwnerPhone(): void
    {
        $kost = factory(Room::class)->create(['owner_phone' => '08123456789', 'is_testing' => false]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id]);

        $request = new Request(['owner_phone' => '08123456789']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByPaymentStatus(): void
    {
        $expected = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_VERIFIED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['payment_status' => 'paid']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByStatus(): void
    {
        $expected = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['status' => 0]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldNotFilterByKostType(): void
    {
        $expected = factory(BookingUser::class)->create([
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['kost_type' => 'all']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageShouldFilterByKostLevel(): void
    {
        $kost = factory(Room::class)->create(['is_testing' => false]);
        $level = factory(KostLevel::class)->create();
        factory(KostLevelMap::class)->create(['kost_id' => $kost->song_id, 'level_id' => $level->id]);
        $bookingDesigner = factory(BookingDesigner::class)->create(['designer_id' => $kost->id]);
        $expected = factory(BookingUser::class)->create(['designer_id' => $kost->song_id, 'booking_designer_id' => $bookingDesigner->id]);

        $request = new Request(['kost_levlel' => $level->id]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertTrue($result->first()->is($expected));
    }
    public function testGetListBookingFromAdminPageWithTransferPermission(): void
    {
        $expected = factory(BookingUser::class)->create([
            'transfer_permission' => BookingUser::ALLOW_DISBURSE,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        $notExpected = factory(BookingUser::class)->create([
            'transfer_permission' => null,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['transfer_permission' => BookingUser::ALLOW_DISBURSE]);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(1, $result);
        $this->assertTrue($result->first()->is($expected));
    }

    public function testGetListBookingFromAdminPageWithEmptyTransferPermissionShouldNotFilter(): void
    {
        $expected = factory(BookingUser::class)->create([
            'transfer_permission' => BookingUser::DISBURSED,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);
        $notExpected = factory(BookingUser::class)->create([
            'transfer_permission' => null,
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['transfer_permission' => '']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(2, $result);
    }

    public function testGetListBookingFromAdminPageWithBookingCode()
    {
        $booking = factory(BookingUser::class)->create([
            'booking_code' => 'HUYT0987',
            'designer_id' => factory(Room::class)->create(['is_testing' => false])->song_id
        ]);

        $request = new Request(['booking_code' => 'HUYT0987']);
        $result = $this->repository->getListBookingFromAdminPage($request, true);

        $this->assertCount(1, $result);
        $this->assertTrue($result->first()->is($booking));
    }

    public function testGetRoomUnitListIsNull()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();

        // run test
        $response = $this->repository->getRoomUnitList($roomEntity);
        $this->assertEmpty($response);
    }

    public function testGetRoomUnitListSuccess()
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $tenantEntity = factory(MamipayTenant::class)->create();
        $roomUnitEntity = factory(RoomUnit::class)->create([
            'designer_id' => $roomEntity->id
        ]);
        $contractEntity = factory(MamipayContract::class)->create([
            'tenant_id' => $tenantEntity->id,
            'status' => 'active'
        ]);
        factory(App\Entities\Mamipay\MamipayContractKost::class)->create([
            'contract_id' => $contractEntity->id,
            'designer_room_id' => $roomUnitEntity->id
        ]);

        // run test
        $response = $this->repository->getRoomUnitList($roomEntity);
        $firstData = $response->first();

        $this->assertEquals($roomUnitEntity->id, $firstData->id);
        $this->assertEquals($roomUnitEntity->name, $firstData->name);
        $this->assertEquals($roomUnitEntity->floor, $firstData->floor);
    }

    public function testGetActiveContractEmptyRoomEmpty(): void
    {
        // run test
        $result = $this->repository->getActiveContractEmptyRoom();
        $this->assertInstanceOf(Collection::class, $result);
        $this->assertSame(0, $result->count());
    }

    public function testGetActiveContractEmptyRoomSuccess(): void
    {
        // prepare data create contract
        $contractEntity = factory(MamipayContract::class)->create([
            'status' => 'active',
            'start_date' => Carbon::now()->format('Y-m-d')
        ]);

        // prepare data create contract type kost with null designer_room_id
        factory(MamipayContractKost::class)->create([
            'contract_id' => $contractEntity->id,
            'room_number' => MamipayContractKost::CHOOSE_ON_THE_SPOT,
            'designer_room_id' => null
        ]);

        // prepare data create booking user
        factory(BookingUser::class)->create([
            'contract_id' => $contractEntity->id
        ]);

        // run test
        $result = $this->repository->getActiveContractEmptyRoom();
        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testCheckTenantAlreadyBooked()
    {
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        $bookingDesignerEntity = factory(BookingDesigner::class)->create([
            'designer_id' => $roomEntity->id
        ]);

        factory(BookingUser::class)->create([
            'user_id' => $userEntity->id,
            'booking_designer_id' => $bookingDesignerEntity->id,
            'status' => BookingUser::BOOKING_STATUS_CHECKED_IN,
            'checkout_date' => Carbon::today()->addMonth(3)->format('Y-m-d'),
            'designer_id' => $roomEntity->song_id
        ]);

        $userBookingActive = $this->repository->checkTenantAlreadyBooked($userEntity, $roomEntity);

        $this->assertIsArray($userBookingActive);
        $this->assertEquals(true, $userBookingActive['isAlreadyBooked']);
        $this->assertTrue($userBookingActive['isAlreadyBooked']);
        $this->assertNotEmpty($userBookingActive['roomActiveBooking']);
    }

    public function testGetKostLevelGoldPlusAndOyoFilter(): void
    {
        // create kost level data
        factory(KostLevel::class)->create([
            'name' => 'Mamikos Goldplus 1'
        ]);
        // run test
        $result = $this->repository->getKostLevelGoldPlusAndOyoFilter();
        $this->assertIsArray($result);
    }

    public function testCreateBookingPricingSuccess()
    {
        // prepare data
        $room = factory(Room::class)->create([
            'price_monthly' => 300000,
        ]);
        $bookingDesigner = factory(BookingDesigner::class)->create([
            'designer_id' => $room->id,
            'type' => BookingDesigner::BOOKING_TYPE_MONTHLY
        ]);
        $flashSale = factory(FlashSale::class)->create();

        // create booking user
        $bookingUser = factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesigner->id,
            'stay_duration' => 2,
            'rent_count_type' => 'monthly'
        ]);

        $bookingUserRoom = factory(BookingUserRoom::class)->create([
            'booking_user_id' => $bookingUser->id
        ]);

        // create booking discount
        factory(BookingDiscount::class)->create([
            'designer_id' => $room->id,
            'price' => 300000,
            'price_type' => 'monthly',
            'markup_type' => 'percentage',
            'markup_value' => 20,
            'discount_type' => 'percentage',
            'discount_value' => 10,
            'discount_source' => 'mamikos_and_owner',
            'discount_type_mamikos' => 'percentage',
            'discount_value_mamikos' => 5,
            'discount_type_owner' => 'percentage',
            'discount_value_owner' => 5,
            'is_active' => 1
        ]);

        // run test
        $result = $this->repository->createBookingPricing($bookingUser, $flashSale, $bookingUserRoom);
        $this->assertEquals($bookingUser->id, $result->id);
        $this->assertNotEquals(0, $result->total_price);
        $this->assertNotEquals(0, $result->price);
        $this->assertNotEquals(0, $result->mamikos_discount_value);
        $this->assertNotEquals(0, $result->owner_discount_value);
    }

    public function testGetExpiredBySlaThree()
    {
        $contract = factory(MamipayContract::class)->create();
        $bookingUser = factory(BookingUser::class)->create([
            'checkin_date' => Carbon::today(),
            'rent_count_type' => 'monthly',
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'contract_id' => $contract->id
        ]);

        factory(MamipayInvoice::class)->create([
            'contract_id' => $bookingUser->contract_id
        ]);

        $bookingStatus = factory(BookingStatus::class)->create([
            'booking_user_id' => $bookingUser->id,
            'status' => BookingUser::BOOKING_STATUS_CONFIRMED,
            'created_at' => Carbon::today()
        ]);

        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_THREE,
            'created_at' => $bookingStatus->created_at
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_MORE_EQUAL_ONE
        ]);

        $result = $this->repository->getExpiredBySLA($bookingUser);
        $this->assertNotNull($result);
    }

    public function testGetExpiredBySlaTwo()
    {
        $bookingUser = factory(BookingUser::class)->create([
            'checkin_date' => Carbon::today(),
            'rent_count_type' => 'monthly',
            'status' => BookingUser::BOOKING_STATUS_BOOKED,
            'created_at' => Carbon::today()
        ]);

        $sla = factory(SLAVersion::class)->create([
            'sla_type' => SLAVersion::SLA_VERSION_TWO,
            'created_at' => $bookingUser->created_at
        ]);
        factory(SLAConfiguration::class)->create([
            'sla_version_id' => $sla->id,
            'case' => SLAConfiguration::CASE_EQUAL
        ]);

        $result = $this->repository->getExpiredBySLA($bookingUser);
        $this->assertNotNull($result);
    }

    public function testGetReminderNotificationSuccess(): void
    {
        // create data
        $bookingFieldCheck = 'created_at';
        $bookingStatusCheck = BookingUser::BOOKING_STATUS_BOOKED;
        $start = Carbon::now()->subMinutes(30);
        $end = Carbon::now()->addMinutes(30);
        factory(BookingUser::class)->create([
            'status' => $bookingStatusCheck,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        // run test
        $result = $this->repository->getReminderNotification($bookingFieldCheck, $bookingStatusCheck, $start, $end);
        $this->assertInstanceOf(BookingUser::class, $result->first());
    }

    public function testBackfillBookingRejectReasonIdIsNull()
    {
        $output = $this->repository->backfillBookingRejectReasonId();
        $this->assertNull($output);
    }

    public function testBackFillBookingRejectReasonIdFromCancelReasonIsSuccess(): void
    {
        // prepare data
        $reason = 'Sudah mendapatkan kost lain';
        factory(BookingReject::class)->create([
            'type' => 'cancel',
            'description' => $reason,
            'is_active' => 1
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_reject_reason_id' => null,
            'cancel_reason' => $reason
        ]);

        // run test
        $this->repository->backfillBookingRejectReasonId();
        $bookingUserEntity = $bookingUserEntity->fresh();
        $this->assertNotNull($bookingUserEntity->booking_reject_reason_id);
    }

    public function testBackFillBookingRejectReasonIdFromRejectReasonIsSuccess(): void
    {
        // prepare data
        $reason = 'Kost penuh';
        factory(BookingReject::class)->create([
            'type' => 'reject',
            'description' => $reason,
            'is_active' => 1
        ]);
        $bookingUserEntity = factory(BookingUser::class)->create([
            'booking_reject_reason_id' => null,
            'reject_reason' => $reason
        ]);

        // run test
        $this->repository->backfillBookingRejectReasonId();
        $bookingUserEntity = $bookingUserEntity->fresh();
        $this->assertNotNull($bookingUserEntity->booking_reject_reason_id);
    }

    public function testGetOwnerBookingNotificationUnreadFalse(): void
    {
        $data = $this->repository->getOwnerBookingNotificationUnread(100012994, 'booking_request');
        // run test
        $this->assertSame(0, $data);
    }

    public function testGetOwnerBookingNotificationUnreadTrue(): void
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        factory(App\Entities\User\Notification::class)->create([
            'user_id' => $userEntity->id,
            'type' => 'booking_request',
            'read' => false
        ]);
        $data = $this->repository->getOwnerBookingNotificationUnread($userEntity->id, 'booking_request');
        // run test
        $this->assertNotSame(0, $data);
    }

    public function testGetTotalOwnerBookingWithStatusEmpty(): void
    {
        $data = $this->repository->getTotalOwnerBookingWithStatus(BookingUser::BOOKING_STATUS_BOOKED, []);
        // run test
        $this->assertSame(0, $data);
    }

    public function testGetTotalOwnerBookingWithStatusSuccess(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        $bookingDesignerEntity = factory(BookingDesigner::class)->create([
            'designer_id' => $roomEntity->id
        ]);
        factory(BookingUser::class)->create([
            'booking_designer_id' => $bookingDesignerEntity->id,
            'status' => BookingUser::BOOKING_STATUS_BOOKED
        ]);

        $bookingDesignerIds[] = $bookingDesignerEntity->id;

        $data = $this->repository->getTotalOwnerBookingWithStatus(BookingUser::BOOKING_STATUS_BOOKED, $bookingDesignerIds);
        // run test
        $this->assertNotSame(0, $data);
    }

    public function testGetProcessTimeBySongIdIsEmpty(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->make();

        // run data
        $data = $this->repository->getProcessTimeBySongId($roomEntity->song_id);
        $this->assertTrue($data->isEmpty());
    }

    public function testGetProcessTimeBySongIdIsNotEmpty(): void
    {
        // prepare data
        $roomEntity = factory(Room::class)->create();
        factory(BookingUser::class)->create([
            'designer_id' => $roomEntity->song_id
        ]);

        // run data
        $data = $this->repository->getProcessTimeBySongId($roomEntity->song_id);
        $this->assertFalse($data->isEmpty());
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}
