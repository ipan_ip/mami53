<?php
namespace App\Repositories\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Notif\Category;
use App\Entities\Room\Room;
use App\Entities\User\UserDesignerViewHistory;
use App\Repositories\Promoted\UserAdViewRepositoryEloquentTest;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class BookingUserDraftRepositoryEloquentTest extends MamiKosTestCase
{
    
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingUserDraftRepositoryEloquent::class);
    }

    public function testCreateOrUpdateSuccess()
    {
        // params create
        $createParams = [
            'user_id' => 1,
            'designer_id' => 1,
            'read' => false,
            'status' => BookingUserDraft::DRAFT_CREATED
        ];

        // params update
        $updateParams = [
            'user_id' => 1,
            'designer_id' => 1,
            'read' => true,
            'status' => BookingUserDraft::DRAFT_CREATED
        ];

        // create data
        $create = $this->repository->createOrUpdate($createParams);

        // update data
        $update = $this->repository->createOrUpdate($updateParams);

        // run test
        $this->assertEquals(1, $create->user_id);
        $this->assertEquals(1, $create->designer_id);
        $this->assertEquals(BookingUserDraft::DRAFT_CREATED, $create->status);
        $this->assertFalse($create->read);

        $this->assertEquals(1, $update->user_id);
        $this->assertEquals(1, $update->designer_id);
        $this->assertEquals(BookingUserDraft::DRAFT_CREATED, $update->status);
        $this->assertTrue($update->read);

        // make sure create or update have same id
        $this->assertEquals($create->id, $update->id);
    }

    public function testTotalOfDraftWithTrashedAndStatus()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'status' => BookingUserDraft::DRAFT_CREATED,
            'deleted_at' => now()
        ]);

        // run test
        $totalOfDraft = $this->repository->totalOfDraftWithTrashedAndStatus($bookingUserDraftEntity, [BookingUserDraft::DRAFT_CREATED, BookingUserDraft::ALREADY_BOOKING]);
        $this->assertEquals(1, $totalOfDraft);
    }

    public function testViewedRoomSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();

        // run test
        $response = $this->repository->viewedRoom($userEntity, $roomEntity);
        $this->assertInstanceOf(UserDesignerViewHistory::class, $response);
        $this->assertEquals($userEntity->id, $response->user_id);
        $this->assertEquals($roomEntity->id, $response->designer_id);
        $this->assertNotNull($response->viewed_at);

    }

    public function testGetDraftByUserAndRoomNull()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();

        // run test
        $response = $this->repository->getDraftByUserAndRoom($userEntity, $roomEntity);
        $this->assertNull($response);
    }

    public function testGetDraftByUserAndRoomSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        $bookingUserEntity = factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);

        // run test
        $response = $this->repository->getDraftByUserAndRoom($userEntity, $roomEntity);
        $this->assertEquals($bookingUserEntity->id, $response->id);
        $this->assertEquals($roomEntity->id, $response->designer_id);
        $this->assertEquals($userEntity->id, $response->user_id);
        $this->assertEquals(BookingUserDraft::DRAFT_CREATED, $response->status);
    }

    public function testGetHomepageShortcutDataIsNull()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->getHomepageShortcutData($userEntity);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('total', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals(0, $data['total']);
        $this->assertNull($data['data']);
    }

    public function testGetHomepageShortcutDataSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create([
            'room_available' => 5
        ]);
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);

        // run test
        $data = $this->repository->getHomepageShortcutData($userEntity);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('total', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals(1, $data['total']);
        $this->assertInstanceOf(BookingUserDraft::class, $data['data']);
    }

    public function testCreateNotificationCenterIsNull()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();

        $notifData = [];
        $notifData['user_id']       = $userEntity->id;
        $notifData['title']         = 'Draft booking berhasil ditambahkan';
        $notifData['type']          = 'booking_draft';
        $notifData['read']          = 'false';
        $notifData['url']           = '/booking';
        $notifData['schema']        = 'bang.kerupux.com://main_search';
        $notifData['designer_id']   = $roomEntity->id;

        // run test
        $data = $this->repository->createNotificationCenter($notifData);
        $this->assertNull($data);
    }

    public function testCreateNotificationCenterSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        $categoryEntity = factory(Category::class)->create(['name' => 'booking']);

        $notifData = [];
        $notifData['user_id']       = $userEntity->id;
        $notifData['title']         = 'Draft booking berhasil ditambahkan';
        $notifData['type']          = 'booking_draft';
        $notifData['read']          = 'false';
        $notifData['url']           = '/booking';
        $notifData['schema']        = 'bang.kerupux.com://main_search';
        $notifData['designer_id']   = $roomEntity->id;

        // run test
        $data = $this->repository->createNotificationCenter($notifData);
        $this->assertEquals($userEntity->id, $data->user_id);
        $this->assertEquals($roomEntity->id, $data->designer_id);
        $this->assertEquals($categoryEntity->id, $data->category_id);
    }

    public function testFindDraftIsNull()
    {
        // run test
        $data = $this->repository->findDraft(1);
        $this->assertNull($data);
    }

    public function testFindDraftSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create([
            'room_available' => 5
        ]);
        $bookingUserDraftEntity = factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);

        // run test
        $data = $this->repository->findDraft($bookingUserDraftEntity->id);
        $this->assertInstanceOf(BookingUserDraft::class, $data);
        $this->assertEquals($bookingUserDraftEntity->id, $data->id);
        $this->assertEquals($userEntity->id, $data->user_id);
        $this->assertEquals($roomEntity->id, $data->designer_id);
        $this->assertEquals($bookingUserDraftEntity->status, $data->status);
    }

    public function testListingViewedAndDraftIsEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $params = [];
        $params['limit'] = 10;
        // run test
        $data = $this->repository->listingViewedAndDraft($userEntity, $params);
        $this->assertInstanceOf(LengthAwarePaginator::class, $data);
        $this->assertEquals(0, $data->total());
        $this->assertEquals($params['limit'], $data->perPage());
    }

    public function testListingViewedAndDraftSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create([
            'room_available' => 5
        ]);
        factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);

        $params = [];
        $params['limit'] = 5;
        // run test
        $data = $this->repository->listingViewedAndDraft($userEntity, $params);
        $this->assertInstanceOf(LengthAwarePaginator::class, $data);
        $this->assertIsArray($data->items());
        $this->assertEquals($params['limit'], $data->perPage());
    }

    public function testUpdateWhenCreatedBookingIsNull()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();

        // run test
        $data = $this->repository->updateWhenCreatedBooking($userEntity, $roomEntity);
        $this->assertNull($data);
    }

    public function testUpdateWhenCreatedBookingSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'checkin' => Carbon::today()->addWeeks(1)->format('Y-m-d'),
            'status' => BookingUserDraft::DRAFT_CREATED
        ]);

        // run test
        $data = $this->repository->updateWhenCreatedBooking($userEntity, $roomEntity);
        $this->assertEquals($userEntity->id, $data->user_id);
        $this->assertEquals($roomEntity->id, $data->designer_id);
        $this->assertEquals(BookingUserDraft::ALREADY_BOOKING, $data->status);
    }

    public function testListingRoomHistoryIsEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $params = [];
        $params['limit'] = 10;
        // run test
        $data = $this->repository->listingRoomHistory($userEntity, $params);
        $this->assertInstanceOf(LengthAwarePaginator::class, $data);
        $this->assertEquals(0, $data->total());
        $this->assertEquals($params['limit'], $data->perPage());
    }

    public function testListingRoomHistorySuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
        ]);

        $params = [];
        $params['limit'] = 5;
        // run test
        $data = $this->repository->listingRoomHistory($userEntity, $params);
        $this->assertInstanceOf(LengthAwarePaginator::class, $data);
        $this->assertIsArray($data->items());
        $this->assertEquals($params['limit'], $data->perPage());
    }

    public function testTotalOfDraftNotYetSeenEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->totalOfDraftNotYetSeen($userEntity);
        $this->assertEquals(0, $data);
    }

    public function testTotalOfDraftNotYetSeenSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingUserDraft::DRAFT_CREATED,
            'read' => false
        ]);

        // run test
        $data = $this->repository->totalOfDraftNotYetSeen($userEntity);
        $this->assertEquals(1, $data);
    }

    public function testSetDraftHasBeenReadEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->setDraftHasBeenRead($userEntity);
        $this->assertEquals(0, $data);
    }

    public function testSetDraftHasBeenReadSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(BookingUserDraft::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'status' => BookingUserDraft::DRAFT_CREATED,
            'read' => false
        ]);

        // run test
        $data = $this->repository->setDraftHasBeenRead($userEntity);
        $this->assertNotEquals(0, $data);
    }

    public function testTotalOfNewLastSeenEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->totalOfNewLastSeen($userEntity);
        $this->assertEquals(0, $data);
    }

    public function testTotalOfNewLastSeenSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'read' => false
        ]);

        // run test
        $data = $this->repository->totalOfNewLastSeen($userEntity);
        $this->assertEquals(1, $data);
    }

    public function testSetNewLastSeenToReadedEmpty()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->setNewLastSeenToReaded($userEntity);
        $this->assertEquals(0, $data);
    }

    public function testSetNewLastSeenToReadedSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
            'read' => false
        ]);

        // run test
        $data = $this->repository->setNewLastSeenToReaded($userEntity);
        $this->assertNotEquals(0, $data);
    }

    public function testFindHistoryViewedIsNull()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->findHistoryViewed($userEntity, 1);
        $this->assertNull($data);
    }

    public function testFindHistoryViewedSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        $userDesignerViewHistoryEntity = factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
        ]);

        // run test
        $data = $this->repository->findHistoryViewed($userEntity, $roomEntity->id);
        $this->assertInstanceOf(UserDesignerViewHistory::class, $data);
        $this->assertEquals($userEntity->id, $data->user_id);
        $this->assertEquals($roomEntity->id, $data->designer_id);
        $this->assertEquals($userDesignerViewHistoryEntity->read, $data->read);
    }

    public function testDeleteHistoryViewedNotFound()
    {
        // prepare data
        $userEntity = factory(User::class)->create();

        // run test
        $data = $this->repository->deleteHistoryViewed($userEntity, 1);
        $this->assertEquals(0, $data);
    }

    public function testDeleteHistoryViewedSuccess()
    {
        // prepare data
        $userEntity = factory(User::class)->create();
        $roomEntity = factory(Room::class)->create();
        factory(UserDesignerViewHistory::class)->create([
            'user_id' => $userEntity->id,
            'designer_id' => $roomEntity->id,
        ]);

        // run test
        $data = $this->repository->deleteHistoryViewed($userEntity, $roomEntity->id);
        $this->assertNotEquals(0, $data);
    }
}
