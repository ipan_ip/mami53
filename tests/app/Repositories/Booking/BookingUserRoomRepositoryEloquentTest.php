<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerRoom;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserRoom;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;

class BookingUserRoomRepositoryTest extends MamiKosTestCase
{
    use WithFaker;
    private $repository;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingUserRoomRepository::class);
        $this->setUpFaker();
    }

    public function testModel()
    {
        $this->assertEquals($this->repository->model(), BookingUserRoom::class);
    }

    public function testGetOccupiedRooms()
    {
        $bookingdDesigner = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class, 5)->create([
            'booking_designer_id' => $bookingdDesigner->id,
            'is_active' => 1,
        ]);
        $bookingUserRoom = factory(BookingUserRoom::class)->create([
            'booking_user_id' => factory(BookingUser::class)->create([
                'status' => $this->faker->randomElement([
                    BookingUser::BOOKING_STATUS_BOOKED,
                    BookingUser::BOOKING_STATUS_CONFIRMED,
                    BookingUser::BOOKING_STATUS_PAID,
                    BookingUser::BOOKING_STATUS_VERIFIED,
                    BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED
                ])
            ]),
            'booking_designer_room_id' => $bookingDesignerRoom[0]->id,
            'checkin_date' => Carbon::now(),
            'checkout_date' => Carbon::now()->addDay(5),
        ]);

        $result = $this->repository->getOccupiedRooms(Carbon::now()->subDay(2), Carbon::now()->addDay(7), [$bookingDesignerRoom[0]->id], ['booking_designer_room']);
        $this->assertEquals($bookingUserRoom->id, $result[0]->id);
    }

    public function testcheckAvailabilityWithActiveRoomIsGreaterThanOccupideRoom()
    {
        $bookingdDesigner = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class, 5)->create([
            'booking_designer_id' => $bookingdDesigner->id,
            'is_active' => 1,
        ]);
        $bookingUserRoom = factory(BookingUserRoom::class)->create([
            'booking_user_id' => factory(BookingUser::class)->create([
                'status' => $this->faker->randomElement([
                    BookingUser::BOOKING_STATUS_BOOKED,
                    BookingUser::BOOKING_STATUS_CONFIRMED,
                    BookingUser::BOOKING_STATUS_PAID,
                    BookingUser::BOOKING_STATUS_VERIFIED,
                    BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED
                ])
            ]),
            'booking_designer_room_id' => $bookingDesignerRoom[0]->id,
            'checkin_date' => Carbon::now(),
            'checkout_date' => Carbon::now()->addDay(5),
        ]);

        $result = $this->repository->checkAvailability($bookingdDesigner, Carbon::now()->subDay(2), Carbon::now()->addDay(7));
        $this->assertTrue($result);
    }

    public function testcheckAvailabilityWithActiveRoomIsNotGreaterThanOccupideRoom()
    {
        $bookingdDesigner = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class)->create([
            'booking_designer_id' => $bookingdDesigner->id,
            'is_active' => 1,
        ]);
        $bookingUserRoom = factory(BookingUserRoom::class)->create([
            'booking_user_id' => factory(BookingUser::class)->create([
                'status' => $this->faker->randomElement([
                    BookingUser::BOOKING_STATUS_BOOKED,
                    BookingUser::BOOKING_STATUS_CONFIRMED,
                    BookingUser::BOOKING_STATUS_PAID,
                    BookingUser::BOOKING_STATUS_VERIFIED,
                    BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED
                ])
            ]),
            'booking_designer_room_id' => $bookingDesignerRoom->id,
            'checkin_date' => Carbon::now(),
            'checkout_date' => Carbon::now()->addDay(5),
        ]);

        $result = $this->repository->checkAvailability($bookingdDesigner, Carbon::now()->subDay(2), Carbon::now()->addDay(7));
        $this->assertFalse($result);
    }

    public function testAssignBookingRoomWithAssignRoomIsGreaterThanRoomTotal()
    {
        $bookingdDesigner = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class, 5)->create([
            'booking_designer_id' => $bookingdDesigner->id,
            'is_active' => 1,
        ]);
        $bookingUser = factory(BookingUser::class)->create([
            'checkin_date' => Carbon::now(),
            'checkout_date' => Carbon::now()->addDay(5),
            'room_total' => 1,
        ]);

        $result = $this->repository->assignBookingRoom($bookingUser, $bookingdDesigner);

        $expectedResult = [
            'booking_user_id' => $bookingUser->id,
            'booking_designer_room_id' => $bookingDesignerRoom[0]->id,
        ];

        $this->assertDatabaseHas('booking_user_room', $expectedResult);
        $this->assertCount(1, $result);
        $this->assertEquals($expectedResult['booking_user_id'], $result[0]->booking_user_id);
        $this->assertEquals($expectedResult['booking_designer_room_id'], $result[0]->booking_designer_room_id);
    }

    public function testAssignBookingRoomWithAssignRoomIsNotGreaterThanRoomTotal()
    {
        $bookingdDesigner = factory(BookingDesigner::class)->create();
        $bookingDesignerRoom = factory(BookingDesignerRoom::class, 5)->create([
            'booking_designer_id' => $bookingdDesigner->id,
            'is_active' => 0,
        ]);
        $bookingUser = factory(BookingUser::class)->create([
            'checkin_date' => Carbon::now(),
            'checkout_date' => Carbon::now()->addDay(5),
            'room_total' => 1,
        ]);

        $result = $this->repository->assignBookingRoom($bookingUser, $bookingdDesigner);

        $expectedResult = [
            'booking_user_id' => $bookingUser->id,
            'booking_designer_room_id' => $bookingDesignerRoom[0]->id,
        ];

        $this->assertFalse($result);
        $this->assertDatabaseMissing('booking_user_room', $expectedResult);
    }
}
