<?php

namespace App\Repositories\Booking;

use App\Test\MamiKosTestCase;
use App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepositoryEloquent;
use App\Entities\Booking\BookingAcceptanceRate;

class BookingAcceptanceRateRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(BookingAcceptanceRateRepositoryEloquent::class);
    }

    public function testModel()
    {
        $this->assertEquals('App\Entities\Booking\BookingAcceptanceRate', $this->repository->model());
    }

    public function testActivate()
    {
        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => 99,
            'is_active' => 0, // set non active
            'average_time' => 120,
            'rate' => 80,
        ]);
        $bar2 = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => 222,
            'is_active' => 0, // set non active
            'average_time' => 80,
            'rate' => 90,
        ]);

        $this->repository->activate();

        $this->assertDatabaseHas('booking_acceptance_rate', [
            'id' => $bar->id,
            'is_active' => 1,
        ]);

        $this->assertDatabaseHas('booking_acceptance_rate', [
            'id' => $bar2->id,
            'is_active' => 1,
        ]);
    }

    public function testDeactivate()
    {
        $bar = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => 33,
            'is_active' => 1, // set active
            'average_time' => 120,
            'rate' => 80,
        ]);
        $bar2 = factory(BookingAcceptanceRate::class)->create([
            'designer_id' => 44,
            'is_active' => 1, // set active
            'average_time' => 80,
            'rate' => 90,
        ]);

        $this->repository->deactivate();

        $this->assertDatabaseHas('booking_acceptance_rate', [
            'id' => $bar->id,
            'is_active' => 0,
        ]);

        $this->assertDatabaseHas('booking_acceptance_rate', [
            'id' => $bar2->id,
            'is_active' => 0,
        ]);
    }

}

