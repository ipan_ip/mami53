<?php

namespace App\Repositories\Promoted;

use App\User;
use App\Entities\Device\UserDevice;
use App\Entities\Promoted\UserAdView;
use App\Repositories\Promoted\UserAdViewRepositoryEloquent;
use App\Test\MamiKosTestCase;

class UserAdViewRepositoryEloquentTest extends MamiKosTestCase
{
    
    protected $userAdViewRepo;
    protected $roomId;
    protected $userId;
    protected $deviceId;

    const TYPE_CLICK = 'click';
    const SESSION_IDENTIFIER = 'randomadsession';

    public function setUp() : void
    {
        parent::setUp();

        $this->userAdViewRepo = app()->make(UserAdViewRepositoryEloquent::class);
        $this->roomId = 10;
        app()->device = null;
        app()->user = null;
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testGetTodayRoomWithIdentifierSession()
    {
        $this->setIdentitySession();
        $dummyUserAdView = factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'session',
            'identifier' => self::SESSION_IDENTIFIER,
            'deleted_at' => null
        ]);

        $userAdViews = $this->userAdViewRepo->getTodayRoom($this->roomId, self::TYPE_CLICK);
        $this->assertEquals($userAdViews, [$dummyUserAdView->toArray()]);
    }

    public function testGetTodayRoomWithIdentifierUser()
    {
        $this->setIdentityUser();
        $dummyUserAdView = factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'user',
            'identifier' => $this->userId,
            'deleted_at' => null
        ]);

        $userAdViews = $this->userAdViewRepo->getTodayRoom($this->roomId, self::TYPE_CLICK);
        $this->assertEquals($userAdViews, [$dummyUserAdView->toArray()]);
    }

    public function testGetTodayRoomWithIdentifierDevice()
    {
        $this->setIdentityDevice();
        $dummyUserAdView = factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'device',
            'identifier' => $this->deviceId,
            'deleted_at' => null
        ]);

        $userAdViews = $this->userAdViewRepo->getTodayRoom($this->roomId, self::TYPE_CLICK);
        $this->assertEquals($userAdViews, [$dummyUserAdView->toArray()]);
    }

    public function testGetTodayRoomWithNoIdentifier()
    {
        $this->setNoIdentity();
        factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'session',
            'identifier' => self::SESSION_IDENTIFIER,
            'deleted_at' => null
        ]);

        $userAdViews = $this->userAdViewRepo->getTodayRoom($this->roomId, self::TYPE_CLICK);
        $this->assertEquals($userAdViews, []);
    }

    public function testCanIncreaseView()
    {
        $this->setIdentitySession();
        factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'session',
            'identifier' => self::SESSION_IDENTIFIER,
            'deleted_at' => null
        ]);
        $canIncreaseView = $this->userAdViewRepo->canIncreaseView($this->roomId, self::TYPE_CLICK);
        $this->assertTrue($canIncreaseView);
    }

    public function testCanIncreaseViewWithNoIdentifier()
    {
        $this->setNoIdentity();
        factory(UserAdView::class)->create([
            'designer_id' => $this->roomId,
            'type' => self::TYPE_CLICK,
            'identifier_type' => 'session',
            'identifier' => self::SESSION_IDENTIFIER,
            'deleted_at' => null
        ]);
        $canIncreaseView = $this->userAdViewRepo->canIncreaseView($this->roomId, self::TYPE_CLICK);
        $this->assertFalse($canIncreaseView);
    }

    public function testSetTodayView()
    {
        $this->setIdentitySession();
        $testUserAdView = $this->userAdViewRepo->setTodayView($this->roomId, self::TYPE_CLICK);
        $userAdViews = $this->userAdViewRepo->getTodayRoom($this->roomId, self::TYPE_CLICK);
        $this->assertEquals($userAdViews[0]['designer_id'], $testUserAdView->designer_id);
        $this->assertEquals($userAdViews[0]['type'], $testUserAdView->type);
        $this->assertEquals($userAdViews[0]['last_view_time'], $testUserAdView->last_view_time);
    }

    private function setNoIdentity()
    {
        $_COOKIE['adsession'] = null;
    }

    private function setIdentitySession()
    {
        $_COOKIE['adsession'] = self::SESSION_IDENTIFIER;
    }

    private function setIdentityUser()
    {
        $user = factory(User::class)->create();
        app()->user = $user;
        $this->userId = $user->id;
    }

    private function setIdentityDevice()
    {
        $userDevice = factory(UserDevice::class)->create();
        app()->device = $userDevice;
        $this->deviceId = $userDevice->id;
    }
}
