<?php 

namespace App\Repositories\Promoted;

use App\Test\MamiKosTestCase;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Promoted\AdsDisplayTracker;
use App\User;
use App;
use Carbon\Carbon;
use StatsLib;

class AdsDisplayTrackerRepositoryEloquentTest extends MamiKosTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = App::make(AdsDisplayTrackerRepository::class);
        
        $mockDate = Carbon::create(2020, 5, 4, 8, 14);
        Carbon::setTestNow($mockDate);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        // Reset test carbon mock
        Carbon::setTestNow();
    }

    private function generateAdDisplayTracker(Room $room, $createdTime = null, $displayCount = 1)
    {
        $now = Carbon::now();
        $displayData = [
            'designer_id' => $room->song_id,
            'display_count' => $displayCount,
            'created_at' => is_null($createdTime) ? $now : $createdTime,
            'updated_at' => is_null($createdTime) ? $now : $createdTime
        ];

        factory(AdsDisplayTracker::class)->create($displayData);

        return true;
    }

    public function testGetRoomDisplayedStatisticToday()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        $startDate = $now->copy()->startOfDay();
        $date23PM = $startDate->copy()->subHour();
        $date3AM = $startDate->copy()->addHours(3);
        $date345AM = $startDate->copy()->addHours(3)->addMinutes(45);
        $validateDate23PM = $date23PM->format('Y-m-d H');
        $validateDate3AM = $date3AM->format('Y-m-d H');
        $validateDate1AM = $startDate->copy()->addHour()->format('Y-m-d H');

        // It will counted
        $this->generateAdDisplayTracker($room, $date3AM, 4);
        $this->generateAdDisplayTracker($room, $date345AM, 2);
        $this->generateAdDisplayTracker($room, $date23PM, 2);
        $this->generateAdDisplayTracker($room, $date23PM->addMinutes(30), 5);

        // not counted
        factory(AdsDisplayTracker::class)->create([
            'designer_id' => $room->song_id,
            'display_count' => 1,
            'created_at' => $now->subDays(2)
        ]);

        $statsOn12AM = $this->repository->getRoomsDisplayedStatistic($user, null, null, $startDate);
        $statsOn1AM = $this->repository->getRoomsDisplayedStatistic($user, null, null, $startDate->copy()->addHour());
        $stats = $this->repository->getRoomsDisplayedStatistic($user, null);

        $currentHour = $now->hour;
        if ($currentHour > 3) {
            $key = array_search($validateDate3AM, array_column($stats, 'date'));
            $this->assertEquals(6, $stats[$key]['value']);
        }
        
        // it will show until current hour
        if ($currentHour <= 23) {
            $this->assertEquals($currentHour + 1, count($stats));
        }
        // if current hour is 12 AM then stats only show 2 statistics, on 11 PM - 00 AM
        $this->assertEquals(2, count($statsOn12AM));
        $this->assertEquals($validateDate23PM, $statsOn12AM[0]['date']);
        // if current hour is 1 AM then stats only show 2 statistics, on 00 AM - 01 AM
        $this->assertEquals(2, count($statsOn1AM));
        $this->assertEquals($validateDate1AM, $statsOn1AM[1]['date']);

    }

    public function testGetRoomDisplayedStatisticYesterday()
    {
        $now = Carbon::now();
        $yesterday = $now->copy()->subDay();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);
        $validateDate9AM = $yesterday->copy()->startOfDay()->addHours(9);

        // not counted
        $this->generateAdDisplayTracker($room, null, 1);

        // It will counted
        $this->generateAdDisplayTracker($room, $validateDate9AM, 4);
        $this->generateAdDisplayTracker($room, $validateDate9AM->addMinutes(23), 4);

        $stats = $this->repository->getRoomsDisplayedStatistic($user, StatsLib::RangeYesterday);

        $key = array_search($validateDate9AM->format('Y-m-d H'), array_column($stats, 'date'));
        $this->assertEquals(8, $stats[$key]['value']);

    }

    public function testGetRoomDisplayedStatistic7Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdDisplayTracker($room, null, 1);

        // It will counted
        $twoDaysBehind = $now->copy()->subDays(2);
        $this->generateAdDisplayTracker($room, $twoDaysBehind, 4);

        $twoDaysBehind = $now->copy()->subDays(2)->subHour();
        $this->generateAdDisplayTracker($room, $twoDaysBehind, 4);

        $stats = $this->repository->getRoomsDisplayedStatistic($user, StatsLib::Range7Days);

        $this->assertEquals(8, $stats[4]['value']);

    }

    public function testGetRoomDisplayedStatistic30Days()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // not counted
        $this->generateAdDisplayTracker($room, null, 1);

        // It will counted
        $twoDaysBehind = $now->copy()->subDays(12);
        $this->generateAdDisplayTracker($room, $twoDaysBehind, 4);

        $twoDaysBehind = $now->copy()->subDays(12)->subHour();
        $this->generateAdDisplayTracker($room, $twoDaysBehind, 6);

        $stats = $this->repository->getRoomsDisplayedStatistic($user, StatsLib::Range30Days);

        $this->assertEquals(10, $stats[17]['value']);

    }

    public function testGetRoomDisplayedStatisticShouldReturnEmpty()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();

        // not counted
        factory(AdsDisplayTracker::class)->create([
            'designer_id' => $room->song_id,
            'display_count' => 1,
        ]);

        $stats = $this->repository->getRoomsDisplayedStatistic($user, null);

        $this->assertEquals(0, count($stats));
    }

    public function testGetRoomDisplayedStatsSummary()
    {
        $now = Carbon::now();

        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "status" => "verified"
        ]);

        // It will counted
        $yesterday = $now->subHours(9);
        $this->generateAdDisplayTracker($room, $yesterday, 4);

        $yesterday = $now->subHours(9);
        $this->generateAdDisplayTracker($room, $yesterday, 2);

        $stats = $this->repository->getRoomDisplayedStatisticSummary($user, StatsLib::Range30Days);

        $this->assertEquals(6, $stats['value']);
    }
}
