<?php

namespace App\Repositories;

use App\Entities\Media\Media;
use App\Entities\Room\Element\TagType;
use App\Test\MamiKosTestCase;
use App\Entities\Room\Element\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use phpmock\MockBuilder;
use function factory;
use Illuminate\Http\JsonResponse;

class TagRepositoryEloquentTest extends MamiKosTestCase
{
    protected $tag;
    protected $repository;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repository = $this->app->make(TagRepositoryEloquent::class);
    }

    public function testModel()
    {
        $this->assertEquals(Tag::class, $this->repository->model());
    }

    // public function testIndexWithoutTypeInputAndCategorized()
    // {
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_bath']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_room']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_park']);

    //     $result = $this->repository->index()->getData()->tags;
    //     $this->assertEquals(count($result), count($tag));
    //     $this->assertEquals($result[0]->tag_id, $tag[0]->id);
    //     $this->assertEquals($result[1]->tag_id, $tag[1]->id);
    //     $this->assertEquals($result[2]->tag_id, $tag[2]->id);
    // }

    // public function testIndexWithTypeInput()
    // {
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_near']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_bath']);

    //     request()->merge(['type' => 'fac_near']);
    //     $result = $this->repository->index()->getData()->tags;

    //     $this->assertEquals(count($result), 1);
    //     $this->assertEquals($result[0]->tag_id, $tag[0]->id);
    // }

    // public function testIndexWithCategorizedInput()
    // {
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_bath']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_room']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_park']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_share']);
    //     $tag[] = factory(Tag::class)->create(['type' => 'fac_near']);

    //     request()->merge(['categorized' => true]);
    //     $result = $this->repository->index()->getData()->tags;

    //     // Assert All Category 5
    //     $this->assertEquals(count($result), 5);

    //     // Assert category fac_bath
    //     $this->assertEquals($result[0]->tags[0]->tag_id, $tag[0]->id);
    //     // Assert category fac_room
    //     $this->assertEquals($result[1]->tags[0]->tag_id, $tag[1]->id);
    //     // Assert category fac_park
    //     $this->assertEquals($result[2]->tags[0]->tag_id, $tag[2]->id);
    //     // Assert category fac_share
    //     $this->assertEquals($result[3]->tags[0]->tag_id, $tag[3]->id);
    //     // Assert category fac_near
    //     $this->assertEquals($result[4]->tags[0]->tag_id, $tag[4]->id);
    // }

    public function testGetMarketplaceFacilities()
    {
        $result = $this->repository->getMarketplaceFacilities();

        $expectedResult = [
            'price_range' => [
                'event' => 'Price',
                'price_min' => ['0', '50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000'],
                'price_max' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000', '2.000.000', '50.000.000', '500.000.000'],
                'price_min_event' => ['Filter_Min_0', 'Filter_Min_50', 'Filter_Min_100', 'Filter_Min_200', 'Filter_Min_300', 'Filter_Min_400', 'Filter_Min_500', 'Filter_Min_600', 'Filter_Min_700', 'Filter_Min_800', 'Filter_Min_900', 'Filter_Min_1000', 'Filter_Min_1500'],
                'price_max_event' => ['Filter_Max_50', 'Filter_Max_100', 'Filter_Max_200', 'Filter_Max_300', 'Filter_Max_400', 'Filter_Max_500', 'Filter_Max_600', 'Filter_Max_700', 'Filter_Max_800', 'Filter_Max_900', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_50000', 'Filter_Max_500000'],
            ]
        ];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetApartmentFacilitiesWithVFalse()
    {
        $result = $this->repository->getApartmentFacilities();

        $expectedResult = [
            'unit_type' => [
                [
                    'fac_id' => 0,
                    'fac_name' => 'Semua',
                    'fac_event' => 'All'
                ],
                [
                    'fac_id' => 1,
                    'fac_name' => '1-Room Studio',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 2,
                    'fac_name' => '1 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 3,
                    'fac_name' => '2 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 4,
                    'fac_name' => '3 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 5,
                    'fac_name' => '4 BR',
                    'fac_event' => ''
                ],
            ],
            'furnished' => [
                [
                    'fac_id' => 0,
                    'fac_name' => 'Semua',
                    'fac_event' => 'all'
                ],
                [
                    'fac_id' => 1,
                    'fac_name' => 'Furnished',
                    'fac_event' => '1'
                ],
                [
                    'fac_id' => 3,
                    'fac_name' => 'Semi Furnished',
                    'fac_event' => '2'
                ],
                [
                    'fac_id' => 2,
                    'fac_name' => 'Not Furnished',
                    'fac_event' => '0'
                ]
            ],
            'price_range' => [
                'event' => 'Price',
                'price_min' => [
                    '500.000',
                    '1.000.000',
                    '2.500.000',
                    '2.000.000'
                ],
                'price_max' => [
                    '5.000.000',
                    '10.000.000',
                    '15.000.000',
                    '20.000.000',
                    '25.000.000',
                    '30.000.000',
                    '35.000.000',
                    '40.000.000',
                    '45.000.000',
                    '50.000.000',
                ]
            ]
        ];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetApartmentFacilitiesWithVTrue()
    {
        $result = $this->repository->getApartmentFacilities(true);

        $price_range = [
            [
                'id' => 1,
                'table' => 'Harian',
                'event' => 'Filter_Day',
                'price_min' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000'],
                'price_max' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000', '2.000.000'],
                'price_min_event' => ['Filter_Min_50', 'Filter_Min_100', 'Filter_Min_200', 'Filter_Min_300', 'Filter_Min_400', 'Filter_Min_500', 'Filter_Min_600', 'Filter_Min_700', 'Filter_Min_800', 'Filter_Min_900', 'Filter_Min_1000', 'Filter_Min_1500'],
                'price_max_event' => ['Filter_Max_50', 'Filter_Max_100', 'Filter_Max_200', 'Filter_Max_300', 'Filter_Max_400', 'Filter_Max_500', 'Filter_Max_600', 'Filter_Max_700', 'Filter_Max_800', 'Filter_Max_900', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000'],
            ],
            [
                'id' => 2,
                'table' => 'Mingguan',
                'event' => 'Filter_Week',
                'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000'],
                'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000'],
            ],
            [
                'id' => 3,
                'table' => 'Bulanan',
                'event' => 'Filter_Month',
                'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000', '15.000.000', '20.000.000'],
                'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000', 'Filter_Max_15000', 'Filter_Max_20000'],
            ],
            [
                'id' => 4,
                'table' => 'Tahunan',
                'event' => 'Filter_Year',
                'price_min' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000'],
                'price_max' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000', '30.000.000', '50.000.000'],
                'price_min_event' => ['Filter_Min_1000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000', 'Filter_Min_9000', 'Filter_Min_12000', 'Filter_Min_15000', 'Filter_Min_20000', 'Filter_Min_25000'],
                'price_max_event' => ['Filter_Max_1000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_9000', 'Filter_Max_12000', 'Filter_Max_15000', 'Filter_Max_20000', 'Filter_Max_25000', 'Filter_Max_30000', 'Filter_Max_50000', 'Filter_Max_20000'],
            ]
        ];
        $expectedResult = [
            'unit_type' => [
                [
                    'fac_id' => 0,
                    'fac_name' => 'Semua',
                    'fac_event' => 'All'
                ],
                [
                    'fac_id' => 1,
                    'fac_name' => '1-Room Studio',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 2,
                    'fac_name' => '1 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 3,
                    'fac_name' => '2 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 4,
                    'fac_name' => '3 BR',
                    'fac_event' => ''
                ],
                [
                    'fac_id' => 5,
                    'fac_name' => '4 BR',
                    'fac_event' => ''
                ],
            ],
            'furnished' => [
                [
                    'fac_id' => 0,
                    'fac_name' => 'Semua',
                    'fac_event' => 'all'
                ],
                [
                    'fac_id' => 1,
                    'fac_name' => 'Furnished',
                    'fac_event' => '1'
                ],
                [
                    'fac_id' => 3,
                    'fac_name' => 'Semi Furnished',
                    'fac_event' => '2'
                ],
                [
                    'fac_id' => 2,
                    'fac_name' => 'Not Furnished',
                    'fac_event' => '0'
                ]
            ],
            'price_range' => $price_range
        ];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetVacancyFacilities()
    {
        $result = $this->repository->getVacancyFacilities();

        $expectedResult = [
            'type' => [
                [
                    'type_key' => 'all',
                    'type_value' => 'Semua Tipe'
                ],
                [
                    'type_key' => 'freelance',
                    'type_value' => 'Freelance'
                ],
                [
                    'type_key' => 'full-time',
                    'type_value' => 'Full-time'
                ],
                [
                    'type_key' => 'part-time',
                    'type_value' => 'Part-time'
                ],
                [
                    'type_key' => 'magang',
                    'type_value' => 'Magang'
                ],
            ],
            'educations' => [
                [
                    'education_key' => 'all',
                    'education_value' => 'Apa Saja'
                ],
                [
                    'education_key' => 'high_school',
                    'education_value' => 'SMA/SMK'
                ],
                [
                    'education_key' => 'diploma',
                    'education_value' => 'D1/D2/D3'
                ],
                [
                    'education_key' => 'bachelor',
                    'education_value' => 'Sarjana/S1'
                ],
                [
                    'education_key' => 'master',
                    'education_value' => 'Master/S2'
                ],
                [
                    'education_key' => 'doctor',
                    'education_value' => 'Doktor/S3'
                ],
                [
                    'education_key' => 'college',
                    'education_value' => 'Mahasiswa'
                ],
                [
                    'education_key' => 'fresh_grad',
                    'education_value' => 'Fresh Graduate'
                ],
            ],
            'sort' => [
                [
                    'sorting_key' => 'mamikos',
                    'sorting_value' => 'Via MamiKos'
                ],
                [
                    'sorting_key' => 'old',
                    'sorting_value' => 'Lowongan Lama'
                ],
                [
                    'sorting_key' => 'new',
                    'sorting_value' => 'Lowongan terbaru'
                ]
            ]
        ];

        $this->assertEquals($result, $expectedResult);
    }

    public function testGetCategorizedTags()
    {
        $tag1 = factory(Tag::class)->create(['type' => 'fac_bath']);
        $tag2 = factory(Tag::class)->create(['type' => 'fac_room']);
        $tag3 = factory(Tag::class)->create(['type' => 'fac_park']);
        $tag4 = factory(Tag::class)->create(['type' => 'fac_share']);
        $tag5 = factory(Tag::class)->create(['type' => 'fac_near']);
        $tag6 = factory(Tag::class)->create(['type' => 'fac_price']);
        $tag7 = factory(Tag::class)->create(['type' => 'keyword']);

        $result = $this->repository->getCategorizedTags();

        $expectedResult = [
            [
                'title' => 'Kamar Mandi',
                'type' => 'fac_bath',
                'tags' => [
                    [
                        'tag_id' => $tag1->id,
                        'name' => $tag1->name,
                        'type' => $tag1->type
                    ]
                ]
            ],
            [
                'title' => 'Fasilitas Kamar',
                'type' => 'fac_room',
                'tags' => [
                    [
                        'tag_id' => $tag2->id,
                        'name' => $tag2->name,
                        'type' => $tag2->type
                    ],
                    [
                        'tag_id' => $tag6->id,
                        'name' => $tag6->name,
                        'type' => $tag6->type
                    ]
                ]
            ],
            [
                'title' => 'Fasilitas Parkir',
                'type' => 'fac_park',
                'tags' => [
                    [
                        'tag_id' => $tag3->id,
                        'name' => $tag3->name,
                        'type' => $tag3->type
                    ]
                ]
            ],
            [
                'title' => 'Fasilitas Umum',
                'type' => 'fac_share',
                'tags' => [
                    [
                        'tag_id' => $tag4->id,
                        'name' => $tag4->name,
                        'type' => $tag4->type
                    ]
                ]
            ],
            [
                'title' => 'Akses Lingkungan',
                'type' => 'fac_near',
                'tags' => [
                    [
                        'tag_id' => $tag5->id,
                        'name' => $tag5->name,
                        'type' => $tag5->type
                    ]
                ]
            ],
            [
                'title' => 'Minimal Pembayaran',
                'type' => 'keyword',
                'tags' => [
                    [
                        'tag_id' => $tag7->id,
                        'name' => $tag7->name,
                        'type' => $tag7->type
                    ]
                ]
            ]
        ];
        $this->assertEquals($result, $expectedResult);
    }

    public function testListTagTypeAll()
    {
        $tag1 = factory(Tag::class)->create(['type' => 'gender']);
        $tag2 = factory(Tag::class)->create();
        $tag1 = factory(Tag::class)->create(['type' => 'category']);

        $response = $this->repository->listTag('all');

        $this->assertCount(3, $response);
    }

    public function testListTagTypeGender()
    {
        $tag1 = factory(Tag::class)->create(['type' => 'gender']);
        $tag2 = factory(Tag::class)->create(['type' => 'category']);

        $response = $this->repository->listTag('gender');

        $this->assertCount(1, $response);
        $this->assertEquals($tag1->name, $response[0]['name']);
        $this->assertEquals($tag1->id, $response[0]['tag_id']);
        $this->assertEquals($tag1->type, $response[0]['type']);
    }

    public function testListTagTypeCategory()
    {
        $tag1 = factory(Tag::class)->create(['type' => 'gender']);
        $tag2 = factory(Tag::class)->create(['type' => 'category']);
        $tag3 = factory(Tag::class)->create(['type' => 'category']);

        $response = $this->repository->listTag('category');

        $this->assertCount(2, $response);
        $this->assertEquals($tag2->name, $response[0]['name']);
        $this->assertEquals($tag2->id, $response[0]['tag_id']);
        $this->assertEquals($tag2->type, $response[0]['type']);
    }

    public function testListTagWithNameGaransiUangKembali()
    {
        factory(Tag::class)->create(['name' => 'garansi uang']);

        $response = $this->repository->listTag('all');

        $this->assertCount(0, $response);
    }

    public function testGetAllTags()
    {
        factory(Tag::class, 5)->create();
        factory(Tag::class)->create(['name' => 'garansi uang kembali']);

        $response = $this->repository->getAllTags('test');

        $this->assertCount(5, $response);
    }

    public function testGetEducationFormat()
    {
        $Vacancy = new \App\Entities\Vacancy\Vacancy;
        $result = $this->repository->getEducationFormat($Vacancy::EDUCATION_OPTION);
        $expectedResult = [
            [
                'education_key' => 'all',
                'education_value' => 'Apa Saja'
            ], [
                'education_key' => 'high_school',
                'education_value' => 'SMA/SMK'
            ], [
                'education_key' => 'diploma',
                'education_value' => 'D1/D2/D3'
            ], [
                'education_key' => 'bachelor',
                'education_value' => 'Sarjana/S1'
            ], [
                'education_key' => 'master',
                'education_value' => 'Master/S2'
            ], [
                'education_key' => 'doctor',
                'education_value' => 'Doktor/S3'
            ], [
                'education_key' => 'college',
                'education_value' => 'Mahasiswa'
            ], [
                'education_key' => 'fresh_grad',
                'education_value' => 'Fresh Graduate'
            ]
        ];
        $this->assertEquals($result, $expectedResult);
    }

    public function testReformatFacilities()
    {
        $facilities = [
            13 => ['AC', 'Filter_AC'],
            12 => ['TV', 'Filter_TV'],
            11 => ['Almari Pakaian', 'Filter_Almari'],
        ];

        $response = $this->repository->reformatFacilities($facilities);

        $this->assertCount(3, $response);
        $this->assertEquals($facilities[13][0], $response[0]['fac_name']);
        $this->assertEquals($facilities[13][1], $response[0]['fac_event']);
        $this->assertEquals($facilities[12][0], $response[1]['fac_name']);
        $this->assertEquals($facilities[12][1], $response[1]['fac_event']);
        $this->assertEquals($facilities[11][0], $response[2]['fac_name']);
        $this->assertEquals($facilities[11][1], $response[2]['fac_event']);
    }

    public function testGetFacilities()
    {
        $response = $this->repository->getFacilities();

        $this->assertCount(13, $response['facs']);
        $this->assertCount(3, $response['other_facs']);
        $this->assertCount(12, $response['time_facs']);
        $this->assertCount(4, $response['price_range']);
    }

    public function testGetFacilitiesForFilterWithTrueIsForFilterAttribute()
    {
        $tag1 = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 1
            ]
        );
        $tag2 = factory(Tag::class)->create(
            [
                'name' => 'fitness',
                'is_for_filter' => 1
            ]
        );
        $tag3 = factory(Tag::class)->create(
            [
                'name' => 'akses kunci 24 jam',
                'is_for_filter' => 1
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag1->id,
                'name' => 'fac_room'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag2->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag3->id,
                'name' => 'fac_share'
            ]
        );

        $response = $this->repository->getFacilitiesForFilter();

        $this->assertNotEquals([], $response['fac_share']);
        $this->assertNotEquals([], $response['fac_room']);
        $this->assertEquals([], $response['kos_rule']);
        $this->assertEquals($this->rentTypeFilter(), $response['rent_type']);
    }

    public function testGetFacilitiesForFilterWithFalseIsForFilterAttribute()
    {
        $tag1 = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 0
            ]
        );
        $tag2 = factory(Tag::class)->create(
            [
                'name' => 'fitness',
                'is_for_filter' => 0
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag1->id,
                'name' => 'fac_room'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag2->id,
                'name' => 'fac_share'
            ]
        );

        $response = $this->repository->getFacilitiesForFilter();

        $this->assertEquals([], $response['fac_share']);
        $this->assertEquals([], $response['fac_room']);
        $this->assertEquals($this->rentTypeFilter(), $response['rent_type']);
    }

    public function testGetRentTypeFilter()
    {
        $this->assertEquals($this->rentTypeFilter(), $this->repository->getRentTypeFilter());
    }

    public function testGetCategorizedFacilitiesFilter()
    {
        $media = factory(Media::class)->create();
        $media1 = factory(Media::class)->create();
        $media2 = factory(Media::class)->create();
        $media3 = factory(Media::class)->create();
        $media4 = factory(Media::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 1,
                'photo_id' => $media->id
            ]
        );

        $tag1 = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 1,
                'photo_id' => $media1->id
            ]
        );

        $tag2 = factory(Tag::class)->create(
            [
                'name' => 'Akses Kunci 24 jam',
                'is_for_filter' => 1,
                'photo_id' => $media2->id
            ]
        );

        $tag3 = factory(Tag::class)->create(
            [
                'name' => 'shower',
                'is_for_filter' => 1,
                'photo_id' => $media3->id
            ]
        );

        $tag4 = factory(Tag::class)->create(
            [
                'name' => 'parkir mobil',
                'is_for_filter' => 1,
                'photo_id' => $media4->id
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_room'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag1->id,
                'name' => 'fac_share'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag2->id,
                'name' => 'kos_rule'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag3->id,
                'name' => 'fac_bath'
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag4->id,
                'name' => 'fac_park'
            ]
        );

        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        $response = $this->repository->getCategorizedFacilitiesFilter($facilities);

        $this->assertEquals($tag->id, $response['fac_room'][0]['fac_id']);
        $this->assertEquals($tag->name, $response['fac_room'][0]['fac_name']);
        $this->assertEquals($tag->photo->getMediaUrl()['real'], $response['fac_room'][0]['fac_icon_url']);
        $this->assertEquals('fac_' . $tag->id, $response['fac_room'][0]['fac_event']);

        $this->assertEquals($tag3->id, $response['fac_room'][1]['fac_id']);
        $this->assertEquals($tag3->name, $response['fac_room'][1]['fac_name']);
        $this->assertEquals($tag3->photo->getMediaUrl()['real'], $response['fac_room'][1]['fac_icon_url']);
        $this->assertEquals('fac_' . $tag3->id, $response['fac_room'][1]['fac_event']);

        $this->assertEquals($tag1->id, $response['fac_share'][0]['fac_id']);
        $this->assertEquals($tag1->name, $response['fac_share'][0]['fac_name']);
        $this->assertEquals($tag1->photo->getMediaUrl()['real'], $response['fac_share'][0]['fac_icon_url']);
        $this->assertEquals('fac_' . $tag1->id, $response['fac_share'][0]['fac_event']);

        $this->assertEquals($tag4->id, $response['fac_share'][1]['fac_id']);
        $this->assertEquals($tag4->name, $response['fac_share'][1]['fac_name']);
        $this->assertEquals($tag4->photo->getMediaUrl()['real'], $response['fac_share'][1]['fac_icon_url']);
        $this->assertEquals('fac_' . $tag4->id, $response['fac_share'][1]['fac_event']);

        $this->assertEquals($tag2->id, $response['kos_rule'][0]['fac_id']);
        $this->assertEquals($tag2->name, $response['kos_rule'][0]['fac_name']);
        $this->assertEquals($tag2->photo->getMediaUrl()['real'], $response['kos_rule'][0]['fac_icon_url']);
        $this->assertEquals('fac_' . $tag2->id, $response['kos_rule'][0]['fac_event']);
    }

    public function testGetCategorizedFacilitiesFilterForFacBath()
    {
        $media = factory(Media::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'shower',
                'is_for_filter' => 1,
                'photo_id' => $media->id
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_bath'
            ]
        );

        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        $response = $this->repository->getCategorizedFacilitiesFilter($facilities);

        $this->assertEquals($tag->id, $response['fac_room'][0]['fac_id']);
    }

    public function testGetCategorizedFacilitiesFilterForFacPark()
    {
        $media = factory(Media::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'parkir mobil',
                'is_for_filter' => 1,
                'photo_id' => $media->id
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_park'
            ]
        );

        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        $response = $this->repository->getCategorizedFacilitiesFilter($facilities);

        $this->assertEquals($tag->id, $response['fac_share'][0]['fac_id']);
    }

    public function testGetCategorizedFacilitiesFilterWithFalseIsForFilterAttribute()
    {
        $media = factory(Media::class)->create();

        $tag = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 0,
                'photo_id' => $media->id
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_room'
            ]
        );

        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        $response = $this->repository->getCategorizedFacilitiesFilter($facilities);

        $this->assertEquals([], $response['fac_room']);
    }

    public function testGetCategorizedFacilitiesFilterWithoutPhoto()
    {
        $tag = factory(Tag::class)->create(
            [
                'name' => 'bed',
                'is_for_filter' => 1,
                'photo_id' => null
            ]
        );

        factory(TagType::class)->create(
            [
                'tag_id' => $tag->id,
                'name' => 'fac_room'
            ]
        );

        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        $response = $this->repository->getCategorizedFacilitiesFilter($facilities);

        $this->assertNull($response['fac_room'][0]['fac_icon_url']);
    }

    /**
     * @group PMS-495
     */    
    public function testGetTagByType()
    {
        $tags = $this->prepareTags();
        $result = $this->repository->getTagByType(['fac_room', 'fac_bath', 'fac_park', 'fac_share', 'fac_near']);

        $this->assertEquals(
            collect(
                $tags['fac_bath']
            )->pluck('id')->sort()->values()->toArray(),
            $result->where('type', 'fac_bath')->pluck('id')->sort()->values()->toArray()
        );
        $this->assertEquals(
            collect(
                $tags['fac_room']
            )->pluck('id')->sort()->values()->toArray(),
            $result->where('type', 'fac_room')->pluck('id')->sort()->values()->toArray()
        );
    }

    /**
     * @group PMS-495
     */
    public function testGetTagByTagTypeNameSuccess()
    {
        $tags = $this->prepareTags();
        factory(TagType::class)->create([
            'tag_id' => $tags['fac_bath'][0]->id,
            'name' => 'fac_room'
        ]);

        $result = $this->repository->getTagByTagTypeName(['fac_room']);
        $this->assertNotEmpty(
            $result->where('type', 'fac_bath')->where('id', $tags['fac_bath'][0]->id)
        );
    }

    private function prepareTags(?array $source = null): array
    {
        $source = $source ?? [
            'fac_room' => [
                'Meja Belajar',
                'Lemari Baju',
                'King Bed'
            ],
            'fac_bath' => [
                'Air Panas',
                'Kloset Duduk',
                'Gayung'
            ],
            'fac_park' => [
                'Parkir Mobil',
                'Parkir Motor'
            ],
            'fac_share' => [
                'Dispenser',
                'Dapur',
                'Laundry'
            ],
            'fac_near' => [
                'Minimarket'
            ],
        ];

        $tags = [];

        collect($source)->each(function ($items, $category) use (&$tags) {
            collect(($items))->each(function ($name) use ($category, &$tags) {
                $tag = factory(Tag::class)->create([
                    'name' => $name,
                    'type' => $category
                ]);
                factory(TagType::class)->create([
                    'tag_id' => $tag->id,
                    'name' => $category
                ]);

                $tags[$category][] = $tag;
            });
        });

        return $tags;
    }

    protected function rentTypeFilter(): array
    {
        return [
            [
                'id' => 1,
                'rent_name' => 'Mingguan',
                'fac_event' => 'filter_period_mingguan'
            ],
            [
                'id' => 2,
                'rent_name' => 'Bulanan',
                'fac_event' => 'filter_period_bulanan'
            ],
            [
                'id' => 4,
                'rent_name' => 'Per 3 Bulan',
                'fac_event' => 'filter_period_3_bulan'
            ],
            [
                'id' => 5,
                'rent_name' => 'Per 6 Bulan',
                'fac_event' => 'filter_period_6_bulan'
            ],
            [
                'id' => 3,
                'rent_name' => 'Tahunan',
                'fac_event' => 'filter_period_tahunan'
            ]
        ];
    }

    public function testGetFacilitySinggahsiniRegistration()
    {
        $arrayTags = ["Dapur","Ruang makan","Ruang tamu"];
        $builder = new MockBuilder();
        $builder->setNamespace(__NAMESPACE__)
            ->setName('config')
            ->setFunction(
                function ($key) use($arrayTags) {
                    if ($key == 'singgahsini.facilities') {
                        return $arrayTags;
                    }
                }
            );
        $mockConfig = $builder->build();
        $mockConfig->enable();

        $tags = factory(Tag::class, count($arrayTags))
            ->create()
            ->each(function($tag, $key) use ($arrayTags){
                $tag->name = $arrayTags[$key];
            });
        Cache::shouldReceive('remember')
            ->with(
                'Singgahsini-facility-registration',
                43200,
                \Closure::class
            )
            ->once()
            ->andReturn($tags);
        $res = $this->repository->getFacilitySinggahsiniRegistration();

        $mockConfig->disable();

        $this->assertSame('Dapur', $res[0]->name);
        $this->assertInstanceOf(Collection::class, $res);
    }

    /**
     * @group PMS-495
     */ 
    public function testIndexWithTypeParam()
    {
        request()->merge(['type' => 'all']);

        $result = $this->repository->index();
        $tags = $result->getOriginalContent();
        $this->assertArrayHasKey('tags', $tags);
    }

    /**
     * @group PMS-495
     */ 
    public function testIndexWithCategorizedParam()
    {
        request()->merge(['categorized' => 1]);

        $result = $this->repository->index();
        $tags = $result->getOriginalContent()['tags'];
        $this->assertEquals($tags[0]['title'], 'Kamar Mandi');
        $this->assertArrayHasKey('tags', $tags[0]);
        $this->assertEquals($tags[1]['title'], 'Fasilitas Kamar');
        $this->assertArrayHasKey('tags', $tags[1]);
        $this->assertEquals($tags[2]['title'], 'Fasilitas Parkir');
        $this->assertArrayHasKey('tags', $tags[2]);
        $this->assertEquals($tags[3]['title'], 'Fasilitas Umum');
        $this->assertArrayHasKey('tags', $tags[3]);
        $this->assertEquals($tags[4]['title'], 'Akses Lingkungan');
        $this->assertArrayHasKey('tags', $tags[4]);
    }
}
