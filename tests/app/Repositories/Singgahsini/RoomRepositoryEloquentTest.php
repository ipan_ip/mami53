<?php

namespace App\Repositories\Singgahsini;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class RoomRepositoryEloquentTest extends MamiKosTestCase
{
    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->app->make(RoomRepositoryEloquent::class);
    }

    public function testModel()
    {
        $this->assertSame(Room::class, $this->repo->model());
    }

    public function testGetRooms()
    {
        $rooms = factory(Room::class, 2)
            ->create();
        $roomsId = $rooms->pluck('id')->toArray();

        Cache::shouldReceive('remember')
            ->with(
                RoomRepositoryEloquent::CACHE_KEY,
                RoomRepositoryEloquent::CACHE_TIME,
                \Closure::class
            )
            ->andReturn($rooms);

        $res = $this->repo->getRooms(
            $roomsId
        );

        $this->assertInstanceof(Collection::class, $res);
    }
}