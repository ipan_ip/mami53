<?php

namespace App\Repositories\Singgahsini;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Singgahsini\Registration\SinggahsiniRegistration;
use App\Repositories\Singgahsini\RegisterRepositoryEloquent;
use App\Test\MamiKosTestCase;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class RegisterRepositoryEloquentTest extends MamiKosTestCase
{
    private const UPLOAD_IMAGE = 'garuda/singgahsini/upload-photo';
    private const REGISTER = 'garuda/singgahsini/register';

    private $repo;

    protected function setUp() : void
    {
        parent::setUp();

        $this->repo = $this->app->make(RegisterRepositoryEloquent::class);
        $this->faker = Faker::create();
    }

    public function testModel()
    {
        $this->assertSame($this->repo->model(), SinggahsiniRegistration::class);
    }

    public function testRegister_Success()
    {
        $dataOwner = [
            'owner_name' => 'yasuo',
            'phone_number' => '9138121381',
            'city' => 'washington',
            'kost_name' => 'kost kost an',
            'address' => 'deket rumah presiden',
        ];
    
        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            $dataOwner
        );
        $res = $this->repo->register($fakeRequest);

        $this->assertInstanceOf(SinggahsiniRegistration::class, $res);

        $this->assertDatabaseHas('mamiroom_registration',[
            'owner_name' => $dataOwner['owner_name'],
            'owner_phone_number' => $dataOwner['phone_number'],
            'city' => $dataOwner['city'],
            'kost_name' => $dataOwner['kost_name'],
            'catatan_alamat' => $dataOwner['address'],
        ]);
    }

    public function testRegisterWithComplateFields_Success()
    {
        $lengthTags = 2;
        $lengthMedia = 6;
        $media = factory(Media::class, $lengthMedia)->create();
        $tags = factory(Tag::class, $lengthTags)->create();

        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            [
                'owner_name' => 'yasuo',
                'phone_number' => '9138121381',
                'city' => 'washington',
                'kost_name' => 'kost kost an',
                'address' => 'deket rumah presiden',
                'province' => 'west lake',
                'subdistrict' => 'banyumanik',
                'kebutuhan' => [1,2,3],
                'total_room' => 100,
                'available_room' => 33,
                'price_monthly' => 1000000,
                'ac' => 0,
                'bathroom' => 1,
                'wifi' => 2,
                'foto_bangunan_tampak_depan' => [$media[0]->id, $media[1]->id],
                'foto_dalam_kamar' => $media[2]->id,
                'foto_tampilan_dalam_bangunan' => $media[3]->id,
                'foto_tampak_dari_jalan' => $media[4]->id,
                'foto_depan_kamar' => $media[5]->id,
                'general_facility' => [$tags[0]->id, $tags[1]->id, 0]
            ]
        );
        $res = $this->repo->register($fakeRequest);

        $this->assertInstanceOf(SinggahsiniRegistration::class, $res);
        $this->assertIsArray($res->kebutuhan);
        $this->assertTrue((bool) $res->fac_lainnya);
        $this->assertSame($res->tags->count(), $lengthTags);
        $this->assertSame($res->media->count(), $lengthMedia);

        $this->assertDatabaseHas('mamiroom_registration',[
            'owner_name' => 'yasuo',
            'owner_phone_number' => '9138121381',
            'city' => 'washington',
            'kost_name' => 'kost kost an',
            'catatan_alamat' => 'deket rumah presiden',
        ]);
    }

    public function testRegister_Success_Fac_False()
    {
        $lengthTags = 2;
        $lengthMedia = 6;
        $media = factory(Media::class, $lengthMedia)->create();
        $tags = factory(Tag::class, $lengthTags)->create();

        $fakeRequest = $this->createFakeRequest(
            self::REGISTER,
            'POST',
            [
                'owner_name' => 'yasuo',
                'phone_number' => '9138121381',
                'kebutuhan' => [1,2,3],
                'province' => 'west lake',
                'city' => 'washington',
                'subdistrict' => 'banyumanik',
                'address' => 'deket rumah presiden',
                'kost_name' => 12,
                'total_room' => 100,
                'available_room' => 33,
                'price_monthly' => 1000000,
                'ac' => 0,
                'bathroom' => 1,
                'wifi' => 2,
                'foto_bangunan_tampak_depan' => [$media[0]->id, $media[1]->id],
                'foto_dalam_kamar' => $media[2]->id,
                'foto_tampilan_dalam_bangunan' => $media[3]->id,
                'foto_tampak_dari_jalan' => $media[4]->id,
                'foto_depan_kamar' => $media[5]->id,
                'general_facility' => [$tags[0]->id, $tags[1]->id]
            ]
        );
        $res = $this->repo->register($fakeRequest);

        $this->assertInstanceOf(SinggahsiniRegistration::class, $res);
        $this->assertFalse((bool) $res->fac_lainnya);
        $this->assertSame($res->tags->count(), $lengthTags);
        $this->assertSame($res->media->count(), $lengthMedia);
    }

    private function createFakeRequest(
        string $path = '/',
        string $method = 'GET',
        array $parameter = []
    ): Request {
        return Request::createFromBase(
            SymfonyRequest::create(
                $path,
                $method,
                $parameter
            )
        );
    }
}