<?php

namespace App\Repositories\Owner;

use App\Test\MamiKosTestCase;
use App\Entities\Owner\StatusHistory;

class StatusHistoryRepositoryEloquentTest extends MamiKosTestCase
{
    public function testModelMustBeStatusHistory()
    {
        $this->assertEquals(
            StatusHistory::class,
            app()->make(StatusHistoryRepositoryEloquent::class)->model()
        );
    }
}
