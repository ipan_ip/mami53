<?php

namespace App\Repositories\DownloadExam;

use App\Entities\DownloadExam\DownloadExamFile;
use App\Test\MamiKosTestCase;

class DownloadExamRepositoryEloquentTest extends MamiKosTestCase
{
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->app->make(DownloadExamRepositoryEloquent::class);
    }

    public function testModel_Success()
    {
        $this->assertSame(DownloadExamFile::class, $this->repository->model());
    }

    public function testGetById_Success()
    {
        $data = factory(DownloadExamFile::class)->create();
        $res = $this->repository->getById($data->id);
        $this->assertInstanceOf(DownloadExamFile::class, $res);
    }
}
