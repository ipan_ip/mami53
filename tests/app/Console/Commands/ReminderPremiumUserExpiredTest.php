<?php

namespace App\Console\Commands;

use App\Test\MamiKosTestCase;
use App\Console\Commands\ReminderPremiumUserExpired;
use App\User;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Room;
use App\Entities\Premium\PremiumRequest;
use Illuminate\Support\Facades\Notification;

class ReminderPremiumUserExpiredTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testCheckIfOwnerHasActiveRoomTrue()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'expired_phone' => 0
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        
        $activeRoom = (new ReminderPremiumUserExpired())->checkIfOwnerHasActiveRoom(RoomOwner::where('id', $roomOwner->id)->get());
        $this->assertTrue($activeRoom);
    }

    public function testCheckIfOwnerHasActiveRoomFalse()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'expired_phone' => 1
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        
        $activeRoom = (new ReminderPremiumUserExpired())->checkIfOwnerHasActiveRoom(RoomOwner::where('id', $roomOwner->id)->get());
        $this->assertFalse($activeRoom);
    }

    public function testReminderPremiumUserExpired()
    {
        $user = factory(User::class)->create([
            'date_owner_limit' => date('Y-m-d', strtotime('-31 day'))
        ]);

        $room = factory(Room::class)->create([
            'expired_phone' => 0
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'user_id' => $user->id,
            'status' => PremiumRequest::PREMIUM_REQUEST_SUCCESS,
            'expired_status' => null,
            'view' => 100,
            'used' => 10
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id
        ]);
        
        Notification::fake();
        Notification::assertNothingSent();

        $runReminder = (new ReminderPremiumUserExpired())->handle();
        $this->assertEquals($runReminder, null);
    }
}