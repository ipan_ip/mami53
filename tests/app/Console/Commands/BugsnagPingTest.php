<?php

use App\Test\MamiKosTestCase;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class BugsnagPingTest extends MamiKosTestCase {

    public function testBugsnagPing() {
        Bugsnag::shouldReceive('notifyException');
        $this->artisan('bugsnag:ping')->assertExitCode(0);
    }

    public function testBugsnagPingWithoutEnv() {
        Bugsnag::shouldReceive('notifyException');
        putenv('BUGSNAG_API_KEY=');
        $this->artisan('bugsnag:ping')
        ->expectsOutput('BUGSNAG_API_KEY not defined in .env')
        ->assertExitCode(0);
    }
}