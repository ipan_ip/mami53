<?php
declare(strict_types=1);

use App\Libraries\MockHelper;
use App\Console\Commands\CurrencyUpdate;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Test\MamiKosTestCase;

/** 
 * @coversDefaultClass App\Console\Commands\CurrencyUpdate
*/
final class CurrecyUpdateTest extends MamikosTestCase
{
    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeCurrencyConverterApiReturnsCurrencyRateWhenApiIsNormal(): void
    {
        $mock = MockHelper::mockFileGetContents(CurrencyUpdate::class, '{"USD_IDR":14099.5}');
        $mock->enable();

        $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi("69cb7ade586985be3d35");
        $this->assertSame(14099.5, $currencyRate);

        $mock->disable();
    }

    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeCurrencyConverterApiThrowsExceptionIfApiKeyIsNull(): void
    {
        Bugsnag::shouldReceive('notifyException')->once();
        $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi(null);
        $this->assertEquals(0, $currencyRate);
    }

    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeCurrencyConverterApiThrowsExceptionIfApiKeyIsEmpty(): void
    {
        Bugsnag::shouldReceive('notifyException')->once();
        $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi("");
        $this->assertEquals(0, $currencyRate);
    }

    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeCurrencyConverterApiThrowsExceptionWhenApiIsDown(): void
    {
        $mock = MockHelper::mockFileGetContents(CurrencyUpdate::class, FALSE);
        $mock->enable();

        Bugsnag::shouldReceive('notifyException')->once();
        try {
            $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi("69cb7ade586985be3d35");
        } 
        finally {
            $mock->disable();
        }   
        $this->assertEquals(0, $currencyRate);
    }
    
    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeForexApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeForexApiReturnsCurrencyRateWhenApiIsNormal(): void
    {
        $mock = MockHelper::mockFileGetContents(CurrencyUpdate::class, '{"rates":{"USDIDR":{"rate":14099.5,"timestamp":1553061143786}},"code":200}');
        $mock->enable();

        $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeForexApi();
        $this->assertSame(14099.5, $currencyRate);

        $mock->disable();
    }

    /**
     * @covers ::GetExchangeRateUSDtoIDRByFreeForexApi
     */
    public function testGetExchangeRateUSDtoIDRByFreeForexApiThrowsExceptionWhenApiIsDown(): void
    {
        $mock = MockHelper::mockFileGetContents(CurrencyUpdate::class, FALSE);
        $mock->enable();

        Bugsnag::shouldReceive('notifyException')->once();
        try {
            $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeForexApi();
        } 
        finally {
            $mock->disable();
        }

        $this->assertEquals(0, $currencyRate);
    }
}