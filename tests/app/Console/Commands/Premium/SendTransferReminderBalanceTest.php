<?php
namespace app\Console\Commands\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;
use App\User;
use App\Console\Commands\Premium\SendTransferReminderBalance;
use App\Repositories\Premium\BalanceRequestRepository;
use App\Entities\User\Notification AS NotifOwner;
use Illuminate\Support\Facades\Notification;

class SendTransferReminderBalanceTest extends MamiKosTestCase
{

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRunTransferReminderBalanceSchedule()
    {
        $this->artisan('premium:send_transfer_reminder_balance')->assertExitCode(0);
    }

    public function testTransferReminderBalanceWhenPremiumPackageNotFound()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => 0,
            'expired_status' => 'false',
            'expired_date' => date('Y-m-d', strtotime('+1 day'))
        ]);
        
        $this->artisan('premium:send_transfer_reminder_balance')->assertExitCode(0);
    }

    public function testTransferReminderBalanceSuccess()
    {
        $premiumRequest = factory(PremiumRequest::class)->create();
        $premiumPackage = factory(PremiumPackage::class)->create();
        $balanceRequest = factory(BalanceRequest::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'premium_package_id' => $premiumPackage->id,
            'expired_status' => 'false',
            'expired_date' => date('Y-m-d', strtotime('+1 day'))
        ]);

        Notification::fake();
        Notification::assertNothingSent();
        
        $this->artisan('premium:send_transfer_reminder_balance')->assertExitCode(0);
    }

}