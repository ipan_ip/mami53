<?php

namespace App\Console\Commands;

use App\Test\MamiKosTestCase;
use App\Console\Commands\Premium\TakingOffPremiumBBK;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\User;
use Carbon\Carbon;


class TakingOffPremiumBBKTest extends MamiKosTestCase 
{

    
    public function setUp() : void
    {
        parent::setUp();
    }

    public function testTakeOffOwnerPremiumBBKSuccess()
    {
        $dateOwnerLimit = Carbon::now()->addDays("1");
        $user = factory(User::class)->create([
            "id" => 1,
            "is_owner" => "true",
            "date_owner_limit" => $dateOwnerLimit->toDateString(),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 1,
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "id" => 147,
                "for" => "package",
                "name" => "PREMIUM BOOKING"
            ]),
        ]);

        $result = (new TakingOffPremiumBBK())->takeOffPremiumBBK(147);
        $newUser = User::where("id", $user->id)->first();
        $dateExpired = Carbon::now()->subDay();

        $this->assertEquals($newUser->date_owner_limit, $dateExpired->toDateString());
        $this->assertNull($result);
    }

    public function testTakeOffOwnerPremiumBBKFail()
    {
        /**
         * Test take off owner premium bbk fail 
         * because owner has been top up their package
         */
        $dateOwnerLimit = Carbon::now()->addDays("1");
        $user = factory(User::class)->create([
            "id" => 1,
            "is_owner" => "true",
            "date_owner_limit" => $dateOwnerLimit->toDateString(),
        ]);

        $package = factory(PremiumPackage::class)->create([
            "id" => 147,
            "for" => "package",
            "name" => "PREMIUM BOOKING"
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 10,
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => $package->id,
        ]);

        factory(BalanceRequest::class)->create([
            'premium_package_id' => $package->id,
            'premium_request_id' => $premiumRequest->id
        ]);


        $result = (new TakingOffPremiumBBK())->takeOffPremiumBBK(147);
        $newUser = User::where("id", $user->id)->first();
        $dateExpired = Carbon::now()->subDay();

        $this->assertNotEquals($newUser->date_owner_limit, $dateExpired->toDateString());
        $this->assertNull($result);
    }
}