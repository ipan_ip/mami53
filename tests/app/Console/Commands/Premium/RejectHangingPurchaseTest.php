<?php
namespace app\Console\Commands\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumRequest;
use App\User;
use App\Entities\Premium\AccountConfirmation;
use Carbon\Carbon;
use App\Entities\Premium\BalanceRequest;

class RejectHangingPurchaseTest extends MamiKosTestCase
{
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRunRejectHangingPurchaseSchedule()
    {
        $this->artisan('premium:reject-hanging-purchase')->assertExitCode(0);
    }

    public function testPremiumRequestNotConfirmation()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING,
            'expired_date' => date('Y-m-d'),
            'expired_status' => PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED
        ]);

        (new RejectHangingPurchase())->handle();
        $this->assertEquals($premiumRequest->status, PremiumRequest::PREMIUM_REQUEST_WAITING);
    }

    public function testPremiumRequestTodayConfirmation()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING,
            'expired_date' => null,
            'expired_status' => null
        ]);

        $confirmation =factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'created_at' => Carbon::now()
        ]);

        (new RejectHangingPurchase())->handle();
        $this->assertEquals($premiumRequest->status, PremiumRequest::PREMIUM_REQUEST_WAITING);
    }

    public function testPremiumRequestYesterdayConfirmation()
    {
        $premiumRequest = factory(PremiumRequest::class)->create([
            'status' => PremiumRequest::PREMIUM_REQUEST_WAITING,
            'expired_date' => null,
            'expired_status' => null
        ]);

        $confirmation =factory(AccountConfirmation::class)->create([
            'premium_request_id' => $premiumRequest->id,
            'view_balance_request_id' => 0,
            'created_at' => Carbon::yesterday(),
            'is_confirm' => AccountConfirmation::CONFIRMATION_WAITING
        ]);

        (new RejectHangingPurchase())->handle();
        $premiumRequest = PremiumRequest::find($premiumRequest->id);
        $this->assertNull($premiumRequest);
    }

    public function testPremiumTopupTodayConfirmation()
    {
        $topUpRequest = factory(BalanceRequest::class)->create([
            'status' => BalanceRequest::TOPUP_WAITING,
            'expired_date' => null,
            'expired_status' => null
        ]);

        $confirmation =factory(AccountConfirmation::class)->create([
            'premium_request_id' => 0,
            'view_balance_request_id' => $topUpRequest->id,
            'created_at' => Carbon::now()
        ]);

        (new RejectHangingPurchase())->handle();
        $this->assertEquals($topUpRequest->status, BalanceRequest::TOPUP_WAITING);
    }

    public function testPremiumTopUpYesterdayConfirmation()
    {
        $topUpRequest = factory(BalanceRequest::class)->create([
            'status' => BalanceRequest::TOPUP_WAITING,
            'expired_date' => null,
            'expired_status' => null
        ]);

        $confirmation = factory(AccountConfirmation::class)->create([
            'premium_request_id' => 0,
            'view_balance_request_id' => $topUpRequest->id,
            'created_at' => Carbon::yesterday(),
            'is_confirm' => AccountConfirmation::CONFIRMATION_WAITING
        ]);

        (new RejectHangingPurchase())->handle();
        $topUpRequest = BalanceRequest::find($topUpRequest->id);
        $this->assertNull($topUpRequest);
    }
}