<?php
namespace app\Console\Commands\Premium;

use App\Test\MamiKosTestCase;
use App\Entities\Premium\PremiumRequest;
use App\User;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Room;
use App\Entities\Promoted\ViewPromote;
use Illuminate\Support\Facades\Notification;
use App\Entities\User\Notification as NotificationCenter;

class DailyBudgetAllocationTest extends MamiKosTestCase
{
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRunDailyAllocationSchedule()
    {
        $this->artisan('premium:daily-allocation')->assertExitCode(0);
    }

    public function testNotificationComponentWhenOwnerHaveManyRoom()
    {
        $notificationComponent = (new DailyBudgetAllocation())->notificationComponent(
                                    3000, // Balance allocation total
                                    3, // Room total
                                    3, // Room allocation total
                                    null, // Room
                                    true // Status
                                );
        
        $this->assertEquals(NotificationCenter::URL['premium_package_list'], $notificationComponent['url']);
    }

    public function testNotificationComponentWhenOwnerHaveOneRoom()
    {
        $room = factory(Room::class)->create([
            'id' => 91029282,
            'name' => 'Kos testing'
        ]);

        $notificationComponent = (new DailyBudgetAllocation())->notificationComponent(
                                    3000, // Balance allocation total
                                    1, // Room total
                                    1, // Room allocation total
                                    $room, // Room
                                    true // Status
                                );

        $this->assertEquals('owner_ads', $notificationComponent['scheme']);
    }

    public function testNotificationComponentWhenBalanceNotAvailable()
    {
        $room = factory(Room::class)->create([
            'id' => 9129282,
            'name' => 'Kos testing'
        ]);

        $notificationComponent = (new DailyBudgetAllocation())->notificationComponent(
                                    3000, // Balance allocation total
                                    1, // Room total
                                    0, // Room allocation total
                                    $room, // Room
                                    false // Status
                                );
        
        $this->assertEquals('owner_profile?go_list_package=true', $notificationComponent['scheme']);
    }

    public function testNotificationComponentWhenBalanceNotAvailableButAlreadyAllocated()
    {
        $notificationComponent = (new DailyBudgetAllocation())->notificationComponent(
            3000, // Balance allocation total
            3, // Room total
            2, // Room allocation total
            null, // Room
            false // Status
        );

        $this->assertEquals('owner_profile?go_list_package=true', $notificationComponent['scheme']);
    }

    public function testDailyAllocationSchedule()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'is_owner' => 'true',
            'date_owner_limit' => date('Y-m-d', strtotime('+7 days'))
        ]);

        $room = factory(Room::class)->create([
            'id' => 1,
            'is_promoted' => 'true'
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'id' => 1,
            'user_id' => $user->id,
            'status' => 'verified',
            'designer_id' => $room->id
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'id' => 1,
            'user_id' => $user->id,
            'view' => 1000,
            'expired_date' => null,
            'status' => '1',
            'used' => 10,
            'allocated' => 20,
            'daily_allocation' => 1,
        ]);

        $roomPromote = factory(ViewPromote::class)->create([
            'id' => 1,
            'designer_id' => $room->id,
            'premium_request_id' => $premiumRequest->id,
            'total' => 20,
            'is_active' => 1,
            'used' => 10,
            'history' => 0,
            'daily_budget' => 20
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        (new DailyBudgetAllocation())->handle();
        $request = PremiumRequest::find($premiumRequest->id);
        $promote = ViewPromote::find($roomPromote->id);
        
        $this->assertEquals(30, $request->allocated);
        $this->assertEquals(10, $promote->history);
        $this->assertEquals(30, $promote->total);
        $this->assertEquals(0, $promote->used);
    }

    public function testBalanceAllocation()
    {
        $roomPromote = factory(ViewPromote::class)->create([
            'id' => 2,
            'designer_id' => factory(Room::class)->create([
                'id' => 2,
                'is_promoted' => 'true'
            ]),
            'premium_request_id' => factory(PremiumRequest::class)->create([
                'id' => 2,
                'user_id' => 2,
                'view' => 1000,
                'expired_date' => null,
                'status' => '1',
                'used' => 10,
                'allocated' => 20,
                'daily_allocation' => 1,
            ])->id,
            'total' => 20,
            'is_active' => 1,
            'used' => 10,
            'history' => 0,
            'daily_budget' => 20
        ]);

        $this->mockAlternatively('App\Repositories\Premium\PremiumAllocationRepository')
            ->shouldReceive('deactivatePremiumPromote')
            ->with(ViewPromote::class)
            ->andReturn();
        
        $result = (new DailyBudgetAllocation())->balanceAllocation($roomPromote);
        $this->assertTrue($result);
    }

    public function testDailyAllocationScheduleWithoutDailyBudget()
    {
        $user = factory(User::class)->create([
            'id' => 1112,
            'is_owner' => 'true',
            'date_owner_limit' => date('Y-m-d', strtotime('+7 days'))
        ]);

        $room = factory(Room::class)->create([
            'id' => 1112,
            'is_promoted' => 'true'
        ]);

        $roomOwner = factory(RoomOwner::class)->create([
            'id' => 1112,
            'user_id' => $user->id,
            'status' => 'verified',
            'designer_id' => $room->id
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            'id' => 1112,
            'user_id' => $user->id,
            'view' => 1000,
            'expired_date' => null,
            'status' => '1',
            'used' => 10,
            'allocated' => 20,
            'daily_allocation' => 1,
        ]);

        $roomPromote = factory(ViewPromote::class)->create([
            'id' => 1112,
            'designer_id' => $room->id,
            'premium_request_id' => $premiumRequest->id,
            'total' => 20,
            'is_active' => 1,
            'used' => 10,
            'history' => 0,
            'daily_budget' => 0
        ]);

        Notification::fake();
        Notification::assertNothingSent();

        (new DailyBudgetAllocation())->handle();
        $request = PremiumRequest::find($premiumRequest->id);
        $promote = ViewPromote::find($roomPromote->id);
        
        $this->assertEquals(20, $request->allocated);
        $this->assertEquals(20, $promote->total);
    }
}