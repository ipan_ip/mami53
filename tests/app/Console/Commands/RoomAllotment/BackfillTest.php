<?php

namespace App\Console\Commands\RoomAllotment;

use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class BackfillTest extends MamiKosTestCase
{
    
    public function testRunCommandSuccess()
    {
        $room = factory(Room::class)->state('active')->create();
        $this->artisan('room-allotment:backfill', ['roomId' => $room->id])->assertExitCode(0);
    }

    public function testRunCommandNotFound()
    {
        $this->artisan('room-allotment:backfill', ['roomId' => 888])->assertExitCode(1);
    }

    public function testRunCommandAlreadyBackfilled()
    {
        $room = factory(Room::class)->state('active')->create();
        factory(RoomUnit::class)->create(['designer_id' => $room->id]);
        $this->artisan('room-allotment:backfill', ['roomId' => $room->id])->assertExitCode(2);
    }
}
