<?php
namespace app\Console\Commands\RoomAllotment;

use App\Test\MamiKosTestCase;

class SyncRoomAllotmentContractTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testSyncRoomAllotmentContractCommandCommitMode(): void
    {
        $this->markTestSkipped('Temporary skip this test');
        $this->artisan('room-allotment-sync-contract:snap', [
            '--csv-file'    => 'room_allotment_contract_not_sync.csv',
            '--mode'        => 'commit',
            '--memory-limit' => -1
        ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }

    public function testSyncRoomAllotmentContractCommandRollbackMode(): void
    {
        $this->markTestSkipped('Temporary skip this test');
        $this->artisan('room-allotment-sync-contract:snap', [
            '--csv-file'    => 'room_allotment_contract_not_sync.csv',
            '--mode'        => 'rollback',
            '--memory-limit' => -1
        ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }
}
