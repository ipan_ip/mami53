<?php

namespace App\Console\Commands\RoomAllotment;

use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;

class BulkBackfillTest extends MamiKosTestCase
{
    
    public function testRunCommandSuccess()
    {
        factory(Room::class, 10)->state('active')->create();
        $this->artisan('room-allotment:bulk-backfill')->assertExitCode(0);
    }

    public function testRunCommandSuccessForInactive()
    {
        factory(Room::class, 10)->state('inactive')->create();
        $this->artisan('room-allotment:bulk-backfill', ['--inactive' => true])->assertExitCode(0);
    }

    public function testRunCommandFailedWrongArgument()
    {
        factory(Room::class, 10)->state('active')->create();
        $this->artisan('room-allotment:bulk-backfill', ['--chunk' => 0])->assertExitCode(1);
    }

    public function testRunCommandNotRunningLimitHit()
    {
        $this->artisan('room-allotment:bulk-backfill')->assertExitCode(2);
    }
}
