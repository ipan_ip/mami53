<?php

namespace App\Console\Commands\RoomAllotment;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;

class NormalizationTest extends MamiKosTestCase
{
    
    private $repo;
    private $room;

    protected function setUp() : void
    {
        parent::setUp();
        $this->repo = app()->make(RoomUnitRepository::class);
    }

    public function testRunCommandRoomNotFound()
    {
        $this->artisan('room-allotment:normalization', ['roomId' => 888])->assertExitCode(Normalization::ITEM_NOT_FOUND_EXIT_CODE);
    }

    public function testRunCommandRoomTotalOutOfSyncSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 5;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);

        $this->assertEquals($roomCount, $room->room_unit->count());

        $room->room_unit->random(mt_rand(1, $roomAvailable))->each(
            function (RoomUnit $unit) {
                $unit->delete();
                return;
            }
        );
        $room->refresh();

        $this->assertNotEquals($roomCount, $room->room_unit->count());
        
        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($roomCount, $room->room_unit->count());
        $this->assertEquals($roomAvailable, count($room->room_unit->where('occupied', false)));
    }

    public function testRunCommandRoomTotalOutOfSyncAvailableDiffSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 4;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);

        $this->assertEquals($roomCount, $room->room_unit->count());

        $room->room_unit->where('occupied', false)->take(2)->each(
            function (RoomUnit $unit) {
                $unit->delete();
                return;
            }
        );
        $room->room_available = 3;
        $room->save();
        $room->refresh();

        $this->assertNotEquals($roomCount, $room->room_unit->count());
        
        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($roomCount, $room->room_unit->count());
        $this->assertEquals($room->room_available, count($room->room_unit->where('occupied', false)));
    }

    public function testRunCommandRoomTotalOutOfSyncMultipleSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 5;
        $rooms[] = $this->prepareRoomModel($roomCount, $roomAvailable);
        $rooms[] = $this->prepareRoomModel($roomCount, $roomAvailable);
        $rooms[] = $this->prepareRoomModel($roomCount, $roomAvailable);

        $ids = [];
        foreach ($rooms as $room) {
            $this->assertEquals($roomCount, $room->room_unit->count());
            $room->room_unit->random(mt_rand(1, $roomAvailable))->each(
                function (RoomUnit $unit) {
                    $unit->delete();
                    return;
                }
            );
            $ids[] = $room->id;
            $room->refresh();
            $this->assertNotEquals($roomCount, $room->room_unit->count());
        }

        $ids[] = 8947;
        $roomAddition = $this->prepareRoomModel($roomCount, $roomAvailable);
        $ids[] = $roomAddition->id;
        
        $this->artisan('room-allotment:normalization', ['roomId' => $ids])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);

        foreach ($rooms as $room) {
            $room->refresh();
            $this->assertEquals($roomCount, $room->room_unit->count());
            $this->assertEquals($roomAvailable, count($room->room_unit->where('occupied', false)));
        }
    }

    public function testRunCommandRoomTotalOutOfSyncBiggerAllotmentSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 5;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);

        $this->assertEquals($roomCount, $room->room_unit->count());

        $room->room_count = 6;
        $room->save();
        $room->refresh();

        $this->assertNotEquals($room->room_count, $room->room_unit->count());
        
        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($room->room_count, $room->room_unit->count());
        $this->assertEquals($roomAvailable, count($room->room_unit->where('occupied', false)));
    }

    public function testRunCommandRoomAvailableOutOfSyncSmallerThanDesignerSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 5;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $this->assertEquals($roomAvailable, count($room->room_unit->where('occupied', false)));

        $changedUnits = [];
        $room->room_unit->where('occupied', false)->random(mt_rand(1, $roomAvailable))->each(
            function (RoomUnit $unit) use (&$changedUnits) {
                $unit->occupied = true;
                $changedUnits[] = $unit->id;
                return $unit->save();
            }
        );
        $room->refresh();

        $this->assertNotEquals($roomAvailable, count($room->room_unit->where('occupied', false)));

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($roomCount, $room->room_unit->count());
        $this->assertEquals($roomAvailable, $room->room_unit->where('occupied', false)->count());
    }

    public function testRunCommandRoomAvailableOutOfSyncBiggerThanDesignerSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 3;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $this->assertEquals($roomAvailable, count($room->room_unit->where('occupied', false)));

        $changedUnits = [];
        $room->room_unit->where('occupied', true)->random(mt_rand(1, $roomAvailable))->each(
            function (RoomUnit $unit) use (&$changedUnits) {
                $unit->occupied = false;
                $changedUnits[] = $unit->id;
                return $unit->save();
            }
        );
        $room->refresh();

        $this->assertNotEquals($roomCount - $roomAvailable, count($room->room_unit->where('occupied', true)));

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($roomCount, $room->room_unit->count());
        $this->assertEquals($roomCount - $roomAvailable, $room->room_unit->where('occupied', true)->count());
    }

    public function testRunCommandRoomTotalOutOfSyncWithContractSuccess()
    {
        $roomCount = 8;
        $roomAvailable = 3;
        $contractNumber = 4;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);

        $occupiedUnits = $room->room_unit->where('occupied', true);

        factory(MamipayContract::class, $contractNumber)->create([
            'type' => 'kost',
            'status' => MamipayContract::STATUS_BOOKED,
            'end_date' => Carbon::now()->addMonth()
        ])->each(
            function ($contract) use ($room, &$occupiedUnits) {
                factory(MamipayContractKost::class)->create([
                    'contract_id' => $contract->id,
                    'designer_id' => $room->id,
                    'designer_room_id' => $occupiedUnits->shift()->id
                ]);
            }
        );

        $room->room_count = 6;
        $room->room_available = 3;
        $room->save();
        
        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($room->room_count, $room->room_unit->count());
        $this->assertEquals($contractNumber, $room->room_unit->where('occupied', true)->count());
        $this->assertEquals(2, count($room->room_unit->where('occupied', false)));
    }

    public function testRunCommandScenarioAvailableOutOfSyncDesignerAvailableSmallerThanRoomUnitAvailable()
    {
        $roomCount = 9;
        $roomAvailable = 4;
        $unitdiscrepancy = 3;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $room->room_unit_occupied->take($unitdiscrepancy)->each(function ($unit) {
            $unit->occupied = false;
            $unit->save();
        });
        $room->refresh();

        $this->assertNotEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    public function testRunCommandScenarioAvailableOutOfSyncDesignerAvailableBiggerThanRoomUnitAvailable()
    {
        $roomCount = 40;
        $roomAvailable = 22;
        $unitdiscrepancy = 1;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $room->room_unit_empty->take($unitdiscrepancy)->each(function ($unit) {
            $unit->occupied = true;
            $unit->save();
        });
        $room->refresh();

        $this->assertNotEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id])->assertExitCode(Normalization::SUCCESS_EXIT_CODE);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    public function testRunCommandScenarioTotalOutOfSyncDesignerBiggerThanAllotmentAvailableSynced()
    {
        $roomCount = 13;
        $roomAvailable = 3;
        $unitdiscrepancy = 6;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $room->room_unit_occupied->take($unitdiscrepancy)->each(function ($unit) {
            $unit->delete();
        });
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertNotEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id]);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    public function testRunCommandScenarioTotalOutOfSyncDesignerBiggerThanAllotmentAvailableBigger()
    {
        $roomCount = 16;
        $roomAvailable = 0;
        $unitdiscrepancyOccupied = 9;
        $unitdiscrepancyAvailable = 0;

        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $room->room_unit_occupied->take($unitdiscrepancyOccupied)->each(function ($unit) {
            $unit->delete();
        });

        $room->room_unit_empty->take($unitdiscrepancyAvailable)->each(function ($unit) {
            $unit->delete();
        });
        $room->refresh();

        $this->assertNotEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id]);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    public function testRunCommandScenarioTotalOutOfSyncDesignerSmallerThanAllotmentAvailableSynced()
    {
        $roomCount = 13;
        $roomAvailable = 3;
        $discrepancyOccupied = 6;
        $discrepancyAvailable = 0;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $this->repo->insertRoomUnit(
            $room->id,
            $room->room_unit->pluck('name')->map(
                static function ($item) {
                    return trim($item);
                }
            )->toArray(),
            $discrepancyOccupied,
            $discrepancyAvailable
        );
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertNotEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id]);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    public function testRunCommandScenarioTotalOutOfSyncDesignerSmallerThanAllotmentAvailableNotSynced()
    {
        $roomCount = 9;
        $roomAvailable = 1;
        $discrepancyOccupied = 1;
        $discrepancyAvailable = 3;
        $room = $this->prepareRoomModel($roomCount, $roomAvailable);
        $room->loadMissing(['room_unit', 'room_unit_empty', 'room_unit_occupied']);

        $this->repo->insertRoomUnit(
            $room->id,
            $room->room_unit->pluck('name')->map(
                static function ($item) {
                    return trim($item);
                }
            )->toArray(),
            $discrepancyOccupied,
            $discrepancyAvailable
        );
        $room->refresh();

        $this->assertNotEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertNotEquals($room->room_count, $room->room_unit->count());

        $this->artisan('room-allotment:normalization', ['roomId' => $room->id]);
        $room->refresh();

        $this->assertEquals($room->room_available, $room->room_unit_empty->count());
        $this->assertEquals($room->room_count, $room->room_unit->count());
    }

    private function prepareRoomModel(int $roomCount, int $roomAvailable): Room
    {
        $room = factory(Room::class)->state('active')->create([
            'room_count' => $roomCount,
            'room_available' => $roomAvailable
        ]);

        $this->repo->backFillRoomUnit($room);
        $room->load('room_unit');

        return $room;
    }
}
