<?php


namespace app\Console\Commands\Thanos;


use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class RevertSnapTest extends MamiKosTestCase
{
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testThanosRevertSnap()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->state('active')->create();
        $roomOwner = factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'user_id' => $user->id
        ]);

        $this->artisan('thanos:snap', ['designerId' => $room->id]);
        $this->artisan('thanos:revert', ['designerId' => $room->id])
            ->expectsOutput('Snap reverted')
            ->assertExitCode(0);
    }

    public function testRoomIdNotFound()
    {
        $this->artisan('thanos:snap', ['designerId' => 999999])
            ->expectsOutput('Room 999999 not found');
    }

}