<?php

namespace App\Console\Commands;

use App\Test\MamiKosTestCase;
use App\Entities\Consultant\ConsultantRoom;

class RemoveConsultantRoomSoftDeletedRecordTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
        $this->markTestSkipped('Skip test for now');
    }

    public function testRemoveSoftDeletedRecordWithoutCustomOptionSuccess()
    {
        factory(ConsultantRoom::class, 20)->create();
        factory(ConsultantRoom::class, 10)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);

        $this->artisan('consultant:remove-soft-deleted-consultant-room')
        ->expectsQuestion('Completely remove all soft deleted record in consultant_designer table?', 'yes')    
        ->assertExitCode(0);
        
        $this->assertEquals(20, ConsultantRoom::count());
        
        $deletedAtIsNull = ConsultantRoom::all()->map(function ($element) {
            return $element['deleted_at'] == null;
        });
        $isMatch = !in_array(false, $deletedAtIsNull->toArray());

        $this->assertTrue($isMatch);
    }

    public function testRemoveSoftDeletedRecordWithLimitSuccess()
    {
        factory(ConsultantRoom::class, 300)->create();
        factory(ConsultantRoom::class, 150)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);

        $this->artisan('consultant:remove-soft-deleted-consultant-room', [
            '--limit' => 120
        ])
        ->expectsQuestion('Completely remove all soft deleted record in consultant_designer table?', 'yes')
        ->assertExitCode(0);
        
        $this->assertEquals(330, ConsultantRoom::count());
    }

    public function testRemoveSoftDeletedRecordWithChunkSizeSuccess()
    {
        factory(ConsultantRoom::class, 300)->create();
        factory(ConsultantRoom::class, 150)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);

        $this->artisan('consultant:remove-soft-deleted-consultant-room', [
            '--limit' => 120, '--chunk-size' => 111
        ])
        ->expectsQuestion('Completely remove all soft deleted record in consultant_designer table?', 'yes')
        ->assertExitCode(0);
        
        $this->assertEquals(330, ConsultantRoom::count());
    }

    public function testRemoveSoftDeletedRecordWithDryRunShouldNotRemoveData()
    {
        factory(ConsultantRoom::class, 20)->create();
        factory(ConsultantRoom::class, 10)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);

        $this->artisan('consultant:remove-soft-deleted-consultant-room', [
            '--dry-run' => true
        ])
        ->expectsQuestion('Completely remove all soft deleted record in consultant_designer table?', 'yes')
        ->assertExitCode(0);
        
        $this->assertEquals(30, ConsultantRoom::count());
    }

    public function testRemoveSoftDeletedRecordWithForce()
    {
        factory(ConsultantRoom::class, 20)->create();
        factory(ConsultantRoom::class, 10)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);

        $this->artisan('consultant:remove-soft-deleted-consultant-room', [
            '--force' => true
        ])->assertExitCode(0);
        
        $this->assertEquals(20, ConsultantRoom::count());
    }
}