<?php

namespace App\Console\Commands;

use App\Test\MamiKosTestCase;
use App\Console\Commands\PromoteOnReminder;
use App\User;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promote\ViewPromote;

class PromoteOnReminderTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testReminderForPremiumOwnerSuccess()
    {
        $user = factory(User::class)->create([
            "id" => 1,
            "is_owner" => "true",
            "date_owner_limit" => date('Y-m-d', strtotime('+10 days')),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 1,
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => 1,
            "allocated" => 0,
            "updated_at" => date('Y-m-d', strtotime('-3 days')),
        ]);

        $result = (new PromoteOnReminder())->handle();
        $this->assertNull($result);
    }
}