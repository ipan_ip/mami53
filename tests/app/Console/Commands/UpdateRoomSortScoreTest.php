<?php

namespace App\Console\Commands;

use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Queue;

class UpdateRoomSortScoreTest extends MamiKosTestCase
{
    use WithoutMiddleware;
    use WithoutEvents;

    public function testHandleWithRoomId()
    {
        Queue::fake();

        $room = factory(Room::class)
            ->states(
                [
                    'active',
                    'mamirooms',
                    'availableBooking'
                ]
            )
            ->create(
                [
                    'sort_score' => 2692
                ]
            );

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--room-id' => $room->id
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithRoomIdReturnError()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--room-id' => 9999999999
                ]
            )
            ->expectsOutput('No Kos data could be calculated!');
    }

    public function testHandleWithFlashSale()
    {
        $flashSale = factory(FlashSale::class)->create(
            [
                'start_time' => Carbon::yesterday()->format('YY-mm-dd'),
                'end_time'=> Carbon::tomorrow()->format('YY-mm-dd')
            ]
        );

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => $flashSale->id
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithFlashSaleNotRunning()
    {
        $flashSale = factory(FlashSale::class)->create(
            [
                'start_time' => Carbon::tomorrow()->format('YY-mm-dd'),
                'end_time'=> Carbon::now()->addWeek()->format('YY-mm-dd')
            ]
        );

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => $flashSale->id
                ]
            )
            ->expectsOutput('Flash Sale is not running!');
    }

    public function testHandleWithFlashSaleNotActive()
    {
        $flashSale = factory(FlashSale::class)->create(
            [
                'is_active' => 0
            ]
        );

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => $flashSale->id
                ]
            )
            ->expectsOutput('Could not recalculate non-active Flash Sale rooms!');
    }

    public function testHandleWithNonExistFlashSale()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => 999
                ]
            )
            ->expectsOutput('Invalid Flash Sale ID!');
    }

    public function testHandleWithChunkSize()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--chunk-size' => 10
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithUncalculatedOnly()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--uncalculated-only' => true
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithLastDayOnly()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--last-day-only' => true
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithNonActive()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--with-non-active' => true
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithDryRun()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--dry-run' => true
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithSampleSize()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--sample-size' => 10
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleWithUsingQueue()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--using-queue' => true
                ]
            )
            ->assertExitCode(0);
    }

    public function testHandleInvalidOptions()
    {
        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--room-id' => 'abc'
                ]
            )
            ->expectsOutput('Something went wrong! Please re-check your options!');

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => 'abc'
                ]
            )
            ->expectsOutput('Something went wrong! Please re-check your options!');

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--flash-sale-id' => 0
                ]
            )
            ->expectsOutput('Something went wrong! Please re-check your options!');

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--chunk-size' => 'abc'
                ]
            )
            ->expectsOutput('Something went wrong! Please re-check your options!');

        $this
            ->artisan(
                'update:room-sort-score',
                [
                    '--chunk-size' => 0
                ]
            )
            ->expectsOutput('Something went wrong! Please re-check your options!');
    }
}
