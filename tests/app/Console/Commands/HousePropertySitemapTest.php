<?php

use App\Test\MamiKosTestCase;

class HousePropertySitemapTest extends MamiKosTestCase {

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGenerate() {
        $sitemapHousePropertyMock = Mockery::mock('overload:App\Entities\Component\SitemapHouseProperty');
        $sitemapHousePropertyMock->shouldReceive('generate')->andReturn(null);
        $this->artisan('sitemap:house-property')->assertExitCode(0);
    }
}