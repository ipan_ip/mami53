<?php

namespace App\Console\Commands\Database;

use App\Test\MamiKosTestCase;
use Exception;

class PingTest extends MamiKosTestCase
{
    public function testUndefinedConnectionFailed()
    {
        $this->artisan('database:ping', ['connection' => 'definitely-not-a-connection'])
            ->expectsOutput("Database [definitely-not-a-connection] not configured.")
            ->assertExitCode(1);
    }

    public function testMysqlConnectionSuccess()
    {
        $this->artisan('database:ping', ['connection' => 'mysql'])->expectsOutput('pinging database success')->assertExitCode(0);
    }

    public function testMongoConnectionSuccess()
    {
        $this->artisan('database:ping', ['connection' => 'mongodb'])->assertExitCode(0);
    }
}