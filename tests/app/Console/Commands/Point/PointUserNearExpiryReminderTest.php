<?php

namespace App\Console\Commands\Point;

use App\Entities\Point\Point;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\User\Notification AS NotifOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PointUserNearExpiryReminderTest extends MamiKosTestCase
{
    public function testRunCommand_WithoutRedeemOrTopDown_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 12,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '4 poin Anda akan kedaluwarsa pada 31 Jan 2021. Yuk dipakai sebelum hangus.'));
    }

    public function testRunCommand_WithRedeemOrTopDown_LessThanNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 10,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 1)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '2 poin Anda akan kedaluwarsa pada 31 Jan 2021. Yuk dipakai sebelum hangus.'));
    }

    public function testRunCommand_WithRedeemOrTopDown_EqualToNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 8,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 1)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithRedeemOrTopDown_GreaterThanNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithoutNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_OwnerBlacklisted_WithNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 4,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_TenantBlacklisted_WithNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 4,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_OwnerBlacklisted_WithoutNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 4,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_TenantBlacklisted_WithoutNearExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 4,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithoutUnexpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '6 poin Anda akan kedaluwarsa pada 31 Jan 2021. Yuk dipakai sebelum hangus.'));
    }

    public function testRunCommand_WithoutUnexpiredPoint_WithRedeemOrTopDown_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 2,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 1)->create([
            'user_id' => $user->id,
            'value' => -2,
        ]);

        $this->artisan('point:near-expiry-reminder')->assertExitCode(0);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '2 poin Anda akan kedaluwarsa pada 31 Jan 2021. Yuk dipakai sebelum hangus.'));
    }
}