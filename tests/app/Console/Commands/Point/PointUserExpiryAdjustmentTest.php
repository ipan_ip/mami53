<?php

namespace App\Console\Commands\Point;

use App\Entities\Point\Point;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\User\Notification AS NotifOwner;
use App\Test\MamiKosTestCase;
use App\User;
use Carbon\Carbon;

class PointUserExpiryAdjustmentTest extends MamiKosTestCase
{
    public function testRunCommand_WithoutRedeemOrTopDown_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 18,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(12, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-6, $pointHistory->value);
        $this->AssertEquals(12, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '6 poin Anda telah kedaluwarsa'));
    }

    public function testRunCommand_WithRedeemOrTopDown_LessThanExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 14,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 1)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(12, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-2, $pointHistory->value);
        $this->AssertEquals(12, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '2 poin Anda telah kedaluwarsa'));
    }

    public function testRunCommand_WithRedeemOrTopDown_EqualToExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 12,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(12, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(-2, $pointHistory->value);
        $this->AssertEquals('Redeem', $pointHistory->notes);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithRedeemOrTopDown_GreaterThanExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 10,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (unexpired)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (+)
        factory(PointHistory::class, 2)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'value' => -2,
            'notes' => 'Redeem'
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(10, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(-2, $pointHistory->value);
        $this->AssertEquals('Redeem', $pointHistory->notes);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithoutExpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(6, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(), $pointUser->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_OwnerBlacklisted_WithExpiredPoint_ShouldSuccessWithoutNotificationSent()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 10,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(4, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-6, $pointHistory->value);
        $this->AssertEquals(4, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_TenantBlacklisted_WithExpiredPoint_ShouldSuccessWithoutNotificationSent()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 10,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 2)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(4, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(), $pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-6, $pointHistory->value);
        $this->AssertEquals(4, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_OwnerBlacklisted_WithoutExpiredPoint_ShouldSuccessWithoutNotificationSent()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(6, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(), $pointUser->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_TenantBlacklisted_WithoutExpiredPoint_ShouldSuccessWithoutNotificationSent()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 1
        ]);

        // Get point (expired soon)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(6, $pointUser->total);
        $this->AssertEquals(Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(), $pointUser->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(0, count($notifOwner));
    }

    public function testRunCommand_WithoutUnexpiredPoint_ShouldSuccess()
    {
        $user = factory(User::class)->state('owner')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 6,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(0, $pointUser->total);
        $this->AssertNull($pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-6, $pointHistory->value);
        $this->AssertEquals(0, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '6 poin Anda telah kedaluwarsa'));
    }

    public function testRunCommand_WithoutUnexpiredPoint_WithoutRedeemOrTopDown_ShouldSuccess()
    {
        $user = factory(User::class)->state('tenant')->create();

        $point = factory(Point::class)->create([
            'value' => Point::DEFAULT_VALUE,
            'expiry_unit' => 'month',
            'expiry_value' => 2,
        ]);

        factory(PointUser::class)->create([
            'user_id' => $user->id,
            'total' => 2,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'is_blacklisted' => 0
        ]);

        // Get point (expired)
        factory(PointHistory::class, 3)->create([
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->subMonthsNoOverflow(1)->endOfMonth(),
            'value' => 2,
        ]);

        // Adjustment point (-)
        factory(PointHistory::class, 1)->create([
            'activity_id' => 0,
            'user_id' => $user->id,
            'expired_date' => Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth(),
            'value' => -2,
        ]);

        // Redeem point
        factory(PointHistory::class, 1)->create([
            'user_id' => $user->id,
            'value' => -2,
        ]);

        $this->artisan('point:expiry-adjustment')->assertExitCode(0);

        $pointUser = PointUser::where('user_id', $user->id)->first();
        $this->AssertEquals($user->id, $pointUser->user_id);
        $this->AssertEquals(0, $pointUser->total);
        $this->AssertNull($pointUser->expired_date);

        $pointHistory = PointHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
        $this->AssertEquals($user->id, $pointHistory->user_id);
        $this->AssertEquals(0, $pointHistory->activity_id);
        $this->AssertEquals(-2, $pointHistory->value);
        $this->AssertEquals(0, $pointHistory->balance);
        $this->AssertEquals('Poin Kedaluwarsa', $pointHistory->notes);
        $this->AssertNull($pointHistory->expired_date);

        $notifOwner = NotifOwner::where('user_id', $user->id)->get();
        $this->assertEquals(1, count($notifOwner));
        $this->assertEquals(0, strpos($notifOwner->first()->title, '2 poin Anda telah kedaluwarsa'));
    }
}