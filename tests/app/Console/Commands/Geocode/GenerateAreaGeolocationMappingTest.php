<?php

namespace App\Console\Commands\Geocode;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Search\InputKeyword;
use App\Entities\Search\InputKeywordSuggestion;
use App\Test\MamiKosTestCase;
use DB;

class GenerateAreaGeolocationMappingTest extends MamiKosTestCase
{

    public function testHandleWithKeywordCreateNewRecord()
    {
        [$suggestion, $areaGeolocation] = $this->setJakartaInputProperty();
        DB::commit();

        $this->artisan('area:generate-geolocation-mapping', ['--keyword' => 'jakarta']);
        $this->assertDatabaseHas(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    public function testHandleWithKeywordUsingInvalidInput()
    {
        [$suggestion, $areaGeolocation] = $this->setJakartaInputProperty();
        DB::commit();

        // the keyword input is invalid
        $this->artisan('area:generate-geolocation-mapping', ['--keyword' => 'test']);
        $this->assertDatabaseMissing(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    public function testHandleWithKeywordWithNoSuggestion()
    {
        // the input keyword doesnt has any suggestion
        $inputKeyword = factory(InputKeyword::class)->create(['keyword' => 'jakarta']);

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);

        DB::commit();

        $this->artisan('area:generate-geolocation-mapping', ['--keyword' => 'jakarta']);
        $this->assertDatabaseMissing(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    public function testHandleWithMultipleKeywordsCreateNewRecord()
    {
        [$suggestion, $areaGeolocation] = $this->setJakartaInputProperty();
        [$suggestion2, $areaGeolocation2] = $this->setBandungInputProperty();
        DB::commit();

        $this->artisan('area:generate-geolocation-mapping', ['--keywords' => 'jakarta,Bandung']);

        $this->assertDatabaseHas(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        $this->assertDatabaseHas(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation2->id,
                'search_input_keyword_suggestion_id' => $suggestion2->id
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    public function testHandleWithSuggestionIDCreateNewRecord()
    {
        [$suggestion, $areaGeolocation] = $this->setJakartaInputProperty();
        DB::commit();

        $this->artisan('area:generate-geolocation-mapping', ['--suggestion-id' => $suggestion->id]);

        $this->assertDatabaseHas(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    public function testHandleWithSuggestionIDUsingInvalidInput()
    {
        [$suggestion, $areaGeolocation] = $this->setJakartaInputProperty();
        DB::commit();

        $this->artisan('area:generate-geolocation-mapping', ['--suggestion-id' => 'test']);

        $this->assertDatabaseMissing(
            'area_geolocation_mapping',
            [
                'area_geolocation_id' => $areaGeolocation->id,
                'search_input_keyword_suggestion_id' => $suggestion->id
            ]
        );

        // because we are using DB::commit(), we need to delete the record manually
        $this->deleteTableRecords();
    }

    private function setJakartaInputProperty(): array
    {
        $inputKeyword = factory(InputKeyword::class)->create(['keyword' => 'jakarta']);

        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'input_id' => $inputKeyword->id,
                'suggestion' => 'Jakarta',
                'area' => 'Daerah Khusus Ibukota Jakarta, Indonesia',
            ]
        );

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);

        return [
            $suggestion,
            $areaGeolocation
        ];
    }

    private function setBandungInputProperty(): array
    {
        $inputKeyword = factory(InputKeyword::class)->create(['keyword' => 'Bandung']);

        $suggestion = factory(InputKeywordSuggestion::class)->create(
            [
                'input_id' => $inputKeyword->id,
                'suggestion' => 'Bandung',
                'area' => 'Jawa Barat, Indonesia',
            ]
        );

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jawa Barat', 'city' => 'Bandung']);

        return [
            $suggestion,
            $areaGeolocation
        ];
    }

    private function deleteTableRecords()
    {
        DB::table('area_geolocation_mapping')->delete();
        DB::table('area_geolocation')->delete();
        DB::table('search_input_keyword_suggestion')->delete();
        DB::table('search_input_keyword')->delete();
    }
}
