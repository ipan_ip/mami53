<?php

namespace App\Console\Commands\Bbk;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class NotifyUnregisteredBbkTest extends MamiKosTestCase
{
    public function testRunCommandDayOneSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 1 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseHas('notification', ['designer_id' => $room->id]);
    }

    public function testRunCommandWithKostStatusRejectSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 1 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_UNVERIFY_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseHas('notification', ['designer_id' => $room->id]);
    }

    public function testRunCommandWithKostStatusVerifiedFailed()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 1 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_VERIFY_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseMissing('notification', ['designer_id' => $room->id]);
    }

    public function testRunCommandDayThreeSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 3 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseHas('notification', ['designer_id' => $room->id]);
    }

    public function testRunCommandDaySevenSuccess()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 7 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseHas('notification', ['designer_id' => $room->id]);
    }

    public function testRunCommandDayTwoFailed()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create([
            'created_at' => date('Y-m-d', strtotime('- 2 day')),
        ]);
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status' => RoomOwner::ROOM_ADD_STATUS,
        ]);
        
        $this->artisan('bbk:notify')->assertExitCode(0);

        $this->assertDatabaseMissing('notification', ['designer_id' => $room->id]);
    }
}
