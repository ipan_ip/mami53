<?php

namespace App\Console\Commands\Bbk;

use App\Entities\Log\ForceBbkCollection;
use App\Entities\Mamipay\MamipayOwnerAgreement;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Test\MamiKosTestCase;
use App\User;

class ForceActivateBbkTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        // clear collection before testing
        ForceBbkCollection::truncate();
    }

    public function testRunCommandWithNoBbkRequest()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);

        $forceBbk = new ForceBbkCollection();
        $forceBbk->id = $room->id;
        $forceBbk->user_id = $user->id;
        $forceBbk->processed = false;
        $forceBbk->save();

        $this->artisan('bbk:activate')->assertExitCode(0);

        $forceBbk = ForceBbkCollection::where('id', $room->id)->first();
        $this->assertTrue($forceBbk->processed);
        $this->assertEquals($forceBbk->status, 'success');
        $this->assertEquals($forceBbk->status_from, null);


        $bbkRequest = $room->booking_owner_requests->first();
        $this->assertEquals($bbkRequest->status, BookingOwnerRequest::BOOKING_APPROVE);
        $this->assertEquals($bbkRequest->requested_by, 'admin');

        $this->assertDatabaseHas(
            'mamipay_owner_agreement',
            [
                'user_id' => $user->id,
                'tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK
            ]
        );
    }

    public function testRunCommandWithBbkRequestWaiting()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);
        factory(BookingOwnerRequest::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status'=> BookingOwnerRequest::BOOKING_WAITING,
        ]);

        $forceBbk = new ForceBbkCollection();
        $forceBbk->id = $room->id;
        $forceBbk->user_id = $user->id;
        $forceBbk->processed = false;
        $forceBbk->save();

        $this->artisan('bbk:activate')->assertExitCode(0);
        
        $forceBbk = ForceBbkCollection::where('id', $room->id)->first();
        $this->assertTrue($forceBbk->processed);
        $this->assertEquals($forceBbk->status, 'success');
        $this->assertEquals($forceBbk->status_from, BookingOwnerRequest::BOOKING_WAITING);


        $bbkRequest = $room->booking_owner_requests->first();
        $this->assertEquals($bbkRequest->status, BookingOwnerRequest::BOOKING_APPROVE);

        $this->assertDatabaseHas(
            'mamipay_owner_agreement',
            [
                'user_id' => $user->id,
                'tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK
            ]
        );
    }

    public function testRunCommandWithBbkRequestReject()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);
        factory(BookingOwnerRequest::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status'=> BookingOwnerRequest::BOOKING_REJECT,
        ]);

        $forceBbk = new ForceBbkCollection();
        $forceBbk->id = $room->id;
        $forceBbk->user_id = $user->id;
        $forceBbk->processed = false;
        $forceBbk->save();

        $this->artisan('bbk:activate')->assertExitCode(0);
        
        $forceBbk = ForceBbkCollection::where('id', $room->id)->first();
        $this->assertTrue($forceBbk->processed);
        $this->assertEquals($forceBbk->status, 'success');
        $this->assertEquals($forceBbk->status_from, BookingOwnerRequest::BOOKING_REJECT);


        $bbkRequest = $room->booking_owner_requests->first();
        $this->assertEquals($bbkRequest->status, BookingOwnerRequest::BOOKING_APPROVE);
        $this->assertEquals($bbkRequest->requested_by, 'admin');

        $this->assertDatabaseHas(
            'mamipay_owner_agreement',
            [
                'user_id' => $user->id,
                'tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK
            ]
        );
    }

    public function testRunCommandWithBbkRequestNotActive()
    {
        $user = factory(User::class)->create();
        $room = factory(Room::class)->create();
        factory(RoomOwner::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
        ]);
        factory(BookingOwnerRequest::class)->create([
            'user_id' => $user->id,
            'designer_id' => $room->id,
            'status'=> BookingOwnerRequest::BOOKING_NOT_ACTIVE,
        ]);

        $forceBbk = new ForceBbkCollection();
        $forceBbk->id = $room->id;
        $forceBbk->user_id = $user->id;
        $forceBbk->processed = false;
        $forceBbk->save();

        $this->artisan('bbk:activate')->assertExitCode(0);
        
        $forceBbk = ForceBbkCollection::where('id', $room->id)->first();
        $this->assertTrue($forceBbk->processed);
        $this->assertEquals($forceBbk->status, 'success');
        $this->assertEquals($forceBbk->status_from, BookingOwnerRequest::BOOKING_NOT_ACTIVE);


        $bbkRequest = $room->booking_owner_requests->first();
        $this->assertEquals($bbkRequest->status, BookingOwnerRequest::BOOKING_APPROVE);
        $this->assertEquals($bbkRequest->requested_by, 'admin');

        $this->assertDatabaseHas(
            'mamipay_owner_agreement',
            [
                'user_id' => $user->id,
                'tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK
            ]
        );
    }
}
