<?php

namespace App\Console\Commands\Room;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Room\Geolocation;
use App\Entities\Room\Room;
use App\Test\MamiKosTestCase;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class GenerateGeolocationMappingTest extends MamiKosTestCase
{
    public function testHandle()
    {
        $this->artisan('room:generate-geolocation-mapping')->assertExitCode(0);
    }

    public function testHandleWithLastUpdateOption()
    {
        // Room is not Updated for 5 days, should not sync data
        $this->artisan('room:generate-geolocation-mapping', ['--last-update' => '3'])
            ->assertExitCode(0);
    }

    public function testHandleWithRoomIdOption()
    {
        $this->setGeolocationProperty();

        // Room ID isn't valid, should not sync data
        $this->artisan('room:generate-geolocation-mapping', ['--room-id' => 'test'])
            ->expectsOutput('Please enter valid ID!')
            ->assertExitCode(0);
    }

    public function testHandleWithKeywordOption()
    {
        $this->setGeolocationProperty();

        // There were No Input Keyword Record in DB, should not sync data
        $this->artisan('room:generate-geolocation-mapping', ['--keyword' => 'Mamiroom'])
            ->expectsOutput('No Kos data could be processed!')
            ->assertExitCode(0);
    }

    public function testHandleWithMultipleKeywordsOption()
    {
        $this->setGeolocationProperty();

        // There were No Input Keyword Record in DB, should not sync data
        $this->artisan('room:generate-geolocation-mapping', ['--keywords' => 'Mamiroom, Sakti'])
            ->expectsOutput('No Kos data could be processed!')
            ->assertExitCode(0);
    }

    private function setGeolocationProperty(): array
    {
        $room = factory(Room::class)->state('active')->create(
            [
                'name'       => 'Mamiroom Sakti',
                'updated_at' => Carbon::now()->subDays(5)->format('Y-m-d')
            ]
        );

        factory(Geolocation::class)->create(
            [
                'designer_id' => $room->id,
                'geolocation' => new Point(40.74894149554007, -73.98615270853044),
            ]
        );

        $areaGeolocation = factory(AreaGeolocation::class)->create(['province' => 'Jakarta Raya', 'city' => 'Jakarta']);
        return [
            $room,
            $areaGeolocation
        ];
    }
}
