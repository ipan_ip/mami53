<?php

namespace app\Console\Commands\Booking;

use App\Entities\Mamipay\MamipayContract;
use App\Jobs\SendPushNotificationQueue;
use App\Test\MamiKosTestCase;
use App\User;
use Illuminate\Support\Facades\Event;
use Mockery;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class BookingOwnerEmptyRoomTest extends MamiKosTestCase
{
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->mockAlternatively('App\Repositories\Booking\BookingUserRepository');
    }

    public function testCommandSuccess(): void
    {
        // disable jobs
        Event::fake();

        // prepare data
        $ownerEntity = factory(User::class)->create();
        $contractEntity = factory(MamipayContract::class)->create([
            'owner_id' => $ownerEntity->id,
        ]);
        $logFormat = 'Send push notification to owner [%s]';

        // add booking user id
        $contractEntity->booking_user_id = 1;

        $data = [];
        $data[] = $contractEntity;

        // mocking repository
        $this->mock->shouldReceive('getActiveContractEmptyRoom')
                    ->once()
                    ->andReturn($data);

        // running test command
        $this->artisan('booking:notification-owner-empty-room')
            ->expectsOutput(sprintf($logFormat, $ownerEntity->id))
            ->assertExitCode(0);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }
}