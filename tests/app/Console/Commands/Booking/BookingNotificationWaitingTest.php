<?php


namespace app\Console\Commands\Booking;


use App\Repositories\Booking\BookingUserRepositoryEloquent;
use App\Test\MamiKosTestCase;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class BookingNotificationWaitingTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testCommandListWaitingBookingNotification()
    {
        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('crawlNotificationWaiting')
            ->andReturn([]);

        $this->artisan('booking:notification-waiting')
            ->expectsOutput('Crawl Booking notification waiting for admin confirmation')
            ->assertExitCode(0);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }
}