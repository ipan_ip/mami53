<?php

namespace app\Console\Commands\Booking;

use App\Test\MamiKosTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Repositories\Booking\BookingUserRepositoryEloquent;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */

class BookingHasFinishedTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testCommandBookingHasFinished()
    {
        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('crawlNotificationBookingHasFinished')
            ->andReturn([]);

        $this->artisan('booking:has-finished')
            ->expectsOutput('Crawl notification booking has finished')
            ->assertExitCode(0);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }

}