<?php
namespace app\Console\Commands\Booking;

use App\Test\MamiKosTestCase;

class ThanosBookingRequestTest extends MamiKosTestCase 
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Console/Commands/Booking/ThanosBookingRequest
     */
    public function testThanosBookingRequestCommandCommitMode()
    {
        $this->artisan('thanos-request-booking:snap', [
            '--csv-file'    => 'booking_owner_request_waiting.csv',
            '--mode'        => 'commit',
            '--memory-limit' => -1
            ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }

    /**
     * @group UG
     * @group UG-1151
     * @group app/Console/Commands/Booking/ThanosBookingRequest
     */
    public function testThanosBookingRequestCommandRollbackMode()
    {
        $this->artisan('thanos-request-booking:snap', [
            '--csv-file'    => 'booking_owner_request_waiting.csv',
            '--mode'        => 'rollback',
            '--memory-limit' => -1
            ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }
}
