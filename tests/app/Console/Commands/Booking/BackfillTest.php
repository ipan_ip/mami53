<?php

namespace app\Console\Commands\Booking;

use App\Test\MamiKosTestCase;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class BackfillTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    public function testBackfillCommandCommitMode(): void
    {
        $this->markTestSkipped('Temporary skip this test');
        $this->artisan('booking:backfill-reject-reason-id', [
            '--mode'        => 'commit',
        ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }

    public function testBackfillCommandRollbackMode(): void
    {
        $this->markTestSkipped('Temporary skip this test');
        $this->artisan('booking:backfill-reject-reason-id', [
            '--mode'        => 'rollback',
        ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);
    }
}