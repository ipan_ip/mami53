<?php

namespace app\Console\Commands\Booking;

use App\Test\MamiKosTestCase;
use Mockery;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class BookingReminderNotificationTest extends MamiKosTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->mockAlternatively('App\Services\Booking\BookingService');
    }

    public function testCommandSuccess(): void
    {
        // mocking repository
        $this->mock->shouldReceive('getReminderNotificationOwnerAcceptBooking')
            ->once()
            ->andReturn(true);

        // running test command
        $this->artisan('booking:reminder-notification')
            ->expectsOutput('Reminder notification running successfully')
            ->assertExitCode(0);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }
}