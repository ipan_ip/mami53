<?php

namespace app\Console\Commands\Booking;

use App\Services\Booking\BookingAcceptanceRateService;
use App\Test\MamiKosTestCase;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class BookingAcceptanceRateCalculationTest extends MamiKosTestCase
{

    public function testCommandBookingAcceptanceCalculation()
    {
        $mockService = $this->mockAlternatively(BookingAcceptanceRateService::class);

        $mockService->shouldReceive('calculateBookingAcceptanceRateAllBookingRooms')
            ->andReturn(true);

        $this->artisan('booking:acceptance-rate-calculation')
            ->assertExitCode(0);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }

}