<?php


namespace app\Console\Commands\Booking;

use App\Repositories\Booking\BookingUserRepositoryEloquent;
use App\Test\MamiKosTestCase;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class BookingNotificationWaitingTodayTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testCommandBookngNotification()
    {
        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('crawlNotificationWaitingToday')
            ->andReturn([]);

        $this->artisan('booking:notification-waiting-today')
            ->expectsOutput('Crawl Booking notification waiting today for owner or admin confirmation')
            ->assertExitCode(0);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }
}