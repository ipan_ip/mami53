<?php

namespace app\Console\Commands\Booking;

use App\Test\MamiKosTestCase;
use App\Repositories\Booking\BookingUserRepositoryEloquent;

/**
 *  @runTestsInSeparateProcesses
 *  @preserveGlobalState disabled
 */
class BookingHasPassedTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testCommandBookingHasPassed()
    {
        $mockRepo = $this->mockAlternatively(BookingUserRepositoryEloquent::class);
        $mockRepo->shouldReceive('crawlNotificationBookingHasPassed')
            ->andReturn([]);

        $this->artisan('booking:has-passed')
            ->expectsOutput('Crawl notification booking has passed')
            ->assertExitCode(0);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }

}