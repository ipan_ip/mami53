<?php

namespace App\Console\Commands;

use App\Test\MamiKosTestCase;
use App\Console\Commands\NotifDateLimitPremium;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\User;

class NotifDateLimitPremiumTest extends MamiKosTestCase
{
    
    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testNotificationToOwnerEmptyData()
    {
        $result = (new NotifDateLimitPremium())->premiumDateLimitNotification('package', 1);
        $this->assertNull($result);
    }

    public function testTrialPackageReminderSuccess()
    {
        $user = factory(User::class)->create([
            "id" => 1,
            "is_owner" => "true",
            "date_owner_limit" => date('Y-m-d', strtotime('+1 days')),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 1,
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "id" => 1,
                "for" => "trial",
            ]),
        ]);

        $result = (new NotifDateLimitPremium())->premiumDateLimitNotification('trial', 1);
        $this->assertNull($result);
    }

    public function testPremiumPackageReminderSuccess()
    {
        $user = factory(User::class)->create([
            "id" => 2,
            "is_owner" => "true",
            "date_owner_limit" => date('Y-m-d', strtotime('+1 days')),
        ]);

        $premiumRequest = factory(PremiumRequest::class)->create([
            "id" => 2,
            "expired_status" => null,
            "user_id" => $user->id,
            "status" => "1",
            "premium_package_id" => factory(PremiumPackage::class)->create([
                "id" => 2,
                "for" => "package",
            ]),
        ]);

        $result = (new NotifDateLimitPremium())->premiumDateLimitNotification('package', 1);
        $this->assertNull($result);
    }
    
}