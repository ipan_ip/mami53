<?php
namespace app\Console\Commands\RoomUnit;

use App\Entities\Level\RoomLevel;
use App\Test\MamiKosTestCase;
use Illuminate\Support\Facades\Config;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;

class AutoAssignRoomLevelTest extends MamiKosTestCase
{
    const COMMIT_MODE = 'commit';
    const ROLLBACK_MODE = 'rollback';

    protected function setUp() : void
    {
        parent::setUp();
        $this->setupGoldplusLevel();
    }

    private function setupGoldplusLevel()
    {
        // creating regular kost level to make Room->level_info works
        factory(KostLevel::class)->create([
            'is_regular' => true,
        ]);

        [$this->kostOldGP1, $this->kostOldGP2] = factory(KostLevel::class)->times(2)->create();
        Config::set('kostlevel.id.goldplus1', $this->kostOldGP1->id);
        Config::set('kostlevel.id.goldplus2', $this->kostOldGP2->id);

        [$this->roomOldGP1, $this->roomOldGP2] = factory(RoomLevel::class)->times(4)->create();
        Config::set('roomlevel.id.goldplus1', $this->roomOldGP1->id);
        Config::set('roomlevel.id.goldplus2', $this->roomOldGP2->id);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }

    /**
     * @group UG
     * @group UG-4602
     * @group app/Console/Commands/RoomUnit/AutoAssignRoomLevel
     */
    public function testAutoAssignRoomLevelCommandCommitMode()
    {
        $room = factory(Room::class)->create(['id' => 123]);
        $room->changeLevel($this->kostOldGP1->id);

        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);

        $this->artisan('roomunit:auto-assign-room-level', [
            '--csv-file'    => 'kost_list_example.csv',
            '--mode'        => self::COMMIT_MODE
            ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);

        $this->assertEquals($this->roomOldGP1->id, $roomUnit->refresh()->room_level_id);

        $this->assertDatabaseHas('auto_assign_room_level_log', [
            'designer_room_id' => $roomUnit->id,
            'room_level_id' => 0,
            'mode' => self::COMMIT_MODE,
            'status' => true
        ], 'mongodb');
    }

    /**
     * @group UG
     * @group UG-4602
     * @group app/Console/Commands/RoomUnit/AutoAssignRoomLevel
     */
    public function testAutoAssignRoomLevelCommandCommitAndRollback()
    {
        $room = factory(Room::class)->create(['id' => 123]);
        $room->changeLevel($this->kostOldGP1->id);

        $roomUnit = factory(RoomUnit::class)->create(['designer_id' => $room->id]);
        $existingRoomLevel = $roomUnit->room_level_id;

        $this->artisan('roomunit:auto-assign-room-level', [
            '--csv-file'    => 'kost_list_example.csv',
            '--mode'        => self::COMMIT_MODE
            ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);

        $this->assertEquals($this->roomOldGP1->id, $roomUnit->refresh()->room_level_id);
        
        $this->assertDatabaseHas('auto_assign_room_level_log', [
            'designer_room_id' => $roomUnit->id,
            'room_level_id' => 0,
            'mode' => self::COMMIT_MODE
        ], 'mongodb');

        $this->artisan('roomunit:auto-assign-room-level', [
            '--csv-file'    => 'kost_list_example.csv',
            '--mode'        => self::ROLLBACK_MODE
            ])
            ->expectsQuestion('Are you sure to process this command? (y/n)', 'y')
            ->assertExitCode(0);

        $this->assertEquals($existingRoomLevel, $roomUnit->refresh()->room_level_id);
        
        $this->assertDatabaseHas('auto_assign_room_level_log', [
            'designer_room_id' => $roomUnit->id,
            'room_level_id' => 0,
            'mode' => self::ROLLBACK_MODE
        ], 'mongodb');

        $this->assertDatabaseMissing('auto_assign_room_level_log', [
            'designer_room_id' => $roomUnit->id,
            'room_level_id' => 0,
            'mode' => self::COMMIT_MODE
        ], 'mongodb');
    }    
}