<?php

namespace app\Console\Commands\Tenant;

use App\Test\MamiKosTestCase;

class DuplicatePhoneNumberFixerTest extends MamiKosTestCase
{
    protected function setUp() : void
    {
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
        \Mockery::close();
    }

    /**
     * @group UG
     * @group UG-4450
     * @group app/Console/Commands/Tenant/DuplicatePhoneNumberFixer
     */
    public function testHandleCommand(): void
    {
        $this->artisan('phone-number-fixer:fix', [
            '--filename'            => 'booking_owner_request_waiting.csv',
            '--operation-mode'      => 'commit',
            '--memory-limit' => '1024M'
            ])
            ->expectsQuestion('Are you sure to process this command? (commit mode) (y/n)', 'n')
            ->assertExitCode(0);
    }
}