var myGlobal = {};

myGlobal.bootstrapValidatorDefaults = {
  excluded: [':disabled', ':not(:visible)'],
  feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
  },
  live: 'enabled',
  message: 'This value is not valid',
  submitButtons: 'button[type="submit"]',
};

function updateValidateStatus (objBootstrapValidator, arrMessage) {
  $.each(arrMessage , function( i, item ){
    objBootstrapValidator.updateStatus(item[0], item[1], item[2]);
  });
}

$(function()
{
  $content = $(".content");

  // setTimeout(function() {
  //   $content.find('.alert').hide();
  // }, 6000);

  $body = $("body");

  $(document).on({
    // ajaxStart: function() {
    //   console.log('called');
    //   $body.addClass("loading");
    // },
    ajaxStop: function() {
      $body.removeClass("loading");
    }
  });
});

// function showModalDelete (deleteResource, deleteId) {
//     var $modalDelete = $('#modalDelete');
//     // $modalDelete.find('#deleteResource').html(deleteResource);
//     // $modalDelete.find('#deleteId').html(deleteId);
//     $modalDelete.modal();
// }