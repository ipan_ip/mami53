function myFileUpload(wrapperFileUpload, url, mediaType, validator) {  
  // alert(wrapperFileUpload.attr('id'));
  Holder.addTheme("thumbnail", {
    background: '#EEE',
    foreground: '#AAA',
    size: 10,
    fontweight: 'normal',
    text: 'Klik untuk upload foto'
  });

  wrapperFileUpload.find('#wrapper-img').click(function () {
    wrapperFileUpload.find('#fileupload').click();
  });

  wrapperFileUpload.find('#fileupload').fileupload({
      url: url,
      formData: {
        watermarking: true,
        watermarking_type: "premium",
        media_type : mediaType
      },
      disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
      imageMaxWidth: 1920,
      imageMaxHeight: 1080,
      maxNumberOfFiles: 1,
      dataType: 'json',
      done: function (e, data) {
          if (data.result.status === true)
          {
            var photoId = '<b>Photo ID: </b>' + data.result.media.id + '<br>';
            var name = '<b>Name: </b>' + data.result.media.file_name + '<br>';
            var url = '<b>URL: </b>' + data.result.media.url.real + '<br>';
            wrapperFileUpload.find('#uploaded-file-info').append(name + photoId + url);
            wrapperFileUpload.find('.media-object').attr('src', data.result.media.url.medium);

            if(data.result.media.file_name.indexOf(".gif") > 1)
            {
              wrapperFileUpload.find('.media-object').attr('src',data.result.media.url.real);
            }
            wrapperFileUpload.find('#photo_id').val(data.result.media.id);
            wrapperFileUpload.find('#fileupload1').val(data.result.media.url.real);

            if(validator === 1){
              $form
                .bootstrapValidator('updateStatus', wrapperFileUpload.find('#photo_id'), 'VALID')
                .bootstrapValidator('validateField', wrapperFileUpload.find('#photo_id'));
            }
          }
          else
          {
            var error = $('<span class="text-danger"/>').text('File upload failed. ' + data.result.meta.message);
            wrapperFileUpload.find('#files').html(error);
          }          
      },
      progressall: function (e, data) {
          var progress = parseInt(data.loaded / data.total * 100, 10);
          wrapperFileUpload.find('#progress .progress-bar').css(
              'width',
              progress + '%'
          );
      },
      fail: function (e, data) {
        var error = $('<span class="text-danger"/>').text('File upload failed.');
          $(data.context.children()[index])
              .append('<br>')
              .append(error);
      },
      add: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
          wrapperFileUpload.find('#progress .progress-bar').css(
              'width',
              0 + '%'
          );

        wrapperFileUpload.find('#files').html('');
        wrapperFileUpload.find('#uploaded-file-info').html('');

        var addedFile = data.files[0];

        var fileUpload = $(this);

        readImage(addedFile,mediaType).done(function (fileInfo)
        {
          if (fileInfo.status === true) {
            if (data.autoUpload || (data.autoUpload !== false &&
                    fileUpload.fileupload('option', 'autoUpload'))) {
                data.process().done(function () {
                    data.submit();
                });
            }
          } else {
            var error = $('<span class="text-danger"/>').text(fileInfo.error);
            wrapperFileUpload.find('#files').html(error);
          }

          var fileInfoDisplay = {
            name : '<b>Name: </b>' + addedFile.name + '<br>',
            type : '<b>Type: </b>' + addedFile.type + '<br>',
            size : '<b>Size: </b>' + fileInfo.sizeInKB + ' KB<br>',
            dimension : '<b>Dimension: </b>' + fileInfo.width + ' x ' + fileInfo.height + ' px<br>',
          };

          var message = '';

          $.each(fileInfoDisplay, function(i, item) {
            message += item;
          });

          wrapperFileUpload.find('#original-file-info').html(message);
        });

        $.blueimp.fileupload.prototype.options.add.call(this, e, data);
      }
  }).prop('disabled', !$.support.fileInput)
      .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function readImage(file,mediaType) {
    var deferred = new $.Deferred();

    // var url = window.URL || window.webkitURL; // alternate use
    
    var reader = new FileReader();
    var image  = new Image();
    var info = {};

    reader.readAsDataURL(file);

    reader.onload = function(_file) {
        image.src    = _file.target.result;              // url.createObjectURL(file);
        image.onload = function() {
            info = {
              status : true,
              width : this.width,
              height : this.height,
              type : file.type,                           // ext only: // file.type.split('/')[1],
              name : file.name,
              sizeInKB : Math.round(file.size/1024)
            };

            if ((this.width < 120 || this.height < 120) && mediaType != 'tag_photo')
            {
              info.status = false;
              info.error = 'Invalid file width and height: ' +
                  this.width + ' x ' + this.height + ' px. ' +
                  'Width and Height must be at least 640 x 640 px.';
            }
            deferred.resolve(info);
        };
        image.onerror = function() {
            info = {
              status : false,
              error : 'Invalid file type: '+ file.type
            };
            deferred.resolve(info);
        };
    };

    return deferred.promise();
}

function uploadFromUrl(wrapper, url, mediaUrl, validator){
    var request = new XMLHttpRequest();
    request.open("GET", url, true);
   
    request.onload = function (e) {
      if (request.readyState === 4) {
        if (request.status === 200) {
          var data = request.responseText;
          obj    = JSON.parse(data);
          //alert(obj.url.small);
          var photoId = '<b>Photo ID: </b>' + obj.id + '<br>';
          var name = '<b>Name: </b>' + obj.file_name + '<br>';
          var url = '<b>URL: </b>' + obj.url.real + '<br>';
          wrapper.find('#uploaded-file-info').append(name + photoId + url);
          console.log(obj);
          wrapper.find('.media-object').attr('src', obj.url.medium);
      
          if(obj.file_name.indexOf(".gif") > 1)
          {
            wrapper.find('.media-object').attr('src',obj.url.real);
          }
          wrapper.find('#photo_id').val(obj.id);
          wrapper.find('#sourceImg').val(mediaUrl);
          wrapper.find('#progress .progress-bar').css('width','100%');

          if(validator === 1){
            $form
                .bootstrapValidator('updateStatus', wrapper.find('#photo_id'), 'VALID')
                .bootstrapValidator('validateField', wrapper.find('#photo_id'));
          }

        } else {
          //console.error(request.statusText);
          var error = $('<span class="text-danger"/>').text("Can't Download image");
          wrapper.find('#files').html(error);
        }
      }
    };
    request.onerror = function (e) {
      console.error(request.statusText);
    };
    request.send(null);
}  