var tab = {
	hash: '',
	request: ''
};

function calculateRating(rating) {
	var value = parseFloat(rating).toFixed(1);
	var splitValue = value.split('.');
	var full = parseInt(splitValue[0]);
	var half = splitValue[1] > 0 ? full + 1 : false;
	return {
		full,
		half
	};
}

var tabChange = function(requestPage) {
	if (tab.hash == 'survey') {
		tab.request = 'owner/surveys';
	} else if (tab.hash == 'telepon') {
		tab.request = 'owner/call';
	} else if (tab.hash == 'chat') {
		tab.request = 'owner/called';
	} else if (tab.hash == 'review') {
		tab.request = 'stories/review';
	}
	stats.loading = true;
	stats.null = false;

	if (requestPage !== undefined) {
		stats.page = requestPage;
	}

	$.ajax({
		type: 'GET',
		url: '/garuda/' + tab.request + '/' + owner.roomId + '?page=' + stats.page,
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			if (response.review) {
				var review = response.review.map(function(review) {
					var clean = calculateRating(review.clean);
					var happy = calculateRating(review.happy);
					var roomFacilities = calculateRating(review.room_facilities);
					var safe = calculateRating(review.safe);
					var publicFacilities = calculateRating(review.public_facilities);
					var pricing = calculateRating(review.pricing);
					var newReviewData = {
						clean,
						happy,
						room_facilities: roomFacilities,
						safe,
						public_facilities: publicFacilities,
						pricing
					};
					return Object.assign(review, newReviewData);
				});
			}
			stats.data = response;
			if (response.review) {
				stats.data.review = review;
			}
			stats.loading = false;

			stats.data.startingListNumber = (stats.page - 1) * 15 + 1;
			// 15 is maximum item on a single page

			if (stats.data.count !== undefined && stats.data.count == 0) {
				stats.null = true;
			} else {
				if (tab.hash === 'review') {
					stats.null = !stats.data.total;
				} else {
					stats.null = !stats.data.count;
				}
			}

			stats.totalPage =
				tab.hash == 'review' ? response.total_page : response.page_total;
		}
	});
};

var stats = {
	data: {},
	loading: false,
	null: false,
	page: 1,
	totalPage: 0
};

var getreplies = {
	rating: {
		clean: {
			full: 0,
			half: 0
		},
		happy: {
			full: 0,
			half: 0
		},
		safe: {
			full: 0,
			half: 0
		},
		pricing: {
			full: 0,
			half: 0
		},
		room_facilities: {
			full: 0,
			half: 0
		},
		public_facilities: {
			full: 0,
			half: 0
		}
	},

	count: 0,
	result: {},
	replies: [],
	caption: 'Foto review dari ',
	jsonld: ''
};

var stateReply = false;

var openChatHandler = function() {
	const chatElement = document.querySelector('.chat-menu');

	chatElement && chatElement.click();
};

$(window).on('hashchange', function() {
	tab.hash = window.location.hash.substr(1);
	stats.page = 1;
	stats.totalPage = 1;
	tabChange();
});

Vue.config.productionTip = false;

var ownerStats = new Vue({
	el: '#statistics',
	data: {
		csrfToken: document.head.querySelector('meta[name="csrf-token"]').content,
		tab,
		tabChange,
		active: 'stats-title-active',
		inactive: 'stats-title-inactive',
		caption: 'Foto Review dari ',
		stats,
		owner,
		stateReply,
		review: {},
		limit: 140,
		replyContent: '',
		promoIsExist: false,
		getreplies
	},
	computed: {
		hasTelp() {
			return this.getQueryString('telp') === 'true';
		},
		charLeft() {
			return this.limit - this.replyContent.length + ' / ' + this.limit;
		}
	},
	mounted() {
		tab.hash = window.location.hash.substr(1);
		tabChange();
	},
	methods: {
		getQueryString: function(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, '\\$&');
			var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
			var results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, ' '));
		},
		selectTab: function(tabName) {
			tab.hash = tabName;
			window.location.hash = tabName;
		},
		nextPage: function(currentPage) {
			tabChange(currentPage + 1);
		},
		previousPage: function(currentPage) {
			tabChange(currentPage - 1);
		},
		surveyChat: function(
			groupChannelUrl,
			surveyUser,
			userId,
			realName,
			adminId
		) {
			// open loading modal
			$('#overlay').modal({
				backdrop: 'static',
				keyboard: false
			});

			var surveyGroupChannelUrl = groupChannelUrl;
			var surveyUserId = userId;

			if (surveyGroupChannelUrl != '' && surveyGroupChannelUrl != null) {
				sb.GroupChannel.getChannel(surveyGroupChannelUrl, function(
					groupChannel,
					error
				) {
					if (error) {
						return;
					}

					var ownerFound = false;

					for (i = 0; i < groupChannel.members.length; i++) {
						var groupChannelMember = groupChannel.members[i];

						if (groupChannelMember.userId == owner.id) {
							ownerFound = true;

							break;
						}
					}

					if (ownerFound) {
						var userGroup = [owner.id];

						var metadata = {
							chat_support: false
						};
						var upsertIfNotExist = true;

						groupChannel.updateMetaData(
							metadata,
							upsertIfNotExist,
							(response, error) => {
								if (error) {
									error && bugsnagClient.notify(JSON.stringify(error));
								}
							}
						);

						sbWidget.showChannel(groupChannelUrl);

						// hide loading modal
						$('#overlay').modal('hide');
						openChatHandler();
					} else {
						var userGroup = [owner.id];

						groupChannel.inviteWithUserIds(userGroup, function(
							response,
							error
						) {
							if (error) {
								return;
							}

							var metadata = {
								chat_support: false
							};
							var upsertIfNotExist = true;

							groupChannel.updateMetaData(
								metadata,
								upsertIfNotExist,
								(response, error) => {
									if (error) {
									}
								}
							);

							sbWidget.showChannel(groupChannelUrl);

							// hide loading modal
							$('#overlay').modal('hide');
							openChatHandler();
						});
					}
				});
			} else {
				var groupName = 'Survey - ' + survey_user_name + ' : ' + owner.room;

				var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
				channelListQuery.includeEmpty = true;

				channelListQuery.next(function(groupChannels, error) {
					if (error) {
						return;
					}

					groupChannels.forEach(function(group, index) {
						if (
							group.name.trim().toLowerCase() === groupName.trim().toLowerCase()
						) {
							surveyGroupChannelUrl = group.url;
						}
					});

					if (surveyGroupChannelUrl == '' || surveyGroupChannelUrl == null) {
						var userGroup = [surveyUserId, adminId, owner.id];

						var groupImage = owner.roomPhoto;

						sb.GroupChannel.createChannelWithUserIds(
							userGroup,
							false,
							groupName,
							groupImage,
							'',
							function(groupChannel, error) {
								if (error) {
									return;
								}

								sbWidget.showChannel(groupChannel.url);

								// hide loading modal
								$('#overlay').modal('hide');
								openChatHandler();
							}
						);
					} else {
						sbWidget.showChannel(surveyGroupChannelUrl);

						// hide loading modal
						$('#overlay').modal('hide');
						openChatHandler();
					}
				});
			}
		},
		telpChat: function(userName, userId, userEmail, adminId) {
			swal('Perhatian', 'Maaf, fitur ini sudah expired.', 'warning');
		},
		telpModal: function(userName, userPhone) {
			$('#download-app').modal('show');
			$('#telpUserName').text(userName);
			$('#telpUserNumber').text(userPhone);

			var clipboard = new Clipboard('.phone-user-btn');

			clipboard.on('success', function(e) {
				swal('', userPhone + ' berhasil disalin ke papanklip!', 'success');

				e.clearSelection();
			});
		},
		replyChat: function(userName, userId, adminId, groupChannelUrl) {
			// open loading modal
			$('#overlay').modal({
				backdrop: 'static',
				keyboard: false
			});

			sb.GroupChannel.getChannel(groupChannelUrl, function(
				groupChannel,
				error
			) {
				if (error) {
					return;
				}

				if (owner.id != null) {
					var userGroup = [owner.id];

					groupChannel.inviteWithUserIds(userGroup, function(response, error) {
						if (error) {
							error && bugsnagClient.notify(JSON.stringify(error));
						}
					});
				}

				var metadata = {
					chat_support: false
				};
				var upsertIfNotExist = true;

				groupChannel.updateMetaData(
					metadata,
					upsertIfNotExist,
					(response, error) => {
						if (error) {
							error && bugsnagClient.notify(JSON.stringify(error));
						}
					}
				);

				sbWidget.showChannel(groupChannelUrl);

				// hide loading modal
				$('#overlay').modal('hide');
				openChatHandler();
			});
		},
		openReplyModal: function(thisToken) {
			stateReply = false;
			$('#overlay').modal({
				backdrop: 'static',
				keyboard: false
			});
			$.ajax({
				type: 'GET',
				url: '/garuda/stories/review/reply/' + thisToken,
				headers: {
					'Content-Type': 'application/json',
					'X-GIT-Time': '1406090202',
					Authorization: 'GIT WEB:WEB'
				},
				crossDomain: true,
				dataType: 'json',
				success: function(getReviewResponse) {
					var clean = calculateRating(getReviewResponse.review.clean);
					var happy = calculateRating(getReviewResponse.review.happy);
					var roomFacilities = calculateRating(
						getReviewResponse.review.room_facilities
					);
					var safe = calculateRating(getReviewResponse.review.safe);
					var publicFacilities = calculateRating(
						getReviewResponse.review.public_facilities
					);
					var pricing = calculateRating(getReviewResponse.review.pricing);
					var rating = {
						clean,
						happy,
						room_facilities: roomFacilities,
						safe,
						public_facilities: publicFacilities,
						pricing
					};
					getreplies.result = getReviewResponse.review;
					getreplies.rating = rating;
					getreplies.count = getReviewResponse.count;
					getreplies.replies = getReviewResponse.reply;
					if (getreplies.count > 0) {
						var jsonldel = document.createElement('script');
						jsonldel.type = 'application/ld+json';
						jsonldel.text = JSON.stringify({
							'@context': 'http://schema.org',
							'@type': 'AggregateRating',
							ratingValue: parseFloat(getreplies.strRating),
							bestRating: 4,
							worstRating: 1,
							ratingCount: getreplies.count
						});
						document.querySelector('head').appendChild(jsonldel);
					}
					stateReply = true;
				},
				error: function(getReviewResponse) {},
				complete: function(getReviewResponse) {
					$('#overlay').modal('hide');
					stateReply = true;
					this.review = review;
					$('#modalShowReply').modal('show');
				}
			});
		},

		sendReply: function() {
			if (this.replyContent.length > 140 || this.replyContent.length == 0) {
				alert('Anda tidak bisa menulis balasan kosong');
			} else {
				$('#overlay').modal({
					backdrop: 'static',
					keyboard: false
				});
				$.ajax({
					type: 'POST',
					url: '/garuda/stories/review/reply',
					headers: {
						'Content-Type': 'application/json',
						'X-GIT-Time': '1406090202',
						Authorization: 'GIT WEB:WEB'
					},
					data: JSON.stringify({
						song_id: getreplies.result.song_id,
						id: getreplies.result.review_id,
						content: this.replyContent,
						_token: this.csrfToken
					}),
					crossDomain: true,
					dataType: 'json',
					success: function(postReplyResponse) {
						$('#overlay').modal('hide');
						swal('', 'Kirim balasan berhasil', 'success');
						location.reload();
					}
				});
			}
		}
	}
});

// RUN SWIPEBOX FUNCTION
$(document).ready(function() {
	$('.swipebox').swipebox({
		useSVG: false,
		removeBarsOnMobile: false,
		hideBarsDelay: 0,
		afterOpen: function() {
			var $selectorClose = $('#swipebox-close');
			var clickAction = 'touchend click';

			$selectorClose.unbind(clickAction);

			$selectorClose.bind(clickAction, function(event) {
				event.preventDefault();
				event.stopPropagation();

				$.swipebox.close();
			});
		},
		loopAtEnd: true
	});
});

// EVENT FOR CLIPBOARD JS
var clipboard = new Clipboard('.phone-info-btn');

clipboard.on('success', function(e) {
	swal('', '+6287738724848 berhasil disalin ke papanklip!', 'success');

	e.clearSelection();
});
