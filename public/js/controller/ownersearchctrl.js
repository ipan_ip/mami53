$(document).ready(function() {
	$('#ownerListMobile').css('display', '');
	$('#ownerList').css('display', '');
});
var data = {
	rooms: []
};

Vue.config.productionTip = false;

////////////////API SUGGESTED KOST (ONCE CALL) & SEARCH KOST OWNER MOBILE////////////////
var searchListMobile = new Vue({
	el: '#ownerListMobile',
	data: {
		data
	}
});

var searchMobile = new Vue({
	el: '#ownerSearchMobile',
	data: {
		ownerInputMobile: '',
		data
	},
	mounted() {
		$.ajax({
			type: 'GET',
			url: '/garuda/owner/suggested',
			headers: {
				'Content-Type': 'application/json',
				'X-GIT-Time': '1406090202',
				Authorization: 'GIT WEB:WEB'
			},
			crossDomain: true,
			dataType: 'json',
			success: function(suggestedResponse) {
				$('#ownerListMobile').show();
				$('#ownerList').show();
				data.rooms = suggestedResponse.rooms;
			}
		});
	},
	methods: {
		ownerSearchMobile: function(ownerInputMobile) {
			if (this.ownerInputMobile != '') {
				$('#overlay').modal({
					backdrop: 'static',
					keyboard: false
				});
				$('#overlay').modal('show');
				$.ajax({
					type: 'POST',
					url: '/garuda/owner/search',
					headers: {
						'Content-Type': 'application/json',
						'X-GIT-Time': '1406090202',
						Authorization: 'GIT WEB:WEB'
					},
					data: JSON.stringify({
						filters: { room_name: ownerInputMobile },
						_token: tokenOwner
					}),
					crossDomain: true,
					dataType: 'json',
					success: function(searchResponse) {
						data.rooms = searchResponse.rooms;
						$('#overlay').modal('hide');
					}
				});
			}
		},
		ownerNew: function() {
			window.open('/ownerpage/addroom');
		}
	}
});

////////////////API SEARCH KOST OWNER DESKTOP////////////////
var searchList = new Vue({
	el: '#ownerList',
	data: {
		data
	}
});

var search = new Vue({
	el: '#ownerSearch',
	data: {
		ownerInput: '',
		data
	},
	methods: {
		ownerSearch: function(ownerInput) {
			if (this.ownerInput != '') {
				$('#overlay').modal({
					backdrop: 'static',
					keyboard: false
				});
				$('#overlay').modal('show');
				$.ajax({
					type: 'POST',
					url: '/garuda/owner/search',
					headers: {
						'Content-Type': 'application/json',
						'X-GIT-Time': '1406090202',
						Authorization: 'GIT WEB:WEB'
					},
					data: JSON.stringify({
						filters: { room_name: ownerInput },
						_token: tokenOwner
					}),
					crossDomain: true,
					dataType: 'json',
					success: function(searchResponse) {
						data.rooms = searchResponse.rooms;
						for (i = 0; i < data.rooms.length; i++) {}
						$('#overlay').modal('hide');
					}
				});
			}
		}
	}
});

var addListing = new Vue({
	el: '#modalAddListing',
	data: {
		listingType: 'Kost'
	},
	methods: {
		openInputVacancy: function() {
			window.open('/input-loker', '_self');
		}
	}
});
