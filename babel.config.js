module.exports = {
	plugins: ['@babel/plugin-syntax-dynamic-import'],
	env: {
		test: {
			presets: ['@babel/preset-env'],
			plugins: ['@babel/plugin-transform-runtime']
		}
	}
};
