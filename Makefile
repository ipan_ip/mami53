run: build
	docker-compose up -d webapp nginx && sleep 5 && docker-compose exec webapp bash -c "composer install"

stop:
	docker-compose down

workspace: .composer run 
	docker-compose exec webapp bash

build:
	docker-compose build

test: run
	./scripts/paratest.sh

.composer:
	mkdir .composer