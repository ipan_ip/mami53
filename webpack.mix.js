const mix = require('laravel-mix');
const path = require('path');
const { GenerateSW } = require('workbox-webpack-plugin');
const CleanObsoleteChunks = require('webpack-clean-obsolete-chunks');

const isAnalyzing = false; // Set to "true" when you want to test the bundle size
const isHMR = process.argv.includes('--hot');
const isWatching = process.argv.includes('--watch') || isHMR;

let STATIC_ASSETS_URL = '/';
// Chunk and assets that processed by Webpack/Mix will use CDN high priority schema on production or server test
if (
	process.env.MIX_STATIC_ASSET_URL &&
	!process.env.APP_ENV.startsWith('local')
) {
	STATIC_ASSETS_URL = process.env.MIX_STATIC_ASSET_URL + '/';
}

const webpackPlugins = [
	new CleanObsoleteChunks({
		verbose: true,
		deep: true
	})
];

if (!isWatching) {
	/*
	 * Only generate service worker on "non watch" mode only because Workbox has an unsolved issue -
	 * when Webpack runs on watch mode.
	 * https://github.com/GoogleChrome/workbox/issues/1790
	 */
	webpackPlugins.push(
		new GenerateSW({
			cacheId: 'mamikos',
			swDest: 'serviceworker.js',
			include: [],
			modifyURLPrefix: {
				'//': '/'
			},
			runtimeCaching: [
				{
					// Ignore url with '/tr/' 'utm_'
					urlPattern: /^((?!\/tr\/)(?!utm_).)*$/,
					handler: 'NetworkFirst'
				}
			],
			skipWaiting: true,
			clientsClaim: true,
			importScripts: [
				'https://cdn.moengage.com/webpush/releases/serviceworker_cdn.min.latest.js'
			]
		})
	);
}

if (isAnalyzing) {
	const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin();

	webpackPlugins.push(new BundleAnalyzerPlugin());
}

mix.webpackConfig({
	plugins: webpackPlugins,
	output: {
		publicPath: isHMR ? 'http://0.0.0.0:8080/' : STATIC_ASSETS_URL, // https://github.com/webpack/webpack-dev-server/issues/1385
		chunkFilename:
			typeof process.env.APP_ENV === 'string' &&
			process.env.APP_ENV.includes('local')
				? 'dist/chunk/[name].js'
				: 'dist/chunk/[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['json', '.js', '.vue', '.css', '.scss'],
		alias: {
			Static: path.resolve(__dirname, 'resources/assets/static'),
			Js: path.resolve(__dirname, 'resources/assets/js'),
			Css: path.resolve(__dirname, 'resources/assets/css'),
			Sass: path.resolve(__dirname, 'resources/assets/sass'),
			Consultant: path.resolve(__dirname, 'resources/assets/js/_consultant'),
			Json: path.resolve(__dirname, 'resources/assets/json')
		},
		fallback: {
			crypto: require.resolve('crypto-browserify'),
			buffer: require.resolve('buffer/'),
			stream: require.resolve('stream-browserify')
		}
	},
	module: {
		rules: [
			{
				// Extend the file-loader's options, of what defined in https://github.com/JeffreyWay/laravel-mix/blob/v6.0.10/src/builder/webpack-rules.js#L14
				test: /(\.(png|jpe?g|gif|webp)$|^((?!font).)*\.svg$)/,
				use: [
					{
						loader: 'file-loader',
						options: {
							esModule: false
						}
					}
				]
			}
		]
	}
});

mix.options({
	terser: {
		parallel: 3
	}
});

// "resourceRoot" means "publicPath" for assets that processed by Mix. See: https://github.com/JeffreyWay/laravel-mix/blob/v6.0.9/src/builder/webpack-rules.js#L36
mix.setResourceRoot(STATIC_ASSETS_URL);

/**
 * Next gen resources.
 * Controlled by /mamikos-cli/commands/build/constants/mix-commands.js
 */
if (process.env.next) {
	const mixResources = require('./mamikos-cli/commands/build/output.js');

	mixResources();
} else {
	/**
	 * We're planning to remove these code.
	 * Don't modify code in this block or we will remove it!
	 * Please use next gen resources instead.
	 */
	const vendor = require('./webpack/webpack.vendor');
	const combine = require('./webpack/webpack.combine');
	const bundle = require('./webpack/webpack.bundle');
	const build = require('./webpack/webpack.build');
	const copy = require('./webpack/webpack.copy');

	vendor.compile.css();
	vendor.compile.js();

	combine.compile();

	bundle.compile.js();

	build.compile();
	copy.compile();
}

if (mix.inProduction()) {
	mix.version(['public/dist/**/*.{css,js}']);
} else {
	// make sure browserSync doesn't collide with webpack-dev-server
	if (!isHMR) {
		mix.browserSync({
			proxy: process.env.APP_URL,
			files: ['public/dist/**/*'],
			open: false
		});
	}
}
