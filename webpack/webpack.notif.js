require('dotenv').config();

const os = require('os');
const axios = require('axios');

const appEnv = process.env.APP_ENV;

if (appEnv === 'production' || appEnv === 'jambu' || appEnv === 'jambu2') {
	sendNotif(process.env.npm_config_command);
}

function sendNotif(arg) {
	let notifMessage;
	let commandType;

	if (arg === 'release') {
		commandType = 'yarn release & clear';
	} else if (arg === 'build') {
		commandType = 'yarn build';
	} else if (arg === 'clear') {
		commandType = 'yarn clear';
	} else if (arg === 'stage-build') {
		commandType = `updating`;
	}

	const hostnameSource = os.hostname();

	if (arg === 'stage-pull') {
		notifMessage = `:warning: *start updating ${appEnv}* on *${hostnameSource}* <!channel>`;
	} else {
		notifMessage = `:tada: *${commandType}* is complete for *${appEnv}* on *${hostnameSource}* <!channel>`;
	}

	axios
		.post(
			'https://hooks.slack.com/services/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',
			{
				text: notifMessage,
				link_names: 1
			}
		)
		.then(response => {
			console.log(response.data);
		})
		.catch(error => {
			if (error.response) {
				console.log(error.response.data);
				console.log(error.response.status);
				console.log(error.response.headers);
			} else if (error.request) {
				console.log(error.request);
			} else {
				console.log('Error', error.message);
			}

			console.log(error.config);
		});
}
