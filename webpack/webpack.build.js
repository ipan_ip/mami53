const mix = require('laravel-mix');
const src = require('./webpack.src');
require('laravel-mix-purgecss');

const assets = 'resources/assets';
const dist = 'public/dist';

const compile = () => {
	// CORE ASSETS
	mix
		.sass(`${assets}/sass/app.scss`, `${dist}/css`)
		.styles([src.css.appmamikos], `${dist}/css/appmamikos.css`)
		.js(`${assets}/js/@vendor/main/script.js`, `${dist}/js/@vendor/main`)
		.js(
			`${assets}/js/@vendor/detail-booking/script.js`,
			`${dist}/js/@vendor/detail-booking`
		);

	// APP / PAGE SPECIFIC ASSETS
	mix
		.js(`${assets}/js/_home/app.js`, `${dist}/js/_home`)
		.js(`${assets}/js/_detail/app.js`, `${dist}/js/_detail`)
		.js(`${assets}/js/_user/app.js`, `${dist}/js/_user`)
		.js(`${assets}/js/_promotion/app.js`, `${dist}/js/_promotion`)
		.js(`${assets}/js/_career/app.js`, `${dist}/js/_career`)
		.js(`${assets}/js/_about-us/app.js`, `${dist}/js/_about-us`)
		.js(`${assets}/js/_landing/house/app.js`, `${dist}/js/_landing/house`)
		.js(
			`${assets}/js/_landing/direct-booking/app.js`,
			`${dist}/js/_landing/direct-booking`
		)
		.js(
			`${assets}/js/_mamirooms/landing/app.js`,
			`${dist}/js/_mamirooms/landing`
		)
		.js(
			`${assets}/js/_ownerpage/mamipay/app.js`,
			`${dist}/js/_ownerpage/mamipay`
		)
		.js(
			`${assets}/js/_ownerpage/billing-management/app.js`,
			`${dist}/js/_ownerpage/billing-management`
		)
		.js(`${assets}/js/_consultant/app.js`, `${dist}/js/_consultant`)
		.js(`${assets}/js/_area/app.js`, `${dist}/js/_area`)
		.js(`${assets}/js/_flash-sale/app.js`, `${dist}/js/_flash-sale`)
		.js(`${assets}/js/_search/app.js`, `${dist}/js/_search`)
		.js(`${assets}/js/_sanjunipero/app.js`, `${dist}/js/_sanjunipero`)
		.js(`${assets}/js/_landing-billing/app.js`, `${dist}/js/_landing-billing`)
		.js(`${assets}/js/_dbet-tenant/app.js`, `${dist}/js/_dbet-tenant`)
		.js(
			`${assets}/js/_landing-singgahsini/app.js`,
			`${dist}/js/_landing-singgahsini`
		)
		.js(
			`${assets}/js/_register-singgahsini/app.js`,
			`${dist}/js/_register-singgahsini`
		)

		.js(`${assets}/js/_admin/abtest/app.js`, `${dist}/js/_admin/abtest`)

		/// /////////////////////////////////////////////////////////////////////////

		.js(`${assets}/js/@global/login-user.js`, `${dist}/js/`)
		.js(`${assets}/js/@global/login-user-jquery.js`, `${dist}/js/`)
		.js(`${assets}/js/@single/navbar-custom.js`, `${dist}/js/`)
		.js(`${assets}/js/@single/navbar-owner.js`, `${dist}/js/`)

		.js(`${assets}/js/404-page/404-page.js`, `${dist}/js/`)

		.js(`${assets}/js/input/input-properti.js`, `${dist}/js/`)
		.js(`${assets}/js/input/input-kost.js`, `${dist}/js/`)
		.js(`${assets}/js/input/input-edit.js`, `${dist}/js/`)
		.js(`${assets}/js/input/input-agent.js`, `${dist}/js/`)

		.js(`${assets}/js/input/input-properti-apartment.js`, `${dist}/js/`)
		.js(`${assets}/js/input/input-edit-apartment.js`, `${dist}/js/`)

		.js(`${assets}/js/user/user-history.js`, `${dist}/js/`)
		.js(`${assets}/js/user/user-scoreboard.js`, `${dist}/js/`)
		.js(`${assets}/js/user/user-promo.js`, `${dist}/js/`)

		.js(`${assets}/js/forum/forum-user.js`, `${dist}/js/`)

		.js(`${assets}/js/agent/agent-check.js`, `${dist}/js/`)

		.js(`${assets}/js/landing/kost/landing-kost.js`, `${dist}/js/`)

		.js(`${assets}/js/landing/landing-project.js`, `${dist}/js/`)
		.js(`${assets}/js/landing/landing-area.js`, `${dist}/js/`)

		.js(`${assets}/js/input/vacancy/input-vacancy.js`, `${dist}/js/`)
		.js(`${assets}/js/vacancy/landing-vacancy.js`, `${dist}/js/`)
		.js(`${assets}/js/vacancy/detail-vacancy.js`, `${dist}/js/`)
		.js(`${assets}/js/vacancy/company-profile.js`, `${dist}/js/`)
		.js(`${assets}/js/vacancy/company-vacancy.js`, `${dist}/js/`)
		.js(`${assets}/js/vacancy/company-landing.js`, `${dist}/js/`)

		.js(`${assets}/js/owner/owner-page.js`, `${dist}/js/`)

		.js(`${assets}/js/newest/newest-list.js`, `${dist}/js/`)
		.js(
			`${assets}/js/promo/all-apartment/promo-list-all-apartment.js`,
			`${dist}/js/`
		)
		.js(`${assets}/js/promo/all-job/promo-list-all-job.js`, `${dist}/js/`)
		.js(`${assets}/js/promo/graduation/graduation-photo.js`, `${dist}/js/`)
		.js(`${assets}/js/promo/graduation/graduation-upload.js`, `${dist}/js/`)

		.js(`${assets}/js/_promo/app.js`, `${dist}/js/_promo/`)
		.js(`${assets}/js/_promo/all-kost/app.js`, `${dist}/js/_promo/all-kost`)
		.js(`${assets}/js/_login/app.js`, `${dist}/js/_login`)

		.js(`${assets}/js/landing-modern/landing-agent.js`, `${dist}/js/`)
		.js(`${assets}/js/landing-modern/landing-standard.js`, `${dist}/js/`)

		.js(`${assets}/js/listing/listing-landing.js`, `${dist}/js/`)

		.js(
			`${assets}/js/_landing-content/app.js`,
			`${dist}/js/_landing-content/app.js`
		)

		.js(
			`${assets}/js/_download-exam/app.js`,
			`${dist}/js/_download-exam/app.js`
		)
		.vue({ version: 2 })
		.options({
			processCssUrls: false
		})

		.purgeCss();
};

module.exports = { compile };
