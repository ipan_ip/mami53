const mix = require('laravel-mix');
const src = require('./webpack.src');

const dist = 'public/dist';

const compile = {
	js() {
		mix.babel(
			[
				src.js.bookingGlobalModule,
				src.js.bookingSelectModule,
				src.js.bookingDetailModule,
				src.js.bookingReviewModule,
				src.js.bookingPaymentModule,
				src.js.bookingReceiptModule,
				src.js.bookingHistoryModule,
				src.js.bookingRootModule
			],
			`${dist}/js/pages/booking/bookingCtrl.js`
		);
	}
};

module.exports = { compile };
