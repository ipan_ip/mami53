## What issue/feature/bugfix/improvement did you solve?

> Answer here..

## How you solve the issue?

> Answer here..

## Approach's Shortcoming (Optional)

> Answer here..

## Things need to be improved in the future (Optional)

> Answer here..

## UI Screenshots (Optional)

> Answer here..

---

[Merge Request Guideline & Principle](https://mamikos.atlassian.net/l/c/3gYG1FS0)
