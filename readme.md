# Mamikos Web

Based on **Laravel 5.7** and **Vue.js v2**

Check out our Project Wiki [here](https://devel.wejoin.us:3000/ali/mami53/-/wikis/home)

### Installation

[Getting Started](https://devel.wejoin.us:3000/ali/mami53/-/wikis/Getting-Started)

### Contributing

* [Branch Management](https://devel.wejoin.us:3000/ali/mami53/-/wikis/Branch-Management)
* [MR Guideline](https://devel.wejoin.us:3000/ali/mami53/-/wikis/MR-Guideline)

### API Documentation

* [Mamikos API V2](https://mamikosv2.docs.apiary.io/)
* [Mamikos API Garuda](https://mamikosgaruda.docs.apiary.io/)
* [Mamikos Consultant API](https://mamikosconsultant.docs.apiary.io/)
* [Mamikos OAuth API](https://mamikosoauth.docs.apiary.io/)

### Reference

* [Laravel Documentation](https://laravel.com/docs/5.7)
* [Vue.js Guide](https://vuejs.org/v2/guide/)
