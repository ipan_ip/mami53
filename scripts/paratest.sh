docker-compose stop database mongo
docker-compose rm -f
docker-compose up -d database mongo

echo "waiting for database availability, it may take a while. please wait.."
until docker-compose exec -T webapp bash -c "php artisan database:ping --env=testing" > /dev/null
do
    sleep 1
done

docker-compose exec -T webapp bash -c "php artisan migrate:multiple --env=testing && \
    ./vendor/mamitech/paratest/bin/paratest --bootstrap vendor/autoload.php --stop-on-failure --testsuite=unit-tests"
docker-compose stop database mongo