<?php

Route::group(
    ['namespace' => 'ConsultantTools'],
    function () {
        Route::get('/', 'AuthController@login')->name('login');
        Route::post('login', 'AuthController@postLogin');

        Route::get('dashboard', 'ViewController@dashboard')->name('dashboard');
        Route::get('property', 'ViewController@property')->name('property');
        Route::get('active-tenant', 'ViewController@activeTenant')->name('active-tenant');
        Route::get('active-tenant/{any}', 'ViewController@activeTenant')->name('active-tenant.detail');
        Route::get('property/{id}', 'ViewController@property')->name('property.detail');
        Route::get('property/{id}/edit', 'ViewController@propertyEdit')->name('property.edit');
        Route::get('property/{id}/edit/room-and-price', 'ViewController@property')->name(
            'property.edit.room-and-price'
        );

        Route::get('room-allotment/{song_id}', 'ViewController@mainView')->name('room-allotment');

        Route::group(
            ['prefix' => 'potential-tenant'],
            function () {
                Route::get('/', 'ViewController@potentialTenant')->name('potential-tenant');
                Route::get('/create', 'ViewController@activeTenant')->name('potential-tenant.create');
                Route::get('/{id}', 'ViewController@potentialTenant')->name('potential-tenant.detail');
                Route::get('/{id}/edit', 'ViewController@potentialTenant')->name('potential-tenant.edit');
                Route::get('/{id}/edit-note', 'ViewController@mainView')->name('potential-tenant.edit.note');
            }
        );

        Route::get('invoice/{id}/{booking}/{contract_status}', 'ViewController@invoice')->name('invoice');

        Route::group(['prefix' => 'contract'], function() {
            Route::get('/', 'ViewController@contract')->name('contract');
            Route::get('/create', 'ViewController@mainView')->name('contract.create');
            Route::get('/create/room-number/{id}', 'ViewController@mainView');
            Route::get('/invoice/{invoiceId}/edit-costs', 'ViewController@mainView')->name('contract.invoice.edit-costs');
            Route::get('/invoice/{invoiceId}/edit-amount', 'ViewController@mainView')->name('contract.invoice.edit-amount');
        });

        Route::group(['prefix' => 'activity'], function() {
            Route::get('/', 'ViewController@mainView')->name('activity');
            Route::get('/property/', 'ViewController@mainView');
            Route::get('/property/detail', 'ViewController@mainView');
            Route::get('/detail', 'ViewController@mainView');
            Route::get('/funnels', 'ViewController@mainView');
            Route::get('/search', 'ViewController@mainView');
            Route::get('/staging/{funnel_id}/{funnel_name}/{funnel_type}', 'ViewController@mainView');
            Route::get('/staging/log/{task_id}', 'ViewController@mainView');
            Route::get('/staging/form/{funnel_id}/{progress_id}/{stage_id}/{task_id}', 'ViewController@mainView');
            Route::get('/staging/form/{edit}/{funnel_id}/{progress_id}/{stage_id}/{task_id}', 'ViewController@mainView');
            Route::get('/staging/search/{funnel_id}/{stage}/{funnel_name}/{stage_name}/{data_type}', 'ViewController@mainView');
        });

        Route::group(['prefix' => 'potential-property'], function() {
            Route::get('/', 'ViewController@mainView');
            Route::get('/{ownerId}/add', 'ViewController@mainView');
            Route::get('/{ownerId}/edit/{id}', 'ViewController@mainView');
            Route::get('/{ownerId}/{id}', 'ViewController@mainView');
        });

        Route::group(['prefix' => 'sales-motion'], function() {
            Route::get('/', 'ViewController@mainView');
            Route::get('/search', 'ViewController@mainView');
            Route::get('/{id}', 'ViewController@mainView');
            Route::get('/{id}/report/add', 'ViewController@mainView');
            Route::get('/{id}/report/search-data', 'ViewController@mainView');
            Route::get('/{id}/report/{report_id}', 'ViewController@mainView');
        });

        Route::group(['prefix' => 'potential-owner'], function() {
            Route::get('/', 'ViewController@mainView');
            Route::get('/add', 'ViewController@mainView');
            Route::get('/edit/{id}', 'ViewController@mainView');
            Route::get('/{id}', 'ViewController@mainView');
        });

        Route::group(['prefix' => 'booking-management'], function() {
            Route::get('/', 'ViewController@mainView');
            Route::get('/{id}', 'ViewController@mainView');
            Route::get('/{id}/accept', 'ViewController@mainView');
            Route::get('/{id}/reject', 'ViewController@mainView');
        });



        Route::group(
            ['middleware' => ['auth:passport', 'permission:access-consultant']],
            function () {
                //API Routes
                Route::group(
                    ['prefix' => 'api'],
                    function () {
                        Route::group(['prefix'=>'dashboard'],function (){
                            Route::get('/', 'DashboardController@data');
                            Route::get('/activity-list','DashboardController@activityProgressList');
                            Route::get('/active-funnel','DashboardController@activeFunnelList');
                            Route::get('/active-data-type','DashboardController@activeDataType');
                            Route::get('/search/activity','DashboardController@searchActivity');
                        });

                        Route::group(
                            ['prefix' => 'property'],
                            function () {
                                Route::get('/', 'PropertyManagementController@list');
                                Route::get('/location-suggestion', 'PropertyManagementController@locationAutoComplete');
                                Route::get('facility-list', 'PropertyManagementController@facility');
                                Route::get('/check-name', 'PropertyManagementController@checkNameAvaibility');

                                Route::post('/update/price/{id}', 'PropertyManagementController@updateBookingData');
                                Route::post('/update/{id}', 'PropertyManagementController@updateProcess');

                                Route::get('/update/{id}', 'PropertyManagementController@updateData');
                                Route::get('/{id}', 'PropertyManagementController@detail');
                            }
                        );

                        Route::group(
                            ['prefix' => 'tenant'],
                            function () {
                                Route::get('search', 'ContractManagementController@searchTenant');

                                Route::post('contract', 'ContractManagementController@createContract');

                                Route::post('upload/selfie', 'ContractManagementController@uploadSelfie');
                                Route::post('upload/identifier', 'ContractManagementController@uploadIdentifier');

                                Route::group(
                                    ['prefix' => 'active'],
                                    function () {
                                        Route::get('/', 'ActiveTenantController@index');
                                        Route::get('/{mamipay_tenant_id}', 'ActiveTenantController@show');
                                    }
                                );

                                Route::group(
                                    ['prefix' => 'potential'],
                                    function () {
                                        Route::get('/', 'PotentialTenantController@list');
                                        Route::get(
                                            '/search-tenant/{phoneNumber}',
                                            'PotentialTenantController@searchTenantByPhoneNumber'
                                        );
                                        Route::get(
                                            '/search-property/{name}',
                                            'PotentialTenantController@searchPropertyByName'
                                        );
                                        Route::get('/{id}', 'PotentialTenantController@detail');

                                        Route::post('/addOrUpdate', 'PotentialTenantController@addOrUpdate');
                                        Route::post('/save-note/{id}','PotentialTenantController@saveNote');
                                    }
                                );
                            }
                        );
                        Route::group(
                            ['prefix' => 'contract'],
                            function () {
                                Route::get('/', 'ContractManagementController@list');
                                Route::get(
                                    '/search-property/{name}',
                                    'ContractManagementController@searchProperty'
                                );
                                Route::get('/tenant-info/{contractId}', 'ContractManagementController@tenantInfo');

                                Route::group(['prefix' => 'invoice'], function(){
                                    Route::get('/year-list/{contract}', 'ContractManagementController@getInvoiceYear');
                                    Route::get('/{contract}/{year}', 'ContractManagementController@invoice');

                                    Route::post('/note/{invoiceId}', 'ContractManagementController@invoiceNote');
                                    Route::post('/reminder/{invoiceId}', 'ContractManagementController@reminder');

                                    Route::group(
                                        ['prefix' => 'update'],
                                        function () {
                                            Route::post(
                                                '{id}/manual',
                                                'ContractManagementController@setInvoiceAsManualPayment'
                                            );
                                            Route::post(
                                                'additional-cost',
                                                'ContractManagementController@modifyAdditionalCost'
                                            );
                                            Route::post(
                                                '{id}/amount',
                                                'ContractManagementController@updateAmount'
                                            );
                                        }
                                    );
                                });
                                Route::post(
                                    '/terminate/{id}',
                                    'ContractManagementController@terminate'
                                );
                                Route::post(
                                    '/cancel/{id}',
                                    'ContractManagementController@cancel'
                                );
                            }
                        );

                        Route::group(
                            ['prefix' => 'activity-management'],
                            function () {
                                Route::group(['prefix'=>'funnel'],function (){
                                    Route::get('/{funnelId}/tasks', 'ActivityManagementController@showAllTask');
                                    Route::get('/{funnelId}/data/{dataId}','ActivityManagementController@detailData');
                                    Route::get('/{funnelId}/{stageId}', 'ActivityManagementController@searchTask');
                                });


                                Route::post('tasks/activate', 'ActivityManagementController@tasksActivation');

                                Route::group(['prefix' => 'task'], function () {
                                    Route::get('/{funnelId}/{taskId}/stages', 'ActivityManagementController@getTaskStages');
                                    Route::get('/{funnelId}/{taskId}/status', 'ActivityManagementController@taskStatus');
                                    Route::post('/backlog', 'ActivityManagementController@moveToBacklog');
                                    Route::post('/todo', 'ActivityManagementController@moveToToDo');
                                    Route::post('/{taskId}', 'ActivityManagementController@taskCreateOrUpdate');
                                });

                                Route::get('stage/{stageId}/form', 'ActivityManagementController@showForm');

                                Route::get('progress/detail/{taskId}', 'ActivityManagementController@progressDetailValue');
                                Route::get('/progress/history/{taskId}', 'ActivityManagementController@progressHistory');
                            }
                        );

                        Route::prefix('potential-property')->group(function () {
                            Route::get('/detail/{id}', 'PotentialPropertyController@detail');
                            Route::get('/{ownerId?}', 'PotentialPropertyController@index');

                            Route::post('/', 'PotentialPropertyController@create');
                            Route::put('{id}', 'PotentialPropertyController@update');
                            Route::delete('/{id}', 'PotentialPropertyController@delete');
                        });

                        Route::prefix('media')->group(function () {
                            Route::delete('{mediaId}', 'MediaController@delete');
                            Route::post('upload', 'MediaController@upload');
                        });

                        Route::prefix('document')->group(function () {
                            Route::get('{document}', 'ConsultantDocumentController@download');
                            Route::post('sales-motion-progress', 'ConsultantDocumentController@uploadSalesMotionProgress');
                            Route::delete('{documentId}', 'ConsultantDocumentController@delete')->where('id', '[0-9]+');
                        });

                        Route::prefix('/room-unit')->group(function () {
                            Route::get('/{songId}/available', 'RoomUnitController@getRoomAvailable');
                            Route::get('/{songId}/total', 'RoomUnitController@getTotalRoom');
                            Route::get('/{songId}', 'RoomUnitController@getRoomList');
                            Route::post('/{songId}/update', 'RoomUnitController@crudBulk');
                        });

                        Route::prefix('sales-motion')->group(function () {
                            Route::prefix('progress')->group(function () {
                                Route::get('/data-associated','SalesMotionProgressController@getDataAssociated');
                                Route::get('{consultantId}', 'SalesMotionProgressController@index')->where('id', '[0-9]+');
                                Route::get('/detail/{progressId}', 'SalesMotionProgressController@detail');
                                Route::post('/store', 'SalesMotionProgressController@store');
                            });
                            Route::get('/list','SalesMotionController@list');

                            Route::get('/{id}', 'SalesMotionController@detail');
                        });

                        Route::group(['prefix' => 'potential-owner'], function () {
                            Route::get('list', 'PotentialOwnerController@list');
                            Route::get('{id}', 'PotentialOwnerController@detail');
                            Route::post('store', 'PotentialOwnerController@store');
                            Route::put('update/{ownerId}', 'PotentialOwnerController@update');
                            Route::delete('{id}', 'PotentialOwnerController@delete');
                        });

                        Route::group(['prefix' => 'booking-management'], function () {
                            Route::get('/', 'BookingManagementController@index');
                            Route::get('/list-status-level', 'BookingManagementController@getStatusAndLevel');
                            Route::get('/detail/contract/{bookingId}', 'BookingManagementController@contractDetail');

                            Route::post('/reject/{bookingId}', 'BookingManagementController@rejectBooking');
                            Route::post('/accept/{bookingId}', 'BookingManagementController@acceptBooking');
                            Route::get('detail/property/{bookingId}', 'BookingManagementController@bookingAndPropertyDetail');
                            Route::get('/detail/tenant/{bookingId}', 'BookingManagementController@tenantDetail');
                        });

                    }
                );

                Route::get('logout', 'AuthController@logout')->name('logout');
            }
        );
    }
);
