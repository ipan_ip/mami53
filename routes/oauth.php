<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:passport', 'as' => 'user-act.'], function () {
    // for cms service
    Route::group(['prefix' => 'cms', 'as' => 'cms.'], function() {
        Route::group(['prefix' => 'user', 'as' => 'user.'], function(){
            Route::get(
                '/',
                'Oauth\Cms\UserController@whoAmI'
            )->name('whoAmI');
            Route::get(
                '{userId}',
                'Oauth\Cms\UserController@getUserById'
            )->where([
                'userId' => '[0-9]+'
            ])->name('byId');
        });
        Route::get(
            'rooms/{songId}',
            'Oauth\Cms\RoomController@getRoom'
        )->where([
            'songId' => '[0-9]+'
        ])->name('rooms.id');
        Route::get(
            'contract/{contractId}',
            'Oauth\Cms\ContractController@getContract'
        )->where([
            'contractId' => '[0-9]+'
        ])->name('contractId.id');
        Route::get(
            'permission',
            'Oauth\Cms\PermissionController'
        )->name('permission');
        Route::get(
            'validation',
            'Oauth\Cms\ValidationController'
        )->name('validation.id');
    });

    Route::group(['prefix' => 'goldplus'], function() {
        Route::get('packages', 'Api\GoldPlus\PackageController@index');
        Route::get('submissions/onboarding', 'Api\GoldPlus\SubmissionController@getOnboardingContent');
        Route::get('submissions/tnc', 'Api\GoldPlus\SubmissionController@getTncContent');
        Route::post('submissions', 'Api\GoldPlus\SubmissionController@store');
        Route::get('unsubscribe/statistic', 'Api\GoldPlus\UnsubscribeController@getStatistic');

         //GP Business onboarding routes
        Route::group(['prefix' => 'onboarding'], function() {
            Route::get('kost-business', 'Api\GoldplusController@getGoldplusOnboardingWords');
            
            //TODO : Angga Bayu S <here> *adding more endpoint related to GP Onboard
        });
    });

    Route::group(['prefix' => 'owner', 'as' => 'owner.'], function () {
        Route::group(['prefix' => 'data', 'as' => 'data.'], function () {
            Route::get('profile', 'Api\OwnerDataController@getProfile')->name('profile');
            Route::get('notification', 'Api\OwnerDataController@getNotificationOwner')->name('notification');

            Route::group(['prefix' => 'kos', 'as' => 'kos.'], function () {
                Route::get('/', 'Api\OwnerDataController@getOwnedKos')->name('iklan-saya');
                
                Route::get('facilities', 'Api\Owner\Data\Kost\FacilitiesController@index')->name('list-facilities');
                Route::get('kos-rules', 'Api\Owner\Data\Kost\KostRulesController@index')->name('list-rules');
                Route::get('minimum-payment', 'Api\Owner\Data\Kost\MinimumPaymentController@index')->name('list-minimum-payment');
                Route::post('duplicate', 'Api\Owner\Data\Kost\DuplicateController@duplicate')->name('duplicate');

                Route::group(['prefix' => 'room-unit', 'as' => 'allotment.'], function() {
                    Route::get('/', 'Api\Room\RoomUnitController@index')->name('index');
                    Route::group(['prefix' => 'single', 'as' => 'single.'], function() {
                        Route::post('/', 'Api\RoomAllotment\RoomAllotmentController@addRoomUnit')->name('add');
                        Route::put('{id}', 'Api\RoomAllotment\RoomAllotmentController@editRoomUnit')->name('update');
                        Route::delete('{id}', 'Api\RoomAllotment\RoomAllotmentController@deleteRoomUnit')->name('delete');
                    });
                    Route::post(
                        'bulk',
                        'Api\RoomAllotment\RoomAllotmentController@updateBulkRoomUnit'
                    )->name('update-bulk');
                });

                Route::group(['prefix' => 'input', 'as' => 'input.'], function () {
                    Route::group(['prefix' => 'stage', 'as' => 'draft.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\DraftController@showDefaultStages'
                        )->name('get.stage');
                        Route::get(
                            '{songId}',
                            'Api\Owner\Input\Kost\DraftController@showStagesById'
                        )->where(['songId' => '[0-9]+'])->name('get.stage.by-id');
                    });
                    Route::group(['prefix' => 'address', 'as' => 'address.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\AddressController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\AddressController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\AddressController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'information', 'as' => 'information.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\InformationController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\InformationController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\InformationController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'kost-photo', 'as' => 'kost-photo.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\KostPhotoController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\KostPhotoController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\KostPhotoController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'room-photo', 'as' => 'room-photo.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\RoomPhotoController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\RoomPhotoController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\RoomPhotoController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'facilities', 'as' => 'facilities.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\FacilitiesController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\FacilitiesController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\FacilitiesController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
                        Route::post(
                            '/',
                            'Api\Owner\Input\Kost\RoomAllotmentController@store'
                        )->name('store');
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\RoomAllotmentController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\RoomAllotmentController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::group(['prefix' => 'price', 'as' => 'price.'], function () {
                        Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                            Route::get(
                                '/',
                                'Api\Owner\Input\Kost\PriceController@get'
                            )->where(['songId' => '[0-9]+'])->name('retrieve');
                            Route::put(
                                '/',
                                'Api\Owner\Input\Kost\PriceController@edit'
                            )->where(['songId' => '[0-9]+'])->name('put');
                        });
                    });

                    Route::put(
                        'submit/{songId}',
                        'Api\Owner\Input\Kost\SubmitController@submit'
                    )->where(['songId' => '[0-9]+'])->name('submit.put');

                    Route::post(
                        'ready-to-verif/{songId}',
                        'Api\Owner\Input\Kost\SubmitController@markReady'
                    )->where(['songId' => '[0-9]+'])->name('submit.mark-as-ready');
                });

                Route::group(['prefix' => 'address', 'as' => 'address.'], function() {
                    Route::get('province', 'Api\Owner\Data\Address\ProvinceController@index')->name('list-province');
                    Route::get('city' , 'Api\Owner\Data\Address\CityController@index')->name('list-city');
                    Route::get('subdistrict', 'Api\Owner\Data\Address\SubdistrictController@index')->name('list-subdistrict');
                });
            });
            
            Route::group(['prefix' => 'list', 'as' => 'list.'], function () {
                Route::get('update', 'Api\OwnerDataController@getListKosForUpdate')->name('update');
                Route::group(['prefix' => 'kos', 'as' => 'kos.'], function () {
                    Route::get(
                        '/',
                        'Api\OwnerDataController@getListKos'
                    )->name('index');
                    Route::get(
                        '{id}',
                        'Api\OwnerDataController@detailKosOwner'
                    )->where([
                        'id' => '[0-9]+'
                    ])->name('id');
                });

                Route::group(['prefix' => 'apartment', 'as' => 'apartment.'], function () {
                    Route::get(
                        '/',
                        'Api\OwnerDataController@getListApartment'
                    )->name('index');
                    Route::get(
                        '{id}',
                        'Api\OwnerDataController@detailKosOwner'
                    )->where([
                        'id' => '[0-9]+'
                    ])->name('id');
                });
                Route::get(
                    'active',
                    'Api\OwnerDataController@getListAllCanPromote'
                )->name('active');
            });

            Route::group(['prefix' => 'survey', 'as' => 'survey.'], function () {
                Route::post('updateroom', 'Api\OwnerDataController@surveyUpdateRoom')->name('updateroom');
                Route::post('premium', 'Api\OwnerDataController@surveyPremium')->name('premium');
            });

            Route::get('booking', 'Api\Booking\Owner\BookingController@getListData')->name('booking');

            Route::group(['prefix' => 'kost-level', 'as' => 'kost-level.'], function () {
                Route::get('flag-count', 'Api\KostLevelController@flagCount')->name('flag-count');
                Route::get('list', 'Api\KostLevelController@kostList')->name('list');
            });

            Route::group(['prefix' => 'property', 'as' => 'property.'], function () {
                Route::get(
                    '/',
                    'Api\Owner\Data\Property\ListController@index'
                )->name('list');
                Route::get(
                    '{id}/type',
                    'Api\Owner\Data\Property\TypeController@index'
                )->where(['id' => '[0-9]+'])->name('by-id.types');
                Route::post(
                    'validate-name',
                    'Api\Owner\Data\Property\NameValidationController@property'
                )->name('validation.name');
                Route::post(
                    '{id}/validate-type-name',
                    'Api\Owner\Data\Property\NameValidationController@kostType'
                )->where(['id' => '[0-9]+'])->name('validation.type.name');
            });

            Route::post('update-room', 'Api\OwnerDataController@updateAllProperty')->name('update-room');
            Route::get('income', 'Api\OwnerDataController@getDashboardTotalIncome')->name('income');
        });

        Route::group(['prefix' => 'edit', 'as' => 'edit.'], function () {
            Route::post('email', 'Api\OwnerController@editEmail')->name('email');
            Route::post('name', 'Api\UserController@editName')->name('name');
            Route::group(['prefix' => 'phone', 'as' => 'phone.'], function () {
                Route::post('/', 'Api\Auth\OwnerController@editPhone')->name('index');
                Route::post('request', 'Api\Auth\OwnerController@editPhoneRequest')->name('request');
            });
        });

        Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
            Route::post(
                'save',
                'Api\OwnerController@postSaveOwnerDesignerViaWeb'
            )->name('save');
            Route::post(
                'draft',
                'Api\OwnerController@postSaveOwnerDesignerViaWeb'
            )->name('draft');

            // room price component
            Route::get('price-component/{songId}', 'Api\Owner\Room\RoomPriceComponentController@index');
            Route::post('price-component/{songId}', 'Api\Owner\Room\RoomPriceComponentController@store');
            Route::put('price-component/{priceComponentId}', 'Api\Owner\Room\RoomPriceComponentController@update');
            Route::delete('price-component/{priceComponentId}', 'Api\Owner\Room\RoomPriceComponentController@destroy');
            Route::post('price-component/toggle/{songId}/{type}', 'Api\Owner\Room\RoomPriceComponentController@switch');

            Route::get('review', 'Api\Owner\Room\OwnerRoomReviewController@index');
        });

        Route::group(['prefix' => 'update_room', 'as' => 'update_room.'], function () {
            Route::post(
                '{id}',
                'Api\OwnerController@updateRoomByOwner'
            )->name('id');
            Route::post(
                'draft/update/{id}',
                'Api\OwnerController@updateRoomByOwner'
            )->name('draft.update.id');
        });

        Route::post('updateroom/{id}', 'Api\PropertyInputController@updateKost')->name('legacy.updateroom');

        Route::group(['prefix' => 'claim', 'as' => 'claim.'], function () {
            Route::post('/', 'Api\OwnerController@postClaimRoomByOwner')->name('index');
            Route::get(
                '{id}',
                'Api\OwnerController@getClaimOwner'
            )->where([
                'id' => '[0-9]+'
            ])->name('id');
        });

        Route::group(['prefix' => 'promotion', 'as' => 'promotion.'], function () {
            Route::post('/', 'Api\PromotionController@postPromotion')->name('index');
            Route::get(
                '{id}',
                'Api\PromotionController@getPromotionOwner'
            )->where([
                'id' => '[0-9]+'
            ])->name('id');
        });

        Route::group(['prefix' => 'premium', 'as' => 'premium.'], function () {
            Route::post('stop-reason', 'Api\PremiumController@stopReason')-> name('stop-reason');
            Route::post('cancel', 'Api\PremiumController@cancelPurchase')-> name('cancel');
            Route::post('trial', 'Api\PremiumController@autoApproveTrial')-> name('trial');
            Route::get('confirmation', 'Api\PremiumController@getConfirmation')-> name('confirmation');
            Route::post('confirmation', 'Web\PremiumController@postPurchaseConfirmation')-> name('confirmation');
            Route::post('confirmation/notification', 'Web\PremiumController@postConfirmationNotif')-> name('confirmation.notification');
            Route::post('confirm', 'Api\PremiumController@postConfirm')-> name('confirm');
            Route::get('invoice', 'Api\PremiumController@getInvoice')->name('invoice');
            Route::group(['prefix' => 'allocation'], function() {
                Route::get('daily/setting', 'Api\Premium\PremiumAllocationController@settingStatus');
                Route::get('{id}/deactivate', 'Api\Premium\PremiumAllocationController@deactivateRoomAllocation');
                Route::post('{id}', 'Api\Premium\PremiumAllocationController@allocation');
            });
            Route::group(['prefix' => 'ads'], function() {
                Route::get('favorite', 'Api\PremiumController@mostFavorite');
                Route::get('statistic/summary', 'Api\PremiumController@getRoomsStatsisticSummary');
                Route::get('statistic', 'Api\PremiumController@getRoomsStatistic');

                Route::get('statistic/{id}/summary', 'Api\PremiumController@getRoomStatsisticSummary');
                Route::get('statistic/{id}', 'Api\PremiumController@getRoomStatistic');

            });
            Route::group(['prefix' => 'ab-test'], function() {
                Route::get('check', 'Api\Premium\AbTestPremiumController@checkOwner');
            });
        });

        Route::group(['prefix' => 'password', 'as' => 'password.'], function () {
            Route::post('set', 'Api\OwnerController@setPassword')->name('set');
            Route::post('update', 'Api\OwnerController@updatePassword')->name('update');
        });

        Route::group(['prefix' => 'verification', 'as' => 'verification.'], function () {
            Route::post('check', 'Api\OwnerController@postVerificationCheck')->name('check');
            Route::post('code', 'Api\OwnerController@postVerification')->name('code');
            Route::post('request', 'Api\OwnerController@postRequestVerification')->name('request');
            Route::post('store', 'Api\OwnerController@postStoreVerification')->name('store');
            Route::post('email', 'Api\OwnerDataController@postVerificationEmail')->name('email');
        });

        Route::get('list', 'Api\OwnerController@getListOwner')->name('list');
        Route::get('edit_room/{id}', 'Api\OwnerController@editRoomOwner')->name('edit_room.id');
        Route::post('update_room_price', 'Api\OwnerController@updateRoomPriceByOwner')->name('update_room_price');
        Route::delete('delete_room/{id}', 'Api\OwnerController@deleteRoomByOwner')->name('delete_room.id');
        Route::get('suggested', 'Api\OwnerController@getSuggestedOwnerRoom')->name('suggested');
        Route::post('search', 'Api\OwnerController@postSearchOwnerRoom')->name('search');
        Route::get('survey/{id}', 'Api\SurveyController@editSurveyOwner')->name('survey.id');
        Route::post('reply', 'Api\OwnerController@autoReply')->name('reply');
        Route::get('information', 'Api\OwnerInformationController@index')->name('information');

        Route::group(['prefix' => 'bbk', 'as' => 'bbk.'], function () {
            Route::post('register', 'Api\Room\BookingOwnerRequestController@registerBulkBbk')->name('register');
            Route::get('status', 'Api\Room\BookingOwnerRequestController@getBbkStatus')->name('status');
        });

        Route::group(['prefix' => 'tenant', 'as' => 'tenant.'], function () {
            Route::get('dashboard', 'Api\Owner\Tenant\OwnerTenantController@getDashboard')->name('dashboard');
            Route::resource('link', 'Api\Owner\Tenant\OwnerTenantController', ['only' => ['store', 'show', 'index']]);
        });
        Route::post('contract-submission/confirm/{id}', 'Api\Owner\Tenant\ContractOwnerSubmissionController@confirm');
        Route::post('contract-submission/reject/{id}', 'Api\Owner\Tenant\ContractOwnerSubmissionController@reject');
        Route::resource('contract-submission', 'Api\Owner\Tenant\ContractOwnerSubmissionController', ['only' => ['show', 'index']]);

        Route::get('booking/room/requests', 'Api\OwnerController@getRoomHasBookingRequest');

        Route::group(['prefix' => 'booking', 'as' => 'booking.'], function () {
            // owner dashboard
            Route::get('dashboard', 'Api\Booking\Owner\OwnerBookingDashboardController@index');
        });
    });

    Route::group(['prefix' => 'stories', 'as' => 'stories.'], function () {
        Route::get('{id}/report', 'Api\OwnerController@getRoomReportOwner');
        Route::get('{id}/suggestion', 'Api\RoomController@getSuggestions')->name('id.suggestion');
        Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
            Route::get('list', 'Api\Chat\ChatController@list')->name('list');
            Route::post('tnc-tracker', 'Api\Chat\ChatTncTrackerController@store')->name('tnc-tracker');
        });
        Route::get('meta/{id}', 'Api\RoomMetaController@getRoomMeta')->name('meta.id');

        Route::post('input', 'Api\PropertyInputController@inputKost')->name('input');
    });

    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        // Point User
        Route::group(['prefix' => 'point', 'as' => 'point.'], function () {
            Route::get('total', 'Api\PointUserController@total');
            Route::get('expiry', 'Api\PointUserController@expiry');
            Route::get('tnc', 'Api\PointUserController@tnc');
            Route::get('activity', 'Api\PointUserController@activity');
            Route::get('history', 'Api\PointUserController@history');
            Route::get('scheme', 'Api\PointUserController@scheme');
        });

        // Polling
        Route::group(['prefix' => 'polling'], function () {
            Route::get('detail/{key}', 'Api\UserPollingController@detail');
            Route::post('submit/{key}', 'Api\UserPollingController@submit');
        });
        
        // Redeem
        Route::get('/redeem/list', 'Api\RewardController@userRedeemList');
        Route::get('/redeem/detail/{id}', 'Api\RewardController@userRedeemDetail')->where('id', '[0-9]+');
    });

    // Reward
    Route::group(['prefix' => 'reward', 'as' => 'reward.'], function () {
        Route::post('register', 'Api\PropertyInputController@registerRewardNumber')->name('register');
        Route::get('list', 'Api\RewardController@index')->name('list');
        Route::get('detail/{id}', 'Api\RewardController@show')->where('id', '[0-9]+')->name('detaile');
        Route::post('redeem/{id}', 'Api\RewardController@redeem')->where('id', '[0-9]+')->name('redeem');
    });

    Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
        Route::group(['prefix' => 'goldplus', 'as' => 'goldplus.'], function () {
            Route::get('/', 'Api\GoldplusController@getListKostsGoldplus');
            Route::get('filters', 'Api\GoldplusController@getGoldplusFilterOptions');
            Route::get('widget', 'Api\GoldPlus\WidgetController@getWidget')->name('widget');
            Route::get('widget-room', 'Api\GoldPlus\WidgetController@getWidgetRoomCount')->name('widget-room');
            Route::get('list-kost', 'Api\GoldPlus\GoldPlusAcquisitionController@getListKostsGoldplus');
            Route::get('list-kost/filters', 'Api\GoldPlus\GoldPlusAcquisitionController@getGoldplusFilterOptions');

            //Statistics Goldplus
            Route::get('statistics/filters/{option}', 'Api\GoldplusStatisticController@getStatisticFilterList');
            Route::get('{songId}', 'Api\GoldplusStatisticController@getDetailKostGp');
            Route::get('{songId}/statistics/{type}', 'Api\GoldplusStatisticController@getGoldplusStatistic');
        });
    });

    Route::group(['prefix' => 'kost', 'as' => 'kost.'], function () {
        Route::get('name', 'Api\RoomController@availableName')->name('name-availability');
    });

    Route::any('mamipay/request/{path}', 'Web\OwnerController@mamipayCallApi')->where('path', '.+')->name('mamipay.gate');
    Route::post('media', 'Api\MediaController@postIndex')->name('upload-media');

    Route::get('location-suggestion', 'Api\Search\LocationSuggestionController@suggestAutocomplete')->name('location-suggestion');
    Route::group(['prefix' => 'general-search', 'as' => 'general-search.'], function () {
        Route::post('/criteria', 'Api\GeneralSearchController@saveCriteria')->name('save-criteria');
        Route::get('/criteria', 'Api\GeneralSearchController@show')->name('show-criteria');
        Route::post('/keyword', 'Api\GeneralSearchController@saveUserInput')->name('save-keyword');
    });

    Route::group(['prefix' => 'config', 'as' => 'config.'], function () {
        Route::group(['prefix' => 'ab-test', 'as' => 'ab-test.'], function () {
            Route::get('owner-experience', 'Api\Owner\ConfigController@abTest')->name('ox');
        });
    });

    Route::get('facility/{type?}', 'Api\TagController@getFacilities')->name('facility-list');
    Route::get('tags', 'Api\TagController@index')->name('tags-list');

    Route::post('phone/check', 'Api\UserController@phoneNumberCheck')->name('phone-check');
    Route::post('feedback', 'Api\SurveyFeedbackController@submitFeedback')->name('feedback');

    Route::get('events', 'Api\RoomController@getHome')->name('events.banner');

    Route::post('logout','Oauth\AuthController@logout')->name('logout');
});

Route::group(['middleware' => 'oauth.client.credential', 'as' => 'machine-act.'],function(){
    Route::get(
        'user/{idUser}',
        'Oauth\UserController@getUser'
    )->where([
        'idUser' => '[0-9]+'
    ])->name('user.id');
});

Route::group(['middleware' => 'auth:passport','prefix' => 'me'], function () {
    // all routes inside namespace 'me' should run in the context of current user
    Route::get('/goldplus-submission', 'Api\Me\GoldPlus\SubmissionController@index');
    Route::get('/goldplus-submission/status', 'Api\Me\GoldPlus\SubmissionController@status');

    Route::get('/goldplus/active-contract', 'Api\Me\GoldPlus\ContractController@activeContract');
});