<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Singgahsini'], function () {
    Route::group(['prefix' => 'api/auth'], function() {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout')->middleware('auth:passport');
    });

    Route::group(['prefix' => 'api', 'middleware' => 'auth:passport'], function() {

    });
});