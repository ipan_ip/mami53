<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('media', 'Api\MediaController@postIndex');
Route::post('api/media', 'Api\MediaController@postIndex');
Route::get('api/user', function (Request $request) {
    return $request->user();
});
