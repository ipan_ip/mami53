<?php

Route::group(['prefix' => 'auth'], function () {
    Route::get('/', 'Admin\AuthController@getIndex')->name('login');
    Route::post('/', 'Admin\AuthController@adminLogin');
    Route::get('/admin/logout', 'Admin\AuthController@logout');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'permission:admin-access']], function () {

    Route::get('/', ['uses' => 'Admin\UserController@home', 'as' => 'dashboard.home']);

    // This would be a future feature: Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('checker', 'Admin\Users\UsersController@getUsersForChecker');
        Route::post('user', 'Admin\Users\UsersController@getSingleUser');
        // Consultant
        Route::get('consultant', 'Admin\Users\UsersController@getConsultantCandidates');
        Route::get('consultant/list', 'Admin\Consultant\ConsultantController@getConsultants');

        // Owner
        Route::get('owner', 'Admin\Users\UsersController@getOwners');
        Route::get('owner/{ownerId}/rent-counts-active-rooms', 'Admin\Booking\OwnerBookingController@rentCountActiveRoomsOwner');

        // Tenant
        Route::get('tenant', 'Admin\BookingUserController@getTenants');
        Route::get('tenant/{id}', 'Admin\BookingUserController@getTenantById');
    });

    Route::group(['prefix' => 'property', 'as' => 'property.'], function () {
        Route::group(['prefix' => 'level', 'as' => 'level.'], function () {
            Route::get('/', 'Admin\Level\PropertyLevelController@index')->name('index');
            Route::get('/create', 'Admin\Level\PropertyLevelController@create')->name('create');
            Route::post('/', 'Admin\Level\PropertyLevelController@store')->name('store');
            Route::get('/edit/{propertyLevel}', 'Admin\Level\PropertyLevelController@edit')->name('edit');
            Route::patch('/{propertyLevel}', 'Admin\Level\PropertyLevelController@update')->name('update');
            Route::delete('/{propertyLevel}', 'Admin\Level\PropertyLevelController@destroy')->name('delete');
        });

        Route::group(['prefix' => 'assign', 'as' => 'assign.'], function () {
            Route::get('/{property}', 'Admin\Property\PropertyAssignmentController@index')->name('index');
            Route::get('/{property}/{room}', 'Admin\Property\PropertyAssignmentController@toggle')->name('toggle');
        });

        Route::group(['prefix' => 'contract', 'as' => 'contract.'], function () {
            Route::get('/{property}/create', 'Admin\Property\PropertyContractController@create')->name('create');
            Route::get('/{property}/renew', 'Admin\Property\PropertyContractController@renew')->name('renew');
            Route::post('/{property}', 'Admin\Property\PropertyContractController@store')->name('store');
            Route::post('/{property}/renew', 'Admin\Property\PropertyContractController@storeRenew')->name('storeRenew');
            Route::patch('/{property}/terminate', 'Admin\Property\PropertyContractController@terminate')->name('terminate');
        });

        Route::get('/', 'Admin\Property\PropertyController@index')->name('index');
        Route::get('/create', 'Admin\Property\PropertyController@create')->name('create');
        Route::post('/', 'Admin\Property\PropertyController@store')->name('store');
        Route::get('/edit/{property}', 'Admin\Property\PropertyController@edit')->name('edit');
        Route::patch('/{property}', 'Admin\Property\PropertyController@update')->name('update');
        Route::delete('/{property}', 'Admin\Property\PropertyController@destroy')->name('destroy');
        Route::get('/{property}', 'Admin\Property\PropertyController@show')->name('show');
    });

    Route::group(['prefix' => 'survey'], function () {
        Route::get('owner', 'Admin\OwnerSurveyController@listSurveyOwner');
    });

    Route::group(['prefix' => 'squarepants'], function () {
        Route::get('notify', 'Admin\Squarepants\NotifyController@notifNow');
    });

    Route::group(['prefix' => 'house_property', 'as' => 'house_property.'], function () {
        Route::get('landing/redirect/{id}', 'Admin\Property\LandingHousePropertyController@redirectPage');
        Route::post('landing/redirect/{id}', 'Admin\Property\LandingHousePropertyController@storeLandingHousePropertyRedirection');
        Route::resource('landing', Admin\Property\LandingHousePropertyController::class);
    });

    Route::group(['prefix' => 'giant'], function () {
        Route::group(['prefix' => 'apartment'], function () {
            Route::group(['prefix' => 'assign'], function () {
                Route::get('new', 'Admin\Giant\Apartment\AgentController@assignNew');
                Route::post('store', 'Admin\Giant\Apartment\AgentController@storeData');
                Route::get('delete/{id}', 'Admin\Giant\Apartment\AgentController@deleted');
                Route::get('/', 'Admin\Giant\Apartment\AgentController@assignAgent');
            });
        });
        Route::group(['prefix' => 'manual'], function () {
            Route::post('review/{id}', 'Admin\Giant\DataController@storeReview');
        });
        Route::group(['prefix' => 'kos'], function () {
            Route::get('list', 'Admin\Giant\DataController@agentList');
            Route::get('edit/{id}', 'Admin\Giant\DataController@getEdit');
            Route::get('agent', 'Admin\Giant\DataController@addNewAgent');
            Route::post('agent', 'Admin\Giant\DataController@storeAgent');
            Route::post('agent/update/{id}', 'Admin\Giant\DataController@updateAgent');
        });
        Route::get('data', 'Admin\Giant\DataController@listData');
        Route::get('reject/{id}', 'Admin\Giant\DataController@rejectDataAgent');
        Route::get('edit/{id}', 'Admin\Giant\DataController@editData');
        Route::post('update/{id}', 'Admin\Giant\DataController@updateData');
        Route::get('delete/{id}', 'Admin\Giant\DataController@deleteData');
    });

    Route::group(['prefix' => 'house_property'], function () {
        Route::get('verify/{id}', 'Admin\HouseProperty\HousePropertyController@verify');
        Route::get('reject/{id}', 'Admin\HouseProperty\HousePropertyController@rejectPage');
        Route::post('reject/{id}', 'Admin\HouseProperty\HousePropertyController@rejectPost');
        Route::get('verify/phone/{id}', 'Admin\HouseProperty\HousePropertyController@phoneVerification');
        Route::resource('card', Admin\HouseProperty\CardController::class);
        Route::get('{propertyId}/card/{cardId}/set_cover', 'Admin\HouseProperty\CardController@setCover');
    });
    Route::resource('house_property', Admin\HouseProperty\HousePropertyController::class);

    /**
     * This route is for room, there was some defined by stories or room
     */
    Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
        Route::get('two', array('as' => 'two.index', 'uses' => 'Admin\RoomController@indexTwo'));
        Route::group(['prefix' => 'agent','as' => 'agent.'], function () {
            Route::get('properties/{id}', 'Admin\Agent\AgentController@getRoomProperties')->name('property');
            Route::post('verify/{id}', 'Admin\Agent\AgentController@agentVerify')->name('verify');
            Route::get('check-property-name/{id}', 'Admin\Agent\AgentController@checkPropertyName')->name('check-property-name');
            Route::post('property', 'Admin\Agent\AgentController@addRoomProperty')->name('add-room-property');
            Route::get('check-unit-type/{id}', 'Admin\Agent\AgentController@checkUnitType')->name('check-unit-type');
            Route::get('property/{id}', 'Admin\Agent\AgentController@getDetailProperty')->name('property-detail');
        });

        Route::get('phone/active/{id}', array('as' => 'phone.active', 'uses' => 'Admin\RoomController@activatePhone'));
        Route::group(['prefix' => 'create'], function () {
            Route::get('owner/{id}', 'Admin\RoomController@createOwner');
            Route::post('owner/{id}', 'Admin\RoomController@storeOwner');
        });
        Route::get('images', 'Admin\MediaController@listPhotos');
        Route::post('available/count/{id}', array('as' => 'available.count', 'uses' => 'Admin\RoomController@availableCount'));
        Route::post('price/{id}', array('as' => 'price', 'uses' => 'Admin\RoomController@updatePrice'));
        Route::get('gender/{id}/{gender}', array('as' => 'gender', 'uses' => 'Admin\RoomController@updateGender'));
        Route::post('update/two/{id}', array('as' => 'update.two', 'uses' => 'Admin\RoomController@updateTwo'));
        Route::get('history-owner/{id}/{owner_id}', array('as' => 'history-owner', 'uses' => 'Admin\RoomController@historyOwner'));
        Route::get('history/{id}', array('as' => 'history', 'uses' => 'Admin\Room\RevisionController@index'));
        Route::get('history/verify/{ownerId}', array('as' => 'history-verify', 'uses' => 'Admin\Owner\StatusHistoryController@index'));
        Route::get('call/{id}', array('as' => 'call', 'uses' => 'Admin\RoomController@CallOwner'));
        Route::get('deactive/{id}', array('as' => 'deactive', 'uses' => 'Admin\RoomController@deactiveKost'));

        Route::group(['prefix' => 'pinned'], function () {
            Route::get('deactivate/{id}', 'Admin\RoomController@pinnedDeactivate');
            Route::post('/', array('as' => 'pinned', 'uses' => 'Admin\RoomController@pinnedKostAdmin'));
        });

        Route::post('search', array('as' => 'search', 'uses' => 'Admin\RoomController@index'));
        Route::get('search', array('as' => 'search', 'uses' => 'Admin\RoomController@index'));
        Route::get('search/api', 'Admin\RoomController@getRooms');
        Route::get('sorting', array('as' => 'sorting', 'uses' => 'Admin\RoomController@designerSorting'));
        Route::post('sort', array('as' => 'sort', 'uses' => 'Admin\RoomController@designerSort'));
        Route::get('add/json', array('as' => 'add.json', 'uses' => 'Admin\RoomController@getAddJson'));
        Route::get('verify/{id}', array('as' => 'verify.action', 'uses' => 'Admin\RoomController@verify'));
        Route::get('justverify/{id}', array('as' => 'justverify.action', 'uses' => 'Admin\RoomController@justVerifyRoom'));
        Route::get('setstatusadd/{id}', array('as' => 'setstatusadd.action', 'uses' => 'Admin\RoomController@setStatusAdd'));

        Route::get('verifyslug/{id}', array('as' => 'verifyslug.action', 'uses' => 'Admin\RoomController@verifyslug'));

        Route::get('booking/{id}', array('as' => 'booking.action', 'uses' => 'Admin\RoomController@canBooking'));

        Route::get('promotion/{id}', array('as' => 'promotion.action', 'uses' => 'Admin\RoomController@getPromotion'));
        Route::post('promotion/{id}', array('as' => 'promotion.action', 'uses' => 'Admin\RoomController@premiumAllocation'));

        Route::get('unbooking/{id}', array('as' => 'unbooking.action', 'uses' => 'Admin\RoomController@cantBooking'));

        Route::get('verifytanpasms/{id}', array('as' => 'verifytanpasms.action', 'uses' => 'Admin\RoomController@verifyTanpaSms'));

        Route::get('unverify/{id}', array('as' => 'unverify.action', 'uses' => 'Admin\RoomController@unverify'));
        Route::get('snap/{id}', array('as' => 'snap.action', 'uses' => 'Admin\RoomController@snap'));
        Route::get('revertsnap/{id}', array('as' => 'revertSnap.action', 'uses' => 'Admin\RoomController@revertSnap'));
        Route::get('highlight/{id}', array('as' => 'highlight.action', 'uses' => 'Admin\RoomController@highlight'));
        Route::get('unhighlight/{id}', array('as' => 'unhighlight.action', 'uses' => 'Admin\RoomController@unhighlight'));
        Route::get('{id}/full', array('as' => 'full', 'uses' => 'Admin\RoomController@markAsFull'));
        Route::get('{id}/available', array('as' => 'available', 'uses' => 'Admin\RoomController@markAsAvailable'));
        Route::post('submit/{id}', array('as' => 'submit', 'uses' => 'Admin\RoomController@submit'));
        Route::get('delete/{id}', array('as' => 'destroy', 'uses' => 'Admin\RoomController@destroy'));
        Route::get('chat', array('as' => 'chat', 'uses' => 'Admin\ChatController@index'));
        Route::get('survey/{id}', array('as' => 'survey', 'uses' => 'Admin\RoomController@getListSurvey'));

        Route::get('redirectkost/{id}', array('as' => 'redirectkost', 'uses' => 'Admin\RoomController@redirectkost'));

        Route::post('redirectkost', array('as' => 'redirectkost.action', 'uses' => 'Admin\RoomController@redirectkostpost'));
        Route::get('rename-photo/{id}', 'Admin\RoomController@renamePhoto');

        Route::get('cs-chat-info/{song_id}', 'Admin\RoomController@getInfoForCS');
        Route::get('check-name', 'Admin\RoomController@availableName');

        Route::get('rename-parser', 'Admin\RoomController@renameParser');
        Route::post('rename-parser', 'Admin\RoomController@renameParserPost');

        Route::get('migrate-apartment/{id}', ['as' => 'migrate', 'uses' => 'Admin\RoomController@kostToApartmentMigrate']);
        Route::post('migrate-apartment/{id}', ['as' => 'migrate.post', 'uses' => 'Admin\RoomController@updateKostToApartment']);

        // Verify by Mamikos Team ~ used on API v1.2.3
        Route::get('verify-mamikos/{id}', array('as' => 'verify_mamikos.action', 'uses' => 'Admin\RoomController@verifyByMamikos'));
        Route::get('unverify-mamikos/{id}', array('as' => 'unverify_mamikos.action', 'uses' => 'Admin\RoomController@unverifyByMamikos'));

        Route::get('video/{id}', array('as' => 'video', 'uses' => 'Admin\RoomController@addVideoTour'));
        Route::get('video/edit/{id}', array('as' => 'video.update', 'uses' => 'Admin\RoomController@updateVideoTour'));
        Route::post('video', array('as' => 'video.store', 'uses' => 'Admin\RoomController@storeVideoTour'));
        Route::post('video/remove', array('as' => 'video.remove', 'uses' => 'Admin\RoomController@removeVideoTour'));

        Route::get('mamirooms/{room}/{action}', 'Admin\RoomController@setMamirooms')->name('mamirooms.action');
        Route::get('testing/{room}/{action}', 'Admin\RoomController@setTesting')->name('testing.action');

        // Bookable Kost Unique Code
        // Ref issue: https://mamikos.atlassian.net/browse/KOS-8557
        Route::get('ucode', array('as' => 'ucode.index', 'uses' => 'Admin\RoomUniqueCodeController@index'));
        Route::get('setting', array('as' => 'ucode.setting', 'uses' => 'Admin\RoomUniqueCodeController@setting'));
        Route::get('cities', array('uses' => 'Admin\RoomUniqueCodeController@getCitiesAjax'));
        Route::get('cities/clean', array('uses' => 'Admin\RoomUniqueCodeController@getUnassignedCitiesAjax'));
        Route::get('codes', array('uses' => 'Admin\RoomUniqueCodeController@getCodesAjax'));
        Route::post('ucode', array('as' => 'ucode.add', 'uses' => 'Admin\RoomUniqueCodeController@add'));
        Route::post('ucode/update', array('as' => 'ucode.update', 'uses' => 'Admin\RoomUniqueCodeController@update'));
        Route::post('ucode/generate', array('as' => 'ucode.generate', 'uses' => 'Admin\RoomUniqueCodeController@generateCode'));

        // Ref task: https://mamikos.atlassian.net/browse/BG-166
        Route::get('ucode/bulk', array('as' => 'ucode.bulk', 'uses' => 'Admin\RoomUniqueCodeController@bulkGenerateCodes'));

        // Facility feature @ API v1.7.2
        // Ref issue: https://mamikos.atlassian.net/browse/KOS-11629
        Route::group(['prefix' => 'facility', 'as' => 'facility.'], function () {
            Route::get('/{id}', ['as' => 'add', 'uses' => 'Admin\Tag\FacilityController@create']);
            Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'Admin\Tag\FacilityController@edit']);
            Route::post('/', ['as' => 'store', 'uses' => 'Admin\Tag\FacilityController@store']);
            Route::post('edit', ['as' => 'update', 'uses' => 'Admin\Tag\FacilityController@update']);
            Route::get('setting/{id}', ['as' => 'setting', 'uses' => 'Admin\Tag\FacilityController@getSetting']);
            Route::post('setting', ['as' => 'setting-update', 'uses' => 'Admin\Tag\FacilityController@updateSetting']);
            Route::post('remove', ['as' => 'remove', 'uses' => 'Admin\Tag\FacilityController@remove']);
        });

        // Room Types @ API v1.7.3
        Route::group(['prefix' => 'type', 'as' => 'type.'], function () {
            Route::get('{id}', ['as' => 'index', 'uses' => 'Admin\RoomTypeController@index']);
            Route::group(['prefix' => 'facility', 'as' => 'facility.'], function () {
                Route::get('/{id}', ['as' => 'add', 'uses' => 'Admin\Tag\FacilityController@typeCreate']);
                Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'Admin\Tag\FacilityController@typeEdit']);
                Route::post('/', ['as' => 'store', 'uses' => 'Admin\Tag\FacilityController@typeStore']);
                Route::post('edit', ['as' => 'update', 'uses' => 'Admin\Tag\FacilityController@typeUpdate']);
                Route::get('setting/{id}', ['as' => 'setting', 'uses' => 'Admin\Tag\FacilityController@typeGetSetting']);
                Route::post('setting', ['as' => 'setting-update', 'uses' => 'Admin\Tag\FacilityController@typeUpdateSetting']);
                Route::post('remove', ['as' => 'remove', 'uses' => 'Admin\Tag\FacilityController@typeRemove']);
            });
        });

        Route::post('duplicate/{id}', 'Admin\RoomController@createRoomDuplicates');
        Route::get('recalculate/{id}', 'Admin\RoomController@recalculateRoomScore');
        Route::get('price-filter/{id}', 'Admin\RoomController@getPriceFilter');
        Route::post('bulk-verify', 'Admin\RoomController@bulkVerify');
        Route::post('bulk-just-verify', 'Admin\RoomController@bulkJustVerify');
        Route::post('bulk-verify-slug', 'Admin\RoomController@bulkVerifySlug');

        Route::group(['prefix' => '{id}/room-unit', 'as' => 'room-unit.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'Admin\Room\RoomUnitController@index']);
            Route::post('/delete-bulk', ['as' => 'delete-bulk', 'uses' => 'Admin\Room\RoomUnitController@deleteBulk']);
            Route::post('/{unit}/update-row', ['as' => 'update-row', 'uses' => 'Admin\Room\RoomUnitController@updateRow']);
            Route::post('/{unit}/delete-row', ['as' => 'delete-row', 'uses' => 'Admin\Room\RoomUnitController@deleteRow']);
            Route::post('/store', ['as' => 'store', 'uses' => 'Admin\Room\RoomUnitController@store']);
            Route::get('/log', ['as' => 'log', 'uses' => 'Admin\Room\RoomUnitRevisionController@index']);
            Route::post('/sync', ['as' => 'sync', 'uses' => 'Admin\Room\RoomUnitController@syncData']);
        });
    });

    Route::resource('room', Admin\RoomController::class);

    Route::group(['middleware' => ['permission:access-kost']], function () {
        Route::get('vrtour/{room}/create', 'Admin\RoomMetadata\RoomAttachmentController@create')->name('vrtour.create');
        Route::post('vrtour/{room}', 'Admin\RoomMetadata\RoomAttachmentController@store')->name('vrtour.store');
        Route::resource('vrtour', Admin\RoomMetadata\RoomAttachmentController::class)->except(['create', 'store', 'show']);
    });

    Route::group(['prefix' => 'kost', 'as' => 'kost.', 'middleware' => ['permission:access-kost']], function () {
        Route::get('/create', 'Admin\Room\KostController@create')->name('create');
        Route::post('/', 'Admin\Room\KostController@store')->name('store');
        Route::get('/{id}/edit', 'Admin\Room\KostController@edit')->name('edit');
        Route::put('/{id}', 'Admin\Room\KostController@update')->name('update');
        Route::group(['prefix' => 'property', 'as' => 'property.'], function () {
            Route::post('/', 'Admin\Room\PropertyController@store')->name('store');
            Route::put('/{id}', 'Admin\Room\PropertyController@update')->name('update');
        });
    });

    Route::resource('agents', Admin\Agent\AgentController::class);
    Route::group(array('prefix' => 'card', 'as' => 'card.'), function () {
        Route::get('/{id}', array('as' => 'index', 'uses' => 'Admin\CardController@index'));
        Route::get('create/{id}', array('as' => 'create', 'uses' => 'Admin\CardController@create'));
        Route::post('store/{room_id}', array('as' => 'store', 'uses' => 'Admin\CardController@store'));
        Route::get('edit/{room_id}/{id}', array('as' => 'edit', 'uses' => 'Admin\CardController@edit'));
        Route::put('update/{room_id}/{id}', array('as' => 'update', 'uses' => 'Admin\CardController@update'));
        Route::get('delete/{room_id}/{id}', array('as' => 'destroy', 'uses' => 'Admin\CardController@destroy'));
        Route::get('answer/{id}', array('as' => 'get', 'uses' => 'Admin\CardController@getAnswer'));
        Route::get('rotate/{id}', array('as' => 'rotate', 'uses' => 'Admin\CardController@getRotate'));
        Route::get('cover/{id}', array('as' => 'cover', 'uses' => 'Admin\CardController@setCover'));
        Route::get('show/{id}', array('as' => 'show', 'uses' => 'Admin\CardController@show'));
        Route::get('preview/{id}', array('as' => 'preview', 'uses' => 'Admin\CardController@getPreview'));
        Route::put('update-photo-group', array('as' => 'update.photo-group', 'uses' => 'Admin\CardController@updatePhotoGroup'));

        Route::group(array('prefix' => 'premium', 'as' => 'premium.'), function () {
            Route::get('/{id}', array('as' => 'index', 'uses' => 'Admin\CardPremiumController@index'));
            Route::get('create/{id}', array('as' => 'create', 'uses' => 'Admin\CardPremiumController@create'));
            Route::post('activate', array('as' => 'activate', 'uses' => 'Admin\CardPremiumController@switchStatus'));
            Route::post('store/{room_id}', array('as' => 'store', 'uses' => 'Admin\CardPremiumController@store'));
            Route::get('edit/{room_id}/{id}', array('as' => 'edit', 'uses' => 'Admin\CardPremiumController@edit'));
            Route::put('update/{room_id}/{id}', array('as' => 'update', 'uses' => 'Admin\CardPremiumController@update'));
            Route::get('delete/{room_id}/{id}', array('as' => 'destroy', 'uses' => 'Admin\CardPremiumController@destroy'));
            Route::get('answer/{id}', array('as' => 'get', 'uses' => 'Admin\CardPremiumController@getAnswer'));
            Route::get('rotate/{id}', array('as' => 'rotate', 'uses' => 'Admin\CardPremiumController@getRotate'));
            Route::get('cover/{id}', array('as' => 'cover', 'uses' => 'Admin\CardPremiumController@setCover'));
        });
    });

    Route::group(['prefix' => 'apartment', 'as' => 'apartment'], function () {
        Route::get('project', 'Admin\ApartmentProjectController@index')->name('.project.index');
        Route::get('project/create', 'Admin\ApartmentProjectController@create')->name('.project.create');
        Route::post('project/store', 'Admin\ApartmentProjectController@store')->name('.project.store');
        Route::get('project/{id}/edit', 'Admin\ApartmentProjectController@edit')->name('.project.edit');
        Route::post('project/update', 'Admin\ApartmentProjectController@update')->name('.project.update');
        Route::get('project/{id}/activate', 'Admin\ApartmentProjectController@activate')->name('.project.activate');
        Route::get('project/{id}/deactivate', 'Admin\ApartmentProjectController@deactivate')->name('.project.deactivate');
        Route::get('project/delete/{id}', 'Admin\ApartmentProjectController@destroy')->name('.project.delete');

        Route::get('project/redirect/landing/{project_id}', 'Admin\ApartmentProjectController@redirectLanding')->name('.redirect.landing');
        Route::post('project/redirect/landing/store', 'Admin\ApartmentProjectController@saveRedirectLanding')->name('.project.redirect.landing.store');

        Route::get('unit/{project_id}', 'Admin\ApartmentUnitController@list')->name('.unit.list');
        Route::get('unit/create/{project_id}', 'Admin\ApartmentUnitController@create')->name('.unit.create');
        Route::post('unit/create/{project_id?}', 'Admin\ApartmentUnitController@store')->name('.unit.store');
        Route::get('unit/{id}/edit', 'Admin\ApartmentUnitController@edit')->name('.unit.edit');
        Route::post('unit/update', 'Admin\ApartmentUnitController@update')->name('.unit.update');

        Route::get('parse', ['as' => 'apartment.parser', 'uses' => 'Admin\ApartmentProjectController@parse']);
        Route::post('parse', ['as' => 'apartment.post-parser', 'uses' => 'Admin\ApartmentProjectController@parsePost']);
        Route::get('parse-simple', ['as' => 'apartment.parser', 'uses' => 'Admin\ApartmentProjectController@parseSimple']);
        Route::post('parse-simple', ['as' => 'apartment.post-parser', 'uses' => 'Admin\ApartmentProjectController@parseSimplePost']);

        Route::get('parse-type', ['as' => 'apartment.parser-type', 'uses' => 'Admin\ApartmentProjectController@parseType']);
        Route::post('parse-type', ['as' => 'apartment.post-parser-type', 'uses' => 'Admin\ApartmentProjectController@parseTypePost']);

        Route::get('parse-tower', ['as' => 'apartment.parser-tower', 'uses' => 'Admin\ApartmentProjectController@parseTower']);
        Route::post('parse-tower', ['as' => 'apartment.post-parser-tower', 'uses' => 'Admin\ApartmentProjectController@parseTowerPost']);

        Route::get('type/{project_id}', 'Admin\ApartmentProjectController@listType')->name('.project.type.list');
        Route::get('type/{project_id}/create', 'Admin\ApartmentProjectController@createType')->name('.project.type.create');
        Route::post('type/{project_id}/store', 'Admin\ApartmentProjectController@storeType')->name('.project.type.store');
        Route::get('type/{project_id}/edit', 'Admin\ApartmentProjectController@editType')->name('.project.type.edit');
        Route::post('type/{project_id}/update', 'Admin\ApartmentProjectController@updateType')->name('.project.type.update');
        Route::get('type/{project_id}/delete', 'Admin\ApartmentProjectController@destroyType')->name('.project.type.delete');

        Route::get('tower/{project_id}', 'Admin\ApartmentProjectController@listTower')->name('.project.tower.list');
        Route::get('tower/{project_id}/create', 'Admin\ApartmentProjectController@createTower')->name('.project.tower.create');
        Route::post('tower/{project_id}/store', 'Admin\ApartmentProjectController@storeTower')->name('.project.tower.store');
        Route::get('tower/{project_id}/edit', 'Admin\ApartmentProjectController@editTower')->name('.project.tower.edit');
        Route::post('tower/{project_id}/update', 'Admin\ApartmentProjectController@updateTower')->name('.project.tower.update');
        Route::get('tower/{project_id}/delete', 'Admin\ApartmentProjectController@destroyTower')->name('.project.tower.delete');
    });

    Route::get('report', ['as' => 'room.report.index', 'uses' => 'Admin\ReportController@index']);
    Route::get('report/{roomId}', ['as' => 'room.report.show', 'uses' => 'Admin\ReportController@show']);
    Route::get('report/followedup/{id}', ['as' => 'room.report.followedup', 'uses' => 'Admin\ReportController@setFollowedUp']);

    Route::post('media', ['as' => 'media.upload', 'uses' => 'Admin\MediaController@storeMedia']);
    Route::post('media/generate', ['as' => 'media.generate', 'uses' => 'Admin\MediaController@generateWatermark']);
    Route::get('media/term/{id}', ['as' => 'media.term.destroy', 'uses' => 'Admin\MediaController@deleteRoomTermMedia']);
    Route::get('media/{id}', ['as' => 'media.destroy', 'uses' => 'Admin\MediaController@deleteCard']);
    Route::get('media/{id}/delete', 'Admin\MediaController@deletePhoto');
    Route::get('media/user/{id}', ['as' => 'media.user.destroy', 'uses' => 'Admin\MediaController@deleteUserPhoto']);
    Route::get('media/round/{room_id}/{id}/delete', ['as' => 'media.round.destroy', 'uses' => 'Admin\MediaController@deleteCardRound']);
    Route::get('media/show/{id}', ['as' => 'media.show', 'uses' => 'Admin\MediaController@getImageReal']);
    Route::resource('dashboard', Admin\DashboardController::class);

    Route::resource('call', Admin\CallController::class);
    Route::get('call/{id}/reply', ['as' => 'call.reply', 'uses' => 'Admin\CallController@getReply']);
    Route::post('call/{id}/reply', ['as' => 'call.reply', 'uses' => 'Admin\CallController@sendReply']);

    Route::resource('referrer', Admin\ReferrerController::class);
    Route::resource('mca', Admin\McaController::class);
    Route::get('mca/verify/{id}', 'Admin\McaController@verify');

    Route::get('landing/parse', ['as' => 'landing.parser', 'uses' => 'Admin\LandingController@parseIndex']);
    Route::post('landing/parse', ['as' => 'landing.post-parser', 'uses' => 'Admin\LandingController@parseCSV']);
    Route::resource('landing', Admin\LandingController::class);
    Route::get('landing/delete/{id}', ['as' => 'landing.destroy', 'uses' => 'Admin\LandingController@destroy']);
    Route::get('landing/redirect/{id}', 'Admin\LandingController@redirectlanding');
    Route::post('landing/redirect/{id}', 'Admin\LandingController@redirectlandingpost');

    //Meta+OG
    Route::group(['prefix' => 'landing-meta', 'as' => 'landing.meta'], function() {
        Route::get('/', 'Admin\LandingMetaOgController@index')->name('.index');
        Route::get('create', 'Admin\LandingMetaOgController@create')->name('.create');
        Route::post('save', 'Admin\LandingMetaOgController@save')->name('.save');
        Route::get('edit/{id}', 'Admin\LandingMetaOgController@edit')->name('.edit');
        Route::post('update/{id}', 'Admin\LandingMetaOgController@update')->name('.update');
    });

    Route::get('landing-apartment/parse', ['as' => 'landing-apartment.parser', 'uses' => 'Admin\LandingApartmentController@parseIndex']);
    Route::post('landing-apartment/parse', ['as' => 'landing-apartment.post-parser', 'uses' => 'Admin\LandingApartmentController@parseCSV']);
    Route::resource('landing-apartment', 'Admin\LandingApartmentController');
    Route::get('landing-apartment/delete/{id}', ['as' => 'landing-apartment.destroy', 'uses' => 'Admin\LandingApartmentController@destroy']);
    Route::get('landing-apartment/redirect/{id}', 'Admin\LandingApartmentController@redirectlanding');
    Route::post('landing-apartment/redirect/{id}', 'Admin\LandingApartmentController@redirectlandingpost');

    Route::get('jobs-landing/parse', ['as' => 'jobs-landing.parser', 'uses' => 'Admin\JobsLandingController@parseIndex']);
    Route::post('jobs-landing/parse', ['as' => 'jobs-landing.post-parser', 'uses' => 'Admin\JobsLandingController@parseCSV']);
    Route::get('jobs-landing/redirect/{id}', 'Admin\JobsLandingController@redirectLanding');
    Route::post('jobs-landing/redirect/{id}', 'Admin\JobsLandingController@redirectLandingPost');
    Route::get('jobs-landing/destroy/{id}', 'Admin\JobsLandingController@destroy')->name('jobs-landing.delete');
    Route::resource('jobs-landing', 'Admin\JobsLandingController');

    Route::get('home-static-landing/delete/{id}', 'Admin\HomeStatic\HomeStaticLandingController@delete')
        ->name('home-static-landing.delete');
    Route::resource('home-static-landing', 'Admin\HomeStatic\HomeStaticLandingController');

    Route::get('jobs/company', 'Admin\JobsController@indexCompany');
    Route::get('jobs/company/new', 'Admin\JobsController@addNewCompany');
    Route::post('jobs/company/new', 'Admin\JobsController@addNewPostCompany');
    Route::get('jobs/company/edit/{id}', 'Admin\JobsController@companyEdit');
    Route::post('jobs/company/edit/{id}', 'Admin\JobsController@companyUpdate');
    Route::get('jobs/company/delete/{id}', 'Admin\JobsController@deleteCompany');

    Route::get('spesialisasi/destroy/{id}', 'Admin\Vacancy\SpesialisasiController@destroy');
    Route::resource('spesialisasi', Admin\Vacancy\SpesialisasiController::class);
    Route::get('industry/destroy/{id}', 'Admin\Vacancy\IndustryController@destroy');
    Route::resource('industry', Admin\Vacancy\IndustryController::class);
    Route::resource('jobs', 'Admin\JobsController');
    Route::group(["prefix" => "jobs"], function () {
        Route::get('redirect/{id}', 'Admin\JobsController@redirectPage');
        Route::post('redirect/{id}', 'Admin\JobsController@storeRedirectData');
        Route::get('i/company', 'Admin\JobsController@getCompany');
        Route::get('i/import', 'Admin\JobsController@import');
        Route::post('i/import', 'Admin\JobsController@storeImportedVacancy');
        Route::get('verifyslug/{id}', 'Admin\JobsController@verifySlugVacancy');
        Route::get('apply/list', 'Admin\JobsApplyController@applyList');
        Route::get('apply/{id}', 'Admin\JobsApplyController@list');
        Route::get('view/{id}', 'Admin\JobsApplyController@viewCv');
        Route::get('verify/{id}', 'Admin\JobsController@verify');
        Route::get('delete/{id}', 'Admin\JobsController@delete');
    });
    Route::get('jobs/create-account/{id}', 'Admin\JobsController@createAccount');
    Route::post('jobs/create-account/{id}', 'Admin\JobsController@storeVacancyAccount');
    Route::resource('vacancy-speciality-types', Admin\Vacancy\VacancySpesialisasiTypeController::class);

    Route::resource('connector-landing', 'Admin\ConnectorLandingController');

    Route::get('jobs/apply/list', 'Admin\JobsApplyController@applyList');
    Route::get('jobs/apply/{id}', 'Admin\JobsApplyController@list');
    Route::get('jobs/view/{id}', 'Admin\JobsApplyController@viewCv');
    Route::get('jobs/verify/{id}', 'Admin\JobsController@verify');
    Route::get('jobs/delete/{id}', 'Admin\JobsController@delete');

    // Whatsapp Notification Management :: used on API v1.7.1
    Route::group(["prefix" => "wa-notification", "as" => "wa-notification"], function () {
        Route::get('/', 'Admin\WhatsAppNotificationController@index')->name('.index');
        Route::post('/', 'Admin\WhatsAppNotificationController@store')->name('.store');
        Route::post('test', 'Admin\WhatsAppNotificationController@sendTest')->name('.test');
        Route::get('reports/{id}', 'Admin\WhatsAppNotificationController@getReports')->name('.reports');
        Route::post('reports/sync', 'Admin\WhatsAppNotificationController@getReportsSynced')->name('.reports.sync');
        Route::post('refresh', 'Admin\WhatsAppNotificationController@refreshReport')->name('.refresh');
        Route::post('sms', 'Admin\WhatsAppNotificationController@updateSms')->name('.sms');
        Route::post('sms/reset', 'Admin\WhatsAppNotificationController@resetSms')->name('.sms.reset');
        Route::post('enable', 'Admin\WhatsAppNotificationController@enable')->name('.enable');
        Route::post('disable', 'Admin\WhatsAppNotificationController@disable')->name('.disable');
        Route::post('activate', 'Admin\WhatsAppNotificationController@activate')->name('.activate');
        Route::post('deactivate', 'Admin\WhatsAppNotificationController@deactivate')->name('.deactivate');
        Route::post('remove', 'Admin\WhatsAppNotificationController@remove')->name('.remove');
        Route::get('scenario', 'Admin\WhatsAppNotificationController@checkScenario');
        Route::post('scenario', 'Admin\WhatsAppNotificationController@createScenario');
        Route::post('events', 'Admin\WhatsAppNotificationController@getUserEvents');
    });

    Route::resource('notif', Admin\NotifController::class);

    Route::get('notification/log', 'Admin\NotificationLogController@index')->middleware('permission:access-notification')->name('notification.log');

    Route::get('send_notif/{id}', array('as' => 'notif.send.id', 'uses' => 'Admin\NotifController@sendNotif'));

    Route::resource('sms', Admin\SmsController::class);
    Route::get('sms/confirm/{id}', ['as' => 'sms.confirm', 'uses' => 'Admin\SmsController@getConfirm']);
    Route::post('sms/send/{id}', ['as' => 'sms.send', 'uses' => 'Admin\SmsController@sendSms']);

    // Tags route
    Route::resource('tag', Admin\Tag\TagController::class);
    Route::group(["prefix" => "tag", "as" => "tag"], function () {
        Route::post('', 'Admin\Tag\TagController@store')->name('.store');
        Route::post('enable', 'Admin\Tag\TagController@enable')->name('.enable');
        Route::post('disable', 'Admin\Tag\TagController@disable')->name('.disable');
        Route::post('set', 'Admin\Tag\TagController@set')->name('.set');
        Route::post('unset', 'Admin\Tag\TagController@unset')->name('.unset');
        Route::post('add-filter', 'Admin\Tag\TagController@addToFilter');
        Route::post('remove-filter', 'Admin\Tag\TagController@removeFromFilter');
        Route::post('display', 'Admin\Tag\TagController@display')->name('.display');
        Route::post('undisplay', 'Admin\Tag\TagController@undisplay')->name('.undisplay');
        Route::post('remove', 'Admin\Tag\TagController@remove')->name('.remove');
        Route::post('order', 'Admin\Tag\TagController@sortOrder')->name('.order');
        Route::post('unique', 'Admin\Tag\TagController@isUnique')->name('.unique');
        Route::post('delete-second-type', 'Admin\Tag\TagController@deleteSecondLabel');
        Route::get('setting/top/{id}', 'Admin\Tag\TagController@setTopFacility')->name('.setting');
    });

    // Tags route
    Route::resource('tag-max-renter', Admin\Tag\TagMaxRenterController::class);

    Route::group(["prefix" => "facility", "as" => "facility."], function () {
        // Facility Categories
        Route::group(["prefix" => "category", "as" => "category."], function () {
            Route::get('/', 'Admin\Tag\FacilityCategoryController@index')->name('index');
            Route::post('/', 'Admin\Tag\FacilityCategoryController@store')->name('store');
            Route::get('list', 'Admin\Tag\FacilityCategoryController@getAllCategories')->name('list');
            Route::post('remove', 'Admin\Tag\FacilityCategoryController@remove')->name('remove');
            Route::get('ajax', 'Admin\Tag\FacilityCategoryController@getAllCategoriesInAjax')->name('ajax');
        });

        // Facility Types
        Route::group(["prefix" => "type", "as" => "type."], function () {
            Route::get('/', 'Admin\Tag\FacilityTypeController@index')->name('index');
            Route::post('/', 'Admin\Tag\FacilityTypeController@store')->name('store');
            Route::post('remove', 'Admin\Tag\FacilityTypeController@remove')->name('remove');
            Route::get('ajax', 'Admin\Tag\FacilityTypeController@getAllTypesInAjax')->name('ajax');
            Route::post('sort', 'Admin\Tag\FacilityTypeController@sort')->name('sort');
        });
    });

    Route::resource('shortlink', Admin\ShortlinkGeneralController::class);
    Route::get('shortlink/destroy/{id}', 'Admin\ShortlinkGeneralController@destroy');

    Route::get('review/reply', 'Admin\ReviewController@reply');
    Route::resource('review', Admin\ReviewController::class);
    Route::get('review/confirm/{id}', 'Admin\ReviewController@changeStatusReview');
    Route::get('review/thread/{id}', 'Admin\ReviewController@threadReview');
    Route::get('review/photo/rotate/{id}', 'Admin\ReviewController@rotatePhotoReview');
    Route::get('review/reply/confirm/{id}', 'Admin\ReviewController@replyConfirm');
    Route::get('review/reply/deleted/{id}', 'Admin\ReviewController@replyDeleted');
    Route::get('review/destroy/{id}', 'Admin\ReviewController@destroy');

    Route::get('question/status/{id}', 'Admin\questionController@changeStatus');
    Route::group(['prefix' => 'question'], function () {
        Route::get('answer/delete/{id}', 'Admin\questionController@answerDeleted');
    });
    Route::resource('question', Admin\questionController::class);

    Route::group(['prefix' => 'chat-condition'], function () {
        Route::post('update', 'Admin\questionController@storeCondition');
        Route::post('delete', 'Admin\questionController@deleteCondition');
        Route::post('order', 'Admin\questionController@sortOrder');
    });

    Route::resource('areas', Admin\AreaController::class);
    Route::get('areas/destroy/{id}', 'Admin\AreaController@destroy');

    Route::get('area-big-mapper/parse', ['as' => 'area-big-mapper.parser', 'uses' => 'Admin\AreaBigMapperController@parse']);
    Route::post('area-big-mapper/parse', ['as' => 'area-big-mapper.post-parser', 'uses' => 'Admin\AreaBigMapperController@parsePost']);
    Route::resource('area-big-mapper', Admin\AreaBigMapperController::class);
    Route::get('area-big-mapper/{id}/destroy', ['as' => 'area-big-mapper.destroy', 'uses' => 'Admin\AreaBigMapperController@destroy']);
    Route::get('area-big-mapper/{id}/sync', ['as' => 'area-big-mapper.sync', 'uses' => 'Admin\AreaBigMapperController@sync']);
    Route::post('area-big-mapper/{id}/update-landing', ['as' => 'area-big-mapper.update-landing', 'uses' => 'Admin\AreaBigMapperController@updateAssociateLanding']);
    Route::post('area-big-mapper/landing-suggestion', 'Admin\AreaBigMapperController@landingSuggestion');

    Route::post('area-big-mapper/suggestion-city-regency', 'Admin\AreaBigMapperController@suggestionCityRegency');

    Route::get('area-big-mapper/remove-landing/{landing_id}', 'Admin\AreaBigMapperController@removeAssociateLanding');
    Route::post('area-big-mapper/{id}/update-landing-apartment', ['as' => 'area-big-mapper.update-landing-apartment', 'uses' => 'Admin\AreaBigMapperController@updateAssociateLandingApartment']);
    Route::post('area-big-mapper/landing-suggestion-apartment', 'Admin\AreaBigMapperController@landingSuggestionApartment');
    Route::get('area-big-mapper/remove-landing-apartment/{landing_id}', 'Admin\AreaBigMapperController@removeAssociateLandingApartment');
    Route::post('area-big-mapper/{id}/update-landing-jobs', ['as' => 'area-big-mapper.update-landing-jobs', 'uses' => 'Admin\AreaBigMapperController@updateAssociateLandingJobs']);
    Route::post('area-big-mapper/landing-suggestion-jobs', 'Admin\AreaBigMapperController@landingSuggestionJobs');
    Route::get('area-big-mapper/remove-landing-jobs/{landing_id}', 'Admin\AreaBigMapperController@removeAssociateLandingJobs');

    Route::resource('bank', Admin\BankController::class);
    Route::get('bank/destroy/{id}', 'Admin\BankController@destroy');
    Route::resource('premium/faq', Admin\PremiumFaqController::class);
    Route::resource('premium', Admin\PremiumController::class);

    Route::get('request/request/new', 'Admin\RequestController@newRequest');
    Route::post('request/request/new', 'Admin\RequestController@newPostRequest');
    Route::get('request/request/package', 'Admin\RequestController@packageList');
    Route::get('request/extend/{id}', 'Admin\RequestController@premiumExtendStatus');
    Route::resource('request', Admin\RequestController::class);
    Route::get('request/balance', 'Admin\RequestController@balance');
    Route::post('request/confirm/{id}', 'Admin\RequestController@confirmationuser');
    Route::get('request/confirmation/{id}', 'Admin\RequestController@confirmation');
    Route::get('request/balance/{id}', 'Admin\RequestController@destroyBalance');
    Route::get('request/deleted/{id}', 'Admin\RequestController@deleted');
    Route::get('request/kost/{id}', 'Admin\RequestController@kostOwner');
    Route::post('request/kost/{id}', 'Admin\RequestController@requestUpdate');
    Route::get('request/kost/{id}/allocation', 'Admin\RequestController@allocationFix');
    Route::get('request/kost/delete/{id}', 'Admin\RequestController@deleteRoomPromotion');

    Route::resource('premium-plus', Admin\Premium\PremiumPlusUserController::class);
    Route::get('premium-plus/{id}/invoice', 'Admin\Premium\PremiumPlusInvoiceController@index');
    Route::get('premium-cashback', 'Admin\Premium\PremiumCashbackController@index')->name('premium-cashback.index');

    Route::get('paket/recommendation/{id}', 'Admin\PaketController@recommendationChange');
    Route::resource('paket', Admin\PaketController::class);
    Route::get('paket/destroy/{id}', 'Admin\PaketController@destroy');

    Route::resource('click-pricing', Admin\ClickPricingController::class);
    Route::get('click-pricing/{id}/delete', ['as' => 'click-pricing.delete', 'uses' => 'Admin\ClickPricingController@destroy']);

    Route::resource('promo', Admin\PromoController::class);
    Route::get('promo/verify/{id}', 'Admin\PromoController@verifyWithButton');
    Route::get('promo/unverify/{id}', 'Admin\PromoController@unverifyPromo');
    Route::get('promo/delete/{id}', 'Admin\PromoController@delete');

    Route::resource('review', Admin\ReviewController::class);
    Route::get('review/deletephoto/{id}', 'Admin\ReviewController@deletePhoto');
    Route::resource('label', Admin\LabelController::class);
    Route::get('label/{id}/delete', ['as' => 'label.destroy', 'uses' => 'Admin\LabelController@destroy']);

    Route::resource('event', Admin\EventController::class);
    Route::get('event/publish/{id}', array('as' => 'event.publish', 'uses' => 'Admin\EventController@publish'));
    Route::get('event/search', array('as' => 'event.search', 'uses' => 'Admin\EventController@index'));
    Route::get('event/delete/{id}', array('as' => 'event.delete', 'uses' => 'Admin\EventController@destroy'));

    Route::group(['prefix' => 'notification-list'], function () {
        Route::get('category/order', 'Admin\Notification\CategoryController@orderList');
        Route::post('category/order', 'Admin\Notification\CategoryController@setOrder');
        Route::get('category/{id}/delete', 'Admin\Notification\CategoryController@destroy');
        Route::resource('category', Admin\Notification\CategoryController::class);
    });

    Route::group(['prefix' => 'kost-level'], function () {
        Route::resource('level', Admin\Level\KostLevelController::class);
        Route::post('kost-list/{id}/removeFlag', 'Admin\Level\KostListController@removeFlag');
        Route::get('kost-list/{id}/history', 'Admin\Level\KostListController@history');
        Route::get('kost-list/upload', 'Admin\Level\KostListController@upload');
        Route::post('kost-list/processCsv', 'Admin\Level\KostListController@processCsv');
        Route::resource('kost-list', Admin\Level\KostListController::class);
        Route::resource('faq', Admin\Level\FaqController::class);

        Route::resource('room-level', Admin\Level\RoomLevelController::class);
        Route::get('room-list/{id}', 'Admin\Level\RoomListController@index')->name('room-list.index');
        Route::put('room-list/{id}/room/{roomId}', 'Admin\Level\RoomListController@update')->name('room-list.update');
        Route::get('room-list/{id}/room/{roomId}/edit', 'Admin\Level\RoomListController@edit')->name('room-list.edit');
        Route::get('room-list/{id}/room/{roomId}/history', 'Admin\Level\RoomListController@history')->name('room-list.history');
        Route::post('room-list/{id}/assignall', 'Admin\Level\RoomListController@assignAll')->name('room-list.assignall');
    });

    // Kost Value Management
    Route::group(['prefix' => 'kost-value'], function () {
        Route::get('/', 'Admin\KostValue\KostValueController@index')->name('kost-value.index');
        Route::get('add', 'Admin\KostValue\KostValueController@create')->name('kost-value.create');
        Route::post('add', 'Admin\KostValue\KostValueController@store')->name('kost-value.store');
        Route::get('edit/{id}', 'Admin\KostValue\KostValueController@edit')->name('kost-value.edit');
        Route::post('edit/{id}', 'Admin\KostValue\KostValueController@update')->name('kost-value.update');
        Route::post('remove', 'Admin\KostValue\KostValueController@remove')->name('kost-value.remove');
        Route::get('values', 'Admin\KostValue\KostValueController@getValues')->name('kost-value.values');
    });

    // Assign Benefit to kost
    Route::group(['prefix' => 'assign-kost-value'], function () {
        Route::get('/', 'Admin\AssignKostValue\AssignKostValueController@index')->name('assign-kost-value.index');
        Route::get('{id}/edit', 'Admin\AssignKostValue\AssignKostValueController@create')->name('assign-kost-value.create');
        Route::post('{id}/edit', 'Admin\AssignKostValue\AssignKostValueController@postAddBenefit')->name('assign-kost-value.postAddBenefit');
        Route::get('data', 'Admin\AssignKostValue\AssignKostValueController@getAllData')->name('assign-kost-value.all');
        Route::post('{id}/remove', 'Admin\AssignKostValue\AssignKostValueController@postRemoveBenefit')->name('assign-kost-value.postDeleteBenefit');
        Route::post('verify', 'Admin\AssignKostValue\AssignKostValueController@verify');
    });

    Route::group(['prefix' => 'property-package', 'as' => 'property-package.'], function () {
        Route::post('/', 'Admin\Level\PropertyPackageController@store')->name('store');

        Route::get('/create', 'Admin\Level\PropertyPackageController@createView')->name('.create');
        Route::put('/{propertyContract}', 'Admin\Level\PropertyPackageController@update')->name('update');

        Route::get('/index-list', 'Admin\Level\PropertyPackageController@indexList');
        Route::get('/dropdown-list', 'Admin\Level\PropertyPackageController@getLevels')->name('dropdown-list');
        Route::post('add-property', 'Admin\Level\PropertyPackageController@addProperty')->name('add-property');
        Route::post('/property/new','Admin\Level\PropertyPackageController@storeProperty');
        Route::get('/search-owner/{phoneNumber}', 'Admin\Level\PropertyPackageController@searchOwnerByPhone');

        Route::get('/{designerId}/room-list', 'Admin\Level\PropertyPackageController@getRoomLists')
            ->where('designerId', '[0-9]+')
            ->name('room-list');

        Route::get('/kost-list/owner/{owner}/property/{property}', 'Admin\Level\PropertyPackageController@kostListAssignToProperty')
            ->name('kost-list-assign-to-property');

        Route::get('/kost-list/property/{property}/{propertyContract}', 'Admin\Level\PropertyPackageController@kostListByProperty')->name('kost-list-by-property');
        Route::get('/detail/{propertyContract}', 'Admin\Level\PropertyPackageController@getPropertyPackageDetail');

        Route::get('/{ownerId}/property-list', 'Admin\Level\PropertyPackageController@getOwnersUnmappedProperties')->name('owner-property-list');

        Route::post('/toggle-assign', 'Admin\Level\PropertyPackageController@toggleAssignment')->name('toggle-assignment');

        Route::post('/{designerId}/{propertyContractId}/assign-all-rooms', 'Admin\Level\PropertyPackageController@assignAllRooms')
            ->where('designerId', '[0-9]+')
            ->name('assign-all-rooms');

        Route::post('/{designerId}/{propertyContractId}/unassign-all-rooms', 'Admin\Level\PropertyPackageController@unassignAllRooms')
            ->where('designerId', '[0-9]+')
            ->name('unassign-all-rooms');

        Route::put('/terminate/{propertyContract}', 'Admin\Level\PropertyPackageController@terminate');
        Route::put('/property/remove/{propertyContract}', 'Admin\Level\PropertyPackageController@removeProperty');
        Route::delete('/{propertyContract}', 'Admin\Level\PropertyPackageController@destroy');

        // VIEW
        Route::get('/', 'Admin\Level\PropertyPackageController@index');
        Route::get('/create/{phone_number?}', 'Admin\Level\PropertyPackageController@createView')->name('.create');
        Route::get('/update/{propertyContractId}', 'Admin\Level\PropertyPackageController@updateView')->name('.update');
        Route::get('/{ownerId}/{propertyId}/{propertyContractId}/assign', 'Admin\Level\PropertyPackageController@assignView')->name('.assign');
        Route::get('/{propertyContract}', 'Admin\Level\PropertyPackageController@detailView')->name('.detail');
    });

    Route::group(['prefix' => 'point', 'as' => 'point.'], function () {
        Route::resource('activity', Admin\Point\ActivityController::class);
        Route::resource('room-group', Admin\Point\OwnerRoomGroupController::class);

        Route::get('setting', 'Admin\Point\SettingController@index')->name('setting.index');
        Route::post('setting/tnc', 'Admin\Point\SettingController@tnc')->name('setting.tnc');
        Route::post('setting/{point}/{activity}', 'Admin\Point\SettingController@post')->name('setting.post');

        Route::get('expiry', 'Admin\Point\ExpiryController@index')->name('expiry.index');
        Route::post('expiry/change', 'Admin\Point\ExpiryController@change')->name('expiry.change');

        Route::get('blacklist', 'Admin\Point\BlacklistController@index')->name('blacklist.index');
        Route::get('blacklist/create', 'Admin\Point\BlacklistController@create')->name('blacklist.create');
        Route::post('blacklist/store', 'Admin\Point\BlacklistController@store')->name('blacklist.store');
        Route::get('blacklist/{id}/toggle', 'Admin\Point\BlacklistController@toggle')->name('blacklist.toggle');
        Route::get('blacklist/{id}/delete', 'Admin\Point\BlacklistController@delete')->name('blacklist.delete');

        Route::resource('segment', Admin\Point\SegmentController::class);

        // Point User
        Route::get('user/index', 'Admin\Point\UserPointController@index')->name('user.index');
        Route::put('user/{pointUser}/update-adjust-point', 'Admin\Point\UserPointController@updateAdjustPoint')->name('user.updateAdjustPoint');
        Route::put('user/update-bulk-adjust', 'Admin\Point\UserPointController@updateBulkAdjustPoint')->name('user.updateBulkAdjustPoint');
        Route::put('user/{pointUser}/update-blacklist', 'Admin\Point\UserPointController@updateBlacklist')->name('user.updateBlacklist');
        Route::put('user/update-bulk-blacklist', 'Admin\Point\UserPointController@updateBulkBlacklist')->name('user.updateBulkBlacklist');

        Route::get('history/{user}', 'Admin\Point\HistoryController@index')->name('history.index');
    });

    Route::group(['prefix' => 'reward-loyalty', 'as' => 'reward-loyalty.'], function () {
        Route::resource('type', Admin\RewardLoyalty\RewardTypeController::class);

        Route::get('list/{reward}/history', 'Admin\RewardLoyalty\RewardListController@rewardHistory')->name('list.history');
        Route::get('list/{reward}/redeem/{id}/history', 'Admin\RewardLoyalty\RewardListController@redeemHistory')->name('list.redeem.history');
        Route::put('list/{reward}/update-redeem-status/{id}', 'Admin\RewardLoyalty\RewardListController@updateRedeemStatus')->name('list.updateRedeemStatus');

        Route::group(['prefix' => 'list/{reward}/targeted-user', 'as' => 'list.targeted-user.'], function () {
            Route::put('remove', 'Admin\RewardLoyalty\RewardTargetedUserController@remove')->name('remove');
        });

        Route::resource('list', Admin\RewardLoyalty\RewardListController::class)->parameters([
            'list' => 'reward'
        ]);
    });

    // Polling
    Route::group(['prefix' => 'polling', 'as' => 'polling.'], function () {
        Route::resource('question', Admin\Polling\PollingQuestionController::class);
        Route::resource('option', Admin\Polling\PollingOptionController::class);
        Route::get('user/index', 'Admin\Polling\UserPollingController@index')->name('user.index');
    });
    Route::resource('polling', Admin\Polling\PollingController::class);

    Route::resource('promoted', Admin\PromotedController::class);
    Route::get('delete/{id}', ['as' => 'promoted.remove', 'uses' => 'Admin\PromotedController@destroy']);
    Route::post('promoted/search', 'Admin\PromotedController@searchName');
    Route::resource('recommendation', Admin\RecommendationController::class);

    // Route::get('owner/check-old-name', 'Admin\OwnerController@checkOldNameKos');
    // Route::group(['prefix' => 'owner'], function () {
    //     Route::get("popup/destroy/{id}", "Admin\Popup\PopupController@destroy");
    //     Route::resource('popup', 'Admin\Popup\PopupController');
    // });

    Route::get('igen', ['as' => 'igen', 'uses' => 'Admin\OwnerController@agenOwner']);

    Route::group(['prefix' => 'owner'], function () {
        // Route::resource('owner', 'Admin\OwnerController');
        Route::get('/', ['as' => 'owner.index', 'uses' => 'Admin\OwnerController@index']);
        Route::get('check-old-name', 'Admin\OwnerController@checkOldNameKos');
        Route::get("popup/destroy/{id}", "Admin\Popup\PopupController@destroy");
        Route::resource('popup', 'Admin\Popup\PopupController');
        Route::get('room', 'Admin\OwnerController@bookingRoom');
        Route::post('search', ['as' => 'owner.search', 'uses' => 'Admin\OwnerController@index']);
        Route::get('room/{phone}', 'Admin\OwnerController@kostOwner');
        Route::get('delete/{id}', ['as' => 'owner.destroy', 'uses' => 'Admin\OwnerController@destroy']);
        Route::get('verify/{id}', ['as' => 'owner.verify', 'uses' => 'Admin\OwnerController@verify']);
        Route::get('unverify/{id}', ['as' => 'owner.unverify', 'uses' => 'Admin\OwnerController@unverify']);
        Route::get('setting/{id}', ['as' => 'owner.setting', 'uses' => 'Admin\OwnerController@settingOwner']);
        Route::get('setting/{any}/{id}', ['as' => 'owner.setting', 'uses' => 'Admin\OwnerController@settingOwnerNotif']);
        Route::get('reject-remark/{id}', 'Admin\OwnerController@editRejectRemark')->name('owner.reject.edit');
        Route::post('reject-remark', 'Admin\OwnerController@updateRejectRemark')->name('owner.reject.update');
        Route::get('change/{id}', ['as' => 'owner.change', 'uses' => 'Admin\OwnerController@changeOwner']);
        Route::post('change/{id}', ['as' => 'owner.change', 'uses' => 'Admin\OwnerController@updateRoomOwner']);
        Route::get('agen/{id}', 'Admin\OwnerController@changeToOwnerPage');

        Route::group(['prefix' => 'data', 'as' => 'owner.data.'], function () {
            Route::group(['prefix' => 'kos', 'as' => 'kos.'], function () {
                Route::group(['prefix' => 'address','as' => 'address.'], function() {
                    Route::get('province', 'Api\Owner\Data\Address\ProvinceController@index')->name('list-province');
                    Route::get('city' , 'Api\Owner\Data\Address\CityController@index')->name('list-city');
                    Route::get('subdistrict', 'Api\Owner\Data\Address\SubdistrictController@index')->name('list-subdistrict');
                });
            });
        });
    });

    // Owner Loyalty feature ~ used on API v1.5.3
    Route::group(['prefix' => 'loyalty'], function () {
        Route::get('/', ['as' => 'loyalty', 'uses' => 'Admin\OwnerLoyaltyController@index']);
        Route::post('import', 'Admin\OwnerLoyaltyController@import');
        Route::post('update', 'Admin\OwnerLoyaltyController@update');
        Route::post('remove', 'Admin\OwnerLoyaltyController@remove');
        Route::get('download', 'Admin\OwnerLoyaltyController@download');
    });

    // Owner Testimonial feature ~ used on API v1.7.5
    // Ref doc: https://mamikos.atlassian.net/browse/BG-94
    Route::group(['prefix' => 'testimonial', 'as' => 'testimonial'], function () {
        Route::get('/', 'Admin\TestimonialController@index')->name('.index');
        Route::post('/', 'Admin\TestimonialController@store');
        Route::post('activate', 'Admin\TestimonialController@activate');
        Route::post('deactivate', 'Admin\TestimonialController@deactivate');
        Route::post('remove', 'Admin\TestimonialController@remove');

        Route::group(['prefix' => 'ajax', 'as' => 'ajax'], function () {
            Route::get('/owner-name', 'Admin\TestimonialController@getOwnerNameAjax');
            Route::get('/kost-name', 'Admin\TestimonialController@getKostNameAjax');
        });
    });

    // Mami-checker feature ~ Used on API v1.4.3
    Route::group(['prefix' => 'mami-checker', 'as' => 'mami-checker'], function () {
        Route::get('/', 'Admin\MamiCheckerController@index')->name('.index');
        Route::post('update', 'Admin\MamiCheckerController@update');
        Route::post('/profile', 'Admin\MamiCheckerController@storeProfile');
        Route::post('/remove', 'Admin\MamiCheckerController@removeProfile');
        Route::get('/all', 'Admin\MamiCheckerController@getAllCheckers');
        Route::post('/checker', 'Admin\MamiCheckerController@getCheckerData');
        Route::post('/track', 'Admin\MamiCheckerController@storeTracker');
        Route::post('/untrack', 'Admin\MamiCheckerController@removeTracker');
    });

    // Top-Owner feature ~ Used on API v1.2.1
    Route::resource('top-owner', 'Admin\TopOwnerController');
    Route::get('top-owner/change-status/{id}', ['as' => 'top-owner.change-status', 'uses' => 'Admin\TopOwnerController@changeStatus']);

    // Feature "Room Class" :: Used on API v1.2.2
    Route::resource('room-class', 'Admin\RoomClassController');
    Route::get('room-class/recalculate/{id}', ['as' => 'room-class.recalculate', 'uses' => 'Admin\RoomClassController@recalculateClass']);
    Route::get('room-class/reset/{id}', ['as' => 'room-class.reset', 'uses' => 'Admin\RoomClassController@resetClass']);

    // Hostile-Owner feature ~ Used on API v1.3.5
    Route::resource('hostile-owner', 'Admin\HostileOwnerController');
    Route::post('hostile-owner/update-reason/{id}', 'Admin\HostileOwnerController@updateHostileReason');

    Route::group(['prefix' => 'competitor', 'as' => 'competitor.'], function () {
        Route::resource('user', Admin\Competitor\CompetitorUserController::class);
        Route::resource('fake-phone', Admin\Competitor\FakePhoneController::class);
    });

    Route::get('mamipay-owner', ['as' => 'mamipay-owner.index', 'uses' => 'Admin\MamiPay\MamipayOwnerController@index']);
    Route::get('mamipay-owner/add', ['as' => 'mamipay-owner.add', 'uses' => 'Admin\MamiPay\MamipayOwnerController@add']);
    Route::post('mamipay-owner/add', ['as' => 'mamipay-owner.add-submit', 'uses' => 'Admin\MamiPay\MamipayOwnerController@addSubmit']);
    Route::get('mamipay-owner/approve/{id}', ['as' => 'mamipay-owner.approve', 'uses' => 'Admin\MamiPay\MamipayOwnerController@approve']);
    Route::get('mamipay-owner/reject/{id}', ['as' => 'mamipay-owner.reject', 'uses' => 'Admin\MamiPay\MamipayOwnerController@reject']);
    Route::get('mamipay-owner/search-city', ['as' => 'mamipay-owner.search-city', 'uses' => 'Admin\MamiPay\MamipayOwnerController@searchCity']);

    Route::resource('mamipay-invoice', 'Admin\MamiPay\MamipayInvoiceController');

    Route::get('chat/config', ['as' => 'chat.config', 'uses' => 'Admin\ChatController@chatConfig']);
    Route::post('chat/config', ['as' => 'chat.config.update', 'uses' => 'Admin\ChatController@chatConfigUpdate']);

    Route::get('mitula/config', ['as' => 'mitula.config', 'uses' => 'Admin\MitulaController@config']);
    Route::post('mitula/config', ['as' => 'mitula.config.update', 'uses' => 'Admin\MitulaController@configUpdate']);
    Route::get('mitula/preview', 'Admin\MitulaController@preview');

    Route::get('verification', 'Admin\VerificationCodeController@index')->name('verification.index');

    Route::group(['prefix' => 'agent'], function () {
        Route::get('images/{phone}', 'Admin\AgentController@getListImages');
    });

    Route::group(['prefix' => 'patrick'], function () {
        Route::group(['prefix' => 'house_property'], function () {
            Route::get('list', 'Admin\Patrick\HousePropertyController@getList');
            Route::post('import', 'Admin\Patrick\HousePropertyController@import');
            Route::get('edit/{id}', 'Admin\Patrick\HousePropertyController@getEdit');
            Route::post('live/{id}', 'Admin\Patrick\HousePropertyController@storeHouseProperty');
        });
        Route::group(['prefix' => 'apartemen'], function () {
            Route::get('list', 'Admin\Patrick\ApartemenController@getListDummyApartemen');
            Route::get('google', 'Admin\Patrick\ApartemenController@fromGoogle');
            Route::group(['prefix' => 'project'], function () {
                Route::get('add', 'Admin\Patrick\ProjectApartmentController@add');
                Route::post('add', 'Admin\Patrick\ProjectApartmentController@store');
            });
            Route::get('search', 'Admin\Patrick\ApartemenController@searchFromGoogle');
            Route::post('live', 'Admin\Patrick\ApartemenController@storeUnit');
            Route::get('edit/{id}', 'Admin\Patrick\ApartemenController@edit');
            Route::get('check-name', 'Admin\Patrick\ApartemenController@getNameProject');
            Route::get('check-phone', 'Admin\Patrick\ApartemenController@checkPhoneKostTemp');
        });
        Route::group(['prefix' => 'vacancy'], function () {
            Route::get('company', 'Admin\Patrick\VacancyController@getCompanyList');
            Route::get('import', 'Admin\Patrick\VacancyController@importPage');
            Route::post('import', 'Admin\Patrick\VacancyController@posyImport');
            Route::get('company/edit/{id}', 'Admin\Patrick\VacancyController@companyEdit');
            Route::post('company/edit/{id}', 'Admin\Patrick\VacancyController@companyUpdate');
            Route::get('list', 'Admin\Patrick\VacancyController@getListDummyVacancy');
            Route::get('edit/{id}', 'Admin\Patrick\VacancyController@editDummyVacancy');
            Route::post('edit/{id}', 'Admin\Patrick\VacancyController@stores');
            Route::get('check-from-no-hp', 'Admin\Patrick\VacancyController@checkFromNoHpVacancy');
            Route::get('del/{id}', 'Admin\Patrick\VacancyController@deleteVacancy');
        });
        Route::get('check', 'Admin\Patrick\ProjectController@checkData');
        Route::get('check-from-no-hp', 'Admin\Patrick\ProjectController@checkFromNoHp');
        Route::get('list', 'Admin\Patrick\ProjectController@index');
        Route::get('check-room', 'Admin\Patrick\ProjectController@checkRoom');
        Route::get('google', 'Admin\Patrick\ProjectController@fromGoogle');
        Route::get('fromgoogle', 'Admin\Patrick\ProjectController@getFromGoogle');
        Route::get('editfromgoogle', 'Admin\Patrick\ProjectController@editFromGoogle');
        Route::post('lives', 'Admin\Patrick\ProjectController@livesKost');
        Route::get('edit/{id}', 'Admin\Patrick\ProjectController@edit');
        Route::get('check/{id}', 'Admin\Patrick\ProjectController@checkingEdit');
        Route::post('live/{id}', 'Admin\Patrick\ProjectController@LiveData');
    });

    Route::resource('landing-content', 'Admin\LandingContentController');
    Route::get('landing-content/{id}/delete', 'Admin\LandingContentController@delete')->name('landing-content.delete');
    Route::get('landing-content/redirect/{id}', 'Admin\LandingContentController@redirectlanding');
    Route::post('landing-content/redirect/{id}', 'Admin\LandingContentController@redirectlandingpost');

    Route::post('landing-content/file/upload', ['as' => 'landing-content.file.upload', 'uses' => 'Admin\LandingContentController@fileUpload']);

    Route::resource('landing-suggestion', 'Admin\LandingSuggestion\LandingSuggestionBlockController');
    Route::get('landing-suggestion/{id}/delete', 'Admin\LandingSuggestion\LandingSuggestionBlockController@delete')->name('landing-suggestion.delete');

    Route::resource('download-exam', 'Admin\DownloadExamController');
    Route::get('download-exam/redirect/{id}', 'Admin\DownloadExamController@redirectPage');
    Route::post('download-exam/redirect/{id}', 'Admin\DownloadExamController@storeRedirectData');
    Route::get('download-exam/{id}/delete', 'Admin\DownloadExamController@delete')->name('download-exam.delete');
    Route::post('download-exam/file/upload', ['as' => 'download-exam.file.upload', 'uses' => 'Admin\DownloadExamController@fileUpload']);

    Route::group(['prefix' => 'booking', 'as' => 'booking'], function () {
        Route::get('rent-count/{rentCountType}', 'Admin\BookingUserController@getRentCount');

        Route::get('rooms', 'Admin\BookingDesignerController@index')->name('.rooms');
        Route::get('rooms/type/show/{designer_id}', 'Admin\BookingDesignerController@type')->name('.rooms.type');
        Route::get('rooms/type/create/{designer_id}', 'Admin\BookingDesignerController@create')->name('.rooms.type.create');
        Route::post('rooms/type/store', 'Admin\BookingDesignerController@store')->name('.rooms.type.store');
        Route::get('rooms/type/edit/{designer_id}/{room_type_id}', 'Admin\BookingDesignerController@edit')->name('.rooms.type.edit');
        Route::post('rooms/type/update/{designer_id}/{room_type_id}', 'Admin\BookingDesignerController@update')->name('.rooms.type.update');
        Route::get('rooms/type/calendar/{designer_id}/{room_type_id}', 'Admin\BookingDesignerController@calendar')->name('.rooms.type.calendar');

        Route::group(['prefix' => 'landing'], function () {
            Route::get('specific', 'Admin\BookingLandingController@specific');
            Route::post('specific', 'Admin\BookingLandingController@storeLandingSpecific');
            Route::get('specific/{id}', 'Admin\BookingLandingController@editSpecific');
            Route::post('specific/{id}', 'Admin\BookingLandingController@updateSpecific');
            Route::get('parent', 'Admin\BookingLandingController@getMainLanding');
            Route::post('parent', 'Admin\BookingLandingController@updateMainLanding');
            Route::post('activate', 'Admin\BookingLandingController@activate');
            Route::post('update', 'Admin\BookingLandingController@update');
            Route::post('set', 'Admin\BookingLandingController@setAsDefault');
            Route::get('ajax', 'Admin\BookingLandingController@getAjaxData')->name('.landing.ajax');
            Route::post('geocode', 'Admin\BookingLandingController@getCityGeocode')->name('.landing.geocode');
            Route::post('remove', 'Admin\BookingLandingController@remove');
            Route::post('sort', 'Admin\BookingLandingController@sortOrder');
            Route::get('/', 'Admin\BookingLandingController@index')->name('.landing.index');
            Route::post('/', 'Admin\BookingLandingController@store');
        });

        Route::prefix('users')->name('.users')->group(function () {
            Route::get('/', 'Admin\BookingUserController@index');
            Route::get('download', 'Admin\BookingUserController@download')->name('.download');
            Route::get('{id}/extend', 'Admin\BookingUserController@extendWaitingTime')->name('.extend');
            Route::get('{id}/preview-confirm', 'Admin\BookingUserController@previewConfirm')->name('.preview-confirm');
            Route::post('{id}/confirm', 'Admin\BookingUserController@confirmAvailability')->name('.confirm');
            Route::post('{id}/cancel', 'Admin\BookingUserController@cancelBooking')->name('.cancel');
            Route::post('{id}/reject', 'Admin\BookingUserController@rejectBooking')->name('.reject');
            Route::post('{booking_user_id}/check-in', 'Admin\BookingUserController@checkIn')->name('.check-in');
            Route::post('{bookingUser}/note', 'Admin\BookingUserController@updateNote')->name('.note');
            Route::post('{bookingUser}/transfer-permission', 'Admin\BookingUserController@setTransferPermissionStatus')->name('.transfer-permission');

            Route::prefix('count')->name('.count')->group(function () {
                Route::get('in-process', 'Admin\BookingUserController@countInProcess')->name('.in-process');
                Route::get('on-going', 'Admin\BookingUserController@countOnGoing')->name('.on-going');
                Route::get('finished', 'Admin\BookingUserController@countFinished')->name('.finished');
            });
        });

        Route::get('monitor', 'Admin\BookingUserController@getViewMonitor')->name('.monitor');

        Route::resource('/users', 'Admin\BookingUserController')->names(
            [
                'index' => '.users',
                'show' => '.users.show',
                'create' => '.users.create',
                'store' => '.users.store',
                'edit' => '.users.edit',
                'update' => '.users.update'
            ]
        );

        Route::group(['prefix' => 'owner', 'as' => '.owner'], function () {
            Route::group(['prefix' => 'request'], function () {
                Route::get('add', 'Admin\Booking\OwnerBookingController@create');
                Route::post('add', 'Admin\Booking\OwnerBookingController@store');
                Route::get('/', 'Admin\Booking\OwnerBookingController@listBookingRequest');
                Route::get('{id}/registered/{by}', 'Admin\Booking\OwnerBookingController@markRegisteredBy');
            });
            Route::get('reject-reason/{roomId}', 'Admin\Booking\OwnerBookingController@listRejectReason');
            Route::post('reject/{roomId}', 'Admin\Booking\OwnerBookingController@requestReject');
            Route::get('approve-bulk/{id}', 'Admin\Booking\OwnerBookingController@requestBulkApprove');
            Route::post('confirm/{id}', 'Admin\Booking\OwnerBookingController@requestConfirm');
            Route::get('deactivate/{id}', 'Admin\Booking\OwnerBookingController@deactivateRoom');

            Route::get('instant-booking', 'Admin\Booking\OwnerBookingController@instantBooking');
            Route::get('instant-booking/create', 'Admin\Booking\OwnerBookingController@createInstantBooking')->name('.instant-booking.create');
            Route::post('instant-booking/store', 'Admin\Booking\OwnerBookingController@storeInstantBooking');
            Route::get('instant-booking/edit/{id}', 'Admin\Booking\OwnerBookingController@editInstantBooking')->where('id', '[0-9]+')->name('.instant-booking.edit');
            Route::post('instant-booking/update/{id}', 'Admin\Booking\OwnerBookingController@updateInstantBooking')->where('id', '[0-9]+');
            Route::post('instant-booking/upload/process', 'Admin\Booking\OwnerBookingController@processUploadInstantBooking');
            Route::get('instant-booking/{id}/status/{status}', 'Admin\Booking\OwnerBookingController@setStatusInstantBooking');
        });

        Route::get('payments', 'Admin\BookingPaymentController@index')->name('.payments');
        Route::get('payments/verify/{payment_id}', 'Admin\BookingPaymentController@verify')->name('.payments.verify');

        Route::get('discount', 'Admin\DiscountManagementController@index')->name('.discount');
        Route::post('discount', 'Admin\DiscountManagementController@store');
        Route::post('discount/verify', 'Admin\DiscountManagementController@verifyRoom');
        Route::post('discount/remove', 'Admin\DiscountManagementController@remove');
        Route::post('discount/remove-all', 'Admin\DiscountManagementController@removeAll');
        // Deprecated, Ref BG-1250 Remove the action for “Non Aktifkan Diskon” & “Aktifkan Diskon”
        // Route::post('discount/status', 'Admin\DiscountManagementController@updateStatus');

        /** booking acceptance rate */
        Route::group(['prefix' => 'acceptance-rate', 'as' => 'booking-acceptance-rate'], function () {
            Route::get('/', 'Admin\Booking\AcceptanceRate\BookingAcceptanceRateController@index')->name('.index');
            Route::get('data', 'Admin\Booking\AcceptanceRate\BookingAcceptanceRateController@getAllData')->name('.all');
            Route::get('update/{status}', 'Admin\Booking\AcceptanceRate\BookingAcceptanceRateController@updateStatus')->name('.update')->where('status', '[0-9]+');;
            Route::get('recalculate', 'Admin\Booking\AcceptanceRate\BookingAcceptanceRateController@recalculate')->name('.all');
        });

        // booking reject reason
        Route::resource('reject-reason', 'Admin\Booking\RejectReason\BookingRejectReasonController')->names(
            [
                'index' => '.reject-reason',
                'create' => '.reject-reason.create',
                'destroy' => '.reject-reason.destroy',
                'store' => '.reject-reason.store',
                'edit' => '.reject-reason.edit',
                'update' => '.reject-reason.update'
            ]
        );

        Route::get('admin-bse', 'Admin\BookingUserController@getAdminBSE');
    });

    Route::get('contract/invoice/{contractId}', 'Admin\Contract\ContractController@showInvoices');
    Route::get('contract/room-allotment/{contractId}', 'Admin\Contract\ContractController@listRoomAllotment');
    Route::post('contract/room-allotment/{contractId}', 'Admin\Contract\ContractController@storeRoomAllotment');
    Route::resource('contract', 'Admin\Contract\ContractController')->only('index');
    Route::resource('dbet-link', 'Admin\Contract\OwnerTenantController');


    Route::get('reward', ['as' => 'reward.index', 'uses' => 'Admin\RewardController@index']);
    Route::get('reward/{id}/sent', ['as' => 'reward.sent', 'uses' => 'Admin\RewardController@sent']);
    Route::get('reward/{id}/unsent', ['as' => 'reward.unsent', 'uses' => 'Admin\RewardController@unsent']);

    Route::resource('voucher', 'Admin\VoucherProgramController');
    Route::get('voucher/{id}/run', 'Admin\VoucherProgramController@runNotifier')->name('voucher.run');
    Route::get('voucher/{id}/pause', 'Admin\VoucherProgramController@pauseNotifier')->name('voucher.pause');
    Route::get('voucher/{id}/stop', 'Admin\VoucherProgramController@stopNotifier')->name('voucher.stop');
    Route::get('voucher/{id}/test-email', 'Admin\VoucherProgramController@testEmail')->name('voucher.testnotif');
    Route::post('voucher/{id}/test-email', 'Admin\VoucherProgramController@testEmailSubmit')->name('voucher.testemailsubmit');
    Route::post('voucher/{id}/test-sms', 'Admin\VoucherProgramController@testSMSSubmit')->name('voucher.testsmssubmit');

    Route::group(['prefix' => 'voucher/recipient', 'as' => 'voucher.recipient'], function () {
        Route::get('/{id}', 'Admin\VoucherProgramController@recipient')->name('.index');
        Route::get('/{id}/create', 'Admin\VoucherProgramController@recipientCreate')->name('.create');
        Route::post('/{id}/store', 'Admin\VoucherProgramController@recipientStore')->name('.store');
        Route::get('/{id}/edit/{recipient_id}', 'Admin\VoucherProgramController@recipientEdit')->name('.edit');
        Route::post('/{id}/update/{recipient_id}', 'Admin\VoucherProgramController@recipientUpdate')->name('.update');
        Route::get('/{id}/statusimport', 'Admin\VoucherProgramController@recipientStatusImport')->name('.statusimport');
        Route::get('/{id}/import', 'Admin\VoucherProgramController@recipientImport')->name('.import');
        Route::post('/{id}/import', 'Admin\VoucherProgramController@recipientImportSubmit')->name('.importsubmit');
        Route::get('/{id}/sendnotif/{recipient_id}', 'Admin\VoucherProgramController@recipientSendNotif')->name('.sendnotif');
    });

    Route::resource('notification-survey', 'Admin\NotificationSurveyController');
    Route::get('notification-survey/{id}/run', 'Admin\NotificationSurveyController@runNotifier')->name('notification-survey.run');
    Route::get('notification-survey/{id}/pause', 'Admin\NotificationSurveyController@pauseNotifier')->name('notification-survey.pause');
    Route::get('notification-survey/{id}/stop', 'Admin\NotificationSurveyController@stopNotifier')->name('notification-survey.stop');
    Route::get('voucher/{id}/test-notification', 'Admin\NotificationSurveyController@testNotification')->name('notification-survey.testnotif');
    Route::post('voucher/{id}/test-notification', 'Admin\NotificationSurveyController@testNotificationSubmit')->name('notification-survey.testnotifsubmit');

    Route::group(['prefix' => 'notification-survey/recipient', 'as' => 'notification-survey.recipient'], function () {
        Route::get('/{id}', 'Admin\NotificationSurveyController@recipient')->name('.index');
        Route::get('/{id}/create', 'Admin\NotificationSurveyController@recipientCreate')->name('.create');
        Route::post('/{id}/store', 'Admin\NotificationSurveyController@recipientStore')->name('.store');
        Route::get('/{id}/edit/{recipient_id}', 'Admin\NotificationSurveyController@recipientEdit')->name('.edit');
        Route::post('/{id}/update/{recipient_id}', 'Admin\NotificationSurveyController@recipientUpdate')->name('.update');
        Route::get('/{id}/sendnotif/{recipient_id}', 'Admin\NotificationSurveyController@recipientSendNotif')->name('.sendnotif');
    });

    Route::group(['prefix' => 'marketplace', 'as' => 'marketplace'], function () {
        Route::get('/product', 'Admin\MarketplaceProductController@index')->name('.product');
        Route::get('/product/{id}/edit', 'Admin\MarketplaceProductController@edit')->name('.product.edit');
        Route::post('/product/{id}/update', 'Admin\MarketplaceProductController@update')->name('.product.update');
        Route::get('/product/{id}/delete', 'Admin\MarketplaceProductController@delete')->name('.product.delete');
    });

    Route::get('reward', ['as' => 'reward.index', 'uses' => 'Admin\RewardController@index']);
    Route::get('reward/{id}/sent', ['as' => 'reward.sent', 'uses' => 'Admin\RewardController@sent']);
    Route::get('reward/{id}/unsent', ['as' => 'reward.unsent', 'uses' => 'Admin\RewardController@unsent']);
    Route::post('log-resolved', 'Admin\CSResolvedController@logResolved');
    Route::get('agent/verified/{phone}', 'Admin\AgentController@verifiedRoom');

    Route::get('role/flush', 'Admin\RolePermission\RoleController@flushRoleAndPermission');
    Route::resource('role', Admin\RolePermission\RoleController::class);
    Route::get('role/{id}/users', ['as' => 'role.users', 'uses' => 'Admin\RolePermission\RoleController@roleUsers']);
    Route::get('role/{id}/attach', ['as' => 'role.users.attach', 'uses' => 'Admin\RolePermission\RoleController@attachRole']);
    Route::get('role/{id}/create', ['as' => 'role.users.create', 'uses' => 'Admin\RolePermission\RoleController@createRole']);
    Route::post('role/{id}/create', ['as' => 'role.users.create', 'uses' => 'Admin\RolePermission\RoleController@createRoleStore']);
    Route::post('role/{id}/attach', ['as' => 'role.users.attach-post', 'uses' => 'Admin\RolePermission\RoleController@attachRolePost']);
    Route::post('role/users/suggestion', ['as' => 'role.users.suggestion', 'uses' => 'Admin\RolePermission\RoleController@getUserForRole']);
    Route::get('role/{id}/detach/{user_id}', ['as' => 'role.users.detach', 'uses' => 'Admin\RolePermission\RoleController@detachRole']);
    Route::get('role/{id}/change-password/{user_id}', ['as' => 'role.users.change-password', 'uses' => 'Admin\RolePermission\RoleController@changePasswordUser']);
    Route::post('role/{id}/change-password/{user_id}', ['as' => 'role.users.change-password', 'uses' => 'Admin\RolePermission\RoleController@updatePasswordUser']);
    Route::get('roles/{id}/permission', ['as' => 'role.permissions', 'uses' => 'Admin\RolePermission\RoleController@permissionRoles']);
    Route::get('roles/{id}/attach-permission', ['as' => 'role.permissions.attach', 'uses' => 'Admin\RolePermission\RoleController@attachPermission']);
    Route::post('roles/{id}/attach-permission', ['as' => 'role.permissions.attach-post', 'uses' => 'Admin\RolePermission\RoleController@attachPermissionPost']);
    Route::get('role/{id}/detach-permission/{permission_id}', ['as' => 'role.permissions.detach', 'uses' => 'Admin\RolePermission\RoleController@detachPermission']);
    Route::get('role/{id}/duplicate', ['as' => 'role.permissions.duplicate', 'uses' => 'Admin\RolePermission\RoleController@duplicate']);
    Route::post('role/{id}/duplicate', ['as' => 'role.permissions.duplicate-post', 'uses' => 'Admin\RolePermission\RoleController@duplicatePost']);

    Route::resource('permission', Admin\RolePermission\PermissionController::class);

    Route::get('account/change-password', ['as' => 'account.change-password', 'uses' => 'Admin\UserController@changePassword']);
    Route::post('account/change-password', ['as' => 'account.update-password', 'uses' => 'Admin\UserController@updatePassword']);
    Route::get('account/verification-identity-card', ['as' => 'account.verification-identity-card', 'uses' => 'Admin\UserIdentityCardController@getVerifyIdentityCard']);
    Route::get('account/verification-identity-card/detail/{id}', ['as' => 'account.verification-identity-card.detail', 'uses' => 'Admin\UserIdentityCardController@getIdentityCardDetail']);
    Route::post('account/verification-identity-card/{id}', ['as' => 'account.verification-identity-card.verify', 'uses' => 'Admin\UserIdentityCardController@verificationIdentityCard']);

    Route::group(['prefix' => 'account', 'as' => 'account', 'middleware' => ['permission:user-account']], function() {
        Route::get('/data', 'Admin\UserAccount\UserAccountController@indexData');

        Route::get('/users', 'Admin\UserAccount\UserAccountController@indexView');

        Route::delete('{id}', 'Admin\UserAccount\UserAccountController@destroy')->name('.destroy');
    });

    Route::resource('list-creator', Admin\LandingListController::class);
    Route::get('list-creator/redirect/{id}', 'Admin\LandingListController@redirectPage');
    Route::post('list-creator/redirect/{id}', 'Admin\LandingListController@storeRedirectData');
    Route::get('list-creator/add/job', 'Admin\LandingListController@createJob')->name('list-creator.create-job');
    Route::post('list-creator/add/job', 'Admin\LandingListController@storeJob')->name('list-creator.store-job');
    Route::get('list-creator/add/specific', 'Admin\LandingListController@createSpecific')->name('list-creator.create-specific');
    Route::post('list-creator/add/specific', 'Admin\LandingListController@storeSpecific')->name('list-creator.store-specific');
    Route::get('list-creator/edit/specific/{id}', 'Admin\LandingListController@editSpecific')->name('list-creator.edit-specific');
    Route::post('list-creator/add/specific/{id}', 'Admin\LandingListController@updateSpecific')->name('list-creator.update-specific');
    Route::get('list-creator/add/kost', 'Admin\LandingListController@createKost')->name('list-creator.create-kost');
    Route::post('list-creator/add/kost', 'Admin\LandingListController@storeKost')->name('list-creator.store-kost');
    Route::get('list-creator/edit/kost/{id}', 'Admin\LandingListController@editKost')->name('list-creator.edit-kost');
    Route::post('list-creator/update/kost/{id}', 'Admin\LandingListController@updateKost')->name('list-creator.update-kost');
    Route::get('list-creator/add/apartment', 'Admin\LandingListController@createApartment')->name('list-creator.create-apartment');
    Route::post('list-creator/add/apartment', 'Admin\LandingListController@storeApartment')->name('list-creator.store-apartment');
    Route::get('list-creator/edit/apartment/{id}', 'Admin\LandingListController@editApartment')->name('list-creator.edit-apartment');
    Route::post('list-creator/update/apartment/{id}', 'Admin\LandingListController@updateApartment')->name('list-creator.update-apartment');
    Route::get('list-creator/delete/{id}', 'Admin\LandingListController@destroy')->name('list-creator.delete');

    Route::get('list-creator/add/company', 'Admin\LandingListController@createCompany')->name('list-creator.create-company');
    Route::post('list-creator/add/company', 'Admin\LandingListController@storeCompany')->name('list-creator.store-company');
    Route::get('list-creator/edit/company/{id}', 'Admin\LandingListController@editCompany')->name('list-creator.edit-company');
    Route::post('list-creator/update/company/{id}', 'Admin\LandingListController@updateCompany')->name('list-creator.update-company');

    Route::resource('aggregator', Admin\AggregatorController::class);
    Route::get('aggregator-partner/delete/{id}', array('as' => 'aggregator.delete', 'uses' => 'Admin\AggregatorController@destroy'));

    Route::group(['prefix' => 'forum', 'as' => 'forum'], function () {
        Route::get('/user/senior', 'Admin\Forum\ForumUserController@seniorList')->name('.user.senior-list');
        Route::get('/user/senior/add', 'Admin\Forum\ForumUserController@createSenior')->name('.user.senior-create');
        Route::post('/user/senior/add', 'Admin\Forum\ForumUserController@storeSenior')->name('.user.senior-store');
        Route::get('/user/senior/official/{user_id}', 'Admin\Forum\ForumUserController@makeSeniorOfficial')->name('.user.make-senior-official');
        Route::get('/user/senior/unofficial/{user_id}', 'Admin\Forum\ForumUserController@makeSeniorUnofficial')->name('.user.make-senior-unofficial');
        Route::get('/user/senior/points/{user_id}', 'Admin\Forum\ForumUserController@points')->name('.user.points');
        Route::get('/user/senior/redeem/{user_id}', 'Admin\Forum\ForumUserController@redeem')->name('.user.redeem');
        Route::post('/user/senior/points/{user_id}', 'Admin\Forum\ForumUserController@storeForumPoint')->name('.user.redeem-post');
    });
    Route::resource('forum-category', Admin\Forum\ForumCategoryController::class);
    Route::resource('forum-thread', Admin\Forum\ForumThreadController::class);
    Route::get('forum-thread/activate/{thread_id}', ['as' => 'forum-thread.activate', 'uses' => 'Admin\Forum\ForumThreadController@activate']);
    Route::get('forum-thread/deactivate/{thread_id}', ['as' => 'forum-thread.deactivate', 'uses' => 'Admin\Forum\ForumThreadController@deactivate']);
    Route::get('forum-answer/list/{thread_id}', ['as' => 'forum-answer.list', 'uses' => 'Admin\Forum\ForumAnswerController@list']);
    Route::get('forum-answer/activate/{answer_id}', ['as' => 'forum-answer.activate', 'uses' => 'Admin\Forum\ForumAnswerController@activate']);
    Route::get('forum-answer/deactivate/{answer_id}', ['as' => 'forum-answer.deactivate', 'uses' => 'Admin\Forum\ForumAnswerController@deactivate']);
    Route::resource('forum-report', Admin\Forum\ForumReportController::class);

    Route::get('photobooth/subscribers/{id}', 'Admin\PhotoboothEventController@subscribers')->name('photobooth.subscribers');
    Route::resource('photobooth', Admin\PhotoboothEventController::class);

    Route::get('zenziva-inbox', 'Admin\ZenzivaInboxController@index')->name('zenziva-inbox.index');
    Route::post('zenziva-inbox/convert', 'Admin\ZenzivaInboxController@convertToDB')->name('zenziva-inbox.convert');

    Route::group(['prefix' => 'villa'], function () {
    });

    Route::group(['prefix' => 'booking-request-wizard', 'as' => 'booking_request_wizard'], function () {
        Route::post('validate-kost', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@checkKost']);
        Route::get('validate-user/{userId}', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@checkUser']);
        Route::get('validate-user-by-phone-number/{phoneNumber}', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@checkUserByPhoneNumber']);
        Route::get('get-room/{userId}', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@getRoom']);
        Route::post('create-request', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@createBookingRequest']);
        Route::post('add-mamipay-owner', ['uses' => 'Admin\Booking\Wizard\RequestWizardController@addMamipayOwner']);
    });

    Route::group(['prefix' => 'booking/users/wizard', 'as' => 'booking_accept_wizard'], function () {
        Route::get('accept/{booking}', ['uses' => 'Admin\Booking\Wizard\AcceptWizardController@detailBooking'])->name('.accept');
        Route::post('accept/{booking}', ['uses' => 'Admin\Booking\Wizard\AcceptWizardController@acceptBooking']);
    });

    Route::group(['prefix' => 'consultant', 'as' => 'consultant'], function () {
        Route::get('/', 'Admin\Consultant\ConsultantController@index')->name('.index');
        Route::post('/store', 'Admin\Consultant\ConsultantController@store')->name('.store');
        Route::post('/update/{id}', 'Admin\Consultant\ConsultantController@update')->name('.update');
        Route::get('/delete/{id}', 'Admin\Consultant\ConsultantController@destroy')->name('.delete');
        Route::get('/room-chat/{id}', 'Admin\Consultant\ConsultantController@getRoomChat')->name('.room-chat');

        Route::group(['prefix' => 'mapping'], function () {
            Route::get('/', 'Admin\Consultant\ConsultantMappingController@index')->name('.mapping.index');
            Route::post('/store', 'Admin\Consultant\ConsultantMappingController@store')->name('.mapping.store');
            Route::post('/update/{id}', 'Admin\Consultant\ConsultantMappingController@update')->name('.mapping.update');
            Route::delete('/delete/{id}', 'Admin\Consultant\ConsultantMappingController@destroy')->name('.mapping.destroy');
        });

        Route::group(['prefix' => 'potential-tenant', 'as' => '.potential-tenant'], function () {
            Route::get('/', 'Admin\Consultant\PotentialTenantController@index')->name('.index');
            Route::get('/data', 'Admin\Consultant\PotentialTenantController@data');
        });

        Route::group(
            [
                'prefix' => 'room-mapping',
                'as' => '.room-mapping',
                'middleware' => ['permission:access-consultant']
            ],
            function () {
                Route::get('/', 'Admin\Consultant\ConsultantRoomMappingController@index')->name('.index');
                Route::get('/data', 'Admin\Consultant\ConsultantRoomMappingController@indexData');
                Route::prefix('/detail')->group(function () {
                    Route::get('/{consultant}/{areaCity}', 'Admin\Consultant\ConsultantRoomMappingController@detail')->name('.detail');
                    Route::get('/edit/{consultant}/{areaCity}', 'Admin\Consultant\ConsultantRoomMappingController@edit');
                    Route::get('/data/{consultant}/{areaCity}', 'Admin\Consultant\ConsultantRoomMappingController@detailData');
                });
                Route::get('/consultant', 'Admin\Consultant\ConsultantRoomMappingController@getConsultant');
                Route::get('/add', 'Admin\Consultant\ConsultantRoomMappingController@add');
                Route::get('/add/data/{consultantId}/{areaCity}', 'Admin\Consultant\ConsultantRoomMappingController@addData');

                Route::middleware(['permission:access-mapping-system'])->group(function () {
                    Route::post('/add/{consultantId}', 'Admin\Consultant\ConsultantRoomMappingController@store');
                    Route::post('reassign', 'Admin\Consultant\ConsultantRoomMappingController@reassign');
                    Route::post('/delete', 'Admin\Consultant\ConsultantRoomMappingController@destroy');
                    Route::post('/area', 'Admin\Consultant\ConsultantRoomMappingController@destroyByArea');
                });
            }
        );

        Route::group(
            [
                'prefix' => 'activity-management',
                'as' => '.activity-management',
                'middleware' => ['permission:access-consultant']
            ],
            function () {
                Route::get('/', 'Admin\Consultant\ActivityManagementController@index')->name('.index');
                Route::get('/funnel', 'Admin\Consultant\ActivityManagementController@funnelData');
                Route::get('/funnel/{funnelId}', 'Admin\Consultant\ActivityManagementController@show');
                Route::get('/staging/{funnelId}', 'Admin\Consultant\ActivityManagementController@stagingData');
                Route::get('/staging/detail/{stageId}', 'Admin\Consultant\ActivityManagementController@stageDetail')->name('.stage.detail');
                Route::get('/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@formsData');

                Route::middleware(['permission:access-activity-management'])->group(function () {
                    Route::post('/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@formAdd');
                    Route::patch('/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@formUpdate');
                });

                Route::get('/create/funnel', 'Admin\Consultant\ActivityManagementController@createFunnel')->name('.create.funnel');
                Route::get('/edit/funnel/{funnelId}', 'Admin\Consultant\ActivityManagementController@editFunnel')->name('.edit.funnel');
                Route::get('/create/staging/{funnelId}', 'Admin\Consultant\ActivityManagementController@createStaging')->name('.create.staging');

                Route::middleware(['permission:access-activity-management'])->group(function () {
                    Route::post('/store/funnel', 'Admin\Consultant\ActivityManagementController@storeFunnel');
                    Route::post('/update/funnel/{funnelId}', 'Admin\Consultant\ActivityManagementController@updateFunnel');
                });

                Route::post('/store/staging/{funnelId}', 'Admin\Consultant\ActivityManagementController@storeStaging');
                Route::post('/update/staging/{stageId}', 'Admin\Consultant\ActivityManagementController@updateStaging');
                Route::post('/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@formAdd');
                Route::patch('/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@formUpdate');

                Route::get('/create/forms/{stageId}', 'Admin\Consultant\ActivityManagementController@createForms');
                Route::post('/delete/staging/{stageId}', 'Admin\Consultant\ActivityManagementController@deleteStaging');

                Route::middleware(['permission:access-delete-activity-management'])->group(function () {
                    Route::post('/delete/staging/{stageId}', 'Admin\Consultant\ActivityManagementController@deleteStaging');
                    Route::post('/delete/funnel/{funnelId}', 'Admin\Consultant\ActivityManagementController@deleteFunnel');
                    Route::post('/delete/forms/{stageId}/{formId}', 'Admin\Consultant\ActivityManagementController@deleteFormInput');
                });
            }
        );

        Route::group(['prefix' => 'potential-owner', 'as' => '.potential-owner'], function () {
            Route::get('/', 'Admin\Consultant\PotentialOwnerController@index')->name('.index');
            Route::get('/{ownerId}/property/data', 'Admin\Consultant\PotentialPropertyController@getPropertyByOwner');
            # Route::get('/add', 'Admin\Consultant\PotentialOwnerController@add');
            Route::get('/data', 'Admin\Consultant\PotentialOwnerController@indexData');
            Route::get('/detail/{owner}', 'Admin\Consultant\PotentialOwnerController@detail');
            Route::get('/edit/{owner}', 'Admin\Consultant\PotentialOwnerController@edit');
            Route::get('/property/{potentialProperty}', 'Admin\Consultant\PotentialPropertyController@detail');

            Route::post('/{ownerId}/property', 'Admin\Consultant\PotentialPropertyController@store');

            Route::middleware(['permission:access-potential-owner'])->group(function () {
                Route::post('/bulk', 'Admin\Consultant\PotentialOwnerController@bulkUpload');
                # Route::post('/store', 'Admin\Consultant\PotentialOwnerController@store');
                Route::post('/update/{owner}', 'Admin\Consultant\PotentialOwnerController@update')->where('id', '[0-9]+');
                Route::delete('/{id}', 'Admin\Consultant\PotentialOwnerController@destroy');
            });

            Route::patch('/{propertyId}/property', 'Admin\Consultant\PotentialPropertyController@update');
            Route::middleware(['permission:delete-potential-property'])->group(function () {
                Route::delete('/{propertyId}/property', 'Admin\Consultant\PotentialPropertyController@destroy');
            });
        });

        Route::group(['prefix' => 'sales-motion', 'as' => '.sales-motion.'], function () {
            Route::get('/', 'Admin\Consultant\SalesMotionController@index')->name('index');

            Route::get('/{id}/consultant', 'Admin\Consultant\SalesMotionController@listConsultant')
                ->name('consultant-list')
                ->where('id', '[0-9]+');

            Route::get('/{id}/assigned/consultant', 'Admin\Consultant\SalesMotionController@listAssignedConsultant')
                ->name('assigned-consultant-list')
                ->where('id', '[0-9]+');

            Route::get('/detail/{id}', 'Admin\Consultant\SalesMotionController@detail')->name('detail');
            Route::get('/add', 'Admin\Consultant\SalesMotionController@add')->name('add');
            Route::get('/edit/{id}', 'Admin\Consultant\SalesMotionController@edit')->name('edit');

            Route::get('/{id}/assignee', 'Admin\Consultant\SalesMotionController@assigneeUpdate')->name('assignee')
                ->where('id', '[0-9]+');

            Route::post('/{salesMotion}/assign-all', 'Admin\Consultant\SalesMotionController@assignAll')->name('assign-all')
                ->where('id', '[0-9]+');

            Route::get('/data', 'Admin\Consultant\SalesMotionController@indexData');

            Route::post('/activate/{salesMotion}', 'Admin\Consultant\SalesMotionController@activate');
            Route::post('/deactivate/{salesMotion}', 'Admin\Consultant\SalesMotionController@deactivate');
            Route::post('/store', 'Admin\Consultant\SalesMotionController@store');
            Route::put('/update/{salesMotionId}', 'Admin\Consultant\SalesMotionController@update');

            Route::put('/{salesMotion}/sync/assignee', 'Admin\Consultant\SalesMotionController@syncAssignee');
            Route::get('/detail/{id}/data', 'Admin\Consultant\SalesMotionController@detailData');
        });
    });

    Route::group(['prefix' => 'cs-admin', 'as' => 'cs-admin'], function () {
        Route::get('/', 'Admin\CsAdmin\CsAdminController@index')->name('.index');
        Route::post('/store', 'Admin\CsAdmin\CsAdminController@store')->name('.store');
        Route::post('/update/{id}', 'Admin\CsAdmin\CsAdminController@update')->name('.update');
        Route::get('/delete/{id}', 'Admin\CsAdmin\CsAdminController@destroy')->name('.delete');
        Route::get('/room-chat/{id}', 'Admin\CsAdmin\CsAdminController@getRoomChat')->name('.room-chat');
    });

    Route::group(['prefix' => 'career', 'as' => 'career.', 'middleware' => ['auth', 'permission:access-career']], function () {
        Route::resource('location', 'Admin\Career\LocationController')->only(['index', 'store', 'update', 'destroy']);
        Route::resource('position', 'Admin\Career\PositionController')->only(['index', 'store', 'update', 'destroy']);
        Route::get('vacancy/toggle/{vacancy}', 'Admin\Career\VacancyController@toggle')->name('vacancy.toggle');
        Route::resource('vacancy', 'Admin\Career\VacancyController');
    });

    // Feature Slider Component
    // Ref Task: https://mamikos.atlassian.net/browse/BG-336
    Route::group(['prefix' => 'slider', 'as' => 'slider'], function () {
        Route::get('/', 'Admin\SliderController@index')->name('.index');
        Route::post('/', 'Admin\SliderController@store')->name('.store');
        Route::get('data', 'Admin\SliderController@getTableData')->name('.data');
        Route::post('verify', 'Admin\SliderController@verify')->name('.verify');
        Route::post('activate', 'Admin\SliderController@activate')->name('.activate');
        Route::post('deactivate', 'Admin\SliderController@deactivate')->name('.deactivate');
        Route::post('remove', 'Admin\SliderController@remove')->name('.remove');
        Route::group(['prefix' => 'page', 'as' => 'page'], function () {
            Route::post('/', 'Admin\SliderController@storePage')->name('.store');
            Route::post('remove', 'Admin\SliderController@removePage')->name('.remove');
            Route::post('activate', 'Admin\SliderController@activatePage')->name('.activate');
            Route::post('deactivate', 'Admin\SliderController@deactivatePage')->name('.deactivate');
            Route::post('setCover', 'Admin\SliderController@setCoverPage')->name('.order');
            Route::post('order', 'Admin\SliderController@orderPage')->name('.order');
        });
    });

    Route::resource('whitelist-features', 'Admin\WhitelistFeatureController');

    // Feature FLash Sale
    // Ref: https://mamikos.atlassian.net/browse/BG-1255
    Route::group(
        [
            'prefix' => 'flash-sale',
            'as' => 'flash-sale'
        ],
        function () {
            Route::get('/', 'Admin\FlashSale\FlashSaleController@index')->name('.index');
            Route::post('/', 'Admin\FlashSale\FlashSaleController@storeData')->name('.store');
            Route::get('data', 'Admin\FlashSale\FlashSaleController@getAllData')->name('.all');
            Route::post('verify', 'Admin\FlashSale\FlashSaleController@verifyName')->name('.verify');
            Route::post('recalculate', 'Admin\FlashSale\FlashSaleController@recalculateRoomScore')->name('.recalculate');
            Route::post('remove', 'Admin\FlashSale\FlashSaleController@removeData')->name('.remove');
            Route::get('data/{id}', 'Admin\FlashSale\FlashSaleController@getData')->name('.get');
            Route::get('update/{id}', 'Admin\FlashSale\FlashSaleController@update')->name('.update');
            Route::post('activate', 'Admin\FlashSale\FlashSaleController@activate')->name('.activate');
            Route::post('deactivate', 'Admin\FlashSale\FlashSaleController@deactivate')->name('.deactivate');
            Route::get('landings', 'Admin\FlashSale\FlashSaleController@getLandings')->name('.landings');
            Route::get('history', 'Admin\FlashSale\FlashSaleController@getHistory')->name('.history');
        }
    );

    /*
     * Admin page for LPL Scoring
     * Ref Task: https://mamikos.atlassian.net/browse/BG-1656
     */
    Route::group(
        [
            'prefix' => 'lpl-score',
            'as' => 'lpl-score'
        ],
        function () {
            Route::get('/', 'Admin\LplScore\LplScoreController@index')->name('.index');
            Route::get('data', 'Admin\LplScore\LplScoreController@getAllData')->name('.all');
            Route::get('update/{id}', 'Admin\LplScore\LplScoreController@getData')->name('.get');
            Route::post('update', 'Admin\LplScore\LplScoreController@updateData')->name('.update');
            Route::post('reorder', 'Admin\LplScore\LplScoreController@reorderData')->name('.reorder');
            Route::get('history', 'Admin\LplScore\LplScoreController@getHistory')->name('.history');
            Route::get('calculator', 'Admin\LplScore\LplScoreController@calculator')->name('.calculator');
        }
    );

    Route::get('home-replicate', 'Admin\Replicate\HomeController@index');

    Route::group([
        'prefix' => 'gold-plus',
        'as' => 'gold-plus'
    ], function () {
        Route::get('/', 'Admin\GoldPlus\GoldPlusController@index');
        Route::post('regenerate-report', 'Admin\GoldPlus\GoldPlusController@regenerateReport');
        Route::get('history/{id}', 'Admin\GoldPlus\GoldPlusController@getHistory');

        Route::group(
            [
                'prefix' => 'package',
                'as' => '.package',
                'middleware' => ['permission:access-gold-plus-package']
            ],
            function () {
                Route::get('/', 'Admin\GoldPlus\GoldPlusController@listPackage')->name('.index');
                Route::get('edit/{id}', 'Admin\GoldPlus\GoldPlusController@editPackage')->name('.edit');
                Route::put('edit/{id}', 'Admin\GoldPlus\GoldPlusController@updatePackage')->name('.update');
            }
        );
    });

    //Sanjunipero Admin
    Route::group(
        [
            'prefix' => 'sanjunipero',
            'as' => 'sanjunipero'
        ],
        function () {
            Route::group(
                [
                    'prefix' => 'parent',
                    'as' => '.parent'
                ],
                function () {
                    Route::get('/', 'Admin\Sanjunipero\DynamicLandingParentController@index')->name('.index');
                    Route::get('create', 'Admin\Sanjunipero\DynamicLandingParentController@create')->name('.create');
                    Route::post('save', 'Admin\Sanjunipero\DynamicLandingParentController@save')->name('.save');
                    Route::get('edit/{id}', 'Admin\Sanjunipero\DynamicLandingParentController@edit')->name('.edit');
                    Route::post('update/{id}', 'Admin\Sanjunipero\DynamicLandingParentController@update')->name('.update');
                    Route::post('activate/{id}', 'Admin\Sanjunipero\DynamicLandingParentController@activate')->name('.activate');
                    Route::post('deactivate/{id}', 'Admin\Sanjunipero\DynamicLandingParentController@deactivate')->name('.deactivate');
                }
            );

            Route::group(
                [
                    'prefix' => 'child',
                    'as' => '.child'
                ],
                function () {
                    Route::get('/', 'Admin\Sanjunipero\DynamicLandingChildController@index')->name('.index');
                    Route::get('create', 'Admin\Sanjunipero\DynamicLandingChildController@create')->name('.create');
                    Route::post('save', 'Admin\Sanjunipero\DynamicLandingChildController@save')->name('.save');
                    Route::get('edit/{id}', 'Admin\Sanjunipero\DynamicLandingChildController@edit')->name('.edit');
                    Route::post('update/{id}', 'Admin\Sanjunipero\DynamicLandingChildController@update')->name('.update');
                    Route::post('activate/{id}', 'Admin\Sanjunipero\DynamicLandingChildController@activate')->name('.activate');
                    Route::post('deactivate/{id}', 'Admin\Sanjunipero\DynamicLandingChildController@deactivate')->name('.deactivate');
                }
            );
        }
    );

    // Testing Related Dashboard
    Route::group(['prefix' => 'testing'], function () {
        Route::get('/testers', 'Admin\Testing\TestersController@index');
        Route::post('/testers/{id}/recycle-phone', 'Admin\Testing\TestersController@recyclePhone');
        Route::post('/testers/{id}/mark-as-tester', 'Admin\Testing\TestersController@markAsTester');
    });

    Route::get('abtest', 'Admin\AbTestController@index');
    Route::get('abtest/detail/{id}', 'Admin\AbTestController@index');
    Route::post('abtest', 'Admin\AbTestController@control');

    /*
     * Geocode administration panel
     * Ref task: https://mamikos.atlassian.net/browse/BG-3727
     */
    Route::group(
        [
            'prefix' => 'geocode',
            'as' => 'geocode'
        ],
        function () {
            Route::get('/', 'Admin\Geocode\IndexController@index')->name('.index');
            Route::get('mapping', 'Admin\Geocode\MappingController@index')->name('.mapping');
            Route::get('mapping/{id}', 'Admin\Geocode\MappingController@getBoundary')->name('.boundary');
        }
    );

    // Admin API
    Route::group(['prefix' => 'api', 'as' => 'api.'], function () {
        Route::group(['prefix' => 'kost', 'as' => 'kost.'], function () {
            Route::get('/reject-reason', 'Admin\Api\Kost\RejectReasonController@index')->name('reject-reason');
            Route::get('/reject-preview', 'Admin\Api\Kost\RejectReasonController@preview')->name('reject-preview');
            Route::group(['prefix' => 'reject', 'as' => 'reject.'], function () {
                Route::get('/{id}', 'Admin\Api\Kost\RejectController@getHistory')->name('history');
                Route::post('/{id}', 'Admin\Api\Kost\RejectController@reject')->name('save');
            });
        });
    });
});
