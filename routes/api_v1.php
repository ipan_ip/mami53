<?php

Route::group(['prefix' => 'midtrans', 'middleware' => 'api.auth'], function() {
    Route::post('/snaptoken', 'Api\Midtrans\SnapController@postToken');
    Route::post('/snapfinish', 'Api\Midtrans\SnapController@finish');

    Route::group(['prefix' => 'gp4'], function() {
        Route::post('token', 'Api\Midtrans\SnapController@createTokenGp');
        Route::post('finish', 'Api\Midtrans\SnapController@finish');
    });
});

// Device and User Registration
Route::group(['prefix' => 'auth'], function () {
    Route::post('device/register', 'Api\AuthController@postDeviceRegister'); // pertama kali buka apps / registerkan app dr hp
    Route::post('social', 'Api\AuthController@postSocial');
    Route::post('logout', 'Api\AuthController@postLogout');

    Route::group(['prefix' => 'tenant'], function () {
        Route::post('register', 'Api\Auth\TenantController@register');
        Route::post('login', 'Api\Auth\TenantController@login');

        Route::group(['prefix' => 'verification'], function () {
            Route::post('request', 'Api\Auth\TenantController@requestVerification');
            Route::post('check', 'Api\Auth\TenantController@verificationCheck');
        });
    });

    // Room owner Registration and Auth
    Route::group(['prefix' => 'owner'], function ()
    {
        Route::post('phone/verify','Api\AuthController@postVerifyOwner');
        Route::post('register','Api\AuthController@postRegisterOwner');
        Route::post('login','Api\AuthController@postLoginOwner');
        Route::put('password','Api\AuthController@putPassword');
        Route::post('storepassword', 'Api\AuthController@postPassword');
        Route::post('register/auto', 'Api\AuthController@autoRegisterOwner');
        Route::post('register/verified', 'Api\AuthController@registerOwner');
    });

    // Verification Code Authentication
    Route::group(['prefix' => 'code'], function () {
        Route::post('request', 'Api\Auth\VerificationCodeController@request');
        Route::post('check', 'Api\Auth\VerificationCodeController@check');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::post('forgot-password', 'Api\Auth\ForgotPasswordController@passwordReset'); // password reset
    });
});

// Forgot Password
Route::group(['prefix' => 'forgot-password'], function() {
    Route::post('/check-account', 'Api\ForgotPasswordController@checkAccount');
    Route::post('/verification-code', 'Api\ForgotPasswordController@checkVerificationCode');
    Route::post('/new-password', 'Api\ForgotPasswordController@resetPassword');
});

// App Information
Route::group(['prefix' => 'app'], function()
{
    Route::get('version', 'Api\AppController@getVersion')->middleware('api.auth:anyone');
    Route::post('campaign', 'Api\AppController@postCampaign');
});

// Notif to new app
Route::group(['prefix' => 'squarepants'], function() {
    Route::post('register', 'Api\Squarepants\AppController@agentRegister');
    Route::get('list', 'Admin\Squarepants\NotifyController@getListData');
});

// App for agent
Route::group(['prefix' => 'giant'], function() {
    Route::post('verification', 'Api\Giant\RegisterController@postVerification')->middleware('api.giant');
    Route::post('login', 'Api\Giant\RegisterController@postLogin')->middleware('api.giant');
    Route::get('list', 'Api\Giant\RegisterController@listData')->middleware('api.giant');
    Route::post('checkin', 'Api\Giant\DataController@postCheckin')->middleware('api.giant');
    Route::post('data', 'Api\Giant\DataController@postData')->middleware('api.giant');
    Route::post('media', 'Api\Giant\DataController@postMediaGiant')->middleware('api.giant');
    Route::post('review', 'Api\Giant\DataController@addMasterReview')->middleware('api.giant');
    Route::get('edited', 'Api\Giant\RegisterController@getEdited')->middleware('api.giant');
    Route::group(['prefix' => 'edit', 'middleware' => 'api.giant'], function() {
        Route::get('{id}', 'Api\Giant\DataController@getEditData');
    });
    Route::group(["prefix" => 'delete', 'middleware' => 'api.giant'], function() {
        Route::post('photo/{id}', 'Api\Giant\UpdateController@deletePhoto');
    });
    Route::group(["prefix" => 'update', 'middleware' => 'api.giant'], function() {
        Route::post('checkin', 'Api\Giant\UpdateController@updateCheckin');
        Route::post('review/{id}', 'Api\Giant\UpdateController@updateReview');
    });
    Route::group(['prefix' => 'photos', 'middleware' => 'api.giant'], function() {
        Route::get('{id}', 'Api\Giant\DataController@getListPhoto');
    });
});

Route::group(['prefix' => 'survey', 'middleware' => 'api.auth:anyone'], function() {
    Route::post('/', 'Api\SurveyController@userSurvey');
});

// Stories is another name of kost / room.
Route::group(['prefix' => 'stories'], function()
{
    Route::group(['prefix' => 'register', 'middleware' => 'api.auth:user'], function() {
        Route::post('upload', 'Api\Room\DocumentController@upload');
        Route::post('/', 'Api\Room\InputController@register');
        Route::group(['prefix' => '{id}'], function() {
            Route::get('type/{typeId}/delete', 'Api\Room\InputController@deleteRoomType');
            Route::get('/', 'Api\Room\InputController@roomStepRegister');
        });
        Route::group(['prefix' => 'facility'], function() {
			Route::get('setting/{id}', 'Api\Room\RoomFacilityController@getSetting');
			Route::get('type/setting/{id}', 'Api\Room\RoomFacilityController@getTypeSetting');
			Route::get('type/tags/{id}', 'Api\Room\RoomFacilityController@getTypeTags');
		});
    });
    Route::get('filters', 'Api\TagController@getFacilitiesFilter');
    Route::post('filters/count', 'Api\RoomFilterController@countFilteredRooms');
    Route::get('{id}/recommendation', 'Api\RoomController@getRecommendationRooms');
    Route::get('{id}/reviews', 'Api\ReviewController@getKosReviews')->middleware('api.auth:device');
    Route::get('{id}/rules', 'Api\RoomController@getKosRules');
    Route::post('free-search', 'Api\RoomController@freeSearch')->middleware('api.auth:anyone');
    Route::post('input-referral', 'Api\PropertyInputController@inputKostReferral')->middleware('api.auth:user');
    Route::post('cluster', 'Api\RoomController@listCluster')->middleware('api.auth:anyone');
    Route::post('list', 'Api\RoomController@listSearch')->middleware('api.auth:anyone');
    Route::post('suggestion', 'Api\RoomController@listFromZero')->middleware('api.auth:anyone');
    Route::post('search_alarm', 'Api\SubscribeController@subscribeFilter')->middleware('api.auth');
    Route::get('love', 'Api\RoomController@listFavorite')->middleware('api.auth:user');
    Route::get('survey', 'Api\RoomController@listSurvey')->middleware('api.auth:user');
    Route::get('telp', 'Api\RoomController@listTelp')->middleware('api.auth:user');
    Route::get('view', 'Api\RoomController@listVisit')->middleware('api.auth:device');
    Route::get('call', 'Api\RoomController@listCall')->middleware('api.auth:device');
    Route::get('chatboot', 'Api\RoomController@getChat');
    Route::get('benefit/{id}', 'Api\Room\BenefitController@index');
    Route::group(['prefix' => 'chat'], function() {
        Route::get('list', 'Api\Chat\ChatController@list');
        Route::post('tnc-tracker', 'Api\Chat\ChatTncTrackerController@store')->middleware('api.auth:user');
    });

    Route::post('addreview','Api\ReviewController@postReview')->middleware('api.auth:user');
    Route::post('booking','Api\BookingController@postBooking')->middleware('api.auth:user');
    Route::post('updatereview','Api\ReviewController@updateReview')->middleware('api.auth:user');

    Route::post('addsurvey', 'Api\SurveyController@postSurvey')->middleware('api.auth:user');
    Route::post('replyreview', 'Api\ReviewController@replyReview')->middleware('api.auth:user');
    Route::post('updateurvey', 'Api\SurveyController@updateSurvey');

    Route::group(['prefix' => 'review', 'middleware' => 'api.auth:device'], function() {
         Route::post('add', 'Api\ReviewController@addNewReview');
         Route::post('reply', 'Api\ReviewController@replyThread');
         Route::get('reply', 'Api\ReviewController@listRepltThreadApi');
         Route::get('{id}', 'Api\ReviewController@getList');
    });

    Route::get('{id}', 'Api\RoomController@show')->middleware('api.auth:device');
    Route::post('{id}/love', 'Api\RoomController@postFavorite')->middleware('api.auth:user');
    Route::post('{id}/view', 'Api\RoomController@postView')->middleware('api.auth:device');
    Route::post('{id}/call', 'Api\RoomController@postCall')->middleware('api.auth:device');
    Route::post('{id}/telp', 'Api\RoomController@postTelp')->middleware('api.auth:device');
    Route::post('{id}/telp/finish', 'Api\RoomController@postTelpFinish')->middleware('api.auth:device');

    Route::get('{id}/phoneowner', 'Api\RoomController@getPhoneOwner');
    Route::get('{id}/photos', 'Api\RoomController@getCardsBySongId');
    Route::get('{id}/gallery', 'Api\Card\CardController@getCardsWithCategory');

    Route::get('{id}/survey', 'Api\SurveyController@getSurvey')->middleware('api.auth:user');

    Route::get('{id}/getreview','Api\ReviewController@getReview')->middleware('api.auth:device');;

    Route::post('{id}/call/reply', 'Api\RoomController@postReply')->middleware('api.auth:device');

    Route::post('{id}/sms', 'Api\RoomController@postSms');
    //Route::get('{id}/roomreview','Api\ReviewController@getRoomReview');
    Route::get('location/{id}', 'Api\RoomController@getRoomLocation');

    // use flag term to send report about unmatched data
    Route::get('flag/type', 'Api\ReportController@reportType');
    Route::post('{id}/flag', 'Api\ReportController@postReport')->middleware('api.auth:user');

    Route::post('save_json_id', 'Api\RoomController@postSubmitKost');

    Route::group(['prefix' => 'house_property', 'middleware' => 'api.auth:device'], function() {
        Route::post('list', 'Api\HouseProperty\HousePropertyController@listSearch');
        Route::post('cluster', 'Api\HouseProperty\HousePropertyController@postCluster');
        Route::post('cluster/list', 'Api\HouseProperty\HousePropertyController@postClusterList');
        Route::post('{id}/call', 'Api\HouseProperty\HousePropertyController@postCall')->middleware('api.auth:user');
        Route::post('{id}/chat/reply', 'Api\HouseProperty\HousePropertyController@postChatReply')->middleware('api.auth:user');
        Route::get('{type}/{song_id}', 'Api\HouseProperty\HousePropertyController@detail');
    });

    Route::group(['prefix'=>'booking'], function() {
        Route::get('type/{room_id}', 'Api\Booking\BookingDesignerController@getType')->middleware('api.auth:device');
        Route::post('schedule', 'Api\Booking\BookingDesignerController@getSchedule')->middleware('api.auth:device');
        Route::post('search', 'Api\Booking\BookingDesignerController@searchAvailable')->middleware('api.auth:device');
        Route::post('price/calculate/{room_id}/{type_id}', 'Api\Booking\BookingUserController@calculate')->middleware('api.auth:device');
        Route::post('book/{room_id}/{type_id}', 'Api\Booking\BookingUserController@book')->middleware('api.auth:user');
    });
    Route::post('input', 'Api\PropertyInputController@inputKost')->middleware('api.auth:device');
    Route::post('input-general', 'Api\PropertyInputController@inputKostGeneral')->middleware('api.auth:device');
    Route::get('input/tags/{type}', 'Api\PropertyInputController@getTagsGroup');
    Route::get('input/as-option', 'Api\PropertyInputController@getInputAs');

    // Facility detail page :: used on API v1.7.2
	Route::get('facility/{id}', 'Api\RoomFacilityController@getDetails');

	// Facility detail page for Room Types:: used on API v1.7.3
	Route::get('facility/{id}/{typeId}', 'Api\RoomFacilityController@getDetailsForTypes');

    Route::group(['prefix' => 'update'], function() {
        Route::post('name/{id}', 'Api\RoomUpdaterController@updateNameType')->middleware('api.auth:user');
        Route::post('price/{id}', 'Api\RoomUpdaterController@updatePriceRoom')->middleware('api.auth:user');
        Route::post('size/{id}', 'Api\RoomUpdaterController@updateRoomSize')->middleware('api.auth:user');
        Route::post('photo/{id}', 'Api\RoomUpdaterController@updatePhoto')->middleware('api.auth:user');
        Route::post('bathroom/{id}', 'Api\RoomUpdaterController@updateBathroom')->middleware('api.auth:user');
        Route::post('facroom/{id}', 'Api\RoomUpdaterController@updateFacRoom')->middleware('api.auth:user');
        Route::post('facshare/{id}', 'Api\RoomUpdaterController@updateFacShare')->middleware('api.auth:user');
        Route::post('parking/{id}', 'Api\RoomUpdaterController@updateParking')->middleware('api.auth:user');
        Route::post('facnear/{id}', 'Api\RoomUpdaterController@updateFacNear')->middleware('api.auth:user');
        Route::post('otherinformation/{id}', 'Api\RoomUpdaterController@updateOtherInformation')->middleware('api.auth:user');
    });

    Route::group(['prefix' => 'edit'], function() {
        Route::get('name/{id}', 'Api\RoomUpdaterController@getNameType')->middleware('api.auth:user');
        Route::get('price/{id}', 'Api\RoomUpdaterController@getPriceRoom')->middleware('api.auth:user');
        Route::get('size/{id}', 'Api\RoomUpdaterController@getRoomSize')->middleware('api.auth:user');
        Route::get('otherinformation/{id}', 'Api\RoomUpdaterController@getOtherInformation')->middleware('api.auth:user');
        Route::get('facnear/{id}', 'Api\RoomUpdaterController@getFacNear')->middleware('api.auth:user');
        Route::get('parking/{id}', 'Api\RoomUpdaterController@getParking')->middleware('api.auth:user');
        Route::get('facshare/{id}', 'Api\RoomUpdaterController@getFacShare')->middleware('api.auth:user');
        Route::get('facroom/{id}', 'Api\RoomUpdaterController@getFacRoom')->middleware('api.auth:user');
        Route::get('bathroom/{id}', 'Api\RoomUpdaterController@getBathroom')->middleware('api.auth:user');
    });

    // Room meta for chat feature
    // Ref task: https://mamikos.atlassian.net/browse/BG-524
    Route::get('meta/{id}', 'Api\RoomMetaController@getRoomMeta')->middleware('api.auth:device');

    Route::get('compare/{id}', 'Api\RoomController@compareNearbyRoom')->middleware('api.auth:device');
    Route::get('nearby/{id}', 'Api\RoomController@getNearbyBySongId')->middleware('api.auth:device');
});

//Kost Level
Route::group(['prefix' => 'kost-level', 'middleware' => 'api.auth:device'], function() {
    Route::get('info', 'Api\KostLevelController@index');
    Route::post('{kostId}/approve', 'Api\KostLevelController@approve');
    Route::post('{kostId}/reject', 'Api\KostLevelController@reject');
    Route::post('{kostId}/ack', 'Api\KostLevelController@ack');
});

Route::group(['prefix' => 'room'], function()
{
    Route::group(['prefix' => 'goldplus'], function() {
        Route::get('/', 'Api\GoldplusController@getListKostsGoldplus')->middleware('api.auth:user');
        Route::get('filters', 'Api\GoldplusController@getGoldplusFilterOptions');
        Route::get('widget', 'Api\GoldPlus\WidgetController@getWidget')->middleware('api.auth:user');
        Route::get('widget-room', 'Api\GoldPlus\WidgetController@getWidgetRoomCount')->middleware('api.auth:user');
        Route::get('list-kost', 'Api\GoldPlus\GoldPlusAcquisitionController@getListKostsGoldplus')->middleware('api.auth:user');
        Route::get('list-kost/filters', 'Api\GoldPlus\GoldPlusAcquisitionController@getGoldplusFilterOptions')->middleware('api.auth:user');
    });

    Route::post('list/cluster', 'Api\RoomController@detailCluster');
    Route::get('{slug}', 'Api\RoomController@showBySlug');

    // get Room Type Number
    Route::get('type/{roomTypeId}/number', 'Api\RoomController@getRoomTypeNumber');
});

Route::group(['prefix'=>'apartment'], function() {
    Route::post('suggestion/name', 'Api\ApartmentProjectController@suggestions');
    Route::post('input', 'Api\PropertyInputController@inputApartment')->middleware('api.auth:user');

    Route::post('project/list/{project_id}/{project_code}', 'Api\ApartmentController@listApartmentByProject');

    Route::get('{id}', 'Api\ApartmentController@show')->middleware('api.auth:device');
    Route::get('unit/{apartmentSlug}/{slug}', 'Api\ApartmentController@showUnit');

    Route::get('landing/{slug}', 'Api\LandingApartmentController@showFilter');
    Route::get('project/landing/{citySlug}/{slug}', 'Api\LandingApartmentController@showProjectFilter');
});

Route::group(['prefix' => 'premium'], function()
{
    Route::get('bank', 'Api\PremiumController@getListBank');
    Route::get('package', 'Api\PremiumController@getPremiumPackage')->middleware('api.auth:user');
    Route::get('balance', 'Api\PremiumController@getPremiumBalance')->middleware('api.auth:user');
    Route::post('confirm', 'Api\PremiumController@postConfirm')->middleware('api.auth:user');
    Route::get('invoice', 'Api\PremiumController@getInvoice')->middleware('api.auth:user');
    Route::get('faq', 'Api\PremiumFaqController@getPremiumFaq');
    Route::group(['prefix' => 'allocation', 'middleware' => 'api.auth:user'], function() {
        Route::get('daily/setting', 'Api\Premium\PremiumAllocationController@settingStatus');
        Route::get('{id}/deactivate', 'Api\Premium\PremiumAllocationController@deactivateRoomAllocation');
        Route::post('{id}', 'Api\Premium\PremiumAllocationController@allocation');
    });
    Route::group(['prefix' => 'ads', 'middleware' => 'api.auth:user'], function() {
        Route::get('favorite', 'Api\PremiumController@mostFavorite');
        Route::get('statistic/summary', 'Api\PremiumController@getRoomsStatsisticSummary');
        Route::get('statistic', 'Api\PremiumController@getRoomsStatistic');

        Route::get('statistic/{id}/summary', 'Api\PremiumController@getRoomStatsisticSummary');
        Route::get('statistic/{id}', 'Api\PremiumController@getRoomStatistic');
    });
    Route::group(['prefix' => 'plus', 'middleware' => 'api.auth:user'], function() {
        Route::get('invoice/{id}', 'Api\Premium\PremiumPlusInvoiceController@detailInvoice');
    });
    Route::post('package/order', 'Api\Premium\OrderPremiumPackageController@buyPremiumPackage')->middleware('api.auth:user');
});

// Tags & Areas
Route::get('facility/{type?}', 'Api\TagController@getFacilities');
Route::get('tags', 'Api\TagController@index');
Route::get('area', 'Api\AreaController@getArea');
Route::get('area/campus', 'Api\AreaController@getCampus');
Route::get('area/mrt', 'Api\AreaController@getMrt');
Route::get('area/city', 'Api\AreaController@getAreaCity');
Route::get('filters/promo', 'Api\PromotionController@getPromoFilters');
Route::get('filters/promo/cities', 'Api\PromotionController@getCitiesWithPromoted');

// Other GET
Route::get('user', 'Api\AuthController@getUser')->middleware('api.auth:user');
Route::get('recommendation', 'Api\RoomController@recommendation')->middleware('api.auth:device');
Route::get('home', 'Api\RoomController@getHome')->middleware('api.auth:device');
Route::get('cities', 'Api\HomeStaticController@getRecommendationCities')->middleware('api.auth:device');
Route::get('profile', 'Api\RoomController@getProfile')->middleware('api.auth:user');
Route::get('kost/name', 'Api\RoomController@availableName');
Route::post('phone/check', 'Api\UserController@phoneNumberCheck');
Route::get('testimony', 'Api\OwnerTestimonialController@getTestimony')->middleware('api.auth:anyone');

// Slider endpoint
// Ref task: https://mamikos.atlassian.net/browse/BG-337
Route::get('sliders', 'Api\SliderController@getSliders')->middleware('api.auth:anyone');
Route::get('slider/{endpoint}', 'Api\SliderController@getSlider')->middleware('api.auth:anyone');

// FLash Sale
// Ref Task: https://mamikos.atlassian.net/browse/BG-1327
Route::group(
    ['prefix' => 'flash-sale'],
    function () {
        Route::get('/', 'Api\FlashSale\FlashSaleController@getAllData')->name('all');
        Route::get('get/{id?}', 'Api\FlashSale\FlashSaleController@getData')->name('get');
        Route::get('running', 'Api\FlashSale\FlashSaleController@getRunningData')->name('running');
        Route::get('upcoming', 'Api\FlashSale\FlashSaleController@getUpcomingData')->name('upcoming');
    }
);

// Landing
Route::get('kost/{slug}', 'Api\LandingController@show');

// Notification List
Route::group(['prefix' => 'notifications', 'middleware' => 'api.auth:device'], function() {
    Route::get('/', 'Api\NotificationController@index');
    Route::post('/', 'Api\NotificationController@read');
    Route::get('/counter', 'Api\NotificationController@counter');
    Route::get('/categories', 'Api\NotificationController@categories');
});
Route::put('notifications/setting', 'Api\AppController@putNotifSetting');
Route::put('device/notif_token', 'Api\AppController@putDevice');

Route::group(["prefix" => "media"], function() {
    Route::post('agent', 'Api\MediaController@postPhotoAgent')->middleware('api.auth:user');
    Route::post('/', 'Api\MediaController@postIndex');
});
// Route::post('subscribe', 'Api\RoomController@subscribeFilter');
Route::post('owner/contact', 'Api\RoomController@postOwnerContact');

Route::get('location-suggestion', 'Api\Search\LocationSuggestionController@suggestAutocomplete');

Route::group(['prefix'=>'general-search'], function() {
    Route::post('/criteria', 'Api\GeneralSearchController@saveCriteriaApp');
    Route::post('/keyword', 'Api\GeneralSearchController@saveUserInput');
    Route::get('/place/{place_name}', 'Api\GeneralSearchController@getPlaceGeolocation');
});

// Survey
Route::post('survey/phone', 'Api\SurveyPhoneFindingController@submitSurvey')->middleware('api.auth:device');

Route::group(['prefix'=>'booking'], function() {
    Route::get('type', 'Api\Booking\BookingDesignerController@getType');
    Route::get('detail/{booking_code}', 'Api\Booking\BookingUserController@detail')->middleware('api.auth:user');
    Route::post('cancel/{booking_code}', 'Api\Booking\BookingUserController@cancel')->middleware('api.auth:user');
    Route::post('payment/confirmation/{booking_code}', 'Api\Booking\BookingPaymentController@confirmation')->middleware('api.auth:user');
    Route::get('history', 'Api\Booking\BookingUserController@history')->middleware('api.auth:user');
    Route::post('guarantee/claim/{booking_code}', 'Api\Booking\BookingUserController@claimGuarantee')->middleware('api.auth:user');
    Route::get('voucher/{booking_code}', 'Api\Booking\BookingUserController@getVoucher')->middleware('api.auth:user');
    Route::get('receipt/{booking_code}', 'Api\Booking\BookingUserController@getReceipt')->middleware('api.auth:user');
    Route::get('counter', 'Api\Booking\BookingUserController@counter')->middleware('api.auth:user');

    Route::get('get-reject-reason/{type}', 'Api\Booking\BookingRejectController@getAllByType')->middleware('api.auth:user');
});

Route::group(['prefix'=>'marketplace'], function() {
    Route::post('input', 'Api\Marketplace\MarketplaceInputController@store')->middleware('api.auth:user');
    Route::post('update/{product_id}', 'Api\Marketplace\MarketplaceInputController@update')->middleware('api.auth:user');
    Route::post('deactivate/{product_id}', 'Api\Marketplace\MarketplaceInputController@deactivate')->middleware('api.auth:user');
    Route::post('activate/{product_id}', 'Api\Marketplace\MarketplaceInputController@activate')->middleware('api.auth:user');
    Route::post('/sold/{product_id}/{state}', 'Api\Marketplace\MarketplaceInputController@markSoldOut')->middleware('api.auth:user');

    Route::post('list', 'Api\Marketplace\MarketplaceController@listSearch')->middleware('api.auth:device');
    Route::post('cluster', 'Api\Marketplace\MarketplaceController@clusterSearch')->middleware('api.auth:device');
    Route::post('list/cluster', 'Api\Marketplace\MarketplaceController@detailCluster')->middleware('api.auth:device');

    Route::get('product/{product_id}', 'Api\Marketplace\MarketplaceController@show')->middleware('api.auth:device');

    Route::get('data/update/{product_id}', 'Api\Marketplace\MarketplaceInputController@getDataForUpdate')->middleware('api.auth:device');

    Route::get('owner/list', 'Api\Marketplace\MarketplaceOwnerController@getOwnerList')->middleware('api.auth:user');

    Route::get('questions', 'Api\Marketplace\MarketplaceController@chatQuestions');
    Route::post('{id}/call', 'Api\Marketplace\MarketplaceController@postCall')->middleware('api.auth:user');
    Route::post('{id}/call/reply', 'Api\Marketplace\MarketplaceController@postReply')->middleware('api.auth:user');
    Route::post('{id}/phone/click', 'Api\Marketplace\MarketplaceController@trackPhoneClick')->middleware('api.auth:user');
});

Route::post('reward/register', 'Api\PropertyInputController@registerRewardNumber');

// Owner
Route::group(['prefix' => 'owner', 'middleware' => 'api.auth'], function () {
    Route::group(['prefix' => 'data', 'middleware' => 'api.auth:user'], function () {
        Route::get('profile', 'Api\OwnerDataController@getProfile');
        Route::get('notification', 'Api\OwnerDataController@getNotificationOwner');
        Route::group(['prefix' => 'kos', 'as' => 'kos.'], function () {
            Route::get('/', 'Api\OwnerDataController@getOwnedKos')->name('iklan-saya');
            
            Route::get('facilities', 'Api\Owner\Data\Kost\FacilitiesController@index')->name('list-facilities');
            Route::get('kos-rules', 'Api\Owner\Data\Kost\KostRulesController@index')->name('list-rules');
            Route::get('minimum-payment', 'Api\Owner\Data\Kost\MinimumPaymentController@index')->name('list-minimum-payment');
            Route::post('duplicate', 'Api\Owner\Data\Kost\DuplicateController@duplicate')->name('duplicate');

            Route::group(['prefix' => 'room-unit', 'as' => 'allotment.'], function() {
                Route::get('/', 'Api\Room\RoomUnitController@index')->name('index');
                Route::group(['prefix' => 'single', 'as' => 'single.'], function() {
                    Route::post('/', 'Api\RoomAllotment\RoomAllotmentController@addRoomUnit')->name('add');
                    Route::put('{id}', 'Api\RoomAllotment\RoomAllotmentController@editRoomUnit')->name('update');
                    Route::delete('{id}', 'Api\RoomAllotment\RoomAllotmentController@deleteRoomUnit')->name('delete');
                });
                Route::post(
                    'bulk',
                    'Api\RoomAllotment\RoomAllotmentController@updateBulkRoomUnit'
                )->name('update-bulk');
            });

            Route::group(['prefix' => 'input', 'as' => 'input.'], function () {
                Route::group(['prefix' => 'stage', 'as' => 'draft.'], function () {
                    Route::get(
                        '/',
                        'Api\Owner\Input\Kost\DraftController@showDefaultStages'
                    )->name('get.stage');
                    Route::get(
                        '{songId}',
                        'Api\Owner\Input\Kost\DraftController@showStagesById'
                    )->where(['songId' => '[0-9]+'])->name('get.stage.by-id');
                });
                Route::group(['prefix' => 'address', 'as' => 'address.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\AddressController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\AddressController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\AddressController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::group(['prefix' => 'information', 'as' => 'information.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\InformationController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\InformationController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\InformationController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::group(['prefix' => 'kost-photo', 'as' => 'kost-photo.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\KostPhotoController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\KostPhotoController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\KostPhotoController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::group(['prefix' => 'room-photo', 'as' => 'room-photo.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\RoomPhotoController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\RoomPhotoController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\RoomPhotoController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });
                
                Route::group(['prefix' => 'facilities', 'as' => 'facilities.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\FacilitiesController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\FacilitiesController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\FacilitiesController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
                    Route::post(
                        '/',
                        'Api\Owner\Input\Kost\RoomAllotmentController@store'
                    )->name('store');
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\RoomAllotmentController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\RoomAllotmentController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::group(['prefix' => 'price', 'as' => 'price.'], function () {
                    Route::group(['prefix' => '{songId}', 'as' => 'edit.'], function () {
                        Route::get(
                            '/',
                            'Api\Owner\Input\Kost\PriceController@get'
                        )->where(['songId' => '[0-9]+'])->name('retrieve');
                        Route::put(
                            '/',
                            'Api\Owner\Input\Kost\PriceController@edit'
                        )->where(['songId' => '[0-9]+'])->name('put');
                    });
                });

                Route::put(
                    'submit/{songId}',
                    'Api\Owner\Input\Kost\SubmitController@submit'
                )->where(['songId' => '[0-9]+'])->name('submit.put');

                Route::post(
                    'ready-to-verif/{songId}',
                    'Api\Owner\Input\Kost\SubmitController@markReady'
                )->where(['songId' => '[0-9]+'])->name('submit.mark-as-ready');
            });

            // Address area
            Route::group(['prefix' => 'address','as' => 'address.'], function() {
                Route::get('province', 'Api\Owner\Data\Address\ProvinceController@index')->name('list-province');
                Route::get('city' , 'Api\Owner\Data\Address\CityController@index')->name('list-city');
                Route::get('subdistrict', 'Api\Owner\Data\Address\SubdistrictController@index')->name('list-subdistrict');
            });
        });
        Route::group(['prefix' => 'list'], function() {
                Route::get('update', 'Api\OwnerDataController@getListKosForUpdate');
                Route::get('kos', 'Api\OwnerDataController@getListKos');
                Route::get('kos/{id}', 'Api\OwnerDataController@detailKosOwner');
                Route::get('apartment', 'Api\OwnerDataController@getListApartment');
                Route::get('apartment/{id}', 'Api\OwnerDataController@detailKosOwner');
                Route::get('active', 'Api\OwnerDataController@getListAllCanPromote');
                Route::get('active/{slug}', 'Api\OwnerDataController@getListRoomActive');
                Route::get('jobs', 'Api\Jobs\JobsViewController@ownerList');
                Route::group(['prefix' => 'jobs'], function() {
                    Route::group(['prefix' => 'apply'], function() {
                        Route::get('jobs/{id}', 'Api\Jobs\JobsViewController@getListApply');
                        Route::get('{id}', 'Api\Jobs\JobsViewController@detailApply');
                    });
                    Route::get('{slug}/report', 'Api\Jobs\JobsViewController@ownerJobsReport');
                    Route::get('{slug}/deactive', 'Api\Jobs\JobsActionController@deactiveJobsOwner');
                    Route::get('{slug}/active', 'Api\Jobs\JobsActionController@activeJobsOwner');
                    Route::get('{slug}/delete', 'Api\Jobs\JobsActionController@deleteJobsOwner');
                    Route::get('{slug}', 'Api\Jobs\JobsViewController@ownerJobsDetail');
                });
        });
        Route::group(['prefix' => 'survey'], function() {
            Route::post('updateroom', 'Api\OwnerDataController@surveyUpdateRoom');
            Route::post('premium', 'Api\OwnerDataController@surveyPremium');
        });
        Route::group(['prefix' => 'kost-level'], function() {
            Route::get('flag-count', 'Api\KostLevelController@flagCount');
            Route::get('list', 'Api\KostLevelController@kostList');
        });
        Route::post('update-room', 'Api\OwnerDataController@updateAllProperty');
        //OWNER DASHBOARD - FINANCIAL REPORT
        Route::get('income', 'Api\OwnerDataController@getDashboardTotalIncome');
        Route::group(['prefix' => 'finance', 'as' => 'financial.report'], function() {
            Route::get('list-kost', 'Api\OwnerDataController@getListKostsIncome')->name('.list-kost');
            Route::get('kost/{id}', 'Api\OwnerDataController@getFinanceKost')->name('.specific-kost');
            Route::get('list-tenant/{id}', 'Api\OwnerDataController@getListTenant')->name('.list-tenant');
            Route::get('list-expense/{id}', 'Api\OwnerDataController@getListKostsExpense')->name('.list-expense');
        });

        Route::group(['prefix' => 'property', 'as' => 'property.'], function () {
            Route::get(
                '/',
                'Api\Owner\Data\Property\ListController@index'
            )->name('list');
            Route::get(
                '{id}/type',
                'Api\Owner\Data\Property\TypeController@index'
            )->where(['id' => '[0-9]+'])->name('by-id.types');
            Route::post(
                'validate-name',
                'Api\Owner\Data\Property\NameValidationController@property'
            )->name('validation.name');
            Route::post(
                '{id}/validate-type-name',
                'Api\Owner\Data\Property\NameValidationController@kostType'
            )->where(['id' => '[0-9]+'])->name('validation.type.name');
        });
    });

    Route::group(['prefix' => 'simple', 'middleware' => 'api.auth:user'], function() {
        Route::post('update/{id}', 'Api\OwnerDataController@simpleUpdateNotif');
    });
    Route::get('profile','Api\OwnerController@getProfileOwner')->middleware('api.auth:user');
    Route::get('news', 'Api\OwnerController@getNewsOwner')->middleware('api.auth:user');
    Route::post('request', 'Api\PremiumController@premiumPurchase')->middleware('api.auth:user');
    Route::post('promotion', 'Api\PromotionController@postPromotion')->middleware('api.auth:user');
    Route::post('pinned','Api\OwnerController@pinnedAction')->middleware('api.auth:user');
    Route::post('balance','Api\PremiumController@buyBalance')->middleware('api.auth:user');
    Route::get('call/{id}', 'Api\OwnerController@getListCallRoom')->middleware('api.auth:user');
    Route::get('claim/{id}','Api\OwnerController@getClaimOwner');
    Route::get('survey/{id}', 'Api\SurveyController@getOwnerSurvey')->middleware('api.auth:user');
    Route::get('review/{id}', 'Api\ReviewController@getOwnerReview')->middleware('api.auth:user');
    Route::get('chat/{id}', 'Api\OwnerController@getChatOwner')->middleware('api.auth:user');
    Route::post('claim','Api\OwnerController@postClaimOwner');
    Route::post('draft', 'Api\OwnerController@postSaveOwnerDesigner');
    Route::post('draft/update/{id}', 'Api\OwnerController@updateRoomOwner');
    Route::post('save', 'Api\OwnerController@postSaveOwnerDesigner');
    Route::get('list','Api\OwnerController@getListOwner');
    Route::get('promotion/{id}', 'Api\PromotionController@getPromotionOwner');
    Route::delete('delete/{id}','Api\OwnerController@deleteRoomByOwner');
    Route::put('available/{id}','Api\OwnerController@updateRoomAvailable');
    Route::get('edit/{id}','Api\OwnerController@editRoomOwner');
    Route::post('update/{id}','Api\OwnerController@updateRoomOwner');
    Route::post('updatesurvey', 'Api\SurveyController@updateSurveyOwner');
    Route::get('updatesurvey/{id}', 'Api\SurveyController@editSurveyOwner');
    Route::get('suggested','Api\OwnerController@getSuggestedOwnerRoom')->middleware('api.auth:user');
    Route::post('search','Api\OwnerController@postSearchOwnerRoom');
    Route::get('report/{id}','Api\OwnerController@getRoomReportOwner');
    Route::post('suggestion/name', 'Api\OwnerController@getSuggestionByName');
    Route::post('verification', 'Api\OwnerController@postVerification')->middleware('api.auth:user');
    Route::post('password/set', 'Api\OwnerController@setPassword')->middleware('api.auth:user');
    Route::post('password/update', 'Api\OwnerController@updatePassword')->middleware('api.auth:user');
    Route::post('request-booking/{id}', 'Api\OwnerController@requestBooking');
    Route::post('suggestion/name', 'Api\OwnerController@getSuggestionByName');
    Route::get('information', 'Api\OwnerInformationController@index');
    
    Route::group(['prefix' => 'bbk'], function () {
        Route::post('register', 'Api\Room\BookingOwnerRequestController@registerBulkBbk');
        Route::get('status', 'Api\Room\BookingOwnerRequestController@getBbkStatus');
    });

    Route::group(['prefix' => 'tenant'], function () {
        Route::get('dashboard', 'Api\Owner\Tenant\OwnerTenantController@getDashboard');
        Route::resource('link', 'Api\Owner\Tenant\OwnerTenantController', ['only' => ['store', 'show', 'index']]);
    });
    Route::post('contract-submission/confirm/{id}', 'Api\Owner\Tenant\ContractOwnerSubmissionController@confirm');
    Route::post('contract-submission/reject/{id}', 'Api\Owner\Tenant\ContractOwnerSubmissionController@reject');
    Route::resource('contract-submission', 'Api\Owner\Tenant\ContractOwnerSubmissionController', ['only' => ['show', 'index']]);

    // new API for update kost
    Route::post('updateroom/{id}', 'Api\PropertyInputController@updateKost')->middleware('api.auth:user');
    Route::get('data/update/apartment/{id}', 'Api\OwnerController@getApartmentDataForUpdate')->middleware('api.auth:user');
    Route::post('updateapartment/{id}', 'Api\PropertyInputController@updateApartment')->middleware('api.auth:user');

    Route::group(['prefix' => 'verification'], function(){
        Route::post('check', 'Api\OwnerController@postVerificationCheck');
        Route::post('code', 'Api\OwnerController@postVerification')->middleware('api.auth:user');
        Route::post('request', 'Api\OwnerController@postRequestVerification');
        Route::post('store', 'Api\OwnerController@postStoreVerification')->middleware('api.auth:user');
        Route::post('email', 'Api\OwnerDataController@postVerificationEmail')->middleware('api.auth:user');
    });

    Route::group(['prefix' => 'edit'], function ()
    {
        Route::post('email', 'Api\OwnerController@editEmail')->middleware('api.auth:user');
        Route::post('name', 'Api\UserController@editName')->middleware('api.auth:user');
        Route::post('phone', 'Api\Auth\OwnerController@editPhone')->middleware('api.auth:user');
        Route::post('phone/request', 'Api\Auth\OwnerController@editPhoneRequest')->middleware('api.auth:user');
    });

    Route::group(['prefix' => 'premium', 'middleware' => 'api.auth:user'], function() {
        Route::get('history', 'Api\PremiumController@history');
        Route::post('stop-reason', 'Api\PremiumController@stopReason');
        Route::post('cancel', 'Api\PremiumController@cancelPurchase');
        Route::post('trial', 'Api\PremiumController@autoApproveTrial');
        Route::get('confirmation', 'Api\PremiumController@getConfirmation');
        Route::group(['prefix' => 'ab-test'], function() {
            Route::get('check', 'Api\Premium\AbTestPremiumController@checkOwner');
        });
    });

    Route::group(['prefix' => 'booking'], function() {
        Route::post('room/create', 'Api\Booking\BookingDesignerController@store')->middleware('api.auth:user');
        Route::post('room/{id}/update', 'Api\Booking\BookingDesignerController@update')->middleware('api.auth:user');

        // Instant Booking
        Route::post('instant/register', 'Api\Booking\Owner\BookingController@registerInstantBook')->middleware('api.auth:user');

        Route::get('room/requests', 'Api\OwnerController@getRoomHasBookingRequest');

        // owner dashboard
        Route::get('dashboard', 'Api\Booking\Owner\OwnerBookingDashboardController@index');
    });

    Route::get('online', 'Api\OwnerController@insertOnlineData');
    Route::get('search/update', 'Api\OwnerDataController@searchUpdate');

    Route::post('register-request', 'Api\OwnerRegisterRequestController@registerRequest');

    // Owner loyalty feature @ API v1.5.3
    Route::get('loyalty', 'Api\OwnerLoyaltyController@getData');

    Route::group(['prefix' => 'forget'], function () {
        Route::post('password/identifier', 'Api\Auth\OwnerController@forgetPasswordIdentifier');
        Route::post('password/request', 'Api\Auth\OwnerController@forgetPasswordRequest');
        Route::post('password/check', 'Api\Auth\OwnerController@forgetPasswordCheck');
        Route::post('password', 'Api\Auth\OwnerController@forgetPassword');
    });

    // mamipay room price component
    Route::group(['prefix' => 'room'], function () {
        Route::get('price-component/{songId}', 'Api\Owner\Room\RoomPriceComponentController@index');
        Route::post('price-component/{songId}', 'Api\Owner\Room\RoomPriceComponentController@store');
        Route::put('price-component/{priceComponentId}', 'Api\Owner\Room\RoomPriceComponentController@update');
        Route::delete('price-component/{priceComponentId}', 'Api\Owner\Room\RoomPriceComponentController@destroy');
        Route::post('price-component/toggle/{songId}/{type}', 'Api\Owner\Room\RoomPriceComponentController@switch');

        Route::get('review', 'Api\Owner\Room\OwnerRoomReviewController@index')->middleware('api.auth:user');
    });
});

// Reward
Route::group(['prefix' => 'reward', 'middleware' => 'api.auth:user'], function () {
    Route::get('/list', 'Api\RewardController@index');
    Route::get('/detail/{id}', 'Api\RewardController@show')->where('id', '[0-9]+');
    Route::post('/redeem/{id}', 'Api\RewardController@redeem')->where('id', '[0-9]+');
});

Route::group(['prefix' => 'user'], function () {
    // Unauthenticated routes for tenant
    Route::group(['prefix' => 'forget'], function () {
        Route::post('password/identifier', 'Api\Auth\TenantController@forgetPasswordIdentifier');
        Route::post('password/request', 'Api\Auth\TenantController@forgetPasswordRequest');
        Route::post('password/check', 'Api\Auth\TenantController@forgetPasswordCheck');
        Route::post('password', 'Api\Auth\TenantController@forgetPassword');
    });

    // contract submission onboarding without auth
    Route::get('contract-submission/onboarding', 'Api\Contract\ContractUserSubmissionController@onboarding');

    // Authenticated routes for tenant
    Route::middleware(['api.auth:user'])->group(function () {
        Route::get('user-social', 'Api\UserDataController@isTenantUserSocial');
        Route::post('change-password', 'Api\Auth\TenantController@changePassword');
        Route::get('profile', 'Api\UserDataController@profile');
        Route::post('checkin', 'Api\UserDataController@checkinAction');
        Route::get('edit', 'Api\UserDataController@getEdit');
        Route::post('password', 'Api\Auth\TenantController@changePassword');
        Route::post('update', 'Api\UserDataController@postUpdate');
        Route::post('media', 'Api\UserDataController@changeProfilePhoto');
        Route::group(['prefix' => 'vacancy'], function () {
            Route::get('apply', 'Api\UserDataController@getUserJobsApply');
        });
        Route::get('checking', 'Api\UserDataController@getCheckUserIncomplete');
        Route::group(['prefix' => 'edit'], function () {
            Route::post('simple', 'Api\UserDataController@simplyEdit');
        });
        Route::post('{userId}/issue-chat-token', 'Api\UserDataController@issueChatToken');
        Route::post('occupation', 'Api\UserDataController@setOccupation');

        Route::group(['prefix' => 'voucher'], function () {
            Route::get('list', 'Api\UserVoucherController@index');
            Route::get('count', 'Api\UserVoucherController@count');
            Route::get('detail/{id}', 'Api\UserVoucherController@show')->where('id', '[0-9]+');
        });

        Route::get('verification/phone-number', 'Api\UserVerificationAccountController@verificationMobilePhone');
        Route::post('verification/verify-code-otp', 'Api\UserVerificationAccountController@checkCodeOtp');
        Route::post('verification/email', 'Api\UserVerificationAccountController@sendVerificationEmail');

        Route::get('verification/identity-card/options', 'Api\UserVerificationAccountController@getIdentityCardOptions');
        Route::get('verification/identity-card', 'Api\UserVerificationAccountController@getIdentityCard');
        Route::post('verification/identity-card/upload', 'Api\UserVerificationAccountController@uploadIdentityCard');
        Route::post('verification/identity-card/{id}', 'Api\UserVerificationAccountController@updateIdentityCardData');
        Route::get('verification/identity-card/{id}/detail', 'Api\UserVerificationAccountController@detailIdentityCard');
        Route::delete('verification/identity-card/{id}/remove', 'Api\UserVerificationAccountController@removeIdentityCard');

        Route::post('activation-mamipay/check-valid-account', 'Api\UserActivationMamipayController@sendCodeOtp');
        Route::post('activation-mamipay/check-verification-code', 'Api\UserActivationMamipayController@checkCodeOtp');
        Route::post('activation-mamipay', 'Api\UserActivationMamipayController@activationUserMamipay');
        Route::post('activation-mamipay/create-pin', 'Api\UserActivationMamipayController@storePin');
        Route::post('activation-mamipay/change-pin', 'Api\UserActivationMamipayController@changePin');
        Route::post('check-pin/', 'Api\UserActivationMamipayController@checkValidPin');
        
        // Account Bank
        Route::get('bank/list', 'Api\UserAccountBankController@getBankList');
        Route::get('bank', 'Api\UserAccountBankController@getUserBank');
        Route::post('bank/store', 'Api\UserAccountBankController@storeAccountBank');
        Route::get('bank/remove/{bankId}', 'Api\UserAccountBankController@removeAccountBank');
        Route::get('bank/{userId}/{status}', 'Api\UserAccountBankController@setStatusAccountBank');

        // Balance and History Transaction
        // Route::get('saldo', 'Api\UserWalletController@getBalance');
        // Route::post('withdraw', 'Api\UserWalletController@withdraw');
        // Route::get('create-va', 'Api\UserWalletController@createVABNI');
        // Route::get('transaction/invoice/{invoiceId}', 'Api\UserHistoryTransactionController@getHistoryInvoiceDetail');
        // Route::get('transaction/invoice/{invoiceId}/download', 'Api\UserHistoryTransactionController@downloadInvoice');
        // Route::get('transaction/invoice/history', 'Api\UserHistoryTransactionController@getHistoryInvoice');
        // Route::get('transaction/{action}', 'Api\UserWalletController@getWalletHistory');
        // Route::get('transaction/{action}/{transactionId}', 'Api\UserWalletController@getWalletHistory');

        Route::get('booking', 'Api\Booking\NewBookingUserController@index');
        Route::get('booking/detail-room/{roomId}', 'Api\Booking\NewBookingUserController@detailRoom');
        Route::get('booking/detail/{bookId}', 'Api\Booking\NewBookingUserController@show');
        Route::post('booking/cancel/{bookId}', 'Api\Booking\NewBookingUserController@cancelBook');
        Route::post('booking/{roomId}/{roomType}', 'Api\Booking\NewBookingUserController@store');

        Route::get('booking/contract/{contractId}', 'Api\Booking\BookingContractController@getContract');
        // Route::post('booking/invoice/filter/year', 'Api\Booking\BookingContractController@filterInvoiceByYear');
        Route::post('booking/invoice/filter/year', 'Api\Booking\BookingContractController@filterInvoiceByYearFromExternal');
        Route::get('booking/invoice/payment-schedule/{invoiceId}', 'Api\Booking\BookingContractController@invoiceDetail');
        Route::get('booking/room', 'Api\Booking\BookingRoomUserController@getRoomWithContract');
        Route::get('booking/status', 'Api\Booking\BookingStatusController@getListStatus');

        // Rent Count
        Route::get('booking/rent-count/{type}', 'Api\Booking\BookingRentCountController@getRentCount');
        Route::get('booking/rent-count/{type}/room/{roomId}', 'Api\Booking\BookingRentCountController@getRentCountByRoom');
        Route::post('booking/estimate-checkout', 'Api\Booking\BookingRentCountController@getCheckoutDate');

        // draft-booking
        Route::get('draft-booking/shortcut', 'Api\Booking\BookingUserDraftController@homepageShortcut');
        Route::get('draft-booking/last-seen', 'Api\Booking\BookingUserDraftController@roomHistoryViewed');
        Route::delete('draft-booking/last-seen/{roomId}', 'Api\Booking\BookingUserDraftController@destroyViewedRoom');
        Route::get('draft-booking/notification', 'Api\Booking\BookingUserDraftController@notification');
        Route::resource('draft-booking', 'Api\Booking\BookingUserDraftController', ['only' => ['index', 'store', 'destroy']]);
        Route::resource('contract', 'Api\Contract\ContractUserController', ['only' => ['index', 'show']]);

        // contract link
        Route::get('contract-submission/shortcut', 'Api\Contract\ContractUserSubmissionController@homepageShortcut');
        Route::get('contract-submission/link/{code}', 'Api\Contract\ContractUserSubmissionController@showLink');
        Route::resource('contract-submission', 'Api\Contract\ContractUserSubmissionController', ['only' => ['store']]);

        // Point User
        Route::group(['prefix' => 'point'], function () {
            Route::get('total', 'Api\PointUserController@total');
            Route::get('expiry', 'Api\PointUserController@expiry');
            Route::get('tnc', 'Api\PointUserController@tnc');
            Route::get('activity', 'Api\PointUserController@activity');
            Route::get('history', 'Api\PointUserController@history');
            Route::get('scheme', 'Api\PointUserController@scheme');
        });

        // Polling
        Route::group(['prefix' => 'polling'], function () {
            Route::get('detail/{key}', 'Api\UserPollingController@detail');
            Route::post('submit/{key}', 'Api\UserPollingController@submit');
        });

        // Redeem
        Route::get('/redeem/list', 'Api\RewardController@userRedeemList');
        Route::get('/redeem/detail/{id}', 'Api\RewardController@userRedeemDetail')->where('id', '[0-9]+');
    });

    // booking draft price without middleware
    Route::get('booking/draft-price/{songId}', 'Api\Booking\BookingDraftPriceController@index');
});

Route::group(['prefix'=>'setting', 'middleware'=>'api.auth:user'], function() {
    Route::get('notif', 'Api\UserController@getSettings');
    Route::post('notif', 'Api\UserController@changeSettings');
    Route::post('phone', 'Api\UserDataController@changePhoneNumber');

    // **** New API endpoints for v1.1.2 ****
    Route::post('photo', 'Api\UserDataController@changeProfilePhoto');
    Route::post('name', 'Api\UserDataController@changeName');
    Route::post('email', 'Api\UserDataController@changeEmail');

    // **** New API endpoints for v1.2.3 ****
    Route::post('terms', 'Api\UserDataController@updateTerms');
});

Route::group(['prefix'=>'notification', 'middleware'=>'api.auth:user'], function() {
    Route::get('counter', 'Api\UserController@getCounter');
    Route::post('read', 'Api\UserController@readCounter');
});

Route::group(['prefix' => 'jobs'], function() {
    Route::group(['prefix' => 'company'], function() {
        Route::post('list', 'Api\Jobs\Company\ProfileController@postListCompanyProfile');
        Route::get('detail/{slug}', 'Api\Jobs\Company\ProfileController@getDetailProfile');
    });
    Route::post('add', 'Api\Jobs\JobsActionController@addNewJobs')->middleware('api.auth:user');
    Route::post('cluster', 'Api\Jobs\JobsViewController@jobsListCluster')->middleware('api.auth:anyone');
    Route::group(['prefix' => 'cv', 'middleware' => 'api.auth:user'], function() {
        Route::post('add', 'Api\Jobs\JobsActionController@addCv');
        Route::post('file', 'Api\Jobs\JobsActionController@addFile');
        Route::get('edit/{id}', 'Api\Jobs\JobsViewController@getDataCV');
        Route::post('update/{id}', 'Api\Jobs\JobsActionController@updateCv');
    });
    Route::post('list', 'Api\Jobs\JobsViewController@jobsListSearch')->middleware('api.auth:anyone');
    Route::get('{slug}', 'Api\Jobs\JobsViewController@detailJobs')->middleware('api.auth:anyone');
    Route::post('list/cluster', 'Api\Jobs\JobsViewController@singleCluster')->middleware('api.auth:anyone');

    Route::group(['prefix' => 'edit', 'middleware' => 'api.auth:user'], function() {
        Route::get('{id}', 'Api\Jobs\JobsActionController@editData');
        Route::post('{id}', 'Api\Jobs\JobsActionController@postEditData');
    });
    Route::post('verification', 'Api\Jobs\JobsActionController@verificationFromUser');

    Route::get('landing/1/{slug}', 'Api\LandingVacancyController@showFilter');
    Route::get('landing/{slug}', 'Api\LandingVacancyController@showFilter');
});

Route::get('related/{id}', 'Api\Related\RelatedController@getRelated')->middleware('api.auth:anyone');
Route::post('related/web', 'Api\Related\RelatedController@relatedWeb');

Route::group(['prefix' => 'geocode'], function() {
    Route::post('cache', 'Api\GeocodeCacheController@saveGeocodeCache');
    Route::get('cache', 'Api\GeocodeCacheController@getGeocodeCache');
});

// Home Static List
Route::group(['prefix' => 'home-static-list'], function() {
    Route::get('/{id}', 'Api\HomeStaticController@getRecommendationRooms');
});

// General Config
Route::group(['prefix' => 'config', 'as' => 'config.'], function() {
    Route::get('general', 'Api\ConfigController@general')->middleware('api.auth:anyone');
    Route::post('chat-pretext', 'Api\ConfigController@getChatPretext')->middleware('api.auth:anyone');

    Route::group(['prefix' => 'ab-test', 'as' => 'ab-test.'], function () {
        Route::get('owner-experience', 'Api\Owner\ConfigController@abTest')->name('ox')->middleware('api.auth:user');
    });
});

// Newly created for Search v2
Route::group(['prefix' => 'suggest'], function() {
    Route::get('/', 'Api\Search\SuggestionController@suggest');
    // Prototyping:
    Route::get('normal', 'Api\Search\SuggestionController@normal');
    Route::get('proto', 'Api\Search\SuggestionController@proto');
});

Route::group(['prefix' => 'channel', 'middleware' => 'api.auth:user'], function() {
	Route::post('/', 'Api\SendBird\SendBirdController@createChannel');
});

Route::group(['prefix' => 'room'], function()
{
    Route::group(['prefix' => 'goldplus'], function()
    {
        Route::get('statistics/filters/{option}', 'Api\GoldplusStatisticController@getStatisticFilterList')->middleware('api.auth:user');
        Route::get('{songId}', 'Api\GoldplusStatisticController@getDetailKostGp')->middleware('api.auth:user');
        Route::get('{songId}/statistics/{type}', 'Api\GoldplusStatisticController@getGoldplusStatistic')->middleware('api.auth:user');
    });
});

Route::group(['prefix' => 'goldplus', 'middleware' => 'api.auth:user'], function() {
    Route::get('packages', 'Api\GoldPlus\PackageController@index');
    Route::get('submissions/onboarding', 'Api\GoldPlus\SubmissionController@getOnboardingContent');
    Route::get('submissions/tnc', 'Api\GoldPlus\SubmissionController@getTncContent');
    Route::post('submissions', 'Api\GoldPlus\SubmissionController@store');
    Route::get('unsubscribe/statistic', 'Api\GoldPlus\UnsubscribeController@getStatistic');

    //GP Business onboarding routes
    Route::group(['prefix' => 'onboarding'], function() {
        Route::get('kost-business', 'Api\GoldplusController@getGoldplusOnboardingWords');
        
        //TODO : Angga Bayu S <here> *adding more endpoint related to GP Onboard
    });
});

Route::group(['prefix' => 'me', 'middleware' => 'api.auth:user'], function () {
    // all routes inside namespace 'me' should run in the context of current user
    Route::get('/goldplus-submission', 'Api\Me\GoldPlus\SubmissionController@index');
    Route::get('/goldplus-submission/status', 'Api\Me\GoldPlus\SubmissionController@status');

    Route::get('/goldplus/active-contract', 'Api\Me\GoldPlus\ContractController@activeContract');
});
//TEMP Sanjunipero
Route::get('sanjunipero/list', 'Api\Sanjunipero\ListController@index')->name('api.sanjunipero.list-kost');

// notification action
Route::group(['prefix'=> 'notif-action', 'middleware'=>'api.auth:anyone'], function() {
    Route::get('show-list-bill/{invoiceId}', 'Api\NotifAction\Contract\NotifActionContractController@showListBill');
    Route::get('mark-payment/{invoiceId}', 'Api\NotifAction\Contract\NotifActionContractController@markPayment');
    Route::get('reminder-payment/{invoiceId}', 'Api\NotifAction\Contract\NotifActionContractController@reminderPayment');
});