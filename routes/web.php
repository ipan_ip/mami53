<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'midtrans'], function () {
    Route::post('/snaptoken', 'Api\Midtrans\SnapController@postToken')->middleware("web");
    Route::post('/snapfinish', 'Web\Midtrans\SnapController@finish')->middleware("web");
    Route::post('/notification', 'Web\Midtrans\SnapController@notification');
});

Route::group(['prefix' => 'garuda', 'middleware' => 'api.web'], function () {
    Route::group(['prefix' => 'mamiroom'], function () {
        Route::group(['prefix' => 'landing'], function () {
            Route::post('input', 'Api\Mamiroom\LandingInput\LandingInputController@store');
            Route::post('photo', 'Api\Mamiroom\LandingInput\LandingInputController@uploadPhoto');
        });
    });
    Route::put('available/{id}', 'Api\OwnerController@updateRoomAvailable');
    Route::post('activation-mamipay/check-valid', 'Api\UserActivationMamipayController@checkValidationPhoneNumberAndSendCode');
    Route::post('activation-mamipay', 'Api\UserActivationMamipayController@activationUserMamipay');
    Route::post('register-request', 'Api\OwnerRegisterRequestController@registerRequest');

    Route::get('area', 'Api\AreaController@getArea');
    Route::get('area/city', 'Api\AreaController@getAreaCity');
    Route::get('area/campus', 'Api\AreaController@getCampus');
    Route::get('area/mrt', 'Api\AreaController@getMrt');
    Route::group(['prefix' => 'forgot-password'], function () {
        Route::post('/check-account', 'Api\ForgotPasswordController@checkAccount');
        Route::post('/verification-code', 'Api\ForgotPasswordController@checkVerificationCode');
        Route::post('/new-password', 'Api\ForgotPasswordController@resetPassword');
        Route::post('/get-verification-code', 'Api\ForgotPasswordController@getLastVerificationCodeAccount');
    });

    // Home Static List
    Route::group(['prefix' => 'home-static-list'], function () {
        Route::get('/{id}', 'Api\HomeStaticController@getRecommendationRooms');
    });

    // General Config
    Route::group(['prefix' => 'config', 'as' => 'config.'], function () {
        Route::get('general', 'Api\ConfigController@general')->name('general');

        Route::group(['prefix' => 'ab-test', 'as' => 'ab-test.'], function () {
            Route::get('owner-experience', 'Api\Owner\ConfigController@abTest')->name('ox');
        });
    });

    Route::post('related/web', 'Api\Related\RelatedController@relatedWeb');

    Route::group(['prefix' => 'notifications', 'middleware' => 'web'], function () {
        Route::get('/', 'Api\NotificationController@index');
        Route::post('/', 'Api\NotificationController@read');
        Route::get('/counter', 'Api\NotificationController@counter');
    });
    Route::get('notifications/categories', 'Api\NotificationController@categories');

    // Reward
    Route::group(['prefix' => 'reward', 'middleware' => 'web'], function () {
        Route::get('/list', 'Api\RewardController@index');
        Route::get('/detail/{id}', 'Api\RewardController@show')->where('id', '[0-9]+');
        Route::post('/redeem/{id}', 'Api\RewardController@redeem')->where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'user', 'middleware' => 'web'], function () {
        Route::middleware('api.auth.session')->group(function () {
            Route::get('user-social', 'Api\UserDataController@isTenantUserSocial');
            Route::get('profile', 'Api\UserDataController@profile');
            Route::get('edit', 'Api\UserDataController@getEdit');
            Route::post('update', 'Api\UserDataController@postUpdate');
            Route::post('activation-mamipay', 'Api\UserActivationMamipayController@activationUserMamipay');
            Route::get('bank', 'Api\UserAccountBankController@getUserBank');
            Route::post('bank/store', 'Api\UserAccountBankController@storeAccountBank');
            Route::post('change-password', 'Api\Auth\TenantController@changePassword');
        });

        Route::group(['prefix' => 'forget'], function () {
            Route::post('password/identifier', 'Api\Auth\TenantController@forgetPasswordIdentifier');
            Route::post('password/request', 'Api\Auth\TenantController@forgetPasswordRequest');
            Route::post('password/check', 'Api\Auth\TenantController@forgetPasswordCheck');
            Route::post('password', 'Api\Auth\TenantController@forgetPassword');
        });

        Route::post('checkin', 'Api\UserDataController@checkinAction');
        Route::post('media', 'Api\UserDataController@changeProfilePhoto');
        Route::group(['prefix' => 'vacancy'], function () {
            Route::get('apply', 'Api\UserDataController@getUserJobsApply');
        });

        Route::get('checking', 'Api\UserDataController@getCheckUserIncomplete');
        Route::group(['prefix' => 'edit'], function () {
            Route::post('simple', 'Api\UserDataController@simplyEdit');
        });

        Route::post('occupation', 'Api\UserDataController@setOccupation');

        Route::group(['prefix' => 'voucher'], function () {
            Route::get('list', 'Api\UserVoucherController@index');
            Route::get('count', 'Api\UserVoucherController@count');
            Route::get('detail/{id}', 'Api\UserVoucherController@show')->where('id', '[0-9]+');
        });

        Route::get('verification/phone-number', 'Api\UserVerificationAccountController@verificationMobilePhone');
        Route::post('verification/verify-code-otp', 'Api\UserVerificationAccountController@checkCodeOtp');
        Route::post('verification/email', 'Api\UserVerificationAccountController@sendVerificationEmail');

        Route::get('verification/identity-card/options', 'Api\UserVerificationAccountController@getIdentityCardOptions');
        Route::get('verification/identity-card', 'Api\UserVerificationAccountController@getIdentityCard');
        Route::post('verification/identity-card/upload', 'Api\UserVerificationAccountController@uploadIdentityCard');
        Route::post('verification/identity-card/{id}', 'Api\UserVerificationAccountController@updateIdentityCardData');
        Route::get('verification/identity-card/{id}/detail', 'Api\UserVerificationAccountController@detailIdentityCard');
        Route::delete('verification/identity-card/{id}/remove', 'Api\UserVerificationAccountController@removeIdentityCard');

        // Activation Mamipay
        Route::post('activation-mamipay/check-valid-account', 'Api\UserActivationMamipayController@sendCodeOtp');
        Route::post('activation-mamipay/check-verification-code', 'Api\UserActivationMamipayController@checkCodeOtp');
        Route::post('activation-mamipay/create-pin', 'Api\UserActivationMamipayController@storePin');
        Route::post('activation-mamipay/change-pin', 'Api\UserActivationMamipayController@changePin');
        Route::post('check-pin/', 'Api\UserActivationMamipayController@checkValidPin');

        // Account Bank
        Route::get('bank/list', 'Api\UserAccountBankController@getBankList');
        Route::get('bank/remove/{bankId}', 'Api\UserAccountBankController@removeAccountBank');
        Route::get('bank/{userId}/{status}', 'Api\UserAccountBankController@setStatusAccountBank');

        // Balance and History Transaction
        // Route::get('/saldo', 'Api\UserWalletController@getBalance');
        // Route::post('withdraw', 'Api\UserWalletController@withdraw');
        // Route::get('transaction/invoice/history', 'Api\UserHistoryTransactionController@getHistoryInvoice');
        // Route::get('transaction/invoice/{invoiceId}', 'Api\UserHistoryTransactionController@getHistoryInvoiceDetail');
        // Route::get('transaction/{action}', 'Api\UserWalletController@getWalletHistory');
        // Route::get('transaction/{action}/{transactionId}', 'Api\UserWalletController@getWalletHistory');

        Route::get('booking/detail-room/{roomId}', 'Api\Booking\NewBookingUserController@detailRoom');
        Route::get('booking', 'Api\Booking\NewBookingUserController@index');
        Route::get('booking/detail/{bookId}', 'Api\Booking\NewBookingUserController@show');
        Route::post('booking/cancel/{bookId}', 'Api\Booking\NewBookingUserController@cancelBook');
        Route::post('booking/{roomId}/{roomType}', 'Api\Booking\NewBookingUserController@store');

        Route::get('booking/contract/{contractId}', 'Api\Booking\BookingContractController@getContract');
        Route::post('booking/invoice/filter/year', 'Api\Booking\BookingContractController@filterInvoiceByYearFromExternal');
        Route::get('booking/invoice/payment-schedule/{invoiceId}', 'Api\Booking\BookingContractController@invoiceDetail');
        Route::get('booking/room', 'Api\Booking\BookingRoomUserController@getRoomWithContract');
        Route::get('booking/status', 'Api\Booking\BookingStatusController@getListStatus');

        // Rent Count
        Route::get('booking/rent-count/{type}', 'Api\Booking\BookingRentCountController@getRentCount');
        Route::get('booking/rent-count/{type}/room/{roomId}', 'Api\Booking\BookingRentCountController@getRentCountByRoom');
        // Estimate Checkout
        Route::post('booking/estimate-checkout', 'Api\Booking\BookingRentCountController@getCheckoutDate');

        // draft-booking
        Route::get('draft-booking/shortcut', 'Api\Booking\BookingUserDraftController@homepageShortcut');
        Route::get('draft-booking/last-seen', 'Api\Booking\BookingUserDraftController@roomHistoryViewed');
        Route::delete('draft-booking/last-seen/{roomId}', 'Api\Booking\BookingUserDraftController@destroyViewedRoom');
        Route::get('draft-booking/notification', 'Api\Booking\BookingUserDraftController@notification');
        Route::resource('draft-booking', 'Api\Booking\BookingUserDraftController', ['only' => ['index', 'store', 'destroy']]);
        Route::resource('contract', 'Api\Contract\ContractUserController', ['only' => ['index', 'show']]);

        // contract link
        Route::get('contract-submission/onboarding', 'Api\Contract\ContractUserSubmissionController@onboarding');
        Route::get('contract-submission/shortcut', 'Api\Contract\ContractUserSubmissionController@homepageShortcut');
        Route::get('contract-submission/link/{code}', 'Api\Contract\ContractUserSubmissionController@showLink');
        Route::resource('contract-submission', 'Api\Contract\ContractUserSubmissionController', ['only' => ['store']]);

        // booking draft price
        Route::get('booking/draft-price/{songId}', 'Api\Booking\BookingDraftPriceController@index');

        // Point User
        Route::group(['prefix' => 'point'], function () {
            Route::get('total', 'Api\PointUserController@total');
            Route::get('expiry', 'Api\PointUserController@expiry');
            Route::get('tnc', 'Api\PointUserController@tnc');
            Route::get('activity', 'Api\PointUserController@activity');
            Route::get('history', 'Api\PointUserController@history');
            Route::get('scheme', 'Api\PointUserController@scheme');
        });

        // Polling
        Route::group(['prefix' => 'polling'], function () {
            Route::get('detail/{key}', 'Api\UserPollingController@detail');
            Route::post('submit/{key}', 'Api\UserPollingController@submit');
        });
        
        // Redeem
        Route::get('/redeem/list', 'Api\RewardController@userRedeemList');
        Route::get('/redeem/detail/{id}', 'Api\RewardController@userRedeemDetail')->where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'stories'], function () {
        Route::post('cluster', 'Api\RoomController@listCluster');
        Route::post('list', 'Api\RoomController@listSearch')->middleware('web');
        Route::post('list2', 'Api\PropertySearchController@searchKost')->middleware('web');
        Route::get('facility/{type?}', 'Api\TagController@getFacilities');
        Route::get('filters', 'Api\TagController@getFacilitiesFilter');
        Route::post('filters/count', 'Api\RoomFilterController@countFilteredRooms');
        Route::post('landing/view/{id}', 'Api\LandingController@viewCount');
        Route::post('search_alarm', 'Api\SubscribeController@subscribeFilter');
        Route::post('subscribe_suggestion', 'Api\SubscribeController@subscribeFilter')->middleware('web');
        Route::post('subscribe_download', 'Api\RoomController@subscribeDownload')->middleware('web');
        Route::post('subscribe_download/update', 'Api\RoomController@subscribeDownloadUpdate')->middleware('web');

        Route::get('love', 'Api\RoomController@listFavorite')->middleware('web');
        Route::get('survey', 'Api\RoomController@listSurvey')->middleware('web');
        Route::get('telp', 'Api\RoomController@listTelp')->middleware('web');
        Route::get('call', 'Api\RoomController@listCall')->middleware('web');
        Route::get('view', 'Api\RoomController@listView')->middleware('web');
        Route::group(['prefix' => 'chat'], function () {
            Route::get('list', 'Api\Chat\ChatController@list');
            Route::post('tnc-tracker', 'Api\Chat\ChatTncTrackerController@store')->middleware('web');
        });

        Route::get('{id}', 'Api\RoomController@show');
        Route::get('{id}/photos', 'Api\RoomController@getCardsBySongId');
        Route::get('{id}/gallery', 'Api\Card\CardController@getCardsWithCategory');

        Route::post('{id}/call', 'Api\RoomController@postCall')->middleware('web');

        // Facility detail page :: used on API v1.7.2
        Route::get('facility/photos/{id}', 'Web\RoomFacilityController@getDetails');

        Route::get('location/{id}', 'Api\RoomController@getRoomLocation');

        Route::post('save_json_id', 'Api\RoomController@postSubmitKost');
        Route::post('input', 'Api\PropertyInputController@inputKost')->middleware('web');
        Route::post('input-simple', 'Api\PropertyInputController@inputKostSimple')/*->middleware('web')*/;
        Route::post('input-referral', 'Api\PropertyInputController@inputKostReferral')->middleware('web');
        Route::post('input-general', 'Api\PropertyInputController@inputKostGeneral')->middleware('web');
        Route::get('input/tags/{type}', 'Api\PropertyInputController@getTagsGroup');

        Route::post('{id}/love', 'Web\Api\RoomController@postFavorite')->middleware('web');
        Route::post('{id}/chat', 'Api\RoomController@postChat')->middleware('web');
        Route::post('{id}/chat/reply', 'Web\RoomController@postChatReply')->middleware('web');
        Route::post('{id}/view', 'Api\RoomController@postView')->middleware('web');

        Route::get('{id}/report', 'Api\OwnerController@getRoomReportOwner');

        Route::post('survey/add', 'Api\SurveyController@postSurvey')->middleware('web');
        Route::get('{id}/survey', 'Api\SurveyController@getSurvey')->middleware('web');

        Route::group(['prefix' => 'review', 'middleware' => 'web'], function () {
            Route::post('add', 'Api\ReviewController@addNewReview');
            Route::post('reply', 'Api\ReviewController@replyThread');
            Route::get('reply/{id}', 'Api\ReviewController@listRepltThread');
            Route::get('{id}', 'Web\ReviewController@getList');
        });

        // use flag term to send report about unmatched data
        Route::get('flag/type', 'Api\ReportController@reportType');
        Route::post('{id}/flag', 'Api\ReportController@postReport')->middleware('web');

        Route::group(['prefix' => 'house_property'], function () {
            Route::post('list', 'Api\HouseProperty\HousePropertyController@listSearch');
            Route::post('cluster', 'Api\HouseProperty\HousePropertyController@postCluster');
            Route::post('cluster/list', 'Api\HouseProperty\HousePropertyController@postClusterList');
            Route::post('{id}/call', 'Api\HouseProperty\HousePropertyController@postCall')->middleware('web');
            Route::post('{id}/chat/reply', 'Api\HouseProperty\HousePropertyController@postChatReply')->middleware('web');
            Route::get('{type}/{song_id}', 'Api\HouseProperty\HousePropertyController@detail');
        });

        Route::post('booking', 'Api\BookingController@postBooking')->middleware('web');

        Route::post('addreview', 'Api\ReviewController@postReview')->middleware('web');
        Route::post('updatereview', 'Api\ReviewController@updateReview')->middleware('web');
        Route::post('replyreview', 'Api\ReviewController@replyReview')->middleware('web');
        Route::get('{id}/getreview', 'Api\ReviewController@getReview')->middleware('web');

        Route::post('exist', 'Api\PropertyInputController@checkExistingInput');
        Route::get('exist/photo/{id}', 'Api\PropertyInputController@checkExistingPhoto');

        Route::group(['prefix' => 'review', 'middleware' => 'web'], function () {
            Route::post('reply', 'Api\ReviewController@replyThread');
            Route::get('reply/{id}', 'Api\ReviewController@listRepltThread');
            Route::get('{id}', 'Web\ReviewController@getList');
        });

        // old version to get recommendation rooms in detail page
        Route::get('{id}/suggestion', 'Api\RoomController@getSuggestions');
        // new version to get recommendation rooms in detail page
        Route::get('{id}/recommendation', 'Api\RoomController@getRecommendationRooms');
        Route::get('{id}/reviews', 'Api\ReviewController@getKosReviews')->middleware('web');
        Route::get('{id}/rules', 'Api\RoomController@getKosRules');
        Route::post('free-search', 'Api\RoomController@freeSearch');
        Route::post('newest', 'Api\RoomController@listNewestLive');
        Route::get('benefit/{id}', 'Api\Room\BenefitController@index');

        Route::group(['prefix' => 'booking'], function () {
            Route::get('type/{room_id}', 'Api\Booking\BookingDesignerController@getType')->middleware('web');
            Route::post('schedule', 'Api\Booking\BookingDesignerController@getSchedule')->middleware('web');
            Route::post('search', 'Api\Booking\BookingDesignerController@searchAvailable')->middleware('web');
            Route::post('price/calculate/{room_id}/{type_id}', 'Api\Booking\BookingUserController@calculate')->middleware('web');
            Route::post('book/{room_id}/{type_id}', 'Api\Booking\BookingUserController@book')->middleware('web');
        });
        Route::group(['prefix' => 'update'], function () {
            Route::post('name/{id}', 'Api\RoomUpdaterController@updateNameType')->middleware('api.auth:user');
            Route::post('price/{id}', 'Api\RoomUpdaterController@updatePriceRoom')->middleware('api.auth:user');
            Route::post('size/{id}', 'Api\RoomUpdaterController@updateRoomSize')->middleware('api.auth:user');
            Route::post('photo/{id}', 'Api\RoomUpdaterController@updatePhoto')->middleware('api.auth:user');
            Route::post('bathroom/{id}', 'Api\RoomUpdaterController@updateBathroom')->middleware('api.auth:user');
            Route::post('facroom/{id}', 'Api\RoomUpdaterController@updateFacRoom')->middleware('api.auth:user');
            Route::post('facshare/{id}', 'Api\RoomUpdaterController@updateFacShare')->middleware('api.auth:user');
            Route::post('parking/{id}', 'Api\RoomUpdaterController@updateParking')->middleware('api.auth:user');
            Route::post('facnear/{id}', 'Api\RoomUpdaterController@updateFacNear')->middleware('api.auth:user');
            Route::post('otherinformation/{id}', 'Api\RoomUpdaterController@updateOtherInformation')->middleware('api.auth:user');
        });

        Route::group(['prefix' => 'edit'], function () {
            Route::get('name/{id}', 'Api\RoomUpdaterController@getNameType')->middleware('api.auth:user');
            Route::get('price/{id}', 'Api\RoomUpdaterController@getPriceRoom')->middleware('api.auth:user');
            Route::get('size/{id}', 'Api\RoomUpdaterController@getRoomSize')->middleware('api.auth:user');
            Route::get('otherinformation/{id}', 'Api\RoomUpdaterController@getOtherInformation')->middleware('api.auth:user');
            Route::get('facnear/{id}', 'Api\RoomUpdaterController@getFacNear')->middleware('api.auth:user');
            Route::get('parking/{id}', 'Api\RoomUpdaterController@getParking')->middleware('api.auth:user');
            Route::get('facshare/{id}', 'Api\RoomUpdaterController@getFacShare')->middleware('api.auth:user');
            Route::get('facroom/{id}', 'Api\RoomUpdaterController@getFacRoom')->middleware('api.auth:user');
            Route::get('bathroom/{id}', 'Api\RoomUpdaterController@getBathroom')->middleware('api.auth:user');
        });

        Route::group(['prefix' => 'jobs'], function () {
            Route::group(['prefix' => 'company'], function () {
                Route::post('list', 'Api\Jobs\Company\ProfileController@postListCompanyProfile');
                Route::get('detail/{slug}', 'Api\Jobs\Company\ProfileController@getDetailProfile');
            });
            Route::post('add', 'Api\Jobs\JobsActionController@addNewJobs')->middleware('web');
            Route::post('cluster', 'Api\Jobs\JobsViewController@jobsListCluster');
            Route::group(['prefix' => 'cv', 'middleware' => 'web'], function () {
                Route::post('add', 'Api\Jobs\JobsActionController@addCv');
                Route::post('file', 'Api\Jobs\JobsActionController@addFile');
            });

            Route::post('landing/view/{id}', 'Api\Jobs\JobsViewController@viewCount');

            Route::post('list', 'Api\Jobs\JobsViewController@jobsListSearch')->middleware('web');

            Route::get('landing/{slug}', 'Api\Jobs\JobsActionController@getLandingJobs');
            Route::get('{slug}', 'Web\Vacancy\VacancyController@detailJobs')->middleware('web');
            Route::post('list/cluster', 'Api\Jobs\JobsViewController@singleCluster');

            Route::group(['prefix' => 'edit', 'middleware' => 'web'], function () {
                Route::get('{id}', 'Api\Jobs\JobsActionController@editData');
                Route::post('{id}', 'Api\Jobs\JobsActionController@postEditData');
                Route::get('edit/{id}', 'Api\Jobs\JobsViewController@getDataCV');
                Route::post('update/{id}', 'Api\Jobs\JobsActionController@updateCv');
            });
            Route::post('verification', 'Api\Jobs\JobsActionController@verificationFromUser');
        });

        Route::group(['prefix' => 'subscribe'], function () {
            Route::get('content', 'Web\LandingContentController@getContent');
        });

        // Room meta for chat feature
        // Ref task: https://mamikos.atlassian.net/browse/BG-524
        Route::get('meta/{id}', 'Api\RoomMetaController@getRoomMeta');

        Route::get('nearby/{id}', 'Api\RoomController@getNearbyBySongId')->middleware('api.auth:device');
    });

    //Kost Level
    Route::group(['prefix' => 'kost-level', 'middleware' => 'web'], function () {
        Route::get('info', 'Api\KostLevelController@index');
        Route::post('{kostId}/approve', 'Api\KostLevelController@approve');
        Route::post('{kostId}/reject', 'Api\KostLevelController@reject');
        Route::post('{kostId}/ack', 'Api\KostLevelController@ack');
    });

    Route::group(['prefix' => 'room'], function () {
        Route::post('list/cluster', 'Api\RoomController@detailCluster')->middleware('web');

        // get Room Type Number
        Route::get('type/{roomTypeId}/number', 'Api\RoomController@getRoomTypeNumber');
    });

    Route::group(['prefix' => 'apartment'], function () {
        Route::get('landing/{slug}', 'Api\LandingApartmentController@show');

        Route::get('project-tags', 'Api\PropertyInputController@getApartmentProjectTags');
        Route::post('suggestion/name', 'Api\ApartmentProjectController@suggestions');
        Route::post('input', 'Api\PropertyInputController@inputApartment')->middleware('web');
        Route::post('request-input', 'Api\PropertyInputController@requestInputPost');

        Route::post('landing/view/{id}', 'Api\LandingApartmentController@viewCount');

        Route::get('project', 'Api\ApartmentProjectController@show');
        Route::post('project/list/{id}/{code}', 'Api\ApartmentController@listApartmentByProject');

        Route::post('list', 'Api\ApartmentProjectController@listSearch');
        Route::post('list/cluster', 'Api\ApartmentProjectController@detailCluster');
        Route::post('cluster', 'Api\ApartmentProjectController@clusterSearch');
    });

    Route::group(['prefix' => 'owner'], function () {
        Route::group(['prefix' => 'data', 'middleware' => 'web'], function () {
            Route::get('profile', 'Api\OwnerDataController@getProfile');
            Route::get('notification', 'Api\OwnerDataController@getNotificationOwner');

            Route::group(['prefix' => 'kos', 'as' => 'kos.'], function () {
                Route::get('/', 'Api\OwnerDataController@getOwnedKos')->name('iklan-saya');
                
                Route::group(['prefix' => 'room-unit', 'as' => 'allotment.'], function() {
                    Route::get('/', 'Api\Room\RoomUnitController@index')->name('index');
                    Route::group(['prefix' => 'single', 'as' => 'single.'], function() {
                        Route::post('/', 'Api\RoomAllotment\RoomAllotmentController@addRoomUnit')->name('add');
                        Route::put('{id}', 'Api\RoomAllotment\RoomAllotmentController@editRoomUnit')->name('update');
                        Route::delete('{id}', 'Api\RoomAllotment\RoomAllotmentController@deleteRoomUnit')->name('delete');
                    });
                    Route::post(
                        'bulk',
                        'Api\RoomAllotment\RoomAllotmentController@updateBulkRoomUnit'
                    )->name('update-bulk');
                });
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('update', 'Api\OwnerDataController@getListKosForUpdate');
                Route::get('kos', 'Api\OwnerDataController@getListKos');
                Route::get('kos/{id}', 'Api\OwnerDataController@detailKosOwner');
                Route::get('apartment', 'Api\OwnerDataController@getListApartment');
                Route::get('apartment/{id}', 'Api\OwnerDataController@detailKosOwner');
                Route::get('active', 'Api\OwnerDataController@getListAllCanPromote');
                Route::get('jobs', 'Api\Jobs\JobsViewController@ownerList');
                Route::group(['prefix' => 'jobs'], function () {
                    Route::group(['prefix' => 'apply'], function () {
                        Route::get('jobs/{id}', 'Api\Jobs\JobsViewController@getListApply');
                        Route::get('{id}', 'Api\Jobs\JobsViewController@detailApply');
                    });
                    Route::get('{slug}/report', 'Api\Jobs\JobsViewController@ownerJobsReport');
                    Route::get('{slug}/deactive', 'Api\Jobs\JobsActionController@deactiveJobsOwner');
                    Route::get('{slug}/active', 'Api\Jobs\JobsActionController@activeJobsOwner');
                    Route::get('{slug}/delete', 'Api\Jobs\JobsActionController@deleteJobsOwner');
                    Route::get('{slug}', 'Api\Jobs\JobsViewController@ownerJobsDetail');
                });
            });
            Route::group(['prefix' => 'survey'], function () {
                Route::post('updateroom', 'Api\OwnerDataController@surveyUpdateRoom');
                Route::post('premium', 'Api\OwnerDataController@surveyPremium');
            });
            Route::group(['prefix' => 'booking'], function () {
                Route::get('/', 'Api\Booking\Owner\BookingController@getListData');
            });
            Route::group(['prefix' => 'kost-level'], function () {
                Route::get('flag-count', 'Api\KostLevelController@flagCount');
                Route::get('list', 'Api\KostLevelController@kostList');
            });

            Route::post('update-room', 'Api\OwnerDataController@updateAllProperty');
            Route::get('income', 'Api\OwnerDataController@getDashboardTotalIncome');
        });
        Route::group(['prefix' => 'edit'], function () {
            Route::post('email', 'Api\OwnerController@editEmail')->middleware('web');
            Route::post('name', 'Api\UserController@editName')->middleware('web');
            Route::post('phone', 'Api\Auth\OwnerController@editPhone')->middleware('web');
            Route::post('phone/request', 'Api\Auth\OwnerController@editPhoneRequest')->middleware('web');
        });
        Route::get('list', 'Api\OwnerController@getListOwner')->middleware('web');
        Route::post('room/save', 'Api\OwnerController@postSaveOwnerDesignerViaWeb')->middleware('web');
        Route::post('room/draft', 'Api\OwnerController@postSaveOwnerDesignerViaWeb')->middleware('web');
        Route::get('edit_room/{id}', 'Api\OwnerController@editRoomOwner')->middleware('web');
        Route::post('update_room/{id}', 'Api\OwnerController@updateRoomByOwner')->middleware('web');
        Route::post('update_room/draft/update/{id}', 'Api\OwnerController@updateRoomByOwner')->middleware('web');
        Route::post('update_room_price', 'Api\OwnerController@updateRoomPriceByOwner')->middleware('web');
        Route::delete('delete_room/{id}', 'Api\OwnerController@deleteRoomByOwner')->middleware('web');
        Route::get('suggested', 'Api\OwnerController@getSuggestedOwnerRoom')->middleware('web');
        Route::get('claim/{id}', 'Api\OwnerController@getClaimOwner');
        Route::post('claim', 'Api\OwnerController@postClaimRoomByOwner')->middleware('web');
        Route::post('search', 'Api\OwnerController@postSearchOwnerRoom')->middleware('web');
        Route::get('survey/{id}', 'Api\SurveyController@editSurveyOwner')->middleware('web');
        Route::post('promotion', 'Api\PromotionController@postPromotion')->middleware('web');
        Route::get('promotion/{id}', 'Api\PromotionController@getPromotionOwner');
        Route::get('information', 'Api\OwnerInformationController@index')->middleware('web');

        Route::group(['prefix' => 'bbk', 'middleware' => 'web'], function () {
            Route::post('register', 'Api\Room\BookingOwnerRequestController@registerBulkBbk');
            Route::get('status', 'Api\Room\BookingOwnerRequestController@getBbkStatus');
        });

        // new flow for update
        Route::get('data/update/{id}', 'Api\OwnerController@getKostDataForUpdate')->middleware('web');
        Route::post('updateroom/{id}', 'Api\PropertyInputController@updateKost')->middleware('web');
        Route::get('data/update/apartment/{id}', 'Api\OwnerController@getApartmentDataForUpdate')->middleware('web');
        Route::post('updateapartment/{id}', 'Api\PropertyInputController@updateApartment')->middleware('web');

        /* history */
        Route::get('call/{id}', 'Api\OwnerController@getListCallRoom')->middleware('web');
        Route::get('review/{id}', 'Api\ReviewController@getOwnerReview')->middleware('web');
        Route::get('called/{id}', 'Api\OwnerController@getChatOwner')->middleware('web');
        Route::get('surveys/{id}', 'Api\SurveyController@getOwnerSurvey')->middleware('web');

        Route::post('survey/update', 'Api\SurveyController@updateSurveyOwner')->middleware('web');
        Route::post('pinned', 'Api\OwnerController@pinnedAction')->middleware('web');
        Route::post('chat/change_name', 'Api\UserController@changeUserChatName')->middleware('web');

        Route::group(['prefix' => 'premium'], function () {
            Route::post('purchase', 'Api\PremiumController@premiumPurchase')->middleware('web');
            Route::post('stop-reason', 'Api\PremiumController@stopReason')->middleware('web');
            Route::post('cancel', 'Api\PremiumController@cancelPurchase')->middleware('web');
            Route::post('trial', 'Api\PremiumController@autoApproveTrial')->middleware('web');
            Route::get('confirmation', 'Api\PremiumController@getConfirmation')->middleware('web');
            Route::post('confirmation', 'Web\PremiumController@postPurchaseConfirmation')->middleware('web');
            Route::post('confirmation/notification', 'Web\PremiumController@postConfirmationNotif')->middleware('web');
            Route::post('confirm', 'Api\PremiumController@postConfirm')->middleware('web');
            Route::get('invoice', 'Api\PremiumController@getInvoice')->middleware('web');
            Route::group(['prefix' => 'allocation', 'middleware' => 'web'], function() {
                Route::get('daily/setting', 'Api\Premium\PremiumAllocationController@settingStatus');
                Route::post('{id}', 'Api\Premium\PremiumAllocationController@allocation');
            });
            Route::group(['prefix' => 'ads', 'middleware' => 'web'], function() {
                Route::get('favorite', 'Api\PremiumController@mostFavorite');
                Route::get('statistic/summary', 'Api\PremiumController@getRoomsStatsisticSummary');
                Route::get('statistic', 'Api\PremiumController@getRoomsStatistic');

                Route::get('statistic/{id}/summary', 'Api\PremiumController@getRoomStatsisticSummary');
                Route::get('statistic/{id}', 'Api\PremiumController@getRoomStatistic');
            });

            Route::group(['prefix' => 'ab-test', 'middleware' => 'web'], function() {
                Route::get('check', 'Api\Premium\AbTestPremiumController@checkOwner');
            });

            Route::post('package/order', 'Api\Premium\OrderPremiumPackageController@buyPremiumPackage')->middleware('web');
        });

        Route::post('request-booking/{id}', 'Api\OwnerController@requestBooking')->middleware('web');

        Route::post('password/set', 'Api\OwnerController@setPassword')->middleware('web');
        Route::post('password/update', 'Api\OwnerController@updatePassword')->middleware('web');

        Route::group(['prefix' => 'verification'], function () {
            Route::post('check', 'Api\OwnerController@postVerificationCheck')->middleware('web');
            Route::post('code', 'Api\OwnerController@postVerification')->middleware('web');
            Route::post('request', 'Api\OwnerController@postRequestVerification')->middleware('web');
            Route::post('store', 'Api\OwnerController@postStoreVerification')->middleware('web');
            Route::post('email', 'Api\OwnerDataController@postVerificationEmail')->middleware('web');
        });

        Route::post('reply', 'Api\OwnerController@autoReply')->middleware('web');

        Route::group(['prefix' => 'survey'], function () {
            Route::post('updateroom', 'Api\OwnerDataController@surveyUpdateRoom')->middleware('web');
        });

        Route::get('search/update', 'Api\OwnerDataController@searchUpdate')->middleware('web');

        Route::post('register-request', 'Api\OwnerRegisterRequestController@registerRequest');

        Route::group(['prefix' => 'booking'], function () {
            // Instant Booking
            Route::post('instant/register', 'Api\Booking\Owner\BookingController@registerInstantBook')->middleware('web');
        });

        Route::group(['prefix' => 'forget'], function () {
            Route::post('password/identifier', 'Api\Auth\OwnerController@forgetPasswordIdentifier')->middleware('web');
            Route::post('password/request', 'Api\Auth\OwnerController@forgetPasswordRequest')->middleware('web');
            Route::post('password/check', 'Api\Auth\OwnerController@forgetPasswordCheck')->middleware('web');
            Route::post('password', 'Api\Auth\OwnerController@forgetPassword')->middleware('web');
        });
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::post('owner/phone/verify', 'Api\AuthController@postVerifyOwner');
        Route::post('owner/register', 'Api\AuthController@postRegisterOwnerViaWeb')->middleware('web');
        Route::post('owner/login', 'Api\AuthController@postLoginOwnerViaWeb')->middleware('web');
        Route::put('owner/password', 'Api\AuthController@putPassword');
        Route::post('storepassword', 'Api\AuthController@postPassword')->middleware('web');
        Route::post('owner/register/auto', 'Api\AuthController@autoRegisterOwner')->middleware('web');
        Route::post('owner/register/verified', 'Api\AuthController@registerOwner')->middleware('web');

        Route::group(['prefix' => 'tenant'], function () {
            Route::post('register', 'Api\Auth\TenantController@register')->middleware('web');
            Route::post('login', 'Api\Auth\TenantController@login')->middleware('web');

            Route::group(['prefix' => 'verification'], function () {
                Route::post('request', 'Api\Auth\TenantController@requestVerification');
                Route::post('check', 'Api\Auth\TenantController@verificationCheck');
            });
        });

        // Verification Code Authentication
        Route::group(['prefix' => 'code'], function () {
            Route::post('request', 'Api\Auth\VerificationCodeController@request');
            Route::post('check', 'Api\Auth\VerificationCodeController@check');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::post('forgot-password', 'Api\Auth\ForgotPasswordController@passwordReset'); // password reset
        });
    });

    Route::get('recommendation', 'Api\RoomController@recommendation')->middleware('api.auth');
    Route::post('media', 'Api\MediaController@postIndex');
    Route::post('subscribe', 'Api\SubscribeController@subscribeFilter');
    Route::post('find-me-apartment', 'Api\SubscribeController@findMeApartment');
    Route::post('owner/contact', 'Api\RoomController@postOwnerContact');
    Route::get('kost/name', 'Api\RoomController@availableName');

    // Prevent token mismatch on login
    Route::get('drip', 'Api\UserController@drip');

    Route::get('profile', 'Api\RoomController@getProfile')->middleware(['web']);
    Route::post('download/link', 'Api\AppController@sendDownloadLink');

    Route::post('premium/request-direction', 'Api\PremiumController@requestDirection');
    Route::post('premium/register-network', 'Api\PremiumController@registerNetwork');

    Route::post('agent-register', 'Api\AgentController@register')->middleware('web');
    Route::post('phone/check', 'Api\UserController@phoneNumberCheck')->middleware('web');

    Route::group(['prefix' => 'setting'], function () {
        Route::get('notif', 'Api\UserController@getSettings')->middleware('web');
        Route::post('notif', 'Api\UserController@changeSettings')->middleware('web');
        Route::post('phone', 'Api\UserDataController@changePhoneNumber')->middleware('web');
    });

    Route::group(['prefix' => 'booking'], function () {
        Route::get('type', 'Api\Booking\BookingDesignerController@getType');
        Route::get('detail/{booking_code}', 'Api\Booking\BookingUserController@detail')->middleware('web');
        Route::post('cancel/{booking_code}', 'Api\Booking\BookingUserController@cancel')->middleware('web');
        Route::post('payment/confirmation/{booking_code}', 'Api\Booking\BookingPaymentController@confirmation')->middleware('web');
        Route::get('history', 'Api\Booking\BookingUserController@history')->middleware('web');
        Route::post('guarantee/claim/{booking_code}', 'Api\Booking\BookingUserController@claimGuarantee')->middleware('web');
        Route::get('voucher/{booking_code}', 'Api\Booking\BookingUserController@getVoucher')->middleware('web');
        Route::get('receipt/{booking_code}', 'Api\Booking\BookingUserController@getReceipt')->middleware('web');
        Route::get('counter', 'Api\Booking\BookingUserController@counter')->middleware('web');

        Route::get('get-reject-reason/{type}', 'Api\Booking\BookingRejectController@getAllByType')->middleware('web');
    });

    Route::post('content/subscribe', 'Api\LandingContentController@subscribe')->middleware('web');

    Route::post('reward/register', 'Api\PropertyInputController@registerRewardNumber')->middleware('web');

    Route::get('facility/{type?}', 'Api\TagController@getFacilities');
    Route::get('tags', 'Api\TagController@index');
    Route::get('filters/promo', 'Api\PromotionController@getPromoFilters');
    Route::get('filters/promo/cities', 'Api\PromotionController@getCitiesPromotedHardCode');

    Route::get('events', 'Api\RoomController@getHome')->middleware('web');

    Route::get('testimony', 'Api\OwnerTestimonialController@getTestimony');

    Route::get('cities', 'Api\HomeStaticController@getRecommendationCities')->middleware('web');

    Route::post('feedback', 'Api\SurveyFeedbackController@submitFeedback')->middleware('web');
    Route::post('brand/survey', 'Api\SurveyBrandAcknowledgementController@submitSurvey');
    Route::post('rent-house/survey', 'Api\FakeDoor\FakeDoorRentHouseController@submitSurvey');

    Route::post('messenger/{event}', 'Api\MessengerController@event');

    Route::post('photobooth/register', 'Api\PhotoboothController@register')->middleware('web');
    Route::post('photobooth/upload', 'Api\PhotoboothController@upload');

    Route::get('location-suggestion', 'Api\Search\LocationSuggestionController@suggestAutocomplete');
    Route::get('landing/suggestion', 'Api\LandingSuggestionBlockController@suggest');

    Route::group(['prefix' => 'general-search'], function () {
        Route::post('/criteria', 'Api\GeneralSearchController@saveCriteria');
        Route::get('/criteria', 'Api\GeneralSearchController@show');
        Route::post('/keyword', 'Api\GeneralSearchController@saveUserInput');
        Route::get('/place/{place_name}', 'Api\GeneralSearchController@getPlaceGeolocation');
    });

    Route::group(['prefix' => 'forum'], function () {
        Route::get('universities', 'Api\Forum\ForumUserController@listUniversities');
        Route::post('user/register', 'Api\Forum\ForumUserController@register')->middleware('web');
        Route::get('user/data/register', 'Api\Forum\ForumUserController@getDataForRegistration')->middleware('web');
        Route::get('user/check-role', 'Api\Forum\ForumUserController@determineRoleFromYearStart');
        Route::get('category/list', 'Api\Forum\ForumCategoryController@list')->middleware('web');
        Route::get('category/option', 'Api\Forum\ForumCategoryController@option');
        Route::get('category/detail/{slug}', 'Api\Forum\ForumCategoryController@show');
        Route::get('thread/list/{slug}', 'Api\Forum\ForumThreadController@list')->middleware('web');
        Route::get('thread/detail/{slug}', 'Api\Forum\ForumThreadController@showThread')->middleware('web');
        Route::post('thread/new', 'Api\Forum\ForumThreadController@createThread')->middleware('web');
        Route::get('thread/answer/list/{slug}', 'Api\Forum\ForumAnswerController@getThreadAnswers')->middleware('web');
        Route::post('thread/answer/{slug}', 'Api\Forum\ForumAnswerController@postAnswer')->middleware('web');
        Route::post('thread/follow/{slug}', 'Api\Forum\ForumThreadController@follow')->middleware('web');
        Route::post('thread/unfollow/{slug}', 'Api\Forum\ForumThreadController@unfollow')->middleware('web');
        Route::post('thread/report/{slug}', 'Api\Forum\ForumThreadController@report')->middleware('web');
        Route::post('answer/voteup/{id}', 'Api\Forum\ForumAnswerController@voteUp')->middleware('web');
        Route::post('answer/votedown/{id}', 'Api\Forum\ForumAnswerController@voteDown')->middleware('web');
        Route::post('answer/report/{id}', 'Api\Forum\ForumAnswerController@report')->middleware('web');
    });

    // Newly created for Search v2 (Web v1.1.0)
    Route::group(['prefix' => 'suggest'], function () {
        Route::get('/', 'Api\Search\SuggestionController@suggest');
        // Prototyping:
        Route::get('normal', 'Api\Search\SuggestionController@normal');
        Route::get('proto', 'Api\Search\SuggestionController@proto');
    });

    Route::get('career/vacancy', 'Api\CareerController@vacancy');

    // Get city name by user location
    // Ref issue: https://mamikos.atlassian.net/browse/BG-361
    Route::group(['prefix' => 'geocode'], function () {
        Route::post('cache', 'Api\GeocodeCacheController@saveGeocodeCache');
        Route::get('cache', 'Api\GeocodeCacheController@getGeocodeCache');
    });

    // Slider endpoint
    // Ref task: https://mamikos.atlassian.net/browse/BG-337
    Route::get('sliders', 'Api\SliderController@getSliders');
    Route::get('slider/{endpoint}', 'Api\SliderController@getSlider');

    // Get chat pretext ID
    // Ref task: https://mamikos.atlassian.net/browse/BG-681
    Route::post('chat-pretext', 'Api\ConfigController@getChatPretext');

    // FLash Sale
    // Ref Task: https://mamikos.atlassian.net/browse/BG-1327
    Route::group(
        ['prefix' => 'flash-sale'],
        function () {
            Route::get('/', 'Api\FlashSale\FlashSaleController@getAllData')->name('all');
            Route::get('get/{id?}', 'Api\FlashSale\FlashSaleController@getData')->name('get');
            Route::get('running', 'Api\FlashSale\FlashSaleController@getRunningData')->name('running');
            Route::get('upcoming', 'Api\FlashSale\FlashSaleController@getUpcomingData')->name('upcoming');
        }
    );

    Route::group(['prefix' => 'room'], function () {
        Route::group(['prefix' => 'goldplus'], function () {
            Route::get('statistics/filters/{option}', 'Api\GoldplusStatisticController@getStatisticFilterList')->middleware('api.auth:user');
            Route::get('{songId}', 'Api\GoldplusStatisticController@getDetailKostGp')->middleware('api.auth:user');
            Route::get('{songId}/statistics/{type}', 'Api\GoldplusStatisticController@getGoldplusStatistic')->middleware('api.auth:user');
        });
    });

    //SANJUNIPERO FOR GARUDA
    Route::get('sanjunipero/list', 'Api\Sanjunipero\ListController@index')->name('.sanjunipero.list-kost');

    //API CS SUPPORT EMAIL
    Route::post('/cs-email','Api\CsSupportEmailController@sendEmail');

    //Singgahsini
    Route::group(['prefix' => 'singgahsini', 'as' => '.singgahsini'], function () {
        Route::post('/register', 'Api\Singgahsini\Registration\RegistrationController@register')->name('.register');
        Route::post('/upload-photo', 'Api\Singgahsini\Registration\RegistrationController@uploadPhoto')->name('.upload-photo');
    });
});

Route::group(['as' => 'web', 'middleware' => ['web']], function () {
    Route::group(['prefix' => 'sewa'], function () {
        Route::get('{slug}', 'Web\HouseProperty\HousePropertyController@detailVilla');
    });
    Route::group(['prefix' => 'disewakan'], function () {
        Route::get('{slug}', 'Web\HouseProperty\HousePropertyController@detailKontrakan');
    });
    Route::get('/', 'Web\HomeController@index');
    Route::get('/cari', 'Web\LandingController@mapSearch');
    Route::get('/cari/{place}/{gender}/{rentType}/{priceRange}', 'Web\LandingController@mapSearch');
    Route::get('carikos', 'Web\RoomController@carikos');

    Route::get('login', 'Web\AuthController@login')->name('.login');
    Route::get('login-pemilik', 'Web\AuthController@login');
    Route::get('login-pencari', 'Web\AuthController@login');
    Route::get('register-pemilik', 'Web\AuthController@login');
    Route::get('register-pencari', 'Web\AuthController@login');
    Route::get('lupa-password-pemilik', 'Web\AuthController@login');
    Route::get('lupa-password-pencari', 'Web\AuthController@login');

    Route::get('login-old', 'Web\AuthController@loginOld');
    Route::get('area', 'Web\MamikosController@landingArea');
    Route::get('kampus', 'Web\MamikosController@landingAreaCampus');
    Route::get('room/{slug}', 'Web\RoomController@show')->name('.room.detail');
    Route::get('room/{slug}/date', 'Web\RoomController@show')->name('.room.detail');
    Route::get('room/{slug}/duration', 'Web\RoomController@show')->name('.room.detail');
    Route::get('room/{slug}/add', 'Web\RoomController@show')->name('.room.detail');
    Route::get('room/{slug}/preview', 'Web\RoomController@show')->name('.room.preview');
    Route::get('booking-detail', 'Web\RoomController@bookingUser');

    Route::get('daftar-agen', 'Web\MamikosController@registerAgentRedirect');
    Route::get('loker/daftar-agen', 'Web\MamikosController@landingAgent');

    Route::get('kost/indekos/{gender?}/{rent_type?}/{area?}', 'Web\LandingController@allKost');
    Route::get('kost/{slug}', 'Web\LandingController@show')->name('.landing');
    Route::get('villa/{slug}', 'Web\HouseProperty\LandingController@villa');
    Route::get('kontrakan/{slug}', 'Web\HouseProperty\LandingController@kontrakan');
    Route::get('kost/infokost/{slug}', 'Web\ListingController@showKost');

    Route::get('loker/{slug}', 'Web\Vacancy\VacancyController@landingNiche');
    Route::get('loker/1/{slug}', 'Web\Vacancy\VacancyController@vacancy');
    Route::get('loker/pekerjaan/{area?}/{education?}/{status?}/{criteria?}', 'Web\Vacancy\VacancyController@allJob');

    Route::get('loker/1/post/{slug}', 'Web\LandingContentController@showLandingContentVacancy');
    Route::get('loker/list/{slug}', 'Web\ListingController@show');
    Route::get('loker/1/list/{slug}', 'Web\ListingController@showGeneral');

    Route::get('lowongan/{slug}', 'Web\Vacancy\VacancyController@vacancyDetail');
    Route::get('perusahaan/profil/{slug}', 'Web\Vacancy\VacancyController@companyProfile');
    Route::get('perusahaan/lowongan/{slug}', 'Web\Vacancy\VacancyController@companyVacancy');
    Route::get('perusahaan', 'Web\Vacancy\VacancyController@companyLanding');
    Route::get('lowongan/1/{slug}', 'Web\Vacancy\VacancyController@vacancyDetailGeneral');
    Route::get('terbaru', 'Web\LandingController@newestLive');
    Route::get('terbaru/{slug}', 'Web\LandingController@newestLive');
    Route::get('promo-kost', 'Web\LandingController@promoKost');

    Route::get('daftar', 'Web\LandingApartmentController@redirectToApartment');
    Route::get('daftar/{slug}', 'Web\LandingApartmentController@show')->name('.landing-apartment');
    Route::get('unit/{apartmentSlug}/{slug}', 'Web\ApartmentController@show');
    Route::get('project/{area}/{slug}', 'Web\ApartmentProjectController@redirector');
    Route::get('apartemen/post/{slug}', 'Web\LandingContentController@showLandingContentApartment');
    Route::get('apartemen/sewa/{area?}/{rent_type?}/{furnished?}/{criteria?}', 'Web\ApartmentController@allApartment');
    Route::get('apartemen/disewakan/{slug}', 'Web\ListingController@showApartment');
    Route::get('apartemen/{area}/{slug}', 'Web\ApartmentProjectController@show')->name('.apartment-project');

    // fakedoor
    Route::get('cari-kontrakan', 'Web\PromoController@kontrakanUser');
    Route::redirect('pasang-iklan-kontrakan', '/');

    Route::get('promosi-kost/premium-package', 'Web\PremiumController@premiumPackage');
    Route::redirect('/ownerpage/premium-package', '/promosi-kost/premium-package');

    Route::group(['middleware' => ['webview.auth'], 'prefix' => 'ownerpage/manage'], function () {
        Route::group(['prefix' => '/add-tenant'], function() {
            Route::get('/check-phone', 'Web\OwnerController@mamipay');
            Route::get('/select-kost', 'Web\OwnerController@mamipay');
            Route::get('/tenant-biodata', 'Web\OwnerController@mamipay');
            Route::get('/payment-others', 'Web\OwnerController@mamipay');
            Route::get('/payment-others-settings', 'Web\OwnerController@mamipay');
            Route::get('/payment-info', 'Web\OwnerController@mamipay');
            Route::get('/payment-detail', 'Web\OwnerController@mamipay');
        });
    });

    Route::group(['middleware' => ['auth']], function () {
        //Single Page Routing
        Route::get('ownerpage', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/settings', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/settings/document-verification', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/add', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/topup', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/notification', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/active', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/kos', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/kos/update', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/kos/{id}', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/apartment', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/apartment/update', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/apartment/{id}', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/vacancy', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/vacancy/{slug}', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/vacancy/{slug}/{id}', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/vacancy/{slug}/{id}/{number}', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/premium/purchase/confirmation', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/premium/balance/confirmation', 'Web\OwnerController@ownerpage');

        Route::get('ownerpage/invoice', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/payment', 'Web\OwnerController@ownerpage');
        Route::get('ownerpage/payment/confirmation', 'Web\OwnerController@paymentConfirmation');


        Route::get('ownerpage/addpromo/{id}', 'Web\OwnerController@ownerPromo');

        //End of Single Page Routing


        Route::group(['prefix' => 'ownerpage/billing-management'], function () {
            Route::get('/', 'Web\OwnerController@billingManagement');

            Route::get('/profile-tenant/{contractId}/payment-tenant', 'Web\OwnerController@billingManagement');
        });

        Route::group(['prefix' => 'ownerpage/manage'], function () {
            Route::get('/', 'Web\OwnerController@mamipay');
            Route::get('/activate', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{tenantId}/biodata-tenant/new-member', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{tenantId}/billing-tenant/new-member', 'Web\OwnerController@mamipay');

            Route::get('/{roomId}/bill', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{contractId}/payment-tenant', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{contractId}/biodata-tenant', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{contractId}/billing-tenant', 'Web\OwnerController@mamipay');

            Route::get('/{roomId}/booking', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{bookingId}/biodata-tenant/accept-booking', 'Web\OwnerController@mamipay');
            Route::get('/profile-tenant/{bookingId}/billing-tenant/accept-booking', 'Web\OwnerController@mamipay');

            Route::get('/{roomId}/room-price', 'Web\OwnerController@mamipay');

            Route::get('/all/property-list', 'Web\OwnerController@mamipay');
        });
        Route::any('ownerpage/mamipay/request/{path}', 'Web\OwnerController@mamipayCallApi')->where('path', '.+');

        Route::get('ownerpage/statistics/{id}', 'Web\OwnerController@getStatistics');
        Route::get('ownerpage/edit-kos/{id}', 'Web\OwnerController@editKost');
        Route::get('ownerpage/edit-apartment/{id}', 'Web\OwnerController@editApartment');
        Route::get('ownerpage/edit-vacancy/{slug}', 'Web\PropertyInputController@editVacancy');

        Route::get('ownerpage/premium/balance', 'Web\PremiumController@balance');
        Route::post('ownerpage/premium/balance', 'Web\PremiumController@postBalance')->name('.buy_balance');

        Route::get('ownerpage/issue-token', 'Web\OwnerController@issueToken')->name('owner.issue.token.from.session');
    });

    Route::get('ownerpage/issue-token-webview', 'Web\OwnerController@issueToken')->middleware('webview.auth')->name('owner.issue.token.for.webview');

    Route::group(['prefix' => 'career'], function () {
        Route::get('/', 'Web\MamikosController@career');
    });

    Route::get('booking/owner', 'Web\Booking\BookingWebController@bookingOwner');
    Route::get('booking/owner/rekeningpemilik', 'Web\Booking\BookingWebController@ownerRekening');
    Route::group(['prefix' => 'booking', 'as' => 'booking'], function () {
        Route::get('/', 'Web\Booking\BookingWebController@index')->name('.index');
        Route::get('/owner/terms-and-conditions', 'Web\Booking\BookingWebController@termsAndConditions');
        Route::get('/{slug}/{parent?}', 'Web\Booking\BookingWebController@index');
    });

    Route::group(['prefix' => 'forum'], function () {
        Route::get('/', 'Web\Forum\ForumController@index');
        Route::get('/{category_slug}', 'Web\Forum\ForumController@category');
        Route::get('/{category_slug}/{thread_slug}', 'Web\Forum\ForumController@thread');
    });
    // shortlink
    Route::get('s/o/{type}', 'Web\OwnerController@registerStatistic');
    Route::get('s/loginpulsa', 'Web\PropertyInputController@shortlinkFormInputKostLogin');
    Route::get('s/pulsa', 'Web\PropertyInputController@shortlinkFormInputKost');
    Route::get('s/reward', 'Web\PropertyInputController@shortlinkFormReward');
    Route::get('s/gp4-invoice/{id}', 'Web\PremiumController@shortlinkGP4WebInvoice');

    Route::get('owner/tutorial/{type}', 'Web\OwnerController@tutorial');

    Route::get('input-properti', 'Web\PropertyInputController@redirectNonPost');
    Route::post('input-properti', 'Web\PropertyInputController@inputProperti');

    Route::get('form', 'Web\AgentController@form');
    Route::get('web/bannerpremium', 'Web\AgentController@BannerPremium');
    Route::get('agen', 'Web\AgentController@formAgen');
    Route::get('agen2', 'Web\AgentController@formAgen2');

    Route::get('promosi-kost', 'Web\MamikosController@promosiKost');
    Route::get('promo', 'Web\PromoController@promo');
    Route::get('promo/nonton-gratis', 'Web\PromoController@nontonGratis');

    Route::get('kost-harian', 'Web\PromoController@promokostharian');
    Route::get('kost-harian/{city?}', 'Web\PromoController@promokostharian');
    Route::get('kost-bebas', 'Web\PromoController@promokostbebas');
    Route::get('kost-bebas/{city?}', 'Web\PromoController@promokostbebas');
    Route::get('kost-kamar-mandi-dalam', 'Web\PromoController@promokostkmdalam');
    Route::get('kost-kamar-mandi-dalam/{city?}', 'Web\PromoController@promokostkmdalam');
    Route::get('kost-pasutri', 'Web\PromoController@promokostpasutri');
    Route::get('kost-pasutri/{city?}', 'Web\PromoController@promokostpasutri');

    Route::get('apartemen', 'Web\ApartmentController@allApartment');
    Route::get('loker', 'Web\Vacancy\VacancyController@allJob');
    Route::get('kost', 'Web\LandingController@allKost');
    Route::get('all-kost/{city}', 'Web\LandingController@allKostCity');

    Route::get('foto-wisuda', 'Web\PhotoboothController@graduationPhoto');
    Route::get('foto-wisuda/download/{voucher}', 'Web\PhotoboothController@download');
    Route::get('foto-wisuda/download', 'Web\PhotoboothController@downloadConfirmation');
    Route::get('foto-wisuda/upload', 'Web\PhotoboothController@upload');

    Route::get('standarisasi-kost', 'Web\MamikosController@landingStandard');

    Route::get('mamirooms', 'Web\MamiroomsController@show');
    Route::get('infokost', 'Web\MamikosController@infokost');
    Route::get('aboutus', 'Web\MamikosController@aboutUs');
    Route::get('tentang-kami', 'Web\MamikosController@tentangKami');
    Route::get('privacy', 'Web\MamikosController@privacyPolicy');
    Route::get('input-faq', 'Web\MamikosController@frequentlyAskedQuestion');

    Route::redirect('carikan-kost', '/');
    Route::redirect('carikan-apartemen', '/');
    Route::get('download-soal', 'Web\MamikosController@downloadSoal');
    Route::get('download-soal/download/{id}', 'Web\DownloadExamController@download');
    Route::get('download-soal/{slug}', 'Web\DownloadExamController@show');
    Route::get('download-soal-cpns', 'Web\MamikosController@downloadSoalKerja');
    Route::get('download-bbm', 'Web\MamikosController@downloadBbm');
    Route::get('download-bbm/{type}', 'Web\MamikosController@redirectBbm');
    Route::get('pengumuman-hasil-sbmptn', 'Web\MamikosController@pengumumanHasil');
    Route::get('pendaftaran-seleksi-mandiri', 'Web\MamikosController@pendaftaranSeleksiMandiri');
    Route::get('jadwal-daftar-ulang-kampus', 'Web\MamikosController@jadwalDaftarUlang');
    Route::get('rute-lengkap-dan-jadwal-terbaru-kereta-krl-commuter-line-jabodetabek', 'Web\MamikosController@jadwalRuteCommuter');
    Route::get('download-soal/link/{type}', 'Web\MamikosController@downloadSoalLink')->middleware(['web']);
    Route::get('hadiahpulsa', 'Web\PromoController@hadiahpulsa');
    Route::get('pulsa-gratis', 'Web\PromoController@hadiahpulsaKaskus');

    Route::get('cek-properti', 'Web\MamikosController@checkProperty');

    Route::get('search', 'Web\RoomController@customSearch3');

    Route::get('konten', 'Web\LandingContentController@redirector');
    Route::get('konten/{slug}', 'Web\LandingContentController@show');
    Route::get('konten/download/{id}', 'Web\LandingContentController@download')->middleware('web');

    Route::get('input-kost', 'Web\PropertyInputController@inputKost');
    Route::get('form-agen', 'Web\PropertyInputController@inputAgent');
    Route::redirect('form-singkat', '/');
    Route::redirect('form-referral', '/');

    Route::get('input-loker', 'Web\PropertyInputController@inputVacancy');

    Route::group(['as' => 'utm'], function () {
        Route::get('twitterjkt', 'Web\LinkController@twitterjkt');
        Route::get('twitterjaksel', 'Web\LinkController@twitterjaksel');
        Route::get('twitterjakbar', 'Web\LinkController@twitterjakbar');
        Route::get('twitterjakpus', 'Web\LinkController@twitterjakpus');
    });

    Route::group(['as' => 'download'], function () {
        Route::get('app', 'Web\LinkController@app');
        Route::get('APPIG', 'Web\LinkController@APPIG');
        Route::get('APPTWITTER', 'Web\LinkController@APPTWITTER');
        Route::get('APPFB', 'Web\LinkController@APPFB');
        Route::get('LINE', 'Web\LinkController@LINE');
        Route::get('APPBLOG', 'Web\LinkController@APPBLOG');
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::post('{type}', 'Web\AuthController@redirectToProvider');
        Route::post('{type}/callback', 'Web\AuthController@handlePostProviderCallback');
        Route::get('{type}/callback', 'Web\AuthController@handleProviderCallback');
        Route::get('redirect', 'Web\AuthController@handleRedirect');
        Route::get('logout', 'Web\RoomController@logout');
    });

    Route::get('unsubscribe', 'Web\MamikosController@emailUnsubscribe');
    Route::get('ratecs/{encodedData}', 'Web\MamikosController@rateCS');
    Route::post('ratecs', 'Web\MamikosController@rateCSPost');

    Route::group(['prefix' => 'amp'], function () {
        Route::get('kost/{slug}', 'Web\LandingController@showAmp')->name('.landingamp');
    });

    Route::group(['prefix' => 'success'], function () {
        Route::get('sukses-email-verifikasi', 'Web\UserController@successEmailVerification');
    });

    Route::group(['prefix' => 'webview'], function () {
        Route::get('booking-syarat-ketentuan', 'Web\MamikosController@webviewBookingTermsCondition');
        Route::get('kebijakan-privasi', 'Web\MamikosController@webviewPrivacyPolicy');
        Route::get('request-booking', 'Web\MamikosController@requestBooking');
        Route::get('scoreboard', 'Web\MamikosController@scoreboard');
        Route::get('brand-survey', 'Web\MamikosController@brandSurvey');
        Route::get('chat-cs', 'Web\MamikosController@chatCs');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Web\UserController@index');
        Route::get('verifikasi-akun', 'Web\UserController@index');
        Route::get('verifikasi-identitas', 'Web\UserController@index');
        Route::get('edit-profil', 'Web\UserController@index');
        Route::get('kost-saya', 'Web\UserController@index');
        Route::get('mamipoin', 'Web\UserController@index');
        Route::get('mamipoin/history', 'Web\UserController@index');
        Route::get('mamipoin/expired', 'Web\UserController@index');
        Route::get('mamipoin/guideline', 'Web\UserController@index');
        Route::get('voucherku', 'Web\UserController@index');
        Route::get('voucherku/detail', 'Web\UserController@index');
        Route::get('cari-kost', 'Web\UserController@index');
        Route::get('booking', 'Web\UserController@index');
        Route::get('booking/draft', 'Web\UserController@index');
        Route::get('booking/last-seen', 'Web\UserController@index');
        Route::get('booking-detail', 'Web\UserController@index');
        Route::get('riwayat-kos', 'Web\UserController@index');
        Route::get('riwayat-kos/{contractId}', 'Web\UserController@index');
        Route::get('riwayat-transaksi', 'Web\UserController@index');
        Route::get('riwayat-transaksi/{id}', 'Web\UserController@index');
        Route::get('aktivasi-mamipay', 'Web\UserController@index');
        Route::get('edit', 'Web\UserController@index');
        Route::get('search', 'Web\UserController@index');
        Route::get('pengaturan', 'Web\UserController@index');
        Route::any('mamipay/request/{path}', 'Web\OwnerController@mamipayCallApi')->where('path', '.+');
    });

    Route::group(['prefix' => 'history'], function () {
        Route::get('/', 'Web\UserController@history');
        Route::get('view', 'Web\UserController@history');
        Route::get('favourite', 'Web\UserController@history');
    });

    Route::redirect('input/{type}', '/'); // fallback for old `/input/*` page

    Route::get('test-qa', 'Web\TestQaController@index');
    Route::post('test-qa/general', 'Web\TestQaController@saveGeneralProfile');
    Route::post('test-qa/cv', 'Web\TestQaController@saveCV');
    Route::get('test-qa/remove-input', 'Web\TestQaController@removeInputCookie');

    // DBET Tenant Route
    Route::group(['prefix' => 'ob'], function () {
        Route::group(['prefix' => 'confirmhere'], function() {
            Route::get('/', 'Web\DbetTenant\DbetTenantWebController@confirmRedirectIndex');
            Route::get('/dbet-{code}', 'Web\DbetTenant\DbetTenantWebController@confirmRedirectCode');
        });

        Route::get('/dbet-{code}', 'Web\DbetTenant\DbetTenantWebController@index');
        Route::get('/dbet-{code}/onboarding', 'Web\DbetTenant\DbetTenantWebController@index');

        // Auth required
        Route::get('/dbet-{code}/form', 'Web\DbetTenant\DbetTenantWebController@form');
        Route::get('/dbet-{code}/biodata', 'Web\DbetTenant\DbetTenantWebController@form');
        Route::get('/dbet-{code}/success', 'Web\DbetTenant\DbetTenantWebController@form');
    });

    /*
     * Flash Sale
     * Ref task: https://mamikos.atlassian.net/browse/BG-1377
     */
    Route::group(
        ['prefix' => 'promo-ngebut', 'as' => 'flash-sale'],
        function () {
            Route::get('/{area?}', 'Web\FlashSale\FlashSaleWebController@index')->name('.index');
        }
    );

    Route::group(
        ['prefix' => 'kost-promo-ngebut', 'as' => 'flash-sale-list'],
        function () {
            Route::get('/{area?}', 'Web\FlashSale\FlashSaleWebController@index')->name('.index');
        }
    );

    //Sanjunipero route
    Route::group(['prefix' => 'kos', 'middleware' => 'web', 'as' => '.sanjunipero'], function () {
        Route::get('/', 'Web\Sanjunipero\LandingController@index')->name('.index');
        Route::get('/{parent}', 'Web\Sanjunipero\LandingController@parent')->name('.parent');
        Route::get('/{parent}/{child}', 'Web\Sanjunipero\LandingController@child')->name('.child');
    });

    //Billing Management LP
    Route::get('manajemen-kos', 'Web\Billing\LandingController@index')->name('.billing');

    //Singgahsini
    Route::group(['prefix' => 'singgahsini', 'middleware' => 'web', 'as' => '.singgahsini'], function () {
        Route::get('/', 'Web\Singgahsini\LandingController@index')->name('.index');

        Route::group(['prefix' => 'daftar', 'middleware' => 'web', 'as' => '.register'], function () {
            Route::get('/', 'Web\Singgahsini\LandingController@register')->name('.index');
            Route::get('/data-pemilik', 'Web\Singgahsini\LandingController@register')->name('.owner-data');
            Route::get('/data-alamat', 'Web\Singgahsini\LandingController@register')->name('.address-data');
            Route::get('/data-kos', 'Web\Singgahsini\LandingController@register')->name('.kost-data');
            Route::get('/data-foto', 'Web\Singgahsini\LandingController@register')->name('.photos-data');
            Route::get('/data-fasilitas', 'Web\Singgahsini\LandingController@register')->name('.facility-data');
        });
    });
});

Route::group(['prefix' => 'agent'], function () {
    Route::get('code/{code}', 'Web\AgentController@agentCode');
    Route::get('verified/{phone}', 'Web\AgentController@verifiedRoom');
});

// Currently we'll turn down this page
// Route::post('carikan-kost', 'Api\FinderController@storeFinder');

// input password by owner
Route::get('p/{id}', 'Web\AuthController@passwordPage')->middleware('web');
Route::get('pp', 'Web\MamikosController@redirectToOwnerPage');

// Dummy Image Route
Route::get('kost/image/{slug}/{card}/{size}', 'Web\LinkController@kostImage');

Route::group(['prefix' => 'lab', 'middleware' => 'access.ip'], function () {
    Route::get('notif/{userId}', 'Web\LabController@sendNotif');
});

Route::get('app/{type}', 'Web\MamikosController@download');
Route::get('download-app', 'Web\MamikosController@downloadAppBanner');

// Prevent token mismatch on login
Route::get('drip', 'Api\UserController@drip')->middleware('web');

Route::group(['prefix' => 'verification'], function () {
    Route::get('email/{id}', 'Api\OwnerDataController@verificationEmailAction');
    Route::get('user/email/{id}', 'Api\UserVerificationAccountController@verificationEmail');
});

// short link general
Route::get('/s/{shortcode}', 'Api\ShortlinkController@redirectUrl')->where('shortcode', '(\w|\/|\-)+');

Route::get('/version', 'Web\ServiceVersionController@show');

Route::get('/vrtour/matterport/{id}', 'Web\VirtualTourFrameController@show');

// Route for logs viewer
Route::get('logs', [
    'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
    'middleware' => 'auth.very_basic'
]);

Route::group(['prefix' => 'channel', 'middleware' => 'web'], function () {
    Route::post('/', 'Api\SendBird\SendBirdController@createChannel');
});