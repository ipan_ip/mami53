<?php

Route::post('sinch/call', 'Webhook\SinchController@handleEvent');

Route::get('messenger', 'Webhook\MessengerController@handleGetWebhook');
Route::post('messenger', 'Webhook\MessengerController@handlePostWebhook');

Route::post('stories/{id}/recount-sort-score', 'Api\RoomController@recountSortScore');
Route::post('stories/{id}/index-kost-search', 'Api\RoomController@indexKostSearch');

Route::group(['prefix' => 'booking'], function ()
{
	Route::post('new-request', 'Webhook\WhatsAppNotificationController@newBookingRequested');
	Route::post('accepted', 'Webhook\WhatsAppNotificationController@requestAccepted');

	// recurring invoice unpaid notification
    Route::post('recurring/unpaid-invoice', 'Webhook\WhatsAppNotificationController@recurringUnpaidInvoice');

    // For paid payments
	Route::group(['prefix' => 'paid'], function ()
	{
		Route::post('down-payment', 'Webhook\WhatsAppNotificationController@paidDownPayment');
		Route::post('settlement', 'Webhook\WhatsAppNotificationController@paidSettlement');
		Route::post('full-payment', 'Webhook\WhatsAppNotificationController@paidFullPayment');
		Route::post('recurring', 'Webhook\WhatsAppNotificationController@paidRecurring');

		// For paid out payments to owners
		Route::group(['prefix' => 'out'], function ()
		{
			Route::post('down-payment', 'Webhook\WhatsAppNotificationController@paidOutDownPayment');
			Route::post('settlement', 'Webhook\WhatsAppNotificationController@paidOutSettlement');
			Route::post('full-payment', 'Webhook\WhatsAppNotificationController@paidOutFullPayment');
			Route::post('recurring', 'Webhook\WhatsAppNotificationController@paidOutRecurring');
		});

		Route::post('success', 'Webhook\WhatsAppNotificationController@paidBooking');
	});
});


Route::group(['prefix' => 'activity', 'as' => '.activity.'], function ()
{
	Route::group(['prefix' => 'infobip', 'as' => 'infobip.'], function()
	{
		Route::post('delivery-report/{activationCodeId}', 'Webhook\Activity\InfobipController@postDeliveryReport')->name('delivery-report');
	});
});
