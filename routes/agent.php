<?php
// App for agent
Route::group(['prefix' => 'giant'], function() {
    Route::post('verification', 'Api\Giant\RegisterController@postVerification')->middleware('api.giant');
    Route::post('login', 'Api\Giant\RegisterController@postLogin')->middleware('api.giant');
    Route::get('list', 'Api\Giant\RegisterController@listData')->middleware('api.giant');
    Route::post('checkin', 'Api\Giant\DataController@postCheckin')->middleware('api.giant');
    Route::post('data', 'Api\Giant\DataController@postData')->middleware('api.giant');
    Route::post('media', 'Api\Giant\DataController@postMediaGiant')->middleware('api.giant');
    Route::post('review', 'Api\Giant\DataController@addMasterReview')->middleware('api.giant');
    Route::get('edited', 'Api\Giant\RegisterController@getEdited')->middleware('api.giant');
    Route::group(['prefix' => 'edit', 'middleware' => 'api.giant'], function() {
        Route::get('{id}', 'Api\Giant\DataController@getEditData');
    });
    Route::group(["prefix" => 'delete', 'middleware' => 'api.giant'], function() {
        Route::post('photo/{id}', 'Api\Giant\UpdateController@deletePhoto');
    });
    Route::group(["prefix" => 'update', 'middleware' => 'api.giant'], function() {
        Route::post('checkin', 'Api\Giant\UpdateController@updateCheckin');
        Route::post('review/{id}', 'Api\Giant\UpdateController@updateReview');
    });
    Route::group(['prefix' => 'photos', 'middleware' => 'api.giant'], function() {
        Route::get('{id}', 'Api\Giant\DataController@getListPhoto');
    });
});