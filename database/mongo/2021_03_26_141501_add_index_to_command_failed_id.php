<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCommandFailedId extends Migration
{
    public function up()
    {
        Schema::connection('mongodb')->table('command_failed_id', function(Blueprint $collection) {
            $collection->index(['command_id', 'session_id'], 'command_id_session_id_index');
        });
    }

    public function down()
    {
        Schema::connection('mongodb')->table('command_failed_id', function(Blueprint $collection) {
            $collection->dropIndex('command_id_session_id_index');
        });
    }
}