<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Entities\Notif\Log\NotificationChannel;
use App\Entities\Notif\Log\NotificationSubchannel;

class CreateNotificationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // this is just for reference, this table exist in mongodb where migration is unecessary.
        return;

        Schema::create('notification_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('notification_id')->nullable();
            $table->unsignedInteger('user_id'); // Receiver of the notification
            $table->enum('channel', [
                NotificationChannel::Email,
                NotificationChannel::SMS,
                NotificationChannel::Push,
            ]);
            $table->enum('subchannel', [
                NotificationSubchannel::PushAPNS,
                NotificationSubchannel::PushGCM,
                NotificationSubchannel::Infobip,
                NotificationSubchannel::MoEngage,
            ]);
            $table->string('target'); // email => email address, sms => phone number, push => device token
            $table->enum('status', ['success', 'pending', 'failed']);
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // this is just for reference
        return;
        
        Schema::dropIfExists('notification_log');
    }
}
