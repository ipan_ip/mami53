<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCommandTracker extends Migration
{
    public function up()
    {
        Schema::connection('mongodb')->table('command_tracker', function(Blueprint $collection) {
            $collection->unique(['command_id', 'session_id'], 'command_id_session_id_unique');
        });
    }

    public function down()
    {
        Schema::connection('mongodb')->table('command_tracker', function(Blueprint $collection) {
            $collection->dropIndex('command_id_session_id_unique');
        });
    }
}