<?php
use Faker\Generator as Faker;

$factory->define(
    \App\Entities\Chat\ChatCondition::class,
    function (Faker $faker) {
        return [
            'id'        => $faker->numberBetween(1, 999),
            'chat_question_id'  => $faker->numberBetween(1, 999),
            'chat_criteria_ids' => '1,2,3',
            'reply' => $faker->text,
            'order' => $faker->numberBetween(0, 10)
        ];
    }
);
