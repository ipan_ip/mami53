<?php
use Faker\Generator as Faker;
use App\Entities\Chat\Call;

$factory->define(
    Call::class,
    function (Faker $faker) {
        return [
            'id'        => mt_rand(111, 999999),
            'add_from'  => null,
            'type'      => 'callme',
            'created_at'    => date('Y-m-d H:i:s'),
            'email'     => $faker->email,
            'user_id'   => mt_rand(1, 999999),
            'designer_id'   => mt_rand(11111, 999999),
            'device_id' => mt_rand(1, 999999),
            'name'      => $faker->name,
            'phone'     => $faker->phoneNumber,
            'note'      => str_random(16),
            'status'    => true,
            'owner_sent'    => 'true',
            'reply_status'  => 'true',
        ];
    }
);
