<?php
use Faker\Generator as Faker;
use App\Entities\Chat\Channel;

$factory->define(
    Channel::class,
    function (Faker $faker) {
        return [
            'global_id'  => $faker->uuid,
            'room_id'  => rand(1, 999999),
            'last_message_at'  => $faker->dateTime,
            'expired_at' => null
        ];
    }
);
