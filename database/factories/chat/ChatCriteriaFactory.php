<?php
use Faker\Generator as Faker;
use App\Entities\Chat\Criteria;

$factory->define(
    Criteria::class,
    function (Faker $faker) {
        return [
            'id'          => $faker->numberBetween(1, 999),
            'name'        => $faker->name,
            'attribute'   => $faker->name,
            'description' => $faker->text
        ];
    }
);
