<?php

use App\Entities\Apartment\ApartmentProjectStyle;
use Faker\Generator as Faker;

$factory->define(
    ApartmentProjectStyle::class,
    function (Faker $faker) {
        return [
            'apartment_project_id' => $faker->numberBetween(1, 100000),
            'type' => $faker->numberBetween(1, 100000),
            'photo_id' => $faker->numberBetween(1, 100000),
            'title' => $faker->streetName
        ];
    }
);
