<?php

use App\Entities\Apartment\ApartmentProjectSlug;
use Faker\Generator as Faker;

$factory->define(
    ApartmentProjectSlug::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug,
            'area_city' => $faker->city
        ];
    }
);
