<?php

use App\Entities\Apartment\ApartmentProject;
use Faker\Generator as Faker;

$factory->define(
    ApartmentProject::class,
    function (Faker $faker) {
        return [
            'name' => $faker->name,
            'project_code' => $faker->regexify('[A-Z]{6}'),
            'address' => $faker->address,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'area_city' => $faker->city,
            'area_subdistrict' => $faker->citySuffix,
            'floor' => $faker->numberBetween(10, 100),
            'unit_count' => $faker->numberBetween(100, 1000)
        ];
    }
);

$factory->state(
    ApartmentProject::class,
    'active',
    [
        'is_active' => 1
    ]
);

$factory->state(
    ApartmentProject::class,
    'inactive',
    [
        'is_active' => 0
    ]
);
