<?php

use App\Entities\Vacancy\Spesialisasi;

$factory->define(
    Spesialisasi::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
        ];
    }
);

$factory->state(
    Spesialisasi::class,
    'active',
    [
        'is_active' => 1
    ]
);

$factory->state(
    Spesialisasi::class,
    'general',
    [
        'group' => 'general'
    ]
);

$factory->state(
    Spesialisasi::class,
    'niche',
    [
        'group' => 'niche'
    ]
);