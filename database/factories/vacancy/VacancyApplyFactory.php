<?php

use App\Entities\Vacancy\VacancyApply;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(
    VacancyApply::class,
    function (Faker $faker) {
        $createdAt = Carbon::now()->subDays(5);
        return [
            'name' => $faker->name,
            'skill' => $faker->sentence,
            'experience' => $faker->sentence,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'last_salary' => $faker->numberBetween(1000000, 10000000),
            'expectation_salary' => $faker->numberBetween(1000000, 10000000),
            'status' => 'waiting',
            'file' => $faker->md5 . '.pdf',
            'device' => 'desktop',
            'created_at' => $createdAt,
            'updated_at' => $createdAt,
            'workplace_status' => 1,
        ];
    }
);