<?php

use App\Entities\Vacancy\Industry;

$factory->define(
    Industry::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
        ];
    }
);

$factory->state(
    Industry::class,
    'active',
    [
        'is_active' => 1
    ]
);