<?php

use App\Entities\Vacancy\CompanyProfile;

$factory->define(
    CompanyProfile::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'slug' => $faker->slug
        ];
    }
);

$factory->state(
    CompanyProfile::class,
    'active',
    [
        'is_active' => 1
    ]
);