<?php

use App\Entities\Vacancy\Vacancy;
use Faker\Generator as Faker;

$factory->define(
    Vacancy::class,
    function (Faker $faker) {
        return [
            'place_name' => $faker->country,
            'salary' => mt_rand(11111, 99999),
            'max_salary' => mt_rand(111111, 999999),
        ];
    }
);

$factory->state(
    Vacancy::class,
    'active',
    [
        'status' => Vacancy::VERIFIED,
        'is_expired' => false,
        'is_active' => true
    ]
);

$factory->state(
    Vacancy::class,
    'full-time',
    [
        'type' => 'full-time'
    ]
);

$factory->state(
    Vacancy::class,
    'part-time',
    [
        'type' => 'part-time'
    ]
);

$factory->state(
    Vacancy::class,
    'magang',
    [
        'type' => 'magang'
    ]
);

$factory->state(
    Vacancy::class,
    'freelance',
    [
        'type' => 'freelance'
    ]
);