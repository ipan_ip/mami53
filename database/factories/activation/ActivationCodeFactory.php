<?php

use App\Http\Helpers\ApiHelper;
use Carbon\Carbon;

$factory->define(
    App\Entities\Activity\ActivationCode::class,
    function (Faker\Generator $faker) {
        return [
            "phone_number" => ('08' . $faker->randomNumber(8)),
            "device" => $faker->randomElement(['desktop', 'android', 'tablet']),
            "code" => ApiHelper::random_string('numeric', '4'),
            "for" => null,
            "expired_at" => Carbon::now()->addHours(6)->format('Y-m-d H:i:s'),
            "created_at" => Carbon::now(),
        ];
    }
);