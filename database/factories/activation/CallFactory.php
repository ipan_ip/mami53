<?php

use Carbon\Carbon;

$factory->define(
    App\Entities\Activity\Call::class,
    function (Faker\Generator $faker) {
        return [
            "add_from" => "desktop",
            "designer_id" => 20,
            "status" => 0,
            "user_id" => 200
        ];
    }
);