<?php

use App\Entities\Feature\WhitelistFeature;
use Faker\Generator as Faker;

$factory->define(
    WhitelistFeature::class,
    function (Faker $faker) {
        return [
            'name' => 'feature_a',
            'user_id' => 3
        ];
    }
);
