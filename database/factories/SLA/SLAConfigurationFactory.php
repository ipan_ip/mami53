<?php

use App\Entities\SLA\SLAConfiguration;
use Faker\Generator as Faker;

$factory->define(\App\Entities\SLA\SLAConfiguration::class, function (Faker $faker) {
    return [
        'case' => $faker->randomElement([
            SLAConfiguration::CASE_EQUAL,
            SLAConfiguration::CASE_LESS_EQUAL_ONE,
            SLAConfiguration::CASE_LESS_EQUAL_TEN,
            SLAConfiguration::CASE_MORE_TEN,
        ]),
    ];
});