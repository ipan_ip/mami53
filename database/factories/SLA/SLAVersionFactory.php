<?php

use App\Entities\SLA\SLAVersion;
use Faker\Generator as Faker;

$factory->define(\App\Entities\SLA\SLAVersion::class, function (Faker $faker) {
    return [
        'sla_type' => $faker->randomElement([
            SLAVersion::SLA_VERSION_TWO,
            SLAVersion::SLA_VERSION_THREE
        ]),
    ];
});