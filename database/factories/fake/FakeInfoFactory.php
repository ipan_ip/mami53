<?php

$factory->define(
    App\Entities\Fake\FakeInfo::class,
    function (Faker\Generator $faker) {
        return [
            'info_type' => 'phone',
            'info' => $faker->e164PhoneNumber,
            'comment' => $faker->name,
        ];
    }
);
