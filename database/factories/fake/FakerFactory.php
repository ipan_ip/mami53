<?php

$factory->define(
    App\Entities\Fake\Faker::class,
    function (Faker\Generator $faker) {
        return [
            'comment' => "{$faker->name} ({$faker->email})",
        ];
    }
);
