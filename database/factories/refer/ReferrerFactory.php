<?php

use App\Entities\Refer\Referrer;
use App\User;

$factory->define(
    Referrer::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => factory(User::class)->create()->id,
            'code' => 'code',
            'type' => 'type',
            'status' => rand(1,99),
        ];
    }
);
