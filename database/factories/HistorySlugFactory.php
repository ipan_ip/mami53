<?php

use App\Entities\Room\HistorySlug;
use Faker\Generator as Faker;

$factory->define(
    HistorySlug::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1,1000),
            'slug' => "this-is-fake-slug-do-not-rely-on-this"
        ];
    }
);
