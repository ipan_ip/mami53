<?php

use App\Entities\Event\EventModel;
use Faker\Generator as Faker;

$factory->define(EventModel::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 100000),
        'title' => $faker->name,
        'description' => $faker->word,
        'action_url' => 'https://help.mamikos.com/',
        'is_owner' => "1",
        'ordering' => 1,
        'is_published' => 1,
    ];
});
