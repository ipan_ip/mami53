<?php

use App\Entities\Search\InputKeywordSuggestion;
use Faker\Generator as Faker;

$factory->define(
    InputKeywordSuggestion::class,
    function (Faker $faker) {
        return [
            'suggestion' => $faker->address,
            'area' => $faker->city,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude
        ];
    }
);
