<?php

use App\Entities\Search\InputKeyword;
use Faker\Generator as Faker;

$factory->define(
    InputKeyword::class,
    function (Faker $faker) {
        return [
            'keyword' => $faker->city,
            'total' => $faker->numberBetween(0, 10)
        ];
    }
);

$factory->state(
    InputKeyword::class,
    'without-hit',
    [
        'total' => 0
    ]
);
