<?php

use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\User;

$factory->define(
    App\User::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->safeEmail,
            'phone_number' => ('081' . $faker->randomNumber(8)),
            'chat_access_token' => str_random(10),
            'password' => bcrypt(str_random(10)),
            'remember_token' => str_random(10),
            'photo_id' => $faker->numberBetween(1, 100),
        ];
    }
);

$factory->state(
    App\User::class,
    'owner',
    [
        'is_owner' => 'true'
    ]
);

$factory->state(
    App\User::class,
    'tenant',
    [
        'is_owner' => 'false'
    ]
);

$factory->state(
    App\User::class,
    'admin',
    [
        'role' => 'administrator'
    ]
);

$factory->afterCreatingState(User::class, 'couldAccessAdmin', function ($user, $faker) {
    $role = factory(Role::class)->create(['name' => 'admin']);

    $user->attachRole($role);
    $role->attachPermission(factory(Permission::class)->create(['name' => 'admin-access']));
});
