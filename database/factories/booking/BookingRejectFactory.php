<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingReject::class, function (Faker $faker) {
    return [
        'description' => $faker->text(10),
        'is_active' => 1,
        'is_affecting_acceptance' => 1,
        'type' => 'cancel',
        'make_kost_not_available' => 0,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
