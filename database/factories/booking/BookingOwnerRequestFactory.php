<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Room\BookingOwnerRequest::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(1, 99999),
        'user_id' => $faker->numberBetween(1, 99999),
        'status' => 'waiting',
        'requested_by' => 'owner',
        'registered_by' => 'system',
        'created_at' => now(),
    ];
});