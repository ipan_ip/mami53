<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingGuest::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone_number' => $faker->phoneNumber,
        'created_at' => now(),
        'updated_at' => now()
    ];
});