<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingDesignerRoom::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});