<?php

use App\Entities\Booking\BookingDiscount;
use Faker\Generator as Faker;

$factory->define(
    BookingDiscount::class,
    function (Faker $faker) {
        return [
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
);

$factory->state(
    BookingDiscount::class,
    'daily',
    [
        'price_type' => 'daily'
    ]
);

$factory->state(
    BookingDiscount::class,
    'weekly',
    [
        'price_type' => 'weekly'
    ]
);

$factory->state(
    BookingDiscount::class,
    'quarterly',
    [
        'price_type' => 'quarterly'
    ]
);

$factory->state(
    BookingDiscount::class,
    'semiannually',
    [
        'price_type' => 'semiannually'
    ]
);

$factory->state(
    BookingDiscount::class,
    'annually',
    [
        'price_type' => 'annually'
    ]
);

$factory->state(
    BookingDiscount::class,
    'monthly',
    [
        'price_type' => 'monthly'
    ]
);