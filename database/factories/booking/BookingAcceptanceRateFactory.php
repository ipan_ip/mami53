<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingAcceptanceRate::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 10000),
        'designer_id' => $faker->numberBetween(1, 10000),
        'is_active' => 1,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
