<?php

use Faker\Generator as Faker;
use App\Entities\Booking\BookingUser;

$factory->define(\App\Entities\Booking\BookingStatus::class, function (Faker $faker) {
    return [
        'status' => $faker->randomElement([
            BookingUser::BOOKING_STATUS_BOOKED,
            BookingUser::BOOKING_STATUS_VERIFIED
        ]),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});