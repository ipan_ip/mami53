<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingOwner::class, function (Faker $faker) {
    return [
        'owner_id' => $faker->numberBetween(1, 100),
        'is_instant_booking' => $faker->boolean,
        'created_at' => now(),
        'updated_at' => now()
    ];
});