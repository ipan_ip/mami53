<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingUser::class, function (Faker $faker) {
    return [
        'checkin_date' => now(),
        'checkout_date' => now(),
        'stay_duration' => 1,
        'booking_code' => $faker->text(200),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});

$factory->define(\App\Entities\Activity\Question::class, function (Faker $faker) {
    return [
        'question' => $faker->text(200),
        'answer' => $faker->name(200),
        'status' => true,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});

$factory->define(\App\Entities\Activity\Call::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});

$factory->define(\App\Entities\Booking\BookingUserRoom::class, function (Faker $faker) {
    return [
        'booking_user_id' => $faker->numberBetween(1, 100),
        'checkin_date' => $faker->dateTime->format("Y-m-d"),
        'checkout_date' => $faker->dateTime->format("Y-m-d"),
        'kost_name' => $faker->name(),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});