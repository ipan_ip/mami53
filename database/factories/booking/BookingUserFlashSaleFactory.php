<?php

use Faker\Generator as Faker;
use App\Entities\Booking\BookingUserFlashSale;

$factory->define(BookingUserFlashSale::class, function (Faker $faker) {
    return [
        'booking_user_id' => 1,
        'flash_sale_id' => 1
    ];
});