<?php

use Faker\Generator as Faker;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserDraft;

$factory->define(\App\Entities\Booking\BookingUserDraft::class, function (Faker $faker) {
    return [
        'tenant_name' => $faker->name,
        'rent_count_type' => $faker->randomElement([
            BookingUser::DAILY_TYPE,
            BookingUser::WEEKLY_TYPE,
            BookingUser::MONTHLY_TYPE,
            BookingUser::QUARTERLY_TYPE,
            BookingUser::SEMIANNUALLY_TYPE,
            BookingUser::YEARLY_TYPE
        ]),
        'duration' => 1,
        'checkin' => now(),
        'checkout' => now(),
        'status' => BookingUserDraft::DRAFT_CREATED,
        'read' => false,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});