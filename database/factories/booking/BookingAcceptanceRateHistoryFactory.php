<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingAcceptanceRateHistory::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 10000),
        'booking_acceptance_rate_id' => $faker->numberBetween(1, 10000),
        'column' => $faker->text(10),
        'original_value' => $faker->text(10),
        'modified_value' => $faker->text(10),
        'triggered_by' => $faker->text(10),
    ];
});
