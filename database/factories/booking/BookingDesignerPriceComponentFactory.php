<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingDesignerPriceComponent::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 1000),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});