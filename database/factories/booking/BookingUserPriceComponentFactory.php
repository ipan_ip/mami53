<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingUserPriceComponent::class, function (Faker $faker) {
    return [
        'price_name' => $faker->randomElement(['base', 'booking_fee']),
        'price_label' => $faker->randomElement(['base', 'booking_fee']),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});