<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Booking\BookingPayment::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});