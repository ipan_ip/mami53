<?php

use App\Entities\Booking\BookingUser;
use Carbon\Carbon;

$factory->define(
    BookingUser::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 10000),
            'checkin_date' => Carbon::now(),
            'stay_duration' => $faker->numberBetween(1, 10000),
            'rent_count_type' => $faker->randomElement([
                BookingUser::DAILY_TYPE,
                BookingUser::WEEKLY_TYPE,
                BookingUser::MONTHLY_TYPE,
                BookingUser::QUARTERLY_TYPE,
                BookingUser::SEMIANNUALLY_TYPE,
                BookingUser::YEARLY_TYPE
            ]),
            'user_id' =>  $faker->numberBetween(1, 10000),
            'status' => $faker->randomElement([
                BookingUser::BOOKING_STATUS_BOOKED,
                BookingUser::BOOKING_STATUS_CONFIRMED,
                BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
                BookingUser::BOOKING_STATUS_PAID,
                BookingUser::BOOKING_STATUS_VERIFIED,
                BookingUser::BOOKING_STATUS_CHECKED_IN,
                BookingUser::BOOKING_STATUS_CANCELLED,
                BookingUser::BOOKING_STATUS_REJECTED,
                BookingUser::BOOKING_STATUS_EXPIRED,
                BookingUser::BOOKING_STATUS_EXPIRED_DATE,
                BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED,
                BookingUser::BOOKING_STATUS_GUARANTEED,
                BookingUser::BOOKING_STATUS_TERMINATED,
                BookingUser::BOOKING_STATUS_FINISHED,
                BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER,
                BookingUser::BOOKING_STATUS_EXPIRED_BY_ADMIN
            ]),
            'designer_id' => $faker->numberBetween(1, 10000),
            'booking_designer_id' => $faker->numberBetween(1, 10000)
        ];
    }
);
