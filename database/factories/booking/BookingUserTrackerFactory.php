<?php

use Faker\Generator as Faker;
use App\Entities\Booking\BookingUser;

$factory->define(\App\Entities\Booking\BookingUserTracker::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});