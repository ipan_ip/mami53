<?php

use App\Entities\Sanjunipero\DynamicLandingParent;
use Faker\Generator as Faker;

$factory->define(
    DynamicLandingParent::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug(),
            'type_kost' => implode(',', [DynamicLandingParent::ALL_GOLDPLUS, DynamicLandingParent::OYO]),
            'title_tag' => $faker->domainName(),
            'title_header' => $faker->domainName(),
            'subtitle_header' => $faker->domainName(),
            'meta_desc' => $faker->sentence(),
            'meta_keywords' => $faker->sentence(),
            'faq_question' => json_encode([$faker->sentence(), $faker->sentence(), $faker->sentence()]),
            'faq_answer' => json_encode([$faker->sentence(), $faker->sentence(), $faker->sentence()]),
            'image_url' => $faker->imageUrl(640, 480),
            'is_active' => mt_rand(0, 1)
        ];
    }
);

$factory->state(
    DynamicLandingParent::class,
    'active',
    [
        'is_active' => true
    ]
);

$factory->state(
    DynamicLandingParent::class,
    'inactive',
    [
        'is_active' => false
    ]
);
