<?php

use App\Entities\Sanjunipero\DynamicLandingChild;
use Faker\Generator as Faker;

$factory->define(
    DynamicLandingChild::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug(),
            'area_type' => DynamicLandingChild::AREA_TYPE,
            'area_name' => $this->faker->state(),
            'slug' => $this->faker->slug(),
            'latitude_1' => $this->faker->latitude(),
            'longitude_1' => $this->faker->longitude(),
            'latitude_2' => $this->faker->latitude(),
            'longitude_2' => $this->faker->longitude(),
            'meta_desc' => $this->faker->sentence(),
            'meta_keywords' => $this->faker->sentence()
        ];
    }
);

$factory->state(
    DynamicLandingChild::class,
    'active',
    [
        'is_active' => true
    ]
);

$factory->state(
    DynamicLandingChild::class,
    'inactive',
    [
        'is_active' => false
    ]
);
