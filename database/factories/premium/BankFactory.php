<?php
use Faker\Generator as Faker;
use App\Entities\Premium\Bank;

$factory->define(Bank::class, function (Faker $faker) {
    return [
        'id' => $faker->randomDigitNotNull,
        'name' => $faker->name(),
        'account_name' => $faker->name(),
        'number' => $faker->numberBetween(1,100),
        'is_active' => $faker->randomElement([0, 1]),
        'created_at' => now(),
        'updated_at' => now()
    ];
});