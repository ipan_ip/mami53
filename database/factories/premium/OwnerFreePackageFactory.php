<?php

use Faker\Generator as Faker;
use App\Entities\Premium\OwnerFreePackage;

$factory->define(OwnerFreePackage::class, function(Faker $faker) {
    return [
        'id' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'premium_package_id' => $faker->randomDigitNotNull,
        'from' => $faker->randomElement(['banner_event_premium']),
        'status' => $faker->randomElement(['submit', 'success']),
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => now()
    ];
});