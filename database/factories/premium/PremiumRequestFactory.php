<?php

use Faker\Generator as Faker;
use App\Entities\Premium\PremiumRequest;

$factory->define(PremiumRequest::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1,100),
        'premium_package_id' => $faker->numberBetween(1,100),
        'status' => $faker->randomElement(['1', '0']),
        'total' => $faker->numberBetween(1,100),
        'view' => $faker->numberBetween(1,100),
        'used' => $faker->numberBetween(1,100),
        'allocated' => $faker->numberBetween(1,100),
        'daily_allocation' => 0
    ];
});
