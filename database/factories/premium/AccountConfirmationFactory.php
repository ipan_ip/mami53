<?php

use Faker\Generator as Faker;
use App\Entities\Premium\AccountConfirmation;

$factory->define(AccountConfirmation::class, function (Faker $faker) {
    return [
        'insert_from' => 'desktop',
        'user_id' => 1,
        'premium_request_id' => $faker->randomDigitNotNull,
        'view_balance_request_id' => 0,
        'bank' => 'BCA',
        'name' => $faker->word,
        'total' => 100123,
        'bank_account_id' => 1,
        'transfer_date' => date('Y-m-d'),
        'is_confirm' => '1',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
