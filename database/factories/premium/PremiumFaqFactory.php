<?php

use Faker\Generator as Faker;
use App\Entities\Premium\PremiumFaq;

$factory->define(PremiumFaq::class, function (Faker $faker) {
    return [
        'question' => 'question',
        'answer' => 'answer',
        'is_active' => $faker->boolean(1),
    ];
});