<?php

use Faker\Generator as Faker;
use App\Entities\Premium\PremiumStopReason;

$factory->define(PremiumStopReason::class, function(Faker $faker) {
    return [
        'id' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'reason' => $faker->word,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});