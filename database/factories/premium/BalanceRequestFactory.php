<?php
use Faker\Generator as Faker;
use App\Entities\Premium\BalanceRequest;

$factory->define(BalanceRequest::class, function (Faker $faker) {
    return [
        'view' => $faker->randomDigitNotNull,
        'price' => 0,
        'premium_package_id' => $faker->numberBetween(1,100),
        'premium_request_id' => $faker->numberBetween(1,100),
        'status' => $faker->boolean(1),
        'expired_date' => date('Y-m-d'),
        'expired_status' => $faker->randomElement(['true', 'false']),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
