<?php
use Faker\Generator as Faker;
use App\Entities\Premium\Payment;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'expired_at' => date('Y-m-d'),
        'name' => $faker->name(),
        'order_id' => $faker->numberBetween(1,100),
        'biller_code' => $faker->numberBetween(1,100),
        'bill_key' => $faker->numberBetween(1,100),
        'total' => $faker->numberBetween(1,100),
        'transaction_id' => $faker->name(),
        'user_id' => $faker->numberBetween(1,100),
        'premium_request_id' => $faker->numberBetween(1,100),
        'payment_type' => $faker->name(),
        'transaction_status' => $faker->randomElement(['pending', 'deny', 'expire', 'success']),
        'created_at' => now(),
        'updated_at' => now(),
        'version'   => $faker->randomElement([1, 2])
    ];
});
