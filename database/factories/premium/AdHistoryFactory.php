<?php

use Faker\Generator as Faker;
use App\Entities\Premium\AdHistory;

$factory->define(AdHistory::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->randomDigitNotNull,
        'type' => AdHistory::TYPE_CLICK,
        'date' => date('Y-m-d'),
        'created_at' => now(),
        'updated_at' => now(),
        'total' => 0,
        'total_click' => 0
    ];
});
