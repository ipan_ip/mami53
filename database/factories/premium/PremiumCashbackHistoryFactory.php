<?php

use Faker\Generator as Faker;
use App\Entities\Premium\PremiumCashbackHistory;

$factory->define(PremiumCashbackHistory::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 100),
        'reference_id' => $faker->numberBetween(1, 100),
        'amount' => $faker->numberBetween(1, 100000),
        'type' => $faker->randomElement(['topup', 'booking_commission']),
    ];
});