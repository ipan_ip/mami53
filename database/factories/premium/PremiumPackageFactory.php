<?php

use Faker\Generator as Faker;
use App\Entities\Premium\PremiumPackage;

$factory->define(PremiumPackage::class, function (Faker $faker) {
    return [
        'for' => PremiumPackage::PACKAGE_TYPE,
        'name' => 'Paket Premium ' . $faker->unique()->randomDigitNotNull,
        'price' => 10000,
        'sale_price' => 5000,
        'special_price' => 0,
        'start_date' => date('Y-m-d', strtotime('-7 days')),
        'sale_limit_date' => date('Y-m-d', strtotime('+60 days')),
        'bonus' => 'fitur bonus',
        'feature' => 'Fitur 1. Fitur 2. Fitur 3.',
        'total_day' => 30,
        'is_active' => '1',
        'is_recommendation' => 0,
        'created_at' => now(),
        'updated_at' => now(),
        'view' => 1000
    ];
});

$factory->state(PremiumPackage::class, 'trial',  function ($faker) {
    return [
        'name' => 'Paket Premium Trial' . $faker->unique()->randomDigitNotNull,
        'for' => PremiumPackage::TRIAL_TYPE,
        'price' => 0,
        'sale_price' => 0
    ];
});

$factory->state(PremiumPackage::class, 'extension',  function ($faker) {
    return [
        'name' => 'Paket Premium Extension' . $faker->unique()->randomDigitNotNull,
        'view' => 0
    ];
});

$factory->state(PremiumPackage::class, 'balance', function ($faker) {
    return [
        'for' => PremiumPackage::BALANCE_TYPE,
        'name' => 'Paket Saldo ' . $faker->unique()->randomDigitNotNull
    ];
});
