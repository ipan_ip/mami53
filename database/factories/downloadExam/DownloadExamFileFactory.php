<?php

use Faker\Generator as Faker;
use App\Entities\DownloadExam\DownloadExamFile;

$factory->define(DownloadExamFile::class, function (Faker $faker) {
    return [
        'download_exam_id' => mt_rand(0, 100),
        'name' => $faker->md5.'.'.$faker->fileExtension,
        'file_name' => $faker->md5.'.'.$faker->fileExtension,
        'file_path' => 'uploads/data/landing/2017-12-28',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
