<?php

use App\Entities\Mamipay\MamipayMedia;
use Faker\Generator as Faker;

$factory->define(MamipayMedia::class, function (Faker $faker) {
    return [
        'user_id' => 0,
        'file_name' => $faker->regexify('[A-Za-z0-9]{8}') . '.jpg',
        'file_path' => 'original/voucher_campaign/20200404',
        'type' => 'voucher_campaign',
        'media_format' => 'image',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
