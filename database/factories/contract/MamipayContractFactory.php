<?php

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use Faker\Generator as Faker;

$factory->define(MamipayContract::class, function (Faker $faker) {
    return [
        'tenant_id' => $faker->numberBetween(1, 10000),
        'type' => 'kost',
        'status' => $faker->randomElement(['terminated', 'active']),
        'duration' => $faker->numberBetween(1, 5),
        'duration_unit' => $faker->randomElement(['month', 'year']),
        'start_date' => $faker->dateTime->format('Y-m-d'),
        'end_date' => $faker->dateTime->format('Y-m-d'),
    ];
});
