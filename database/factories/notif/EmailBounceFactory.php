<?php

use Faker\Generator as Faker;
use App\Entities\Notif\EmailBounce;

$factory->define(EmailBounce::class, function (Faker $faker) {
    return [
        "user_id" => null,
        "email" => "anonymous@gmail.com",
        "created_at" => "2018-07-20 21:15:00"
    ];
});
