<?php

use App\Entities\Notif\SettingNotif;
use Faker\Generator as Faker;

$factory->define(
    SettingNotif::class,
    function (Faker $faker) {
        return [
            'for' => $faker->randomElement(
                [
                    'user',
                    'designer'
                ]
            ),
            'identifier' => $faker->numberBetween(100000, 999999),
            'key' => $faker->randomElement(
                [
                    'app_chat',
                    'alarm_email',
                    'update_kost'
                ]
            ),
            'value' => $faker->randomElement(
                [
                    'true',
                    'false'
                ]
            )
        ];
    }
);

$factory->state(
    SettingNotif::class,
    'for_user',
    [
        'for' => 'user'
    ]
);


$factory->state(
    SettingNotif::class,
    'for_room',
    [
        'for' => 'designer'
    ]
);
