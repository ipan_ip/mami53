<?php

use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Http\Helpers\NotificationTemplateHelper;
use Faker\Generator as Faker;

$factory->define(NotificationWhatsappTemplate::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement([
            NotificationWhatsappTemplate::TYPE_TRIGGERED,
            NotificationWhatsappTemplate::TYPE_SCHEDULED,
        ]),
        'target_user' => $faker->randomElement([
            'owner',
            'tenant',
            'all',
        ]),
        'content' => $faker->text,
    ];
});

$factory->state(NotificationWhatsappTemplate::class, 'active', [
    'is_active' => true
]);

$factory->state(NotificationWhatsappTemplate::class, 'otp-wa', [
    'name' => NotificationTemplateHelper::NAME_REQUEST_VERIFICATION_CODE,
    'type' => NotificationWhatsappTemplate::TYPE_TRIGGERED,
]);
