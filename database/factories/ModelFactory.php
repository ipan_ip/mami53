<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Http\Helpers\ApiHelper;
use Carbon\Carbon;

$factory->define(
    App\Entities\Device\UserDevice::class,
    function (Faker\Generator $faker) {
        return [
            'created_at' => Carbon::now(),
            'model' => $faker->randomElement(['Xiaomi Mi 4i', 'iPhone6,2']),
            'email' => $faker->safeEmail,
            'user_id' => $faker->randomNumber(1),
            'identifier' => $faker->ean13,
            'uuid' => $faker->uuid,
            'platform' => $faker->randomElement(['ios', 'android']),
            'device_token' => $faker->sha256,
            'app_notif_all' => $faker->boolean(50),
            'app_notif_status_changed' => $faker->boolean(50),
            'app_notif_by_reference' => $faker->boolean(50),
            'app_install_count' => 1,
            'app_update_count' => 0,
            'enable_app_popup' => true,
            'device_last_login' => $faker->boolean(50),
            'login_status' => $faker->randomElement(['login', 'not_login']),
        ];
    }
);

$factory->state(
    App\Entities\Device\UserDevice::class,
    'ios',
    [
        'model' => 'iPhone6,2',
        'platform' => 'ios'
    ]
);

$factory->state(
    App\Entities\Device\UserDevice::class,
    'android',
    [
        'model' => 'Xiaomi Mi 4i',
        'platform' => 'android',
    ]
);

$factory->state(
    App\Entities\Device\UserDevice::class,
    'login',
    [
        'device_last_login' => true,
        'login_status' => 'login',
    ]
);

$factory->state(
    App\Entities\Device\UserDevice::class,
    'not_login',
    [
        'device_last_login' => false,
        'login_status' => 'not_login',
    ]
);

$factory->define(
    App\Entities\Mamipay\MamipayContractKost::class,
    function (Faker\Generator $faker) {
        return [
            'contract_id' => $faker->numberBetween(1, 1000),
            'designer_id' => $faker->numberBetween(1, 1000),
            'room_number' => $faker->numberBetween(1, 500),
            'rent_type' => $faker->randomElement(['month', 'year']),
        ];
    }
);

$factory->define(
    App\Entities\Mamipay\MamipayTenant::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 1000),
            'email' => $faker->email,
            'phone_number' => $faker->phoneNumber,
            'name' => $faker->name,
            'gender' => $faker->randomElement(array('male', 'female')),
            'occupation' => $faker->randomElement(array('karyawan', 'wirausaha')),
        ];
    }
);

$factory->define(
    App\Entities\Mamipay\MamipayOwner::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100),
            'status' => $faker->randomElement(array('approved', 'rejected')),
            'role' => $faker->randomElement(array('pemilik', 'pengelola')),
        ];
    }
);

$factory->define(
    App\Entities\Area\Area::class,
    function (Faker\Generator $faker) {
        return [
            'parent_name' => $faker->city,
            'big_city' => $faker->city,
            'name' => $faker->city,
        ];
    }
);

$factory->define(
    App\Entities\Consultant\Consultant::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->safeEmail,
            'user_id' => $faker->numberBetween(1, 100),
            'chat_id' => $faker->numberBetween(1, 100),
            'device_key' => str_random(10),
            'user_key' => str_random(10)
        ];
    }
);

$factory->define(
    App\Entities\Consultant\PotentialTenant::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->safeEmail,
            'phone_number' => $faker->phoneNumber,
            'consultant_id' => $faker->numberBetween(1, 100),
            'gender' => $faker->randomElement(['male', 'female']),
            'job_information' => $faker->randomElement(['Mahasiswa', 'Karyawan']),
            'property_name' => str_random(10),
            'designer_id' => $faker->numberBetween(1, 100),
            'price' => $faker->numberBetween(1, 1000),
            'due_date' => $faker->numberBetween(1, 31),
            'scheduled_date' => $faker->date('Y-m-d'),
            'duration_unit' => $faker->randomElement(['day', 'week', 'month', '3_month', '6_month'])
        ];
    }
);

$factory->define(
    App\Entities\Consultant\ConsultantNote::class,
    function (Faker\Generator $faker) {
        return [
            'content' => str_random(10),
            'consultant_id' => $faker->numberBetween(1, 100),
            'noteable_type' => str_random(10),
            'noteable_id' => $faker->numberBetween(1, 100),
            'activity' => str_random(10),
        ];
    }
);

$factory->define(
    App\Entities\Consultant\DesignerChangesByConsultant::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_id' => $faker->numberBetween(1, 100),
            'designer_id' => $faker->numberBetween(1, 100),
            'field_name' => str_random(10),
            'value_before' => str_random(10),
            'value_after' => str_random(10),
            'designer_price_id' => $faker->numberBetween(1, 100),
        ];
    }
);

$factory->define(
    App\Entities\Slider\Slider::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100),
            'name' => $faker->name,
            'endpoint' => $faker->word,
            'is_active' => $faker->numberBetween(0, 1)
        ];
    }
);

$factory->define(
    App\Entities\Slider\SliderPage::class,
    function (Faker\Generator $faker) {
        return [
            'slider_id' => $faker->numberBetween(1, 100),
            'media_id' => $faker->numberBetween(1, 100),
            'user_id' => $faker->numberBetween(1, 100),
            'title' => $faker->title,
            'content' => $faker->text,
            'redirect_label' => $faker->name,
            'redirect_link' => $faker->url,
            'is_cover' => $faker->numberBetween(0, 1),
            'is_active' => $faker->numberBetween(0, 1)
        ];
    }
);

$factory->define(
    App\Entities\Landing\Landing::class,
    function (Faker\Generator $faker) {
        return [
            'slug' => $faker->slug,
            'type' => $faker->randomElement(['area', 'campus']),
            'user_id' => $faker->numberBetween(1, 100),
            'photo_1_id' => $faker->numberBetween(1, 100),
            'photo_2_id' => $faker->numberBetween(1, 100),
            'latitude_1' => $faker->latitude,
            'longitude_1' => $faker->latitude,
            'latitude_2' => $faker->latitude,
            'longitude_2' => $faker->latitude,
            'area_city' => $faker->city,
            'area_subdistrict' => $faker->streetName,
            'price_min' => $faker->numberBetween(1, 10000),
            'price_max' => $faker->numberBetween(10000, 10000),
            'rent_type' => $faker->randomElement(['0', '1', '2', '3']),
            'description_1' => $faker->text,
            'description_2' => $faker->text,
            'description_3' => $faker->text,
            'heading_1' => $faker->sentence,
            'redirect_id' => $faker->numberBetween(1, 100),
            'parent_id' => null
        ];
    }
);

$factory->define(
    App\Entities\Consultant\ConsultantMapping::class,
    function (Faker\Generator $faker) {
        return [
            'area_city' => $faker->city,
            'is_mamirooms' => $faker->boolean(50),
            'consultant_id' => $faker->numberBetween(1, 100),
        ];
    }
);

$factory->define(
    App\Entities\User\UserNotificationCounter::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(0, 10000),
            'notification_type' => $faker->randomElement(['chat', 'survey', 'telp', 'update_kost', 'recommendation']),
            'notification_text' => $faker->sentence(4),
            'counter' => $faker->numberBetween(0, 10),
            'last_read' => $faker->randomElement([null, Carbon::now()])
        ];
    }
);


$factory->define(
    App\Entities\Entrust\Role::class,
    function (Faker\Generator $faker) {
        return [
            'name' => str_random(10),
            'display_name' => str_random(10),
            'description' => str_random(10),
        ];
    }
);

$factory->define(
    App\Entities\Entrust\Permission::class,
    function (Faker\Generator $faker) {
        return [
            'name' => str_random(10),
            'display_name' => str_random(10),
            'description' => str_random(10),
        ];
    }
);

$factory->define(
    App\Entities\Testimonial\Testimonial::class,
    function (Faker\Generator $faker) {
        return [
            'photo_id' => $faker->numberBetween(1,100),
            'owner_name' => $faker->name,
            'kost_name' => $faker->name,
            'quote' => $faker->text,
            'is_active' => $faker->numberBetween(0,1),
            'order' => $faker->numberBetween(1,10),
            'creator_id' => $faker->numberBetween(1,100)
        ];
    }
);

$factory->define(
    App\Entities\User\UserMedia::class,
    function (Faker\Generator $faker) {
        static $id = 1;
        return [
            'id' => $id++, // Incrementing id by default.
            'user_id' => $faker->numberBetween(1, 999999),
            'upload_identifier_type' => $faker->randomElement([null, 'user_id','device_id','android_id','ios_id','admin_ip_address']),
            'file_name' => $faker->text(10),
            'file_path' => $faker->text(10),
            'type' => $faker->randomElement([null, 'photo_profile','e_ktp','passport','sim']),
            'from' => $faker->randomElement([null, 'user_profile','booking']),
            'server_location' => $faker->randomElement([null, 'macbook-pro','server-live']),
            'media_format' => $faker->randomElement([null, 'jpg','gif','mp3','png','jpeg','bmp']),
            'description' => $faker->text,
        ];
    }
);

$factory->define(
    \App\Entities\Level\KostLevelMap::class,
    function (Faker\Generator $faker) {
        return [
            'kost_id' => $faker->numberBetween(1, 1000),
            'level_id' => $faker->numberBetween(1, 100)
        ];
    }
);

$factory->define(
    App\Entities\Promoted\AdsDisplayTracker::class,
    function (Faker\Generator $faker) {
        return [
            'identifier'        => $faker->text(10),
            'identifier_type'   => $faker->randomElement(['device', 'user', 'session']),
            'designer_id'       => $faker->numberBetween(1, 999999),
            'display_count'     => $faker->numberBetween(1, 999999),
            'source'            => $faker->text(10),
            'created_at'        => now(),
            'updated_at'        => now(),  
        ];
    }
);

$factory->define(
    App\Entities\Promoted\AdsDisplayTrackerTemp::class,
    function (Faker\Generator $faker) {
        return [
            'identifier'        => $faker->text(10),
            'identifier_type'   => $faker->randomElement(['device', 'user', 'session']),
            'designer_id'       => $faker->numberBetween(1, 999999),
            'display_count'     => $faker->numberBetween(1, 999999),
            'source'            => $faker->text(10),
            'status'            => $faker->text(10),
            'created_at'        => now(),
            'updated_at'        => now(),  
        ];
    }
);

$factory->define(
    App\Entities\Premium\AdsInteractionTracker::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id'   => $faker->numberBetween(1, 999999),
            'user_id'       => $faker->numberBetween(1, 999999),
            'type'          => $faker->randomElement(['like', 'click']),
            'platform'      => $faker->randomElement(['web-desktop', 'web-mobile', 'android', 'ios'])
        ];
    }
);

$factory->define(
    App\Entities\Premium\PremiumPlusUser::class,
    function (Faker\Generator $faker) {
        return [
            'name'              => $faker->name,
            'phone_number'      => $faker->phoneNumber,
            'email'             => $faker->email,
            'designer_id'       => $faker->numberBetween(1, 999999),
            'consultant_id'     => $faker->numberBetween(1, 999999),
            'user_id'           => $faker->numberBetween(1, 999999),
            'request_date'      => now(),
            'activation_date'   => now(),
            'active_period'     => $faker->numberBetween(1, 12),
            'premium_rate'      => $faker->numberBetween(1, 100),
            'static_rate'       => $faker->numberBetween(1, 9999999),
            'registered_room'   => $faker->numberBetween(1, 100),
            'guaranteed_room'   => $faker->numberBetween(1, 50),
            'releasable_room'   => $faker->numberBetween(1, 50),
            'total_monthly_premium' => $faker->numberBetween(1, 1000000)
        ];
    }
);

$factory->define(
    App\Entities\Premium\PremiumPlusInvoice::class,
    function (Faker\Generator $faker) {
        return [
            'premium_plus_user_id'  => $faker->numberBetween(1, 999999),
            'shortlink'             => $faker->text(5),
            'invoice_number'        => $faker->text(12),
            'name'                  => $faker->name,
            'amount'                => $faker->numberBetween(1, 9999999),
            'status'                => $faker->randomElement(['paid', 'unpaid'])
        ];
    }
);
