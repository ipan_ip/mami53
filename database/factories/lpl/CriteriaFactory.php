<?php

use App\Entities\Lpl\Criteria;
use Faker\Generator as Faker;

$factory->define(
    Criteria::class,
    function (Faker $faker) {
        return [
            'name' => $faker->numberBetween(1, 1000),
            'description' => 'This is a testing criteria',
            'attribute' => $faker->asciify('**********'),
            'order' => 10,
            'score' => 512
        ];
    }
);
