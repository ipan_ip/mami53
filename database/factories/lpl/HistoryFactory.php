<?php

use App\Entities\Lpl\History;
use Faker\Generator as Faker;

$factory->define(
    History::class,
    function (Faker $faker) {
        return [
            'lpl_criteria_id' => $faker->numberBetween(1, 100),
            'original_score' => 64,
            'modified_score' => 128
        ];
    }
);
