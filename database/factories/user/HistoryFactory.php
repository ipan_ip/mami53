<?php

use App\Entities\User\History;
use Faker\Generator as Faker;

$factory->define(History::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 100000),
        'designer_id' => $faker->numberBetween(1, 100000),
        'time' => $faker->date(),
        'is_active' => $faker->boolean
    ];
});
