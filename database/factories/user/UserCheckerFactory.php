<?php

$factory->define(
    App\Entities\User\UserChecker::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' =>  $faker->numberBetween(1,99),
            'from_db' =>  false,
            'created_at' =>  now(),
            'updated_at' =>  now(),
        ];
    }
);