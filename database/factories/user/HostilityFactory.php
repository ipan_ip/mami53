<?php

use App\Entities\User\Hostility;
use App\Entities\User\HostilityReason;
use App\User;
use Faker\Generator as Faker;

$factory->define(Hostility::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'reason' => $faker->randomElement(HostilityReason::getValues()),
        'created_by' => $faker->numberBetween(1, 10000),
        'updated_by' => $faker->numberBetween(1, 10000)
    ];
});
