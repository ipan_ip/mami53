<?php

$factory->define(
    App\Entities\User\UserSocial::class,
    function (Faker\Generator $faker) {
        return [
            'email' => $faker->safeEmail(),
            'name' => $faker->name(),
            'user_id' => $faker->randomNumber(1),
            'type' => $faker->randomElement(['facebook', 'google']),
            'identifier' => $faker->randomNumber(5),
        ];
    }
);