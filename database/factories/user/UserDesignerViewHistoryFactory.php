<?php

use App\Entities\User\UserDesignerViewHistory;
use Faker\Generator as Faker;

$factory->define(UserDesignerViewHistory::class, function (Faker $faker) {
    return [
        'viewed_at' => now()
    ];
});
