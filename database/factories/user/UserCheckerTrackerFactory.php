<?php

$factory->define(
    App\Entities\User\UserCheckerTracker::class,
    function (Faker\Generator $faker) {
        return [
            'user_checker_id' => $faker->numberBetween(1,99),
            'designer_id' => $faker->numberBetween(1,99),
            'action' => 'room-check',
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
);