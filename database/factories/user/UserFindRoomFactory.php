<?php

use App\Entities\Landing\Landing;
use App\Entities\User\UserFindRoom;
use App\User;
use Faker\Generator as Faker;

$factory->define(UserFindRoom::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create(),
        'landing_id' => factory(Landing::class)->create(),
        'name' => $faker->words(2, true),
        'email' => $faker->email(),
        'phone_number' => null,
        'note' => $faker->text(),
        'start_date' => now(),
        'duration_month' => $faker->numberBetween(1, 12),
        'current_city' => null,
        'destination_city' => null,
        'university' => null,
        'created_at' => now(),
        'updated_at' => null,
        'deleted_at' => null,
    ];
});
