<?php

$factory->define(
    App\Entities\User\UserMedia::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->randomNumber(),
            'type' => $faker->randomElement(['photo_profile','e_ktp','passport','sim']),
            'description' => $faker->randomElement(['photo_identitiy','selfie_identitiy']),
        ];
    }
);