<?php

use Faker\Generator as Faker;
use App\Entities\Premium\ClickPricing;

$factory->define(ClickPricing::class, function (Faker $faker) {
    $for = array_rand(
        array_flip([
            ClickPricing::FOR_TYPE_CHAT, 
            ClickPricing::FOR_TYPE_CLICK
        ])
    );

    $price = array_rand(array_flip([200, 300, 400, 500, 600]));
    
    return [
        'for' => $for,
        'area_city' => $faker->city,
        'lower_limit_price' => $price,
        'higher_limit_price' => $price,
        'property_median_price' => ClickPricing::PROPERTY_MEDIAN_PRICE_FALLBACK,
        'low_price' => $price,
        'high_price' => $price
    ];
});

$factory->defineAs(
    ClickPricing::class,
    ClickPricing::FOR_TYPE_CLICK,
    function () {
        return [
            'for' => ClickPricing::FOR_TYPE_CLICK,
            'area_city' => ClickPricing::DEFAULT_AREA_LABEL_CLICK,
            'lower_limit_price' => ClickPricing::CLICK_PRICING_FALLBACK,
            'higher_limit_price' => ClickPricing::CLICK_PRICING_FALLBACK,
            'property_median_price' => ClickPricing::PROPERTY_MEDIAN_PRICE_FALLBACK,
            'low_price' => ClickPricing::CLICK_PRICING_FALLBACK,
            'high_price' => ClickPricing::CLICK_PRICING_FALLBACK
        ];
    }
);

$factory->defineAs(
    ClickPricing::class, 
    ClickPricing::FOR_TYPE_CHAT,
    function () {
        return [
            'for' => ClickPricing::FOR_TYPE_CHAT,
            'area_city' => ClickPricing::DEFAULT_AREA_LABEL_CHAT,
            'lower_limit_price' => ClickPricing::CHAT_PRICING_FALLBACK,
            'higher_limit_price' => ClickPricing::CHAT_PRICING_FALLBACK,
            'property_median_price' => ClickPricing::PROPERTY_MEDIAN_PRICE_FALLBACK,
            'low_price' => ClickPricing::CHAT_PRICING_FALLBACK,
            'high_price' => ClickPricing::CHAT_PRICING_FALLBACK
        ];
    }
);

$factory->state(
    ClickPricing::class,
    ClickPricing::FOR_TYPE_CLICK,
    function () {
        return [
            'for' => ClickPricing::FOR_TYPE_CLICK,
        ];
    }
);

$factory->state(
    ClickPricing::class,
    ClickPricing::FOR_TYPE_CHAT,
    function () {
        return [
            'for' => ClickPricing::FOR_TYPE_CHAT,
        ];
    }
);

