<?php

use App\Entities\Geocode\GeocodeCache;
use Faker\Generator as Faker;

$factory->define(GeocodeCache::class, function (Faker $faker) {
    return [
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'postal_code' => $faker->postcode,
        'country' => 'indonesia',
    ];
});
