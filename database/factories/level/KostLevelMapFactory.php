<?php

use App\Entities\Level\KostLevelMap;
use Faker\Generator as Faker;

$factory->define(
    KostLevelMap::class,
    function (Faker $faker) {
        return [
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
);
