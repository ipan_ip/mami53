<?php

use App\Entities\Level\KostLevelValue;
use Faker\Generator as Faker;

$factory->define(
    KostLevelValue::class,
    function (Faker $faker) {
        return [
            'name' => $faker->randomElement(
                [
                    'Free of Charge',
                    'Insurance Tenant Protection',
                    'Booking Langsung',
                    'Pro Service',
                    'Mamichecker',
                    'Garansi Uang Kembali',
                    'Dikelola Penuh Oleh Mamikos'
                ]
            ),
            'description' => $faker->text
        ];
    }
);
