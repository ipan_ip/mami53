<?php

use App\Entities\Level\KostLevel;
use Faker\Generator as Faker;

$factory->define(
    KostLevel::class,
    function (Faker $faker) {
        return [
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
);

$factory->state(
    KostLevel::class,
    'goldplus',
    [
        'name' => collect([
                'Mamikos Goldplus 1',
                'Mamikos Goldplus 2',
                'Mamikos Goldplus 3',
            ])->random()
    ]
);

$factory->state(
    KostLevel::class,
    'regular',
    [
        'is_regular' => true,
    ]
);

$factory->state(
    KostLevel::class,
    'gp1',
    [
        'id' => config('kostlevel.id.goldplus1'),
    ]
);


$factory->state(
    KostLevel::class,
    'gp2',
    [
        'id' => config('kostlevel.id.goldplus2'),
    ]
);


$factory->state(
    KostLevel::class,
    'gp3',
    [
        'id' => config('kostlevel.id.goldplus3'),
    ]
);

$factory->state(
    KostLevel::class,
    'gp_level_1',
    [
        'key' => KostLevel::GP_LEVEL_KEY_PREFIX.'1',
    ]
);

$factory->state(
    KostLevel::class,
    'gp_level_2',
    [
        'key' => KostLevel::GP_LEVEL_KEY_PREFIX.'2',
    ]
);

$factory->state(
    KostLevel::class,
    'gp_level_3',
    [
        'key' => KostLevel::GP_LEVEL_KEY_PREFIX.'3',
    ]
);

$factory->state(
    KostLevel::class,
    'gp_level_4',
    [
        'key' => KostLevel::GP_LEVEL_KEY_PREFIX.'4',
    ]
);

$factory->state(
    KostLevel::class,
    'gp4',
    [
        'id' => config('kostlevel.id.goldplus4'),
    ]
);

$factory->state(
    KostLevel::class,
    'rand-gp1',
    [
        'id' => collect(KostLevel::getGoldplusLevelIdsByLevel(1))->random(),
    ]
);

$factory->state(
    KostLevel::class,
    'rand-gp2',
    [
        'id' => collect(KostLevel::getGoldplusLevelIdsByLevel(2))->random(),
    ]
);

$factory->state(
    KostLevel::class,
    'rand-gp3',
    [
        'id' => collect(KostLevel::getGoldplusLevelIdsByLevel(3))->random(),
    ]
);

$factory->state(
    KostLevel::class,
    'rand-gp4',
    [
        'id' => collect(KostLevel::getGoldplusLevelIdsByLevel(4))->random(),
    ]
);

$factory->afterCreatingState(KostLevel::class, 'gp1-with-config', function (KostLevel $kostLevel, Faker $faker) {
    Config::set('kostlevel.ids.goldplus1', $kostLevel->id);
});

$factory->afterCreatingState(KostLevel::class, 'gp2-with-config', function (KostLevel $kostLevel, Faker $faker) {
    Config::set('kostlevel.ids.goldplus2', $kostLevel->id);
});

$factory->afterCreatingState(KostLevel::class, 'gp3-with-config', function (KostLevel $kostLevel, Faker $faker) {
    Config::set('kostlevel.ids.goldplus3', $kostLevel->id);
});

$factory->afterCreatingState(KostLevel::class, 'gp4-with-config', function (KostLevel $kostLevel, Faker $faker) {
    Config::set('kostlevel.ids.goldplus4', $kostLevel->id);
});
