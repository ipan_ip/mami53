<?php

use App\Entities\Level\PropertyLevel;
use Faker\Generator as Faker;

$factory->define(
    PropertyLevel::class,
    function (Faker $faker) {
        return [
            'name' => $faker->name,
            'max_rooms' => $faker->numberBetween(1, 100),
            'minimum_charging' => $faker->randomFloat(2, 0, 100000)
        ];
    }
);
