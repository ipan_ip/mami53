<?php

use App\Entities\Level\RoomLevel;
use Faker\Generator as Faker;

$factory->define(RoomLevel::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'order' => $faker->numberBetween(1, 100),
        'is_regular' => $faker->boolean(50),
        'is_hidden' => $faker->boolean(50),
        'notes' => $faker->sentence,
        'charging_fee' => $faker->randomElement([10, 20, 50]),
        'charging_type' => $faker->randomElement(['percentage', 'amount']),
        'charge_for_owner' => $faker->boolean(50),
        'charge_for_consultant' => $faker->boolean(50),
        'charge_for_booking' => $faker->boolean(50),
        'charge_invoice_type' => $faker->randomElement(['all', 'per_contract', 'per_tenant']),
    ];
});

$factory->state(
    RoomLevel::class,
    'gp_level_1',
    [
        'key' => RoomLevel::GP_LEVEL_KEY_PREFIX.'1',
    ]
);

$factory->state(
    RoomLevel::class,
    'gp_level_2',
    [
        'key' => RoomLevel::GP_LEVEL_KEY_PREFIX.'2',
    ]
);

$factory->state(
    RoomLevel::class,
    'gp_level_3',
    [
        'key' => RoomLevel::GP_LEVEL_KEY_PREFIX.'3',
    ]
);

$factory->state(
    RoomLevel::class,
    'gp_level_4',
    [
        'key' => RoomLevel::GP_LEVEL_KEY_PREFIX.'4',
    ]
);
