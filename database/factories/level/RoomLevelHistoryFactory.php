<?php

use App\Entities\Level\RoomLevelHistory;
use Faker\Generator as Faker;

$factory->define(RoomLevelHistory::class, function (Faker $faker) {
    return [
        'action' => $faker->randomElement(['upgrade', 'downgrade'])
    ];
});
