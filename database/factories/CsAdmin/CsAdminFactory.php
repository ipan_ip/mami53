<?php

use App\Entities\CsAdmin\CsAdmin;

$factory->define(
    CsAdmin::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100000),
            'name' => $faker->name(),
            'email' => $faker->email,
            'user_key' => $faker->word,
            'access_default' => $faker->word,
            'access_token' => $faker->word,
        ];
    }
);
