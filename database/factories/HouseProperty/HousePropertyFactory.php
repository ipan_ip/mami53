<?php
use Faker\Generator as Faker;
use App\Entities\HouseProperty\HouseProperty;

$factory->define(HouseProperty::class, function (Faker $faker) {
    return [
        'song_id' => $faker->numberBetween(100000, 999999),
        'name' => 'Villa Exclusive ' . ucfirst($faker->userName),
        'area_city' => $faker->city,
        'area_subdistrict' => $faker->streetName,
        'area_big' => $faker->state,
        'is_active' => $faker->numberBetween(0, 1),
        'type' => $faker->randomElement(
            [
                'villa',
                'rented_house'
            ]
        ),
        'address' => $faker->address
    ];
});
