<?php

use App\Entities\Squarepants\AgentData;
use Faker\Generator as Faker;

$factory->define(AgentData::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});