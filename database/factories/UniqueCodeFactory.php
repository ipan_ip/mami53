<?php

use App\Entities\Room\Element\UniqueCode;
use App\Entities\Room\Room;
use App\User;
use Faker\Generator as Faker;

$factory->define(
    UniqueCode::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(100000, 999999),
            'code' => $faker->numberBetween(100, 999) . 'AB',
            'trigger' => $faker->name
        ];
    }
);
