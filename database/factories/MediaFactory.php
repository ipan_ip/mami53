<?php

use App\Entities\Media\Media;
use Faker\Generator as Faker;

$factory->define(
    Media::class,
    function (Faker $faker) {
        return [
            'upload_identifier' => $faker->ipv4,
            'upload_identifier_type' => 'admin_ip_address',
            'file_name' => $faker->regexify('[A-Za-z0-9]{8}'),
            'file_path' => 'public/storage/images',
            'type' => 'user_photo',
            'server_location' => 'server-live',
            'media_format' => 'jpg',
        ];
    }
);
