<?php

use App\Entities\FlashSale\FlashSaleAreaLanding;
use Faker\Generator as Faker;

$factory->define(
    FlashSaleAreaLanding::class,
    function (Faker $faker) {
        return [
            'flash_sale_area_id' => $faker->numberBetween(100, 999),
            'landing_id'         => $faker->numberBetween(100, 999),
            'added_by'           => 'administrator :: mamiteam', 
        ];
    }
);
