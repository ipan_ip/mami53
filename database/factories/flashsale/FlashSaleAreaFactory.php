<?php

use App\Entities\FlashSale\FlashSaleArea;
use Faker\Generator as Faker;

$factory->define(
    FlashSaleArea::class,
    function (Faker $faker) {
        return [
            'added_by'  => 'administrator :: mamiteam', 
            'name'      => $faker->name,
        ];
    }
);