<?php

use App\Entities\FlashSale\FlashSaleHistory;
use Faker\Generator as Faker;

$factory->define(
    FlashSaleHistory::class,
    function (Faker $faker) {
        return [
            'flash_sale_id'  => $faker->numberBetween(100, 999),
            'original_value' => '',
            'modified_value' => '',
            'triggered_by'   => 'administrator :: mamiteam', 
        ];
    }
);
