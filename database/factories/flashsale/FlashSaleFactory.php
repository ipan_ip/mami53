<?php

use App\Entities\FlashSale\FlashSale;
use Faker\Generator as Faker;

$factory->define(
    FlashSale::class,
    function (Faker $faker) {
        return [
            'photo_id'      => $faker->numberBetween(100, 999),
            'start_time'    => $faker->dateTime->format('YY-mm-dd'),
            'end_time'      => $faker->dateTime->format('YY-mm-dd'),
            'created_by'    => 'administrator :: mamiteam', 
            'is_active'     => 1,
        ];
    }
);