<?php

$factory->define(
    App\Entities\Owner\SurveySatisfaction::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 1000),
            'identifier' => $faker->numberBetween(1, 1000),
        ];
    }
);
