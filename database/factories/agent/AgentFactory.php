<?php

use App\Entities\Agent\Agent;

$factory->define(
    Agent::class,
    function (Faker\Generator $faker) {
        return [
            'unique_code' => $faker->randomDigitNotNull(),
            'type' => 'kos',
            'name' => $faker->name(),
            'phone_number' => $faker->phoneNumber(),
            'is_active' => (int)rand(0, 1),
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null,
        ];
    }
);
