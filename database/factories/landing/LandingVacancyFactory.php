<?php

use App\Entities\Landing\LandingVacancy;
use Faker\Generator as Faker;

$factory->define(
    LandingVacancy::class,
    function (Faker $faker) {
        return [
            'type' => 'all',
            'slug' => $faker->slug(),
            'area_city' => $faker->cityPrefix(),
            'area_subdistrict' => $faker->cityPrefix(),
        ];
    }
);