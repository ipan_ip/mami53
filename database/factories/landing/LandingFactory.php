<?php

use App\Entities\Landing\LandingApartment;
use Faker\Generator as Faker;
use App\Entities\Landing\LandingList;
use App\Entities\Landing\Landing;

$factory->define(
    LandingList::class,
    function (Faker $faker) {
        return [
            'type' => $faker->randomElement(['kost', 'job', 'spesific', 'apartment']),
            'slug' => $faker->slug,
            'is_active' => $faker->numberBetween(0, 1),
            'parent_id' => $faker->numberBetween(0, 100),
        ];
    }
);

$factory->state(
    LandingList::class,
    LandingList::BOOKING_SPECIFIC,
    [
        'type' => LandingList::BOOKING_SPECIFIC,
    ]
);

$factory->define(
    LandingApartment::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug,
            'latitude_1' => $faker->latitude,
            'longitude_1' => $faker->latitude,
            'latitude_2' => $faker->latitude,
            'longitude_2' => $faker->latitude,
            'area_city' => $faker->city,
            'area_subdistrict' => $faker->streetName,
            'description_1' => $faker->text,
            'description_2' => $faker->text,
            'description_3' => $faker->text,
            'heading_1' => $faker->text(100),
            'heading_2' => $faker->text(100),
            'price_min' => $faker->numberBetween(1, 10000),
            'price_max' => $faker->numberBetween(10000, 10000),
            'rent_type' => $faker->randomElement(['0', '1', '2', '3']),
        ];
    }
);

$factory->define(
    Landing::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug,
            'latitude_1' => $faker->latitude,
            'longitude_1' => $faker->latitude,
            'latitude_2' => $faker->latitude,
            'longitude_2' => $faker->latitude,
            'area_city' => $faker->city,
            'area_subdistrict' => $faker->streetName,
            'heading_1' => $faker->text(100),
            'heading_2' => $faker->text(100),
            'description_1' => $faker->text,
            'description_2' => $faker->text,
            'description_3' => $faker->text,
            'keyword' => $faker->text,
        ];
});

$factory->state(
    Landing::class,
    'campus',
    [
        'type' => 'campus',
    ]
);

$factory->state(
    Landing::class,
    'area',
    [
        'type' => 'area',
    ]
);