<?php

use App\Entities\Landing\LandingMetaOg;
use Faker\Generator as Faker;

$factory->define(
    LandingMetaOg::class,
    function (Faker $faker) {
        return [
            'title' => $faker->sentence(2),
            'description' => $faker->paragraph(3),
            'keywords' => $faker->paragraph(3),
            'is_active' => mt_rand(0,1),
            'image' => $faker->imageUrl(640, 480),
        ];
    }
);

$factory->state(
    LandingMetaOg::class,
    'active',
    [
        'is_active' => true,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::HOMEPAGE,
    [
        'page_type' => LandingMetaOg::HOMEPAGE,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_BOOKING_OWNER,
    [
        'page_type' => LandingMetaOg::LANDING_BOOKING_OWNER,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_AREA,
    [
        'page_type' => LandingMetaOg::LANDING_AREA,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::DETAIL_KOST,
    [
        'page_type' => LandingMetaOg::DETAIL_KOST,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_CAMPUS,
    [
        'page_type' => LandingMetaOg::LANDING_CAMPUS,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_CARI,
    [
        'page_type' => LandingMetaOg::LANDING_CARI,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_PROMOSI_KOST,
    [
        'page_type' => LandingMetaOg::LANDING_PROMOSI_KOST,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_PROMO_KOST,
    [
        'page_type' => LandingMetaOg::LANDING_PROMO_KOST,
    ]
);

$factory->state(
    LandingMetaOg::class,
    LandingMetaOg::LANDING_PAKET_PREMIUM,
    [
        'page_type' => LandingMetaOg::LANDING_PAKET_PREMIUM,
    ]
);