<?php

use App\Entities\Landing\LandingHouseProperty;
use Faker\Generator as Faker;

$factory->define(
    LandingHouseProperty::class,
    function (Faker $faker) {
        return [
            'property_type' => $faker->randomElement(
                [
                    'villa',
                    'rented_house'
                ]
            ),
            'type' => $faker->randomElement(['kost', 'job', 'spesific', 'apartment']),
            'slug' => $faker->slug,
            'is_active' => $faker->numberBetween(0, 1),
            'parent_id' => $faker->numberBetween(0, 100),
        ];
    }
);