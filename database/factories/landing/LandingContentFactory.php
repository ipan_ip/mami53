<?php

use App\Entities\LandingContent\LandingContent;
use Faker\Generator as Faker;

$factory->define(
    LandingContent::class,
    function (Faker $faker) {
        return [
            'title' => $faker->sentence(3),
            'slug' => $faker->slug,
            'type' => 'konten'
        ];
    }
);