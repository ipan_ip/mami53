<?php

use App\Entities\Activity\CallReply;
use Faker\Generator as Faker;

$factory->define(
    CallReply::class,
    function (Faker $faker) {
        return [
            'call_id' => $faker->numberBetween(1, 100000),
            'message' => $faker->name
        ];
    }
);
