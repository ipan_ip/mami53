<?php

use App\Entities\Activity\ActivityLog;
use Faker\Generator as Faker;

$factory->define(
    ActivityLog::class,
    function (Faker $faker) {
        return [
            'id' => $faker->numberBetween(0, 100000),
            'causer_id' => $faker->numberBetween(0, 100000),
            'causer_type' => 'App\User',
            'log_name' => $faker->name . ' edited something',
            'description' => $faker->name . ' edited something',
            'subject_id' => $faker->numberBetween(0, 10000),
            'created_at' => $faker->date(),
        ];
    }
);
