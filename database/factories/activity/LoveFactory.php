<?php

use App\Entities\Activity\Love;
use Faker\Generator as Faker;

$factory->define(
    Love::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1000, 9999),
            'user_id' => $faker->numberBetween(100, 999),
            'status' => $faker->randomElement(
                [
                    'true',
                    'false'
                ]
            ),
            'type' => $faker->randomElement(
                [
                    'like',
                    'bookmark',
                    'series'
                ]
            )
        ];
    }
);
