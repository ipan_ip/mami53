<?php

use App\Entities\Activity\ChatAnswer;
use Faker\Generator as Faker;

$factory->define(
    ChatAnswer::class,
    function (Faker $faker) {
        return [
            'chat_question_id' => $faker->numberBetween(1, 100000),
            'condition' => $faker->name,
            'answer' => $faker->name
        ];
    }
);
