<?php

use App\Entities\Activity\TrackingFeature;
use Faker\Generator as Faker;

$factory->define(
    TrackingFeature::class,
    function (Faker $faker) {
        return [
            'for' => $faker->numberBetween(100, 999),
            'identifier' => $faker->numberBetween(100, 999),
            'total' => $faker->numberBetween(100, 999),
            'from' => $faker->numberBetween(100, 999),
        ];
    }
);
