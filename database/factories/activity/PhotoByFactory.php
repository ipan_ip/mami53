<?php

use App\Entities\Activity\PhotoBy;
use Faker\Generator as Faker;

$factory->define(
    PhotoBy::class,
    function (Faker $faker) {
        return [
            'user_id' => $faker->numberBetween(100, 999),
            'type' => $faker->numberBetween(100, 999),
            'identifier' => $faker->numberBetween(100, 999),
            'photo' => $faker->numberBetween(100, 999),
        ];
    }
);
