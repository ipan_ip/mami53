<?php

use App\Entities\Activity\Tracking;
use Faker\Generator as Faker;

$factory->define(
    Tracking::class,
    function (Faker $faker) {
        return [
            'user_id' => $faker->numberBetween(100, 999),
            'type' => $faker->numberBetween(100, 999),
            'device' => $faker->randomElement(['android', 'iphone', 'tablet', 'desktop', 'robot']),
            'notif_date' => $faker->date(),
            'notif_status' => $faker->numberBetween(0, 1)
        ];
    }
);
