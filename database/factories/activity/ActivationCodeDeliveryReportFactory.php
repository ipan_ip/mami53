<?php

use Faker\Generator as Faker;
use App\Entities\Activity\ActivationCodeDeliveryReport;
use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use Carbon\Carbon;

$factory->define(ActivationCodeDeliveryReport::class, function (Faker $faker) {
    return [
        'network' => $faker->company,
        'status' => $faker->randomElement(ActivationCodeDeliveryReportStatus::getValues()),
        'reason' =>  $faker->realText(),
        'send_at' =>  Carbon::now()->addDays($faker->numberBetween(0, 10)),
        'done_at' =>  Carbon::now()->addDays($faker->numberBetween(0, 10)),
    ];
});
