<?php

use App\Entities\Activity\CallReply;
use App\Entities\Activity\ChatCount;
use Faker\Generator as Faker;

$factory->define(
    ChatCount::class,
    function (Faker $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100000),
            'device_id' => $faker->numberBetween(1, 100000),
            'designer_id' => $faker->numberBetween(1, 100000),
            'status' => $faker->boolean,
            'type' => $faker->randomElement(['view', 'ads']),
            'count' => $faker->numberBetween(1, 100000),
            'bookmark_id' => $faker->numberBetween(1, 100000)
        ];
    }
);
