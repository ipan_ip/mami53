<?php

use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp3;
use Faker\Generator as Faker;

$factory->define(
    View::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 999),
            'status' => 'true',
            'user_id' => $faker->numberBetween(1, 999),
            'device_id' => $faker->numberBetween(1, 999),
            'type' => 'view',
            'count' => $faker->numberBetween(1, 100)
        ];
    }
);

$factory->define(
    ViewTemp3::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 999),
            'user_id' => $faker->numberBetween(1, 999),
            'device_id' => $faker->numberBetween(1, 999),
            'type' => 'view',
            'count' => $faker->numberBetween(1, 100),
            'status' => 'logged',
        ];
    }
);
