<?php

use App\Entities\Activity\RatingCS;
use Faker\Generator as Faker;

$factory->define(
    RatingCS::class,
    function (Faker $faker) {
        return [
            'admin_id' => $faker->numberBetween(1000, 9999),
            'group_id' => $faker->numberBetween(1000, 9999),
            'user_id' => $faker->numberBetween(100, 999),
            'rating' => $faker->numberBetween(1, 5),
            'comment' => $faker->word
        ];
    }
);
