<?php

use App\Entities\NotificationSurvey\NotificationSurvey;
use Faker\Generator as Faker;

$factory->define(NotificationSurvey::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});