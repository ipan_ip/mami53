<?php

use App\Entities\NotificationSurvey\NotificationSurveyRecipient;
use Faker\Generator as Faker;

$factory->define(NotificationSurveyRecipient::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now(),
    ];
});