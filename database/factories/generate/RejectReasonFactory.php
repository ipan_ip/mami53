<?php

use App\Entities\Generate\RejectReason;

$factory->define(
    RejectReason::class,
    function (Faker\Generator $faker) {
        return [
            'group' => 'room_photo',
            'content' => $faker->sentence,
        ];
    }
);
