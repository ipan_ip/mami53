<?php

use App\Entities\Generate\ListingReason;

$factory->define(
    ListingReason::class,
    function (Faker\Generator $faker) {
        return [
            'from' => $faker->randomElement(["dummy", "live", "room"]),
            'listing_id' => rand(1, 99),
            'content' => $faker->sentence,
        ];
    }
);
