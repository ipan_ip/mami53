<?php

use App\Entities\Generate\ListingRejectReason;

$factory->define(
    ListingRejectReason::class,
    function (Faker\Generator $faker) {
        return [
            'scraping_listing_reason_id' => rand(1, 99),
            'reject_reason_id' => rand(1, 99),
            'is_fixed' => false,
            'content_other' => null,
        ];
    }
);
