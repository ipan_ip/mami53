<?php

use App\Entities\Alarm\UserFilter;
use Faker\Generator as Faker;

$factory->define(
    UserFilter::class,
    function (Faker $faker) {
        return [
            'email' => $faker->email,
            'user_id' => $faker->numberBetween(1, 100000),
            'device_id' => $faker->numberBetween(1, 100000),
            'subscriber_id' => $faker->numberBetween(1, 100000),
            'filter' => $faker->numberBetween(1, 100000),
            'source' => $faker->numberBetween(1, 100000),
            'product_type' => $faker->numberBetween(1, 100000),
            'last_sent' => $faker->date()
        ];
    }
);
