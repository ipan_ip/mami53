<?php

use Faker\Generator as Faker;
use App\Entities\User\Notification;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigitNotNull,
        'category_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'url' => $faker->url,
        'scheme' => $faker->url,
        'read' => 'false',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
