<?php

use Faker\Generator as Faker;
use App\Entities\Notif\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => $faker->imageUrl(),
        'order' => $faker->randomDigitNotNull,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
