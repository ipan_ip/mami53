<?php

$factory->define(
    App\Entities\Facility\FacilityCategory::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 100000)
        ];
    }
);