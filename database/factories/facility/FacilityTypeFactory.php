<?php

$factory->define(
    App\Entities\Facility\FacilityType::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 100000)
        ];
    }
);