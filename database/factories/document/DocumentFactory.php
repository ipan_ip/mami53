<?php

use App\Entities\Document\Document;
use Faker\Generator as Faker;

$factory->define(
    Document::class,
    function (Faker $faker) {
        return [
            'file_name' => $faker->regexify('[A-Za-z0-9]{8}'),
            'file_path' => 'public/storage/images',
            'type' => 'room_term',
            'format' => 'pdf',
        ];
    }
);
