<?php

use App\Entities\Point\PointOwnerRoomGroup;
use Faker\Generator;

$factory->define(PointOwnerRoomGroup::class, function (Generator $faker) {
    return [
        'floor' => 0,
        'ceil' => 0,
    ];
});
