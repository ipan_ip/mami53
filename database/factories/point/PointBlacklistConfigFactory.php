<?php

use App\Entities\Point\PointBlacklistConfig;
use Faker\Generator;

$factory->define(PointBlacklistConfig::class, function (Generator $faker) {
    return [
        'usertype' => $faker->word,
        'value' => $faker->randomNumber(2),
        'is_blacklisted' => rand(0, 1),
    ];
});
