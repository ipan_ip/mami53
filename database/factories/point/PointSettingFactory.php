<?php

use App\Entities\Point\PointSetting;
use Faker\Generator;

$factory->define(PointSetting::class, function (Generator $faker) {
    return [
        'point_id' => 0,
        'activity_id' => 0,
        'room_group_id' => 0,
        'received_each' => 1,
        'times_to_received' => 1,
        'limit_type' => $faker->randomElement(['once', 'daily', 'weekly', 'monthly']),
        'limit' => 1,
        'is_active' => $faker->randomElement([0, 1]),
    ];
});
