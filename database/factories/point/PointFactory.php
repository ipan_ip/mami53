<?php

use App\Entities\Point\Point;
use Faker\Generator;

$factory->define(Point::class, function (Generator $faker) {
    return [
        'type' => substr($faker->word . time() . rand(1, 1000), 0, 25),
        'title' => $faker->words(3, true),
        'kost_level_id' => 0,
        'room_level_id' => 0,
        'value' => Point::DEFAULT_VALUE,
        'tnc' => $faker->randomHtml(2, 2),
        'expiry_value' => rand(1, 12),
        'expiry_unit' => 'month',
        'target' => $faker->randomElement(array_keys(Point::getTargetOptions())),
        'is_published' => 1,
    ];
});
