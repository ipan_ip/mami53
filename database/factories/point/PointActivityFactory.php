<?php

use App\Entities\Point\PointActivity;
use Faker\Generator;

$factory->define(PointActivity::class, function (Generator $faker) {
    return [
        'key' => $faker->word . time() . rand(1, 1000),
        'name' => $faker->words(3, true),
        'title' => $faker->words(3, true),
        'description' => $faker->paragraph,
        'target' => $faker->randomElement(['owner', 'tenant']),
        'sequence' => rand(0, 5),
        'is_active' => $faker->randomElement([0, 1]),
    ];
});
