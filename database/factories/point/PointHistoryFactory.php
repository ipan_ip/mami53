<?php

use App\Entities\Point\PointHistory;
use Faker\Generator;

$factory->define(PointHistory::class, function (Generator $faker) {
    return [
        'user_id' => 0,
        'activity_id' => 0,
        'redeem_id' => 0,
        'value' => 0,
        'balance' => 0,
        'notes' => $faker->sentence,
    ];
});
