<?php

use App\Entities\Point\PointUser;
use Faker\Generator;

$factory->define(PointUser::class, function (Generator $faker) {
    return [
        'user_id' => 0,
        'total' => $faker->randomNumber(2),
        'is_blacklisted' => rand(0, 1),
    ];
});
