<?php

$factory->define(
    App\Entities\Config\AppConfig::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->word,
            'value' => $faker->word
        ];
    }
);
