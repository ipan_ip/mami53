<?php

use App\Entities\Landing\HomeStaticLanding;
use Faker\Generator as Faker;

$factory->define(
    HomeStaticLanding::class,
    function (Faker $faker) {
        return [
            'parent_id' => null,
            'type' => $faker->randomElement(
                [
                    'kos',
                    'apartment'
                ]
            ),
            'name' => $faker->randomElement(
                [
                    'Kos Jakarta',
                    'Kos Surabaya',
                    'Kos Yogyakarta',
                    'Kos Bandung',
                    'Kos Malang'
                ]
            )
        ];
    }
);
