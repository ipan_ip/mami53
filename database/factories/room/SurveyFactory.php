<?php

use App\Entities\Room\Room;
use App\Entities\Room\Survey;
use App\User;

$factory->define(
    Survey::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => factory(Room::class)->create(),
            'user_id' => factory(User::class)->create(),
            'name' => 'name',
            'gender' => $faker->randomElement(['male', 'female']),
            'birthday' => now(),
            'job' => $faker->randomElement(['kuliah', 'kerja', 'lainnya']),
            'work_place' => 'work place',
            'position' => 'position',
            'semester' => '1',
            'kost_time' => 'kost time',
            'time' => now(),
            'notification_time' => now(),
            'notification' => $faker->randomElement(['0', '1']),
            'forward' => rand(1, 5),
            'serious' => rand(1, 5),
            'chat_group_id' => null,
            'admin_id' => null
        ];
    }
);
