<?php

use Faker\Generator as Faker;
use App\Entities\Room\DataStage;

$factory->define(DataStage::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(1, 1000),
        'name' => DataStage::AVAILABLE_STAGES[
            array_rand(DataStage::AVAILABLE_STAGES)
        ],
        'complete' => false,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});

$factory->state(
    DataStage::class,
    'complete',
    [
        'complete' => true,
    ]
);

