<?php

use App\Entities\Level\KostLevelValue;
use App\Entities\Room\KostValue;
use App\Entities\Room\Room;

$factory->define(
    KostValue::class,
    function () {
        return [
            'designer_id' => factory(Room::class)->create(),
            'kost_level_value_id' => factory(KostLevelValue::class)->create(),
            'created_by' => now(),
        ];
    }
);
