<?php

$factory->define(
    App\Entities\Room\RoomTypeFacility::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 100000)
        ];
    }
);
