<?php


$factory->define(
    \App\Entities\Room\PriceFilter::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(0, 100),
            'final_price_daily' => 10000,
            'final_price_weekly' => 100000,
            'final_price_monthly' => 1000000,
            'final_price_quarterly' => 3000000,
            'final_price_semiannually' => 6000000,
            'final_price_yearly' => 12000000
        ];
    }
);