<?php

$factory->define(
    App\Entities\Room\RoomOwner::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'user_id' => $faker->numberBetween(1, 1000),
        ];
    }
);

$factory->state(
    App\Entities\Room\RoomOwner::class,
    'owner',
    [
        'owner_status' => App\Entities\Room\RoomOwner::STATUS_TYPE_KOS_OWNER,
        'status' => App\Entities\Room\RoomOwner::ROOM_VERIFY_STATUS
    ]
);
