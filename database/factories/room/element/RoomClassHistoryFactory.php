<?php

use App\Entities\Room\Room;
use App\Entities\Room\RoomClassHistory;

$factory->define(
    RoomClassHistory::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => factory(Room::class)->create(),
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null,
        ];
    }
);
