<?php

use App\Entities\Room\Element\InputTracker;
use App\Entities\Room\Room;
use App\User;

$factory->define(
    InputTracker::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => factory(User::class)->create()->id,
            'designer_id' => factory(Room::class)->create()->id,
            'input_source' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
);
