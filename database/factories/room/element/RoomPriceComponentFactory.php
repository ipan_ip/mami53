<?php

use App\Entities\Room\Element\RoomPriceComponent;
use App\Entities\Room\Room;

$factory->define(
    RoomPriceComponent::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => factory(Room::class)->create()->id,
            'price_name' => $faker->randomElement(['maintenance','parking']),
            'price_label' => 'price label '.rand(1,999),
            'price_reguler' => 0.00,
            'price_sale' => 0.00,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null
        ];
    }
);
