<?php

use App\Entities\Room\Element\CreationFlag;
use Faker\Generator as Faker;

$factory->define(CreationFlag::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(0, 1500),
        'create_from' => collect([
            CreationFlag::CREATE_NEW,
            CreationFlag::DUPLICATE_FROM_TYPE,
            CreationFlag::DUPLICATE_WITHOUT_TYPE
        ])->random(),
        'log' => null
    ];
});
