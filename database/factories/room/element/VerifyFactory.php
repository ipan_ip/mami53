<?php

use App\Entities\Room\Element\Verify;

$factory->define(
    Verify::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'action' => 'verify',
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null
        ];
    }
);
