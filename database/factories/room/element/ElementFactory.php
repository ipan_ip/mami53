<?php

use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use Faker\Generator as Faker;

$factory->define(
    Tag::class,
    function (Faker $faker) {
        return [
            'name' => $faker->name,
            'code' => $faker->name,
            'type' => $faker->randomElement(['fac_room', 'fac_share']),
            'is_for_filter' => $faker->numberBetween(0, 1)
        ];
    }
);

$factory->define(
    TagType::class,
    function (Faker $faker) {
        return [
            'tag_id' => $faker->numberBetween(0, 100),
            'name' => $faker->randomElement(['fac_share', 'fac_room', 'service', 'kos_rule', 'fac_bath']),
        ];
    }
);

$factory->define(
    RoomTag::class,
    function (Faker $faker) {
        return [
            'tag_id' => $faker->numberBetween(1, 1000),
            'designer_id' => $faker->numberBetween(1, 1000),
        ];
    }
);

$factory->define(
    CardPremium::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'description' => $faker->sentence(2, true),
        ];
    }
);

$factory->define(
    Card::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'description' => $faker->sentence(2, true),
        ];
    }
);
