<?php

use App\Entities\Room\Element\Report;
use Faker\Generator as Faker;

$factory->define(Report::class, function (Faker $faker) {
    return [
        'report_parent_id' => $faker->numberBetween(1, 100000),
        'designer_id' => $faker->numberBetween(1, 100000),
        'user_id' => $faker->numberBetween(1, 100000),
        'report_type' => $faker->word,
        'report_description' => $faker->word,
        'status' => $faker->word
    ];
});
