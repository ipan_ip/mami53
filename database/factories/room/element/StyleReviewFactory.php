<?php

use App\Entities\Room\Element\StyleReview;

$factory->define(
    StyleReview::class,
    function (Faker\Generator $faker) {
        static $roomName = 0;
        return [
            'review_id' => $faker->numberBetween(1, 1000),
            'photo_id' => $faker->numberBetween(1, 1000),
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null
        ];
    }
);
