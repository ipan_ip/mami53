<?php

use App\Entities\Room\Element\Unit;

$factory->define(
    Unit::class,
    function (Faker\Generator $faker) {
        static $roomName = 0;
        return [
            'designer_type_id' => $faker->numberBetween(1, 1000),
            'name' => 'unit '.(string) ++$roomName,
            'floor' => '1',
            'is_available' => true,
            'is_active' => true
        ];
    }
);
