<?php

use App\Entities\Room\Element\RoomUnit;

$factory->define(
    RoomUnit::class,
    function (Faker\Generator $faker) {
        static $roomName = 0;
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'name' => (string) ++$roomName,
            'floor' => '1',
            'occupied' => false,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null
        ];
    }
);
