<?php

use App\Entities\Room\Element\TypeCard;

$factory->define(
    TypeCard::class,
    function (Faker\Generator $faker) {
        static $roomName = 0;
        return [
            'designer_type_id' => $faker->numberBetween(1, 1000),
            'description' => $faker->text(),
            'type' => $faker->randomElement(['video_file', 'video', 'image', 'gif', 'text', 'webview_html', 'webview_url', 'link_webview', 'link', 'quiz', 'faisal', 'vote']),
            'photo_id' => $faker->numberBetween(1, 1000),
            'sub_type' => $faker->randomElement(['video_file', 'video', 'image', 'gif', 'text', 'webview_html', 'webview_url', 'link_webview', 'link']),
            'layout_text' => $faker->randomElement(['box', 'flow', 'center']),
            'layout_photo' => $faker->randomElement(['default', 'keep_text']),
            'gender' => $faker->randomElement(['male', 'female']),
            'source' => null,
            'ordering' => null,
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null
        ];
    }
);
