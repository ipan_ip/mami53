<?php

use App\Entities\Room\Element\RoomTermDocument;

$factory->define(
    RoomTermDocument::class,
    function (Faker\Generator $faker) {
        return [
            'designer_term_id' => $faker->numberBetween(1, 100)
        ];
    }
);
