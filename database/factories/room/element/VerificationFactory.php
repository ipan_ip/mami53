<?php

use App\Entities\Room\Element\Verification;
use Faker\Generator as Faker;

$factory->define(Verification::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(0, 1000),
        'requested_by_user_id' => $faker->numberBetween(0, 1000),
        'is_ready' =>  $faker->boolean()
    ];
});
