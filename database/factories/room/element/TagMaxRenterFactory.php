<?php

use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagMaxRenter;
use Faker\Generator as Faker;

$factory->define(
    TagMaxRenter::class,
    function (Faker $faker) {
        return [
            'tag_id' => factory(Tag::class)->create()
        ];
    }
);
