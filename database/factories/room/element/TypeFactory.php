<?php

use App\Entities\Room\Element\Type;

$factory->define(
    Type::class,
    function (Faker\Generator $faker) {
        static $roomName = 0;
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'name' => 'type '.(string) ++$roomName,
            'maximum_occupancy' => $faker->numberBetween(1, 10),
            'tenant_type' => '0',
            'room_size' => '3 x 4',
            'total_room' => $faker->numberBetween(1, 10),
            'is_available' => $faker->randomElement([true,false]),
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null
        ];
    }
);
