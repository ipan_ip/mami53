<?php

use App\Entities\Room\Price;

$factory->define(
    Price::class,
    function () {
        return [
            'reference' => Price::ROOM_TYPE_REFERENCE,
        ];
    }
);
