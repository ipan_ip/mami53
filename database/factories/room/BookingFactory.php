<?php

use App\Entities\Room\Booking;
use App\User;

$factory->define(
    Booking::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => factory(User::class)->create(),
            'designer_id' => factory(User::class)->create(),
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null,
        ];
    }
);
