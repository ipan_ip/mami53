<?php

use App\Entities\Room\PriceAdditional;

$factory->define(
    PriceAdditional::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
        ];
    }
);
