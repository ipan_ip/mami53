<?php

use Faker\Generator as Faker;
use App\Entities\Room\ReviewReply;

$factory->define(ReviewReply::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 100000),
        'review_id' => $faker->numberBetween(1, 100000),
        'content' => $faker->word,
        'status' => $faker->word
    ];
});
