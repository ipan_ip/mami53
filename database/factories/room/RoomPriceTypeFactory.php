<?php

use App\Entities\Room\RoomPriceType;

$factory->define(
    RoomPriceType::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' =>  $faker->numberBetween(1,100),
            'type' => 'usd',
            'is_active' => $faker->randomElement([true,false]),
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null
        ];
    }
);