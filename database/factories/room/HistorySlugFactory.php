<?php

use App\Entities\Room\HistorySlug;

$factory->define(
    HistorySlug::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(100000, 999999),
            'slug' => $faker->slug,
            'status' => true,
        ];
    }
);
