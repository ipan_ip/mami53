<?php

use App\Entities\Room\RoomTerm;

$factory->define(
    RoomTerm::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 100)
        ];
    }
);
