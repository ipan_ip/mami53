<?php

use App\Entities\Room\ThanosHidden;

$factory->define(
    ThanosHidden::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'snapped' => true,
            'log' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
);
