<?php

use App\Entities\Room\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(1,1000),
        'user_id' => $faker->numberBetween(1,1000),
        'is_anonim' => $faker->numberBetween(0,1),
    ];
});
