<?php

use App\Entities\Room\Element\Type;

$factory->define(
    App\Entities\Room\RoomTypeFacilitySetting::class,
    function (Faker\Generator $faker) {
        return [
            'designer_type_id' => factory(Type::class)->create(),
            'active_tags' => $faker->text(),
        ];
    }
);
