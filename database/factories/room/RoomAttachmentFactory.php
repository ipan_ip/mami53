<?php

$factory->define(
    App\Entities\Room\RoomAttachment::class,
    function (Faker\Generator $faker) {
        return [
            'matterport_id' => $faker->md5()
        ];
    }
);

$factory->state(
    App\Entities\Room\RoomAttachment::class,
    'active',
    [
        'is_matterport_active' => true
    ]
);

$factory->state(
    App\Entities\Room\RoomAttachment::class,
    'inactive',
    [
        'is_matterport_active' => false
    ]
);