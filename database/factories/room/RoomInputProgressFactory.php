<?php

$factory->define(
    App\Entities\Room\RoomInputProgress::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 100000)
        ];
    }
);