<?php

use App\Entities\Room\ContactUser;
use App\User;
use App\Entities\Room\Room;

$factory->define(
    ContactUser::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => factory(User::class)->create(),
            'designer_id' => factory(Room::class)->create(),
            'status_call' => null,
            'created_at' => now(),
        ];
    }
);
