<?php

use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Element\UniqueCode;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;

$factory->define(
    Room::class,
    function (Faker\Generator $faker) {
        return [
            'song_id' => $faker->numberBetween(100000, 999999),
            'name' => 'Kos Exclusive ' . ucfirst($faker->userName),
            'gender' => $faker->numberBetween(0, 1),
            'area' => $faker->city,
            'area_city' => $faker->city,
            'area_subdistrict' => $faker->streetName,
            'area_big' => $faker->state,
            'status' => $faker->numberBetween(0, 1),
            'room_available' => $faker->numberBetween(1, 10),
            'room_count' => $faker->numberBetween(10, 99),
            'is_booking' => 0,
            'sort_score' => 0,
            'address' => $faker->address
        ];
    }
);


$factory->state(
    Room::class,
    'active',
    [
        'is_active' => 'true',
        'expired_phone' => 0
    ]
);

$factory->state(
    Room::class,
    'inactive',
    [
        'is_active' => 'false',
        'expired_phone' => 1
    ]
);

$factory->state(
    Room::class,
    'mamirooms',
    [
        'is_mamirooms' => 1
    ]
);

$factory->state(
    Room::class,
    'notMamirooms',
    [
        'is_mamirooms' => 0
    ]
);

$factory->state(
    Room::class,
    'availableBooking',
    [
        'is_booking' => 1
    ]
);

$factory->state(
    Room::class,
    'notAvailableBooking',
    [
        'is_booking' => 0
    ]
);


$factory->state(
    Room::class,
    'notTesting',
    [
        'is_testing' => 0
    ]
);

$factory->state(
    Room::class,
    'availablePriceMonthly',
    function ($faker) {
        return [
            'price_monthly' => $faker->numberBetween(1000000, 9999999)
        ];
    }
);

$factory->state(
    Room::class,
    'premium',
    [
        'is_promoted' => 'true'
    ]
);

$factory->state(
    Room::class,
    'notPremium',
    [
        'is_promoted' => 'false'
    ]
);
$factory->state(
    Room::class,
    'with-all-prices',
    function () {
        static $dailyPriceRanges    = ['10000', '20000', '30000', '40000', '50000'];
        static $weeklyPriceRanges   = ['100000', '200000', '300000', '400000', '500000'];
        static $monthlyPriceRanges  = ['1000000', '2000000', '3000000', '4000000', '5000000'];
        static $yearlyPriceRanges   = ['10000000', '20000000', '30000000', '40000000', '50000000'];
        static $quarterlyPriceRanges   = ['600000', '6500000', '7000000', '7500000'];
        static $semiannuallyPriceRanges   = ['8000000', '8500000', '9000000', '9500000'];

        return [
            'price_daily' => collect($dailyPriceRanges)->random(),
            'price_weekly' => collect($weeklyPriceRanges)->random(),
            'price_monthly' => collect($monthlyPriceRanges)->random(),
            'price_yearly' => collect($yearlyPriceRanges)->random(),
            'price_quarterly' => collect($quarterlyPriceRanges)->random(),
            'price_semiannually' => collect($semiannuallyPriceRanges)->random(),
        ];
    }
);

$factory->state(
    Room::class,
    'with-small-rooms',
    [
        'room_available' => rand(1, 8),
        'room_count' => rand(8, 15),
    ]
);

$factory->state(
    Room::class,
    'with-room-unit',
    [
        'room_available' => rand(1, 8),
        'room_count' => rand(8, 15),
    ]
);

$factory->afterCreatingState(
    Room::class,
    'with-room-unit',
    function ($room) {
        factory(RoomUnit::class, $room->room_count)->create([
            'occupied' => true,
            'designer_id' => $room->id
        ])->take($room->room_available)->each(function ($unit) {
            $unit->occupied = false;
            $unit->save();
        });
    }
);

$factory->state(
    Room::class,
    'with-unique-code',
    []
);

$factory->afterCreatingState(
    Room::class,
    'with-unique-code',
    function ($room, Faker\Generator $faker) {
        static $increments = 0;
        static $codeSuffle = ['AB', 'BD', 'CT', 'AU', 'OA', 'IP'];
        factory(UniqueCode::class)->create([
            'designer_id' => $room->id,
            'code' => $faker->numberBetween(10, 99) . collect($codeSuffle)->random() . (string) $increments++
        ]);
    }
);

$factory->state(
    Room::class,
    'with-slug',
    [
        "slug" => "this-is-the-slug-please-test-me",
    ]
);

$factory->state(
    Room::class,
    'attribute-complete-state',
    function (Faker\Generator $faker) {
        return [
            'owner_name' => $faker->name,
            'owner_phone' => ('08' . $faker->randomNumber(8)),
            'manager_name' => $faker->name,
            'manager_phone' => ('08' . $faker->randomNumber(8)),
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'address' => $faker->address,
            'photo_count' => $faker->numberBetween(5, 20),
            'description' => $faker->sentences(3, true)
        ];
    }
);

$factory->state(
    Room::class,
    'with-latlong',
    [
        "latitude" => -7.772256,
        "longitude" => 110.370937
    ]
);

$factory->state(
    Room::class,
    'gp1',
    []
);

$factory->afterCreatingState(
    Room::class,
    'gp-random',
    function (Room $room) {
        $room->changeLevel(
            collect(KostLevel::getGoldplusLevelIds())->random()
        );
    }
);

$factory->afterCreatingState(
    Room::class,
    'new-gp-random',
    function (Room $room) {
        $room->changeLevel(
            collect(KostLevel::getGoldplusLevelIdsByLevel())->random()
        );
    }
);

$factory->afterCreatingState(
    Room::class,
    'gp3-random',
    function (Room $room) {
        $room->level()->detach();
        $room->level()->attach(collect(KostLevel::getGoldplusLevelIdsByGroups(3))->random());
    }
);

$factory->state(
    Room::class,
    'with-owner',
    []
);

$factory->afterCreatingState(
    Room::class,
    'with-owner',
    function ($room, Faker\Generator $faker) {
        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'owner_status' => $faker->randomElement(['Pemilik Kos', 'Pemilik Kost'])
        ]);
    }
);

$factory->state(
    Room::class,
    'with-address',
    function (Faker\Generator $faker) {
        return [
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'address' => $faker->address,
            'area_city' => collect(['Malang', 'Sleman', 'Surabaya', 'Bekasi', 'Bandung', 'Jakarta Selatan'])->random(),
            'area_subdistrict' => collect(['Blimbing', 'Depok', 'Jemursari', 'Jatiasih', 'Sukasari', 'Jagakarsa'])->random(),
        ];
    }
);

$factory->state(
    Room::class,
    'with-mamipay-owner',
    []
);

$factory->afterCreatingState(
    Room::class,
    'with-mamipay-owner',
    function ($room, Faker\Generator $faker) {
        $user = factory(User::class)->create(['is_owner' => 'true']);

        factory(MamipayOwner::class)->create([
            'user_id' => $user->id,
            'status' => 'approved'
        ]);

        factory(RoomOwner::class)->create([
            'designer_id' => $room->id,
            'owner_status' => $faker->randomElement(['Pemilik Kos', 'Pemilik Kost']),
            'user_id' => $user->id
        ]);
    }
);

$factory->state(
    Room::class,
    'with-kost-level',
    []
);

$factory->afterCreatingState(
    Room::class,
    'with-kost-level',
    function ($room, Faker\Generator $faker) {
        $level = factory(KostLevel::class)->create();

        factory(KostLevelMap::class)->create([
            'kost_id' => $room->song_id,
            'level_id' => $level->id
        ]);
    }
);

$factory->state(
    Room::class,
    'with-consultant',
    []
);

$factory->afterCreatingState(
    Room::class,
    'with-consultant',
    function ($room, Faker\Generator $faker) {
        factory(ConsultantRoom::class)->create([
            'consultant_id' => $faker->numberBetween(1, 10000),
            'designer_id' => $room->id
        ]);
    }
);

$factory->state(
    Room::class,
    'with-owner-collumn',
    function (Faker\Generator $faker) {
        return [
            'owner_name' => $faker->name,
            'owner_phone' => ('08' . $faker->randomNumber(8)),
            'manager_name' => $faker->name,
            'manager_phone' => ('08' . $faker->randomNumber(8)),
        ];
    }
);

$factory->state(
    Room::class,
    'with-address-note',
    [
        'guide' => 'Dekat wartel pertigaan kedua belok lagi'
    ]
);

$factory->afterCreatingState(
    Room::class,
    'with-address-note',
    function ($room, Faker\Generator $faker) {
        $province = factory(Province::class)->create(['name' => 'Jawa Tengah']);
        $city = factory(City::class)->create(['area_province_id' => $province->id, 'name' => 'Kota Solo']);
        $subdistrict = factory(Subdistrict::class)->create(['area_city_id' => $city->id, 'name' => 'Kelayar']);

        factory(AddressNote::class)->create([
            'designer_id' => $room->id,
            'area_province_id' => $province->id,
            'area_city_id' => $city->id,
            'area_subdistrict_id' => $subdistrict->id,
            'note' => $faker->sentences(2, true)
        ]);
    }
);

$factory->state(
    Room::class,
    'has-price-remark',
    ['price_remark' => 'DP 500 rb']
);
