<?php

$factory->define(
    App\Entities\Room\RoomFacility::class,
    function (Faker\Generator $faker) {
        return [
            'id' => $faker->numberBetween(1, 100000)
        ];
    }
);