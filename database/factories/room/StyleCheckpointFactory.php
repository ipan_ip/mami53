<?php

use App\Entities\Room\Room;
use App\Entities\Room\StyleCheckpoint;
use App\User;

$factory->define(
    StyleCheckpoint::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => factory(Room::class)->create(),
            'user_id' => factory(User::class)->create(),
            'photo_id' => $faker->numberBetween(1, 9999),
            'created_at' => now(),
            'updated_at' => null,
            'deleted_at' => null,
        ];
    }
);
