<?php

use App\Entities\Room\Geolocation;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Point;

$factory->define(
    Geolocation::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->numberBetween(1,100),
        'geolocation' => new Point($faker->latitude, $faker->longitude)
    ];
});
