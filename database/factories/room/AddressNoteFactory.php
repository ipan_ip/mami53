<?php

use App\Entities\Room\Element\AddressNote;
use Faker\Generator as Faker;

$factory->define(AddressNote::class, function (Faker $faker) {
    return [
        'designer_id' => $faker->randomNumber(2),
        'note' => $faker->sentences(3, true),
    ];
});
