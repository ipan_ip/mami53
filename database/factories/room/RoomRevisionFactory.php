<?php

use App\Entities\Room\RoomRevision;
use App\User;

$factory->define(
    RoomRevision::class,
    function (Faker\Generator $faker) {
        return [
            'revisionable_type' => $faker->words(2, true),
            'revisionable_id' => $faker->numberBetween(100000, 999999),
            'user_id' => factory(User::class)->create(),
            'key' => $faker->word(),
            'old_value' => null,
            'new_value' => null,
            'created_at' => now(),
        ];
    }
);
