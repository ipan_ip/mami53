<?php

$factory->define(
    App\Entities\Room\RoomAvailableTracker::class,
    function (Faker\Generator $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'before_value' => 0,
            'after_value' => 0,
            'price_before' => 0,
            'price_after' => 0,
            'price_daily_before' => 0,
            'price_daily_after' => 0,
            'price_weekly_before' => 0,
            'price_weekly_after' => 0,
            'price_monthly_before' => 0,
            'price_monthly_after' => 0,
            'price_yearly_before' => 0,
            'price_yearly_after' => 0,
        ];
    }
);
