<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;

$factory->define(
    MamipayRoomPriceComponentAdditional::class,
    function (Faker $faker) {
        return [
            'key' => $faker->words(3, true),
            'value' => $faker->words(3, true),
        ];
    }
);
