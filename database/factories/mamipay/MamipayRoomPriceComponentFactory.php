<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;

$factory->define(
    MamipayRoomPriceComponent::class,
    function (Faker $faker) {
        return [
            'component' => $faker->randomElement([
                MamipayRoomPriceComponentType::ADDITIONAL,
                MamipayRoomPriceComponentType::FINE,
                MamipayRoomPriceComponentType::DEPOSIT,
                MamipayRoomPriceComponentType::DP
            ]),
            'name' => $faker->words(3, true),
            'label' => $faker->words(3, true),
            'price' => 500000,
            'is_active' => false
        ];
    }
);
