<?php

use App\Entities\Mamipay\Misc\ManualPayout;
use App\Entities\Mamipay\Misc\ManualPayout\Status;
use App\Entities\Mamipay\Misc\ManualPayout\Type;
use Faker\Generator as Faker;

$factory->define(ManualPayout::class, function (Faker $faker) {
    return [
        'provider' => 'bigflip',
        'transaction_id' => null,
        'destination_account' => '1231232312',
        'destination_bank' => $faker->randomElement(['bni', 'bca', 'mandiri', 'dbs', 'bca_syr', 'citibank']),
        'destination_bank_city' => $faker->numberBetween(100, 999),
        'destination_name' => $faker->name,
        'transfer_amount' => $faker->numberBetween(1000000, 10000000),
        'reason' => 'Coba Refund',
        'invoice_id' => $faker->numberBetween(0, 1000000),
        'status' => $faker->randomElement(Status::getValues()),
        'type' => $faker->randomElement(Type::getValues()),
        'transferred_at' => $faker->dateTime(),
        'created_by' => $faker->numberBetween(1, 1000000)
    ];
});
