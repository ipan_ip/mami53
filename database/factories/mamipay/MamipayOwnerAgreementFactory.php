<?php

use App\Entities\Mamipay\MamipayOwnerAgreement;

$factory->define(
    MamipayOwnerAgreement::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100000),
            'tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
);
