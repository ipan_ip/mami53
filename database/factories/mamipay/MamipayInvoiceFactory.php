<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayInvoice;

$factory->define(MamipayInvoice::class, function (Faker $faker) {
    return [
        'contract_id' => 148,
        'shortlink' => 'G1Vq0',
        'invoice_number' => '31250698/2019/03/0001',
        'shortlink' => 'i9v4e',
        'name' => 'Pembayaran bulan ke-1',
        'scheduled_date' => $faker->dateTime,
        'amount' => 3350000,
        'status' => MamipayInvoice::STATUS_UNPAID,
        'from_booking' => $faker->randomElement([null, 0, 1])
    ];
});
