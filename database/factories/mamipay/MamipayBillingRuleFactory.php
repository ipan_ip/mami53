<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayBillingRule;

$factory->define(
    MamipayBillingRule::class,
    function (Faker $faker) {
        return [
            'tenant_id' => 100,
            'room_id' => 101,
            'fixed_billing' => false,
            'billing_date' => $faker->dateTime->format("Y-m-d"),
            'base_amount' => 2000000.0,
            'first_amount' => 1000000.0,
            'deposit_amount' => 500000.0,
            'fine_amount' => 100000.0,
            'fine_maximum_length' => 10,
            'fine_duration_type' => 'day',
        ];
    }
);
