<?php

use App\Entities\Mamipay\MamipayUserDevice;
use Faker\Generator as Faker;

$factory->define(MamipayUserDevice::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10000),
        'mamiroom_user_id' => $faker->numberBetween(1, 10000),
        'mamiroom_user_phone_number' => $faker->phoneNumber,
        'device_token' => $faker->numberBetween(1, 10000),
        'email' => $faker->email,
        'model' => $faker->numberBetween(1, 10000),
        'identifier' => $faker->numberBetween(1, 10000),
        'uuid' => $faker->numberBetween(1, 10000),
        'platform' => $faker->numberBetween(1, 10000),
        'platform_version_code' => $faker->numberBetween(1, 10000),
        'app_notif_token' => $faker->numberBetween(1, 10000),
        'app_version_code' => $faker->numberBetween(1, 10000),
        'app_install_count' => $faker->numberBetween(1, 100),
        'app_update_count' => $faker->numberBetween(1, 100),
        'is_device_last_login' => $faker->numberBetween(0, 1),
        'login_status' => $faker->numberBetween(1, 10000),
    ];
});
