<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayContractKost;

$factory->define(
    MamipayContractKost::class,
    function (Faker $faker) {
        return [
            'rent_type' => collect(['month', '3_month', '6_month'])->random(1),
            'room_number' => $faker->numberBetween(1, 100),
        ];
    }
);
