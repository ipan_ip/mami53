<?php

use App\Entities\Mamipay\Payout\PayoutTransaction;
use App\Entities\Mamipay\Payout\PayoutTransactionMode;
use App\Entities\Mamipay\TransferStatus;
use Faker\Generator as Faker;

$factory->define(PayoutTransaction::class, function (Faker $faker) {
    return [
        'contract_invoice_id' => $faker->numberBetween(1, 10000000),
        'mode' => $faker->randomElement(PayoutTransactionMode::getValues()),
        'mode_value' => '30.00',
        'kost_price' => '1700000.00',
        'total_payout_amount' => '510000.00',
        'charge_percentage' => 5.0,
        'charge_amount' => '25500.00',
        'additional_amount' => '0.00',
        'transfer_amount' => '484500.00',
        'transfer_status' => $faker->randomElement(TransferStatus::getValues()),
        'processed_at' => $faker->dateTime,
        'transferred_at' => $faker->dateTime,
        'failed_at' => null,
        'transfer_reference' => 'FLIPSPLT-1245',
        'transferred_by' => 4
    ];
});
