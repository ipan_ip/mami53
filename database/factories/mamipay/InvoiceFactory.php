<?php

use App\Entities\Mamipay\Invoice;
use Faker\Generator as Faker;
use App\Entities\Mamipay\Enums\InvoiceStatus;
use App\Entities\Property\PropertyContractOrder;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'invoice_number' => 'INV/31250698/2019/03/0001',
        'status' => InvoiceStatus::UNPAID,
    ];
});

$factory->afterMakingState(Invoice::class, 'withPropertyContractOrder', function ($invoice, $faker) {
    $order = factory(PropertyContractOrder::class)->create(['amount' => 10000]);
    $invoice->order_type = PropertyContractOrder::ORDER_TYPE;
    $invoice->order_id = $order->id;
    $invoice->amount = $order->amount;
});
