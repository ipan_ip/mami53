<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayAdditionalCost;


$factory->define(MamipayAdditionalCost::class, function (Faker $faker) {
    return [
        "cost_title" => "Makan",
        "cost_value" => 6500.0,
        "cost_type" => "other",
        "sort_order" => 0,
        "created_at" => "2019-02-25 08:48:50",
        "updated_at" => "2019-02-25 08:48:50",
        "deleted_at" => null,   
    ];
});
