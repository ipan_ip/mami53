<?php

use App\Entities\Mamipay\MamipayTenant;
use Faker\Generator as Faker;

$factory->define(MamipayTenant::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'phone_number' => '082111111111',
        'email' => 'mami@mamipay.com'
    ];
});
