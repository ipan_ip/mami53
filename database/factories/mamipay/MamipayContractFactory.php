<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayContract;

$factory->define(
    MamipayContract::class,
    function (Faker $faker) {
        return [
            'owner_id' => $faker->numberBetween(1, 100),
            'tenant_id' => $faker->numberBetween(1, 100),
            'type' => $faker->randomElement(['kost', 'apartement']),
            'status' => $faker->randomElement(['terminated', 'active']),
            'duration' => $faker->numberBetween(1, 5),
            'duration_unit' => $faker->randomElement(['month', 'year']),
            'start_date' => $faker->dateTime->format("Y-m-d"),
            'end_date' => $faker->dateTime->format("Y-m-d"),
        ];
    }
);
