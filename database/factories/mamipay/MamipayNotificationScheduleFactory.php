<?php

use Faker\Generator as Faker;
use App\Entities\Mamipay\MamipayNotificationSchedule;

$factory->define(MamipayNotificationSchedule::class, function (Faker $faker) {
    return [
        'contract_invoice_id' => $faker->numberBetween(1, 10000),
        'scheduled_at' => $faker->date(),
        'status' => $faker->word,
        'sent_at' => $faker->date(),
        'type_notif' => $faker->word
    ];
});
