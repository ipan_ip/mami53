<?php

$factory->define(
    App\Entities\Owner\TopOwnerHistory::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 1000),
        ];
    }
);