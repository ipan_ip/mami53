<?php
use Faker\Generator as Faker;
use App\Entities\Log\ThanosBookingRequestLog;

$factory->define(
    ThanosBookingRequestLog::class,
    function (Faker $faker) {
        return [
            'booking_request' => [
                'id'            => $faker->numberBetween(1, 9999999),
                'status_from'   => 'waiting',
                'status_to'     => 'reject',
                'operation_result' => [
                    'success'   => true,
                    'message'   => 'OK',
                    'failed_reason' => '',
                ],
            ]
        ];
    }
);