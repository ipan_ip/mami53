<?php
use Faker\Generator as Faker;
use App\Entities\Log\RoomAllotmentContractLog;

$factory->define(
    RoomAllotmentContractLog::class,
    function (Faker $faker) {
        return [
            'designer_room_id' => $faker->numberBetween(1, 9999999),
            'occupied_before' => 0,
            'occupied_after' => 1
        ];
    }
);