<?php
use Faker\Generator as Faker;
use App\Entities\Log\DuplicatePhoneNumberFixerLog;

$factory->define(
    DuplicatePhoneNumberFixerLog::class,
    function (Faker $faker) {
        return [
            '_id'           => mt_rand(111111, 999999),
            'user_id'       => mt_rand(111111, 999999),
            'phone_number'  => '08'.mt_rand(111111, 999999),
            'mode'          => 'rollback',
            'status'        => true
        ];
    }
);