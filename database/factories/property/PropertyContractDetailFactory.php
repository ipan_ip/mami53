<?php

use App\Entities\Property\PropertyContractDetail;
use Faker\Generator as Faker;

$factory->define(
    PropertyContractDetail::class,
    function (Faker $faker) {
        return [
            'property_contract_id' => $faker->numberBetween(1, 10000),
            'property_id' => $faker->numberBetween(1, 10000),
        ];
    }
);
