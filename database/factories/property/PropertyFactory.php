<?php

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use Faker\Generator as Faker;

$factory->define(
    Property::class,
    function (Faker $faker) {
        return [
            'name' => sprintf('Property %s', $faker->name),
            'has_multiple_type' => $faker->boolean()
        ];
    }
);

$factory->afterCreatingState(Property::class, 'withActiveContract', function ($property, $faker) {
    $level = factory(PropertyLevel::class)->create();
    $contract = factory(PropertyContract::class)->create(['property_level_id' => $level->id, 'status' => 'active', 'assigned_by' => 0]);

    factory(PropertyContractDetail::class)->create([
        'property_id' => $property->id,
        'property_contract_id' => $contract->id
    ]);
});

$factory->afterCreatingState(Property::class, 'withInactiveContract', function ($property, $faker) {
    $level = factory(PropertyLevel::class)->create();
    $contract = factory(PropertyContract::class)->create(['property_level_id' => $level->id, 'status' => 'inactive', 'assigned_by' => 0]);

    factory(PropertyContractDetail::class)->create([
        'property_id' => $property->id,
        'property_contract_id' => $contract->id
    ]);
});
