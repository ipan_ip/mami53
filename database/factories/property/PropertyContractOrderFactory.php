<?php

use App\Entities\Property\PropertyContractOrder;
use Faker\Generator as Faker;

$factory->define(
    PropertyContractOrder::class,
    function (Faker $faker) {
        return [
            'name' => sprintf('Pembayaran Kontrak %s', $faker->name),
        ];
    }
);
