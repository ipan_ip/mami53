<?php

use App\Entities\Dbet\DbetLink;

$factory->define(
    DbetLink::class,
    function (Faker\Generator $faker) {
        return [
            'is_required_identity' => $faker->numberBetween(0, 1),
            'is_required_due_date' => $faker->numberBetween(0, 1),
            'due_date' => $faker->numberBetween(1, 31)
        ];
    }
);
