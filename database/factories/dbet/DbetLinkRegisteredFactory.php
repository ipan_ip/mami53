<?php

use App\Entities\Dbet\DbetLinkRegistered;

$factory->define(
    DbetLinkRegistered::class,
    function (Faker\Generator $faker) {
        return [
            'fullname' => $faker->name,
            'gender' => $faker->randomElement(['male', 'female']),
            'phone' => ('081' . $faker->randomNumber(8)),
            'jobs' => 'Karyawan',
            'work_place' => 'PT Mama Teknologi',
            'due_date' => $faker->numberBetween(1, 31),
            'rent_count_type' => '',
            'price' => $faker->numberBetween(300000, 500000),
            'status' => 'pending'
        ];
    }
);
