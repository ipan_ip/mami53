<?php

use Faker\Generator as Faker;
use App\Entities\Device\UserDevice;

$factory->define(UserDevice::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'model' => 'Xiaomi Mi 4i',
        'email' => 'anggaprmdi@gmail.com',
        'user_id' => 4,
        'identifier' => '7492a8afaff57444',
        'uuid' => '7706b113-560f-4601-b9b7-ec11e4efd275867634020801021',
        'platform' => 'android',
        'platform_version_code_old' => 21,
        'platform_version_code' => 21,
        'device_token' => '4db8f36b06246fc2fae17fa5c8e81613788061e54c316ee94041703238dc9fe5',
        'app_notif_token' => 'sd98asjd9a8sd',
        'app_notif_all' => true,
        'app_notif_status_changed' => false,
        'app_notif_by_reference' => true,
        'app_version_code' => 2000,
        'app_install_count' => 3,
        'app_update_count' => 0,
        'enable_app_popup' => true,
        'device_last_login' => true,
        'login_status' => 'not_login',
        'last_rate' => now(),
        'birth' => 1999,
        'gender' => 'male',
        'last_rent_type' => null,
        'last_alarm_id' => null,
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
