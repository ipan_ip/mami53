<?php

use App\Entities\Forum\ForumCategory;
use Faker\Generator as Faker;

$factory->define(
    ForumCategory::class,
    function (Faker $faker) {
        return [
            'name' => $faker->cityPrefix(),
            'slug' => $faker->cityPrefix(),
            'description' => '',
            'ordinal' => mt_rand(0, 1000),
            'question_count' => mt_rand(0, 1000),
            'answer_count' => mt_rand(0, 1000),
        ];
    }
);
