<?php

use App\Entities\Forum\ForumThread;
use Faker\Generator as Faker;

$factory->define(
    ForumThread::class,
    function (Faker $faker) {
        return [
            'category_id' => mt_rand(0, 10),
            'title' => $faker->streetName(),
            'slug' => $faker->slug(),
            'description' => $faker->sentence(3),
            'vote_up' => mt_rand(0, 100),
            'vote_down' => mt_rand(0, 100),
            'answer_count' => mt_rand(0, 100),
            'is_active' => mt_rand(0, 1),
            'last_answer_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
    }
);
