<?php

use Faker\Generator as Faker;
use App\Entities\Promoted\UserAdView;

$factory->define(UserAdView::class, function (Faker $faker) {
    return [
        'type' => 'click',
        'identifier' => $faker->word,
        'identifier_type' => 'session',
        'designer_id' => $faker->randomDigitNotNull,
        'last_view_time' => now(),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
