<?php

use App\Entities\Promoted\HistoryPromote;
use Carbon\Carbon;

$factory->define(HistoryPromote::class, function () {
    return [
        'for' => 'click',
        'start_view' => 0,
        'end_view' => rand(1, 1000),
        'start_date' => Carbon::now()->subDay(7),
        'end_date' => Carbon::now()->subDay(1),
        'used_view' => 0,
        'view_promote_id' => null,
        'session_id' => null,
        'created_at' => Carbon::now()->subDay(7),
        'updated_at' => Carbon::now()->subDay(7),
        'deleted_at' => null
    ];
});
