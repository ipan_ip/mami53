<?php

use App\Entities\Promoted\Promotion;
use Faker\Generator as Faker;

$factory->define(
    Promotion::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(10000, 99999),
            "title" => $faker->title,
            "content" => $faker->text(100),
            "start_date" => $faker->date('Y-m-d', 'now'),
            "finish_date" => $faker->dateTimeBetween('now', '+30 days')->format('Y-m-d'),
            "is_show" => 'true',
            'verification' => 'true'
        ];
    }
);
