<?php

use App\Entities\Promoted\Promoted;
use Faker\Generator as Faker;

$factory->define(
    Promoted::class,
    function (Faker $faker) {
        return [
            'name' => $faker->title,
            'designer_count' => null,
            'is_published' => rand(1,100),
            'created_at' => now()
        ];
    }
);
