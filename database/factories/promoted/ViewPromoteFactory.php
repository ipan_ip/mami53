<?php

use Faker\Generator as Faker;
use App\Entities\Promoted\ViewPromote;

$factory->define(ViewPromote::class, function (Faker $faker) {
    return [
        'for' => 'click',
        'designer_id' => $faker->randomDigitNotNull,
        'premium_request_id' => $faker->randomDigitNotNull,
        'total' => $faker->randomDigitNotNull,
        'used' => $faker->randomDigitNotNull,
        'history' => $faker->randomDigitNotNull,
        'is_active' => $faker->boolean(1),
        'session_id' => $faker->randomDigitNotNull,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
