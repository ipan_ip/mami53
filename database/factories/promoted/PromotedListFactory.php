<?php

use App\Entities\Promoted\Promoted;
use App\Entities\Promoted\PromotedList;
use App\Entities\Room\Room;
use Faker\Generator as Faker;

$factory->define(
    PromotedList::class,
    function (Faker $faker) {
        return [
            'designer_id' => factory(Room::class)->create(),
            'list_id' => factory(Promoted::class)->create(),
            'type' => null,
            'list_id' => null,
            'ordinal_position' => null,
            'period_end' => null,
            'created_at' => now()
        ];
    }
);
