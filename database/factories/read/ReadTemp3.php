<?php
use Faker\Generator as Faker;
use App\Entities\Read\ReadTemp3;

$factory->define(
    ReadTemp3::class,
    function (Faker $faker) {
        return [
            'id'        => mt_rand(111, 999999),
            'user_id'   => mt_rand(1, 999999),
            'status'    => true,
            'type'      => 'view',
            'count'     => mt_rand(1, 100),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ];
    }
);

