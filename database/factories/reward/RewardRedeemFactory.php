<?php

use App\Entities\Reward\RewardRedeem;
use Faker\Generator;

$factory->define(RewardRedeem::class, function (Generator $faker) {
    return [
        'public_id' => now()->format('ymdHisv') . rand(0, 9),
        'reward_id' => 1,
        'user_id' => 1936666,
        'status' => $faker->randomElement([
            RewardRedeem::STATUS_ONPROCESS,
            RewardRedeem::STATUS_FAILED,
            RewardRedeem::STATUS_SUCCESS
        ]),
        'notes' => $faker->words(5, true),
    ];
});
