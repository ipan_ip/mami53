<?php

use App\Entities\Reward\RewardQuota;
use Faker\Generator;

$factory->define(RewardQuota::class, function (Generator $faker) {
    return [
        'reward_id' => 1,
        'type' => $faker->randomElement((new RewardQuota)->getTypeList()),
        'value' => rand(1, 5),
    ];
});
