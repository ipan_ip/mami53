<?php

use App\Entities\Reward\RewardTargetConfig;
use Faker\Generator;

$factory->define(RewardTargetConfig::class, function (Generator $faker) {
    return [
        'type' => $faker->randomElement(RewardTargetConfig::getTargetTypes()),
        'value' => $faker->word
    ];
});
