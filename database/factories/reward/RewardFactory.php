<?php

use App\Entities\Reward\Reward;
use Faker\Generator;

$factory->define(Reward::class, function (Generator $faker) {
    $startDate = $faker->dateTimeBetween('-2 months', '-2 weeks');
    $endDate = $faker->dateTimeBetween('+2 weeks', '+2 months');

    return [
        'media_id' => 1661113,
        'name' => $faker->words(3, true),
        'description' => $faker->paragraph,
        'start_date' => $startDate,
        'end_date' => $endDate,
        'redeem_value' => $faker->numberBetween(5, 100),
        'redeem_currency' => $faker->randomElement(['rupiah', 'point']),
        'tnc' => $faker->paragraph,
        'howto' => $faker->paragraph,
        'sequence' => rand(0, 5),
        'user_target' => $faker->randomElement(array_keys(Reward::getUserTargets())),
        'is_active' => rand(0, 1),
        'is_published' => rand(0, 1),
        'is_testing' => rand(0, 1),
    ];
});
