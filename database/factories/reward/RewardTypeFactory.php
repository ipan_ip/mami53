<?php

use App\Entities\Reward\RewardType;
use Faker\Generator;

$factory->define(RewardType::class, function (Generator $faker) {
    return [
        'key' => $faker->word,
        'name' => $faker->sentence,
    ];
});
