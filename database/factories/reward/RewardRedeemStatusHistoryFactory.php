<?php

use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use Faker\Generator;

$factory->define(RewardRedeemStatusHistory::class, function (Generator $faker) {
    return [
        'redeem_id' => 0,
        'user_id' => 0,
        'action' => $faker->randomElement([
            RewardRedeem::STATUS_ONPROCESS,
            RewardRedeem::STATUS_FAILED,
            RewardRedeem::STATUS_SUCCESS
        ]),
    ];
});
