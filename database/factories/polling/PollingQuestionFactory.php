<?php

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use Faker\Generator;

$factory->define(PollingQuestion::class, function (Generator $faker) {
    return [
        'polling_id' => function () {
            return factory(Polling::class)->create()->id;
        },
        'question' => $faker->sentence .'?',
        'type' => $faker->randomElement(array_keys(PollingQuestion::getTypesDropdown())),
        'sequence' => rand(0, 5),
        'is_active' => rand(0, 1),
    ];
});
