<?php

use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use Faker\Generator;

$factory->define(PollingQuestionOption::class, function (Generator $faker) {
    return [
        'question_id' => function () {
            return factory(PollingQuestion::class)->create()->id;
        },
        'option' => $faker->sentence,
        'setting' => null,
        'sequence' => rand(0, 5),
    ];
});
