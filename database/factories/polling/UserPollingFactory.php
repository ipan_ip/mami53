<?php

use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Entities\Polling\UserPolling;
use App\User;
use Faker\Generator;

$factory->define(UserPolling::class, function (Generator $faker) {
    $question = factory(PollingQuestion::class)->create([
        'type' => PollingQuestion::TYPE_SELECTIONLIST
    ]);

    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'question_id' => $question->id,
        'option_id' => function () use ($question) {
            return factory(PollingQuestionOption::class)->create([
                'question_id' => $question->id
            ])->id;
        },
        'answer' => $faker->words(2, true),
    ];
});
