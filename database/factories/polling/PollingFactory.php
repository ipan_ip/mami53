<?php

use App\Entities\Polling\Polling;
use Faker\Generator;

$factory->define(Polling::class, function (Generator $faker) {
    return [
        'key' => $faker->words(3, true),
        'descr' => $faker->sentence,
        'is_active' => rand(0, 1),
    ];
});
