<?php

use Carbon\Carbon;

$factory->define(
    App\Entities\Consultant\PotentialOwner::class,
    function (Faker\Generator $faker) {
        return [
            'name' => $faker->name,
            'phone_number' => $faker->phoneNumber,
            'email' => $faker->email,
            'total_property' => $faker->numberBetween(0, 9),
            'total_room' => $faker->numberBetween(0, 500),
            'user_id' => $faker->numberBetween(0, 100000),
            'remark' => $faker->word,
            'date_to_visit' => $faker->date,
            'created_by' => $faker->numberBetween(0, 100000),
            'updated_by' => $faker->numberBetween(0, 100000),
            'bbk_status' => 'non-bbk',
            'followup_status' => 'new'
        ];
    }
);
