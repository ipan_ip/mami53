<?php

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;

$factory->define(
    \App\Entities\Consultant\ActivityManagement\ActivityProgress::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_activity_funnel_id' => $faker->numberBetween(1, 10000),
            'consultant_id' => $faker->numberBetween(1, 10000),
            'current_stage' => $faker->randomNumber(1),
            'progressable_type' => $faker->randomElement([
                ActivityProgress::MORPH_TYPE_CONTRACT,
                ActivityProgress::MORPH_TYPE_DBET,
                ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
                ActivityProgress::MORPH_TYPE_PROPERTY
            ]),
            'progressable_id' => $faker->numberBetween(1, 10000),
        ];
    }
);

$factory->state(ActivityProgress::class, 'withDBET', function ($faker) {
    $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_DBET]);

    return [
        'consultant_activity_funnel_id' => $funnel->id,
        'progressable_type' => ActivityProgress::MORPH_TYPE_DBET,
        'progressable_id' => factory(PotentialTenant::class)->create()->id
    ];
});

$factory->state(ActivityProgress::class, 'withContract', function ($faker) {
    $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_CONTRACT]);
    $tenant = factory(MamipayTenant::class)->create();

    return [
        'consultant_activity_funnel_id' => $funnel->id,
        'progressable_type' => ActivityProgress::MORPH_TYPE_CONTRACT,
        'progressable_id' => factory(MamipayContract::class)->create(['tenant_id' => $tenant->id])->id
    ];
});

$factory->state(ActivityProgress::class, 'withPotentialProperty', function ($faker) {
    $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_PROPERTY]);

    return [
        'consultant_activity_funnel_id' => $funnel->id,
        'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
        'progressable_id' => factory(PotentialProperty::class)->create()->id
    ];
});

$factory->state(ActivityProgress::class, 'withPotentialOwner', function ($faker) {
    $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_POTENTIAL_OWNER]);

    return [
        'consultant_activity_funnel_id' => $funnel->id,
        'progressable_type' => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER,
        'progressable_id' => factory(PotentialOwner::class)->create()->id
    ];
});

$factory->state(ActivityProgress::class, 'withProperty', function ($faker) {
    $funnel = factory(ActivityFunnel::class)->create(['related_table' => ActivityFunnel::TABLE_PROPERTY]);

    return [
        'consultant_activity_funnel_id' => $funnel->id,
        'progressable_type' => ActivityProgress::MORPH_TYPE_PROPERTY,
        'progressable_id' => factory(Room::class)->create()->id
    ];
});

$factory->state(ActivityProgress::class, 'withForm', function ($faker) {
    return ['current_stage' => $faker->numberBetween(1, 9)];
});

$factory->afterCreatingState(ActivityProgress::class, 'withForm', function ($progress, $faker) {
    factory(ActivityForm::class)->create([
        'consultant_activity_funnel_id' => $progress->consultant_activity_funnel_id,
        'stage' => $progress->current_stage
    ]);
});
