<?php

$factory->define(
    \App\Entities\Consultant\ActivityManagement\ActivityForm::class,
    function (Faker\Generator $faker) {
        return [
            'name' => str_random(10),
            'consultant_activity_funnel_id' => $faker->numberBetween(1, 10000),
            'detail' => str_random(10),
            'stage' => $faker->randomNumber(1),
            'form_element' => [
                'test' => 'test'
            ]
        ];
    }
);
