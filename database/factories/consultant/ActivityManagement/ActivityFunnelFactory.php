<?php

$factory->define(
    \App\Entities\Consultant\ActivityManagement\ActivityFunnel::class,
    function (Faker\Generator $faker) {
        return [
            'name' => str_random(10),
            'consultant_role' => $faker->randomElement(['admin', 'supply', 'demand']),
            'related_table' => $faker->randomElement([
                'consultant_tools_potential_tenant',
                'mamipay_contract',
                'designer'
            ]),
            'total_stage' => $faker->numberBetween(1, 9)
        ];
    }
);
