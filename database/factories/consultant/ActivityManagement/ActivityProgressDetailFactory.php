<?php

$factory->define(
    \App\Entities\Consultant\ActivityManagement\ActivityProgressDetail::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_activity_form_id' => $faker->numberBetween(1, 10000),
            'consultant_activity_progress_id' => $faker->numberBetween(1, 10000),
            'value' => $faker->word
        ];
    }
);
