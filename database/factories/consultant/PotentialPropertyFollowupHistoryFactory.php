<?php

use App\Entities\PotentialProperty\PotentialPropertyFollowupHistory;

$factory->define(
    PotentialPropertyFollowupHistory::class,
    function (Faker\Generator $faker) {
        return [
            'potential_property_id' => $faker->numberBetween(1, 100000),
            'designer_id' => $faker->numberBetween(1, 100000),
            'new_at' => now()->toDateTimeString(),
        ];
    }
);
