<?php

use App\Entities\PotentialOwner\PotentialOwnerFollowupHistory;

$factory->define(
    PotentialOwnerFollowupHistory::class,
    function (Faker\Generator $faker) {
        return [
            'potential_owner_id' => $faker->numberBetween(1, 100000),
            'user_id' => $faker->numberBetween(1, 100000),
            'new_at' => now()->toDateTimeString(),
        ];
    }
);