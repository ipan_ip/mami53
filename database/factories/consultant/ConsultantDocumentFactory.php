<?php

use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use Faker\Generator;

$factory->define(
    ConsultantDocument::class,
    function (Generator $faker) {
        return [
            'documentable_type' => 'sales_motion_progress',
            'documentable_id' => factory(SalesMotionProgress::class)->create()->id,
            'file_name' => ($faker->word . '.' . $faker->randomElement(['pdf', 'docx', 'doc'])),
            'file_path' => config('consultant.document.folder_location')
        ];
    }
);
