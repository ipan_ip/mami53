<?php

use App\Entities\Consultant\ConsultantRole;

$factory->define(
    ConsultantRole::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_id' => $faker->numberBetween(0, 100000),
            'role' => $faker->randomElement([
                ConsultantRole::ADMIN,
                ConsultantRole::DEMAND,
                ConsultantRole::SUPPLY
            ])
        ];
    }
);
