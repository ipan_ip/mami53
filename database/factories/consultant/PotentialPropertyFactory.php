<?php

use App\Entities\Consultant\ConsultantRole;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Media\Media;

$factory->define(
    PotentialProperty::class,
    function (Faker\Generator $faker) {
        return [
            'name' => ('Kost ' . $faker->address . ' ' . $faker->buildingNumber),
            'media_id' => factory(Media::class)->create()->id,
            'address' => $faker->address,
            'area_city' => $faker->city,
            'province' => $faker->city,
            'total_room' => $faker->numberBetween(1, 20),
            'offered_product' => implode(',', ['gp1', 'gp2', 'gp3']),
            'consultant_potential_owner_id' => $faker->numberBetween(1, 100000),
            'consultant_id' => $faker->numberBetween(1, 100000),
            'remark' => $faker->word,
            'bbk_status' => $faker->randomElement([PotentialProperty::BBK_STATUS_BBK, PotentialProperty::BBK_STATUS_NON_BBK, PotentialProperty::BBK_STATUS_WAITING]),
            'followup_status' => $faker->randomElement([PotentialProperty::FOLLOWUP_STATUS_APPROVED, PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS, PotentialProperty::FOLLOWUP_STATUS_NEW, PotentialProperty::FOLLOWUP_STATUS_PAID, PotentialProperty::FOLLOWUP_STATUS_REJECTED]),
            'created_by' => $faker->numberBetween(1, 100000),
            'updated_by' => $faker->numberBetween(1, 100000),
            'is_priority' => $faker->boolean
        ];
    }
);
