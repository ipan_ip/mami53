<?php

$factory->define(
    App\Entities\Consultant\ConsultantRoom::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_id' => $faker->numberBetween(1, 10000),
            'designer_id' => $faker->numberBetween(1, 10000)
        ];
    }
);
