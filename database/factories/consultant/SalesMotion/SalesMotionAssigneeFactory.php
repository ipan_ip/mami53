<?php

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Room\Room;

$factory->define(
    SalesMotionAssignee::class,
    function (Faker\Generator $faker) {
        return [
            'consultant_sales_motion_id' => $faker->numberBetween(1, 10000),
            'consultant_id' => $faker->numberBetween(1, 10000),
        ];
    }
);
