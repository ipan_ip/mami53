<?php

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Media\Media;
use Faker\Generator;

$factory->define(
    SalesMotionProgress::class,
    function (Generator $faker) {
        return [
            'consultant_id' => $faker->numberBetween(1, 10000),
            'consultant_sales_motion_id' => $faker->numberBetween(1, 10000),
            'media_id' => $faker->numberBetween(1, 10000),
            'progressable_type' => $faker->word,
            'progressable_id' => $faker->numberBetween(1, 10000),
            'remark' => $faker->word,
            'status' => $faker->randomElement([
                'deal',
                'interested',
                'not_interested'
            ])
        ];
    }
);
