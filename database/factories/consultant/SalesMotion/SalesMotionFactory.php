<?php

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Media\Media;
use App\User;

$factory->define(
    SalesMotion::class,
    function (Faker\Generator $faker) {
        $user = factory(User::class)->create();

        return [
            'name' => $faker->name,
            'type' => $faker->randomElement(
                [
                    'promotion',
                    'campaign',
                    'product',
                    'feature'
                ]
            ),
            'created_by_division' => $faker->randomElement(
                [
                    'marketing',
                    'commercial'
                ]
            ),
            'objective' => $faker->word,
            'terms_and_condition' => $faker->word,
            'benefit' => $faker->word,
            'requirement' => $faker->word,
            'is_active' => $faker->boolean,
            'start_date' => $faker->date(),
            'end_date' => $faker->date(),
            'objective' => $faker->sentence(6),
            'terms_and_condition' => $faker->sentence(6),
            'benefit' => $faker->sentence(6),
            'requirement' => $faker->sentence(6),
            'media_id' => factory(Media::class)->create()->id,
            'url' => $faker->url,
            'max_participant' => $faker->numberBetween(1, 10000) . ' ' . $faker->word,
            'updated_by' => $user->id,
            'created_by' => $user->id
        ];
    }
);
