<?php

use App\Entities\Room\Element\Card;
use Faker\Generator as Faker;

$factory->define(
    Card::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(1, 1000),
            'photo_id' => $faker->numberBetween(1, 1000),
            'description' => 'This is a testing photo',
            'type' => 'image',
            'sub_type' => 'image',
            'ordering' => $faker->numberBetween(0, 10)
        ];
    }
);
