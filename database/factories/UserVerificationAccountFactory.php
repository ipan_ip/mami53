<?php

use App\Entities\User\UserVerificationAccount;

$factory->define(
    UserVerificationAccount::class,
    function (Faker\Generator $faker) {
        return [
            'user_id' => $faker->numberBetween(1, 100),
            'is_verify_phone_number' => $faker->numberBetween(0,1),
            'is_verify_email' => $faker->numberBetween(0,1),
            'is_verify_facebook' => $faker->numberBetween(0,1),
            'identity_card' => $faker->randomElement(['verified', 'rejected', 'waiting', 'canceled']),
            'is_verify_owner' => $faker->numberBetween(0,1),
        ];
    }
);