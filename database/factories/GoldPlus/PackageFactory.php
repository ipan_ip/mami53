<?php

use App\Entities\GoldPlus\Package;

$factory->define(
    Package::class,
    function () {
        return [
            'code' => 'gp1',
            'name' => 'Goldplus 1',
            'description' => 'Paket Dasar GoldPlus 1',
            'price' => 1145000,
            'periodicity' => 'monthly',
            'unit_type' => 'all',
            'active' => true
        ];
    }
);

$factory->state(
    Package::class,
    'gp1',
    [
        'code' => Package::CODE_GP1,
    ]
);

$factory->state(
    Package::class,
    'gp2',
    [
        'code' => Package::CODE_GP2,
    ]
);

$factory->state(
    Package::class,
    'gp3',
    [
        'code' => Package::CODE_GP3,
    ]
);
