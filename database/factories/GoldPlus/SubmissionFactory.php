<?php

use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\Entities\GoldPlus\Submission;
use Faker\Generator as Faker;

$factory->define(
    Submission::class,
    function (Faker $faker) {
        $listingIds = [];
        for ($i = 0; $i < $faker->numberBetween(1, 6); $i++) {
            $listingIds[] = $faker->randomNumber();
        }
        return [
            'user_id' => $faker->randomNumber(),
            'package_id' => $faker->randomNumber(),
            'listing_ids' => implode(Submission::DELIMITER_LISTING_IDS, $listingIds),
            'status' => SubmissionStatus::NEW,
        ];
    }
);

$factory->state(
    Submission::class,
    'allProcessed',
    [
        'status' => SubmissionStatus::DONE,
    ]
);
