<?php

use App\Entities\Area\AreaGeolocationMapping;
use Faker\Generator as Faker;

$factory->define(
    AreaGeolocationMapping::class,
    function (Faker $faker) {
        return [
            'area_geolocation_id' => $faker->numberBetween(0, 100),
            'search_input_keyword_suggestion_id' => $faker->numberBetween(0, 100)
        ];
    }
);
