<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Area\Subdistrict::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 10000),
        'area_city_id' => $faker->numberBetween(1, 1000),
        'name' => $faker->city
    ];
});
