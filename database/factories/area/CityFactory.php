<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Area\City::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 1000),
        'area_province_id' => $faker->numberBetween(1, 100),
        'name' => $faker->city
    ];
});
 