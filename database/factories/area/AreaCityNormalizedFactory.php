<?php

use App\Entities\Area\AreaCityNormalized;
use Faker\Generator as Faker;

$factory->define(
    AreaCityNormalized::class,
    function (Faker $faker) {
        return [
            'designer_id' => $faker->numberBetween(10000, 99999),
            'sanitized_address' => $faker->address,
            'match_keyword' => $faker->words,
            'match_score' => $faker->randomFloat(6),
            'normalized_subdistrict' => $faker->city,
            'normalized_city' => $faker->citySuffix,
            'normalized_province' => $faker->state
        ];
    }
);
