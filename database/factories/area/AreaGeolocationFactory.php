<?php

use App\Entities\Area\AreaGeolocation;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;

$factory->define(
    AreaGeolocation::class,
    function (Faker $faker) {
        return [
            'province' => $faker->state,
            'city' => '',
            'subdistrict' => '',
            'village' => '',
            'geolocation' => new Polygon([new LineString([
                new Point(40.74894149554006, -73.98615270853043),
                new Point(40.74848633046773, -73.98648262023926),
                new Point(40.747925497790725, -73.9851602911949),
                new Point(40.74837050671544, -73.98482501506805),
                new Point(40.74894149554006, -73.98615270853043)
            ])])
        ];
    }
);
