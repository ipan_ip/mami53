<?php

use Faker\Generator as Faker;
use App\Entities\Area\Province;

$factory->define(
    Province::class,
    function (Faker $faker) {
        return [
            'id' => $faker->unique()->numberBetween(1, 99999),
            'name' => $faker->words(2,true),
        ];
    }
);
