<?php

use App\Entities\Area\AreaBigMapper;
use Faker\Generator as Faker;

$factory->define(
    AreaBigMapper::class,
    function (Faker $faker) {
        return [
            'area_city' => $faker->city,
            'area_big' => $faker->citySuffix,
            'province' => $faker->state
        ];
    }
);
