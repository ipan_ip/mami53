<?php

use App\Entities\Voucher\VoucherProgram;
use Faker\Generator;

$factory->define(VoucherProgram::class, function (Generator $faker) {
    return [
        'name' => $faker->word,
        'email_subject' => $faker->sentence,
        'email_template' => $faker->paragraph,
        'sms_template' => $faker->paragraph,
        'status' => $faker->randomElement([
            VoucherProgram::STATUS_CREATED,
            VoucherProgram::STATUS_RUNNING,
            VoucherProgram::STATUS_PAUSED,
            VoucherProgram::STATUS_FINISHED,
            VoucherProgram::STATUS_STOPPED,
        ]),
    ];
});
