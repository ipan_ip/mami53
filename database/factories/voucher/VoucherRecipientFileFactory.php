<?php

use App\Entities\Voucher\VoucherRecipientFile;
use Faker\Generator;

$factory->define(VoucherRecipientFile::class, function (Generator $faker) {
    return [
        'voucher_program_id' => 1,
        'file_path' => ('uploads/data/voucher_recipient' . $faker->date()),
        'file_name' => $faker->md5.'.'.$faker->fileExtension,
        'rows_read' => $faker->numberBetween(1, 1000),
        'status' => $faker->randomElement([
            VoucherRecipientFile::STATUS_CREATED,
            VoucherRecipientFile::STATUS_RUNNING,
            VoucherRecipientFile::STATUS_FINISHED,
            VoucherRecipientFile::STATUS_FAILED,
        ]),
        'options' => '',
    ];
});
