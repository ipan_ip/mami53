<?php

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Entities\Mamipay\MamipayVoucherPrefix;
use App\Entities\Mamipay\MamipayVoucherPublicCampaign;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Entities\Mamipay\MamipayVoucherUsage;
use Faker\Generator as Faker;

$factory->define(MamipayVoucherPublicCampaign::class, function (Faker $faker) {
    return [
        'title' => $faker->words(3, true),
        'media_id' => 0,
        'tnc' => $faker->paragraphs(3, true),
        'is_published' => rand(0, 1),
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});

$factory->define(MamipayVoucherPrefix::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'voucher_limit' => rand(2, 4),
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});

$factory->define(MamipayVoucher::class, function (Faker $faker) {
    $startDate = now()->subHours(1)->format('Y-m-d H:i:s');
    $endDate = now()->addDays(2)->format('Y-m-d H:i:s');

    return [
        'voucher_name' => $faker->words(3, true),
        'prefix_id' => 0,
        'voucher_code' => $faker->word(),
        'public_campaign_id' => 0,
        'campaign_team' => $faker->randomElement(['business', 'marketing', 'other', 'hr']),
        'for_booking' => 1,
        'for_recurring' => 1,
        'for_pelunasan' => 1,
        'voucher_type' => $faker->randomElement(['amount', 'percentage']),
        'voucher_amount' => $faker->randomElement([50000, 100000, 300000]),
        'voucher_max_amount' => $faker->randomElement([100000, 300000, 500000]),
        'voucher_min_amount' => $faker->randomElement([10000, 30000, 40000]),
        'limit' => rand(1, 5),
        'user_limit' => rand(0, 3),
        'min_contract_duration' => $faker->randomElement(['week', 'month', '3_month', '6_month', 'year']),
        'start_date' => $startDate,
        'end_date' => $endDate,
        'is_subsidized' => rand(0, 1),
        'is_active' => rand(0, 1),
        'is_testing' => rand(0, 1),
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});

$factory->define(MamipayVoucherTargetIdentifier::class, function (Faker $faker) {
    return [
        'voucher_id' => 0,
        'type' => 'kost_type',
        'identifier' => 'bbk',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});

$factory->define(MamipayVoucherExcludeIdentifier::class, function (Faker $faker) {
    return [
        'voucher_id' => 0,
        'type' => 'kost_type',
        'identifier' => 'bbk',
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});

$factory->define(MamipayVoucherUsage::class, function (Faker $faker) {
    return [
        'source' => 'invoice',
        'source_id' => 0,
        'voucher_id' => 0,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null
    ];
});
