<?php

use App\Entities\Voucher\VoucherRecipient;
use Faker\Generator;

$factory->define(VoucherRecipient::class, function (Generator $faker) {
    return [
        'voucher_program_id' => 1,
        'user_id' => 1,
        'voucher_code' => ('MAMI' . $faker->randomNumber(8)),
        'email' => $faker->email,
        'phone_number' => ('08' . $faker->randomNumber(8)),
        'status' => $faker->randomElement([
            VoucherRecipient::STATUS_CREATED,
            VoucherRecipient::STATUS_IMPORTED,
            VoucherRecipient::STATUS_SENT,
            VoucherRecipient::STATUS_FAILED,
        ]),
    ];
});
