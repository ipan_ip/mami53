<?php

use Carbon\Carbon;
use Illuminate\Support\Str;
use Laravel\Passport\Token;

$factory->define(
    Token::class,
    function (Faker\Generator $faker) {
        return [
            'id' => Str::random(16),
            'user_id' => $faker->numberBetween(1, 100000),
            'client_id' => $faker->numberBetween(1, 5),
            'name' => $faker->sentence,
            'scopes' => '',
            'revoked' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'expires_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }
);