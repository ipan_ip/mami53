<?php

use Faker\Generator as Faker;
use App\Entities\Owner\Activity;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10000),
        'logged_in_at' => $faker->dateTimeBetween('-1 weeks', 'now'),
        'updated_property_at' => $faker->dateTimeBetween('-1 weeks', 'now')
    ];
});
