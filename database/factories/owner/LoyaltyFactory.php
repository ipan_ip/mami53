<?php

use Faker\Generator as Faker;
use App\Entities\Owner\Loyalty;

$factory->define(Loyalty::class, function (Faker $faker) {
    return [
        'owner_phone_number' => ('08' . $faker->randomNumber(8)),
        'booking_activation_date' => now(),
        'importer_user_id' => $faker->randomNumber(),
        'total_listed_room' => $faker->randomNumber(3),
        'total_booking_response' => $faker->randomNumber(3),
        'total_booking_transaction' => $faker->randomNumber(3),
        'total_review' => $faker->randomNumber(3),
        'loyalty_booking_response' => $faker->randomNumber(3),
        'loyalty_booking_transaction' => $faker->randomNumber(3),
        'loyalty_review' => $faker->randomNumber(3),
        'total' => $faker->randomNumber(3),
    ];
});
