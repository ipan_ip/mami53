<?php
use Faker\Generator as Faker;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;

$factory->define(
    OwnerGoldplusStatistic::class,
    function (Faker $faker) {
        return [
            '_id' => sha1(\uniqid('', true)),
        ];
    }
);
