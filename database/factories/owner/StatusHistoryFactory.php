<?php

use App\Entities\Owner\EditStatus;
use App\Entities\Owner\StatusHistory;
use App\Entities\Room\RoomOwner;
use App\User;
use Faker\Generator as Faker;

$factory->define(StatusHistory::class, function (Faker $faker) {
    $owner = factory(RoomOwner::class)->create(['status' => $faker->randomElement(EditStatus::getValues())]);

    return [
        'designer_owner_id' => $owner,
        'old_status' => $faker->randomElement(EditStatus::getValues()),
        'new_status' => $owner->status,
        'created_by' => factory(User::class)->create()->id
    ];
});
