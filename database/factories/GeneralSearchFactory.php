<?php

use App\Entities\Search\GeneralSearch;
use Faker\Generator as Faker;

$factory->define(
   GeneralSearch::class,
    function (Faker $faker) {
        return [
            'slug' => $faker->slug,
            'keywords' => $faker->name,
            'gender' =>  $faker->randomElement([0, 1, 2]),
            'rent_type' => $faker->randomElement([0, 1, 2, 3]),
            'price_min' => $faker->numberBetween(1, 10000),
            'price_max' => $faker->numberBetween(10000, 10000),
            'longitude' => $faker->longitude,
            'latitude' => $faker->latitude
        ];
    }
);
