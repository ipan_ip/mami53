<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantSalesMotionAssigneeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_sales_motion_assignee', function (Blueprint $table) {
            $table->unsignedInteger('consultant_sales_motion_id');
            $table->unsignedInteger('consultant_id');

            $table->primary(['consultant_sales_motion_id', 'consultant_id'], 'sales_motion_assignee_primary_key');
            $table->index('consultant_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_sales_motion_assignee');
    }
}
