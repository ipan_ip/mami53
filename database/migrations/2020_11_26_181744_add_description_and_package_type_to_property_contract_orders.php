<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionAndPackageTypeToPropertyContractOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->text('description')->after('property_contracts_id')->nullable();
            $table->string('package_type', 6)->after('property_contracts_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('package_type');
        });
    }
}
