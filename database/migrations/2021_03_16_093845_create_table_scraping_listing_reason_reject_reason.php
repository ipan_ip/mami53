<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableScrapingListingReasonRejectReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scraping_listing_reason_reject_reason', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('scraping_listing_reason_id')->nullable(false);
            $table->unsignedInteger('reject_reason_id')->nullable(false);
            $table->boolean('is_fixed');
            $table->string('content_other', 200)->nullable();

            $table->index('scraping_listing_reason_id', 'scraping_listing_reason_id_index');
            $table->index('reject_reason_id', 'reject_reason_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scraping_listing_reason_reject_reason');
    }
}
