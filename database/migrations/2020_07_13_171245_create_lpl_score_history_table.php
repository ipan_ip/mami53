<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLplScoreHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_log')
            ->create(
                'lpl_criteria_history',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('admin_id')
                        ->comment('To store ID of the Admin who triggered action');
                    $table->unsignedInteger('lpl_criteria_id');
                    $table->integer('original_score')
                        ->default(0);
                    $table->integer('modified_score')
                        ->default(0);
                    $table->timestamps();

                    $table->index('admin_id');
                    $table->index('lpl_criteria_id');
                }
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_log')
            ->dropIfExists('lpl_criteria_history');
    }
}
