<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToMamipayManualPayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * DB CHANGE APPROVAL LINK : https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/440041561/2020-05-05+-+Add+New+Column+to+mamipay+manual+payout
     */
    public function up()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->enum('type', ['disbursement', 'refund', 'refund_outside'])
                ->after('status')
                ->index('manual_payout_type_index')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
