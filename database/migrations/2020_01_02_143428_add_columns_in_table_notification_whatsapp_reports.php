<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInTableNotificationWhatsappReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_whatsapp_reports', function (Blueprint $table) {
        	$table->boolean('refreshable')->default(true)->after('status_description');
            $table->tinyInteger('refresh_tryout')->default(0)->after('refreshable');
			$table->timestamp('refresh_time')->after('refresh_tryout');
            $table->text('api_response')->nullable()->after('refresh_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_whatsapp_reports', function (Blueprint $table) {
			$table->dropColumn('refreshable');
            $table->dropColumn('refresh_tryout');
            $table->dropColumn('refresh_time');
            $table->dropColumn('api_response');
        });
    }
}
