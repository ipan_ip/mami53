<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatConditionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_log')->create('chat_condition_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chat_condition_id');
            $table->string('action', 100);
            $table->unsignedInteger('triggered_by');
            $table->timestamps();

            $table->index('chat_condition_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_condition_history');
    }
}
