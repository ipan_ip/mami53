<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConsultantRoleEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_role', function (Blueprint $table) {
            DB::statement("ALTER TABLE consultant_role MODIFY COLUMN role ENUM('admin','supply','demand','admin_chat','admin_bse');");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_role', function (Blueprint $table) {
            DB::statement("ALTER TABLE consultant_role MODIFY COLUMN role ENUM('admin','supply','demand','admin_chat');");
        });
    }
}
