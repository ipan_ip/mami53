<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantPotentialOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_potential_owner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone_number')->unique();
            $table->string('email')->nullable()->unique();
            $table->tinyInteger('total_property')->default(0);
            $table->integer('total_room')->default(0);
            $table->unsignedInteger('user_id')->nullable()->unique();
            $table->string('remark')->default('-');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by');
            $table->index('created_by');
            $table->unsignedInteger('updated_by')->nullable();
            $table->index('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_potential_owner');
    }
}
