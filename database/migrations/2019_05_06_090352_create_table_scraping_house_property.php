<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableScrapingHouseProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scraping_house_property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 10)->nullable();
            $table->string('name', 225);
            $table->string('address', 225)->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('area_subdistrict', 70)->nullable();
            $table->string('area_city', 70)->nullable();
            $table->string('area_big', 70)->nullable();
            $table->integer('price_daily')->nullable();
            $table->integer('price_weekly')->nullable();
            $table->integer('price_monthly')->nullable();
            $table->integer('price_yearly')->nullable();
            $table->integer('bed_room')->nullable();
            $table->string('size', 10)->nullable();
            $table->integer('guest_max')->nullable();
            $table->integer('bed_total')->nullable();
            $table->integer('bath_total')->nullable();
            $table->string('parking', 15)->nullable();
            $table->text('other_cost_information')->nullable();
            $table->text('other_information')->nullable();
            $table->text('description')->nullable();
            $table->string('owner_name', 100)->nullable();
            $table->string('owner_phone', 15)->nullable();
            $table->string('manager_name', 100)->nullable();
            $table->string('manager_phone', 15)->nullable();
            $table->string('input_from', 20)->nullable();
            $table->string('agent_name', 20)->nullable();
            $table->string('cover')->nullable();
            $table->string('status', 10)->nullable();
            $table->string('web_url', 200)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scraping_house_property');
    }
}
