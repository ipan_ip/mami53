<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVacancySearchHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('vacancy_search_history', function (Blueprint $table) {
            $table->renameColumn('filters', 'filter');
            $table->renameColumn('titikTengah', 'center_point');
            $table->renameColumn('namaKota', 'city_name');
        });

        Schema::rename('vacancy_search_history', 'log_vacancy_search_history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename('log_vacancy_search_history', 'vacancy_search_history');

        Schema::table('vacancy_search_history', function (Blueprint $table) {
            $table->renameColumn('filter', 'filters');
            $table->renameColumn('center_point', 'titikTengah');
            $table->renameColumn('city_name', 'namaKota');
        });
    }
}
