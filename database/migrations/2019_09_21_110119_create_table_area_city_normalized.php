<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAreaCityNormalized extends Migration
{    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_log')->create('area_city_normalized', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->text('sanitized_address')->nullable();
            $table->string('match_keyword')->nullable();
            $table->float('match_score', 8, 6)->nullable();
            $table->string('normalized_subdistrict')->nullable();
            $table->string('normalized_city')->nullable();
            $table->string('original_city')->nullable()->comment('area_city');
            $table->string('normalized_province')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_log')->dropIfExists('area_city_normalized');
    }
}
