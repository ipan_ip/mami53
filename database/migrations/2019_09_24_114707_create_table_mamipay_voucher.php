<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMamipayVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_voucher', function (Blueprint $table) {
            $table->increments('id');
            $table->text('voucher_name');  
            $table->text('voucher_code'); 
            $table->boolean('for_booking'); 
            $table->boolean('for_recurring');
            $table->text('voucher_type');
            $table->float('voucher_amount', 9, 0);
            $table->float('voucher_max_amount', 9, 0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_voucher');
    }
}
