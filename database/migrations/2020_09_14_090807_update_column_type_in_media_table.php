<?php

use Illuminate\Database\Migrations\Migration;

class UpdateColumnTypeInMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "ALTER TABLE `media` MODIFY COLUMN `type` ENUM ('user_photo', 'designer_photo', 'style_photo', 'round_style_photo', 'review_photo', 'company_photo', 'photobooth', 'flash_sale_photo')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(
            "ALTER TABLE `media` MODIFY COLUMN `type` ENUM ('user_photo', 'designer_photo', 'style_photo', 'round_style_photo', 'review_photo', 'company_photo', 'photobooth')"
        );
    }
}
