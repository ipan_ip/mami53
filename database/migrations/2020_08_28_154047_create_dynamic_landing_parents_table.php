<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicLandingParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_landing_parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 255);
            $table->string('type_kost', 50);
            $table->string('title_tag', 100);
            $table->string('title_header', 100);
            $table->string('subtitle_header', 100)->nullable();
            $table->text('meta_desc')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('faq_question')->nullable();
            $table->text('faq_answer')->nullable();
            $table->string('image_url', 255)->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->index('slug', 'slug');
            $table->index('is_active', 'is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_landing_parents');
    }
}
