<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')
                ->nullable()
                ->comment('foreign key to designer.id');
            $table->string('name')
                ->comment('name of the room');
            $table->string('floor', 50)
                ->nullable()
                ->comment('floor of the room');
            $table->boolean('occupied')
                ->default(false)
                ->comment('flagging status if room is occupied or empty');
            $table->softDeletes();
            $table->timestamps();
            
            $table->index(['designer_id', 'occupied'], 'designer_occupied_status');
            $table->unique(['designer_id', 'name'], 'designer_unique_room_name');
            $table->foreign('designer_id')->references('id')->on('designer')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_room');
    }
}
