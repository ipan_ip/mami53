<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * This class purposed for migration
 * 
 * @author Angga Bayu S<angga@mamiteam.com>
 * @link https://mamikos.atlassian.net/browse/KOS-14730 
 */
class AddIndexToUserDeviceTable extends Migration
{
    /**
     * Run the migrations.
     * Add index for column app_notif_token
     * Based on discussion -> https://devel.wejoin.us:3000/ali/mami53/merge_requests/7421#note_104352 
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_device', function (Blueprint $table) {
            $table->index('app_notif_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_device', function (Blueprint $table) {
            $table->dropIndex(['app_notif_token']);
        });
    }
}
