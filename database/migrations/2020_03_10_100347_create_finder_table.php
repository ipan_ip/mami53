<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'finder',
            function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('city', 100);
                $table->string('gender', 10);
                $table->integer('min_price');
                $table->integer('max_price');
                $table->text('facility');
                $table->text('note')->nullable();
                $table->date('check_in');
                $table->smallInteger('duration');
                $table->string('name', 255);
                $table->string('email', 255);
                $table->string('phone_number', 255);
                $table->boolean('checked')->default(false);
                $table->timestamps();
                $table->softDeletes();

                $table->index('city');
                $table->index('name');
                $table->index('email');
                $table->index('phone_number');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finder');
    }
}
