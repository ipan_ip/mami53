<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDesignerVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_verification', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')->unique()->comment('foreign key to designer.id');
            $table->unsignedInteger('requested_by_user_id')->nullable()->comment('foreign key to user.id, null when running from command');
            $table->boolean('is_ready')->default(false)->comment('readiness status of the kost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_verification');
    }
}
