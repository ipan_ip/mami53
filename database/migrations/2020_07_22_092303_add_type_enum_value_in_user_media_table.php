<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Add new enum options for type column in user_media. Adding 'Kartu Keluarga' and 'Kartu Tanda Mahasiswa" options.
 */
class AddTypeEnumValueInUserMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `user_media` MODIFY COLUMN `type` ENUM ('photo_profile', 'e_ktp', 'passport', 'sim', 'kk', 'ktm')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `user_media` MODIFY COLUMN `type` ENUM ('photo_profile', 'e_ktp', 'passport', 'sim')");
    }
}
