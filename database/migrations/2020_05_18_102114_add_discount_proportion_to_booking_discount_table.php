<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountProportionToBookingDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->enum(
                'discount_source',
                [
                    'mamikos', 
                    'owner', 
                    'mamikos_and_owner'
                ])->after('discount_value');
            
            $table->enum(
                'discount_type_mamikos',
                [
                    'percentage',
                    'nominal'
                ])->default('percentage')->after('discount_source');
            
            $table->integer('discount_value_mamikos')->default(0)->after('discount_type_mamikos');

            $table->enum(
                'discount_type_owner',
                [
                    'percentage',
                    'nominal'
                ])->default('percentage')->after('discount_value_mamikos');

            $table->integer('discount_value_owner')->default(0)->after('discount_type_owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->dropColumn('discount_source');
            $table->dropColumn('discount_type_owner');
            $table->dropColumn('discount_type_mamikos');
            $table->dropColumn('discount_value_owner');
            $table->dropColumn('discount_value_mamikos');
        });
    }
}
