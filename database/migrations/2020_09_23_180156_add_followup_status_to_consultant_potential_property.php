<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFollowupStatusToConsultantPotentialProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->enum('bbk_status', ['non-bbk', 'waiting', 'bbk']);
            $table->enum('followup_status', ['new', 'in-progress', 'paid', 'approved', 'rejected'])
                ->default('new');
            $table->unsignedSmallInteger('potential_gp_total_room');
            $table->text('potential_gp_room_list');
            $table->unsignedInteger('designer_id');

            $table->index('bbk_status', 'bbk_status');
            $table->index('followup_status', 'followup_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->dropIndex('bbk_status');
            $table->dropIndex('followup_status');

            $table->dropColumn('designer_id');
            $table->dropColumn('potential_gp_room_list');
            $table->dropColumn('potential_gp_total_room');
            $table->dropColumn('followup_status');
            $table->dropColumn('bbk_status');
        });
    }
}
