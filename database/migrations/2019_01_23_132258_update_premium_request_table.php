<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePremiumRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_request', function (Blueprint $table) {
            $table->Integer('auto_upgrade')->nullable()->comment("When this premium is done, will auto upgrade in this package");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_request', function (Blueprint $table) {
            $table->dropColumn('auto_upgrade');
        });
    }
}
