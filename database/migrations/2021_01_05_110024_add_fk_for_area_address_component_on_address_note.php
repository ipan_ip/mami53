<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkForAreaAddressComponentOnAddressNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_address_note', function (Blueprint $table) {
            $table->unsignedInteger('area_province_id')->nullable()->after('designer_id');
            $table->unsignedInteger('area_city_id')->nullable()->after('area_province_id');
            $table->unsignedInteger('area_subdistrict_id')->nullable()->after('area_city_id');

            $table->index(['area_province_id'], 'province_id_index');
            $table->index(['area_city_id'], 'city_id_index');
            $table->index(['area_subdistrict_id'], 'subdistrict_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_address_note', function (Blueprint $table) {
            $table->dropIndex('province_id_index');
            $table->dropIndex('city_id_index');
            $table->dropIndex('subdistrict_id_index');

            $table->dropColumn('area_province_id');
            $table->dropColumn('area_city_id');
            $table->dropColumn('area_subdistrict_id');
        });
    }
}
