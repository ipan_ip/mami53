<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomLevelToDesignerRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_room', function (Blueprint $table) {
            $table->unsignedInteger('room_level_id')->after('designer_id');
            $table->boolean('is_charge_by_room')->default(false)->after('occupied');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_room', function (Blueprint $table) {
            $table->dropColumn('room_level_id');
            $table->dropColumn('is_charge_by_room');
        });
    }
}
