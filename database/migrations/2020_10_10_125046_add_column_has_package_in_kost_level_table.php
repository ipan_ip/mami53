<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnHasPackageInKostLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'kost_level',
            function (Blueprint $table) {
                $table->boolean('has_value')->default(false)->after('is_hidden');
                $table->index('has_value');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'kost_level',
            function (Blueprint $table) {
                $table->dropIndex(['has_value']);
                $table->dropColumn('has_value');
            }
        );
    }
}
