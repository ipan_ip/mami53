<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnUserIdInOwnerLoyaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_loyalty', function (Blueprint $table) {
			$table->renameColumn('user_id', 'owner_phone_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_loyalty', function (Blueprint $table) {
			if (Schema::hasColumn('owner_loyalty', 'owner_phone_number')) {
				$table->renameColumn('owner_phone_number', 'user_id');
			}
        });
    }
}
