<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlSchemeToNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->string('url', 250)->nullable();
            $table->string('scheme', 250)->nullable();
            $table->string('text_button_one', 100)->nullable();
            $table->string('url_button_one', 250)->nullable();
            $table->string('scheme_button_one', 250)->nullable();
            $table->string('text_button_two', 100)->nullable();
            $table->string('url_button_two', 250)->nullable();
            $table->string('scheme_button_two', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('scheme');
            $table->dropColumn('text_button_one');
            $table->dropColumn('url_button_one');
            $table->dropColumn('scheme_button_one');
            $table->dropColumn('text_button_two');
            $table->dropColumn('url_button_two');
            $table->dropColumn('scheme_button_two');
        });
    }
}
