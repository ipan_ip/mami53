<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemoveFlagEnumActionInKostLevelHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE kost_level_history CHANGE COLUMN action action ENUM('upgrade', 'downgrade', 'approve', 'reject', 'remove_flag') DEFAULT NULL");
        Schema::table('kost_level_history', function (Blueprint $table) {
            $table->integer('flag')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE kost_level_history CHANGE COLUMN action action ENUM('upgrade', 'downgrade', 'approve', 'reject') DEFAULT NULL");
        Schema::table('kost_level_history', function (Blueprint $table) {
            $table->boolean('flag')->nullable()->change();
        });
    }
}
