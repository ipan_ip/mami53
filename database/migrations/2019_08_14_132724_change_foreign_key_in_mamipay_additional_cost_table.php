<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeyInMamipayAdditionalCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_additional_cost', function (Blueprint $table) {
            $table->unsignedInteger('group_id')->nullable()->change();
            $table->unsignedInteger('parent_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_additional_cost', function (Blueprint $table) {
            $table->integer('group_id')->nullable()->change();
            $table->integer('parent_id')->nullable()->change();
        });
    }
}
