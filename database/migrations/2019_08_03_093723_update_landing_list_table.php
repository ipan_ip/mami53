<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLandingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_list', function (Blueprint $table) {
            $table->tinyInteger('is_active')->default(0)->after('is_default');
            $table->integer('sort')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_list', function (Blueprint $table) {
            $table->dropColumn('is_active');
            $table->dropColumn('sort');
        });
    }
}
