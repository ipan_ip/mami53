<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScaleColumnToDesignerReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_review', function (Blueprint $table) {
            $table->float('rating', 8, 2)->change();
            $table->float('cleanliness', 8, 2)->change();
            $table->float('comfort', 8, 2)->change();
            $table->float('safe', 8, 2)->change();
            $table->float('price', 8, 2)->change();
            $table->float('room_facility', 8, 2)->change();
            $table->float('public_facility', 8, 2)->change();
            $table->tinyInteger('scale')->after('rating')->default(4);

            $table->index('rating', 'rating');
            $table->index('scale', 'scale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_review', function (Blueprint $table) {
            $table->dropIndex('scale');
            $table->dropIndex('rating');

            $table->dropColumn('scale');
            $table->integer('public_facility')->change();
            $table->integer('room_facility')->change();
            $table->integer('price')->change();
            $table->integer('safe')->change();
            $table->integer('comfort')->change();
            $table->integer('cleanliness')->change();
            $table->integer('rating')->change();
        });
    }
}
