<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReferenceAndReferenceIdInPriceAdditionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('price_additional', function (Blueprint $table) {
            $table->string('reference', 20);
            $table->unsignedInteger('reference_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_additional', function (Blueprint $table) {
            $table->dropColumn('reference');
            $table->dropColumn('reference_id');
        });
    }
}
