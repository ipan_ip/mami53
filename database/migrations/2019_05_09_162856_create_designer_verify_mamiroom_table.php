<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerVerifyMamiroomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_verify_mamiroom', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->enum('action', ['verify', 'unverify'])->nullable()->default('verify');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_verify_mamiroom');
    }
}
