<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingHousePropertyTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_house_property_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('landing_house_property_id');
            $table->unsignedInteger('tag_id');
            $table->foreign('landing_house_property_id', 'FK_landing_house_property_id')->references('id')->on('landing_house_property')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
            $table->foreign('tag_id', 'FK_tag_id')->references('id')->on('tag')->onDelete('RESTRICT')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_house_property_tag');
    }
}
