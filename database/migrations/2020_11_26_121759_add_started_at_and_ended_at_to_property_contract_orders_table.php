<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartedAtAndEndedAtToPropertyContractOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->dateTime('ended_at')->after('scheduled_date')->nullable();
            $table->dateTime('paid_at')->after('scheduled_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->dropColumn('paid_at');
            $table->dropColumn('ended_at');
        });
    }
}
