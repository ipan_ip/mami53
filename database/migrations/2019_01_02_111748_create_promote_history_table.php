<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreatePromoteHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promote_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('for', 10)->nullable()->default('click');
            $table->integer('start_view')->nullable();
            $table->integer('end_view')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->integer('used_view')->nullable();
            $table->integer('view_promote_id')->nullable();
            $table->integer('session_id')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->index('view_promote_id');
            $table->index('for');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promote_history');
    }
}
