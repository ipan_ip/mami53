<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisburseConfirmStatusColumnToBookingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->enum(
                'transfer_permission',
                [
                    'confirmed',
                    'allow_refund',
                    'allow_disburse',
                    'refunded',
                    'disbursed',
                ]
            )->nullable();
            $table->unsignedInteger('owner_id')->nullable();

            $table->index('owner_id', 'owner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->dropIndex('owner_id');

            $table->dropColumn('owner_id');
            $table->dropColumn('transfer_permission');
        });
    }
}
