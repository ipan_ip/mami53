<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnDesignerPriceFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_price_filter', function (Blueprint $table) {
            $table->index('final_price_weekly');
            $table->index('final_price_quarterly');
            $table->index('final_price_semiannually');
            $table->index('final_price_yearly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_price_filter', function (Blueprint $table) {
            $table->dropIndex('final_price_weekly');
            $table->dropIndex('final_price_quarterly');
            $table->dropIndex('final_price_semiannually');
            $table->dropIndex('final_price_yearly');
        });
    }
}
