<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomLandingInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_landing_input', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200)->nullable();
            $table->string('address', 150)->nullable();
            $table->integer('room_total')->nullable();
            $table->integer('room_available')->nullable();
            $table->integer('price_monthly')->nullable();
            $table->string('room_condition', 250)->nullable();
            $table->smallInteger('is_register')->default(0);
            $table->string('location_detail', 255)->nullable();
            $table->string('owner_name', 100)->nullable();
            $table->string('owner_phone', 15)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamiroom_landing_input');
    }
}
