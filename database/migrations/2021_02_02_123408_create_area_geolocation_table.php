<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaGeolocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'area_geolocation',
            function (Blueprint $table) {
                $table->increments('id');
                $table->char('province', 100);
                $table->char('city', 100)->nullable();
                $table->char('subdistrict', 100)->nullable();
                $table->char('village', 100)->nullable();
                $table->geometry('geolocation');
                $table->timestamps();

                $table->spatialIndex('geolocation');
            }
        );

        // Indexes
        DB::statement("ALTER TABLE `area_geolocation` ADD FULLTEXT INDEX `province` (`province`)");
        DB::statement("ALTER TABLE `area_geolocation` ADD FULLTEXT INDEX `city` (`city`)");
        DB::statement("ALTER TABLE `area_geolocation` ADD FULLTEXT INDEX `subdistrict` (`subdistrict`)");
        DB::statement("ALTER TABLE `area_geolocation` ADD FULLTEXT INDEX `village` (`village`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_geolocation');
    }
}
