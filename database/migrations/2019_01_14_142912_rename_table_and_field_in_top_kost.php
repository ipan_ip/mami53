<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableAndFieldInTopKost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("top_kost", "designer_top");
        Schema::rename("top_kost_list", "designer_top_list");

        Schema::table('designer_top', function (Blueprint $table) {
            $table->renameColumn('kost_count', 'designer_count');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("designer_top", "top_kost");
        Schema::rename("designer_top_list", "top_kost_list");

        Schema::table('designer_top', function (Blueprint $table) {
            $table->renameColumn('designer_count', 'kost_count');         
        });
    }
}
