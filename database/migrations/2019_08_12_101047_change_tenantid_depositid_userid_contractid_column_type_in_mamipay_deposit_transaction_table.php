<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTenantidDepositidUseridContractidColumnTypeInMamipayDepositTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {             
            $table->unsignedInteger('contract_id')->nullable()->change();
            $table->unsignedInteger('tenant_id')->nullable()->change();
            $table->unsignedInteger('user_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //revert back column type
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->integer('contract_id')->nullable()->change();
            $table->integer('tenant_id')->nullable()->change();
            $table->integer('user_id')->nullable()->change();
        });
    }
}
