<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOwnerRegisterRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_register_request', function (Blueprint $table) {
            $table->string('room_name')->nullable();
            $table->string('room_slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_register_request', function (Blueprint $table) {
            $table->dropColumn('room_slug');
            $table->dropColumn('room_name');
        });
    }
}
