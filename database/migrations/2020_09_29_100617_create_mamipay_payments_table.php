<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount', 18, 2);
            $table->decimal('paid_amount', 18, 2);
            $table->string('status', 31);
            $table->unsignedInteger('invoice_id')->index('payments_invoice_id_index');
            $table->string('payment_method', 127);
            $table->string('destination', 255)->index('payments_destination_index')->nullable();
            $table->string('payment_number', 255)->index('payments_payment_number_index')->nullable();
            $table->string('transaction_id', 255)->index('payments_transaction_id_index')->nullable();
            $table->text('deeplink_url')->nullable();
            $table->unsignedInteger('webhook_id')->index('payments_webhook_id_index')->nullable();
            $table->dateTime('paid_at')->nullable();
            $table->dateTime('cancelled_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_payments');
    }
}
