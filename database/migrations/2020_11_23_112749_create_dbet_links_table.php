<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbetLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dbet_links', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->string('code')->unique();
            $table->tinyInteger('is_required_identity')->default(0);
            $table->tinyInteger('is_required_due_date')->default(0);
            $table->integer('due_date')->default(0);
            $table->timestamps();
            $table->softDeletes();

            // create index
            $table->index('designer_id', 'designer_id');
            $table->index('code', 'code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dbet_links');
    }
}
