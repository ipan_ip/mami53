<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number', 63)->index('invoices_invoice_number_index');
            $table->string('status', 31);
            $table->string('order_type', 127);
            $table->unsignedInteger('order_id');
            $table->decimal('amount', 18, 2);
            $table->decimal('paid_amount', 18, 2)->nullable();
            $table->dateTime('expiry_time')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('paid_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['order_type', 'order_id'], 'invoices_order_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_invoices');
    }
}
