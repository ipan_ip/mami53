<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVerifyIdentityCardFieldOnUserVerificationCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->renameColumn('is_verify_identity_card', 'identity_card');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->renameColumn('identity_card', 'is_verify_identity_card');
        });
    }
}
