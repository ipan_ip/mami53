<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiryFieldsToPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->unsignedInteger('expiry_value')->default(0)->after('tnc');
            $table->string('expiry_unit', 10)->default('month')->after('expiry_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->dropColumn('expiry_value');
            $table->dropColumn('expiry_unit');
        });
    }
}
