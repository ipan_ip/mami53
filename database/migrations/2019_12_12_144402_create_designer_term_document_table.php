<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerTermDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_term_document', function (Blueprint $table) {
            $table->increments('id');
            $table->string('real_file_name', 255);
            $table->unsignedInteger('designer_term_id');
            $table->unsignedInteger('document_id');
            $table->unsignedInteger('media_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_term_document');
    }
}
