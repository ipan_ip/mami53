<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSmsContentInNotificationWhatsappTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_whatsapp_template', function (Blueprint $table) {
            $table->text('sms_content')->after('content')->nullable();
			$table->tinyInteger('sms_mode')->after('also_send_sms')->default(0)->comment="0 = synchronized, 1 = fallback, 2 = independent";
			$table->renameColumn('also_send_sms', 'sms_active')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_whatsapp_template', function (Blueprint $table) {
            $table->dropColumn('sms_content');
			$table->dropColumn('sms_mode');
			$table->renameColumn('sms_active', 'also_send_sms')->change();
        });
    }
}
