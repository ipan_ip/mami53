<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerFacilitySettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_facility_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
			$table->text('active_tags')->comment="This will be placed for active tags of related Kos/Apt in JSON format";
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_facility_setting');
    }
}
