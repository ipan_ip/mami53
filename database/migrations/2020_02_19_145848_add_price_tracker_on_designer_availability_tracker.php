<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceTrackerOnDesignerAvailabilityTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_availability_tracker', function (Blueprint $table) {
            $table->unsignedInteger('price_before')->after('after_value')->nullable();
            $table->unsignedInteger('price_after')->after('price_before')->nullable();
            $table->unsignedInteger('price_daily_before')->after('price_after')->nullable()->default(0);
            $table->unsignedInteger('price_daily_after')->after('price_daily_before')->nullable()->default(0);
            $table->unsignedInteger('price_weekly_before')->after('price_daily_after')->nullable()->default(0);
            $table->unsignedInteger('price_weekly_after')->after('price_weekly_before')->nullable()->default(0);
            $table->unsignedInteger('price_monthly_before')->after('price_weekly_after')->nullable()->default(0);
            $table->unsignedInteger('price_monthly_after')->after('price_monthly_before')->nullable()->default(0);
            $table->unsignedInteger('price_yearly_before')->after('price_monthly_after')->nullable()->default(0);
            $table->unsignedInteger('price_yearly_after')->after('price_yearly_before')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_availability_tracker', function (Blueprint $table) {
            $table->dropColumn('price_before');
            $table->dropColumn('price_after');
            $table->dropColumn('price_daily_before');
            $table->dropColumn('price_daily_after');
            $table->dropColumn('price_weekly_before');
            $table->dropColumn('price_weekly_after');
            $table->dropColumn('price_monthly_before');
            $table->dropColumn('price_monthly_after');
            $table->dropColumn('price_yearly_before');
            $table->dropColumn('price_yearly_after');
        });
    }
}
