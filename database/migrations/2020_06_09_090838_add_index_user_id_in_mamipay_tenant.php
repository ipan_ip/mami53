<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexUserIdInMamipayTenant extends Migration
{
    const INDEX_NAME = 'mamipay_tenant_user_id_index';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->index('user_id', self::INDEX_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropIndex(self::INDEX_NAME);
        });
    }
}
