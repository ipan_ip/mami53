<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousePropertyRejectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_property_reject', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_property_id')->unsigned()->nullable();
            $table->string('detail', 250)->nullable();
            $table->foreign('house_property_id', 'FK_house_property_property_id')->references('id')->on('house_property')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_property_reject');
    }
}
