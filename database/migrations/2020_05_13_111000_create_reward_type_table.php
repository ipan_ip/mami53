<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 50);
            $table->string('name', 50);
            $table->timestamps();
            $table->softDeletes();

            $table->unique('key', 'key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_type');
    }
}
