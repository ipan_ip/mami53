<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefundReasonColumnInBookingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'booking_user',
            function (Blueprint $table) {
                $table->enum(
                    'refund_reason',
                    [
                        'room_is_full',
                        'rejected_by_owner',
                        'room_not_like_in_ads',
                        'booking_is_cancelled',
                        'non_instant_booking',
                        'owner_not_accept_mamipay'
                    ]
                )->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'booking_user',
            function (Blueprint $table) {
                $table->dropColumn('refund_reason');
            }
        );
    }
}
