<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateAgentReviewsDummyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_reviews_dummy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->integer('designer_id');
            $table->integer('cleanliness')->nullable()->default(0);
            $table->integer('comfort')->nullable()->default(0);
            $table->integer('safe')->nullable()->default(0);
            $table->integer('price')->nullable()->default(0);
            $table->integer('room_facilities')->nullable()->default(0);
            $table->integer('public_facilities')->nullable()->default(0);
            $table->text('content');
            $table->enum('status', ['live', 'waiting', 'rejected', 'deleted']);
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->string('photo', 255)->nullable();

            $table->index('deleted_at', 'deleted_at');
            $table->index('status', 'status');
            $table->index('designer_id', 'designer_id');
            $table->index('agent_id', 'agent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_reviews_dummy');
    }
}
