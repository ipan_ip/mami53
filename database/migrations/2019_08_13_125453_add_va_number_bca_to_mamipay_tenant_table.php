<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVaNumberBcaToMamipayTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_tenant','va_number_bca')){                
                $table->string('va_number_bca')->nullable()->after('va_number_bni');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropColumn('va_number_bca');
        });
    }
}
