<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantPotentialOwnerFollowupHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_potential_owner_followup_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('potential_owner_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamp('new_at')->nullable();
            $table->timestamp('ongoing_at')->nullable();
            $table->timestamp('done_at')->nullable();
            $table->timestamps();

            $table->index('potential_owner_id', 'potential_owner_id');
            $table->index('user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_potential_owner_followup_history');
    }
}
