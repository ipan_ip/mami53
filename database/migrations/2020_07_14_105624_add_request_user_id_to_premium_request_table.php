<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestUserIdToPremiumRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_request', function (Blueprint $table) {
            $table->unsignedInteger('request_user_id')->nullable();
            $table->index('request_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_request', function (Blueprint $table) {
            $table->dropIndex(['request_user_id']);
            $table->dropColumn('request_user_id');
        });
    }
}
