<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsActiveToBookingOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_owner', function (Blueprint $table) {
            if (!Schema::hasColumn('booking_owner','is_active')){                
                $table->integer('is_active')->nullable()->after('owner_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_owner', function (Blueprint $table) {
            $table->dropColumn('is_active');            
        });
    }
}
