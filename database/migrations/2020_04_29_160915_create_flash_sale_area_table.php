<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlashSaleAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'flash_sale_area',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('flash_sale_id');
                $table->string('name', 100);
                $table->string('added_by', 100);
                $table->timestamps();
                $table->softDeletes();

                $table->index('flash_sale_id');
                $table->index('name');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_sale_area');
    }
}
