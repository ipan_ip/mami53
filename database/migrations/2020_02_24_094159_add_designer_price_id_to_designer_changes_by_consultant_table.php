<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesignerPriceIdToDesignerChangesByConsultantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_changes_by_consultant', function (Blueprint $table) {
            $table->unsignedInteger('designer_price_id')->after('value_after')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_changes_by_consultant', function (Blueprint $table) {
            $table->dropColumn('designer_price_id');
        });
    }
}
