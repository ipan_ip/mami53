<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInDesignerSortScoreTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
			$table->boolean('is_guarantee')->default(false)->after('can_booking');

			$table->boolean('hostility')->default(false)->change(); // no need, this field creation use tinyInteger() which is same
			$table->boolean('available_room_count')->default(false)->change();

			$table->renameColumn('hostility', 'is_not_hostile');
			$table->renameColumn('available_room_count', 'has_available_room');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
            $table->renameColumn('has_available_room', 'available_room_count');
			$table->renameColumn('is_not_hostile', 'hostility');

			$table->integer('has_available_room')->default(0)->change();
			// $table->boolean('is_not_hostile')->default(0)->change();

			$table->dropColumn('is_guarantee');
        });
    }
}
