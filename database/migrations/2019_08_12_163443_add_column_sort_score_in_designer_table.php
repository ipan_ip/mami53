<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSortScoreInDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->integer('sort_score')->default(0)->index();
            
            $table->index(['sort_score', 'kost_updated_date'], 'sort_score_kost_updated_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->dropIndex('sort_score_kost_updated_date');
            $table->dropColumn('sort_score'); 
        });
    }
}
