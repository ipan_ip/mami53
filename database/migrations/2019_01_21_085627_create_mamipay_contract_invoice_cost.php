<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayContractInvoiceCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_contract_invoice_cost', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id');
            $table->string('cost_title',500);
            $table->float('cost_value',9,0);
            $table->integer('sort_order')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_contract_invoice_cost');
    }
}
