<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_level', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->unsignedInteger('order')->default(0);
            $table->boolean('is_regular')->default(false);
            $table->boolean('is_hidden')->default(false);
            $table->string('notes')->nullable();
            $table->decimal('charging_fee', 16,2)->default(0);

            $table->enum('charging_type', ['percentage', 'amount'])
                ->default('percentage');
            
            $table->boolean('charge_for_owner')->default(false);
            $table->boolean('charge_for_consultant')->default(false);
            $table->boolean('charge_for_booking')->default(false);

            $table->enum('charge_invoice_type', ['all', 'per_contract', 'per_tenant'])
                ->default('all');

            $table->timestamps();
            $table->softDeletes();

            $table->index('order', 'order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_level');
    }
}
