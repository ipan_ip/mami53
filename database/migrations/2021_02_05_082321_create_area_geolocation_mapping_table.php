<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaGeolocationMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'area_geolocation_mapping',
            function (Blueprint $table) {
                $table->unsignedInteger('area_geolocation_id');
                $table->unsignedInteger('search_input_keyword_suggestion_id');
                $table->timestamps();

                // Table's Primary Key (composite index)
                $table->primary(
                    [
                        'area_geolocation_id',
                        'search_input_keyword_suggestion_id'
                    ],
                    'area_geolocation_mapping_id'
                );

                // Table's Foreign Keys
                $table->foreign('area_geolocation_id', 'area_geolocation_id_fk')
                    ->references('id')
                    ->on('area_geolocation');
                $table->foreign('search_input_keyword_suggestion_id', 'search_input_keyword_suggestion_fk')
                    ->references('id')
                    ->on('search_input_keyword_suggestion');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_geolocation_mapping');
    }
}
