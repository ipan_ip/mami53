<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnTitlePageTypeLandingMetaOg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_meta_og', function (Blueprint $table) {
            if (! Schema::hasColumn('title', 'page_type')) {
                $table->text('title');
                $table->string('page_type', 50);

                $table->index('page_type', 'page_type');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_meta_og', function (Blueprint $table) {
            $table->dropIndex('page_type');

            $table->dropColumn('title');
            $table->dropColumn('page_type');
        });
    }
}
