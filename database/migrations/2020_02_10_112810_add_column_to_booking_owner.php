<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToBookingOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_owner', function (Blueprint $table) {
            $table->text('rent_counts')->nullable()->after('is_active');
            $table->text('additional_prices')->nullable()->after('rent_counts');
            $table->boolean('is_instant_booking')->default(false)->after('rent_counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_owner', function (Blueprint $table) {
            $table->dropColumn('rent_counts');
            $table->dropColumn('additional_prices');
            $table->dropColumn('is_instant_booking');
        });
    }
}
