<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsVerifyOwnerColumnToUserVerificationAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->boolean('is_verify_owner')->after('identity_card')->nullable(); // Indicate whether the related user is an owner
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->dropColumn('is_verify_owner');
        });
    }
}
