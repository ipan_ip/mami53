<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_designer', function (Blueprint $table) {
            $table->unsignedInteger('consultant_id');
            $table->unsignedInteger('designer_id');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['consultant_id', 'designer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_designer');
    }
}
