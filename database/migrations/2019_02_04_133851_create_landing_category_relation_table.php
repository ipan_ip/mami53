<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingCategoryRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_category_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('landing_id');
            $table->integer('landing_category_id');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->index(['landing_id', 'landing_category_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_category_relation');                
    }
}
