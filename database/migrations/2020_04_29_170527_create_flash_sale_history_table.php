<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlashSaleHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_log')
            ->create(
                'flash_sale_history',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('flash_sale_id');
                    $table->text('original_value');
                    $table->text('modified_value');
                    $table->string('triggered_by', 100);
                    $table->timestamps();

                    $table->index('flash_sale_id');
                }
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_log')
            ->dropIfExists('flash_sale_history');
    }
}
