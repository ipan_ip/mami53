<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKostLevelHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kost_level_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kost_id');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->enum('action', ['upgrade', 'downgrade', 'approve', 'reject'])->nullable();
            $table->boolean('is_force')->nullable();
            $table->boolean('flag')->nullable();
            $table->boolean('flag_reminder')->nullable();
            $table->timestamps();

            $table->index('kost_id', 'kost_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_level_history');
    }
}
