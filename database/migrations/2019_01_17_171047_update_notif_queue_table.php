<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotifQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('notif_queue', function (Blueprint $table) {
            $table->text('token')->nullable()->change();
        });

        Schema::rename('notif_queue', 'notification_queue');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename('notification_queue', 'notif_queue');

        Schema::table('notif_queue', function (Blueprint $table) {
            $table->string('token', 500)->nullable();
        });
    }
}
