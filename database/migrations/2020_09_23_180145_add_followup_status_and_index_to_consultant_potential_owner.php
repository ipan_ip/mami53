<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFollowupStatusAndIndexToConsultantPotentialOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_potential_owner', function (Blueprint $table) {
            $table->enum('bbk_status', ['non-bbk', 'waiting', 'bbk']);
            $table->enum('followup_status', ['new', 'ongoing', 'done'])
                ->default('new');

            $table->index('bbk_status', 'bbk_status');
            $table->index(['followup_status', 'updated_at'], 'followup_status_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_potential_owner', function (Blueprint $table) {
            $table->dropIndex('followup_status_updated_at');
            $table->dropIndex('bbk_status');
            $table->dropColumn('followup_status');
            $table->dropColumn('bbk_status');
        });
    }
}
