<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerNameToMamiroomOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mamiroom_owner')) {
            Schema::table('mamiroom_owner', function (Blueprint $table) {
                if (!Schema::hasColumn('mamiroom_owner','owner_name')){                
                    $table->string('owner_name')->nullable()->after('phone_number');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamiroom_owner', function (Blueprint $table) {
            $table->dropColumn('owner_name');
        });
    }
}
