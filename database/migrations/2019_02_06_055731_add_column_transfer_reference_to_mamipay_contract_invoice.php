<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTransferReferenceToMamipayContractInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_contract_invoice','transfer_reference')){                
                $table->string('transfer_reference')->nullable()->after('transfer_status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->dropColumn('transfer_reference');
        });
    }
}
