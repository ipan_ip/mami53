<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousePropertyLandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_house_property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_type', 20);
            $table->string('type', 15)->nullable();
            $table->string('slug', 255)->nullable();
            $table->integer('parent_id')->unsigned();
            $table->string('heading_1', 150)->nullable();
            $table->string('heading_2', 150)->nullable();
            $table->string('keyword', 100)->nullable();
            $table->text('description')->nullable();
            $table->double('latitude_1')->nullable();
            $table->double('longitude_1')->nullable();
            $table->double('latitude_2')->nullable();
            $table->double('longitude_2')->nullable();
            $table->string('area_city', 100)->nullable();
            $table->string('area_subdistrict', 100)->nullable();
            $table->integer('price_min')->nullable();
            $table->integer('price_max')->nullable();
            $table->string('rent_type', 15)->nullable();
            $table->integer('dummy_counter')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_house_property');
    }
}
