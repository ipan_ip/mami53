<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasMultipleTypeOnProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->boolean('has_multiple_type')->default(true);

            $table->index(['owner_user_id', 'has_multiple_type'], 'has_multiple_type_by_owner_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropIndex('has_multiple_type_by_owner_index');

            $table->dropColumn('has_multiple_type');
        });
    }
}
