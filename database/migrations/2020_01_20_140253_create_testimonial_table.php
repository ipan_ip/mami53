<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'testimonial',
            function (Blueprint $table)
            {
                $table->increments('id');
                $table->unsignedInteger('photo_id');
                $table->string('owner_name', 50);
                $table->string('kost_name', 100);
                $table->text('quote');
                $table->boolean('is_active')->default(false);
                $table->smallInteger('order')->default(0);
                $table->unsignedInteger('creator_id');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonial');
    }
}
