<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateMarketplaceProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('title', 190)->nullable();
            $table->string('slug', 190)->nullable();
            $table->text('description')->nullable();
            $table->string('contact_phone', 50)->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('address', 255)->nullable();
            $table->decimal('price_regular', 10, 0)->nullable();
            $table->decimal('price_sale', 10, 0)->nullable();
            $table->string('condition', 10)->nullable();
            $table->string('price_unit', 100)->nullable();
            $table->string('status', 30)->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->string('product_type', 100)->nullable();
            $table->string('availability', 50)->nullable()->default('available');
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('title', 'title');
            $table->index('slug', 'slug');
            $table->index('latitude', 'latitude');
            $table->index('longitude', 'longitude');
            $table->index('user_id', 'user_id');
            $table->index('price_regular', 'price_regular');
            $table->index('price_sale', 'price_sale');
            $table->index('is_active', 'is_active');
            $table->index('deleted_at', 'deleted_at');
            $table->index('status', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_product');
    }
}
