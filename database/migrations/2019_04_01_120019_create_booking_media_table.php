<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_user_id');
            $table->string('type', 20)->nullable();
            $table->string('file_name', 250)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('booking_user_id', 'booking_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_media');
    }
}
