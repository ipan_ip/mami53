<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPriceDueDateAndDesignerIdInConsultantToolsPotentialTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->unsignedInteger('designer_id')->after('property_name')->nullable();
            $table->integer('price')->after('designer_id')->nullable();
            $table->tinyInteger('due_date')->after('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->dropColumn('designer_id');
            $table->dropColumn('price');
            $table->dropColumn('due_date');
        });
    }
}
