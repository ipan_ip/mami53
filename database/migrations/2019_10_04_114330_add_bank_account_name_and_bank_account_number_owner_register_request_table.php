<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountNameAndBankAccountNumberOwnerRegisterRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_register_request', function (Blueprint $table) {
            $table->string('bank_account_name', 200)->nullable();
            $table->string('bank_account_number', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_register_request', function (Blueprint $table) {
            $table->dropColumn('bank_account_number');
            $table->dropColumn('bank_account_name');
        });
    }
}
