<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomLevelIdToMamipayContractInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * Approval Docs : https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/727292521/2020-09-02+-+PAY+Add+new+column+room+level+id+in+mamipay+contract+invoice+table
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->unsignedInteger('room_level_id')
                ->nullable()
                ->after('is_fraud')
                ->index('contract_invoice_room_level_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->dropIndex('contract_invoice_room_level_id_index');
            $table->dropColumn('room_level_id');
        });
    }
}
