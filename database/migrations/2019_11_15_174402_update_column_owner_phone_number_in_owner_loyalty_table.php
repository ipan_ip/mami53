<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnOwnerPhoneNumberInOwnerLoyaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_loyalty', function (Blueprint $table) {
			$table->string('owner_phone_number', 15)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_loyalty', function (Blueprint $table) {
        	$table->dropColumn('owner_phone_number');
			$table->unsignedInteger('user_id')->after('id');
        });
    }
}
