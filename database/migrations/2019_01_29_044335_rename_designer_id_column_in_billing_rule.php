<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDesignerIdColumnInBillingRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_billing_rule', function (Blueprint $table) {
            $table->renameColumn('designer_id', 'room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_billing_rule', function (Blueprint $table) {
            $table->renameColumn('room_id', 'designer_id');
        });
    }
}
