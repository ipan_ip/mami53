<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantSalesMotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_sales_motion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', [
                'promotion',
                'campaign',
                'product',
                'feature'
            ]);
            $table->enum('created_by_division', [
                'marketing',
                'commercial'
            ]);
            $table->boolean('is_active');
            $table->date('start_date');
            $table->date('end_date');

            $table->unsignedInteger('media_id')->nullable();
            $table->index('media_id');

            $table->string('objective');
            $table->string('terms_and_condition');
            $table->string('benefit');
            $table->string('requirement');

            $table->string('url')->nullable();
            $table->string('max_participant');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_sales_motion');
    }
}
