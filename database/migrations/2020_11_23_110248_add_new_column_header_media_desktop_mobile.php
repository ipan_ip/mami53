<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnHeaderMediaDesktopMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dynamic_landing_parents', function (Blueprint $table) {
            if (! Schema::hasColumn('dynamic_landing_parents', 'desktop_header_media_id')) {
                $table->integer('desktop_header_media_id');

                $table->index('desktop_header_media_id', 'desktop_header_media_id');
            }

            if (! Schema::hasColumn('dynamic_landing_parents', 'mobile_header_media_id')) {
                $table->integer('mobile_header_media_id');

                $table->index('mobile_header_media_id', 'mobile_header_media_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamic_landing_parents', function (Blueprint $table) {
            $table->dropIndex('desktop_header_media_id');
            $table->dropIndex('mobile_header_media_id');

            $table->dropColumn('desktop_header_media_id');
            $table->dropColumn('mobile_header_media_id');
        });
    }
}
