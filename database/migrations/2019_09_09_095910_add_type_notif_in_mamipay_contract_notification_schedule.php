<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeNotifInMamipayContractNotificationSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_notification_schedule', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_contract_notification_schedule','type_notif')){                
                $table->string('type_notif')->nullable()->after('sent_at');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_notification_schedule', function (Blueprint $table) {
            $table->dropColumn('type_notif');
        });
    }
}
