<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerVacancyLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_vacancy_location', function (Blueprint $table) {
            $table->unsignedInteger('career_vacancy_id');
            $table->unsignedInteger('career_location_id');

            $table->primary(['career_vacancy_id', 'career_location_id'], 'id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_vacancy_location');
    }
}
