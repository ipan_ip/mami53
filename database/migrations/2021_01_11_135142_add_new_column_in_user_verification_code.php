<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnInUserVerificationCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verification_code', function (Blueprint $table) {
            $table->unsignedSmallInteger('via')->nullable();
            $table->string('destination', 150)->nullable();

            $table->index([
                'via',
                'destination',
            ], 'user_verification_code_via_destination_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_verification_code', function (Blueprint $table) {
            $table->dropIndex('user_verification_code_via_destination_idx');

            $table->dropColumn([
                'via',
                'destination'
            ]);
        });
    }
}
