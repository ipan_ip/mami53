<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostLevelValueMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'kost_level_value_mapping',
            function (Blueprint $table) {
                $table->unsignedInteger('kost_level_id');
                $table->unsignedInteger('kost_level_value_id');
                $table->unsignedInteger('created_by');
                $table->timestamps();

                $table->primary(
                    [
                        'kost_level_id',
                        'kost_level_value_id'
                    ],
                    'kost_level_value_mapping_id'
                );

                $table->foreign('kost_level_id')->references('id')->on('kost_level');
                $table->foreign('kost_level_value_id')->references('id')->on('kost_level_value');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_level_value_mapping');
    }
}
