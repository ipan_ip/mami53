<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoldplusPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goldplus_package', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 6);
            $table->string('name', 64);
            $table->string('description');
            $table->unsignedInteger('price');
            $table->enum('periodicity', ['daily', 'weekly', 'monthly', 'quarterly', 'semianually', 'yearly']);
            $table->enum('unit_type', ['single', 'all']);
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goldplus_package');
    }
}
