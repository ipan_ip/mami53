<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnDestinationTransactionIdPaymentNumberAtMamipayContractPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_payment', function (Blueprint $table) {
            $table->index('destination', 'contract_payment_destination_index');
            $table->index('payment_number', 'contract_payment_payment_number_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_payment', function (Blueprint $table) {
            $table->dropIndex('contract_payment_destination_index');
            $table->dropIndex('contract_payment_payment_number_index');
        });
    }
}
