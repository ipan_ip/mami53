<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbetLinkRegisteredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dbet_link_registered', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->unsignedInteger('designer_room_id')->nullable();
            $table->string('fullname')->nullable();
            $table->string('gender')->nullable();
            $table->string('phone')->nullable();
            $table->string('jobs')->nullable();
            $table->string('work_place')->nullable();
            $table->unsignedInteger('identity_id')->nullable();
            $table->integer('due_date')->default(0);
            $table->string('rent_count_type')->nullable();
            $table->integer('price')->default(0);
            $table->text('additional_price_detail')->nullable();
            $table->string('group_channel_url')->nullable();
            $table->enum('status',
                [
                    'pending',
                    'verified',
                    'rejected',
                ]
            )->default('pending');
            $table->timestamps();
            $table->softDeletes();

            // create index
            $table->index('designer_id', 'designer_id');
            $table->index('designer_room_id', 'designer_room_id');
            $table->index('status', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dbet_link_registered');
    }
}
