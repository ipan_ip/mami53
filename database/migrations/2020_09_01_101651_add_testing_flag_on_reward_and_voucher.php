<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTestingFlagOnRewardAndVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reward', function (Blueprint $table) {
            $table->boolean('is_testing')->default(false)->after('is_published');
        });

        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->boolean('is_testing')->default(false)->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reward', function (Blueprint $table) {
            $table->dropColumn('is_testing');;
        });

        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropColumn('is_testing');;
        });
    }
}
