<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceIdToMamipayManualPayout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->bigInteger('invoice_id')->nullable()->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->dropColumn('invoice_id');
        });
    }
}
