<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('point_id');
            $table->unsignedInteger('activity_id');
            $table->unsignedInteger('room_group_id')->nullable();
            $table->unsignedInteger('received_each')->default(0);
            $table->unsignedInteger('times_to_received')->default(0);
            $table->string('limit_type', 25);
            $table->integer('limit');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_setting');
    }
}
