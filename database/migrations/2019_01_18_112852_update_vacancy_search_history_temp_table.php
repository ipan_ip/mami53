<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVacancySearchHistoryTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('vacancy_search_history_temp', function (Blueprint $table) {
            $table->renameColumn('filters', 'filter');
            $table->renameColumn('titikTengah', 'center_point');
            $table->renameColumn('namaKota', 'city_name');
        });

        Schema::rename('vacancy_search_history_temp', 'log_vacancy_search_history_temp');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename('log_vacancy_search_history_temp', 'vacancy_search_history_temp');

        Schema::table('vacancy_search_history_temp', function (Blueprint $table) {
            $table->renameColumn('filter', 'filters');
            $table->renameColumn('center_point', 'titikTengah');
            $table->renameColumn('city_name', 'namaKota');
        });
    }
}
