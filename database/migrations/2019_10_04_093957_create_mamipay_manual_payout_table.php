<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayManualPayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_manual_payout', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider');
            $table->string('transaction_id');
            $table->string('destination_account');
            $table->string('destination_bank');
            $table->string('destination_name');
            $table->float('transfer_amount', 9, 0);
            $table->longText('reason');
            $table->string('status');    
            $table->unsignedInteger('created_by');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_manual_payout');
    }
}
