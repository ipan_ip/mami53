<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountCityToMamipayOwnerProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_owner_profile','bank_account_city')){                
                $table->string('bank_account_city')->nullable()->after('bank_account_owner');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            $table->dropColumn('bank_account_city');
        });
    }
}
