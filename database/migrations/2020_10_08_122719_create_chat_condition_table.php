<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chat_question_id');
            $table->string('chat_criteria_ids', 100);
            $table->text('reply');
            $table->smallInteger('order')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('chat_question_id');
            $table->index('chat_criteria_ids');
            $table->index('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_condition');
    }
}
