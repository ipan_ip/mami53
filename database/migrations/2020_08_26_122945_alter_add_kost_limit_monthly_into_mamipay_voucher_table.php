<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddKostLimitMonthlyIntoMamipayVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->unsignedInteger('kost_limit_monthly')->default(0)->after('kost_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropColumn('kost_limit_monthly');
        });
    }
}
