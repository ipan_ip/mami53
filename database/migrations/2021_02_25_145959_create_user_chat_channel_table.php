<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_chat_channel', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable(false);
            $table->unsignedInteger('room_id')->nullable(false);
            $table->unsignedInteger('chat_channel_id')->nullable(false);
            $table->enum('joined_as', ['tenant', 'owner', 'consultant'])->nullable(false);
            $table->timestamps();
        });

        Schema::table('user_chat_channel', function (Blueprint $table) {
            $table->unique(['user_id', 'chat_channel_id'], 'user_id_chat_channel_id');
            $table->index(['room_id', 'user_id', 'joined_as'], 'room_id_user_id_joined_as');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_chat_channel', function (Blueprint $table) {
            $table->dropIndex('room_id_user_id_joined_as');
            $table->dropIndex('user_id_chat_channel_id');
        });
        Schema::dropIfExists('user_chat_channel');
    }
}
