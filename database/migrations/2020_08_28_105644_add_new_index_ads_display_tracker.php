<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewIndexAdsDisplayTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_ads_display_tracker', function (Blueprint $table) {
            $table->index('created_at', 'ads_display_tracker_created_at_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_ads_display_tracker', function (Blueprint $table) {
            $table->dropIndex('ads_display_tracker_created_at_index');
        });
    }
}
