<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostTypeToMamipayContractInvoiceCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice_cost', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_contract_invoice_cost','cost_type')){                
                $table->string('cost_type')->nullable()->after('cost_value');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice_cost', function (Blueprint $table) {
            $table->dropColumn('cost_value');
        });
    }
}
