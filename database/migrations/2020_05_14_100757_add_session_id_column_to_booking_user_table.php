<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * DB Changelog
 * https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/461307926/Draft+2020-05-14+-+Add+a+new+column+session+id+to+booking+user
 */
class AddSessionIdColumnToBookingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->string('moengage_session_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->dropColumn('moengage_session_id');
        });
    }
}
