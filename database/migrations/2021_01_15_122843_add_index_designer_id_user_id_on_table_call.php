<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexDesignerIdUserIdOnTableCall extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call', function(Blueprint $table) {
            $table->index(['designer_id', 'user_id'], 'designer_id_user_id');
            $table->dropIndex('designer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call', function(Blueprint $table) {
            $table->index('designer_id', 'designer_id');
            $table->dropIndex('designer_id_user_id');
        });
    }
}
