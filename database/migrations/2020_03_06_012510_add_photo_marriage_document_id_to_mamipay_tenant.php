<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoMarriageDocumentIdToMamipayTenant extends Migration
{
    /**
     * Run the migrations.
     * link approval : https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/299630648/2020-03-05+Add+New+Column+to+mamipay+tenant
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->integer('photo_marriage_document_id')->nullable()->after('marital_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropColumn('photo_marriage_document_id');
        });
    }
}
