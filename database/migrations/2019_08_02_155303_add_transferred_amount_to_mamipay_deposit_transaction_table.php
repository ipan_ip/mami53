<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferredAmountToMamipayDepositTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_deposit_transaction','transferred_amount')){                
                $table->float('transferred_amount', 9, 0)->nullable()->after('amount');
            }    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->dropColumn('transferred_amount');
        });
    }
}
