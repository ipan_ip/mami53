<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicLandingChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_landing_children', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->string('area_type', 10);
            $table->string('area_name', 100);
            $table->string('slug', 255);
            $table->double('longitude_1');
            $table->double('latitude_1');
            $table->double('longitude_2');
            $table->double('latitude_2');
            $table->text('meta_desc')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('dynamic_landing_parents')
                ->onDelete('cascade')
                ->comment('Foreign key to dynamic_landing_parents.id');

            $table->index('parent_id', 'parent_id');
            $table->index('area_type', 'area_type');
            $table->index('slug', 'slug');
            $table->index('longitude_1', 'longitude_1');
            $table->index('latitude_1', 'latitude_1');
            $table->index('longitude_2', 'longitude_2');
            $table->index('latitude_2', 'latitude_2');
            $table->index('is_active', 'is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_landing_children');
    }
}
