<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerStyleCheckpointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_style_checkpoint', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->unsignedInteger('user_id');
            $table->integer('photo_id');
            $table->timestamps();
            $table->softDeletes();

            $table->index('designer_id', 'designer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_style_checkpoint');
    }
}
