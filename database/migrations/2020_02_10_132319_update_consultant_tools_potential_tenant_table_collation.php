<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class UpdateConsultantToolsPotentialTenantTableCollation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE consultant_tools_potential_tenant CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci");
        DB::statement("ALTER TABLE consultant_tools_potential_tenant MODIFY email VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE consultant_tools_potential_tenant CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        DB::statement("ALTER TABLE consultant_tools_potential_tenant MODIFY email VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
    }
}
