<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCodeOnAreaCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('area_city', function (Blueprint $table) {
            $table->string('code', 2)->after('name')->nullable();
            $table->boolean('is_generated')->after('code')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('area_city', function (Blueprint $table) {
            $table->dropColumn('is_generated');
            $table->dropColumn('code');
        });
    }
}
