<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveJoinedAsFromUserChatChannel extends Migration
{
    public function up()
    {
        Schema::table('user_chat_channel', function (Blueprint $table) {
            $table->dropIndex('room_id_user_id_joined_as');
            $table->dropColumn('joined_as');

            $table->index(['room_id', 'user_id'], 'room_id_user_id_index');
        });
    }

    public function down()
    {
        Schema::table('user_chat_channel', function (Blueprint $table) {
            $table->dropIndex('room_id_user_id_index');

            $table->enum('joined_as', ['tenant', 'owner', 'consultant'])->nullable(false);
            $table->index(['room_id', 'user_id', 'joined_as'], 'room_id_user_id_joined_as');
        });
    }
}
