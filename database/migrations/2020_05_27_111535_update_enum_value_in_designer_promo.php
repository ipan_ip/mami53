<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEnumValueInDesignerPromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE designer_promo MODIFY COLUMN verification ENUM('false', 'true', 'ignore') default 'false'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE designer_promo MODIFY COLUMN verification ENUM('false', 'true') default 'false'");
    }
}
