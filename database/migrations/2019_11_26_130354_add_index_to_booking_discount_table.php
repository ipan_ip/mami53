<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToBookingDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->index('designer_id', 'designer_id');
            $table->index('price_type', 'price_type');
            $table->index('is_active', 'is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->dropIndex('designer_id', 'designer_id');
            $table->dropIndex('price_type', 'price_type');
            $table->dropIndex('is_active', 'is_active');
        });
    }
}
