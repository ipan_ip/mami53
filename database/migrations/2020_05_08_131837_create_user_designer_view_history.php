<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDesignerViewHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_designer_view_history', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->comment('foreign key to user.id');
            $table->unsignedInteger('designer_id')->comment('foreign key to designer.id');
            $table->dateTime('viewed_at');
            $table->boolean('read')->default(false);

            $table->index('designer_id', 'designer_id');
            $table->index('user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_designer_view_history');
    }
}
