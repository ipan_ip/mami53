<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * DB Changelog 
 * https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/391053373/2020-04-14+Create+a+new+shadow+table+designer+hidden+thanos
 */
class CreateDesignerHiddenThanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_hidden_thanos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')
                ->nullable()
                ->comment('foreign key to designer.id');
            $table->boolean('snapped')
                ->default(true)
                ->comment('flagging status thanos, If its an ex or a currently snapped or reactivated');
            $table->json('log')
                ->nullable()
                ->comment('nullable json field to store possible temporary schema-less runtime info/log');
            $table->timestamps();

            $table->index(['designer_id', 'snapped'], 'designer_snapped');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_hidden_thanos');
    }
}
