<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user', function($table) {
            $table->String('contact_room_number')->nullable();
            $table->String('contact_gender')->nullable();
            $table->String('contact_parent_phone', 14)->nullable();
            $table->String('contact_parent_name', 100)->nullanble();
            $table->Integer('bill_date')->nullable();
            $table->Integer('date_expired')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user', function($table) {
            $table->dropColumn('contact_room_number');
            $table->dropColumn('contact_gender');
            $table->dropColumn('contact_parent_phone');
            $table->dropColumn('contact_parent_name');
            $table->dropColumn('bill_date');
            $table->dropColumn('date_expired');
        });
    }
}
