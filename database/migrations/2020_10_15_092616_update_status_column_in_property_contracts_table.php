<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusColumnInPropertyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            DB::statement("ALTER TABLE property_contracts MODIFY COLUMN status ENUM('active', 'inactive','terminated','cancelled') DEFAULT 'inactive' NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            DB::statement("ALTER TABLE property_contracts MODIFY COLUMN status ENUM('active', 'inactive') DEFAULT 'active' NOT NULL;");
        });
    }
}
