<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOnesignal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('notification_onesignal_queue'); //undoable
        Schema::dropIfExists('user_one_signal'); //undoable
        Schema::table('user_device', function (Blueprint $table) {
            $table->dropColumn('one_signal_push_token');
            $table->dropColumn('one_signal_player_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_device', function (Blueprint $table) {
            $table->string('one_signal_push_token', 255)->nullable();
            $table->string('one_signal_player_id', 255)->nullable();
        });
    }
}
