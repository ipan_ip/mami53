<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateDesignerReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_parent_id')->nullable();
            $table->integer('designer_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('report_type', 250)->nullable();
            $table->text('report_description')->nullable();
            $table->string('status', 50)->nullable()->default('new');
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('designer_id', 'designer_id');
            $table->index('user_id', 'user_id');
            $table->index('report_parent_id', 'report_parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_report');
    }
}
