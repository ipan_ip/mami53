<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationColumnToConsultantToolsPotentialTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            // Duration of contract for potential tenant
            $table->enum(
                'duration_unit',
                [
                    'day',
                    'week',
                    'month',
                    'year',
                    '3_month',
                    '6_month',
                ]
            )
            ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->dropColumn('duration_unit');
        });
    }
}
