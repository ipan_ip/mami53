<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomRegistrationMediaMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_registration_media_map', function (Blueprint $table) {
            $table->unsignedInteger('registration_id');
            $table->unsignedInteger('media_id');
            $table->unsignedTinyInteger('type')->length(1);
            $table->timestamps();

            $table->primary(['registration_id', 'media_id']);

            $table->foreign('registration_id', 'fk_registration_media')
                ->references('id')
                ->on('mamiroom_registration')
                ->onDelete('RESTRICT')
                ->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamiroom_registration_media_map', function (Blueprint $table) {
            $table->dropForeign('fk_registration_media');
        });

        Schema::dropIfExists('mamiroom_registration_media_map');
    }
}
