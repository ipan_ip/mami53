<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantActivityFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'consultant_activity_form',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('consultant_activity_funnel_id');
                $table->tinyInteger('stage');
                $table->string('name');
                $table->string('detail');
                $table->json('form_element');
                $table->timestamps();
                $table->softDeletes();
                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->unsignedInteger('deleted_by')->nullable();
                $table->index('consultant_activity_funnel_id');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_activity_form');
    }
}
