<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignerKostValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'designer_kost_value',
            function (Blueprint $table) {
                $table->unsignedInteger('designer_id');
                $table->unsignedInteger('kost_level_value_id');
                $table->unsignedInteger('created_by');
                $table->timestamps();

                $table->primary(
                    [
                        'designer_id',
                        'kost_level_value_id'
                    ],
                    'designer_kost_value_id'
                );

                $table->foreign('designer_id')->references('id')->on('designer');
                $table->foreign('kost_level_value_id')->references('id')->on('kost_level_value');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_kost_value');
    }
}
