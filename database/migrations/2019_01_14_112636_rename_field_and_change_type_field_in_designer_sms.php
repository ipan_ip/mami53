<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFieldAndChangeTypeFieldInDesignerSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('designer_sms', function (Blueprint $table) {
            $table->renameColumn('cities', 'city'); 
            $table->text('message')->nullable()->change();
        });

        Schema::table('designer_sms', function (Blueprint $table) {
            $table->text('city')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('designer_sms', function (Blueprint $table) {
            $table->renameColumn('city', 'cities'); 
            $table->string('message', 500)->nullable()->change();
        });

        Schema::table('designer_sms', function (Blueprint $table) {
            $table->string('cities', 500)->nullable()->change();
        });
    }
}
