<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVerificationAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verification_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('is_verify_phone_number')->nullable();
            $table->string('is_verify_email')->nullable();
            $table->string('is_verify_facebook')->nullable();
            $table->string('is_verify_identity_card')->nullable();
            $table->nullableTimestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_verification_account');
    }
}
