<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdAndAreaAndIsIndexedColumnOnLandingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_list', function (Blueprint $table) {
            $table->smallInteger('parent_id')->default(0);
            $table->string('area', 100)->nullable();
            $table->smallInteger('is_indexed')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_list', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('area');
            $table->dropColumn('is_indexed');
        });
    }
}
