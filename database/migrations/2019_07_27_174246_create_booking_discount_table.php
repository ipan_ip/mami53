<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->enum('price_type', ['daily', 'weekly', 'monthly', 'semiannually', 'annually', 'daily_usd', 'weekly_usd', 'monthly_usd', 'semiannually_usd', 'annually_usd'])->default('monthly');
            $table->string('ad_link', 100)->nullable();
            $table->enum('markup_type', ['percentage', 'nominal'])->default('percentage');
            $table->integer('markup_value')->default(0);
            $table->enum('discount_type', ['percentage', 'nominal'])->default('percentage');
            $table->integer('discount_value')->default(0);
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_discount');
    }
}
