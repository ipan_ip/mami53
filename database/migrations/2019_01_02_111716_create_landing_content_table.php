<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateLandingContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 190)->nullable();
            $table->string('slug', 190)->nullable();
            $table->string('type', 190)->default('konten');
            $table->text('content_open')->nullable();
            $table->text('content_hidden')->nullable();
            $table->string('button_label', 100)->nullable();
            $table->string('login_subtitle', 250)->nullable();
            $table->string('location_placeholder', 100)->nullable();
            $table->tinyInteger('need_login')->default(1);
            $table->unsignedInteger('redirect_id')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('title', 'title');
            $table->index('slug', 'slug');
            $table->index('type', 'type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_content');
    }
}
