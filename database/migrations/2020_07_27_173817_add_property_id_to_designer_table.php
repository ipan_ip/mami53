<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyIdToDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->integer('property_id')->nullable()->index('designer_property_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->dropIndex('designer_property_index');
            $table->dropColumn('property_id');
        });
    }
}
