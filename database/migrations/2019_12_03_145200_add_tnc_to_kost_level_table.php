<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTncToKostLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->text('tnc')->nullable();
            $table->unsignedInteger('join_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->dropColumn('tnc');
            $table->dropColumn('join_count');
        });
    }
}
