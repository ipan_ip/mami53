<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsFraudToMamipayContractInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->boolean('is_fraud')->default(false)->after('from_auto_extend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->dropColumn('is_fraud');
        });
    }
}
