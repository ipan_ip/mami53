<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerificatorAndRejectionColumnsInBookingOwnerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_owner_request', function (Blueprint $table) {
            $table->unsignedInteger('verificator_user_id')->nullable();
            $table->string('reject_reason', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_owner_request', function (Blueprint $table) {
            $table->dropColumn('verificator_user_id');
            $table->dropColumn('reject_reason');
        });
    }
}
