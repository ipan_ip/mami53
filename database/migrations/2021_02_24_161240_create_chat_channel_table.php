<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_channel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('global_id', 255)->nullable(false);
            $table->unsignedInteger('room_id')->nullable(false);
            $table->dateTime('last_message_at')->nullable(false);
            $table->dateTime('expired_at');
            $table->timestamps();

        });

        Schema::table('chat_channel', function (Blueprint $table) {
            $table->unique('global_id', 'global_id');
            $table->index('last_message_at', 'last_message_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_channel', function (Blueprint $table) {
            $table->dropIndex('last_message_at');
            $table->dropIndex('global_id');
        });

        Schema::dropIfExists('chat_channel');
    }
}
