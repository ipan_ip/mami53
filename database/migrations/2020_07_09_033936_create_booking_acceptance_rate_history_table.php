<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingAcceptanceRateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_log')->create('booking_acceptance_rate_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('booking_acceptance_rate_id');
            $table->string('column', 100);
            $table->text('original_value');
            $table->text('modified_value');
            $table->string('triggered_by', 100);
            $table->timestamps();

            $table->index('booking_acceptance_rate_id');
            $table->index('column');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_log')->dropIfExists('booking_acceptance_rate_history');
    }
}
