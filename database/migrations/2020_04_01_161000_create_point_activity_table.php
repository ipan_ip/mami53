<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 100);
            $table->string('name', 100);
            $table->string('target', 10);
            $table->timestamps();
            $table->softDeletes();

            $table->unique('key', 'key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_activity');
    }
}
