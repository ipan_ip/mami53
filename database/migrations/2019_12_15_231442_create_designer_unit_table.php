<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_unit', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('designer_type_id');
			$table->string('name', 255);
			$table->string('floor', 50)->nullable();
			$table->boolean('is_available')->default(1);
			$table->boolean('is_active')->default(0);
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_unit');
    }
}
