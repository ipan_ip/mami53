<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypePremiumCashbackHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_cashback_history', function(Blueprint $table) {
            $table->enum('type', ['topup', 'booking_commission'])->default('topup');

            $table->index('type', 'cashback_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_cahback_history', function(Blueprint $table) {
            $table->dropIndex('cashback_type');
            $table->dropColumn('type');
        });
    }
}
