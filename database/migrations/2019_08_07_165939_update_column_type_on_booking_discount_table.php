<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTypeOnBookingDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        // rename old column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->renameColumn('price_type','price_type_tmp');
        });
            
        // create new column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->string('price_type', 50)->default('monthly')->after('designer_id');
        });

        // moving datas
        $all = DB::table('booking_discount')->get();
        foreach($all as $item)
        {
            DB::table('booking_discount')->where('id', $item->id)->update(['price_type' => $item->price_type_tmp]);
        }

        // removing old column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->dropColumn('price_type_tmp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        // rename old column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->renameColumn('price_type','price_type_tmp');
        });
            
        // create new column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->enum('price_type', ['daily', 'weekly', 'monthly', 'quearterly', 'semiannually', 'annually', 'daily_usd', 'weekly_usd', 'monthly_usd', 'quearterly_usd', 'semiannually_usd', 'annually_usd'])->default('monthly')->after('designer_id');
        });

        // moving datas
        $all = DB::table('booking_discount')->get();
        foreach($all as $item)
        {
            DB::table('booking_discount')->where('id', $item->id)->update(['price_type' => $item->price_type_tmp]);
        }

        // removing old column
        Schema::table('booking_discount', function (Blueprint $table) {
            $table->dropColumn('price_type_tmp');
        });
    }
}
