<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRentCountTypeInBookingUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->string('rent_count_type')->after('stay_duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->dropColumn('rent_count_type');     
        });
    }
}
