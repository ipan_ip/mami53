<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromAutoExtendToMamipayContractInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_contract_invoice','from_auto_extend')){                
                $table->boolean('from_auto_extend')->nullable()->after('from_booking');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->dropColumn('from_auto_extend');
        });
    }
}
