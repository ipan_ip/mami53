<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashSaleAreaLandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_sale_area_landing', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('flash_sale_area_id');
            $table->unsignedInteger('landing_id');
            $table->string('added_by', 100);
            $table->timestamps();
            $table->softDeletes();

            $table->index('flash_sale_area_id');
            $table->index('landing_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_sale_area_landing');
    }
}
