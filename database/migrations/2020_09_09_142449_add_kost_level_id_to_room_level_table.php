<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKostLevelIdToRoomLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->unsignedInteger('kost_level_id')->nullable()->after('charge_invoice_type');
            $table->index('kost_level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->dropIndex(['kost_level_id']);
            $table->dropColumn('kost_level_id');
        });
    }
}
