<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomLevelKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->string('key', 64)->nullable()->after('name');

            $table->unique(['key'], 'room_level_key_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->dropUnique('room_level_key_index');
            
            $table->dropColumn('key');
        });
    }
}
