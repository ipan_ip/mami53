<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagToKostLevelMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kost_level_map', function (Blueprint $table) {
            $table->boolean('flag')->nullable();
            $table->unsignedInteger('flag_level_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kost_level_map', function (Blueprint $table) {
            $table->dropColumn('flag');
            $table->dropColumn('flag_level_id');
            $table->dropTimestamps();
        });
    }
}
