<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerStatusHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_status_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_owner_id');
            $table->foreign('designer_owner_id')->references('id')->on('designer_owner');
            $table->enum('old_status', ['draft1', 'draft2', 'add', 'verified', 'rejected', 'unverified', 'claim'])->nullable();
            $table->enum('new_status', ['draft1', 'draft2', 'add', 'verified', 'rejected', 'unverified', 'claim']);
            $table->timestamps();
            $table->unsignedInteger('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_status_history');
    }
}
