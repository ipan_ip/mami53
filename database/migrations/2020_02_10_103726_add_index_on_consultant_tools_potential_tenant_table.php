<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnConsultantToolsPotentialTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->index('email');
            $table->index('designer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->dropIndex(['designer_id']);
            $table->dropIndex(['email']);
        });
    }
}
