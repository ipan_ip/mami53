<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDesignerCreationFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_creation_flag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')->unique()->comment('foreign key to designer.id');
            $table->string('create_from', 50)->default('')->comment('creation source');
            $table->json('log')->nullable()->comment('schemaless kos creation log');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_creation_flag');
    }
}
