<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeyInMamipayAdditionalCostGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_additional_cost_group', function (Blueprint $table) {
            $table->unsignedInteger('owner')->nullable()->change();
            $table->unsignedInteger('designer')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_additional_cost_group', function (Blueprint $table) {
            $table->integer('owner')->nullable()->change();
            $table->integer('designer')->nullable()->change();
        });
    }
}
