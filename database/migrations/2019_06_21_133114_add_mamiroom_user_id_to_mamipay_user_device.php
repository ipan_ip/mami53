<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMamiroomUserIdToMamipayUserDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mamipay_user_device')) {
            Schema::table('mamipay_user_device', function (Blueprint $table) {
                if (!Schema::hasColumn('mamipay_user_device','mamiroom_owner_id')){                
                    $table->integer('mamiroom_user_id')->nullable()->after('user_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_user_device', function (Blueprint $table) {
            $table->dropColumn('mamiroom_user_id');  
        });
    }
}
