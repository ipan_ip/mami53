<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomLevelHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_level_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_level_id');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->enum('action', ['upgrade', 'downgrade']);
            $table->timestamps();

            $table->index('room_id', 'room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_level_history');
    }
}
