<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerAdsInteractionTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_ads_interaction_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('designer_id');
            $table->enum('type', ['like', 'click']);
            $table->enum('platform', ['web-desktop', 'web-mobile', 'android', 'ios']);
            $table->timestamps();

            $table->index('user_id', 'user_id');
            $table->index('designer_id', 'designer_id');
            $table->index('type', 'type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_ads_interaction_tracker');
    }
}
