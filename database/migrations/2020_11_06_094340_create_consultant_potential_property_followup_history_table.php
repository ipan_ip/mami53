<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantPotentialPropertyFollowupHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_potential_property_followup_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('potential_property_id');
            $table->unsignedInteger('designer_id')->nullable();
            $table->timestamp('new_at')->nullable();
            $table->timestamp('in_progress_at')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->timestamps();

            $table->index('potential_property_id', 'potential_property_id');
            $table->index('designer_id', 'designer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_potential_property_followup_history');
    }
}
