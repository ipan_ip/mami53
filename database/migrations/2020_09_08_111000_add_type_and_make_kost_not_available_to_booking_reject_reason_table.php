<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndMakeKostNotAvailableToBookingRejectReasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->enum('type', ['reject', 'cancel']);
            $table->boolean('make_kost_not_available')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('make_kost_not_available');
        });
    }
}
