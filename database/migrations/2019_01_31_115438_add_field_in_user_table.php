<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->string('marital_status')->nullable();
            $table->string('phone_number_additional')->nullable();
            $table->string('province')->nullable()->after('semester');
            $table->string('subdistrict')->nullable()->after('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('marital_status');
            $table->dropColumn('phone_number_additional');
            $table->dropColumn('province');
            $table->dropColumn('subdistrict');
        });
    }
}
