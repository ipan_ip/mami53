<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousePropertyMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_property_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media_type', 10)->nullable();
            $table->integer('house_property_id')->unsigned();
            $table->string('description', 200)->nullable();
            $table->string('upload_identifier', 30)->nullable();
            $table->string('upload_identifier_type')->nullable();
            $table->string('file_name', 255)->nullable();
            $table->string('file_path', 50)->nullable();
            $table->string('type', 30)->nullable();
            $table->string('media_format', 5)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_property_media');
    }
}
