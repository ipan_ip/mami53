<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerPriceFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_price_filter', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->integer('final_price_daily')->default(0);
            $table->integer('final_price_weekly')->default(0);
            $table->integer('final_price_monthly')->default(0);
            $table->integer('final_price_yearly')->default(0);
            $table->integer('final_price_quarterly')->default(0);
            $table->integer('final_price_semiannually')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('designer_id');
            $table->index('final_price_monthly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_price_filter');
    }
}
