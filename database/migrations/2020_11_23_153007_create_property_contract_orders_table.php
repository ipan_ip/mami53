<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyContractOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_contract_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 127);
            $table->string('status', 45);
            $table->decimal('amount', 18, 2);
            $table->date('scheduled_date');
            $table->bigInteger('owner_id');
            $table->bigInteger('property_contracts_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_contract_orders');
    }
}
