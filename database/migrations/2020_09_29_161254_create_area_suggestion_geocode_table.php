<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaSuggestionGeocodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'area_suggestion_geocode',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('suggestion_id');
                $table->text('requested_address');
                $table->boolean('is_cached')->default(false);
                $table->integer('place_id');
                $table->char('osm_type', 50);
                $table->char('category', 50);
                $table->char('type', 50);
                $table->text('display_name');
                $table->string('box');
                $table->geometry('original_geolocation')->nullable();
                $table->geometry('geolocation');
                $table->tinyInteger('expanded_from')->default(0);
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('suggestion_id')->references('id')->on('search_input_keyword_suggestion');

                $table->index('place_id');
                $table->index('osm_type');
                $table->index('category');
                $table->index('type');
                $table->index('expanded_from');
            }
        );

        // Add SPATIAL index to geometry columns
        DB::statement(
            'ALTER TABLE `area_suggestion_geocode` ADD SPATIAL INDEX area_suggestion_geocode_geolocation_index(`geolocation`)'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_suggestion_geocode');
    }
}
