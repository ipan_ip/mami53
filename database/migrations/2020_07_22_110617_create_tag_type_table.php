<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Entities\Room\Element\TagType;

class CreateTagTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_type', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tag_id');
            $table->enum('name', [
                'keyword',
                'emotion',
                'concern',
                'theme',
                'category',
                'gender',
                'period',
                'fac_room',
                'fac_bath',
                'fac_share',
                'fac_outside',
                'fac_park',
                'fac_near',
                'fac_project',
                'fac_price',
                'kos_rule',
                'service'
                ]);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tag_id');
        });

        //get the old data from table tag
        $oldTagData = DB::table('tag')
            ->select('id', 'type', 'created_at', 'updated_at', 'deleted_at')
            ->get();

        //put the old data to new table tag_type
        foreach ($oldTagData as $data){
            $newData = new TagType();
            $newData->tag_id = $data->id;
            $newData->name = $data->type;
            $newData->created_at = $data->created_at;
            $newData->updated_at = $data->updated_at;
            $newData->deleted_at = $data->deleted_at;
            $newData->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_type');
    }
}
