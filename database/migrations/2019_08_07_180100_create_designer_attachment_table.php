<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_attachment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id')->unique();
            $table->string('matterport_id')->nullable();
            $table->boolean('is_matterport_active')->default(false);
            $table->timestamps();
           
            $table->foreign('designer_id')->references('id')->on('designer')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_attachment');
    }
}
