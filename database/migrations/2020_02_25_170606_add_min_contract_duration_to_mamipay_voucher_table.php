<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinContractDurationToMamipayVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->string('min_contract_duration')->nullable()->after('user_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropColumn('min_contract_duration');
        });
    }
}
