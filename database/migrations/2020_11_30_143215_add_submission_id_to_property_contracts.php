<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmissionIdToPropertyContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->bigInteger('submission_id')->nullable()->index('contract_owner_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->dropColumn('submission_id');
        });
    }
}
