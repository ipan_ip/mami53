<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEarnableColumnInPointHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_history', function (Blueprint $table) {
            $table->string('earnable_type', 50)->nullable()->after('redeem_id');
            $table->unsignedInteger('earnable_id')->nullable()->after('earnable_type');

            $table->index(['user_id'], 'point_history_user_index');
            $table->index(['earnable_type', 'earnable_id'], 'point_history_earnable_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_history', function (Blueprint $table) {
            $table->dropIndex('point_history_earnable_index');
            $table->dropIndex('point_history_user_index');

            $table->dropColumn('earnable_type');
            $table->dropColumn('earnable_id');
        });
    }
}
