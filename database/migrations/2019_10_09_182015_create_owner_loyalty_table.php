<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerLoyaltyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_loyalty', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('importer_user_id');
            $table->date('data_date');
            $table->date('booking_activation_date');
            $table->smallInteger('total_listed_room');
            $table->smallInteger('total_booking_response');
            $table->smallInteger('total_booking_transaction');
            $table->smallInteger('total_review');
            $table->smallInteger('loyalty_booking_response');
            $table->smallInteger('loyalty_booking_transaction');
            $table->smallInteger('loyalty_review');
            $table->smallInteger('total');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_loyalty');
    }
}
