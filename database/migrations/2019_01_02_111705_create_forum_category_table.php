<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateForumCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('name', 190);
            $table->string('slug', 190);
            $table->text('description')->nullable();
            $table->unsignedInteger('ordinal')->default(1000);
            $table->unsignedInteger('question_count')->default(0);
            $table->unsignedInteger('answer_count')->default(0);
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('parent_id', 'parent_id');
            $table->index('name', 'name');
            $table->index('slug', 'slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_category');
    }
}
