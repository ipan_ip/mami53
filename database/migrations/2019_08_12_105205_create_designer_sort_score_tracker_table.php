<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerSortScoreTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_sort_score_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->tinyInteger('hostility')->default(0);
            $table->boolean('can_booking')->default(false);
            $table->boolean('is_saldo_on')->default(false);
            $table->boolean('is_premium')->default(false);
            $table->boolean('has_photo')->default(false);
            $table->integer('available_room_count')->default(0);
            $table->integer('score')->default(0);
            $table->timestamps();

            $table->foreign('designer_id')->references('id')->on('designer')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_sort_score_tracker');
    }
}
