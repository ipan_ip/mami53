<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTenantIdToMamipayDepositTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_deposit_transaction','tenant_id')){                
                $table->integer('tenant_id')->nullable()->after('user_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->dropColumn('tenant_id');
        });
    }
}
