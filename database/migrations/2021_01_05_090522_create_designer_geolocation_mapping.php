<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerGeolocationMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_geolocation_mapping', function (Blueprint $table) {
            $table->unsignedInteger('designer_id');
            $table->unsignedInteger('area_suggestion_geocode_id');
            $table->timestamps();

            // Table's Primary Key (composite index)
            $table->primary(
                [
                    'designer_id',
                    'area_suggestion_geocode_id'
                ],
                'designer_geolocation_mapping_id'
            );

            // Table's Foreign Keys
            $table->foreign('designer_id')->references('id')->on('designer');
            $table->foreign('area_suggestion_geocode_id')->references('id')->on('area_suggestion_geocode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_geolocation_mapping');
    }
}
