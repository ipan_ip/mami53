<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateToVisitColumnToConsultantPotentialOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_potential_owner', function (Blueprint $table) {
            $table->date('date_to_visit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_potential_owner', function (Blueprint $table) {
            $table->dropColumn('date_to_visit');
        });
    }
}
