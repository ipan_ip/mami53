<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class RemoveUnusedPushNotifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('log_notification_availability', '__log_notification_availability');
        Schema::rename('owner_update_code', '__owner_update_code');
        Schema::rename('notification_queue', '__notification_queue');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('__notification_queue', 'notification_queue');
        Schema::rename('__owner_update_code', 'owner_update_code');
        Schema::rename('__log_notification_availability', 'log_notification_availability');
    }
}
