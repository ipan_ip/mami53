<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiumPlusUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premium_plus_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->unsignedInteger('designer_id');
            $table->unsignedInteger('consultant_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->dateTime('request_date')->nullable();
            $table->dateTime('activation_date')->nullable();
            $table->integer('active_period')->default(0);
            $table->integer('premium_rate')->default(0);
            $table->integer('static_rate')->default(0);
            $table->integer('registered_room')->default(0);
            $table->integer('guaranteed_room')->default(0);
            $table->integer('releasable_room')->default(0);
            $table->integer('total_monthly_premium')->default(0);
            $table->integer('grace_period')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('phone_number', 'phone_number');
            $table->index('designer_id', 'designer_id');
            $table->index('consultant_id', 'consultant_id');
            $table->index('user_id', 'user_id');
            $table->index('grace_period', 'grace_period');
            $table->index('email', 'email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premium_plus_user');
    }
}
