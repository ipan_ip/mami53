<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnHousePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('house_property', function (Blueprint $table) {
            $table->smallInteger('building_year')->nullable();
            $table->smallInteger('is_available')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_property', function (Blueprint $table) {
            $table->dropColumn('building_year');
            $table->dropColumn('is_available');
        });
    }
}
