<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdentifierUuidIndexOnMamipayUserDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_user_device', function (Blueprint $table) {
            $table->index('identifier', 'mamipay_user_device_identifier_index');
            $table->index('uuid', 'mamipay_user_device_uuid_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_user_device', function (Blueprint $table) {
            $table->dropIndex('mamipay_user_device_identifier_index');
            $table->dropIndex('mamipay_user_device_uuid_index');
        });
    }
}
