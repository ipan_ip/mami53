<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsAffectingAcceptanceOnBookingRejectReasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->boolean('is_affecting_acceptance')->default(false)->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->dropColumn('is_affecting_acceptance');
        });
    }
}
