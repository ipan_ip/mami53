<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexForPremiumPromoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_promote', function (Blueprint $table) {
            $table->index(['for'], 'for_index');
            $table->index(['designer_id'], 'designer_id_index');
            $table->index(['premium_request_id'], 'premium_request_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_promote', function (Blueprint $table) {
            $table->dropIndex('for_index');
            $table->dropIndex('designer_id_index');
            $table->dropIndex('premium_request_id_index');
        });
    }
}
