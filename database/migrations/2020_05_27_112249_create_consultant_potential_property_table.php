<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantPotentialPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_potential_property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('owner_name');
            $table->string('owner_phone');
            $table->string('owner_email');
            $table->unsignedInteger('media_id');
            $table->enum('role', [
                'admin',
                'supply',
                'demand'
            ]);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('consultant_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_potential_property');
    }
}
