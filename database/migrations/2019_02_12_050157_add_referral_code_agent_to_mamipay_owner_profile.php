<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferralCodeAgentToMamipayOwnerProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_owner_profile','referral_code_agent')){                
                $table->string('referral_code_agent')->nullable()->after('bank_account_city');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            $table->dropColumn('referral_code_agent');
        });
    }
}
