<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnScoreVersionToDesignerSortScoreTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
            $table->integer('score_version')->default(0)->after('score');
            $table->boolean('is_bar')->default(0)->after('is_flash_sale');
            $table->boolean('has_good_booking_performance')->default(0)->after('is_flash_sale');
            $table->boolean('is_basic')->default(0)->after('is_flash_sale');

            $table->index('designer_id', 'designer_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
            $table->dropIndex('designer_id_index');

            $table->dropColumn('score_version');
            $table->dropColumn('is_bar');
            $table->dropColumn('booking_performance');
        });
    }
}
