<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateForumAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('thread_id');
            $table->unsignedInteger('user_id');
            $table->text('answer');
            $table->integer('vote_up')->default(0);
            $table->integer('vote_down')->default(0);
            $table->tinyInteger('best_answer')->default(0);
            $table->tinyInteger('is_active')->default(1);
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('thread_id', 'thread_id');
            $table->index('user_id', 'user_id');
            $table->index('is_active', 'is_active');
        });

        \DB::statement("ALTER TABLE `forum_answer` ADD FULLTEXT INDEX `asnwer` (`answer`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_answer');
    }
}
