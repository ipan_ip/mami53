<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMamiroomUserToMamipayUserDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mamipay_user_device')) {
            Schema::table('mamipay_user_device', function (Blueprint $table) {
                if (!Schema::hasColumn('mamipay_user_device','mamiroom_user_phone_number')){                
                    $table->string('mamiroom_user_phone_number')->nullable()->after('user_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_user_device', function (Blueprint $table) {
            $table->dropColumn('mamiroom_user_phone_number');  
        });
    }
}
