<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlashSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'flash_sale',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100);
                $table->dateTime('start_time')->nullable();
                $table->dateTime('end_time')->nullable();
                $table->unsignedInteger('photo_id')->nullable();
                $table->boolean('is_active')->default(false);
                $table->string('created_by', 100);
                $table->timestamps();
                $table->softDeletes();

                $table->index('name');
                $table->index('start_time');
                $table->index('end_time');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_sale');
    }
}
