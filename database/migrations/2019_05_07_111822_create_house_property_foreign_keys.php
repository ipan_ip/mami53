<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousePropertyForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('house_property_tag', function ($table) {
            $table->foreign('house_property_id', 'FK_house_property_tag')->references('id')->on('house_property')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->foreign('tag_id', 'FK_villa_tag_tag')->references('id')->on('tag')->onDelete('RESTRICT')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_property_tag', function (Blueprint $table) {
            $table->dropForeign('FK_house_property_tag');
            $table->dropForeign('FK_villa_tag_tag');
        });
    }
}
