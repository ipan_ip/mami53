<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnPremiumPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_payment', function(Blueprint $table) {
            $table->string('source', 100)->nullable();
            $table->unsignedInteger('source_id')->nullable();
            $table->integer('version')->nullable()->default(1); // default 1 to handle backward compatibility

            $table->index('source', 'source');
            $table->index('source_id', 'source_id');
            $table->index('user_id', 'user_id');
            $table->index('premium_request_id', 'premium_request_id');
            $table->index('transaction_status', 'transaction_status');
            $table->index('created_at', 'created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_payment', function(Blueprint $table) {
            $table->dropColumn('source');
            $table->dropColumn('source_id');
            $table->dropColumn('version');

            $table->dropIndex('user_id');
            $table->dropIndex('premium_request_id');
            $table->dropIndex('transaction_status');
            $table->dropIndex('created_at');
        });
    }
}
