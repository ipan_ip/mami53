<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingUserFlashSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_user_flash_sale', function (Blueprint $table) {
            $table->unsignedInteger('booking_user_id')
                ->comment('foreign key to booking_user.id');

            $table->unsignedInteger('flash_sale_id')
                ->comment('foreign key to flash_sales.id');

            $table->index('booking_user_id', 'booking_user_id');
            $table->index('flash_sale_id', 'flash_sale_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_user_flash_sale');
    }
}
