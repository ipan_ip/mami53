<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayBillingRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mamipay_billing_rule')) {
            Schema::create('mamipay_billing_rule', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('tenant_id');
                $table->integer('designer_id');
                $table->boolean('fixed_billing')->default(0);
                $table->integer('billing_date')->nullable();
                $table->float('first_amount', 9, 0);
                $table->float('deposit_amount', 9, 0);
                $table->float('fine_amount', 9, 0);       
                $table->integer('fine_maximum_length');       
                $table->string('fine_duration_type');       
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_billing_rule');
    }
}
