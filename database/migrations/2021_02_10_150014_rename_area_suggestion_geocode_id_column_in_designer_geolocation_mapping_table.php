<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAreaSuggestionGeocodeIdColumnInDesignerGeolocationMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // To prevent "foreign key constraint fails" error
        Schema::disableForeignKeyConstraints();

        Schema::table('designer_geolocation_mapping', function (Blueprint $table) {
            $table->dropForeign(['area_suggestion_geocode_id']);
            $table->renameColumn('area_suggestion_geocode_id', 'area_geolocation_id');

            $table->foreign('area_geolocation_id')->references('id')->on('area_geolocation');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // To prevent "foreign key constraint fails" error
        Schema::disableForeignKeyConstraints();

        Schema::table('designer_geolocation_mapping', function (Blueprint $table) {
            $table->dropForeign(['area_geolocation_id']);
            $table->renameColumn('area_geolocation_id', 'area_suggestion_geocode_id');

            $table->foreign('area_suggestion_geocode_id')->references('id')->on('area_suggestion_geocode');
        });

        Schema::enableForeignKeyConstraints();
    }
}
