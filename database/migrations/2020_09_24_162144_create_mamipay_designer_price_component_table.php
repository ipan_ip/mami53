<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayDesignerPriceComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_designer_price_component', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->enum('component',
                [
                    'additional',
                    'fine',
                    'deposit',
                    'dp'
                ]
            )->default('additional');
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->integer('price')->default(0);
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->softDeletes();

            // create index
            $table->index('designer_id', 'designer_id');
            $table->index('component', 'component');
            $table->index('is_active', 'is_active');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_designer_price_component');
    }
}
