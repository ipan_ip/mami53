<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateSlugTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slug', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designer_id')->nullable();
            $table->string('slug', 255)->nullable();
            $table->enum('status', ['true', 'false'])->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index(['designer_id', 'slug'], 'designer_id_slug');
            $table->index('designer_id', 'designer_id');
            $table->index('slug', 'slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slug');
    }
}
