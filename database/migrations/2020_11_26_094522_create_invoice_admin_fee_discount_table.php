<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceAdminFeeDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_admin_fee_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->decimal('amount', 18, 2);
            $table->unsignedInteger('kost_level_id');
            $table->boolean('is_booking_transaction')->default(0);
            $table->boolean('is_nonbooking_transaction')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_admin_fee_discount');
    }
}
