<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyContractDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_contract_detail', function (Blueprint $table) {
            $table->unsignedInteger('property_contract_id');
            $table->unsignedInteger('property_id');
            $table->timestamps();

            $table->primary(['property_contract_id', 'property_id'], 'property_contract_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_contract_detail');
    }
}
