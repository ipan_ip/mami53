<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduledDateColumnToConsultantToolsPotentialTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            // Record when potential tenant will enter the kost
            $table->date('scheduled_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_tools_potential_tenant', function (Blueprint $table) {
            $table->dropColumn('scheduled_date');
        });
    }
}
