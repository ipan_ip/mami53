<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignerFacilityTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designer_facility', function (Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('designer_id');
			$table->unsignedInteger('tag_id');
			$table->unsignedInteger('creator_id');
			$table->unsignedInteger('photo_id');
			$table->string('name', 50)->nullable();
			$table->text('note')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('designer_facility');
	}
}
