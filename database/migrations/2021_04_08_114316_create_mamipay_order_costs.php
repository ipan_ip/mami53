<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayOrderCosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_order_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->string('order_type', 127);
            $table->string('cost_title', 80)->nullable();
            $table->decimal('cost_value', 18, 2);
            $table->string('cost_type', 51)->index('cost_type_index');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['order_type', 'order_id'], 'order_type_order_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_order_costs', function (Blueprint $table) {
            $table->dropIndex('cost_type_index');
        });

        Schema::dropIfExists('mamipay_order_costs');
    }
}
