<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlashSaleSchemaToBookingUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->integer('price')->after('total_price')->nullable();
            $table->integer('mamikos_discount_value')->after('price')->nullable();
            $table->integer('owner_discount_value')->after('mamikos_discount_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('mamikos_discount_value');
            $table->dropColumn('owner_discount_value');
        });
    }
}
