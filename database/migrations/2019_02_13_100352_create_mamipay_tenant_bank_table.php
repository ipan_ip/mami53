<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayTenantBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_tenant_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('name');
            $table->string('bank');
            $table->string('bank_code');
            $table->string('bank_number');
            $table->string('primary', 11)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_tenant_bank');
    }
}
