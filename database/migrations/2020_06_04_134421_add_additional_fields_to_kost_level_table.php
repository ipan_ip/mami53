<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToKostLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * APPROVAL DOCS : https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/501416051/2020-06-02+Add+column+change+column+type+in+kost+level+table
     */
    public function up()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->enum('charge_invoice_type', ['all', 'per_contract', 'per_tenant'])
                ->default('all')->after('charging_fee');
            
            $table->boolean('charge_for_owner')->default(false)->after('charging_fee');
            $table->boolean('charge_for_consultant')->default(false)->after('charging_fee');
            $table->boolean('charge_for_booking')->default(false)->after('charging_fee');

            $table->enum('charging_type', ['percentage', 'amount'])
                ->default('percentage')->after('charging_fee');

            $table->decimal('charging_fee',16,2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->dropColumn('charge_invoice_type');
            $table->dropColumn('charge_for_owner');
            $table->dropColumn('charge_for_consultant');
            $table->dropColumn('charge_for_booking');
            $table->dropColumn('charging_type');
            $table->unsignedInteger('charging_fee')->change();
        });
    }
}
