<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_filter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->double('latitude_1')->nullable();
            $table->double('longitude_1')->nullable();
            $table->double('latitude_2')->nullable();
            $table->double('longitude_2')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_filter');
    }
}
