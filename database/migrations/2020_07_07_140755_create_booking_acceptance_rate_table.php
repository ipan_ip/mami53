<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingAcceptanceRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_acceptance_rate', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
            $table->integer('rate')->default(0);
            $table->integer('average_time')->default(0);
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('designer_id');
            $table->index('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_acceptance_rate');
    }
}
