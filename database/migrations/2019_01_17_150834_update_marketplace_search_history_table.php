<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMarketplaceSearchHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('marketplace_search_history', function (Blueprint $table) {
            $table->renameColumn('filters', 'filter');
            $table->renameColumn('titikTengah', 'center_point');
            $table->renameColumn('namaKota', 'city_name');
        });

        Schema::rename('marketplace_search_history', 'log_marketplace_search_history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename('log_marketplace_search_history', 'marketplace_search_history');
        
        Schema::table('marketplace_search_history', function (Blueprint $table) {
            $table->renameColumn('filter', 'filters');
            $table->renameColumn('center_point', 'titikTengah');
            $table->renameColumn('city_name', 'namaKota');
        });
    }
}
