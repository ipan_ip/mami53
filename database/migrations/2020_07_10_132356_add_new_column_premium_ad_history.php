<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnPremiumAdHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_ad_history', function($table) {
            $table->enum('platform', ['web-desktop', 'web-mobile', 'android', 'ios'])->nullable();

            $table->index('created_at', 'premium_ad_history_created_at_index');
            $table->index('date', 'premium_ad_history_date_index');
            $table->index('designer_id', 'premium_ad_history_designer_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_ad_history', function($table) {
            $table->dropColumn('platform');            

            $table->dropIndex('premium_ad_history_created_at_index');
            $table->dropIndex('premium_ad_history_date_index');
            $table->dropIndex('premium_ad_history_designer_id_index');
        });
    }
}
