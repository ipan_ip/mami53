<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableForDesignerFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("rename_queue", "designer_rename_queue");
        Schema::rename("user_input_reward", "designer_user_input_reward");

        Schema::rename("label", "designer_label");
        Schema::table('designer_label', function (Blueprint $table) {
            $table->renameColumn('tags', 'tag');         
        });

        Schema::rename("recommendation", "designer_recommendation");
        Schema::rename("match", "designer_match");

        Schema::rename("style", "designer_style");
        Schema::rename("style_review", "designer_review_style");
        Schema::table('designer_review_style', function (Blueprint $table) {
            $table->renameColumn('reviews_id', 'review_id');         
        });

        Schema::rename("app_popup", "designer_app_popup");
        Schema::rename("ads_display_tracker", "designer_ads_display_tracker");
        Schema::rename("contact_room", "designer_contact");
        Schema::rename("room_available_tracker", "designer_availability_tracker");
        Schema::rename("user_ad_view", "designer_ad_view");
        Schema::rename("v_fav", "v_favorite");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("designer_rename_queue", "rename_queue");
        Schema::rename("designer_user_input_reward", "user_input_reward");
        Schema::rename("designer_label", "label");
        Schema::table('label', function (Blueprint $table) {
            $table->renameColumn('tag', 'tags');         
        });

        Schema::rename("designer_recommendation", "recommendation");
        Schema::rename("designer_match", "match");

        Schema::rename("designer_style", "style");
        Schema::rename("designer_review_style", "review_style");
        Schema::table('review_style', function (Blueprint $table) {
            $table->renameColumn('review_id', 'reviews_id');         
        });

        Schema::rename("designer_app_popup", "app_popup");
        Schema::rename("designer_ads_display_tracker", "ads_display_tracker");
        Schema::rename("designer_contact", "contact_room");
        Schema::rename("designer_availability_tracker", "room_available_tracker");
        Schema::rename("designer_ad_view", "user_ad_view");
        Schema::rename("v_favorite", "v_fav");
    }
}
