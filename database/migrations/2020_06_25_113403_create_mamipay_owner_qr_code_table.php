<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayOwnerQrCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_owner_qr_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_user_id')->unique();
            $table->char('code', 7)->unique();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->dateTime('last_usage_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_owner_qr_code');
    }
}
