<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingMetaOgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_meta_og', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->text('keywords');
            $table->string('image', 255)->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->index('is_active', 'is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_meta_og');
    }
}
