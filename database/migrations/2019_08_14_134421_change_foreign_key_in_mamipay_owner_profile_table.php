<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeyInMamipayOwnerProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            $table->unsignedInteger('photo_profile_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            $table->integer('photo_profile_id')->nullable()->change();
        });
    }
}
