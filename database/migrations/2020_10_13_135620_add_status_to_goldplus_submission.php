<?php

use App\Entities\GoldPlus\Enums\SubmissionStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToGoldplusSubmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goldplus_submission', function (Blueprint $table) {
            $table->string('status', 15)->default(SubmissionStatus::NEW);
            $table->index('status', 'gp_submission_status_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goldplus_submission', function (Blueprint $table) {
            $table->dropIndex('gp_submission_status_idx');
            $table->dropColumn('status');
        });
    }
}
