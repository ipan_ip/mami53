<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAlarmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('alarm', function (Blueprint $table) {
            $table->text('note')->change();
            $table->text('schedule')->change();
            $table->text('filters')->change();
            $table->text('location')->change();
            $table->renameColumn('filters', 'filter');
        });

        Schema::rename('alarm', 'notification_alarm');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename('notification_alarm', 'alarm');

        Schema::table('alarm', function (Blueprint $table) {
            $table->string('note', 500)->change();
            $table->string('schedule', 500)->change();
            $table->string('filter', 500)->change();
            $table->string('location', 500)->change();
            $table->renameColumn('filter', 'filters');
        });

    }
}
