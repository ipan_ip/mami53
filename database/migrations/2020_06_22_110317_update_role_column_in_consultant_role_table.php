<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateRoleColumnInConsultantRoleTable extends Migration
{
    /**
     *  Run the migrations.
     *
     *  DB Statement is used because doctrine dbal does not
     *  support modification of enum column.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_role', function (Blueprint $table) {
            DB::statement("ALTER TABLE consultant_role MODIFY COLUMN role ENUM('admin','supply','demand','admin_chat');");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_role', function (Blueprint $table) {
            DB::statement("ALTER TABLE consultant_role MODIFY COLUMN role ENUM('admin','supply','demand');");
        });
    }
}
