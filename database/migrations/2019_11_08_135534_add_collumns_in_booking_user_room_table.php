<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsInBookingUserRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_user_room', function (Blueprint $table) {
            $table->string('kost_name', 255)->after('checkout_date')->nullable();
            $table->string('kost_type', 100)->after('kost_name')->nullable();
            $table->string('number', 50)->after('kost_type')->nullable();
            $table->string('floor', 100)->after('number')->nullable();
            $table->integer('price')->after('floor')->nullable();
            $table->text('facilities')->after('price')->nullable();
            $table->unsignedInteger('designer_id')->after('booking_designer_room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_user_room', function (Blueprint $table) {
            $table->dropColumn('designer_id');
            $table->dropColumn('kost_name');
            $table->dropColumn('kost_type');
            $table->dropColumn('number');
            $table->dropColumn('floor');
            $table->dropColumn('price');
            $table->dropColumn('facilities');
        });
    }
}
