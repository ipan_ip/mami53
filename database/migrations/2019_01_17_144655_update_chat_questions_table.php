<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::rename("chat_questions", "chat_question");
        Schema::table('chat_question', function (Blueprint $table) {
            $table->renameColumn('chat_untuk', 'chat_for');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('chat_question', function (Blueprint $table) {
            $table->renameColumn('chat_for', 'chat_untuk');
        });
        Schema::rename("chat_question", "chat_questions");
    }
}
