<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->index('property_contract_property_index');
            $table->integer('property_level_id')->index('property_contract_level_index');
            $table->dateTime('joined_at');
            $table->dateTime('ended_at')->nullable();
            $table->integer('assigned_by')->index('property_contract_assigned_index');
            $table->integer('ended_by')->nullable()->index('property_contract_ended_index');
            $table->integer('total_package');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_contracts');
    }
}
