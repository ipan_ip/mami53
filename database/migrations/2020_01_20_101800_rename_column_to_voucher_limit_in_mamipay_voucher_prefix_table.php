<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnToVoucherLimitInMamipayVoucherPrefixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher_prefix', function (Blueprint $table) {
            $table->renameColumn('user_limit', 'voucher_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher_prefix', function (Blueprint $table) {
            $table->renameColumn('voucher_limit', 'user_limit');
        });
    }
}
