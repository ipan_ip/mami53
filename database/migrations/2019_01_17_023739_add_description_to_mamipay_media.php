<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToMamipayMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_media', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_media','description')){
                $table->string('description',200)->after('type');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_media', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
