<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFromBookingInMamipayContractInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_contract_invoice','from_booking')){                
                $table->integer('from_booking')->nullable()->after('transferred_at');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice', function (Blueprint $table) {
            $table->dropColumn('from_booking');            
        });
    }
}
