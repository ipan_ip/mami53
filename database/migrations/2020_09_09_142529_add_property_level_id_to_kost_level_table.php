<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyLevelIdToKostLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->unsignedInteger('property_level_id')->nullable();
            $table->index('property_level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->dropIndex(['property_level_id']);
            $table->dropColumn('property_level_id');
        });
    }
}
