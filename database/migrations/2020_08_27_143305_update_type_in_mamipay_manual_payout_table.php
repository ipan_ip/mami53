<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTypeInMamipayManualPayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * APPROVAL: https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/715227308/2020-08-27+-+PAY+Add+new+value+of+type+enum+in+mamipay+manual+payout+table
     */
    public function up()
    {
        DB::statement("ALTER TABLE mamipay_manual_payout MODIFY COLUMN type ".
            "ENUM ('disbursement', 'refund', 'refund_outside', 'refund_charging', 'payout_tenant', 'payout_owner')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE mamipay_manual_payout MODIFY COLUMN type ".
            "ENUM ('disbursement', 'refund', 'refund_outside')");
    }
}
