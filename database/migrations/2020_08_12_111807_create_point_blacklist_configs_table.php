<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointBlacklistConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_blacklist_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usertype', 100);
            $table->string('value', 100);
            $table->boolean('is_blacklisted')->default(1);
            $table->timestamps();
            
            $table->unique(['usertype', 'value']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_blacklist_configs');
    }
}
