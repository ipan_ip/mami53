<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayAdditionalCostGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mamipay_additional_cost_group')) {
            Schema::create('mamipay_additional_cost_group', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('owner');
                $table->integer('designer');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_additional_cost_group');
    }
}
