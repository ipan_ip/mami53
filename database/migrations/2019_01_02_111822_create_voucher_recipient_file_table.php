<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateVoucherRecipientFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_recipient_file', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voucher_program_id')->nullable();
            $table->string('file_path', 255)->nullable();
            $table->string('file_name', 255)->nullable();
            $table->unsignedInteger('rows_read')->nullable();
            $table->string('status', 50)->nullable();
            $table->text('options')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('voucher_program_id', 'voucher_program_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_recipient_file');
    }
}
