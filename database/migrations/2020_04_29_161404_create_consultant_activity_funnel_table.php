<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantActivityFunnelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'consultant_activity_funnel',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->enum('consultant_role', ['admin', 'supply', 'demand']);
                $table->string('related_table');
                $table->tinyInteger('total_stage');
                $table->timestamps();
                $table->softDeletes();
                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->unsignedInteger('deleted_by')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_activity_funnel');
    }
}
