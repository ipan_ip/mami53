<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConsultantPotentialPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->string('address');
            $table->string('area_city');
            $table->string('province');
            $table->unsignedInteger('total_room');
            $table->enum('offered_product', ['gp', 'bbk']);
            $table->unsignedInteger('consultant_potential_owner_id');
            $table->string('remark');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by')->nullable();

            $table->index('consultant_potential_owner_id', 'potential_owner_index');
            $table->index('media_id');

            $table->dropColumn('owner_name');
            $table->dropColumn('owner_phone');
            $table->dropColumn('owner_email');
            $table->dropColumn('role');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('area_city');
            $table->dropColumn('province');
            $table->dropColumn('total_room');
            $table->dropColumn('offered_product');
            $table->dropColumn('consultant_potential_owner_id');
            $table->dropColumn('remark');

            $table->string('owner_name');
            $table->string('owner_phone');
            $table->string('owner_email');
            $table->enum('role', [
                'admin',
                'supply',
                'demand'
            ]);
            $table->unsignedInteger('user_id')->nullable();
            $table->dropIndex(['media_id']);
        });
    }
}
