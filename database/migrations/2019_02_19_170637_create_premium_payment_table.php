<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiumPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premium_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('expired_at')->nullable();
            $table->string('name', 30)->nullable();
            $table->string('order_id', 150)->nullable();
            $table->string('biller_code', 30)->nullable();
            $table->string('bill_key', 30)->nullable();
            $table->integer('total')->nullable();
            $table->string('transaction_id', 255)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('premium_request_id')->nullable();
            $table->string('payment_type', 30)->nullable();
            $table->string('transaction_status', 50)->nullable();
            $table->string('payment_code', 100)->nullable();
            $table->string('pdf_url', 255)->nullable();
            $table->string('note', 255)->nullable();
            $table->Timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premium_payment');
    }
}
