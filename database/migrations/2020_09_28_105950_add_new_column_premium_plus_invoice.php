<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnPremiumPlusInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_plus_invoice', function(Blueprint $table) {
            $table->enum('notif_status', ['send', 'unsend'])->default('unsend');

            $table->index('notif_status', 'notif_status');
            $table->index('status', 'status');
            $table->index('due_date', 'due_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_plus_invoice', function(Blueprint $table) {
            $table->dropIndex('notif_status');
            $table->dropIndex('status');
            $table->dropIndex('due_date');

            $table->dropColumn('notif_status');
        });
    }
}
