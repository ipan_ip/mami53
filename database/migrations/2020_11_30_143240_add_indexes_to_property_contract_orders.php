<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToPropertyContractOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->index(['owner_id'], 'contract_order_owner_index');
            $table->index(['property_contracts_id'], 'contract_order_property_contracts_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contract_orders', function (Blueprint $table) {
            $table->dropIndex('contract_order_owner_index');
            $table->dropIndex('contract_order_property_contracts_index');
        });
    }
}
