<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_page', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('slider_id');
            $table->unsignedInteger('media_id');
            $table->unsignedInteger('user_id');
            $table->string('title', 50);
            $table->text('content');
            $table->string('redirect_label', 50)->nullable();
            $table->string('redirect_link', 50)->nullable();
            $table->tinyInteger('order');
            $table->boolean('is_cover')->default(false);
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_page');
    }
}
