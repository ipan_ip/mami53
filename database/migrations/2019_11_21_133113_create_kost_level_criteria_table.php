<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKostLevelCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kost_level_criteria', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('level_id');
            $table->text('text');
            $table->timestamps();
            $table->softDeletes();

            $table->index('level_id', 'level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_level_criteria');
    }
}
