<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableInFeatureMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        
        Schema::rename("photo_by", "media_photo_by");
        Schema::rename("photo_old", "media_photo_old");
        Schema::rename("photo_complete_queue", "media_photo_complete_queue");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        
        Schema::rename("media_photo_by", "photo_by");
        Schema::rename("media_photo_old", "photo_old");
        Schema::rename("media_photo_complete_queue", "photo_complete_queue");
    }
}
