<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMamipayVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->string('voucher_code', 64)->change();

            $table->index(['voucher_code'], 'voucher_code_index');
            $table->index(['prefix_id'], 'prefix_id_index');
            $table->index(['public_campaign_id'], 'public_campaign_id_index');
            $table->index(['start_date', 'end_date'], 'start_date_end_date_index');
            $table->index(['is_active'], 'is_active_index');
            $table->index(['is_testing'], 'is_testing_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropIndex('voucher_code_index');
        });

        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropIndex('prefix_id_index');
            $table->dropIndex('public_campaign_id_index');
            $table->dropIndex('start_date_end_date_index');
            $table->dropIndex('is_active_index');
            $table->dropIndex('is_testing_index');

            $table->text('voucher_code')->change();
        });
    }
}
