<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevelToDesignerSortScoreTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
			$table->boolean('is_super_level')->default(false)->after('is_guarantee');
			$table->boolean('is_oke_level')->default(false)->after('is_super_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
			$table->dropColumn('is_super_level');
			$table->dropColumn('is_oke_level');
        });
    }
}
