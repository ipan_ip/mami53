<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddTitleAndDescriptionColumnIntoPointActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_activity', function (Blueprint $table) {
            $table->string('title', 100)->nullable()->after('target');
            $table->text('description')->nullable()->after('title');
            $table->unsignedInteger('sequence')->default(0)->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_activity', function (Blueprint $table) {
            $table->dropcolumn('title');
            $table->dropcolumn('description');
            $table->dropColumn('sequence');
        });
    }
}
