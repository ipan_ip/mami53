<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayDesignerPriceComponentAdditionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_designer_price_component_additional', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mamipay_designer_price_component_id');
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // create index
            $table->index('mamipay_designer_price_component_id', 'mamipay_designer_price_component_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_designer_price_component_additional');
    }
}
