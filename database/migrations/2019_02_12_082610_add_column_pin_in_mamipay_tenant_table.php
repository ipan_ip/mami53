<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPinInMamipayTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_tenant','pin')){                
                $table->string('pin')->nullable()->after('va_number_hana');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropColumn('pin');            
        });
    }
}
