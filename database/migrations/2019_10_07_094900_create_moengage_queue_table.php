<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoengageQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_moengage_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('moengage_id', 100)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('message', 250)->nullable();
            $table->string('text_button_one', 100)->nullable();
            $table->string('url_button_one', 250)->nullable();
            $table->string('text_button_two', 100)->nullable();
            $table->string('url_button_two', 250)->nullable();
            $table->string('image', 250)->nullable();
            $table->string('status', 50)->default('new');
            $table->string('handler', 250)->nullable();
            $table->text('params')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('user_id', 'user_id');
            $table->index('moengage_id', 'moengage_id');
            $table->index('status', 'status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_moengage_queue');
    }
}
