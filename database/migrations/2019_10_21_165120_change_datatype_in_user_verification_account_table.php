<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeInUserVerificationAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->boolean('is_verify_phone_number')->default(false)->change();
            $table->boolean('is_verify_email')->default(false)->change();
            $table->boolean('is_verify_facebook')->default(false)->change();
            $table->string('verify_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_verification_account', function (Blueprint $table) {
            $table->string('is_verify_phone_number', 255)->change();
            $table->string('is_verify_email', 255)->change();
            $table->string('is_verify_email', 255)->change();
            $table->dropColumn('verify_link');
        });
    }
}
