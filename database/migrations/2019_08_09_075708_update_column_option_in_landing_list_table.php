<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnOptionInLandingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        // NOTE: 
        // Using native method: "$table->mediumText('option')->change()" isn't working at all! Looks odd!
        // Therefore, this workaround is required to get the objective
        // 
        // Ref: https://github.com/laravel/framework/issues/21847
        */

        // rename old column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->renameColumn('option','option_tmp');
        });
            
        // create new column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->mediumText('option')->after('description');
        });

        // moving datas
        $all = DB::table('landing_list')->get();
        foreach($all as $item)
        {
            DB::table('landing_list')->where('id', $item->id)->update(['option' => $item->option_tmp]);
        }

        // removing old column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->dropColumn('option_tmp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // rename old column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->renameColumn('option','option_tmp');
        });
            
        // create new column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->text('option')->after('description');
        });

        // moving datas
        $all = DB::table('landing_list')->get();
        foreach($all as $item)
        {
            DB::table('landing_list')->where('id', $item->id)->update(['option' => $item->option_tmp]);
        }

        // removing old column
        Schema::table('landing_list', function (Blueprint $table) {
            $table->dropColumn('option_tmp');
        });
    }
}
