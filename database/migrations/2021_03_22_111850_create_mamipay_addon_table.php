<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayAddonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_addon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 80);
            $table->string('short_description', 150)->nullable();
            $table->decimal('initial_price', 18, 2);
            $table->text('note')->nullable();
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_addon');
    }
}
