<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampaignTeamAndUsageLimitToMamipayVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->string('campaign_team')->default('other')->after('voucher_code');
            $table->string('limit')->nullable()->after('voucher_min_amount');
            $table->string('limit_type')->nullable()->after('voucher_min_amount');    
            $table->boolean('for_pelunasan')->default(false)->after('for_recurring');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropColumn('campaign_team');
            $table->dropColumn('limit');
            $table->dropColumn('limit_type');
            $table->dropColumn('for_pelunasan');
        });
    }
}
