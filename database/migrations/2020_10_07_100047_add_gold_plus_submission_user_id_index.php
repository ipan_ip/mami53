<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoldPlusSubmissionUserIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goldplus_submission', function (Blueprint $table) {
            $table->index('user_id', 'user_id');
            $table->index('created_at', 'created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goldplus_submission', function (Blueprint $table) {
            $table->dropIndex('created_at');
            $table->dropIndex('user_id');
        });
    }
}
