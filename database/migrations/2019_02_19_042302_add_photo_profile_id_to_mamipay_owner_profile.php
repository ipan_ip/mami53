<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoProfileIdToMamipayOwnerProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_owner_profile','photo_profile_id')){                
                $table->integer('photo_profile_id')->nullable()->after('room_count');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_owner_profile', function (Blueprint $table) {
            $table->dropColumn('photo_profile_id');
        });
    }
}
