<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChargingNameToRoomLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * Approval Docs : https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/718111593/2020-08-31+-+PAY+Add+new+column+charging+name+in+room+level+table
     */
    public function up()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->string('charging_name', 100)->nullable()->after('charging_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_level', function (Blueprint $table) {
            $table->dropColumn('charging_name');
        });
    }
}
