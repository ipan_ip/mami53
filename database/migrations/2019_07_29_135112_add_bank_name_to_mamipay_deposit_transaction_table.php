<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankNameToMamipayDepositTransactionTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_deposit_transaction','bank_name')){                
                $table->string('bank_name')->nullable()->after('account_holder');
            }    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->dropColumn('bank_name');
        });
    }
}
