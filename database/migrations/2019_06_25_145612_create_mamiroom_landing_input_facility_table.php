<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomLandingInputFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_landing_input_facility', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mamiroom_landing_input_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->foreign('mamiroom_landing_input_id', 'FK_mamiroom_input_id')->references('id')->on('mamiroom_landing_input')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->foreign('tag_id', 'FK_mamiroom_tag_tag_id')->references('id')->on('tag')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamiroom_landing_input_facility');
    }
}
