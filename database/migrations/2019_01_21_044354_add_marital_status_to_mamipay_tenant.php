<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaritalStatusToMamipayTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('mamipay_tenant','marital_status')){
            Schema::table('mamipay_tenant', function (Blueprint $table) {
                $table->string('marital_status')->nullable()->after('photo_document_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropColumn('marital_status');
        });
    }
}
