<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomRegistrationTagMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_registration_tag_map', function (Blueprint $table) {
            $table->unsignedInteger('registration_id');
            $table->unsignedInteger('tag_id')->nullable();
            $table->timestamps();

            $table->primary(['registration_id', 'tag_id']);

            $table->foreign('registration_id', 'fk_registration_tag')
                ->references('id')
                ->on('mamiroom_registration')
                ->onDelete('RESTRICT')
                ->onUpdate('RESTRICT');

            $table->foreign('tag_id', 'fk_tag')
                ->references('id')
                ->on('tag')
                ->onDelete('RESTRICT')
                ->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamiroom_registration_tag_map', function (Blueprint $table) {
            $table->dropForeign('fk_registration_tag');
            $table->dropForeign('fk_tag');
        });

        Schema::dropIfExists('mamiroom_registration_tag_map');
    }
}
