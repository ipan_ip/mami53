<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateUserDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_device', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('model', 125)->nullable();
            $table->string('email', 60)->nullable();
            $table->integer('user_id')->nullable();
            $table->string('identifier', 50)->nullable();
            $table->string('uuid', 255)->nullable();
            $table->enum('platform', ['android', 'ios'])->nullable();
            $table->unsignedInteger('platform_version_code_old')->nullable();
            $table->string('platform_version_code', 11)->nullable();
            $table->string('device_token', 255)->nullable();
            $table->string('app_notif_token', 255)->nullable();
            $table->string('one_signal_push_token', 255)->nullable();
            $table->string('one_signal_player_id', 255)->nullable();
            $table->enum('app_notif_all', ['true', 'false'])->nullable()->default('true');
            $table->enum('app_notif_status_changed', ['true', 'false'])->nullable()->default('true');
            $table->enum('app_notif_by_reference', ['true', 'false'])->nullable()->default('true');
            $table->unsignedInteger('app_version_code')->nullable();
            $table->integer('app_install_count')->nullable();
            $table->integer('app_update_count')->nullable()->default(0);
            $table->enum('enable_app_popup', ['true', 'false'])->nullable()->default('true');
            $table->enum('device_last_login', ['true', 'false'])->nullable()->default('true');
            $table->enum('login_status', ['not_login', 'skip', 'returning_skip', 'login'])->nullable()->default('not_login');
            $table->timestamp('last_rate')->nullable();
            $table->smallInteger('birth')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->tinyInteger('last_rent_type')->nullable();
            $table->integer('last_alarm_id')->nullable();
            $table->softDeletes();

            $table->index('device_token', 'device_token');
            $table->index('deleted_at', 'deleted_at');
            $table->index('identifier', 'identifier');
            $table->index('uuid', 'uuid');
            $table->index('platform', 'platform');
            $table->index(['identifier', 'uuid', 'platform'], 'identifier_uuid_platform');
            $table->index('user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_device');
    }
}
