<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayDepositTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deposit_id');
            $table->string('transaction_id');
            $table->integer('user_id')->nullable();
            $table->string('account_number');
            $table->float('amount', 9, 0);
            $table->string('provider');
            $table->text('remark');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_deposit_transaction');
    }
}
