<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPartnerColomnAtKostLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->boolean('is_partner')->default(false);
            $table->index('is_partner', 'is_partner_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kost_level', function (Blueprint $table) {
            $table->dropIndex('is_partner_index');
            $table->dropColumn('is_partner');
        });
    }
}
