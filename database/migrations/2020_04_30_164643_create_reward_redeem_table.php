<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardRedeemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_redeem', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('public_id')->index();
            $table->unsignedInteger('reward_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->string('status', 25);
            $table->string('notes', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_redeem');
    }
}
