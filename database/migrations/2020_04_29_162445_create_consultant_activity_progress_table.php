<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantActivityProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'consultant_activity_progress',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('consultant_id');
                $table->unsignedInteger('consultant_activity_funnel_id');
                $table->string('progressable_type');
                $table->unsignedInteger('progressable_id');
                $table->tinyInteger('current_stage');
                $table->timestamps();
                $table->softDeletes();
                $table->index('consultant_id');
                $table->index('consultant_activity_funnel_id');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_activity_progress');
    }
}
