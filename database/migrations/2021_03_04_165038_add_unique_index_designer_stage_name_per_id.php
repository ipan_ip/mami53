<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexDesignerStageNamePerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_data_stage_status', function (Blueprint $table) {
            $table->unique(['designer_id', 'name'], 'designer_id_name_unique_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_data_stage_status', function (Blueprint $table) {
            $table->dropIndex('designer_id_name_unique_index');
        });
    }
}
