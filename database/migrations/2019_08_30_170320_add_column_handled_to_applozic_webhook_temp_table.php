<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHandledToApplozicWebhookTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applozic_webhook_temp', function (Blueprint $table) {
            $table->boolean('handled')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applozic_webhook_temp', function (Blueprint $table) {
            $table->dropColumn('handled');     
        });
    }
}
