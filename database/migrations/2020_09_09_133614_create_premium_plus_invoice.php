<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiumPlusInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premium_plus_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('premium_plus_user_id'); // reference to premium_plus_user
            $table->string('shortlink', 50);
            $table->string('invoice_number', 100)->nullable();
            $table->string('name', 255)->nullable();
            $table->decimal('amount', 16, 0);
            $table->enum('status', ['paid', 'unpaid']);
            $table->timestamp('paid_at')->nullable();
            $table->decimal('total_paid_amount', 16, 0)->nullable();
            $table->timestamp('due_date')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index('premium_plus_user_id', 'premium_plus_user_id');
            $table->index('shortlink', 'shortlink');
            $table->index('name', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premium_plus_invoice');
    }
}
