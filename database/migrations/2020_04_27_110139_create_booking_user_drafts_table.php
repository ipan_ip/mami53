<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * DB Changelog
 * https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/415465622/Draft+2020-04-24+Create+a+new+table+booking+user+drafts
 */
class CreateBookingUserDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_user_drafts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')->comment('foreign key to designer.id');
            $table->unsignedInteger('user_id')->comment('foreign key to user.id');
            $table->unsignedInteger('designer_owner_id')->nullable()->comment('foreign key to designer_owner.id');
            $table->string('rent_count_type')->nullable();
            $table->integer('duration')->nullable()->unsigned();
            $table->date('checkin')->nullable();
            $table->date('checkout')->nullable();
            $table->string('tenant_name')->nullable();
            $table->string('tenant_phone')->nullable();
            $table->string('tenant_job')->nullable();
            $table->text('tenant_introduction')->nullable();
            $table->string('tenant_work_place')->nullable();
            $table->text('tenant_description')->nullable();
            $table->enum('tenant_gender', ['male', 'female'])->nullable();
            $table->integer('total_renter_count')->nullable()->unsigned();
            $table->boolean('is_married')->default(false);
            $table->enum('status', ['draft_created', 'already_booking']);
            $table->boolean('read')->default(false);
            $table->string('session_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('designer_id', 'designer_id');
            $table->index('user_id', 'user_id');
            $table->index('designer_owner_id', 'designer_owner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_user_drafts');
    }
}
