<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddDesignerRoomIdIntoMamipayContractTypeKost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_type_kost', function (Blueprint $table) {
            $table->unsignedBigInteger('designer_room_id')
                ->nullable()
                ->comment('foreign key to designer_room.id')
                ->after('room_number');

            $table->index('designer_room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_type_kost', function (Blueprint $table) {
            $table->dropIndex(['designer_room_id']);
            $table->dropColumn('designer_room_id');
        });
    }
}
