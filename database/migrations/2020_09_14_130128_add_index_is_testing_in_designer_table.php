<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexIsTestingInDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'designer',
            function (Blueprint $table) {
                $table->index('is_testing');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'designer',
            function (Blueprint $table) {
                $table->dropIndex(
                    [
                        'is_testing'
                    ]
                );
            }
        );
    }
}
