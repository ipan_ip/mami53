<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateReadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('read', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('device_id')->nullable();
            $table->unsignedInteger('designer_id')->nullable();
            $table->enum('status', ['true', 'false']);
            $table->enum('type', ['view', 'ads'])->default('view');
            $table->integer('count')->default(1);
            $table->integer('bookmark_id')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('device_id', 'device_id');
            $table->index('user_id', 'user_id');
            $table->index('designer_id', 'designer_id');
            $table->index('status', 'status');
            $table->index('deleted_at', 'deleted_at');
            $table->index('type', 'type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('read');
    }
}
