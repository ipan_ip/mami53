<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

const TABLE_NAME = 'designer_unique_code';
const INDEX_NAME = 'designer_unique_code_designer_id_index';
const COLUMN_NAME = 'designer_id';

class AddDesignerIdIndexInDesignerUniqueCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            TABLE_NAME,
            function (Blueprint $table) {
                if (!$this->doesIndexExist(TABLE_NAME, INDEX_NAME)) {
                    $table->index([COLUMN_NAME]);
                }
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            TABLE_NAME,
            function (Blueprint $table) {
                if ($this->doesIndexExist(TABLE_NAME, INDEX_NAME)) {
                    $table->dropIndex([COLUMN_NAME]);
                }
            }
        );
    }

    /**
     * @param string $tableName
     * @param string $indexName
     * @return bool
     */
    private function doesIndexExist(string $tableName, string $indexName): bool
    {
        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        $indexesFound = $sm->listTableIndexes($tableName);

        return array_key_exists($indexName, $indexesFound);
    }
}
