<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFieldAndTableInReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("reviews", "designer_review");
        Schema::rename("reviews_reply", "designer_review_reply");

        Schema::table('designer_review', function (Blueprint $table) {
            $table->renameColumn('room_facilities', 'room_facility');
            $table->renameColumn('public_facilities', 'public_facility');
            // $table->renameColumn('disadvantages', 'disadvantage');
            // $table->renameColumn('advantages', 'advantage');            
        });

        Schema::table('designer_review_reply', function (Blueprint $table) {
            $table->renameColumn('reviews_id', 'review_id');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::rename("designer_review", "reviews");
        Schema::rename("designer_review_reply", "reviews_reply");

        Schema::table('designer_review', function (Blueprint $table) {
            $table->renameColumn('room_facility', 'room_facilities');
            $table->renameColumn('public_facility', 'public_facilities');
            // $table->renameColumn('disadvantages', 'disadvantage');
            // $table->renameColumn('advantages', 'advantage');            
        });

        Schema::table('designer_review_reply', function (Blueprint $table) {
            $table->renameColumn('review_id', 'reviews_id');         
        });
    }
}
