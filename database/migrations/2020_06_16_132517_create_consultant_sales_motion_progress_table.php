<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantSalesMotionProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant_sales_motion_progress', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('consultant_id');
            $table->index('consultant_id');

            $table->unsignedInteger('consultant_sales_motion_id');
            $table->index('consultant_sales_motion_id', 'progress_to_sales_motion_index');

            $table->unsignedInteger('media_id')->nullable();
            $table->index('media_id');

            $table->string('progressable_type');

            $table->unsignedInteger('progressable_id');
            $table->index('progressable_id');

            $table->text('remark');
            $table->enum('status', [
                'deal',
                'interested',
                'not_interested'
            ]);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_sales_motion_progress');
    }
}
