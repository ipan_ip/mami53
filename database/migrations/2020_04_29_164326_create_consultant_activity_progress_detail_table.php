<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultantActivityProgressDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'consultant_activity_progress_detail',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('consultant_activity_progress_id');
                $table->unsignedInteger('consultant_activity_form_id');
                $table->json('value');
                $table->timestamps();
                $table->softDeletes();
                $table->index('consultant_activity_progress_id','progress_id');
                $table->index('consultant_activity_form_id','form_id');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant_activity_progress_detail');
    }
}
