<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexIntoMamipayVoucherUsage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher_usage', function (Blueprint $table) {
            $table->index('source_id', 'mamipay_voucher_usage_source_id_index');
            $table->index('voucher_id', 'mamipay_voucher_usage_voucher_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher_usage', function (Blueprint $table) {
            $table->dropIndex('mamipay_voucher_usage_source_id_index');
            $table->dropIndex('mamipay_voucher_usage_voucher_id_index');
        });
    }
}
