<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddLimitDailyIntoMamipayVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->unsignedInteger('limit_daily')->default(0)->after('limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher', function (Blueprint $table) {
            $table->dropColumn('limit_daily');
        });
    }
}
