<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnInPremiumPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('premium_package', function (Blueprint $table) {
            $table->renameColumn('features', 'feature');
            $table->renameColumn('days_count', 'total_day');
            $table->renameColumn('views', 'view');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('premium_package', function (Blueprint $table) {
            $table->renameColumn('feature', 'features');
            $table->renameColumn('total_day', 'days_count');
            $table->renameColumn('view', 'views');
        });
    }
}
