<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_type', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('designer_id');
			$table->string('name', 255);
			$table->tinyInteger('maximum_occupancy');
			$table->string('tenant_type', 10)->default('0');
			$table->string('room_size', 10)->nullable();
			$table->tinyInteger('total_room');
			$table->boolean('is_available')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_type');
    }
}
