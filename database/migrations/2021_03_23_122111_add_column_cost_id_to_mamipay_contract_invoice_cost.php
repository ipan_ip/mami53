<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCostIdToMamipayContractInvoiceCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_contract_invoice_cost', function (Blueprint $table) {
            $table->unsignedInteger('cost_id')->nullable()->after('cost_type');

            $table->string('cost_title', 80)->change();
            $table->string('cost_type', 20)->change();

            $table->index('cost_id', 'cost_id_index');
            $table->index('cost_type', 'cost_type_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_contract_invoice_cost', function (Blueprint $table) {
            $table->dropColumn('cost_id');

            $table->string('cost_title', 500)->change();
            $table->string('cost_type', 255)->change();

            $table->dropIndex('cost_id_index');
            $table->dropIndex('cost_type_index');
        });
    }
}
