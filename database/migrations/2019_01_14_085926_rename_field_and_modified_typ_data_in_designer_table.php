<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFieldAndModifiedTypDataInDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('designer', function (Blueprint $table) {
            $table->text('remarks')->nullable()->change();
            $table->renameColumn('remarks', 'remark');
            
            $table->text('price_remark')->nullable()->change();
            $table->text('address')->nullable()->change();            
            $table->text('fac_room_other')->nullable()->change();            
            $table->text('fac_bath_other')->nullable()->change();            
            $table->text('fac_share_other')->nullable()->change();            
            $table->text('fac_near_other')->nullable()->change();            
            $table->text('payment_duration')->nullable()->change();            
            $table->text('description')->nullable()->change();            

            $table->integer('review_count')->nullable();
            $table->float('review_rating')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('designer', function (Blueprint $table) {
            $table->string('remark', 500)->nullable()->change();
            $table->renameColumn('remark', 'remarks');

            $table->string('price_remark', 500)->nullable()->change();
            $table->string('address', 500)->nullable()->change();            
            $table->string('fac_room_other', 500)->nullable()->change();            
            $table->string('fac_bath_other', 500)->nullable()->change();            
            $table->string('fac_share_other', 500)->nullable()->change();            
            $table->string('fac_near_other', 5000)->nullable()->change();            
            $table->string('payment_duration', 500)->nullable()->change();            
            $table->string('description', 100)->nullable()->change();   

            $table->dropColumn(['review_count', 'review_rating']);
        });
    }
}
