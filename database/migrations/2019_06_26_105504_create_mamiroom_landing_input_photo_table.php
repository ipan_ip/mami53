<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomLandingInputPhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_landing_input_photo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mamiroom_landing_input_id')->unsigned()->nullable();
            $table->string('photo', 250)->nullable();
            $table->foreign('mamiroom_landing_input_id', 'FK_mamiroom_landing_id')->references('id')->on('mamiroom_landing_input')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamiroom_landing_input_photo');
    }
}
