<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointTargetColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->enum('target', ['owner', 'tenant'])->default('owner')->after('expiry_unit');

            $table->index('target', 'point_target_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->dropIndex('point_target_index');
            
            $table->dropColumn('target');
        });
    }
}
