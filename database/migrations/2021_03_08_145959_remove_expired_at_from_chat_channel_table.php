<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveExpiredAtFromChatChannelTable extends Migration
{
    public function up()
    {
        Schema::table('chat_channel', function (Blueprint $table) {
            $table->dropColumn('expired_at');
        });
    }

    public function down()
    {
        Schema::table('chat_channel', function (Blueprint $table) {
            $table->dateTime('expired_at')->nullable();
        });
    }
}
