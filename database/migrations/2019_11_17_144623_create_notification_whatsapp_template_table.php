<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationWhatsappTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_whatsapp_template', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('creator_id');
            $table->string('name', 50);
            $table->text('content');
			$table->string('target_user', 10)->default('owner');
			$table->boolean('also_send_sms')->default(false);
            $table->enum('type', ['triggered', 'scheduled'])->default('triggered');
			$table->string('on_event', 100)->nullable();
			$table->string('schedule_period', 20)->nullable();
            $table->time('schedule_time')->nullable();
            $table->date('schedule_date')->nullable();
            $table->datetime('milestone')->nullable();
			$table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_whatsapp_template');
    }
}
