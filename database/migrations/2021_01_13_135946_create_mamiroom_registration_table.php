<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamiroomRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamiroom_registration', function (Blueprint $table) {
            $table->increments('id');
            $table->string('owner_name', 50);
            $table->string('owner_phone_number', 30);
            $table->text('kebutuhan');
            $table->string('province', 40);
            $table->string('city', 100);
            $table->string('subdistrict', 100);
            $table->text('catatan_alamat');
            $table->string('kost_name', 100);
            $table->unsignedInteger('total_room')->default(0);
            $table->unsignedInteger('available_room')->default(0);
            $table->unsignedInteger('price_monthly')->default(0);
            $table->unsignedTinyInteger('ac')->length(1);
            $table->unsignedTinyInteger('bathroom')->length(1);
            $table->unsignedTinyInteger('wifi')->length(1);
            $table->enum('fac_lainnya', [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamiroom_registration');
    }
}
