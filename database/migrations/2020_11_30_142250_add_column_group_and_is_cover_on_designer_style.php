<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGroupAndIsCoverOnDesignerStyle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_style', function (Blueprint $table) {
            $table->string('group', 30)->after('photo_id')->nullable();
            $table->boolean('is_cover')->after('group')->default(false);
            $table->index(['designer_id', 'group'], 'designer_style_designer_id_group_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_style', function (Blueprint $table) {
            $table->dropIndex('designer_style_designer_id_group_index');
            $table->dropColumn('is_cover');
            $table->dropColumn('group');
        });
    }
}
