<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationWhatsappReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_whatsapp_reports', function (Blueprint $table) {
            $table->increments('id');
			$table->string('message_id', 50)->unique();
			$table->unsignedInteger('template_id');
			$table->text('content');
			$table->enum('type', ['scheduled', 'triggered', 'testing'])->default('triggered');
			$table->string('to', 20);
			$table->integer('status_group_id')->unsigned();
			$table->string('status_group_name', 20);
			$table->integer('status_id')->unsigned();
			$table->string('status_name', 20);
			$table->text('status_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_whatsapp_reports');
    }
}
