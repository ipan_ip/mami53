<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeyInMamipayBillingRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_billing_rule', function (Blueprint $table) {
            $table->unsignedInteger('tenant_id')->nullable()->change();
            $table->unsignedInteger('room_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_billing_rule', function (Blueprint $table) {
            $table->integer('tenant_id')->nullable()->change();
            $table->integer('room_id')->nullable()->change();
        });
    }
}
