<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayAdditionalCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mamipay_additional_cost')) {
            Schema::create('mamipay_additional_cost', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('group_id')->nullable();
                $table->string('parent_type',50);
                $table->integer('parent_id');
                $table->string('field_title',500);
                $table->float('field_value',9,0);
                $table->integer('sort_order');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_additional_cost');
    }
}
