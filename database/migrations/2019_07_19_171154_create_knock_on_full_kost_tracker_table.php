<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnockOnFullKostTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knock_on_full_kost_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('designer_id');
            $table->enum('purpose', ['nomor-pemilik'])->nullable()->default('nomor-pemilik');
            $table->timestamps();

            $table->index('user_id');
            $table->index('designer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knock_on_full_kost_tracker');
    }
}
