<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHousePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('house_property', function($table) {
            $table->integer('is_furnished')->nullable()->default(0);
            $table->integer('is_nego')->nullable()->default(0);
            $table->integer('floor')->nullable();
            $table->integer('expired_phone')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_property', function($table) {
            $table->dropColumn('is_furnished');
            $table->dropColumn('is_nego');
            $table->dropColumn('floor');
            $table->dropColumn('expired_phone');
        });
    }
}
