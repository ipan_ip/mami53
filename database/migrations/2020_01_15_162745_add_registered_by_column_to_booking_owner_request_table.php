<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegisteredByColumnToBookingOwnerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_owner_request', function (Blueprint $table) {
            $table->enum('registered_by', ['system', 'admin', 'consultant'])->default('system');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_owner_request', function (Blueprint $table) {
            $table->dropColumn('registered_by');
        });
    }
}
