<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFailedAtToMamipayManualPayout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->dateTime('failed_at')->nullable()->after('transferred_at');
            $table->string('failed_reason', 150)->nullable()->after('failed_at');
            $table->index('invoice_id', 'manual_payout_invoice_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_manual_payout', function (Blueprint $table) {
            $table->dropColumn('failed_at');
            $table->dropColumn('failed_reason');
            $table->dropIndex('manual_payout_invoice_id_index');
        });
    }
}
