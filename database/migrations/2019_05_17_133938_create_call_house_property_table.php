<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallHousePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_house_property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('add_from', 15)->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('house_property_id')->nullable();
            $table->unsignedInteger('device_id')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('note', 500)->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('email', 200)->nullable();
            $table->enum('status', ['true', 'false']);
            $table->integer('chat_admin_id')->nullable();
            $table->integer('chat_group_id')->nullable();
            $table->string('reply_status', 7)->nullable()->default('false');
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->index('house_property_id');
            $table->index('created_at', 'created_at');
            $table->index('chat_group_id', 'chat_group_id');
            $table->index('user_id', 'user_id');
            $table->index('device_id', 'device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_house_property');
    }
}
