<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayWebhookLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mamipay_webhook_log')) {
            Schema::create('mamipay_webhook_log', function (Blueprint $table) {
                $table->increments('id');
                $table->string('transaction_id')->nullable();
                $table->string('provider')->nullable();
                $table->longText('request')->nullable();
                $table->longText('decrypted_request')->nullable();
                $table->boolean('handled')->default(false);
                $table->dateTime('handled_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_webhook_log');
    }
}
