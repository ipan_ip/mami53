<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCollumnInUserSearchHistoryTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('user_search_history_temp', function (Blueprint $table) {
            $table->renameColumn('filters', 'filter');
            $table->renameColumn('titikTengah', 'center_point');
            $table->renameColumn('namaKota', 'city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('user_search_history_temp', function (Blueprint $table) {
            $table->renameColumn('filter', 'filters');
            $table->renameColumn('center_point', 'titikTengah');
            $table->renameColumn('city', 'namaKota');
        });
    }
}
