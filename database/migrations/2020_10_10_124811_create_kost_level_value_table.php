<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostLevelValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'kost_level_value',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 50);
                $table->text('description')->nullable();
                $table->unsignedInteger('image_id');
                $table->unsignedInteger('small_image_id');
                $table->unsignedInteger('created_by');
                $table->timestamps();
                $table->softDeletes();
            }
        );

        DB::statement("ALTER TABLE `kost_level_value` ADD FULLTEXT INDEX `name` (`name`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_level_value');
    }
}
