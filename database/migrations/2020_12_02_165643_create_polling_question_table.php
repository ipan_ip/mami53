<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollingQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polling_question', function (Blueprint $table) {
            $typeList = ['alphanumeric', 'numeric', 'boolean', 'checklist', 'selectionlist'];

            $table->increments('id');
            $table->unsignedInteger('polling_id');
            $table->string('question');
            $table->enum('type', $typeList)->default('alphanumeric');
            $table->unsignedInteger('sequence')->default(0);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->index('polling_id', 'polling_question_polling_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polling_question');
    }
}
