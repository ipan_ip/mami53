<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTargetConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_target_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_id');
            $table->string('type', 50);
            $table->string('value', 50);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('reward', function (Blueprint $table) {
            $table->enum('user_target', ['all', 'owner', 'tenant'])->default('all')->after('howto');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_target_configs');

        Schema::table('reward', function (Blueprint $table) {
            $table->dropColumn('user_target');
        });
    }
}
