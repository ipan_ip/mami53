<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->enum('upload_identifier_type', ['user_id', 'device_id', 'android_id', 'ios_id', 'admin_ip_address'])->nullable();
            $table->text('file_name')->nullable();
            $table->text('file_path')->nullable();
            $table->enum('type', ['photo_profile', 'e_ktp', 'passport', 'sim'])->nullable();
            $table->enum('from', ['user_profile', 'booking'])->nullable();
            $table->enum('server_location', ['macbook-pro', 'server-live'])->nullable();
            $table->enum('media_format', ['jpg', 'gif', 'mp3', 'png', 'jpeg', 'bmp'])->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_media');
    }
}
