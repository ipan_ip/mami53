<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSquarepantsDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("squarepants_data", "agent_call_data");
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('agent_call_data', function (Blueprint $table) {
            $table->renameColumn('agent_id', 'agent_call_app_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('agent_call_data', function (Blueprint $table) {
            $table->renameColumn('agent_call_app_id', 'agent_id');
        });
        Schema::rename("agent_call_data", "squarepants_data");
    }
}
