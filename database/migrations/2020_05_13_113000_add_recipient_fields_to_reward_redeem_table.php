<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecipientFieldsToRewardRedeemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reward_redeem', function (Blueprint $table) {
            $table->string('recipient_name', 25)->nullable()->after('notes');
            $table->string('recipient_phone', 25)->nullable()->after('recipient_name');
            $table->string('recipient_notes', 100)->nullable()->after('recipient_phone');
            $table->text('recipient_data')->nullable()->after('recipient_notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reward_redeem', function (Blueprint $table) {
            $table->dropColumn('recipient_name');
            $table->dropColumn('recipient_phone');
            $table->dropColumn('recipient_notes');
            $table->dropColumn('recipient_data');
        });
    }
}
