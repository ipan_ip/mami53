<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBaseAmountToMamipayBillingRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mamipay_billing_rule')) {
            Schema::table('mamipay_billing_rule', function (Blueprint $table) {
                if (!Schema::hasColumn('mamipay_billing_rule','base_amount')){                
                    $table->float('base_amount', 9, 0)->nullable()->after('billing_date');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_billing_rule', function (Blueprint $table) {
            $table->dropColumn('base_amount');
        });
    }
}
