<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingHousePropertyCategoryRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_house_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('landing_house_id')->unsigned();
            $table->integer('landing_category')->unsigned();
            $table->softDeletes();
            $table->nullableTimestamps();
            $table->index(['landing_house_id', 'landing_category']);
            $table->foreign('landing_house_id', 'FK_landing_house_id')->references('id')->on('landing_house_property')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->foreign('landing_category', 'FK_landing_category')->references('id')->on('landing_category')->onDelete('RESTRICT')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_house_relation');
    }
}
