<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntoDesignerSortScoreTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
            $table->boolean('is_flash_sale')->default(0)->after('has_available_room');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_sort_score_tracker', function (Blueprint $table) {
            $table->dropColumn('is_flash_sale');
        });
    }
}
