<?php
 
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateBulkyForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alarm_email', function ($table) {
            $table->foreign('designer_id', 'FK_alarm_email_designer')->references('id')->on('designer')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('designer_verify', function ($table) {
            $table->foreign('designer_id', 'FK_designer_verify_designer')->references('id')->on('designer')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('landing_tag', function ($table) {
            $table->foreign('landing_id', 'FK_landing_tag_landing')->references('id')->on('landing')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
            $table->foreign('tag_id', 'FK_landing_tag_tag')->references('id')->on('tag')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('landing', function ($table) {
            $table->foreign('parent_id', 'FK_landing_landing')->references('id')->on('landing')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('notif', function ($table) {
            $table->foreign('photo_id', 'FK_notif_media')->references('id')->on('media')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('notif_queue', function ($table) {
            $table->foreign('notif_data_id', 'FK_notif_queue_notif_data')->references('id')->on('notif_data')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
         
        Schema::table('recommendation', function ($table) {
            $table->foreign('designer_id', 'FK_recommendation_designer')->references('id')->on('designer')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
            $table->foreign('user_id', 'FK_recommendation_user')->references('id')->on('user')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
            $table->foreign('device_id', 'FK_recommendation_user_device')->references('id')->on('user_device')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
 
        Schema::table('area_city', function ($table) {
            $table->foreign('area_province_id', 'area_city_province_id_foreign')->references('id')->on('area_province')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alarm_email', function(Blueprint $table)
        {
            $table->dropForeign('FK_alarm_email_designer');
        });
 
        Schema::table('designer_verify', function(Blueprint $table)
        {
            $table->dropForeign('FK_designer_verify_designer');
        });
 
        Schema::table('landing_tag', function (Blueprint $table) {
            $table->dropForeign('FK_landing_tag_landing');
            $table->dropForeign('FK_landing_tag_tag');
        });
 
        Schema::table('landing', function (Blueprint $table) {
            $table->dropForeign('FK_landing_landing');
        });
 
        Schema::table('notif', function (Blueprint $table) {
            $table->dropForeign('FK_notif_media');
        });
 
        Schema::table('notif_queue', function (Blueprint $table) {
            $table->dropForeign('FK_notif_queue_notif_data');
        });
 
        Schema::table('recommendation', function (Blueprint $table) {
            $table->dropForeign('FK_recommendation_designer');
            $table->dropForeign('FK_recommendation_user');
            $table->dropForeign('FK_recommendation_user_device');
        });
 
        Schema::table('area_city', function (Blueprint $table) {
            $table->dropForeign('area_city_province_id_foreign');
        });
    }
}