<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInterceptBbkTrackerTable
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 * @see PRD : https://mamikos.atlassian.net/wiki/spaces/U/pages/211878331/Flow+Activate+Booking+Langsung
 */
class CreateInterceptBbkTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('intercept_bbk_tracker', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mamipay_owner_profile_id')->index();
            $table->timestamp("intercept_at")->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('intercept_bbk_tracker');
    }
}
