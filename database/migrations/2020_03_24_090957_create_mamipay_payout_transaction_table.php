<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMamipayPayoutTransactionTable extends Migration
{
    /**
     * https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/326729869/2020-03-18+-+Create+New+Table+mamipay+payout+transaction
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mamipay_payout_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('contract_invoice_id')->index('contract_invoice_id');
            $table->enum('mode', ['percentage', 'amount']);
            $table->decimal('mode_value', 16, 2);
            $table->decimal('kost_price', 16, 2);
            $table->decimal('total_payout_amount', 16, 2);
            $table->float('charge_percentage');
            $table->decimal('charge_amount', 16, 2);
            $table->decimal('additional_amount', 16, 2);
            $table->decimal('transfer_amount', 16, 2)->index('transfer_amount');
            $table->enum('transfer_status', ['processing', 'transferred', 'failed'])->index('transfer_status');
            $table->dateTime('processed_at')->nullable();
            $table->dateTime('transferred_at')->nullable();
            $table->dateTime('failed_at')->nullable();
            $table->string('transfer_reference')->nullable()->index('transfer_reference');
            $table->unsignedInteger('transferred_by')->index('transferred_by')->comment('foreign key to user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mamipay_payout_transaction');
    }
}
