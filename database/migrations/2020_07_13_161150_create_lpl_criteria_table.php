<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLplCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'lpl_criteria',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100)
                    ->comment('name of the criteria');
                $table->text('description')
                    ->nullable();
                $table->string('attribute', 50)
                    ->unique()
                    ->comment('name of the criteria\'s attribute');
                $table->smallInteger('order')
                    ->nullable()
                    ->comment('criteria ordering');
                $table->integer('score')
                    ->default(0)
                    ->comment('calculated score in Decimal format');
                $table->timestamps();
                $table->softDeletes();

                // Create composite Unique attribute
                $table->unique(
                    [
                        'attribute',
                        'deleted_at'
                    ]
                );

                $table->index('name');
                $table->index('attribute');

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lpl_criteria');
    }
}
