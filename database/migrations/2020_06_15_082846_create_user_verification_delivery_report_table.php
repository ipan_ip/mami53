<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVerificationDeliveryReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verification_delivery_report', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_verification_code_id');
            $table->string('network')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->string('reason')->nullable();
            $table->timestamp('send_at')->nullable();
            $table->timestamp('done_at')->nullable();
            $table->timestamps();

            $table->index('user_verification_code_id', 'usr_ver_delivery_code_id_idx');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_verification_delivery_report');
    }
}
