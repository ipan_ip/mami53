<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataLeadsColumnsToConsultantPotentialPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->boolean('is_priority')->default(true);

            $table->text('offered_product')->change();

            $table->unsignedInteger('media_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultant_potential_property', function (Blueprint $table) {
            $table->dropColumn('is_priority');

            DB::statement("ALTER TABLE consultant_potential_property MODIFY COLUMN offered_product ENUM('gp', 'bbk')");

            $table->unsignedInteger('media_id')->nullable(false)->change();
        });
    }
}
