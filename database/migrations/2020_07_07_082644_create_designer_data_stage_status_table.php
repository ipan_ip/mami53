<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerDataStageStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_data_stage_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('designer_id')
                ->nullable()
                ->comment('foreign key to designer.id');
            $table->string('name')
                ->comment('name of the stage');
            $table->boolean('complete')
                ->default(false)
                ->comment('completeness status of the stage');
            $table->timestamps();

            $table->foreign('designer_id')->references('id')->on('designer')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_data_stage_status');
    }
}
