<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddRedeemableColumnInPointHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_history', function (Blueprint $table) {
            $table->string('redeemable_type', 50)->nullable()->after('redeem_id');
            $table->unsignedInteger('redeemable_id')->nullable()->after('redeemable_type');
            $table->unsignedInteger('redeem_value')->default(0)->after('redeemable_id');

            $table->index(['redeemable_type', 'redeemable_id'], 'point_history_redeemable_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_history', function (Blueprint $table) {
            $table->dropIndex('point_history_redeemable_index');

            $table->dropColumn('redeemable_type');
            $table->dropColumn('redeemable_id');
            $table->dropColumn('redeem_value');
        });
    }
}
