<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUnreadCountInOwnerChatStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_chat_status', function (Blueprint $table) {
            $table->unsignedSmallInteger('unread_count')->after('last_logged_in_at')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_chat_status', function (Blueprint $table) {
            $table->dropColumn('unread_count');     
        });
    }
}
