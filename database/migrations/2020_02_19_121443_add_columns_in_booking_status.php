<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Entities\Booking\StatusChangedBy;

class AddColumnsInBookingStatus extends Migration
{
    /**
     * reference to the addition of migration here :
     * https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/298057903/Draft+2020-02-28+-+Add+New+Column+to+booking+status
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_status', function (Blueprint $table) {
            $table->unsignedInteger('changed_by_id')->after('status')->nullable();
            $table->enum('changed_by_role',
                [
                    StatusChangedBy::SYSTEM,
                    StatusChangedBy::ADMIN,
                    StatusChangedBy::TENANT,
                    StatusChangedBy::CONSULTANT,
                    StatusChangedBy::OWNER,
                    StatusChangedBy::UNKNOWN
                ])->default(StatusChangedBy::UNKNOWN)->after('changed_by_id');

            $table->index('changed_by_role', 'changed_by_role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_status', function (Blueprint $table) {
            $table->dropIndex('changed_by_role');

            $table->dropColumn('changed_by_id');
            $table->dropColumn('changed_by_role');
        });
    }
}
