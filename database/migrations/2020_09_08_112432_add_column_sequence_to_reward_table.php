<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSequenceToRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reward', function($table) {
            $table->unsignedInteger('sequence')->after('howto')->default(0);
            $table->index('sequence', 'reward_sequence_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reward', function($table) {
            $table->dropIndex('reward_sequence_index');
            $table->dropColumn('sequence');
        });
    }
}
