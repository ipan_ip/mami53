<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankInformationToMamipayDepositTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            if (!Schema::hasColumn('mamipay_deposit_transaction','account_holder')){                
                $table->string('account_holder')->nullable()->after('account_number');
            }    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_deposit_transaction', function (Blueprint $table) {
            $table->dropColumn('account_holder');
        });
    }
}
