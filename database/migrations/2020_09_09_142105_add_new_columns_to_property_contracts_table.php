<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToPropertyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->boolean('is_autocount')->nullable()->after('status');
            $table->unsignedInteger('total_room_package')->nullable()->after('is_autocount');
            $table->unsignedInteger('updated_by')->nullable()->after('total_room_package');
            $table->unsignedInteger('deleted_by')->nullable()->after('updated_by');
            $table->softDeletes();

            $table->index('updated_by');
            $table->index('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->dropIndex(['updated_by']);
            $table->dropIndex(['deleted_by']);
            $table->dropColumn('is_autocount');
            $table->dropColumn('total_room_package');
            $table->dropColumn('updated_by');
            $table->dropColumn('deleted_by');
            $table->dropColumn('deleted_at');
        });
    }
}
