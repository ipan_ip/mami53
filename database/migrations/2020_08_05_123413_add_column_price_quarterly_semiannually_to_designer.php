<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPriceQuarterlySemiAnnuallyToDesigner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->integer('price_quarterly')->default(0);
            $table->integer('price_semiannually')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer', function (Blueprint $table) {
            $table->dropColumn('price_quarterly');
            $table->dropColumn('price_semiannually');
        });
    }
}
