<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnDesignerMatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designer_match', function($table) {
            $table->boolean('is_promoted_room')->default(false)->nullable();

            $table->index('created_at', 'designer_match_created_at_index');
            $table->index('is_promoted_room', 'designer_match_is_promoted_room_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designer_match', function($table) {
            $table->dropIndex('designer_match_is_promoted_room_index');
            $table->dropIndex('designer_match_created_at_index');

            $table->dropColumn('is_promoted_room');
        });
    }
}
