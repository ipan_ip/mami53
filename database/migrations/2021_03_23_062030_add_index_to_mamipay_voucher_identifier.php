<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMamipayVoucherIdentifier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mamipay_voucher_by_kost_identifier', function (Blueprint $table) {
            $table->index(['voucher_id'], 'voucher_id_index');
            $table->index(['type', 'identifier'], 'type_identifier_index');
        });

        Schema::table('mamipay_voucher_exclude_identifier', function (Blueprint $table) {
            $table->dropIndex('type');
            
            $table->index(['voucher_id'], 'voucher_id_index');
            $table->index(['type', 'identifier'], 'type_identifier_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_voucher_by_kost_identifier', function (Blueprint $table) {
            $table->dropIndex('voucher_id_index');
            $table->dropIndex('type_identifier_index');
        });

        Schema::table('mamipay_voucher_exclude_identifier', function (Blueprint $table) {
            $table->dropIndex('voucher_id_index');
            $table->dropIndex('type_identifier_index');
            
            $table->index(['type'], 'type');
        });
    }
}
