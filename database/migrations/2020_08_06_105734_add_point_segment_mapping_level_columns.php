<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointSegmentMappingLevelColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->string('title', 50)->after('type');
            $table->unsignedInteger('kost_level_id')->after('title');
            $table->unsignedInteger('room_level_id')->after('kost_level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('kost_level_id');
            $table->dropColumn('room_level_id');
        });
    }
}
