<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoDocumentIdToMamipayTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('mamipay_tenant','photo_document_id')){
            Schema::table('mamipay_tenant', function (Blueprint $table) {
                $table->integer('photo_document_id')->nullable()->after('photo_identifier_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mamipay_tenant', function (Blueprint $table) {
            $table->dropColumn('photo_document_id');
        });
    }
}
