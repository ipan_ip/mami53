<?php

use Illuminate\Database\Seeder;
use App\Entities\Chat\Criteria;
use Carbon\Carbon;

class ChatCriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Criteria::query()->truncate();

        DB::table('chat_criteria')->insert($this->getSeederData());
    }

    /**
     * Compile default criteria for seeder
     *
     * @return array[]
     */
    public function getSeederData()
    {
        $now = Carbon::now();

        return [
            [
                'name' => 'gp1',
                'attribute' => 'is_gp1',
                'description' => 'Kos is Mamikos Goldplus 1 on Kos level',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'gp2',
                'attribute' => 'is_gp2',
                'description' => 'Kos is Mamikos Goldplus 2 on Kos level',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'gp3',
                'attribute' => 'is_gp3',
                'description' => 'Kos is Mamikos Goldplus 3 on Kos level',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'gp4',
                'attribute' => 'is_gp4',
                'description' => 'Kos is Mamikos Goldplus 4 on Kos level',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Mamirooms',
                'attribute' => 'is_mamirooms',
                'description' => 'Kos is Mamirooms',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'FreeListing',
                'attribute' => 'is_free_listing',
                'description' => 'Kos is Non BBK and owner of the kos is not premium',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'PremiumSaldoOn',
                'attribute' => 'is_premium_saldo_on',
                'description' => 'Owner is premium and with active saldo',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'PremiumSaldoOff',
                'attribute' => 'is_premium_saldo_off',
                'description' => 'Owner is premium and with nonactive saldo',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'BBK',
                'attribute' => 'is_bbk',
                'description' => 'Kos joins BBK',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'NotBBK',
                'attribute' => 'is_not_bbk',
                'description' => 'Kos does not join BBK',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'RoomAvailable',
                'attribute' => 'is_available',
                'description' => 'There are room available in kos',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'RoomNotAvailable',
                'attribute' => 'is_not_available',
                'description' => 'There are not room available in kos',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
    }
}
