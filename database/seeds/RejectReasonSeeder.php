<?php

use App\Entities\Generate\RejectReason;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RejectReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RejectReason::query()->truncate();

        DB::table('reject_reason')->insert($this->getSeederData());
    }

    public function getSeederData(): array
    {
        $now = Carbon::now();

        return [
            [
                'group' => 'address',
                'content' => 'Tidak lengkap (isi RT/RW dan nomor rumah)',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'address',
                'content' => 'Posisi di peta kurang sesuai',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'address',
                'content' => null,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Tidak boleh menggunakan frame',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Tidak boleh ada logo/cap selain Mamikos',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Tidak boleh digabung (kolase) dengan foto lain',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Harus lengkap (Tampak Depan, Dalam, dan Tampak dari Jalan)',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Tidak boleh blur',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Tidak boleh berbentuk screenshot',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => 'Foto fasilitas harus sesuai dengan fasilitas yang tersedia',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'building_photo',
                'content' => null,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Tidak boleh menggunakan frame',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Tidak boleh ada logo/cap selain Mamikos',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Tidak boleh digabung (kolase) dengan foto lain',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Harus lengkap (Tampak Depan dan Dalam)',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Tidak boleh blur',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Tidak boleh berbentuk screenshot',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => 'Foto fasilitas harus sesuai dengan fasilitas yang tersedia',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'room_photo',
                'content' => null,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'kost_ads',
                'content' => 'Tidak sesuai kategori. Silakan gunakan aplikasi "Dekatmu"',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'kost_ads',
                'content' => 'Tidak dapat tampil. Silakan lengkapi pendaftaran Booking Langsung',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'kost_ads',
                'content' => 'Anda sudah punya kos dengan data yang sama',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'kost_ads',
                'content' => null,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'apartment_ads',
                'content' => 'Hanya dapat diiklankan oleh pengguna Premium. Silakan hubungi CS',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'apartment_ads',
                'content' => 'Data kepemilikan kurang lengkap. Silakan hubungi CS',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'group' => 'apartment_ads',
                'content' => null,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ];
    }
}
