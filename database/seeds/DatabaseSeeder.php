<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LandingListTableSeeder::class,
            ClickPricingSeeder::class,
            LplCriteriaSeeder::class,
            RejectReasonSeeder::class,
        ]);
    }
}
