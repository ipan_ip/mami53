<?php

use App\Entities\Premium\ClickPricing;
use Illuminate\Database\Seeder;

class ClickPricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CHAT)->make()->toArray()
        );

        ClickPricing::firstOrCreate(
            factory(ClickPricing::class, ClickPricing::FOR_TYPE_CLICK)->make()->toArray()
        );
    }
}
