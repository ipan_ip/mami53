<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class GoldplusPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            [
                "code" => "gp1",
                "name" => "Goldplus 1",
                "description" => "Paket Dasar Goldplus 1",
                "price" => 145000,
                "periodicity" => "monthly",
                "unit_type" => "all",
                "active" => true
            ],
            [
                "code" => "gp2",
                "name" => "Goldplus 2",
                "description" => "Goldplus 1 + Super Assistant",
                "price" => 295000,
                "periodicity" => "monthly",
                "unit_type" => "all",
                "active" => true
            ],
            [
                "code" => "gp3",
                "name" => "Goldplus 3",
                "description" => "Goldplus 2 + Bantuan Account Manager",
                "price" => 595000,
                "periodicity" => "monthly",
                "unit_type" => "single",
                "active" => true
            ]
        ];

        foreach ($packages as $package) {
            DB::table('goldplus_package')->insert([
                'code' => $package['code'],
                'name' => $package['name'],
                'description' => $package['description'],
                'price' => $package['price'],
                'periodicity' => $package['periodicity'],
                'unit_type' => $package['unit_type'],
                'active' => $package['active'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
