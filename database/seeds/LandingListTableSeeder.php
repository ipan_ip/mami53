<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LandingListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // remove existing parent landing data if existed
        $landing = \App\Entities\Landing\LandingList::getParentBookingLanding();
        if (!is_null($landing)) 
        {
            $landing->delete();
        }

        $now = Carbon::now();

        // Inserting parent landing data for booking kost
        DB::table('landing_list')->insert([
            'type' => "booking",
            'slug' => "",
            'keyword' => "Meta Keyword - booking, booking kost, booking kamar kost, booking rumah kost, booking kost harian, booking kost bulanan, booking kost promo, booking penginapan murah, booking hotel murah",
            'description' => "Meta Description - Situs booking kost No 1 di indonesia dengan harga termurah. Bebas biaya transaksi, diskon dan promo kost booking setiap hari. Cari lokasi terdekat dan pesan kamar sekarang",
            'option' => '{"title":"Booking Kost Murah - Harga Promo","article":""}',
			'is_default' => 1,
            'is_active' => 1,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
