<?php

use App\Entities\Lpl\Criteria;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LplCriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate existing data first
        Criteria::query()->truncate();

        // Update the criteria using LPL version 2
        // ref link : https://mamikos.atlassian.net/browse/BG-3283
        DB::table('lpl_criteria')->insert($this->getSeederDataVersion());
    }

    /**
     * Compile default criteria with new version for seeder
     * @return array[]
     */
    public function getSeederDataVersion()
    {
        $now = Carbon::now();

        return [
            [
                'name' => 'Non-hostile Owner',
                'description' => 'Owner is NOT a competitor',
                'attribute' => 'is_not_hostile',
                'order' => 12,
                'score' => 2048,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Photo Availability',
                'description' => 'Kos has available photo',
                'attribute' => 'has_photo',
                'order' => 11,
                'score' => 1024,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Room Availability',
                'description' => 'Kos has available room',
                'attribute' => 'has_room',
                'order' => 10,
                'score' => 512,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Promo Ngebut',
                'description' => 'Kos is included in "Promo Ngebut" program',
                'attribute' => 'is_flash_sale',
                'order' => 9,
                'score' => 256,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Mamiroom',
                'description' => 'Kos is a Mamiroom typed',
                'attribute' => 'is_mamiroom',
                'order' => 8,
                'score' => 128,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Gold Plus 4',
                'description' => 'Kos has a GP 4 level',
                'attribute' => 'is_gp_4',
                'order' => 8,
                'score' => 128,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Gold Plus 3',
                'description' => 'Kos has a GP 3 level',
                'attribute' => 'is_gp_3',
                'order' => 7,
                'score' => 64,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Gold Plus 2',
                'description' => 'Kos has a GP 2 level',
                'attribute' => 'is_gp_2',
                'order' => 6,
                'score' => 32,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Gold Plus 1',
                'description' => 'Kos has a GP 1 level',
                'attribute' => 'is_gp_1',
                'order' => 6,
                'score' => 32,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Basic Listing',
                'description' => 'Kos has a regular level',
                'attribute' => 'is_basic',
                'order' => 6,
                'score' => 32,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Premium: Active Saldo',
                'description' => 'Kos is a premium typed with allocated saldo',
                'attribute' => 'is_premium_saldo_on',
                'order' => 5,
                'score' => 16,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Kos BBK',
                'description' => 'Kos has Booking capability',
                'attribute' => 'is_booking',
                'order' => 4,
                'score' => 8,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Kos Good Booking Performance',
                'description' => 'Kos has a good of booking performance value',
                'attribute' => 'has_good_booking_performance',
                'order' => 3,
                'score' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Kos Good BAR',
                'description' => 'Kos has a good of BAR value',
                'attribute' => 'is_bar',
                'order' => 2,
                'score' => 2,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Money-back Guarantee',
                'description' => 'Kos has a Money-back guarantee',
                'attribute' => 'is_guaranteed',
                'order' => 1,
                'score' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
    }
}
