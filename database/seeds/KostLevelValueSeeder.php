<?php

use App\Entities\Level\KostLevelValue;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KostLevelValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate existing data first
        $this->truncateTable();

        // Inserting parent landing data for booking kost
        DB::table('kost_level_value')->insert($this->getSeederData());
    }

    private function truncateTable()
    {
        if (KostLevelValue::query()->count()) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('kost_level_value')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    private function getSeederData(): array
    {
        $now = Carbon::now();

        return [
            [
                'name' => 'Free of Charge',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Insurance Tenant Protection',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Booking Langsung',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Pro Service',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Mamichecker',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Garansi Uang Kembali',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Dikelola Penuh Oleh Mamikos',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
    }
}
