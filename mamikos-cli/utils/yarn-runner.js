/**
 * https://stackabuse.com/executing-shell-commands-with-node-js/
 */

const chalk = require('chalk');
const { parseArgsStringToArgv } = require('string-argv');
const spawn = require('cross-spawn');

module.exports = (command, commandNameAlias = '') => {
	const commandName = commandNameAlias || command;

	console.log(
		chalk.black.bgGreen(` Yarn is executing `),
		chalk.grey(commandName)
	);

	const ls = spawn('yarn', parseArgsStringToArgv(command));

	ls.stdout.on('data', data => {
		console.log(chalk.white(`${data}`));
	});

	ls.stderr.on('data', data => {
		console.log(chalk.grey(`${data}`));
	});

	ls.on('error', error => {
		console.log(`error: ${error.message}`);
	});

	ls.on('close', code => {
		console.log(
			chalk.black.bgGreen(` Yarn finished `),
			chalk.grey(commandName),
			chalk.green.bold(`[${code}]`)
		);
	});
};
