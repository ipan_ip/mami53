const mix = require('laravel-mix');

const compile = () => {
	mix
		.sass('resources/assets/sass/app.scss', 'public/dist/css')
		.styles(
			['resources/assets/css/@appmamikos.css'],
			'public/dist/css/appmamikos.css'
		)
		.js(
			'resources/assets/js/@vendor/main/script.js',
			'public/dist/js/@vendor/main'
		)
		.js(
			'resources/assets/js/@vendor/detail-booking/script.js',
			'public/dist/js/@vendor/detail-booking'
		);
};

module.exports = { compile };
