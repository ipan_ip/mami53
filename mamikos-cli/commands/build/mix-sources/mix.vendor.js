const mix = require('laravel-mix');

const src = require('../constants/path');

const dist = 'public/dist';

const compile = {
	css() {
		mix
			.styles(
				[
					src.css.bootstrap_3,
					src.css.fontAwesome,
					src.css.leaflet,
					src.css.sweetalert2,
					src.css.swiper,
					src.css.swipebox,
					src.css.angularjsDatepicker,
					src.css.eonasdanBootstrapDatetimepicker,
					src.css.jqueryBarRating_stars,
					src.css.jqueryBarRating_starsO
				],
				`${dist}/css/vendor.css`
			)

			.styles(
				[
					src.css.bootstrap_3,
					src.css.fontAwesome,
					src.css.sweetalert2,
					src.css.jqueryBarRating_stars,
					src.css.jqueryBarRating_starsO
				],
				`${dist}/css/vendorsimple.css`
			)

			.styles(
				[
					src.css.bootstrap_3,
					src.css.fontAwesome,
					src.css.leaflet,
					src.css.sweetalert2
				],
				`${dist}/css/vendorlanding.css`
			)

			.styles(
				[
					src.css.bootstrap_3,
					src.css.fontAwesome,
					src.css.sweetalert2,
					src.css.appmamikos,
					src.css.common
				],
				`${dist}/css/common.css`
			)

			.styles(
				[
					src.css.bootstrap_4,
					src.css.bootstrapVue,
					src.css.fontAwesome,
					src.css.sweetalert2
				],
				`${dist}/css/common2.css`
			)

			/// ///////////////////////////////////////////////////////////////////////

			.styles(
				['node_modules/leaflet/dist/leaflet.css'],
				'public/dist/vendor/leaflet/leaflet.css'
			)

			.styles(
				['node_modules/photo-sphere-viewer/dist/photo-sphere-viewer.min.css'],
				'public/dist/vendor/photo-sphere-viewer/photo-sphere-viewer.css'
			);
	},
	js() {
		mix
			.scripts(
				[
					src.js.vue,
					src.js.angular,
					src.js.jquery,
					src.js.bootstrap_3,
					src.js.jqueryValidate,
					src.js.jqueryBarRating,
					src.js.axios,
					src.js.swiper,
					src.js.swipebox,
					src.js.jsCookie,
					src.js.blueimpMd5,
					src.js.sweetalert2,
					src.js.jockey,
					src.js.dropzone,
					src.js.bugsnagJs,
					src.js.angularResource,
					src.js.angularCookies,
					src.js.angularPreloadImage,
					src.js.angularUiBootstrap,
					src.js.ngFileUpload,
					src.js.ngmap,
					src.js.markerwithlabel
				],
				`${dist}/js/vendor.js`
			)

			.scripts(
				[
					src.js.babelPolyfill,
					src.js.vue,
					src.js.jquery,
					src.js.axios,
					src.js.jsCookie,
					src.js.sweetalert2,
					src.js.bugsnagJs
				],
				`${dist}/js/vendorsimple.js`
			)

			.scripts(
				[
					src.js.babelPolyfill,
					src.js.vue,
					src.js.axios,
					src.js.angular,
					src.js.jquery,
					src.js.bootstrap_3,
					src.js.leaflet,
					src.js.jquerySticky,
					src.js.jsCookie,
					src.js.blueimpMd5,
					src.js.sweetalert2,
					src.js.jockey,
					src.js.bugsnagJs,
					src.js.angularResource,
					src.js.angularCookies,
					src.js.angularPreloadImage,
					src.js.angularUiBootstrap,
					src.js.ngFileUpload,
					src.js.ngmap
				],
				`${dist}/js/vendorlanding.js`
			)

			.scripts(
				[
					src.js.babelPolyfill,
					src.js.vue,
					src.js.vueLazyLoad,
					src.js.axios,
					src.js.angular,
					src.js.jquery,
					src.js.bootstrap_3,
					src.js.moment,
					src.js.eonasdanBootstrapDatetimepicker,
					src.js.jquerySticky,
					src.js.jqueryValidate,
					src.js.jqueryBarRating,
					src.js.swiper,
					src.js.swipebox,
					src.js.jsCookie,
					src.js.blueimpMd5,
					src.js.sweetalert2,
					src.js.dropzone,
					src.js.bugsnagJs
				],
				`${dist}/js/vendordetail.js`
			)

			.scripts(
				[src.js.bugsnagJs, src.js.jquery, src.js.jsCookie],
				`${dist}/js/common.js`
			)

			.scripts([src.js.bugsnagJs, src.js.jsCookie], `${dist}/js/common2.js`)

			/// ///////////////////////////////////////////////////////////////////////

			.scripts(
				['node_modules/moment/moment.js'],
				'public/dist/vendor/moment/moment.js'
			)

			.scripts(
				['node_modules/leaflet/dist/leaflet.js'],
				'public/dist/vendor/leaflet/leaflet.js'
			)

			.scripts(
				[
					'node_modules/three/build/three.min.js',
					'node_modules/promise-polyfill/dist/polyfill.min.js',
					'node_modules/uevent/uevent.min.js',
					'node_modules/dot/doT.min.js',
					'node_modules/nosleep.js/dist/NoSleep.min.js',
					'node_modules/three/examples/js/controls/DeviceOrientationControls.js',
					'node_modules/three/examples/js/effects/StereoEffect.js',
					'node_modules/photo-sphere-viewer/dist/photo-sphere-viewer.min.js'
				],
				'public/dist/vendor/photo-sphere-viewer/photo-sphere-viewer.js'
			);
	}
};

module.exports = { compile };
