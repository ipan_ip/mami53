const mix = require('laravel-mix');

const controller = 'resources/assets/js/_controller';
const dist = 'public/dist';

const compile = () => {
	mix
		.combine(
			[`${controller}/combined/maincontroller.js`],
			`${dist}/js/bundle/maincontroller.js`
		)

		.combine(
			[`${controller}/combined/bundleexamvarious.js`],
			`${dist}/js/bundle/bundleexamvarious.js`
		);
};

module.exports = { compile };
