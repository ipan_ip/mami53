const mix = require('laravel-mix');

const compile = () => {
	mix
		.copyDirectory('public/js/controller', 'public/dist/js/controller')
		.copyDirectory(
			'node_modules/leaflet/dist/images',
			'public/dist/vendor/leaflet/images'
		)
		.copy(
			'node_modules/sendbird/SendBird.min.js',
			'public/dist/vendor/sendbird-chat/SendBird.min.js'
		)
		.copy(
			'node_modules/sendbird-web-widget/dist/widget.SendBird.js',
			'public/dist/vendor/sendbird-chat/widget.SendBird.js'
		)
		.copy('node_modules/bangul-vue/assets', 'public/assets/bangul');
};

module.exports = { compile };
