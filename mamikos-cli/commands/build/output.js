const vendor = require('./mix-sources/mix.vendor');
const copy = require('./mix-sources/mix.copy');

const combine = require('./mix-sources/mix.combine');
const bundle = require('./mix-sources/mix.bundle');

const core = require('./mix-sources/mix.core');

const app = require('./mix-sources/mix.app');

module.exports = () => {
	/* Vendor / Third Party Resources */
	vendor.compile.css();
	vendor.compile.js();
	copy.compile();

	/* Legacy Angular Code */
	combine.compile();
	bundle.compile.js();

	/* Core Assets */
	core.compile();

	/* Main App */
	app.compile();
};
