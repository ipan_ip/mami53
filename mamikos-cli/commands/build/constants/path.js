const base = 'node_modules';
const assets = 'resources/assets';
const controller = 'resources/assets/js/_controller';

const css = {
	/* Vendor Libraries  */
	bootstrap_3: `${base}/bootstrap-css-only/css/bootstrap.min.css`,
	bootstrap_4: `${base}/bootstrap/dist/css/bootstrap.min.css`,
	bootstrapVue: `${base}/bootstrap-vue/dist/bootstrap-vue.min.css`,
	fontAwesome: `${base}/font-awesome/css/font-awesome.min.css`,
	leaflet: `${base}/leaflet/dist/leaflet.css`,
	sweetalert2: `${base}/sweetalert2/dist/sweetalert2.min.css`,
	swiper: `${base}/swiper/dist/css/swiper.min.css`,
	swipebox: `${base}/swipebox/src/css/swipebox.min.css`,
	angularjsDatepicker: `${base}/angularjs-datepicker/dist/angular-datepicker.min.css`,
	eonasdanBootstrapDatetimepicker: `${base}/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css`,
	jqueryBarRating_stars: `${base}/jquery-bar-rating/dist/themes/fontawesome-stars.css`,
	jqueryBarRating_starsO: `${base}/jquery-bar-rating/dist/themes/fontawesome-stars-o.css`,
	common: `${assets}/css/common.css`,
	appmamikos: `${assets}/css/@appmamikos.css`
};

const js = {
	/* Vendor Libraries  */
	// vue: `${base}/vue/dist/vue.js`,
	vue: `${base}/vue/dist/vue.min.js`,
	vueLazyLoad: `${base}/vue-lazyload/vue-lazyload.js`,
	babelPolyfill: `${base}/@babel/polyfill/dist/polyfill.min.js`,
	es6PromiseAuto: `${base}/es6-promise/dist/es6-promise.auto.min.js`,
	angular: `${base}/angular/angular.min.js`,
	angularResource: `${base}/angular-resource/angular-resource.min.js`,
	angularCookies: `${base}/angular-cookies/angular-cookies.min.js`,
	angularPreloadImage: `${base}/angular-preload-image/angular-preload-image.min.js`,
	angularUiBootstrap: `${base}/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js`,
	ngFileUpload: `${base}/ng-file-upload/dist/ng-file-upload.min.js`,
	ngmap: `${base}/ngmap/build/scripts/ng-map.min.js`,
	markerwithlabel: `${base}/markerwithlabel/index.js`,
	jquery: `${base}/jquery.2/node_modules/jquery/dist/jquery.min.js`,
	jquerySticky: `${base}/jquery-sticky/jquery.sticky.js`,
	jqueryValidate: `${base}/jquery-validation/dist/jquery.validate.min.js`,
	jqueryBarRating: `${base}/jquery-bar-rating/dist/jquery.barrating.min.js`,
	moment: `${base}/moment/min/moment.min.js`,
	bootstrap_3: `${base}/bootstrap-sass/assets/javascripts/bootstrap.min.js`,
	bootstrap_4: `${base}/bootstrap/dist/js/bootstrap.min.js`,
	eonasdanBootstrapDatetimepicker: `${base}/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js`,
	swiper: `${base}/swiper/dist/js/swiper.min.js`,
	swipebox: `${base}/swipebox/src/js/jquery.swipebox.min.js`,
	leaflet: `${base}/leaflet/dist/leaflet.js`,
	axios: `${base}/axios/dist/axios.min.js`,
	jsCookie: `${base}/js-cookie/src/js.cookie.js`,
	blueimpMd5: `${base}/blueimp-md5/js/md5.min.js`,
	sweetalert2: `${base}/sweetalert2/dist/sweetalert2.min.js`,
	jockey: `${base}/jockey/jockey.min.js`,
	dropzone: `${base}/dropzone/dist/min/dropzone.min.js`,
	bugsnagJs: `${base}/bugsnag-js/dist/bugsnag.min.js`,

	/* Old Angular 1 Controllers */
	/* Booking Modules */
	bookingGlobalModule: `${controller}/pages/booking/modules/bookingglobalmodule.js`,
	bookingSelectModule: `${controller}/pages/booking/modules/bookingselectmodule.js`,
	bookingDetailModule: `${controller}/pages/booking/modules/bookingdetailmodule.js`,
	bookingReviewModule: `${controller}/pages/booking/modules/bookingreviewmodule.js`,
	bookingPaymentModule: `${controller}/pages/booking/modules/bookingpaymentmodule.js`,
	bookingReceiptModule: `${controller}/pages/booking/modules/bookingreceiptmodule.js`,
	bookingHistoryModule: `${controller}/pages/booking/modules/bookinghistorymodule.js`,
	bookingRootModule: `${controller}/pages/booking/modules/bookingrootmodule.js`
};

module.exports = { css, js };
