module.exports = {
	dev: 'cross-env next=true mix',
	watch: 'cross-env next=true mix watch',
	'watch-poll': 'cross-env next=true mix watch -- --watch-options-poll=1000',
	prod: 'cross-env next=true mix --production',
	hot: 'cross-env next=true mix watch --hot'
};
