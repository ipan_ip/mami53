const assets = 'resources/assets';
const dist = 'public/dist';

const sources = {
	_home: {
		entry: `${assets}/js/_home/app.js`,
		output: `${dist}/js/_home`
	},
	_detail: {
		entry: `${assets}/js/_detail/app.js`,
		output: `${dist}/js/_detail`
	},
	_user: {
		entry: `${assets}/js/_user/app.js`,
		output: `${dist}/js/_user`
	},
	_promotion: {
		entry: `${assets}/js/_promotion/app.js`,
		output: `${dist}/js/_promotion`
	},
	_career: {
		entry: `${assets}/js/_career/app.js`,
		output: `${dist}/js/_career`
	},
	'_dbet-tenant': {
		entry: `${assets}/js/_dbet-tenant/app.js`,
		output: `${dist}/js/_dbet-tenant`
	},
	'_about-us': {
		entry: `${assets}/js/_about-us/app.js`,
		output: `${dist}/js/_about-us`
	},
	'_landing/house': {
		entry: `${assets}/js/_landing/house/app.js`,
		output: `${dist}/js/_landing/house`
	},
	'_landing/direct-booking': {
		entry: `${assets}/js/_landing/direct-booking/app.js`,
		output: `${dist}/js/_landing/direct-booking`
	},
	'_mamirooms/landing': {
		entry: `${assets}/js/_mamirooms/landing/app.js`,
		output: `${dist}/js/_mamirooms/landing`
	},
	'_ownerpage/mamipay': {
		entry: `${assets}/js/_ownerpage/mamipay/app.js`,
		output: `${dist}/js/_ownerpage/mamipay`
	},
	'_ownerpage/billing-management': {
		entry: `${assets}/js/_ownerpage/billing-management/app.js`,
		output: `${dist}/js/_ownerpage/billing-management`
	},
	_consultant: {
		entry: `${assets}/js/_consultant/app.js`,
		output: `${dist}/js/_consultant`
	},
	_area: {
		entry: `${assets}/js/_area/app.js`,
		output: `${dist}/js/_area`
	},
	'_flash-sale': {
		entry: `${assets}/js/_flash-sale/app.js`,
		output: `${dist}/js/_flash-sale`
	},
	_search: {
		entry: `${assets}/js/_search/app.js`,
		output: `${dist}/js/_search`
	},
	_sanjunipero: {
		entry: `${assets}/js/_sanjunipero/app.js`,
		output: `${dist}/js/_sanjunipero`
	},
	'_landing-billing': {
		entry: `${assets}/js/_landing-billing/app.js`,
		output: `${dist}/js/_landing-billing`
	},
	'_landing-singgahsini': {
		entry: `${assets}/js/_landing-singgahsini/app.js`,
		output: `${dist}/js/_landing-singgahsini`
	},
	'_register-singgahsini': {
		entry: `${assets}/js/_register-singgahsini/app.js`,
		output: `${dist}/js/_register-singgahsini`
	},
	_promo: {
		entry: `${assets}/js/_promo/app.js`,
		output: `${dist}/js/_promo/`
	},
	'_promo/all-kost': {
		entry: `${assets}/js/_promo/all-kost/app.js`,
		output: `${dist}/js/_promo/all-kost`
	},
	_login: {
		entry: `${assets}/js/_login/app.js`,
		output: `${dist}/js/_login`
	},
	'_download-exam': {
		entry: `${assets}/js/_download-exam/app.js`,
		output: `${dist}/js/_download-exam/app.js`
	},
	'_landing-content': {
		entry: `${assets}/js/_landing-content/app.js`,
		output: `${dist}/js/_landing-content/app.js`
	},
	'_admin/abtest': {
		entry: `${assets}/js/_admin/abtest/app.js`,
		output: `${dist}/js/_admin/abtest`
	},

	'@global/login-user.js': {
		entry: `${assets}/js/@global/login-user.js`,
		output: `${dist}/js/`
	},
	'@global/login-user-jquery.js': {
		entry: `${assets}/js/@global/login-user-jquery.js`,
		output: `${dist}/js/`
	},
	'@single/navbar-custom.js': {
		entry: `${assets}/js/@single/navbar-custom.js`,
		output: `${dist}/js/`
	},
	'@single/navbar-owner.js': {
		entry: `${assets}/js/@single/navbar-owner.js`,
		output: `${dist}/js/`
	},
	'404-page/404-page.js': {
		entry: `${assets}/js/404-page/404-page.js`,
		output: `${dist}/js/`
	},
	'input/input-properti.js': {
		entry: `${assets}/js/input/input-properti.js`,
		output: `${dist}/js/`
	},
	'input/input-kost.js': {
		entry: `${assets}/js/input/input-kost.js`,
		output: `${dist}/js/`
	},
	'input/input-edit.js': {
		entry: `${assets}/js/input/input-edit.js`,
		output: `${dist}/js/`
	},
	'input/input-agent.js': {
		entry: `${assets}/js/input/input-agent.js`,
		output: `${dist}/js/`
	},
	'input/input-properti-apartment.js': {
		entry: `${assets}/js/input/input-properti-apartment.js`,
		output: `${dist}/js/`
	},
	'input/input-edit-apartment.js': {
		entry: `${assets}/js/input/input-edit-apartment.js`,
		output: `${dist}/js/`
	},
	'user/user-history.js': {
		entry: `${assets}/js/user/user-history.js`,
		output: `${dist}/js/`
	},
	'user/user-scoreboard.js': {
		entry: `${assets}/js/user/user-scoreboard.js`,
		output: `${dist}/js/`
	},
	'user/user-promo.js': {
		entry: `${assets}/js/user/user-promo.js`,
		output: `${dist}/js/`
	},
	'forum/forum-user.js': {
		entry: `${assets}/js/forum/forum-user.js`,
		output: `${dist}/js/`
	},
	'agent/agent-check.js': {
		entry: `${assets}/js/agent/agent-check.js`,
		output: `${dist}/js/`
	},
	'landing/kost/landing-kost.js': {
		entry: `${assets}/js/landing/kost/landing-kost.js`,
		output: `${dist}/js/`
	},
	'landing/landing-project.js': {
		entry: `${assets}/js/landing/landing-project.js`,
		output: `${dist}/js/`
	},
	'landing/landing-area.js': {
		entry: `${assets}/js/landing/landing-area.js`,
		output: `${dist}/js/`
	},
	'input/vacancy/input-vacancy.js': {
		entry: `${assets}/js/input/vacancy/input-vacancy.js`,
		output: `${dist}/js/`
	},
	'vacancy/landing-vacancy.js': {
		entry: `${assets}/js/vacancy/landing-vacancy.js`,
		output: `${dist}/js/`
	},
	'vacancy/detail-vacancy.js': {
		entry: `${assets}/js/vacancy/detail-vacancy.js`,
		output: `${dist}/js/`
	},
	'vacancy/company-profile.js': {
		entry: `${assets}/js/vacancy/company-profile.js`,
		output: `${dist}/js/`
	},
	'vacancy/company-vacancy.js': {
		entry: `${assets}/js/vacancy/company-vacancy.js`,
		output: `${dist}/js/`
	},
	'vacancy/company-landing.js': {
		entry: `${assets}/js/vacancy/company-landing.js`,
		output: `${dist}/js/`
	},
	'owner/owner-page.js': {
		entry: `${assets}/js/owner/owner-page.js`,
		output: `${dist}/js/`
	},
	'newest/newest-list.js': {
		entry: `${assets}/js/newest/newest-list.js`,
		output: `${dist}/js/`
	},
	'promo/all-apartment/promo-list-all-apartment.js': {
		entry: `${assets}/js/promo/all-apartment/promo-list-all-apartment.js`,
		output: `${dist}/js/`
	},
	'promo/all-job/promo-list-all-job.js': {
		entry: `${assets}/js/promo/all-job/promo-list-all-job.js`,
		output: `${dist}/js/`
	},
	'promo/graduation/graduation-photo.js': {
		entry: `${assets}/js/promo/graduation/graduation-photo.js`,
		output: `${dist}/js/`
	},
	'promo/graduation/graduation-upload.js': {
		entry: `${assets}/js/promo/graduation/graduation-upload.js`,
		output: `${dist}/js/`
	},
	'landing-modern/landing-agent.js': {
		entry: `${assets}/js/landing-modern/landing-agent.js`,
		output: `${dist}/js/`
	},
	'landing-modern/landing-standard.js': {
		entry: `${assets}/js/landing-modern/landing-standard.js`,
		output: `${dist}/js/`
	},
	'listing/listing-landing.js': {
		entry: `${assets}/js/listing/listing-landing.js`,
		output: `${dist}/js/`
	}
};

module.exports = sources;
