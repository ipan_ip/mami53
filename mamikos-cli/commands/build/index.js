const fs = require('fs').promises;
const inquirer = require('inquirer');
const commander = require('commander');
const path = require('path');

const yarnRunner = require('../../utils/yarn-runner');
const mixCommands = require('./constants/mix-commands');
const appSources = require('./constants/app-sources');

// Used to generate a file that contains resources to compile by Laravel-Mix
function createWebpackMixMainFile(sourceNames = []) {
	function generateMixes() {
		const filteredSources =
			sourceNames.length > 0 ? sourceNames : Object.keys(appSources);

		return filteredSources.reduce((result, source) => {
			const { entry, output } = appSources[source];

			return `${result}.js("${entry}", "${output}")`;
		}, '');
	}

	const content = `
		const mix = require('laravel-mix');
		require('laravel-mix-purgecss');
	
		const compile = () => { 
			mix
				${generateMixes()}
				.vue({ version: 2 })
				.purgeCss();
	 	}

		module.exports = { compile };
	`;

	try {
		const mixAppPath = path.join(__dirname, 'mix-sources/mix.app.js');

		return fs.writeFile(mixAppPath, content).then(() => {
			// ask prettier to format the generated file. It's useful when you want to check the generated file
			yarnRunner(`prettier ${mixAppPath} --write`, 'prettier');
		});
	} catch (err) {
		console.error('Error generating webpack.main.js file');
	}
}

module.exports = () => {
	const program = new commander.Command('build');

	program
		.option('--partial', 'build partial resources')
		.option(
			'--mode <mode>',
			'build mode. valid parameter: "dev" | "watch" | "watch-poll" | "hot" | "prod"',
			'dev'
		)
		.action(function({ partial, mode }) {
			if (!['dev', 'watch', 'watch-poll', 'hot', 'prod'].includes(mode))
				throw new Error('Invaild mode parameter!');

			if (partial) {
				// install inquirer's checkbox-plus plugin
				inquirer.registerPrompt(
					'checkbox-plus',
					require('inquirer-checkbox-plus-prompt')
				);

				inquirer
					.prompt([
						{
							type: 'checkbox-plus',
							name: 'answers',
							message: 'Press space to select the source(s)',
							highlight: true,
							searchable: true,
							source: function(_, input) {
								input = input || '';

								return new Promise(function(resolve) {
									const data = Object.keys(appSources).filter(item =>
										item.includes(input)
									);

									resolve(data);
								});
							}
						}
					])
					.then(({ answers }) => {
						createWebpackMixMainFile(answers).then(() => {
							yarnRunner(mixCommands[mode]);
						});
					})
					.catch(error => {
						console.log('=>', error);
					});
			} else {
				createWebpackMixMainFile().then(() => {
					yarnRunner(mixCommands[mode]);
				});
			}
		});

	return program;
};
