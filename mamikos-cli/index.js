const { program } = require('commander');

const build = require('./commands/build');
const notify = require('./commands/notify');

program.version('0.0.1', '-v, --version');

program.addCommand(build());
program.addCommand(notify());

program.parse(process.argv);
