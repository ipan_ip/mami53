<?php

return [
    'webhook' => [
        'faker_report' => env('SLACK_WEBHOOK_FAKER_REPORT', ''),
        'unread_owners' => env('SLACK_WEBHOOK_UNREAD_OWNERS', '')
    ]
];