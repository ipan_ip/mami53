<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => env('DEFAULT_STORAGE', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'visibility' => 'public',
        ],

        'upload' => [
            'driver' => 'local',
            'root'=> public_path(),
            'visibility' => 'public'
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('S3_KEY'),
            'secret' => env('S3_SECRET_KEY'),
            'region' => env('S3_REGION'),
            'bucket' => env('S3_BUCKET'),
            'visibility' => 'public',
            'options' => ['CacheControl' => 'max-age=315360000']
        ],

        's3-pay' => [
            'driver' => 's3',
            'key' => env('MAMIPAY_AWS_ACCESS_KEY_ID'),
            'secret' => env('MAMIPAY_AWS_SECRET_ACCESS_KEY'),
            'region' => env('MAMIPAY_AWS_DEFAULT_REGION'),
            'bucket' => env('MAMIPAY_AWS_BUCKET'),
            'url' => env('MAMIPAY_AWS_URL'),
            'options' => ['CacheControl' => 'max-age=315360000']
        ],

    ],

];
