<?php

return [
    'goldplus' => [
        'statistic' => [
            'enable-daily-gathering' => env('SCHEDULE_GOLDPLUS_STATISTIC_DAILY_ENABLED', env('APP_ENV') == 'production')
        ],
    ],
];
