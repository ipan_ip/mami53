<?php

return [
    'email' => env('CS_SUPPORT_EMAIL'),
    'email_sender' => env('CS_EMAIL_SENDER'),
];