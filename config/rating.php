<?php

/*
|--------------------------------------------------------------------------
| Rating Review / Room Configuration
|--------------------------------------------------------------------------
|
| Each kos / room has a rating. this variable is to determinate
| what scale we use on our mamikos system, 
| and this env variable helping us to do migration 
| from rating on scale 4 system to 5 rating scale system
|
*/

return [
    'scale' => env('RATING_SCALE', '4'),
];
