<?php

return [
    'listing' => [
        'bbk' => env('LISTING_BBK_CASHBACK'),
        'bbk_premium' => env('LISTING_PREMIUM_BBK_CASHBACK'),
    ],
    'goldplus' => [
        'gp1' => env('GOLDPLUS1_CASHBACK'),
        'gp2' => env('GOLDPLUS2_CASHBACK'),
        'gp3' => env('GOLDPLUS3_CASHBACK'),
    ] 
];