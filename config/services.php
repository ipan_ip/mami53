<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'zenziva' => [
        'userkey' => 'bfmrpj',
        'passkey' => 'lfU4L9DlntXNb32wWfdClApUxhbn6sCf'
    ],

    'verifikasi' => [
        'userkey' => 'l52hy0',
        'passkey' => '69ip8t6g0c1xoxQ4qk47aVMlHKOojP32'
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID','607562576051242'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET','5e3ca109d3462547843c8453e24e2534'),
        'redirect' => env('APP_URL').'/auth/facebook/callback'
    ],
    'google' => [
        // 'client_id' => '741381588229-o059lt1hoiprg1a89qt6b4kdbbcl96me.apps.googleusercontent.com',
        'client_id' => '326410828484-lqhq2s6s8c3emrga6iui60vhd2b5urv1.apps.googleusercontent.com',
        // 'client_secret' => 'drSdE3T6-KfD2_R6rSCGWuor',
        'client_secret' => 'IxuG7j4hxN-tt2q2P3w5KFnt',
        'redirect' => env('APP_URL').'/auth/google/callback',
        'developer_key' => 'AIzaSyD3oyqidcajKFfgBiHrNZv3S5Fa9Ho-xIA'
    ],
    'apple' => [
        'client_id' => env('APPLE_CLIENT_ID', 'com.git.applesignin.testing'),
        'client_secret' => env('APPLE_CLIENT_SECRET'),
        'redirect' => env('APP_URL').'/auth/apple/callback',
    ],
    'line'=>[
        // 'channel_access_token'=>'QdALhwYtAu0RC3OBq/WF+odbvg884GRVgCsqicl6FNnb/PYRbLCi/alkpX/amASMNxHokEUV5NeFU8xHAClip1BDscFLJyKbjFtNnzobW9bUXBVmxt9zLI2j9I3ImKn1Un4vf5X2XzKhOiZKdkYYigdB04t89/1O/w1cDnyilFU=',
        // 'channel_secret'=>'40ede07b94fb6d31f5b129ecbe0cd5e0'
        'channel_access_token'=>'k+vFQ2i4wW7I9UyG7cQ2SPa7mssutP4wKbv/Rq3vjeXwpBEj6AbUPdspOkJoYPFbA6k4mGHPLpQqyGeS0F+pGN/2UrbjdM4ZAkUpwEFpcUwq3W8vHAycEGBjukYWZNyxmDGJkqRy0kAle1IUeL2fagdB04t89/1O/w1cDnyilFU=',
        'channel_secret'=>'e74911ef910861c8e27853e7a6ccbaa0'
    ],
    'messenger' => [
        'app_access_token' => 'EAAIokyZBGPCoBAD8KF0Q1hfBPv0Y2ZCWVy8e2PdxGXnpDLr1dfkb7jlKZBpReOmGoZCdpVwd5ppgZCdCiBdvg5D2tpiEqqLYsvWYU5mzsKFQGtpvsoZCv8pIl444FIxIPWtaL5o1pPQUH2jRdzYQZAt2GHCriJefc3jNnePiyjwvAZDZD',
        'app_secret' => '5e3ca109d3462547843c8453e24e2534'
        // 'app_access_token' => 'EAAFylZAehX4kBAIz1JmEF6ZBTaGYzeTn13pHN4PDOGDwfVHZCZCLp2JcqUb3Mra8XL5LUeKslLnzkmRy1YICDOAzzq8Qo1jrt0WSgNlZAWcyemhzgVl5stWqZBhCFOHaSeAL6fSuTwGyzGrgT7kVl4B3l9IMuE6NLxIWqi0bfy7QZDZD',
        // 'app_secret' => '4853fe61fe921a52dfa0c0e58251462f'
    ],
    'currencyconverter' => [
        'api_key' => env('CURRENCYCONVERTER_API_KEY', '69cb7ade586985be3d35')
    ],
    'command' => [
        'allow_compensate_removing_benefit' => env('ALLOW_COMMAND_COMPENSATE_FOR_REMOVING_BENEFIT', false)
    ],
    'share' => [
        'base_url' => env('BASE_SHARE_URL', 'https://mamikos.com/')
    ],
    'osm' => [
        'host' => env('OSM_HOST', 'https://osm.mamikos.com')
    ],
    'midtrans' => [
        'server_key' => env('MIDTRANS_SERVER_KEY', 'SB-Mid-server-uQm-BoHOw1ejSvbipx0wSONp'),
        'is_production' => env('MIDTRANS_PRODUCTION', false)
    ],
];
