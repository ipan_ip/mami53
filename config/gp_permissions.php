<?php

/**
 * Static configuration of goldplus access list.
 * Please use associative format inside each `allow` key. So we can use isset for better performance.
 * isset(config('gp_permissions')[9]['allow']['r:detail']).
 *
 * WARN: Don't use config('kostlevel.id.goldplus1) as config facade cannot be used in config files, it will always return empty.
 */

use App\Enums\Goldplus\Permission\GoldplusPermission;

return [
    0 => [
        // Always allow non-gp
        'allow' => [
            GoldplusPermission::ROOM_UPDATE => 1,
            GoldplusPermission::ROOM_DELETE => 1,
            GoldplusPermission::ROOM_UNIT_READ => 1,
            GoldplusPermission::ROOM_UNIT_UPDATE => 1,
            GoldplusPermission::ROOM_UNIT_DELETE => 1,
            GoldplusPermission::PRICE_READ => 1,
            GoldplusPermission::PRICE_UPDATE => 1,
            GoldplusPermission::CLICK_UPDATE => 1,
            GoldplusPermission::CHAT_READ => 1,
            GoldplusPermission::SURVEY_READ => 1,
            GoldplusPermission::REVIEW_READ => 1,
            GoldplusPermission::BOOKING_UPDATE => 1,
        ],
    ],
    1 => [
        'allow' => [
            GoldplusPermission::ROOM_UPDATE => 1,
            GoldplusPermission::ROOM_DELETE => 1,
            GoldplusPermission::ROOM_UNIT_READ => 1,
            GoldplusPermission::ROOM_UNIT_UPDATE => 1,
            GoldplusPermission::ROOM_UNIT_DELETE => 1,
            GoldplusPermission::PRICE_READ => 1,
            GoldplusPermission::PRICE_UPDATE => 1,
            GoldplusPermission::CLICK_UPDATE => 1,
            GoldplusPermission::CHAT_READ => 1,
            GoldplusPermission::SURVEY_READ => 1,
            GoldplusPermission::REVIEW_READ => 1,
            GoldplusPermission::BOOKING_UPDATE => 1,
        ],
    ],
    2 => [
        'allow' => [
            GoldplusPermission::ROOM_UPDATE => 1,
            GoldplusPermission::ROOM_DELETE => 1,
            GoldplusPermission::ROOM_UNIT_READ => 1,
            GoldplusPermission::ROOM_UNIT_UPDATE => 1,
            GoldplusPermission::ROOM_UNIT_DELETE => 1,
            GoldplusPermission::PRICE_READ => 1,
            GoldplusPermission::PRICE_UPDATE => 1,
            GoldplusPermission::CLICK_UPDATE => 1,
            GoldplusPermission::CHAT_READ => 1,
            GoldplusPermission::SURVEY_READ => 1,
            GoldplusPermission::REVIEW_READ => 1,
            GoldplusPermission::BOOKING_UPDATE => 1,
        ],
    ],
    3 => [],
    4 => [
        'allow' => [
            GoldplusPermission::ROOM_UNIT_READ => 1,
            GoldplusPermission::ROOM_UNIT_UPDATE => 1,
            GoldplusPermission::ROOM_UNIT_DELETE => 1,
            GoldplusPermission::PRICE_READ => 1,
            GoldplusPermission::PRICE_UPDATE => 1,
        ],
    ],
];
