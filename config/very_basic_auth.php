<?php

/**
 * Configuration for the "HTTP Very Basic Auth"-middleware
 */
return [
    // Username
    'user' => 'jack',

    // Password
    'password' => 'sparrow',

    // Environments where the middleware is active
    'envs' => [
        'production',
        'anggit',
        'development',
        'jambu',
        'jambu2',
        'putrali',
        'putrali2',
        'semangka',
        'semangka2',
        'songturu',
        'songturu2',
        'local',
    ],

    // Message to display if the user "opts out"/clicks "cancel"
    'error_message' => 'Non-authorized Access!',

    // If you prefer to use a view with your error message you can uncomment "error_view".
    // This will superseed your default response message
    // 'error_view'        => 'very_basic_auth::default'
];
