<?php

return [
    'limit' => [
        'daily_question' => env('CHAT_DAILY_QUESTION_LIMIT', '20'),
        'daily_question_room' => env('CHAT_DAILY_QUESTION_ROOM_LIMIT', '5'),
    ],

    'question' => [
        'booking_question_id' => env('CHAT_BOOKING_QUESTION_ID', '29')
    ],
    'goldplus' => [
        'dedicated_consultant_ids' => env('CHAT_GOLDPLUS_DEDICATED_CONSULTANT_IDS', ''),
        'default_gp3_cs_id' => env('DEFAULT_GP3_CS_ID', '99447427')
    ],
];
