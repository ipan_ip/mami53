<?php

return [
    'id' => [
        'oke' => env('OKE_LEVEL_ID'),
        'super' => env('SUPER_LEVEL_ID'),
        'goldplus1' => env('GOLDPLUS1_LEVEL_ID'),
        'goldplus2' => env('GOLDPLUS2_LEVEL_ID'),
        'goldplus3' => env('GOLDPLUS3_LEVEL_ID'),
        'goldplus4' => env('GOLDPLUS4_LEVEL_ID'),
        'goldplus1_promo' => env('GOLDPLUS1_PROMO_LEVEL_ID'),
        'goldplus2_promo' => env('GOLDPLUS2_PROMO_LEVEL_ID'),
        'goldplus3_promo' => env('GOLDPLUS3_PROMO_LEVEL_ID'),
        'goldplus4_promo' => env('GOLDPLUS4_PROMO_LEVEL_ID'),
        'new_goldplus1' => env('NEW_GOLDPLUS1_LEVEL_ID'),
        'new_goldplus2' => env('NEW_GOLDPLUS2_LEVEL_ID'),
        'new_goldplus3' => env('NEW_GOLDPLUS3_LEVEL_ID'),
        'oyo' => env('OYO_LEVEL_ID'),
        'aparkost' => env('APARKOST_LEVEL_ID'),
        'mamirooms' => env('MAMIROOMS_LEVEL_ID')
    ],
    'ids' => [
        'goldplus1' => env('GOLDPLUS1_LEVEL_IDS'),
        'goldplus2' => env('GOLDPLUS2_LEVEL_IDS'),
        'goldplus3' => env('GOLDPLUS3_LEVEL_IDS'),
        'goldplus4' => env('GOLDPLUS4_LEVEL_IDS'),
    ],
    'campaign' => [
        'internal' => env('KOST_LEVEL_INTERNAL_CAMPAIGN')
    ],
];
