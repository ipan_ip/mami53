<?php

use App\Libraries\StringHelper;

if (! function_exists('asset_url')) {
    /**
     * Get the asset path and translate it to right destination
     *
     * @param  string  $path
     * @param  string  $isVersioned
     * @param  string  $isHighPriority
     * @return \Illuminate\Support\HtmlString|string
     *
     * @throws \Exception
     */
    function asset_url($path, $isVersioned = false, $isHighPriority = false) {
        $resultPath = '/' . $path;
        $cdnUrl  = config('app.static_asset2_url');
        $env     = config('app.env');

        // Call mix function for versioned file
        if ($isVersioned) {
          $resultPath = mix($path);
        }

        // Use static_asset_url config for high priority assets
        if ($isHighPriority) {
          $cdnUrl = config('app.static_asset_url');
        }

        // Reference CDN assets only in production or staging environemnt.
        // In other environments, we should reference locally built assets.
        if ($cdnUrl && StringHelper::startsWith($env, 'local') === false) {
            $resultPath = $cdnUrl . $resultPath;
        }

        return $resultPath;
    }
}

if (! function_exists('mix_url')) {
  /**
   * Get the asset path for mix / versioned file
   *
   * @param  string  $path
   * @return \Illuminate\Support\HtmlString|string
   *
   * @throws \Exception
   */
  function mix_url($path) {
      return asset_url($path, true, true);
  }
}