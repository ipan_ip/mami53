<?php

return array(

  /*
  |--------------------------------------------------------------------------
  | Api Setting, Key and Token
  |--------------------------------------------------------------------------
  |
  | The Place to Put All Api Setting, Key and Token
  | both External and Internal Api Server
  |
  */

  /**
   * Debug Level
   * 0 : Show Default Laravel Debug Tracker (if debug is set to true)
   * 1 : Show Error Message Only
   * 2 : Show Enough Debug
   * 3 : Show Complete Debug with Debugbar
   */

  'debug_level' => 1,

  'http_code' => array(
    'default' => 200,
  ),

  /**
   * Avoid Replay Attacks
   *
   * The Time Limit of The Same Signature Can Make The Same Request
   * So the server can decide if this is an "old" request, and deny it,
   * or still "new" request within acceptable bounds (time limit), and continue the request
   */
  'signature_time_limit' => 5, // in minutes

  /**
   * Hashing
   * Api Key and Access Token above is Produced Using
   * hash_hmac('sha256', Str::random(10) . date('Y-m-d H:i:s') . 'data', Config::get('app.key'));
   */

  // Cron Jobs Access Token
  'cron_access_token'    => 'c372771edafb7890b211cb44580964db99dc028cf0451f03a9518de14d4de254',

  // Devel Access Token
  'devel_access_token'   => '858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6',

  // Secret Key for Generate Signature every Making a Request, Both Client and Server
  'secret_key'           => '596870d89f4daf33406f69fd1aa200030d7d859341b0c32b2977e0333d0200ec',

  // Guest Token
  'guest_token'          => '01e42ad0e6cb7fea9c28f46a52fcf120a7a838475ff7244c287be8be6377e95a',

  'api_secret_key'       => 'dad1b6d8341d782e77c781b2c296e3ac',

  /**
   * Log
   */

  'log' => array(
    'enable' => true,
  ),

  /**
   * Media
   */

  'media' => array(
    'random_name_length' => 8, // character

    'cdn_url'            => env('STATIC_RESOURCE_CDN_URL', 'https://static.mamikos.com/'),
    'folder_backup'      => env('MEDIA_BACKUP_FOLDER', '/mnt/mamikos-images-live2/mamikos/'),
    'folder_data'        => 'uploads/data',
    'folder_cache'       => 'uploads/cache/data',//'uploads/cache/data',
    'folder_tag_icon'    => 'uploads/tags',
    'folder_benefit_icon' => 'uploads/benefits',
    'folder_reviews'     => 'uploads/reviews',
    'folder_cv'          => 'uploads/cv',
    'folder_scrap_data'  => 'uploads/sink',
    'folder_photobooth'  => 'uploads/photobooth',
    'size' => array(
      'user'     => array(
        'small'  => '240x320',
        'medium' => '360x480',
        'large'  => '540x720',
      ),
      'designer' => array(
        'small'  => 'x',
        'medium' => 'x',
        'large'  => 'x',
      ),
      'style'    => array(
        'small'   => '240x320',
        'medium'  => '360x480',
        'large'   => '540x720',
        'facebook'=> '600x600'
      ),
      'round_style'    => array(
        'small'   => '360x480',
        'medium'  => '1080x1440',
        'large'   => '2160x1080'
      ),
      'review'    => array(
        'small'   => '240x320',
        'medium'  => '360x480',
        'large'   => '540x720'
      ),
      'photobooth' => array(
        'small'   => '240x320',
        'medium'  => '360x480',
        'large' => 'realxreal'
      ),
      'event' => array(
       'small' => '240x320',
       'medium' => '360x480',
       'large' => '540x720'
      ),
      'company'   => [
        'small'   => '80x80',
        'medium'  => '150x150',
        'large'   => '300x300'
      ],
        'flash_sale' => [
            'small' => '360x480',
            'medium' => '1080x1440',
            'large' => '2160x1080'
        ],
      'sanjunipero_header_desktop'   => [
        'small'   => '341x113',
        'medium'  => '1024x341',
        'large'   => '2048x682'
      ],
      'sanjunipero_header_mobile'   => [
        'small'   => '360x480',
        'medium'  => '1080x1440',
        'large'   => '2160x2880'
      ],
    ),
    'real_size'=>array(
      'user'     => array(
        'small'  => '240x320',
        'medium' => '720x405',
        'large'  => '960x540',
      ),
      'designer' => array(
        'small'  => 'x',
        'medium' => 'x',
        'large'  => 'x',
      ),
      'style'    => array(
        'small'   => '240x320',
        'medium'  => '720x405',
        'large'   => '1280x720',
        // 'large'   => '1920x1080',
        'facebook'=> '600x600'
      ),
      'round_style'    => array(
        'small'   => '360x480',
        'medium'  => '1080x1440',
        'large'   => '2160x1080'
      ),
      'review'    => array(
        'small'   => '240x320',
        'medium'  => '360x480',
        'large'   => '540x720'
      ),
      'photobooth' => array(
        'small'   => '240x320',
        'medium'  => '360x480',
        'large' => 'realxreal'
      ),
      'event' => array(
        'small' => '313x133',
        'medium' => '377x159',
        'large' => '817x346'
      ),
      'company'   => [
        'small'   => '80x80',
        'medium'  => '150x150',
        'large'   => '300x300'
      ],
        'flash_sale' => [
            'small' => '360x480',
            'medium' => '1080x1440',
            'large' => '2160x1080'
        ],
      'sanjunipero_header_desktop'   => [
        'small'   => '341x113',
        'medium'  => '1024x341',
        'large'   => '2048x682'
      ],
      'sanjunipero_header_mobile'   => [
        'small'   => '360x480',
        'medium'  => '1080x1440',
        'large'   => '2160x2880'
      ],
    )
  ),


  /**
  * Admin ID
  *
  */

  'admin_ids' => array(
    1,
    2
  ),


  /**
   * Registration
   */

  'verification_code' => array(
    // Message
    'message_template'    => 'Hair GIT verification code is : {verification_code}',
    // Character Length
    'string_length'       => 6,
    // Type of The String
    'string_type'         => 'nozero',
    // Interval The Verification Should ReGenerate
    'regenerate_interval' => 10, // seconds
    // Limit the SMS to be Sent to the Same Phone Number
    'sent_limit'          => 10,
    // Threshold to Sent via SMS (avoid Bulking Sent)
    'sent_threshold'      => 60, // seconds
  ),

  /**
   * SMS
   */

  /**
  * Billing Decrement
  */
  'ads_billing_unit'=>200,
  'ads_billing_chat'=>2000,

  /**
   * Facebook
   * @link developers.facebook.com hair git
   */

    'facebook' => array(
      'default' => 'strories',
      'strories' => array(
        'app_id' => env('FB_APP_ID'),
        'app_secret' => env('FB_APP_SECRET'),
        'default_token' => env('FB_DEFAULT_TOKEN'),
        //'app_id'     => '789958467741995',
        //'app_secret' => '2bd6c5e3f4780c119c37e9da7b8cc7e9',
      ),
      'testos' => array(
        'app_id'     => '1608029039420076',
        'app_secret' => '61d3b80d201a2ba85b6fa4af6eb637e2',
        //'app_id'     => '789958467741995',
        //'app_secret' => '2bd6c5e3f4780c119c37e9da7b8cc7e9',
      ),
      'girls' => array(
        'app_id'     => '609091202528283',
        'app_secret' => '6b4b1098829c8dacf39bcdc01e11be89',
        //'app_id'     => '391597821005635',
        //'app_secret' => '2c1df50c8e31cdb6408727c630e78249',
      ),
      'mami' => array(
        'app_id'     => '1672916352954288',
        'app_secret' => '610262414a804ae267faa02a7dc1b5e1',
        //'app_id'     => '391597821005635',
        //'app_secret' => '2c1df50c8e31cdb6408727c630e78249',
      ),
    ),

  'kakao' => array(
    'token' => '',
  ),

  'google' => array(
    'gcm' => array(
      'api_key' => 'AIzaSyCQuHWAxk_kXzFB6Ee4jT_ox_ciF6dLTl0',
//       'api_key' => 'AIzaSyBw6zUfbhbNlZLkp7ibtrLSa9wWOVEySkU',
    ),
    'plus' => array(
      'web_application' => array(
        'client_id'     => '1041920978470-qsr10p1pkbe7of5g9m6ob3ef9vq3u56s.apps.googleusercontent.com',
        'client_secret' => 'l_6MiJHU5aUfB802zpSW_Fng',
      )
    ),
    'android_publisher' => array(
      'service_account' => array(
        'client_id'            => '1041920978470-6g4933rsmk4k7srkkecvpad3hhs8h9q2.apps.googleusercontent.com',
        'service_account_name' => '1041920978470-6g4933rsmk4k7srkkecvpad3hhs8h9q2@developer.gserviceaccount.com',
        'key_file_location'    => 'key/google/service_account_key.p12',
        'scope'                => 'https://www.googleapis.com/auth/androidpublisher',
      )
    )
  ),

  'mamipay' => array(
    'url'     => env('MAMIPAY_URL', 'https://pay.mamikos.com'),
    'token'   => env('MAMIPAY_AUTH_TOKEN', 'GIT-PAY 5A6709585C100E99D127A629AD637A517B71AECDCE4B70AD5DE0CB966851294C'),
    'invoice' => env('MAMIPAY_INVOICE_URL', 'https://pay.mamikos.com/invoice'),
  ),

  'infobip' => array(
    'endpoint' => env('INFOBIP_ENDPOINT', 'https://api-id1.infobip.com/'),
    'auth_header' => env('INFOBIP_AUTH_HEADER', ''),
  ),
  
  'cs' => array(
      'phone_number' => '0819-2974-9399'
  ),

  'app-identifier' => [
    'android-hash' => 'b0lUOxG7rtF',
  ],
);
