<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FCM URL
    |--------------------------------------------------------------------------
    | Provide FCM url for send push notification
    */
    'url' => env('FCM_URL', 'https://fcm.googleapis.com/fcm/send'),

    /*
    |---------------------------------
    -----------------------------------------
    | FCM SERVER KEY
    |--------------------------------------------------------------------------
    | Provide FCM Server Key for send push notification
    */
    'server_key' => env('FCM_SERVER_KEY', '')
];