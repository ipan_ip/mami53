<?php

return [
    'command_enabled' => env('AREA_GENERATE_GEOCODE_ENABLED', false),
    'default_max' => env('AREA_GENERATE_GEOCODE_DEFAULT_MAX', 1000),
    'dry_run_sample_size' => env('AREA_GENERATE_GEOCODE_DRY_RUN_SIZE', 100)
];