<?php

return [
    'features' => [
        'provide_tenant_phone_number' => [
            'on' => env('CLOSEBETA_PROVIDE_TENANT_PHONE_NUMBER_ON', false),
            'user_ids' => json_decode(env('CLOSEBETA_PROVIDE_TENANT_PHONE_NUMBER_USER_IDS', "[]")),
        ]
    ],
];