<?php

return [
    'vpn' => [
        'mamivpn' => env('MAMIVPN_IP', '127.0.0.1')
    ]
];
