<?php

return [
    'id' => [
        'goldplus1' => env('GOLDPLUS1_ROOM_LEVEL_ID'),
        'goldplus2' => env('GOLDPLUS2_ROOM_LEVEL_ID'),
        'goldplus3' => env('GOLDPLUS3_ROOM_LEVEL_ID'),
        'goldplus4' => env('GOLDPLUS4_ROOM_LEVEL_ID'),
        'new_goldplus1' => env('NEW_GOLDPLUS1_ROOM_LEVEL_ID'),
        'new_goldplus2' => env('NEW_GOLDPLUS2_ROOM_LEVEL_ID'),
        'new_goldplus3' => env('NEW_GOLDPLUS3_ROOM_LEVEL_ID')
    ]
];
