<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Encryption Keys
    |--------------------------------------------------------------------------
    |
    | Passport uses encryption keys while generating secure access tokens for
    | your application. By default, the keys are stored as local files but
    | can be set via environment variables when that is more convenient.
    |
    */

    'private_key' => env('PASSPORT_PRIVATE_KEY'),

    'public_key' => env('PASSPORT_PUBLIC_KEY'),

    'proxy' => [
        'client_id' => env('PASSPORT_PROXY_CLIENT_ID'),
        'client_secret' => env('PASSPORT_PROXY_CLIENT_SECRET'),
    ],

    'expire' => [
        'access_token' => env('PASSPORT_ACCESS_TOKEN_EXPIRE', 30), //minutes
        'refresh_token' => env('PASSPORT_REFRESH_TOKEN_EXPIRE', 10), //day
        'personal_access_token' => env('PASSPORT_PERSONAL_ACCESS_TOKEN_EXPIRE', 10), //day
    ],

    'client_id' => env('PASSPORT_CLIENT_ID', null),
    'client_secret' => env('PASSPORT_CLIENT_SECRET', null),
];
