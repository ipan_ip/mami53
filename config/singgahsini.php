<?php

return [
    'ids' => explode(',', env('SINGGAHSINI_IDS', '')),
    'facilities' => explode(',', env('SINGGAHSINI_FACILITY', ''))
];