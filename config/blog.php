<?php

return [
    'info' => [ // Main Blog https://mamikos.com/info
        'api_base_url' => env('BLOG_INFO_API_BASE_URL', 'https://blog-api.mamikos.com'),
    ],
];