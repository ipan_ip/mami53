<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Debug blacklist
    |--------------------------------------------------------------------------
    |
    | When an exception is uncaught and the APP_DEBUG environment variable is true,
    | the debug page will show all environment variables and their contents.
    | so for security reason we need to blacklist all expose .env variable
    |
    */

    'debug_blacklist' => [
        '_COOKIE' => array_keys($_COOKIE),
        '_SERVER' => array_keys($_SERVER),
        '_ENV' => array_keys($_ENV),
    ],

    'trace_in_sql' => [
        'enabled' => env('APP_TRACE_IN_SQL_ENABLED', false),
        'prefix' => env('APP_TRACE_IN_SQL_PREFIX', ''),
    ],

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Asia/Jakarta',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    'default_icon_path' => '/general/img/manifest/android-chrome-192x192.png',

    'short_url' => env('SHORT_URL', 'https://mamikos.com/s/'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,

        /*
         * Package Service Providers...
         */
        Laravel\Tinker\TinkerServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        Cviebrock\EloquentSluggable\ServiceProvider::class,
        Davibennun\LaravelPushNotification\LaravelPushNotificationServiceProvider::class,
        Jenssegers\Agent\AgentServiceProvider::class,
	    Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class,
        Prettus\Repository\Providers\RepositoryServiceProvider::class,
        Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
        SocialiteProviders\Manager\ServiceProvider::class,
        Barryvdh\Debugbar\ServiceProvider::class,
        PulkitJalan\Google\GoogleServiceProvider::class,
	    Elibyy\TCPDF\ServiceProvider::class,
        Appstract\Opcache\OpcacheServiceProvider::class,
        App\Providers\CustomBladeServiceProvider::class,
        App\Providers\SendBirdServiceProvider::class,
        Zizaco\Entrust\EntrustServiceProvider::class,
        Barryvdh\DomPDF\ServiceProvider::class,
        Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class,
        Olssonm\VeryBasicAuth\VeryBasicAuthServiceProvider::class,
        Jenssegers\Mongodb\MongodbServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,
        App\Providers\ABTestServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\LibraryServiceProvider::class,
        App\Providers\HorizonServiceProvider::class,
        App\Providers\PassportServiceProvider::class,
        App\Providers\RepositoryServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\TelescopeServiceProvider::class,
        App\Providers\WhatsAppBusinessServiceProvider::class,
        App\Providers\WhitelistFeatureServiceProvider::class,
        Superbalist\LaravelPubSub\PubSubServiceProvider::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Image' => Intervention\Image\Facades\Image::class,
        'Input' => \Illuminate\Support\Facades\Input::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,
        'HTML' => Collective\Html\HtmlFacade::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => App\Facades\MamikosStorage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,

        'Media' => App\Entities\Media\Media::class,
        'Notif' => App\Entities\Notif\Notif::class,
        'PushNotification' => Davibennun\LaravelPushNotification\Facades\PushNotification::class,
        'Socialite' => Laravel\Socialite\Facades\Socialite::class,
		'Bugsnag' => Bugsnag\BugsnagLaravel\Facades\Bugsnag::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'Google' => PulkitJalan\Google\Facades\Google::class,
        'Agent' => Jenssegers\Agent\Facades\Agent::class,
        'PDF' => Elibyy\TCPDF\Facades\TCPDF::class,
        'Entrust'   => Zizaco\Entrust\EntrustFacade::class,
        'StatsLib' => App\Libraries\StatisticsLibrary::class,

        'Excel' => Maatwebsite\Excel\Facades\Excel::class,

        'SendBird' => App\Facades\SendBirdFacade::class,
        'ABTestClient' => App\Facades\ABTestClientFacade::class,
        'WhatsAppBusiness' => App\Facades\WhatsAppBusinessFacade::class,
        'WhitelistFeature' => App\Facades\WhitelistFeatureFacade::class,
        'ASDecoder' => App\Facades\AppleSignIn\ASDecoder::class,
    ],

    'static_asset_url' => env('MIX_STATIC_ASSET_URL'),
    'static_asset2_url' => env('MIX_STATIC_ASSET2_URL'),
];
