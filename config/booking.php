<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Prebook Check-In Max Month
    |--------------------------------------------------------------------------
    |
    | Provide to client side iOS, Android, and Web
    | Value of maximal month, tenant can check-in before submit booking
    |
    */

    'prebook_checkin_max_month' => (int) env('BOOKING_PREBOOK_CHECKIN_MAX_MONTH', 3),

    /*
    |--------------------------------------------------------------------------
    | Prebook Check-In Normal Month
    |--------------------------------------------------------------------------
    |
    | Provide to client side iOS, Android, and Web
    | Value of normal month
    |
    */

    'prebook_checkin_normal_month' => (int) env('BOOKING_PREBOOK_CHECKIN_NORMAL_MONTH', 2),

    /*
    |--------------------------------------------------------------------------
    | Prebook Owner Premium
    |--------------------------------------------------------------------------
    |
    | Configuration for prebook and owner is premium
    |
    */

    'prebook_premium_active' => (bool) env('BOOKING_PREBOOK_PREMIUM_ACTIVE', false),

    /*
    |--------------------------------------------------------------------------
    | Prebook Premium Max
    |--------------------------------------------------------------------------
    |
    | Provide to limit owner premium have pre-booking
    | Value of maximal booking with BOOKING_PREBOOK_CHECKIN_MAX_MONTH
    */

    'prebook_premium_max' => (int) env('BOOKING_PREBOOK_PREMIUM_MAX', 1000000),


    /*
    |--------------------------------------------------------------------------
    | Scheduler Owner Confirm Booking
    |--------------------------------------------------------------------------
    |
    | Provide scheduler notification for owner to confirm booking
    | Value is an array with a unit of minutes
    */
    'scheduler_owner_confirm_booking_notification' => [
        (int) env('BOOKING_SCHEDULER_OWNER_CONFIRM_BOOKING_NOTIFICATION_FIRST', 300), // value for first scheduler notification
        (int) env('BOOKING_SCHEDULER_OWNER_CONFIRM_BOOKING_NOTIFICATION_SECOND', 1380), // value for second scheduler notification
        (int) env('BOOKING_SCHEDULER_OWNER_CONFIRM_BOOKING_NOTIFICATION_THIRD', 5700), // value for third scheduler notification
    ],

    /*
    |--------------------------------------------------------------------------
    | Booking with Calendar
    |--------------------------------------------------------------------------
    |
    | Configuration for calendar to show and hide
    | when want to create booking
    */
    'is_booking_with_calendar' => (bool) env('BOOKING_WITH_CALENDAR', false),

    /*
    |--------------------------------------------------------------------------
    | Owner add tenant via link
    |--------------------------------------------------------------------------
    |
    | Configuration for owner create link
    | for tenant fill it
    */
    'is_allow_dbet_tenant' => (bool) env('BOOKING_ALLOW_OWNER_ADD_TENANT_LINK', false),

    /*
    |--------------------------------------------------------------------------
    | Admin - Booking Data Menu
    |--------------------------------------------------------------------------
    |
    | Configuration for admin booking data menu
    */
    'admin' => [
        'tag' => [
            'survey_required_id' => env('SURVEY_REQUIRED_TAG_ID', null),
            'dp_required_id' => env('DP_REQUIRED_TAG_ID', null),
            'prorate_id' => env('PRORATE_TAG_ID', null),
            'students_only_id' => env('STUDENTS_ONLY_TAG_ID', null),
            'employees_only_id' => env('EMPLOYEES_ONLY_TAG_ID', null),
            'security_id' => env('SECURITY_TAG_ID', null)
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Booking restriction
    |--------------------------------------------------------------------------
    |
    | Configuration for time setting restriction booking check-in time
    */
    'booking_restriction' => [
        'is_active' => (bool) env('BOOKING_RESTRICTION_IS_ACTIVE', false),
        'available_time_start' => env('BOOKING_HOUR_OPEN', "06:00:00"),
        'available_time_end' => env('BOOKING_HOUR_CLOSE', "18:00:00"),
    ],
];
