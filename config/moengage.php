<?php

return [
    'api_base_url' => env('MOENGAGE_API_BASE_URL', 'https://api.moengage.com'),
    'api_url' => env('MOENGAGE_API_URL', 'https://pushapi.moengage.com/v2'),
    'endpoint_notifications' => env('MOENGAGE_ENDPOINT_NOTIFICATIONS', '/transaction/sendpush'),
    'app_id' => env('MOENGAGE_APP_ID'),
    'app_secret' => env('MOENGAGE_APP_SECRET'),
    'campaign_name' => env('MOENGAGE_CAMPAIGN_NAME', 'System'),
    'data_api_id' => env('MOENGAGE_DATA_API_ID'),
    'data_api_key' => env('MOENGAGE_DATA_API_KEY'),
];
