<?php
return [
    'api_key' => env('AB_TEST_API_KEY', ''),
    'authorization' => env('AB_TEST_AUTHORIZATION', ''),
    'base_url' => env('AB_TEST_BASE_URL', ''),
    'path_url' => env('AB_TEST_PATH_URL', ''),

    'base_path' => [
        'dashboard_api' => env('AB_TEST_DASHBOARD_BASE_PATH', '/beta/abtest-platform/')
    ],
    'experiment' => [
        'premium' => env('PREMIUM_AB_TEST_EXPERIMENT_ID', 0),
        'ox' => [
            'force_bbk' => [
                'id' => env('OX_FORCE_BBK_AB_TEST_EXPERIMENT_ID', null),
                'use_varian' => env('OX_FORCE_BBK_AB_TEST_USE_VARIAN', false),
                'varian' => env('OX_FORCE_BBK_AB_TEST_PREFERRED_VARIAN', 'control'),
            ],
        ]
    ]
];