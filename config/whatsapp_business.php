<?php

return [
    'auth_header'               => env('INFOBIP_WA_BUSINESS_AUTH_HEADER', ""),
    'endpoint_url'              => env('INFOBIP_WA_BUSINESS_ENDPOINT', "https://mjvg2.api.infobip.com/"),
    'scenario_key'              => env('INFOBIP_WA_SCENARIO_KEY', ""),
    'scenario_name'             => 'MAMIKOS WhatsApp Notification',
    'sender_number'             => '6285722221300',
	'namespace'					=> '05d08a30_5ea8_4cb5_9707_9f4d71419013',
	'language'					=> 'id',
];
