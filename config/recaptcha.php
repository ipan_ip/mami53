<?php

return [
    'RECAPTCHA_BASE_URL' => (!empty('RECAPTCHA_BASE_URL')) ? env('RECAPTCHA_BASE_URL') : 'https://www.google.com/recaptcha/api/siteverify',
    'RECAPTCHA_SECRET_KEY' => (!empty(env('RECAPTCHA_SECRET_KEY'))) ? env('RECAPTCHA_SECRET_KEY') : '6LeVJNUUAAAAAGLUwrrCjd21GEOgY4Wd7yL-esj5',
];
