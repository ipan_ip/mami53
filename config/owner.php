<?php

return [
    'dashboard_url' => env('OWNER_DASHBOARD_URL', '/ownerpage'),
    'oauth2_domain' => env('OAUTH2_DOMAIN', 'mamikos.com'),
    'notification' => [
        'unread_chat' => [
            'on' => env('OWNER_NOTIFY_UNREAD_CHAT_ON', true),
            'option' => env('OWNER_NOTIFY_UNREAD_CHAT_OPTION', ''),
            'schedule' => env('OWNER_NOTIFY_UNREAD_CHAT_SCHEDULE', '0 19 * * *')
        ],
        'unregistered_bbk' => [
            'on' => env('OWNER_NOTIFY_UNREGISTERED_BBK_ON', true),
            'chunk' => env('OWNER_NOTIFY_UNREGISTERED_BBK_CHUNK', 100),
        ],
        'update_kost_popover_active' => env('OWNER_UPDATE_KOST_POPOVER_ACTIVE', false),
    ],
    'input' => [
        'auto-ready-to-verify-delay' => env('OWNER_AUTO_ASK_KOST_VERIFICATION_DELAY', 15),
    ]
];
