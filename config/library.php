<?php

return [
    /**
     * By default will enable infobip faker if the environment app is not production.
     * Can be overrided by setting the env LIBRARY_INFOBIP_FAKER_ENABLED=false
     */
    'infobip' => [
        'faker_enabled' => env('LIBRARY_INFOBIP_FAKER_ENABLED', config('app.env') !== 'production'),
    ],
];
