<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Max Per Page
    |--------------------------------------------------------------------------
    */
    'max_per_page' => env('KOSTSEARCHER_MAX_PER_PAGE', 20),

    /*
    |--------------------------------------------------------------------------
    | Max Load More Page
    |--------------------------------------------------------------------------
    |
    | SECURITY: anti scraping feature.
    | Loading more pages than max_load_more_page, we will give already provided data randomly.
    |
    */
    'max_load_more_item' => env('KOSTSEARCHER_MAX_LOAD_MORE_ITEM', 200),

    'kost' => [
        'index_name' => env('KOSTSEARCHER_KOST_INDEX_NAME', 'active-kosts'),
    ],
    
    'apartment' => [
        'index_name' => env('KOSTSEARCHER_APARTMENT_INDEX_NAME', 'active-apartments'),
    ],

    'beta' => [
        'reroute_enabled' => env('KOSTSEARCHER_BETA_REROUTE_ENABLED', false),
        // when it's true: KOSTSEARCHER_BETA_USER_ID_ENDINGS and KOSTSEARCHER_BETA_ALLOW_TESTER_ONLY will be ignored.
        'allow_anonymous' => env('KOSTSEARCHER_BETA_ALLOW_ANONYMOUS', false),
        'reroute_user_id_endings' => env('KOSTSEARCHER_BETA_USER_ID_ENDINGS', '[]'),
        'allow_tester_only' => env('KOSTSEARCHER_BETA_ALLOW_TESTER_ONLY', false),
    ],

    'logger' => [
        'enabled' => (bool) env('KOSTSEARCHER_LOGGER_ENABLED', false),
        'index_name' => env('KOSTSEARCHER_LOGGER_INDEX_NAME', 'index-kost-job-logger'),
    ]
];