<?php

return [
    'paths' => ["*"],
    'allowed_methods' => ['*'],
    'allowed_origins' => ['*'],
    'allowed_headers' => ['*'],
    'max_age' => 86400,
];
