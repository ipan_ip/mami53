module.exports = {
	roots: ['<rootDir>'],
	testMatch: ['<rootDir>/tests-fe/**/*.spec.js'],
	testPathIgnorePatterns: [
		'<rootDir>/node_modules/*',
		'resources/assets/js/_controller/*'
	],
	moduleFileExtensions: ['vue', 'js', 'json'],
	collectCoverage: false,
	collectCoverageFrom: [
		'<rootDir>/resources/assets/js/*.js',
		'<rootDir>/resources/assets/js/**/*.{vue,js}',
		'!<rootDir>/resources/assets/js/_controller/**',
		'!<rootDir>/resources/assets/js/@vendor/**',
		'!<rootDir>/resources/assets/js/agent/**',
		'!<rootDir>/resources/assets/js/landing-modern/**',
		'!<rootDir>/resources/assets/js/login/**',
		'!<rootDir>/resources/assets/js/user/components/user-profile/**',
		'!<rootDir>/resources/assets/js/user/components/user-promo/**',
		'!<rootDir>/resources/assets/js/input/components/input-kost/**',
		'!<rootDir>/resources/assets/js/find/**',
		'!<rootDir>/resources/assets/js/promo/all-kost/**',
		'!<rootDir>/resources/assets/js/promo/graduation/**',
		'!<rootDir>/resources/assets/js/newest/**',
		'!<rootDir>/resources/assets/js/forum/**',
		// This component only used on forum page
		'!<rootDir>/resources/assets/js/@global/login-user-jquery.js',
		'!<rootDir>/resources/assets/js/vacancy/**',
		'!<rootDir>/resources/assets/js/listing/**',
		'!**/node_modules/**',
		'!**/public/**',
		'!<rootDir>/resources/assets/js/_consultant/**/data/*',
		'!<rootDir>/resources/assets/js/_consultant/router/paths/*',
		// Currently exclude this from coverage because this code will be removed on CORE-952
		'!<rootDir>/resources/assets/js/@global/components/content/*',
		'!<rootDir>/resources/assets/js/@global/components/countdown/*',
		/*
		 * Exclude _landing/house because /kontrakan/:slug page isn't managed anymore.
		 * We don't need to create the test files unless business team decided use kontrakan feature again
		 */
		'!<rootDir>/resources/assets/js/_landing/house/**',
		// Exclude entry files
		'!<rootDir>/resources/assets/js/main.js',
		'!<rootDir>/resources/assets/js/starter.js',
		'!<rootDir>/resources/assets/js/starter2.js',
		'!<rootDir>/resources/assets/js/starter3.js',
		'!<rootDir>/resources/assets/js/**/app.js',
		'!<rootDir>/resources/assets/js/user/*.js',
		'!<rootDir>/resources/assets/js/promo/all-job/promo-list-all-job.js',
		'!<rootDir>/resources/assets/js/promo/all-apartment/promo-list-all-apartment.js',
		'!<rootDir>/resources/assets/js/owner/owner-page.js',
		'!<rootDir>/resources/assets/js/landing/kost/landing-kost.js',
		'!<rootDir>/resources/assets/js/input/vacancy/input-vacancy.js',
		'!<rootDir>/resources/assets/js/input/*.js',
		'!<rootDir>/resources/assets/js/404-page/404-page.js',
		'!<rootDir>/resources/assets/js/landing/landing-area.js',
		'!<rootDir>/resources/assets/js/landing/landing-project.js',
		'!<rootDir>/resources/assets/js/@single/*.js'
	],
	coverageReporters: ['html', 'text-summary'],
	transform: {
		'^.+\\.js$': 'babel-jest',
		'^.+\\.vue$': 'vue-jest'
	},
	transformIgnorePatterns: ['/node_modules/(?!crypto-es).+\\.js$'],
	moduleNameMapper: {
		'Js/(.*)$': '<rootDir>/resources/assets/js/$1',
		'Json/(.*)$': '<rootDir>/resources/assets/json/$1',
		'tests-fe/(.*)$': '<rootDir>/tests-fe/$1',
		'Consultant/(.*)$': '<rootDir>/resources/assets/js/_consultant/$1',
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/__mocks__/fileMock.js',
		'\\.(css|less)$': 'identity-obj-proxy'
	},
	snapshotSerializers: ['jest-serializer-vue'],
	setupFiles: ['<rootDir>/tests-fe/utils/mock-localstorage/index.js'],
	coverageThreshold: {
		global: {
			statements: 55,
			branches: 50,
			functions: 58,
			lines: 56
		}
	}
};
