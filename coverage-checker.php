<?php

$inputFile = $argv[1];
if (!file_exists($inputFile)) {
    throw new InvalidArgumentException('Invalid input file provided');
}
$guildName = $argv[2] ?? 'BE';
$projectName = $argv[3] ?? 'mami53';
$branchName = $argv[4] ?? 'master';

$xml = new SimpleXMLElement(file_get_contents($inputFile));
$metrics = $xml->xpath('//class/metrics');

$totalMethods = 0;
$checkedMethods = 0;
foreach ($metrics as $metric) {
    $totalMethods += (int) $metric['methods'];
    $checkedMethods += (int) $metric['coveredmethods'];
}
$coverage = ($checkedMethods / $totalMethods) * 100;

$hook = 'https://hooks.slack.com/services/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';
$template = '[%s][%s](%s) Code coverage methods: %.2f%% or %d/%d.';
$message = sprintf($template, $guildName, $projectName, $branchName, $coverage, $checkedMethods, $totalMethods) . PHP_EOL;
$result = '';

try {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $hook);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['text' => $message]));
    $result = curl_exec($curl);
    curl_close($curl);
} catch (Exception $e) {
    unset($e);
}
echo (empty($result) ? '' : $result . PHP_EOL) . $message;
