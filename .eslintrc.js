module.exports = {
	extends: [
		'standard',
		'plugin:vue/recommended',
		// Turns off all rules that are unnecessary or might conflict with Prettier.
		'prettier',
		'prettier/vue',
		'plugin:prettier/recommended'
	],
	plugins: [
		'jest',
		'vue',
		// Runs Prettier as an ESLint rule and reports differences as individual ESLint issues.
		'prettier'
	],
	env: {
		browser: true,
		es6: true,
		jest: true
	},
	parserOptions: {
		parser: 'babel-eslint'
	},
	globals: {
		Moengage: 'readonly',
		axios: 'readonly',
		swal: 'readonly',
		bugsnagClient: 'readonly',
		sb: 'readonly',
		sbWidget: 'readonly',
		tracker: 'readonly',
		Cookies: 'readonly',
		authCheck: 'readonly',
		authData: 'readonly',
		oxWebUrl: 'readonly',
		oxOauth2Domain: 'readonly',
		Vue: 'readonly',
		validator: 'readonly',
		debounce: 'readonly',
		$: 'readonly',
		translateWords: 'readonly',
		AB_TEST_URL: 'readonly',
		AB_TEST_API_KEY: 'readonly',
		AB_TEST_API_AUTHORIZATION: 'readonly',
		ga: 'readonly'
	}
};
