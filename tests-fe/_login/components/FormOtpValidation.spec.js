import { createLocalVue, shallowMount } from '@vue/test-utils';
import FormOtpValidation from 'Js/_login/components/FormOtpValidation.vue';
import Vuex from 'vuex';
import loginStore from './__mocks__/store';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(loginStore);

const propsData = {
	phoneNumber: '08123456789',
	otpRequestUrl: 'someurl.site',
	otpCheckUrl: 'someurl2.site',
	initializeRequest: false,
	backButton: 'back',
	page: 'somepage',
	user: 'user'
};

const profile = {
	username: '',
	phoneNumber: '',
	email: '',
	password: '',
	otpCode: '',
	captcha: ''
};

global.axios = mockAxios;
global.tracker = jest.fn();
global.alert = jest.fn();

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

describe('FormOtpValidation.vue', () => {
	const requestOtpSpy = jest.spyOn(FormOtpValidation.methods, 'requestOtp');
	const startCountDownSpy = jest.spyOn(
		FormOtpValidation.methods,
		'startCountDown'
	);
	const generateSubmitParamsSpy = jest.spyOn(
		FormOtpValidation.methods,
		'generateSubmitParams'
	);
	const maskCharsSpy = jest.spyOn(FormOtpValidation.methods, 'maskChars');
	const trackSubmitOTPSpy = jest.spyOn(
		FormOtpValidation.methods,
		'trackSubmitOTP'
	);

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(FormOtpValidation, {
			localVue,
			store,
			propsData
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.form-otp-validation').exists()).toBe(true);
	});

	describe('Computed Properties', () => {
		it('Should return proper storedProfile data', () => {
			expect(wrapper.vm.$store.state.profile).toBeTruthy();
			expect(wrapper.vm.storedProfile).toBe(wrapper.vm.$store.state.profile);

			wrapper.vm.$store.state.profile = null;
			expect(wrapper.vm.$store.state.profile).toBeFalsy();
			expect(wrapper.vm.storedProfile).toBe(null);

			wrapper.vm.$store.state.profile = profile;
		});

		it('Should return trackers tag name properly', () => {
			const testCases = [
				{
					role: 'owner',
					tagName: '[Owner]',
					gtmTagClass: 'track-verifikasi-register-owner'
				},
				{
					role: 'user',
					tagName: '[User]',
					gtmTagClass: 'track-verifikasi-register-tenant'
				}
			];

			testCases.forEach(tc => {
				wrapper.setProps({ user: tc.role });

				expect(wrapper.vm.trackerTag).toBe(tc.tagName);
				expect(wrapper.vm.gtmTrackVerificationClass).toBe(tc.gtmTagClass);
			});
		});
	});

	describe('Methods', () => {
		it('Should handle init request properly', () => {
			expect(wrapper.vm.initializeRequest).toBe(false);

			wrapper.vm.handleInitRequest();
			expect(startCountDownSpy).toBeCalled();

			wrapper.setProps({ initializeRequest: true });

			wrapper.vm.handleInitRequest();
			expect(requestOtpSpy).toBeCalled();
		});

		it('Should call tracker event when requesting OTP while the action is set to resend', () => {
			wrapper.vm.requestOtp('resend');

			expect(global.tracker).toBeCalledWith('moe', [
				`${wrapper.vm.trackerTag} Click Resend OTP`,
				{
					request_from: wrapper.vm.page,
					interface: 'desktop'
				}
			]);

			jest.clearAllMocks();
		});

		it('Should notify bugsnag when OTP request failed', async () => {
			axios.mockReject('error');
			wrapper.vm.requestOtp();

			await wrapper.vm.$nextTick();
			expect(global.bugsnagClient.notify).toBeCalled();
		});

		it('Should return proper OTP submit request payloads', () => {
			const testCases = [
				{
					page: 'register',
					returnValue: {
						phone_number: wrapper.vm.phoneNumber,
						code: wrapper.vm.otpInput.toUpperCase()
					}
				},
				{
					page: 'not-register',
					returnValue: {
						phone_number: wrapper.vm.phoneNumber,
						verification_code: wrapper.vm.otpInput.toUpperCase()
					}
				}
			];

			testCases.forEach(tc => {
				wrapper.setProps({ page: tc.page });
				wrapper.vm.generateSubmitParams();
				expect(generateSubmitParamsSpy).toHaveReturnedWith(tc.returnValue);
			});
		});

		it('Should handle mask chars properly', () => {
			const event = {
				preventDefault: jest.fn()
			};

			// OTP Value length 4 or more
			wrapper.setData({ otpInput: '1234' });
			wrapper.vm.maskChars(event);
			expect(event.preventDefault).toBeCalled();

			// OTP Value length less than 4
			wrapper.setData({ otpInput: '123' });
			wrapper.vm.maskChars(event);
			expect(maskCharsSpy).toHaveReturnedWith(true);
		});

		it('Should start countdown properly', () => {
			const clearIntervalSpy = jest.spyOn(window, 'clearInterval');

			window.setInterval = jest.fn();
			wrapper.vm.startCountDown();

			// to simulate calling clearInterval
			wrapper.setData({ resendCounter: -1000 });

			// run first argument of first call, to simulate reaching 0 second
			window.setInterval.mock.calls[0][0]();

			expect(window.setInterval).toBeCalled();
			expect(clearIntervalSpy).toBeCalled();
			expect(wrapper.vm.formattedTime).toBe('01:00');
		});

		it('Should emit back properly', async () => {
			wrapper.vm.handleBackButton();

			await wrapper.vm.$nextTick();

			expect(wrapper.emitted('back')).toBeTruthy();
		});
	});

	describe('Submit OTP API Request', () => {
		it('Should handle invalid OTP request request properly', () => {
			wrapper.setData({ otpInput: '123' });
			wrapper.vm.submitOtp();
			expect(wrapper.vm.isOtpValid).toBe(false);
		});

		it('Should handle success flow OTP Submit request properly', async () => {
			const mockResponse = {
				data: {
					status: true
				}
			};

			wrapper.setData({ otpInput: '1234' });
			axios.mockResolve(mockResponse);

			wrapper.vm.submitOtp();
			await wrapper.vm.$nextTick();
			expect(wrapper.emitted('verified')).toBeTruthy();
			expect(trackSubmitOTPSpy).toBeCalledWith(true);
		});

		it('Should handle error flow OTP Submit request that caused by falsy API response properly', async () => {
			const mockResponse = {
				data: {
					status: false
				}
			};

			wrapper.setData({ otpInput: '1234' });
			axios.mockResolve(mockResponse);

			wrapper.vm.submitOtp();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.isOtpValid).toBe(false);
			expect(trackSubmitOTPSpy).toBeCalledWith(false);
		});

		it('Should handle unknown error flow OTP Submit request response properly', async () => {
			wrapper.setData({ otpInput: '1234' });
			axios.mockReject('error');

			wrapper.vm.submitOtp();
			await wrapper.vm.$nextTick();
			expect(global.bugsnagClient.notify).toBeCalled();
		});

		it('Should handle tracking event on each submit request success statuses properly', () => {
			const trackerParam = {
				interface: 'desktop',
				request_from: wrapper.vm.page,
				success_status: null,
				fail_reason: null
			};

			const testCases = [
				{
					role: 'owner',
					trackerParam: {
						...trackerParam,
						owner_name: wrapper.vm.storedProfile.username,
						owner_phone_number: wrapper.vm.storedProfile.phoneNumber,
						owner_email: wrapper.vm.storedProfile.email
					}
				},
				{
					role: 'tenant',
					trackerParam: {
						...trackerParam,
						tenant_name: wrapper.vm.storedProfile.username,
						tenant_phone_number: wrapper.vm.storedProfile.phoneNumber,
						tenant_email: wrapper.vm.storedProfile.email
					}
				}
			];

			testCases.forEach(tc => {
				// Success Flow
				tc.trackerParam.success_status = true;
				tc.trackerParam.fail_reason = null;

				wrapper.setProps({ user: tc.role });
				wrapper.vm.trackSubmitOTP(true);

				expect(global.tracker).toBeCalledWith('moe', [
					`${wrapper.vm.trackerTag} Verifikasi Phone Number`,
					tc.trackerParam
				]);

				// Error Flow
				tc.trackerParam.success_status = false;
				tc.trackerParam.fail_reason = 'kode salah';

				wrapper.setProps({ user: tc.role });
				wrapper.vm.trackSubmitOTP(false);

				expect(global.tracker).toBeCalledWith('moe', [
					`${wrapper.vm.trackerTag} Verifikasi Phone Number`,
					tc.trackerParam
				]);
			});
		});
	});
});
