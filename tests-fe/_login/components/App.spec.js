import { createLocalVue, shallowMount } from '@vue/test-utils';
import App from 'Js/_login/components/App.vue';
import Vuex from 'vuex';
import store from './__mocks__/store';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty('window.performance', {
	navigation: {
		type: 2
	}
});

mockWindowProperty('window.location', {
	reload: jest.fn()
});

const $route = {
	path: '/login-pencari',
	meta: {
		title: 'login pencari'
	}
};

describe('App.vue', () => {
	const createWrapper = () =>
		shallowMount(App, {
			localVue,
			mocks: { $route },
			stubs: { RouterView: { template: '<div />' } },
			store: new Vuex.Store(store)
		});

	it('should form container', () => {
		const wrapper = createWrapper();
		expect(wrapper.find('.form-container').exists()).toBe(true);
	});

	it('should set redirect page properly', () => {
		const redirectPath = encodeURI('/some/redirect-path');
		mockWindowProperty('window.location', {
			reload: jest.fn(),
			href: `http://test.com/login?redirect_path=${redirectPath}`
		});

		const wrapper = createWrapper();
		expect(wrapper.vm.$store.state.redirectPath).toBe(redirectPath);
	});
});
