import { createLocalVue, shallowMount } from '@vue/test-utils';
import ForgetPassword from 'Js/_login/components/ForgetPassword.vue';
import Vuex from 'vuex';
import authStore from './__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();
localVue.use(Vuex);

const $route = {
	path: '/lupa-password-pemilik',
	query: {
		step: null
	}
};
const $router = { replace: jest.fn(), push: jest.fn(), go: jest.fn() };
const stubs = {
	AuthContainer: mockComponent,
	ForgetPasswordIdInput: mockComponent,
	ForgetPasswordOTPMethodSelection: mockComponent,
	ForgetPasswordOTPInput: mockComponent,
	ForgetPasswordNewPasswordInput: mockComponent,
	ForgetPasswordSuccessState: mockComponent
};
const store = new Vuex.Store(authStore);

describe('ForgetPassword.vue', () => {
	let wrapper;
	const returnToInputIdSpy = jest.spyOn(
		ForgetPassword.methods,
		'returnToInputId'
	);
	const handleAccessRestrictionSpy = jest.spyOn(
		ForgetPassword.methods,
		'handleAccessRestriction'
	);

	wrapper = shallowMount(ForgetPassword, {
		localVue,
		stubs,
		mocks: { $route, $router },
		store
	});

	const reverseStepSpy = jest.spyOn(wrapper.vm, 'reverseStep');

	it('Should exists', () => {
		expect(wrapper.find('.forget-password-wrapper').exists()).toBe(true);
	});

	it('Should handle reverse step properly', () => {
		wrapper.vm.reverseStep();
		expect($router.go).toBeCalledWith(-1);
	});

	it('Should handle forward step properly', () => {
		const mockKey = 'input-otp';
		const role = (wrapper.vm.$store.state.userRole = {
			key: 'owner'
		});

		wrapper.vm.forwardStep(mockKey);
		expect($router.push).toBeCalledWith({
			name: `forget-password-${role.key}`,
			query: { step: mockKey }
		});
	});

	it('Should handle access restriction properly', () => {
		const stepKeys = [
			'select-otp-method',
			'input-otp',
			'input-new-password',
			'success-state',
			'else'
		];

		stepKeys.forEach(key => {
			wrapper.vm.$store.state.forgetPassword.currentStepKey = key;
			wrapper.vm.handleAccessRestriction();

			key === 'else'
				? expect(handleAccessRestrictionSpy).toHaveReturnedWith(null)
				: expect(returnToInputIdSpy).toHaveBeenCalled();
		});
	});

	it('Should return isAtInputOTP value properly', () => {
		wrapper.vm.$store.state.forgetPassword.currentStepKey = 'input-otp';
		wrapper.vm.$store.state.forgetPassword.selectedOTPMethod.key = 0;

		expect(wrapper.vm.isAtInputOTP).toBeTruthy();
	});

	it('Should return isCurrentlyAtFinalSteps value properly', () => {
		wrapper.vm.$store.state.forgetPassword.currentStepKey =
			'input-new-password';
		wrapper.vm.$store.state.forgetPassword.inputedOTPCode = '1234';

		expect(wrapper.vm.isAtInputNewPassword).toBeTruthy();
		expect(wrapper.vm.isCurrentlyAtFinalSteps).toBeTruthy();
	});

	it('Should handle abort modal actions properly', () => {
		wrapper.setData({ isShowAbortModal: true });
		wrapper.setData({ routeTarget: '/mockroute' });

		// Truthy action
		// At step 3 (input OTP)
		wrapper.vm.$store.state.currentStepKey = 'input-otp';
		wrapper.vm.$store.state.selectedOTPMethod = { key: 1 };
		wrapper.vm.handleAbortModalActions(true);
		expect(reverseStepSpy).toHaveBeenCalled();

		// At step 4 (input new password)
		wrapper.vm.$store.state.currentStepKey = 'input-new-password';
		wrapper.vm.$store.state.inputedOTPCode = '1234';
		wrapper.vm.handleAbortModalActions(true);
		expect(returnToInputIdSpy).toHaveBeenCalled();

		// Falsy action
		wrapper.vm.handleAbortModalActions(false);
		expect(wrapper.vm.routeTarget).toBe(null);

		expect(wrapper.vm.isShowAbortModal).toBe(false);
	});

	it('Should add and remove listener on destroy', () => {
		const addEvent = jest
			.spyOn(global, 'addEventListener')
			.mockImplementation(() => {});
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});

		wrapper = shallowMount(ForgetPassword, {
			localVue,
			stubs,
			mocks: { $route, $router },
			store
		});

		expect(addEvent).toBeCalledWith('beforeunload', expect.any(Function));

		wrapper.destroy();

		expect(removeEvent).toBeCalledWith('beforeunload', expect.any(Function));
	});

	it('Should handle step shifting properly', () => {
		const stepKey = 'select-otp-method';

		$route.query = { step: stepKey };

		wrapper = shallowMount(ForgetPassword, {
			localVue,
			stubs,
			mocks: { $route, $router },
			store
		});

		expect(wrapper.vm.currentStepKey).toBe(stepKey);
	});

	it('Should handle in component navigation guard properly', () => {
		const beforeRouteUpdate = wrapper.vm.$options.beforeRouteUpdate;
		const next = jest.fn();

		const mockTo = { path: '/', query: { step: '/input-otp' } };
		const mockFrom = { path: '/', query: { step: 'input-new-password' } };

		// Showing abort modal on final steps (input-otp/input-new-password)
		wrapper.vm.$store.state.forgetPassword.currentStepKey = mockFrom.query.step;
		wrapper.vm.$store.state.forgetPassword.inputedOTPCode = '1234';
		beforeRouteUpdate.call(wrapper.vm, mockTo, mockFrom, next);
		expect(next).toBeCalledWith(false);
		expect(wrapper.vm.routeTarget).toEqual(mockTo);
		expect(wrapper.vm.isShowAbortModal).toBe(true);

		// Proceeding to route target if 'routeTarget' value has been assigned.
		beforeRouteUpdate.call(wrapper.vm, mockTo, mockFrom, next);
		expect(next).toHaveBeenCalled();

		// Proceeding to route target if currently isn't at final steps.
		wrapper.vm.$store.state.forgetPassword.currentStepKey = 'input-id';
		beforeRouteUpdate.call(wrapper.vm, mockTo, mockFrom, next);
		expect(next).toHaveBeenCalled();
	});
});
