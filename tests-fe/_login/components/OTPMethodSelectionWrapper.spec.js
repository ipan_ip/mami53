import { createLocalVue, shallowMount } from '@vue/test-utils';
import OTPMethodSelectionWrapper from 'Js/_login/components/OTPMethodSelectionWrapper.vue';

const localVue = createLocalVue();

const mockComponent = { template: '<div />' };

const stubs = {
	BgIcon: mockComponent
};

const mockOTPMethod = {
	key: 1,
	label: 'wa',
	value: '08123456789'
};

describe('OTPMethodSelectionWrapper.vue', () => {
	const wrapper = shallowMount(OTPMethodSelectionWrapper, {
		localVue,
		stubs,
		propsData: {
			methodItems: [mockOTPMethod]
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.otp-method-selection-wrapper').exists()).toBe(true);
	});

	it('Should handle OTP Method selection properly', async () => {
		const sentItem = {
			key: mockOTPMethod.key,
			label: mockComponent.label
		};

		wrapper.vm.selectMethod(sentItem);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('select')[0][0]).toEqual(sentItem);
	});
});
