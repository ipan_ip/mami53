import forgetPassword from './modules/ForgetPassword';

export default {
	modules: {
		forgetPassword
	},
	state: {
		token: 'token',
		captchaKey: '',
		redirectPath: '',
		profile: {
			username: '',
			phoneNumber: '',
			email: '',
			password: '',
			otpCode: '',
			captcha: ''
		},
		userRole: {
			key: '',
			label: ''
		}
	},
	mutations: {
		setProfileUsername(state, payload) {
			state.profile.username = payload;
		},
		setProfilePhoneNumber(state, payload) {
			state.profile.phoneNumber = payload;
		},
		setProfileEmail(state, payload) {
			state.profile.email = payload;
		},
		setProfilePassword(state, payload) {
			state.profile.password = payload;
		},
		setProfileOtpCode(state, payload) {
			state.profile.otpCode = payload;
		},
		setRegistrationData(state, payload) {
			state.profile.username = payload.username;
			state.profile.phoneNumber = payload.phoneNumber;
			state.profile.email = payload.email;
			state.profile.password = payload.password;
			state.profile.captcha = payload.captcha;
		},
		setCaptchaKey(state, payload) {
			state.captchaKey = payload;
		},
		setRedirectPath(state, payload) {
			state.redirectPath = payload;
		},
		setUserRole(state, payload) {
			state.userRole = payload;
		}
	}
};
