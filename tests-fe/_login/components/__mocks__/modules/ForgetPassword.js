export default {
	namespaced: true,
	state: {
		currentStepKey: 'input-id',
		selectedUserIdType: '',
		userEmailOrPhoneNumberValue: '',
		otpMethods: [],
		selectedOTPMethod: {
			key: null,
			label: ''
		},
		otpResendCounter: {
			sms: 0,
			whatsapp: 0,
			email: 0
		},
		isOTPValid: false,
		inputedOTPCode: null,
		isForgetPasswordSuccess: false
	},
	mutations: {
		setCurrentStepKey(state, payload) {
			state.currentStepKey = payload;
		},
		setSelectedUserIdType(state, payload) {
			state.selectedUserIdType = payload;
		},
		setUserEmailOrPhoneNumberValue(state, payload) {
			state.userEmailOrPhoneNumberValue = payload;
		},
		setOTPMethods(state, payload) {
			state.otpMethods = payload;
		},
		setSelectedOTPMethod(state, payload) {
			state.selectedOTPMethod = payload;
		},
		setOTPResendCounter(state, payload) {
			state.otpResendCounter = payload;
		},
		setIsOTPValid(state, payload) {
			state.isOTPValid = payload;
		},
		setInputedOTPCode(state, payload) {
			state.inputedOTPCode = payload;
		},
		setIsForgetPasswordSuccess(state, payload) {
			state.isForgetPasswordSuccess = payload;
		}
	},
	getters: {
		steps: () => {
			return [
				{
					id: 1,
					key: 'input-id',
					title: 'Lupa Password',
					description:
						'Masukkan nomor handphone yang terdaftar di Mamikos, dan lakukan verifikasi untuk membuat password baru.',
					component: 'ForgetPasswordIdInput'
				},
				{
					id: 2,
					key: 'select-otp-method',
					title: 'Pilih Metode Verifikasi',
					description:
						'Kami akan mengirimkan kode verifikasi sesuai dengan metode yang Anda pilih di bawah ini.',
					component: 'ForgetPasswordOTPMethodSelection'
				},
				{
					id: 3,
					key: 'input-otp',
					title: `Verifikasi Akun Pemilik Kos`,
					description:
						'Nomor handphone ini telah terdaftar sebagai akun pemilik kos di Mamikos.',
					component: 'ForgetPasswordOTPInput'
				},
				{
					id: 4,
					key: 'input-new-password',
					title: 'Ubah Password',
					description:
						'Password baru akan digunakan untuk akses ke akun Mamikos Anda.',
					component: 'ForgetPasswordNewPasswordInput'
				},
				{
					id: 5,
					key: 'success-message',
					title: 'Password Berhasil Diubah',
					description: '',
					component: 'ForgetPasswordSuccessState'
				}
			];
		},
		currentStep: () => {
			return {
				id: 5,
				key: 'success-message',
				title: 'Password Berhasil Diubah',
				description: '',
				component: 'ForgetPasswordSuccessState'
			};
		},
		otpResendCounterWarningMessage: () => {
			return jest.fn(() => {
				return `Mohon tunggu 60 detik lagi untuk kirim ulang kode verifikasi.`;
			});
		},
		selectedOTPResendCounter: state => {
			return state.otpResendCounter[
				state.selectedOTPMethod.label.toLowerCase()
			];
		}
	},
	actions: {
		startOTPResendCountdown: () => jest.fn()
	}
};
