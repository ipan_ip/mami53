import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ContainerRegister from 'Js/_login/components/ContainerRegister.vue';
import store from './__mocks__/store';
import { flushPromises } from 'tests-fe/utils/promises';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = { template: '<div />' };

const axios = {
	post: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: {}
		}
	})
};

global.navigator = { isMobile: true };
global.oxOauth2Domain = 'ox-oauth-2-domain';
global.Cookies = { set: jest.fn() };
global.tracker = jest.fn();
global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();
global.axios = axios;

const locationReplace = jest.fn();
mockWindowProperty('window.location', { replace: locationReplace });
mockWindowProperty('Cookies', {
	get: jest.fn(() => 'samesite')
});

const stubs = {
	FormRegister: mockComponent,
	FormOtpValidation: mockComponent,
	FormIntercept: mockComponent,
	FormInterceptAdd: mockComponent
};

describe('ContainerRegister.vue', () => {
	const wrapper = shallowMount(ContainerRegister, {
		localVue,
		stubs,
		propsData: {
			user: 'tenant'
		},
		store: new Vuex.Store(store)
	});

	it('should render form register', () => {
		expect(wrapper.findComponent(stubs.FormRegister).exists()).toBe(true);
	});

	it('should change state to intercept when user successfully registered', async () => {
		wrapper.vm.changeState('verification');
		await wrapper.vm.$nextTick();

		expect(wrapper.findComponent(stubs.FormOtpValidation).exists()).toBe(true);
		wrapper.findComponent(stubs.FormOtpValidation).vm.$emit('verified');

		await flushPromises();

		expect(wrapper.vm.currentState).toBe('intercept');
		await wrapper.vm.$nextTick();

		expect(wrapper.findComponent(stubs.FormIntercept).exists()).toBe(true);
	});

	it('should skip intercept and redirected to redirect path when user successfully registered with redirect path query specified', async () => {
		wrapper.vm.$store.state.redirectPath = '/ob/dbet-123/test';
		wrapper.vm.changeState('verification');
		await wrapper.vm.$nextTick();

		expect(wrapper.findComponent(stubs.FormOtpValidation).exists()).toBe(true);
		wrapper.findComponent(stubs.FormOtpValidation).vm.$emit('verified');
		await flushPromises();

		expect(wrapper.vm.currentState).not.toBe('intercept');
		expect(locationReplace).toBeCalledWith('/ob/dbet-123/test');
	});
});
