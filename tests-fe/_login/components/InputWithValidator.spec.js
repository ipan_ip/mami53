import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import InputWithValidator from 'Js/_login/components/InputWithValidator';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
window.validator = new Validator();
Validator.localize('id', id);

describe('InputWithValidator.vue', () => {
	const localVue = createLocalVue();
	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(InputWithValidator, {
			localVue,
			propsData: {
				validationOption: ''
			}
		});
	});

	it('Should render properly', () => {
		expect(wrapper.find('.input-with-validator').exists()).toBe(true);
	});

	it('Should render input label properly', () => {
		wrapper.setProps({
			inputLabel: 'Nama Lengkap',
			customClassLabel: 'custom-label-class'
		});

		wrapper.vm.$nextTick(() => {
			const label = wrapper.find('label');
			expect(label.exists()).toBe(true);
			expect(label.text()).toBe('Nama Lengkap');
			expect(label.classes(wrapper.props('customClassLabel'))).toBe(true);
		});
	});

	it('Should render text field input properly', () => {
		wrapper.setProps({
			inputName: 'nama-lengkap',
			inputLength: '10',
			inputType: 'text',
			inputPlaceholder: 'Masukkan nama lengkap',
			customClassInput: 'custom-input-class',
			validationOption: 'min:5'
		});

		wrapper.vm.$nextTick(() => {
			const input = wrapper.find('.default-input');

			expect(input.exists()).toBe(true);
			expect(input.attributes('v-validate')).toBe(
				wrapper.props.validationOption
			);
			expect(input.attributes('name')).toBe(wrapper.props('inputName'));
			expect(input.attributes('maxlength')).toBe(wrapper.props('inputLength'));
			expect(input.attributes('type')).toBe(wrapper.props('inputType'));
			expect(input.attributes('placeholder')).toBe(
				wrapper.props('inputPlaceholder')
			);
			expect(input.classes(wrapper.props('customClassInput'))).toBe(true);
		});
	});

	it('Should display show password icon if input type is password', () => {
		wrapper.setProps({
			inputType: 'password'
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.password-eye').exists()).toBe(true);
		});
	});

	it('Should handle show password method properly', () => {
		expect(wrapper.vm.localType).toBe(null);

		wrapper.setProps({
			inputType: 'password'
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.find({ ref: 'inputText' }).attributes('type')).toBe(
				'password'
			);
			wrapper.find('.password-eye').trigger('click');

			expect(wrapper.vm.localType).toBe('text');
			expect(wrapper.find('.password-eye').classes('--open')).toBe(true);
		});

		wrapper.vm.$nextTick(() => {
			wrapper.find('.password-eye').trigger('click');

			expect(wrapper.vm.localType).toBe(null);
			expect(wrapper.find('.password-eye').classes('--open')).toBe(false);
		});
	});

	it('Should handle input value properly', () => {
		const testVal = 'Test Value';
		wrapper.find({ ref: 'inputText' }).setValue(testVal);
		wrapper.find({ ref: 'inputText' }).trigger('input');

		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('input')).toBeTruthy();
		});
	});

	it('Should return error message if validation failed', async () => {
		wrapper.setProps({
			validationOption: 'min:5'
		});

		await wrapper.vm.$nextTick();

		wrapper.find({ ref: 'inputText' }).setValue('hehe');
		wrapper.find({ ref: 'inputText' }).trigger('input');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('input')).toBeTruthy();
		expect(wrapper.vm.errorMessage).toEqual(
			wrapper.vm.errors.items.map(item => item.msg).join(', ')
		);
	});
});
