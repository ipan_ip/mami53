import { createLocalVue, shallowMount } from '@vue/test-utils';
import ContainerLogin from 'Js/_login/components/ContainerLogin.vue';
import Vuex from 'vuex';
import store from './__mocks__/store';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = { template: '<div />' };

const stubs = {
	FormLogin: mockComponent,
	FormSelect: mockComponent
};

mockWindowProperty('Cookies', {
	get: jest.fn(() => 'samesite')
});

describe('ContainerLogin.vue', () => {
	const wrapper = shallowMount(ContainerLogin, {
		localVue,
		stubs,
		propsData: {
			user: 'select'
		},
		store: new Vuex.Store(store)
	});

	it('should render form container', () => {
		expect(wrapper.find('.form-container').exists()).toBe(true);
	});

	it('should correct form', async () => {
		await wrapper.setProps({ user: 'select' });
		expect(wrapper.findComponent(stubs.FormSelect).exists()).toBe(true);

		await wrapper.setProps({ user: 'tenant' });
		expect(wrapper.findComponent(stubs.FormLogin).exists()).toBe(true);
	});

	it('should emit back when form login emit back', async () => {
		await wrapper.setProps({ user: 'tenant' });
		wrapper.findComponent(stubs.FormLogin).vm.$emit('back', 'select');

		expect(wrapper.emitted('back')[0][0]).toBe('select');
	});

	it('should change state properly', () => {
		wrapper.vm.changeState('login');
		expect(wrapper.vm.currentState).toBe('login');
	});

	it('should use redirect path properly', async () => {
		wrapper.vm.$store.state.redirectPath = '/redirect-path';
		await wrapper.setProps({ user: 'tenant' });

		expect(
			wrapper.findComponent(stubs.FormLogin).attributes('redirect-path')
		).toBe('/redirect-path');
	});
});
