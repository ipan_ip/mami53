import { createLocalVue, shallowMount } from '@vue/test-utils';
import AuthContainer from 'Js/_login/components/AuthContainer.vue';

const localVue = createLocalVue();

const mockComponent = { template: '<div />' };

const stubs = {
	BgCard: mockComponent,
	BgIcon: mockComponent
};

describe('AuthContainer.vue', () => {
	const wrapper = shallowMount(AuthContainer, {
		localVue,
		stubs,
		propsData: {
			activeBackButton: 'back',
			componentTitle: 'Mock Title',
			componentDescription: 'Mock Description'
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.auth-container').exists()).toBe(true);
	});

	it('Should emitted back when back button clicked', async () => {
		wrapper.vm.handleBackClicked();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('back')).toBeTruthy();
	});
});
