import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import ModalFormLogin from 'Js/_login/components/ModalFormLogin';

describe('ModalFormLogin.vue', () => {
	const localVue = createLocalVue();

	const formSelectStub = `<div class="login-select-container"><div class="select-buttons"><button class="btn-owner" @click="$emit('stateChanged', 'owner')">Pemilik Kos</button><button class="btn-tenant" @click="$emit('stateChanged', 'tenant')">Pencari Kos</button></div></div>`;

	const formOtpValidationStub = {
		template: `<div><form></form></div>`,
		props: {
			otpRequestUrl: '/',
			otpCheckUrl: '/',
			page: 'register'
		}
	};

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ModalFormLogin, {
			localVue,
			propsData: {
				showModalLogin: true,
				startingState: 'default'
			},
			stubs: {
				formSelect: formSelectStub,
				formOtpValidation: formOtpValidationStub
			}
		});
	});

	it('Should render the correct state', () => {
		expect(wrapper.find('.login-select-container').exists()).toBe(true);

		wrapper.find('.login-select-container .btn-owner').trigger('click');
		expect(wrapper.vm.currentState).toBe('owner');

		wrapper.find('.login-select-container .btn-tenant').trigger('click');
		expect(wrapper.vm.currentState).toBe('tenant');
	});

	it('Should handle close modal properly', () => {
		jest.useFakeTimers();

		wrapper.vm.closeModal();

		expect(wrapper.emitted('closeModal')).toBeTruthy();
		expect(wrapper.vm.loginHeaderText).toBe('Login Akun');
		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 300);

		jest.runAllTimers();

		expect(wrapper.vm.currentState).toBe('default');
	});

	it(`Should store phone number and set current state to verification if users phone number isn't verified`, () => {
		const mockPhoneNumber = '08123456789';
		wrapper.vm.verifyPhonenumber(mockPhoneNumber);

		expect(wrapper.vm.storedPhoneNumber).toBe(mockPhoneNumber);
		expect(wrapper.vm.currentState).toBe('verification');
	});
});
