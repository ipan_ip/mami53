import { createLocalVue, shallowMount } from '@vue/test-utils';
import ForgetPasswordIdInput from 'Js/_login/components/forget-password/ForgetPasswordIdInput.vue';
import Vuex from 'vuex';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import store from '../__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';
import MixinAuthTrackers from 'Js/_login/mixins/MixinAuthTrackers';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');
window.validator = new Validator();
Validator.localize('id', id);

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VeeValidate, {
	locale: 'id'
});
localVue.mixin([MixinAuthTrackers]);

const stubs = {
	BgField: mockComponent,
	BgInput: mockComponent,
	BgButton: mockComponent,
	MamiLoadingInline: mockComponent
};

global.tracker = jest.fn();
global.axios = mockAxios;
global.alert = jest.fn();

const windowOpen = jest.fn();
global.open = windowOpen;

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

describe('ForgetPasswordIdInput.vue', () => {
	const handleUserInputSpy = jest.spyOn(
		ForgetPasswordIdInput.methods,
		'handleUserInput'
	);
	const wrapper = shallowMount(ForgetPasswordIdInput, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('Should exists', () => {
		expect(wrapper.find('.forget-password-id-input').exists()).toBe(true);
	});

	it('Should handle input value changes properly', async () => {
		wrapper.vm.inputValue = 'mock input';
		await wrapper.vm.$nextTick();
		expect(handleUserInputSpy).toBeCalledWith(wrapper.vm.inputValue);
	});

	it('Should handle immediate validation properly', () => {
		jest.useFakeTimers();

		wrapper.vm.handleImmediateValidation();

		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 100);

		jest.runAllTimers();
	});

	it('Should handle contact CS properly', () => {
		wrapper.setData({
			csContactItems: { wa_number: '08124356', pretext: 'mock text' }
		});
		const waEndpoint = `https://api.whatsapp.com/send?phone=${wrapper.vm.csContactItems.wa_number}&text=${wrapper.vm.csContactItems.pretext}`;

		wrapper.vm.handleContactCS();

		expect(windowOpen).toBeCalledWith(waEndpoint);
	});

	it('Should handle submit properly', async () => {
		wrapper.setData({ inputValue: '0812435', isPassedAPIValidation: true });
		expect(wrapper.vm.isFormValid).toBe(true);

		wrapper.vm.handleSubmit();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('next-step')).toBeTruthy();
	});

	describe('API Validation', () => {
		it('Should handle success flow API Validation request properly', async () => {
			const mockResponse = {
				data: {
					status: true
				}
			};

			const mockValue = '0812435676';
			axios.mockResolve(mockResponse);

			wrapper.vm.validateWithAPI(mockValue);
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.isPassedAPIValidation).toBe(true);
		});

		it('Should handle error flow API Validation request properly', async () => {
			const mockResponse = {
				data: {
					status: false,
					contact: {
						wa_number: '0812345647',
						pretext: 'mock pretext'
					}
				}
			};

			const mockValue = '0812435676';
			axios.mockResolve(mockResponse);

			wrapper.vm.validateWithAPI(mockValue);
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.csContactItems).toEqual(mockResponse.data.contact);
		});
	});
});
