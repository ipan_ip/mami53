import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ForgetPasswordSuccessState from 'Js/_login/components/forget-password/ForgetPasswordSuccessState.vue';
import store from '../__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinReverseStep from 'Js/_login/mixins/MixinReverseStep';

window.Vue = require('vue');
const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin(MixinReverseStep);

mockWindowProperty('window.location', { href: '/forget-password-pemilik' });

const stubs = {
	BgButton: mockComponent,
	BgIllustration: mockComponent
};

describe('ForgetPasswordSuccessState.vue', () => {
	const wrapper = shallowMount(ForgetPasswordSuccessState, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('Should exists', () => {
		expect(wrapper.find('.forget-password-success-state').exists()).toBe(true);
	});

	it('Should handle go to home properly', async () => {
		wrapper.vm.returnToHomePage();
		await wrapper.vm.$nextTick();
		expect(window.location.href).toBe('/');
	});
});
