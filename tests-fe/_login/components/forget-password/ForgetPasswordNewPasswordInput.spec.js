import { createLocalVue, shallowMount } from '@vue/test-utils';
import ForgetPasswordNewPasswordInput from 'Js/_login/components/forget-password/ForgetPasswordNewPasswordInput.vue';
import Vuex from 'vuex';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import authStore from '../__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.md5 = require('md5');
window.validator = new Validator();
Validator.localize('id', id);

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VeeValidate, {
	locale: 'id'
});

const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD'
	};
});

const stubs = {
	BgField: mockComponent,
	BgInputPassword: mockComponent,
	BgButton: mockComponent,
	MamiLoadingInline: mockComponent
};
const store = new Vuex.Store(authStore);
const swalError = jest.fn();

global.axios = mockAxios;
global.alert = jest.fn();

mockWindowProperty('document.head.querySelector', querySelector);

describe('ForgetPasswordNewPasswordInput.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ForgetPasswordNewPasswordInput, {
			localVue,
			stubs,
			store,
			mocks: { swalError }
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.forget-password-new-password-input').exists()).toBe(
			true
		);
	});

	describe('Computed Properties', () => {
		it('Should return `isPasswordConfirmed` truthy value properly', () => {
			wrapper.setData({
				newPasswordValue: 'password123',
				confirmNewPasswordValue: 'password123'
			});

			wrapper.vm.errors.items = [];

			expect(wrapper.vm.isPasswordConfirmed).toBeTruthy();
		});

		it('Should return `userRolePayloadName` value properly', () => {
			wrapper.vm.$store.state.userRole.key = 'owner';
			expect(wrapper.vm.userRolePayloadName).toBe('owner_edit_password');

			wrapper.vm.$store.state.userRole.key = 'tenant';
			expect(wrapper.vm.userRolePayloadName).toBe('tenant_forget_pwd');
		});
	});

	describe('Methods', () => {
		it('Should validate input on input changes', async () => {
			wrapper.setData({ newPasswordValue: 'mock123' });

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.$validator.errors.any()).toBe(false);
		});
	});

	describe('API Request', () => {
		it('Should handle success flow of submit new password API request properly', async () => {
			const authenticateUserSpy = jest.spyOn(wrapper.vm, 'authenticateUser');

			const mockResponse = {
				data: {
					status: true
				}
			};

			const mockValue = 'password123';
			axios.mockResolve(mockResponse);

			wrapper.vm.submitNewPassword(mockValue);
			await wrapper.vm.$nextTick();
			expect(authenticateUserSpy).toHaveBeenCalled();
		});

		it('Should handle error flow of submit new password API request properly', async () => {
			const mockResponse = {
				data: {
					status: false
				}
			};

			const mockValue = 'password123';
			axios.mockResolve(mockResponse);

			wrapper.vm.submitNewPassword(mockValue);
			await wrapper.vm.$nextTick();
			expect(swalError).toHaveBeenCalled();
		});
	});
});
