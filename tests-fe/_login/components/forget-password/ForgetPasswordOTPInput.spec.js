import ForgetPasswordOTPInput from 'Js/_login/components/forget-password/ForgetPasswordOTPInput';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import store from '../__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';
import MixinAuthTrackers from 'Js/_login/mixins/MixinAuthTrackers';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinAuthTrackers]);

const stubs = {
	BgField: mockComponent,
	BgInput: mockComponent,
	MamiLoadingInline: mockComponent
};

global.tracker = jest.fn();
global.axios = mockAxios;
global.alert = jest.fn();

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

describe('ForgetPasswordOTPInput.vue', () => {
	const wrapper = shallowMount(ForgetPasswordOTPInput, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('Should component exist', () => {
		expect(wrapper.find('.forget-password-o-t-p-input').exists()).toBe(true);
	});

	// Test Case for computed data
	describe('Should return computed properly', () => {
		it('Should return userRolePayloadName value', () => {
			const testCases = [
				{
					userRole: {
						key: 'owner'
					}
				},
				{
					userRole: {
						key: 'tenant'
					}
				}
			];

			testCases.forEach(tc => {
				wrapper.vm.$store.state.userRole = tc.userRole;

				tc.userRole.key === 'owner'
					? expect(wrapper.vm.userRolePayloadName).toBe('owner_edit_password')
					: expect(wrapper.vm.userRolePayloadName).toBe('tenant_forget_pwd');
			});
		});
	});

	describe('Testing all methods', () => {
		const $emit = jest.spyOn(wrapper.vm, '$emit');
		it('Should call API successfully on getOTP()', async () => {
			axios.mockResolve({
				data: {
					status: true,
					meta: {
						response_code: 200,
						severity: 'OK',
						message:
							'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
					},
					verification_code: {
						can_resend_at: 1610602814
					}
				}
			});

			await wrapper.vm.getOTP();

			expect(wrapper.vm.isStartCountDown).toBeTruthy();
		});
		it('Should check OTP by API', async () => {
			axios.mockResolve({
				data: {
					status: true,
					meta: {
						response_code: 200,
						severity: 'OK',
						message:
							'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
					}
				}
			});
			await wrapper.vm.handleVerifyOTP();

			expect($emit).toBeCalled();
		});
	});
});
