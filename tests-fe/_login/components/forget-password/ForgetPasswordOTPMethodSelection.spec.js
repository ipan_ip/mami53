import { createLocalVue, shallowMount } from '@vue/test-utils';
import ForgetPasswordOTPMethodSelection from 'Js/_login/components/forget-password/ForgetPasswordOTPMethodSelection.vue';
import Vuex from 'vuex';
import store from '../__mocks__/store';
import mockComponent from 'tests-fe/utils/mock-component';
import MixinAuthTrackers from 'Js/_login/mixins/MixinAuthTrackers';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinAuthTrackers]);

const stubs = {
	BgToast: mockComponent,
	OTPMethodSelectionWrapper: mockComponent
};

global.tracker = jest.fn();

describe('ForgetPasswordOTPMethodSelection.vue', () => {
	const wrapper = shallowMount(ForgetPasswordOTPMethodSelection, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('Should exists', () => {
		expect(wrapper.find('.forget-password-otp-method-selection').exists()).toBe(
			true
		);
	});

	it('Should handle select OTP method properly', () => {
		const testCases = [
			{
				name: 'falsy',
				currentKey: 1,
				previousKey: 1,
				resendCounter: 60,
				label: 'sms'
			},
			{
				name: 'truthy',
				currentKey: 1,
				previousKey: 0,
				resendCounter: 0,
				label: 'whatsapp'
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.forgetPassword.selectedOTPMethod = {
				key: tc.previousKey,
				label: tc.label
			};
			wrapper.vm.$store.state.forgetPassword.otpResendCounter[tc.label] =
				tc.resendCounter;

			const item = { key: tc.currentKey, label: tc.label };

			wrapper.vm.handleSelectOTPMethod(item);

			tc.name === 'falsy'
				? expect(wrapper.vm.isShowWarningToast).toBe(true)
				: expect(wrapper.emitted('next-step')[0][0]).toBe('input-otp');
		});
	});
});
