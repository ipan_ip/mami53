import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import '@babel/polyfill';
import FormLogin from 'Js/_login/components/login/FormLogin';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { flushPromises } from 'tests-fe/utils/promises';

global.tracker = jest.fn();
const localVue = createLocalVue();

const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD'
	};
});

global.oxOauth2Domain = 'ox-oauth-domain';

const swal = jest.fn();
global.swal = swal;

const axios = {
	post: jest.fn().mockRejectedValue(new Error('error')),
	get: jest.fn()
};
global.axios = axios;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const tracker = jest.fn();
global.tracker = tracker;

const Cookies = { set: jest.fn() };
global.Cookies = Cookies;

const alert = jest.fn();
const locationReplace = jest.fn();

mockWindowProperty('document.head.querySelector', querySelector);
mockWindowProperty('alert', alert);
mockWindowProperty('window.location', { replace: locationReplace });

describe('FormLogin.vue', () => {
	const $router = { push: jest.fn() };

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(FormLogin, {
			localVue,
			props: {
				backButton: false,
				mode: '',
				user: 'user',
				fromBookingLanding: false
			},
			stubs: {
				InputWithValidator: mockComponent,
				FormLoginSocial: mockComponent,
				RouterLink: RouterLinkStub
			},
			mocks: { $router, swalError: jest.fn() }
		});
	});

	it('Should redirect user as owner to reset password owner', () => {
		wrapper.setProps({
			user: 'owner'
		});

		const gotoResetPasswordSpy = jest.spyOn(wrapper.vm, 'gotoResetPassword');
		wrapper.setMethods({ gotoResetPassword: gotoResetPasswordSpy });

		const resetPasswordLink = wrapper.find(
			'[data-path="btn_goToResetPassword"]'
		);

		expect(resetPasswordLink.exists()).toBe(true);
		resetPasswordLink.trigger('click');

		expect(gotoResetPasswordSpy).toHaveBeenCalled();
		expect($router.push).toBeCalledWith('/lupa-password-pemilik');
	});

	it('Should track user as owner click reset password link event properly', () => {
		wrapper.setProps({
			user: 'owner'
		});

		wrapper.vm.trackForgetPassword();

		expect(global.tracker).toBeCalledWith('moe', [
			'[Owner] Login - Click Forget Password',
			{ interface: 'desktop' }
		]);
	});

	it('Should track user as tenant click reset password link event properly', () => {
		wrapper.vm.trackForgetPassword();

		expect(global.tracker).toBeCalledWith('moe', [
			'[User] Login - Click Forget Password',
			{ interface: 'desktop' }
		]);
	});

	it('Should redirect user back to login page when back button is clicked', async () => {
		wrapper.setProps({ backButton: true });
		const handleBackButtonSpy = jest.spyOn(wrapper.vm, 'handleBackButton');
		wrapper.setMethods({ handleBackButton: handleBackButtonSpy });

		await wrapper.vm.$nextTick();

		const backButton = wrapper.find('.login-exit');
		expect(backButton.exists()).toBe(true);
		backButton.trigger('click');

		expect(handleBackButtonSpy).toHaveBeenCalled();
		expect($router.push).toBeCalledWith('login');
	});

	it('Should return correct reset password link', () => {
		wrapper.setProps({ user: 'owner' });
		expect(wrapper.vm.routleLinkResetPassword).toBe('/lupa-password-pemilik');

		wrapper.setProps({ user: 'tenant' });
		expect(wrapper.vm.routleLinkResetPassword).toBe('/lupa-password-pencari');
	});

	it('Should validate email properly', () => {
		const invalidEmail = ['invalid-email', '@mail.com', 'invalid@mail'];
		const validEmail = 'valid_email@mail.com';

		invalidEmail.forEach(email => {
			expect(wrapper.vm.validateEmail(email)).toBe(false);
		});

		expect(wrapper.vm.validateEmail(validEmail)).toBe(true);
	});

	it('Should validate phone properly', () => {
		const invalidPhone = ['00011232', '+222000000', ''];
		const validPhone = '082727272727';

		invalidPhone.forEach(phone => {
			expect(wrapper.vm.validatePhone(phone)).toBe(false);
		});

		expect(wrapper.vm.validatePhone(validPhone)).toBe(true);
	});

	it('Should go to register page properly', () => {
		wrapper.vm.gotoRegisterPage();
		expect($router.push).toBeCalledWith('login');
	});

	describe('submitLogin', () => {
		it('should show alert when user submit invalid phone number', async () => {
			jest.useFakeTimers();
			await wrapper.setData({ phoneNumber: '000222' /* invalid */ });
			const formInput = wrapper.find('form.input-form');
			formInput.trigger('submit.prevent');

			jest.advanceTimersByTime(500);

			expect(swal).toBeCalledWith(expect.objectContaining({ type: 'error' }));
			jest.useRealTimers();
		});

		it('should call submit login if given valid phone number', async () => {
			jest.clearAllMocks();
			jest.useFakeTimers();
			const spySubmitLogin = jest.spyOn(wrapper.vm, 'submitLoginApi');
			await wrapper.setData({ phoneNumber: '082727272727' /* valid */ });
			const formInput = wrapper.find('form.input-form');
			formInput.trigger('submit.prevent');

			jest.advanceTimersByTime(500);

			expect(swal).not.toBeCalled();
			expect(spySubmitLogin).toBeCalledWith(
				expect.objectContaining({ phone_number: '082727272727' })
			);
			jest.useRealTimers();
			spySubmitLogin.mockRestore();
		});
	});

	describe('submitLoginApi', () => {
		const parameterLogin = {
			password: 'asmlkXISla123kas',
			_token: 'token',
			phone_number: '082727272727'
		};

		it('should call bugsnag and alert if there is error on submit', async () => {
			jest.clearAllMocks();
			global.axios.post = jest.fn().mockRejectedValue(new Error('error'));
			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(bugsnagClient.notify).toBeCalled();
			expect(alert).toBeCalled();
		});

		it('should send tracker when login return error message', async () => {
			jest.clearAllMocks();
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'fail reason'
					}
				}
			});
			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(tracker).toBeCalledWith('moe', [
				'[User] Login',
				expect.objectContaining({
					login_result: false,
					fail_reason: 'fail reason'
				})
			]);
		});

		it('should redirect properly when user login successfully', async () => {
			jest.clearAllMocks();
			wrapper.setProps({ user: 'tenant' });
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true,
					redirect_path: '/some-redirect-path'
				}
			});
			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(tracker).toBeCalledWith('moe', [
				'[User] Login',
				expect.objectContaining({
					login_result: true,
					fail_reason: null
				})
			]);

			expect(locationReplace).toBeCalledWith('/some-redirect-path');
		});

		it('should redirect properly when user login successfully', async () => {
			jest.clearAllMocks();
			wrapper.setProps({ user: 'tenant' });
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true,
					redirect_path: '/some-redirect-path'
				}
			});
			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(tracker).toBeCalledWith('moe', [
				'[User] Login',
				expect.objectContaining({
					login_result: true,
					fail_reason: null
				})
			]);

			expect(locationReplace).toBeCalledWith('/some-redirect-path');
		});

		it('should redirect to specified page when user login successfully in not modal type login', async () => {
			jest.clearAllMocks();
			wrapper.setProps({
				user: 'tenant',
				redirectPath: '/props-redirect-path',
				mode: ''
			});
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true,
					redirect_path: '/some-redirect-path'
				}
			});
			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(tracker).toBeCalledWith('moe', [
				'[User] Login',
				expect.objectContaining({
					login_result: true,
					fail_reason: null
				})
			]);

			expect(locationReplace).toBeCalledWith('/props-redirect-path');
		});

		it('should get owner data and redirect properly when owner successfully logged in', async () => {
			jest.clearAllMocks();
			await wrapper.setProps({ user: 'owner', fromBookingLanding: true });
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true,
					membership: {
						is_mamipay_user: true
					}
				}
			});

			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(locationReplace).toBeCalledWith('/ownerpage/manage/all/bill');
		});

		it('should report to bugsnag when owner successfully logged in but error on getting owner data', async () => {
			jest.clearAllMocks();
			await wrapper.setProps({ user: 'owner', fromBookingLanding: true });
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			global.axios.get = jest.fn().mockRejectedValue(new Error('error'));

			await wrapper.vm.submitLoginApi(parameterLogin);
			await flushPromises();

			expect(locationReplace).not.toBeCalled();
			expect(bugsnagClient.notify).toBeCalled();
		});
	});
});
