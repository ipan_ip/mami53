import { shallowMount, createLocalVue } from '@vue/test-utils';
import FormLoginSocial from 'Js/_login/components/login/FormLoginSocial';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

mockWindowProperty('document.head.querySelector', () => {
	return {
		content: 'token'
	};
});

mockWindowProperty('window.location', {
	href: 'http://test.com/login',
	origin: 'http://test.com',
	pathname: 'test-som-path-name'
});

describe('FormLoginSocial.vue', () => {
	const localVue = createLocalVue();
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(FormLoginSocial, {
			localVue
		});
	});

	it('Should have facebook login buttons', () => {
		expect(wrapper.findAll('.btn-facebook').exists()).toBe(true);
	});
	it('Should have google login buttons', () => {
		expect(wrapper.findAll('.btn-google').exists()).toBe(true);
	});

	it('Should have facebook login "form" ', () => {
		const form = wrapper.find('.form-login-facebook');
		expect(form.exists()).toBe(true);
	});

	it('Should return converted url properly', async () => {
		expect(wrapper.vm.convertedUrl).toBe('http://test.com/login');

		await wrapper.setProps({ redirectPath: '/redirect-path' });
		expect(wrapper.vm.convertedUrl).toBe('http://test.com/redirect-path');
	});

	// TODO : create unit test for form action
});
