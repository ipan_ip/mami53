import { shallowMount, createLocalVue } from '@vue/test-utils';
import FormSelect from 'Js/_login/components/login/FormSelect';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const locationReplace = jest.fn();

mockWindowProperty('window.location', {
	href: 'http://test.com/login?interface=android' /* webview */,
	replace: locationReplace
});

describe('FormSelect.vue', () => {
	const localVue = createLocalVue();
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(FormSelect, {
			localVue,
			mocks: {
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should have login title', () => {
		expect(
			wrapper.find('.login-select-container .modal-login-title').exists()
		).toBe(true);
	});

	it('Should have user state buttons', () => {
		expect(wrapper.findAll('.btn-login-selector').exists()).toBe(true);
	});

	it('Should emit state changes to the right role when user clicked state button', () => {
		const spyState = jest.spyOn(wrapper.vm, 'selectState');
		wrapper.setMethods({ selectState: spyState });

		wrapper.find('.login-owner-home').trigger('click');
		expect(spyState).toBeCalledWith('owner');
		wrapper.find('.login-user-home').trigger('click');
		expect(spyState).toBeCalledWith('tenant');
	});

	it('Should redirect to home properly', async () => {
		await wrapper.setProps({ isModal: false });
		const backButton = () => wrapper.find('.exit');

		// webview
		backButton().trigger('click');

		expect(locationReplace).toBeCalledWith(
			'bang.kerupux.com://owner_profile?home=true'
		);
	});
});
