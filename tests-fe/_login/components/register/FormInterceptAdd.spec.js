import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import Vuex from 'vuex';
import FormInterceptAdd from 'Js/_login/components/register/FormInterceptAdd';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	state: {
		profile: {
			phoneNumber: ''
		},
		token: ''
	}
});

window.open = jest.fn();
window.open('foo');

describe('FormInterceptAdd.vue', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(FormInterceptAdd, {
			localVue,
			store
		});
	});

	it('Should redirect to previous page when user clicked back button', () => {
		const handleCloseInterceptSpy = jest.spyOn(
			wrapper.vm,
			'handleCloseIntercept'
		);
		const exitButton = wrapper.find('.intercept-exit');
		wrapper.setMethods({ handleCloseIntercept: handleCloseInterceptSpy });

		expect(exitButton.exists()).toBe(true);
		exitButton.trigger('click');
		expect(handleCloseInterceptSpy).toHaveBeenCalled();
		expect(wrapper.emitted('closed')).toBeTruthy();
	});

	it('Should render ad type list section properly', () => {
		expect(wrapper.find('.intercept-radio').exists()).toBe(true);
		expect(wrapper.findAll('.intercept-radio').length).toEqual(
			wrapper.vm.listingTypeOptions.length
		);
	});

	it('Should change user role to be matched with selected ad type list', () => {
		const watchListingTypeSpy = jest.spyOn(wrapper.vm, 'watchListingType');
		wrapper.setMethods({ watchListingType: watchListingTypeSpy });

		wrapper.vm.$options.watch.listingType.call(wrapper.vm, 'kost');
		expect(watchListingTypeSpy).toHaveBeenCalled();
		expect(wrapper.vm.inputAs).toBe('Pemilik kos');

		wrapper.vm.$options.watch.listingType.call(wrapper.vm, 'apartment');
		expect(watchListingTypeSpy).toHaveBeenCalled();
		expect(wrapper.vm.inputAs).toBe('Pemilik apartemen');

		wrapper.vm.$options.watch.listingType.call(wrapper.vm, 'vacancy');
		expect(watchListingTypeSpy).toHaveBeenCalled();
		expect(wrapper.vm.inputAs).toBe('Employer');
	});

	it('Should redirect to input "Loker" page when Add Vacancy Ad Button clicked', async () => {
		const openInputVacancySpy = jest.spyOn(wrapper.vm, 'openInputVacancy');
		wrapper.setMethods({ openInputVacancySpy: 'openInputVacancy' });
		wrapper.vm.listingType = 'vacancy';

		await wrapper.vm.$nextTick();

		wrapper.find('form').trigger('submit.prevent');

		await wrapper.vm.$nextTick();

		expect(openInputVacancySpy).toHaveBeenCalled();
		expect(window.open).toHaveBeenCalledWith('foo');
	});
});
