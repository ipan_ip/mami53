import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueRecaptcha from 'vue-recaptcha';
import FormRegister from 'Js/_login/components/register/FormRegister.vue';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = { template: '<div />' };

const stubs = {
	InputWithValidator: mockComponent,
	'router-link': mockComponent,
	VueRecaptcha
};

const captchaKey = 'test';

const store = new Vuex.Store({
	state: {
		profile: {
			username: 'John Doe',
			phoneNumber: '08123456789',
			email: 'john@example.com'
		},
		captchaKey
	},
	mutations: {
		setRegistrationData: jest.fn()
	}
});

const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD' // token
	};
});

mockWindowProperty('document.head.querySelector', querySelector);

global.tracker = jest.fn();
global.swal = jest.fn().mockResolvedValue(true);
window.md5 = require('md5');

describe('FormRegister.vue', () => {
	const validateRegistrationDataSpy = jest.spyOn(
		FormRegister.methods,
		'validateRegistrationData'
	);
	const goToVerificationSpy = jest.spyOn(
		FormRegister.methods,
		'goToVerification'
	);
	const storeRegistrationDataSpy = jest.spyOn(
		FormRegister.methods,
		'storeRegistrationData'
	);
	const checkErrorFieldSpy = jest.spyOn(
		FormRegister.methods,
		'checkErrorField'
	);
	const resetCaptchaSpy = jest.spyOn(FormRegister.methods, 'resetCaptcha');

	let wrapper;

	beforeEach(() => {
		jest.useFakeTimers();

		wrapper = shallowMount(FormRegister, {
			localVue,
			stubs,
			store,
			propsData: {
				userTypeText: 'Test Type',
				user: 'user',
				errorRegisterMessages: {
					phone_number: {
						message: 'Error gan'
					},
					email: {
						message: 'Error gan'
					}
				}
			}
		});

		wrapper.vm.$refs = {
			recaptcha: { reset: jest.fn() }
		};
	});

	it('Should exists', () => {
		expect(wrapper.find('.register-form').exists()).toBe(true);
	});

	describe('Computed Properties', () => {
		it('Should return proper captchaKey', () => {
			expect(wrapper.vm.captchaKey).toBe(captchaKey);
		});

		it('Should return disable submit action properly', () => {
			wrapper.setData({ password: 'hehelmao' });
			expect(wrapper.vm.isSubmitDisabled).toBe(false);

			wrapper.setData({ password: '' });
			expect(wrapper.vm.isSubmitDisabled).toBe(true);
		});

		it('Should return trackers tag name properly', () => {
			const testCases = [
				{
					role: 'owner',
					tagName: '[Owner]',
					gtmTagClass: 'track-register-owner'
				},
				{
					role: 'user',
					tagName: '[User]',
					gtmTagClass: 'track-register-tenant'
				}
			];

			testCases.forEach(tc => {
				wrapper.setProps({ user: tc.role });

				expect(wrapper.vm.trackerTag).toBe(tc.tagName);
				expect(wrapper.vm.gtmTrackRegisterClass).toBe(tc.gtmTagClass);
			});
		});

		it('Should return proper phoneCustomErrorMessage', () => {
			const testCases = [
				{
					isServerError: true,
					terminatedPhoneMessage: wrapper.vm.errorRegisterMessages.phone_number,
					terminatedEmailMessage: wrapper.vm.errorRegisterMessages.email
				},
				{
					isServerError: false,
					terminatedPhoneMessage: '',
					terminatedEmailMessage: ''
				}
			];

			testCases.forEach(tc => {
				const phoneNumberMessages = [
					{
						typeValidation: 'regex',
						text: 'Mohon di awali dengan 08'
					},
					{
						typeValidation: 'isSubmitTerminated',
						text: tc.terminatedPhoneMessage
					}
				];
				const emailMessages = [
					{
						typeValidation: 'regex',
						text: 'Mohon masukan email yang valid'
					},
					{
						typeValidation: 'isSubmitTerminated',
						text: tc.terminatedEmailMessage
					}
				];

				if (!tc.isServerError)
					wrapper.setProps({ errorRegisterMessages: null });

				expect(wrapper.vm.phoneCustomErrorMessage).toEqual(phoneNumberMessages);
				expect(wrapper.vm.emailCustomErrorMessage).toEqual(emailMessages);
			});
		});
	});

	describe('Methods', () => {
		it('Should redirect to homepage when closed', () => {
			wrapper.vm.handleCloseButton();
			mockWindowProperty('window.location', { href: '/' });
			expect(window.location.href).toBe('/');
			jest.clearAllMocks();
		});

		it('Should handle submit registration properly', () => {
			// Ensure registration data is valid
			wrapper.setData({ password: 'hahalmao' });
			wrapper.vm.validateRegistrationData();
			expect(validateRegistrationDataSpy).toHaveReturnedWith(true);

			const testCases = [
				{
					name: 'isCaptchaVerified',
					isCaptchaVisible: true,
					isVerifiedByCaptcha: true
				},
				{
					name: 'isCaptchaNotVerified',
					isCaptchaVisible: true,
					isVerifiedByCaptcha: false
				},
				{
					name: 'isCaptchaHidden',
					isCaptchaVisible: false,
					isVerifiedByCaptcha: false
				},
				{
					name: 'error',
					isCaptchaVisible: false,
					isVerifiedByCaptcha: false
				}
			];

			testCases.forEach(tc => {
				// make registration data invalid on error case
				if (tc.name === 'error') wrapper.setData({ password: '' });

				wrapper.setData({
					isCaptchaVisible: tc.isCaptchaVisible,
					isVerifiedByCaptcha: tc.isVerifiedByCaptcha
				});

				wrapper.vm.submitRegistration();

				switch (tc.name) {
					case 'isCaptchaVerified':
						expect(goToVerificationSpy).toBeCalled();
						break;
					case 'isCaptchaNotVerified':
						expect(wrapper.vm.isCaptchaWarningVisible).toBe(true);
						break;
					case 'isCaptchaHidden':
						expect(wrapper.vm.isCaptchaVisible).toBe(true);
						break;
					default:
						expect(global.swal).toBeCalled();
				}
			});
		});

		it('Should handle go to verification method properly', async done => {
			wrapper.vm.goToVerification();

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(100);
			jest.useRealTimers();

			setTimeout(() => {
				expect(storeRegistrationDataSpy).toBeCalled();
				expect(resetCaptchaSpy).toBeCalled();
				done();
			}, 0);
		});

		it('Should handle verify captcha properly', () => {
			const mockPayload = 'looks good!';

			wrapper.vm.verifyCaptcha(mockPayload);

			expect(wrapper.vm.isCaptchaWarningVisible).toBe(false);
			expect(wrapper.vm.captchaResponse).toEqual(mockPayload);
			expect(wrapper.vm.isVerifiedByCaptcha).toBe(true);
		});

		it('Should emit reset error message properly', async () => {
			wrapper.vm.resetErrorMessage();

			await wrapper.vm.$nextTick();

			expect(wrapper.emitted('reset')[0][0]).toEqual(
				wrapper.vm.errorRegisterMessages
			);
		});

		it('Should handle error field to return error message properly', () => {
			const field = 'test';

			wrapper.vm.checkErrorField(field);

			expect(checkErrorFieldSpy).toHaveReturnedWith(
				Object.keys(wrapper.vm.errorRegisterMessages).includes(field)
			);

			wrapper.setProps({
				errorRegisterMessages: null
			});

			wrapper.vm.checkErrorField(field);

			expect(checkErrorFieldSpy).toHaveReturnedWith(false);
		});
	});
});
