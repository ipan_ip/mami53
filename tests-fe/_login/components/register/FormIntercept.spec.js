import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import FormIntercept from 'Js/_login/components/register/FormIntercept';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	state: {
		profile: {
			username: ''
		}
	}
});

describe('FormIntercept.vue', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(FormIntercept, {
			localVue,
			store,
			propsData: {
				userTypeText: '',
				user: ''
			}
		});
	});

	it('Should display username properly', async () => {
		wrapper.vm.$store.state.profile.username = 'John Doe';

		expect(wrapper.vm.storedUsername).toBe('John Doe');

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.intercept-title').text()).toBe(
			`Hai ${wrapper.vm.storedUsername},`
		);
	});

	it('Should emit add-kost event when the user as owner clicked on "Pasang Iklan" button', async () => {
		wrapper.setProps({
			user: 'owner'
		});
		const interceptButton = wrapper.find('.intercept-button');

		expect(interceptButton.exists()).toBe(true);
		interceptButton.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('add-kost')).toBeTruthy();
	});

	it('Should redirect window.location to /cari when user as tenant clicked on "Cari Kos Sekarang" button', () => {
		const replace = jest.fn();
		mockWindowProperty('window.location', { replace });

		wrapper.setProps({
			user: 'tenant'
		});
		const interceptButton = wrapper.find('.intercept-button');

		expect(interceptButton.exists()).toBe(true);
		interceptButton.trigger('click');

		expect(window.location).toBe('/cari');
	});

	it('Should redirect window.location to /ownerpage when user as owner clicked on intercept link', () => {
		window.oxWebUrl = 'oxWebUrl';
		const replace = jest.fn();
		mockWindowProperty('window.location', { replace });

		wrapper.setProps({
			user: 'owner'
		});
		const interceptLink = wrapper.find('.intercept-link');

		expect(interceptLink.exists()).toBe(true);
		interceptLink.trigger('click');

		expect(window.location).toBe(window.oxWebUrl);
	});

	it('Should redirect window.location to home when user as tenant clicked on intercept link', () => {
		const replace = jest.fn();
		mockWindowProperty('window.location', { replace });

		wrapper.setProps({
			user: 'tenant'
		});
		const interceptLink = wrapper.find('.intercept-link');

		expect(interceptLink.exists()).toBe(true);
		interceptLink.trigger('click');

		expect(window.location).toBe('/');
	});
});
