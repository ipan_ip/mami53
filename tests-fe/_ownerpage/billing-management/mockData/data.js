export const roomAllotment = {
	id: 123,
	name: 'allotment name',
	floor: 'allotment floor'
};

export const userRooms = [
	{
		id: 123,
		booking_status: true,
		room_title: 'room 123'
	},
	{
		id: 1234,
		booking_status: false,
		room_title: 'room 1234'
	}
];

export const schedules = [
	{
		scheduled_date_raw: '2020/02/02',
		status: 'paid',
		amount: 2000,
		payment_status_formatted: 'diluar mamipay',
		payment_description: 'description',
		invoice_link: 'link',
		manual_reminder: null,
		id: 1,
		invoice_number: 123,
		payment_status: 'not_in_mamipay'
	}
];

export const contractData = {
	contract_profile: {
		name: 'tenant name',
		tenant_photo: {},
		start_date: '2020/02/02',
		end_date: '2020/12/12'
	},
	billing_rule: { room_id: 123 },
	contract_type: 'paid',
	termination_info: false,
	has_checked_in: true,
	checkin_time: '2020/02/02'
};
