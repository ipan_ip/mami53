import mockWindowProperty from 'tests-fe/utils/mock-window-property';

mockWindowProperty(
	'document.head.querySelector',
	jest.fn().mockImplementation(() => ({ content: 'csrf-token-123' }))
);

global.oxWebUrl = 'https://owner.test.com';
