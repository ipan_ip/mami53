import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ProfileTenant from 'Js/_ownerpage/billing-management/components/profile-tenant/ProfileTenant';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('ProfileTenant.vue', () => {
	const mockStore = {
		state: {
			tenantProfile: {
				tenantProfile: { roomAllotment: {} },
				widgetInfo: { tenantName: 'Harry Styles', roomNumber: 'Kamar A01' },
				statusPage: 'member'
			},
			manageBooking: { acceptBookingProfile: { name: 'Chris Evans' } }
		}
	};

	it('should load the component', () => {
		const wrapper = shallowMount(ProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#billingManagementProfileTenant').exists()).toBe(true);
	});

	it('should render correct tenant name', () => {
		const wrapper = shallowMount(ProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});
		expect(wrapper.find('.name-tenant').text()).toBe('Harry Styles');
	});

	it('should render the correct tenant name when status page is accept booking', () => {
		mockStore.state.tenantProfile.statusPage = 'acceptBooking';
		const wrapper = shallowMount(ProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('.name-tenant').text()).toBe('Chris Evans');
	});
});
