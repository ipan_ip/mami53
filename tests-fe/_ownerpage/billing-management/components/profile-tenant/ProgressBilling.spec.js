import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ProgressBilling from 'Js/_ownerpage/billing-management/components/profile-tenant/ProgressBilling';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ProgressBilling.vue', () => {
	const mockStore = {
		state: {
			tenantProfile: { widgetInfo: { totalTransferredAmount: 1000000 } }
		}
	};
	const wrapper = shallowMount(ProgressBilling, {
		localVue,
		store: new Vuex.Store(mockStore)
	});

	it('should load the component', () => {
		expect(wrapper.find('#billingManagementProgressBilling').exists()).toBe(
			true
		);
	});
});
