import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import '../../mockData/window';
import ContentPayment from 'Js/_ownerpage/billing-management/components/profile-tenant/ContentPayment';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import defaultStore from 'Js/_ownerpage/billing-management/store/index';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;
mockWindowProperty('oxWebUrl', '/ownerpage');
mockWindowProperty('window.location', { href: '/' });

global.axios = {
	post: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: {}
		}
	})
};

global.bugsnagClient = { notify: jest.fn() };

describe('ContentPayment.vue', () => {
	const $toasted = { show: jest.fn() };
	const navigator = { isMobile: false };

	const wrapper = shallowMount(ContentPayment, {
		localVue,
		store: new Vuex.Store(defaultStore),
		mocks: {
			$toasted,
			navigator,
			openSwalLoading: jest.fn(),
			swalError: jest.fn()
		}
	});

	it('should render the correct component', () => {
		expect(wrapper.find('#billingManagementContentPayment').exists()).toBe(
			true
		);
	});

	it('should open detail tagihan page', () => {
		wrapper.vm.openDetailBill({
			column: { field: '' },
			row: { menu: 12345 }
		});

		expect(window.location).toBe(
			'/ownerpage/billing-management/12345/detail-bill?fromTenantProfile=true'
		);
	});

	it('should return late days', () => {
		expect(wrapper.vm.diffFromToday(new Date())).toEqual(0);
	});

	it('should open modal send notification', () => {
		wrapper.vm.openModalSendNotification(12345);
		expect(wrapper.vm.lastInvoiceId).toBe(12345);
		expect(wrapper.vm.isModalNotification).toBe(true);
	});

	it('should send notification', () => {
		const spySendNotificationToTenant = jest.spyOn(
			wrapper.vm,
			'sendNotificationToTenant'
		);
		wrapper.vm.openModalSendNotification(12345);
		wrapper.vm.sendNotification();

		expect(spySendNotificationToTenant).toBeCalled();
		expect(wrapper.vm.isModalNotification).toBe(false);

		spySendNotificationToTenant.mockRestore();
	});
});
