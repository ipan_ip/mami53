import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../../mockData/window';
import 'tests-fe/utils/mock-vue';
import BillingManagementMain from 'Js/_ownerpage/billing-management/components/profile-tenant/BillingManagementMain';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import defaultStore from 'Js/_ownerpage/billing-management/store/index';
import * as storage from 'Js/@utils/storage';
import {
	roomAllotment,
	userRooms,
	schedules,
	contractData
} from '../../mockData/data';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const $route = {
	name: 'newMember',
	params: {
		typeProfile: 'new-member',
		tenantId: 1234
	},
	query: {}
};

const mockStore = { ...defaultStore };

mockStore.mutations = {
	...mockStore.mutations,
	setRentPeriode: jest.fn(),
	setWorkplace: jest.fn(),
	setTenantBirthday: jest.fn(),
	setOwnerTermsAgreement: jest.fn()
};

const initStoreData = storeData => {
	storeData.commit('setRoomAllotment', roomAllotment);
	storeData.commit('setAcceptBookingProfile', {
		name: 'tenant name'
	});
	storeData.commit('setListBookingSelected', {
		id: 123
	});
	storeData.commit('setUserRooms', userRooms);
	storeData.commit('setUserRoom', { id: 123 });
	storeData.commit('setWidgetInfo', { roomNumber: 'room number' });
	return storeData;
};

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;

const store = new Vuex.Store(mockStore);
initStoreData(store);

const GlobalNavbar = { template: '<div />' };

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

let contractApiResponse = jest.fn().mockResolvedValue({
	data: {
		status: true,
		...contractData
	}
});

const initAxios = () => ({
	get: (endpoint = '') => {
		if (new RegExp('$/contract/.+/payment-schedules/.+$`').test(endpoint)) {
			return jest.fn().mockResolvedValue({
				data: {
					status: true,
					schedules: schedules
				}
			})();
		} else if (new RegExp('^/contract/.+').test(endpoint)) {
			return contractApiResponse();
		} else if (new RegExp('^room/list$').test(endpoint)) {
			return jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: userRooms
				}
			})();
		} else if (new RegExp('^/room/detail/.+$').test(endpoint)) {
			return jest.fn().mockResolvedValue({
				data: {
					status: true,
					room: {
						price_daily: '200000',
						price_weekly: '400000',
						price_monthly: '1200000',
						price_quarterly: '3600000',
						price_semiannualy: '7200000',
						price_yearly: '14400000'
					}
				}
			})();
		} else if (new RegExp('^/owner/data/update/.+$').test(endpoint)) {
			return jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {}
				}
			})();
		}

		return jest.fn().mockResolvedValue({ data: { data: {}, status: true } })();
	}
});

global.axios = initAxios();

describe('BillingManagementMain.vue', () => {
	let wrapper;

	beforeEach(async () => {
		initStoreData(store);
		wrapper = shallowMount(BillingManagementMain, {
			localVue,
			store,
			mocks: {
				$route,
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			},
			stubs: { GlobalNavbar, RouterView: true }
		});

		initStoreData(wrapper.vm.$store);
		await wrapper.vm.$nextTick();
	});

	it('should render billing management', () => {
		expect(wrapper.find('#billingManagementMain').exists()).toBe(true);
	});

	it('should return room number properly', () => {
		initStoreData(wrapper.vm.$store);
		expect(wrapper.vm.tenantRoom).toBe('allotment name allotment floor');

		wrapper.vm.$store.state.tenantProfile.tenantProfile.roomAllotment = null;
		expect(wrapper.vm.tenantRoom).toBe('Kamar room number');

		wrapper.vm.$store.state.tenantProfile.widgetInfo.roomNumber = '';
		expect(wrapper.vm.tenantRoom).toBe('Kamar Pilih di Tempat');
	});

	it('should return tenant data properly', () => {
		expect(wrapper.vm.tenantDataName).toBe('tenant name');
	});

	it('should handle close checkin notification banner', async () => {
		wrapper.setData({ isTenantCheckin: true });
		wrapper.vm.closeCheckinNotificationBanner();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isTenantCheckin).toBe(false);
	});

	it('should handle terminate request submitted properly', () => {
		wrapper.setData({ terminationInfo: { status: 'waiting' } });
		wrapper.vm.handleTerminateRequestSubmitted(true);
		expect(wrapper.vm.terminationInfo.status).toBe('confirmed');
		expect(wrapper.vm.isShowTerminationCard).toBe(true);

		wrapper.vm.handleTerminateRequestSubmitted(false);
		expect(wrapper.vm.terminationInfo.status).toBe('rejected');
		expect(wrapper.vm.isShowTerminationCard).toBe(false);
	});

	it('should return full year properly', () => {
		expect(wrapper.vm.formatDate('02-02-2020')).toBe('Periode 2020');
	});

	it('should get payment data properly', () => {
		wrapper.vm.$route.params.tenantId = 1010;
		const spyGetContractBill = jest.spyOn(wrapper.vm, 'getContractBill');
		wrapper.vm.getDataPayment('02-02-2020');

		expect(spyGetContractBill).toBeCalledWith(1010, '2020');
		spyGetContractBill.mockRestore();
	});

	it('should get room properly', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.$route.params.tenantId = 12;
		wrapper.vm.getRoom(1212);
		await wrapper.vm.$nextTick();
		await new Promise(resolve => setImmediate(resolve));

		expect(spyCommit).toBeCalledWith('setRentCountList', expect.any(Object));

		spyCommit.mockRestore();
	});

	it('should return checkin time properly', () => {
		wrapper.setData({ tenantCheckinTime: '2020/02/02' });
		expect(wrapper.vm.checkinTextTime).toContain('2 February 2020');
	});

	it('should return banner checkin state properly', () => {
		storage.local.removeItem(`banner-checkin-close-123`);

		expect(wrapper.vm.isBannerCheckinClose).toBe(false);
	});

	it('should return roomList properly', () => {
		expect(wrapper.vm.roomList).toEqual([{ ...userRooms[0] }]);
	});

	it('should navigate back properly', () => {
		const windowOpen = jest.fn();
		const windowReload = jest.fn();
		const windowClose = jest.fn();
		const historyGo = jest.fn();

		mockWindowProperty('window.open', windowOpen);
		mockWindowProperty('window.history.go', historyGo);
		mockWindowProperty('window.close', windowClose);
		mockWindowProperty('window.location', {
			href: 'http://test.com/hello?from=accept-contract',
			reload: windowReload
		});

		wrapper.vm.backPrevPage();
		expect(windowOpen).toBeCalledWith('https://owner.test.com', '_self');

		mockWindowProperty('window.location', {
			href: 'http://test.com/hello',
			reload: windowReload
		});
		mockWindowProperty('document.referrer', '');
		wrapper.vm.checkHomeReturn();
		wrapper.vm.backPrevPage();

		expect(windowReload).toBeCalled();

		const tempDocument = window.document;

		jest.clearAllMocks();
		Object.defineProperty(window, 'document', {
			value: {
				location: {
					value: {
						href: 'http://test.com/hello'
					},
					writable: true,
					configurable: true
				}
			},
			writable: true,
			configurable: true
		});
		mockWindowProperty('document.referrer', 'http://test.com/hello');
		wrapper.vm.checkHomeReturn();
		wrapper.vm.backPrevPage();

		expect(windowReload).not.toBeCalled();
		expect(windowClose).toBeCalled();

		window.document = tempDocument;

		windowReload.mockRestore();
		windowClose.mockRestore();
	});

	it('should handle error in fetching contract data', async () => {
		contractApiResponse = jest.fn().mockRejectedValue('error');
		global.axios = initAxios();

		wrapper.vm.getContract(1);
		await new Promise(resolve => setImmediate(resolve));

		expect(bugsnagClient.notify).toBeCalledWith('error');
		expect(wrapper.emitted('error')[0]).toBeTruthy();

		contractApiResponse = jest
			.fn()
			.mockResolvedValue({ data: { status: false } });
		global.axios = initAxios();

		wrapper.vm.getContract(1);
		await new Promise(resolve => setImmediate(resolve));

		expect(wrapper.emitted('error')[1]).toBeTruthy();
	});

	describe('status page', () => {
		const mount = route => {
			const mockStore = initStoreData(store);

			return shallowMount(BillingManagementMain, {
				localVue,
				store: mockStore,
				mocks: {
					$route: route || $route,
					openSwalLoading: jest.fn(),
					closeSwalLoading: jest.fn(),
					swalError: jest.fn()
				},
				stubs: { GlobalNavbar, RouterView: true }
			});
		};

		it('should set new member page', () => {
			const wrapper = mount({ params: { typeProfile: 'new-member' } });
			expect(wrapper.vm.statusPage).toBe('newMember');
		});

		it('should set accept booking page', () => {
			const wrapper = mount({ params: { typeProfile: 'accept-booking' } });
			expect(wrapper.vm.statusPage).toBe('acceptBooking');
		});

		it('should set member page', () => {
			const wrapper = mount({
				params: { typeProfile: 'member' },
				name: 'tenantBill'
			});

			expect(wrapper.vm.statusPage).toBe('member');
			expect(wrapper.vm.isTenantBillPage).toBe(true);
		});
	});

	describe('beforeRouteEnter', () => {
		const mockStore = initStoreData(store);

		wrapper = shallowMount(BillingManagementMain, {
			localVue,
			store: mockStore,
			mocks: {
				$route,
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			},
			stubs: { GlobalNavbar, RouterView: true }
		});

		it('should set previous page properly', () => {
			wrapper.vm.$options.beforeRouteEnter.call(
				wrapper.vm,
				undefined,
				'/from',
				jest.fn(fn => {
					fn(wrapper.vm);
				})
			);

			expect(wrapper.vm.previousRoute).toBe('/from');
		});
	});
});
