import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../../mockData/window';
import BillMenu from 'Js/_ownerpage/billing-management/components/profile-tenant/BillMenu';
import Vuex from 'vuex';
import defaultStore from 'Js/_ownerpage/billing-management/store/index';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const mockStore = { ...defaultStore };
mockStore.modules.tenantProfile.state.billingProfile = [
	{
		status: 'paid'
	}
];

const store = new Vuex.Store(mockStore);

describe('BillMenu', () => {
	const wrapper = shallowMount(BillMenu, {
		localVue,
		store,
		propsData: { dataIndex: 0, invoiceId: 1 }
	});

	it('should render bill menu', () => {
		expect(wrapper.find('#billingManagementBillMenu').exists()).toBe(true);
	});
});
