import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../../mockData/window';
import 'tests-fe/utils/mock-vue';
import ProfileMenu from 'Js/_ownerpage/billing-management/components/profile-tenant/ProfileMenu';
import defaultStore from 'Js/_ownerpage/billing-management/store/index';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(defaultStore);

global.axios = {
	delete: jest.fn().mockResolvedValue({
		data: {
			status: true
		}
	})
};

global.bugsnagClient = { notify: jest.fn() };

const $toasted = { show: jest.fn() };

describe('ProfileMenu.vue', () => {
	const $route = { name: 'biodataTenant', params: { tenantId: 12345 } };

	const generateWrapper = () => {
		const mountProp = {
			localVue,
			store,
			mocks: {
				$route,
				$toasted,
				openSwalLoading: jest.fn(),
				swalError: jest.fn()
			},
			stubs: ['router-link']
		};

		return shallowMount(ProfileMenu, mountProp);
	};

	it('should load the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('#billingManagementProfileMenu').exists()).toBe(true);
	});

	it('should stop rent properly', () => {
		const wrapper = generateWrapper();
		const stopRentButton = wrapper.find(
			'manage-other-custom-action-modal-stub .btn-danger'
		);
		stopRentButton.trigger('click');
	});

	it('should return termination info properly', async () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.vm.$store.state.tenantProfile.widgetInfo = null;
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowTerminationMenu).toBe(true);

		wrapper.vm.$store.state.tenantProfile.statusPage = 'newMember';
		wrapper.vm.$store.state.tenantProfile.widgetInfo = {};
		wrapper.vm.$store.state.tenantProfile.widgetInfo.terminationInfo = null;
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowTerminationMenu).toBe(false);

		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.vm.$store.state.tenantProfile.widgetInfo.terminationInfo = {
			status: 'other'
		};
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowTerminationMenu).toBe(true);
	});
});
