import { createLocalVue, shallowMount } from '@vue/test-utils';
import MixinClipboard from 'Js/_ownerpage/mamipay/mixins/MixinClipboard';

const localVue = createLocalVue();
const mockComponent = {
	template: '<div></div>',
	mixins: [MixinClipboard]
};

const createWrapper = ({ mocks = {} }) => {
	return shallowMount(mockComponent, {
		localVue,
		mocks
	});
};

describe('MixinClipboard', () => {
	it('should show toast when text copied', () => {
		const $toasted = {
			show: jest.fn()
		};

		const wrapper = createWrapper({ mocks: { $toasted } });

		wrapper.vm.clipboardOnCopy();

		expect($toasted.show).toBeCalled();
	});

	it('should show toast when text fails to copied', () => {
		const $toasted = {
			show: jest.fn()
		};

		const wrapper = createWrapper({ mocks: { $toasted } });

		wrapper.vm.clipboardOnError();

		expect($toasted.show).toBeCalled();
	});
});
