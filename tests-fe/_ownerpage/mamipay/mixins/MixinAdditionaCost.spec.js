import { shallowMount, createLocalVue } from '@vue/test-utils';
import MixinAdditionalCost from 'Js/_ownerpage/mamipay/mixins/MixinAdditionalCost.js';
import Vuex from 'vuex';
import additionalPrice from 'tests-fe/_ownerpage/mamipay/components/page-tenant-profile/__mocks__/additional-price.json';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = {
	template: '<div></div>',
	mixins: [MixinAdditionalCost]
};

const $store = {
	state: {
		allDetailRoom: {
			room_available: 1
		}
	}
};

describe('MixinAdditionalCost', () => {
	const wrapper = shallowMount(mockComponent, {
		localVue,
		store: new Vuex.Store($store)
	});

	describe('Method: getFormattedOtherCost', () => {
		it('should return empty array if no parameter given', () => {
			expect(wrapper.vm.getFormattedOtherCost()).toEqual([]);
		});

		it('should return expected array', () => {
			expect(wrapper.vm.getFormattedOtherCost(additionalPrice[0].data)).toEqual(
				[
					{
						field_title: 'Pintu',
						field_value: 200000
					},
					{
						field_title: 'Kursi',
						field_value: 50000
					}
				]
			);
		});
	});

	describe('Method: getSingleCostValue', () => {
		it('should return meta value if reference key contains prefix meta', () => {
			expect(
				wrapper.vm.getSingleCostValue(
					[{ meta: [{ key: 'metaKey', value: 'metaValue' }] }],
					{ data: ['meta.metaKey', 'default'] }
				)
			).toEqual({
				data: 'metaValue'
			});
		});

		it('should return meta default value if reference key contains prefix meta and has no value', () => {
			expect(
				wrapper.vm.getSingleCostValue(
					[{ meta: [{ key: 'metaKey', value: 'metaValue' }] }],
					{ data: ['meta.notFound', 'default'] }
				)
			).toEqual({
				data: 'default'
			});
		});

		it('should return object value from given key', () => {
			expect(
				wrapper.vm.getSingleCostValue([{ price: 10000 }], {
					price: ['price', 0]
				})
			).toEqual({
				price: 10000
			});
		});
	});

	describe('Method: getMetaValue', () => {
		it('should return correct meta value', () => {
			expect(
				wrapper.vm.getMetaValue(
					[{ key: 'metaKey', value: 'metaValue' }],
					'metaKey',
					'default'
				)
			).toBe('metaValue');
		});

		it('should return default value if no matched meta', () => {
			expect(
				wrapper.vm.getMetaValue(
					[{ key: 'metaKey', value: 'metaValue' }],
					'notMatched',
					'default'
				)
			).toBe('default');
		});
	});

	describe('Method: getDiscountedAmount', () => {
		it('should return correct dp value', () => {
			expect(wrapper.vm.getDiscountedAmount(10, 10000)).toBe(1000);
		});

		it('should return 0 if no params given', () => {
			expect(wrapper.vm.getDiscountedAmount()).toBe(0);
		});
	});

	describe('Method: getFormattedAdditionalCost', () => {
		it('should return correct formatted additional price', () => {
			const formattedAdditionalPrice = wrapper.vm.getFormattedAdditionalCost(
				additionalPrice
			);

			expect(formattedAdditionalPrice).toEqual({
				additional: {
					type: 'additional',
					active: true,
					data: [
						{
							field_title: 'Pintu',
							field_value: 200000
						},
						{
							field_title: 'Kursi',
							field_value: 50000
						}
					]
				},
				dp: {
					type: 'dp',
					active: true,
					data: {
						percentage: '50'
					}
				},
				deposit: {
					type: 'deposit',
					active: true,
					data: {
						price: 12000
					}
				},
				fine: {
					active: true,
					type: 'fine',
					data: {
						price: 40000,
						duration: '3',
						durationType: 'day'
					}
				}
			});
		});
	});
});
