import { createLocalVue, shallowMount } from '@vue/test-utils';
import MixinProfileContentNavigation from 'Js/_ownerpage/mamipay/mixins/MixinProfileContentNavigation.js';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
const mockComponent = {
	template: '<div></div>',
	mixins: [MixinProfileContentNavigation]
};

const path = '/profile-tenant/1617/biodata-tenant';
const matched = [{ path: '/profile-tenant/:tenantId' }];

const $route = { path, matched };

const $router = {
	push: jest.fn()
};

const oxWebUrl = 'http://owner.mamikos.com';
global.oxWebUrl = oxWebUrl;

const resetLocation = () => {
	mockWindowProperty(
		'location',
		'http://mamikos.com/profile-tenant/1617/biodata-tenant'
	);
};

const createWrapper = ({ mocks = {}, attrs = {} }) => {
	return shallowMount(mockComponent, {
		localVue,
		mocks,
		attrs
	});
};

describe('MixinProfileContentNavigation.js', () => {
	describe('Go back to dashboard when goBack called', () => {
		test('no previous-route attrs', () => {
			resetLocation();
			const wrapper = createWrapper({ mocks: { $route, $router } });
			wrapper.vm.goBack();
			expect(window.location).toBe(oxWebUrl);
		});

		test('no previous path', () => {
			resetLocation();
			const wrapper = createWrapper({
				mocks: { $route, $router },
				attrs: { 'previous-route': { path: '' } }
			});
			wrapper.vm.goBack();
			expect(window.location).toBe(oxWebUrl);
		});

		test('previous path is /', () => {
			resetLocation();
			const wrapper = createWrapper({
				mocks: { $route, $router },
				attrs: { 'previous-route': { path: '/', matched: [{ path: '/' }] } }
			});
			wrapper.vm.goBack();
			expect(window.location).toBe(oxWebUrl);
		});

		test('previous path is equal to current page', () => {
			resetLocation();
			const wrapper = createWrapper({
				mocks: { $route, $router },
				attrs: { 'previous-route': $route }
			});
			wrapper.vm.goBack();
			expect(window.location).toBe(oxWebUrl);
		});
	});

	describe('Go back to previous page when goBack called', () => {
		test('previous path is not /', () => {
			resetLocation();
			const path = '/not-slash';
			const matched = [{ path: '/not-slash' }];
			const wrapper = createWrapper({
				mocks: { $route, $router },
				attrs: { 'previous-route': { path, matched } }
			});
			const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
			wrapper.vm.goBack();
			expect(spyRouterPush).toBeCalledWith({ path: '/not-slash' });
			spyRouterPush.mockRestore();
		});

		test('previous path is not equal to current route', () => {
			resetLocation();
			const path = '/not-current-route';
			const matched = [{ path: '/not-current-route' }];
			const wrapper = createWrapper({
				mocks: { $route, $router },
				attrs: { 'previous-route': { path, matched } }
			});
			const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
			wrapper.vm.goBack();
			expect(spyRouterPush).toBeCalledWith({ path });
			spyRouterPush.mockRestore();
		});
	});
});
