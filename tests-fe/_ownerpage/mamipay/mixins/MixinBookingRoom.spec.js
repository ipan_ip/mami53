import { shallowMount, createLocalVue } from '@vue/test-utils';
import MixinBookingRoom from 'Js/_ownerpage/mamipay/mixins/MixinBookingRoom.js';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = {
	template: '<div></div>',
	mixins: [MixinBookingRoom]
};

const $store = {
	state: {
		allDetailRoom: {
			room_available: 1
		}
	}
};

describe('MixinBookingRoom', () => {
	const wrapper = shallowMount(mockComponent, {
		localVue,
		store: new Vuex.Store($store)
	});

	describe('Method sanitizeStr', () => {
		it('should sanitize Kamar A9871 to A9871', () => {
			expect(wrapper.vm.sanitizeStr('kamar A9871', 'kamar')).toBe('A9871');
			expect(wrapper.vm.sanitizeStr('Kamar A9871', 'kamar')).toBe('A9871');
			expect(wrapper.vm.sanitizeStr('A9871', 'kamar')).toBe('A9871');
		});

		it('should sanitize Lantai B887 to B887', () => {
			expect(wrapper.vm.sanitizeStr('lantai B887', 'lantai')).toBe('B887');
			expect(wrapper.vm.sanitizeStr('Lantai B887', 'lantai')).toBe('B887');
			expect(wrapper.vm.sanitizeStr('B887', 'lantai')).toBe('B887');
		});
	});

	describe('Room Availability', () => {
		it('should render room available if any', () => {
			expect(wrapper.vm.roomAvailableStr).toBe('Sisa 1 kamar');
		});

		it('should render kost full if not room available', () => {
			wrapper.vm.$store.state.allDetailRoom.room_available = 0;
			expect(wrapper.vm.isKostFull).toBe(true);
			expect(wrapper.vm.roomAvailableStr).toBe('Kamar penuh');
		});
	});
});
