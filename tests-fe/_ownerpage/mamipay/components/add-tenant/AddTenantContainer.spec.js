import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantContainer from 'Js/_ownerpage/mamipay/components/add-tenant/AddTenantContainer.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockWindowProperty('window.scrollTo', jest.fn());

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

describe('AddTenantContainer.vue', () => {
	const wrapper = shallowMount(AddTenantContainer, {
		localVue,
		mocks: {
			$route: {
				meta: { stepTitle: 'Add Tenant' },
				name: 'addTenantSelectKos'
			},
			$store: {
				state: {
					addTenant: {
						kostFullModal: {
							show: false,
							id: 1
						}
					}
				},
				commit: jest.fn()
			}
		},
		stubs: ['router-view']
	});

	it('should add sticky class on title', () => {
		Element.prototype.getBoundingClientRect = jest.fn(() => {
			return { top: 0 };
		});

		wrapper.vm.addClassOnTitle();
		expect(wrapper.find('.--sticky-title').exists()).toBe(true);
	});

	it('should remove sticky class on title', () => {
		Element.prototype.getBoundingClientRect = jest.fn(() => {
			return { top: 15 };
		});

		wrapper.vm.addClassOnTitle();
		expect(wrapper.find('.--sticky-title').exists()).toBe(false);
	});

	it('should remove scroll event listener before component destroyed', () => {
		const spyRemoveEventListener = jest.spyOn(window, 'removeEventListener');
		wrapper.vm.$destroy();
		expect(spyRemoveEventListener).toBeCalled();
		spyRemoveEventListener.mockRestore();
	});

	it('should save modal state on store', () => {
		wrapper.vm.closeAddTenantKostFullModal();
		expect(wrapper.vm.$store.commit).toBeCalled();
	});
});
