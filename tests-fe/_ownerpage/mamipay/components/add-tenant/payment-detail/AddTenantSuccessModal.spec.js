import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantSuccessModal from 'Js/_ownerpage/mamipay/components/add-tenant/payment-detail/AddTenantSuccessModal.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
const AddTenantModal = {
	template: '<div></div>'
};

describe('AddTenantSuccessModal.vue', () => {
	const wrapper = shallowMount(AddTenantSuccessModal, {
		localVue,
		stubs: { AddTenantModal },
		store: new Vuex.Store({
			modules: { addTenant: store },
			mutations: { setRoomAllotment: jest.fn() }
		}),
		propsData: { isShowModal: true, roomId: 123, contractId: 1234 },
		mocks: {
			$router: {
				push: jest.fn()
			}
		}
	});

	it('should render add success modal', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit close if modal closed', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit close if secondary button clicked', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('secondary-button-clicked');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should navigate to first step if primary button clicked', () => {
		const spyPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.findComponent(AddTenantModal).vm.$emit('primary-button-clicked');
		expect(spyPush).toBeCalled();
		spyPush.mockRestore();
	});
});
