import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantSaveModal from 'Js/_ownerpage/mamipay/components/add-tenant/payment-detail/AddTenantSaveModal.vue';

const localVue = createLocalVue();
const AddTenantModal = {
	template: '<div></div>'
};

global.oxWebUrl = 'owner.mamikos.com';

describe('AddTenantSaveModal.vue', () => {
	const wrapper = shallowMount(AddTenantSaveModal, {
		localVue,
		stubs: { AddTenantModal },
		propsData: { isShowModal: true },
		mocks: {
			$router: {
				push: jest.fn()
			}
		}
	});

	it('should render add success modal', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit close if modal closed', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit close if secondary button clicked', () => {
		jest.clearAllMocks();
		wrapper.findComponent(AddTenantModal).vm.$emit('secondary-button-clicked');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit save if primary button clicked', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('primary-button-clicked');
		expect(wrapper.emitted('save')[0]).toBeTruthy();
	});
});
