import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantPaymentDetail from 'Js/_ownerpage/mamipay/components/add-tenant/payment-detail/AddTenantPaymentDetail.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

const mockStore = { ...store };
mockStore.state.selectedKost = {
	id: 1234,
	original_prices: { price_monthly: 1000 }
};
mockStore.state.params.rent_type = 'month';
mockStore.state.params.amount = 500000;
mockStore.state.depositAmount = 9000;
mockStore.state.isDepositShown = true;
mockStore.state.params.start_date = '1998-01-01';
mockStore.state.rentCountInfoType = 'monthly';
mockStore.state.params.additional_costs = [
	{ field_value: 6000, field_title: 'additional cost 1' },
	{ field_value: 4000, field_title: 'additional cost 2' },
	{ field_value: 2000, field_title: 'additional cost 3' }
];

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
window.Vue = localVue;
mockSwalLoading();
localVue.use(Vuex);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;
const tracker = jest.fn();
global.tracker = tracker;

describe('AddTenantPaymentDetail.vue', () => {
	const mainStore = {
		state: { authData: { all: {} } },
		modules: { addTenant: mockStore }
	};

	const wrapper = shallowMount(AddTenantPaymentDetail, {
		localVue,
		store: new Vuex.Store(mainStore),
		mixins: [
			{
				computed: {
					navigator() {
						return {
							isMobile: true
						};
					}
				}
			}
		]
	});

	it('should render add-tenant-payment-detail', () => {
		expect(wrapper.find('.add-tenant-payment-detail').exists()).toBe(true);
	});

	it('should render all main prices', () => {
		expect(wrapper.findAll('.--main-price li').length).toBe(2);
	});

	it('should render all other prices', () => {
		expect(wrapper.findAll('.--other-price li').length).toBe(3);
	});

	it('should count correct total price', () => {
		expect(wrapper.vm.totalPrice).toBe(500000 + 9000 + 6000 + 4000 + 2000);
	});

	it('should set contract id and show modal success if add tenant submit success', async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					data: {
						contract: {
							id: 123123
						}
					},
					status: true
				}
			})
		};
		await wrapper.vm.saveAddTenantData();
		expect(tracker).toBeCalled();
	});

	it('should set contract id to 0 and not showing modal success if add tenant submit status false', async () => {
		jest.clearAllMocks();
		wrapper.setData({ isShowModalSuccess: false });
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					data: {},
					status: false
				}
			})
		};
		await wrapper.vm.saveAddTenantData();
		expect(tracker).not.toBeCalled();
		expect(wrapper.vm.isShowModalSuccess).toBe(false);
		expect(wrapper.vm.newContractId).toBe(0);
	});

	it('should set contract id to 0, not showing modal, and call bugsnag success if add tenant submit failed', async () => {
		jest.clearAllMocks();
		wrapper.setData({ isShowModalSuccess: false });
		global.axios = {
			post: jest.fn().mockRejectedValue(new Error('error'))
		};
		await wrapper.vm.saveAddTenantData();
		expect(tracker).not.toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
		expect(wrapper.vm.isShowModalSuccess).toBe(false);
		expect(wrapper.vm.newContractId).toBe(0);
	});

	it('should show modal confirmation if button Simpan clicked', async () => {
		await wrapper
			.find('.add-tenant-payment-detail__action-btn .btn')
			.trigger('click');
		expect(wrapper.vm.isShowModalSave).toBe(true);
	});
});
