import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantModalGenderMismatch from 'Js/_ownerpage/mamipay/components/add-tenant/biodata/AddTenantModalGenderMismatch.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('AddTenantModalGenderMismatch.vue', () => {
	const wrapper = shallowMount(AddTenantModalGenderMismatch, { localVue });

	it('should emit an event when button is clicked', () => {
		wrapper.vm.handleModalEvent('event');
		expect(wrapper.emitted('event')).toBeTruthy();
	});
});
