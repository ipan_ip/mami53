import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantBiodata from 'Js/_ownerpage/mamipay/components/add-tenant/biodata/AddTenantBiodata.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
const mockStore = { ...store };
mockWindowProperty('tracker', jest.fn());

describe('AddTenantBiodata.vue', () => {
	const mainStore = {
		state: { authData: { all: {} } },
		modules: { addTenant: mockStore }
	};

	const generateWrapper = ({} = {}) =>
		shallowMount(AddTenantBiodata, {
			localVue,
			store: new Vuex.Store(mainStore),
			mocks: {
				$router: { push: jest.fn() },
				$store: { commit: jest.fn() }
			}
		});

	it('should return gender male if gender is not set yet', () => {
		mockStore.state.params.gender = '';
		mockStore.state.selectedKost = { gender: 1 };
		const wrapper = generateWrapper();
		expect(wrapper.vm.genderSelected).toBe('male');
	});

	it('should assign the new gender value', () => {
		const wrapper = generateWrapper();

		wrapper.vm.genderSelected = 'female';
		expect(store.state.params.gender).toBe('female');
	});

	it('should disable button next if name is not filled', () => {
		mockStore.state.params.name = '';
		const wrapper = generateWrapper();
		expect(wrapper.vm.isDisabledNextButton).toBe(true);
	});

	it('should push to the next route', () => {
		const wrapper = generateWrapper();
		wrapper.vm.nextSection();
		expect(wrapper.vm.$router.push).toBeCalled();
	});

	it('should close modal gender error', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.addTenant.params.gender = 'male';
		wrapper.vm.$store.state.addTenant.selectedKost = { gender: 2 };
		wrapper.vm.setDefaultGender();
		expect(wrapper.vm.isGenderMismatch).toBe(false);
		expect(wrapper.vm.$store.state.addTenant.params.gender).toBe('female');
	});

	it('should show correct error message for name', () => {
		const wrapper = generateWrapper();
		mockStore.state.params.name = '';
		expect(wrapper.vm.errorNameMessage.toLowerCase()).toBe('wajib diisi');

		mockStore.state.params.name = 'ma';
		expect(wrapper.vm.errorNameMessage.toLowerCase()).toBe(
			'minimal mengandung 3 karakter'
		);

		mockStore.state.params.name = 'mam!kos';
		expect(wrapper.vm.errorNameMessage.toLowerCase()).toBe(
			'hanya dapat diisi dengan huruf'
		);

		mockStore.state.params.name = 'mamikos';
		expect(wrapper.vm.errorNameMessage.toLowerCase()).toBe('');
	});
});
