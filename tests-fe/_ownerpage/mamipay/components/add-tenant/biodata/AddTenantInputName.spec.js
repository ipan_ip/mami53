import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantInputName from 'Js/_ownerpage/mamipay/components/add-tenant/biodata/AddTenantInputName.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
let mockStore = { ...store };

describe('AddTenantInputName.vue', () => {
	it('should save the name on vuex', async () => {
		const wrapper = shallowMount(AddTenantInputName, {
			localVue,
			store: new Vuex.Store({ modules: { addTenant: mockStore } })
		});

		wrapper.vm.tenantName = 'mamikos aplikasi pencarian kos';
		await wrapper.vm.$nextTick();
		expect(store.state.params.name).toBe('mamikos aplikasi pencarian kos');
	});
});
