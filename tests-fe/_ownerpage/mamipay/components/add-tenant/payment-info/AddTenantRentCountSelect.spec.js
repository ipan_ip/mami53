import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantRentCountSelect from 'Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantRentCountSelect.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';

const localVue = createLocalVue();
window.Vue = localVue;
mockSwalLoading();
localVue.use(Vuex);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const mockStore = { ...store };
mockStore.state.selectedKost = {
	id: 111,
	original_price: {
		price_monthly: 25000,
		price_daily: 0,
		price_weekly: 0,
		price_quarterly: 0,
		price_semiannualy: 0
	}
};

const VuexStore = new Vuex.Store({ modules: { addTenant: store } });
global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			data: ['1 Bulan', '2 Bulan'],
			status: true
		}
	})
};

describe('AddTenantRentCountSelect.vue', () => {
	const wrapper = shallowMount(AddTenantRentCountSelect, {
		localVue,
		store: VuexStore
	});

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set correct rent count options', () => {
		expect(wrapper.vm.rentCountOptions.length).toBe(1);
	});

	it('should apply selected rent count', async () => {
		await wrapper.setData({ rentCount: 'monthly' });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentCount).toBe('monthly');
		expect(wrapper.vm.$store.state.addTenant.rentDurationList.length).toBe(2);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should reset payment info if get status false on rent count request', async () => {
		global.axios.get = jest.fn().mockResolvedValue({
			data: {
				status: false
			}
		});
		await wrapper.setData({ rentCount: 'monthly' });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentCount).toBe('');
		expect(wrapper.vm.$store.state.addTenant.rentDurationList.length).toBe(0);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should reset payment info if rent count request failed', async () => {
		wrapper.vm.$store.state.addTenant.params.rent_type = 'month';
		wrapper.vm.$store.state.addTenant.params.setRentCountInfoType = 'monthly';
		wrapper.vm.$store.state.addTenant.rentDurationList = ['1 Bulan', '2 Bulan'];
		global.axios.get = jest.fn().mockRejectedValue(new Error('error'));
		await wrapper.setData({ rentCount: 'monthly' });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentCount).toBe('');
		expect(wrapper.vm.$store.state.addTenant.rentDurationList.length).toBe(0);
		expect(wrapper.element).toMatchSnapshot();
	});
});
