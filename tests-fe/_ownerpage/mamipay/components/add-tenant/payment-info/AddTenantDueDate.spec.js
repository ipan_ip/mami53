import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantDueDate from 'Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantDueDate.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);
const VuexStore = new Vuex.Store({ modules: { addTenant: store } });
const Datepicker = {
	template: '<div class="mock-datepicker" />',
	methods: {
		close: jest.fn()
	}
};

describe('AddTenantDueDate.vue', () => {
	const wrapper = shallowMount(AddTenantDueDate, {
		localVue,
		store: VuexStore,
		stubs: {
			Datepicker
		}
	});

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set isValid to false if input is null', async () => {
		await wrapper.setData({ selectedDate: null });
		expect(wrapper.vm.isValid).toBe(false);
	});

	it('should set isValid to true', async () => {
		await wrapper.setData({ selectedDate: '2020-02-02' });
		expect(wrapper.vm.isValid).toBe(true);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set isValid to false if input is invalid', async () => {
		await wrapper.setData({ selectedDate: 'invalid date' });
		expect(wrapper.vm.isValid).toBe(false);
	});

	it('should close date picker if datepicker ref is exist', () => {
		wrapper.vm.closeDatePicker();
		expect(Datepicker.methods.close).toBeCalled();
	});

	it('should not close date picker if datepicker ref is not exist', () => {
		jest.clearAllMocks();
		delete wrapper.vm.$refs.dueDatePicker;
		wrapper.vm.closeDatePicker();
		expect(Datepicker.methods.close).not.toBeCalled();
	});
});
