import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantPriceInput from 'Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantPriceInput.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
const VuexStore = new Vuex.Store({ modules: { addTenant: store } });

describe('AddTenantPriceInput.vue', () => {
	const wrapper = shallowMount(AddTenantPriceInput, {
		localVue,
		store: VuexStore,
		propsData: {
			priceVal: 0
		}
	});

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set isValid to false if input is empty', async () => {
		await wrapper.setData({ price: 2000 });
		expect(wrapper.vm.isValid).toBe(true);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set isValid to true', async () => {
		await wrapper.setData({ price: 0 });
		expect(wrapper.vm.isValid).toBe(false);
	});

	it('should return error message min char', async () => {
		await wrapper.setProps({ priceVal: 200 });
		expect(wrapper.vm.errorMessage).toBe('Minimal mengandung 4 karakter');
	});
});
