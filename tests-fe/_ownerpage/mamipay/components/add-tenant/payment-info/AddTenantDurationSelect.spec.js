import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantDurationSelect from 'Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantDurationSelect.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
const VuexStore = new Vuex.Store({ modules: { addTenant: store } });

describe('AddTenantDurationSelect.vue', () => {
	const wrapper = shallowMount(AddTenantDurationSelect, {
		localVue,
		store: VuexStore
	});

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should disable input if no duration list given', () => {
		wrapper.vm.$store.state.addTenant.rentDurationList = [];
		expect(wrapper.vm.disabledSelect).toBe(true);
	});

	it('should not disable input if duration list given', () => {
		wrapper.vm.$store.state.addTenant.rentDurationList = ['1 Bulan', '2 Bulan'];
		expect(wrapper.vm.disabledSelect).toBe(false);
	});

	it('should set isValid to false if input is empty', async () => {
		await wrapper.setData({ rentDuration: 0 });
		expect(wrapper.vm.isValid).toBe(false);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set isValid to true', async () => {
		await wrapper.setData({ rentDuration: 1 });
		expect(wrapper.vm.isValid).toBe(true);
	});
});
