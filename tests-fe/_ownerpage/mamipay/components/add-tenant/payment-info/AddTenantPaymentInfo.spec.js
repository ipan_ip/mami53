import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddTenantPaymentInfo from 'Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantPaymentInfo.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import dayjs from 'dayjs';
import 'dayjs/locale/id';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);
mockWindowProperty('tracker', jest.fn());

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const mockStore = { ...store };

const assignParams = (prop, value) => {
	mockStore.state.params[prop] = value;
};

const mainStore = {
	state: { authData: { all: {} } },
	modules: { addTenant: store }
};

const VuexStore = new Vuex.Store(mainStore);

const generateWrapper = stubs =>
	shallowMount(AddTenantPaymentInfo, {
		localVue,
		store: VuexStore,
		stubs,
		mocks: {
			$router: {
				push: jest.fn()
			}
		}
	});

describe('AddTenantPaymentInfo.vue', () => {
	const wrapper = generateWrapper();

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should not go to next page if some input is not valid', () => {
		const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.find('form').trigger('submit.prevent');
		expect(spyRouterPush).not.toBeCalled();
		spyRouterPush.mockRestore();
	});

	it('should go to next page if all inputs are valid', () => {
		assignParams('rent_type', 'month');
		assignParams('duration', 1);
		assignParams('amount', 20000);
		assignParams('start_date', '2020-02-02');

		const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.find('form').trigger('submit.prevent');
		expect(spyRouterPush).toBeCalled();
		spyRouterPush.mockRestore();
	});

	it('should update rent price if setRentType called', () => {
		wrapper.vm.setRentType(5000);
		expect(wrapper.vm.$store.state.addTenant.params.amount).toBe(5000);
	});
});
