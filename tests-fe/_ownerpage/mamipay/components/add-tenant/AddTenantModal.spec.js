import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantModal from 'Js/_ownerpage/mamipay/components/add-tenant/AddTenantModal.vue';

const localVue = createLocalVue();
const MamiModalWrap = {
	template: '<div><slot></slot></div>'
};

global.oxWebUrl = 'owner.mamikos.com';

describe('AddTenantModal.vue', () => {
	const wrapper = shallowMount(AddTenantModal, {
		localVue,
		stubs: { MamiModalWrap },
		propsData: {
			secondaryButtonText: 'Secondary Button',
			primaryButtonText: 'Primary Button',
			isShowModal: true
		}
	});

	it('should render add tenat modal', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit close if modal closed', () => {
		wrapper.findComponent(MamiModalWrap).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit secondary-button-clicked if secondary button clicked', () => {
		wrapper.find('.add-tenant-modal__secondary-btn').trigger('click');
		expect(wrapper.emitted('secondary-button-clicked')[0]).toBeTruthy();
	});

	it('should emit primary-button-clicked if primary button clicked', () => {
		wrapper.find('.add-tenant-modal__primary-btn').trigger('click');
		expect(wrapper.emitted('primary-button-clicked')[0]).toBeTruthy();
	});

	it('should remove modal open class before component destroyed', () => {
		const spyRemoveClass = jest.spyOn(document.body.classList, 'remove');
		wrapper.vm.$destroy();
		expect(spyRemoveClass).toBeCalledWith('modal-open');
	});
});
