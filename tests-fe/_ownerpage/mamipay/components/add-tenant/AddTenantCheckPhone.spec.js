import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantCheckPhone from 'Js/_ownerpage/mamipay/components/add-tenant/AddTenantCheckPhone.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
mockWindowProperty('tracker', jest.fn());

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

let axios = {
	get: jest.fn().mockResolvedValue()
};
global.axios = axios;

let mockStore = { ...store };

let mockTenantStore = {
	state: {
		tenantProfile: {
			roomAllotment: {
				available: true,
				disable: false,
				floor: 'Lantai 1',
				id: 68224,
				name: 'Kamar 5'
			}
		}
	}
};

mockStore.state.selectedKost = {
	room_title: 'Kost Bella Luna',
	area_formatted: 'Sleman'
};

mockStore.state.ownerKostList = [
	{ room_title: 'Kost Bella Luna' },
	{ room_title: 'Kost Anjani' }
];

let mainStore = {
	state: {
		userRoom: [],
		authData: { all: {} }
	},
	modules: { addTenant: mockStore, tenantProfile: mockTenantStore },
	mutations: {
		setUserRoom: jest.fn(),
		setRoomAllotment: jest.fn()
	}
};

describe('AddTenantCheckPhone.vue', () => {
	const generateWrapper = ({ methodsData, mocksProp = {} } = {}) => {
		const mountProp = {
			localVue,
			store: new Vuex.Store(mainStore),
			methods: { ...methodsData },
			mocks: { ...mocksProp }
		};

		return shallowMount(AddTenantCheckPhone, mountProp);
	};

	it('should render the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('#addTenantCheckPhone').exists()).toBe(true);
	});

	it('should save tenant phone number', () => {
		const wrapper = generateWrapper();

		wrapper.vm.onInputPhoneNumber('08122333455');
		expect(wrapper.vm.tenantPhone).toBe('08122333455');
	});

	it('should push to the add tenant biodata page', () => {
		const wrapper = generateWrapper({
			mocksProp: { $router: { push: jest.fn() } }
		});

		wrapper.vm.nextSection();
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'addTenantBiodata'
		});
	});

	it('should open modal contract active', () => {
		const wrapper = generateWrapper();
		wrapper.vm.toggleModalContract(true);
		expect(wrapper.vm.isModalContractActive).toBe(true);
	});

	it('should send api to check tenant phone number', async () => {
		const toggleModalContract = jest.fn();
		const wrapper = generateWrapper({ methodsData: { toggleModalContract } });

		axios.get = jest.fn().mockResolvedValue({
			data: { status: true, tenant: { is_active_contract: true } }
		});

		await wrapper.vm.checkPhoneNumber();
		expect(axios.get).toBeCalled();
		expect(toggleModalContract).toBeCalledWith(true);
	});

	it('should commit existing profile tenant and throw to the next page', async () => {
		const nextSection = jest.fn();
		const wrapper = generateWrapper({
			methodsData: { nextSection }
		});

		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				tenant: { active_contract_id: 0, tenant_name: 'mamikos user' }
			}
		});

		await wrapper.vm.checkPhoneNumber();
		expect(mockStore.state.params.name).toBe('mamikos user');
		expect(nextSection).toBeCalled();
	});

	it('should set tenant phone and throw to the next page', async () => {
		const nextSection = jest.fn();
		const wrapper = generateWrapper({
			methodsData: { nextSection }
		});

		axios.get = jest.fn().mockResolvedValue({ data: { status: false } });

		wrapper.vm.tenantPhone = '08122333411';
		await wrapper.vm.checkPhoneNumber();
		expect(mockStore.state.params.phone_number).toBe('08122333411');
		expect(nextSection).toBeCalled();
	});

	it('should assign empty string to display room name', () => {
		const wrapper = generateWrapper();
		const roomTemp = { name: '' };

		wrapper.vm.assignDisplayRoomValue(roomTemp);
		expect(wrapper.vm.displayRoomValue).toBe('');
		expect(store.state.params.designer_room_id).toBe('');
	});
});
