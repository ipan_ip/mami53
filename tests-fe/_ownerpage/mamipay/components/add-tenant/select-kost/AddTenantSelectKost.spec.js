import '../../__mocks__/mockToken';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantSelectKost from 'Js/_ownerpage/mamipay/components/add-tenant/select-kost/AddTenantSelectKost.vue';
import Vuex from 'vuex';
import store from 'Js/_ownerpage/mamipay/store';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
window.Vue = localVue;
mockSwalLoading();
localVue.use(Vuex);

mockWindowProperty('tracker', jest.fn());

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const mockComponent = {
	template: '<div></div>'
};

const AddTenantKostFullModal = { ...mockComponent };
const AddTenantKostList = { ...mockComponent };

const kostList = [
	{
		room_title: 'Kost Mamikos Indonesia',
		area_formatted: 'Mlati, Sleman',
		room_available: 0,
		id: 1
	},
	{
		room_title: 'Kost Mamikos Indonesia 2',
		area_formatted: 'Mlati, Sleman',
		room_available: 1,
		id: 2
	}
];

global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			data: kostList,
			status: true
		}
	})
};

global.bugsnagClient = { notify: jest.fn() };
const $router = {
	push: jest.fn(),
	replace: jest.fn()
};

const mockStore = new Vuex.Store(store);
mockStore.commit('setAuthData', { all: {} });

const generateWrapper = ({ methodsData } = {}) =>
	shallowMount(AddTenantSelectKost, {
		localVue,
		store: mockStore,
		stubs: {
			AddTenantKostFullModal,
			AddTenantKostList
		},
		mocks: {
			$router,
			$route: { params: {} }
		},
		methods: { ...methodsData }
	});

describe('AddTenantSelectKost.vue', () => {
	const wrapper = generateWrapper();

	it('should render add-tenant-select-kost', () => {
		expect(wrapper.find('.add-tenant-select-kost').exists()).toBe(true);
	});

	it('should set kost list response if response status is true', async () => {
		expect(wrapper.vm.kostList).toEqual(kostList);
	});

	it('should set show modal kost full true if selected kost is full', () => {
		wrapper
			.findComponent(AddTenantKostList)
			.vm.$emit('select-not-available-room', 1);
		expect(wrapper.vm.kostFullModal.id).toBe(1);
		expect(wrapper.vm.kostFullModal.show).toBe(true);
	});

	it('should set show modal close if kost full modal closed', () => {
		wrapper.vm.closeKostFullModal();
		expect(wrapper.vm.kostFullModal.id).toBe(0);
		expect(wrapper.vm.kostFullModal.show).toBe(false);
	});

	it('should store selected room if room is available', () => {
		wrapper
			.findComponent(AddTenantKostList)
			.vm.$emit('select-room', kostList[1]);
		expect(wrapper.vm.$store.state.addTenant.selectedKost).toBe(kostList[1]);
	});

	it('should set kost list to empty array if request status is false', async () => {
		const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'error'
					}
				}
			})
		};
		await wrapper.vm.getOwnerKostList();
		expect(spySwalError).toBeCalled();
		expect(wrapper.vm.kostList).toEqual([]);
		spySwalError.mockRestore();
	});

	it('should set kost list to empty array if request failed', async () => {
		const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
		const spyBugsnagClientNotify = jest.spyOn(global.bugsnagClient, 'notify');
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};
		await wrapper.vm.getOwnerKostList();
		expect(global.EventBus.$emit).toBeCalled();
		expect(spyBugsnagClientNotify).toBeCalled();
		expect(wrapper.vm.kostList).toEqual([]);
		spySwalError.mockRestore();
		spyBugsnagClientNotify.mockRestore();
	});

	it('should go to next step if id is specified from query string', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: kostList,
					status: true
				}
			})
		};

		mockWindowProperty('window.location', {
			href: 'https://test.test/add-tenant/select-kost?k=2' // exist and available
		});

		jest.clearAllMocks();
		const wrapper = await generateWrapper();
		wrapper.vm.checkSelectedKostFromQueryString();
		await wrapper.vm.$nextTick();
		expect($router.push).toBeCalled();
	});

	it('should show if id specified from querystring if from not available kost', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: kostList,
					status: true
				}
			})
		};

		mockWindowProperty('window.location', {
			href: 'https://test.test/add-tenant/select-kost?k=1' // exist but not available
		});

		jest.clearAllMocks();
		const wrapper = await generateWrapper();
		wrapper.vm.checkSelectedKostFromQueryString();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.kostFullModal.show).toBe(true);
	});

	it('should do nothing if id from querystring is not found', async () => {
		mockWindowProperty('window.location', {
			href: 'https://test.test/add-tenant/select-kost?k=999'
		});
		jest.clearAllMocks();
		const wrapper = await generateWrapper();
		wrapper.vm.setKostFullModal({ show: false, id: 0 });
		wrapper.vm.checkSelectedKostFromQueryString();

		expect(wrapper.vm.kostFullModal.show).toBe(false);
		expect($router.push).not.toBeCalled();
	});

	it('should do nothing if no id specified from querystring', () => {
		mockWindowProperty('window.location', {
			href: 'https://test.test/add-tenant/select-kost'
		});

		jest.clearAllMocks();
		const wrapper = generateWrapper();
		wrapper.vm.setKostFullModal({ show: false, id: 0 });
		wrapper.vm.checkSelectedKostFromQueryString();

		expect(wrapper.vm.kostFullModal.show).toBe(false);
		expect($router.push).not.toBeCalled();
	});

	it('should send tracker to moengage', () => {
		const wrapper = generateWrapper();
		wrapper.vm.trackSelectKosVisited();
		expect(tracker).toBeCalled();
	});

	it('should handle scroll to bottom properly', async () => {
		mockWindowProperty('window.innerHeight', 200);
		mockWindowProperty('document.documentElement.scrollTop', 200);
		mockWindowProperty('document.getElementById', () => ({
			offsetHeight: 400
		}));

		const wrapper = generateWrapper();
		await new Promise(resolve => setImmediate(resolve));

		jest.clearAllMocks();
		const spyGetOwnerKostList = jest.spyOn(wrapper.vm, 'getOwnerKostList');
		wrapper.vm.setScrollBottomToLoadMoreKost();

		expect(spyGetOwnerKostList).toBeCalled();

		jest.clearAllMocks();
		mockWindowProperty('document.documentElement.scrollTop', 100);
		wrapper.vm.setScrollBottomToLoadMoreKost();

		expect(spyGetOwnerKostList).not.toBeCalled();

		spyGetOwnerKostList.mockRestore();
	});

	it('should remove scroll listener before component destroyed', () => {
		const removeEventListener = jest.fn();
		mockWindowProperty('window.removeEventListener', removeEventListener);

		const wrapper = generateWrapper();
		wrapper.vm.$destroy();

		expect(removeEventListener).toBeCalled();
	});
});
