import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantKostList from 'Js/_ownerpage/mamipay/components/add-tenant/select-kost/AddTenantKostList.vue';

const localVue = createLocalVue();

describe('AddTenantKostList.vue', () => {
	const wrapper = shallowMount(AddTenantKostList, {
		localVue,
		propsData: {
			kostList: [
				{
					room_title: 'Kost Mamikos Indonesia',
					area_formatted: 'Mlati, Sleman',
					room_available: 0,
					id: 1
				},
				{
					room_title: 'Kost Mamikos Indonesia 2',
					area_formatted: 'Mlati, Sleman',
					room_available: 1,
					id: 2
				}
			]
		}
	});

	it('should render kost item', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render all kost', () => {
		expect(wrapper.findAll('li').length).toBe(2);
	});

	it('should emit selected room if selected room is available', () => {
		const listItems = wrapper.findAll('li');
		listItems.at(1).trigger('click');
		expect(wrapper.emitted('select-room')[0]).toBeTruthy();
	});

	it('should emit select-not-available-room if selected room is not available', () => {
		const listItems = wrapper.findAll('li');
		listItems.at(0).trigger('click');
		expect(wrapper.emitted('select-not-available-room')[0]).toBeTruthy();
	});

	it('should not render kost if 0 kost given', async () => {
		await wrapper.setProps({ kostList: [] });
		expect(wrapper.findAll('li').length).toBe(0);
	});
});
