import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantKostFullModal from 'Js/_ownerpage/mamipay/components/add-tenant/select-kost/AddTenantKostFullModal.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
const AddTenantModal = {
	template: '<div></div>'
};

global.oxWebUrl = 'owner.mamikos.com';

describe('AddTenantKostFullModal.vue', () => {
	const wrapper = shallowMount(AddTenantKostFullModal, {
		localVue,
		stubs: { AddTenantModal },
		propsData: { isShowModal: true, roomId: 123 }
	});

	it('should render add tenat kost full modal', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit close if modal closed', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit close if secondary button clicked', () => {
		wrapper.findComponent(AddTenantModal).vm.$emit('secondary-button-clicked');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should navigate to owner edit room page if primary button clicked', () => {
		mockWindowProperty('window.location', { href: '' });
		wrapper.findComponent(AddTenantModal).vm.$emit('primary-button-clicked');
		expect(window.location.href).toBe('owner.mamikos.com/kos/rooms/123/edit');
	});
});
