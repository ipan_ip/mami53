import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantKostItem from 'Js/_ownerpage/mamipay/components/add-tenant/select-kost/AddTenantKostItem.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

global.oxWebUrl = 'owner.mamikos.com';

describe('AddTenantKostItem.vue', () => {
	const wrapper = shallowMount(AddTenantKostItem, {
		localVue,
		propsData: {
			kostName: 'Kost Mamikos Indonesia',
			kostLocation: 'Mlati, Sleman'
		}
	});

	it('should render kost item', () => {
		expect(wrapper.element).toMatchSnapshot();
	});
});
