import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantAdditionalCost from 'Js/_ownerpage/mamipay/components/add-tenant/payment-others/AddTenantAdditionalCost.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
const mockStore = { ...store };

describe('AddTenantAdditionalCost.vue', () => {
	const wrapper = shallowMount(AddTenantAdditionalCost, {
		localVue,
		store: new Vuex.Store({
			modules: {
				addTenant: mockStore
			}
		}),
		mocks: {
			$toasted: {
				toasts: [],
				show: jest.fn()
			},
			$router: {
				push: jest.fn(),
				go: jest.fn()
			}
		}
	});

	it('should render add tenant additional cost', () => {
		expect(wrapper.find('.form-additional-cost-settings').exists()).toBe(true);
	});

	it('should render all additional costs', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			},
			{
				field_value: 50000,
				field_title: 'listrik'
			}
		];

		await wrapper.vm.$nextTick();
		expect(wrapper.findAll('.form-additional-cost.--field').length).toBe(2);
		expect(wrapper.vm.isFilledCosts).toBe(true);
		expect(wrapper.vm.isValidAdditionalCosts).toBe(true);
	});

	it('should get isValidAdditionalCosts with true if not additional cost added', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [];
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isValidAdditionalCosts).toBe(true);
	});

	it('should push new addtional cost if button add clicked and all additional costs are valid', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			},
			{
				field_value: 50000,
				field_title: 'listrik'
			}
		];
		await wrapper.vm.$nextTick();
		await wrapper.vm.addCost();
		expect(wrapper.findAll('.form-additional-cost.--field').length).toBe(3);
	});

	it('should show toast if button add clicked and all additional costs are not valid', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			},
			{
				field_value: 0,
				field_title: 'listrik'
			}
		];
		await wrapper.vm.$nextTick();
		const spyToastShow = jest.spyOn(wrapper.vm.$toasted, 'show');
		await wrapper.vm.addCost();
		expect(spyToastShow).toBeCalled();
		expect(wrapper.vm.costs.length).toBe(2);
		spyToastShow.mockRestore();
	});

	it('should delete selected additional cost if button delete clicked', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			},
			{
				field_value: 9000,
				field_title: 'listrik'
			}
		];
		await wrapper.vm.$nextTick();
		const deleteButtonForListrik = wrapper
			.findAll('.form-additional-cost.--field')
			.at(1);
		await deleteButtonForListrik.find('button').trigger('click');
		expect(wrapper.vm.costs.length).toBe(1);
		expect(wrapper.vm.costs).not.toContain({
			field_value: 9000,
			field_title: 'listrik'
		});
	});

	it('should delete selected additional cost and set new empty field if button delete clicked when costs length is 1', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			}
		];
		await wrapper.vm.$nextTick();
		const deleteButton = wrapper.findAll('.form-additional-cost.--field').at(0);
		await deleteButton.find('button').trigger('click');
		expect(wrapper.vm.costs.length).toBe(1);
		expect(wrapper.vm.costs[0].field_title).toBe('');
		expect(wrapper.vm.costs[0].field_value).toBe(0);
	});

	it('should save additional costs and call router push if button save clicked and all inputs are valid', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'sampah'
			}
		];
		const spyRouterGo = jest.spyOn(wrapper.vm.$router, 'go');
		await wrapper.vm.$nextTick();
		const saveButton = wrapper.find(
			'.form-additional-cost-settings__action button'
		);
		await saveButton.trigger('click');
		expect(spyRouterGo).toBeCalled();
		spyRouterGo.mockRestore();
	});

	it('should show toast if button save clicked and inputs are not valid', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: ''
			}
		];
		const spyToastedShow = jest.spyOn(wrapper.vm.$toasted, 'show');
		await wrapper.vm.$nextTick();
		const saveButton = wrapper.find(
			'.form-additional-cost-settings__action button'
		);
		await saveButton.trigger('click');
		expect(spyToastedShow).toBeCalled();
		spyToastedShow.mockRestore();
	});

	it('should set related key and index of additional costs', async () => {
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{
				field_value: 8000,
				field_title: 'listrik'
			}
		];
		await wrapper.vm.$nextTick();
		wrapper.vm.setOtherCostField('title', 'air', 0);
		expect(wrapper.vm.costs[0].field_title).toBe('air');
	});

	it('should set local saveCostGroup when store save_cost_group changed', async () => {
		wrapper.vm.$store.state.addTenant.params.save_cost_group = true;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isSaveCostGroup).toBe(true);
	});
});
