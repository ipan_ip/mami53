import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantPaymentOthers from 'Js/_ownerpage/mamipay/components/add-tenant/payment-others/AddTenantPaymentOthers.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';

import additionalPrice from 'tests-fe/_ownerpage/mamipay/components/page-tenant-profile/__mocks__/additional-price.json';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
window.Vue = localVue;
mockSwalLoading();
localVue.use(Vuex);
const mockStore = { ...store };
mockWindowProperty('tracker', jest.fn());

const $toasted = {
	show: jest.fn()
};

const mockAdditionalCosts = [{ field_title: 'listrik', field_value: 9000 }];
global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			additional_costs: mockAdditionalCosts,
			room_additional_price: additionalPrice
		}
	})
};

global.bugsnagClient = {
	notify: jest.fn()
};

describe('AddTenantPaymentOthers.vue', () => {
	navigator.isMobile = true;

	const wrapper = mount(AddTenantPaymentOthers, {
		localVue,
		store: new Vuex.Store({
			state: { authData: { all: {} } },
			modules: {
				addTenant: mockStore
			}
		}),
		mocks: {
			$router: { push: jest.fn() },
			$toasted
		},
		mixins: [
			{
				computed: {
					navigator() {
						return { isMobile: true };
					}
				}
			}
		]
	});

	it('should render the component', () => {
		expect(wrapper.find('#addTenantPaymentOthers').exists()).toBe(true);
	});

	it('should store deposit price value', () => {
		wrapper.vm.setDepositPrice(2000);
		expect(mockStore.state.depositAmount).toBe(2000);
	});

	it('should store fine price value', () => {
		wrapper.vm.setFinePrice(3000);
		expect(mockStore.state.params.fine_amount).toBe(3000);
	});

	it('should set additional costs from api', async () => {
		await wrapper.vm.getAdditionalCostGroup();
		expect(wrapper.vm.$store.state.addTenant.params.additional_costs).toEqual([
			{ field_value: 200000, field_title: 'Pintu' },
			{ field_value: 50000, field_title: 'Kursi' }
		]);
	});

	it('should set loaded additional cost group to false false if response status if false', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};
		await wrapper.vm.getAdditionalCostGroup();
		expect(wrapper.vm.$store.state.addTenant.isLoadedAdditionalCostGroup).toBe(
			false
		);
	});

	it('should call bugsnag notify if failed fetching additional cost', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};
		const spyBugnsagNotify = jest.spyOn(global.bugsnagClient, 'notify');
		await wrapper.vm.getAdditionalCostGroup();
		expect(spyBugnsagNotify).toBeCalled();
	});

	it('should throw to the next page if additional costs are valid', () => {
		wrapper.vm.setFinePrice(0);
		wrapper.setData({ isValidAdditionalCosts: true });
		wrapper.vm.nextSection();
		expect(wrapper.vm.$router.push).toBeCalled();
	});

	it('should not throw to the next page if additional costs are not valid', async () => {
		jest.clearAllMocks();
		wrapper.vm.setFinePrice(0);
		wrapper.vm.$store.state.addTenant.params.additional_costs = [
			{ field_value: 0, field_title: 'listrik' }
		];
		wrapper.vm.$refs = {
			addTenantAdditionalCost: { showUncompleteInputToast: jest.fn() }
		};
		await wrapper.vm.$nextTick();
		const spyToast = jest.spyOn(
			wrapper.vm.$refs.addTenantAdditionalCost,
			'showUncompleteInputToast'
		);
		wrapper.vm.nextSection();
		expect(spyToast).toBeCalled();
		spyToast.mockRestore();
	});

	describe('Additional Price', () => {
		const additionalPriceMutations = [
			'setAdditionalCost',
			'setSaveCostGroup',
			'setDepositAmount',
			'setFinePayment',
			'setFineMaxLength',
			'setFineDurationType'
		].map(item => `addTenant/${item}`);

		const setLoadedAdditionalCostGroup =
			'addTenant/setLoadedAdditionalCostGroup';

		it('should set additional price properly', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true,
					room_additional_price: additionalPrice
				}
			});

			await wrapper.vm.getAdditionalCostGroup();
			[...additionalPriceMutations, setLoadedAdditionalCostGroup].forEach(
				mutation => {
					expect(spyCommit).toHaveBeenCalledWith(mutation, expect.anything());
				}
			);
			expect(spyCommit).toBeCalledWith(setLoadedAdditionalCostGroup, true);
			spyCommit.mockRestore();
		});

		it('should not set additional price if active is false', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true,
					room_additional_price: additionalPrice.map(price => {
						return {
							...price,
							active: false
						};
					})
				}
			});

			await wrapper.vm.getAdditionalCostGroup();

			additionalPriceMutations.forEach(mutation => {
				expect(spyCommit).not.toBeCalledWith(mutation);
			});
			expect(spyCommit).not.toBeCalledWith('addTenant/setSaveCostGroup', false);
			spyCommit.mockRestore();
		});

		it('should not set additional price if fetched response status is false', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			const spySwalClose = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			axios.get = jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			});

			await wrapper.vm.getAdditionalCostGroup();

			additionalPriceMutations.forEach(mutation => {
				expect(spyCommit).not.toBeCalledWith(mutation);
			});
			expect(spyCommit).toBeCalledWith(setLoadedAdditionalCostGroup, false);
			expect(spySwalClose).toBeCalled();
			spyCommit.mockRestore();
			spySwalClose.mockRestore();
		});

		it('should call bugsnag notify if failed to fetch', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			axios.get = jest.fn().mockRejectedValue(new Error('error'));

			await wrapper.vm.getAdditionalCostGroup();

			additionalPriceMutations.forEach(mutation => {
				expect(spyCommit).not.toBeCalledWith(mutation);
			});
			expect(spyCommit).toBeCalledWith(setLoadedAdditionalCostGroup, false);
			expect(bugsnagClient.notify).toBeCalled();
			spyCommit.mockRestore();
		});
	});
});
