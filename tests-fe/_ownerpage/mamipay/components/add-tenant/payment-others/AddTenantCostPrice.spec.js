import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantCostPrice from 'Js/_ownerpage/mamipay/components/add-tenant/payment-others/AddTenantCostPrice.vue';

const localVue = createLocalVue();
const ManageCurrencyInput = {
	template: '<div></div>',
	props: {
		value: Number
	},
	methods: {
		emitInput(val) {
			this.$emit('input', val);
		}
	}
};

describe('AddTenantCostPrice.vue', () => {
	const wrapper = shallowMount(AddTenantCostPrice, {
		localVue,
		propsData: {
			value: 0
		},
		stubs: { ManageCurrencyInput }
	});

	it('should render the component', () => {
		expect(wrapper.find('.cost-price-container').exists()).toBe(true);
	});

	it('should save current cost label on selected index', () => {
		const input = wrapper.findComponent(ManageCurrencyInput);
		input.vm.emitInput(8000);
		expect(wrapper.emitted('input')[0][0]).toBe(8000);
	});
});
