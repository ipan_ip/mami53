import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddTenantCostTitle from 'Js/_ownerpage/mamipay/components/add-tenant/payment-others/AddTenantCostTitle.vue';

const localVue = createLocalVue();
describe('AddTenantCostTitle.vue', () => {
	const wrapper = shallowMount(AddTenantCostTitle, {
		localVue,
		propsData: {
			value: ''
		}
	});

	it('should render the component', () => {
		expect(wrapper.find('.cost-title-container').exists()).toBe(true);
	});

	it('should emit inputted value', () => {
		const input = wrapper.find('input');
		input.element.value = 'laundry';
		input.trigger('input');
		expect(wrapper.emitted('input')[0][0]).toBe('laundry');
	});
});
