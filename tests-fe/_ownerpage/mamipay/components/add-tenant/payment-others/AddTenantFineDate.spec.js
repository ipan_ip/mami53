import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantFineDate from 'Js/_ownerpage/mamipay/components/add-tenant/payment-others/AddTenantFineDate.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
let mockStore = { ...store };

describe('AddTenantFineDate.vue', () => {
	const wrapper = shallowMount(AddTenantFineDate, {
		localVue,
		store: new Vuex.Store({
			modules: {
				addTenant: mockStore
			}
		})
	});

	it('should render the component', () => {
		expect(wrapper.find('#addTenantFineDate').exists()).toBe(true);
	});

	it('should save duration type selected', () => {
		wrapper.vm.durationType = 'week';
		expect(mockStore.state.params.fine_duration_type).toBe('week');
	});

	it('should save duration days count', () => {
		wrapper.vm.fineDurationMax = '6';
		expect(mockStore.state.params.fine_maximum_length.toString()).toBe('6');
	});
});
