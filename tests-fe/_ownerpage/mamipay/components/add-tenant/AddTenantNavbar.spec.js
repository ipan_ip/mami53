import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import AddTenantNavbar from 'Js/_ownerpage/mamipay/components/add-tenant/AddTenantNavbar.vue';
import store from 'Js/_ownerpage/mamipay/store/modules/StoreAddTenant.js';

const localVue = createLocalVue();
localVue.use(Vuex);
let mockStore = { ...store };
window.oxWebUrl = 'https://owner-page.mamikos.com';
Object.defineProperty(window, 'location', {
	value: {
		href: ''
	}
});

describe('AddTenantNavbar.vue', () => {
	const generateWrapper = ({ mocksRoute = {} } = {}) => {
		const mountProp = {
			localVue,
			store: new Vuex.Store({ modules: { addTenant: mockStore } }),
			propsData: {
				activeStepIndex: 1,
				totalStep: 4,
				stepTitle: 'Data penyewa'
			},
			mocks: {
				$router: { go: jest.fn(), push: jest.fn() },
				$route: mocksRoute
			}
		};
		return shallowMount(AddTenantNavbar, mountProp);
	};

	it('should go to the previous route if it is not select kost page', () => {
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantPaymentOther' }
		});

		wrapper.vm.backToPrevStep();
		expect(wrapper.find('#addTenantProfileNavbar').exists()).toBe(true);
		expect(wrapper.vm.$router.go).toBeCalledWith(-1);
	});

	it('should go to ownerpage when add tenant is included', () => {
		mockStore.state.journey = ['router-path-add-tenant'];
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantSelectKost' }
		});

		wrapper.vm.backToPrevStep();
		expect(window.location.href).toBe('https://owner-page.mamikos.com');
	});

	it('should push to the router path when add tenant is not included', () => {
		mockStore.state.journey = ['router-path-select-kost'];
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantSelectKost' }
		});

		wrapper.vm.backToPrevStep();
		expect(wrapper.vm.$router.push).toBeCalled();
	});

	it('should go to ownerpage when router path is not included', () => {
		mockStore.state.journey = ['router-add-tenant'];
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantSelectKost' }
		});

		wrapper.vm.backToPrevStep();
		expect(window.location.href).toBe('https://owner-page.mamikos.com');
	});

	it('should go to the first journey when add tenant and router path is not included', () => {
		mockStore.state.journey = ['https://mamikos.com'];
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantSelectKost' }
		});

		wrapper.vm.backToPrevStep();
		expect(window.location.href).toBe('https://mamikos.com');
	});

	it('should go to ownerpage when journey is empty', () => {
		mockStore.state.journey = [];
		const wrapper = generateWrapper({
			mocksRoute: { name: 'addTenantSelectKost' }
		});

		wrapper.vm.backToPrevStep();
		expect(window.location.href).toBe('https://owner-page.mamikos.com');
	});
});
