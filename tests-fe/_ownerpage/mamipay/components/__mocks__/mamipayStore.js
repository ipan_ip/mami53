import StoreTenantProfile from 'Js/_ownerpage/mamipay/store//modules/StoreProfileTenant';
import StoreManageBooking from 'Js/_ownerpage/mamipay/store//modules/StoreManageBooking';
import StoreBookingPropertyList from 'Js/_ownerpage/mamipay/store//modules/StoreBookingPropertyList';

const currentTime = new Date();

export default {
	modules: {
		tenantProfile: StoreTenantProfile,
		manageBooking: StoreManageBooking,
		bookingPropertyList: StoreBookingPropertyList
	},
	state: {
		token: 'ABCD',
		authCheck: {},
		authData: {
			all: {
				id: 1234567,
				name: 'Naruto Uzumaki',
				email: 'naruto@mamiteam.com',
				address: null,
				is_verify: '1',
				job: null,
				work_place: null,
				position: null,
				semester: null,
				province: null,
				city: null,
				subdistrict: null,
				education: null,
				birthday: '2020-03-04 00:00:00',
				description: null,
				introduction: null,
				nickname: null,
				gender: null,
				age: 22,
				token: 'OHNO',
				phone_number: null,
				fb_location: null,
				fb_education: null,
				phone_country_code: null,
				last_login: null,
				mamipay_last_login: null,
				photo_id: null,
				role: 'user',
				heart_account: 10,
				___follow_count: 0,
				___follower_count: 0,
				created_at: '2020-03-04 06:40:01',
				updated_at: '2020-03-04 06:40:01',
				deleted_at: null,
				last_open_notif: null,
				is_owner: 'false',
				is_top_owner: 'false',
				date_owner_limit: null,
				register_from: 'desktop',
				marital_status: null,
				phone_number_additional: null,
				agree_on_tc: 'false',
				hostility: 0,
				chat_access_token: '',
				photo_user: '',
				consultant: null
			}
		},
		tenantStatus: {},
		isMamipayUser: false,
		userRooms: [],
		userRoom: [],
		userRoomStatus: {},
		allDetailRoom: {},
		listBill: {},
		listBillReport: {},
		listBooking: {},
		totalIncome: '0',
		totalTransfer: '0',
		searchBill: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear(),
			criteria: '',
			limit: '',
			offset: '',
			perPage: 3,
			totalPage: 0,
			sort: 'desc',
			order: 'name',
			billDatePeriode: '',
			selectedNumberPagination: 1
		},
		billReport: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear()
		},
		showHighlight: false,
		rejectReasons: [],
		bankList: [
			{
				key: 'bni',
				bank_name: 'Bank BNI'
			},
			{
				key: 'bca',
				bank_name: 'Bank BCA'
			}
		],
		ownerDetail: {
			name: 'Naruto Uzumaki',
			phone_number: null,
			bank_name_key: '',
			bank_account_owner: ''
		},
		bulkProcessBookingActivation: false
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setTenantStatus(state, payload) {
			state.tenantStatus = payload;
		},
		setIsMamipayUser(state, payload) {
			state.isMamipayUser = payload;
		},
		setUserRooms(state, payload) {
			state.userRooms = payload;
		},
		setUserRoom(state, payload) {
			state.userRoom = payload;
		},
		setUserRoomStatus(state, payload) {
			state.userRoomStatus = payload;
		},
		setListBill(state, payload) {
			state.listBill = payload;
		},
		setListBillReport(state, payload) {
			state.listBillReport = payload;
		},
		setListBooking(state, payload) {
			state.listBooking = payload;
		},
		setTotalIncome(state, payload) {
			state.totalIncome = payload;
		},
		setTotalTransfer(state, payload) {
			state.totalTransfer = payload;
		},
		setSearchBillMonth(state, payload) {
			state.searchBill.month = payload;
		},
		setSearchBillYear(state, payload) {
			state.searchBill.year = payload;
		},
		setSearchBillCriteria(state, payload) {
			state.searchBill.criteria = payload;
		},
		setSearchBillLimit(state, payload) {
			state.searchBill.limit = payload;
		},
		setSearchBillOffset(state, payload) {
			state.searchBill.offset = payload;
		},
		setSearchBillTotalPage(state, payload) {
			state.searchBill.totalPage = payload;
		},
		setSearchBillOrder(state, payload) {
			state.searchBill.order = payload;
		},
		setSearchBillSort(state, payload) {
			state.searchBill.sort = payload;
		},
		setSearchBillPerPage(state, payload) {
			state.searchBill.perPage = payload;
		},
		setBillDatePeriode(state, payload) {
			state.searchBill.billDatePeriode = payload;
		},
		setBillSelectedNumberPagination(state, payload) {
			state.searchBill.selectedNumberPagination = payload;
		},
		setBillReportYear(state, payload) {
			state.billReport.year = payload;
		},
		setBillReportMonth(state, payload) {
			state.billReport.month = payload;
		},
		setPriceDaily(state, payload) {
			state.userRoom.price_daily = payload;
		},
		setPriceMonthly(state, payload) {
			state.userRoom.price_monthly = payload;
		},
		setPriceWeekly(state, payload) {
			state.userRoom.price_weekly = payload;
		},
		setPriceQuarterly(state, payload) {
			state.userRoom.price_quarterly = payload;
		},
		setPriceSemiannualy(state, payload) {
			state.userRoom.price_semiannualy = payload;
		},
		setPriceYearly(state, payload) {
			state.userRoom.price_yearly = payload;
		},
		setAllDetailRoom(state, payload) {
			state.allDetailRoom = payload;
		},
		setHighlight(state, payload) {
			state.showHighlight = payload;
		},
		setRoomAvailability(state, payload) {
			state.allDetailRoom.room_available = payload;
		},
		setRejectReasons(state, payload) {
			state.rejectReasons = payload;
		},
		setBankList(state, payload) {
			state.bankList = payload;
		},
		setOwnerDetail(state, payload) {
			state.ownerDetail = payload;
		},
		setBulkProcessBookingActivation(state, payload) {
			state.bulkProcessBookingActivation = payload;
		}
	},
	actions: {
		fetchBankList() {
			jest.fn().mockName('fetchBankList');
		}
	}
};
