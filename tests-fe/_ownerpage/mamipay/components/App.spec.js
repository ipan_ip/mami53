import { shallowMount, createLocalVue } from '@vue/test-utils';
import App from 'Js/_ownerpage/mamipay/components/App';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);

const generateWrapper = ({ route, mixins }) => {
	return shallowMount(App, {
		localVue,
		mocks: { $route: route },
		mixins,
		stubs: { routerView: { template: `<div />` } }
	});
};

const createNavigatorIsMobile = (isMobile = false) => {
	return {
		computed: {
			navigator() {
				return { isMobile };
			}
		}
	};
};

global.alert = jest.fn();

describe('App.vue', () => {
	it('should check device and show alert if page is not responsive and device is mobile', () => {
		const spyOnAlert = jest.spyOn(global, 'alert');
		generateWrapper({
			route: { name: 'notResponsivePage' },
			mixins: [createNavigatorIsMobile(true)]
		});
		expect(spyOnAlert).toBeCalled();
		spyOnAlert.mockRestore();
	});

	it('should not show alert if page is not responsive and device is not mobile', () => {
		const spyOnAlert = jest.spyOn(global, 'alert');
		generateWrapper({
			route: { name: 'notResponsivePage' },
			mixins: [createNavigatorIsMobile(false)]
		});
		expect(spyOnAlert).not.toBeCalled();
		spyOnAlert.mockRestore();
	});

	it('should not show alert if page is responsive and device is mobile', () => {
		const spyOnAlert = jest.spyOn(global, 'alert');
		generateWrapper({
			route: { name: 'responsivePages', meta: { isResponsive: true } },
			mixins: [createNavigatorIsMobile(true)]
		});
		expect(spyOnAlert).not.toBeCalled();
		spyOnAlert.mockRestore();
	});

	it('should listen to event bus trigger toggle error modal', () => {
		const wrapper = generateWrapper({
			route: { name: 'responsivePages', meta: { isResponsive: true } },
			mixins: [createNavigatorIsMobile(true)]
		});

		EventBus.$emit('showModalError');
		expect(EventBus.$on).toBeCalled();
	});

	it('should open modal error', () => {
		const wrapper = generateWrapper({
			route: { name: 'responsivePages', meta: { isResponsive: true } },
			mixins: [createNavigatorIsMobile(true)]
		});

		wrapper.vm.isModalError = false;
		wrapper.vm.toggleModalError(true);
		expect(wrapper.vm.isModalError).toBe(true);
	});
});
