import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import mockVLazy from '../../../utils/mock-v-lazy';
import ManageModalBookingConfirmation from 'Js/_ownerpage/mamipay/components/ManageModalBookingConfirmation.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
mockVLazy(localVue);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const $toasted = {
	show: jest.fn(),
	clear: jest.fn()
};

mockWindowProperty('tracker', jest.fn());

describe('ManageModalBookingConfirmation', () => {
	let wrapper, _this;

	const mockStore = {
		state: {
			authData: { all: { name: 'Mamikos' } }
		}
	};

	beforeEach(() => {
		wrapper = shallowMount(ManageModalBookingConfirmation, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				showModal: true
			},
			mocks: {
				$toasted
			}
		});

		_this = wrapper.vm;
	});

	it('Should showing modal', async () => {
		const modalWrapper = wrapper.find('modal-stub');
		await _this.$nextTick();
		expect(modalWrapper.attributes('value')).toBe('true');
	});

	it('Should showing toasted when term not checked true', async () => {
		_this.checked = false;
		await _this.$nextTick();
		_this.reqDaftarBooking();
		expect($toasted.show).toBeCalled();
	});

	it('Should emit and refresh component to first state', async () => {
		_this.checked = true;
		await _this.$nextTick();
		_this.reqDaftarBooking();
		expect(wrapper.emitted().reqDaftarBooking).toBeTruthy();
		expect(_this.isFinishReading).toBeFalsy();
		await _this.$nextTick();
		expect(_this.renderComponent).toBeTruthy();
		expect(_this.isFinishReading).toBeFalsy();
	});

	it('Should emit and refresh component to first state if cancel modal', async () => {
		_this.cancelModal();
		await _this.$nextTick();
		expect(_this.isFinishReading).toBeFalsy();
		expect(_this.renderComponent).toBeTruthy();
		expect(_this.isFinishReading).toBeFalsy();
		expect(global.EventBus.$emit).toBeCalledWith('modalActiveBooking', false);
	});

	it('should send correct tracker to moengage', () => {
		_this.trackTnCClicked();
		expect(tracker).toBeCalledWith('moe', [
			'[Owner] Activation BBK - TnC Booking Clicked',
			{
				interface: navigator.isMobile ? 'mobile' : 'desktop',
				owner_name: 'Mamikos'
			}
		]);
	});
});
