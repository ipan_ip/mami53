import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageEmptyBooking from 'Js/_ownerpage/mamipay/components/ManageEmptyBooking.vue';

const localVue = createLocalVue();

describe('ManageEmptyBooking.vue', () => {
	window.open = jest.fn();
	const propsData = {
		totalKos: 10,
		totalBooking: 10
	};
	const wrapper = shallowMount(ManageEmptyBooking, { localVue, propsData });

	it('should load the component', () => {
		expect(wrapper.find('#manageEmptyBooking').exists()).toBe(true);
	});

	it('should return empty message for active filter', () => {
		wrapper.setProps({ noFilter: false });
		expect(wrapper.vm.message.title.toLowerCase()).toBe(
			'belum ada pengajuan booking'
		);
	});

	it('should return empty message for kost have not been registered', () => {
		wrapper.setProps({ noFilter: true, totalKos: 0 });
		expect(wrapper.vm.message.title.toLowerCase()).toBe(
			'kos anda belum terdaftar'
		);
		wrapper.vm.registerKos();
		expect(window.open).toBeCalledWith('/ownerpage/add', '_blank');
	});

	it('should return empty message for bbk active but there is no booking', () => {
		wrapper.setProps({
			totalKos: 10,
			totalBooking: 0,
			roomData: { bbk_status: { key: 'approve' } }
		});
		expect(wrapper.vm.message.title.toLowerCase()).toBe(
			'belum ada pengajuan booking'
		);
		wrapper.vm.registerKos();
		expect(wrapper.emitted('register-bbk')).toBeTruthy();
	});

	it('should return empty message for bbk is waiting approval', () => {
		wrapper.setProps({
			totalBooking: 10,
			roomData: { bbk_status: { key: 'waiting' } }
		});
		expect(wrapper.vm.message.caption.toLowerCase()).toBe(
			'terima kasih sudah mendaftar ke fitur booking langsung. mohon menunggu, tim kami sedang memverifikasi data anda.'
		);
	});

	it('should return empty message for bbk is not registered', () => {
		wrapper.setProps({
			totalBooking: 0,
			roomData: { bbk_status: { key: 'not_active' } }
		});
		expect(wrapper.vm.message.caption.toLowerCase()).toBe(
			'booking langsung adalah fitur agar kos anda bisa di-booking di mamikos. daftar ke fitur ini agar anda bisa mengelola booking di sini.'
		);
	});
});
