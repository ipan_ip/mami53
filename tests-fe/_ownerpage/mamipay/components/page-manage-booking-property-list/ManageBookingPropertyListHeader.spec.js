import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import '../__mocks__/mockToken';
import ManageBookingPropertyListHeader from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/ManageBookingPropertyListHeader';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

global.bugsnagClient = { notify: jest.fn() };

describe('ManageBookingPropertyListHeader.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageBookingPropertyListHeader, {
			localVue,
			store,
			stubs: {
				ManageBookingPropertyListFilters: mockComponent,
				ManageBookingPropertyListSelectionControl: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-header').exists()).toBe(true);
	});

	it('Should call setIsSelectAll mutation with false value when a change in selectedFilter data is happened', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.vm.$store.commit(
			'bookingPropertyList/setSelectedFilter',
			'approve'
		);

		await wrapper.vm.$nextTick();

		expect(spyCommit).toBeCalledWith(
			'bookingPropertyList/setIsSelectAll',
			false
		);
	});

	it('Should call setIsSelectAll mutation with false value when a change in isLoading data is happened', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.vm.$store.state.bookingPropertyList.isLoadingProperties = true;

		await wrapper.vm.$nextTick();

		expect(spyCommit).toBeCalledWith(
			'bookingPropertyList/setIsSelectAll',
			false
		);
	});

	it('Should emit submit when submitBookingProperties method is called', () => {
		wrapper.vm.submitBookingProperties();
		expect(wrapper.emitted('submit')).toBeTruthy();
	});
});
