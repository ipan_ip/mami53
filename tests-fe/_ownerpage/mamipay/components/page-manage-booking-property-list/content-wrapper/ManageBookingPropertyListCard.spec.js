import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageBookingPropertyListCard from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/content-wrapper/ManageBookingPropertyListCard.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

const $router = { push: jest.fn() };

window.oxWebUrl = 'https://ownerpage.mamikos.com';
global.oxWebUrl = window.oxWebUrl;

const replace = jest.fn();
mockWindowProperty('window.location', { replace });

describe('ManageBookingPropertyListCard.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageBookingPropertyListCard, {
			localVue,
			stubs: {
				SkeletonLoader: mockComponent,
				MamiInfo: mockComponent
			},
			mocks: {
				$router
			}
		});

		wrapper.setProps({
			propertyDetail: {
				id: 1,
				photo: {
					small: ''
				},
				bbk_status: {
					key: 'approve',
					value: 'Aktif'
				},
				room_title: 'Mock Room Title',
				gender: '0',
				last_update: '1 hari yang lalu',
				room_available: 0
			}
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-card').exists()).toBe(true);
	});

	it('Should redirect window.location to /ownerpage when kelola tagihan clicked', () => {
		const manageBillLink = wrapper.find('.manage-bill-link');

		expect(manageBillLink.exists()).toBe(true);
		manageBillLink.trigger('click');

		expect(window.location).toBe(window.oxWebUrl + '/billing-management');
	});
});
