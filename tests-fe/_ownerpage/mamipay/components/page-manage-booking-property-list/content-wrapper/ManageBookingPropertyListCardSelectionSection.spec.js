import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingPropertyListCardSelectionSection from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/content-wrapper/ManageBookingPropertyListCardSelectionSection.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import propertyList from '../__mocks__/propertyList.json';

const localVue = createLocalVue();

describe('ManageBookingPropertyListCardSelectionSection.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageBookingPropertyListCardSelectionSection, {
			localVue,
			stubs: {
				ManageBookingPropertyListCard: mockComponent
			}
		});

		wrapper.setProps({ propertyList });
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-cards').exists()).toBe(true);
	});

	it("Should update local selectedProperties value to match parent's", async () => {
		wrapper.setProps({
			selectedProperties: [1, 2]
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.localSelectedProperties).toBe(
			wrapper.vm.selectedProperties
		);
	});

	it('Should emit setSelectedProperties when local selectedProperties value changed', async () => {
		wrapper.setData({
			localSelectedProperties: [1, 3]
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('setSelectedProperties')[0][0]).toBe(
			wrapper.vm.localSelectedProperties
		);
	});
});
