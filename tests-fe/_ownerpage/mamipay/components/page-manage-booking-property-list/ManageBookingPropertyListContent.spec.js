import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import '../__mocks__/mockToken';
import ManageBookingPropertyListContent from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/ManageBookingPropertyListContent';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

global.bugsnagClient = { notify: jest.fn() };

describe('ManageBookingPropertyListContent.vue', () => {
	const wrapper = shallowMount(ManageBookingPropertyListContent, {
		localVue,
		store,
		stubs: {
			ManageBookingPropertyListCardSelectionSection: mockComponent,
			ManageBookingPropertyListCardLoadingPlaceholder: mockComponent,
			ManageMessagePage: mockComponent
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-content').exists()).toBe(true);
	});

	it('Should call setNumberOfSelected mutation with selectedProperties value length when a change happened', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		const mockPropertyIds = [1, 2, 4, 5];

		wrapper.vm.$store.state.bookingPropertyList.selectedProperties = mockPropertyIds;

		await wrapper.vm.$nextTick();

		expect(spyCommit).toBeCalledWith(
			'bookingPropertyList/setNumberOfSelectedProperties',
			mockPropertyIds.length
		);
	});

	it('Should emit loadMore when loadMore method is called', async () => {
		wrapper.vm.loadMore();

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('loadMore')).toBeTruthy();
	});
});
