import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingPropertyListFilters from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/header-wrapper/ManageBookingPropertyListFilters';

const localVue = createLocalVue();

describe('ManageBookingPropertyListFilters.vue', () => {
	const wrapper = shallowMount(ManageBookingPropertyListFilters, {
		localVue,
		propsData: {
			items: ['all', 'approve', 'waiting', 'not_active'],
			selectedFilter: 'all'
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-filter').exists()).toBe(true);
	});

	it('Should emit select when a filter button is clicked', () => {
		const filterValue = 'approve';

		wrapper.vm.onSelect(filterValue);

		expect(wrapper.emitted('select')[0][0]).toBe(filterValue);
	});
});
