import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import ManageBookingPropertyListSelectionControl from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/header-wrapper/ManageBookingPropertyListSelectionControl';

const localVue = createLocalVue();

describe('ManageBookingPropertyListSelectionControl.vue', () => {
	const wrapper = shallowMount(ManageBookingPropertyListSelectionControl, {
		localVue,
		propsData: {
			numberOfSelected: 0,
			isSelectAll: false
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props-selections').exists()).toBe(
			true
		);
	});

	it("Should update local isSelectAll value to match parent's", async () => {
		wrapper.setProps({
			isSelectAll: true
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isSelectAllChecked).toBe(wrapper.vm.isSelectAll);
	});

	it('Should emit setIsSelectAll when local isSelectAll value changed', async () => {
		wrapper.setData({
			isSelectAllChecked: true
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('setIsSelectAll')[0][0]).toBe(
			wrapper.vm.isSelectAllChecked
		);
	});

	it('Should emit submit when onSubmit function is called', () => {
		wrapper.vm.onSubmit();
		expect(wrapper.emitted('submit')).toBeTruthy();
	});
});
