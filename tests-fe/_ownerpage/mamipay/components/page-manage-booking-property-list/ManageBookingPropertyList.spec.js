import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import '../__mocks__/mockToken';
import ManageBookingPropertyList from 'Js/_ownerpage/mamipay/components/page-manage-booking-property-list/ManageBookingPropertyList';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';
import mockComponent from 'tests-fe/utils/mock-component';
import MixinNavigatorIsMobile from 'Js/@mixins/MixinNavigatorIsMobile';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile]);
localVue.use(Vuex);

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

global.tracker = jest.fn();
navigator.isMobile = true;
global.bugsnagClient = { notify: jest.fn() };

describe('ManageBookingPropertyList.vue', () => {
	const reFetchPropertiesSpy = jest.spyOn(
		ManageBookingPropertyList.methods,
		'reFetchProperties'
	);
	const handleFetchPropertiesSpy = jest.spyOn(
		ManageBookingPropertyList.methods,
		'handleFetchProperties'
	);
	const wrapper = shallowMount(ManageBookingPropertyList, {
		localVue,
		store,
		stubs: {
			ManageBookingPropertyListHeader: mockComponent,
			ManageBookingPropertyListContent: mockComponent,
			ManageModalBooking: mockComponent
		}
	});
	const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

	it('Should exists', () => {
		expect(wrapper.find('.manage-booking-props').exists()).toBe(true);
	});

	it('Should call reFetchProperties method when a change in selectedFilter is happened', async () => {
		wrapper.vm.$store.state.bookingPropertyList.selectedFilter = 'approve';

		await wrapper.vm.$nextTick();

		expect(reFetchPropertiesSpy).toHaveBeenCalled();
	});

	it('Should handle load more properties properly', () => {
		const nextOffset =
			wrapper.vm.$store.state.bookingPropertyList.currentOffset +
			wrapper.vm.$store.state.bookingPropertyList.perScreenLimit;

		wrapper.vm.loadMore();

		expect(spyCommit).toBeCalledWith(
			'bookingPropertyList/setCurrentOffset',
			nextOffset
		);

		expect(handleFetchPropertiesSpy).toHaveBeenCalled();
	});

	it('Should handle change active modal component name properly', () => {
		const mockComponentName = 'mockModal';

		wrapper.vm.changeActiveModalComponent(mockComponentName);

		expect(wrapper.vm.activeModalComponentName).toBe(mockComponentName);
	});

	it('Should handle submit booking method properly', () => {
		wrapper.vm.submitBookingProperties();

		expect(spyCommit).toBeCalledWith('setBulkProcessBookingActivation', true);
	});
});
