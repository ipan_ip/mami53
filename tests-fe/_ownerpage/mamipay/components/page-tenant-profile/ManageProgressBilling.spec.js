import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ManageProgressBilling from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProgressBilling.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

let mockStore = {
	state: {
		tenantProfile: {
			widgetInfo: {
				billingDate: 9,
				countDuration: '6 bulan',
				contractType: 'Perbulan',
				totalContractAmount: '1000000',
				totalTransferredAmount: '250000'
			}
		}
	}
};

describe('ManageProgressBilling', () => {
	it('should mount vue component', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#manageProgressBilling').exists()).toBe(true);
	});

	it('should show total transferred amount and total contract amount', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const amountValues = wrapper.findAll('.text-value');
		expect(amountValues.at(0).text()).toBe('Rp250000');
		expect(amountValues.at(1).text()).toBe('Rp1000000');
	});

	it('should show contract type from store', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const contractType = wrapper.findAll('.cost-add.bold').at(0);
		expect(contractType.text()).toBe('Perbulan');
	});

	it('should show rent duration from store', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const countDuration = wrapper.findAll('.cost-add.bold').at(1);
		expect(countDuration.text()).toBe('6 bulan');
	});

	it('should show billing date from store', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const billingDate = wrapper.findAll('.cost-add.bold').at(2);
		expect(billingDate.text()).toBe('Tanggal 9');
	});

	it('should count percentage progress to match style', () => {
		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const percentageProgressStyle = wrapper.find('.wrapper-progress span');
		expect(percentageProgressStyle.element.getAttribute('style')).toBe(
			'width: 25%;'
		);
	});

	it('should render dash if billing date is not exists', () => {
		mockStore.state.tenantProfile.widgetInfo.billingDate = null;

		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const billingDate = wrapper.findAll('.cost-add.bold').at(2);
		expect(billingDate.text()).toBe('Tanggal -');
	});

	it('should render dash if duration is not exists', () => {
		mockStore.state.tenantProfile.widgetInfo.countDuration = null;

		const wrapper = shallowMount(ManageProgressBilling, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const countDuration = wrapper.findAll('.cost-add.bold').at(1);
		expect(countDuration.text()).toBe('-');
	});
});
