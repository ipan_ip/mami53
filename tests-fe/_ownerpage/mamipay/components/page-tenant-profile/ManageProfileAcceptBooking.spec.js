import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageProfileAcceptBooking from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileAcceptBooking.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const mockStore = {
	state: {
		manageBooking: {
			acceptBookingProfile: {
				gender: 'female',
				occupation: 'Mahasiswa',
				occupation_description: 'ITB',
				email: 'user@mail.com',
				parent_name: 'charlie'
			}
		},
		tenantProfile: {
			statusPage: 'member'
		}
	}
};

describe('ManageProfileAcceptBooking', () => {
	it('should mount the component', () => {
		const wrapper = shallowMount(ManageProfileAcceptBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#manageProfileAcceptBooking').exists()).toBe(true);
	});

	it('should show tenant gender', () => {
		const wrapper = shallowMount(ManageProfileAcceptBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const genderText = wrapper.findAll('.desc').at(0);
		expect(genderText.text()).toBe('Perempuan');
	});

	it('should show tenant job', () => {
		const wrapper = shallowMount(ManageProfileAcceptBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const jobText = wrapper.findAll('.desc').at(2);
		expect(jobText.text()).toBe('Mahasiswa - ITB');
	});

	it('should show tenant email', () => {
		const wrapper = shallowMount(ManageProfileAcceptBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const emailText = wrapper.findAll('.desc').at(3);
		expect(emailText.text()).toBe('user@mail.com');
	});

	it('should show parent name', () => {
		const wrapper = shallowMount(ManageProfileAcceptBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const parentName = wrapper.findAll('.desc').at(4);
		expect(parentName.text()).toBe('charlie');
	});
});
