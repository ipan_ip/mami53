import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ManageDetailTimeline from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageDetailTimeline.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

const timeline = [
	{
		passed: true,
		time: '2020-09-20',
		description: 'Lorem ipsum'
	},
	{
		passed: true,
		time: '2020-09-21',
		description: 'Lorem ipsum version 2'
	}
];

describe('ManageDetailTimeline', () => {
	let wrapper, store;

	beforeEach(() => {
		wrapper = shallowMount(ManageDetailTimeline, {
			localVue,
			propsData: {
				timelineInvoice: timeline
			}
		});
	});

	it('should mount a vue component', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render timeline card', () => {
		const timelineCards = wrapper.findAll('.timeline-card');
		expect(timelineCards.length).toBe(2);
	});

	it('should show one connector', () => {
		const connectors = wrapper.findAll('.connector');
		expect(connectors.length).toBe(1);
	});

	it('should show date based on index', () => {
		const infoDates = wrapper.findAll('.info-date');
		expect(infoDates.at(0).text()).toBe('System - 2020-09-20');
		expect(infoDates.at(1).text()).toBe('System - 2020-09-21');
	});

	it('should show description based on index', () => {
		const infoPayments = wrapper.findAll('.info-payment span');
		expect(infoPayments.at(0).text()).toBe('Lorem ipsum');
		expect(infoPayments.at(1).text()).toBe('Lorem ipsum version 2');
	});
});
