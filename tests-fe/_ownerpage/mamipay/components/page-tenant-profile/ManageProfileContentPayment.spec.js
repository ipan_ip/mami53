import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageProfileContentPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileContentPayment.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;
	return global.EventBus;
});

jest.mock('Js/@mixins/MixinNavigatorIsMobile', () => {});

const mockStore = {
	state: {
		tenantProfile: {
			tenantBill: {
				otherCost: [{ field_title: '', field_value: '', id: 0 }]
			}
		}
	}
};

describe('ManageProfileContentPayment', () => {
	it('should render vue component', () => {
		const wrapper = shallowMount(ManageProfileContentPayment, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#manageProfileContentPayment').exists()).toBe(true);
	});

	it('should open modal payment', () => {
		const wrapper = shallowMount(ManageProfileContentPayment, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const params = { column: { field: 'status' }, row: { originalIndex: 3 } };
		wrapper.vm.openModalPayment(params);
		expect(wrapper.vm.lastIndexBilling).toBe(3);
		expect(wrapper.vm.isModalBillingShow).toBeTruthy();
	});

	it('should open modal billing', () => {
		const wrapper = shallowMount(ManageProfileContentPayment, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		wrapper.vm.openModalBilling(6);
		expect(wrapper.vm.lastIndexBilling).toBe(6);
		expect(wrapper.vm.isModalBillingShow).toBeTruthy();
	});

	it('should open modal send notif', () => {
		const wrapper = shallowMount(ManageProfileContentPayment, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		wrapper.vm.openModalSendNotification(67);
		expect(wrapper.vm.lastInvoiceId).toBe(67);
		expect(wrapper.vm.isModalNotification).toBeTruthy();
	});

	it('should send notification', () => {
		const sendNotificationToTenant = jest.fn();

		const wrapper = shallowMount(ManageProfileContentPayment, {
			localVue,
			store: new Vuex.Store(mockStore),
			methods: { sendNotificationToTenant }
		});

		wrapper.vm.lastInvoiceId = 67;
		wrapper.vm.sendNotification();
		expect(sendNotificationToTenant).toBeCalledWith(67);
	});
});
