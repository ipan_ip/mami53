import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageBillMenu from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageBillMenu.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const mockStore = {
	state: {
		tenantProfile: {
			billingProfile: [
				{
					billingAmount: 126400,
					dateBill: '2020-03-09',
					information: 'Dibayar',
					invoiceLink: 'https://padang2.mamikos.com/invoice/I7uQD',
					invoiceNumber: '66591690/2020/03/0015',
					menu: 1516,
					reminder: false,
					status: 'paid'
				}
			]
		}
	}
};

describe('ManageBillMenu', () => {
	it('should mount bill component', () => {
		const wrapper = shallowMount(ManageBillMenu, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				dataIndex: 0
			}
		});

		expect(wrapper.find('#manageBillMenu').exists()).toBe(true);
	});

	it('should render two images', () => {
		const wrapper = shallowMount(ManageBillMenu, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				dataIndex: 0
			}
		});

		const imagesRendered = wrapper.findAll('img');
		expect(imagesRendered.length).toBe(2);
	});

	it('should show link to detail bill', () => {
		const wrapper = shallowMount(ManageBillMenu, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				dataIndex: 0
			}
		});

		const menuLinks = wrapper.findAll('.menu-link span');
		expect(menuLinks.length).toBe(1);
		expect(menuLinks.at(0).text()).toBe('Detail Tagihan');
	});

	it('should show link to add another cost', () => {
		mockStore.state.tenantProfile.billingProfile[0].status = 'waiting';

		const wrapper = shallowMount(ManageBillMenu, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				dataIndex: 0
			}
		});

		const addCostLink = wrapper.findAll('.menu-link span').at(1);
		expect(addCostLink.text()).toBe('Tambah Biaya Lainnya');
	});

	it('should open modal bill', () => {
		const wrapper = shallowMount(ManageBillMenu, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				dataIndex: 0
			}
		});

		const menuLink = wrapper.find('.menu-link');
		menuLink.trigger('click');
		expect(wrapper.emitted().openModalBill).toBeTruthy();
	});
});
