import 'tests-fe/utils/mock-vue';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailBillingOtherCost from 'Js/_ownerpage/mamipay/components/page-tenant-profile/detail-billing/DetailBillingOtherCost.vue';

const localVue = createLocalVue();

describe('DetailBillingOtherCost.vue', () => {
	const wrapper = shallowMount(DetailBillingOtherCost, { localVue });

	it('should render component', () => {
		expect(wrapper.find('.wrapper-bill').exists()).toBe(true);
	});
});
