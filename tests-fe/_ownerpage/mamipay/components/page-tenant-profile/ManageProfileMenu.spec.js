import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageProfileMenu from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileMenu.vue';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const generateWrapper = ({
	storeProp = {},
	methodsProp = {},
	mocksProp = {}
} = {}) => {
	const scrollTo = jest.fn();
	const currentTime = new Date();

	const defaultStore = {
		state: {
			tenantProfile: {
				tenantBill: {
					otherCost: [
						{
							field_title: '',
							field_value: '',
							id: 0
						}
					]
				},
				tenantProfile: {
					fullName: 'Mami 53',
					work: 'mahasiswa'
				},
				billingProfile: [
					{
						billingAmount: 126400,
						dateBill: '2020-03-09',
						information: 'Dibayar',
						invoiceLink: 'https://padang2.mamikos.com/invoice/I7uQD',
						invoiceNumber: '66591690/2020/03/0015',
						menu: 1516,
						reminder: false,
						status: 'waiting'
					}
				],
				widgetInfo: {
					terminationInfo: null
				},
				statusPage: 'member'
			},
			searchBill: {
				month: currentTime.getMonth() + 1,
				year: currentTime.getFullYear(),
				criteria: '',
				limit: '',
				offset: '',
				perPage: 3,
				totalPage: 0,
				sort: 'desc',
				order: 'name',
				billDatePeriode: '',
				selectedNumberPagination: 1
			}
		},
		mutations: {
			setSearchBillLimit: jest.fn(),
			setSearchBillPerPage: jest.fn(),
			setSearchBillOffset: jest.fn()
		}
	};

	const mockStore = {
		...defaultStore,
		...storeProp
	};

	const $route = {
		name: 'biodataTenant',
		params: {
			tenantId: 4404
		}
	};

	const $dayjs = jest.fn(() => ({
		format: jest.fn().mockReturnValue('Minggu, 23 Agustus 2020')
	}));

	const shallowMountProp = {
		localVue,
		store: new Vuex.Store(mockStore),
		directives: {
			'scroll-to': scrollTo
		},
		methods: {
			...methodsProp
		},
		mocks: {
			$route,
			$dayjs,
			...mocksProp
		},
		stubs: {
			RouterLink: RouterLinkStub
		}
	};

	return shallowMount(ManageProfileMenu, shallowMountProp);
};

describe('ManageProfileMenu.vue', () => {
	it('should render component correctly', () => {
		const wrapper = generateWrapper();

		expect(wrapper.find('#manageProfileMenu').exists()).toBeTruthy();
	});

	it('should hide ManageProfile menu list when tenant status is acceptBooking', async () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'acceptBooking'
					}
				}
			}
		});

		expect(wrapper.findAll('.parent')).toHaveLength(0);
	});

	it('should show ManageProfile menu list when tenant status is not acceptBooking', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'member'
					}
				}
			}
		});

		expect(wrapper.findAll('.parent').length).toBeGreaterThan(0);
	});

	it('should have class parent-active when route name matched', async () => {
		const wrapper = generateWrapper();

		expect(wrapper.find('.parent.parent-active').exists()).toBeTruthy();
		expect(wrapper.find('.parent.parent-active').text()).toBe(
			'Biodata Penyewa'
		);

		wrapper.vm.$route.name = 'tenantBill';
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.parent.parent-active').exists()).toBeTruthy();
		expect(wrapper.find('.parent.parent-active').text()).toBe(
			'Atur Kontrak & Tagihan Penyewa'
		);

		wrapper.vm.$route.name = 'paymentTenant';
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.parent.parent-active').exists()).toBeTruthy();
		expect(wrapper.find('.parent.parent-active').text()).toBe(
			'Tagihan Pembayaran'
		);
	});

	it('should hide Tagihan Pembayaran menu when tenant status is not member', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'newMember'
					}
				}
			}
		});

		const FirstParentMenu = wrapper.findAll('.parent').at(0);

		expect(FirstParentMenu.text()).not.toBe('Tagihan Pembayaran');
	});

	it('should show Tagihan Pembayaran menu when tenant status is member', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'member'
					}
				}
			}
		});

		const FirstParentMenu = wrapper.findAll('.parent').at(0);

		expect(FirstParentMenu.exists()).toBeTruthy();
		expect(FirstParentMenu.text()).toBe('Tagihan Pembayaran');
	});

	it('should show Termination Menu when tenant status is member and not terminated', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'member'
					}
				}
			}
		});

		expect(wrapper.vm.isShowTerminationMenu).toBeTruthy();
		expect(wrapper).toMatchSnapshot();
	});

	it('should emmit actionModalStopRent when stop contract button clicked', async () => {
		const wrapper = generateWrapper();

		await wrapper.vm.openModalReasonStopRent();

		expect(global.EventBus.$emit).toBeCalledWith('actionModalStopRent', {
			isOpenModal: true,
			contractId: 4404,
			tenantName: 'Mami 53',
			checkoutDateList: []
		});
	});

	it('should labelling checkout list correctly', () => {
		const wrapper = generateWrapper();

		const checkoutDateList = ['2020-08-23'];

		const labeledCheckoutList = wrapper.vm.labellingCheckoutList(
			checkoutDateList
		);

		expect(labeledCheckoutList).toEqual([
			{
				label: 'Minggu, 23 Agustus 2020',
				value: '2020-08-23'
			}
		]);
	});

	it('should set link menu correctly', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'member'
					}
				}
			}
		});

		const url = wrapper.vm.urlLinkTenant('profile');
		expect(url).toBe('/profile-tenant/4404/biodata-tenant');
	});

	it('should set link menu correctly if status member is new member', () => {
		const wrapper = generateWrapper({
			storeProp: {
				state: {
					tenantProfile: {
						tenantProfile: {
							fullName: 'Mami 53',
							work: 'mahasiswa'
						},
						statusPage: 'newMember'
					}
				}
			}
		});

		const url = wrapper.vm.urlLinkTenant('profile');
		expect(url).toBe('/profile-tenant/4404/biodata-tenant/new-member');
	});
});
