import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ManageProfileMain from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileMain';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import additionalPrice from './__mocks__/additional-price.json';
import Vuex from 'vuex';
import dayjs from 'dayjs';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const $route = {
	name: 'newMember',
	params: {
		typeProfile: 'new-member',
		tenantId: 1234
	},
	query: {}
};

const store = {
	state: {
		authCheck: {
			all: true,
			owner: true
		},
		authData: {
			all: {
				id: 1
			}
		},
		tenantProfile: {
			widgetInfo: {
				roomNumber: 'room number'
			},
			tenantProfile: {
				roomId: 123,
				roomAllotment: {
					name: 'allotment name',
					floor: 'allotment floor'
				}
			},
			statusPage: 'newMember',
			tenantBill: {
				otherCost: []
			}
		},
		manageBooking: {
			acceptBookingProfile: {
				name: 'tenant name'
			},
			listBookingSelected: {
				room_id: 123
			}
		},
		userRoom: {
			id: 123
		},
		userRooms: [
			{
				id: 123,
				booking_status: true,
				room_title: 'room 123'
			}
		]
	},
	mutations: {
		setSearchBookingPerPage: jest.fn(),
		setSearchBookingLimit: jest.fn(),
		setSearchBookingOffset: jest.fn(),
		setSearchBookingGroup: jest.fn(),
		setBookingSelectedNumberPagination: jest.fn(),
		resettenantProfile: jest.fn(),
		resettenantBill: jest.fn(),
		resetWidgetInfo: jest.fn(),
		setStatusPage: jest.fn(),
		setRoomId: jest.fn(),
		setUserRoomStatus: jest.fn(),
		setLoadingState: jest.fn(),
		setSelectedPaymentYear: jest.fn(),
		setUserRoom: jest.fn(),
		setForOtherTenant: jest.fn(),
		setBillingProfile: jest.fn()
	},
	actions: {
		resettenantProfile: jest.fn(),
		resettenantBill: jest.fn(),
		resetWidgetInfo: jest.fn()
	}
};

const mockAdditionalPriceMutations = [
	'setOtherCost',
	'setCostDeposit',
	'setCostFine',
	'setDurationDay',
	'setDayType',
	'setDownPayment',
	'setCostDownPayment'
];

mockAdditionalPriceMutations.forEach(mutation => {
	store.mutations[mutation] = jest.fn();
});

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;

const GlobalNavbar = { template: '<div />' };

global.oxWebUrl = 'owner.mamikos.com';
const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: {}
		}
	})
};

const ManageProfileTerminationCard = {
	template: '<div class="manage-profile-termination-card" />'
};

describe('ManageProfileMain.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageProfileMain, {
			localVue,
			store: new Vuex.Store(store),
			mocks: {
				$route,
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			},
			stubs: { GlobalNavbar, ManageProfileTerminationCard, RouterView: true }
		});
	});

	it('should render profile main wrapper', () => {
		expect(wrapper.find('.profile-main-wrapper').exists()).toBe(true);
	});

	it('should return room number properly', () => {
		expect(wrapper.vm.tenantRoom).toBe('allotment name allotment floor');

		wrapper.vm.$store.state.tenantProfile.tenantProfile.roomAllotment = null;
		expect(wrapper.vm.tenantRoom).toBe('Kamar room number');

		wrapper.vm.$store.state.tenantProfile.widgetInfo.roomNumber = '';
		expect(wrapper.vm.tenantRoom).toBe('Kamar Pilih di Tempat');
	});

	it('should return tenant data properly', () => {
		expect(wrapper.vm.tenantDataName).toBe('tenant name');
	});

	it('should handle close checkin notification banner', async () => {
		wrapper.setData({ isTenantCheckin: true });
		wrapper.vm.closeCheckinNotificationBanner();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isTenantCheckin).toBe(false);
	});

	it('should handle terminate request submitted properly', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.setData({ terminationInfo: { status: 'waiting' } });
		await wrapper.vm.$nextTick();

		wrapper.findComponent(ManageProfileTerminationCard).vm.$emit('confirmed');
		expect(wrapper.vm.terminationInfo.status).toBe('confirmed');
		expect(wrapper.vm.isShowTerminationCard).toBe(true);

		wrapper.findComponent(ManageProfileTerminationCard).vm.$emit('rejected');
		expect(wrapper.vm.terminationInfo.status).toBe('rejected');
		expect(wrapper.vm.isShowTerminationCard).toBe(false);

		wrapper
			.findComponent(ManageProfileTerminationCard)
			.vm.$emit('submit-error');
		expect(global.EventBus.$emit).toBeCalledWith('showModalError');
	});

	it('should return full year properly', () => {
		expect(wrapper.vm.formatDate('02-02-2020')).toBe('2020');
	});

	it('should get payment data properly', () => {
		wrapper.vm.$route.params.tenantId = 1010;
		const spyGetContractBill = jest.spyOn(wrapper.vm, 'getContractBill');
		wrapper.vm.getDataPayment('02-02-2020');
		expect(spyGetContractBill).toBeCalledWith(1010, '2020');
		spyGetContractBill.mockRestore();
	});

	it('should get room properly', () => {
		wrapper.vm.$route.params.tenantId = 12;
		const getUserRoomById = jest.spyOn(wrapper.vm, 'getUserRoomById');
		const getAllDetailRoom = jest.spyOn(wrapper.vm, 'getAllDetailRoom');
		wrapper.vm.getRoom(1212);
		expect(getUserRoomById).toBeCalledWith(1212, 12);
		expect(getAllDetailRoom).toBeCalledWith(1212);
	});

	it('should set previous page properly', () => {
		ManageProfileMain.beforeRouteEnter.call(
			wrapper.vm,
			undefined,
			'/from',
			jest.fn(fn => {
				fn(wrapper.vm);
			})
		);

		expect(wrapper.vm.previousRoute).toBe('/from');
		expect(wrapper.vm.isFirstPageAfter).toBe(false);
	});

	describe('Additional Price', () => {
		const wrapper = shallowMount(ManageProfileMain, {
			localVue,
			store: new Vuex.Store(store),
			mocks: {
				$route: { params: { tenantId: 123 }, name: 'tenantBill' }
			},
			stubs: { 'router-view': { template: '<div />' } },
			methods: {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			}
		});

		it('should set additional price properly', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

			wrapper.vm.setAdditionalCost('month', additionalPrice);
			[...mockAdditionalPriceMutations, 'setForOtherTenant'].forEach(
				mutation => {
					expect(spyCommit).toHaveBeenCalledWith(mutation, expect.anything());
				}
			);
			spyCommit.mockRestore();
		});

		it('should not set additional price if active is false', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			wrapper.vm.setAdditionalCost(
				'month',
				additionalPrice.map(price => {
					return {
						...price,
						active: false
					};
				})
			);

			mockAdditionalPriceMutations.forEach(mutation => {
				expect(spyCommit).not.toBeCalledWith(mutation);
			});
			spyCommit.mockRestore();
		});
	});
});
