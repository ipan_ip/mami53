import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex, { Store } from 'vuex';
import 'tests-fe/utils/mock-debounce';
import ManageInputWork from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputWork.vue';
import VeeValidate, { Validator } from 'vee-validate';
import { flushPromises } from 'tests-fe/utils/promises';
import id from 'vee-validate/dist/locale/id';

const localVue = createLocalVue();

localVue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

global.bugsnagClient = { notify: jest.fn() };

const store = {
	state: {
		tenantProfile: {
			tenantProfile: {
				work: ''
			},
			statusPage: 'newMember'
		}
	},
	mutations: {
		setWork(state, payload) {
			state.tenantProfile.tenantProfile.work = payload;
		}
	}
};

describe('ManageInputWork.vue', () => {
	const wrapper = shallowMount(ManageInputWork, {
		localVue,
		store: new Store(store)
	});
	it('should render properly', async () => {
		expect(wrapper.find('#inputWork').exists()).toBeTruthy();
	});

	it('should set computed data properly', async () => {
		wrapper.vm.inputWork = 'Mahasiswa';
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputWork).toBe('Mahasiswa');
		expect(wrapper.vm.statusPage).toBe(store.state.tenantProfile.statusPage);
	});

	it('should set inputWork properly', async () => {
		const input = wrapper.find('#inputWork');
		await input.setValue('Mahasiswa');

		expect(wrapper.vm.inputWork).toBe('Mahasiswa');
	});

	it('should set isError false if value valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputWork');
		await input.setValue('Mahasiswa');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeFalsy();
		expect(wrapper.vm.inputWork).toBe('Mahasiswa');
		jest.clearAllTimers();
	});

	it('should set isError true if value not valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputWork');
		await input.setValue(null);
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeTruthy();
		jest.clearAllTimers();
	});

	it('should not set isError if there is error on debounce', async () => {
		global.validator.validate = jest.fn().mockRejectedValue(new Error('error'));
		wrapper.setData({ isError: false });
		jest.useFakeTimers();

		const input = wrapper.find('#inputWork');
		await input.setValue('Error');
		await input.trigger('change');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeFalsy();
	});
});
