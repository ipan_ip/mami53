import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ManageInputDurationDay from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputDurationDay.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	state: {
		tenantProfile: {
			tenantBill: {
				durationDay: 0,
				dayType: 'day'
			}
		}
	},
	mutations: {
		setDurationDay(state, payload) {
			state.tenantProfile.tenantBill.durationDay = payload;
		},
		setDayType(state, payload) {
			state.tenantProfile.tenantBill.dayType = payload;
		}
	}
});

const mockGetElementById = id => {
	const mockElement = document.createElement('div');
	mockElement.setAttribute('id', id);
	return mockElement;
};
global.document.getElementById = mockGetElementById;

const wrapper = shallowMount(ManageInputDurationDay, {
	localVue,
	store
});

describe('ManageInputDurationDay', () => {
	it('computed inputDurationDay can set value', () => {
		wrapper.vm.inputDurationDay = 5;
		const inputDurationDay = wrapper.vm.inputDurationDay;
		expect(inputDurationDay).toBe(5);
	});

	it('computed inputDayType can set value', () => {
		wrapper.vm.inputDayType = 'week';
		const inputDayType = wrapper.vm.inputDayType;
		expect(inputDayType).toBe('week');
	});
});
