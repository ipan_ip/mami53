import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputRentDuration from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputRentDuration.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: 'newMember',
			roomSelectedDetail: {
				rentDurationList: ['1 Bulan', '2 Bulan']
			},
			tenantBill: {
				rentDuration: 1
			},
			widgetInfo: {
				countDuration: '1 Bulan'
			}
		}
	},
	mutations: {
		setRentDuration: (state, payload) => {
			state.tenantProfile.tenantBill.rentDuration = payload;
		}
	}
};

describe('ManageInputRentDuration.vue', () => {
	const wrapper = shallowMount(ManageInputRentDuration, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set correct duration', async () => {
		wrapper.find('select#inputStatus').setValue(2);
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentDuration).toBe(2);
	});

	it('should reset duration option when rentDurationList changed', async () => {
		wrapper.vm.$store.state.tenantProfile.roomSelectedDetail.rentDurationList = [
			'3 Bulan',
			'6 Bulan',
			'12 Bulan'
		];
		await wrapper.vm.$nextTick();
		expect(wrapper.findAll('select#inputStatus > option').length).toBe(4);
	});

	it('should set selected duration', async () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.rentDuration = 3;
		const rentDurationList = ['1', '2', '3'];
		wrapper.vm.$store.state.tenantProfile.roomSelectedDetail.rentDurationList = rentDurationList;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentDuration).toBe(2);
	});

	it('should not set selected duration if no list given', async () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.rentDuration = 3;
		wrapper.vm.$store.state.tenantProfile.roomSelectedDetail.rentDurationList = null;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.rentDuration).toBe(3);
	});

	it('should disable select when status page is acceptBooking', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'acceptBooking';
		await wrapper.vm.$nextTick();
		expect(wrapper.find('select#inputStatus').attributes('disabled')).toBe(
			'disabled'
		);
	});

	it('should show input when status page is member', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		await wrapper.vm.$nextTick();
		expect(wrapper.find('select#inputStatus').exists()).toBe(false);
		expect(wrapper.find('input[type=text]').exists()).toBe(true);
	});

	it('should set input value to "-" if no given countDuration', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.vm.$store.state.tenantProfile.widgetInfo.countDuration = 0;
		await wrapper.vm.$nextTick();
		expect(wrapper.find('input[type=text]').element.value).toBe('-');
	});
});
