import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import ManageInputCheckIn from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputCheckIn.vue';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const store = {
	modules: {
		tenantProfile: {
			state: {
				statusPage: '',
				tenantBill: {
					checkInDate: ''
				}
			},
			mutations: {
				setCheckInDate: (state, payload) => {
					state.tenantBill.checkInDate = payload;
				}
			}
		}
	}
};

const datePickerShowCalendar = jest.fn();

const stubs = {
	Datepicker: {
		template: '<input type="date"/>',
		methods: {
			showCalendar: datePickerShowCalendar
		}
	}
};

describe('ManageInputCheckIn.vue', () => {
	const wrapper = shallowMount(ManageInputCheckIn, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render properly', () => {
		expect(wrapper.find('.form-group').exists()).toBe(true);
		expect(wrapper.find('.input-mask').exists()).toBe(true);
		expect(wrapper.findComponent(stubs.Datepicker).exists()).toBe(true);
	});

	it('should disable datepicker properly', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'acceptBooking';
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isDisabledCheckIn).toBe(true);
		expect(
			wrapper.findComponent(stubs.Datepicker).attributes('disabled')
		).toBeTruthy();

		wrapper.vm.$store.state.tenantProfile.statusPage = '';
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isDisabledCheckIn).toBe(false);
		expect(
			wrapper.findComponent(stubs.Datepicker).attributes('disabled')
		).toBeFalsy();
	});

	it('should set date and validate properly', async () => {
		jest.useFakeTimers();
		await wrapper.setData({ dateSelected: '2020/02/02' });
		jest.advanceTimersByTime(2000);
		jest.runAllTimers();
		await flushPromises();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isError).toBe(false);
		expect(wrapper.vm.dateSelected).toBe('2020/02/02');

		await wrapper.setData({ dateSelected: '' });
		jest.advanceTimersByTime(2000);
		jest.runAllTimers();
		await flushPromises();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isError).toBe(true);

		global.validator.validate = jest.fn().mockRejectedValue('error');
		wrapper.setData({ isError: false });

		await wrapper.setData({ dateSelected: '2020/01/01' });
		jest.advanceTimersByTime(2000);
		jest.runAllTimers();
		await flushPromises();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isError).toBe(false);

		jest.clearAllTimers();
	});

	it('should set date properly', () => {
		const selected = dayjs('2020/02/02').format('dddd, D MMMM YYYY');
		const dateSelected = dayjs('2020/02/02').format('YYYY-MM-DD');

		wrapper.vm.formatDate('2020/02/02');
		expect(wrapper.vm.selected).toBe(selected);
		expect(wrapper.vm.dateSelected).toBe(dateSelected);
	});
});
