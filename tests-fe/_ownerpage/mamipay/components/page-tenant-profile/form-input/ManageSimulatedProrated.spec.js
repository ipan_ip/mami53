import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageSimulatedProrated from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageSimulatedProrated.vue';
import Vuex from 'vuex';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					fixedBilling: true,
					checkInDate: '2020/02/02',
					proratedDate: '2020/01/01'
				},
				statusPage: 'member'
			}
		}
	}
};

describe('ManageSimulatedProrated.vue', () => {
	const wrapper = shallowMount(ManageSimulatedProrated, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render wrapper simulation properly', () => {
		expect(wrapper.find('.wrapper-simulated').exists()).toBe(true);
	});

	it('should returning first checkin date properly', async () => {
		const boxSimulated = index => wrapper.findAll('.box-simulated').at(index);

		wrapper.vm.$store.state.tenantProfile.tenantBill.fixedBilling = false;
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		await wrapper.vm.$nextTick();

		expect(boxSimulated(0).text()).toBe(
			dayjs('2020/02/02').format('D MMMM YYYY')
		);
		expect(boxSimulated(1).text()).toBe('2020/01/01');

		wrapper.vm.$store.state.tenantProfile.tenantBill.fixedBilling = false;
		wrapper.vm.$store.state.tenantProfile.statusPage = 'new-member';
		await wrapper.vm.$nextTick();

		expect(boxSimulated(0).text()).toBe('');
		expect(boxSimulated(1).text()).toBe('2020/01/01');
	});
});
