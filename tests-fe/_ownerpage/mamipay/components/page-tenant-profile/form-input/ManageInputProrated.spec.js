import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputProrated from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputProrated.vue';
import Vuex from 'vuex';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					checkInDate: '',
					rentCount: 'month',
					dateBill: 0,
					discountRentPrice: 20000,
					rentPrice: 15000,
					prorated: 13000
				}
			},
			mutations: {
				setProrated: (state, payload) => {
					state.tenantBill.prorated = payload;
				},
				setRealProrated: jest.fn(),
				setProratedDate: jest.fn()
			}
		}
	}
};

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const axios = {
	post: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: {
				prorata_amount: 18000,
				prorata_amount_to_owner: 17000,
				end_date: '2020/01/01'
			}
		}
	})
};
global.axios = axios;

const mocks = {
	swalError: jest.fn()
};

describe('ManageInputProrated.vue', () => {
	const wrapper = shallowMount(ManageInputProrated, {
		localVue,
		mocks,
		store: new Vuex.Store(store)
	});

	it('should render input prorated', () => {
		expect(wrapper.find('#filterPriceMin').exists()).toBe(true);
	});

	it('should set prorated properly', () => {
		wrapper.setData({ inputProrated: 9000 });

		expect(wrapper.vm.inputProrated).toBe(9000);
	});

	it('should return prorated nominal properly', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		const setTenantBill = (key, val) => {
			wrapper.vm.$store.state.tenantProfile.tenantBill[key] = val;
		};

		setTenantBill('checkInDate', '2020/02/02');
		setTenantBill('rentCount', 'month');
		setTenantBill('dateBill', 2);

		await flushPromises();

		expect(spyCommit).toBeCalledWith('setRealProrated', 18000);
		expect(spyCommit).toBeCalledWith('setProrated', 17000);
		expect(spyCommit).toBeCalledWith('setProratedDate', '2020/01/01');
		expect(wrapper.vm.proratedNominal).toBe(17000);

		spyCommit.mockRestore();
	});

	it('should handle post calculate prorate properly', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		// status false
		const axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};
		global.axios = axios;
		wrapper.vm.proratedCalculation(1000, 2000);
		await flushPromises();

		expect(axios.post).toBeCalledWith(
			'/calculate-prorata',
			expect.objectContaining({
				amount: 1000,
				original_amount: 2000
			})
		);
		expect(spyCommit).not.toBeCalled();

		// error
		global.axios = {
			post: jest.fn().mockRejectedValue('error')
		};
		wrapper.vm.proratedCalculation(1000, 2000);
		await flushPromises();

		expect(mocks.swalError).toBeCalled();
		expect(global.bugsnagClient.notify).toBeCalled();
	});
});
