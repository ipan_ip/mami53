import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputCostFine from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputCostFine.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: '',
			tenantBill: {
				fixedBilling: 500000,
				costFine: 20000
			}
		}
	},
	mutations: {
		setCostFine: jest.fn()
	}
};

const ManageCurrencyInput = { template: '<input type="tel" />' };

describe('ManageInputCostFine.vue', () => {
	const wrapper = shallowMount(ManageInputCostFine, {
		localVue,
		store: new Vuex.Store(store),
		stubs: { ManageCurrencyInput }
	});

	it('should render form input', () => {
		const isExist = selector => wrapper.find(selector).exists();

		expect(isExist('.form-group')).toBe(true);
		expect(isExist('.form-group > label')).toBe(true);
		expect(isExist('#inputCostFine')).toBe(true);
	});

	it('should set cost fine properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.findComponent(ManageCurrencyInput).vm.$emit('input', 123);

		expect(spyCommit).toBeCalledWith('setCostFine', 123);
		spyCommit.mockRestore();
	});

	it('should return fixed billing from state', () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.fixedBilling = 900000;

		expect(wrapper.vm.fixedBilling).toBe(900000);
	});
});
