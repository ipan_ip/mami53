import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputRoomNumber from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputRoomNumber.vue';

const localVue = createLocalVue();

const $store = {
	state: {
		allDetailRoom: {
			room_available: 2
		},
		tenantProfile: {
			statusPage: 'not-member',
			tenantProfile: {
				roomNumber: 'Kamar 123',
				roomAllotment: { id: null }
			}
		},
		userRoom: {
			id: 5
		}
	},
	commit: jest.fn(function(commit, val) {
		if (commit === 'setRoomNumber') {
			this.state.tenantProfile.tenantProfile.roomNumber = val;
		} else if (commit === 'setRoomAvailability') {
			this.state.allDetailRoom.room_available = val;
		}
	})
};

const $route = {
	name: 'biodataTenant'
};

const RoomAllotmentWrapper = {
	template: '<div />'
};

const $toasted = { show: jest.fn() };

describe('ManageInputRoomNumber.vue', () => {
	const wrapper = shallowMount(ManageInputRoomNumber, {
		localVue,
		stubs: { RoomAllotmentWrapper },
		mocks: { $route, $store, $toasted }
	});

	it('should render room available info', () => {
		expect(wrapper.find('.room-available').exists()).toBe(true);
	});

	it('should set room allotment modal to true if input container clicked', async () => {
		await wrapper.find('.room-number-container').trigger('click');
		expect(wrapper.vm.showRoomAllotmentModal).toBe(true);
	});

	it('should not show room number if no room number is selected', () => {
		wrapper.vm.$store.state.tenantProfile.tenantProfile.roomAllotment = {
			id: null,
			name: '',
			floor: ''
		};
		expect(wrapper.vm.roomNumberLabel).toBe('');
	});

	it('should show correct room number label', async () => {
		const mockRoomAllotmentData = {
			name: 'Mami 1',
			floor: 'Lantai 2'
		};
		wrapper.vm.$store.state.tenantProfile.tenantProfile.roomAllotment = {
			...mockRoomAllotmentData
		};
		expect(wrapper.vm.roomNumberLabel).toBe(
			`${mockRoomAllotmentData.name} ${mockRoomAllotmentData.floor}`
		);
	});

	it('should update room available after room loaded', () => {
		wrapper.findComponent(RoomAllotmentWrapper).vm.$emit('room-loaded', 4);
		expect(wrapper.vm.roomAvailable).toBe(4);
	});

	it('should show toast if input is select on location', () => {
		const spyToasted = jest.spyOn(wrapper.vm.$toasted, 'show');
		wrapper.findComponent(RoomAllotmentWrapper).vm.$emit('select-on-location');
		expect(spyToasted).toBeCalled();
		spyToasted.mockRestore();
	});
});
