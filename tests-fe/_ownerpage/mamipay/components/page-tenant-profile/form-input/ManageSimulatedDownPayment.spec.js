import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageSimulatedDownPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageSimulatedDownPayment.vue';
import Vuex from 'vuex';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					downPayment: 5000,
					checkInDate: '2020-02-02'
				}
			},
			mutations: {
				setDownPaymentSettlementDate: jest.fn(),
				setDownPaymentDate: jest.fn()
			}
		}
	}
};

describe('ManageSimulatedDownPayment.vue', () => {
	const wrapper = shallowMount(ManageSimulatedDownPayment, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render properly', () => {
		expect(wrapper.find('.form-group').exists()).toBe(true);
		expect(wrapper.find('.wrapper-simulated').exists()).toBe(true);
		expect(wrapper.findAll('.box-simulated').length).toBe(2);
	});

	it('should render checkin date properly', () => {
		const firstDp = wrapper
			.findAll('.box-simulated')
			.at(1)
			.text();

		expect(firstDp).toBe(dayjs('2020-02-02').format('D MMMM YYYY'));
	});

	it('should render first date dp properly', async () => {
		const nextWeek = dayjs()
			.add(7, 'day')
			.format('D MMMM YYYY');

		const tomorrow = dayjs()
			.add(1, 'day')
			.format('D MMMM YYYY');

		const today = dayjs().format('D MMMM YYYY');

		wrapper.vm.$store.state.tenantProfile.tenantBill.checkInDate = nextWeek;
		await wrapper.vm.$nextTick();

		const firstDp = () =>
			wrapper
				.findAll('.box-simulated')
				.at(0)
				.text();

		expect(firstDp()).toBe(dayjs(tomorrow).format('D MMMM YYYY'));

		wrapper.vm.$store.state.tenantProfile.tenantBill.checkInDate = today;
		await wrapper.vm.$nextTick();

		expect(firstDp()).toBe(dayjs(today).format('D MMMM YYYY'));
	});

	it('should handle empty checkin date properly', async () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.downPayment = 0;
		wrapper.vm.$store.state.tenantProfile.tenantBill.checkInDate = '';

		await wrapper.vm.$nextTick();

		const boxSimulated = index =>
			wrapper
				.findAll('.box-simulated')
				.at(index)
				.text();

		expect(boxSimulated(0)).toBe('');
		expect(boxSimulated(1)).toBe('');
	});
});
