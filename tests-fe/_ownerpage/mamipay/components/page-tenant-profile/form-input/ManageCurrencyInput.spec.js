import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageCurrencyInput from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageCurrencyInput.vue';

const localVue = createLocalVue();

describe('ManageCurrencyInput.vue', () => {
	describe('disabled input', () => {
		const wrapper = shallowMount(ManageCurrencyInput, {
			localVue,
			propsData: { disabled: true }
		});

		it('should do nothing when input clicked', () => {
			wrapper.find('input[type="tel"]').trigger('click');
			expect(wrapper.vm.isFocused).toBe(false);
		});
	});

	describe('blured input', () => {
		const wrapper = shallowMount(ManageCurrencyInput, { localVue });
		it('should render input type tel when it is not selected', () => {
			expect(wrapper.find('input[type="tel"]').exists()).toBe(true);
		});
	});

	describe('focused input', () => {
		let wrapper;
		const inputNumber = () => wrapper.find('input[type="tel"]');
		beforeEach(() => {
			wrapper = shallowMount(ManageCurrencyInput, {
				localVue,
				propsData: { isFocused: true }
			});
		});

		it('should emit input value', async () => {
			const inputValue = 9000;
			inputNumber().setValue(inputValue);
			await wrapper.vm.$nextTick();
			const emitInput = wrapper.emitted('input');
			expect(emitInput[emitInput.length - 1]).toEqual([inputValue]);
		});

		it.skip('should mask input value and emit amount value when blured', async () => {
			const inputValue = 9000;
			inputNumber().setValue(inputValue);
			inputNumber().trigger('blur');
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.maskedAmount).toBe('Rp 9.000');
			expect(wrapper.find('input[type="tel"').exists()).toBe(true);
			const emitBlur = wrapper.emitted('input');
			expect(emitBlur[emitBlur.length - 1]).toEqual([inputValue]);
		});
	});
});
