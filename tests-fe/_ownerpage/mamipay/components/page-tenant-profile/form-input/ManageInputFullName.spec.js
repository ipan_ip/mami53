import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import Vuex, { Store } from 'vuex';
import { flushPromises } from 'tests-fe/utils/promises';
import ManageInputFullName from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputFullName.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

const store = {
	state: {
		tenantProfile: {
			tenantProfile: {
				fullName: ''
			}
		}
	},
	mutations: {
		setFullName: (state, payload) => {
			state.tenantProfile.tenantProfile.fullName = payload;
		}
	}
};

global.bugsnagClient = { notify: jest.fn() };

describe('ManageInputFullName.vue', () => {
	const wrapper = shallowMount(ManageInputFullName, {
		localVue,
		store: new Store(store)
	});

	it('should render input properly', () => {
		expect(wrapper.find('#inputTenantName').exists()).toBeTruthy();
	});

	it('should set isError to true if value is not valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputTenantName');
		input.element.value = 'hello';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeTruthy();
		jest.clearAllTimers();
	});

	it('should set isError to false if value validation is valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputTenantName');
		input.element.value = 'Bambang Budiman';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeFalsy();
		jest.clearAllTimers();
	});

	it('should not set isError if there is error on debounce', async () => {
		global.validator.validate = jest.fn().mockRejectedValue('error');
		wrapper.setData({ isError: false });
		jest.useFakeTimers();

		const input = wrapper.find('#inputTenantName');
		input.element.value = 'error';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBe(false);
	});
});
