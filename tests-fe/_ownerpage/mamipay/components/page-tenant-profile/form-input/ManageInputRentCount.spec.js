import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputRentCount from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputRentCount.vue';
import Vuex from 'vuex';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				unavailableRent: [],
				statusPage: '',
				roomSelectedDetail: {
					rentCountList: [
						{ value: 'week', label: 'Per Minggu' },
						{ value: 'month', label: 'Per Bulan' },
						{ value: '3_month', label: 'Per 3 Bulan' },
						{ value: '6_month', label: 'Per 6 Bulan' },
						{ value: 'year', label: 'Per Tahun' }
					]
				},
				tenantBill: {
					rentCount: 'month',
					rentDuration: 3,
					otherCost: []
				},
				tenantProfile: {
					roomId: 123,
					fullName: 'xxxx'
				}
			},
			mutations: {
				setRentCount: (state, payload) => {
					state.tenantBill.rentCount = payload;
				},
				setRentDuration: (state, payload) => {
					state.tenantBill.rentDuration = payload;
				}
			}
		},
		userRoomStatus: {
			state: {
				room: {
					original_price: {
						price_yearly: 12000000,
						price_monthly: 1000000,
						price_quarterly: 3000000,
						price_semiannualy: 6000000,
						price_weekly: 250000
					}
				}
			}
		}
	},
	mutations: {
		setRentDurationList: jest.fn(),
		setRentPrice: jest.fn(),
		setLoadingState: jest.fn()
	}
};

const axios = {
	get: jest.fn().mockRejectedValue(new Error('error'))
};

global.axios = axios;

const bugsnagClient = { notify: jest.fn() };

global.bugsnagClient = bugsnagClient;

const mocks = {
	$route: { name: '' },
	openSwalLoading: jest.fn(),
	swalError: jest.fn()
};

describe('ManageInputRentCount.vue', () => {
	const wrapper = shallowMount(ManageInputRentCount, {
		localVue,
		mocks,
		store: new Vuex.Store(store)
	});

	it('should render input', () => {
		expect(wrapper.find('#inputStatus').exists()).toBe(true);
	});

	it('should show error when selected rent count is not available', async () => {
		wrapper.vm.$store.state.tenantProfile.unavailableRent = ['3_month'];
		wrapper.vm.rentCount = '3_month';
		await wrapper.vm.$nextTick();

		expect(mocks.swalError).toBeCalled();
		expect(wrapper.vm.rentCount).toBe('');
	});

	it('should return rent duration from state', () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.rentDuration = 10;

		expect(wrapper.vm.rentDuration).toBe(10);
	});

	it('should set rent price properly', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.vm.$store.state.tenantProfile.unavailableRent = [];
		await wrapper.vm.$nextTick();

		const setRentCountSelected = async type => {
			const index = wrapper.vm.$store.state.tenantProfile.roomSelectedDetail.rentCountList.findIndex(
				rentCount => rentCount.value === type
			);

			const option = wrapper.findAll('select > option').at(index + 1);
			return await option.setSelected();
		};

		await setRentCountSelected('week');
		expect(spyCommit).toBeCalledWith('setRentPrice', 250000);

		await setRentCountSelected('month');
		expect(spyCommit).toBeCalledWith('setRentPrice', 1000000);

		await setRentCountSelected('3_month');
		expect(spyCommit).toBeCalledWith('setRentPrice', 3000000);

		await setRentCountSelected('6_month');
		expect(spyCommit).toBeCalledWith('setRentPrice', 6000000);

		await setRentCountSelected('year');
		expect(spyCommit).toBeCalledWith('setRentPrice', 12000000);

		spyCommit.mockRestore();
	});
});
