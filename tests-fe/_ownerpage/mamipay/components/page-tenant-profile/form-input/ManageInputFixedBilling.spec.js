import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputFixedBilling from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputFixedBilling.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	ToggleButton: { template: '<input type="checkbox" />' }
};

const store = {
	modules: {
		tenantProfile: {
			state: {
				statusPage: '',
				tenantBill: {
					fixedBilling: 20000
				}
			},
			mutations: {
				setFixedBilling: (state, payload) => {
					state.tenantBill.fixedBilling = payload;
				},
				setDateBill: jest.fn(),
				setProrated: jest.fn(),
				setRealProrated: jest.fn(),
				setProratedDate: jest.fn()
			}
		}
	}
};

describe('ManageInputFixedBilling.vue', () => {
	const wrapper = shallowMount(ManageInputFixedBilling, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render form input fixed billing', () => {
		expect(wrapper.find('#formInputFixedBilling').exists()).toBe(true);
	});

	it('should set data when fixed billing changed', () => {
		wrapper.findComponent(stubs.ToggleButton).vm.$emit('input', 22222);

		expect(wrapper.vm.inputFixedBilling).toBe(22222);
	});

	it('should reset all data when fixed billing changed to 0', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.findComponent(stubs.ToggleButton).vm.$emit('input', 0);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputFixedBilling).toBe(0);
		expect(spyCommit).toBeCalledWith('setFixedBilling', 0);
		expect(spyCommit).toBeCalledWith('setDateBill', 0);
		expect(spyCommit).toBeCalledWith('setProrated', 0);
		expect(spyCommit).toBeCalledWith('setRealProrated', 0);
		expect(spyCommit).toBeCalledWith('setProratedDate', '');
		spyCommit.mockRestore();
	});

	it('should not reset all data when fixed billing changed to 0 but page is  member', async () => {
		jest.clearAllMocks();
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.findComponent(stubs.ToggleButton).vm.$emit('input', 123);
		await wrapper.vm.$nextTick();

		jest.clearAllMocks();

		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		await wrapper.vm.$nextTick();

		wrapper.findComponent(stubs.ToggleButton).vm.$emit('input', 0);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputFixedBilling).toBe(0);
		expect(spyCommit).toBeCalledWith('setFixedBilling', 0);
		expect(spyCommit).not.toBeCalledWith('setDateBill', expect.anything());
		expect(spyCommit).not.toBeCalledWith('setProrated', expect.anything());
		expect(spyCommit).not.toBeCalledWith('setRealProrated', expect.anything());
		expect(spyCommit).not.toBeCalledWith('setProratedDate', expect.anything());
		spyCommit.mockRestore();
	});
});
