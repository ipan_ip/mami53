import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputCostDeposit from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputCostDeposit.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: '',
			tenantBill: {
				fixedBilling: 500000,
				costDeposit: 20000
			}
		}
	},
	mutations: {
		setCostDeposit: jest.fn()
	}
};

const ManageCurrencyInput = { template: '<input type="tel" />' };

describe('ManageInputCostDeposit.vue', () => {
	const wrapper = shallowMount(ManageInputCostDeposit, {
		localVue,
		store: new Vuex.Store(store),
		stubs: { ManageCurrencyInput }
	});

	it('should render form input', () => {
		const isExist = selector => wrapper.find(selector).exists();

		expect(isExist('.form-group')).toBe(true);
		expect(isExist('.form-group > label')).toBe(true);
		expect(isExist('#inputCostDeposit')).toBe(true);
	});

	it('should set disabled properly', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isDisabledCostDeposit).toBe(true);
		expect(
			wrapper.find('#inputCostDeposit').attributes('disabled')
		).not.toBeFalsy();
	});

	it('should set cost deposit properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.findComponent(ManageCurrencyInput).vm.$emit('input', 123);

		expect(spyCommit).toBeCalledWith('setCostDeposit', 123);
	});

	it('should return fixed billing from state', () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.fixedBilling = 900000;

		expect(wrapper.vm.fixedBilling).toBe(900000);
	});
});
