import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageInputOtherCostPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputOtherCostPayment.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

mockVLazy(localVue);

const axios = {
	delete: jest.fn(() => {
		return Promise.resolve({});
	})
};
global.axios = axios;

const openSwalLoading = jest.fn();
const swalError = jest.fn();

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const $toasted = {
	show: jest.fn()
};

const mockStore = {
	state: {
		tenantProfile: {
			otherBill: [],
			tenantBill: {
				otherCost: []
			}
		}
	},
	mutations: {
		setOtherBill(state, payload) {
			state.otherBill = payload;
		}
	}
};

describe('ManageOwnerListNavigation.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageInputOtherCostPayment, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				statusInvoice: 'string',
				invoiceId: 0,
				invoiceNumber: 'string'
			},
			mocks: { $toasted },
			methods: { openSwalLoading, swalError }
		});
	});

	it('Should add new other bill payment', () => {
		expect(wrapper.vm.otherBill.length).toBe(0);
		wrapper.vm.addOtherBill();
		expect(wrapper.vm.otherBill.length).toBe(1);
	});

	it('Should set fixed to all other bill', () => {
		wrapper.vm.isApplyAllCost = true;
		wrapper.vm.checkBoxControl();
		expect(wrapper.vm.otherBill[0].type).toBe('fixed');
	});

	it('Should delete one other bill', () => {
		expect(wrapper.vm.otherBill.length).toBe(1);
		wrapper.vm.deleteOtherBillModal(0, 1, true);
		expect(wrapper.vm.otherBill.length).toBe(0);
	});

	it('Should no error when other bill value is not null', async () => {
		await wrapper.vm.addOtherBill();
		expect(wrapper.vm.isInputFieldValueError(0)).toBeFalsy();
	});
});
