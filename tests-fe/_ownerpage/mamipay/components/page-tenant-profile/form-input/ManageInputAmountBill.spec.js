import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageInputAmountBill from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputAmountBill.vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const bugsnagClient = {
	notify: jest.fn()
};

global.bugsnagClient = bugsnagClient;

const openSwalLoading = jest.fn();
const getContractBill = jest.fn();
const swalError = jest.fn();
const $toasted = {
	show: jest.fn()
};

const localVue = createLocalVue();
localVue.use(Vuex);

const mockStore = {
	state: {
		tenantProfile: {
			otherBill: [],
			tenantBill: {
				otherCost: []
			}
		}
	},
	mutations: {
		setOtherBill(state, payload) {
			state.otherBill = payload;
		}
	}
};

describe('ManageInputAmountBill.vue', () => {
	let wrapper;

	global.axios = mockAxios;

	beforeEach(() => {
		wrapper = shallowMount(ManageInputAmountBill, {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				statusInvoice: 'string',
				invoiceId: 0,
				invoiceNumber: 'string'
			},
			mocks: {
				$dayjs: jest.fn(() => ({ format: jest.fn() })),
				$toasted
			},
			methods: { openSwalLoading, swalError, getContractBill }
		});
	});

	it('Should show toasted if success update amount price', async () => {
		axios.mockResolve({
			data: {
				status: true
			}
		});

		wrapper.vm.updateAmountPrice();
		await wrapper.vm.$nextTick;
		expect(openSwalLoading).toBeCalled();
	});

	it('Should show toasted if false update amount price', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});

		await wrapper.vm.updateAmountPrice();
		expect(openSwalLoading).toBeCalled();
	});

	it('should show toasted message', () => {
		wrapper.vm.toastedAlert('Test');
		expect($toasted.show).toBeCalledWith('Test', {
			type: 'info',
			className: 'toasted-default',
			theme: 'bubble',
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('Should have eventBus updateInvoiceData and isModalAdditionalPriceClose ', () => {
		expect(global.EventBus.$on).toBeCalledWith(
			'isUpdateAmountBill',
			expect.any(Function)
		);

		expect(global.EventBus.$on).toBeCalledWith(
			'isSaveToOtherBillCheckbox',
			expect.any(Function)
		);
	});
});
