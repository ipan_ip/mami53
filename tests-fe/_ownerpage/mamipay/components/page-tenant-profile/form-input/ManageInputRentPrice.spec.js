import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputRentPrice from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputRentPrice.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				widgetInfo: null,
				tenantBill: {
					rentCount: 'month'
				}
			},
			mutations: {
				setRentPrice: jest.fn(),
				setDiscountRentPrice: jest.fn()
			}
		},
		userRoom: {
			state: {
				price_yearly: 1200,
				price_monthly: 100,
				price_quarterly: 300,
				price_semiannualy: 600,
				price_weekly: 25,
				original_price: {
					price_yearly: 1212,
					price_monthly: 111,
					price_quarterly: 333,
					price_semiannualy: 666,
					price_weekly: 77
				}
			}
		}
	}
};

const stubs = {
	VueNumeric: { template: '<input type="tel" />' }
};

describe('ManageInputRentPrice.vue', () => {
	const wrapper = shallowMount(ManageInputRentPrice, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render properly', () => {
		expect(wrapper.find('.form-group').exists()).toBe(true);
		expect(wrapper.find('.form-group > label').exists()).toBe(true);
		expect(wrapper.find('.form-group > .form-control').exists()).toBe(true);
	});

	it('should return price properly', async () => {
		const setRentCount = rentCount => {
			wrapper.vm.$store.state.tenantProfile.tenantBill.rentCount = rentCount;
			return wrapper.vm.$nextTick();
		};
		const rentPrice = () =>
			parseInt(wrapper.findComponent(stubs.VueNumeric).attributes('value'));

		await setRentCount('month');
		expect(rentPrice()).toBe(111);

		await setRentCount('3_month');
		expect(rentPrice()).toBe(333);

		await setRentCount('6_month');
		expect(rentPrice()).toBe(666);

		await setRentCount('year');
		expect(rentPrice()).toBe(1212);

		await setRentCount('week');
		expect(rentPrice()).toBe(77);
	});

	it('should return price properly when flash sale', async () => {
		wrapper.vm.$store.state.tenantProfile.widgetInfo = {
			isFlashSaleContract: true
		};

		const setRentCount = rentCount => {
			wrapper.vm.$store.state.tenantProfile.tenantBill.rentCount = rentCount;
			return wrapper.vm.$nextTick();
		};
		const rentPrice = () =>
			parseInt(wrapper.findComponent(stubs.VueNumeric).attributes('value'));

		await setRentCount('month');
		expect(rentPrice()).toBe(100);

		await setRentCount('3_month');
		expect(rentPrice()).toBe(300);

		await setRentCount('6_month');
		expect(rentPrice()).toBe(600);

		await setRentCount('year');
		expect(rentPrice()).toBe(1200);

		await setRentCount('week');
	});
});
