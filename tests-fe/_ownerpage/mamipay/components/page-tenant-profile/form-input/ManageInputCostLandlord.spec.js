import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputCostLandlord from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputCostLandlord.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: '',
			tenantBill: {
				realProrated: 0,
				rentCount: ''
			}
		},
		userRoom: {
			original_price: {
				price_yearly: 12000000,
				price_monthly: 1000000,
				price_quarterly: 3000000,
				price_semiannualy: 6000000,
				price_weekly: 250000
			}
		}
	},
	mutations: {
		setRentPrice: jest.fn()
	}
};

const VueNumeric = { template: '<input type="tel" />' };

describe('ManageInputCostLandlord.vue', () => {
	const wrapper = shallowMount(ManageInputCostLandlord, {
		localVue,
		store: new Vuex.Store(store),
		stubs: { VueNumeric }
	});

	it('should render form input', () => {
		const isExist = selector => wrapper.find(selector).exists();

		expect(isExist('.form-group')).toBe(true);
		expect(isExist('.form-group > label')).toBe(true);
		expect(isExist('#filterPriceMin')).toBe(true);
	});

	it('should return price properly', async () => {
		const setRentCount = async type => {
			wrapper.vm.$store.state.tenantProfile.tenantBill.rentCount = type;
			return await wrapper.vm.$nextTick();
		};

		const inputValue = () =>
			wrapper.findComponent(VueNumeric).attributes('value');

		await setRentCount('year');
		expect(inputValue()).toBe('12000000');

		await setRentCount('month');
		expect(inputValue()).toBe('1000000');

		await setRentCount('3_month');
		expect(inputValue()).toBe('3000000');

		await setRentCount('6_month');
		expect(inputValue()).toBe('6000000');

		await setRentCount('week');
		expect(inputValue()).toBe('250000');

		await setRentCount('day');
		expect(inputValue()).toBe('0');
	});

	it('should return prorated price instead when the value is given', async () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.realProrated = 9999;

		await wrapper.vm.$nextTick();
		expect(wrapper.findComponent(VueNumeric).attributes('value')).toBe('9999');
	});

	it('should return 0 if no original price room', async () => {
		wrapper.vm.$store.state.userRoom.original_price = null;
		wrapper.vm.$store.state.tenantProfile.tenantBill.realProrated = 0;

		await wrapper.vm.$nextTick();
		expect(wrapper.findComponent(VueNumeric).attributes('value')).toBe('0');
	});
});
