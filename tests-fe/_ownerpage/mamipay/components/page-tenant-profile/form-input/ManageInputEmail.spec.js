import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import Vuex, { Store } from 'vuex';
import { flushPromises } from 'tests-fe/utils/promises';
import ManageInputEmail from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputEmail.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

const store = {
	state: {
		tenantProfile: {
			tenantProfile: {
				email: ''
			}
		}
	},
	mutations: {
		setEmail: (state, payload) => {
			state.tenantProfile.tenantProfile.email = payload;
		}
	}
};

global.bugsnagClient = { notify: jest.fn() };

describe('ManageInputEmail.vue', () => {
	const wrapper = shallowMount(ManageInputEmail, {
		localVue,
		store: new Store(store)
	});

	it('should render input type email', () => {
		expect(wrapper.find('input[type=email]').exists()).toBe(true);
	});

	it('should set isError to true if email is not valid', async () => {
		jest.useFakeTimers();
		const emailInput = wrapper.find('input[type=email]');
		emailInput.element.value = 'notvalidemail';
		await emailInput.trigger('input');
		jest.runAllTimers();
		await flushPromises();
		expect(wrapper.vm.isError).toBe(true);
		jest.clearAllTimers();
	});

	it('should set isError to false if email validation is valid', async () => {
		jest.useFakeTimers();
		const emailInput = wrapper.find('input[type=email]');
		emailInput.element.value = 'email@mamiteam.com';
		await emailInput.trigger('input');
		jest.runAllTimers();
		await flushPromises();
		expect(wrapper.vm.isError).toBe(false);
		jest.clearAllTimers();
	});

	it('should not set isError if there is error on debounce', async () => {
		global.validator.validate = jest.fn().mockRejectedValue('error');
		wrapper.setData({ isError: false });
		jest.useFakeTimers();
		const emailInput = wrapper.find('input[type=email]');
		emailInput.element.value = 'error';
		await emailInput.trigger('input');
		jest.runAllTimers();
		await flushPromises();
		expect(wrapper.vm.isError).toBe(false);
	});
});
