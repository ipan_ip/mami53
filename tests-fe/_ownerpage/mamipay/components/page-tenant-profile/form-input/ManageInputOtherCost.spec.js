import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputOtherCost from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputOtherCost.vue';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const axios = {
	delete: jest
		.fn()
		.mockResolvedValueOnce({ data: { status: true } })
		.mockResolvedValueOnce({ data: { status: false } })
		.mockRejectedValueOnce(new Error('error'))
};
global.axios = axios;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const store = () => {
	return {
		modules: {
			tenantProfile: {
				state: {
					tenantBill: {
						otherCost: [],
						forOtherTenant: false
					}
				},
				mutations: {
					setOtherCost(state, payload) {
						state.tenantBill.otherCost = payload;
					},
					setForOtherTenant(state, payload) {
						state.tenantBill.forOtherTenant = payload;
					}
				}
			}
		}
	};
};

const mocks = {
	$toasted: { show: jest.fn() },
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn()
};

const stubs = {
	checkbox: {
		template: '<input class="checkbox-stub" type="checkbox" />',
		props: { type: String }
	},
	ManageCurrencyInput: {
		template: '<input class="manage-currency-input" type="input" />'
	}
};

describe('ManageInputOtherCost.vue', () => {
	let wrapper;
	let setOtherCost;

	beforeEach(() => {
		wrapper = shallowMount(ManageInputOtherCost, {
			localVue,
			mocks,
			stubs,
			store: new Vuex.Store(store())
		});

		setOtherCost = otherCost => {
			wrapper.vm.costData = otherCost;
		};
	});

	afterEach(() => {
		wrapper.vm.$destroy();
	});

	it('should render all form group', async () => {
		expect(wrapper.findAll('.form-group').length).toBe(0);

		setOtherCost([
			{
				field_title: 'other cost 1',
				field_value: 100000,
				id: 1
			}
		]);

		await wrapper.vm.$nextTick();

		expect(wrapper.findAll('.form-group').length).toBe(1);
	});

	it('should add new field when add button clicked and all costs are valid', async () => {
		setOtherCost([
			{
				field_title: 'other cost 1',
				field_value: 100000
			}
		]);

		await wrapper.vm.$nextTick();

		wrapper.find('.add-question').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('add')[0]).toBeTruthy();
		expect(wrapper.findAll('.form-group').length).toBe(2);
	});

	it('should not add new field when add button clicked and there is invalid cost', async () => {
		setOtherCost([
			{
				field_title: 'other cost 1',
				field_value: 0 // invalid
			}
		]);

		await wrapper.vm.$nextTick();

		wrapper.find('.add-question').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('add')).toBeFalsy();
		expect(wrapper.findAll('.form-group').length).toBe(1);
		expect(mocks.$toasted.show).toBeCalled();
	});

	it('should delete cost without calling API if no id is given', async () => {
		setOtherCost([
			{
				field_title: 'other cost 1',
				field_value: 40000
			}
		]);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.link-delete-question')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('delete')[0]).toBeTruthy();
		expect(wrapper.findAll('.form-group').length).toBe(0);
		expect(axios.delete).not.toBeCalled();
	});

	it('should delete cost by API if there is id on that cost', async () => {
		setOtherCost([
			{
				field_title: 'other cost 1',
				field_value: 40000,
				id: 123
			},
			{
				field_title: 'other cost 2',
				field_value: 50000,
				id: 1234
			},
			{
				field_title: 'other cost 3',
				field_value: 60000,
				id: 12345
			}
		]);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.link-delete-question')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(wrapper.emitted('delete')[0]).toBeTruthy();
		expect(wrapper.findAll('.form-group').length).toBe(2);
		expect(axios.delete).toBeCalledWith('/additional-cost-field/delete/123');

		// delete response status false
		wrapper
			.findAll('.link-delete-question')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(axios.delete).toBeCalledWith('/additional-cost-field/delete/1234');
		expect(wrapper.findAll('.form-group').length).toBe(2);

		// delete failed
		wrapper
			.findAll('.link-delete-question')
			.at(1)
			.trigger('click');

		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(axios.delete).toBeCalledWith('/additional-cost-field/delete/12345');
		expect(wrapper.findAll('.form-group').length).toBe(2);
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should set for other tenant properly', async () => {
		const checkboxStub = wrapper.findComponent(stubs.checkbox);
		checkboxStub.vm.$emit('input', true);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.forOtherTenant).toBe(true);
	});
});
