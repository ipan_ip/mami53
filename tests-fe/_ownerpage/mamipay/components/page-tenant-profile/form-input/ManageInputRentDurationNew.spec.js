import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputRentDurationNew from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputRentDurationNew.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					toggleRentDurationNew: true,
					newRentDuration: ''
				},
				roomSelectedDetail: {
					rentDurationList: ['1 Bulan', '2 Bulan']
				}
			},
			mutations: {
				setNewRentDuration: (state, payload) => {
					state.tenantBill.newRentDuration = payload;
				}
			}
		}
	}
};

describe('ManageInputRentDurationNew.vue', () => {
	const wrapper = shallowMount(ManageInputRentDurationNew, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render properly', () => {
		const isExists = selector => wrapper.find(selector).exists();

		expect(isExists('.form-group')).toBe(true);
		expect(isExists('.form-group > label')).toBe(true);
		expect(isExists('.form-group > select')).toBe(true);
	});

	it('should set rent duration properly', async () => {
		const option = wrapper.findAll('select > option').at(1);
		await option.setSelected();

		expect(wrapper.find('option:checked').element.value).toBe('1');
	});
});
