import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex, { Store } from 'vuex';
import 'tests-fe/utils/mock-debounce';
import ManageInputStatus from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputStatus.vue';
import VeeValidate, { Validator } from 'vee-validate';
import { flushPromises } from 'tests-fe/utils/promises';
import id from 'vee-validate/dist/locale/id';

const localVue = createLocalVue();

localVue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

global.bugsnagClient = { notify: jest.fn() };

const store = {
	state: {
		tenantProfile: {
			tenantProfile: {
				fullName: 'Mamipay team',
				status: null
			},
			statusPage: 'newMember'
		}
	},
	mutations: {
		setStatus(state, payload) {
			state.tenantProfile.tenantProfile.status = payload;
		}
	}
};

describe('ManageInputStatus.vue', () => {
	const wrapper = shallowMount(ManageInputStatus, {
		localVue,
		store: new Store(store)
	});
	it('should render properly', async () => {
		await wrapper.setData({
			label: 'Status'
		});

		expect(wrapper.find('label').text()).toBe('Status *');
	});

	it('should set computed data properly', async () => {
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.placeholderCondition).toBe('Pilih status');

		wrapper.vm.inputStatus = 'Belum Kawin';
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputStatus).toBe('Belum Kawin');
		expect(wrapper.vm.statusPage).toBe(store.state.tenantProfile.statusPage);
	});

	it('should set inputStatus properly', async () => {
		const input = wrapper.find('#inputStatus');
		await input.setValue('Kawin');

		expect(wrapper.vm.inputStatus).toBe('Kawin');
	});

	it('should set isError false if value valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputStatus');
		await input.setValue('Kawin');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeFalsy();
		expect(wrapper.vm.inputStatus).toBe('Kawin');
		jest.clearAllTimers();
	});

	it('should set isError true if value not valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputStatus');
		await input.setValue(null);

		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeTruthy();
		jest.clearAllTimers();
	});

	it('should not set isError if there is error on debounce', async () => {
		global.validator.validate = jest.fn().mockRejectedValue('error');
		wrapper.setData({ isError: false });
		jest.useFakeTimers();

		const input = wrapper.find('#inputStatus');
		await input.setValue(null);
		await input.trigger('change');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBeFalsy();
	});
});
