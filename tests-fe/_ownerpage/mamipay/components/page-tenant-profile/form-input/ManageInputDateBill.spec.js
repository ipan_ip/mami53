import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ManageInputDateBill from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputDateBill.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	state: {
		tenantProfile: {
			tenantBill: {
				checkInDate: '',
				fixedBilling: false,
				dateBill: 0
			}
		}
	},
	mutations: {
		setDateBill(state, payload) {
			state.tenantProfile.tenantBill.dateBill = payload;
		}
	}
});

const wrapper = shallowMount(ManageInputDateBill, {
	localVue,
	store
});

describe('ManageInputDateBill', () => {
	it('If checkInDate is null, it must fill with number 28', () => {
		const checkInDate = wrapper.vm.checkInDate;
		expect(checkInDate).toBe(28);
	});

	it('computed dateBill can set value', () => {
		wrapper.vm.dateBill = '30 Oktober 2019';
		const dateBill = wrapper.vm.dateBill;
		expect(dateBill).toBe('30 Oktober 2019');
	});
});
