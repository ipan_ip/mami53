import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputCostDownPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputCostDownPayment.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: '',
			tenantBill: {
				downPayment: true,
				costDownPayment: 5000,
				totalSimulatedCalculation: 5000,
				rentPrice: 0,
				discountRentPrice: 0
			}
		}
	},
	mutations: {
		setCostDownPayment: jest.fn()
	}
};

const ManageCurrencyInput = { template: '<input type="tel" />' };

describe('ManageInputCostDownPayment.vue', () => {
	const wrapper = shallowMount(ManageInputCostDownPayment, {
		localVue,
		store: new Vuex.Store(store),
		stubs: { ManageCurrencyInput }
	});

	const isExist = selector => wrapper.find(selector).exists();

	const expectValidationError = isError => {
		expect(wrapper.vm.validationInput).toBe(isError);
		expect(isExist('#filterPriceMin + p')).toBe(isError);
		expect(wrapper.find('#filterPriceMin').classes('input-error')).toBe(
			isError
		);
	};

	it('should render form input', () => {
		expect(isExist('.form-group')).toBe(true);
		expect(isExist('.form-group > label')).toBe(true);
		expect(isExist('#filterPriceMin')).toBe(true);
	});

	it('should set disabled properly', async () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.downPayment = false;

		await wrapper.vm.$nextTick();
		expect(
			wrapper.find('#filterPriceMin').attributes('disabled')
		).not.toBeFalsy();
	});

	it('should set cost dp properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.findComponent(ManageCurrencyInput).vm.$emit('input', 123);

		expect(spyCommit).toBeCalledWith('setCostDownPayment', 123);
		spyCommit.mockRestore();
	});

	it('should return validationInput true if is dp and dp is more than discount price', async () => {
		const setTenantBill = (state, val) => {
			wrapper.vm.$store.state.tenantProfile.tenantBill[state] = val;
		};

		setTenantBill('downPayment', true);
		setTenantBill('costDownPayment', 10000);

		await wrapper.vm.$nextTick();

		expectValidationError(true);
	});

	it('should return validationInput true if is dp and dp is more than discount total simulated calculation', async () => {
		const setTenantBill = (state, val) => {
			wrapper.vm.$store.state.tenantProfile.tenantBill[state] = val;
		};

		setTenantBill('downPayment', true);
		setTenantBill('costDownPayment', 5000);
		setTenantBill('discountRentPrice', 10000);
		setTenantBill('totalSimulatedCalculation', 4000);

		await wrapper.vm.$nextTick();

		expectValidationError(true);
	});

	it('should return validationInput true if is dp and dp is 0', async () => {
		const setTenantBill = (state, val) => {
			wrapper.vm.$store.state.tenantProfile.tenantBill[state] = val;
		};

		setTenantBill('downPayment', true);
		setTenantBill('costDownPayment', 0);

		await wrapper.vm.$nextTick();

		expectValidationError(true);
	});

	it('should return validationInput false if is not dp', async () => {
		const setTenantBill = (state, val) => {
			wrapper.vm.$store.state.tenantProfile.tenantBill[state] = val;
		};

		setTenantBill('downPayment', false);

		await wrapper.vm.$nextTick();

		expectValidationError(false);
	});

	it('should return rent price from state', () => {
		wrapper.vm.$store.state.tenantProfile.tenantBill.rentPrice = 900000;

		expect(wrapper.vm.rentPrice).toBe(900000);
	});
});
