import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import Vuex from 'vuex';
import { flushPromises } from 'tests-fe/utils/promises';
import ManageInputPhoneNumber from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputPhoneNumber.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantProfile: {
					phoneNumber: ''
				},
				newTenantNumberPhone: ''
			},
			mutations: {
				setPhoneNumber: (state, payload) => {
					state.tenantProfile.phoneNumber = payload;
				},
				setNewTenantNumberPhone: (state, payload) => {
					state.tenantProfile.newTenantNumberPhone = payload;
				}
			}
		}
	}
};

global.bugsnagClient = { notify: jest.fn() };

describe('ManageInputPhoneNumber.vue', () => {
	const wrapper = shallowMount(ManageInputPhoneNumber, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render inputPhoneNumber', () => {
		expect(wrapper.find('#inputPhoneNumber').exists()).toBe(true);
	});

	it('should set isError true when input is invalid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputPhoneNumber');
		input.element.value = '011111';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBe(true);
		jest.clearAllTimers();
	});

	it('should set isError false when input is valid', async () => {
		jest.useFakeTimers();

		const input = wrapper.find('#inputPhoneNumber');
		input.element.value = '082111222333';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBe(false);
		jest.clearAllTimers();
	});

	it('should not set isError if validator validate throws error', async () => {
		global.validator.validate = jest.fn().mockRejectedValue('error');
		await wrapper.setData({ isError: false });

		jest.useFakeTimers();

		const input = wrapper.find('#inputPhoneNumber');
		input.element.value = '082111222334';
		await input.trigger('input');
		jest.runAllTimers();
		await flushPromises();

		expect(wrapper.vm.isError).toBe(false);
		jest.clearAllTimers();
	});

	it('should set tenant new phone number properly', () => {
		jest.clearAllMocks();
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.setData({ newTenantPhoneNumber: '087222111222' });

		expect(wrapper.emitted('validation')[0]).toBeTruthy();
		expect(spyCommit).toBeCalledWith('setNewTenantNumberPhone', '087222111222');
	});

	it('should not use new tenant phone number if input phone number is exists on mounted', () => {
		store.modules.tenantProfile.state.tenantProfile.phoneNumber =
			'082222111222';
		store.modules.tenantProfile.state.newTenantNumberPhone = '082222111333';

		const wrapper = shallowMount(ManageInputPhoneNumber, {
			localVue,
			store: new Vuex.Store(store)
		});

		expect(wrapper.vm.inputPhoneNumber).toBe('082222111222');
	});
});
