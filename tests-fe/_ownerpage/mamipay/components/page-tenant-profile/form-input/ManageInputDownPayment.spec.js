import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputDownPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputDownPayment.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					downPayment: false
				}
			},
			mutations: {
				setDownPayment: (state, payload) => {
					state.tenantBill.downPayment = payload;
				},
				setCostDownPayment: jest.fn()
			}
		}
	}
};

const mocks = {
	$route: {
		params: {
			typeProfile: 'member'
		}
	}
};

const stubs = {
	ToggleButton: { template: '<div v-bind="$attrs"/>' }
};

describe('ManageInputDownPayment.vue', () => {
	const wrapper = shallowMount(ManageInputDownPayment, {
		localVue,
		mocks,
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render form input down payment', () => {
		expect(wrapper.find('#formInputDownPayment').exists()).toBe(true);
		expect(wrapper.find('.dp-disable-alert').exists()).toBe(true);
	});

	it('should toggle dp properly', () => {
		const toggleButton = () => wrapper.findComponent(stubs.ToggleButton);

		toggleButton().vm.$emit('change', { value: true });

		expect(wrapper.emitted('active')[0]).toBeTruthy();
		expect(wrapper.vm.inputDownPayment).toBe(true);

		toggleButton().vm.$emit('change', { value: false });

		expect(wrapper.vm.isModal).toBe(true);
		expect(wrapper.vm.inputDownPayment).toBe(false);
	});

	it('should set cancel deactivate dp properly', () => {
		const deactivateModalCancelBtn = wrapper.find(
			'.dp-disable-alert .btn-success'
		);
		deactivateModalCancelBtn.trigger('click');

		expect(wrapper.vm.inputDownPayment).toBe(true);
		expect(wrapper.vm.isModal).toBe(false);
	});

	it('should deactivate dp properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		const deactivateModalConfirmBtn = wrapper.find(
			'.dp-disable-alert .btn-primary'
		);
		deactivateModalConfirmBtn.trigger('click');

		expect(wrapper.vm.inputDownPayment).toBe(false);
		expect(wrapper.vm.isModal).toBe(false);
		expect(spyCommit).toBeCalledWith('setCostDownPayment', 0);
		spyCommit.mockRestore();
	});
});
