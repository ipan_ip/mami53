import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageSimulatedCostDownPayment from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageSimulatedCostDownPayment.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				tenantBill: {
					prorated: 0,
					discountRentPrice: 80000,
					otherCost: [{ field_value: 10000 }, { field_value: 20000 }],
					costDownPayment: 5000,
					costDeposit: 2000
				},
				statusPage: 'member'
			},
			mutations: {
				setTotalSimulatedCalculation: jest.fn()
			}
		}
	}
};

describe('ManageSimulatedCostDownPayment.vue', () => {
	const wrapper = shallowMount(ManageSimulatedCostDownPayment, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render simuated cost down payment', () => {
		expect(wrapper.find('.wrapper-addition').exists()).toBe(true);
	});

	it('should set total addition properly', () => {
		expect(wrapper.vm.totalAddition).toBe(80000 + 10000 + 20000 + 2000);
	});

	it('should return remaining price properly', () => {
		expect(wrapper.vm.remainingPrice).toBe(wrapper.vm.totalAddition - 5000);
	});

	it('should return rent price properly', () => {
		expect(wrapper.vm.rentPriceReal).toBe(80000);

		// if prorated
		wrapper.vm.$store.state.tenantProfile.tenantBill.prorated = 50000;
		expect(wrapper.vm.rentPriceReal).toBe(50000);
	});
});
