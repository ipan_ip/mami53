import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputTenantRentFees from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputTenantRentFees.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	modules: {
		tenantProfile: {
			state: {
				statusPage: '',
				tenantBill: {
					prorated: 90000,
					rentCount: 'month'
				}
			}
		},
		userRoom: {
			state: {
				original_price: {
					price_yearly: 1200,
					price_monthly: 100,
					price_quarterly: 300,
					price_semiannualy: 600,
					price_weekly: 25
				},
				price_yearly: 1211,
				price_monthly: 111,
				price_quarterly: 311,
				price_semiannualy: 611,
				price_weekly: 26
			}
		}
	},
	mutations: {
		setDiscountRentPrice: jest.fn()
	}
};

describe('ManageInputTenantRentFees.vue', () => {
	const wrapper = shallowMount(ManageInputTenantRentFees, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render input properly', () => {
		expect(wrapper.find('.form-group').exists()).toBe(true);
		expect(wrapper.find('.form-group > label').exists()).toBe(true);
		expect(wrapper.find('.form-group > #filterPriceMin').exists()).toBe(true);
	});

	it('should render rent price properly if is not prorated', async () => {
		const rentPrice = () =>
			wrapper.find('vue-numeric-stub').attributes('value');
		const setRentCount = val => {
			wrapper.vm.$store.state.tenantProfile.tenantBill.rentCount = val;
			return wrapper.vm.$nextTick();
		};
		wrapper.vm.$store.state.tenantProfile.tenantBill.prorated = 0;

		await setRentCount('month');
		expect(rentPrice()).toBe('111');

		await setRentCount('3_month');
		expect(rentPrice()).toBe('311');

		await setRentCount('6_month');
		expect(rentPrice()).toBe('611');

		await setRentCount('year');
		expect(rentPrice()).toBe('1211');

		await setRentCount('week');
		expect(rentPrice()).toBe('26');

		// changed to new member page
		wrapper.vm.$store.state.tenantProfile.statusPage = 'newMember';

		await setRentCount('month');
		expect(rentPrice()).toBe('100');

		// no rentcount given
		await setRentCount('');
		expect(rentPrice()).toBe('0');
	});
});
