import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputGender from 'Js/_ownerpage/mamipay/components/page-tenant-profile/form-input/ManageInputGender.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		tenantProfile: {
			statusPage: 'member',
			tenantProfile: {
				gender: 'male'
			}
		},
		allDetailRoom: {
			gender: 1
		}
	},
	mutations: {
		setGender: (state, payload) => {
			state.tenantProfile.tenantProfile.gender = payload;
		}
	}
};

const swal = jest.fn(_ => {
	return {
		then: jest.fn((result, dismiss) => {
			result(true);
			dismiss();
		})
	};
});

global.swal = swal;

describe('ManageInputGender.vue', () => {
	const wrapper = shallowMount(ManageInputGender, {
		localVue,
		store: new Vuex.Store(store)
	});

	it('should render input gender', () => {
		expect(wrapper.find('.form-group').exists()).toBe(true);
		expect(wrapper.findAll('.radio-control').length).toBe(2);
	});

	it('should call swal when selected gender is mismatch', async () => {
		await wrapper.setData({ inputGender: 'female' });
		expect(swal).toBeCalled();
	});

	it('should change gender properly', () => {
		wrapper.vm.changeGender('male');
		expect(wrapper.vm.inputGender).toBe('female');
		wrapper.vm.changeGender('female');
		expect(wrapper.vm.inputGender).toBe('male');
	});
});
