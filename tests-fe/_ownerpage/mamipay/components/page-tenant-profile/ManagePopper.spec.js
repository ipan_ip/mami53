import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import ManagePopper from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManagePopper.vue';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin(MixinNavigatorIsMobile);

const generateWrapper = () => {
	return shallowMount(ManagePopper, {
		localVue,
		propsData: {
			description: 'mamikos'
		}
	});
};

describe('ManagePopper.vue', () => {
	it('Should render properly', () => {
		const wrapper = generateWrapper();

		expect(wrapper).toMatchSnapshot();
	});

	it('should set prop description correctly', () => {
		const wrapper = generateWrapper();

		expect(wrapper.find('.popper > .popper-hl-body').text()).toBe('mamikos');
	});

	it('should set trigger to click if device is Mobile', () => {
		navigator.isMobile = true;

		const wrapper = generateWrapper();

		expect(wrapper.vm.trigger).toBe('click');
	});

	it('should set trigger to hover if device is not Mobile', () => {
		navigator.isMobile = false;

		const wrapper = generateWrapper();

		expect(wrapper.vm.trigger).toBe('hover');
	});

	it('should close popper when hide method is called', async () => {
		const wrapper = generateWrapper();

		wrapper.vm.$refs = {
			popper: {
				doClose: jest.fn()
			}
		};

		await wrapper.vm.$nextTick();

		const spyDoClose = jest.spyOn(wrapper.vm.$refs.popper, 'doClose');

		wrapper.vm.hide();
		expect(spyDoClose).toBeCalled();
	});
});
