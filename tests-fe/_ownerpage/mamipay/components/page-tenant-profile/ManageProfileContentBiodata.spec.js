import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import Vuex from 'vuex';
import ManageProfileContentBiodata from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileContentBiodata.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

global.tracker = jest.fn();

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const mockStore = {
	state: {
		tenantProfile: {
			tenantProfile: {
				fullName: '',
				work: 'Mahasiswa',
				roomNumber: 12345,
				roomAllotment: { id: 123 }
			},
			statusPage: '',
			tenantBill: { otherCost: [] },
			widgetInfo: { contractId: 123 }
		},
		manageBooking: { acceptBookingProfile: { user_id: 12345 } },
		allDetailRoom: { name: 'Singgah Sini', area_city: 'Sleman' }
	},
	mutations: {
		setFullName: jest.fn(),
		setOtherCost: jest.fn(),
		setConfirmParamsDate: jest.fn(),
		setConfirmParamsFrom: jest.fn()
	}
};

const $router = { push: jest.fn() };
const $toasted = { show: jest.fn() };
const $route = { params: { tenantId: 12345 } };
const toastProp = {
	type: 'info',
	className: 'toasted-default',
	theme: 'bubble',
	position: 'bottom-center',
	duration: 2000
};

const mocks = {
	openSwalLoading: jest.fn(),
	swalError: jest.fn(),
	swalSuccess: jest.fn(),
	$router,
	$toasted,
	$route
};

describe('ManageProfileContentBiodata.vue', () => {
	const shallowMountProp = {
		localVue,
		store: new Vuex.Store(mockStore),
		mocks
	};

	const wrapper = shallowMount(ManageProfileContentBiodata, shallowMountProp);

	it('should mount the component', () => {
		expect(wrapper.find('#manageProfileContentBiodata').exists()).toBe(true);
	});

	it('should set phone number message validation', () => {
		wrapper.vm.setPhoneNumberValidation('tenant', 'nomor telepon salah');
		expect(wrapper.vm.phoneNumberValidation.tenant).toBe('nomor telepon salah');
	});

	it('should push the route to billing page', () => {
		wrapper.vm.backToBill();
		expect($router.push).toBeCalledWith('/all/bill');
	});

	it('should show the toast with the correct message', () => {
		wrapper.vm.toastedWarning();
		expect($toasted.show).toBeCalledWith(
			'Ada beberapa isian yang belum diisi',
			toastProp
		);
	});

	it('should reset phone number error message on tenant', () => {
		wrapper.vm.phoneNumberValidation = { tenant: 'error tenant', parent: '' };
		expect(wrapper.vm.setPhoneNumberErrorMessage()).toEqual('error tenant');
	});

	it('should reset phone number error message on parent', () => {
		wrapper.vm.phoneNumberValidation = { tenant: '', parent: 'error parent' };
		expect(wrapper.vm.setPhoneNumberErrorMessage()).toEqual('error parent');
	});

	it('should show warning toast with error message tenant type', () => {
		const spyIsInvalidBiodataForm = jest
			.spyOn(wrapper.vm, 'isInvalidBiodataForm')
			.mockImplementation(() => false);
		const spyToastedWarning = jest.spyOn(wrapper.vm, 'toastedWarning');

		wrapper.vm.phoneNumberValidation = { tenant: 'error tenant', parent: '' };
		wrapper.vm.checkInput();

		expect(spyToastedWarning).toBeCalledWith('error tenant');

		spyToastedWarning.mockRestore();
		spyIsInvalidBiodataForm.mockRestore();
	});

	it('should show warning toast with default message because biodata form is invalid', () => {
		const spyIsInvalidBiodataForm = jest
			.spyOn(wrapper.vm, 'isInvalidBiodataForm')
			.mockImplementation(() => true);
		const spyToastedWarning = jest.spyOn(wrapper.vm, 'toastedWarning');
		wrapper.vm.checkInput();

		expect(spyToastedWarning).toBeCalled();

		spyToastedWarning.mockRestore();
		spyIsInvalidBiodataForm.mockRestore();
	});

	it('should show warning toast with default message if tenant is mahasiswa and invalid biodata form return false', () => {
		const spyIsInvalidBiodataForm = jest
			.spyOn(wrapper.vm, 'isInvalidBiodataForm')
			.mockImplementation(() => true);
		const spyToastedWarning = jest.spyOn(wrapper.vm, 'toastedWarning');

		wrapper.vm.phoneNumberValidation = { tenant: '', parent: '' };
		wrapper.vm.checkInput();

		expect(spyToastedWarning).toBeCalled();

		spyToastedWarning.mockRestore();
		spyIsInvalidBiodataForm.mockRestore();
	});

	it('should call update biodata method', async () => {
		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.vm.$store.state.tenantProfile.tenantProfile.work = '';
		wrapper.vm.phoneNumberValidation = { tenant: '', parent: '' };

		const spyIsInvalidBiodataForm = jest
			.spyOn(wrapper.vm, 'isInvalidBiodataForm')
			.mockImplementation(() => false);

		global.axios = {
			put: jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			})
		};

		await wrapper.vm.$nextTick();
		const saveButton = () => wrapper.find('.wrapper-button .btn-primary');
		await saveButton().trigger('click');
		await flushPromises();

		expect(mocks.swalSuccess).toBeCalledWith(null, 'Berhasil update biodata.');

		jest.clearAllMocks();
		global.axios = {
			put: jest.fn().mockRejectedValue()
		};

		await saveButton().trigger('click');
		await flushPromises();

		expect(bugsnagClient.notify).toBeCalled();

		spyIsInvalidBiodataForm.mockRestore();
	});

	it('should push the route page to billing tenant for new member', () => {
		mockStore.state.tenantProfile.statusPage = '';
		const spyIsInvalidBiodataForm = jest
			.spyOn(wrapper.vm, 'isInvalidBiodataForm')
			.mockImplementation(() => false);
		const spyToastedWarning = jest.spyOn(wrapper.vm, 'toastedWarning');

		wrapper.vm.checkInput();

		expect(mocks.$router.push).toBeCalledWith(
			'/profile-tenant/12345/billing-tenant/new-member'
		);

		spyToastedWarning.mockRestore();
		spyIsInvalidBiodataForm.mockRestore();
	});

	it('should assign error message when room id is null and should show the toast', () => {
		mockStore.state.tenantProfile.tenantProfile.roomAllotment.id = null;
		wrapper.vm.checkInputAcceptBooking();

		expect(mocks.$toasted.show).toBeCalledWith(
			'Nomor Kamar masih kosong.',
			toastProp
		);
	});

	it('should call send tracker method', () => {
		mockStore.state.tenantProfile.tenantProfile.roomAllotment.id = 12345;
		const spyTrackOwnerFillingRoomNumber = jest.spyOn(
			wrapper.vm,
			'trackOwnerFillingRoomNumber'
		);
		wrapper.vm.checkInputAcceptBooking();

		expect(spyTrackOwnerFillingRoomNumber).toBeCalled();
		expect(mocks.$router.push).toBeCalled();

		spyTrackOwnerFillingRoomNumber.mockRestore();
	});

	it('should send tracker to moengage', () => {
		wrapper.vm.trackOwnerFillingRoomNumber();
		expect(global.tracker).toBeCalled();
	});
});
