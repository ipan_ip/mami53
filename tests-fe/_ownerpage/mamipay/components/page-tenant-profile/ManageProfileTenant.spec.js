import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageProfileTenant from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileTenant.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

let mockStore = {
	state: {
		tenantProfile: {
			statusPage: 'member',
			widgetInfo: {
				tenantName: 'Anak Mami',
				roomNumber: 'B12',
				phoneNumber: '08122333455',
				profilePicture: { medium: 'http://mamikos.com/profile.jpg' }
			},
			tenantProfile: {
				roomAllotment: {
					name: 'Kamar 1',
					floor: 'Lantai 2'
				}
			},
			dataAcceptBooking: 'should show data'
		},
		manageBooking: {
			acceptBookingProfile: {
				tenant_photo: { medium: 'http://mamikos.com/profile-accept.jpg' },
				name: 'Anak Mami Kos'
			}
		}
	}
};

describe('ManageProfileTenant', () => {
	it('should render profile tenant component', () => {
		const wrapper = shallowMount(ManageProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#managetenantProfile').exists()).toBe(true);
		expect(wrapper.vm.tenantAcceptBooking).toBe('should show data');
	});

	it('should show profile picture', () => {
		const wrapper = shallowMount(ManageProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const profilePicture = wrapper.findAll('img').at(0);
		expect(profilePicture.attributes('data-src')).toBe(
			'http://mamikos.com/profile.jpg'
		);
	});

	it('should show correct tenant name', () => {
		const wrapper = shallowMount(ManageProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const tenantName = wrapper.find('.name-tenant');
		expect(tenantName.text()).toBe('Anak Mami');
	});

	it('should show correct room number and phone number', () => {
		const wrapper = shallowMount(ManageProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const numbersText = wrapper.findAll('span');
		expect(numbersText.at(1).text()).toBe('Kamar 1 Lantai 2');
	});

	it('should show correct photo ptofile if status is accept booking', () => {
		mockStore.state.tenantProfile.statusPage = 'acceptBooking';
		const wrapper = shallowMount(ManageProfileTenant, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		const profilePicture = wrapper.findAll('img').at(0);
		expect(profilePicture.attributes('data-src')).toBe(
			'http://mamikos.com/profile-accept.jpg'
		);
	});
});
