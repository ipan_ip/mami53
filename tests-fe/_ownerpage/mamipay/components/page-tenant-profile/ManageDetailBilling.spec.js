import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from '../../../../utils/mock-v-lazy';
import ManageDetailBilling from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageDetailBilling.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

jest.mock('Js/@mixins/MixinNavigatorIsMobile', () => {});

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

localVue.directive('clipboard', jest.fn());

const mockStore = {
	state: {
		tenantProfile: {
			tenantBill: { otherCost: [{ field_title: '', field_value: '', id: 0 }] },
			tenantProfile: {
				fullName: 'Mami 53',
				roomAllotment: { name: 'Kamar Mawar A' }
			},
			billingProfile: [
				{
					billingAmount: 126400,
					dateBill: '2020-03-09',
					information: 'Dibayar',
					invoiceLink: 'https://padang2.mamikos.com/invoice/I7uQD',
					invoiceNumber: '66591690/2020/03/0015',
					menu: 1516,
					reminder: false,
					status: 'waiting'
				}
			],
			widgetInfo: { roomNumber: 'kamar 2' }
		}
	}
};

const $toasted = { show: jest.fn() };
const openSwalLoading = jest.fn();
const swalError = jest.fn();
const closeSwalLoading = jest.fn();

const generateWrapper = ({ methodsProp = {} } = {}) => {
	const shallowMountProp = {
		localVue,
		store: new Vuex.Store(mockStore),
		propsData: {
			showModal: true,
			dataIndex: 0
		},
		mocks: {
			$dayjs: jest.fn(() => ({ format: jest.fn() })),
			$toasted
		},
		methods: { ...methodsProp }
	};
	return shallowMount(ManageDetailBilling, shallowMountProp);
};

describe('ManageDetailBilling.vue', () => {
	it('should mount the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('#manageDetailBilling').exists()).toBe(true);
	});

	it('should apply all coast value', () => {
		const wrapper = generateWrapper();
		wrapper.vm.actionApplyAllCost(9000);
		expect(wrapper.vm.isApplyAllCost).toBe(9000);
	});

	it('should show toast alert', () => {
		const wrapper = generateWrapper();
		wrapper.vm.toastedAlert('hello');
		expect($toasted.show).toBeCalledWith('hello', {
			type: 'info',
			className: 'toasted-default',
			theme: 'bubble',
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('should update additional price', () => {
		const updateAdditionalPrice = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { updateAdditionalPrice } });
		wrapper.vm.saveBilling();
		expect(updateAdditionalPrice).toBeCalled();
	});

	it('should close modal and reset basic price', () => {
		const wrapper = generateWrapper();
		wrapper.vm.cancelModal();
		expect(wrapper.emitted('closeModal')).toBeTruthy();
		expect(wrapper.vm.basicAmount).toBe(0);
	});

	it('should set pay outside mamipay', () => {
		const setManualPayment = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { setManualPayment } });
		wrapper.vm.setToPayOutsideMamipay();
		expect(setManualPayment).toBeCalled();
		expect(wrapper.vm.modalPayOutsideMamipay).toBe(false);
	});

	it('should emit update amount bill if update additional price api fetch return resolved value', async () => {
		const axios = {
			post: jest.fn().mockResolvedValue({ data: {} })
		};
		global.axios = axios;
		const normalizeOtherBill = jest.fn();
		const wrapper = generateWrapper({
			methodsProp: { closeSwalLoading, normalizeOtherBill, openSwalLoading }
		});
		await wrapper.vm.updateAdditionalPrice('MAMIX5627282');
		expect(EventBus.$emit).toBeCalledWith('isUpdateAmountBill');
		expect(closeSwalLoading).toBeCalled();
	});

	it('should show error popup when failed fetching api update additional price', async () => {
		const axios = {
			post: jest.fn().mockRejectedValue('error')
		};
		global.axios = axios;
		const normalizeOtherBill = jest.fn();
		const wrapper = generateWrapper({
			methodsProp: { openSwalLoading, normalizeOtherBill, swalError }
		});
		wrapper.vm.updateAdditionalPrice('MAMIX5627282');
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalled();
	});

	it('should get payment schedule if set manual payment successful', async () => {
		const axios = {
			put: jest.fn().mockResolvedValue({
				data: { status: true, meta: { message: 'success' } }
			})
		};
		global.axios = axios;
		const getPaymentSchedule = jest.fn();
		const wrapper = generateWrapper({
			methodsProp: {
				openSwalLoading,
				closeSwalLoading,
				getPaymentSchedule
			}
		});
		wrapper.vm.setManualPayment('MAMIX5627282');
		await wrapper.vm.$nextTick();
		expect(getPaymentSchedule).toBeCalledWith('MAMIX5627282');
	});

	it('should show error popup when set manual payment failed', async () => {
		const axios = {
			put: jest.fn().mockRejectedValue('error')
		};
		global.axios = axios;
		const wrapper = generateWrapper({
			methodsProp: { openSwalLoading, swalError }
		});
		wrapper.vm.setManualPayment('MAMIX5627282');
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalled;
	});

	it('should assign discount and deposit', () => {
		const wrapper = generateWrapper();
		wrapper.vm.getOtherCostComponent({
			other_cost: [
				{ original_type: 'deposit', value: 700000 },
				{ original_type: 'discount', value: 70000 }
			],
			detail_cost: { admin: 5000 }
		});
		expect(wrapper.vm.otherCostComponent.discount.value).toBe(70000);
		expect(wrapper.vm.otherCostComponent.deposit.value).toBe(700000);
	});

	it('should open modal payment outside mamipay', async () => {
		const wrapper = generateWrapper();
		wrapper.vm.paidOutside = true;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.modalPayOutsideMamipay).toBe(true);
	});

	it('should set paid outside flag to false when modal pay outside is closed', async () => {
		const wrapper = generateWrapper();
		wrapper.vm.modalPayOutsideMamipay = false;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.paidOutside).toBe(false);
	});

	it('should set paid outside flag to false when offline payment is false', async () => {
		const wrapper = generateWrapper();
		wrapper.vm.offlinePaymentCheck = true;
		wrapper.vm.offlinePaymentCheck = false;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.paidOutside).toBe(false);
	});

	it('should show correct flash sale properties', () => {
		const flashSaleProps = {
			amount: 900000,
			title: 'Diskon 50%',
			detail: { mamikos: true, owner: false }
		};
		const wrapper = generateWrapper();
		wrapper.vm.isFlashSale = flashSaleProps;
		expect(wrapper.vm.flashSaleAmount).toBe(900000);
		expect(wrapper.vm.flashSaleTitle).toBe('Diskon 50%');
		expect(wrapper.vm.flashSaleMamikos).toBe(true);
		expect(wrapper.vm.flashSaleOwner).toBe(false);
	});

	it('should get other cost component if get payment schedule successful', async () => {
		const axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: {} }
			})
		};
		global.axios = axios;
		const getOtherCostComponent = jest.fn();
		const wrapper = generateWrapper({
			methodsProp: { getOtherCostComponent, openSwalLoading, closeSwalLoading }
		});
		await wrapper.vm.getPaymentSchedule('MAMIX5627282');
		expect(axios.get).toBeCalledWith('/payment-schedule/detail/MAMIX5627282');
	});

	it('should return correct disbursement info text', () => {
		const wrapper = generateWrapper();
		wrapper.vm.isDisbursed = true;
		wrapper.vm.disburseAmount = 90000;
		expect(wrapper.vm.disbursementInfo).toBe(
			'Jumlah yang sudah diterima Rp90,000'
		);
	});
});
