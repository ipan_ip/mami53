import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageProfileTerminationCard from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileTerminationCard';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();

global.bugsnagClient = { notify: jest.fn() };

const windowOpen = jest.fn();
global.open = windowOpen;

global.axios = { get: jest.fn() };

describe('ManageProfileTerminationCard.vue', () => {
	const createWrapper = (status, isAllowReject = false) =>
		shallowMount(ManageProfileTerminationCard, {
			localVue,
			propsData: {
				tenantName: 'tenant',
				roomNumber: 'kamar 21',
				status,
				terminationId: 99,
				isAllowReject
			},
			mocks: {
				swalError: jest.fn(),
				$toasted: { show: jest.fn() }
			},
			stubs: {
				MamiButton: { template: `<button @click="$emit('click')" />` }
			}
		});

	describe('status: waiting', () => {
		let wrapper, toastShow;

		beforeEach(() => {
			wrapper = createWrapper('waiting');
			toastShow = jest.spyOn(wrapper.vm.$toasted, 'show');
		});

		afterEach(() => {
			toastShow.mockRestore();
		});

		it('should render termination card action', () => {
			expect(wrapper.find('.termination-card-action').exists()).toBe(true);
			expect(wrapper.find('.termination-card-info').exists()).toBe(false);
		});

		it('should render data correctly', () => {
			expect(
				wrapper.find('.termination-card-action__description').text()
			).toEqual(expect.stringContaining('tenant'));

			expect(
				wrapper.find('.termination-card-action__description').text()
			).toEqual(expect.stringContaining('kamar 21'));
		});

		it('should show button reject correctly', async () => {
			await wrapper.setProps({ isAllowReject: true });
			expect(
				wrapper.find('.termination-card-action__button-reject').exists()
			).toBe(true);

			await wrapper.setProps({ isAllowReject: false });
			expect(
				wrapper.find('.termination-card-action__button-reject').exists()
			).toBe(false);
		});

		describe('confirming', () => {
			let wrapper, toastShow;

			beforeEach(async () => {
				wrapper = createWrapper('waiting', true);
				toastShow = jest.spyOn(wrapper.vm.$toasted, 'show');
			});

			afterEach(() => {
				toastShow.mockRestore();
			});

			it('should emit confirm and show toast if confirmed', async () => {
				const axiosGet = jest.fn().mockResolvedValueOnce({
					data: {
						status: true,
						meta: {
							message: 'message success from api'
						}
					}
				});
				global.axios.get = axiosGet;
				const confirmButton = wrapper.find(
					'.termination-card-action__button-confirm'
				);
				await confirmButton.trigger('click');
				await flushPromises();

				expect(toastShow).toBeCalledWith(
					'message success from api',
					expect.any(Object)
				);
				expect(wrapper.emitted('confirmed')[0]).toBeTruthy();
			});

			it('should show error when api status return false', async () => {
				const axiosGet = jest.fn().mockResolvedValueOnce({
					data: {
						status: false,
						meta: {
							message: 'message failed from api'
						}
					}
				});
				global.axios.get = axiosGet;
				const confirmButton = wrapper.find(
					'.termination-card-action__button-confirm'
				);
				await confirmButton.trigger('click');
				await flushPromises();

				expect(toastShow).toBeCalledWith(
					'message failed from api',
					expect.any(Object)
				);
			});

			it('should call bugsnag when there is error', async () => {
				const axiosGet = jest.fn().mockRejectedValue(new Error('error'));
				global.axios.get = axiosGet;
				const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');

				const confirmButton = wrapper.find(
					'.termination-card-action__button-confirm'
				);
				await confirmButton.trigger('click');
				await flushPromises();

				expect(spyNotify).toBeCalled();
				spyNotify.mockRestore();
			});

			it('should not proceed confirmation if it is still confirming', async () => {
				jest.clearAllMocks();
				const spyAxiosGet = jest.spyOn(global.axios, 'get');
				await wrapper.setData({ isConfirming: true });
				const confirmButton = wrapper.find(
					'.termination-card-action__button-confirm'
				);
				await confirmButton.trigger('click');
				await flushPromises();

				expect(spyAxiosGet).not.toBeCalled();
				spyAxiosGet.mockRestore();
			});
		});

		describe('rejecting', () => {
			let wrapper, toastShow;
			const terminateRejectApi = '/contract/reject-terminate/99';

			beforeEach(() => {
				wrapper = createWrapper('waiting', true);
				toastShow = jest.spyOn(wrapper.vm.$toasted, 'show');
			});

			afterEach(() => {
				toastShow.mockRestore();
			});

			it('should emit reject and show toast if rejected', async () => {
				const axiosPost = jest.fn().mockResolvedValueOnce({
					data: {
						status: true,
						meta: {
							message: 'message success from api'
						}
					}
				});
				global.axios.post = axiosPost;
				const rejectButton = wrapper.find(
					'.termination-card-action__button-reject'
				);
				await rejectButton.trigger('click');
				await flushPromises();

				expect(axiosPost).toBeCalledWith(terminateRejectApi);
				expect(toastShow).toBeCalledWith(
					'message success from api',
					expect.any(Object)
				);
				expect(wrapper.emitted('rejected')[0]).toBeTruthy();
			});

			it('should show error when api status return false', async () => {
				const axiosPost = jest.fn().mockResolvedValueOnce({
					data: {
						status: false,
						meta: {
							message: 'message failed from api'
						}
					}
				});
				global.axios.post = axiosPost;

				const rejectButton = wrapper.find(
					'.termination-card-action__button-reject'
				);
				await rejectButton.trigger('click');
				await flushPromises();

				expect(axiosPost).toBeCalledWith(terminateRejectApi);
				expect(toastShow).toBeCalledWith(
					'message failed from api',
					expect.any(Object)
				);
			});

			it('should call bugsnag when there is error', async () => {
				const axiosPost = jest.fn().mockRejectedValue(new Error('error'));
				global.axios.post = axiosPost;

				const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');

				const rejectButton = wrapper.find(
					'.termination-card-action__button-reject'
				);
				await rejectButton.trigger('click');
				await flushPromises();

				expect(axiosPost).toBeCalledWith(terminateRejectApi);
				expect(spyNotify).toBeCalled();
				spyNotify.mockRestore();
			});

			it('should not proceed rejection if it is still rejecting', async () => {
				jest.clearAllMocks();
				const spyAxiosPost = jest.spyOn(global.axios, 'post');
				await wrapper.setData({ isRejecting: true });
				const rejectButton = wrapper.find(
					'.termination-card-action__button-reject'
				);
				await rejectButton.trigger('click');
				await flushPromises();

				expect(spyAxiosPost).not.toBeCalled();
				spyAxiosPost.mockRestore();
			});
		});
	});

	describe('status: confirmed', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = createWrapper('confirmed');
		});

		it('should render termination card info', () => {
			expect(wrapper.find('.termination-card-info').exists()).toBe(true);
		});

		it('should not render termination card action', () => {
			expect(wrapper.find('.termination-card-action').exists()).toBe(false);
		});
	});
});
