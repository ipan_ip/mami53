import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-debounce';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ManageProfileContentBilling from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageProfileContentBilling.vue';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

mockWindowProperty('window.location', { reload: jest.fn() });

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
global.window.scroll = jest.fn();
global.window.location.hash = '#perpanjanganDurasi';

const $toasted = { show: jest.fn() };
const $router = { push: jest.fn() };
const openSwalLoading = jest.fn();
const swalError = jest.fn();

const axios = {
	post: jest.fn().mockResolvedValue(),
	get: jest.fn().mockResolvedValue()
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const tracker = jest.fn();
global.tracker = tracker;

const mockStore = {
	state: {
		authData: {
			all: { id: 1, email: 'email@email.com', phone_number: '087222888111' }
		},
		tenantProfile: {
			statusPage: 'newMember',
			tenantBill: {
				rentCount: 'monthly',
				otherCost: [{ field_title: '', field_value: '', id: 0 }],
				checkInDate: '',
				rentPrice: '',
				rentDuration: '',
				fixedBilling: false,
				toggleRentDurationNew: false,
				downPayment: false,
				costFine: 0
			},
			tenantProfile: { fullName: 'Mami 53', roomNumber: '', question: [] },
			loadingState: {
				contract: false,
				contractBill: false
			},
			confirmParams: {
				request_date: '2020-02-01',
				from: 'accept'
			},
			widgetInfo: { contractId: 1 }
		},
		manageBooking: {
			ownerTermsAgreement: true,
			listBookingSelected: { id: 1 },
			acceptBookingProfile: { code: 1 }
		},
		userRoom: {
			id: 2
		}
	},
	mutations: {
		setForOtherTenant: jest.fn()
	}
};

describe('ManageProfileContentBilling', () => {
	it('should mount the component', () => {
		const wrapper = shallowMount(ManageProfileContentBilling, {
			localVue,
			mocks: {
				$toasted,
				$router,
				$route: { params: { tenantId: 123 }, name: 'tenantBill' }
			},
			store: new Vuex.Store(mockStore),
			methods: { openSwalLoading, swalError }
		});

		expect(wrapper.find('#manageProfileContentBiodata').exists()).toBe(true);
	});

	it('should call window scroll', () => {
		const wrapper = shallowMount(ManageProfileContentBilling, {
			localVue,
			mocks: {
				$toasted,
				$router,
				$route: { params: { tenantId: 123 } }
			},
			store: new Vuex.Store(mockStore),
			methods: { openSwalLoading, swalError }
		});

		expect(window.scroll).toBeCalledWith({
			behavior: 'smooth',
			top: 1030,
			left: 0
		});

		wrapper.vm.$destroy();
	});

	it('should go to form filling page', () => {
		const wrapper = shallowMount(ManageProfileContentBilling, {
			localVue,
			mocks: {
				$toasted,
				$router,
				$route: { params: { tenantId: 123 } }
			},
			store: new Vuex.Store(mockStore),
			methods: { openSwalLoading, swalError }
		});

		wrapper.vm.goToBiodataForm();
		expect(wrapper.vm.$router.push).toBeCalledWith(
			'/profile-tenant/123/biodata-tenant/new-member'
		);
	});

	it('should show skeleton loading if is member page and loading contract or bill', async () => {
		const wrapper = shallowMount(ManageProfileContentBilling, {
			localVue,
			store: new Vuex.Store(mockStore),
			mocks: {
				$route: { params: { tenantId: 123 }, name: 'tenantBill' }
			},
			methods: { openSwalLoading }
		});

		wrapper.vm.$store.state.tenantProfile.statusPage = 'member';
		wrapper.vm.$store.state.tenantProfile.loadingState.contractBill = true;

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.skeleton-loader-content-billing').exists()).toBe(
			true
		);
	});

	describe('form submit handler', () => {
		const mocksFn = {
			$route: { params: { tenantId: 123 }, name: 'tenantBill' },
			$router,
			openSwalLoading: jest.fn(),
			swalError: jest.fn(),
			swalSuccess: jest.fn((a, message, fn) => {
				fn && fn();
			}),
			$toasted: { show: jest.fn() }
		};

		const wrapper = shallowMount(ManageProfileContentBilling, {
			localVue,
			store: new Vuex.Store(mockStore),
			mocks: mocksFn
		});

		const getSaveButton = () => wrapper.find('.wrapper-button .btn-primary');
		const setStatusPage = async statusPage => {
			wrapper.vm.$store.state.tenantProfile.statusPage = statusPage;
			return await wrapper.vm.$nextTick();
		};
		const setTenantBillState = payload => {
			Object.keys(payload).forEach(key => {
				wrapper.vm.$store.state.tenantProfile.tenantBill[key] = payload[key];
			});
		};

		it('should handle accept booking properly', async () => {
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			await setStatusPage('acceptBooking');
			getSaveButton().trigger('click');

			expect(mocksFn.$toasted.show).toBeCalledWith(
				'Ada beberapa isian yang belum diisi',
				expect.any(Object)
			);

			jest.clearAllMocks();

			setTenantBillState({
				checkInDate: '2020-02-02',
				rentCount: 'monthly',
				rentDuration: 2,
				fixedBilling: false,
				toggleRentDurationNew: false,
				downPayment: false,
				otherCost: [],
				costFine: 0,
				rentPrice: 10000
			});

			getSaveButton().trigger('click');
			await flushPromises();

			expect(wrapper.vm.isValidBillingForm()).toBe(false);
			expect(mocksFn.swalSuccess).toBeCalled();

			jest.clearAllMocks();
			global.axios.post = jest.fn().mockRejectedValue('error');

			getSaveButton().trigger('click');
			await flushPromises();

			expect(bugsnagClient.notify).toBeCalled();
		});

		it('should handle update contract properly', async () => {
			jest.clearAllMocks();
			global.axios.put = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			await setStatusPage('member');

			setTenantBillState({
				checkInDate: '2020-02-02',
				rentCount: 'monthly',
				rentDuration: 2,
				fixedBilling: false,
				toggleRentDurationNew: false,
				downPayment: false,
				otherCost: [],
				costFine: 0,
				rentPrice: 10000
			});

			getSaveButton().trigger('click');
			await flushPromises();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.isValidBillingForm()).toBe(false);
			expect(mocksFn.swalSuccess).toBeCalled();

			jest.clearAllMocks();
			global.axios.put = jest.fn().mockRejectedValue('error');

			getSaveButton().trigger('click');
			await flushPromises();

			expect(bugsnagClient.notify).toBeCalled();
		});
	});
});
