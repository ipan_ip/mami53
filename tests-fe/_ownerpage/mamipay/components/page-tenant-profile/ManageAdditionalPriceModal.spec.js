import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageAdditionalPriceModal from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageAdditionalPriceModal.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

const $store = {
	state: {
		tenantProfile: {
			additionalPriceModal: {
				state: ''
			}
		}
	},
	getters: {
		isShowAdditionalPriceModal: false
	},
	commit: jest.fn()
};

describe('ManageAdditionalPriceModal.vue', () => {
	const wrapper = shallowMount(ManageAdditionalPriceModal, {
		localVue,
		mocks: { $store }
	});

	it('should render additional price modal', () => {
		expect(wrapper.find('.additiona-price-modal').exists()).toBe(true);
	});

	it('should set modal state when it changes', () => {
		wrapper.vm.$store.state.tenantProfile.additionalPriceModal.state = 'dp';
		wrapper.setData({ isShowModal: true });
		expect($store.commit).toBeCalledWith('setAdditionalPriceModal', {
			show: true,
			type: 'dp'
		});
	});
});
