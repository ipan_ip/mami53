import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import ManageCheckPhone from 'Js/_ownerpage/mamipay/components/page-tenant-profile/ManageCheckPhone.vue';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

mockVLazy(localVue);
window.Vue = require('vue');
mockSwalLoading();

global.axios = mockAxios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const mockStore = {
	state: {
		tenantProfile: {
			tenantBill: {
				otherCost: [{ field_title: '', field_value: '', id: 0 }]
			},
			userSelectedProfile: {
				tenant_id: '123'
			},
			phoneNumber: ''
		},
		newTenantNumberPhone: ''
	},
	mutations: {
		setOtherCost(state, payload) {
			state.tenantBill.otherCost = payload;
		},
		setNewTenantNumberPhone(state, payload) {
			state.newTenantNumberPhone = payload;
		},
		setPhoneNumber(state, payload) {
			state.tenantProfile.phoneNumber = payload;
		},
		setUserSelectedProfile: jest.fn()
	}
};

describe('ManageCheckPhone', () => {
	const generateWrapper = ({ mocksProp = {} } = {}) => {
		const shallowMountProp = {
			localVue,
			store: new Vuex.Store(mockStore),
			mocks: {
				...mocksProp
			}
		};
		return shallowMount(ManageCheckPhone, shallowMountProp);
	};

	it('should mount vue component', () => {
		const wrapper = generateWrapper();

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should check phone number', async () => {
		// status: false
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					tenant: {
						name: 'test'
					}
				}
			})
		};

		const wrapper = generateWrapper();
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		const phoneInput = wrapper.find('#numberPhone');
		phoneInput.setValue('083835555450');
		await wrapper.vm.$nextTick();

		wrapper.find('form').trigger('submit.prevent');
		await flushPromises();

		expect(wrapper.vm.isButtonDisabledCheckPhone).toBe(true);
		expect(spyCommit).toBeCalledWith('setUserSelectedProfile', {
			name: 'test'
		});
		expect(wrapper.vm.statusCheck).toBe(1);

		// status: false
		jest.clearAllMocks();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};
		await wrapper.setData({ statusCheck: null });
		wrapper.find('form').trigger('submit.prevent');
		await flushPromises();

		expect(spyCommit).toBeCalledWith('setUserSelectedProfile', {});
		expect(wrapper.vm.statusCheck).toBe(0);

		// failed
		await wrapper.setData({ statusCheck: null });
		global.axios.get = jest.fn().mockRejectedValue();
		wrapper.find('form').trigger('submit.prevent');
		await flushPromises();

		expect(wrapper.vm.isButtonDisabledCheckPhone).toBe(false);
		expect(wrapper.vm.statusCheck).toBe(null);

		spyCommit.mockRestore();
	});

	it('should throw to next section for existing tenant_id', () => {
		const wrapper = generateWrapper({
			mocksProp: { $router: { push: jest.fn() } }
		});

		wrapper.vm.nextSection();
		expect(wrapper.vm.$router.push).toBeCalledWith(
			'/profile-tenant/123/biodata-tenant/new-member'
		);
	});

	it('should throw to next section for nonexisting tenant_id', () => {
		mockStore.state.tenantProfile.userSelectedProfile.tenant_id = null;
		const wrapper = generateWrapper({
			mocksProp: { $router: { push: jest.fn() } }
		});

		wrapper.vm.tenantNumber = '083835555450';
		wrapper.vm.nextSection();
		expect(mockStore.state.newTenantNumberPhone).toBe('083835555450');
		expect(mockStore.state.tenantProfile.phoneNumber).toBe('083835555450');
		expect(wrapper.vm.$router.push).toBeCalledWith(
			'/profile-tenant/0/biodata-tenant/new-member'
		);
	});

	it('should change status member', () => {
		const wrapper = generateWrapper();

		wrapper.vm.changeStatus(2);
		expect(wrapper.vm.isButtonDisabledCheckPhone).toBe(false);

		wrapper.vm.changeStatus(1);
		expect(wrapper.vm.statusCheck).toBe(1);
	});
});
