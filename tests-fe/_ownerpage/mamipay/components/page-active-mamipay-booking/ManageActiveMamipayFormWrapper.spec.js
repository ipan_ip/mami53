import '../__mocks__/mockToken';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import Vuex from 'vuex';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import ManageActiveMamipayFormWrapper from 'Js/_ownerpage/mamipay/components/page-active-mamipay-booking/ManageActiveMamipayFormWrapper';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockComponent from 'tests-fe/utils/mock-component';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';

window.Vue = require('vue');
window.validator = new Validator();
Validator.localize('id', id);

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.directive('validate', jest.fn());
localVue.use(VeeValidate, {
	locale: 'id'
});

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

mockSwalLoading();

global.bugsnagClient = { notify: jest.fn() };
global.tracker = jest.fn();
global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: mockStore.state.bankList
		}
	})
};

describe('ManageActiveMamipayFormWrapper.vue', () => {
	const wrapper = shallowMount(ManageActiveMamipayFormWrapper, {
		localVue,
		store,
		stubs: {
			MamiTextField: mockComponent,
			MamiAutocomplete: mockComponent,
			MamiInfor: mockComponent,
			MamiCheckbox: mockComponent
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.mamipay-activation-form-wrapper').exists()).toBe(
			true
		);
	});

	it('Should set bank name selection status flag to true when a bank name is selected', () => {
		wrapper.vm.onSelectedBankName();

		expect(wrapper.vm.bankNameIsSelected).toBe(true);
	});

	it('Should revert bank name selection status flag back to false when the value of bankNameValue data is set', async () => {
		wrapper.vm.bankNameValue = 'Some Bank 2';

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.bankNameIsSelected).toBe(false);
	});

	it('Should add new error object when an additional error prop is provided', async () => {
		const additionalError = {
			field: 'some_field',
			messages: 'some messages'
		};

		wrapper.setProps({
			additionalError
		});

		const addNewErrorSpy = jest.spyOn(wrapper.vm, 'addNewError');

		await wrapper.vm.$nextTick();

		expect(addNewErrorSpy).toBeCalledWith(
			additionalError.field,
			additionalError.messages
		);
	});

	it('Should handle error fetching bank list properly', async () => {
		global.axios.get = jest.fn().mockRejectedValue('error');
		wrapper.vm.fetchBankList();
		await new Promise(resolve => setImmediate(resolve));

		expect(global.bugsnagClient.notify).toBeCalledWith('error');
	});

	it('Should emit submit when the form is submitted', () => {
		wrapper.vm.onSubmitForm();

		expect(wrapper.emitted('submit')[0][0]).toBe(wrapper.vm.ownerPayload);
	});
});
