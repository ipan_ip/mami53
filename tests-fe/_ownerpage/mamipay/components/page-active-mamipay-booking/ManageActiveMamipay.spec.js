import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import '../__mocks__/mockToken';
import ManageActiveMamipay from 'Js/_ownerpage/mamipay/components/page-active-mamipay-booking/ManageActiveMamipay';
import mockComponent from 'tests-fe/utils/mock-component';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import Vuex from 'vuex';

window.Vue = require('vue');

const localVue = createLocalVue();
localVue.use(Vuex);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('ManageActiveMamipay.vue', () => {
	const $route = {
		query: {}
	};
	const wrapper = shallowMount(ManageActiveMamipay, {
		localVue,
		stubs: {
			ManageFormRegister: mockComponent,
			MamiIntroSlider: mockComponent
		},
		mocks: { $route },
		store: new Vuex.Store(defaultStore)
	});

	it('Should exists', () => {
		expect(wrapper.find('.mamipay-activation-section').exists()).toBe(true);
	});

	it('Should display form wrapper component when "goToForm" method is called', () => {
		wrapper.vm.goToForm();

		expect(wrapper.vm.isIntro).toBe(false);
	});
});
