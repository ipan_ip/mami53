import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import '../__mocks__/mockToken';
import Vuex from 'vuex';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import ManageIntroSlider from 'Js/_ownerpage/mamipay/components/page-active-mamipay-booking/ManageIntroSlider';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';
import MixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import MixinNavigatorIsMobile from 'Js/@mixins/MixinNavigatorIsMobile';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinGetQueryString, MixinNavigatorIsMobile]);
mockVLazy(localVue);

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

global.bugsnagClient = { notify: jest.fn() };
global.tracker = jest.fn();

describe('ManageIntroSlider.vue', () => {
	const nextSlideSpy = jest.spyOn(ManageIntroSlider.methods, 'nextSlide');
	const wrapper = shallowMount(ManageIntroSlider, {
		localVue,
		store,
		stubs: {
			swiper,
			swiperSlide
		}
	});

	it('Should exists', () => {
		expect(wrapper.find('.mamipay-activation-intro').exists()).toBe(true);
	});

	it('Should call nextSlide function when next slide button is clicked', async () => {
		expect(
			wrapper.find("[data-path='btn_mamipayIntroNextSlide']").exists()
		).toBe(true);
		wrapper.find("[data-path='btn_mamipayIntroNextSlide']").trigger('click');

		await wrapper.vm.$nextTick();

		expect(nextSlideSpy).toHaveBeenCalled();
	});

	it('Should emit skipSlide when the slider is on the last index', () => {
		const skipSlideSpy = jest.spyOn(wrapper.vm, 'skipSlide');

		wrapper.vm.indexImage = wrapper.vm.imageSlide.length - 1;
		wrapper.vm.nextSlide();

		expect(wrapper.vm.isLastSlide).toBe(true);
		expect(skipSlideSpy).toBeCalled();
		expect(wrapper.emitted('skipSlide')).toBeTruthy();
	});
});
