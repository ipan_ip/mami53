import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import ManageActiveMamipayNavbar from 'Js/_ownerpage/mamipay/components/page-active-mamipay-booking/ManageActiveMamipayNavbar';
import MixinNavigatorIsMobile from 'Js/@mixins/MixinNavigatorIsMobile';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile]);
mockVLazy(localVue);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('ManageActiveMamipayNavbar.vue', () => {
	const wrapper = shallowMount(ManageActiveMamipayNavbar, {
		localVue
	});

	it('Should exists', () => {
		expect(wrapper.find('.mamipay-activation-navbar').exists()).toBe(true);
	});

	it('Should call EventBus emit "backToIntro" when back button is clicked', async () => {
		const backButton = wrapper.find(
			"[data-path='btn_mamipayActivationBackToIntro']"
		);

		expect(backButton.exists()).toBe(true);

		backButton.trigger('click');

		await wrapper.vm.$nextTick();

		expect(global.EventBus.$emit).toBeCalledWith('backToIntro');
	});
});
