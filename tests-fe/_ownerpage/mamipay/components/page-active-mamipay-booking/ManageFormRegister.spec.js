import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import '../__mocks__/mockToken';
import Vuex from 'vuex';
import ManageFormRegister from 'Js/_ownerpage/mamipay/components/page-active-mamipay-booking/ManageFormRegister';
import defaultStore from 'Js/_ownerpage/mamipay/store';
import mockStore from '../__mocks__/mamipayStore';
import MixinNavigatorIsMobile from 'Js/@mixins/MixinNavigatorIsMobile';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinNavigatorIsMobile]);

const store = new Vuex.Store(defaultStore);
store.commit('setAuthData', mockStore.state.authData);
store.commit('setAuthCheck', { tenant: true, owner: false, all: true });

const $router = { push: jest.fn() };

mockSwalLoading();
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

global.axios = mockAxios;
global.bugsnagClient = { notify: jest.fn() };

describe('ManageFormRegister.vue', () => {
	const trackActivationSubmittedSpy = jest.spyOn(
		ManageFormRegister.methods,
		'trackActivationSubmitted'
	);
	const wrapper = shallowMount(ManageFormRegister, {
		localVue,
		store,
		stubs: {
			ManageActiveMamipayFormNavbar: mockComponent,
			ManageActiveMamipayFormWrapper: mockComponent
		},
		mocks: { $router }
	});

	it('Should exists', () => {
		expect(wrapper.find('.mamipay-activation-register').exists()).toBe(true);
	});

	it('Should handle submit mamipay activation successully', async () => {
		axios.mockResolve({
			data: {
				status: 200
			}
		});

		wrapper.vm.submitMamipayActivation();

		await wrapper.vm.$nextTick();

		expect(trackActivationSubmittedSpy).toBeCalled();
	});

	it('Should handle submit mamipay activation and return response error', async () => {
		const mockErrorMessageObject = {
			name: 'There is an error in name field'
		};

		axios.mockResolve({
			data: {
				status: false,
				messages: mockErrorMessageObject
			}
		});

		wrapper.vm.submitMamipayActivation();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.formAdditionalError).toEqual({
			field: Object.keys(mockErrorMessageObject)[0],
			messages: mockErrorMessageObject.name
		});
	});

	it('Should handle submit mamipay activation and reject the request', async () => {
		axios.mockReject('error');

		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');

		wrapper.vm.submitMamipayActivation();

		await wrapper.vm.$nextTick();

		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Terjadi galat. Silakan coba lagi.'
		);
	});
});
