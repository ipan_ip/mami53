import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageMain from 'Js/_ownerpage/mamipay/components/ManageMain.vue';
import Vuex from 'vuex';
import { flushPromises } from 'tests-fe/utils/promises';

window.Vue = require('vue');
window.oxWebUrl = 'local.mamikos.com';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn((e, fn) => {
			fn({ status: false, message: '' });
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({});
	}),
	post: jest.fn(() => {
		return Promise.resolve({});
	}),
	catch: jest.fn()
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn(),
	catch: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const localVue = createLocalVue();
localVue.use(Vuex);

const authCheck = {
	all: false,
	owner: false,
	user: false
};
const authData = {
	all: {
		name: 'mamitest123'
	}
};

const parentComponent = {
	data() {
		return {
			state: {
				authCheck,
				authData
			}
		};
	}
};

global.authData = authData;
global.authCheck = authCheck;

const store = {
	state: {
		profile: {}
	},
	mutations: {
		setProfile(state, payload) {
			state.profile = payload;
		}
	}
};

jest.mock('Js/@components/GlobalNavbar', () => {
	return {
		template: '<div />',
		render: jest.fn()
	};
});

const mocks = {
	swalError: jest.fn(),
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn()
};

describe('ManageMain', () => {
	let wrapper, _this;

	beforeEach(() => {
		wrapper = shallowMount(ManageMain, {
			parentComponent,
			localVue,
			mocks,
			store: new Vuex.Store(store)
		});

		_this = wrapper.vm;
	});

	it('Should change activeBookingComponent value', async () => {
		await _this.modalBookingComponent(true);
		expect(_this.activeBookingComponent).toBeTruthy();

		await _this.modalBookingComponent(false);
		expect(_this.activeBookingComponent).toBeFalsy();
	});

	it('Should call when created state', async () => {
		expect(global.EventBus.$on).toBeCalledWith(
			'modalActiveBooking',
			expect.any(Function)
		);

		expect(global.EventBus.$on).toBeCalledWith(
			'activeAlert',
			expect.any(Function)
		);

		expect(global.EventBus.$on).toBeCalledWith(
			'isLoadingMain',
			expect.any(Function)
		);
	});

	it('Should set profile data properly', async () => {
		const profileData = {
			status: true,
			user: { name: 'Name', phone: '0800000' }
		};

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { ...profileData }
			})
		};

		wrapper.vm.getOwnerProfile();
		await flushPromises();

		expect(wrapper.vm.$store.state.profile).toEqual(profileData);
	});

	it('Should handle error fetch profile properly', async () => {
		const profileData = {
			status: false,
			user: { name: 'Name', phone: '0800000' }
		};

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { ...profileData }
			})
		};

		wrapper.vm.getOwnerProfile();
		await flushPromises();

		expect(mocks.swalError).toBeCalled();

		global.axios = {
			get: jest.fn().mockRejectedValue('error')
		};

		wrapper.vm.getOwnerProfile();
		await flushPromises();

		expect(bugsnagClient.notify).toBeCalled();
	});
});
