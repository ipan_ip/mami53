import { createLocalVue, shallowMount } from '@vue/test-utils';
import RejectBookingModal from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/RejectBookingModal.vue';

const localVue = createLocalVue();

describe('RejectBookingModal.vue', () => {
	let wrapper;
	const propsData = {
		showModal: true,
		modalTitle: 'modal title',
		modalCaption: 'modal caption',
		buttonText: 'button text'
	};

	beforeEach(() => {
		wrapper = shallowMount(RejectBookingModal, {
			localVue,
			propsData,
			stubs: {
				MamiModalWrap: {
					template: `<div class="modal-wrap" @click="$emit('close')"><slot></slot></div>`
				}
			}
		});
	});

	it('should render reject booking modal', () => {
		expect(wrapper.find('#rejectBookingModal').exists()).toBe(true);
	});

	it('should render correct modal title', () => {
		expect(wrapper.find('.modal-title').text()).toBe(propsData.modalTitle);
	});

	it('should render correct modal caption', () => {
		expect(wrapper.find('.modal-caption').text()).toBe(propsData.modalCaption);
	});

	it('should render correct button text', () => {
		expect(wrapper.find('.modal-action button').text()).toBe(
			propsData.buttonText
		);
	});

	it('should emit close when modal wrap closed', done => {
		wrapper.find('.modal-wrap').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('close')[0]).toBeTruthy();
			done();
		});
	});

	it('should emit send when button primary clicked', done => {
		wrapper.find('.modal-action button').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('send')[0]).toBeTruthy();
			done();
		});
	});
});
