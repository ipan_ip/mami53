import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingRejectModal from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/ManageBookingRejectModal.vue';

const localVue = createLocalVue();

describe('ManageBookingRejectModal', () => {
	const generateWrapper = options =>
		shallowMount(ManageBookingRejectModal, { localVue, ...options });

	it('should mount manage booking reject modal properly', () => {
		const wrapper = generateWrapper();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render modal wrapper', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.manage-booking-reject-modal').exists()).toBe(true);
	});

	it('should set body overflow to hidden if modal shown', done => {
		const wrapper = generateWrapper();
		wrapper.setProps({ showModal: true });
		wrapper.vm.$nextTick(() => {
			expect(
				wrapper
					.find('.manage-booking-reject-modal')
					.element.getAttribute('value')
			).toBe('true');
			expect(document.body.style.overflow).toBe('hidden');
			done();
		});
	});

	it('should set body overflow to inherit if modal not shown', done => {
		const wrapper = generateWrapper({ propsData: { showModal: true } });
		wrapper.setProps({ showModal: false });
		wrapper.vm.$nextTick(() => {
			expect(document.body.style.overflow).toBe('inherit');
			done();
		});
	});

	it('should emit closed when modal closed', () => {
		const stubs = {
			modal: `<div class="manage-booking-reject-modal" @click="$emit('closed')"></div>`
		};

		const wrapper = generateWrapper({ stubs });
		wrapper.find('.manage-booking-reject-modal').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('closed')).toBeTruthy();
		});
	});
});
