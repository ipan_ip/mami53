import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingReject from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/ManageBookingReject.vue';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import { flushPromises } from 'tests-fe/utils/promises';
import Vuex from 'vuex';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const AUTO_NULL_ID = 2;

const rejectReasonResolvedValue = {
	data: {
		data: [
			{
				id: 1,
				description: 'Kost sudah Penuh'
			},
			{
				id: 2,
				description: 'Kamar sudah tersewa',
				make_kost_not_available: true
			},
			{
				id: 3,
				description: 'Kost sedang direnovasi'
			}
		],
		status: true
	}
};

const localVue = createLocalVue();
window.Vue = localVue;
mockSwalLoading();
localVue.use(Vuex);
global.axios = {
	get: jest.fn().mockResolvedValue(rejectReasonResolvedValue),
	post: jest.fn().mockResolvedValue({ data: { status: true, data: {} } })
};

global.bugsnagClient = { notify: jest.fn() };

describe('ManageBookingReject.vue', () => {
	const $store = {
		state: {
			rejectReasons: [],
			allDetailRoom: { room_available: 2 },
			manageBooking: {
				listBookingSelected: {
					id: 1
				}
			}
		},
		mutations: {
			setRejectReasons(state, payload) {
				state.rejectReasons = payload;
			},
			setRoomAvailability(state, payload) {
				state.allDetailRoom.room_available = payload;
			},
			setSearchBookingPerPage() {},
			setSearchBookingLimit() {},
			setSearchBookingOffset() {},
			setSearchBookingGroup() {},
			setBookingSelectedNumberPagination() {}
		}
	};

	const mocks = {
		$dayjs: jest.fn(() => {
			return {
				format: jest.fn()
			};
		})
	};

	const propsData = { showModal: true };

	const stubs = {
		RejectBookingModalIntercept: {
			template: '<div class="reject-booking-modal-intercept"></div>'
		},
		RejectBookingModal: {
			template:
				'<div class="reject-booking-modal"><button class="close">close</button></div>'
		}
	};

	const wrapper = shallowMount(ManageBookingReject, {
		localVue,
		propsData,
		mocks,
		stubs,
		store: new Vuex.Store($store)
	});

	it('should render manage booking reject', async () => {
		expect(wrapper.find('#manageBookingReject').exists()).toBe(true);
	});

	it('should show reason description if selected reason is "Lainnya"', async () => {
		await wrapper.setData({ selectedReason: wrapper.vm.otherReasonId });
		expect(wrapper.find('.reject-reason__other-reason').exists()).toBe(true);
	});

	it('should close all modal when modal success reject closed', () => {
		jest.clearAllMocks();
		wrapper.findComponent(stubs.RejectBookingModalIntercept).vm.$emit('close');
		expect(wrapper.emitted('closeModal')[0]).toBeTruthy();
		expect(global.EventBus.$emit).toBeCalledWith('activeToggleAll');
	});

	it('should emit closeModal when modal reject closed', () => {
		jest.clearAllMocks();
		wrapper.vm.closeModalReject();
		expect(wrapper.emitted('closeModal')[0]).toBeTruthy();
	});

	it('should set room availability to 0 after booking success if selected reason is auto null option', async () => {
		jest.clearAllMocks();
		await wrapper.vm.getRejectReasons();
		await wrapper.setData({ selectedReason: AUTO_NULL_ID });
		wrapper.vm.onSubmittedBookingRejectReason();
		expect(wrapper.vm.$store.state.allDetailRoom.room_available).toBe(0);
	});

	it('should close all modal after booking success if selected reason is "Lainnya"', () => {
		wrapper.setData({ selectedReason: wrapper.vm.otherReasonId });
		jest.clearAllMocks();
		wrapper.vm.onSubmittedBookingRejectReason();
		expect(wrapper.emitted('closeModal')[0]).toBeTruthy();
		expect(global.EventBus.$emit).toBeCalledWith('activeToggleAll');
	});

	it('should set booking reason to other reason when submitting booking if "Lainnya" is selected', async () => {
		jest.clearAllMocks();
		const otherReasonDescription = 'other reason description';
		const spyRejectBookingRequest = jest.spyOn(
			wrapper.vm,
			'rejectBookingRequest'
		);
		wrapper.setData({
			selectedReason: wrapper.vm.otherReasonId,
			otherReasonDescription,
			isAcceptTermsAndConditions: true
		});
		wrapper.setProps({ showModal: false });
		await wrapper.vm.$nextTick();
		wrapper.setProps({ showModal: true });
		await wrapper.vm.$nextTick();
		wrapper.find('.reject-reason__btn-action .btn-submit').trigger('click');
		expect(spyRejectBookingRequest).toBeCalledWith(
			wrapper.vm.otherReasonId,
			otherReasonDescription,
			expect.any(Function)
		);
		spyRejectBookingRequest.mockRestore();
	});

	it('should use booking reason value when submitting booking if selected value is not "Lainnya"', async () => {
		jest.clearAllMocks();
		wrapper.setProps({ showModal: true });
		await wrapper.vm.getRejectReasons();
		await flushPromises();
		const spyRejectBookingRequest = jest.spyOn(
			wrapper.vm,
			'rejectBookingRequest'
		);
		await wrapper.setData({
			selectedReason: AUTO_NULL_ID,
			isAcceptTermsAndConditions: true
		});

		wrapper.find('.reject-reason__btn-action .btn-submit').trigger('click');
		expect(spyRejectBookingRequest).toBeCalledWith(
			wrapper.vm.reasonOptions[1].id,
			wrapper.vm.reasonOptions[1].description,
			expect.any(Function)
		);
		spyRejectBookingRequest.mockRestore();
	});

	it('should show error and emit close when get booking request return false', async () => {
		jest.clearAllMocks();
		global.swal = jest.fn().mockResolvedValue(true);
		global.swal.showLoading = jest.fn();
		global.axios.get = jest
			.fn()
			.mockResolvedValue({ data: { status: false, meta: { message: '' } } });

		await wrapper.vm.getRejectReasons();
		await flushPromises();
		expect(wrapper.emitted('closeModal')[0]).toBeTruthy();
	});

	it('should call bugnsag when get booking request failed', async () => {
		jest.clearAllMocks();
		global.axios.get = jest.fn().mockRejectedValue();

		await wrapper.vm.getRejectReasons();
		await flushPromises();
		expect(bugsnagClient.notify).toBeCalled();
	});
});
