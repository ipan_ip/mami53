import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingRejectOptions from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/ManageBookingRejectOptions.vue';
import reasonOptions from './__mocks__/reject-reason-options.json';

const localVue = createLocalVue();
const AUTO_NULL_ID = 2;
const AUTO_NULL_INDEX = 1;

const RejectBookingModal = { template: '<div class="reject-booking-modal"/>' };

describe('ManageBookingRejectOptions.vue', () => {
	const generateWrapper = () =>
		shallowMount(ManageBookingRejectOptions, {
			localVue,
			propsData: {
				reasonOptions
			},
			stubs: { RejectBookingModal }
		});

	it('should render reject options wrapper', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.manage-booking-reject-options').exists()).toBe(true);
	});

	it('should render correct amount of reason options', () => {
		const wrapper = generateWrapper();
		expect(wrapper.findAll('.reject-options').length).toBe(
			reasonOptions.length
		);
	});

	it('should show auto null intercept if reason option is auto null', async () => {
		const wrapper = generateWrapper();
		await wrapper.setData({ selectedOption: AUTO_NULL_ID });
		expect(wrapper.vm.isShowIntercept).toBe(true);
	});

	it('should emit input when owner confirm auto null modal', async () => {
		jest.clearAllMocks();
		const wrapper = generateWrapper();
		await wrapper
			.findAll('.reject-options')
			.at(AUTO_NULL_INDEX)
			.trigger('click.native');

		expect(wrapper.vm.isShowIntercept).toBe(true);
		expect(wrapper.vm.applyValue).toBe(AUTO_NULL_ID);
		wrapper.findComponent(RejectBookingModal).vm.$emit('send');
		expect(wrapper.emitted('input')[0][0]).toBe(AUTO_NULL_ID);
	});

	it('should not emit input when owner close auto null modal', async () => {
		jest.clearAllMocks();
		const wrapper = generateWrapper();
		await wrapper
			.findAll('.reject-options')
			.at(AUTO_NULL_INDEX)
			.trigger('click.native');

		expect(wrapper.vm.isShowIntercept).toBe(true);
		expect(wrapper.vm.applyValue).toBe(AUTO_NULL_ID);

		wrapper.findComponent(RejectBookingModal).vm.$emit('close');
		expect(wrapper.emitted('input')).toBeFalsy();
	});

	it('should set state if option auto null is selected', async () => {
		jest.clearAllMocks();
		const wrapper = generateWrapper();
		await wrapper.setProps({ value: AUTO_NULL_ID });
		const autoNullSelectedOptions = wrapper
			.findAll('.reject-options')
			.at(AUTO_NULL_INDEX);

		expect(autoNullSelectedOptions.classes('--show-info')).toBe(true);
	});

	it('should emit input when if not auto null option selected', async () => {
		jest.clearAllMocks();
		const wrapper = generateWrapper();
		wrapper
			.findAll('.reject-options')
			.at(0)
			.trigger('click.native');

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isShowIntercept).toBe(false);
		expect(wrapper.emitted('input')[0][0]).toBe(reasonOptions[0].id);
	});
});
