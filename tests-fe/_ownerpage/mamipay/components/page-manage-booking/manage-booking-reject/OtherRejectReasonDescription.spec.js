import { createLocalVue, shallowMount } from '@vue/test-utils';
import OtherRejectReasonDescription from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/OtherRejectReasonDescription.vue';

const localVue = createLocalVue();

describe('OtherRejectReasonDescription.vue', () => {
	let wrapper;

	beforeEach(() => {
		const propsData = { value: '', maxLength: 60 };
		wrapper = shallowMount(OtherRejectReasonDescription, {
			localVue,
			propsData
		});
	});

	afterEach(() => {
		wrapper.destroy();
	});

	it('should mount component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render correct counter', done => {
		const textarea = wrapper.find('.other-reason-description__textarea');
		const counter = wrapper.find('.other-reason-description__counter');
		const input = 'Kost saya penuh';
		textarea.element.value = input;
		textarea.trigger('input');
		const expectedCounter = '15/60';
		wrapper.vm.$nextTick(() => {
			expect(counter.text()).toBe(expectedCounter);
			done();
		});
	});

	it('should emit input value when inputting', () => {
		const textarea = wrapper.find('.other-reason-description__textarea');
		const input = 'Kost sedang penuh';
		textarea.element.value = input;
		jest.clearAllMocks();
		textarea.trigger('input');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.otherReasonDescription).toBe(input);
			expect(wrapper.emitted('input')[0][0]).toBe(input);
		});
	});
});
