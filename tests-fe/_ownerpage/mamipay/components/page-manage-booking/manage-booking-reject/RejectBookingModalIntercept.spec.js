import { createLocalVue, shallowMount } from '@vue/test-utils';
import RejectBookingModalIntercept from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-reject/RejectBookingModalIntercept.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowLocation from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

global.oxWebUrl = 'https://ownerpage.mamikos.com/';
mockWindowLocation('window.location', '');

const $store = {
	state: {
		userRoom: {
			id: 1232
		}
	}
};

describe('RejectBookingModalIntercept.vue', () => {
	let wrapper;
	beforeEach(() => {
		const propsData = { showModal: true };
		const mountOptions = {
			localVue,
			propsData,
			mocks: {
				$store
			},
			stubs: {
				routerLink: { template: '<div><slot></slot></div>' },
				MamiModalWrap: {
					template: `<div class="modal-wrapper" @click="$emit('close')"><slot></slot></div>`
				}
			}
		};
		wrapper = shallowMount(RejectBookingModalIntercept, mountOptions);
	});

	it('should mount modal reject intercept properly', () => {
		expect(wrapper.find('#rejectBookingModalIntercept').exists()).toBe(true);
		expect(wrapper.find('.modal-title').exists()).toBe(true);
		expect(wrapper.find('.modal-caption').exists()).toBe(true);
		expect(wrapper.find('.modal-action').exists()).toBe(true);
	});

	it('should emit close when modal closed', () => {
		wrapper.find('.modal-wrapper').trigger('click');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should change location when update kost clicked', () => {
		wrapper.find('.modal-action .btn-success').trigger('click');
		expect(global.location).toBe(
			`${global.oxWebUrl}kos/rooms/${$store.state.userRoom.id}/edit`
		);
	});
});
