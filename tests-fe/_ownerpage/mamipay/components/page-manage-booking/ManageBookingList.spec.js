import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingList from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingList.vue';
import Vuex from 'vuex';
import manageBookingList from './__mocks__/owner-booking-list.json';
import detailRoom from './__mocks__/owner-detail-room.json';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

const $dayjs = jest.fn(date => {
	return {
		valueOf: jest.fn(() => {
			return new Date(date).getTime();
		})
	};
});

const $store = {
	state: {
		manageBooking: {
			listBooking: {
				data: manageBookingList
			}
		},
		userRoomStatus: {
			booking_active_status: 'approved'
		},
		allDetailRoom: detailRoom,
		authData: { all: { name: 'owner mamirooms' } }
	},
	mutations: {
		setSelectedDetailBooking: jest.fn()
	}
};

const ManageBookingListCard = { template: '<div />' };

const tracker = jest.fn();
const open = jest.fn();
global.tracker = tracker;
global.open = open;

describe('ManageBookingList.vue', () => {
	const wrapper = shallowMount(ManageBookingList, {
		localVue,
		store: new Vuex.Store($store),
		stubs: { ManageBookingListCard },
		mocks: { $dayjs }
	});

	it('should render manage booking list', () => {
		expect(wrapper.find('#manageBookingList').exists()).toBe(true);
	});

	it('should render all booking item', () => {
		expect(wrapper.findAll('.manage-booking-list__item').length).toBe(
			wrapper.vm.bookingList.length
		);
	});

	it('should set selected booking list if view detail clicked', () => {
		jest.clearAllMocks();
		wrapper
			.findComponent(ManageBookingListCard)
			.vm.$emit('view-detail-clicked', { ...detailRoom });
		expect(tracker).toBeCalledWith('moe', [
			'[Owner] Visit Detail Booking',
			expect.any(Object)
		]);
	});
});
