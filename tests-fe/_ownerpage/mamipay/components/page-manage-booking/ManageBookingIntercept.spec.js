import '@babel/polyfill';
import { mount, createLocalVue } from '@vue/test-utils';
import ManageBookingIntercept from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingIntercept.vue';

const localVue = createLocalVue();

const $emit = jest.fn();

describe('ManageBookingIntercept', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(ManageBookingIntercept, {
			localVue,
			propsData: {
				showModal: true,
				modalTitle: '',
				modalCaption: '',
				confirmText: 'Saya Mengerti'
			},
			mocks: {
				$emit
			}
		});
	});

	it('Should mount component properly', () => {
		expect(
			wrapper.find('.manage-booking-intercept-modal').exists()
		).toBeTruthy();
	});

	it('Should emit close', () => {
		wrapper.vm.closeModal();

		expect($emit).toBeCalled();
	});
});
