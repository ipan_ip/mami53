import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageOwnerBooking from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageOwnerBooking.vue';
import Vuex from 'vuex';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerBookingListData from './__mocks__/owner-booking-list.json';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);
window.Vue = localVue;
mockSwalLoading();

jest.mock('Js/@utils/makeAPICall.js');
jest.mock('pretty-checkbox/src/pretty-checkbox.scss', () => {});
jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn({
				data: {
					room_id: 1
				}
			});
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const store = {
	state: {
		userRoom: {
			id: 1,
			room_title: 'mamirooms'
		},
		manageBooking: {
			searchBooking: {
				group: `&group[]=0`
			},
			listBooking: {
				data: [],
				total: 0
			},
			listBookingSelected: {
				id: 1,
				confirm_expired_date: '1990-02-02'
			},
			isLoadingBookingList: false
		},
		userRoomStatus: {
			booking_active_status: 'waiting'
		},
		userRooms: []
	},
	mutations: {
		setSearchBookingPerPage: jest.fn(),
		setSearchBookingLimit: jest.fn(),
		setSearchBookingOffset: jest.fn(),
		setSearchBookingGroup: jest.fn(),
		setBookingSelectedNumberPagination: jest.fn(),
		setTotalIncome: jest.fn(),
		setTotalTransfer: jest.fn(),
		setUserRoomStatus: jest.fn(),
		setBookingListLoading: jest.fn(),
		setRejectBookingFrom: jest.fn(),
		setListBooking: jest.fn(),
		setListBookingSelected: jest.fn(),
		setSearchBookingTotalPage: jest.fn(),
		setSelectedDetailBooking: jest.fn(),
		setUserRooms: (state, payload) => {
			state.userRooms = payload;
		},
		setUserRoom: (state, payload) => {
			state.userRoom = payload;
		},
		setAllDetailRoom: (state, payload) => {
			state.allDetailRoom = payload;
		}
	}
};

const bugsnagClient = {
	notify: jest.fn(),
	catch: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const $route = {
	params: {
		seq: 'all'
	},
	query: {}
};

function CancelToken(executor) {
	executor && executor();
}

CancelToken.source = function() {
	return {
		token: '123'
	};
};

const axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: ownerBookingListData
		}
	}),
	CancelToken
};

global.axios = axios;

const ManageBookingList = { template: '<div/>' };

const shallowMountComponent = () =>
	shallowMount(ManageOwnerBooking, {
		localVue,
		store: new Vuex.Store(store),
		mocks: { $route },
		stubs: { ManageBookingList }
	});

describe('ManageOwnerBooking.vue', () => {
	const wrapper = shallowMountComponent();

	it('should render manage owner booking', () => {
		expect(wrapper.find('#manageOwnerBooking').exists()).toBe(true);
	});

	it('should set correct description', () => {
		const groups = [
			'',
			'Butuh Konfirmasi',
			'Tunggu Pembayaran',
			'Terbayar',
			'Anda Tolak',
			'Dibatalkan',
			'Pembayaran Diterima',
			'Kedaluwarsa',
			'Sewa Berakhir'
		];
		wrapper.vm.$store.state.userRoomStatus.booking_active_status = 'approved';
		groups.forEach((label, index) => {
			wrapper.vm.$store.state.manageBooking.searchBooking.group = `&group[]=${index}`;
			expect(wrapper.vm.getEmptyBookingDescription).toEqual(
				expect.stringContaining(label)
			);
		});
	});

	it('should show correct empty illustration', () => {
		wrapper.vm.$store.state.manageBooking.searchBooking.group = `&group[]=0`;
		expect(wrapper.vm.getEmptyBookingIllustration).toEqual(
			expect.stringContaining('have-not-booking')
		);
		wrapper.vm.$store.state.manageBooking.searchBooking.group = '';
		expect(wrapper.vm.getEmptyBookingIllustration).toEqual(
			expect.stringContaining('empty-bill')
		);
	});

	it('should show modal dialog reject', () => {
		wrapper.vm.showRejectBooking();
		expect(wrapper.vm.isModalDialogReject).toBe(true);
		expect(wrapper.vm.isModalAcceptBooking).toBe(false);
	});

	it('should close modal dialog reject', () => {
		wrapper.vm.closeModalDialogReject();
		expect(wrapper.vm.isModalDialogReject).toBe(false);
	});

	it('should close modal dialog accept', () => {
		wrapper.vm.closeModalAcceptBooking();
		expect(wrapper.vm.isModalAcceptBooking).toBe(false);
	});

	it('should navigate to correct edit room', () => {
		global.oxWebUrl = 'owner.mamikos.com';
		mockWindowProperty('window.location', '');
		wrapper.vm.goToRoomAndPriceSettings();
		expect(window.location).not.toBe('');
	});

	it('show detail booking', () => {
		wrapper.vm.showDetailBooking();
		expect(wrapper.vm.showModalDetailBooking).toBe(true);
	});

	it('should show expired page', () => {
		wrapper.vm.showReasonReject();
		expect(wrapper.vm.expiredPage).toBe(true);
	});

	it('should set expired to false if booking list showen', () => {
		wrapper.vm.showBookingList();
		expect(wrapper.vm.expiredPage).toBe(false);
	});

	it('should show expired page if booking is expired and owner call goToUserBilling', () => {
		wrapper.vm.goToUserBilling();
		expect(wrapper.vm.expiredPage).toBe(true);
	});

	it('should set expired page to false if is loading', async () => {
		wrapper.vm.$store.state.manageBooking.isLoadingBookingList = true;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.expiredPage).toBe(false);
	});

	it.skip('should render booking list if any', async () => {
		store.state.userRoom.booking_status = true;
		store.state.userRoomStatus.booking_active_status = 'approved';
		store.state.manageBooking.listBooking.data = [...ownerBookingListData];
		store.state.manageBooking.listBooking.total = ownerBookingListData.length;
		const wrapper = shallowMountComponent();
		wrapper.setData({ expiredPage: false });
		await wrapper.vm.$nextTick();
		await Promise.resolve;
		await Promise.prototype.then;
		expect(wrapper.findComponent(ManageBookingList).exists()).toBe(true);
	});

	it('should not render booking list if no booking list data', async () => {
		store.state.userRoom.booking_status = true;
		store.state.userRoomStatus.booking_active_status = 'approved';
		store.state.manageBooking.listBooking.data = null;
		store.state.manageBooking.listBooking.total = 1;
		const wrapper = shallowMountComponent();
		wrapper.setData({ expiredPage: false });
		await wrapper.vm.$nextTick();
		expect(wrapper.findComponent(ManageBookingList).exists()).toBe(false);
	});

	it('should get correct tenant booking id', async () => {
		const wrapper = shallowMountComponent();
		wrapper.vm.$store.state.manageBooking.listBookingSelected = {
			...ownerBookingListData[0]
		};
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.tenantIdBooking).toBe(ownerBookingListData[0].id);
	});

	it('should call bugsnagClient.notify on failed ManageBookingIntercept api call', async () => {
		makeAPICall.mockImplementation(() => false);

		await wrapper.vm.getInterceptData();
		await wrapper.vm.$nextTick();

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should close Manage booking intercept', () => {
		wrapper.vm.closeManageBookingIntercept();
		expect(wrapper.vm.isShowIntercept).toBe(false);
	});
});
