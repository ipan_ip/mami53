import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingCategories from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingCategories.vue';
import Vuex from 'vuex';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import dayjs from 'dayjs';

const localVue = createLocalVue();
window.Vue = localVue;
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;
mockSwalLoading();

jest.mock('pretty-checkbox/src/pretty-checkbox.scss', () => {});

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const store = {
	state: {
		manageBooking: {
			searchBooking: {
				limit: 5
			}
		},
		userRoom: {
			id: 2
		}
	},
	mutations: {
		setBookingSelectedNumberPagination: jest.fn(),
		setSearchBookingGroup: jest.fn(),
		setSearchBookingPerPage: jest.fn(),
		setSearchBookingLimit: jest.fn(),
		setSearchBookingOffset: jest.fn(),
		setBookingListLoading: jest.fn()
	}
};

function CancelToken(executor) {
	executor && executor();
}

CancelToken.source = function() {
	return {
		token: '123'
	};
};

global.axios = {
	get: jest.fn().mockRejectedValue('error'),
	CancelToken
};

global.bugsnagClient = {
	notify: jest.fn()
};

describe('ManageBookingCategories.vue', () => {
	const wrapper = shallowMount(ManageBookingCategories, {
		localVue,
		store: new Vuex.Store(store),
		mocks: {
			$route: { query: {} }
		}
	});

	const setValue = (index, value) => {
		const currentDataCategories = wrapper.vm.dataCategories;
		const updatedValue = { ...currentDataCategories[index], value };
		currentDataCategories.splice(index, 1, updatedValue);
		return [...currentDataCategories];
	};

	it('should render manage booking categories', () => {
		expect(wrapper.find('.manage-booking-categories').exists()).toBe(true);
	});

	it('should set correct category group', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		jest.clearAllMocks();
		wrapper.setData({ dataCategories: setValue(1, true) });
		wrapper.vm.categoriesChange(1);
		expect(spyCommit).toHaveBeenCalledWith(
			'setSearchBookingGroup',
			'&group[]=1'
		);
		spyCommit.mockRestore();
	});

	it('should reset category group if all categories value is false', async () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		jest.clearAllMocks();
		wrapper.setData({ dataCategories: setValue(1, false) });
		wrapper.setData({ dataCategories: setValue(0, false) });
		wrapper.vm.categoriesChange(0);
		expect(spyCommit).toHaveBeenCalledWith(
			'setSearchBookingGroup',
			'&group[]=0'
		);
		expect(wrapper.vm.dataCategories[0].value).toBe(true);
		spyCommit.mockRestore();
	});

	it('should get isThereToggleSelected to true when "all" category is true and there is other category selected', () => {
		wrapper.vm.dataCategories.forEach((_, index) => {
			wrapper.setData({ dataCategories: setValue(index, false) });
		});
		wrapper.setData({ dataCategories: setValue(0, true) });
		wrapper.setData({ dataCategories: setValue(2, true) });
		expect(wrapper.vm.isThereToggleSelected()).toBe(true);
	});

	it('should get isAllToggleActive to true when all categories value is true expect for "all" category', () => {
		wrapper.vm.dataCategories.forEach((_, index) => {
			wrapper.setData({ dataCategories: setValue(index, true) });
		});
		wrapper.setData({ dataCategories: setValue(0, false) });
		expect(wrapper.vm.isAllToggleActive()).toBe(true);
	});

	it('should reset to all category selected', () => {
		wrapper.vm.dataCategories.forEach((_, index) => {
			wrapper.setData({ dataCategories: setValue(index, true) });
		});
		wrapper.setData({ dataCategories: setValue(0, false) });
		wrapper.vm.reconditionToggle();
		expect(wrapper.vm.toggleAll).toBe(true);
	});
});
