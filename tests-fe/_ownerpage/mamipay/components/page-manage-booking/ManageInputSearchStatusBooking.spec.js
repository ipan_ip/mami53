import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageInputSearchStatusBooking from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageInputSearchStatusBooking.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('ManageInputSearchStatusBooking.vue', () => {
	let mockStore = {
		state: {
			userRoom: { id: '123' },
			searchBill: { perPage: 3, limit: '', offset: '' }
		},
		mutations: {
			setSearchBillPerPage(state, payload) {
				state.searchBill.perPage = payload;
			},
			setSearchBillLimit(state, payload) {
				state.searchBill.limit = payload;
			},
			setSearchBillOffset(state, payload) {
				state.searchBill.offset = payload;
			}
		}
	};

	it('should render the component', () => {
		const wrapper = shallowMount(ManageInputSearchStatusBooking, {
			localVue,
			store: new Vuex.Store(mockStore)
		});

		expect(wrapper.find('#inputSearchStatusBooking').exists()).toBe(true);
	});

	it('should call get bill method', () => {
		const methods = {
			getBill: jest.fn()
		};

		const wrapper = shallowMount(ManageInputSearchStatusBooking, {
			localVue,
			store: new Vuex.Store(mockStore),
			methods
		});

		wrapper.vm.textSearch = 'mamikos';
		wrapper.vm.getSearch();
		expect(methods.getBill).toBeCalledWith('123', 'mamikos');
	});
});
