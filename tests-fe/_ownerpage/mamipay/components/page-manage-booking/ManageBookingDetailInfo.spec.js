import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingDetailInfo from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingDetailInfo.vue';

describe('ManageBookingDetailInfo', () => {
	const bookingDetailProp = {
		total_renter: 2,
		is_married: false,
		gender: 'female',
		marital_status: 'Belum kawin',
		work_place: 'Astra',
		job: 'Karyawan',
		email: 'user@mail.com',
		parent_name: 'Dante',
		parent_phone_number: '08747565753'
	};
	const tooltip = jest.fn();

	it('should return masked number', () => {
		const wrapper = shallowMount(ManageBookingDetailInfo, {
			directives: {
				tooltip
			},
			propsData: {
				bookingDetail: bookingDetailProp
			}
		});

		expect(wrapper.vm.maskingPhoneNumber('08747565753')).toBe('08747565XXX');
	});

	it('should return is verified true props', () => {
		const wrapper = shallowMount(ManageBookingDetailInfo, {
			directives: { tooltip },
			propsData: {
				bookingDetail: bookingDetailProp
			}
		});

		wrapper.setProps({ isStatusSuccessOrVerified: true });
		expect(wrapper.vm.isStatusSuccessOrVerified).toBe(true);
	});

	it('should return is verified false', () => {
		const isStatusSuccessOrVerified = true;
		const wrapper = shallowMount(ManageBookingDetailInfo, {
			directives: { tooltip },
			propsData: {
				bookingDetail: bookingDetailProp,
				isStatusSuccessOrVerified
			}
		});

		wrapper.setProps({ isStatusSuccessOrVerified: false });
		expect(wrapper.vm.isStatusSuccessOrVerified).toBe(false);
	});
});
