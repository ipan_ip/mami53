import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingListCard from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingListCard.vue';
import dayjs from 'dayjs';
import ownerBookingList from './__mocks__/owner-booking-list.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
mockVLazy(localVue);

describe('ManageBookingListCard.vue', () => {
	const wrapper = shallowMount(ManageBookingListCard, {
		localVue,
		propsData: { bookingData: { ...ownerBookingList[0].action } }
	});

	it('should render manage booking list card', () => {
		expect(wrapper.find('.manage-booking-list-card').exists()).toBe(true);
	});
});
