import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingCountdown from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingCountdown.vue';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

describe('ManageBookingCountdown.vue', () => {
	const mountComponent = (
		expiredDate = dayjs()
			.add(5, 'second')
			.format()
	) => {
		return shallowMount(ManageBookingCountdown, {
			localVue,
			propsData: { expiredDate }
		});
	};

	it('should render manageBookingCountdown', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('#manageBookingCountdown').exists()).toBe(true);
	});

	it('should set timer to expired date', async () => {
		jest.useFakeTimers();
		const wrapper = mountComponent();
		jest.advanceTimersByTime(1000);
		await wrapper.vm.$nextTick();

		expect(wrapper.find('#manageBookingCountdown').text()).toBe('0:00:04');

		jest.useRealTimers();
	});

	it('should set to 00:00:00 if time is up and stop countdown', async () => {
		jest.useFakeTimers();
		const spyClearInterval = jest
			.spyOn(window, 'clearInterval')
			.mockImplementation(jest.fn());
		const wrapper = mountComponent(
			dayjs()
				.add(-5, 'second')
				.format()
		);

		jest.advanceTimersByTime(1000);
		await wrapper.vm.$nextTick();

		expect(wrapper.find('#manageBookingCountdown').text()).toBe('00:00:00');
		expect(spyClearInterval).toBeCalled();

		jest.useRealTimers();
	});
});
