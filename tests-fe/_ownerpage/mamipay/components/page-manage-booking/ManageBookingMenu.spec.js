import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageBookingMenu from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingMenu.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ManageBookingMenu.vue', () => {
	it('shoult emit open detail booking', () => {
		const wrapper = shallowMount(ManageBookingMenu, { localVue });

		const buttonDetailPayment = wrapper.findAll('.menu-link').at(0);
		buttonDetailPayment.trigger('click');
		expect(wrapper.emitted('openDetailBooking')).toBeTruthy();
	});

	it('should emit open reject booking', () => {
		const wrapper = shallowMount(ManageBookingMenu, {
			localVue,
			propsData: { statusBooking: 'booked' }
		});

		const buttonRejectPayment = wrapper.findAll('.menu-link').at(1);
		buttonRejectPayment.trigger('click');
		expect(wrapper.emitted('openRejectBooking')).toBeTruthy();
	});
});
