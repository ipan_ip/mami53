import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingDetail from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingDetail.vue';
import manageBookingList from './__mocks__/owner-booking-list.json';
import dayjs from 'dayjs';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

const $toasted = {
	show: jest.fn()
};

global.clearInterval = jest.fn();
global.setInterval = jest.fn(fn => {
	fn();
});

const bookingDetail = { ...manageBookingList[0].action };

mockWindowProperty('scrollTo', jest.fn());

const createWrapper = (propsData = bookingDetail) =>
	shallowMount(ManageBookingDetail, {
		localVue,
		mocks: { $toasted },
		propsData: { bookingDetail: { ...propsData } }
	});

describe('ManageBookingDetail.vue', () => {
	it('should render all components', () => {
		const wrapper = createWrapper();

		const isExist = selector => wrapper.find(selector).exists();

		expect(isExist('#manageBookingDetail')).toBe(true);
		expect(isExist('.booking-info-card')).toBe(true);
		expect(isExist('.booking-tenant-info')).toBe(true);
		expect(isExist('.booking-property-info')).toBe(true);
	});

	it('should show toast when room is not selected and status is paid', async () => {
		jest.clearAllMocks();
		const wrapper = createWrapper({ ...bookingDetail, status: 'paid' });
		await wrapper.vm.$nextTick();

		expect($toasted.show).toBeCalled();
	});

	it('should show toast when room is not selected and status is confirmed', async () => {
		jest.clearAllMocks();
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'confirmed',
			confirm_expired_date: dayjs()
				.add(1, 'day')
				.format('YYYY-MM-DD')
		});
		await wrapper.vm.$nextTick();

		expect($toasted.show).toBeCalled();
	});

	it('should set overflow to be unset when mounted to support position sticky', async () => {
		const mockContentSection = document.createElement('div');
		mockWindowProperty('document.getElementById', id => {
			if (id === 'contentSection') {
				return mockContentSection;
			}
			return null;
		});

		const wrapper = createWrapper();
		await wrapper.vm.$nextTick();

		expect(mockContentSection.style.overflow).toBe('unset');
	});

	it('should set overflow auto before destroyed', () => {
		const mockContentSection = document.createElement('div');
		mockContentSection.style.overflow = 'unset';
		mockWindowProperty('document.getElementById', id => {
			if (id === 'contentSection') {
				return mockContentSection;
			}
			return null;
		});

		const wrapper = createWrapper();
		wrapper.destroy();

		expect(mockContentSection.style.overflow).toBe('auto');
	});

	it('should remove scroll event listener and set scroll before component destroyed', () => {
		const wrapper = createWrapper();
		const spyAutoScroll = jest.spyOn(wrapper.vm, 'toggleAutoScroll');

		wrapper.destroy();
		expect(spyAutoScroll).toBeCalledWith('remove');
		spyAutoScroll.mockRestore();
	});

	it('should clearInterval and remove auto scroll when user scroll', async () => {
		const wrapper = createWrapper();
		const spyClearInterval = jest.spyOn(global, 'clearInterval');
		const spyAutoScroll = jest.spyOn(wrapper.vm, 'toggleAutoScroll');
		await wrapper.setData({ autoScrollInterval: {} });
		wrapper.vm.onUserScroll();
		expect(spyClearInterval).toBeCalled();
		expect(spyAutoScroll).toBeCalled();
		spyClearInterval.mockRestore();
		spyAutoScroll.mockRestore();
	});

	it('should clearInterval and remove auto scroll when user press up or down keys', async () => {
		const wrapper = createWrapper();
		const spyClearInterval = jest.spyOn(global, 'clearInterval');
		const spyAutoScroll = jest.spyOn(wrapper.vm, 'toggleAutoScroll');
		await wrapper.setData({ autoScrollInterval: {} });
		wrapper.vm.onUserScrollByKeys({ keyCode: 38 });
		expect(spyClearInterval).toBeCalled();
		expect(spyAutoScroll).toBeCalled();
		jest.clearAllMocks();
		wrapper.vm.onUserScrollByKeys({ keyCode: 40 });
		expect(spyClearInterval).toBeCalled();
		expect(spyAutoScroll).toBeCalled();
		spyClearInterval.mockRestore();
		spyAutoScroll.mockRestore();
	});
});
