import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingDetailId from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingDetailId.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ManageBookingDetailId.vue', () => {
	const mocks = {
		$Lazyload: {
			$on: jest.fn((event, onError) => {
				onError({ src: '' });
			})
		}
	};

	const wrapper = shallowMount(ManageBookingDetailId, { localVue, mocks });

	it('should render user identification component', () => {
		expect(wrapper.find('.user-identification').exists()).toBe(true);
	});

	it('should render 2 id containers', () => {
		expect(
			wrapper.findAll('.user-identification__photo-container').length
		).toBe(2);
	});

	it('should render user identification disclaimer if both ids missing', () => {
		expect(wrapper.find('.user-identification__disclaimer').exists()).toBe(
			true
		);
	});

	it('should render user identification disclaimer if one id missing', done => {
		wrapper.setProps({
			idPhoto: 'id-photo-test.jpg'
		});
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.user-identification__disclaimer').exists()).toBe(
				true
			);
			done();
		});
	});

	it('should open modal if loaded photo clicked', done => {
		wrapper.setProps({
			idPhoto: 'id-photo-test.jpg'
		});
		wrapper.vm.$nextTick(() => {
			wrapper.find('[data-path="img_idPhoto"]').trigger('click');
			expect(wrapper.vm.showedPhotoSrc).toBe('id-photo-test.jpg');
			expect(wrapper.vm.isShowImageModal).toBe(true);
			done();
		});
	});

	it('should close modal if button close modal clicked', () => {
		wrapper.find('.close-button').trigger('click');
		expect(wrapper.vm.isShowImageModal).toBe(false);
	});

	it('should not open modal if not loaded photo clicked', done => {
		wrapper.setProps({
			selfiePhoto: ''
		});
		wrapper.vm.$nextTick(() => {
			wrapper.find('[data-path="img_selfiePhoto"]').trigger('click');
			expect(wrapper.vm.isShowImageModal).toBe(false);
			done();
		});
	});

	it('should not render user identification disclaimer if all id are exist', done => {
		wrapper.setData({ photoRequestError: false });
		wrapper.setProps({
			idPhoto: 'id-photo-test.jpg',
			selfiePhoto: 'selfie-photo-test.jpg'
		});
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.user-identification__disclaimer').exists()).toBe(
				false
			);
			done();
		});
	});

	it('should show disclaimer if error on request photo id', done => {
		const propsData = {
			idPhoto: 'id-photo-test.jpg',
			selfiePhoto: 'selfie-photo-test.jpg'
		};
		mocks.$Lazyload = {
			$on: jest.fn((event, onError) => {
				onError({ src: 'id-photo-test.jpg' });
			})
		};
		const wrapper = shallowMount(ManageBookingDetailId, {
			localVue,
			mocks,
			propsData
		});
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.user-identification__disclaimer').exists()).toBe(
				true
			);
			done();
		});
	});
});
