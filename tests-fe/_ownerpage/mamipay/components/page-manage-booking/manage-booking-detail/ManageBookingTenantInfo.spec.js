import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingTenantInfo from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-detail/ManageBookingTenantInfo.vue';
import manageBookingList from '../__mocks__/owner-booking-list.json';
import dayjs from 'dayjs';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;
const bookingDetail = { ...manageBookingList[0].action };

const createWrapper = (propsData = bookingDetail) =>
	shallowMount(ManageBookingTenantInfo, {
		localVue,
		propsData: { bookingDetail: { ...propsData } }
	});

describe('ManageBookingTenantInfo.vue', () => {
	it('should render manage booking tenant info', () => {
		const wrapper = createWrapper();

		expect(wrapper.find('.manage-booking-tenant-info').exists()).toBe(true);
	});

	it('should show phone disclaimer if booking status is not paid or verified', () => {
		const wrapper = createWrapper({ ...bookingDetail, status: 'confirmed' });

		expect(wrapper.find('.tenant-phone-disclaimer').exists()).toBe(true);
	});

	it('should not show phone disclaimer if booking status is paid or verified', () => {
		const wrapper = createWrapper({ ...bookingDetail, status: 'paid' });

		expect(wrapper.find('.tenant-phone-disclaimer').exists()).toBe(false);
	});

	it('shoud show reject reason if booking status is rejected', () => {
		const rejectReason = 'this is reject reason';
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'rejected',
			reject_reason: rejectReason
		});

		expect(wrapper.find('.tenant-description__content').text()).toBe(
			rejectReason
		);
	});

	it('shoud show cancel reason if booking status is cancelled', () => {
		const cancelReason = 'this is cancel reason';
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'cancelled',
			cancel_reason: cancelReason
		});

		expect(wrapper.find('.tenant-description__content').text()).toBe(
			cancelReason
		);
	});

	it('shoud show tenant description if booking status is not rejected or cancelled', () => {
		const tenantDescription = 'this is about me';
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'booked',
			about_renter: tenantDescription
		});

		expect(wrapper.find('.tenant-description__content').text()).toBe(
			tenantDescription
		);
	});

	it('should mask phone number if status is not paid or verified', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'booked',
			phone_number: '08123456789'
		});

		expect(wrapper.find('.tenant-phone').text()).toBe('08123456XXX');
	});

	it('should show full phone number if status is paid or verified', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'paid',
			phone_number: '08123456789'
		});

		expect(wrapper.find('.tenant-phone').text()).toBe('08123456789');
	});

	it('should set default avatar if tenant avatar is empty', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			photo: null
		});

		expect(wrapper.find('.tenant-avatar__image').attributes('data-src')).toBe(
			wrapper.vm.defaultTenantAvatar
		);
	});
});
