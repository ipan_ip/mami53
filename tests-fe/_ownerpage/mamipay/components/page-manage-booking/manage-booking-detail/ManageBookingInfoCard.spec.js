import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingInfoCard from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-detail/ManageBookingInfoCard.vue';
import manageBookingList from '../__mocks__/owner-booking-list.json';
import dayjs from 'dayjs';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
const bookingDetail = { ...manageBookingList[0].action };

const createWrapper = (propsData = bookingDetail) =>
	shallowMount(ManageBookingInfoCard, {
		localVue,
		propsData: { bookingDetail: { ...propsData } }
	});

describe('ManageBookingInfoCard.vue', () => {
	it('should render manage booking info card', () => {
		const wrapper = createWrapper();

		expect(wrapper.find('.manage-booking-info-card').exists()).toBe(true);
	});

	it('should render hypen if date is not valid or empty', async () => {
		const wrapper = createWrapper({
			...bookingDetail,
			checkin_date: '!!@#@@ not a valid date',
			request_date: 'not a valid date too'
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.date-checkin').text()).toBe('-');
		expect(wrapper.find('.card-date').text()).toBe('-');
		expect(wrapper.find('.date-checkout').text()).not.toBe('-');
	});

	it('should set class empty to footer if there is no booking action', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'rejected'
		});

		expect(wrapper.find('.card-footer').classes('is-empty')).toBe(true);
	});

	it('should not set class empty to footer if there is booking action', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			status: 'paid'
		});

		expect(wrapper.find('.card-footer').classes('is-empty')).not.toBe(true);
	});
});
