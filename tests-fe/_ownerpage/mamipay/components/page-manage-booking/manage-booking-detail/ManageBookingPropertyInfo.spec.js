import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingPropertyInfo from 'Js/_ownerpage/mamipay/components/page-manage-booking/manage-booking-detail/ManageBookingPropertyInfo.vue';
import manageBookingList from '../__mocks__/owner-booking-list.json';
import dayjs from 'dayjs';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;
const bookingDetail = { ...manageBookingList[0].action };

const createWrapper = (propsData = bookingDetail) =>
	shallowMount(ManageBookingPropertyInfo, {
		localVue,
		propsData: { bookingDetail: { ...propsData } },
		store: new Vuex.Store({ state: { allDetailRoom: {} } })
	});

describe('ManageBookingPropertyInfo.vue', () => {
	it('should render manage booking tenant info', () => {
		const wrapper = createWrapper();

		expect(wrapper.find('.manage-booking-property-info').exists()).toBe(true);
	});

	it('should show flash sale label if booked with flash sale', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			is_flash_sale: true,
			flash_sale: { discount: 0, price_after_discount: 20000 }
		});

		expect(wrapper.find('.property-flash-sale-label').exists()).toBe(true);
		expect(wrapper.find('.property-flash-sale-price').exists()).toBe(false);
	});

	it('should show flash sale price if booked with flash sale price', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			is_flash_sale: true,
			flash_sale: { discount: '20%', price_after_discount: 20000 }
		});

		expect(wrapper.find('.property-flash-sale-label').exists()).toBe(true);
		expect(wrapper.find('.property-flash-sale-price').exists()).toBe(true);
	});

	it('should show goldplus label if room is goldplus', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			room_allotment: {
				is_gold_plus: true
			}
		});

		expect(wrapper.find('.goldplus-label').exists()).toBe(true);
	});

	it('should show correct property name', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			room_name: 'property name',
			room_subdistrict: 'subdistrict',
			room_city: 'city'
		});

		expect(wrapper.find('.property-name').text()).toBe(
			'property name, subdistrict, city'
		);
	});

	it('should render kamar penuh if room available is 0', () => {
		const wrapper = createWrapper({
			...bookingDetail,
			room_name: 'property name',
			room_subdistrict: 'subdistrict',
			room_available: 0
		});

		expect(wrapper.find('.property-room-available').text()).toBe('Kamar penuh');
	});
});
