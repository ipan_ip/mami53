import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingStatusHandle from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingStatusHandle.vue';
import ownerBookingStatusTest from './__mocks__/owner-booking-status-test.json';

const localVue = createLocalVue();

describe('ManageBookingStatusHandle.vue', () => {
	const wrapper = shallowMount(ManageBookingStatusHandle, {
		localVue,
		propsData: {
			statusBooking: ownerBookingStatusTest[0].status,
			statusLabel: ownerBookingStatusTest[0].label
		}
	});

	it('should render manageBookingStatusHandle', () => {
		expect(wrapper.find('.manage-booking-status').exists()).toBe(true);
	});

	it('should render correct class by given status', () => {
		ownerBookingStatusTest.forEach(bookingStatus => {
			wrapper.setProps({ statusBooking: bookingStatus.status });
			expect(wrapper.vm.bookingStatusClass).toEqual(
				expect.objectContaining({ [bookingStatus.class]: true })
			);
		});
	});
});
