import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingDueDate from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingDueDate.vue';
import dayjs from 'dayjs';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

describe('ManageBookingDueDate.vue', () => {
	const createWrapper = (status = '', dueDate = '') => {
		return shallowMount(ManageBookingDueDate, {
			localVue,
			propsData: {
				status,
				dueDate
			}
		});
	};

	it('should render manage booking due date', () => {
		const wrapper = createWrapper();
		expect(wrapper.find('.manage-booking-due-date').exists()).toBe(true);
	});

	it('should render empty string if status is not booked or confirmed', () => {
		const wrapper = createWrapper('verified');
		expect(wrapper.find('.manage-booking-due-date').text()).toBe('');
	});

	it('should render due date label if status is booked and not expired', () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', tomorrow);
		expect(wrapper.find('.manage-booking-due-date').text()).toEqual(
			expect.stringContaining('Konfirmasi sebelum')
		);
	});

	it('should render due date label if status is confirmed and not expired', () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('confirmed', tomorrow);
		expect(wrapper.find('.manage-booking-due-date').text()).toEqual(
			expect.stringContaining('Pembayaran kedaluwarsa')
		);
	});

	it('should render expired if booking is expired', () => {
		const yesterday = dayjs()
			.subtract(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('confirmed', yesterday);
		expect(wrapper.find('.manage-booking-due-date').text()).toBe('Expired');
	});

	it('should set timer and expired if any', async () => {
		global.setInterval = jest.fn(fn => {
			fn();
		});

		const yesterday = dayjs()
			.subtract(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('confirmed');
		wrapper.vm.setDueDateCountdown(yesterday);
		expect(wrapper.vm.isExpired).toBe(true);
	});

	it('should call clearInterval when component destoryed', () => {
		const wrapper = createWrapper('confirmed');
		const spyClearInterval = jest.spyOn(window, 'clearInterval');
		wrapper.destroy();
		expect(spyClearInterval).toBeCalled();
		spyClearInterval.mockRestore();
	});
});
