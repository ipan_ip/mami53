import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageBookingActionButton from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingActionButton.vue';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import ownerBookingList from './__mocks__/owner-booking-list.json';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus.js', () => {
	const EventBus = {
		$on: jest.fn((event, fn) => {
			fn();
		}),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

const tracker = jest.fn();
global.tracker = tracker;

global.oxWebUrl = 'owner.mamikos.com';
global.open = jest.fn();

const stubs = {
	BgButton: {
		template: `<button class="bg-button" @click="$emit('click')"><slot/></button>`
	}
};

describe('ManageBookingActionButton.vue', () => {
	const bookingData = ownerBookingList[0].action;

	const createWrapper = (
		status = 'booked',
		expiredDate = null,
		roomAvailable = 1
	) => {
		const store = {
			state: {
				allDetailRoom: {
					room_available: roomAvailable
				},
				authData: {
					all: {
						id: 1
					}
				}
			}
		};

		return shallowMount(ManageBookingActionButton, {
			localVue,
			stubs,
			propsData: {
				bookingData: {
					...bookingData,
					status,
					confirm_expired_date: expiredDate
				}
			},
			store: new Vuex.Store(store)
		});
	};

	it('should render manage booking action button', () => {
		const wrapper = createWrapper();
		expect(wrapper.find('.manage-booking-action-button').exists()).toBe(true);
	});

	it('should render button confirm and reject if booking status is booked', () => {
		const wrapper = createWrapper('booked');
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');

		expect(actionButtons.length).toBe(2);
		expect(actionButtons.at(0).text()).toBe('Tolak');
		expect(actionButtons.at(1).text()).toBe('Terima');
	});

	it('should render button chat if booking status is confirmed', () => {
		const wrapper = createWrapper('confirmed');
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');

		expect(actionButtons.length).toBe(1);
		expect(actionButtons.at(0).text()).toBe('Chat Calon Penyewa');
	});

	it('should render button contract if booking status is paid', () => {
		const wrapper = createWrapper('paid');
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');

		expect(actionButtons.length).toBe(1);
		expect(actionButtons.at(0).text()).toBe('Lihat Kontrak');
	});

	it('should render button invoice if booking status is verified', () => {
		const wrapper = createWrapper('verified');
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');

		expect(actionButtons.length).toBe(1);
		expect(actionButtons.at(0).text()).toBe('Lihat Bukti Pembayaran');
	});

	it('should call tracker booking confirm if booking confirm clicked', async () => {
		const wrapper = createWrapper('booked');
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(1).trigger('click');

		expect(tracker).toBeCalledWith('moe', [
			'[Owner] Click Button di List Booking',
			expect.objectContaining({ type_of_button: 'confirm' })
		]);
	});

	it('should call tracker booking reject if booking reject clicked', async () => {
		const wrapper = createWrapper('booked');
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(0).trigger('click');

		expect(tracker).toBeCalledWith('moe', [
			'[Owner] Click Button di List Booking',
			expect.objectContaining({ type_of_button: 'reject' })
		]);
	});

	it('should call tracker chat if chat button clicked', async () => {
		const wrapper = createWrapper('confirmed');
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(0).trigger('click');

		expect(tracker).toBeCalledWith('moe', [
			'[Owner] Click Button di List Booking',
			expect.objectContaining({ type_of_button: 'chat' })
		]);
		expect(global.open).toBeCalledWith(global.oxWebUrl, '_blank');
	});

	it('should emit active expired page when booking is expired on confirm', async () => {
		const yesterday = dayjs()
			.subtract(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', yesterday);
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(1).trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('activeExpiredPage', true);
	});

	it('should emit active expired page when booking is expired on reject', async () => {
		const yesterday = dayjs()
			.subtract(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', yesterday);
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(1).trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('activeExpiredPage', true);
	});

	it('should emit ashowKostFullModal when kost is full', async () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', tomorrow, 0);
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(1).trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('showKostFullModal');
	});

	it('should emit showUpBookingAcceptModal when kost is available', async () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', tomorrow, 1);
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(1).trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith(
			'showUpBookingAcceptModal',
			expect.any(Object)
		);
	});

	it('should emit showUpBookingRejectModal when kost is available', async () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		const wrapper = createWrapper('booked', tomorrow, 1);
		jest.clearAllMocks();
		const actionButtons = wrapper.findAll('.manage-booking-action-button > *');
		await actionButtons.at(0).trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith(
			'showUpBookingRejectModal',
			expect.any(Object)
		);
	});

	it('should open whatsapp when chat to css called', async () => {
		const windowOpen = jest.fn();
		global.open = windowOpen;

		const wrapper = createWrapper('rejected');
		await wrapper.setProps({
			isInfoCard: true,
			bookingData: {
				room_permissions: ['read:customer-service'],
				status: 'rejected'
			}
		});
		await wrapper.vm.$nextTick();

		wrapper.find('.bg-button').trigger('click');

		expect(windowOpen).toBeCalled();

		jest.clearAllMocks();

		await wrapper.setProps({
			isInfoCard: true,
			bookingData: {
				room_permissions: ['read:customer-service'],
				status: 'expired'
			}
		});

		wrapper.find('.bg-button').trigger('click');

		expect(windowOpen).toBeCalled();
	});
});
