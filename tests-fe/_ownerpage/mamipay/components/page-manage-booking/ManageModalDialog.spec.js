import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageModalDialog from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageModalDialog.vue';

const localVue = createLocalVue();
const shallowMountComponent = stubs => {
	return shallowMount(ManageModalDialog, {
		localVue,
		stubs,
		propsData: { showModal: true }
	});
};

describe('ManageModalDialog.vue', () => {
	const wrapper = shallowMountComponent();

	it('should render manage modal dialog', () => {
		expect(wrapper.find('#manageModalDialog').exists()).toBe(true);
	});

	it('should emit confirm when confirm button clicked', () => {
		const confirmButton = wrapper.find('.modal-action .btn-primary');
		confirmButton.trigger('click');
		expect(wrapper.emitted('confirm')[0]).toBeTruthy();
	});

	it('should emit reject when reject button clicked', () => {
		const confirmButton = wrapper.find('.modal-action .btn-success');
		confirmButton.trigger('click');
		expect(wrapper.emitted('reject')[0]).toBeTruthy();
	});

	it('should emit close when modal closed', () => {
		const MamiModalWrap = { template: '<div></div>' };
		const wrapper = shallowMountComponent({ MamiModalWrap });
		wrapper.findComponent(MamiModalWrap).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});
});
