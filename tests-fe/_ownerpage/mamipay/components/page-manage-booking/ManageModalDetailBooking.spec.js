import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageModalDetailBooking from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageModalDetailBooking.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import listBooking from './__mocks__/owner-booking-list.json';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ManageModalDetailBooking.vue', () => {
	const $store = {
		state: {
			manageBooking: {
				listBookingSelected: listBooking[0]
			}
		}
	};

	const mountComponent = (bookingStatus = 'booked') => {
		$store.state.manageBooking.listBookingSelected.status = bookingStatus;
		return shallowMount(ManageModalDetailBooking, {
			localVue,
			propsData: { showModal: true },
			mocks: { $store },
			stubs: {
				modal: `<div class="modal-manage-detail-booking" @closed="$emit('closed')"><slot></slot></div>`
			}
		});
	};

	it('should render manage modal edit room', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('#manageModalEditRoom').exists()).toBe(true);
	});

	it('should set error image if image cannot rendered properly', async () => {
		const wrapper = mountComponent();
		const DEFAULT_ERROR_IMAGE = '/general/img/pictures/default-user-image.png';
		const targetImage = { target: { src: '/link' } };
		wrapper.vm.onImageError(targetImage);
		expect(targetImage.target.src).toBe(DEFAULT_ERROR_IMAGE);
	});

	it('should emit closeModal when modal closed', () => {
		const wrapper = mountComponent();
		jest.clearAllMocks();
		wrapper.find('.modal-manage-detail-booking').trigger('closed');
		expect(wrapper.emitted('closeModal')[0][0]).toBe(false);
	});

	it('should render confirm and reject booking if booking status is booked', () => {
		const wrapper = mountComponent('booked');
		const acceptButton = wrapper.find('.wrapper-footer .btn-primary');
		const rejectButton = wrapper.find('.wrapper-footer .btn-success');
		expect(acceptButton.exists() && rejectButton.exists()).toBe(true);
	});

	it('should emit open modal accept booking if accept button is clicked', () => {
		const wrapper = mountComponent('booked');
		const acceptButton = wrapper.find('.wrapper-footer .btn-primary');
		acceptButton.trigger('click');
		expect(wrapper.emitted('closeModal')[0][0]).toBe(false);
		expect(wrapper.emitted('modalAcceptBooking')[0][0]).toBe(true);
	});

	it('should emit open modal reason booking if reject button is clicked', () => {
		const wrapper = mountComponent('booked');
		const acceptButton = wrapper.find('.wrapper-footer .btn-success');
		acceptButton.trigger('click');
		expect(wrapper.emitted('closeModal')[0][0]).toBe(false);
		expect(wrapper.emitted('modalDialogReason')[0][0]).toBe(true);
	});

	it('should render label Ditolak if booking status is rejected', () => {
		const wrapper = mountComponent('rejected');
		expect(wrapper.find('.wrapper-detail span').text()).toBe('Ditolak');
	});

	it('should return correct gender info', () => {
		const wrapper = mountComponent('rejected');
		const genderList = ['Campur', 'Laki - laki', 'Perempuan'];
		genderList.forEach((gender, id) => {
			$store.state.manageBooking.listBookingSelected.gender = id;
			expect(wrapper.vm.genderType).toBe(gender);
		});
	});
});
