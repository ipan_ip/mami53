import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageBookingPagination from 'Js/_ownerpage/mamipay/components/page-manage-booking/ManageBookingPagination.vue';

const localVue = createLocalVue();

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

describe('ManageBookingPagination.vue', () => {
	const $store = {
		state: {
			manageBooking: {
				searchBooking: {
					totalPage: 2,
					selectedNumberPagination: 1,
					limit: 5
				}
			}
		},
		commit: jest.fn()
	};

	const $dayjs = jest.fn(() => {
		return {
			format: jest.fn()
		};
	});

	const options = {
		localVue,
		mocks: { $store, $dayjs },
		stubs: {
			paginate: {
				template: `<div ref="paginate" class="paginate"><button class="next" @click="changePage(value + 1)">next</button><button class="prev" @click="changePage(value - 1)">prev</button></div>`,
				props: {
					clickHandler: {
						type: Function,
						default: () => {}
					},
					value: {
						type: Number,
						dafault: 0
					}
				},
				methods: {
					changePage(val) {
						this.clickHandler(val);
						this.$emit('change', val);
					}
				}
			}
		}
	};

	it('should render manage booking pagination', () => {
		const wrapper = shallowMount(ManageBookingPagination, options);
		expect(wrapper.find('.manage-booking-pagination').exists()).toBe(true);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set selected page if page change and get booking list related to page selected', () => {
		const wrapper = shallowMount(ManageBookingPagination, options);
		const spyGetBookingList = jest
			.spyOn(wrapper.vm, 'getBookingList')
			.mockImplementation(jest.fn());

		jest.clearAllMocks();
		wrapper.find('.paginate .next').trigger('click');
		wrapper.setData({ selectedNumberPagination: 2 });
		expect(spyGetBookingList).toBeCalledWith({ offset: 5 });
	});
});
