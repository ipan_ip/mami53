import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageOtherCustomActionModal from 'Js/_ownerpage/mamipay/components/other-custom/ManageOtherCustomActionModal.vue';

const localVue = createLocalVue();

describe('ManageOtherCustomActionModal.vue', () => {
	const modal = { template: '<div/>' };
	const wrapper = shallowMount(ManageOtherCustomActionModal, {
		localVue,
		stubs: { modal },
		propsData: {
			showModal: true
		}
	});

	it('should render component', () => {
		expect(wrapper.find('#otherCustomActionModal').exists()).toBe(true);
	});

	it('should emit update show modal if modal closed', () => {
		wrapper.findComponent(modal).vm.$emit('closed');
		expect(wrapper.emitted('update:showModal')[0][0]).toBe(false);
	});
});
