import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from '../../../utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ManageModalBookingConfirmed from 'Js/_ownerpage/mamipay/components/ManageModalBookingConfirmed.vue';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({});
	})
};
global.axios = axios;

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const bugsnagClient = {
	notify: jest.fn(),
	catch: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const $router = { push: jest.fn() };
let mockStore = {
	state: {
		bulkProcessBookingActivation: false,
		isFromGoldPlusFlow: false
	}
};

const swalError = jest.fn();

describe('ManageModalBookingConfirmed', () => {
	let wrapper, _this;

	mockWindowProperty('location', {
		reload: jest.fn()
	});

	mockWindowProperty('oxWebUrl', '/ownerpage');

	const generateWrapper = () => {
		const shallowMountProp = {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: {
				showModal: true,
				isLoading: false,
				seq: 0
			},
			mocks: {
				$router
			}
		};
		return shallowMount(ManageModalBookingConfirmed, shallowMountProp);
	};

	it('Should showing modal', async () => {
		wrapper = generateWrapper();

		const modalWrapper = wrapper.find('modal-stub');
		await wrapper.vm.$nextTick();
		expect(modalWrapper.attributes('value')).toBe('true');
	});

	test('Checking cancel modal function ', async () => {
		wrapper = generateWrapper();

		wrapper.vm.cancelModal();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted().changeComponent).toBeTruthy();
		expect(global.EventBus.$emit).toBeCalledWith('modalActiveBooking', false);
		expect($router.push).toBeCalled();
		expect(location.reload).toBeCalled();
	});

	it('should emit eventbus when bulk request is true', async () => {
		mockStore.state.bulkProcessBookingActivation = true;
		wrapper - generateWrapper();

		wrapper.vm.cancelModal();
		await wrapper.vm.$nextTick();
		expect(EventBus.$emit).toBeCalledWith('modalActiveBooking', false);
		expect(EventBus.$emit).toBeCalledWith('bulkBookingSucceeded');
	});

	it('should change window location to pms page', async () => {
		mockStore.state.isFromGoldPlusFlow = true;
		wrapper = generateWrapper();

		wrapper.vm.cancelModal();
		await wrapper.vm.$nextTick();
		expect(window.location.href).toBe('/ownerpage/goldplus/submission');
	});
});
