import '@babel/polyfill';
import { mount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ManageModalBooking from 'Js/_ownerpage/mamipay/components/ManageModalBooking.vue';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({});
	}),
	post: jest.fn(() => {
		return Promise.resolve({});
	}),
	catch: jest.fn()
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn(),
	catch: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const localVue = createLocalVue();

const $store = {
	state: {
		userRoom: {
			id: ''
		},
		bookingPropertyList: {
			selectedProperties: [1, 2, 3]
		}
	}
};

const tracker = jest.fn();
global.tracker = tracker;

global.alert = jest.fn();

const $router = { push: jest.fn() };

describe('ManageModalBooking', () => {
	let wrapper, _this;

	beforeEach(() => {
		wrapper = mount(ManageModalBooking, {
			localVue,
			propsData: {
				showModal: true,
				selectedComponent: ''
			},
			mocks: {
				$router,
				$store,
				openSwalLoading: jest.fn(),
				swalError: jest.fn(),
				sessionStorage: {
					setItem: jest.fn()
				}
			}
		});

		_this = wrapper.vm;

		mockWindowProperty('location', {
			reload: jest.fn()
		});
	});

	it('Should call emit changeComponent with value ManageModalBookingConfirmation', async () => {
		_this.changeTo();
		expect(wrapper.emitted().changeComponent[0][0]).toBe(
			'ManageModalBookingConfirmation'
		);
	});

	it('Should call emit changeComponent with value ManageModalBookingConfirmed', async () => {
		_this.nextAction();
		expect(wrapper.emitted().changeComponent[0][0]).toBe(
			'ManageModalBookingConfirmed'
		);
	});

	it('Should run tracker if status true', async () => {
		axios.post = jest.fn().mockResolvedValue({
			data: {
				status: true
			}
		});
		global.axios = axios;
		await _this.requestBookingToAdmin();
		expect(tracker).toBeCalled();
		jest.clearAllMocks();
	});

	it('Should run pop up if status false', async () => {
		axios.post = jest.fn().mockResolvedValue({
			data: {
				status: false
			}
		});
		global.axios = axios;

		const swal = jest.fn().mockResolvedValue(true);
		global.swal = swal;

		await _this.requestBookingToAdmin();
		await expect(swal).toBeCalled();
		expect(global.EventBus.$emit).toBeCalledWith('modalActiveBooking', false);
	});

	it('should call requestBookingToAdmin if it is not bulkBookingRequest', async () => {
		_this.$store.state.bulkProcessBookingActivation = false;
		const spyRequestBookingToAdmin = jest.spyOn(_this, 'requestBookingToAdmin');
		await _this.requestBooking();
		expect(spyRequestBookingToAdmin).toBeCalled();
	});

	it('should call bulkBookingRequest method when bulkProcessBookingActivation true', async () => {
		global.axios.post = jest.fn().mockResolvedValue({ data: { status: true } });
		_this.$store.state.bulkProcessBookingActivation = true;
		jest.clearAllMocks();
		await _this.requestBooking();
		expect(wrapper.emitted('changeComponent')[0][0]).toBe(
			'ManageModalBookingConfirmed'
		);
	});

	it('should call bugsnagClient notify if bulk process booking activation return status false', async () => {
		jest.clearAllMocks();
		global.axios.post = jest
			.fn()
			.mockResolvedValue({ data: { status: false } });
		_this.$store.state.bulkProcessBookingActivation = true;
		await _this.requestBooking();
		await wrapper.vm.$nextTick();
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should call bugsnagClient notify if bulk process booking activation failed', async () => {
		jest.clearAllMocks();
		global.axios.post = jest.fn().mockRejectedValue(new Error('error'));
		_this.$store.state.bulkProcessBookingActivation = true;
		await _this.requestBooking();
		await wrapper.vm.$nextTick();
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should render mami loading inline when isLoading is true', async () => {
		await wrapper.setData({ isLoading: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.mami-loading-inline').exists()).toBe(true);
	});
});
