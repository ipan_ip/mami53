import { mount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ManageBookingPrivacyPolicy from 'Js/_ownerpage/mamipay/components/ManageBookingPrivacyPolicy.vue';

const localVue = createLocalVue();

const boundingClientRect = returnVal => {
	mockWindowProperty('document.querySelector', () => {
		return {
			getBoundingClientRect: jest.fn(() => {
				return returnVal;
			})
		};
	});
};

describe('ManageBookingPrivacyPolicy', () => {
	let wrapper, _this;

	beforeEach(() => {
		wrapper = mount(ManageBookingPrivacyPolicy, {
			localVue
		});

		_this = wrapper.vm;
	});

	it('Should return emit true if element in viewport', () => {
		boundingClientRect({ top: 0, left: 0, bottom: 0, right: 0 });
		wrapper.find({ ref: 'textareaPrivacyPolicy' }).trigger('scroll');
		expect(wrapper.emitted().progressReading[0][0]).toBeTruthy();
	});

	it('Should return emit false if element in not viewport', () => {
		boundingClientRect({ top: 0, left: 0, bottom: 800, right: 800 });
		wrapper.find({ ref: 'textareaPrivacyPolicy' }).trigger('scroll');
		expect(wrapper.emitted().progressReading).toBeFalsy();
	});
});
