import { shallowMount, createLocalVue } from '@vue/test-utils';
import RoomAllotmentWrapper from 'Js/_ownerpage/mamipay/components/room-allotment/RoomAllotmentWrapper';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowPoperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const axios = {
	get: jest.fn().mockResolvedValue()
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

global.oxWebUrl = 'https://ownerpage.mamikos.com/';
mockWindowPoperty('window.location', '');

describe('RoomAllotmentWrapper', () => {
	const mockStore = {
		state: {
			userRoom: {
				id: 123
			},
			tenantProfile: {
				tenantProfile: {
					roomNumber: 0,
					roomAllotment: { id: 123 },
					selectedRoomAllotment: null
				}
			},
			addTenant: {
				queryData: {
					room: 112233
				}
			}
		},
		mutations: {
			setRoomNumber(state, payload) {
				state.tenantProfile.roomNumber = payload;
			},
			setRoomAllotment() {}
		}
	};

	const openSwalLoading = jest.fn();
	const closeSwalLoading = jest.fn();

	const mocks = {
		openSwalLoading,
		closeSwalLoading
	};

	const RoomAllotmentSearchInput = {
		template: '<div class="room-allotment-search-input"></div>'
	};

	const generateWrapper = ({ methodsProp = {} } = {}) => {
		const mountProp = {
			localVue,
			store: new Vuex.Store(mockStore),
			propsData: { showModal: true },
			mocks: {
				...mocks,
				$toasted: {
					toasts: [],
					show: jest.fn((eventName, options) => {
						options.action.onClick('event', { goAway: jest.fn() });
					})
				},
				$router: { push: jest.fn() }
			},
			stubs: { RoomAllotmentSearchInput },
			methods: { ...methodsProp }
		};
		return shallowMount(RoomAllotmentWrapper, mountProp);
	};

	it('should assign the room list', async () => {
		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {
					room_available: 2,
					units: [{ id: 123, name: 'Kamar Mawar', floor: 'Lantai 1' }]
				}
			}
		});

		const wrapper = generateWrapper();

		await wrapper.vm.getRoomNumberList();
		expect(wrapper.vm.availableRoom).toBe(2);
		expect(wrapper.vm.rooms.length).toBe(3);
	});

	it('should send error report to bugsnag', async done => {
		axios.get = jest.fn().mockRejectedValue('error');

		const wrapper = generateWrapper();

		await wrapper.vm.getRoomNumberList();
		wrapper.vm.$nextTick(() => {
			expect(bugsnagClient.notify).toBeCalled();
			expect(closeSwalLoading).toBeCalled();
			done();
		});
	});

	it('should emit close modal', () => {
		const wrapper = generateWrapper();

		wrapper.find('.room-add-new span').trigger('click');
		expect(global.location).toBe(
			`${oxWebUrl}kos/rooms/${mockStore.state.userRoom.id}/edit`
		);
	});

	it('should show toast when disabled room clicked', () => {
		const wrapper = generateWrapper();

		wrapper.vm.showWarningToast();
		expect(wrapper.vm.$toasted.show).toBeCalled();
	});

	it('should save the selected room number', () => {
		const wrapper = generateWrapper();
		const spyCloseModal = jest.spyOn(wrapper.vm, 'closeModal');
		wrapper.setData({ roomAllotmentId: '' });
		wrapper.vm.selectRoom();

		expect(spyCloseModal).toBeCalled();
		spyCloseModal.mockRestore();
	});

	it('should reset search value if delete search button clicked', async () => {
		const wrapper = generateWrapper();
		await wrapper.setData({ searchVal: 'search value' });
		wrapper.findComponent(RoomAllotmentSearchInput).vm.$emit('delete');
		expect(wrapper.vm.searchVal).toBe('');
	});

	it('should emit close when closeModal called with close', () => {
		const wrapper = generateWrapper();
		wrapper.vm.closeModal('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should get previous selected room properly', async () => {
		const wrapper = generateWrapper();

		const selectedRoom = {
			id: 123,
			name: 'Kamar Mawar',
			floor: 'Lantai 1'
		};

		await wrapper.setProps({ statusPage: 'member' });
		await wrapper.setData({
			rooms: [{ id: 123, name: 'Kamar Mawar', floor: 'Lantai 1' }]
		});
		wrapper.vm.$store.state.tenantProfile.tenantProfile.selectedRoomAllotment = selectedRoom;
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.previousRoomAllotmentData).toEqual(selectedRoom);
	});

	it('should set roomAllotmentId if tenant profile room number changed', async () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.tenantProfile.tenantProfile.roomNumber = 100;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.roomAllotmentId).toBe(100);
	});

	it('should call select room function if room id existed on query', () => {
		const wrapper = generateWrapper();
		const spySelectRoom = jest.spyOn(wrapper.vm, 'selectRoom');

		wrapper.vm.checkRoomId();

		expect(spySelectRoom).toBeCalledWith(112233);

		jest.clearAllMocks();
		wrapper.vm.$store.state.addTenant.queryData.room = '';
		wrapper.vm.checkRoomId();

		expect(spySelectRoom).not.toBeCalled();

		spySelectRoom.mockRestore();
	});

	it('set virtual keyboard state properly', async () => {
		const wrapper = generateWrapper();
		await wrapper.setData({
			initialHeight: 400,
			showModal: true,
			isVirtualKeyboard: false
		});
		mockWindowPoperty('window.innerHeight', 200);
		wrapper.vm.checkVirtualKeyboard();

		expect(wrapper.vm.isVirtualKeyboard).toBe(true);
	});

	it('should remove event listener resize before component destroyed', () => {
		const wrapper = generateWrapper({ methodsProp: { openSwalLoading } });

		const mockRemoveEventListener = jest.fn();
		mockWindowPoperty('window.removeEventListener', mockRemoveEventListener);
		wrapper.vm.$destroy();

		expect(mockRemoveEventListener).toBeCalled();
	});
});
