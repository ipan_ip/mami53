import { shallowMount, createLocalVue } from '@vue/test-utils';
import Toasted from 'vue-toasted';
import RoomAllotmentSearchInput from 'Js/_ownerpage/mamipay/components/room-allotment/RoomAllotmentSearchInput';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
global.Vue = localVue;
localVue.use(Toasted);

describe('RoomAllotmentSearchInput.vue', () => {
	it('should render the component', () => {
		const wrapper = shallowMount(RoomAllotmentSearchInput, { localVue });
		expect(wrapper.find('#roomAllotmentSearchInput').exists()).toBe(true);
	});

	it('should render the input form', () => {
		const wrapper = shallowMount(RoomAllotmentSearchInput, { localVue });
		expect(wrapper.findAll('input').length).toBe(1);
	});

	it('should emit the input event', () => {
		const wrapper = shallowMount(RoomAllotmentSearchInput, { localVue });
		wrapper.find('input').trigger('input');
		expect(wrapper.emitted('input')).toBeTruthy();
	});

	it('should emit delete if button delete clicked', async () => {
		const wrapper = shallowMount(RoomAllotmentSearchInput, { localVue });
		mockWindowProperty(
			'document.getElementById',
			jest.fn(selector => {
				if (selector === 'roomAllotmentInputElement') {
					return { value: 'something' };
				}
				return {};
			})
		);
		await wrapper.find('.search-delete-value').trigger('click');
		expect(wrapper.emitted('delete')[0]).toBeTruthy();
	});
});
