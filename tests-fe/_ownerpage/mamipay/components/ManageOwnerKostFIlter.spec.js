import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageOwnerKostFilter from 'Js/_ownerpage/mamipay/components/ManageOwnerKostFilter.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import 'dayjs/locale/id';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = { $on: jest.fn(), $emit: jest.fn() };
	global.EventBus = EventBus;
	return global.EventBus;
});

const setTimeout = jest.fn((fn, _) => fn());
mockWindowProperty('setTimeout', setTimeout);

function CancelToken(executor) {
	executor && executor();
}

CancelToken.source = function() {
	return {
		token: '123'
	};
};

const axios = {
	get: jest.fn().mockResolvedValue(),
	CancelToken
};

global.axios = axios;

const stubs = {
	BgDropdown: {
		template: '<div><slot name="trigger"/><slot /></div>'
	}
};

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;
const openSwalLoading = jest.fn();
const swalError = jest.fn();

describe('ManageOwnerKostFilter.vue', () => {
	const mockingStoreMutations = {
		setSearchBookingPerPage() {},
		setSearchBillLimit() {},
		setSearchBillOffset() {},
		setSearchBillYear() {},
		setSearchBillMonth() {},
		setSearchBillCriteria() {},
		setBillReportYear() {},
		setBillReportMonth() {},
		setSearchBookingLimit() {},
		setSearchBookingOffset() {},
		setSearchBookingGroup() {},
		setBookingSelectedNumberPagination() {},
		setBookingListLoading() {}
	};

	const mockStore = {
		state: {
			userRoom: { room_title: '' },
			userRooms: [],
			manageBill: {
				searchBill: { criteria: '' },
				listBillReport: {},
				billReport: { month: '' }
			},
			manageBooking: { searchBooking: { limit: '' } }
		},
		mutations: {
			...mockingStoreMutations,
			setUserRoom(state, payload) {
				state.userRoom = payload;
			},
			addCancelTokenBookingList: jest.fn()
		}
	};

	const generateWrapper = () => {
		const shallowMountProp = {
			localVue,
			stubs,
			store: new Vuex.Store(mockStore),
			mocks: { swalError, openSwalLoading }
		};
		return shallowMount(ManageOwnerKostFilter, shallowMountProp);
	};

	it('should render the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.select-filter-kos').exists()).toBe(true);
	});

	it('should call the api get function after setTimeout', () => {
		const wrapper = generateWrapper();
		const spyGetPropertyKost = jest.spyOn(wrapper.vm, 'getPropertyKost');
		wrapper.vm.onSearchKost();

		expect(setTimeout).toHaveBeenCalled();
		expect(spyGetPropertyKost).toBeCalled();

		spyGetPropertyKost.mockRestore();
	});

	it('should assign filtered room from the fetched data', async () => {
		const wrapper = generateWrapper();
		axios.get = jest.fn().mockResolvedValue({
			data: { status: true, data: ['kost 1', 'kost 2'], total: 2 }
		});
		await flushPromises();
		await wrapper.vm.$nextTick();

		jest.useFakeTimers();

		wrapper
			.find('.select-filter-kos__input-search')
			.vm.$emit('input', 'mamikos');

		jest.advanceTimersByTime(500);

		await flushPromises();

		expect(axios.get).toBeCalledWith(
			'room/list?keyword=mamikos',
			expect.any(Object)
		);

		expect(wrapper.vm.searchedRooms.length).toBe(2);

		axios.get = jest.fn().mockResolvedValue({
			data: { status: true, data: [], total: 0 }
		});
		wrapper.setData({ cancelRequest: jest.fn() });
		await wrapper.vm.$nextTick();

		wrapper
			.find('.select-filter-kos__input-search')
			.vm.$emit('input', 'mamikos2');

		jest.advanceTimersByTime(500);

		await flushPromises();
		await wrapper.vm.$nextTick();

		expect(axios.get).toBeCalledWith(
			'room/list?keyword=mamikos2',
			expect.any(Object)
		);
		expect(wrapper.vm.searchedRooms.length).toBe(0);
		expect(wrapper.find('.select-filter-kos__empty').exists()).toBe(true);

		jest.useRealTimers();
	});

	it('should send error to bugsnag', async () => {
		const wrapper = generateWrapper();
		axios.get = jest.fn().mockRejectedValue(new Error('error'));

		wrapper.vm.keywordSearch = 'mamirooms';
		await wrapper.vm.getPropertyKost();
		expect(axios.get).toBeCalledWith(
			'room/list?keyword=mamirooms',
			expect.any(Object)
		);
		await wrapper.vm.$nextTick();
		expect(global.EventBus.$emit).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should save picked kost data', async () => {
		const wrapper = generateWrapper();

		wrapper.vm.pickKost({
			room_title: 'kost mamirooms mamikos murah di yogyakarta'
		});
		await wrapper.vm.$nextTick();

		expect(
			wrapper.find('.select-filter-kos__input-trigger').attributes('value')
		).toBe('kost mamirooms mamikos murah di yogyakarta');

		wrapper.vm.pickKost({
			room_title: 'kost mamirooms'
		});
		await wrapper.vm.$nextTick();

		expect(
			wrapper.find('.select-filter-kos__input-trigger').attributes('value')
		).toBe('kost mamirooms');
	});
});
