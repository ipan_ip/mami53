import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageInputSearchTenant from 'Js/_ownerpage/mamipay/components/page-manage-bill/ManageInputSearchTenant';
// import MixinMamipayBill from 'Js/_ownerpage/mamipay/mixins/MixinMamipayBill';

describe('ManageInputSearchTenant.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	// localVue.mixin(MixinMamipayBill);
	mockVLazy(localVue);

	const currentTime = new Date();

	const store = new Vuex.Store({
		state: {
			userRoom: [],
			manageBill: {
				searchBill: {
					month: currentTime.getMonth() + 1,
					year: currentTime.getFullYear(),
					criteria: '',
					limit: '',
					offset: '',
					perPage: 3,
					totalPage: 0,
					sort: 'desc',
					order: 'name',
					billDatePeriode: '',
					selectedNumberPagination: 1
				}
			}
		},
		mutations: {
			setUserRoom(state, payload) {
				state.userRoom = payload;
			},
			setSearchBillLimit(state, payload) {
				state.manageBill.searchBill.limit = payload;
			},
			setSearchBillPerPage(state, payload) {
				state.manageBill.searchBill.perPage = payload;
			},
			setSearchBillOffset(state, payload) {
				state.manageBill.searchBill.offset = payload;
			},
			setSearchBillMonth(state, payload) {
				state.manageBill.searchBill.month = payload;
			},
			setBillSelectedNumberPagination(state, payload) {
				state.manageBill.searchBill.selectedNumberPagination = payload;
			}
		}
	});

	const wrapper = shallowMount(ManageInputSearchTenant, {
		localVue,
		store,
		mocks: {
			$store: { commit: jest.fn() }
		},
		propsData: {
			dataLength: 0
		}
	});

	const { vm } = wrapper;

	it('should render props data properly', () => {
		wrapper.setProps({ dataLength: 1 });
		expect(vm.dataLength).toBe(1);
	});

	it('should render userRoomId properly', () => {
		vm.$store.commit('setUserRoom', { id: 85539445 });
		expect(vm.userRoomId).toBe(85539445);
	});

	it('should render billMonth properly', () => {
		vm.$store.commit('setSearchBillMonth', 12);
		expect(vm.billMonth).toBe(12);
	});

	it('should render textSearch properly', () => {
		const input = wrapper.find('.input-filter-kost');
		input.element.value = 'Testing';
		input.trigger('input');
		expect(vm.textSearch).toBe('Testing');
	});

	it('should run getSearch properly', () => {
		vm.getBill = jest.fn();
		const buttonSearch = wrapper.find('.icon-down');
		buttonSearch.trigger('click');
	});
});
