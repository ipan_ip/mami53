const currentTime = new Date();

const PaginationState = {
	state: {
		userRoom: {
			id: 100001
		},
		manageBill: {
			searchBill: {
				month: currentTime.getMonth() + 1,
				year: currentTime.getFullYear(),
				criteria: '',
				limit: '',
				offset: '',
				perPage: 3,
				totalPage: 10,
				sort: 'desc',
				order: 'name',
				billDatePeriode: '',
				selectedNumberPagination: 1
			}
		}
	},
	mutations: {
		setBillSelectedNumberPagination(state, payload) {
			state.manageBill.searchBill.selectedNumberPagination = payload;
		},
		setSearchBillOffset(state, payload) {
			state.manageBill.searchBill.offset = payload;
		}
	}
};

export default PaginationState;
