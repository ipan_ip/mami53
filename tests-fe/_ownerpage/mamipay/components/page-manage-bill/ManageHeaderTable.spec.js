import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import ManageHeaderTable from 'Js/_ownerpage/mamipay/components/page-manage-bill/ManageHeaderTable';

const localVue = createLocalVue();
mockVLazy(localVue);

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

jest.mock('Js/_ownerpage/mamipay/mixins/MixinMamipayBill.js', () => {
	return '';
});

describe('ManageHeaderTable.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(ManageHeaderTable, {
			localVue,
			propsData: {
				tenantId: '100001',
				dataLength: 5
			}
		});
	});

	it('should render periode tagihan', () => {
		expect(wrapper.find('.right-box').exists()).toBeTruthy();
		expect(wrapper.find('.title').text()).toBe('Periode Tagihan');
	});

	it('should render Download Laporan', () => {
		expect(wrapper.find('.left-box').exists()).toBeTruthy();
		expect(wrapper.find('button').text()).toBe('Download laporan');
	});

	it('should show hentikan kontrak button', async () => {
		wrapper.setData({
			isHiddenButtonStopContract: false
		});

		await wrapper.vm.$nextTick;
		expect(wrapper.find('button').text()).toBe('Hentikan Kontrak');
	});

	it('should disabled Hentikan Kontrak button', async () => {
		wrapper.setData({
			isHiddenButtonStopContract: false
		});
		wrapper.setProps({ tenantId: '' });

		await wrapper.vm.$nextTick;
		expect(wrapper.vm.isButtonDisabled).toBe(true);
		expect(wrapper.find('button').attributes('disabled')).toBe('disabled');
	});

	it('should open Repot Bill modal', async () => {
		await wrapper.find('button').trigger('click');
		expect(wrapper.vm.openReportBill).toBe(true);
	});

	it('should run methods', async () => {
		wrapper.setData({
			isHiddenButtonStopContract: false
		});

		wrapper.setMethods({
			openModalReasonStopRent: jest.fn()
		});

		await wrapper.vm.$nextTick;
		wrapper.find('button').trigger('click');
		expect(wrapper.vm.openModalReasonStopRent).toBeCalled();
	});

	it('should emit event bus', async () => {
		wrapper.vm.openModalReasonStopRent();

		await wrapper.vm.$nextTick;
		expect(global.EventBus.$emit).toBeCalledWith('actionModalStopRent', {
			isOpenModal: true,
			contractId: '100001'
		});
	});
});
