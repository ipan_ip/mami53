import Vuex from 'vuex';
import { createLocalVue, mount } from '@vue/test-utils';
import PaginationState from 'ManageBillPagination';
import ManageBillPagination from 'Js/_ownerpage/mamipay/components/page-manage-bill/ManageBillPagination';

jest.mock('Js/_ownerpage/mamipay/mixins/MixinMamipayBill.js', () => {
	return '';
});

describe('ManageBillPagination.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	const wrapper = mount(ManageBillPagination, {
		localVue,
		store: new Vuex.Store(PaginationState)
	});

	wrapper.setMethods({
		getBill: jest.fn()
	});

	it('should render pagination', () => {
		expect(wrapper.find('#manageTablePagination')).toBeTruthy();
		expect(wrapper.find('.pagination-table')).toBeTruthy();
	});

	it('should commit setSearchBillOffset', async () => {
		const store = new Vuex.Store(PaginationState);
		wrapper.vm.pageChanged();
		await wrapper.vm.$nextTick;
		store.commit('setSearchBillOffset', '2');
		expect(store.state.manageBill.searchBill.offset).toBe('2');
		expect(wrapper.vm.getBill).toBeCalled();
	});

	it('should set page 1 as default', () => {
		expect(wrapper.vm.selectedNumberPagination).toBe(1);
	});

	it('should change selectedNumberPagination value', async () => {
		expect(wrapper.vm.selectedNumberPagination).toBe(1);

		const page = wrapper.findAll('a').at(2);
		await page.trigger('click');
		expect(wrapper.vm.selectedNumberPagination).toBe(2);
	});
});
