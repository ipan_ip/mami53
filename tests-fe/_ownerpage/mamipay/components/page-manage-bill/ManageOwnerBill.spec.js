import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import ownerBillJson from './__mocks__/ownerBill.json';
import ownerParams from './__mocks__/ownerBillParam.json';
import 'tests-fe/utils/mock-vue';

jest.mock('Js/_ownerpage/mamipay/mixins/MixinMamipayBill', () => ({}));
const eventBus = {
	$emit: jest.fn()
};
jest.doMock('Js/_ownerpage/mamipay/event-bus/event-bus', () => eventBus);

const ManageOwnerBill = require('Js/_ownerpage/mamipay/components/page-manage-bill/ManageOwnerBill')
	.default;

describe('ManageOwnerBill.vue', () => {
	const currentTime = new Date();
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const store = new Vuex.Store({
		state: {
			userRoom: ownerBillJson,
			userRooms: [ownerBillJson],
			userRoomStatus: {
				booking_active_status: false
			},
			manageBill: {
				searchBill: {
					month: currentTime.getMonth() + 1,
					year: currentTime.getFullYear(),
					criteria: '',
					limit: '',
					offset: '',
					perPage: 3,
					totalPage: 0,
					sort: 'desc',
					order: 'name',
					billDatePeriode: '',
					selectedNumberPagination: 1
				},
				listBill: {}
			}
		},
		mutations: {
			setSearchBillOrder() {},
			setSearchBillSort() {},
			setBillSelectedNumberPagination() {}
		}
	});

	const wrapper = shallowMount(ManageOwnerBill, {
		localVue,
		store,
		stubs: ['router-link'],
		mocks: {
			$store: { commit: jest.fn() },
			$route: { params: { seq: 'all' } },
			$router: { push: jest.fn() },
			$dayjs: jest.fn(() => ({ format: jest.fn() })),
			getUserRooms: jest.fn(),
			getAllDataBill: jest.fn()
		}
	});

	const { vm } = wrapper;

	vm.getBill = jest.fn();

	it('should run searchInput properly', () => {
		vm.searchInput('testing');
		vm.$nextTick(() => {
			expect(vm.querySearch).toBe('testing');
		});
	});

	it('should emit event modalActiveBooking true when Request Aktifkan Fitur Booking is clicked', () => {
		vm.$store.state.userRoom.booking_status = false;

		vm.$nextTick(() => {
			const buttonsEmit = wrapper.find('.btn-primary');
			buttonsEmit.trigger('click');
			expect(eventBus.$emit).toBeCalled();
		});
	});

	it('should run convertToRupiah properly', () => {
		vm.convertToRupiah(200000000);
		expect(vm.convertToRupiah).toBeTruthy();
	});

	it('should run rowsBillLength properly', () => {
		const wrapper = shallowMount(ManageOwnerBill, {
			localVue,
			store,
			stubs: ['router-link'],
			mocks: {
				$store: { commit: jest.fn() },
				$route: { params: { seq: 'all' } },
				$dayjs: jest.fn(() => ({ format: jest.fn() })),
				getUserRooms: jest.fn(),
				getAllDataBill: jest.fn()
			}
		});

		wrapper.vm.$store.state.manageBill.listBill = { data: [{ id: 1 }] };
		expect(wrapper.vm.rowsBillLength).toBe(1);
	});

	it('should run styleControl properly', () => {
		const wrapper = shallowMount(ManageOwnerBill, {
			localVue,
			store,
			stubs: ['router-link'],
			mocks: {
				$store: { commit: jest.fn() },
				$route: { params: { seq: 'all' } },
				$dayjs: jest.fn(() => ({ format: jest.fn() })),
				getUserRooms: jest.fn(),
				getAllDataBill: jest.fn()
			}
		});

		const dataStyleControl = [
			'vgt-table striped',
			'vgt-table striped empty-data'
		];

		expect(wrapper.vm.styleControl).toBe(dataStyleControl[0]);
		wrapper.vm.$store.state.manageBill.listBill = [];
		expect(wrapper.vm.styleControl).toBe(dataStyleControl[1]);
	});

	it('should run userRoomsLength properly with return 0', () => {
		const wrapper = shallowMount(ManageOwnerBill, {
			localVue,
			store,
			stubs: ['router-link'],
			mocks: {
				$store: { commit: jest.fn() },
				$route: { params: { seq: 'all' } },
				$dayjs: jest.fn(() => ({ format: jest.fn() })),
				getUserRooms: jest.fn(),
				getAllDataBill: jest.fn()
			}
		});

		// check userRooms with null value
		wrapper.vm.$store.state.userRooms = null;
		expect(wrapper.vm.userRoomsLength).toBe(0);

		// check userRooms empty array value
		wrapper.vm.$store.state.userRooms = [];
		expect(wrapper.vm.userRoomsLength).toBe(0);
	});

	it('should run created with getUserRooms', () => {
		const wrapper = shallowMount(ManageOwnerBill, {
			localVue,
			store,
			stubs: ['router-link'],
			mocks: {
				$store: { commit: jest.fn() },
				$route: { params: { seq: 'to' } },
				$dayjs: jest.fn(() => ({ format: jest.fn() })),
				getUserRooms: jest.fn(),
				getAllDataBill: jest.fn()
			}
		});

		wrapper.vm.$store.state.userRoom = [];
		expect(wrapper.vm.getUserRooms).toBeCalled();
	});

	it('should run goToDetailBill properly', () => {
		const dataParams = {
			column: {
				field: 'tenantName',
				label: 'Nama Penghuni'
			},
			row: {
				contractId: 123
			}
		};

		vm.goToDetailBill(dataParams);
		expect(vm.$router.push).toBeCalledWith(
			'/profile-tenant/123/payment-tenant'
		);
	});

	it('should run onSortChange properly', () => {
		const dataParams = ownerParams;

		dataParams.forEach(data => {
			vm.onSortChange(data.data);
			expect(vm.getBill).toBeCalled();
		});
	});

	it('should run selectionChanged properly', async () => {
		const arrayValue = [123, 321];
		const valueJoin = arrayValue.join();
		const dataParams = {
			selectedRows: [{ menu: arrayValue[0] }, { menu: arrayValue[1] }]
		};
		vm.selectionChanged(dataParams);
		await vm.$nextTick();
		expect(vm.deleteItem).toBe(valueJoin);
	});

	it('should run onImageError properly', () => {
		const dataParams = {
			target: {
				scr: '/general/img/pictures/default-user-image.png'
			}
		};
		vm.onImageError(dataParams);
		expect(vm.onImageError).toBeTruthy();
	});
});
