import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import 'tests-fe/utils/mock-vue';

jest.mock('Js/_ownerpage/mamipay/mixins/MixinMamipayBill', () => ({}));
const eventBus = {
	$emit: jest.fn()
};
jest.doMock('Js/_ownerpage/mamipay/event-bus/event-bus', () => eventBus);
const ManageBillMenu = require('Js/_ownerpage/mamipay/components/page-manage-bill/ManageBillMenu.vue')
	.default;

const localVue = createLocalVue();
mockVLazy(localVue);
const propsData = {
	tenantId: 11,
	tenantName: 'susan',
	statusTermination: 'noCheckout'
};
describe('ManageBillMenu.vue', () => {
	const wrapper = shallowMount(ManageBillMenu, {
		localVue,
		propsData: { ...propsData },
		stubs: ['router-link']
	});

	it('should mount properly', () => {
		expect(wrapper.find('.menu-link span.green').text()).toBe('Tagihan');
		expect(wrapper.find('.menu-link span.red').exists()).toBe(true);

		const wrapper1 = shallowMount(ManageBillMenu, {
			localVue,
			propsData: { ...propsData, statusTermination: '' },
			stubs: ['router-link']
		});
		expect(wrapper1.find('.menu-link span.red').exists()).toBe(false);
	});

	it('should emit event bus when Hentikan Kontrak menu is clicked', () => {
		const terminateMenu = wrapper.find('a.menu-link:last-child');
		terminateMenu.trigger('click');
		expect(eventBus.$emit).toBeCalled();
	});
});
