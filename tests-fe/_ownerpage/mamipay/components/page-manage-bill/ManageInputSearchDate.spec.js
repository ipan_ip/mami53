import Vuex from 'vuex';
import '../__mocks__/mockToken';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import ManageInputSearchDate from 'Js/_ownerpage/mamipay/components/page-manage-bill/ManageInputSearchDate';
import defaultStore from 'Js/_ownerpage/mamipay/store';

describe('ManageInputSearchDate.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const currentTime = new Date();

	const store = new Vuex.Store(defaultStore);

	const wrapper = shallowMount(ManageInputSearchDate, {
		localVue,
		store,
		mocks: {
			$store: { commit: jest.fn() },
			$dayjs: jest.fn(() => ({ format: jest.fn() }))
		}
	});

	const { vm } = wrapper;

	vm.getBill = jest.fn();
	vm.getReportBill = jest.fn();

	it('should run data properly', () => {
		wrapper.setData({ dateSelected: currentTime });
		expect(vm.dateSelected).toBe(currentTime);
	});

	it('should render userRoomId properly', () => {
		vm.$store.commit('setUserRoom', { id: 85539445 });
		expect(vm.userRoomId).toBe(85539445);
	});

	it('should run formatDate properly', () => {
		vm.formatDate(currentTime);
		expect(vm.formatDate).toBeTruthy();
	});

	it('should run getDataBill properly', () => {
		vm.getDataBill('2020-03-14');
		expect(vm.getBill).toHaveBeenCalledTimes(1);
		expect(vm.getReportBill).toHaveBeenCalledTimes(1);
	});
});
