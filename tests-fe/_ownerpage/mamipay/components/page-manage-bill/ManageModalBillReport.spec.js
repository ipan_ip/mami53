import { createLocalVue, mount } from '@vue/test-utils';
import dayjs from 'dayjs';
import 'tests-fe/utils/mock-vue';
import mockAxios, { axiosFn } from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';
import ManageModalBillReport from 'Js/_ownerpage/mamipay/components/page-manage-bill/ManageModalBillReport.vue';

global.bugsnagClient = { notify: jest.fn() };
const localVue = createLocalVue();
mockVLazy(localVue);
const store = new Vuex.Store({
	state: {
		userRoom: { room_title: 'title', id: 1 },
		manageBill: {
			listBillReport: [
				{
					id: 1,
					name: 'bbbbbbbbbbb',
					status: 'unpaid_late',
					dueDate: '01 Maret 2020',
					rentCost: 1298996500,
					fixedCost: [
						{
							invoice_id: 2522,
							id: 2271,
							name: 'tes tambah biaya',
							amount: 2000,
							original_type: 'fixed'
						}
					],
					additionalCost: [
						{
							invoice_id: 2523,
							name: 'Deposit',
							amount: 800000000,
							original_type: 'deposit'
						},
						{
							invoice_id: 2523,
							name: 'Denda',
							amount: 1000,
							original_type: 'other',
							fine_delay: 47,
							fine_duration: 'day'
						}
					]
				},
				{
					id: 2,
					name: 'bbbbbbbbbbb',
					status: 'unpaid_late',
					dueDate: '10 Maret 2020',
					rentCost: 130000000,
					fixedCost: '-',
					additionalCost: [
						{
							invoice_id: 2524,
							name: 'Deposit',
							amount: 80000000,
							original_type: 'deposit'
						}
					]
				},
				{
					id: 3,
					name: 'brown',
					status: 'unpaid_late',
					dueDate: '23 Maret 2020',
					rentCost: 2300000,
					fixedCost: '-',
					additionalCost: [
						{
							invoice_id: 2022,
							name: 'Deposit',
							amount: 1200000,
							original_type: 'deposit'
						}
					]
				}
			],
			searchBill: { billDatePeriod: 'Mei - 2020' },
			billReport: { month: '3', year: '2020' }
		}
	},
	mutations: {
		setSearchBillPerPage(state, payload) {
			state.manageBill.searchBill.perPage = payload;
		},
		setBillDatePeriode(state, payload) {
			state.manageBill.searchBill.billDatePeriode = payload;
		},
		setSearchBillLimit(state, payload) {
			state.manageBill.searchBill.limit = payload;
		},
		setSearchBillOffset(state, payload) {
			state.manageBill.searchBill.offset = payload;
		},
		setBillReportMonth() {},
		setBillReportYear() {}
	}
});

localVue.prototype.$dayjs = dayjs;
describe('ManageModalBillReport.vue', () => {
	const wrapper = mount(ManageModalBillReport, {
		localVue,
		propsData: {
			showModal: true
		},
		store
	});
	wrapper.setMethods({
		swalError: jest.fn(),
		openSwalLoading: jest.fn()
	});

	it('should render properly', () => {
		expect(wrapper.vm.billDatePeriode).toBe(dayjs().format('MMMM - YYYY'));
		expect(wrapper.findAll('.report-table tbody tr').length).toBe(3);
		expect(wrapper.vm.userRoomId).toBe(1);
	});

	it('should download pdf properly', async () => {
		window.URL.createObjectURL = jest.fn();
		window.URL.revokeObjectURL = jest.fn();
		global.axios = axiosFn(true, { data: '' });
		wrapper.find('.btn.btn-primary').trigger('click');
		await Promise.resolve;
		expect(wrapper.vm.isDisabledDownloadPdf).toBe(false);

		await wrapper.vm.$nextTick;

		global.axios = axiosFn(true);
		wrapper.find('.btn.btn-primary').trigger('click');
		await Promise.reject;
		await Promise.prototype.catch;
		expect(wrapper.vm.isDisabledDownloadPdf).toBe(false);
		expect(wrapper.vm.swalError).toBeCalled();
		expect(window.bugsnagClient.notify).toBeCalled();
	});

	it('should get new report bill ', () => {
		global.axios = mockAxios;
		wrapper.vm.getReportBill = jest.fn();
		wrapper.find('.cell.month').trigger('click');
		expect(wrapper.vm.getReportBill).toBeCalled();
	});

	it('should close modal properly', () => {
		wrapper.find('button.close').trigger('click');
		expect(wrapper.emitted().closeModal[0][0]).toBe(false);
	});
});
