import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ManageModalStopRent from 'Js/_ownerpage/mamipay/components/ManageModalStopRent.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { flushPromises } from 'tests-fe/utils/promises';
import dayjs from 'dayjs';

jest.mock('Js/_ownerpage/mamipay/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({});
	})
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

const tracker = jest.fn();
global.tracker = tracker;

const $toasted = {
	show: jest.fn()
};

mockWindowProperty('document.querySelector', selector => {
	if (selector === '#ManageProfileModalStopRent .modal-content') {
		return document.createElement('div');
	}
});

mockWindowProperty('window.location.reload', jest.fn());

const UserKostStopRentDate = { template: '<div/>' };

describe('ManageModalStopRent.vue', () => {
	let wrapper, _this;

	beforeEach(() => {
		wrapper = shallowMount(ManageModalStopRent, {
			localVue,
			mocks: {
				swalError: jest.fn(),
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				$toasted,
				navigator: {
					isMobile: true
				},
				$refs: {
					wrapperTitle: {
						getBoundingClientRect: () => {
							return {
								top: 20
							};
						}
					}
				}
			},
			stubs: {
				UserKostStopRentDate
			}
		});

		_this = wrapper.vm;
	});

	it('Should showing modal', async () => {
		const modalWrapper = wrapper.find('modal-stub');
		expect(modalWrapper.attributes('value')).toBe(undefined);
		_this.showModal = true;
		await _this.$nextTick();
		expect(modalWrapper.attributes('value')).toBe('true');
	});

	describe('Listing reason', () => {
		it('Should showing list reason stop rent', async () => {
			let wrapperListReason = wrapper.find('ul.wrapper-list-reason');
			let listReason = wrapperListReason.findAll('li');

			expect(listReason.length).toBe(1);

			_this.reasonLists = [
				'Dilarang membawa binatang peliharaan',
				'Setiap hari membersihkan lingkungan',
				'Tidak boleh membuang sampah sembarangan'
			];

			await _this.$nextTick();

			wrapperListReason = wrapper.find('ul.wrapper-list-reason');
			listReason = wrapperListReason.findAll('li');

			expect(listReason.length).toBe(4);
			expect(wrapperListReason).toMatchSnapshot();
		});
	});

	describe('Input textarea other reason', () => {
		it('Should showing input text other reason if owner selected other reason', async () => {
			let wrapperInputArea = wrapper
				.find('textarea.--is-custom-text-area')
				.exists();

			expect(wrapperInputArea).toBeFalsy();

			_this.reasonRadioValue = 'other';

			await _this.$nextTick();

			wrapperInputArea = wrapper
				.find('textarea.--is-custom-text-area')
				.exists();

			expect(wrapperInputArea).toBeTruthy();
		});

		it('Should not showing input text other reason if owner not selected other reason', async () => {
			_this.reasonRadioValue = 'Dilarang membawa binatang peliharaan';

			await _this.$nextTick();

			const wrapperInputArea = wrapper
				.find('textarea.--is-custom-text-area')
				.exists();

			expect(wrapperInputArea).toBeFalsy();
		});

		it('Should counting text input', async () => {
			_this.reasonRadioValue = 'other';

			await _this.$nextTick();

			const reasonTextArea = wrapper.find('textarea.--is-custom-text-area');
			reasonTextArea.setValue('kost nya baguss sekali');

			expect(_this.otherReason).not.toBe('');
			expect(_this.reasonTextCount).toBe(22);
		});

		it('Should disabled button "Konfirmasi Pemberhentian Sewa" if reasonTextCount >= 100', async () => {
			_this.reasonRadioValue = 'other';

			await _this.$nextTick();

			const reasonTextArea = wrapper.find('textarea.--is-custom-text-area');
			reasonTextArea.setValue(
				'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
			);
			const buttonConfirmation = wrapper.find('.btn-submit-termination');

			expect(_this.otherReason).not.toBe('');
			expect(_this.reasonTextCount).toBeGreaterThanOrEqual(100);
			expect(buttonConfirmation.attributes('disabled')).toBeTruthy();
		});
	});

	it('Validation Button Confirmation', () => {
		_this.reasonRadioValue = 'other';
		_this.otherReason = '';
		_this.dateSelected = '';
		_this.isDisabledBtnSubmit = '';

		expect(_this.isButtonSubmitNotValid).toBeTruthy();
	});

	it('Should make data back to initialState when modal closed', () => {
		_this.showModal = true;
		_this.onModalClosedOrCancel();

		const initialData = {
			showModal: false,
			reasonRadioValue: '',
			otherReason: '',
			dateSelected: '',
			isDisabledBtnSubmit: false,
			contractId: '',
			tenantName: '',
			checkoutDateList: [],
			reasonLists: []
		};

		expect(_this.showModal).toBeFalsy();
		expect(_this.$data).toMatchObject(initialData);
	});

	it('Should get Reason List from API', async () => {
		axios.get = jest.fn().mockResolvedValue({
			data: {
				data: [
					'Dilarang membawa binatang peliharaan',
					'Setiap hari membersihkan lingkungan',
					'Tidak boleh membuang sampah sembarangan'
				],
				status: true
			}
		});

		global.axios = axios;

		await _this.getReasonList();

		expect(_this.reasonLists.length).toBe(3);
	});

	it('Should showing toeasted and clear previous data', async () => {
		jest.useFakeTimers();
		const reload = jest.spyOn(global.location, 'reload');
		const initialData = {
			showModal: false,
			reasonRadioValue: '',
			otherReason: '',
			dateSelected: '',
			isDisabledBtnSubmit: false,
			contractId: '',
			tenantName: '',
			checkoutDateList: []
		};

		axios.post = jest.fn().mockResolvedValue({
			data: {
				status: true
			}
		});

		global.axios = axios;

		await _this.submitAllData();
		await flushPromises();

		jest.advanceTimersByTime(1000);
		expect($toasted.show).toBeCalled();
		expect(_this.$data).toMatchObject(initialData);
		expect(reload).toBeCalled();
		reload.mockRestore();
	});

	it('should show toast when response status is false', async () => {
		axios.post = jest.fn().mockResolvedValue({
			data: {
				status: false,
				meta: {
					message: 'status false'
				}
			}
		});

		global.axios = axios;

		await _this.submitAllData();
		await flushPromises();

		expect($toasted.show).toBeCalledWith('status false', expect.any(Object));

		// catch error
		axios.post = jest.fn().mockRejectedValue(new Error('error'));
		global.axios = axios;

		await _this.submitAllData();
		await flushPromises();

		expect(global.EventBus.$emit).toHaveBeenCalledWith('showModalError');
	});

	it('should set date properly', () => {
		wrapper.findComponent(UserKostStopRentDate).vm.$emit('select', new Date());
		expect(wrapper.vm.dateSelected).toBe(
			dayjs(new Date()).format('YYYY-MM-DD')
		);
	});

	it('should set add class --sticky-title when it is sticky', async () => {
		mockWindowProperty('document.querySelector', selector => {
			if (selector === '#ManageProfileModalStopRent .modal-content') {
				return {
					addEventListener: jest.fn(),
					removeEventListener: jest.fn()
				};
			}
		});

		wrapper = shallowMount(ManageModalStopRent, {
			localVue,
			mocks: {
				swalError: jest.fn(),
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				$toasted,
				navigator: {
					isMobile: true
				}
			}
		});

		wrapper.vm.setStickyModalTitle();
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.wrapper-title').classes('--sticky-title')).toBe(true);
	});

	it('should remove scroll listener on before destroy', () => {
		const removeEventListener = jest.fn();
		mockWindowProperty('document.querySelector', () => {
			return {
				removeEventListener
			};
		});
		wrapper.destroy();
		expect(removeEventListener).toBeCalled();
	});
});
