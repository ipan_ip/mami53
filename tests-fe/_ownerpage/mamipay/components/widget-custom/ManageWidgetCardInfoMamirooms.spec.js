import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageWidgetCardInfoMamirooms from 'Js/_ownerpage/mamipay/components/widget-custom/ManageWidgetCardInfoMamirooms.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ManageWidgetCardInfoMamirooms.vue', () => {
	const wrapper = shallowMount(ManageWidgetCardInfoMamirooms, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.info-mamirooms').exists()).toBe(true);
	});
});
