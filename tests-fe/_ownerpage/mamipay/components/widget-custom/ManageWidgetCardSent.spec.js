import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageWidgetCardSent from 'Js/_ownerpage/mamipay/components/widget-custom/ManageWidgetCardSent.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('ManageWidgetCardSent.vue', () => {
	const mockStore = { state: { totalTransfer: 10000000 } };
	const wrapper = shallowMount(ManageWidgetCardSent, {
		localVue,
		store: new Vuex.Store(mockStore)
	});

	it('should load the component', () => {
		expect(wrapper.find('.card-sent').exists()).toBe(true);
	});

	it('should load correct total transfer', () => {
		expect(wrapper.find('.nominal').text()).toBe('Rp10,000,000');
	});
});
