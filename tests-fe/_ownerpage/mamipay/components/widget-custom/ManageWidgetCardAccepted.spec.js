import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ManageWidgetCardAccepted from 'Js/_ownerpage/mamipay/components/widget-custom/ManageWidgetCardAccepted.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('ManageWidgetCardAccepted.vue', () => {
	const mockStore = { state: { totalIncome: 100000000 } };
	const wrapper = shallowMount(ManageWidgetCardAccepted, {
		localVue,
		store: new Vuex.Store(mockStore)
	});

	it('should load the component', () => {
		expect(wrapper.find('.card-accepted').exists()).toBe(true);
	});

	it('should show correct total income', () => {
		expect(wrapper.find('.nominal').text()).toBe('Rp100,000,000');
	});
});
