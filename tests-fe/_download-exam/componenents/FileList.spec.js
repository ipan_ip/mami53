import { shallowMount } from '@vue/test-utils';
import FileList from 'Js/_download-exam/components/FileList';

describe('FileList.vue', () => {
	let wrapper;

	const createComponent = options => {
		wrapper = shallowMount(FileList, options);
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent({
				propsData: {
					files: []
				}
			});
			const findFileListWrapper = () =>
				wrapper.find('.download-exam__file-list');
			expect(findFileListWrapper().exists()).toBe(true);
		});

		it('should render correct list', () => {
			createComponent({
				propsData: {
					files: [
						{ name: 'test', _seq: '/test' },
						{ name: 'test2', _seq: '/test2' }
					]
				}
			});

			const findFileList = () =>
				wrapper.findAll('.download-exam__file-list-item');
			expect(findFileList().exists()).toBe(true);
			expect(findFileList()).toHaveLength(2);
		});
	});
});
