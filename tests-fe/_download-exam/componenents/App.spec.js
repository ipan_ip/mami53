import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import * as storage from 'Js/@utils/storage';

import App from 'Js/_download-exam/components/App.vue';

jest.mock('Js/@utils/makeAPICall.js');

mockWindowProperty('downloadArticle', {
	slug: 'konten',
	examId: '1',
	title: '',
	content: '',
	excerpt: ''
});
mockWindowProperty('isSubscribed', false);
mockWindowProperty('examFiles', []);
mockWindowProperty('authCheck', {
	all: false,
	user: false
});
const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD' // token
	};
});
mockWindowProperty('location', {
	reload: jest.fn()
});
mockWindowProperty('document.head.querySelector', querySelector);

describe('App.vue', () => {
	let wrapper;
	const localVue = createLocalVue();

	const createComponent = options => {
		wrapper = shallowMount(App, {
			localVue,
			stubs: {
				SubscribeForm: mockComponentWithAttrs({
					class: 'subscribe-form',
					'@submit': `$emit('submit', {
              name: 'User',
              email: 'test@test.com',
              phoneNumber: '085888999888',
              currentLocation: 'Tokyo',
              university: 'MIT',
              gender: '0,1',
              destination: 'Tokyo'
            })`
				}),
				ModalFormLogin: mockComponentWithAttrs({
					class: 'modal-login',
					'@click': "$emit('closeModal')"
				}),
				FileList: mockComponentWithAttrs({
					class: 'file-list'
				})
			},
			...options
		});
	};

	const findSubscribeForm = () => wrapper.find('.subscribe-form');
	const findModalLogin = () => wrapper.find('.modal-login');
	const findFileList = () => wrapper.find('.file-list');

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			expect(wrapper.find('.download-exam').exists()).toBe(true);
		});

		it('should show modal form', () => {
			createComponent();

			expect(findSubscribeForm().exists()).toBe(true);
		});

		it('should open modal login when try to submit form before login', () => {
			createComponent();

			const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');
			expect(findSubscribeForm().exists()).toBe(true);
			findSubscribeForm().trigger('submit');
			expect(wrapper.vm.isModalLoginVisible).toBe(true);

			expect(spyStorageLocalSetItem).toHaveBeenCalled();
		});

		it('should hide modal login when click close', async () => {
			createComponent();
			await wrapper.setData({
				isModalLoginVisible: true
			});

			expect(findModalLogin().exists()).toBe(true);
			findModalLogin().trigger('click');
			expect(wrapper.vm.isModalLoginVisible).toBe(false);
		});
	});

	describe('after login', () => {
		beforeEach(() => {
			// Login data
			mockWindowProperty('authCheck', {
				all: true,
				user: true
			});
		});

		it('should send data when there is data in storage', async () => {
			createComponent();

			makeAPICall.mockReturnValueOnce({
				status: 200,
				data: true
			});
			expect(makeAPICall).toHaveBeenCalledTimes(1);
		});

		it('should show list file if already subscribed', () => {
			mockWindowProperty('isSubscribed', true);
			createComponent();

			expect(findFileList().exists()).toBe(true);
		});
	});
});
