import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import SubscribeForm from 'Js/_download-exam/components/SubscribeForm';

import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

describe('SubscribeForm.vue', () => {
	let wrapper;
	const localVue = createLocalVue();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	const createComponent = options => {
		wrapper = shallowMount(SubscribeForm, {
			localVue,
			...options
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();
			const findSubscribeForm = () => wrapper.find('.download-exam__form');
			expect(findSubscribeForm().exists()).toBe(true);
		});

		it('should emit submit event when fired', () => {
			createComponent();

			wrapper.vm.$emit('submit');
			expect(wrapper.emitted().submit).toBeTruthy();
		});
	});
});
