import { mount } from '@vue/test-utils';

import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import ConfirmationModal from 'Js/_admin/abtest/components/ConfirmationModal';
import startMock from '../__mocks__/start_experiment.json';

const mountComponent = (options = {}) => {
	const wrapper = mount(ConfirmationModal, {
		...options,
		propsData: {
			value: true, // <- show the modal
			experimentId: 1,
			modalType: 'start',
			...options.propsData
		},
		stubs: { BgIcon: { template: `<div class="bg-c-icon" />` } }
	});

	return wrapper;
};

jest.mock('Js/@utils/makeAPICall.js');

afterEach(makeAPICall.mockReset);

test('should show emit submit-succeed-callback event when sucessfully start the experiment', async () => {
	const wrapper = mountComponent();

	const confirmBtn = wrapper.find('[data-testid="confirm-btn"]');

	makeAPICall.mockReturnValueOnce(startMock);

	await confirmBtn.trigger('click');

	// button should be disabled while submitting
	expect(confirmBtn.element.disabled).toBe(true);

	await wrapper.vm.$nextTick();

	// modal should disappear
	expect(
		wrapper.find('[data-testid="confirmation-modal"]').exists()
	).toBeFalsy();

	// should emit submit-succeed-callback event
	expect(wrapper.emitted()['submit-succeed-callback']).toBeTruthy();
});

test('should show a native window alert when failed to start the experiment', async () => {
	window.alert = jest.fn();

	const wrapper = mountComponent();

	const confirmBtn = wrapper.find('[data-testid="confirm-btn"]');

	makeAPICall.mockReturnValueOnce(null);

	await confirmBtn.trigger('click');

	// modal should still appears
	expect(
		wrapper.find('[data-testid="confirmation-modal"]').exists()
	).toBeTruthy();

	// Make sure users see the native alert with the correct message
	expect(window.alert).toHaveBeenCalledWith(
		'Something went wrong. Please try again'
	);
});
