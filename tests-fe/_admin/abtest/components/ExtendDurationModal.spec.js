import { mount } from '@vue/test-utils';

import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import ExtendDurationModal from 'Js/_admin/abtest/components/ExtendDurationModal';
import {
	EP_UPDATE_EXPERIMENT,
	EP_DETAIL_EXPERIMENT
} from 'Js/_admin/abtest/constants/endpoints';
import detailMock from '../__mocks__/detail_experiment.json';
import updateMock from '../__mocks__/update_experiment.json';

jest.mock('Js/@utils/makeAPICall.js');

const mountComponent = (options = {}) => {
	makeAPICall.mockReturnValue(detailMock);

	const wrapper = mount(ExtendDurationModal, {
		...options,
		propsData: {
			value: true,
			experimentId: 1,
			...options.propsData
		},
		stubs: { BgIcon: { template: `<div class="bg-c-icon" />` } }
	});

	return wrapper;
};

afterEach(makeAPICall.mockReset);

test('should show emit submit-succeed-callback event when submit succeed', async () => {
	const wrapper = mountComponent();

	const extendDurationModal = wrapper.find(
		'[data-testid="extend-duration-modal"]'
	);

	// make sure the modal shown first
	expect(extendDurationModal.exists()).toBe(true);

	// make sure users see the correct confirmation message
	expect(extendDurationModal.find('.bg-c-modal__body-description').text()).toBe(
		'Extend this experiment duration day?'
	);

	const submitBtn = extendDurationModal.find(
		'[data-testid="confirm-extend-btn"]'
	);

	makeAPICall.mockReturnValueOnce(detailMock); // <- detail data will be requested first
	makeAPICall.mockReturnValueOnce(updateMock);

	await submitBtn.trigger('click');

	// Make sure we request the correct data to API first
	expect(makeAPICall).toHaveBeenNthCalledWith(1, {
		method: 'post',
		url: EP_DETAIL_EXPERIMENT,
		data: {
			experiment_id: 1
		}
	});

	// Make sure we send the correct format & data to API when submitting
	// extend_day_duration is using day_duration value. This is expected, due to product requirement
	expect(makeAPICall).toHaveBeenNthCalledWith(2, {
		method: 'post',
		url: EP_UPDATE_EXPERIMENT,
		data: {
			id: 1,
			name_experiment: detailMock.data.name_experiment,
			day_duration: detailMock.data.day_duration,
			extend_day_duration: detailMock.data.day_duration,
			control_name: detailMock.data.control_name,
			control_value: detailMock.data.control_value,
			varian: [
				{
					varian_type: 'varian_a',
					varian_name: detailMock.data.varian_a_name,
					varian_value: detailMock.data.varian_a_value
				},
				{
					varian_type: 'varian_b',
					varian_name: detailMock.data.varian_b_name,
					varian_value: detailMock.data.varian_b_value
				}
			],
			max_user: detailMock.data.max_user,
			user_type: detailMock.data.user_type,
			owner_option: detailMock.data.owner_option,
			only_active_listing: detailMock.data.only_active_listing,
			goals: detailMock.data.goals.map(i => i.goal).join(',')
		}
	});

	// button should be disabled while submitting
	expect(submitBtn.element.disabled).toBe(true);

	await wrapper.vm.$nextTick();

	// make sure the modal disappear
	expect(wrapper.find('[data-testid="extend-duration-modal"]').exists()).toBe(
		false
	);

	// should emit submit-succeed-callback event
	expect(wrapper.emitted()['submit-succeed-callback']).toBeTruthy();
});

test('should show alert when submit failed', async () => {
	window.alert = jest.fn();

	const wrapper = mountComponent();

	const confirmExtendBtn = wrapper.find('[data-testid="confirm-extend-btn"]');

	makeAPICall.mockReturnValueOnce(null); // detail data will be requested first
	makeAPICall.mockReturnValueOnce(null);

	await confirmExtendBtn.trigger('click');

	// Make sure users see the native alert with the correct message
	expect(window.alert).toHaveBeenCalledWith(
		'Something went wrong. Please try again'
	);

	window.alert.mockClear();
});
