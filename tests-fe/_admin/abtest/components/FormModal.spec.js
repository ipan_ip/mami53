import { mount, createLocalVue } from '@vue/test-utils';
import VeeValidate from 'vee-validate';

import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import FormModal from 'Js/_admin/abtest/components/FormModal';
import { EP_UPDATE_EXPERIMENT } from 'Js/_admin/abtest/constants/endpoints';
import 'Js/_admin/abtest/utils/vee-validate-rules';
import detailMock from '../__mocks__/detail_experiment.json';
import updateMock from '../__mocks__/update_experiment.json';
import createMock from '../__mocks__/create_experiment.json';

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
localVue.use(VeeValidate);

const mountComponent = (options = {}) => {
	makeAPICall.mockReturnValue({
		...detailMock,
		data: {
			...detailMock.data,
			is_active: false,
			start_date: null
		}
	});

	const wrapper = mount(FormModal, {
		...options,
		localVue,
		propsData: {
			value: false,
			experimentId: 1,
			modalType: 'add',
			...options.propsData
		},
		stubs: { BgIcon: { template: `<div class="bg-c-icon" />` } }
	});

	return wrapper;
};

afterEach(makeAPICall.mockReset);

test('should only shows "Owner Option" and "Only Active Listing" form when users choose "owner" on User Type dropdown', async () => {
	const wrapper = mountComponent();
	await wrapper.setProps({ value: true }); // show the modal

	// expect Owner Option and Only Active Listing form to be hidden first
	expect(wrapper.find('[data-testid="ownerOptionForm"]').exists()).toBe(false);
	expect(wrapper.find('[data-testid="onlyActiveListingForm"]').exists()).toBe(
		false
	);

	const userTypeForm = wrapper.findAll('[data-testid="userTypeForm"]').at(1);
	// simulate selecting an item from dropdown
	userTypeForm.element.value = 'user';
	await userTypeForm.trigger('change');

	// expect Owner Option and Only Active Listing form still hidden because it's not "owner"
	expect(wrapper.find('[data-testid="ownerOptionForm"]').exists()).toBe(false);
	expect(wrapper.find('[data-testid="onlyActiveListingForm"]').exists()).toBe(
		false
	);

	// simulate selecting an item from dropdown
	userTypeForm.element.value = 'owner';
	await userTypeForm.trigger('change');

	// expect Owner Option and Only Active Listing form to be shown
	expect(wrapper.find('[data-testid="ownerOptions"]').exists()).toBe(true);
	expect(wrapper.find('[data-testid="onlyActiveListing"]').exists()).toBe(true);
});

test('should close the modal when users decide to cancel the action', async () => {
	const wrapper = mountComponent();
	await wrapper.setProps({ value: true }); // show the modal

	const cancelBtn = wrapper.find('[data-testid="cancel-form-btn"]');

	await cancelBtn.trigger('click');

	// modal should be closed
	expect(wrapper.find('[data-testid="form-modal"]').exists()).toBe(false);
});

test('should show alert when submit failed', async () => {
	window.alert = jest.fn();

	const wrapper = mountComponent();
	await wrapper.setProps({ value: true });

	await wrapper.vm.$nextTick();

	const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

	makeAPICall.mockReturnValueOnce(null);

	// Fill only the required form to simplify the process
	const nameForm = wrapper.find('[data-testid="nameForm"]');
	nameForm.element.value = '[test]-test';
	nameForm.trigger('input');

	const dayDuration = wrapper.find('[data-testid="dayDuration"]');
	dayDuration.element.value = '30';
	dayDuration.trigger('input');

	const sampleUserForm = wrapper.find('[data-testid="sampleUserForm"]');
	sampleUserForm.element.value = '1000';
	sampleUserForm.trigger('input');

	// vee-validate won't work unless we trigger it manually
	await wrapper.vm.$validator.validate();

	// Simulate submit using null
	await submitFormButton.trigger('click');

	await wrapper.vm.$nextTick();

	// Modal should still shown
	expect(wrapper.find('[data-testid="form-modal"]').exists()).toBe(true);

	// Make sure users see the native alert with the correct message
	expect(window.alert).toHaveBeenCalledWith(
		'Something went wrong. Please try again'
	);

	window.alert.mockClear();
});

test('Add experiment should emit submit-succeed-callback event when submit succeed', async () => {
	const wrapper = mountComponent();
	await wrapper.setProps({ value: true });

	// modal should visible
	expect(wrapper.find('[data-testid="form-modal"]').exists()).toBe(true);

	// The loading indicator should NOT be shown
	expect(wrapper.find('[data-testid="loading-detail-data"]').exists()).toBe(
		false
	);

	const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

	// Submit button also should NOT disabled when modal opened
	expect(submitFormButton.element.disabled).toBe(false);

	// Fill all forms
	const nameForm = wrapper.find('[data-testid="nameForm"]');
	nameForm.element.value = '[test]-test';
	nameForm.trigger('input');

	const dayDuration = wrapper.find('[data-testid="dayDuration"]');
	dayDuration.element.value = '30';
	dayDuration.trigger('input');

	const sampleUserForm = wrapper.find('[data-testid="sampleUserForm"]');
	sampleUserForm.element.value = '1000';
	sampleUserForm.trigger('input');

	const userTypeForm = wrapper.findAll('[data-testid="userTypeForm"]').at(1);
	userTypeForm.element.value = 'owner';
	await userTypeForm.trigger('change');

	const ownerOptionsForm = wrapper
		.findAll('[data-testid="ownerOptions"]')
		.at(1);
	ownerOptionsForm.element.value = 'non_premium';
	ownerOptionsForm.trigger('change');

	const onlyActiveListingForm = wrapper
		.findAll('[data-testid="onlyActiveListing"]')
		.at(1);
	onlyActiveListingForm.element.value = 'true';
	onlyActiveListingForm.trigger('change');

	const experimentGoalsForm = wrapper.find(
		'[data-testid="experimentGoalsForm"]'
	);
	experimentGoalsForm.element.value = 'Goal A,Goal B,Goal C';
	experimentGoalsForm.trigger('input');

	const controlNameForm = wrapper.find('[data-testid="controlNameForm"]');
	controlNameForm.element.value = 'test control name';
	controlNameForm.trigger('input');

	const weightControlForm = wrapper.find('[data-testid="weightControlForm"]');
	weightControlForm.element.value = '60';
	weightControlForm.trigger('input');

	const variantANameForm = wrapper.find('[data-testid="variantANameForm"]');
	variantANameForm.element.value = 'a';
	variantANameForm.trigger('input');

	const variantAWeightForm = wrapper.find('[data-testid="variantAWeightForm"]');
	variantAWeightForm.element.value = '100';
	variantAWeightForm.trigger('input');

	const variantBNameForm = wrapper.find('[data-testid="variantBNameForm"]');
	variantBNameForm.element.value = 'b';
	variantBNameForm.trigger('input');

	const variantBWeightForm = wrapper.find('[data-testid="variantBWeightForm"]');
	variantBWeightForm.element.value = '110';
	variantBWeightForm.trigger('input');

	// vee-validate won't work unless we trigger it manually
	await wrapper.vm.$validator.validate();

	// Simulate submit with mocked data
	makeAPICall.mockReturnValueOnce(createMock);
	await submitFormButton.trigger('click');

	// The submit button should disabled when API still process our request
	expect(submitFormButton.element.disabled).toBe(true);

	await wrapper.vm.$nextTick();

	// Modal should be dissapear
	expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(false);

	// should emit submit-succeed-callback event
	expect(wrapper.emitted()['submit-succeed-callback']).toBeTruthy();
});

describe('Edit experiment', () => {
	it('should emit submit-succeed-callback event when submit succeed', async () => {
		const wrapper = mountComponent();
		await wrapper.setProps({ value: true, modalType: 'edit' });

		// modal should visible
		expect(wrapper.find('[data-testid="form-modal"]').exists()).toBe(true);

		const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

		// All forms should be pre-filled using data from API
		const nameForm = wrapper.find('[data-testid="nameForm"]');
		expect(nameForm.element.value).toBe(detailMock.data.name_experiment);

		const dayDuration = wrapper.find('[data-testid="dayDuration"]');
		expect(dayDuration.element.value).toBe(
			String(detailMock.data.day_duration)
		);

		const sampleUserForm = wrapper.find('[data-testid="sampleUserForm"]');
		expect(sampleUserForm.element.value).toBe(String(detailMock.data.max_user));

		const userTypeForm = wrapper.findAll('[data-testid="userTypeForm"]').at(1);
		expect(userTypeForm.element.value).toBe(detailMock.data.user_type);

		const ownerOptionsForm = wrapper
			.findAll('[data-testid="ownerOptions"]')
			.at(1);
		expect(ownerOptionsForm.element.value).toBe(detailMock.data.owner_option);

		const onlyActiveListingForm = wrapper
			.findAll('[data-testid="onlyActiveListing"]')
			.at(1);
		expect(onlyActiveListingForm.element.value).toBe(
			String(detailMock.data.only_active_listing)
		);

		const experimentGoalsForm = wrapper.find(
			'[data-testid="experimentGoalsForm"]'
		);
		expect(experimentGoalsForm.element.value).toBe(
			detailMock.data.goals.map(i => i.goal).join(',')
		);

		const controlNameForm = wrapper.find('[data-testid="controlNameForm"]');
		expect(controlNameForm.element.value).toBe(detailMock.data.control_name);

		const weightControlForm = wrapper.find('[data-testid="weightControlForm"]');
		expect(weightControlForm.element.value).toBe(
			String(detailMock.data.control_value)
		);

		const variantANameForm = wrapper.find('[data-testid="variantANameForm"]');
		expect(variantANameForm.element.value).toBe(detailMock.data.varian_a_name);

		const variantAWeightForm = wrapper.find(
			'[data-testid="variantAWeightForm"]'
		);
		expect(variantAWeightForm.element.value).toBe(
			String(detailMock.data.varian_a_value)
		);

		const variantBNameForm = wrapper.find('[data-testid="variantBNameForm"]');
		expect(variantBNameForm.element.value).toBe(detailMock.data.varian_b_name);

		const variantBWeightForm = wrapper.find(
			'[data-testid="variantBWeightForm"]'
		);
		expect(variantBWeightForm.element.value).toBe(
			String(detailMock.data.varian_b_value)
		);

		// Simulate submit with mocked data
		makeAPICall.mockReturnValueOnce(updateMock);
		await submitFormButton.trigger('click');

		// Make sure we send the correct format & data to API
		expect(makeAPICall).toHaveBeenNthCalledWith(2, {
			method: 'post',
			url: EP_UPDATE_EXPERIMENT,
			data: {
				id: 1,
				name_experiment: detailMock.data.name_experiment,
				day_duration: detailMock.data.day_duration,
				control_name: detailMock.data.control_name,
				control_value: detailMock.data.control_value,
				varian: [
					{
						varian_type: 'varian_a',
						varian_name: detailMock.data.varian_a_name,
						varian_value: detailMock.data.varian_a_value
					},
					{
						varian_type: 'varian_b',
						varian_name: detailMock.data.varian_b_name,
						varian_value: detailMock.data.varian_b_value
					}
				],
				max_user: detailMock.data.max_user,
				user_type: detailMock.data.user_type,
				owner_option: detailMock.data.owner_option,
				only_active_listing: detailMock.data.only_active_listing,
				goals: detailMock.data.goals.map(i => i.goal).join(',')
			}
		});

		// The submit button should disabled when API still process our request
		expect(submitFormButton.element.disabled).toBe(true);

		await wrapper.vm.$nextTick();

		// Modal should be dissapear
		expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(false);

		// should emit submit-succeed-callback event
		expect(wrapper.emitted()['submit-succeed-callback']).toBeTruthy();
	});

	it('should shows an error message when detail data failed to fetch', async () => {
		makeAPICall.mockReturnValueOnce(null);

		const wrapper = mountComponent();
		await wrapper.setProps({ value: true, modalType: 'edit' });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('[data-testid="detail-data-error-msg"]').text()).toBe(
			'Something went wrong when fetching the data'
		);
	});
});
