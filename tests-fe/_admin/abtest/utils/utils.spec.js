import { getStatus } from 'Js/_admin/abtest/utils/helpers.js';

describe('getStatus()', () => {
	it('should return the correct value based on parameter', () => {
		// when isActive is true and "startDate" isn't passed or falsy
		expect(getStatus(true)).toBe('Running');
		expect(getStatus(true, '')).toBe('Running');

		// when isActive is false
		expect(getStatus(false)).toBe('Not Yet');

		// when isActive is false but "startDate" is passed
		expect(getStatus(false, '2020-10-10 10:10:10')).toBe('Done');
	});
});
