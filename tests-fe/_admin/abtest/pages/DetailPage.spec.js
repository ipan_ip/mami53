import { mount, createLocalVue } from '@vue/test-utils';
import VeeValidate from 'vee-validate';

import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import DetailPage from 'Js/_admin/abtest/pages/DetailPage';
import { EP_DETAIL_EXPERIMENT } from 'Js/_admin/abtest/constants/endpoints';
import detailMock from '../__mocks__/detail_experiment.json';
import startMock from '../__mocks__/start_experiment.json';
import deleteMock from '../__mocks__/delete_experiment.json';
import updateMock from '../__mocks__/update_experiment.json';

const routerReplaceMock = jest.fn();

const mountComponent = (options = {}) => {
	makeAPICall.mockReturnValue(detailMock);

	const wrapper = mount(DetailPage, {
		localVue,
		stubs: { BgIcon: { template: `<div class="bg-c-icon" />` } },
		mocks: {
			$route: {
				params: { id: 1 }
			},
			$router: {
				replace: routerReplaceMock
			}
		},
		...options
	});

	return wrapper;
};

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
localVue.use(VeeValidate);

afterEach(makeAPICall.mockReset);

test('should shows all the information properly when fetch data succeed', async () => {
	const wrapper = mountComponent();

	// loading indicator should shows first
	expect(
		wrapper.find('[data-testid="loading-indicator"]').exists()
	).toBeTruthy();

	// loading indicator should disappear
	await wrapper.vm.$nextTick();
	expect(
		wrapper.find('[data-testid="loading-indicator"]').exists()
	).toBeFalsy();

	expect(wrapper).toMatchSnapshot();
});

test('should shows error message when failed to fetch data', async () => {
	makeAPICall.mockReturnValueOnce(null);

	const wrapper = mountComponent();

	// loading indicator should shows first
	expect(
		wrapper.find('[data-testid="loading-indicator"]').exists()
	).toBeTruthy();

	// loading indicator should disappear
	await wrapper.vm.$nextTick();
	expect(
		wrapper.find('[data-testid="loading-indicator"]').exists()
	).toBeFalsy();

	// shows the correct error message
	expect(wrapper.find('[data-testid="error-msg"]').text()).toBe(
		'Something went wrong. Please try again'
	);
});

test('"Start", "Delete" & "Edit" button only shown when the experiment\'s status is "Not Yet"', async () => {
	makeAPICall.mockReturnValueOnce({
		...detailMock,
		data: {
			...detailMock.data,
			is_active: false,
			start_date: null
		}
	});

	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	expect(wrapper.find('[data-testid="start-btn"]').exists()).toBeTruthy();
	expect(wrapper.find('[data-testid="delete-btn"]').exists()).toBeTruthy();
	expect(wrapper.find('[data-testid="edit-btn"]').exists()).toBeTruthy();

	// also make sure these forms are hidden
	expect(wrapper.find('[data-testid="stop-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="extend-btn"]').exists()).toBeFalsy();
});

test('"Stop" & "Extend" button only shown when the experiment\'s status is "Running"', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	expect(wrapper.find('[data-testid="stop-btn"]').exists()).toBeTruthy();
	expect(wrapper.find('[data-testid="extend-btn"]').exists()).toBeTruthy();

	// also make sure these forms are hidden
	expect(wrapper.find('[data-testid="start-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="delete-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="edit-btn"]').exists()).toBeFalsy();
});

test('"Start", "Delete", "Edit", "Stop", & "Extend" button should not visible when the experiment\'s status is "Done"', async () => {
	makeAPICall.mockReturnValueOnce({
		...detailMock,
		data: {
			...detailMock.data,
			is_active: false
		}
	});

	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// also make sure these forms are hidden
	expect(wrapper.find('[data-testid="stop-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="extend-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="start-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="delete-btn"]').exists()).toBeFalsy();
	expect(wrapper.find('[data-testid="edit-btn"]').exists()).toBeFalsy();
});

test('Edit Experiment should show success message when submit succeed', async () => {
	makeAPICall.mockReturnValueOnce({
		...detailMock,
		data: {
			...detailMock.data,
			is_active: false,
			start_date: null
		}
	});

	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	makeAPICall.mockReturnValueOnce(detailMock);

	const editButton = wrapper.find('[data-testid="edit-btn"]');

	// modal should visible when users click edit button on a particular row
	await editButton.trigger('click');
	expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(true);

	const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

	await wrapper.vm.$nextTick();

	// Simulate submit with mocked data
	makeAPICall.mockReturnValueOnce(updateMock);
	await submitFormButton.trigger('click');

	await wrapper.vm.$nextTick();

	// Modal should be dissapear
	expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(false);

	// Make sure users see the success message
	expect(wrapper.find('[data-testid="success-alert"]').text()).toBe(
		'Succesfully update the experiment'
	);
});

test('Extend Experiment should show success message when submit succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	const extendBtn = wrapper.find('[data-testid="extend-btn"]');
	await extendBtn.trigger('click');

	const extendDurationModal = wrapper.find(
		'[data-testid="extend-duration-modal"]'
	);

	// make sure the modal shown first
	expect(extendDurationModal.exists()).toBe(true);

	const submitBtn = extendDurationModal.find(
		'[data-testid="confirm-extend-btn"]'
	);

	makeAPICall.mockReturnValueOnce(detailMock); // <- detail data will be requested first
	makeAPICall.mockReturnValueOnce(updateMock);

	await submitBtn.trigger('click');

	// Make sure we request the correct data to API first
	expect(makeAPICall).toHaveBeenNthCalledWith(2, {
		method: 'post',
		url: EP_DETAIL_EXPERIMENT,
		data: {
			experiment_id: 1
		}
	});

	await wrapper.vm.$nextTick();

	// expect user see the correct feedback message after submit
	expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
		/Duration Day is succesfully extended/i
	);
});

describe('Start Experiment', () => {
	beforeEach(() => {
		makeAPICall.mockReturnValueOnce({
			...detailMock,
			data: {
				...detailMock.data,
				is_active: false,
				start_date: null
			}
		});
	});

	it('should show the modal with the correct confirmation when users click the start button', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const startBtn = wrapper.find('[data-testid="start-btn"]');
		await startBtn.trigger('click');

		// make sure the modal shown
		const confirmationModal = wrapper.find(
			'[data-testid="confirmation-modal"]'
		);
		expect(confirmationModal.exists()).toBe(true);

		// make sure users see the correct modal confirmation
		expect(
			confirmationModal.find('.bg-c-modal__body-description').text()
		).toMatch(/start this experiment\?/i);
	});

	it('should show the success message when sucessfully start the experiment', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const startBtn = wrapper.find('[data-testid="start-btn"]');
		await startBtn.trigger('click');

		const submitBtn = wrapper.find('[data-testid="confirm-btn"]');

		makeAPICall.mockReturnValueOnce(startMock);
		makeAPICall.mockReturnValueOnce(detailMock);

		await submitBtn.trigger('click');

		await wrapper.vm.$nextTick();

		// modal should disappear
		expect(
			wrapper.find('[data-testid="confirmation-modal"]').exists()
		).toBeFalsy();

		await wrapper.vm.$nextTick();

		// expect user see the correct feedback message after submit
		expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
			/succesfully start the experiment/i
		);
	});
});

describe('Stop Experiment', () => {
	it('should show the modal with the correct confirmation when users click the stop button', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const stopBtn = wrapper.find('[data-testid="stop-btn"]');
		await stopBtn.trigger('click');

		// make sure the modal shown
		const confirmationModal = wrapper.find(
			'[data-testid="confirmation-modal"]'
		);
		expect(confirmationModal.exists()).toBe(true);

		// make sure users see the correct modal confirmation
		expect(
			confirmationModal.find('.bg-c-modal__body-description').text()
		).toMatch(/stop this experiment\?/i);
	});

	it('should show the success message when sucessfully stop the experiment', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const stopBtn = wrapper.find('[data-testid="stop-btn"]');
		await stopBtn.trigger('click');

		const submitBtn = wrapper.find('[data-testid="confirm-btn"]');

		makeAPICall.mockReturnValueOnce(startMock);
		makeAPICall.mockReturnValueOnce(detailMock);

		await submitBtn.trigger('click');

		await wrapper.vm.$nextTick();

		// modal should disappear
		expect(
			wrapper.find('[data-testid="confirmation-modal"]').exists()
		).toBeFalsy();

		await wrapper.vm.$nextTick();

		// expect user see the correct feedback message after submit
		expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
			/succesfully stop the experiment/i
		);
	});
});

describe('Delete Experiment', () => {
	beforeEach(() => {
		makeAPICall.mockReturnValueOnce({
			...detailMock,
			data: {
				...detailMock.data,
				is_active: false,
				start_date: null
			}
		});
	});

	it('should show the modal with the correct confirmation when users click the delete button', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const deleteBtn = wrapper.find('[data-testid="delete-btn"]');
		await deleteBtn.trigger('click');

		// make sure the modal shown
		const confirmationModal = wrapper.find(
			'[data-testid="confirmation-modal"]'
		);
		expect(confirmationModal.exists()).toBe(true);

		// make sure users see the correct modal confirmation
		expect(
			confirmationModal.find('.bg-c-modal__body-description').text()
		).toMatch(/delete this experiment\?/i);
	});

	it('should show the success message when sucessfully delete the experiment', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		const deleteBtn = wrapper.find('[data-testid="delete-btn"]');
		await deleteBtn.trigger('click');

		const submitBtn = wrapper.find('[data-testid="confirm-btn"]');

		makeAPICall.mockReturnValueOnce(deleteMock);
		makeAPICall.mockReturnValueOnce(detailMock);

		await submitBtn.trigger('click');

		await wrapper.vm.$nextTick();

		// modal should disappear
		expect(
			wrapper.find('[data-testid="confirmation-modal"]').exists()
		).toBeFalsy();

		await wrapper.vm.$nextTick();

		// expect user see the correct feedback message after submit
		expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
			/experiment deleted/i
		);

		// because the data is deleted, make sure to redirect the user back to list page
		expect(routerReplaceMock).toHaveBeenCalledWith('/');
	});
});
