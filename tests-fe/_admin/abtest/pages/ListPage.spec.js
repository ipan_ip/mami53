/* eslint-disable camelcase */
import { mount, createLocalVue } from '@vue/test-utils';
import VeeValidate from 'vee-validate';

import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { EP_ADD_EXPERIMENT } from 'Js/_admin/abtest/constants/endpoints';
import ListPage from 'Js/_admin/abtest/pages/ListPage';
import FormModal from 'Js/_admin/abtest/components/FormModal';
import 'Js/_admin/abtest/utils/vee-validate-rules';
import listExperimentMockData from '../__mocks__/list_experiment.json';
import startExperimentMockData from '../__mocks__/start_experiment.json';
import stopExperimentMockData from '../__mocks__/stop_experiment.json';
import deleteExperimentMockData from '../__mocks__/delete_experiment.json';
import createExperimentMockData from '../__mocks__/create_experiment.json';
import detailMock from '../__mocks__/detail_experiment.json';
import updateExperimentMockData from '../__mocks__/update_experiment.json';

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
localVue.use(VeeValidate);

const mountComponent = (options = {}) => {
	makeAPICall.mockReturnValue({
		data: listExperimentMockData.data
	});

	const wrapper = mount(ListPage, {
		localVue,
		stubs: { BgIcon: { template: `<div class="bg-c-icon" />` } },
		options
	});

	return wrapper;
};

afterEach(makeAPICall.mockReset);

test('should show loading indicator first before fetch the experiment data', async () => {
	const wrapper = mountComponent();

	const loadingSelector = '[data-testid="loading-indicator"]';

	expect(wrapper.find(loadingSelector).exists()).toBe(true);

	await wrapper.vm.$nextTick();

	expect(wrapper.find(loadingSelector).exists()).toBe(false);
});

test('should render all experiment data', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	expect(wrapper).toMatchSnapshot();
});

test('should show error message when experiment data fetch process fail', async () => {
	makeAPICall.mockReturnValueOnce(null);

	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	expect(wrapper.find('[data-testid="error-msg"]').text()).toBe(
		'Something went wrong. Please try again'
	);
});

test('"Start", "Delete" & "Edit" button only shown when a particular experiment\'s status is "Not Yet"', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the first experiment's status is "Not Yet". So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(0);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	expect(optionDrodownItemsBtn.length).toBe(4);
	expect(optionDrodownItemsBtn.at(0).text()).toBe('Start');
	expect(optionDrodownItemsBtn.at(1).text()).toBe('Monitor');
	expect(optionDrodownItemsBtn.at(2).text()).toBe('Delete');
	expect(optionDrodownItemsBtn.at(3).text()).toBe('Edit');
});

test('"Stop" & "Extend" button only shown when a particular experiment\'s status is "Running"', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the second experiment's status is "Running". So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(1);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	expect(optionDrodownItemsBtn.length).toBe(3);
	expect(optionDrodownItemsBtn.at(0).text()).toBe('Monitor');
	expect(optionDrodownItemsBtn.at(1).text()).toBe('Stop');
	expect(optionDrodownItemsBtn.at(2).text()).toBe('Extend');
});

test('"Start", "Delete", "Edit", "Stop", & "Extend" button should not visible when a particular experiment\'s status is "Done"', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the third experiment's status is "Done". So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(2);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	expect(optionDrodownItemsBtn.length).toBe(1);
	expect(optionDrodownItemsBtn.at(0).text()).toBe('Monitor');
});

test('should show the correct confirmation modal when "Start" button clicked. Then, it should show the success message after submit process succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the first experiment's status is "Not Yet".
	// Thus, it'll have "Start" button shown. So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(0);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	const startBtn = optionDrodownItemsBtn.at(0);

	// make sure we're selecting the right button
	expect(startBtn.text()).toBe('Start');

	await startBtn.trigger('click');

	// make sure the modal shown first
	const confirmationModal = wrapper.find('[data-testid="confirmation-modal"]');
	expect(confirmationModal.exists()).toBe(true);

	// make sure users see the correct confirmation message
	expect(
		confirmationModal.find('.bg-c-modal__body-description').text()
	).toMatch(/start this experiment\?/i);

	const submitBtn = confirmationModal.find('[data-testid="confirm-btn"]');

	makeAPICall.mockReturnValueOnce(startExperimentMockData);

	await submitBtn.trigger('click');

	await wrapper.vm.$nextTick();

	// expect user see the correct feedback message after submit
	expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
		/succesfully start the experiment/i
	);
});

test('should show the correct confirmation modal when "Delete" button clicked. Then, it should show the success message after submit process succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the first experiment's status is "Not Yet".
	// Thus, it'll have "Delete" button shown. So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(0);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	const deleteBtn = optionDrodownItemsBtn.at(2);

	// make sure we're selecting the right button
	expect(deleteBtn.text()).toBe('Delete');

	await deleteBtn.trigger('click');

	// make sure the modal shown first
	const confirmationModal = wrapper.find('[data-testid="confirmation-modal"]');
	expect(confirmationModal.exists()).toBe(true);

	// make sure users see the correct confirmation message
	expect(
		confirmationModal.find('.bg-c-modal__body-description').text()
	).toMatch(/delete this experiment\?/i);

	const submitBtn = confirmationModal.find('[data-testid="confirm-btn"]');

	makeAPICall.mockReturnValueOnce(deleteExperimentMockData);

	await submitBtn.trigger('click');

	await wrapper.vm.$nextTick();

	// expect user see the correct feedback message after submit
	expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
		/experiment deleted/i
	);
});

test('should show the correct confirmation modal when "Stop" button clicked. Then, it should show the success message after submit process succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the second experiment's status is "Running".
	// Thus, it'll have "Stop" button shown. So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(1);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	const stopBtn = optionDrodownItemsBtn.at(1);

	// make sure we're selecting the right button
	expect(stopBtn.text()).toBe('Stop');

	await stopBtn.trigger('click');

	// make sure the modal shown first
	const confirmationModal = wrapper.find('[data-testid="confirmation-modal"]');
	expect(confirmationModal.exists()).toBe(true);

	expect(confirmationModal.find('.bg-c-modal__body-description').text()).toBe(
		'Stop this experiment?'
	);

	expect(
		confirmationModal.find('.bg-c-modal__body-description').text()
	).toMatch(/stop this experiment\?/i);

	const submitBtn = confirmationModal.find('[data-testid="confirm-btn"]');

	makeAPICall.mockReturnValueOnce(stopExperimentMockData);

	await submitBtn.trigger('click');

	await wrapper.vm.$nextTick();

	// expect user see the correct feedback message after submit
	expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
		/succesfully stop the experiment/i
	);
});

test('Add Experiment should show success message when submit succeed', async () => {
	const wrapper = mountComponent();

	const addButton = wrapper.find('[data-testid="add-new-btn"]');
	await addButton.trigger('click');

	const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

	// Fill all forms
	const nameForm = wrapper.find('[data-testid="nameForm"]');
	nameForm.element.value = '[test]-test';
	nameForm.trigger('input');

	const dayDuration = wrapper.find('[data-testid="dayDuration"]');
	dayDuration.element.value = '30';
	dayDuration.trigger('input');

	const sampleUserForm = wrapper.find('[data-testid="sampleUserForm"]');
	sampleUserForm.element.value = '1000';
	sampleUserForm.trigger('input');

	const userTypeForm = wrapper.findAll('[data-testid="userTypeForm"]').at(1);
	userTypeForm.element.value = 'owner';
	await userTypeForm.trigger('change');

	const ownerOptionsForm = wrapper
		.findAll('[data-testid="ownerOptions"]')
		.at(1);
	ownerOptionsForm.element.value = 'non_premium';
	ownerOptionsForm.trigger('change');

	const onlyActiveListingForm = wrapper
		.findAll('[data-testid="onlyActiveListing"]')
		.at(1);
	onlyActiveListingForm.element.value = 'true';
	onlyActiveListingForm.trigger('change');

	const experimentGoalsForm = wrapper.find(
		'[data-testid="experimentGoalsForm"]'
	);
	experimentGoalsForm.element.value = 'Goal A,Goal B,Goal C';
	experimentGoalsForm.trigger('input');

	const controlNameForm = wrapper.find('[data-testid="controlNameForm"]');
	controlNameForm.element.value = 'test control name';
	controlNameForm.trigger('input');

	const weightControlForm = wrapper.find('[data-testid="weightControlForm"]');
	weightControlForm.element.value = '60';
	weightControlForm.trigger('input');

	const variantANameForm = wrapper.find('[data-testid="variantANameForm"]');
	variantANameForm.element.value = 'a';
	variantANameForm.trigger('input');

	const variantAWeightForm = wrapper.find('[data-testid="variantAWeightForm"]');
	variantAWeightForm.element.value = '100';
	variantAWeightForm.trigger('input');

	const variantBNameForm = wrapper.find('[data-testid="variantBNameForm"]');
	variantBNameForm.element.value = 'b';
	variantBNameForm.trigger('input');

	const variantBWeightForm = wrapper.find('[data-testid="variantBWeightForm"]');
	variantBWeightForm.element.value = '110';
	variantBWeightForm.trigger('input');

	// vee-validate won't work unless we trigger it manually
	await wrapper.findComponent(FormModal).vm.$validator.validate();

	expect(submitFormButton.element.disabled).toBe(false);

	// Simulate submit with mocked data
	makeAPICall.mockReturnValueOnce(createExperimentMockData);
	await submitFormButton.trigger('click');

	// Make sure we send the correct format & data to API
	expect(makeAPICall).toHaveBeenNthCalledWith(2, {
		method: 'post',
		url: EP_ADD_EXPERIMENT,
		data: {
			name_experiment: '[test]-test',
			day_duration: '30',
			control_name: 'test control name',
			control_value: '60',
			varian: [
				{
					varian_type: 'varian_a',
					varian_name: 'a',
					varian_value: '100'
				},
				{
					varian_type: 'varian_b',
					varian_name: 'b',
					varian_value: '110'
				}
			],
			max_user: '1000',
			user_type: 'owner',
			owner_option: 'non_premium',
			only_active_listing: true,
			goals: 'Goal A,Goal B,Goal C'
		}
	});

	await wrapper.vm.$nextTick();

	// Make sure users see the success message
	expect(wrapper.find('[data-testid="success-alert"]').text()).toBe(
		'Succesfully create the experiment'
	);
});

test('Edit Experiment should show success message when submit succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	makeAPICall.mockReturnValueOnce(detailMock);

	const editButton = wrapper.findAll('[data-testid="edit-btn"]').at(0);

	// modal should visible when users click edit button on a particular row
	await editButton.trigger('click');
	expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(true);

	const submitFormButton = wrapper.find('[data-testid="submit-form-btn"]');

	await wrapper.vm.$nextTick();

	// Simulate submit with mocked data
	makeAPICall.mockReturnValueOnce(updateExperimentMockData);
	await submitFormButton.trigger('click');

	await wrapper.vm.$nextTick();

	// Modal should be dissapear
	expect(wrapper.find('[data-testid="modalForm"]').exists()).toBe(false);

	// Make sure users see the success message
	expect(wrapper.find('[data-testid="success-alert"]').text()).toBe(
		'Succesfully update the experiment'
	);
});

test('Extend Experiment should show success message when submit succeed', async () => {
	const wrapper = mountComponent();

	await wrapper.vm.$nextTick();

	// based on the mock data, the second experiment's status is "Running".
	// Thus, it'll have "Extend" button shown. So we use it.
	const optionDropdownBtn = wrapper.findAll('[data-testid="option-btn"]').at(1);

	const optionDrodownItemsBtn = optionDropdownBtn.findAll('a');

	const extendBtn = optionDrodownItemsBtn.at(2);

	// make sure we're clicking the right button
	expect(extendBtn.text()).toBe('Extend');

	await extendBtn.trigger('click');

	const extendDurationModal = wrapper.find(
		'[data-testid="extend-duration-modal"]'
	);

	// make sure the modal shown first
	expect(extendDurationModal.exists()).toBe(true);

	// make sure users see the correct confirmation message
	expect(extendDurationModal.find('.bg-c-modal__body-description').text()).toBe(
		'Extend this experiment duration day?'
	);

	const submitBtn = extendDurationModal.find(
		'[data-testid="confirm-extend-btn"]'
	);

	makeAPICall.mockReturnValueOnce(detailMock); // <- detail data will be requested first
	makeAPICall.mockReturnValueOnce(updateExperimentMockData);

	await submitBtn.trigger('click');

	// button should be disabled while submitting
	expect(submitBtn.element.disabled).toBe(true);

	await wrapper.vm.$nextTick();

	// // expect user see the correct feedback message after submit
	expect(wrapper.find('[data-testid="success-alert"]').text()).toMatch(
		/Duration Day is succesfully extended/i
	);
});
