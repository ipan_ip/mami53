import { createLocalVue, shallowMount } from '@vue/test-utils';
import lazy from '../../utils/mock-v-lazy/index';
import mockComponent from '../../utils/mock-component/index';
import CareerFooter from 'Js/_career/components/CareerFooter.vue';

const localVue = createLocalVue();
lazy(localVue);

describe('CareerFooter.vue', () => {
	const wrapper = shallowMount(CareerFooter, {
		localVue,
		stubs: { MamiFooter: mockComponent }
	});

	it('should render career footer', () => {
		expect(wrapper.find('footer').exists()).toBe(true);
	});
});
