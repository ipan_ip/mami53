import { createLocalVue, shallowMount } from '@vue/test-utils';
import lazy from '../../utils/mock-v-lazy/index';
import CareerNavbar from 'Js/_career/components/CareerNavbar.vue';

const localVue = createLocalVue();
lazy(localVue);
let wrapper = null;

jest.mock('Js/@mixins/MixinListenerBurgerNavigation', () => {
	return '';
});

describe('CareerNavbar.vue', () => {
	const wrapper = shallowMount(CareerNavbar, {
		localVue
	});

	it('should render career navbar', () => {
		expect(wrapper.find('.navbar-career').exists()).toBe(true);
	});
});
