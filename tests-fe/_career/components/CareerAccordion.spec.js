import { createLocalVue, shallowMount } from '@vue/test-utils';
import vacancies from './__mocks__/vacancies.json';
import mockVLazy from '../../utils/mock-v-lazy';
import CareerAccordion from 'Js/_career/components/CareerAccordion.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CareerAccordion.vue', () => {
	const propsData = { vacancies: [vacancies] };
	const wrapper = shallowMount(CareerAccordion, { localVue, propsData });

	it('should render career navbar', () => {
		expect(wrapper.find('.career-container').exists()).toBe(true);
	});

	it('should have correct vacancy position', () => {
		expect(wrapper.props().vacancies[0].position.name).toBe(
			vacancies.position.name
		);
	});
});
