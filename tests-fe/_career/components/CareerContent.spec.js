import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockComponent from '../../utils/mock-component';
import vacancies from './__mocks__/vacancies.json';
import mockVLazy from '../../utils/mock-v-lazy';
import CareerContent from 'Js/_career/components/CareerContent.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CareerContent.vue', () => {
	const propsData = { vacancies: [vacancies] };
	const wrapper = shallowMount(CareerContent, {
		localVue,
		propsData,
		stubs: { CareerAccordion: mockComponent }
	});

	it('should render career content', () => {
		expect(wrapper.find('.container').exists()).toBe(true);
	});
});
