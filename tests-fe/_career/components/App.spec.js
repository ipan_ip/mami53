import { createLocalVue, shallowMount } from '@vue/test-utils';
import App from 'Js/_career/components/App.vue';
import mockComponent from '../../utils/mock-component';
import mockWindowProperty from '../../utils/mock-window-property';
import vacancies from './__mocks__/vacancies.json';

const localVue = createLocalVue();

const axios = {
	get: jest.fn(() => Promise.resolve({ data: { data: [vacancies] } }))
};
const axiosError = {
	get: jest.fn(() => Promise.reject({ data: { data: [vacancies] } }))
};
const notify = jest.fn();

global.axios = axios;

describe('App.vue', () => {
	const wrapper = shallowMount(App, {
		localVue,
		stubs: {
			CareerNavbar: mockComponent,
			CareerHeader: mockComponent,
			CareerContent: mockComponent,
			CareerFooter: mockComponent
		}
	});

	it('should render div element', () => {
		expect(wrapper.find('div').exists()).toBe(true);
	});

	describe('Reject promise', () => {
		global.window = mockWindowProperty('bugsnagClient', { notify });

		global.axios = axiosError;
		const wrapper = shallowMount(App, {
			localVue,
			stubs: {
				CareerNavbar: mockComponent,
				CareerHeader: mockComponent,
				CareerContent: mockComponent,
				CareerFooter: mockComponent
			}
		});

		it('should call bugsnag if error', () => {
			wrapper.vm.getVacancyList();
			expect(notify).toBeCalled();
		});
	});
});
