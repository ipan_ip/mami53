import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsAbout from 'Js/_mamirooms/landing/components/MamiroomsAbout';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
jest.mock('Js/_mamirooms/landing/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('MamiroomsAbout.vue', () => {
	const wrapper = shallowMount(MamiroomsAbout, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.mamirooms-about').exists()).toBe(true);
	});

	it('should toggle modal register', () => {
		const buttonContact = wrapper.find('.btn-about');
		buttonContact.trigger('click');
		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', true);
	});

	it('should set correct value to scrolled past and navbar fixed when window onscroll called', () => {
		window.pageYOffset = 700;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(false);
		expect(wrapper.vm.isNavbarFixed).toBe(false);
		window.pageYOffset = 1000;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(false);
		expect(wrapper.vm.isNavbarFixed).toBe(true);
		window.pageYOffset = 1500;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(true);
		expect(wrapper.vm.isNavbarFixed).toBe(true);
	});
});
