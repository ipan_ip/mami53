import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsFeatures from 'Js/_mamirooms/landing/components/MamiroomsFeatures';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
jest.mock('Js/_mamirooms/landing/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('MamiroomsFeatures.vue', () => {
	const wrapper = shallowMount(MamiroomsFeatures, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.mamirooms-features').exists()).toBe(true);
	});

	it('should toggle modal register', () => {
		const buttonContact = wrapper.find('.btn-download');
		buttonContact.trigger('click');
		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', true);
	});
});
