import { createLocalVue, shallowMount } from '@vue/test-utils';
import App from 'Js/_mamirooms/landing/components/App';

const localVue = createLocalVue();
jest.mock('Js/_mamirooms/landing/event-bus', () => {
	const EventBus = {
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('App.vue', () => {
	const wrapper = shallowMount(App, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.app-wrapper').exists()).toBe(true);
	});

	it('listen to eventbus event', () => {
		wrapper.vm.listenEventBus();
		expect(global.EventBus.$on).toBeCalledWith(
			'toggleModalRegister',
			expect.any(Function)
		);
	});

	it('should close the modal register', () => {
		wrapper.setData({ modalShow: true });
		wrapper.vm.cancelModal();
		expect(wrapper.vm.modalShow).toBe(false);
	});

	it('should change gallery state', () => {
		wrapper.setData({ stateGallery: false });
		wrapper.vm.changeGallery(true);
		expect(wrapper.vm.stateGallery).toBe(true);
	});

	it('should set correct value to scrolled past and navbar fixed when window onscroll called', () => {
		window.pageYOffset = 700;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(false);
		expect(wrapper.vm.isNavbarFixed).toBe(false);
		window.pageYOffset = 1000;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(false);
		expect(wrapper.vm.isNavbarFixed).toBe(true);
		window.pageYOffset = 1500;
		window.dispatchEvent(new window.UIEvent('scroll', { detail: 2000 }));
		expect(wrapper.vm.isScrolledPast).toBe(true);
		expect(wrapper.vm.isNavbarFixed).toBe(true);
	});
});
