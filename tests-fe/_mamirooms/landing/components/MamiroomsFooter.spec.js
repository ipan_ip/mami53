import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsFooter from 'Js/_mamirooms/landing/components/MamiroomsFooter';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('MamiroomsFooter.vue', () => {
	const wrapper = shallowMount(MamiroomsFooter, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.mamirooms-footer').exists()).toBe(true);
	});
});
