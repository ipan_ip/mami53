import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsPreviewGalery from 'Js/_mamirooms/landing/components/MamiroomsPreviewGalery.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('MamiroomsPreviewGalery.vue', () => {
	const wrapper = shallowMount(MamiroomsPreviewGalery, { localVue });

	it('should render section gallery', () => {
		expect(wrapper.find('#mamiroomsPreviewGalery').exists()).toBe(true);
	});

	it('should render all galery data', () => {
		const gallery = wrapper.findAll(
			'.bg-list-konten > div:not(#buttonGallery)'
		);

		expect(gallery.length).toBe(wrapper.vm.listGalery.length);

		wrapper.vm.listGalery.forEach((data, i) => {
			const galleryItem = gallery.at(i);

			expect(galleryItem.find('h4').text()).toBe(data.name);
			expect(galleryItem.find('h5').text()).toBe(data.address);
			expect(galleryItem.find('.image-kost').attributes('data-src')).toBe(
				`/general/img/pictures/mamirooms/galery-room/${data.image}`
			);
		});
	});

	it('should emit change-gallery when button gallery clicked', () => {
		wrapper.find('#buttonGallery button').trigger('click');
		expect(wrapper.emitted('change-gallery')[0][0]).toBe(true);
	});
});
