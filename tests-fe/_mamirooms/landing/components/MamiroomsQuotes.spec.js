import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsQuotes from 'Js/_mamirooms/landing/components/MamiroomsQuotes.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('MamiroomsQuotes.vue', () => {
	jest.useFakeTimers();

	const wrapper = shallowMount(MamiroomsQuotes, { localVue });

	const expectQuoteContentToBe = content => {
		expect(wrapper.find('.quotes-title').text()).toBe(`"${content.text}"`);
		expect(wrapper.find('.quotes-photo-image').attributes('data-src')).toBe(
			content.photo
		);
		expect(wrapper.find('.quotes-name').text()).toBe(content.name);
		expect(wrapper.find('.quotes-occupation').text()).toBe(content.occupation);
	};

	it('should show quotes section', () => {
		expect(wrapper.find('#quotes').exists()).toBe(true);
	});

	it('should show first quote on mounted', () => {
		expectQuoteContentToBe(wrapper.vm.quotesList[0]);
	});

	it('should auto slide to next quote', async () => {
		await wrapper.setData({ currentSlide: 1 });
		jest.advanceTimersByTime(15000);

		expect(wrapper.vm.currentSlide).toBe(2);
		expectQuoteContentToBe(wrapper.vm.quotesList[1]);
	});

	it('should move to selected slide when carausel selector clicked', async () => {
		await wrapper.setData({ currentSlide: 0 });
		const selectedIndex = wrapper.vm.quotesList.length - 2;
		wrapper
			.findAll('.carousel-selector')
			.at(selectedIndex)
			.trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.currentSlide).toBe(selectedIndex);
		expectQuoteContentToBe(wrapper.vm.quotesList[selectedIndex]);
	});

	it('should back to first index when it comes to an end', async () => {
		const lastQuoteIndex = wrapper.vm.quotesList.length - 1;
		await wrapper.setData({ currentSlide: lastQuoteIndex });
		jest.advanceTimersByTime(15000);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.currentSlide).toBe(0);
		expectQuoteContentToBe(wrapper.vm.quotesList[0]);
	});
});
