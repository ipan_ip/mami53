import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsHero from 'Js/_mamirooms/landing/components/MamiroomsHero';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
jest.mock('Js/_mamirooms/landing/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('MamiroomsHero.vue', () => {
	const wrapper = shallowMount(MamiroomsHero, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.mamirooms-hero').exists()).toBe(true);
	});

	it('should emit toggle modal register', () => {
		wrapper.find('.hero-button').trigger('click');
		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', true);
	});

	it('should return correct background image when screen is less than 1200', () => {
		window.innerWidth = 1000;
		expect(wrapper.vm.backgroundImageHero.src).toBe(
			'/general/img/pictures/mamirooms/bg_hero_home-mamirooms-1199.jpg'
		);
	});
});
