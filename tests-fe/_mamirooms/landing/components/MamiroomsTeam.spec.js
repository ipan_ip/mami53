import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsTeam from 'Js/_mamirooms/landing/components/MamiroomsTeam.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.directive('lazy', (el, binding) => {
	el.setAttribute('background-image', binding.value.src);
	el.setAttribute('data-src', binding.value.loading);
});

const mockInnerWidth = width => {
	mockWindowProperty('window.innerWidth', width);
};

describe('MamiroomsTeam.vue', () => {
	const mount = (innerWidth = 1366) => {
		mockInnerWidth(innerWidth);
		const wrapper = shallowMount(MamiroomsTeam, { localVue });
		const wrapperTeam = () => wrapper.find('.wrapper-team');
		const wrapperTeamBg = () => wrapperTeam().attributes('background-image');
		const wrapperTeamBgLoading = () => wrapperTeam().attributes('data-src');
		const expectWrapperTeamBgToBe = breakpoint => {
			expect(wrapperTeamBg()).toBe(
				`/general/img/pictures/mamirooms/bg_footer-mamirooms-mamikos-${breakpoint}.jpg`
			);
		};

		return { wrapper, wrapperTeamBgLoading, expectWrapperTeamBgToBe };
	};

	it('should show mamiroomsTeam section', () => {
		const { wrapper, wrapperTeamBgLoading } = mount(1366);

		expect(wrapper.find('#mamiroomsTeam').exists()).toBe(true);
		expect(wrapperTeamBgLoading()).toBe(
			'/general/img/pictures/mamirooms/bg_footer-mamirooms-mamikos-0.jpg'
		);
	});

	it('should return correct image for > 1200 width', async () => {
		const { expectWrapperTeamBgToBe } = mount(1366);
		expectWrapperTeamBgToBe('1439');
	});

	it('should return correct image for < 1200 and > 992 width', async () => {
		const { expectWrapperTeamBgToBe } = mount(1192);
		expectWrapperTeamBgToBe('1199');
	});

	it('should return correct image for < 992 and > 768 width', async () => {
		const { expectWrapperTeamBgToBe } = mount(800);
		expectWrapperTeamBgToBe('991');
	});

	it('should return correct image for < 768 width', async () => {
		const { expectWrapperTeamBgToBe } = mount(500);
		expectWrapperTeamBgToBe('767');
	});
});
