import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsModalUploadPhoto from 'Js/_mamirooms/landing/components/MamiroomsModalUploadPhoto.vue';

jest.mock('Js/_mamirooms/landing/event-bus.js', () => {
	global.EventBus = {
		$emit: jest.fn()
	};
	return global.EventBus;
});

const localVue = createLocalVue();

const stubs = {
	dropzone: {
		template: '<div class="dropzone"></div>',
		props: {
			options: Object
		},
		methods: {
			removeFile: jest.fn()
		}
	}
};

const parentComponent = {
	data() {
		return {
			state: {
				token: 'token-123'
			}
		};
	}
};

global.swal = jest.fn().mockResolvedValue(true);

describe('MamiroomsModalUploadPhoto.vue', () => {
	const wrapper = shallowMount(MamiroomsModalUploadPhoto, {
		parentComponent,
		localVue,
		stubs
	});

	it('should render properly', () => {
		expect(wrapper.find('.wrapper-photo-kost').exists()).toBe(true);

		wrapper.vm.dropzoneObjects.forEach((option, index) => {
			expect(
				wrapper
					.findAll('.dropzone')
					.at(index)
					.attributes('id')
			).toBe(option.id);
		});
	});

	it('should emit button disabled when upload on progress', () => {
		wrapper.findComponent(stubs.dropzone).vm.$emit('vdropzone-sending');

		expect(wrapper.emitted('button-disabled')[0][0]).toBe(true);
	});

	it('should handle dropzonde file added properly', () => {
		const dropzone = wrapper.findAllComponents(stubs.dropzone).at(0);
		dropzone.vm.$emit('vdropzone-file-added', 'file');

		expect(wrapper.vm.lastImage).toBe(dropzone.attributes('id'));
		expect(wrapper.vm.lastFile).toBe('file');
	});

	it('should handle remove photo properly', () => {
		const dropzone = wrapper.findAllComponents(stubs.dropzone).at(0);
		dropzone.vm.$emit('vdropzone-removed-file', 'file');

		expect(wrapper.emitted('depan-kost')[0][0]).toBe('');

		const dropzone2 = wrapper.findAllComponents(stubs.dropzone).at(1);
		dropzone2.vm.$emit('vdropzone-removed-file', 'file');

		expect(wrapper.emitted('kamar-kost')[0][0]).toBe('');
	});

	it('should handle success upload properly', async () => {
		wrapper.setData({ lastImage: 'frontKost' });
		wrapper
			.findAllComponents(stubs.dropzone)
			.at(0)
			.vm.$emit('vdropzone-success', 'file', { status: true, photo_id: 1 });

		expect(wrapper.emitted('depan-kost')[1][0]).toBe(1);

		wrapper.setData({ lastImage: 'roomKost' });
		wrapper
			.findAllComponents(stubs.dropzone)
			.at(0)
			.vm.$emit('vdropzone-success', 'file', { status: true, photo_id: 1 });

		expect(wrapper.emitted('kamar-kost')[1][0]).toBe(1);

		// response status: false
		const removeFile = jest.fn();
		const roomKost = { ...wrapper.vm.$refs.roomKost };
		wrapper.vm.$refs.roomKost = { removeFile };

		wrapper
			.findAllComponents(stubs.dropzone)
			.at(0)
			.vm.$emit('vdropzone-success', 'file', { status: false });
		await new Promise(resolve => setImmediate(resolve));

		expect(removeFile).toBeCalled();

		wrapper.vm.$refs.roomKost = roomKost;
	});
});
