import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsNavbar from 'Js/_mamirooms/landing/components/MamiroomsNavbar.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

jest.mock('Js/_mamirooms/landing/event-bus.js', () => {
	global.EventBus = {
		$emit: jest.fn()
	};
	return global.EventBus;
});

function MutationObserver(cb) {
	this.observe = () => {
		const mutations = [{ type: 'attributes' }];
		cb(mutations);
	};
}

global.MutationObserver = MutationObserver;

const iconBarFn = () => {
	const iconBarEl = document.createElement('div');
	iconBarEl.setAttribute('class', 'icon-bar');
	return iconBarEl;
};

const navbarToggleFn = () => {
	const navbarToggle = document.createElement('div');
	navbarToggle.setAttribute('class', 'navbar-toggle');
	return navbarToggle;
};

mockWindowProperty('document.referrer', 'http://mamikostest.com/referrer');

const store = {
	state: {
		authCheck: {
			owner: true,
			tenant: false
		}
	}
};

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('MamiroomsNavbar.vue', () => {
	const wrapper = shallowMount(MamiroomsNavbar, {
		localVue,
		store: new Vuex.Store(store),
		data() {
			return {
				iconBar: [iconBarFn(), iconBarFn(), iconBarFn()],
				navbarToggle: [navbarToggleFn()]
			};
		}
	});

	it('should render navbar', () => {
		expect(wrapper.find('navbar-stub').exists()).toBe(true);
	});

	it('should render all menu items', () => {
		const menu = wrapper.findAll('.btn.btn-link.text-primary');

		wrapper.vm.menuNavbar.forEach((menuItem, index) => {
			expect(menu.at(index).text()).toBe(menuItem.title);
		});
	});

	it('should scroll to content when menu item clicked', async () => {
		const menu = wrapper.findAll('.btn.btn-link.text-primary');

		const scrollIntoView = jest.fn();
		const spyQuerySelector = jest
			.spyOn(document, 'querySelector')
			.mockImplementation(selector => {
				return { scrollIntoView };
			});

		const mamiroomsFeaturesEl = document.createElement('section');
		mamiroomsFeaturesEl.setAttribute('id', 'mamiroomsFeatures');
		document.body.appendChild(mamiroomsFeaturesEl);

		await wrapper.vm.$nextTick();

		menu.at(0).trigger('click');
		await wrapper.vm.$nextTick();

		expect(scrollIntoView).toBeCalled();

		spyQuerySelector.mockRestore();
	});

	it('should open link properly', () => {
		window.open = jest.fn();
		const spyOpen = jest.spyOn(window, 'open');
		wrapper.vm.openLink();

		expect(spyOpen).toBeCalled();
		spyOpen.mockRestore();
	});

	it('should toggle modal register', async () => {
		await wrapper.setProps({ isNavbarFixed: true });
		wrapper.find('.btn.btn-info.btn-box.shadow-orange').trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', true);
	});

	it('should handle go back properly', () => {
		const spyGo = jest.spyOn(window.history, 'go');
		wrapper.vm.goBack();

		expect(spyGo).toBeCalledWith(-1);

		spyGo.mockRestore();
	});
});
