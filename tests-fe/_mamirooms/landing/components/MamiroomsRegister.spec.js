import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsRegister from 'Js/_mamirooms/landing/components/MamiroomsRegister.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock('Js/_mamirooms/landing/event-bus.js', () => {
	global.EventBus = {
		$emit: jest.fn()
	};
	return global.EventBus;
});

const localVue = createLocalVue();

const parentComponent = {
	data() {
		return {
			state: {
				register: {
					conditions: ['condition1', 'condition2', 'condition3'],
					facilities: [
						{ name: 'facility1', id: 1 },
						{ name: 'facility2', id: 2 },
						{ name: 'facility3', id: 3 }
					]
				}
			}
		};
	}
};

const mocks = {
	swalSuccess: jest.fn((a, b, cb) => {
		cb();
	}),
	swalError: jest.fn()
};

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

mockWindowProperty('window.location', { href: 'http://test.com' });

describe('MamiroomsRegister.vue', () => {
	const wrapper = shallowMount(MamiroomsRegister, {
		parentComponent,
		localVue,
		mocks
	});

	it('should render properly', () => {
		expect(wrapper.find('#mamiroomsGalery').exists()).toBe(true);
		expect(wrapper.find('.form-custom').exists()).toBe(true);
	});

	it('should handle close properly', () => {
		wrapper.find('.link-exit').trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', false);
	});

	it('should handle modal upload photo emit event properly', () => {
		const modal = wrapper.find('mamirooms-modal-upload-photo-stub');
		modal.vm.$emit('depan-kost', 'photo-1');
		modal.vm.$emit('kamar-kost', 'photo-2');
		modal.vm.$emit('button-disabled', false);

		expect(wrapper.vm.params.photos).toEqual({
			frontKost: 'photo-1',
			roomKost: 'photo-2'
		});
		expect(wrapper.vm.isDisabled).toBe(false);
	});

	it('should handle show modal state', () => {
		wrapper.setProps({ showModal: false });
		wrapper.setData({ showModal: true });

		expect(wrapper.vm.showModal).toBe(false);
		expect(wrapper.vm.showMyModal).toBe(true);
	});

	it('should handle keyup properly', () => {
		jest.clearAllMocks();
		window.dispatchEvent(new KeyboardEvent('keyup', { key: 'Space' }));

		expect(global.EventBus.$emit).not.toBeCalled();

		jest.clearAllMocks();
		window.dispatchEvent(new KeyboardEvent('keyup', { key: 'Escape' }));

		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', false);
	});

	it('should set focus when input is invalid', () => {
		jest.useFakeTimers();

		const focus = jest.fn();
		const spyGetElementById = jest
			.spyOn(document, 'getElementById')
			.mockImplementation(() => {
				return { focus };
			});

		wrapper.vm.$el
			.querySelector('#inputKostName')
			.dispatchEvent(new Event('invalid'));
		jest.advanceTimersByTime(1000);

		expect(focus).toBeCalled();

		jest.useRealTimers();
		spyGetElementById.mockRestore();
	});

	it('should handle cancel properly', () => {
		wrapper.find('.btn-cancel').trigger('click');

		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', false);
	});

	it('should handle send data properly', async () => {
		wrapper.vm.params.accountMamikos = 1;
		wrapper.vm.params.facility = { kamarmandiluar: 2 };
		const axios = {
			post: jest
				.fn()
				.mockResolvedValueOnce({
					data: {
						status: true
					}
				})
				.mockResolvedValueOnce({
					data: {
						status: false,
						meta: {
							message: 'message'
						}
					}
				})
				.mockRejectedValue('error')
		};
		global.axios = axios;

		wrapper.find('.form-custom').trigger('submit.prevent');
		await flushPromises();

		expect(window.location.href).toBe('/mamirooms');

		// status: false
		wrapper.find('.form-custom').trigger('submit.prevent');
		await flushPromises();

		expect(mocks.swalError).toBeCalled();

		// error
		wrapper.find('.form-custom').trigger('submit.prevent');
		await flushPromises();

		expect(bugsnagClient.notify).toBeCalled();

		jest.clearAllMocks();

		// invalid
		wrapper.vm.params.accountMamikos = 0;
		wrapper.find('.form-custom').trigger('submit.prevent');

		expect(axios.post).not.toBeCalled();
	});
});
