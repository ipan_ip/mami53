import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiroomsGalery from 'Js/_mamirooms/landing/components/MamiroomsGalery';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
jest.mock('Js/_mamirooms/landing/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('MamiroomsGalery.vue', () => {
	const wrapper = shallowMount(MamiroomsGalery, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.mamirooms-galery').exists()).toBe(true);
	});

	it('should emit eventbus modal register and galery change', () => {
		wrapper.find('.btn-download').trigger('click');
		expect(wrapper.emitted('change-gallery')[0][0]).toEqual(false);
		expect(global.EventBus.$emit).toBeCalledWith('toggleModalRegister', true);
	});

	it('should emit change gallery when user press esc', () => {
		window.dispatchEvent(new window.KeyboardEvent('keyup', { key: 'Escape' }));
		expect(wrapper.emitted('change-gallery')[0][0]).toEqual(false);
	});

	it('should close the gallery', () => {
		wrapper.find('.link-exit').trigger('click');
		expect(wrapper.emitted('change-gallery')[0][0]).toEqual(false);
	});
});
