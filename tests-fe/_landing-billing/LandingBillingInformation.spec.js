import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingInformation from 'Js/_landing-billing/components/LandingBillingInformation';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('LandingBillingInformation.vue', () => {
	const wrapper = shallowMount(LandingBillingInformation);
	it('should render a landing-billing-information main div', () => {
		expect(wrapper.find('.landing-billing-info')).toBeTruthy();
	});
	it('should render a landing-billing-text', () => {
		expect(wrapper.find('.landing-billing-text')).toBeTruthy();
	});
	it('should render a landing-billing-text container', () => {
		expect(wrapper.find('.landing-billing-text__container')).toBeTruthy();
	});
	it('should render landing-billing-text title', () => {
		const headerTitle = wrapper.find('.landing-billing-text__title');
		expect(headerTitle.text()).toBe('Manajemen Kos');
	});
	it('should render landing-billing-text description', () => {
		const headerTitle = wrapper.find('.landing-billing-text__description');
		expect(headerTitle.text()).toBe(
			'Mamikos membantu Anda mengurus berbagai aktivitas finansial dalam kos Anda, antara lain Kelola Tagihan, Kelola Penyewa, Kelola Booking dan Laporan Keuangan.'
		);
	});
	it('showVideo is false by default', () => {
		expect(wrapper.vm.showVideo).toBe(false);
	});
});

describe('LandingBillingInformation.vue is mobile', () => {
	mockWindowProperty('navigator', { isMobile: true });
	mockWindowProperty('window', { innerWidth: 600 });

	const wrapper = shallowMount(LandingBillingInformation, {
		propsData: {
			video: 'X4GY_uSflVU'
		}
	});

	it('isMobile is true', () => {
		window.open = jest.fn();
		window.open(`https://youtu.be/X4GY_uSflVU`, '_blank', 'noopener');
		expect(window.open).toHaveBeenCalledWith(
			`https://youtu.be/X4GY_uSflVU`,
			'_blank',
			'noopener'
		);
	});

	it('playVideo return window.open when isMobile is true', () => {
		wrapper.vm.playVideo({
			isMobile: true
		});
		expect(window.open).toBeCalled();
	});

	it('closeModal return showVideo false when clicked', () => {
		wrapper.vm.closeModal({
			showVideo: true
		});
		expect(wrapper.vm.showVideo).toBe(false);
	});
});
