import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingMobileMenu from 'Js/_landing-billing/components/LandingBillingMobileMenu';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingMobileMenu.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(LandingBillingMobileMenu, {
			localVue,
			propsData: {
				show: true,
				menus: [
					{
						label: 'Tentang',
						link: '#about'
					},
					{
						label: 'Manfaat Fitur',
						link: '#feature'
					},
					{
						label: 'Produk',
						link: '#product'
					},
					{
						label: 'Tanya Jawab',
						link: '#faq'
					}
				]
			}
		});
	};

	const closeSpy = jest.spyOn(LandingBillingMobileMenu.methods, 'closeMenu');

	it('should render mobile navbar menu', () => {
		createComponent();
		expect(wrapper.find('.mobile-navbar-menu')).toBeTruthy();
	});

	it('closeMenu should set toggleMenu to false when clicked', async () => {
		createComponent();

		wrapper.vm.$emit('toggleMenu', false);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('toggleMenu')).toBeTruthy();
		const button = wrapper.find('.item');

		await button.trigger('click');
		expect(closeSpy).toBeCalled();
	});
});
