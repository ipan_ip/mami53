import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingEntryPoint from 'Js/_landing-billing/components/LandingBillingEntryPoint';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingEntryPoint.vue', () => {
	const wrapper = shallowMount(LandingBillingEntryPoint, {
		localVue
	});

	it('should render entry point text', () => {
		const headerTitle = wrapper.find('.landing-billing-entrypoint-text');
		expect(headerTitle.text()).toBe(
			'Yuk, kelola kos dengan lebih mudah dan teratur.'
		);
	});
});
