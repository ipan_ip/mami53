import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingNavbar from 'Js/_landing-billing/components/LandingBillingNavbar';

describe('LandingBillingNavbar.vue', () => {
  const wrapper = shallowMount(LandingBillingNavbar);
  it('should render a landing-billing-navbar main div', () => {
    expect(wrapper.find('.landing-billing-navbar')).toBeTruthy();
  });
  it('should render a landing-billing-navbar container', () => {
    expect(wrapper.find('.landing-billing-navbar__container')).toBeTruthy();
  });
  it('showMobileMenu is false by default', () => {
    expect(wrapper.vm.showMobileMenu).toBe(false);
  });
  it('showMobileMenu is true when clicked', async () => {
    const button = wrapper.find('.landing-billing-navbar__mobile');
    await button.trigger('click');
    expect(wrapper.vm.showMobileMenu).toBe(true);
  });
  it('isScrolling is false by default', () => {
    expect(wrapper.vm.isScrolling).toBe(false);
  });
  it('Navbar is sticky when scrolled past top', () => {
    const localVue = createLocalVue();
    const wrapper = shallowMount(LandingBillingNavbar, {
      localVue,
      attachedToDocument: true,
    });
    window.dispatchEvent(new CustomEvent('scroll', { scrollY: 2000 }));
    wrapper.destroy();
    expect(wrapper.find('.landing-billing-navbar--fixed')).toBeTruthy();
  });
})