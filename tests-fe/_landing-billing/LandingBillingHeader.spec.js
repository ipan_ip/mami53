import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingHeader from 'Js/_landing-billing/components/LandingBillingHeader';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingHeader.vue', () => {
  const wrapper = shallowMount(LandingBillingHeader);
  it('should render a landing-billing-navbar main div', () => {
    expect(wrapper.find('.container-fluid landing-billing-header')).toBeTruthy();
  });
  it('should render header title "Kelola Sewa & Tagihan Makin Mudah dengan Fitur Manajemen Kos"', () => {
			const headerTitle = wrapper.find('.landing-billing-header__title');
			expect(headerTitle.text()).toBe('Kelola Sewa & Tagihan Makin Mudah dengan Fitur Manajemen Kos');
    });
  it('isMobile is false by default', async () => {
    await wrapper.setData({ isMobile: false });
    expect(wrapper.vm.bgImage).toBe('/general/img/pictures/landing-billing/background-header-desktop.jpg');
  });
})

describe('LandingBillingHeader.vue is mobile', () => {
    mockWindowProperty('navigator', {isMobile: true});
    mockWindowProperty('window', {innerWidth: 600});

    const wrapper = shallowMount(LandingBillingHeader);
    
    it('isMobile is true', async () => {
    expect(wrapper.vm.bgImage).toBe('/general/img/pictures/landing-billing/background-header-mobile.jpg');
  });
});