import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingFooter from 'Js/_landing-billing/components/LandingBillingFooter';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingFooter.vue', () => {
	const wrapper = shallowMount(LandingBillingFooter);

	it('should render a landing-billing-navbar container', () => {
		expect(wrapper.find('.landing-billing-footer')).toBeTruthy();
	});
});
