import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingBillingFaq from 'Js/_landing-billing/components/LandingBillingFaq';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const billingClickSpy = jest.spyOn(LandingBillingFaq.methods, 'faqClicked');

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingFaq.vue', () => {
	const wrapper = shallowMount(LandingBillingFaq, {
		localVue
	});
	const button = wrapper.findAll('.accordion-header');

	it('should render a landing-billing-faq main div', () => {
		expect(wrapper.find('.landing-billing-faq')).toBeTruthy();
	});

	it('should render landing-billing-faq main title', () => {
		const mainTitle = wrapper.find('.main-title');
		expect(mainTitle.text()).toBe('Tanya Jawab');
	});

	it('should render landing-billing-faq sub title', () => {
		const subTitle = wrapper.find('.sub-title');
		expect(subTitle.text()).toBe('Pertanyaan yang sering ditanyakan');
	});

	it('accordion should open when clicked', async () => {
		expect(wrapper.vm.activeAccordion).toBe('');
		await button.at(1).trigger('click');

		expect(billingClickSpy).toBeCalledWith(1);
		expect(wrapper.vm.activeAccordion).toBe(1);
	});

	it('activeAccordion should set closed when activeAccordion same with value', async () => {
		await wrapper.setData({ activeAccordion: 1 });
		await button.at(1).trigger('click');
		expect(billingClickSpy).toBeCalledWith(1);
		expect(wrapper.vm.activeAccordion).toBe('closed');
	});
});
