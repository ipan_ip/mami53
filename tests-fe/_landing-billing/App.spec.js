import { shallowMount } from '@vue/test-utils';
import App from 'Js/_landing-billing/components/App';

describe('App.vue', () => {
	const wrapper = shallowMount(App);

	it('should render a landing-billing-warp main div', () => {
		expect(wrapper.find('.landing-billing-warp')).toBeTruthy();
	});
});
