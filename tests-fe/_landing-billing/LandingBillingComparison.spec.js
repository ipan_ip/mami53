import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingBillingComparison from 'Js/_landing-billing/components/LandingBillingComparison';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingBillingComparison.vue', () => {
	const wrapper = shallowMount(LandingBillingComparison, {
		localVue
	});

	it('should render a landing-billing-comparison container', () => {
		expect(wrapper.find('.landing-billing-comparison__container')).toBeTruthy();
	});

	describe('LandingBillingComparison.vue is mobile', () => {
		mockWindowProperty('navigator', { isMobile: true });
		mockWindowProperty('window', { innerWidth: 600 });

		it('should render a landing-billing-comparison-table-mobile-container', () => {
			expect(
				wrapper.find('.landing-billing-comparison-table-mobile-container')
			).toBeTruthy();
		});
	});
});
