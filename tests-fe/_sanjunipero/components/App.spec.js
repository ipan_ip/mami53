import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import App from 'Js/_sanjunipero/components/App.vue';
import KostListStore from './__mocks__/kostListStore';

describe('App.vue', () => {
	let wrapper;
	const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin([MixinNavigatorIsMobile]);

	mockWindowProperty('scrollTo', jest.fn());
	mockWindowProperty(
		'setTimeout',
		jest
			.fn(fn => {
				fn && fn();
			})
			.mockReturnValue(4000)
	);

	const initRoomsSpy = jest.spyOn(App.methods, 'initRooms');
	const pageChangedSpy = jest.spyOn(App.methods, 'pageChanged');

	const paginateStub = {
		template: `<button class="update-page" @click="clickHandler(2)"></button>`,
		props: ['clickHandler']
	};

	beforeEach(() => {
		wrapper = shallowMount(App, {
			store: new Vuex.Store(KostListStore),
			localVue,
			stubs: {
				paginate: paginateStub
			}
		});
	});

	it('should call initRooms on created', () => {
		expect(initRoomsSpy).toHaveBeenCalled();
	});

	it('should loaded san junipero page', () => {
		expect(wrapper.find('.sanjunipero-wrap')).toBeTruthy();
		expect(wrapper.find('.list')).toBeTruthy();
		expect(wrapper.find('.pagination-section')).toBeTruthy();
	});

	it('should show correct total pages', () => {
		let total = Math.ceil(
			wrapper.vm.$store.state.mainData.kos.totalRooms /
				wrapper.vm.$store.state.mainData.kos.params.limit
		);

		expect(wrapper.vm.totalPageAll).toEqual(total);

		if (total >= 10) {
			total = 10;
		}

		expect(wrapper.vm.totalPage).toEqual(total);
	});

	it('should fetch room on change page', async () => {
		const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
		const fetchRoomsSpy = jest.spyOn(wrapper.vm, 'fetchRooms');
		const paginationElement = wrapper.find('button.update-page');

		expect(paginationElement.exists()).toBe(true);

		await paginationElement.trigger('click');

		expect(pageChangedSpy).toBeCalled();

		expect(commitSpy).toBeCalledWith('setOffset', 1);

		expect(setTimeout).toBeCalled();
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 50);

		setTimeout(() => {
			expect(window.scrollTo).toBeCalled();
			expect(window.scrollTo).toBeCalledWith({ top: 0, behavior: 'smooth' });
		}, 50);

		expect(fetchRoomsSpy).toHaveBeenCalledTimes(1);
	});
});
