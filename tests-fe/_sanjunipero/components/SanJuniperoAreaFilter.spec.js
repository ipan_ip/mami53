import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import SanJuniperoAreaFilter from 'Js/_sanjunipero/components/SanJuniperoAreaFilter.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty('location', {
	href: '/kos/testing/ugm-jogja'
});

mockWindowProperty('parent', {
	slug: 'testing'
});

const closeActionSpy = jest.spyOn(SanJuniperoAreaFilter.methods, 'closeAction');

const handleUpdateValueSpy = jest.spyOn(
	SanJuniperoAreaFilter.methods,
	'handleUpdateValue'
);

const dropdownData = {
	title: 'Sekitar UGM',
	icon: 'search-icon',
	data: [
		{ area_name: 'Semua Area', slug: '' },
		{
			id: 3,
			parent_id: 26,
			area_type: 'campus',
			area_name: 'Sekitar UGM',
			slug: 'ugm-jogja'
		}
	],
	active: true
};

describe('SanJuniperoAreaFilter.vue', () => {
	const wrapper = shallowMount(SanJuniperoAreaFilter, {
		localVue
	});

	it('should redirect and close dropdown', async () => {
		await wrapper.setProps({
			dropdownData
		});
		const list = wrapper.findAll('.area-list li');

		expect(list.length).toBe(2);
		await list.at(1).trigger('click');

		expect(handleUpdateValueSpy).toHaveBeenCalledWith('ugm-jogja');
		expect(closeActionSpy).toBeCalled();
		expect(location.href).toBe('/kos/testing/ugm-jogja');
	});

	it('should searchedAreaLists return similiar value with input', async () => {
		const searchInput = wrapper.find('.area-search-form');

		await searchInput.setValue('UGM');
		expect(wrapper.vm.searchInputValue).toBe('UGM');
	});
});
