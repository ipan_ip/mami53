import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import SanJuniperoFaqs from 'Js/_sanjunipero/components/SanJuniperoFaqs.vue';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

mockWindowProperty('parent', {
	faq_question: ['pertanyaan 1', 'pertanyaan 2'],
	faq_answer: ['jawaban 1', 'jawaban 2']
});

const faqClickedSpy = jest.spyOn(SanJuniperoFaqs.methods, 'faqClicked');

describe('SanJuniperoFaqs.vue', () => {
	const wrapper = shallowMount(SanJuniperoFaqs, {
		localVue
	});

	const panelHeader = wrapper.findAll('.accordion-header');
	const panelDescription = wrapper.findAll('.accordion-description');

	it('should load faqs correctly', () => {
		expect(panelHeader.length).toBe(2);
		expect(panelDescription.length).toBe(2);
	});

	it('should open panel on click at header', async () => {
		expect(wrapper.vm.activeAccordion).toBe('');
		await panelHeader.at(1).trigger('click');

		expect(faqClickedSpy).toBeCalledWith(1);
		expect(wrapper.vm.activeAccordion).toBe(1);
	});

	it('should set activeAccordion to Closed if value and activeAccrotdion same', async () => {
		await wrapper.setData({ activeAccordion: 1 });
		await panelHeader.at(1).trigger('click');

		expect(faqClickedSpy).toBeCalledWith(1);
		expect(wrapper.vm.activeAccordion).toBe('closed');
	});
});
