import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import SanJuniperoAreaFilterMobile from 'Js/_sanjunipero/components/SanJuniperoAreaFilterMobile.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty('location', {
	href: '/kos/testing/ugm-jogja'
});

mockWindowProperty('parent', {
	slug: 'testing'
});

mockWindowProperty(
	'setTimeout',
	jest
		.fn(fn => {
			fn && fn();
		})
		.mockReturnValue(4000)
);

const handleActiveInputSpy = jest.spyOn(
	SanJuniperoAreaFilterMobile.methods,
	'handleActiveInput'
);

const handleUpdateValueSpy = jest.spyOn(
	SanJuniperoAreaFilterMobile.methods,
	'handleUpdateValue'
);

const dropdownData = {
	title: 'Sekitar UGM',
	icon: 'search-icon',
	data: [
		{ area_name: 'Semua Area', slug: '' },
		{
			id: 3,
			parent_id: 26,
			area_type: 'campus',
			area_name: 'Sekitar UGM',
			slug: 'ugm-jogja'
		}
	],
	active: true
};

describe('SanJuniperoAreaFilterMobile.vue', () => {
	const wrapper = shallowMount(SanJuniperoAreaFilterMobile, {
		localVue
	});

	const searchInput = wrapper.find('.sj-area-filter-mobile__search-input');

	it('should set searchInputValue as true on input focus', async () => {
		expect(searchInput.exists()).toBeTruthy();
		expect(wrapper.vm.isOptionsActive).toBe(false);

		await searchInput.trigger('focus');
		expect(handleActiveInputSpy).toBeCalled();
		expect(wrapper.vm.isOptionsActive).toBe(true);
	});

	it('should reset searchInputValue at input blur', async () => {
		await wrapper.setData({
			searchInputValue: 'sekitar',
			isOptionsActive: true
		});
		expect(wrapper.vm.searchInputValue).toBe('sekitar');
		expect(wrapper.vm.isOptionsActive).toBe(true);

		await searchInput.trigger('blur');
		expect(handleActiveInputSpy).toBeCalled();
		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 0);
		setTimeout(() => {
			expect(wrapper.vm.searchInputValue).toBe('');
			expect(wrapper.vm.isOptionsActive).toBe(false);
		}, 0);
	});

	it('should searchInputValue return similiar value with input', async () => {
		await searchInput.setValue('UGM');
		expect(wrapper.vm.searchInputValue).toBe('UGM');
	});

	it('should redirect and close dropdown', async () => {
		await wrapper.setProps({
			dropdownData
		});
		const list = wrapper.findAll('.sj-area-filter-mobile__area-list li');

		expect(list.length).toBe(1);
		await list.at(0).trigger('click');

		expect(handleUpdateValueSpy).toHaveBeenCalledWith(
			'ugm-jogja',
			'Sekitar UGM'
		);
		expect(wrapper.vm.searchInputPlaceholder).toBe('Sekitar UGM');
		expect(location.href).toBe('/kos/testing/ugm-jogja');
	});
});
