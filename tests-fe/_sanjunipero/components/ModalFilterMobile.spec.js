import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import ModalFilterMobile from 'Js/_sanjunipero/components/ModalFilterMobile.vue';
import Vuex from 'vuex';
import kostListStore from './__mocks__/kostListStore';

jest.mock('Js/@utils/makeAPICall.js');

const mixinFilterHelper = require('Js/_sanjunipero/mixins/MixinFilterHelper');

const localVue = createLocalVue();
localVue.mixin(mixinFilterHelper);
localVue.use(Vuex);
const store = new Vuex.Store(kostListStore);

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

mockWindowProperty('parent', {
	children: []
});

const handleApplyFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleApplyFilter'
);

const handleSetFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleSetFilter'
);

const handleSetPriceSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleSetPrice'
);

const handleResetFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleResetFilter'
);

const fetchCountRoomsSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'fetchCountRooms'
);

const baseFilterPriceStub = {
	template: `<button class="update-price" @click="$emit('set-value', [10000, 200000])" />`,
	methods: {
		reCalculatePrice: jest.fn(),
		resetAction: jest.fn()
	}
};

const baseFilterOptionStub = {
	template: `<button class="update-value" @click="$emit('set-value', [1], 'gender')" />`
};

describe('ModalFilterMobile.vue', () => {
	let wrapper = shallowMount(ModalFilterMobile, {
		localVue,
		store,
		computed: {
			getIsFacilitiesExist() {
				return true;
			}
		},
		stubs: {
			'base-filter-price': baseFilterPriceStub,
			'base-filter-option': baseFilterOptionStub
		}
	});

	it('Computed applyButtonText should return correctly', async () => {
		const testCase = [
			{
				totalFilteredKos: 0,
				expect: 'Tampilkan 0 Kos'
			},
			{
				totalFilteredKos: 200,
				expect: 'Tampilkan 200 Kos'
			},
			{
				totalFilteredKos: 1350,
				expect: 'Tampilkan 900+ Kos'
			}
		];

		for (const test of testCase) {
			await wrapper.setData({ totalFilteredKos: test.totalFilteredKos });
			expect(wrapper.vm.applyButtonText).toEqual(test.expect);
		}
	});

	it('Watch isShow should return correctly', async () => {
		const payload = {
			price_range: [10000, 50000000],
			gender: [1, 0],
			rent_type: 3
		};

		await wrapper.setProps({ isShow: false });
		expect(wrapper.vm.filters).not.toEqual(payload);

		wrapper.setData({ filters: payload });
		await wrapper.setProps({ isShow: true });
		expect(wrapper.vm.filters).toEqual(payload);

		await wrapper.setProps({ isShow: false });
		expect(wrapper.vm.filters).toEqual(payload);
	});

	it('Should call method handleApplyFilter correctly', async () => {
		await wrapper.setProps({ isShow: true });
		const closeFilterElement = wrapper.find('span.modal-header-close');

		expect(closeFilterElement.exists()).toBe(true);

		await closeFilterElement.trigger('click');

		expect(handleApplyFilterSpy).toBeCalledWith(false);
		expect(wrapper.emitted('toggle-modal-handler')).toBeTruthy();
	});

	it('Should call method handleSetFilter correctly', async () => {
		const updateFilterElement = wrapper.find('button.update-value');

		expect(updateFilterElement.exists()).toBe(true);

		await updateFilterElement.trigger('click');

		expect(handleSetFilterSpy).toBeCalledWith([1], 'gender');
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleResetFilter correctly', async () => {
		const resetFilterElement = wrapper.find('button.search-btn-reset');

		expect(resetFilterElement.exists()).toBe(true);

		await resetFilterElement.trigger('click');

		expect(handleResetFilterSpy).toBeCalled();
		expect(wrapper.vm.filters).toEqual({
			gender: [],
			price_range: [10000, 20000000],
			rent_type: 2
		});
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleSetPrice correctly', async () => {
		const updatedPriceElement = wrapper.find('button.update-price');

		expect(updatedPriceElement.exists()).toBe(true);

		await updatedPriceElement.trigger('click');

		expect(handleSetPriceSpy).toBeCalledWith([10000, 200000]);
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleApplyFilter with fetch correctly', async () => {
		wrapper = shallowMount(ModalFilterMobile, {
			localVue,
			store,
			stubs: {
				'base-filter-price': baseFilterPriceStub,
				'base-filter-option': baseFilterOptionStub
			}
		});
		await wrapper.setProps({ isShow: true });
		await wrapper.setData({ isCountRoomsLoading: false });
		const saveElement = wrapper.find('button.search-btn-save');

		expect(saveElement.exists()).toBe(true);

		await saveElement.trigger('click');

		expect(handleApplyFilterSpy).toBeCalled();
	});
});
