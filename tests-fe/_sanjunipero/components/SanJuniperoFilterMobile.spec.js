import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import SanJuniperoFilterMobile from 'Js/_sanjunipero/components/SanJuniperoFilterMobile.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import kostListStore from './__mocks__/kostListStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(kostListStore);

const addEvent = jest
	.spyOn(global, 'addEventListener')
	.mockImplementation(() => {});

const removeEvent = jest
	.spyOn(global, 'removeEventListener')
	.mockImplementation(() => {});

const handleCloseSortingSpy = jest.spyOn(
	SanJuniperoFilterMobile.methods,
	'handleCloseSorting'
);

const handleToggleModalSpy = jest.spyOn(
	SanJuniperoFilterMobile.methods,
	'handleToggleModal'
);

const handleScrollSpy = jest.spyOn(
	SanJuniperoFilterMobile.methods,
	'handleScroll'
);

const modalSortingMobileStub = {
	template: `<button class="close-sorting" @click="$emit('toggle-modal-handler')" />`
};

const modalFilterMobileStub = {
	template: `<button class="close-filter" @click="$emit('toggle-modal-handler', 'Filter')" />`
};

describe('SanJuniperoFilterMobile.vue', () => {
	let wrapper = shallowMount(SanJuniperoFilterMobile, {
		localVue,
		store,
		stubs: {
			'modal-sorting-mobile': modalSortingMobileStub,
			'modal-filter-mobile': modalFilterMobileStub
		}
	});

	it('Should call method handleToggleModal to open sorting modal correctly', async () => {
		const openSortingElement = wrapper.findAll('button.filter-button').at(1);

		await openSortingElement.trigger('click');

		expect(handleToggleModalSpy).toBeCalledWith('Urutkan');
		expect(wrapper.vm.showSortingModal).toBe(true);
	});

	it('Should call method handleCloseSorting correctly', async () => {
		await wrapper.setData({ showSortingModal: true });
		const closeSortingElement = wrapper.find('button.close-sorting');

		expect(closeSortingElement.exists()).toBe(true);

		await closeSortingElement.trigger('click');

		expect(handleCloseSortingSpy).toBeCalled();
		expect(wrapper.vm.showSortingModal).toBe(false);
	});

	it('Should call method handleToggleModal to close filter modal correctly', async () => {
		await wrapper.setData({ showFilterModal: true });
		const closeFilterElement = wrapper.find('button.close-filter');

		expect(closeFilterElement.exists()).toBe(true);

		await closeFilterElement.trigger('click');

		expect(handleToggleModalSpy).toBeCalledWith('Filter');
		expect(wrapper.vm.showFilterModal).toBe(false);
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const testCase = [
			{
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true
			},
			{
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: true
			},
			{
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false
			}
		];

		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});

		for (const test of testCase) {
			mockWindowProperty('pageYOffset', test.pageYOffset);

			wrapper = shallowMount(SanJuniperoFilterMobile, {
				localVue,
				store
			});

			if (!test.ref) wrapper.vm.$refs.filterButton = null;

			await wrapper.setData({ lastScrollPosition: test.lastScrollPosition });
			await mockMapEventListener.scroll({ scrollTo: test.lastScrollPosition });

			expect(handleScrollSpy).toBeCalled();
		}
	});

	it('Computed totalActive should return correctly', () => {
		const payload = {
			tag_ids: [1, 2],
			gender: [1, 0],
			rent_type: 3
		};
		wrapper.vm.$store.commit('setFilter', payload);

		expect(wrapper.vm.totalActive).toEqual({ Filter: 3, Urutkan: 0 });
	});

	it('Should remove listener on destroy', () => {
		expect(addEvent).toHaveBeenCalled();

		wrapper.destroy();

		expect(removeEvent).toHaveBeenCalled();
	});
});
