import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import SanJuniperoFilter from 'Js/_sanjunipero/components/SanJuniperoFilter.vue';
import Vuex from 'vuex';
import kostListStore from './__mocks__/kostListStore';

jest.mock('Js/@utils/makeAPICall.js');

const mixinFilterHelper = require('Js/_sanjunipero/mixins/MixinFilterHelper');

const localVue = createLocalVue();
localVue.mixin(mixinFilterHelper);
localVue.use(Vuex);
const store = new Vuex.Store(kostListStore);

mockWindowProperty('parent', {
	children: []
});

const baseMainFilterStub = {
	template: `<button class="base-main-filter" @click="$emit('save-action')" />`
};

const baseMainPrice = {
	template: `<button class="base-main-price" @click="$emit('save-action')" />`
};

const baseMainFilterSortingStub = {
	template: `<button class="set-sorting" @click="$emit('set-value', ['price','desc'])" />`
};

const applyFilterSpy = jest.spyOn(SanJuniperoFilter.methods, 'applyFilter');
const setSortingSpy = jest.spyOn(SanJuniperoFilter.methods, 'setSorting');

describe('SanJuniperoFilter.vue', () => {
	it('should call method applyFilter correctly', async () => {
		const wrapper = shallowMount(SanJuniperoFilter, {
			localVue,
			store,
			stubs: {
				'base-main-filter': baseMainFilterStub,
				'base-main-price': baseMainPrice
			}
		});

		const filterElement = wrapper.find('button.base-main-filter');
		expect(filterElement.exists()).toBe(true);

		await filterElement.trigger('click');
		expect(applyFilterSpy).toBeCalled();
	});

	it('should call method setSorting correctly', async () => {
		const wrapper = shallowMount(SanJuniperoFilter, {
			localVue,
			store,
			stubs: {
				'base-main-filter': baseMainFilterSortingStub
			}
		});

		const sortingElement = wrapper.find(
			'button.set-sorting[filter-type="sorting"]'
		);
		expect(sortingElement.exists()).toBe(true);

		await sortingElement.trigger('click');
		expect(setSortingSpy).toBeCalled();
	});
});
