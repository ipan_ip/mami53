export default {
	state: {
		authCheck: {},
		authData: {},
		mainData: {
			kos: {
				params: {
					filters: {
						gender: [],
						price_range: [10000, 20000000],
						tags_ids: [],
						level_info: [],
						rent_type: 2,
						property_type: 'all',
						random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
						booking: 0,
						flash_sale: false
					},
					sorting: {
						field: 'price',
						direction: '-'
					},
					location: [],
					point: {},
					include_promoted: false,
					limit: 20,
					offset: 0,
					slug: window.location.pathname
				},
				kosList: [],
				clusterList: [],
				isEmptyList: false,
				totalRooms: 0,
				responseData: null,
				hasMore: false,
				tagRooms: [],
				tagShare: [],
				tagRules: [],
				rentTypeList: []
			}
		},
		isMoreLoading: false,
		isLoading: true,
		isFacLoading: true,
		activeType: 'kos'
	},
	getters: {
		getAllParams: state => {
			return state.mainData.kos.params;
		},
		getRooms: state => {
			return state.mainData.kos.kosList;
		},
		getKostFilters: state => {
			return state.mainData.kos.params.filters;
		},
		getKostLocationFilter: state => {
			return state.mainData.kos.params.location;
		},
		getKostSorting: state => {
			return [
				state.mainData.kos.params.sorting.field,
				state.mainData.kos.params.sorting.direction
			];
		},
		getTotalRooms: state => {
			return state.kos.totalRooms;
		},
		getRentTypeList: state => {
			return state.mainData.kos.rentTypeList;
		},
		getFacilitiesExist: state => {
			return (
				state.mainData.kos.tagRooms.length && state.mainData.kos.tagShare.length
			);
		},
		getTagRules: state => {
			return state.mainData.kost.tagRules;
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setFacLoading(state, payload) {
			state.isFacLoading = payload;
		},
		setMoreLoading(state, payload) {
			state.isMoreLoading = payload;
		},
		setEmptyList(state, payload) {
			state.mainData[state.activeType].isEmptyList = payload;
		},
		setHasMore(state, payload) {
			state.mainData[state.activeType].hasMore = payload;
		},
		setOffset(state, payload) {
			state.mainData[state.activeType].params.offset = payload;
		},
		setSort(state, payload) {
			state.mainData[state.activeType].params.sorting.field = payload[0];
			state.mainData[state.activeType].params.sorting.direction = payload[1];
		},
		setFilter(state, payload) {
			state.mainData[state.activeType].params.filters = {
				...state.mainData[state.activeType].params.filters,
				...payload
			};
		},
		setLocationFilter(state, payload) {
			state.mainData[state.activeType].params.location = payload;
		},
		setRentTypeList(state, payload) {
			const rentList = payload.map(rent => {
				return {
					...rent,
					value: rent.id,
					name: rent.rent_name
				};
			});
			state.mainData.kos.rentTypeList = rentList;
		},
		setRooms(state, payload) {
			if (payload.status) {
				if (payload.isRequestMore) {
					state.mainData[state.activeType][`${state.activeType}List`] = [
						...state.mainData[state.activeType][`${state.activeType}List`],
						...payload.rooms
					];
				} else {
					state.mainData[state.activeType][`${state.activeType}List`] =
						payload.rooms;
				}
				state.mainData[state.activeType].totalRooms = payload.total;
			} else {
				state.mainData[state.activeType][`${state.activeType}List`] = [];
				state.mainData[state.activeType].totalRooms = 0;
			}
		},
		setResponseData(state, payload) {
			state.mainData[state.activeType].responseData = payload;
		}
	},
	actions: {
		settingInitialLocationFilter({ commit, dispatch }) {
			let locationValue;
			if (window.child) {
				locationValue = [
					[window.child.longitude_1, window.child.latitude_1],
					[window.child.longitude_2, window.child.latitude_2]
				];
			} else {
				locationValue = [];
			}
			commit('setLocationFilter', locationValue);
		},
		settingFilter({ commit, dispatch }, payload) {
			commit('setFilter', payload);
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchRooms');
		},
		async fetchFacilitiesList({ commit }) {
			commit('setFacLoading', true);
			Promise(resolve => {
				const response = [
					{
						id: 1,
						rent_name: 'Mingguan',
						fac_event: 'filter_period_mingguan'
					},
					{
						id: 2,
						rent_name: 'Bulanan',
						fac_event: 'filter_period_bulanan'
					},
					{
						id: 3,
						rent_name: 'Tahunan',
						fac_event: 'filter_period_tahunan'
					},
					{
						id: 4,
						rent_name: 'Per 3 Bulan',
						fac_event: 'filter_period_3_bulan'
					},
					{
						id: 5,
						rent_name: 'Per 6 Bulan',
						fac_event: 'filter_period_6_bulan'
					}
				];

				if (response.status) {
					commit('setRentTypeList', response.rent_type);
				}

				commit('setFacLoading', false);
				resolve(true);
			});
		},
		async fetchRooms({ commit, state, dispatch }, loadingMore = false) {
			commit('setLoading', true);

			const response = {
				status: true,
				rooms: [
					{
						_id: 66591690,
						address: '',
						apartment_project_id: 0,
						area_label: 'Abepura, Jayapura, Papua',
						available_room: 9,
						cards: [],
						checker: false,
						city: 'Jayapura',
						class: 0,
						class_badge: null,
						click_count: 6415,
						expired_status: null,
						fac_bath_ids: [],
						fac_room_ids: [],
						fac_share_ids: [],
						floor: '0',
						furnished_status: 'Not Furnished',
						gender: 2,
						goldplus: 2,
						goldplus_status: 'Mamikos Goldplus 2',
						has_round_photo: false,
						has_video: false,
						is_booking: true,
						is_flash_sale: true,
						is_mamirooms: false,
						is_premium_owner: true,
						is_promoted: false,
						is_testing: false,
						is_top_owner: false,
						keyword: [null],
						label: { label: 'AC Murah', priority: 1 },
						label_array: [{ label: 'AC Murah', priority: 1 }],
						last_updated: 1608533093,
						level_info: {
							id: 7,
							name: 'Mamikos Goldplus 2',
							is_regular: false,
							flag: null,
							flag_id: null
						},
						love_by_me: false,
						min_month: null,
						not_updated: false,
						online: 'Update Hari Ini [3877]',
						other_discounts: [
							{ discount: '2%', rent_type_unit: 'minggu', order: 2 },
							{ discount: '17%', rent_type_unit: '6 bulan', order: 5 },
							{ discount: '2%', rent_type_unit: 'tahun', order: 6 }
						],
						owner_phone: '',
						owner_phone_array: [],
						photo_count: 0,
						photo_url: {
							real:
								'https://static8.kerupux.com/uploads/data/style/2019-05-15/qx9EseJR.jpg',
							small:
								'https://static8.kerupux.com/uploads/cache/data/style/2019-05-15/qx9EseJR-240x320.jpg',
							medium:
								'https://static8.kerupux.com/uploads/cache/data/style/2019-05-15/qx9EseJR-360x480.jpg',
							large:
								'https://static8.kerupux.com/uploads/cache/data/style/2019-05-15/qx9EseJR-540x720.jpg'
						},
						price_title: '700 rb',
						price_title_format: {
							currency_symbol: 'Rp',
							price: '1.100.000',
							discount_price: '800.000',
							discount: '27%',
							rent_type_unit: 'bulan'
						},
						price_title_time: '1.100.000 / bulan',
						promo_title: null,
						rating: 0,
						rating_string: '0.0',
						review_count: 0,
						'room-title': 'Kost Garden Abepura ',
						share_url:
							'https://jambu.kerupux.com/room/kost-jayapura-kost-putri-murah-kost-garden-abepura-',
						size: '10X10',
						status: 0,
						'status-title': 'null',
						status_kos: 'verified',
						subdistrict: 'Abepura',
						top_facility: [],
						unique_code: '167GR',
						unit_type: null,
						unit_type_rooms: null,
						updated: '2020-12-21 13:44:53',
						verification_status: {
							is_verified_address: true,
							is_verified_phone: true,
							is_visited_kost: false,
							is_verified_kost: true,
							is_verified_by_mamikos: false
						}
					},
					{
						_id: 59248877,
						address: '',
						apartment_project_id: 0,
						area_label: 'Langke Rembong, Manggarai, Manggarai',
						available_room: 10,
						cards: [],
						checker: true,
						city: 'Manggarai',
						class: 0,
						class_badge: null,
						click_count: 78,
						expired_status: null,
						fac_bath_ids: [],
						fac_room_ids: [],
						fac_share_ids: [],
						floor: '0',
						furnished_status: 'Not Furnished',
						gender: 1,
						goldplus: 3,
						goldplus_status: 'Mamikos Goldplus 3',
						has_round_photo: false,
						has_video: false,
						is_booking: true,
						is_flash_sale: false,
						is_mamirooms: true,
						is_premium_owner: true,
						is_promoted: false,
						is_testing: false,
						is_top_owner: false,
						keyword: [null],
						label: { label: 'Bebas 24 Jam', priority: 2 },
						label_array: [{ label: 'Bebas 24 Jam', priority: 2 }],
						last_updated: 1606727593,
						level_info: {
							id: 10,
							name: 'Mamikos Goldplus 3',
							is_regular: false,
							flag: null,
							flag_id: null
						},
						love_by_me: false,
						min_month: null,
						not_updated: false,
						online: 'Update 3 Minggu Lalu [3783]',
						other_discounts: [],
						owner_phone: '',
						owner_phone_array: [],
						photo_count: 0,
						photo_url: {
							real:
								'https://static8.kerupux.com/uploads/data/style/2020-06-18/IAuw7OQD.jpg',
							small:
								'https://static8.kerupux.com/uploads/cache/data/style/2020-06-18/IAuw7OQD-240x320.jpg',
							medium:
								'https://static8.kerupux.com/uploads/cache/data/style/2020-06-18/IAuw7OQD-360x480.jpg',
							large:
								'https://static8.kerupux.com/uplunknown getter: getRoomsads/cache/data/style/2020-06-18/IAuw7OQD-540x720.jpg'
						},
						price_title: '1 jt',
						price_title_format: {
							currency_symbol: 'Rp',
							price: '1.000.000',
							rent_type_unit: 'bulan'
						},
						price_title_time: '1.000.000 / bulan',
						promo_title: null,
						rating: 0,
						rating_string: '0.0',
						review_count: 0,
						'room-title': 'kuncara kos3',
						share_url:
							'https://jambu.kerupux.com/room/kost-manggarai-kost-putra-eksklusif-kuncara-kos3',
						size: '3x3',
						status: 0,
						'status-title': 'null',
						status_kos: 'verified',
						subdistrict: 'Langke Rembong',
						top_facility: ['K. Mandi Dalam', 'WiFi', 'Akses 24 Jam'],
						unique_code: '457KK',
						unit_type: null,
						unit_type_rooms: null,
						updated: '2020-11-30 16:13:13',
						verification_status: {
							is_verified_address: true,
							is_verified_phone: true,
							is_visited_kost: false,
							is_verified_kost: true,
							is_verified_by_mamikos: true
						}
					},
					{
						_id: 13354696,
						address: '',
						apartment_project_id: 0,
						area_label: 'Langke Rembong, Manggarai, Manggarai',
						available_room: 27,
						cards: [],
						checker: true,
						city: 'Manggarai',
						class: 0,
						class_badge: null,
						click_count: 66,
						expired_status: null,
						fac_bath_ids: [],
						fac_room_ids: [],
						fac_share_ids: [],
						floor: '0',
						furnished_status: 'Not Furnished',
						gender: 0,
						goldplus: 2,
						goldplus_status: 'Mamikos Goldplus 2',
						has_round_photo: false,
						has_video: false,
						is_booking: true,
						is_flash_sale: false,
						is_mamirooms: true,
						is_premium_owner: true,
						is_promoted: false,
						is_testing: false,
						is_top_owner: false,
						keyword: [null],
						label: { label: 'Bebas 24 Jam', priority: 2 },
						label_array: [{ label: 'Bebas 24 Jam', priority: 2 }],
						last_updated: 1606727593,
						level_info: {
							id: 7,
							name: 'Mamikos Goldplus 2',
							is_regular: false,
							flag: null,
							flag_id: null
						},
						love_by_me: false,
						min_month: null,
						not_updated: false,
						online: 'Update 3 Minggu Lalu [3751]',
						other_discounts: [],
						owner_phone: '',
						owner_phone_array: [],
						photo_count: 0,
						photo_url: {
							real:
								'https://static8.kerupux.com/uploads/data/style/2020-06-18/fGG7khp7.jpg',
							small:
								'https://static8.kerupux.com/uploads/cache/data/style/2020-06-18/fGG7khp7-240x320.jpg',
							medium:
								'https://static8.kerupux.com/uploads/cache/data/style/2020-06-18/fGG7khp7-360x480.jpg',
							large:
								'https://static8.kerupux.com/uploads/cache/data/style/2020-06-18/fGG7khp7-540x720.jpg'
						},
						price_title: '1 jt',
						price_title_format: {
							currency_symbol: 'Rp',
							price: '1.000.000',
							rent_type_unit: 'bulan'
						},
						price_title_time: '1.000.000 / bulan',
						promo_title: null,
						rating: 0,
						rating_string: '0.0',
						review_count: 0,
						'room-title': 'kuncara kos2',
						share_url:
							'https://jambu.kerupux.com/room/kost-manggarai-kost-campur-eksklusif-kuncara-kos2',
						size: '4x4',
						status: 0,
						'status-title': 'null',
						status_kos: 'verified',
						subdistrict: 'Langke Rembong',
						top_facility: ['K. Mandi Dalam', 'WiFi', 'Akses 24 Jam'],
						unique_code: '931KK',
						unit_type: null,
						unit_type_rooms: null,
						updated: '2020-11-30 16:13:13',
						verification_status: {
							is_verified_address: true,
							is_verified_phone: true,
							is_visited_kost: false,
							is_verified_kost: true,
							is_verified_by_mamikos: false
						}
					}
				],
				total: 2020,
				isRequestMore: false
			};

			if (response) {
				const rooms = response.rooms || [];

				if (rooms.length > 0) {
					const payload = {
						status: true,
						rooms,
						total: response.total,
						isRequestMore: loadingMore
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
				}
				commit('setHasMore', response['has-more']);
				commit('setResponseData', response);
				commit('setLoading', false);
			} else {
				commit('setEmptyList', true);
				commit('setRooms', { status: false });
				commit('setHasMore', false);
				commit('setLoading', false);
			}
		}
	}
};
