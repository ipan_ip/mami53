import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import 'tests-fe/utils/mock-vue';
import SanJuniperoHeader from 'Js/_sanjunipero/components/SanJuniperoHeader.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

mockWindowProperty('parent', {
	title_header: 'This is title',
	subtitle_header: 'This is Subtitle',
	desktop_header_image: {
		large: 'image_desktop.jpg'
	},
	mobile_header_image: {
		large: 'image_mobile.jpg'
	}
});

describe('SanJuniperoHeader.vue', () => {
	const wrapper = shallowMount(SanJuniperoHeader, {
		localVue
	});

	it('should text correctly', () => {
		const title = wrapper.find('.title');
		const subtitle = wrapper.find('.subtitle');

		expect(title.exists()).toBeTruthy();
		expect(wrapper.vm.title).toBe('This is title');
		expect(title.text()).toBe(wrapper.vm.title);

		expect(subtitle.exists()).toBeTruthy();
		expect(wrapper.vm.subtitle).toBe('This is Subtitle');
		expect(subtitle.text()).toBe(wrapper.vm.subtitle);
	});

	it('should show background image in desktop', () => {
		expect(wrapper.vm.bgImage).toBe('image_desktop.jpg');
	});

	it('should show background image in mobile', () => {
		const wrapper = shallowMount(SanJuniperoHeader, {
			localVue,
			computed: {
				isMobile() {
					return true;
				}
			}
		});

		expect(wrapper.vm.bgImage).toBe('image_mobile.jpg');
	});

	it('should show default background image', () => {
		mockWindowProperty('parent', {
			title_header: 'This is title',
			subtitle_header: 'This is Subtitle'
		});

		const wrapper = shallowMount(SanJuniperoHeader, {
			localVue
		});

		expect(wrapper.vm.bgImage).toBe(
			'/general/img/pictures/sanjunipero/background-header.jpg'
		);
	});
});
