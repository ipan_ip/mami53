import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetKostSubmitted from 'Js/@global/components/content/WidgetKostSubmitted.vue';
import mockWindowProperty from '../../../utils/mock-window-property';

const localVue = createLocalVue();

describe('WidgetKostSubmitted.vue', () => {
	mockWindowProperty('campusAlias', null);
	let wrapper;
	const propsData = {
		job: 'kuliah',
		area: 'Jakal',
		rooms: [
			{
				share_url: 'https://google.com',
				price_title: 'title Example',
				photo_url: {
					large: '/path/to/image.jpg',
					medium: '/path/to/image.jpg',
					small: '/path/to/image.jpg'
				},
				gender: 'Man'
			}
		]
	};

	beforeEach(() => {
		wrapper = shallowMount(WidgetKostSubmitted, {
			localVue,
			propsData
		});
	});

	it('getMoreKostLink method works', () => {
		expect(wrapper.vm.getMoreKostLink).toBe(
			`https://mamikos.com/kost/kost-dekat-${wrapper.vm.area.toLowerCase()}-murah`
		);
		wrapper.setProps({ job: 'intern' });
		expect(wrapper.vm.getMoreKostLink).toBe(
			`https://mamikos.com/kost/kost-${wrapper.vm.area.toLowerCase()}-murah`
		);
	});

	it('isMoreThanOneRoom method works when only one room', () => {
		expect(wrapper.vm.isMoreThanOneRoom).toBe(false);
	});

	it('isMoreThanOneRoom method works when room is more than one', () => {
		propsData.rooms.push({
			share_url: 'https://google.com',
			price_title: 'Kost Murah',
			photo_url: {
				large: '/path/to/image.jpg',
				medium: '/path/to/image.jpg',
				small: '/path/to/image.jpg'
			},
			gender: 'Women'
		});
		wrapper = shallowMount(WidgetKostSubmitted, {
			localVue,
			propsData
		});
		expect(wrapper.vm.isMoreThanOneRoom).toBe(true);
	});
});
