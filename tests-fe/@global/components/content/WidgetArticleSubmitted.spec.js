import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetArticleSubmitted from 'Js/@global/components/content/WidgetArticleSubmitted.vue';

const localVue = createLocalVue();

describe('WidgetArticleSubmitted.vue', () => {
	it('renders correctly', () => {
		const propsData = {
			listArticle: [{ link: 'https://google.com', title: 'title A' }]
		};

		const wrapper = shallowMount(WidgetArticleSubmitted, {
			localVue,
			propsData
		});

		expect(wrapper).toMatchSnapshot();
	});
});
