import { shallowMount, createLocalVue } from '@vue/test-utils';
import '../../../utils/mock-vue';
import WidgetKostContainer from 'Js/@global/components/content/WidgetKostContainer.vue';
import MixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';

const localVue = createLocalVue();
localVue.mixin([MixinGetQueryString]);

const cookies = {
	get: jest.fn(),
	set: jest.fn()
};

window.Cookies = cookies;
global.axios = mockAxios;
window.Vue = require('vue');

jest.mock('Js/@global/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({});
	})
};

global.axios = axios;

describe('WidgetKostContainer.vue', () => {
	const wrapper = shallowMount(WidgetKostContainer, {
		localVue,
		stubs: {
			RoomList: mockComponent,
			WidgetKostDefault: mockComponent,
			WidgetKostSubmitted: mockComponent
		}
	});

	it('call getUserSubmit method', () => {
		const getUserSubmit = jest.fn();
		const getRoomList = jest.fn();
		shallowMount(WidgetKostContainer, {
			localVue,
			stubs: {
				RoomList: mockComponent,
				WidgetKostDefault: mockComponent,
				WidgetKostSubmitted: mockComponent
			},
			methods: { getUserSubmit, getRoomList }
		});
		expect(getUserSubmit).toHaveBeenCalled();
	});

	it('should call getRoomList when userSubmit job is true', () => {
		const spyUpdate = jest.spyOn(wrapper.vm, 'getRoomList');
		wrapper.setMethods({ getRoomList: spyUpdate });

		const userSubmit = {
			job: true,
			area: null
		};

		wrapper.vm.$nextTick(() => {
			wrapper.setData({ userSubmit });
			expect(spyUpdate).toBeCalled();
		});
	});

	it('isLoading works', () => {
		wrapper.vm.getRoomList();
		expect(wrapper.vm.isLoading).toEqual(true);
	});

	it('should get room list data properly', async () => {
		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				kos: null,
				article: null
			}
		});
		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(wrapper.vm.roomData.length).toEqual(0);
	});
});
