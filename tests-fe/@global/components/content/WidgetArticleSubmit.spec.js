import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetArticleSubmit from 'Js/@global/components/content/WidgetArticleSubmit.vue';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();

jest.mock('Js/@global/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('WidgetArticleSubmit.vue', () => {
	beforeEach(() => {
		shallowMount(WidgetArticleSubmit, {
			localVue,
			stubs: {
				WidgetArticleSubmitted: mockComponent
			}
		});
	});

	it('Should call getRelatedArticle', () => {
		expect(global.EventBus.$on).toBeCalledWith(
			'getRelatedArticle',
			expect.any(Function)
		);
	});
});
