import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetArticleBlog from 'Js/@global/components/content/WidgetArticleBlog.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from '../../../utils/mock-window-property';

const localVue = createLocalVue();

jest.mock('Js/@global/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('WidgetArticleBlog.vue', () => {
	mockWindowProperty('newArticle', null);
	const wrapper = shallowMount(WidgetArticleBlog, {
		localVue,
		stubs: {
			WidgetArticleBlogNew: mockComponent
		}
	});

	it('mount WidgetArticleBlog component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
