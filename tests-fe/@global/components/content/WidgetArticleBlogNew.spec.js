import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetArticleBlogNew from 'Js/@global/components/content/WidgetArticleBlogNew.vue';

const localVue = createLocalVue();

describe('WidgetArticleBlogNew.vue', () => {
	it('renders correctly', () => {
		const propsData = {
			listArticle: [{ link: 'https://google.com', title: 'title A' }]
		};
		const wrapper = shallowMount(WidgetArticleBlogNew, {
			propsData,
			localVue
		});
		expect(wrapper).toMatchSnapshot();
	});

	it('listArticle is empty', () => {
		const propsData = {
			listArticle: []
		};
		const wrapper = shallowMount(WidgetArticleBlogNew, {
			propsData,
			localVue
		});
		expect(wrapper.find('li').exists()).toBe(false);
	});
});
