import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetKostDefault from 'Js/@global/components/content/WidgetKostDefault.vue';

const localVue = createLocalVue();

describe('WidgetKostDefault.vue', () => {
	it('renders correctly', () => {
		const propsData = {
			loading: false
		};
		const wrapper = shallowMount(WidgetKostDefault, {
			localVue,
			propsData
		});
		expect(wrapper).toMatchSnapshot();
	});
});
