import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LoginUser from 'Js/@global/components/LoginUser';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD' // token
	};
});

mockWindowProperty('document.head.querySelector', querySelector);

describe('LoginUser.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(LoginUser, {
			propsData: {
				showModal: true
			}
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('#modalUser');
			expect(findContainer().exists()).toBe(true);
		});
	});

	it('should emit close event when fired', () => {
		createComponent();

		wrapper.vm.$emit('close');
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
