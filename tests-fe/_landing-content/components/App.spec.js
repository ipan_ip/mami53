import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import App from 'Js/_landing-content/components/App.vue';

mockWindowProperty('landingContent', {
	type: 'konten',
	seq: '',
	title: '',
	contentOpen: '',
	contentHidden: '',
	buttonLabel: '',
	isLoginRequired: 1
});
mockWindowProperty('latestArticles', []);
mockWindowProperty('authData', {
	all: {
		name: 'User',
		photo_user: ''
	}
});
mockWindowProperty('authCheck', {
	all: false,
	user: false
});

describe('App.vue', () => {
	let wrapper;
	const localVue = createLocalVue();

	const createComponent = options => {
		wrapper = shallowMount(App, {
			localVue,
			stubs: {
				ModalFormLogin: mockComponentWithAttrs({
					class: 'modal-login',
					'@click': "$emit('closeModal')"
				}),
				BgButton: mockComponentWithAttrs({
					class: 'button-login',
					'@click': "$emit('click')"
				})
			},
			...options
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			expect(wrapper.find('.landing-content').exists()).toBe(true);
		});

		it('should show modal login when click buttonLabel', () => {
			createComponent();

			const buttonLogin = wrapper.find('.button-login');
			buttonLogin.trigger('click');
			expect(wrapper.vm.isModalLoginVisible).toBe(true);
		});

		it('should hide modal login when click close', async () => {
			createComponent();
			await wrapper.setData({
				isModalLoginVisible: true
			});

			const modalLogin = wrapper.find('.modal-login');
			expect(modalLogin.exists()).toBe(true);
			modalLogin.trigger('click');
			expect(wrapper.vm.isModalLoginVisible).toBe(false);
		});
	});
});
