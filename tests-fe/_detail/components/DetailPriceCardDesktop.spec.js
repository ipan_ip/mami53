import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailPriceCardDesktop from 'Js/_detail/components/DetailPriceCardDesktop.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import dayjs from 'dayjs';

import detailStore from './__mocks__/detailStore';
import priceCardInfo from './__mocks__/priceCardInfo';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;
localVue.directive('tooltip', jest.fn());
localVue.mixin(mixinNavigator);
localVue.use(Vuex);

const tracker = jest.fn();
global.tracker = tracker;

const store = new Vuex.Store(detailStore);

const BookingInputCheckin = {
	template: '<div class="booking-input-checkin"/>',
	methods: {
		openDatePickerFromOutside: jest.fn()
	}
};

describe('DetailPriceCardDesktop.vue', () => {
	const generateWrapper = ({ mocksProp = {} } = {}) => {
		const mountProp = {
			mocks: {
				...mocksProp
			},
			localVue,
			propsData: {
				priceDaily: '400.000',
				priceMonthly: '4.200.000',
				priceWeekly: '1.800.000',
				priceYearly: '40.200.000',
				priceQuarterly: '13.000.000',
				priceSemiannually: '21.000.000',
				isCheckin: false,
				priceCardInfo
			},
			store,
			stubs: { BookingInputCheckin }
		};

		return shallowMount(DetailPriceCardDesktop, mountProp);
	};

	const wrapper = generateWrapper();

	it('should render price card', async () => {
		expect(wrapper.find('#detailPriceCardDesktop').exists()).toBe(true);
	});

	describe('computed: disabledChat', () => {
		it('should fill disabledChat data with data from store', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.disabledChat).toBe(
					detailStore.state.extras.disabledChat
				);
				done();
			});
		});
	});

	describe('computed: authCheck', () => {
		it('should fill authCheck data with data from store', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
				done();
			});
		});
	});

	describe('computed: isBooking', () => {
		it('should fill isBooking data with data from store', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBooking).toBe(detailStore.state.detail.is_booking);
				done();
			});
		});
	});

	describe('computed: checker', () => {
		it('should fill checker data with data from store', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.checker).toBe(detailStore.state.detail.checker);
				done();
			});
		});
	});

	describe('computed: msgTracking', () => {
		it('should fill msgTracking data with data from store', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTracking).toBe(
					`track-message ${detailStore.state.detail.msg_tracking}`
				);
				done();
			});
		});
	});

	describe('computed: priceZero', () => {
		it('should be true if the all price is 0', done => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceZero).toBe(true);
				done();
			});
		});

		it('should be false if one of the price is not 0', done => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '3000000',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceZero).toBe(false);
				done();
			});
		});
	});

	describe('computed: isBookingDisabled', () => {
		it('should be true if there is room available or price is not empty', done => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBookingDisabled).toBe(
					priceCardInfo.available_room === 0 || wrapper.vm.priceZero
				);
				done();
			});
		});

		it('should disable action button when after night validation is active', () => {
			wrapper.vm.nightValidation = true;
			expect(wrapper.vm.nightValidation).toBe(true);
			expect(wrapper.vm.isBookingDisabled).toBe(true);
		});
	});

	describe('method: openModalPromo', () => {
		it('should emit EventBus openModalPromo', done => {
			wrapper.vm.openModalPromo();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalPromo');
				done();
			});
		});
	});

	describe('method: openModalChat', () => {
		it('should emit EventBus openModalChat', done => {
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalChat');
				done();
			});
		});
	});

	describe('method: claimKost', () => {
		it('should emit EventBus openModalClaim', done => {
			wrapper.vm.claimKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalClaim');
				done();
			});
		});
	});

	describe('method: checkinKost', () => {
		it('should emit EventBus openModalCheckin', done => {
			wrapper.vm.checkinKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalCheckin');
				done();
			});
		});
	});

	describe('method: bookingKost', () => {
		it('should emit EventBus openBookingKost', done => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '3000000',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$store.state.detail.is_booking = true;
			wrapper.vm.$store.state.isCheckinDateFirst = true;
			wrapper.vm.$store.state.params = { checkin: '2020-02-02' };
			wrapper.vm.bookingKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openBookingKost', {
					isAvailable: !wrapper.vm.isBookingDisabled
				});
				done();
			});
		});

		it('should not emit EventBus openBookingKost is not valid', done => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '3000000',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$store.state.detail.is_booking = true;
			wrapper.vm.$store.state.isCheckinDateFirst = true;
			wrapper.vm.$store.state.params = { checkin: '' };
			jest.clearAllMocks();
			wrapper.vm.bookingKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).not.toBeCalledWith('openBookingKost');
				done();
			});
		});
	});

	describe('method: openBookingInputCheckin', () => {
		it('should call openDatePickerFromOutside', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					computed: {
						isBookingDisabled() {
							return false;
						}
					}
				}
			});
			wrapper.vm.openBookingInputCheckin();
			expect(
				BookingInputCheckin.methods.openDatePickerFromOutside
			).toBeCalled();
		});
	});

	describe('method: handleDatePickerOpened', () => {
		it('should handle when checkin date picker openend', async () => {
			const classList = { add: jest.fn(), remove: jest.fn() };
			mockWindowProperty('document.getElementById', id => {
				return {
					classList
				};
			});
			wrapper.vm.handleDatePickerOpened(true);
			expect(classList.remove).toBeCalled();

			wrapper.vm.handleDatePickerOpened(false);
			expect(classList.add).toBeCalled();
		});
	});

	describe('method: setIsOverrideDraftCheckin', () => {
		it('should commit override draft to true', async () => {
			jest.clearAllMocks();
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			wrapper.vm.handleOnDateSelected();
			expect(spyCommit).toBeCalledWith('setIsOverrideDraftCheckin', true);
			spyCommit.mockRestore();
			expect(tracker).toBeCalled();
		});
	});

	describe('method: checkIsValidDate', () => {
		it('should return false if date is not valid', () => {
			expect(wrapper.vm.checkIsValidDate('')).toBe(false);
		});

		it('should return true if date is valid', () => {
			expect(wrapper.vm.checkIsValidDate('2020-02-02')).toBe(true);
		});
	});

	describe('method: toggleAllPrice', () => {
		it('should show price pop up', () => {
			wrapper.vm.isShowAllPrice = false;
			wrapper.vm.toggleAllPrice();
			expect(wrapper.vm.isShowAllPrice).toBe(true);
		});

		it('should close price pop up', () => {
			wrapper.vm.isShowAllPrice = true;
			wrapper.vm.toggleAllPrice();
			expect(wrapper.vm.isShowAllPrice).toBe(false);
		});
	});
});
