import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import Vuex from 'vuex';
import DetailModalReport from 'Js/_detail/components/DetailModalReport.vue';
import detailStore from './__mocks__/detailStore';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');

const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');

global.tracker = jest.fn();

describe('DetailModalReport.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});

	const localVue = createLocalVue();
	localVue.mixin([MixinSwalLoading, MixinSwalSuccessError]);
	localVue.use(Vuex);

	const store = new Vuex.Store(detailStore);

	global.axios = mockAxios;

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetailModalReport, {
			propsData: {
				showModal: true
			},
			localVue,
			store
		});
	});

	it('Should displayed properly', () => {
		expect(wrapper.find('#modalReport').exists()).toBe(true);
	});

	it('Should emit close when closed', () => {
		wrapper.find('.modal-promo__close span').trigger('click');

		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('Should call sendReport method when the form is submitted and return success and track event properly', async () => {
		const mockMoEngageData = {
			user_id: wrapper.vm.authData.all.id,
			is_booking: wrapper.vm.detail.is_booking,
			is_premium: wrapper.vm.detail.is_premium_owner,
			is_promoted: wrapper.vm.detail.is_promoted,
			success_status: 200,
			property_type: wrapper.vm.detailTypeName,
			property_id: wrapper.vm.seq,
			property_city: wrapper.vm.detail.area_city,
			property_name: wrapper.vm.detail.room_title
		};

		axios.mockResolve({
			data: {
				status: 200
			}
		});

		const sendReportSpy = jest.spyOn(wrapper.vm, 'sendReport');
		wrapper.setData({
			report: {
				type: 'photo',
				description: 'Lorem ipsum dolor sit amet'
			}
		});
		wrapper.setMethods({ sendReport: sendReportSpy });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.modal-promo').exists()).toBe(true);
		wrapper.find('.modal-promo').trigger('submit');

		await wrapper.vm.$nextTick();

		expect(sendReportSpy).toBeCalled();
		expect(global.tracker).toBeCalledWith('moe', [
			'[User] Submit Report',
			mockMoEngageData
		]);
	});

	it('Should call sendReport method when the form is submitted and return success with additional error', async () => {
		axios.mockResolve({
			data: {
				status: false,
				meta: {
					message: 'error'
				}
			}
		});

		const sendReportSpy = jest.spyOn(wrapper.vm, 'sendReport');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setData({
			report: {
				type: 'photo',
				description: 'Lorem ipsum dolor sit amet'
			}
		});
		wrapper.setMethods({ sendReport: sendReportSpy });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.modal-promo').exists()).toBe(true);
		wrapper.find('.modal-promo').trigger('submit');

		await wrapper.vm.$nextTick();

		expect(sendReportSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(null, 'error');
	});

	it('Should call sendReport method when the form is submitted and return error', async () => {
		axios.mockReject('error');

		const sendReportSpy = jest.spyOn(wrapper.vm, 'sendReport');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setData({
			report: {
				type: 'photo',
				description: 'Lorem ipsum dolor sit amet'
			}
		});
		wrapper.setMethods({ sendReport: sendReportSpy });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.modal-promo').exists()).toBe(true);
		wrapper.find('.modal-promo').trigger('submit');

		await wrapper.vm.$nextTick();

		expect(sendReportSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Terjadi galat. Silakan coba lagi.'
		);
	});
});
