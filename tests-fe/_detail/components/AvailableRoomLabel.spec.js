import AvailableRoomLabel from 'Js/_detail/components/AvailableRoomLabel.vue';

import { shallowMount } from '@vue/test-utils';

describe('AvailableRoomLabel.vue', () => {
	const wrapper = shallowMount(AvailableRoomLabel, {
		propsData: {
			availableRoom: 0
		}
	});

	it('Computed availableData should return correctly', async () => {
		expect(wrapper.vm.availableData).toEqual({ title: 'Penuh', style: 'grey' });

		await wrapper.setProps({ availableRoom: 2 });

		expect(wrapper.vm.availableData).toEqual({
			title: `Sisa 2 Kamar`,
			style: 'red'
		});
	});
});
