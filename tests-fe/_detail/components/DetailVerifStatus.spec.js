import DetailVerifStatus from 'Js/_detail/components/DetailVerifStatus.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailVerifStatus.vue', () => {
	const wrapper = shallowMount(DetailVerifStatus, {
		localVue,
		store
	});

	it('Computed verificationStatus should return correctly', () => {
		expect(wrapper.vm.verificationStatus).toBe(
			wrapper.vm.$store.state.detail.verification_status
		);
	});

	it('Computed isVerifiedPhone should return correctly', async () => {
		expect(wrapper.vm.isVerifiedPhone).toBeNull();
	});
});
