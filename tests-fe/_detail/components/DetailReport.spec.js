import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import Vuex from 'vuex';
import DetailReport from 'Js/_detail/components/DetailReport.vue';
import detailStore from './__mocks__/detailStore';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');

jest.useFakeTimers();
jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const detailModalReportStub = {
	template: `<div class="modal-stub"><button @click="$emit('close')"></button></div>`
};

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinSwalSuccessError]);

const setTimeout = jest.fn((fn, _) => fn());
Element.prototype.scrollIntoView = jest.fn();

describe('DetailReport.vue', () => {
	const store = new Vuex.Store(detailStore);
	let wrapper;
	beforeEach(() => {
		mockWindowProperty('setTimeout', setTimeout);
		wrapper = shallowMount(DetailReport, {
			localVue,
			stubs: {
				detailModalReport: detailModalReportStub
			},
			store
		});
	});

	it('Should renders properly', () => {
		expect(wrapper.find('.kost-report-container').exists()).toBe(true);
	});

	it('Should renders report popover', async () => {
		wrapper.setData({
			isPopover: true
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.popover-report').exists()).toBe(true);
	});

	it('Should close popover properly', async () => {
		const closePopoverSpy = jest.spyOn(wrapper.vm, 'closePopover');
		wrapper.setMethods({ closePopover: closePopoverSpy });
		wrapper.setData({
			isPopover: true
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.popover-report').exists()).toBe(true);
		wrapper.find('.popover-report .popover-close').trigger('click');

		await wrapper.vm.$nextTick();

		expect(closePopoverSpy).toHaveBeenCalled();
		expect(wrapper.vm.isPopover).toBe(false);
	});

	it('Should show report detail modal properly', async () => {
		wrapper
			.find('.kost-report-container .kost-report .kost-report__request a')
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isPopover).toBe(false);
		expect(wrapper.vm.isModalReport).toBe(true);
	});

	it('Should emit EventBus openModalLogin when the user is not logged in', async () => {
		wrapper.vm.authCheck.all = false;

		wrapper
			.find('.kost-report-container .kost-report .kost-report__request a')
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isPopover).toBe(false);
		expect(global.EventBus.$emit).toBeCalledWith('openModalLogin');
	});

	it('Should show error when user is not tenant but owner while opening modal', async () => {
		wrapper.vm.authCheck.all = true;
		wrapper.vm.authCheck.user = false;
		wrapper.vm.authCheck.owner = true;

		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setMethods({ swalError: swalErrorSpy });

		wrapper
			.find('.kost-report-container .kost-report .kost-report__request a')
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isPopover).toBe(false);
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
		);
	});

	it('Should close report detail modal properly', async () => {
		const closeModalReportSpy = jest.spyOn(wrapper.vm, 'closeModalReport');
		wrapper.setMethods({ closeModalReport: closeModalReportSpy });

		wrapper.find('.modal-stub button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(closeModalReportSpy).toHaveBeenCalled();
		expect(wrapper.vm.isModalReport).toBe(false);
	});

	it('Should triggers scrollIntoView when a chat is replied', () => {
		wrapper.vm.$options.watch.isChatReplied.call(wrapper.vm, true);

		expect(wrapper.vm.isPopover).toBe(true);

		expect(
			wrapper.vm.$refs.kostReportSection.scrollIntoView
		).toHaveBeenCalled();

		expect(setTimeout).toHaveBeenCalledTimes(1);
	});
});
