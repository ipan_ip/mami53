import BadgeSwiper from 'Js/_detail/components/BadgeSwiper.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import kostLevel from './__mocks__/kostLevel.json';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.directive('tooltip', jest.fn());

describe('BadgeSwiper.vue', () => {
	global.axios = mockAxios;
	const notify = jest.fn();
	let wrapper = null;
	mockWindowProperty('bugsnagClient', {
		notify
	});
	const activeBadgesProps = ['moneyBackGuarantee'];

	beforeEach(() => {
		axios.mockResolve({
			data: {
				...kostLevel
			}
		});
		wrapper = shallowMount(BadgeSwiper, {
			localVue,
			propsData: {
				activeBadges: activeBadgesProps
			}
		});
	});

	it('should render badges swiper', () => {
		expect(wrapper.find('.badges-slider-list').exists()).toBe(true);
	});

	it('should render all list badges', () => {
		expect(wrapper.findAll('.badges-slider-item').length).toBe(
			activeBadgesProps.length
		);
	});

	describe('method: getKostLevel', () => {
		describe('resolved Promise', () => {
			beforeEach(() => {
				axios.mockResolve({
					data: {
						...kostLevel
					}
				});
				wrapper = shallowMount(BadgeSwiper, {
					localVue
				});
			});

			it('should update okeKost and superKost', () => {
				expect(wrapper.vm.badges.okeKost.isActive).toBe(false);
				expect(wrapper.vm.badges.superKost.isActive).toBe(false);
			});
		});

		describe('rejected Promise', () => {
			beforeEach(() => {
				axios.mockReject('error');

				wrapper = shallowMount(BadgeSwiper, {
					localVue
				});
			});

			it('should call bugsnagClient notify when fetch data', () => {
				expect(notify).toBeCalled();
			});
		});
	});
});
