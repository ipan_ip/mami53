import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailPriceCardMobile from 'Js/_detail/components/DetailPriceCardMobile.vue';
import MixinBookingAfterNightValidation from 'Js/_booking/request/mixins/MixinBookingAfterNightValidation.js';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';
import priceCardInfo from './__mocks__/priceCardInfo';
import priceCardInfoDisc from './__mocks__/priceCardInfoDisc';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.directive('tooltip', jest.fn());
localVue.mixin([mixinNavigator, MixinBookingAfterNightValidation]);
localVue.use(Vuex);

global.tracker = jest.fn();
global.authData = detailStore.state.authData;

const store = new Vuex.Store(detailStore);

const BookingInputCheckinModal = {
	template: '<div class="booking-input-checkin-modal" />'
};

describe('DetailPriceCardMobile.vue', () => {
	const $router = { currentRoute: { path: '/' } };
	const wrapper = shallowMount(DetailPriceCardMobile, {
		localVue,
		propsData: {
			priceDaily: '400.000',
			priceMonthly: '4.200.000',
			priceWeekly: '1.800.000',
			priceYearly: '40.200.000',
			priceQuarterly: '13.000.000',
			priceSemiannually: '21.000.000',
			isCheckin: false,
			priceCardInfo
		},
		mocks: { $router },
		store,
		stubs: { BookingInputCheckinModal }
	});

	it('should render price card', async () => {
		expect(wrapper.find('.price-card-container').exists()).toBe(true);
		expect(wrapper.find('.price-room').exists()).toBe(true);
	});

	it('Computed isKostBenefitAvailable should return correctly', () => {
		expect(wrapper.vm.isKostBenefitAvailable).toBe(false);
	});

	describe('computed: disabledChat', () => {
		it('should fill disabledChat data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.disabledChat).toBe(
					detailStore.state.extras.disabledChat
				);
			});
		});
	});

	describe('computed: authCheck', () => {
		it('should fill authCheck data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
			});
		});
	});

	describe('computed: isBooking', () => {
		it('should fill isBooking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBooking).toBe(detailStore.state.detail.is_booking);
			});
		});
	});

	describe('computed: msgTracking', () => {
		it('should fill msgTracking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTracking).toBe(
					`track-message ${detailStore.state.detail.msg_tracking}`
				);
			});
		});
	});

	describe('computed: priceFooterFormated', () => {
		it('should create formated price for display', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceFooterFormated).toBe(
					`Rp ${wrapper.vm.priceDefault.price} / ${wrapper.vm.priceDefault.rent_type_unit}`
				);
			});
		});

		it('should create formated price for display with discount', async () => {
			wrapper.setProps({
				priceDaily: '400.000',
				priceMonthly: '4.200.000',
				priceWeekly: '1.800.000',
				priceYearly: '40.200.000',
				priceQuarterly: '13.000.000',
				priceSemiannually: '21.000.000',
				isCheckin: false,
				priceCardInfo: priceCardInfoDisc
			});
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.priceFooterFormated).toEqual(
				`Rp ${wrapper.vm.priceDefault.discount_price} / ${wrapper.vm.priceDefault.rent_type_unit}`
			);
		});
	});

	describe('computed: priceDefault', () => {
		it('should be monthly price if props priceMonthly is not empty', () => {
			wrapper.setProps({
				priceDaily: '400.000',
				priceMonthly: '4.200.000',
				priceWeekly: '1.800.000',
				priceYearly: '40.200.000',
				priceQuarterly: '13.000.000',
				priceSemiannually: '21.000.000',
				isCheckin: false,
				priceCardInfo
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(priceCardInfo.price.price_monthly);
			});
		});

		it('should be semiannually price if props priceSemiannually is not empty', () => {
			wrapper.setProps({
				priceDaily: '0',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '21.000.000'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(
					priceCardInfo.price.price_semiannually
				);
			});
		});

		it('should be yearly price if props priceYearly is not empty', () => {
			wrapper.setProps({
				priceDaily: '0',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '40.200.000',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(priceCardInfo.price.price_yearly);
			});
		});

		it('should be weekly price if props priceWeekly is not empty', () => {
			wrapper.setProps({
				priceDaily: '0',
				priceMonthly: '0',
				priceWeekly: '1.800.000',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(priceCardInfo.price.price_weekly);
			});
		});

		it('should be daily price if props priceDaily is not empty', () => {
			wrapper.setProps({
				priceDaily: '400.000',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(priceCardInfo.price.price_daily);
			});
		});

		it('should be quarterly price if props priceQuarterly is not empty', () => {
			wrapper.setProps({
				priceDaily: '0',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '13.000.000',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.priceDefault).toBe(
					priceCardInfo.price.price_quarterly
				);
			});
		});
	});

	describe('computed: hasDp', () => {
		it('should return true if price detail has dp', () => {
			wrapper.vm.$store.state.priceDetailData = {
				monthly: { down_payment: 7000 }
			};

			expect(wrapper.vm.hasDp).toBe(true);
		});

		it('should return false if price detail has no dp', () => {
			wrapper.vm.$store.state.priceDetailData = {
				monthly: { down_payment: 0 }
			};

			expect(wrapper.vm.hasDp).toBe(true);
		});
		it('should return false if price detail has no dp property', () => {
			wrapper.vm.$store.state.priceDetailData = {
				monthly: {}
			};

			expect(wrapper.vm.hasDp).toBe(true);
		});
	});

	describe('computed: isBookingDisabled', () => {
		it('should be true if the all price is 0', () => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBookingDisabled).toBe(true);
			});
		});

		it('should be false if one of the price is not 0', () => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '3000000',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBookingDisabled).toBe(false);
			});
		});
	});

	describe('watch: isShowCheckinDateModal', () => {
		it('should set new checkin date modal state from props', async () => {
			await wrapper.setData({ isShowBookingInputCheckinModal: false });
			await wrapper.setProps({ isShowCheckinDateModal: true });

			expect(wrapper.vm.isShowBookingInputCheckinModal).toBe(true);
		});
	});

	describe('method: openModalPriceEstimation', () => {
		it('should emit EventBus showPriceEstimationModal', () => {
			wrapper
				.find('.card-price__footer-wrapper .price-room-sub')
				.trigger('click');

			expect(global.EventBus.$emit).toBeCalledWith(
				'showPriceEstimationModal',
				true
			);
		});
	});

	describe('method: openModalPromo', () => {
		it('should emit EventBus openModalPromo', () => {
			wrapper.vm.openModalPromo();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalPromo');
			});
		});
	});

	describe('method: openModalChat', () => {
		it('should emit EventBus openModalChat', () => {
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalChat');
			});
		});
	});

	describe('method: claimKost', () => {
		it('should emit EventBus openModalClaim', () => {
			wrapper.vm.claimKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalClaim');
			});
		});
	});

	describe('method: checkinKost', () => {
		it('should emit EventBus openModalCheckin', () => {
			wrapper.vm.checkinKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalCheckin');
			});
		});
	});

	describe('method: bookingKost', () => {
		it('should emit EventBus openBookingKost if it is valid', () => {
			wrapper.vm.bookingKost(true);

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openBookingKost', {
					isAvailable: !wrapper.vm.isBookingDisabled,
					isSendTracker: true
				});
			});
		});
	});

	describe('method: handleCheckinActionButtonClicked', () => {
		it('should call tracker and booking kost', async () => {
			const spyTracker = jest.spyOn(global, 'tracker');
			const spyBookingKost = jest.spyOn(wrapper.vm, 'bookingKost');
			await wrapper.vm.$nextTick();
			wrapper
				.findComponent(BookingInputCheckinModal)
				.vm.$emit('action-button-clicked');

			expect(spyTracker).toBeCalled();
			expect(spyBookingKost).toBeCalled();
			spyTracker.mockRestore();
			spyBookingKost.mockRestore();
		});
	});

	describe('method: handleOnDateSelected', () => {
		it('should commit override draft to true', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			wrapper.findComponent(BookingInputCheckinModal).vm.$emit('date-selected');
			expect(spyCommit).toBeCalledWith('setIsOverrideDraftCheckin', true);
			spyCommit.mockRestore();
		});
	});

	describe('method: handleBookingFlow', () => {
		it('should emit trackClickBooking and validateOpenBookingKost if is checkin date first', () => {
			wrapper.vm.$store.state.isCheckinDateFirst = true;
			jest.clearAllMocks();
			wrapper.vm.handleBookingFlow();
			expect(global.EventBus.$emit).toBeCalledWith(
				'validateOpenBookingKost',
				expect.any(Object)
			);
		});
	});

	describe('method: showBookingInputCheckinModal', () => {
		it('should set checkin modal to true', () => {
			wrapper.vm.showBookingInputCheckinModal();
			expect(wrapper.vm.isShowBookingInputCheckinModal).toBe(true);
		});
	});

	it('Should call method setBookingRedirectionSource correctly', () => {});
});
