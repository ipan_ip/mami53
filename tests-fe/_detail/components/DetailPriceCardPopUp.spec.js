import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailPriceCardPopUp from 'Js/_detail/components/DetailPriceCardPopUp.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.directive('tooltip', jest.fn());

describe('DetailPriceCardPopUp.vue', () => {
	let priceList = [
		{
			amount: '900.000',
			discount: '1.000.000',
			label: 'Per Bulan',
			name: 'monthly',
			show: true
		},
		{
			amount: '9000.000',
			discount: '10.000.000',
			label: 'Per 6 Bulan',
			name: 'semiannually',
			show: true
		},
		{
			amount: '1.190.000',
			discount: '1.400.000',
			label: 'Per Tahun',
			name: 'yearly',
			show: true
		}
	];

	const wrapper = shallowMount(DetailPriceCardPopUp, {
		localVue,
		propsData: { priceList }
	});

	it('should render the component', () => {
		expect(wrapper.find('#detailPriceCardPopUp').exists()).toBe(true);
	});

	it('should render existed price', () => {
		expect(wrapper.find('[data-path=txt_priceMonthly]').exists()).toBe(true);
		expect(wrapper.find('[data-path=txt_priceSemiannually]').exists()).toBe(
			true
		);
		expect(wrapper.find('[data-path=txt_priceYearly]').exists()).toBe(true);
	});

	it('should emit close event when close is clicked', () => {
		wrapper.vm.closePrice();
		expect(wrapper.emitted('close')).toBeTruthy();
	});
});
