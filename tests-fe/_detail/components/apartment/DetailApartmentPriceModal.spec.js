import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailApartmentPriceModal from 'Js/_detail/components/apartment/DetailApartmentPriceModal.vue';
import detailStore from '../__mocks__/detailApartmentStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const modalStub = {
	template: `<button class="close" @click="$emit('cancel')"/>`
};

const closeModalSpy = jest.spyOn(
	DetailApartmentPriceModal.methods,
	'closeModal'
);

describe('DetailApartmentPriceModal.vue', () => {
	const store = new Vuex.Store(detailStore);

	const wrapper = shallowMount(DetailApartmentPriceModal, {
		localVue,
		store,
		stubs: {
			modal: modalStub
		},
		propsData: {
			showModal: false
		}
	});

	it('Computed priceDaily should return correctly from store', () => {
		expect(wrapper.vm.priceDaily).toBe(
			wrapper.vm.$store.state.detail.price_daily
		);
	});

	it('Computed priceWeekly should return correctly from store', () => {
		expect(wrapper.vm.priceWeekly).toBe(
			wrapper.vm.$store.state.detail.price_weekly
		);
	});

	it('Computed priceMonthly should return correctly from store', () => {
		expect(wrapper.vm.priceMonthly).toBe(
			wrapper.vm.$store.state.detail.price_monthly
		);
	});

	it('Computed priceYearly should return correctly from store', () => {
		expect(wrapper.vm.priceYearly).toBe(
			wrapper.vm.$store.state.detail.price_yearly
		);
	});

	it('Should call method close on closing the modal', async () => {
		await wrapper.setProps({ showModal: true });

		const closeElement = wrapper.find('button.close');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalSpy).toBeCalled();
	});
});
