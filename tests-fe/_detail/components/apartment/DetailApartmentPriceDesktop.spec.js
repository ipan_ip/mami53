import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailApartmentPriceDesktop from 'Js/_detail/components/apartment/DetailApartmentPriceDesktop.vue';
import detailStore from '../__mocks__/detailApartmentStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const openModalChatSpy = jest.spyOn(
	DetailApartmentPriceDesktop.methods,
	'openModalChat'
);
const openModalPromoSpy = jest.spyOn(
	DetailApartmentPriceDesktop.methods,
	'openModalPromo'
);

describe('DetailApartmentPriceDesktop.vue', () => {
	const store = new Vuex.Store(detailStore);

	const wrapper = shallowMount(DetailApartmentPriceDesktop, {
		localVue,
		store
	});

	it('Computed updatedAt should return correctly from the store', () => {
		expect(wrapper.vm.updatedAt).toBe(
			wrapper.vm.$store.state.detail.updated_at
		);
	});

	it('Computed priceDaily should return correctly from store', () => {
		expect(wrapper.vm.priceDaily).toBe(
			wrapper.vm.$store.state.detail.price_daily
		);
	});

	it('Computed priceWeekly should return correctly from store', () => {
		expect(wrapper.vm.priceWeekly).toBe(
			wrapper.vm.$store.state.detail.price_weekly
		);
	});

	it('Computed priceMonthly should return correctly from store', () => {
		expect(wrapper.vm.priceMonthly).toBe(
			wrapper.vm.$store.state.detail.price_monthly
		);
	});

	it('Computed priceYearly should return correctly from store', () => {
		expect(wrapper.vm.priceYearly).toBe(
			wrapper.vm.$store.state.detail.price_yearly
		);
	});

	it('Computed priceComponents should return correctly with static data', () => {
		expect(wrapper.vm.priceComponents).toEqual([]);
	});

	it('Computed priceMin should return correctly from the store', () => {
		expect(wrapper.vm.priceMin).toBe(
			wrapper.vm.$store.state.detail.fac_keyword
		);
	});

	it('Computed msgTracking should return correctly', () => {
		expect(wrapper.vm.msgTracking).toBe('track-message track-message-apt');
	});

	it('Computed isBooking should return correctly', () => {
		expect(wrapper.vm.isBooking).toBe(
			wrapper.vm.$store.state.detail.is_booking
		);
	});

	it('Computed checker should return correctly', () => {
		expect(wrapper.vm.checker).toBeNull();
	});

	it('Computed promotion should return correctly', () => {
		expect(wrapper.vm.promotion).toBe(wrapper.vm.$store.state.detail.promotion);
	});

	it('Should call method openModalChat when clicking the span', async () => {
		const chatElement = wrapper.find('button.track-message');

		expect(chatElement.exists()).toBe(true);

		await chatElement.trigger('click');

		expect(openModalChatSpy).toBeCalled();
	});

	it('Should call method openModalPromo when clicking the span', async () => {
		const promoElement = wrapper.find('span.text-primary');

		expect(promoElement.exists()).toBe(true);

		await promoElement.trigger('click');

		expect(openModalPromoSpy).toBeCalled();
	});
});
