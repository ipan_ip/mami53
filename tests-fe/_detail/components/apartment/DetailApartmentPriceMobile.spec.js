import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailApartmentPriceMobile from 'Js/_detail/components/apartment/DetailApartmentPriceMobile.vue';
import detailStore from '../__mocks__/detailApartmentStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const openModalPriceSpy = jest.spyOn(
	DetailApartmentPriceMobile.methods,
	'openModalPrice'
);
const openModalChatSpy = jest.spyOn(
	DetailApartmentPriceMobile.methods,
	'openModalChat'
);
const openModalPromoSpy = jest.spyOn(
	DetailApartmentPriceMobile.methods,
	'openModalPromo'
);

describe('DetailApartmentPriceMobile.vue', () => {
	const store = new Vuex.Store(detailStore);

	const wrapper = shallowMount(DetailApartmentPriceMobile, {
		localVue,
		store
	});

	it('Computed priceOtherComponents should return correctly from the store', () => {
		expect(wrapper.vm.priceOtherComponents).toEqual(
			wrapper.vm.$store.state.detail.price_components
		);
	});

	it('Computed priceMin should return correctly from the store', () => {
		expect(wrapper.vm.priceMin).toBe(
			wrapper.vm.$store.state.detail.fac_keyword
		);
	});

	it('Computed promotion should return correctly from the store', () => {
		expect(wrapper.vm.promotion).toBe(wrapper.vm.$store.state.detail.promotion);
	});

	it('Computed priceComponents should return correctly with static data', () => {
		expect(wrapper.vm.priceComponents).toEqual([
			{
				label: 'hari',
				price: wrapper.vm.$store.state.detail.price_daily
			},
			{
				label: 'minggu',
				price: wrapper.vm.$store.state.detail.price_weekly
			},
			{
				label: 'bulan',
				price: wrapper.vm.$store.state.detail.price_monthly
			},
			{
				label: 'tahun',
				price: wrapper.vm.$store.state.detail.price_yearly
			}
		]);
	});

	it('Computed priceTitle should return correctly from the store', () => {
		expect(wrapper.vm.priceTitle).toBe(
			wrapper.vm.$store.state.detail.price_title
		);
	});

	it('Computed priceShown should return correctly from the store', () => {
		expect(wrapper.vm.priceShown).toEqual([{ label: 'bulan', price: 4500000 }]);
	});

	it('Computed msgTracking should return correctly', () => {
		expect(wrapper.vm.msgTracking).toBe('track-message track-message-apt');
	});

	it('Should call method openModalPrice when clicking the span', async () => {
		const openElement = wrapper.find('span.price-room-sub');

		expect(openElement.exists()).toBe(true);

		await openElement.trigger('click');

		expect(openModalPriceSpy).toBeCalled();
	});

	it('Should call method openModalChat when clicking the span', async () => {
		const chatElement = wrapper.find('button.track-message');

		expect(chatElement.exists()).toBe(true);

		await chatElement.trigger('click');

		expect(openModalChatSpy).toBeCalled();
	});

	it('Should call method openModalPromo when clicking the span', async () => {
		const promoElement = wrapper.find('span.text-primary');

		expect(promoElement.exists()).toBe(true);

		await promoElement.trigger('click');

		expect(openModalPromoSpy).toBeCalled();
	});
});
