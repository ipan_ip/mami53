import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailApartmentFacilities from 'Js/_detail/components/apartment/DetailApartmentFacilities.vue';
import detailStore from '../__mocks__/detailApartmentStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

describe('DetailApartmentFacilities.vue', () => {
	const store = new Vuex.Store(detailStore);

	const wrapper = shallowMount(DetailApartmentFacilities, {
		localVue,
		store
	});

	it('Computed unitProp should return correctly from store', () => {
		expect(wrapper.vm.unitProp).toEqual(
			wrapper.vm.$store.state.detail.unit_properties
		);
	});

	it('Computed facUnit should return correctly from store', () => {
		expect(wrapper.vm.facUnit).toEqual(
			wrapper.vm.$store.state.detail.fac_room_icon
		);
	});

	it('Computed facDesc should return correctly from store', () => {
		expect(wrapper.vm.facDesc).toEqual(
			wrapper.vm.$store.state.detail.description
		);
	});

	it('Computed facProject should return correctly from store', () => {
		expect(wrapper.vm.facProject).toEqual(
			wrapper.vm.$store.state.detail.project.facilities
		);
	});
});
