import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import ContainerDetailApartment from 'Js/_detail/components/apartment/ContainerDetailApartment.vue';
import detailStore from '../__mocks__/detailApartmentStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import EventBus from 'Js/_detail/event-bus';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const listenEventBusSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'listenEventBus'
);
const checkOpenedChatSectionSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'checkOpenedChatSection'
);
const openModalLoginSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'openModalLogin'
);
const closeModalPriceSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'closeModalPrice'
);
const closeModalLoginSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'closeModalLogin'
);
const closeModalPromoSpy = jest.spyOn(
	ContainerDetailApartment.methods,
	'closeModalPromo'
);

const modalPriceStub = {
	template: `<button class="close-modal-price" @click="$emit('close')"/>`
};
const modalPromoStub = {
	template: `<button class="close-modal-promo" @click="$emit('close')"/>`
};
const modalLoginStub = {
	template: `<button class="close-modal-login" @click="$emit('closeModal')"/>`
};

const stubs = {
	'detail-apartment-price-modal': modalPriceStub,
	'detail-modal-promo': modalPromoStub,
	'modal-form-login': modalLoginStub
};

global.document.querySelector = jest.fn(() => ({
	childElementCount: jest.fn()
}));

describe('ContainerDetailApartment.vue', () => {
	const store = new Vuex.Store(detailStore);

	const wrapper = shallowMount(ContainerDetailApartment, {
		localVue,
		store,
		stubs
	});

	it('Computed authCheck should return correctly from store', () => {
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('Computed breadcrumb should return correctly from store', () => {
		expect(wrapper.vm.breadcrumb).toEqual(
			wrapper.vm.$store.state.detail.breadcrumbs
		);
	});

	it('Computed promotion should return correctly from store', () => {
		expect(wrapper.vm.promotion).toEqual(
			wrapper.vm.$store.state.detail.promotion
		);
	});

	it('Computed photo360 should return null if not exist', () => {
		expect(wrapper.vm.photo360).toBeNull();
	});

	it('Computed youtubeId should return null if not exist', () => {
		expect(wrapper.vm.youtubeId).toBeNull();
	});

	it('Computed matterportId should return null if not exist', () => {
		expect(wrapper.vm.matterportId).toBeNull();
	});

	it('Computed hasMedia should return false if media not exist', () => {
		expect(wrapper.vm.hasMedia).toBe(false);
	});

	it('Computed isDesktopFavShare should return correctly', () => {
		expect(wrapper.vm.isDesktopFavShare).toBe(true);
	});

	it('Computed priceMonthly should return correctly', () => {
		expect(wrapper.vm.priceMonthly).toBe('4,500,000');
	});

	it('Should call listenEventBus method on created', () => {
		expect(listenEventBusSpy).toBeCalled();
	});

	it('Should call closeModalPrice method correctly', () => {
		expect(listenEventBusSpy).toBeCalled();
	});

	it('Should call method closeModalPrice correctly', async () => {
		EventBus.$emit('openModalPrice');

		expect(wrapper.vm.isModalPrice).toBe(true);

		const closeElement = wrapper.find('button.close-modal-price');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalPriceSpy).toBeCalled();
		expect(wrapper.vm.isModalPrice).toBe(false);
	});

	it('Should call method closeModalPromo correctly', async () => {
		EventBus.$emit('openModalPromo');

		expect(wrapper.vm.isModalPromo).toBe(true);

		const closeElement = wrapper.find('button.close-modal-promo');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalPromoSpy).toBeCalled();
		expect(wrapper.vm.isModalPromo).toBe(false);
	});

	it('Should call toggle method of login modal correctly', async () => {
		EventBus.$emit('openModalLogin');

		expect(openModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLoginVisible).toBe(true);

		const closeElement = wrapper.find('button.close-modal-login');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLoginVisible).toBe(false);
	});

	it('Should call method checkOpenedChatSection correctly', async () => {
		wrapper.vm.$refs.detailModalChat.assignSelectedQuestion = jest.fn();

		EventBus.$emit('openModalChat');

		expect(checkOpenedChatSectionSpy).toBeCalled();
	});
});
