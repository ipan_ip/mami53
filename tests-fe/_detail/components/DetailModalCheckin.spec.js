import { shallowMount } from '@vue/test-utils';
import DetailModalCheckin from 'Js/_detail/components/DetailModalCheckin.vue';

window.Vue = require('vue');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('DetailModalCheckin.vue', () => {
	const wrapper = shallowMount(DetailModalCheckin, {
		propsData: {
			showModal: true
		}
	});

	it('should render price card', async () => {
		expect(wrapper.find('#modalCheckin').exists()).toBe(true);
	});

	describe('method: closeModal', () => {
		it('should emit close', () => {
			const { closeModal } = wrapper.vm;

			closeModal();
			expect(wrapper.emitted('close')).toBeTruthy();
		});
	});

	describe('method: sendCheckinKost', () => {
		it('should emit EventBus sendCheckinKost', () => {
			wrapper.vm.sendCheckinKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith(
					'sendCheckinKost',
					wrapper.vm.checkinDate
				);
			});
		});
	});

	describe('method: assignDefaultDate', () => {
		it('should correctly format default date when month is 1 digit', () => {
			const currentDate = new Date('2020-01-01T00:00:00.000Z');
			const realDate = Date;
			global.Date = class extends Date {
				constructor(date) {
					if (date) {
						return super(date);
					}

					return currentDate;
				}
			};

			wrapper.vm.assignDefaultDate();

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.checkinDate).toBe('2020-01-1');
				global.Date = realDate;
			});
		});

		it('should correctly format default date when month is 2 digit', () => {
			const currentDate = new Date('2020-12-01T00:00:00.000Z');
			const realDate = Date;
			global.Date = class extends Date {
				constructor(date) {
					if (date) {
						return super(date);
					}

					return currentDate;
				}
			};

			wrapper.vm.assignDefaultDate();

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.checkinDate).toBe('2020-12-1');
				global.Date = realDate;
			});
		});
	});
});
