import BookingBenefitTrigger from 'Js/_detail/components/BookingBenefitTrigger.vue';

import { shallowMount } from '@vue/test-utils';

const handleClickInsideComponentSpy = jest.spyOn(
	BookingBenefitTrigger.methods,
	'handleClickInsideComponent'
);

describe('BookingBenefitTrigger.vue', () => {
	const wrapper = shallowMount(BookingBenefitTrigger, {
		propsData: {
			onLearnMore: jest.fn()
		}
	});

	it('Method handleClickInsideComponent should called correctly', async () => {
		const clickElement = wrapper.find('div.bookin-benefit-trigger');

		expect(clickElement.exists()).toBe(true);

		await clickElement.trigger('click');

		expect(handleClickInsideComponentSpy).toBeCalled();
	});
});
