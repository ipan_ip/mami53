import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailMap from 'Js/_detail/components/DetailMap.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

window.Vue = require('vue');
const mixinDetailType = require('Js/_detail/mixins/MixinDetailType');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

global.osmHost = 'https://osm.mamikos.com';
global.tracker = jest.fn();
global.L = {
	tileLayer: jest.fn(() => ({
		addTo: jest.fn()
	})),
	control: {
		zoom: jest.fn().mockReturnValue({
			addTo: jest.fn()
		})
	},
	map: jest.fn().mockReturnValue({
		attributionControl: {
			addAttribution: jest.fn()
		},
		scrollWheelZoom: {
			disable: jest.fn()
		}
	}),
	circleMarker: jest.fn().mockReturnValue({
		addTo: jest.fn().mockReturnValue({
			bindTooltip: jest.fn().mockReturnValue({
				openTooltip: jest.fn()
			})
		})
	}),
	tooltip: jest.fn().mockReturnValue({
		setContent: jest.fn(),
		getElement: jest.fn().mockReturnValue({
			classList: {
				add: jest.fn()
			},
			addEventListener: jest.fn((type, func) => func())
		})
	})
};

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(mixinDetailType);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);
const initMapObjectFn = jest.spyOn(DetailMap.methods, 'initMapObject');
const initMapAttributionFn = jest.spyOn(
	DetailMap.methods,
	'initMapAttribution'
);
const initMapLayerFn = jest.spyOn(DetailMap.methods, 'initMapLayer');
const initMapMarkerTooltipFn = jest.spyOn(
	DetailMap.methods,
	'initMapMarkerTooltip'
);
const initMapCustomControlFn = jest.spyOn(
	DetailMap.methods,
	'initMapCustomControl'
);
const initMapSpy = jest.spyOn(DetailMap.methods, 'initMap');

describe('DetailMap.vue', () => {
	const wrapper = shallowMount(DetailMap, {
		localVue,
		store
	});

	it('should render map', async () => {
		wrapper.vm.initMapMarkerTooltip();
		expect(wrapper.find('.map-section').exists()).toBe(true);
		expect(wrapper.find('.btn-default').exists()).toBe(true);
	});

	describe('computed: authCheck', () => {
		it('should fill authCheck data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
			});
		});
	});

	describe('computed: longitude', () => {
		it('should fill longitude data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.longitude).toBe(detailStore.state.detail.location[0]);
			});
		});
	});

	describe('computed: latitude', () => {
		it('should fill latitude data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.latitude).toBe(detailStore.state.detail.location[1]);
			});
		});
	});

	describe('computed: latLng', () => {
		it('should fill latLng data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.latLng).toMatchObject({
					lat: wrapper.vm.latitude || null,
					lng: wrapper.vm.longitude || null
				});
			});
		});
	});

	describe('computed: subdistrict', () => {
		it('should fill subdistrict data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.subdistrict).toBe(
					detailStore.state.detail.area_subdistrict
				);
			});
		});
	});

	describe('computed: city', () => {
		it('should fill city data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.city).toBe(detailStore.state.detail.area_city);
			});
		});
	});

	describe('computed: bigCity', () => {
		it('should fill bigCity data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.bigCity).toBe(
					detailStore.state.detail.area_city_keyword
				);
			});
		});
	});

	describe('computed: fullCityName', () => {
		it('should fill fullCityName data with subdistrict, city, and big city', () => {
			wrapper.vm.$nextTick(() => {
				const cityNameResult = [];
				cityNameResult.push(wrapper.vm.subdistrict);
				cityNameResult.push(wrapper.vm.city);
				cityNameResult.push(wrapper.vm.bigCity);

				expect(wrapper.vm.fullCityName).toBe(cityNameResult.join(', '));
			});
		});
	});

	describe('computed: msgTrackingRaw', () => {
		it('should fill msgTrackingRaw data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTrackingRaw).toBe(
					detailStore.state.detail.msg_tracking
				);
			});
		});
	});

	describe('computed: msgTracking', () => {
		it('should fill msgTracking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTracking).toBe(
					`track-message ${detailStore.state.detail.msg_tracking}`
				);
			});
		});
	});

	describe('computed: msgTrackingMap', () => {
		it('should fill msgTrackingMap data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTrackingMap).toBe(
					`${detailStore.state.detail.msg_tracking}-map`
				);
			});
		});
	});

	describe('computed: isLimitedContent', () => {
		it('should fill isLimitedContent data with longitude and latitude', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isLimitedContent).toBe(
					detailStore.state.extras.isLimitedContent
				);
			});
		});
	});

	describe('method: initMap', () => {
		it('should call methoda for map initiation', () => {
			wrapper.vm.initMap();

			wrapper.vm.$nextTick(() => {
				expect(initMapObjectFn).toBeCalled();
				expect(initMapAttributionFn).toBeCalled();
				expect(initMapLayerFn).toBeCalled();
				expect(initMapMarkerTooltipFn).toBeCalled();
				expect(initMapCustomControlFn).toBeCalled();
			});
		});
	});

	describe('method: initMapObject', () => {
		it('should call L map method and pass correct property', () => {
			wrapper.vm.initMapObject();

			wrapper.vm.$nextTick(() => {
				expect(global.L.map).toBeCalledWith('detailMap', {
					center: wrapper.vm.latLng,
					zoom: 16,
					minZoom: 10,
					maxZoom: 18,
					zoomControl: false
				});
			});
		});
	});

	describe('method: initMapLayer', () => {
		it('should call L tileLayer method and pass image link', () => {
			wrapper.vm.initMapLayer();

			wrapper.vm.$nextTick(() => {
				expect(global.L.tileLayer).toBeCalledWith(
					`${global.osmHost}/hot/{z}/{x}/{y}.png`
				);
			});
		});
	});

	describe('method: initMapCustomControl', () => {
		it('should call L control method and pass custom setting', () => {
			wrapper.setData({
				map: {
					scrollWheelZoom: {
						disable: jest.fn()
					}
				}
			});
			wrapper.vm.initMapCustomControl();

			wrapper.vm.$nextTick(() => {
				expect(global.L.control.zoom).toBeCalledWith({
					position: 'bottomright'
				});
			});
		});
	});

	describe('method: openModalChat', () => {
		it('should call EventBus emit sendPretextChat', () => {
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('sendPretextChat', {
					string: 'Tanya alamat ?',
					for: 'kos'
				});
			});
		});
	});

	describe('method: openModalLogin', () => {
		it('should update isModalLoginVisible  to true', () => {
			const sendMapTrackerFn = jest.spyOn(wrapper.vm, 'sendMapTracker');
			wrapper.setMethods({ sendMapTracker: sendMapTrackerFn });
			wrapper.vm.openModalLogin();

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isModalLoginVisible).toBe(true);
				expect(sendMapTrackerFn).toHaveBeenCalled();
				sendMapTrackerFn.mockRestore();
			});
		});
	});

	describe('method: closeModalLogin', () => {
		it('should update isModalLoginVisible  to false', () => {
			wrapper.vm.closeModalLogin();

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isModalLoginVisible).toBe(false);
			});
		});
	});

	describe('method: sendMapTracker', () => {
		it('should send correct tracking property', () => {
			wrapper.vm.sendMapTracker();

			expect(global.tracker).toBeCalledWith('moe', [
				'[User] Detail Property - Click Lihat Peta',
				{
					is_limitedcontent: wrapper.vm.isLimitedContent,
					is_premium: detailStore.state.detail.is_premium_owner || false,
					is_promoted: detailStore.state.detail.is_promoted || false,
					is_booking: detailStore.state.detail.is_booking || false,
					is_login: wrapper.vm.authCheck.all || null,
					property_type: wrapper.vm.detailTypeName || null,
					property_id: detailStore.state.detail.seq || null,
					property_city: detailStore.state.detail.area_city,
					property_name: detailStore.state.detail.room_title || null
				}
			]);
		});
	});

	it('should init marker with type apartment', async () => {
		const wrapper = shallowMount(DetailMap, {
			localVue,
			store,
			computed: {
				detailType() {
					return 'apartment';
				}
			}
		});

		const toggleElement = wrapper.find('button.btn-default');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(initMapSpy).toBeCalled();
	});
});
