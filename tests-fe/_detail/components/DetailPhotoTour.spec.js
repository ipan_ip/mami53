import DetailPhotoTour from 'Js/_detail/components/DetailPhotoTour.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailPhotoTour.vue', () => {
	const wrapper = shallowMount(DetailPhotoTour, {
		localVue,
		store
	});

	it('Computed matterportId should return correctly', () => {
		expect(wrapper.vm.matterportId).toBe(
			wrapper.vm.$store.state.detail.matterport_id
		);
	});

	it('Computed iframeSource should return correctly', async () => {
		expect(wrapper.vm.iframeSource).toBe('');

		await wrapper.setProps({ isTour: true });

		expect(wrapper.vm.iframeSource).toBe(
			`https://my.matterport.com/show/?m=123&hl=1&play=1`
		);
	});
});
