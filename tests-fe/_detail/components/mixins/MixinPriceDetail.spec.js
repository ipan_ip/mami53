import { shallowMount } from '@vue/test-utils';
import MixinPriceDetail from 'Js/_detail/mixins/MixinPriceDetail.js';
import priceDetailData from '../__mocks__/priceDetailData.json';
import { flushPromises } from 'tests-fe/utils/promises';

const MockComponent = {
	template: '<div />',
	mixins: [MixinPriceDetail],
	render: h => h('div')
};

const axios = {
	get: jest
		.fn()
		.mockResolvedValueOnce({ data: { data: { nice: true }, status: true } })
		.mockResolvedValueOnce({ data: {}, status: false })
		.mockRejectedValueOnce(new Error('error'))
};

const bugsnagClient = { notify: jest.fn() };

global.axios = axios;
global.bugsnagClient = bugsnagClient;

const availablePrices = {
	monthly: { price: 50000, unitLabel: 'Per Bulan' }
};

describe('MixinPriceDetail.js', () => {
	const wrapper = shallowMount(MockComponent, {
		mocks: {
			$store: {
				state: {
					priceDetailState: 'loaded',
					priceDetailData,
					params: {
						rent_count_type: 'monthly'
					},
					detail: { _id: 123 }
				},
				getters: {
					availablePrices,
					defaultRentType: 'monthly',
					defaultRentTypeLabel: 'Per Bulan'
				},
				commit: jest.fn(),
				dispatch: jest.fn()
			}
		}
	});

	it('should return data properly', () => {
		expect(wrapper.vm.priceDetailState).toBe('loaded');
		expect(wrapper.vm.isLoadedPriceEstimation).toBe(true);
		expect(wrapper.vm.isLoadingPriceEstimation).toBe(false);
		expect(wrapper.vm.rentTypeParams).toBe('monthly');
		expect(wrapper.vm.availablePriceKeys).toEqual(['monthly']);
		expect(wrapper.vm.defaultRentType).toBe('monthly');
		expect(wrapper.vm.defaultRentTypeLabel).toBe('Per Bulan');
		expect(wrapper.vm.selectedRentPrice).toBe(availablePrices.monthly);
		expect(wrapper.vm.songId).toBe(123);
	});

	it('should set data properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.setData({ rentTypeParams: 'quarterly' });
		expect(spyCommit).toBeCalledWith('setRentCountType', 'quarterly');

		wrapper.setData({ priceDetailState: 'failed' });
		expect(spyCommit).toBeCalledWith('setPriceDetailState', 'failed');
		spyCommit.mockRestore();
	});

	it('should not get price estimation if it is loading', () => {
		wrapper.vm.$store.state.priceDetailState = 'loading';
		wrapper.vm.getBookingPriceEstimation();
		expect(axios.get).not.toBeCalled();
	});

	it('should set price detail properly', async () => {
		const spyDispatch = jest.spyOn(wrapper.vm.$store, 'dispatch');
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.$store.state.priceDetailState = '';
		await wrapper.vm.getBookingPriceEstimation();
		await flushPromises();
		expect(axios.get).toBeCalledWith('/user/booking/draft-price/123', {
			params: { is_flash_sale: false }
		});
		expect(spyDispatch).toBeCalledWith(
			'setFormattedPriceDetail',
			expect.objectContaining({ nice: true })
		);
		expect(spyCommit).toBeCalledWith('setPriceDetailState', 'loaded');
		spyDispatch.mockRestore();
	});

	it('should not set price detail if status false', async () => {
		const spyDispatch = jest.spyOn(wrapper.vm.$store, 'dispatch');
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.$store.state.priceDetailState = '';
		await wrapper.vm.getBookingPriceEstimation();
		await flushPromises();
		expect(axios.get).toBeCalledWith('/user/booking/draft-price/123', {
			params: { is_flash_sale: false }
		});
		expect(spyDispatch).not.toBeCalledWith(
			'setFormattedPriceDetail',
			expect.objectContaining({ nice: true })
		);
		expect(spyCommit).toBeCalledWith('setPriceDetailState', 'failed');
		spyDispatch.mockRestore();
	});

	it('should not set price detail if error', async () => {
		const spyDispatch = jest.spyOn(wrapper.vm.$store, 'dispatch');
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.$store.state.priceDetailState = '';
		await wrapper.vm.getBookingPriceEstimation();
		await flushPromises();
		expect(axios.get).toBeCalledWith('/user/booking/draft-price/123', {
			params: { is_flash_sale: false }
		});
		expect(spyDispatch).not.toBeCalledWith(
			'setFormattedPriceDetail',
			expect.objectContaining({ nice: true })
		);
		expect(spyCommit).toBeCalledWith('setPriceDetailState', 'failed');
		expect(bugsnagClient.notify).toBeCalled();
		spyDispatch.mockRestore();
	});
});
