import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import MixinDetailType from 'Js/_detail/mixins/MixinDetailType.js';
import detailStore from '../__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const MockComponent = {
	template: '<div />',
	mixins: [MixinDetailType],
	render: h => h('div')
};

const store = new Vuex.Store(detailStore);

describe('MixinDetailType.js', () => {
	const wrapper = shallowMount(MockComponent, {
		localVue,
		store
	});

	it('should return data properly', () => {
		expect(wrapper.vm.detailType).toBe('kost');
		expect(wrapper.vm.detailTypeName).toBe('Kost');
	});
});
