import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import MixinKostReview from 'Js/_detail/mixins/MixinKostReview.js';

const localVue = createLocalVue();

const MockComponent = {
	template: '<div />',
	mixins: [MixinKostReview],
	render: h => h('div')
};

describe('MixinKostReview.js', () => {
	const wrapper = shallowMount(MockComponent, {
		localVue
	});

	it('should handle method handleCalculateReviewDate properly', () => {
		const testCase = [
			{
				day: 1,
				text: 'Hari ini'
			},
			{
				day: 2,
				text: 'Kemarin'
			},
			{
				day: 3,
				text: '3 hari yang lalu'
			},
			{
				day: 2,
				text: 'Kemarin'
			},
			{
				day: 21,
				text: '3 minggu yang lalu'
			},
			{
				day: 90,
				text: '3 bulan yang lalu'
			},
			{
				day: 730,
				text: '2 tahun yang lalu'
			}
		];

		for (const test of testCase) {
			const result = wrapper.vm.handleCalculateReviewDate(test.day);
			expect(result).toBe(test.text);
		}
	});

	it('should handle method handleCalculateReviewDate properly', () => {
		expect(MixinKostReview.filters.ratingText(2)).toBe('2.0');

		expect(MixinKostReview.filters.ratingText('mock')).toBe('mock');
	});
});
