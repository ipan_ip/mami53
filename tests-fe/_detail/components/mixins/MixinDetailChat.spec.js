import { shallowMount, createLocalVue } from '@vue/test-utils';
import MixinDetailChat from 'Js/_detail/mixins/MixinDetailChat.js';
import detailStore from '../__mocks__/detailStore';
import Vuex from 'vuex';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

jest.mock('Js/@utils/makeAPICall.js');

const openSwalLoadingSpy = jest.fn();
const closeSwalLoadingSpy = jest.fn();
const swalErrorSpy = jest.fn();

const Component = {
	mixins: [MixinDetailChat],
	render() {},
	methods: {
		openSwalLoading: openSwalLoadingSpy,
		closeSwalLoading: closeSwalLoadingSpy,
		swalError: swalErrorSpy
	}
};

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

global.axios = {
	post: jest
		.fn()
		.mockResolvedValueOnce({
			data: {
				status: true,
				admin_id: 1,
				admin_list: [2],
				call_id: 3,
				group_channel_url: 'channel_url'
			}
		})
		.mockResolvedValueOnce({
			data: {
				status: true,
				admin_id: 1,
				admin_list: [2],
				call_id: 3,
				group_channel_url: null
			}
		})
		.mockResolvedValueOnce({
			data: {
				status: false
			}
		})
		.mockResolvedValueOnce(new Error('API error'))
};
global.Cookies = {
	get: jest
		.fn()
		.mockResolvedValueOnce(1)
		.mockResolvedValueOnce(0),
	set: jest.fn()
};

makeAPICall.mockReturnValueOnce({});

global.bugsnagClient = {
	notify: jest.fn()
};

describe('MixinDetailChat.js', () => {
	const generateWrapper = () => {
		return shallowMount(Component, {
			localVue,
			store,
			data() {
				return {
					detailType: 'kost'
				};
			}
		});
	};

	let wrapper = generateWrapper();

	it('Computed token should return correctly from the store', () => {
		expect(wrapper.vm.token).toBe(wrapper.vm.$store.state.token);
	});

	it('Computed authData should return correctly from the store', () => {
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('Computed detail should return correctly from the store', () => {
		expect(wrapper.vm.detail).toEqual(wrapper.vm.$store.state.detail);
	});

	it('Computed questions should return correctly from the store', () => {
		expect(wrapper.vm.questions).toEqual(
			wrapper.vm.$store.state.detail.questions
		);
	});

	it('Computed seq should return correctly from the store', () => {
		expect(wrapper.vm.seq).toBe(wrapper.vm.$store.state.detail._id);
	});

	it('Computed roomTitle should return correctly from the store', () => {
		expect(wrapper.vm.roomTitle).toBe(
			wrapper.vm.$store.state.detail.room_title
		);
	});

	it('Computed isPromoted should return correctly from the store', () => {
		expect(wrapper.vm.isPromoted).toBe(
			wrapper.vm.$store.state.detail.is_promoted
		);
	});

	it('Computed isPremiumOwner should return correctly from the store', () => {
		expect(wrapper.vm.isPremiumOwner).toBe(
			wrapper.vm.$store.state.detail.is_premium_owner
		);
	});

	it('Computed enableChat should return correctly from the store', () => {
		expect(wrapper.vm.enableChat).toBe(
			wrapper.vm.$store.state.detail.enable_chat
		);
	});

	it('Computed ownerId should return correctly from the store', () => {
		expect(wrapper.vm.ownerId).toBe(wrapper.vm.$store.state.detail.owner_id);
	});

	it('Computed ownerChatName should return correctly from the store', () => {
		expect(wrapper.vm.ownerChatName).toBe(
			wrapper.vm.$store.state.detail.owner_chat_name
		);
	});

	it('Computed cards should return correctly from the store', () => {
		expect(wrapper.vm.cards).toEqual(wrapper.vm.$store.state.detail.cards);
	});

	it('Computed chatCallPostUrl should return correctly according to the type of property', async () => {
		expect(wrapper.vm.chatCallPostUrl).toBe('/stories/62041738/call');

		await wrapper.setData({ detailType: 'villa' });

		expect(wrapper.vm.chatCallPostUrl).toBe(
			'/stories/house_property/62041738/call'
		);
	});

	it('Computed chatReplyPostUrl should return correctly based on property type and promoted status', async () => {
		expect(wrapper.vm.chatReplyPostUrl).toBe(
			'/stories/house_property/62041738/chat/reply'
		);

		wrapper.setData({ detailType: 'kost' });
		await wrapper.vm.$nextTick();

		wrapper.vm.$store.commit('setDetail', {
			is_promoted: true,
			pay_for: 'chat'
		});

		expect(wrapper.vm.chatReplyPostUrl).toBe(
			'/stories/62041738/chat/reply?from_ads=true'
		);

		await wrapper.vm.$store.commit('setDetail', {
			is_promoted: false,
			pay_for: null
		});

		expect(wrapper.vm.chatReplyPostUrl).toBe('/stories/62041738/chat/reply');
	});

	it('Computed chatUtm should return correctly from the store', () => {
		expect(wrapper.vm.chatUtm).toBe('');
	});

	it('Computed priceCardInfo should return correctly from the store', () => {
		expect(wrapper.vm.priceCardInfo).toEqual(
			wrapper.vm.$store.state.priceCardInfo
		);
	});

	it('Method closeKeyModal should handle escape key correctly when user press it', () => {
		const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal');

		wrapper.vm.closeKeyModal({ key: 'Escape' });

		expect(closeModalSpy).toBeCalledTimes(1);

		wrapper.vm.closeKeyModal({ key: 'Enter' });

		expect(closeModalSpy).toBeCalledTimes(1);
	});

	it('Method assignSelectedQuestion should handle question correctly based on set that passed on the parameter', () => {
		const testCase = [
			{
				set: null,
				outputSelectionQuestion: {
					button: '',
					id: 16,
					question: 'Saya butuh cepat nih. Bisa booking sekarang?'
				},
				outputSelectionQuestionId: 16
			},
			{
				set: {
					question: 'Tanya alamat',
					id: 1
				},
				outputSelectionQuestion: { question: 'Tanya alamat', id: 1 },
				outputSelectionQuestionId: 1
			}
		];

		for (const test of testCase) {
			wrapper.vm.assignSelectedQuestion(test.set);

			expect(wrapper.vm.selectedQuestion).toEqual(test.outputSelectionQuestion);
			expect(wrapper.vm.selectedQuestionId).toEqual(
				test.outputSelectionQuestionId
			);
		}
	});

	it('Watch selectedQuestionId should set selectedQuestion when the id is available on the questions array', async () => {
		await wrapper.setData({
			selectedQuestionId: 16
		});

		expect(wrapper.vm.selectedQuestion).toEqual({
			button: '',
			id: 16,
			question: 'Saya butuh cepat nih. Bisa booking sekarang?'
		});
	});

	it('Method sendChat should called correctly with additional source parameter that handle the modal status', async () => {
		const getGroupChatByChannelUrlSpy = jest.spyOn(
			wrapper.vm,
			'getGroupChatByChannelUrl'
		);
		const getGroupChatByNameSpy = jest.spyOn(wrapper.vm, 'getGroupChatByName');

		const testCase = [
			{
				getGroupChatByChannelUrl: true,
				getGroupChatByName: false,
				error: false,
				status: true,
				type: 'kost',
				source: 'modal'
			},
			{
				getGroupChatByChannelUrl: false,
				getGroupChatByName: true,
				error: false,
				status: true,
				type: 'apartment',
				source: null
			},
			{
				getGroupChatByChannelUrl: false,
				getGroupChatByName: false,
				error: false,
				status: false,
				type: 'kost',
				source: null
			},
			{
				getGroupChatByChannelUrl: false,
				getGroupChatByName: false,
				error: true,
				status: true,
				type: 'kost',
				source: null
			}
		];

		for (const test of testCase) {
			await wrapper.vm.$store.commit('setDetail', {
				type: test.type
			});

			wrapper.vm.sendChat(test.source);

			expect(openSwalLoadingSpy).toBeCalled();

			await wrapper.vm.$nextTick();

			test.getGroupChatByChannelUrl &&
				expect(getGroupChatByChannelUrlSpy).toBeCalledWith('channel_url');

			test.getGroupChatByName &&
				expect(getGroupChatByNameSpy).toBeCalledWith(
					'Umriya Afini : Kost Rumah Kamang Residence Ragunan Pasar Minggu Jakarta Selatan'
				);

			const isCallSwallError = !test.status || test.error;

			isCallSwallError && expect(swalErrorSpy).toBeCalled();
		}
	});

	it('Method getGroupChatByChannelUrl should handled correctly based on sendbird widget data', async () => {
		const testCase = [
			{
				sb: {
					GroupChannel: {
						createMyGroupChannelListQuery: jest.fn(() => {
							return {
								includeEmpty: false,
								channelUrlsFilter: [],
								next: jest.fn(mockFunction =>
									mockFunction(
										[
											{
												sendUserMessage: jest.fn(() => {
													return {};
												})
											}
										],
										false
									)
								)
							};
						})
					},
					UserMessageParams: jest.fn()
				},
				callSendGroupChat: true,
				callSwallError: false
			},
			{
				sb: {
					GroupChannel: {
						createMyGroupChannelListQuery: jest.fn(() => {
							return {
								includeEmpty: false,
								channelUrlsFilter: [],
								next: jest.fn(mockFunction => mockFunction([], false))
							};
						})
					},
					UserMessageParams: jest.fn()
				},
				callSendGroupChat: false,
				callSwallError: true
			},
			{
				sb: {
					GroupChannel: {
						createMyGroupChannelListQuery: jest.fn(() => {
							return {
								includeEmpty: false,
								channelUrlsFilter: [],
								next: jest.fn(mockFunction => mockFunction([], true))
							};
						})
					},
					UserMessageParams: jest.fn()
				},
				callSendGroupChat: false,
				callSwallError: false
			}
		];

		for (const test of testCase) {
			global.sb = test.sb;

			wrapper = generateWrapper();

			const sendChatToGroupChannelSpy = jest.spyOn(
				wrapper.vm,
				'sendChatToGroupChannel'
			);

			wrapper.vm.getGroupChatByChannelUrl('channel_url');

			await wrapper.vm.$nextTick();

			test.callSendGroupChat && expect(sendChatToGroupChannelSpy).toBeCalled();
			test.callSwallError && expect(swalErrorSpy).toBeCalled();
		}
	});

	it('Method getGroupChatByName should handled correctly based on sendbird widget data', async () => {
		const generateGroupChannel = (id, isError) => {
			return {
				createMyGroupChannelListQuery: jest.fn(() => {
					return {
						includeEmpty: false,
						channelNameContainsFilter: '',
						next: jest.fn(mockFunction =>
							mockFunction(
								[
									{
										members: [
											{
												userId: id
											}
										],
										name: 'group_1',
										sendUserMessage: jest.fn(() => {
											return {};
										})
									}
								],
								isError
							)
						)
					};
				})
			};
		};

		const testCase = [
			{
				sb: {
					GroupChannel: {
						...generateGroupChannel('31481', false),
						createChannelWithUserIds: jest.fn()
					},
					UserMessageParams: jest.fn()
				},
				error: false,
				callCreateChatGroupChannel: true,
				callSendChatToGroupChannel: false
			},
			{
				sb: {
					GroupChannel: {
						...generateGroupChannel('31480', false),
						createChannelWithUserIds: jest.fn()
					},
					UserMessageParams: jest.fn()
				},
				error: false,
				callCreateChatGroupChannel: false,
				callSendChatToGroupChannel: true
			},
			{
				sb: {
					GroupChannel: {
						...generateGroupChannel('31480', false),
						createChannelWithUserIds: jest.fn()
					},
					UserMessageParams: jest.fn()
				},
				error: false,
				includeAdmin: true,
				callCreateChatGroupChannel: false,
				callSendChatToGroupChannel: true
			},
			{
				sb: {
					GroupChannel: {
						...generateGroupChannel('31480', true),
						createChannelWithUserIds: jest.fn()
					},
					UserMessageParams: jest.fn()
				},
				error: true
			}
		];

		for (const test of testCase) {
			global.sb = test.sb;

			wrapper = generateWrapper();

			const checkGroupChannelExistanceSpy = jest.spyOn(
				wrapper.vm,
				'checkGroupChannelExistance'
			);
			const createChatGroupChannelSpy = jest.spyOn(
				wrapper.vm,
				'createChatGroupChannel'
			);
			const sendChatToGroupChannelSpy = jest.spyOn(
				wrapper.vm,
				'sendChatToGroupChannel'
			);

			if (test.includeAdmin) {
				wrapper.setData({ adminList: [31480] });
			}

			wrapper.vm.getGroupChatByName('group_1');

			await wrapper.vm.$nextTick();

			if (!test.error) {
				expect(checkGroupChannelExistanceSpy).toBeCalled();
				test.callCreateChatGroupChannel &&
					expect(createChatGroupChannelSpy).toBeCalled();
				test.callSendChatToGroupChannel &&
					expect(sendChatToGroupChannelSpy).toBeCalled();
			}
		}
	});

	it('Method createChatGroupChannel should handled correctly based on sendbird widget data', async () => {
		const generateGroupChannel = (createChannelError, sendMessageError) => {
			return {
				createChannelWithUserIds: jest.fn(
					(user, status, group, image, params, mockFunction) =>
						mockFunction(
							{
								sendUserMessage: jest.fn((params, mockSendMessage) =>
									mockSendMessage('message', sendMessageError)
								),
								updateMetaData: jest.fn()
							},
							createChannelError
						)
				)
			};
		};

		const testCase = [
			{
				sb: {
					GroupChannel: generateGroupChannel(true, false),
					UserMessageParams: jest.fn()
				},
				errorCreateChannel: true,
				errorSendMessage: false
			},
			{
				sb: {
					GroupChannel: generateGroupChannel(false, true),
					UserMessageParams: jest.fn()
				},
				errorCreateChannel: true,
				errorSendMessage: true
			},
			{
				sb: {
					GroupChannel: generateGroupChannel(false, false),
					UserMessageParams: jest.fn()
				},
				errorCreateChannel: false,
				errorSendMessage: false,
				enableChat: true,
				questionId: 1,
				sbWidget: {
					showChannel: jest.fn()
				}
			},
			{
				sb: {
					GroupChannel: generateGroupChannel(false, false),
					UserMessageParams: jest.fn()
				},
				errorCreateChannel: false,
				errorSendMessage: false,
				enableChat: true,
				questionId: 0,
				sbWidget: {
					showChannel: jest.fn()
				}
			}
		];

		for (const test of testCase) {
			global.sb = test.sb;
			global.sbWidget = test.sbWidget;

			wrapper = generateWrapper();

			const insertChannelMetadataSpy = jest.spyOn(
				wrapper.vm,
				'insertChannelMetadata'
			);
			const sendAutoChatReplySpy = jest.spyOn(wrapper.vm, 'sendAutoChatReply');

			wrapper.vm.$store.commit('setDetail', { enable_chat: test.enableChat });
			wrapper.setData({
				selectedQuestion: {
					...wrapper.vm.selectedQuestion,
					id: test.questionId
				}
			});
			wrapper.vm.createChatGroupChannel();

			await wrapper.vm.$nextTick();

			if (!test.errorSendMessage && !test.errorCreateChannel) {
				expect(insertChannelMetadataSpy).toBeCalled();
			}

			!!test.questionId && expect(sendAutoChatReplySpy).toBeCalled();
			!test.questionId && expect(closeSwalLoadingSpy).toBeCalled();
		}
	});

	it('Method sendChatToGroupChannel should handled correctly based on sendbird widget data', async () => {
		const testCase = [
			{
				sendUserMessage: jest.fn((params, mockFunction) =>
					mockFunction('message', false)
				),
				id: 1,
				callSendAutoChatReply: true
			},
			{
				sendUserMessage: jest.fn((params, mockFunction) =>
					mockFunction('message', false)
				),
				id: 0,
				callCloseSwalLoading: true
			},
			{
				sendUserMessage: jest.fn((params, mockFunction) =>
					mockFunction('message', true)
				),
				id: 1
			}
		];

		const insertChannelMetadataSpy = jest.spyOn(
			wrapper.vm,
			'insertChannelMetadata'
		);
		const sendAutoChatReplySpy = jest.spyOn(wrapper.vm, 'sendAutoChatReply');

		for (const test of testCase) {
			await wrapper.setData({
				groupChannel: {
					sendUserMessage: test.sendUserMessage
				},
				selectedQuestion: {
					id: test.id
				}
			});

			wrapper.vm.sendChatToGroupChannel();

			expect(insertChannelMetadataSpy).toBeCalled();

			test.callSendAutoChatReply && expect(sendAutoChatReplySpy).toBeCalled();
			test.callCloseSwalLoading && expect(closeSwalLoadingSpy).toBeCalled();
		}
	});

	it('Method insertChannelMetadata should handled correctly based on propertyType widget data', async () => {
		const testCase = ['kost', 'apartment', 'house'];

		const updateMetaDataSpy = jest.fn();

		for (const test of testCase) {
			await wrapper.setData({
				detailType: test,
				groupChannel: {
					updateMetaData: updateMetaDataSpy
				}
			});

			wrapper.vm.insertChannelMetadata();
		}

		await wrapper.vm.$nextTick();

		expect(updateMetaDataSpy).toBeCalledTimes(3);
	});
});
