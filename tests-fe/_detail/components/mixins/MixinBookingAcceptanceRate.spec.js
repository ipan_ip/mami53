import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import MixinBookingAcceptanceRate from 'Js/_detail/mixins/MixinBookingAcceptanceRate.js';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);

const MockComponent = {
	template: '<div />',
	mixins: [MixinBookingAcceptanceRate],
	render: h => h('div')
};

describe('MixinBookingAcceptanceRate.js', () => {
	it('should return null in bookingAcceptanceRate when detail booking is null', () => {
		const wrapper = shallowMount(MockComponent, {
			localVue,
			mocks: {
				$store: {
					state: {
						detail: {
							booking: null
						}
					}
				}
			}
		});

		expect(wrapper.vm.bookingAcceptanceRate).toBeNull();

		wrapper.destroy();
	});

	it('should return bookingAcceptanceRate properly when only have chatResponseTime', () => {
		const testCase = [
			{
				chatResponseTime: 0,
				label: '± 1 jam'
			},
			{
				chatResponseTime: 12,
				label: '± 12 jam'
			},
			{
				chatResponseTime: 28,
				label: '± 24 jam'
			}
		];

		for (const test of testCase) {
			const wrapper = shallowMount(MockComponent, {
				localVue,
				mocks: {
					$store: {
						state: {
							detail: {
								booking: {
									active_from: '15-12-2020',
									average_time: null,
									accepted_rate: null
								},
								reply_average_time: test.chatResponseTime
							}
						}
					}
				}
			});

			expect(wrapper.vm.bookingAcceptanceRate.ownerRates).not.toBeNull();
			expect(wrapper.vm.bookingAcceptanceRate.ownerRates[0].label).toBe(
				test.label
			);
		}
	});

	it('should return bookingAcceptanceRate properly when only have averageTime', () => {
		const testCase = [
			{
				averageTime: 0,
				label: '± 1 jam'
			},
			{
				averageTime: 12,
				label: '± 12 jam'
			},
			{
				averageTime: 28,
				label: '± 1 hari'
			}
		];

		for (const test of testCase) {
			const wrapper = shallowMount(MockComponent, {
				localVue,
				mocks: {
					$store: {
						state: {
							detail: {
								booking: {
									active_from: '15-12-2020',
									average_time: test.averageTime,
									accepted_rate: null
								},
								reply_average_time: null
							}
						}
					}
				}
			});

			expect(wrapper.vm.bookingAcceptanceRate.ownerRates).not.toBeNull();
			expect(wrapper.vm.bookingAcceptanceRate.ownerRates[0].label).toBe(
				test.label
			);
		}
	});

	it('should return bookingAcceptanceRate properly when only have acceptanceRate', () => {
		const wrapper = shallowMount(MockComponent, {
			localVue,
			mocks: {
				$store: {
					state: {
						detail: {
							booking: {
								active_from: '15-12-2020',
								average_time: null,
								accepted_rate: 80
							},
							reply_average_time: null
						}
					}
				}
			}
		});

		expect(wrapper.vm.bookingAcceptanceRate.ownerRates).not.toBeNull();
		expect(wrapper.vm.bookingAcceptanceRate.ownerRates[0].label).toBe('80%');
	});
});
