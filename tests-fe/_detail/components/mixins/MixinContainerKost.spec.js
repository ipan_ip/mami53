import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import MixinContainerKost from 'Js/_detail/mixins/MixinContainerKost.js';
import MixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import detailStore from '../__mocks__/detailStore';
import Vuex from 'vuex';
import EventBus from 'Js/_detail/event-bus';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

jest.mock('Js/@utils/makeAPICall.js');

const swalErrorSpy = jest.fn();
const openSwalLoadingSpy = jest.fn();
const closeSwalLoadingSpy = jest.fn();

const Component = {
	mixins: [MixinContainerKost, MixinGetQueryString],
	render() {},
	data() {
		return {
			navigator: {
				isMobile: false
			},
			isListenEventBus: false
		};
	},
	methods: {
		swalSuccess(name, statement, mockFunc) {
			mockFunc();
		},
		swalError: swalErrorSpy,
		openSwalLoading: openSwalLoadingSpy,
		closeSwalLoading: closeSwalLoadingSpy
	}
};

const localVue = createLocalVue();
localVue.use(Vuex);

let store = new Vuex.Store(detailStore);

global.superKostId = 4;
global.okeKostId = 3;
global.axios = {
	post: jest.fn(path => {
		if (path === '/owner/claim' || path === '/user/checkin') {
			return new Promise(resolve =>
				resolve({
					data: {
						status: true
					}
				})
			);
		}
	})
};
global.bugsnagClient = {
	notify: jest.fn()
};
global.tracker = jest.fn();
global.document.querySelector = jest.fn().mockReturnValue({
	childElementCount: 1
});
global.document.querySelectorAll = jest.fn().mockReturnValue([
	{
		click: jest.fn()
	}
]);
global.window.open = jest.fn();

describe('MixinContainerKost.js', () => {
	const generateWrapper = () => {
		return shallowMount(Component, {
			localVue,
			store,
			mocks: {
				$router: {
					push: jest.fn()
				}
			}
		});
	};

	let wrapper = generateWrapper();

	it('Computed token should return correctly from the store', () => {
		expect(wrapper.vm.token).toBe(wrapper.vm.$store.state.token);
	});

	it('Computed authData should return correctly from the store', () => {
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('Computed authCheck should return correctly from the store', () => {
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('Computed detail should return correctly from the store', () => {
		expect(wrapper.vm.detail).toEqual(wrapper.vm.$store.state.detail);
	});

	it('Computed detailTypeName should return correctly from the store', () => {
		expect(wrapper.vm.detailTypeName).toBe(
			wrapper.vm.$store.state.detailTypeName
		);
	});

	it('Computed breadcrumbs should return correctly from the store', async () => {
		expect(wrapper.vm.breadcrumbs).toEqual(
			wrapper.vm.$store.state.detail.breadcrumbs
		);

		await wrapper.vm.$store.commit('setDetail', {
			breadcrumbs: undefined
		});

		expect(wrapper.vm.breadcrumbs).toEqual([]);
	});

	it('Computed gender should return correctly from the store', async () => {
		expect(wrapper.vm.gender).toBe(wrapper.vm.$store.state.detail.gender);

		await wrapper.vm.$store.commit('setDetail', {
			gender: undefined
		});

		expect(wrapper.vm.gender).toBe(0);
	});

	it('Computed seq should return correctly from the store', async () => {
		expect(wrapper.vm.seq).toBe(wrapper.vm.$store.state.detail._id);

		await wrapper.vm.$store.commit('setDetail', {
			seq: undefined
		});

		expect(wrapper.vm.seq).toBe(null);

		await wrapper.vm.$store.commit('setDetail', {
			seq: 62041738
		});
	});

	it('Computed location should return correctly from the store', async () => {
		expect(wrapper.vm.location).toBe(wrapper.vm.$store.state.detail.location);

		await wrapper.vm.$store.commit('setDetail', {
			location: undefined
		});

		expect(wrapper.vm.location).toEqual([null, null]);

		await wrapper.vm.$store.commit('setDetail', {
			location: [106.816102, -6.3042925]
		});
	});

	it('Computed isPremiumOwner should return correctly from the store', () => {
		expect(wrapper.vm.isPremiumOwner).toBe(
			wrapper.vm.$store.state.detail.is_premium_owner
		);
	});

	it('Computed latitude longitude should return correctly from the store', async () => {
		expect(wrapper.vm.latitude).toBe(wrapper.vm.$store.state.detail.latitude);
		expect(wrapper.vm.longitude).toBe(wrapper.vm.$store.state.detail.longitude);

		await wrapper.vm.$store.commit('setDetail', {
			latitude: undefined,
			longitude: undefined
		});

		expect(wrapper.vm.latitude).toBe(null);
		expect(wrapper.vm.longitude).toBe(null);

		await wrapper.vm.$store.commit('setDetail', {
			latitude: -6.304981,
			longitude: 106.816885
		});
	});

	it('Computed urllanding should return correctly from the store', async () => {
		expect(wrapper.vm.urllanding).toBe(
			wrapper.vm.$store.state.detail.urllanding
		);

		await wrapper.vm.$store.commit('setDetail', {
			urllanding: undefined
		});

		expect(wrapper.vm.urllanding).toBe('/');
	});

	it('Computed alreadyBooked should return correctly from the store', async () => {
		expect(wrapper.vm.alreadyBooked).toBe(
			wrapper.vm.$store.state.detail.already_booked
		);

		await wrapper.vm.$store.commit('setDetail', {
			already_booked: undefined
		});

		expect(wrapper.vm.alreadyBooked).toBe(false);
	});

	it('Computed checker should return correctly from the store', async () => {
		expect(wrapper.vm.checker).toBe(wrapper.vm.$store.state.detail.checker);

		await wrapper.vm.$store.commit('setDetail', {
			checker: undefined
		});

		expect(wrapper.vm.checker).toBe(null);

		await wrapper.vm.$store.commit('setDetail', {
			checker: false
		});
	});

	it('Computed isSuperKost should return false because the level info is different with the superKostId', () => {
		expect(wrapper.vm.isSuperKost).toBe(false);
	});

	it('Computed isOkeKost should return false because the level info is different with the okeKostId', () => {
		expect(wrapper.vm.isOkeKost).toBe(false);
	});

	it('Computed isMamirooms should return correctly from the store', () => {
		expect(wrapper.vm.isMamirooms).toBe(
			wrapper.vm.$store.state.detail.is_mamirooms
		);
	});

	it('Computed isVerified should return correctly from the store', () => {
		expect(wrapper.vm.isVerified).toBe(wrapper.vm.$store.state.detail.checker);
	});

	it('Computed deviceType should return correctly based on the navigator status', () => {
		expect(wrapper.vm.deviceType).toBe('desktop');

		wrapper.setData({
			navigator: {
				isMobile: true
			}
		});

		expect(wrapper.vm.deviceType).toBe('mobile');
	});

	it('Computed kostLevelInfoName should return correctly from the store', () => {
		expect(wrapper.vm.kostLevelInfoName).toBe(
			wrapper.vm.$store.state.detail.level_info.name
		);
	});

	it('Computed priceCardInfo should return correctly from the store', () => {
		expect(wrapper.vm.priceCardInfo).toBe(
			wrapper.vm.$store.state.priceCardInfo
		);
	});

	it('Computed priceDaily, priceWeekly, priceMonthly, priceQuarterly, priceSemiannually, priceYearly should return correctly from the priceCardInfo data and handle the availability of it', async () => {
		const priceData = {
			price_daily: {
				currency_symbol: 'Rp',
				price: '400.000',
				rent_type_unit: 'hari'
			},
			price_weekly: {
				currency_symbol: 'Rp',
				price: '1.800.000',
				rent_type_unit: 'minggu'
			},
			price_monthly: {
				currency_symbol: 'Rp',
				price: '4.200.000',
				rent_type_unit: 'bulan'
			},
			price_yearly: {
				currency_symbol: 'Rp',
				price: '32.200.000',
				rent_type_unit: 'tahun'
			},
			price_quarterly: {
				currency_symbol: 'Rp',
				price: '8.200.000',
				rent_type_unit: 'quarter'
			},
			price_semiannually: {
				currency_symbol: 'Rp',
				price: '16.200.000',
				rent_type_unit: 'semi'
			}
		};

		const priceVariable = [
			{
				tag: 'priceDaily',
				variable: 'price_daily'
			},
			{
				tag: 'priceWeekly',
				variable: 'price_weekly'
			},
			{
				tag: 'priceMonthly',
				variable: 'price_monthly'
			},
			{
				tag: 'priceQuarterly',
				variable: 'price_quarterly'
			},
			{
				tag: 'priceSemiannually',
				variable: 'price_semiannually'
			},
			{
				tag: 'priceYearly',
				variable: 'price_yearly'
			}
		];

		await wrapper.vm.$store.commit('setPriceCardInfoPrice', priceData);

		for (const price of priceVariable) {
			expect(wrapper.vm[price.tag]).toBe(
				wrapper.vm.priceCardInfo.price[price.variable].price
			);
		}

		await wrapper.vm.$store.commit('setPriceCardInfoPrice', {
			price_daily: {
				discount_price: '400.000'
			},
			price_weekly: {
				discount_price: '1.800.000'
			},
			price_monthly: {
				discount_price: '4.200.000'
			},
			price_yearly: {
				discount_price: '32.200.000'
			},
			price_quarterly: {
				discount_price: '8.200.000'
			},
			price_semiannually: {
				discount_price: '16.200.000'
			}
		});

		for (const price of priceVariable) {
			expect(wrapper.vm[price.tag]).toBe(
				wrapper.vm.priceCardInfo.price[price.variable].discount_price
			);
		}

		await wrapper.vm.$store.commit('setPriceCardInfoPrice', {
			price_daily: null,
			price_weekly: null,
			price_monthly: null,
			price_yearly: null,
			price_quarterly: null,
			price_semiannually: null
		});

		for (const price of priceVariable) {
			expect(wrapper.vm[price.tag]).toBe('0');
		}

		await wrapper.vm.$store.commit('setPriceCardInfoPrice', priceData);
	});

	it('Computed isMoneyBackGuarantee should filtered correctly from the store if have "Garansi Uang Kembali"', async () => {
		expect(wrapper.vm.isMoneyBackGuarantee).toBe(false);

		await wrapper.vm.$store.commit('setDetail', {
			fac_room: [
				...wrapper.vm.$store.state.detail.fac_room,
				'Garansi Uang Kembali'
			]
		});

		expect(wrapper.vm.isMoneyBackGuarantee).toBe(true);
	});

	it('Computed activeBadges should return correctly based on the property type and the availability of kost data', async () => {
		expect(wrapper.vm.activeBadges).toEqual(['moneyBackGuarantee']);

		await wrapper.vm.$store.commit('setDetailTypeName', 'apartment');

		expect(wrapper.vm.activeBadges).toEqual([]);

		await wrapper.vm.$store.commit('setDetailTypeName', 'kost');
	});

	it('Method listenEventBus should be handled correctly', async () => {
		const openModalLoginSpy = jest.spyOn(wrapper.vm, 'openModalLogin');
		const openModalCheckinSpy = jest.spyOn(wrapper.vm, 'openModalCheckin');
		const sendClaimKostSpy = jest.spyOn(wrapper.vm, 'sendClaimKost');
		const sendCheckinKostSpy = jest.spyOn(wrapper.vm, 'sendCheckinKost');
		const openBookingKostSpy = jest.spyOn(wrapper.vm, 'openBookingKost');
		const openModalMobilePriceSpy = jest.spyOn(
			wrapper.vm,
			'openModalMobilePrice'
		);
		const closeModalMobilePriceSpy = jest.spyOn(
			wrapper.vm,
			'closeModalMobilePrice'
		);
		const trackBookingClickSpy = jest.spyOn(wrapper.vm, 'trackBookingClick');
		const checkOpenedChatSectionSpy = jest.spyOn(
			wrapper.vm,
			'checkOpenedChatSection'
		);

		wrapper.vm.listenEventBus();

		EventBus.$on('openModalPromo', jest.fn());
		EventBus.$on('openModalLogin', jest.fn());
		EventBus.$on('openModalClaim', jest.fn());
		EventBus.$on('openModalCheckin', jest.fn());
		EventBus.$on('sendClaimKost', jest.fn());
		EventBus.$on('sendCheckinKost', jest.fn());
		EventBus.$on('openBookingKost', jest.fn());
		EventBus.$on('trackClickBooking', jest.fn());
		EventBus.$on('validateOpenBookingKost', jest.fn());
		EventBus.$on('setPriceInfo', jest.fn());
		EventBus.$on('showPriceEstimationModal', jest.fn());
		EventBus.$on('openModalChat', jest.fn());

		await EventBus.$emit('openModalPromo');
		await EventBus.$emit('openModalLogin');
		await EventBus.$emit('openModalClaim');
		await EventBus.$emit('openModalCheckin');
		await EventBus.$emit('sendClaimKost');
		await EventBus.$emit('sendCheckinKost');
		await EventBus.$emit('openBookingKost', {
			isAvailable: true,
			redirectionSource: '',
			isSendTracker: true
		});
		await EventBus.$emit('openModalMobilePrice');
		await EventBus.$emit('closeModalMobilePrice');
		await EventBus.$emit('trackClickBooking');
		await EventBus.$emit('validateOpenBookingKost', {
			isAvailable: true
		});
		await EventBus.$emit('setPriceInfo', {
			show: false,
			info: '',
			items: []
		});
		await EventBus.$emit('showPriceEstimationModal', true);

		expect(wrapper.vm.isModalPromo).toBe(true);
		expect(openModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalClaim).toBe(true);
		expect(openModalCheckinSpy).toBeCalled();
		expect(sendClaimKostSpy).toBeCalled();
		expect(sendCheckinKostSpy).toBeCalled();
		expect(openBookingKostSpy).toBeCalled();
		expect(openModalMobilePriceSpy).toBeCalled();
		expect(closeModalMobilePriceSpy).toBeCalled();
		expect(trackBookingClickSpy).toBeCalled();
		expect(wrapper.vm.isShowPriceEstimationModal).toBe(true);

		// openModalChat handler
		const openModalChatTestCase = [
			{
				authCheck: {
					all: false,
					user: false,
					owner: false,
					admin: false
				},
				callOpenModalLogin: true
			},
			{
				authCheck: {
					all: true,
					user: true,
					owner: false,
					admin: false
				},
				callCheckOpenedChatSection: true,
				haveRefDetailmodalChat: true,
				payload: {
					question: 'Tanya Alamat?',
					id: 1
				}
			},
			{
				authCheck: {
					all: true,
					user: true,
					owner: false,
					admin: false
				},
				callCheckOpenedChatSection: true,
				haveRefDetailmodalChat: true,
				payload: 'undefined'
			},
			{
				authCheck: {
					all: true,
					user: true,
					owner: false,
					admin: false
				},
				callCheckOpenedChatSection: true,
				haveRefDetailmodalChat: false,
				payload: {
					question: 'Tanya Alamat?',
					id: 1
				}
			},
			{
				authCheck: {
					all: true,
					user: false,
					owner: true,
					admin: false
				},
				callSwallError: true
			}
		];

		for (const test of openModalChatTestCase) {
			await wrapper.vm.$store.commit('setAuthCheck', test.authCheck);

			await EventBus.$emit('openModalChat', test.payload);

			test.callOpenModalLogin && expect(openModalLoginSpy).toBeCalled();
			test.callCheckOpenedChatSection &&
				expect(checkOpenedChatSectionSpy).toBeCalled();

			if (test.haveRefDetailmodalChat) {
				wrapper.vm.$refs.detailModalChat = {
					showModal: false,
					assignSelectedQuestion: jest.fn()
				};
			} else {
				wrapper.vm.$refs.detailModalChat = null;
			}

			test.callSwallError && expect(swalErrorSpy).toBeCalled();
		}

		// openModalChat handler
		const sendPretextChatTestCase = [
			{
				authCheck: {
					all: false,
					user: false,
					owner: false,
					admin: false
				},
				callOpenModalLogin: true
			},
			{
				authCheck: {
					all: true,
					user: true,
					owner: false,
					admin: false
				},
				response: {
					question: 'Tanya Alamat?',
					id: 1
				}
			},
			{
				authCheck: {
					all: true,
					user: true,
					owner: false,
					admin: false
				},
				response: undefined
			},
			{
				authCheck: {
					all: true,
					user: false,
					owner: true,
					admin: false
				},
				callSwallError: true
			}
		];

		for (const test of sendPretextChatTestCase) {
			await wrapper.vm.$store.commit('setAuthCheck', test.authCheck);

			wrapper.vm.$refs.detailModalChat = {
				sendChat: jest.fn(),
				assignSelectedQuestion: jest.fn()
			};

			makeAPICall.mockReturnValue(test.response);

			await EventBus.$emit('sendPretextChat');

			test.callOpenModalLogin && expect(openModalLoginSpy).toBeCalled();
			test.response && expect(checkOpenedChatSectionSpy).toBeCalled();

			test.callSwallError && expect(swalErrorSpy).toBeCalled();
		}
	});

	it('Method closeModalPromo should close the modal promo', () => {
		wrapper.setData({ isModalPromo: true });

		wrapper.vm.closeModalPromo();

		expect(wrapper.vm.isModalPromo).toBe(false);
	});

	it('Method closeModalLogin should close the modal login', () => {
		wrapper.setData({ isModalLoginVisible: true, modalLoginState: 'tenant' });

		wrapper.vm.closeModalLogin();

		expect(wrapper.vm.isModalLoginVisible).toBe(false);
		expect(wrapper.vm.modalLoginState).toBe('default');
	});

	it('Method closeModalClaim should close the modal claim', () => {
		wrapper.setData({ isModalClaim: true });

		wrapper.vm.closeModalClaim();

		expect(wrapper.vm.isModalClaim).toBe(false);
	});

	it('Method closeModalBooking should close the modal booking', () => {
		wrapper.setData({ isModalBooking: true });

		wrapper.vm.closeModalBooking();

		expect(wrapper.vm.isModalBooking).toBe(false);
	});

	it('Method closeModalGenderMismatch should close the modal gender', () => {
		wrapper.setData({ isModalGenderMismatch: true });

		wrapper.vm.closeModalGenderMismatch();

		expect(wrapper.vm.isModalGenderMismatch).toBe(false);
	});

	it('Method closeModalGenderMismatch should close the modal gender mismatch', () => {
		wrapper.setData({ isModalGenderMismatch: true });

		wrapper.vm.closeModalGenderMismatch();

		expect(wrapper.vm.isModalGenderMismatch).toBe(false);
	});

	it('Method closeModalGenderNull should close the modal gender', () => {
		wrapper.setData({ isModalGenderNull: true });

		wrapper.vm.closeModalGenderNull();

		expect(wrapper.vm.isModalGenderNull).toBe(false);
	});

	it('Method closePriceInfoModal should update the priceInfoModal', () => {
		wrapper.setData({
			priceInfoModal: {
				show: true,
				info: 'info modal',
				items: ['price']
			}
		});

		wrapper.vm.closePriceInfoModal();

		expect(wrapper.vm.priceInfoModal).toEqual({
			show: false,
			info: '',
			items: []
		});
	});

	it('Method trackBookingClick should called correctly with redirection_source in url exist', () => {
		wrapper.vm.trackBookingClick({ redirectionSource: 'property detail page' });

		expect(global.tracker).toBeCalled();

		wrapper.vm.trackBookingClick({ redirectionSource: 'riwayat kos' });

		expect(global.tracker).toBeCalled();
	});

	it('Method trackBookingFormVisited should called correctly with redirectionSourceTracker based on window width', () => {
		const testCase = [
			{ innerWidth: '400', payload: 'riwayat kos' },
			{ innerWidth: '1366', payload: null }
		];

		for (const test of testCase) {
			global.innerWidth = test.innerWidth;

			wrapper = generateWrapper();

			wrapper.vm.trackBookingFormVisited(test.payload);

			expect(global.tracker).toBeCalled();
		}
	});

	it('Method goToGoldPlusLanding should called correctly and open a new window if the url is available', async () => {
		const testCase = ['https://mamikos.com/goldplus', ''];

		for (const test of testCase) {
			global.process.env.MIX_GOLDPLUS_LANDING_URL = test;

			wrapper = generateWrapper();

			await wrapper.vm.goToGoldPlusLanding();

			!!test && expect(global.window.open).toBeCalled();
		}
	});

	it('Method goToLanding should called correctly and open a new window if the url is available', async () => {
		const testCase = ['https://mamikos.com/mamirooms', ''];

		for (const test of testCase) {
			global.process.env.MIX_MAMIROOMS_LANDING_URL = test;

			wrapper = generateWrapper();

			await wrapper.vm.goToLanding({
				preventDefault: jest.fn()
			});

			!!test && expect(global.window.open).toBeCalled();
		}
	});

	it('Method getRedirectionSourceQueryParams should be handled correctly based on the referrer', async () => {
		const testCase = [
			{
				referrer: 'riwayat kos'
			},
			{
				referrer: ''
			}
		];

		for (const test of testCase) {
			Object.defineProperty(document, 'referrer', {
				value: test.referrer,
				configurable: true
			});

			wrapper = generateWrapper();

			const removeUrlHashAndProtocolSpy = jest.spyOn(
				wrapper.vm,
				'removeUrlHashAndProtocol'
			);

			await wrapper.vm.getRedirectionSourceQueryParams();

			!!test.referrer && expect(removeUrlHashAndProtocolSpy).toBeCalled();
		}
	});

	it('Method openBookingKost should be handled correctly', async () => {
		const testCase = [
			{
				isAvailable: true,
				isSendTracker: true,
				redirectionSource: 'riwayat kos',
				gender: 2
			},
			{
				isAvailable: true,
				isSendTracker: false,
				redirectionSource: '',
				gender: 1
			}
		];

		for (const test of testCase) {
			const trackBookingClickSpy = jest.spyOn(wrapper.vm, 'trackBookingClick');

			await wrapper.vm.$store.commit('setAuthCheck', {
				all: true,
				user: true,
				owner: false,
				admin: false
			});

			await wrapper.vm.$store.commit('setGender', 'male');
			await wrapper.vm.$store.commit('setDetail', {
				gender: test.gender
			});

			wrapper.vm.openBookingKost(test);

			test.isSendTracker && expect(trackBookingClickSpy).toBeCalled();
		}
	});

	it('Mounted should handled correctly when is_booking is true', async () => {
		store = new Vuex.Store({
			...detailStore,
			state: {
				...detailStore.state,
				detail: {
					...detailStore.state.detail,
					is_booking: true
				}
			}
		});

		const testCase = [
			{
				url: 'https://mamikos.com?booking=true&redirection_source=booking form',
				callOpenBookingKost: true
			},
			{
				url: 'https://mamikos.com?booking=true',
				callOpenBookingKost: true
			},
			{
				url: 'https://mamikos.com?draft=true',
				callGetRedirectionSourceQueryParams: true,
				callOpenBookingKost: true
			}
		];

		const openBookingKostSpy = jest.spyOn(
			MixinContainerKost.methods,
			'openBookingKost'
		);

		for (const test of testCase) {
			delete window.location;
			window.location = {
				href: test.url,
				assign: jest.fn(),
				replace: jest.fn(),
				reload: jest.fn()
			};

			wrapper = generateWrapper();

			test.callOpenBookingKost && expect(openBookingKostSpy).toBeCalled();
		}
	});

	it('Handle API call for method sendClaimKost and sendCheckinKost and return false', async () => {
		global.axios = {
			post: jest.fn(path => {
				if (path === '/owner/claim' || path === '/user/checkin') {
					return new Promise(resolve =>
						resolve({
							data: {
								status: false
							}
						})
					);
				}
			})
		};

		wrapper = generateWrapper();

		wrapper.vm.sendClaimKost();
		wrapper.vm.sendCheckinKost();

		expect(swalErrorSpy).toBeCalled();
	});

	it('Handle API call for method sendClaimKost and sendCheckinKost and return error', async () => {
		global.axios = {
			post: jest.fn(path => {
				if (path === '/owner/claim' || path === '/user/checkin') {
					return Promise.reject(new Error('API error'));
				}
			})
		};

		wrapper = generateWrapper();

		wrapper.vm.sendClaimKost();
		wrapper.vm.sendCheckinKost();

		expect(global.bugsnagClient.notify).toBeCalled();
	});
});
