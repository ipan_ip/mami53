import { shallowMount, createLocalVue } from '@vue/test-utils';
import MixinDetailAds from 'Js/_detail/mixins/MixinDetailAds.js';
import detailStore from '../__mocks__/detailStore';
import Vuex from 'vuex';

const Component = {
	mixins: [MixinDetailAds],
	render() {}
};

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

global.moment = jest.fn(() => {
	return {
		add: jest.fn().mockResolvedValue(30)
	};
});
global.axios = {
	post: jest.fn(() => {
		return new Promise(resolve => {
			resolve(true);
		});
	})
};
global.document.querySelector = jest.fn().mockResolvedValue({
	content: jest.fn().mockResolvedValue(30)
});
global.Cookies = {
	getJSON: jest.fn().mockReturnValue(undefined)
};
global.performance.navigation = {
	type: 1
};

describe('MixinDetailAds.js', () => {
	let wrapper = shallowMount(Component, {
		localVue,
		store
	});

	it('Computed token should return correctly from the store', () => {
		expect(wrapper.vm.token).toBe(wrapper.vm.$store.state.token);
	});

	it('Computed seq should return correctly from the store', () => {
		expect(wrapper.vm.seq).toBe(wrapper.vm.$store.state.detail._id);
	});

	it('Method handleAds should handled correctly', async () => {
		const testCase = [
			{
				cookiesValue: undefined,
				axios: global.axios
			},
			{
				cookiesValue: [
					{
						id: 62041738,
						expire: 100
					}
				],
				time: 50,
				axios: global.axios
			},
			{
				cookiesValue: [
					{
						id: 62041738,
						expire: 100
					}
				],
				time: 200,
				axios: global.axios
			},
			{
				cookiesValue: [
					{
						id: 62041732,
						expire: 100
					}
				],
				time: 200,
				axios: {
					post: jest.fn(() => {
						return Promise.reject(new Error('API error'));
					})
				}
			}
		];

		for (const test of testCase) {
			global.axios = test.axios;
			global.Cookies = {
				getJSON: jest.fn().mockReturnValue(test.cookiesValue)
			};
			global.Date = jest.fn(() => {
				return {
					getTime: jest.fn(() => test.time)
				};
			});

			wrapper = shallowMount(Component, {
				localVue,
				store
			});

			await wrapper.vm.$nextTick();

			wrapper.vm.handleAds();
		}
	});
});
