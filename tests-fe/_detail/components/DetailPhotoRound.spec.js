import DetailPhotoRound from 'Js/_detail/components/DetailPhotoRound.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailPhotoRound.vue', () => {
	const wrapper = shallowMount(DetailPhotoRound, {
		localVue,
		store
	});

	it('Computed photoRound should return correctly', () => {
		expect(wrapper.vm.photoRound).toBe(
			wrapper.vm.$store.state.detail.photo_360
		);
	});
});
