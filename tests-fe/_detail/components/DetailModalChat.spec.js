import DetailModalChat from 'Js/_detail/components/DetailModalChat.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailModalChat.vue', () => {
	const wrapper = shallowMount(DetailModalChat, {
		localVue,
		propsData: {
			priceMonthly: '1000000'
		},
		store
	});

	it('should render detail modal chat', () => {
		expect(wrapper.find('.modal-chat').exists()).toBe(true);
	});

	it('computed bookingButtonText should render correctly', () => {
		expect(wrapper.vm.bookingButtonText).toBe('Ajukan Sewa');
	});

	describe('computed: isBooking', () => {
		it('should fill isBooking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBooking).toBe(detailStore.state.detail.is_booking);
			});
		});
	});

	describe('computed: availableRoomRaw', () => {
		it('should fill isBooking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.availableRoomRaw).toBe(
					detailStore.state.detail.available_room
				);
			});
		});
	});

	describe('computed: msgTrackingSubmit', () => {
		it('should fill msgTrackingSubmit data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTrackingSubmit).toBe(
					`${detailStore.state.detail.msg_tracking}-submit`
				);
			});
		});
	});

	describe('computed: availableRoom', () => {
		it('should fill availableRoom data with data availableRoomRaw', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.availableRoom).toBe(wrapper.vm.availableRoomRaw);
			});
		});
	});

	describe('computed: bookingAvailable', () => {
		it('should true if isBooking and availableRoom more than 0', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.bookingAvailable).toBe(
					wrapper.vm.isBooking && wrapper.vm.availableRoom > 0
				);
			});
		});
	});

	describe('computed: isBookingDisabled', () => {
		it('should true if price is not available', () => {
			wrapper.setProps({
				priceDaily: '100000',
				priceMonthly: '0',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBookingDisabled).toBe(true);
			});
		});

		it('should false if price is available', () => {
			wrapper.setProps({
				priceDaily: '0',
				priceMonthly: '3000000',
				priceWeekly: '0',
				priceYearly: '0',
				priceQuarterly: '0',
				priceSemiannually: '0'
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isBookingDisabled).toBe(false);
			});
		});
	});

	describe('method: bookingKost', () => {
		it('should emit EventBus openBookingKost', () => {
			wrapper.vm.bookingKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openBookingKost', {
					isAvailable: !wrapper.vm.isBookingDisabled,
					redirectionSource: 'pretext'
				});
			});
		});
	});
});
