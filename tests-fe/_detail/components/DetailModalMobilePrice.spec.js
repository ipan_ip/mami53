import { shallowMount, createLocalVue } from '@vue/test-utils';
import priceCard from './__mocks__/priceCardInfo';
import priceCardDisc from './__mocks__/priceCardInfoDisc';
import Vuex from 'vuex';

import DetailModalMobilePrice from 'Js/_detail/components/DetailModalMobilePrice.vue';

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailModalMobilePrice.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetailModalMobilePrice, {
			localVue,
			propsData: {
				priceCardInfo: priceCard,
				priceMonthly: priceCard.price.price_monthly.price,
				priceDaily: priceCard.price.price_daily.price,
				priceWeekly: priceCard.price.price_weekly.price,
				priceQuarterly: priceCard.price.price_quarterly.price,
				priceSemiannually: priceCard.price.price_semiannually.price,
				priceYearly: priceCard.price.price_yearly.price,
				showModal: false
			}
		});
	});

	it('Should render computed discount null if data is not provide discount', () => {
		expect(wrapper.vm.discountDaily).toEqual(null);
		expect(wrapper.vm.discountWeekly).toEqual(null);
		expect(wrapper.vm.discountMonthly).toEqual(null);
		expect(wrapper.vm.discountQuarterly).toEqual(null);
		expect(wrapper.vm.discountSemianually).toEqual(null);
		expect(wrapper.vm.discountYearly).toEqual(null);
	});

	it('Should render computed correctly with discount', () => {
		const wrapper = shallowMount(DetailModalMobilePrice, {
			localVue,
			propsData: {
				priceCardInfo: priceCardDisc,
				priceMonthly: priceCardDisc.price.price_monthly.price,
				priceDaily: priceCardDisc.price.price_daily.price,
				priceWeekly: priceCardDisc.price.price_weekly.price,
				priceQuarterly: priceCardDisc.price.price_quarterly.price,
				priceSemiannually: priceCardDisc.price.price_semiannually.price,
				priceYearly: priceCardDisc.price.price_yearly.price,
				showModal: false
			}
		});

		expect(wrapper.vm.discountDaily).toEqual(
			priceCardDisc.price.price_daily.price
		);
		expect(wrapper.vm.discountWeekly).toEqual(
			priceCardDisc.price.price_weekly.price
		);
		expect(wrapper.vm.discountMonthly).toEqual(
			priceCardDisc.price.price_monthly.price
		);
		expect(wrapper.vm.discountQuarterly).toEqual(
			priceCardDisc.price.price_quarterly.price
		);
		expect(wrapper.vm.discountSemianually).toEqual(
			priceCardDisc.price.price_semiannually.price
		);
		expect(wrapper.vm.discountYearly).toEqual(
			priceCardDisc.price.price_yearly.price
		);
	});

	it('Should call closeModal on click button close', async () => {
		const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal');
		wrapper.setMethods({ closeModal: closeModalSpy });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('a.btn-close-filter').exists()).toBe(true);

		wrapper.find('a.btn-close-filter').trigger('click');
		await wrapper.vm.$nextTick();

		expect(closeModalSpy).toBeCalled();
		expect(global.EventBus.$emit).toBeCalledWith('closeModalMobilePrice');
	});
});
