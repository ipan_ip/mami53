import 'tests-fe/utils/mock-vue';
import DetailPhoto360 from 'Js/_detail/components/DetailPhoto360.vue';
import EventBus from 'Js/_detail/event-bus';
import detailStore from './__mocks__/detailStore';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const store = new Vuex.Store(detailStore);

const initViewerSpy = jest.spyOn(DetailPhoto360.methods, 'initViewer');
const pauseViewerSpy = jest.spyOn(DetailPhoto360.methods, 'pauseViewer');

global.PhotoSphereViewer = jest.fn(() => ({
	stopAutorotate: jest.fn()
}));
describe('DetailPhoto360.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin(mixinNavigator);
	const wrapper = shallowMount(DetailPhoto360, {
		localVue,
		store
	});

	it('Should call initViewer and pauseViewer method on mounted', () => {
		expect(initViewerSpy).toBeCalled();
		expect(pauseViewerSpy).toBeCalled();
	});

	it('Should call EventBus on pause', async () => {
		const stopAutoRotateSpy = jest.fn();

		EventBus.$on('pause360', stopAutoRotateSpy);
		EventBus.$emit('pause360');
		expect(stopAutoRotateSpy).toBeCalled();
	});

	it('Computed photo360 should return correctly', async () => {
		expect(wrapper.vm.photo360).toEqual({
			real:
				'https://static.mamikos.com/uploads/data/style/2019-03-04/CJU7qaTU.jpg',
			small:
				'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-360x480.jpg',
			medium:
				'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-1080x1440.jpg',
			large:
				'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-2160x1080.jpg'
		});

		wrapper.vm.$store.commit('setMediaPhoto360Null', null);

		expect(wrapper.vm.photo360).toEqual(null);
	});
});
