import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import DetailPhotoCarousel from 'Js/_detail/components/DetailPhotoCarousel';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import galleryData from './__mocks__/slideGallery.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';
window.Vue = require('vue');
const MixinDetailType = require('Js/_detail/mixins/MixinDetailType');
const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
describe('DetailPhotoCarousel.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.mixin([MixinDetailType, MixinNavigator]);
	localVue.use(Vuex);
	const store = new Vuex.Store({
		state: {
			detail: {
				type: 'Kost'
			},
			detailTypeName: 'Kost'
		}
	});
	let wrapper;

	const detailSmallCarouselCustom = {
		template:
			'<a class="update-slide" @click="updateSlide(1)">Update Slide</a>',
		props: ['updateSlide']
	};

	const stubComponent = {
		swiper,
		swiperSlide,
		'detail-small-carousel': detailSmallCarouselCustom
	};

	beforeEach(() => {
		mockWindowProperty('innerWidth', 500);
		wrapper = shallowMount(DetailPhotoCarousel, {
			localVue,
			store,
			stubs: stubComponent,
			propsData: {
				carouselGalleryPhoto: galleryData.small,
				isSingle: false,
				initialState: 'dummy-url-not-same'
			}
		});
	});

	it('Should render update swiperOptionTop correctly if prop isSingle equal to true', async () => {
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});
		wrapper = shallowMount(DetailPhotoCarousel, {
			localVue,
			store,
			stubs: stubComponent,
			propsData: {
				carouselGalleryPhoto: galleryData.solo,
				initialState: 0
			}
		});

		expect(wrapper.vm.swiperOptionTop.loop).toBe(false);
		expect(wrapper.vm.swiperOptionTop.keyboard.enabled).toBe(false);
		expect(wrapper.vm.swiperOptionTop.simulateTouch).toBe(false);

		await wrapper.vm.$nextTick();
		wrapper.destroy();

		expect(removeEvent).toHaveBeenCalled();
	});

	it('Computed isKost should return correctly', () => {
		expect(wrapper.vm.isKost).toBe(true);
	});

	it('Should call closeCarousel method on click button', async () => {
		const closeElement = wrapper.findAll('span').at(1);

		expect(closeElement.exists()).toBe(true);

		const closeCarouselSpy = jest.spyOn(wrapper.vm, 'closeCarousel');
		wrapper.setMethods({ closeCarousel: closeCarouselSpy });

		await closeElement.trigger('click');

		expect(closeCarouselSpy).toBeCalled();
		expect(wrapper.emitted('close-carousel')).toBeTruthy();
	});

	it('Should call method updateSlide correctly', async () => {
		expect(wrapper.find('a.update-slide').exists()).toBe(true);

		const updateSlideSpy = jest.spyOn(wrapper.vm, 'updateSlide');
		wrapper.setMethods({ updateSlide: updateSlideSpy });
		wrapper.find('a.update-slide').trigger('click');
		await wrapper.vm.$nextTick();

		expect(updateSlideSpy).toBeCalledWith(1);
	});

	it('Should cover updateIndex method when called by slide change', async () => {
		const detailSmallCarouselCustom = {
			template:
				'<a class="update-slide" @click="updateSlide(4)">Update Slide</a>',
			props: ['updateSlide']
		};
		wrapper = shallowMount(DetailPhotoCarousel, {
			localVue,
			store,
			stubs: {
				...stubComponent,
				'detail-small-carousel': detailSmallCarouselCustom
			},
			propsData: {
				carouselGalleryPhoto: galleryData.small,
				isSingle: false,
				initialState: 'dummy-url-not-same'
			}
		});
		expect(wrapper.find('a.update-slide').exists()).toBe(true);

		const updateSlideSpy = jest.spyOn(wrapper.vm, 'updateSlide');
		wrapper.setMethods({ updateSlide: updateSlideSpy });
		wrapper.find('a.update-slide').trigger('click');
		await wrapper.vm.$nextTick();

		expect(updateSlideSpy).toBeCalledWith(4);
	});

	it('Should call closeCarousel method on keypress esc keyboard', async () => {
		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});
		wrapper = shallowMount(DetailPhotoCarousel, {
			localVue,
			store,
			stubs: stubComponent,
			propsData: {
				carouselGalleryPhoto: galleryData.small,
				isSingle: false,
				initialState: 'dummy-url-not-same'
			}
		});
		const closeCarouselSpy = jest.spyOn(wrapper.vm, 'closeCarousel');
		wrapper.setMethods({ closeCarousel: closeCarouselSpy });
		await wrapper.vm.$nextTick();
		mockMapEventListener.keydown({ keyCode: 27 });
		await wrapper.vm.$nextTick();

		expect(closeCarouselSpy).toBeCalled();
	});

	it('Should not call closeCarousel method on keypress other than esc keyboard', async () => {
		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});
		wrapper = shallowMount(DetailPhotoCarousel, {
			localVue,
			store,
			stubs: stubComponent,
			propsData: {
				carouselGalleryPhoto: galleryData.small,
				isSingle: false,
				initialState: 'dummy-url-not-same'
			}
		});
		const closeCarouselSpy = jest.spyOn(wrapper.vm, 'closeCarousel');
		wrapper.setMethods({ closeCarousel: closeCarouselSpy });
		await wrapper.vm.$nextTick();
		mockMapEventListener.keydown({ keyCode: 24 });
		await wrapper.vm.$nextTick();

		expect(closeCarouselSpy).not.toBeCalled();
	});
});
