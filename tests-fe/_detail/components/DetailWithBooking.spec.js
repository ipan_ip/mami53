import 'tests-fe/utils/mock-vue';
import DetailWithBooking from 'Js/_detail/components/DetailWithBooking.vue';
import { shallowMount } from '@vue/test-utils';

global.detail = {
	is_booking: false
};

describe('DetailWithBooking.vue', () => {
	const wrapper = shallowMount(DetailWithBooking, {
		mocks: {
			$route: {
				meta: {
					booking_request: false
				}
			}
		}
	});

	it('Computed isBookingRequestProcess should return correctly', () => {
		expect(wrapper.vm.is_booking).toBe(global.detail.is_booking);
		expect(wrapper.vm.isBookingRequestProcess).toBe(false);
	});
});
