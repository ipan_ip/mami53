import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailVideo from 'Js/_detail/components/DetailVideo.vue';
import detailStore from './__mocks__/detailStore';
import mockComponent from '../../utils/mock-component';

import EventBus from 'Js/_detail/event-bus';
window.Vue.config.silent = true;

const videoToggler = jest.fn();
EventBus.$on('videoTour', videoToggler);
EventBus.$on('pauseVideo', videoToggler);

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailVideo.vue', () => {
	const store = new Vuex.Store(detailStore);
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetailVideo, {
			localVue,
			store,
			stubs: {
				youtubeStub: mockComponent
			}
		});
	});

	it('Should call EventBus on when assigning video tour id', async () => {
		wrapper.vm.assignVideoTourId();
		await EventBus.$emit('videoTour');

		expect(videoToggler).toBeCalled();
	});

	it('Should call EventBus on when pausing video', async () => {
		wrapper.vm.pauseYoutubeVideo();
		await EventBus.$emit('pauseVideo');

		expect(videoToggler).toBeCalled();
	});
});
