import LastUpdateText from 'Js/_detail/components/LastUpdateText.vue';

import { shallowMount } from '@vue/test-utils';

describe('LastUpdateText.vue', () => {
	const wrapper = shallowMount(LastUpdateText, {
		mocks: {
			$dayjs: jest.fn(() => ({
				locale: jest.fn(() => ({
					format: jest.fn().mockReturnValue('20 May 2020')
				}))
			}))
		},
		propsData: { updatedDate: '20 May 2020' }
	});

	it('Computed updatedDateFormatted should return correctly', () => {
		expect(wrapper.vm.updatedDateFormatted).toBe('20 May 2020');
	});
});
