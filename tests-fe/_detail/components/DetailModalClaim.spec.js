import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import DetailModalClaim from 'Js/_detail/components/DetailModalClaim.vue';

import detailStore from './__mocks__/detailStore';

window.Vue = require('vue');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailModalClaim.vue', () => {
	const wrapper = shallowMount(DetailModalClaim, {
		localVue,
		propsData: {
			showModal: true
		},
		store
	});

	it('should render modal claim', () => {
		expect(wrapper.find('#modalClaim').exists()).toBe(true);
	});

	it('should close modal when click close icon', () => {
		const closeModalFn = jest.spyOn(wrapper.vm, 'closeModal');
		wrapper.setMethods({ closeModal: closeModalFn });

		wrapper.vm.$nextTick(() => {
			wrapper.find('.claim-close span').trigger('click');
			expect(closeModalFn).toHaveBeenCalled();
			closeModalFn.mockRestore();
		});
	});

	describe('computed: roomTitle', () => {
		it('should fill roomTitle data with room_title', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.roomTitle).toBe(detailStore.state.detail.room_title);
			});
		});
	});

	describe('computed: subdistrict', () => {
		it('should fill subdistrict data with area_subdistrict', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.subdistrict).toBe(
					detailStore.state.detail.area_subdistrict
				);
			});
		});
	});

	describe('computed: city', () => {
		it('should fill city data with area_city', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.city).toBe(detailStore.state.detail.area_city);
			});
		});
	});

	describe('method: closeModal', () => {
		it('should emit close', () => {
			const { closeModal } = wrapper.vm;

			closeModal();
			expect(wrapper.emitted('close')).toBeTruthy();
		});
	});

	describe('method: sendClaimKost', () => {
		it('should emit EventBus sendClaimKost', () => {
			wrapper.vm.sendClaimKost();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith(
					'sendClaimKost',
					wrapper.vm.claimer
				);
			});
		});
	});
});
