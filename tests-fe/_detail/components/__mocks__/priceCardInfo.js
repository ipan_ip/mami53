export default {
	available_room: 4,
	updated_at: '3 June 2019 &#8226; 11:26',
	price: {
		price_daily: {
			currency_symbol: 'Rp',
			price: '400.000',
			rent_type_unit: 'hari'
		},
		price_weekly: {
			currency_symbol: 'Rp',
			price: '1.800.000',
			rent_type_unit: 'minggu'
		},
		price_monthly: {
			currency_symbol: 'Rp',
			price: '4.200.000',
			rent_type_unit: 'bulan'
		},
		price_yearly: {
			currency_symbol: 'Rp',
			price: '40.200.000',
			rent_type_unit: 'bulan'
		},
		price_quarterly: {
			currency_symbol: 'Rp',
			price: '13.000.000',
			rent_type_unit: 'bulan'
		},
		price_semiannually: {
			currency_symbol: 'Rp',
			price: '21.000.000',
			rent_type_unit: 'bulan'
		}
	},
	fac_price_icon: [
		{
			id: 84,
			name: 'Sudah termasuk listrik',
			photo_url: 'https://static.mamikos.com/uploads/tags/P3pcGw8I.png',
			small_photo_url: 'https://static.mamikos.com/uploads/tags/DeJim679.png'
		}
	],
	fac_keyword: [null],
	min_month: null,
	promotion: {
		title: 'Diskon 500rb utk bulan pertama',
		content: 'Hanya 4,2jt untuk 1 bulan pertama.\r\nKAMAR TERBATAS\r\n',
		from: '13 Apr 2019',
		to: '30 Apr 2020'
	}
};
