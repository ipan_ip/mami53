export default {
	state: {
		authCheck: { all: true, user: true, owner: false, admin: false },
		authData: {
			all: {
				id: 1936828,
				name: 'Umriya Afini',
				email: 'umriya@mamiteam.com',
				address: null,
				is_verify: '1',
				job: null,
				work_place: null,
				position: null,
				semester: null,
				province: null,
				city: null,
				subdistrict: null,
				education: null,
				birthday: '2020-03-04 00:00:00',
				description: null,
				introduction: null,
				nickname: null,
				gender: null,
				age: 22,
				token: 'OHNO',
				phone_number: null,
				fb_location: null,
				fb_education: null,
				phone_country_code: null,
				last_login: null,
				mamipay_last_login: null,
				photo_id: null,
				role: 'user',
				heart_account: 10,
				___follow_count: 0,
				___follower_count: 0,
				created_at: '2020-03-04 06:40:01',
				updated_at: '2020-03-04 06:40:01',
				deleted_at: null,
				last_open_notif: null,
				is_owner: 'false',
				is_top_owner: 'false',
				date_owner_limit: null,
				register_from: 'desktop',
				marital_status: null,
				phone_number_additional: null,
				agree_on_tc: 'false',
				hostility: 0,
				chat_access_token: '',
				photo_user: '',
				consultant: null
			}
		},
		bookingBenefit: {
			isShowModal: false,
			isModalCloseable: false,
			steps: [
				{
					title: 'Mudah ',
					content: 'Mudah dan cepat menemukan kos yang cocok',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/BlcmMS33.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-540x720.jpg'
					},
					is_cover: false,
					redirect: null
				},
				{
					title: 'Langsung ',
					content: 'Bisa pesan langsung tanpa takut keduluan',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/lyLpZf7h.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-540x720.jpg'
					},
					is_cover: false,
					redirect: null
				},
				{
					title: 'Positif ',
					content: 'Tanggapan positif dari pengguna',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/6GUTTLEw.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-540x720.jpg'
					},
					is_cover: false,
					redirect: {
						label: 'Pelajari lebih lanjut',
						url: 'https://mamiteam.shortcm.li/benefit'
					}
				}
			],
			cover: {
				title: 'Keuntungan',
				content: 'Keuntungan booking di Mamikos',
				image: {
					real:
						'https://static.mamikos.com/uploads/data/slide/2020-04-06/81JRvLxX.png',
					small:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-240x320.jpg',
					medium:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-360x480.jpg',
					large:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-540x720.jpg'
				},
				is_cover: true,
				redirect: null
			},
			highlightedTarget: null,
			redirect: {
				label: 'Pelajari lebih lanjut',
				url: 'https://mamiteam.shortcm.li/benefit'
			}
		},
		detail: {
			_id: 62425237,
			seq: 62425237,
			apartment_project_id: 782,
			youtube_id: null,
			unit_code: 'ASP03-003',
			breadcrumbs: [
				{ url: 'http://jambu.mamikos.com', name: 'Home' },
				{
					url: 'http://jambu.mamikos.com/daftar/apartemen-di-solo-surakarta',
					name: 'Sewa Apartemen di Solo Surakarta Murah'
				},
				{
					url:
						'https://mamikos.com/apartemen/jawa-tengah/apartemen-solo-paragon',
					name: 'Apartemen Solo Paragon'
				},
				{
					url:
						'https://jambu.mamikos.com/unit/apartemen-solo-paragon/apartemen-solo-paragon-tipe-studio-fully-furnished-lt-8-banjarsari-surakarta-1room-studio',
					name:
						'Apartemen Solo Paragon Tipe Studio Fully Furnished LT 8 Banjarsari Surakarta'
				}
			],
			room_title:
				'Apartemen Solo Paragon Tipe Studio Fully Furnished LT 8 Banjarsari Surakarta',
			'room-title':
				'Apartemen Solo Paragon Tipe Studio Fully Furnished LT 8 Banjarsari Surakarta',
			location_id: 97745787,
			location: [110.808893, -7.5616769999999995],
			latitude: -7.563,
			longitude: 110.81,
			address: ' ',
			description:
				'Solo Paragon Apartment @apkp.apt — Tipe Studio di lantai 8 Paragon Hotel\r\n✅Facilities:\r\nDouble bed, AC, TV LED, hot/cold shower, free water, living room, kitchen bar & set, swimming pool, playground, mall\r\n✅T&C:\r\nCheck in time @ 14.00\r\nCheck out time @ 12.00\r\nDeposit & identity may required',
			remarks: '',
			min_month: null,
			love_by_me: false,
			love_count: 0,
			share_url:
				'https://jambu.mamikos.com/unit/apartemen-solo-paragon/apartemen-solo-paragon-tipe-studio-fully-furnished-lt-8-banjarsari-surakarta-1room-studio',
			name_slug:
				'Apartemen Solo Paragon Tipe Studio Fully Furnished LT 8 Banjarsari Surakarta 1Room Studio',
			slug:
				'apartemen-solo-paragon-tipe-studio-fully-furnished-lt-8-banjarsari-surakarta-1room-studio',
			expired_phone: false,
			available_room: 1,
			is_promoted: true,
			room_count: '1',
			label: null,
			label_array: null,
			cards: [
				{
					id: 894897,
					as_cover: true,
					photo_id: 1632807,
					description: 'Bagian Depan Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-05-06/hncVpeO4.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-05-06/hncVpeO4.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-540x720.jpg'
					},
					file_name: 'hncVpeO4.jpg',
					size: null
				},
				{
					id: 894898,
					as_cover: false,
					photo_id: 1632808,
					description: 'Kamar Mandi',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-05-06/n1PIRzqy.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-05-06/n1PIRzqy.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/n1PIRzqy-540x720.jpg'
					},
					file_name: 'n1PIRzqy.jpg',
					size: null
				}
			],
			status_survey: false,
			admin_id: '12042',
			area_city: 'Surakarta',
			area_subdistrict: 'Banjarsari',
			index_status: 'index, follow',
			is_indexed: 1,
			size: '30',
			photo_round_url: null,
			photo_url: {
				real:
					'https://static.mamikos.com/uploads/data/style/2019-05-06/hncVpeO4.jpg',
				small:
					'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-240x320.jpg',
				medium:
					'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-360x480.jpg',
				large:
					'https://static.mamikos.com/uploads/cache/data/style/2019-05-06/hncVpeO4-540x720.jpg'
			},
			available_rating: true,
			review_count: 0,
			reviews: [],
			owner_id: '1903015',
			is_premium_owner: false,
			owner_chat_name: '',
			owner_phone_array: [],
			owned_by_me: false,
			is_booking: false,
			booking_type: [],
			price_remark: '',
			price_daily_time: '349 rb/hr',
			price_weekly_time: '2.2 jt/mg',
			price_monthly_time: '4.5 jt/bl',
			price_yearly_time: '45 jt/th',
			price_daily: 349000,
			price_monthly: 4500000,
			price_yearly: 45000000,
			price_weekly: 2200000,
			price_title: 4500000,
			price_components: [],
			price_shown: ['monthly'],
			fac_room: ['Access card', 'Sofa', 'Kulkas', 'Dispenser', 'Meja makan'],
			fac_share: ['Dapur'],
			fac_bath: [],
			fac_near: [],
			fac_park: [],
			fac_keyword: [null],
			fac_room_other: '',
			fac_bath_other: '',
			fac_share_other: '',
			fac_near_other: '',
			fac_room_icon: [
				{
					id: 87,
					name: 'Access card',
					photo_url: 'https://static.mamikos.com/uploads/tags/AORm8R1w.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/GJHwsDy5.png'
				},
				{
					id: 80,
					name: 'Sofa',
					photo_url: 'https://static.mamikos.com/uploads/tags/PO9hWSUO.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/fCbF8qwg.png'
				},
				{
					id: 19,
					name: 'Kulkas',
					photo_url: 'https://static.mamikos.com/uploads/tags/5DsV6zN6.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/wAQzEMGt.png'
				},
				{
					id: 18,
					name: 'Dispenser',
					photo_url: 'https://static.mamikos.com/uploads/tags/1e0dLxOi.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/7zGWyDKn.png'
				},
				{
					id: 149,
					name: 'Meja makan',
					photo_url: 'https://static.mamikos.com/uploads/tags/3OrK1rfR.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/CXgpqBYo.png'
				}
			],
			fac_share_icon: [
				{
					id: 27,
					name: 'Dapur',
					photo_url: 'https://static.mamikos.com/uploads/tags/fJmTFCND.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/TEJJ3gIi.png'
				}
			],
			fac_bath_icon: [],
			fac_near_icon: [],
			fac_park_icon: [],
			promotion: {
				title: 'PROMO HARIAN 319RB/MALAM',
				content: 'Periode inap 27 Mei - 3 Juni 2019',
				from: '27 May 2019',
				to: '03 Jun 2020'
			},
			pay_for: 'click',
			phone_manager: null,
			phone_owner: null,
			phone: '',
			verification_status: {
				is_verified_address: true,
				is_verified_phone: true,
				is_visited_kost: false,
				is_verified_kost: true,
				is_verified_by_mamikos: false
			},
			updated_at: '9 June 2019 &#8226; 15:50',
			has_photo_round: false,
			photo_360: null,
			map_guide: '',
			unit_properties: {
				condition: 'Furnished',
				unit_type: '1-Room Studio',
				floor: '8',
				size: '30 m²'
			},
			unit_properties_object: [
				{ name: 'condition', label: 'Kondisi', value: 'Furnished', icon: '' },
				{
					name: 'unit_type',
					label: 'Tipe Unit',
					value: '1-Room Studio',
					icon:
						'http://jambu.mamikos.com/general/img/icons/ic_apartment_building.png'
				},
				{
					name: 'floor',
					label: 'Lantai',
					value: '8',
					icon: 'http://jambu.mamikos.com/general/img/icons/ic_villa_floor.png'
				},
				{
					name: 'size',
					label: 'Luas',
					value: '30 m²',
					icon:
						'http://jambu.mamikos.com/general/img/icons/ic_villa_building.png'
				}
			],
			project: {
				name: 'Apartemen Solo Paragon',
				address: 'Jl. Dr. Soetomo, Surakarta, Jawa Tengah, Indonesia 57125',
				photos: [
					{
						id: 1416,
						photo_id: 490970,
						description: '',
						photo_url: {
							real:
								'https://static.mamikos.com/uploads/data/style/2017-11-23/ygdxTmZV.jpg',
							small:
								'https://static.mamikos.com/uploads/cache/data/style/2017-11-23/ygdxTmZV-240x320.jpg',
							medium:
								'https://static.mamikos.com/uploads/cache/data/style/2017-11-23/ygdxTmZV-360x480.jpg',
							large:
								'https://static.mamikos.com/uploads/cache/data/style/2017-11-23/ygdxTmZV-540x720.jpg'
						}
					}
				],
				description:
					'<div style="text-align: justify;"><h2><b>Sewa Apartemen Solo Paragon Murah</b></h2><div>Apartemen Solo Paragon Murah – Apartemen kini tidak hanya dibangun di kota-kota besar saja melainkan hampir di seluruh kota di Indonesia yang memiliki setidaknya satu apartemen. Seperti di Kota Solo ini misalnya. Kota Solo yang berada di Provinsi Jawa Tengah pun juga tidak mau kalah dengan kota besar lain seperti Ibukota Jakarta. Anda bisa menemukan apartemen yang juga memberikan kenyamanan lebih di kota ini. Apartemen ini biasanya menjadi pilihan penginapan bagi para pendatang maupun wisatawan yang sedang berlibur di kota ini.</div><div><br></div><div>Jika Anda termasuk salah satu wisatawan maupun pendatang yang sedang bingung mencari penginapan yang tepat, Apartemen Solo Paragon ini bisa menjadi solusinya. Apartemen yang dibangun dengan berbagai keunggulan ini tentunya akan memberikan Anda kenyamanan sebuah hunian yang tidak kalah dengan hotel berbintang. Selain itu apartemen yang berada di kawasan strategis Kota Solo ini juga akan memudahkan mobilitas Anda terutama akses menuju ke tempat-tempat penting.</div><div><br></div><h2><b>Informasi Seputar Apartemen Solo Paragon</b></h2><div>Apartemen Solo Paragon terletak dijalan Yosodipuro No. 133, Mangkubumen, Surakarta. &nbsp;Apartemen ini dibangun oleh PT Surindo Gapura Prima yang telah selesai pada tahun 2010 silam. Apartemen ini merupakan apartemen tertinggi pertama yang dibangun di Kota Solo. Apartemen Solo Paragon merupakan campuran antara hotel dengan apartemen yang berada di pusat bisnis dan perbelanjaan di kota Solo, Jawa Tengah.</div><div><br></div><div>Lokasi ini sangat strategis karena berada di pusat komplek terpadu Solo Paragon. Di dalam komplek ini, tersedia mall dan Carrefour Transmart untuk memenuhi kebutuhan sehari-hari. Jika Anda sering bepergian dengan menggunakan pesawat, apartemen ini terletak 11 KM dari Bandar Udara Internasional Adi Sumarmo yang bisa Anda tempuh dengan kendaraan selama 29 menit melalui jalan Adi Sucipto. Selain itu, ada kabar gembira untuk Anda yang ingin membeli apartemen ini. Pemerintah sedang mengerjakan proyek pembangunan jalan tol Solo-Semarang yang akan selesai dibangun pada tahun 2020 mendatang.</div><div><br></div><div>Untuk fasilitas rumah sakit, Anda bisa memilih Rumah Sakit Kasih Ibu Surakarta yang berjarak 1,3 KM dan bisa Anda tempuh menggunakan kendaraan selama 4 menit dari Apartemen Solo Paragon. Jika Anda sering bepergian menggunakan kereta api, Anda bisa langsung menuju Stasiun Solo Balapan yang berjarak 2,1 KM dan bisa ditempuh dengan kendaraan selama 6 menit. Untuk pergi ke Universitas Sebelas Maret, Anda hanya perlu menempuh jarak 5.4 KM dengan waktu tempuh menggunakan kendaraan selama 13 menit.</div><div><br></div><h2><b>Fasilitas Apartemen Solo Paragon</b></h2><div>Apartemen Solo Paragon memiliki banyak fasilitas umum yang bisa digunakan oleh penghuninya. Untuk bersantai, Anda bisa datang ke cafe untuk sekedar melepas penat. Jika Anda suka berolah raga, Anda bisa menggunakan fasilitas gymnasium, lapangan basket, serta jogging track. Seperti pada umumnya, apartemen ini juga sudah memiliki kolam renang. Jika Anda ingin relaksasi, Anda bisa datang ke spa. Jika Anda merasa lapar, Anda bisa langsung mengunjungi restoran yang tersedia di apartemen ini, jadi Anda tidak perlu keluar apartemen. Semua fasilitas di atas pastinya akan sangat memanjakan penghuninya. Untuk keamanannya, apartemen ini dilengkapi dengan penjagaan dan CCTV yang beroperasi selama 24 jam. Untuk setiap unit juga sudah dilengkapi dengan kartu akses penghuni.</div><div><br></div><h2><b>Biaya Sewa Apartemen Solo Paragon</b></h2><div>Kota Solo sama seperti kota lainnya di Provinsi Jawa Tengah, yaitu terkenal dengan biaya hidupnya yang cukup murah. Tentunya biaya hidup di kota ini sangat jauh berbeda jika dibandingkan dengan besarnya biaya hidup di ibukota. Dengan begitu biaya sewa di kota ini pun bisa dibilang telatif murah. Untuk sewa per bulannya pun masih diangka 5 juta per bulan. Harga sewa ini masih sebanding dengan fasilitas apartemen dan unit yang akan Anda dapatkan. Anda juga bisa menghemat pengeluaran transportasi karena apartemen ini dekat dengan pusat perbelanjaan dan tempat lain yang Anda butuhkan.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Harian</b></h2><div>Bagi Anda para wisatawan yang sedang merencanakan liburannya di Kota Solo ini bisa memilih Sewa Apartemen Solo Paragon Harian. Sewa apartemen ini menjadi pilihan yang paling tepat bagi Anda karena apartemen ini berjarak cukup dekat dengan beberapa tempat wisata menarik. Di samping itu, apartemen ini juga memiliki akses yang mudah sehingga Anda pun tidak akan kesulitan untuk menemukan transportasi umum untuk menuju ke lokasi wisata tersebut.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Mingguan</b></h2><div>Selain sewa apartemen harian, apartemen ini juga menyediakan sewa apartemen dalam jangka waktu bulanan. Sewa Apartemen Solo Paragon Mingguan bisa menjadi pilihan terbaik bagi Anda para pebisnis maupun pekerja kantoran yang sedang memiliki acara di kota ini. Harga sewa mingguannya pun cukup terjangkau dengan berbagai kelebihan fasilitasnya. Anda tentunya akan merasakan pengalaman menginap yang menyenangkan selama berada di Kota Solo ini.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Bulanan</b></h2><div>Bagi Anda mahasiswa semester akhir maupun pegawai yang tinggal merantau di Kota Solo ini bisa memilih apartemen sebagai alternatif pilihan hunian sementara. Sewa Apartemen Solo Paragon Bulanan ini akan menjawab semua kebutuhan Anda selama tinggal di kota ini. Kebutuhan Anda mulai dari pakaian, makanan, serta kebutuhan pokok lain bisa Anda dapatkan dengan mudah melalui fasilitas dan pusat perbelanjaan yang berada dalam satu kompleks dengan apartemen ini. Dengan harga sewa yang terjangkau, Anda tidak akan menyesal menjadikan apartemen ini sebagai hunian Anda.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Tahunan</b></h2><div>Sewa Apartemen Solo Paragon Tahunan bisa menjadi solusi terbaik bagi Anda yang telah berencana tinggal dalam jangka waktu tahunan di kota ini. Karena apartemen ini menawarkan beragam tipe unit, Anda bisa turut membawa keluarga Anda untuk tinggal di apartemen ini. Anda yang memiliki anak usia sekolah pun tidak perlu khawatir karena ada banyak sekali sekolah negeri maupun swasta yang berada dekat dengan apartemen ini.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Tipe Unit (Studio, 1 BR, 2 BR, 3 BR)&nbsp;</b></h2><div>Apartemen Solo Paragon dibangun diatas tanah seluas 41.000 m<sup>2</sup>. Terdapat satu buah tower setinggi 25 lantai yang terbagi menjadi 445 unit hunian yang siap dipasarkan. Terdapat 160 unit studio yang memiliki luas ruangan 29 m<sup>2</sup>, 130 unit 2 bedroom yang memiliki luas ruangan 58 m<sup>2</sup>, serta 30 unit 3 bedroom dengan luas ruangan 75 m<sup>2</sup>. Semua unit hunian di apartemen ini memiliki balkon yang memiliki pemandangan kota Solo dan ke kolam renang.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon Tipe Furnished / Unfurnished</b></h2><div>Sebelum memutuskan untuk memilih unit yang akan Anda sewa, sebaiknya Anda pertimbangkan terlebih dahulu secara matang. Pertimbangan ini sangat penting jika Anda tidak ingin kecewa nantinya. Beberapa pertimbangan ini misalnya saja berkaitan dengan tipe unit yang akan Anda pilih. Ada berbagai macam tipe unit apartemen seperti 1 BR, 2 BR, 3 BR yang masing-masing tersedia dalam kondisi furnished dan unfurnished. Untuk kondisi furnished ini Anda akan mendapatkan unit apartemen yang sudah siap huni lengkap dengan semua furniture pendukungnya. Sedangkan kondisi unfurnished ini masih dalam keadaan unit kosongan tanpa furniture apa pun. Jika Anda menginginkan unit yang tertata sesuai dengan selera Anda, unit unfurnished ini bisa menjadi pilihan yang tepat bagi Anda. unit unfurnished juga memiliki harga yang berbeda dengan unit furnished karena cenderung lebih murah.</div><div><br></div><h2><b>Sewa Apartemen Solo Paragon di Mamikos</b></h2><div>Apartemen Solo Paragon merupakan apartemen pertama di Kota Solo yang berada di pusat kota dan berada di kawasan perbelanjaan. Dengan berbagai fasilitas dan akses lokasi yang strategis, akan cocok sekali jika Anda ingin berinvestasi di apartemen ini. Jika Anda sedang mencari apartemen untuk dihuni atau investasi di kota Solo, Anda bisa mendapatkan informasi selengkapnya di <a href="https://mamikos.com" target="_blank">Mamikos.com</a> atau menggunakan <a href="https://mamikos.com/app" target="_blank">Aplikasi Mamikos</a> yang bisa diunduh melalui Google PlayStore atau AppStore secara gratis. Selain itu, Anda juga bisa mendapat informasi alternatif hunian lain di Solo seperti <a href="https://mamikos.com/apartemen/surakarta/solo-center-point-apartment" target="_blank">Solo Center Point Apartment</a>.&nbsp;</div></div>',
				facilities: [
					{
						id: 91,
						name: 'Basketball Court',
						photo_url: 'https://static.mamikos.com/uploads/tags/LApRkMdY.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/hc6Kwv8m.png'
					},
					{
						id: 95,
						name: 'Broadband Internet',
						photo_url: 'https://static.mamikos.com/uploads/tags/XOaoJ9JB.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/Sen9QsUh.png'
					},
					{
						id: 97,
						name: 'Cafe',
						photo_url: 'https://static.mamikos.com/uploads/tags/dQegQq8v.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/mlJuiVUm.png'
					},
					{
						id: 111,
						name: 'Gymnasium',
						photo_url: 'https://static.mamikos.com/uploads/tags/Mg0BF9JE.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/VOHvBoYI.png'
					},
					{
						id: 115,
						name: 'Jogging Track',
						photo_url: 'https://static.mamikos.com/uploads/tags/QHUZny8T.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/lNtvxCJV.png'
					},
					{
						id: 116,
						name: "Kid's Club",
						photo_url: 'https://static.mamikos.com/uploads/tags/QkbSzyAk.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/h74rxP5c.png'
					},
					{
						id: 121,
						name: 'Mall',
						photo_url: 'https://static.mamikos.com/uploads/tags/DUmAxIbA.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/DHL6JQhT.png'
					},
					{
						id: 124,
						name: 'Multi-function Room',
						photo_url: 'https://static.mamikos.com/uploads/tags/7V8zj7S8.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/V4rbHLOI.png'
					},
					{
						id: 125,
						name: 'Parking Lot',
						photo_url: 'https://static.mamikos.com/uploads/tags/DwJaaXAG.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/N97ox1Mp.png'
					},
					{
						id: 126,
						name: 'Restaurant',
						photo_url: 'https://static.mamikos.com/uploads/tags/E1yFDyQs.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/AOGnLnVv.png'
					},
					{
						id: 130,
						name: 'Security',
						photo_url: 'https://static.mamikos.com/uploads/tags/agYGgA7v.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/PWE7zo4U.png'
					},
					{
						id: 133,
						name: 'Spa',
						photo_url: 'https://static.mamikos.com/uploads/tags/bUMNWWQt.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/LATFYE9B.png'
					},
					{
						id: 134,
						name: 'Swimming Pool',
						photo_url: 'https://static.mamikos.com/uploads/tags/0UvJTUEg.png',
						small_photo_url:
							'https://static.mamikos.com/uploads/tags/H1n9IpK1.png'
					}
				]
			},
			has_video: false,
			report_types: [
				{
					type: 'photo',
					name: 'Foto tidak sesuai (depan/bangunan/kamar/kamar mandi)'
				},
				{ type: 'address', name: 'Alamat lengkap tidak sesuai' },
				{ type: 'phone', name: 'Nomor telepon tidak bisa dihubungi' },
				{ type: 'price', name: 'Harga tidak sesuai' },
				{ type: 'facility', name: 'Fasilitas tidak sesuai' },
				{ type: 'other', name: 'Laporkan lainnya' }
			],
			related: [],
			not_updated: false,
			enable_chat: true,
			allow_phonenumber_via_chat: true,
			questions: [
				{
					id: 3,
					question: 'Bagaimana bisa menghubungi apartemen ini?',
					button: ''
				}
			],
			chat_version: 1,
			msg_tracking: 'track-message-apt',
			autocomplete: true,
			type: 'apartment'
		},
		detailTypeName: 'Apartemen',
		extras: { isLimitedContent: false, disabledChat: false },
		isChatReplied: false,
		priceCardInfo: {},
		showBookingRequestProcess: false,
		token: 'ABCD'
	}
};
