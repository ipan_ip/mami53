export default {
	status: true,
	meta: {
		response_code: 200,
		code: 200,
		severity: 'OK',
		message:
			'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
	},
	page: 1,
	'next-page': 2,
	limit: 3,
	offset: 0,
	'has-more': false,
	total: 4,
	'total-str': '1',
	toast: null,
	total_page: 2,
	review: [
		{
			review_id: 14652,
			token: 'MTQ2NTIsMTkzODE0MQ==',
			user_id: 1938141,
			name: 'Karen At Rini Qa',
			is_anonim: false,
			song_id: 23423720,
			rating: '3.6',
			clean: 4,
			happy: 4,
			safe: 4,
			pricing: 1.6,
			room_facilities: 4,
			public_facilities: 4,
			content: 'aman damai nyaman',
			tanggal: '14 May 2020',
			status: 'live',
			is_me: true,
			photo: [
				{
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					}
				}
			],
			share_hashtag: 'Mamikos',
			share_word: '',
			share_url:
				'https://jambu.kerupux.com/room/kost-bantul-kost-campur-eksklusif-kost-sukandebi-carolin',
			reply_show: false,
			can_reply: false,
			reply_owner: {
				date: '2020-09-05 18:00:00',
				reply: 'replied'
			},
			reply_count: 2
		},
		{
			review_id: 14652,
			token: 'MTQ2NTIsMTkzODE0MQ==',
			user_id: 1938141,
			name: 'Karen At Rini Qa',
			is_anonim: false,
			song_id: 23423720,
			rating: '3.6',
			clean: 4,
			happy: 4,
			safe: 4,
			pricing: 1.6,
			room_facilities: 4,
			public_facilities: 4,
			content: 'aman damai nyaman',
			tanggal: '14 May 2020',
			status: 'live',
			is_me: true,
			photo: [
				{
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					}
				}
			],
			share_hashtag: 'Mamikos',
			share_word: '',
			share_url:
				'https://jambu.kerupux.com/room/kost-bantul-kost-campur-eksklusif-kost-sukandebi-carolin',
			reply_show: false,
			can_reply: false,
			reply_owner: {
				date: '2020-09-05 18:00:00',
				reply: 'replied'
			},
			reply_count: 2
		},
		{
			review_id: 14652,
			token: 'MTQ2NTIsMTkzODE0MQ==',
			user_id: 1938141,
			name: 'Karen At Rini Qa',
			is_anonim: false,
			song_id: 23423720,
			rating: '3.6',
			clean: 4,
			happy: 4,
			safe: 4,
			pricing: 1.6,
			room_facilities: 4,
			public_facilities: 4,
			content: 'aman damai nyaman',
			tanggal: '14 May 2020',
			status: 'live',
			is_me: true,
			photo: [
				{
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					}
				}
			],
			share_hashtag: 'Mamikos',
			share_word: '',
			share_url:
				'https://jambu.kerupux.com/room/kost-bantul-kost-campur-eksklusif-kost-sukandebi-carolin',
			reply_show: false,
			can_reply: false,
			reply_owner: {
				date: '2020-09-05 18:00:00',
				reply: 'replied'
			},
			reply_count: 2
		},
		{
			review_id: 14652,
			token: 'MTQ2NTIsMTkzODE0MQ==',
			user_id: 1938141,
			name: 'Karen At Rini Qa',
			is_anonim: false,
			song_id: 23423720,
			rating: '3.6',
			clean: 4,
			happy: 4,
			safe: 4,
			pricing: 1.6,
			room_facilities: 4,
			public_facilities: 4,
			content: 'aman damai nyaman',
			tanggal: '14 May 2020',
			status: 'live',
			is_me: true,
			photo: [
				{
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					}
				}
			],
			share_hashtag: 'Mamikos',
			share_word: '',
			share_url:
				'https://jambu.kerupux.com/room/kost-bantul-kost-campur-eksklusif-kost-sukandebi-carolin',
			reply_show: false,
			can_reply: false,
			reply_owner: {
				date: '2020-09-05 18:00:00',
				reply: 'replied'
			},
			reply_count: 2
		}
	],
	rating: '3.6',
	all_rating: {
		clean: 4,
		happy: 4,
		safe: 4,
		pricing: 1.6,
		room_facilities: 4,
		public_facilities: 4
	},
	owner: false
};
