export default {
	state: {
		detail: {
			cards: [
				{
					id: 686906,
					as_cover: false,
					photo_id: 1169132,
					description: 'Bagian Depan Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2018-09-04/F2j3RSvE.jpeg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2018-09-04/F2j3RSvE.jpeg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2018-09-04/F2j3RSvE-540x720.jpg'
					},
					file_name: 'F2j3RSvE.jpeg',
					size: null
				}
			]
		}
	}
};
