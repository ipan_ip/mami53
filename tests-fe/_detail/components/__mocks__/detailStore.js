export default {
	state: {
		authCheck: { all: true, user: true, owner: false, admin: false },
		isChatReplied: false,
		authData: {
			all: {
				id: 1234567,
				name: 'Umriya Afini',
				email: 'umriya@mamiteam.com',
				address: null,
				is_verify: '1',
				job: null,
				work_place: null,
				position: null,
				semester: null,
				province: null,
				city: null,
				subdistrict: null,
				education: null,
				birthday: '2020-03-04 00:00:00',
				description: null,
				introduction: null,
				nickname: null,
				gender: null,
				age: 22,
				token: 'OHNO',
				phone_number: null,
				fb_location: null,
				fb_education: null,
				phone_country_code: null,
				last_login: null,
				mamipay_last_login: null,
				photo_id: null,
				role: 'user',
				heart_account: 10,
				___follow_count: 0,
				___follower_count: 0,
				created_at: '2020-03-04 06:40:01',
				updated_at: '2020-03-04 06:40:01',
				deleted_at: null,
				last_open_notif: null,
				is_owner: 'false',
				is_top_owner: 'false',
				date_owner_limit: null,
				register_from: 'desktop',
				marital_status: null,
				phone_number_additional: null,
				agree_on_tc: 'false',
				hostility: 0,
				chat_access_token: '',
				photo_user: '',
				consultant: null
			}
		},
		bookingBenefit: {
			isShowModal: true,
			isModalCloseable: false,
			steps: [
				{
					title: 'Mudah ',
					content: 'Mudah dan cepat menemukan kos yang cocok',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/BlcmMS33.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/BlcmMS33-540x720.jpg'
					},
					is_cover: false,
					redirect: null
				},
				{
					title: 'Langsung ',
					content: 'Bisa pesan langsung tanpa takut keduluan',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/lyLpZf7h.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/lyLpZf7h-540x720.jpg'
					},
					is_cover: false,
					redirect: null
				},
				{
					title: 'Positif ',
					content: 'Tanggapan positif dari pengguna',
					image: {
						real:
							'https://static.mamikos.com/uploads/data/slide/2020-04-06/6GUTTLEw.png',
						small:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/6GUTTLEw-540x720.jpg'
					},
					is_cover: false,
					redirect: {
						label: 'Pelajari lebih lanjut',
						url: 'https://mamiteam.shortcm.li/benefit'
					}
				}
			],
			cover: {
				title: 'Keuntungan',
				content: 'Keuntungan booking di Mamikos',
				image: {
					real:
						'https://static.mamikos.com/uploads/data/slide/2020-04-06/81JRvLxX.png',
					small:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-240x320.jpg',
					medium:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-360x480.jpg',
					large:
						'https://static.mamikos.com/uploads/cache/data/slide/2020-04-06/81JRvLxX-540x720.jpg'
				},
				is_cover: true,
				redirect: null
			},
			highlightedTarget: null,
			redirect: {
				label: 'Pelajari lebih lanjut',
				url: 'https://mamiteam.shortcm.li/benefit'
			}
		},
		flashSaleData: {
			isShowModal: true,
			steps: [
				{
					title: 'Mamikos Promo Ngebut',
					showTitle: true,
					content:
						'Diskon bayar sewa Kos mulai dari Rp100.000, sampai tanggal 21 Juni 2020.',
					image: {
						large: '/general/img/pictures/flashsale/flashsale-detail-FTUE.png'
					}
				}
			],
			redirect: {
				label: 'Saya Mengerti',
				url: null
			}
		},
		priceDetailState: '',
		bookingRequestProcess: true,
		detail: {
			has_recommendation: false,
			is_flash_sale: true,
			_id: 62041738,
			seq: 62041738,
			goldplus: false,
			love_by_me: false,
			city_code: 'jaksel4943',
			youtube_id: '5I0NsyxKJvk',
			booking: {
				active_from: '15-12-2020',
				average_time: 17,
				accepted_rate: 89
			},
			name_slug:
				'Kost Jakarta Selatan Kost Campur Eksklusif Kost Rumah Kamang Residence Ragunan Pasar Minggu Jakarta Selatan',
			slug:
				'kost-jakarta-selatan-kost-campur-eksklusif-kost-rumah-kamang-residence-ragunan-pasar-minggu-jakarta-selatan-1',
			area_city_keyword: 'jakarta selatan',
			area_city: 'Jakarta Selatan',
			area_subdistrict: 'Pasar Minggu',
			gender: 0,
			status: 0,
			available_room: 4,
			index_status: 'index, follow',
			is_indexed: 1,
			room_title:
				'Kost Rumah Kamang Residence Ragunan Pasar Minggu Jakarta Selatan',
			location_id: 97307524,
			location: [106.816102, -6.3042925],
			latitude: -6.304981,
			longitude: 106.816885,
			address: ' ',
			description:
				'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Ev',
			html_description: '',
			remarks:
				'Biaya untuk pasangan atau sharing kamar: 4,3jt. Uang deposit 500rb yg refundable pada saat selesai sewa ',
			min_month: null,
			price_remark:
				'Apabila sharing kamar, biayanya 4,3jt/bln. Security deposit 500rb yg refundable pada saat selesai sewa ',
			price_keyword: null,
			urllanding:
				'http://jambu.mamikos.com/kost/kost-pasar-minggu-jakarta-murah',
			namalanding: 'Kost Pasar Minggu',
			breadcrumbs: [
				{ url: 'http://jambu.mamikos.com', name: 'Home' },
				{
					url: 'http://jambu.mamikos.com/kost/kost-jakarta-murah',
					name: 'Kost Jakarta'
				},
				{
					url: 'http://jambu.mamikos.com/kost/kost-pasar-minggu-jakarta-murah',
					name: 'Kost Pasar Minggu'
				},
				{
					url:
						'http://jambu.mamikos.com/room/kost-jakarta-selatan-kost-campur-eksklusif-kost-rumah-kamang-residence-ragunan-pasar-minggu-jakarta-selatan-1',
					name:
						'Kost Rumah Kamang Residence Ragunan Pasar Minggu Jakarta Selatan'
				}
			],
			cards: [
				{
					id: 192044,
					as_cover: false,
					photo_id: 297764,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/LW6Tzd5m.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/LW6Tzd5m-540x720.jpg'
					},
					file_name: 'LW6Tzd5m.jpg',
					size: null
				},
				{
					id: 192039,
					as_cover: false,
					photo_id: 339235,
					description: 'Bagian Depan Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-07-24/iPyi9p2j.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-07-24/iPyi9p2j.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-07-24/iPyi9p2j-540x720.jpg'
					},
					file_name: 'iPyi9p2j.jpg',
					size: null
				},
				{
					id: 192040,
					as_cover: false,
					photo_id: 297759,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/0YD18rJK.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/0YD18rJK.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/0YD18rJK-540x720.jpg'
					},
					file_name: '0YD18rJK.jpg',
					size: null
				},
				{
					id: 192041,
					as_cover: false,
					photo_id: 297761,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/dE6pPDEB.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/dE6pPDEB.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/dE6pPDEB-540x720.jpg'
					},
					file_name: 'dE6pPDEB.jpg',
					size: null
				},
				{
					id: 192042,
					as_cover: false,
					photo_id: 297762,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/JWsVjEji.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/JWsVjEji.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/JWsVjEji-540x720.jpg'
					},
					file_name: 'JWsVjEji.jpg',
					size: null
				},
				{
					id: 192043,
					as_cover: false,
					photo_id: 297763,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/hRStmcsR.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/hRStmcsR.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hRStmcsR-540x720.jpg'
					},
					file_name: 'hRStmcsR.jpg',
					size: null
				},
				{
					id: 192045,
					as_cover: true,
					photo_id: 297760,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/2zXplY9t.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/2zXplY9t.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-540x720.jpg'
					},
					file_name: '2zXplY9t.jpg',
					size: null
				},
				{
					id: 192046,
					as_cover: false,
					photo_id: 297765,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/ZvUWwORM.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/ZvUWwORM.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ZvUWwORM-540x720.jpg'
					},
					file_name: 'ZvUWwORM.jpg',
					size: null
				},
				{
					id: 192047,
					as_cover: false,
					photo_id: 297766,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/ORZ6vK6j.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/ORZ6vK6j.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/ORZ6vK6j-540x720.jpg'
					},
					file_name: 'ORZ6vK6j.jpg',
					size: null
				},
				{
					id: 192048,
					as_cover: false,
					photo_id: 297767,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/hSbwg94Z.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/hSbwg94Z.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/hSbwg94Z-540x720.jpg'
					},
					file_name: 'hSbwg94Z.jpg',
					size: null
				},
				{
					id: 192049,
					as_cover: false,
					photo_id: 297768,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/oByYdAhJ.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/oByYdAhJ.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/oByYdAhJ-540x720.jpg'
					},
					file_name: 'oByYdAhJ.jpg',
					size: null
				},
				{
					id: 192051,
					as_cover: false,
					photo_id: 297770,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/KXy57dd6.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2017-06-15/KXy57dd6.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/KXy57dd6-540x720.jpg'
					},
					file_name: 'KXy57dd6.jpg',
					size: null
				},
				{
					id: 843289,
					as_cover: false,
					photo_id: 1501892,
					description: 'Fasilitas Kos',
					type: 'image',
					photo_url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-03-04/Kmb9usHY.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-540x720.jpg'
					},
					url: {
						real:
							'https://static.mamikos.com/uploads/data/style/2019-03-04/Kmb9usHY.jpg',
						small:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-240x320.jpg',
						medium:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-360x480.jpg',
						large:
							'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/Kmb9usHY-540x720.jpg'
					},
					file_name: 'Kmb9usHY.jpg',
					size: null
				}
			],
			view_count: 18837,
			love_count: 266,
			call_count: 563,
			is_booking: false,
			is_mamirooms: false,
			already_booked: false,
			booking_type: [],
			pay_for: 'click',
			size: '3,5x5,5 ',
			photo_url: {
				real:
					'https://static.mamikos.com/uploads/data/style/2017-06-15/2zXplY9t.jpg',
				small:
					'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-240x320.jpg',
				medium:
					'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-360x480.jpg',
				large:
					'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-540x720.jpg'
			},
			price_tag: 'eksklusif',
			price_title_formats: {
				price_daily: {
					currency_symbol: 'Rp',
					price: '400.000',
					rent_type_unit: 'hari'
				},
				price_weekly: {
					currency_symbol: 'Rp',
					price: '1.800.000',
					rent_type_unit: 'minggu'
				},
				price_monthly: {
					currency_symbol: 'Rp',
					price: '4.200.000',
					rent_type_unit: 'bulan'
				},
				price_yearly: null,
				price_quarterly: null,
				price_semiannually: null
			},
			price_daily: 400000,
			price_weekly: 1800000,
			price_monthly: 4200000,
			price_quarterly: 0,
			price_semiannualy: 0,
			price_yearly: 0,
			price_title: 4200000,
			class: 0,
			class_badge: null,
			top_facilities: [
				{
					id: 1,
					name: 'Kamar mandi dalam',
					photo_url: 'https://static.mamikos.com/uploads/tags/JKp9z6Xm.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/TCWpQeCV.png'
				},
				{
					id: 2,
					name: 'Kloset duduk',
					photo_url: 'https://static.mamikos.com/uploads/tags/r5PJur0V.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/9G5IqAq7.png'
				},
				{
					id: 7,
					name: 'Wastafel',
					photo_url: 'https://static.mamikos.com/uploads/tags/rKET7SW6.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/i4XHNRf8.png'
				},
				{
					id: 8,
					name: 'Air panas',
					photo_url: 'https://static.mamikos.com/uploads/tags/C6E2y9fw.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/u9PPN0uP.png'
				},
				{
					id: 10,
					name: 'Kasur',
					photo_url: 'https://static.mamikos.com/uploads/tags/KrCtQKLC.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/1BsAad8X.png'
				},
				{
					id: 12,
					name: 'TV',
					photo_url: 'https://static.mamikos.com/uploads/tags/QLMywDFu.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/0gfL0Vc6.png'
				},
				{
					id: 13,
					name: 'AC',
					photo_url: 'https://static.mamikos.com/uploads/tags/KbyE5nig.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/33fWta7r.png'
				},
				{
					id: 14,
					name: 'Meja belajar',
					photo_url: 'https://static.mamikos.com/uploads/tags/LfsGUPIL.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/XgUYNIKV.png'
				},
				{
					id: 15,
					name: 'Wifi - Internet',
					photo_url: 'https://static.mamikos.com/uploads/tags/dJTW4I3q.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/WWNFnoxd.png'
				},
				{
					id: 79,
					name: 'TV Kabel',
					photo_url: 'https://static.mamikos.com/uploads/tags/PGOIGGGE.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/DlkwQgpV.png'
				}
			],
			facility_photos: [],
			facility_count: 0,
			fac_room: [
				'Lemari pakaian',
				'Kursi belajar',
				'Dispenser',
				'Kulkas',
				'Wastafel',
				'Bisa pasutri',
				'Cleaning service',
				'Sekamar berdua',
				'Access card'
			],
			fac_share: [
				'Ruang tamu',
				'Ruang makan',
				'Dapur',
				'Ruang santai',
				'Ruang cuci',
				'Mesin cuci',
				'Security',
				'Laundry',
				'Kulkas',
				'Dispenser',
				'TV',
				'Taman',
				'Ruang keluarga',
				'Gazebo',
				'Balcon',
				'Akses kunci 24 jam'
			],
			fac_bath: ['Shower'],
			fac_near: [
				'Warung makan / Rumah makan',
				'Mini market / Warung kelontong',
				'ATM / Bank',
				'Apotek / Klinik',
				'Kampus / Sekolah',
				'Halte bus / Pos ojek',
				'Pusat belanja / Mall',
				'Masjid'
			],
			fac_park: ['Parkir mobil', 'Parkir motor', 'Parkir sepeda'],
			fac_price: ['Sudah termasuk listrik'],
			fac_keyword: [null],
			fac_room_other:
				"Fasilitas yang kami tawarkan adalah:- Luas kamar 3,5 x 5,5 m- Balkon (Lt.2 & Lt.3)- Pulsa listrik Rp 200.000/bulan- Shower air panas & dingin didalam kamar- Spring Bed 160x200- AC 1 PK- 32' LED TV- Cable TV dgn siaran International News, HBO, Star World, FOX Premium, Cinemax, Sports (Bien 3)- Kunci kamar menggunakan Key Card- Wifi gratis & meja utk bekerja/belajar- Kulkas mini bar dalam kamar- Sekuriti CCTV 24 jam- Parkiran luas. Parkir Gratis untuk 1 unit kendaraan- Halaman yang l",
			fac_bath_other: '',
			fac_share_other: '',
			fac_near_other:
				'Akses Lokasi : - Hanya 100 m dari terminal ragunan, - 5 menit dari TB. Simatupang dan kawasan industri cilandak, Rumah Kamang Residence sangat strategis untuk semua kalangan.- 2 menit jalan kaki ke Terminal Bis & Transjakarta. - 5 menit berkendara ke Tol JORR Simatupang untuk ke airport dan Daerah Sudirman & Thamrin',
			fac_room_icon: [
				{
					id: 11,
					name: 'Lemari pakaian',
					photo_url: 'https://static.mamikos.com/uploads/tags/khi5fPCV.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/qcMdJYfK.png'
				},
				{
					id: 17,
					name: 'Kursi belajar',
					photo_url: 'https://static.mamikos.com/uploads/tags/uySfjyYR.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/Y6r81XfZ.png'
				},
				{
					id: 18,
					name: 'Dispenser',
					photo_url: 'https://static.mamikos.com/uploads/tags/1e0dLxOi.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/7zGWyDKn.png'
				},
				{
					id: 19,
					name: 'Kulkas',
					photo_url: 'https://static.mamikos.com/uploads/tags/5DsV6zN6.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/wAQzEMGt.png'
				},
				{
					id: 44,
					name: 'Wastafel',
					photo_url: 'https://static.mamikos.com/uploads/tags/dwfN12Mc.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/9fGHznfa.png'
				},
				{
					id: 60,
					name: 'Bisa pasutri',
					photo_url: 'https://static.mamikos.com/uploads/tags/ihcuf53p.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/0p9NMH89.png'
				},
				{
					id: 61,
					name: 'Cleaning service',
					photo_url: 'https://static.mamikos.com/uploads/tags/61nZBnud.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/ebuOCtlG.png'
				},
				{
					id: 76,
					name: 'Sekamar berdua',
					photo_url: 'https://static.mamikos.com/uploads/tags/h8qsnkx6.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/Efm6AVHc.png'
				},
				{
					id: 87,
					name: 'Access card',
					photo_url: 'https://static.mamikos.com/uploads/tags/AORm8R1w.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/GJHwsDy5.png'
				}
			],
			fac_share_icon: [
				{
					id: 25,
					name: 'Ruang tamu',
					photo_url: 'https://static.mamikos.com/uploads/tags/gtZbb3Ez.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/YdmgG98H.png'
				},
				{
					id: 26,
					name: 'Ruang makan',
					photo_url: 'https://static.mamikos.com/uploads/tags/9URKajJA.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/sMhkM27P.png'
				},
				{
					id: 27,
					name: 'Dapur',
					photo_url: 'https://static.mamikos.com/uploads/tags/fJmTFCND.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/TEJJ3gIi.png'
				},
				{
					id: 28,
					name: 'Ruang santai',
					photo_url: 'https://static.mamikos.com/uploads/tags/dCJtL0lv.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/ROPl92nj.png'
				},
				{
					id: 30,
					name: 'Ruang cuci',
					photo_url: 'https://static.mamikos.com/uploads/tags/HRSFtYqp.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/6XwQib8W.png'
				},
				{
					id: 31,
					name: 'Mesin cuci',
					photo_url: 'https://static.mamikos.com/uploads/tags/eLTE9pJ1.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/l9UWQXi0.png'
				},
				{
					id: 32,
					name: 'Security',
					photo_url: 'https://static.mamikos.com/uploads/tags/l1H1ZCyb.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/JHPpCAZ3.png'
				},
				{
					id: 35,
					name: 'Laundry',
					photo_url: 'https://static.mamikos.com/uploads/tags/zv0Vm47e.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/SnMi8uxm.png'
				},
				{
					id: 42,
					name: 'Kulkas',
					photo_url: 'https://static.mamikos.com/uploads/tags/ersDhWsF.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/CSeZM1Fq.png'
				},
				{
					id: 43,
					name: 'Dispenser',
					photo_url: 'https://static.mamikos.com/uploads/tags/Ym9vmt5G.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/oaYXV7WM.png'
				},
				{
					id: 45,
					name: 'TV',
					photo_url: 'https://static.mamikos.com/uploads/tags/7h0FOhqu.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/Czeq9LpD.png'
				},
				{
					id: 49,
					name: 'Taman',
					photo_url: 'https://static.mamikos.com/uploads/tags/rZjxgVQF.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/c4ubmME9.png'
				},
				{
					id: 50,
					name: 'Ruang keluarga',
					photo_url: 'https://static.mamikos.com/uploads/tags/aaaIjzyR.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/GC4aYNSt.png'
				},
				{
					id: 52,
					name: 'Gazebo',
					photo_url: 'https://static.mamikos.com/uploads/tags/4bCqx5nd.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/NAPHxB8J.png'
				},
				{
					id: 53,
					name: 'Balcon',
					photo_url: 'https://static.mamikos.com/uploads/tags/RcBZCOMT.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/Zvx9bqgS.png'
				},
				{
					id: 59,
					name: 'Akses kunci 24 jam',
					photo_url: 'https://static.mamikos.com/uploads/tags/2uIBfAtw.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/f6ygKpVw.png'
				}
			],
			fac_bath_icon: [
				{
					id: 3,
					name: 'Shower',
					photo_url: 'https://static.mamikos.com/uploads/tags/PybSBNxB.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/fY7Fjerx.png'
				}
			],
			fac_near_icon: [
				{
					id: 33,
					name: 'Warung makan / Rumah makan',
					photo_url: 'https://static.mamikos.com/uploads/tags/5HkZuW9F.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/bYMFkCbX.png'
				},
				{
					id: 34,
					name: 'Mini market / Warung kelontong',
					photo_url: 'https://static.mamikos.com/uploads/tags/zBrcLQ1B.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/SNRpf9UF.png'
				},
				{
					id: 36,
					name: 'ATM / Bank',
					photo_url: 'https://static.mamikos.com/uploads/tags/Igy80EEh.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/Utyhz509.png'
				},
				{
					id: 37,
					name: 'Apotek / Klinik',
					photo_url: 'https://static.mamikos.com/uploads/tags/JsTKoL2o.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/bd4swvK2.png'
				},
				{
					id: 38,
					name: 'Kampus / Sekolah',
					photo_url: 'https://static.mamikos.com/uploads/tags/7uPrpPn7.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/lEiEJjvl.png'
				},
				{
					id: 39,
					name: 'Halte bus / Pos ojek',
					photo_url: 'https://static.mamikos.com/uploads/tags/VHakmDIM.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/njvPv0Mj.png'
				},
				{
					id: 40,
					name: 'Pusat belanja / Mall',
					photo_url: 'https://static.mamikos.com/uploads/tags/3XRXj5eZ.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/pHk621vB.png'
				},
				{
					id: 85,
					name: 'Masjid',
					photo_url: 'https://static.mamikos.com/uploads/tags/kM3509fp.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/oA6isB16.png'
				}
			],
			fac_park_icon: [
				{
					id: 22,
					name: 'Parkir mobil',
					photo_url: 'https://static.mamikos.com/uploads/tags/4sUEbcfF.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/TgAHJIRA.png'
				},
				{
					id: 23,
					name: 'Parkir motor',
					photo_url: 'https://static.mamikos.com/uploads/tags/DE0DiIOg.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/PfKvkMPK.png'
				},
				{
					id: 24,
					name: 'Parkir sepeda',
					photo_url: 'https://static.mamikos.com/uploads/tags/J3baOkOR.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/tU9m8CkF.png'
				}
			],
			fac_price_icon: [
				{
					id: 84,
					name: 'Sudah termasuk listrik',
					photo_url: 'https://static.mamikos.com/uploads/tags/P3pcGw8I.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/DeJim679.png'
				}
			],
			owner_id: '31480',
			owner_header: 'Pemilik Top',
			owner_name: null,
			owner_gender: null,
			owner_photo_url: 'https://jambu.mamikos.com/storage/placeholder.png',
			is_premium_owner: false,
			is_top_owner: true,
			owner_chat_name: '',
			phone: '',
			expired_phone: false,
			updated_at: '3 June 2019 &#8226; 11:26',
			verification_status: {
				is_verified_address: true,
				is_verified_phone: true,
				is_visited_kost: true,
				is_verified_kost: true,
				is_verified_by_mamikos: false
			},
			has_photo_round: true,
			photo_360: {
				real:
					'https://static.mamikos.com/uploads/data/style/2019-03-04/CJU7qaTU.jpg',
				small:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-360x480.jpg',
				medium:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-1080x1440.jpg',
				large:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-2160x1080.jpg'
			},
			map_guide: '',
			not_updated: false,
			admin_id: '40003',
			is_promoted: true,
			has_video: true,
			rating: 4,
			review_count: 4,
			promotion: {
				title: 'Diskon 500rb utk bulan pertama',
				content: 'Hanya 4,2jt untuk 1 bulan pertama.\r\nKAMAR TERBATAS\r\n',
				from: '13 Apr 2019',
				to: '30 Apr 2020'
			},
			report_types: [
				{
					type: 'photo',
					name: 'Foto tidak sesuai (depan/bangunan/kamar/kamar mandi)'
				},
				{ type: 'address', name: 'Alamat lengkap tidak sesuai' },
				{ type: 'phone', name: 'Nomor telepon tidak bisa dihubungi' },
				{ type: 'price', name: 'Harga tidak sesuai' },
				{ type: 'facility', name: 'Fasilitas tidak sesuai' },
				{ type: 'other', name: 'Laporkan lainnya' }
			],
			price_title_time: '400.000 / hari',
			building_year: null,
			enable_chat: true,
			allow_phonenumber_via_chat: true,
			checker: null,
			unique_code: '',
			level_info: {
				id: 2,
				name: 'Reguler',
				is_regular: true,
				flag: null,
				flag_id: null
			},
			checkin_attempt: false,
			questions: [
				{
					id: 16,
					question: 'Saya butuh cepat nih. Bisa booking sekarang?',
					button: ''
				},
				{ id: 20, question: 'disclaimer?', button: '' },
				{ id: 21, question: 'Boleh Tahu Amaran kos', button: '' },
				{ id: 18, question: 'Ada diskon untuk kos ini?', button: '' },
				{ id: 12, question: 'Alamat kos di mana?', button: '' },
				{ id: 6, question: 'Cara menghubungi pemilik?', button: '' },
				{ id: 19, question: 'Boleh tanya-tanya dulu?', button: '' },
				{ id: 11, question: 'Masih ada kamar ?', button: '' },
				{ id: 13, question: 'Bisa pasutri ?', button: '' },
				{ id: 14, question: 'Boleh bawa hewan ?', button: '' }
			],
			chat_version: 1,
			msg_tracking: 'track-message-kost',
			autocomplete: true,
			matterport_id: 123,
			type: 'kost'
		},
		detailTypeName: 'Kost',
		extras: { isLimitedContent: false, disabledChat: false },
		priceCardInfo: {
			available_room: 4,
			updated_at: '3 June 2019 &#8226; 11:26',
			price: {
				price_daily: {
					currency_symbol: 'Rp',
					price: '400.000',
					rent_type_unit: 'hari'
				},
				price_weekly: {
					currency_symbol: 'Rp',
					price: '1.800.000',
					rent_type_unit: 'minggu'
				},
				price_monthly: {
					currency_symbol: 'Rp',
					price: '4.200.000',
					rent_type_unit: 'bulan'
				},
				price_yearly: null,
				price_quarterly: null,
				price_semiannually: null
			},
			fac_price_icon: [
				{
					id: 84,
					name: 'Sudah termasuk listrik',
					photo_url: 'https://static.mamikos.com/uploads/tags/P3pcGw8I.png',
					small_photo_url:
						'https://static.mamikos.com/uploads/tags/DeJim679.png'
				}
			],
			fac_keyword: [null],
			min_month: null,
			promotion: {
				title: 'Diskon 500rb utk bulan pertama',
				content: 'Hanya 4,2jt untuk 1 bulan pertama.\r\nKAMAR TERBATAS\r\n',
				from: '13 Apr 2019',
				to: '30 Apr 2020'
			}
		},
		params: {
			rent_count_type: ''
		},
		showBookingRequestProcess: false,
		token: 'ABCD',
		isOverrideDraftCheckin: false,
		isUserAbTest: false,
		isAbTestChecked: false
	},
	// mutation helper for test case
	mutations: {
		setUserAbTest(state, payload) {
			state.isUserAbTest = payload;
		},
		setAbTestCheckedStatus(state, payload) {
			state.isAbTestChecked = payload;
		},
		setIdKost(state, payload) {
			state.detail._id = payload;
		},
		setPriceDetailState(state, payload) {
			state.priceDetailState = payload;
		},
		setFTUEPayload(state, payload) {
			state[payload.tag] = { ...state.bookingBenefit, ...payload.data };
		},
		setNullCase(state) {
			state.detail.size = null;
			state.detail.remarks = null;
			state.detail.price_remark = null;
			state.detail.fac_near_other = null;
			state.detail.fac_room_other = null;
			state.detail.fac_near_icon = null;
			state.detail.fac_park_icon = null;
			state.detail.fac_share_icon = null;
			state.detail.fac_bath_icon = null;
			state.detail.fac_room_icon = null;
			state.detail.top_facilities = null;
		},
		setPriceCardInfoPrice(state, payload) {
			state.priceCardInfo.price = payload;
		},
		setMediaMatterportNull(state, payload) {
			state.detail.matterport_id = payload;
		},
		setMediaYoutubeNull(state, payload) {
			state.detail.youtube_id = payload;
		},
		setMediaPhoto360Null(state, payload) {
			state.detail.photo_360 = payload;
		},
		setDetailClass(state, payload) {
			state.detail.class = payload;
		},
		setDetailLove(state, payload) {
			state.detail.love_by_me = payload;
		},
		setDetail(state, payload) {
			state.detail = {
				...state.detail,
				...payload
			};
		},
		setBookingRequest(state, payload) {
			state.bookingRequestProcess = payload;
		},
		setBookingRequestProcess(state, payload) {
			state.showBookingRequestProcess = payload;
		},
		setExtras(state, payload) {
			state.extras = {
				...state.detail.extras,
				isLimitedContent: payload
			};
		},
		setIsChatReplied(state, payload) {
			state.isChatReplied = payload;
		},
		setTrackerCondition(state) {
			state.authCheck = { ...state.authCheck, all: null };
			state.detailTypeName = null;
		},
		setDetailTypeName(state, payload) {
			state.detailTypeName = payload;
		},
		setGender(state, payload) {
			state.authData.all.gender = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setLimitedContent(state, payload) {
			state.extras.isLimitedContent = payload;
		},
		setIsOverrideDraftCheckin(state, payload) {
			state.isOverrideDraftCheckin = payload;
		},
		setDetailGoldplus(state, payload) {
			state.detail.goldplus = payload;
		},
		setDetailMamirooms(state, payload) {
			state.detail.is_mamirooms = payload;
		},
		setRentCountType: jest.fn(),
		setIsOverrideDraftRentCountType: jest.fn()
	}
};
