import '@babel/polyfill';
import { shallowMount } from '@vue/test-utils';
import GenderLabel from 'Js/_detail/components/GenderLabel.vue';

describe('GenderLabel.vue', () => {
	const wrapper = shallowMount(GenderLabel, {
		propsData: {
			genderId: 0
		}
	});

	it('should render label Campur', async () => {
		wrapper.setProps({
			genderId: 0
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.activeGender).toBe(wrapper.vm.genders[0]);
		expect(wrapper.find('.--purple').exists()).toBe(true);
	});

	it('should render label Putra', async () => {
		wrapper.setProps({
			genderId: 1
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.activeGender).toBe(wrapper.vm.genders[1]);
		expect(wrapper.find('.--blue').exists()).toBe(true);
	});

	it('should render label Putri', async () => {
		wrapper.setProps({
			genderId: 2
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.activeGender).toBe(wrapper.vm.genders[2]);
		expect(wrapper.find('.--pink').exists()).toBe(true);
	});
});
