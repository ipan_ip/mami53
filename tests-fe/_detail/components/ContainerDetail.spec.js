import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import ContainerDetail from 'Js/_detail/components/ContainerDetail.vue';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from '../../utils/mock-component';

import detailStore from './__mocks__/detailStore';
window.Vue = require('vue');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
mockVLazy(localVue);
localVue.use(Vuex);

mockWindowProperty('superKostId', 123);
mockWindowProperty('okeKostId', 456);
const mockStore = detailStore;
mockStore.state.isCheckinDateFirst = true;

const store = new Vuex.Store(detailStore);

const DetailKostPriceEstimationModal = { template: '<div/>' };

const scrollToComponentSpy = jest.spyOn(
	ContainerDetail.methods,
	'scrollToComponent'
);

describe('ContainerDetail.vue', () => {
	const wrapper = shallowMount(ContainerDetail, {
		localVue,
		stubs: {
			LoginUser: mockComponent,
			BreadcrumbTrails: mockComponent,
			RelatedCard: mockComponent,
			DetailKostPriceEstimationModal
		},
		store
	});

	it('should render detail container', async () => {
		expect(wrapper.find('#detailKostContainer').exists()).toBe(true);
	});

	describe('computed: photo360', () => {
		it('should fill photo360 data with data from store', () => {
			expect(wrapper.vm.photo360).toEqual({
				real:
					'https://static.mamikos.com/uploads/data/style/2019-03-04/CJU7qaTU.jpg',
				small:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-360x480.jpg',
				medium:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-1080x1440.jpg',
				large:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-2160x1080.jpg'
			});

			wrapper.vm.$store.commit('setMediaPhoto360Null', null);

			expect(wrapper.vm.photo360).toBe(null);
		});
	});

	describe('computed: matterportId', () => {
		it('should fill matterportId data with data from store', () => {
			expect(wrapper.vm.matterportId).toEqual(123);

			wrapper.vm.$store.commit('setMediaMatterportNull', null);

			expect(wrapper.vm.matterportId).toBe(null);
		});
	});

	describe('computed: youtubeId', () => {
		it('should fill youtubeId data with data from store', () => {
			expect(wrapper.vm.youtubeId).toEqual('5I0NsyxKJvk');

			wrapper.vm.$store.commit('setMediaYoutubeNull', null);

			expect(wrapper.vm.youtubeId).toBe(null);
		});
	});

	describe('computed: hasMedia', () => {
		it('should fill hasMedia data with data from store correctly', () => {
			wrapper.vm.$store.commit('setMediaYoutubeNull', true);
			wrapper.vm.$store.commit('setMediaMatterportNull', true);
			wrapper.vm.$store.commit('setMediaPhoto360Null', true);

			expect(wrapper.vm.hasMedia).toBe(true);

			wrapper.vm.$store.commit('setMediaYoutubeNull', null);
			wrapper.vm.$store.commit('setMediaMatterportNull', null);
			wrapper.vm.$store.commit('setMediaPhoto360Null', null);

			expect(wrapper.vm.hasMedia).toBe(false);
		});
	});

	describe('computed: authData', () => {
		it('should fill authData data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authData).toBe(detailStore.state.authData);
			});
		});
	});

	describe('computed: authCheck', () => {
		it('should fill authCheck data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
			});
		});
	});

	describe('computed: isDesktopFavShare', () => {
		it('should fill isDesktopFavShare data with data from store correctly', () => {
			const testCase = [
				{
					detailType: 'kost',
					width: 460,
					authCheck: {
						user: true,
						all: true,
						owner: false
					},
					expected: false
				},
				{
					detailType: 'apartment',
					width: 1366,
					authCheck: {
						user: false,
						all: false,
						owner: false
					},
					expected: true
				}
			];

			for (const test of testCase) {
				mockWindowProperty('innerWidth', test.width);

				const wrapper = shallowMount(ContainerDetail, {
					localVue,
					stubs: {
						LoginUser: mockComponent,
						BreadcrumbTrails: mockComponent,
						RelatedCard: mockComponent
					},
					store,
					data() {
						return {
							detailType: test.detailType
						};
					}
				});

				wrapper.vm.$store.commit('setAuthCheck', test.authCheck);

				expect(wrapper.vm.isDesktopFavShare).toBe(test.expected);
			}
		});
	});

	describe('method: openModalChat', () => {
		it('should emit event bus event openModalChat', () => {
			wrapper
				.findComponent(DetailKostPriceEstimationModal)
				.vm.$emit('chat-clicked');

			expect(global.EventBus.$emit).toBeCalledWith('openModalChat');
		});
	});

	describe('method: scrollToComponent', () => {
		it('should call scrollToComponent method correctly', () => {
			mockWindowProperty('scrollTo', jest.fn());
			const testCase = [
				`<button class="scroll-component" @click="$emit('scroll-to-component', 'rating')"/>`,
				`<button class="scroll-component" @click="$emit('scroll-to-component', 'detailKostRelated')"/>`
			];

			for (const test of testCase) {
				const wrapper = shallowMount(ContainerDetail, {
					localVue,
					stubs: {
						DetailKostOverview: test,
						DetailKostPriceEstimationModal
					},
					store
				});

				const scrollElement = wrapper.find('button.scroll-component');

				expect(scrollElement.exists()).toBe(true);

				scrollElement.trigger('click');

				expect(scrollToComponentSpy).toBeCalled();
			}
		});
	});

	describe('method: handleEstimatePriceBookingClicked', () => {
		it('should handle booking if refs is exists', async () => {
			const setBookingRedirectionSource = jest.fn();
			const showBookingInputCheckinModal = jest.fn();
			wrapper.vm.$refs = {
				detailPriceCardMobile: {
					setBookingRedirectionSource,
					showBookingInputCheckinModal
				}
			};
			wrapper
				.findComponent(DetailKostPriceEstimationModal)
				.vm.$emit('booking-clicked');
			await wrapper.vm.$nextTick();

			expect(setBookingRedirectionSource).toBeCalled();
			expect(showBookingInputCheckinModal).toBeCalled();
		});

		it('should not handle booking if refs is not exists', () => {
			const setBookingRedirectionSource = jest.fn();
			const showBookingInputCheckinModal = jest.fn();

			wrapper.vm.$refs = {};
			wrapper
				.findComponent(
					DetailKostPriceEstimationModal,
					showBookingInputCheckinModal
				)
				.vm.$emit('booking-clicked');

			expect(setBookingRedirectionSource).not.toBeCalled();
			expect(showBookingInputCheckinModal).not.toBeCalled();
		});
	});
});
