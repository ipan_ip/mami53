import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import DetailFacilities from 'Js/_detail/components/DetailFacilities';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import DetailStore from './__mocks__/detailStore';
import Vuex from 'vuex';

window.Vue = require('vue');

describe('DetailFacilities.vue', () => {
	const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.mixin([MixinNavigator]);
	localVue.use(Vuex);
	const store = new Vuex.Store(DetailStore);
	global.tracker = jest.fn();
	let wrapper;

	const loginUserStub = {
		template: `<a class="close-cta" @click="$emit('closeModal')">close</a>`
	};
	const BgButton = {
		template: `<button class="btn-fac-more" @click="$emit('click')"/>`
	};
	beforeEach(() => {
		wrapper = shallowMount(DetailFacilities, {
			localVue,
			store,
			data() {
				return {
					detailType: 'kost'
				};
			},
			stubs: {
				'modal-form-login': loginUserStub,
				BgButton
			}
		});
	});

	it('Should render computed authCheck and detail from the store', () => {
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
		expect(wrapper.vm.detail).toEqual(wrapper.vm.$store.state.detail);
	});

	it('Should render computed kostClass from the store', () => {
		expect(wrapper.vm.kostClass).toEqual(wrapper.vm.$store.state.detail.class);
	});

	it('Should render computed with else value if the store value is null or no provided', async () => {
		wrapper.vm.$store.commit('setNullCase');
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.roomSize).toEqual('');
		expect(wrapper.vm.kostInfoOther).toEqual('');
		expect(wrapper.vm.priceInfoOther).toEqual('');
		expect(wrapper.vm.topFacilities).toEqual([]);
		expect(wrapper.vm.roomFacilities).toEqual([]);
		expect(wrapper.vm.bathFacilities).toEqual([]);
		expect(wrapper.vm.sharedFacilities).toEqual([]);
		expect(wrapper.vm.parkFacilities).toEqual([]);
		expect(wrapper.vm.areaFacilities).toEqual([]);
		expect(wrapper.vm.roomFacilitiesOther).toEqual([]);
		expect(wrapper.vm.areaFacilitiesOther).toEqual([]);
	});

	it('Should render computed kostClassString correctly according to store data', async () => {
		expect(wrapper.vm.kostClassString).toEqual('yang bisa kamu dapatkan');

		wrapper.vm.$store.commit('setDetailClass', 1);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Basic');

		wrapper.vm.$store.commit('setDetailClass', 11);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Basic Plus');

		wrapper.vm.$store.commit('setDetailClass', 2);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Exclusive');

		wrapper.vm.$store.commit('setDetailClass', 21);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Exclusive Plus');

		wrapper.vm.$store.commit('setDetailClass', 22);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Exclusive Max');

		wrapper.vm.$store.commit('setDetailClass', 3);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Residence');

		wrapper.vm.$store.commit('setDetailClass', 31);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Residence Plus');

		wrapper.vm.$store.commit('setDetailClass', 32);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.kostClassString).toEqual('Kelas Residence Max');
	});

	it('Should not change isAllfacilities if detailType is kost', () => {
		expect(wrapper.vm.isAllfacilities).toBe(false);
	});

	it('Should change isAllfacilities if detailType is not kost', () => {
		wrapper = shallowMount(DetailFacilities, {
			localVue,
			store,
			data() {
				return {
					detailType: 'apartement'
				};
			},
			stubs: {
				'login-user': loginUserStub
			}
		});

		expect(wrapper.vm.isAllfacilities).toBe(true);
	});

	it('Should call showAllFacilities method on button click with null condition on is_login and property_type', async () => {
		wrapper.vm.$store.commit('setTrackerCondition');
		const showAllFacilitiesSpy = jest.spyOn(wrapper.vm, 'showAllFacilities');
		wrapper.setMethods({ showAllFacilities: showAllFacilitiesSpy });

		expect(wrapper.find('button.btn-fac-more').exists()).toBe(true);

		wrapper.find('button.btn-fac-more').trigger('click');
		await wrapper.vm.$nextTick();

		expect(showAllFacilitiesSpy).toBeCalled();
	});

	it('Should call openModalLogin method on button click if isLimitedContent true and call closeModalLogin on close modal', async () => {
		wrapper.vm.$store.commit('setExtras', true);
		const openModalLoginSpy = jest.spyOn(wrapper.vm, 'openModalLogin');
		const closeModalLoginSpy = jest.spyOn(wrapper.vm, 'closeModalLogin');
		wrapper.setMethods({
			openModalLogin: openModalLoginSpy,
			closeModalLogin: closeModalLoginSpy
		});

		expect(wrapper.find('button.btn-fac-more').exists()).toBe(true);

		wrapper.find('button.btn-fac-more').trigger('click');
		await wrapper.vm.$nextTick();

		expect(openModalLoginSpy).toBeCalled();
		expect(wrapper.find('a.close-cta').exists()).toBe(true);

		wrapper.find('a.close-cta').trigger('click');
		await wrapper.vm.$nextTick();

		expect(closeModalLoginSpy).toBeCalled();
	});
});
