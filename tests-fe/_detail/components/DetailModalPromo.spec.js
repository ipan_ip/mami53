import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailModalPromo from 'Js/_detail/components/DetailModalPromo.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';

window.Vue = require('vue');
const mixinDetailType = require('Js/_detail/mixins/MixinDetailType');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(mixinDetailType);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);
const promoContent = {
	content: 'Bebas biaya laundry untuk 2 bulan pertama',
	from: '1 April 2020',
	title: 'Free laundry 2 bulan',
	to: '30 Agustus 2020'
};

describe('DetailModalPromo.vue', () => {
	const wrapper = shallowMount(DetailModalPromo, {
		localVue,
		propsData: {
			showModal: true,
			isCheckin: false,
			promoContent
		},
		store
	});

	it('should render modal promo correctly', () => {
		expect(wrapper.find('#modalPromo').exists()).toBe(true);
		expect(wrapper.find('.promo-content__title label').text()).toBe(
			promoContent.title
		);
		expect(wrapper.find('.promo-content__desc span').text()).toBe(
			promoContent.content
		);
		expect(wrapper.find('.promo-content__date span').text()).toBe(
			`${promoContent.from} - ${promoContent.to}`
		);
	});

	describe('computed: authCheck', () => {
		it('should fill authCheck data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
			});
		});
	});

	describe('computed: msgTracking', () => {
		it('should fill msgTracking data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.msgTracking).toBe(
					`track-message ${detailStore.state.detail.msg_tracking}`
				);
			});
		});
	});

	describe('method: closeModal', () => {
		it('should emit close', () => {
			const { closeModal } = wrapper.vm;

			closeModal();
			expect(wrapper.emitted('close')).toBeTruthy();
		});
	});

	describe('method: openModalChat', () => {
		it('should open modal chat if user logged in', () => {
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalChat');
			});
		});

		it("should open modal login if user hasn't logged in", () => {
			const customDetailStore = { ...detailStore };

			customDetailStore.state.authCheck.all = false;
			const store = new Vuex.Store(customDetailStore);

			const wrapper = shallowMount(DetailModalPromo, {
				localVue,
				propsData: {
					showModal: true,
					isCheckin: false,
					promoContent
				},
				store
			});
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('openModalLogin');
			});
		});

		it('should alert error if owner logged in', () => {
			const customDetailStore = { ...detailStore };

			customDetailStore.state.authCheck = {
				all: true,
				user: false,
				owner: true
			};
			const store = new Vuex.Store(customDetailStore);

			const wrapper = shallowMount(DetailModalPromo, {
				localVue,
				propsData: {
					showModal: true,
					isCheckin: false,
					promoContent
				},
				store
			});
			wrapper.setMethods({
				swalError: jest.fn()
			});
			wrapper.vm.openModalChat();

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.swalError).toBeCalled();
			});
		});
	});
});
