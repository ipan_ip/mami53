import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import DetailSmallCarousel from 'Js/_detail/components/DetailSmallCarousel';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
window.Vue = require('vue');
describe('DetailSmallCarousel.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	mockWindowProperty('innerWidth', 500);
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(DetailSmallCarousel, {
			localVue,
			propsData: {
				slideGallery: [
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					}
				],
				updateSlide: jest.fn(),
				activeSlide: 0
			}
		});
	});

	it('Should render gallery picture equal to props slideGallery', () => {
		expect(
			wrapper.findAll('div.detail-small-carousel__img-wrapper').length
		).toEqual(wrapper.vm.slideGallery.length);
	});

	it('Should render gallery 6 section when props slideGallery is 6', () => {
		mockWindowProperty('innerWidth', 1366);
		wrapper = shallowMount(DetailSmallCarousel, {
			localVue,
			propsData: {
				slideGallery: [
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					},
					{
						photo_url: {
							large: 'dummy-url',
							small: 'dummy-url'
						},
						isPotrait: false,
						description: 'dummy description'
					}
				],
				updateSlide: jest.fn(),
				activeSlide: 0
			}
		});
		expect(
			wrapper.findAll('div.detail-small-carousel__img-wrapper').length
		).toEqual(6);
	});
});
