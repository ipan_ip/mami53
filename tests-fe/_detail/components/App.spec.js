import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import App from 'Js/_detail/components/App.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import detailStore from './__mocks__/detailStore';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';

window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin([mixinNavigator, MixinSwalLoading, MixinSwalSuccessError]);
mockVLazy(localVue);
localVue.use(Vuex);
const store = new Vuex.Store(detailStore);

describe('App.vue', () => {
	let mocks = {
		$route: {
			path: '/',
			meta: {
				booking_request: false
			}
		},
		$router: {
			replace: jest.fn()
		},
		$dayjs: jest.fn(() => {
			return {
				locale: jest.fn(() => ({
					format: jest.fn()
				}))
			};
		})
	};
	const stubs = {
		'navbar-custom': mockComponent,
		'container-detail': mockComponent,
		'container-detail-apartment': mockComponent,
		'overlay-stepper-modal': mockComponent
	};
	const flashSaleOnShowTrackerSpy = jest.spyOn(
		App.methods,
		'flashSaleOnShowTracker'
	);
	const checkFTUESpy = jest.spyOn(App.methods, 'checkFTUE');
	const redirectFlashSaleTrackerSpy = jest.spyOn(
		App.methods,
		'redirectFlashSaleTracker'
	);
	const setFlashSaleModalSpy = jest.spyOn(App.methods, 'setFlashSaleModal');
	const redirectBenefitTrackerSpy = jest.spyOn(
		App.methods,
		'redirectBenefitTracker'
	);
	const BenefitOnSwipTrackerSpy = jest.spyOn(
		App.methods,
		'BenefitOnSwipTracker'
	);
	const getBookingBenefitDataSpy = jest.spyOn(
		App.methods,
		'getBookingBenefitData'
	);
	const closeBookingBenefitModalSpy = jest.spyOn(
		App.methods,
		'closeBookingBenefitModal'
	);
	const closeFlashSaleModalSpy = jest.spyOn(App.methods, 'closeFlashSaleModal');
	const notify = jest.fn();

	mockWindowProperty('open', jest.fn());
	mockWindowProperty('tracker', jest.fn());
	mockWindowProperty('performance', {
		navigation: {
			type: 2
		}
	});
	mockWindowProperty('moment', () => {
		return {
			add: jest.fn()
		};
	});
	mockWindowProperty('document.querySelector', () => {
		return {
			content: '21 Mei 2020'
		};
	});
	mockWindowProperty('Cookies', {
		get: jest.fn(() => undefined),
		set: jest.fn(),
		remove: jest.fn(),
		getJSON: jest.fn(() => [])
	});
	mockWindowProperty('bugsnagClient', {
		notify
	});
	global.axios = {
		get: jest.fn().mockImplementation(url => {
			switch (url) {
				case '/sliders':
					return Promise.resolve({
						data: {
							sliders: [
								{
									name: 'BOOKING BENEFIT',
									endpoint: '/getBenefit'
								}
							]
						}
					});
				case '/getBenefit':
					return Promise.resolve({
						data: {
							slider: {
								pages: [
									{
										is_cover: true
									},
									{
										is_cover: false
									}
								]
							}
						}
					});
			}
		})
	};

	let wrapper = shallowMount(App, {
		localVue,
		store,
		mocks,
		stubs
	});

	const newRoomId = wrapper.vm.detail._id.toString();

	it('should store a single id, if no existing idkamar cookie', () => {
		Cookies.get = jest.fn(() => undefined);

		wrapper.vm.setIdKamarCookies(newRoomId);

		expect(Cookies.set).toBeCalledWith('idkamar', newRoomId, {
			expires: 15
		});
	});

	it('should concatenate the existing idkamar cookie with the new one', () => {
		Cookies.get = jest.fn(() => '1');

		wrapper.vm.setIdKamarCookies(newRoomId);

		expect(Cookies.remove).toBeCalledWith('idkamar');
		expect(Cookies.set).toBeCalledWith('idkamar', '1,' + newRoomId, {
			expires: 15
		});
	});

	it('should eliminate the first n data on idkamar cookie if its size exceed the limit', () => {
		Cookies.get = jest.fn(() => '1,2,3,4,5');

		// simulate limit cookie to 28 byte, then remove the first 3 items
		wrapper.vm.setIdKamarCookies(newRoomId, 28, 3);

		expect(Cookies.remove).toBeCalledWith('idkamar');
		expect(Cookies.set).toBeCalledWith('idkamar', '4,5,' + newRoomId, {
			expires: 15
		});
	});

	it('should remove the data if exist and insert it at the end of array', () => {
		wrapper.vm.$store.commit('setAuthCheck', {
			all: false,
			user: false,
			owner: false,
			admin: false
		});

		wrapper.vm.$store.commit('setBookingRequest', false);

		mocks = {
			...mocks,
			$route: {
				path: '/booking-request',
				meta: {
					booking_request: true
				}
			}
		};

		Cookies.get = jest.fn(() => '1,2,3');

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs
		});

		wrapper.vm.setIdKamarCookies(1, 28, 10);

		expect(Cookies.remove).toBeCalledWith('idkamar');
		expect(Cookies.set).toBeCalledWith('idkamar', '2,3,1', {
			expires: 15
		});
	});

	it('should render computed showBookingRequest correctly', () => {
		wrapper.vm.$store.commit('setAuthCheck', {
			all: true,
			user: true,
			owner: false,
			admin: false
		});

		expect(wrapper.vm.showBookingRequest).toBe(false);

		wrapper.vm.$store.commit('setBookingRequest', true);

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks: {
				...mocks,
				$route: {
					path: '/',
					meta: {
						booking_request: true
					}
				}
			},
			stubs
		});

		expect(wrapper.vm.showBookingRequest).toBe(true);
	});

	it('should render computed deviceType correctly', () => {
		expect(wrapper.vm.deviceType).toBe('desktop');

		mockWindowProperty('innerWidth', 400);

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs
		});

		expect(wrapper.vm.deviceType).toBe('mobile');
	});

	it('should call method flashSaleOnShowTracker correctly', async () => {
		const overlayStepperModalStub = {
			template: `<div><button class="redirect-flashsale" @click="$emit('redirectTracker')"/><button class="close-modal" @click="onClose"/></div>`,
			props: ['onClose']
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs: {
				...stubs,
				'overlay-stepper-modal': overlayStepperModalStub
			},
			data() {
				return {
					detailType: 'kost'
				};
			},
			computed: {
				detail() {
					return {
						...detailStore.state.detail,
						flash_sale: {
							id: 123124
						}
					};
				}
			}
		});

		expect(checkFTUESpy).toBeCalled();

		await wrapper.vm.$nextTick();

		expect(flashSaleOnShowTrackerSpy).toBeCalled();
	});

	it('should call method redirectFlashSaleTracker and setFlashSaleModal correctly when click on redirect button', async () => {
		const redirectElement = wrapper.find('button.redirect-flashsale');
		const closeElement = wrapper.find('button.close-modal');

		expect(redirectElement.exists()).toBe(true);
		expect(closeElement.exists()).toBe(true);

		await redirectElement.trigger('click');

		expect(redirectFlashSaleTrackerSpy).toBeCalled();
		expect(setFlashSaleModalSpy).toBeCalled();

		await closeElement.trigger('click');

		expect(closeFlashSaleModalSpy).toBeCalled();
	});

	it('should call method redirectBenefitTracker, BenefitOnSwipTracker, and closeBookingBenefitModal correctly', async () => {
		const overlayStepperModalStub = {
			template: `<div><button class="redirect-benefit" @click="$emit('redirectTracker', 'desktop')"/><button class="swipe-tracker" @click="$emit('swipeTracker', {deviceName: 'desktop', content: 'asd',activeSlide: 1})"/></div>`
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs: {
				...stubs,
				'overlay-stepper-modal': overlayStepperModalStub
			}
		});

		const redirectElement = wrapper.find('button.redirect-benefit');
		const swiperElement = wrapper.find('button.swipe-tracker');

		expect(redirectElement.exists()).toBe(true);
		expect(swiperElement.exists()).toBe(true);

		await redirectElement.trigger('click');

		expect(redirectBenefitTrackerSpy).toBeCalled();

		await swiperElement.trigger('click');

		expect(BenefitOnSwipTrackerSpy).toBeCalled();
	});

	it('Should call checkFTUE method with show booking FTUE', async () => {
		global.axios = {
			get: jest.fn().mockImplementation(url => {
				switch (url) {
					case '/sliders':
						return Promise.resolve({
							data: {
								sliders: [
									{
										name: 'BOOKING BENEFIT',
										endpoint: '/getBenefit'
									}
								]
							}
						});
					case '/getBenefit':
						return Promise.reject(new Error('fail'));
				}
			})
		};

		const overlayStepperModalStub = {
			template: `<button class="close-benefit" @click="onClose"/>`,
			props: ['onClose']
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs: {
				...stubs,
				'overlay-stepper-modal': overlayStepperModalStub
			},
			data() {
				return {
					detailType: 'kost'
				};
			}
		});

		wrapper.vm.$store.commit('setDetail', {
			is_booking: true
		});
		wrapper.vm.$store.commit('setDetail', {
			flashSaleData: null
		});
		wrapper.vm.checkFTUE();

		const closeElement = wrapper.find('button.close-benefit');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeBookingBenefitModalSpy).toBeCalled();
	});

	it('Should call getBookingBenefitDataSpy with reject value', async () => {
		global.axios = {
			get: jest.fn().mockImplementation(url => {
				switch (url) {
					case '/sliders':
						return Promise.reject(new Error('fail'));
					case '/getBenefit':
						return Promise.reject(new Error('fail'));
				}
			})
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs
		});

		expect(getBookingBenefitDataSpy).toBeCalled();
	});
});
