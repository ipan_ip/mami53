import 'tests-fe/utils/mock-vue';
import DetailHeader from 'Js/_detail/components/DetailHeader.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import mockComponent from '../../utils/mock-component';

import photoGallery from './__mocks__/photoGallery';
import EventBus from 'Js/_detail/event-bus';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

global.tracker = jest.fn();

const mockUserData = {
	name: 'Test',
	id: '_01'
};

const mockMoEngageData = {
	interface: 'desktop',
	tab_name: 'Photo',
	is_owner: true,
	user_id: mockUserData.id
};

describe('DetailHeader.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin(mixinNavigator);
	let wrapper = shallowMount(DetailHeader, {
		localVue,
		propsData: {
			hasMedia: true
		},
		stubs: {
			DetailPhoto: mockComponent,
			DetailPhoto360: mockComponent,
			DetailVideo: mockComponent,
			DetailPhotoTour: mockComponent,
			DetailGallerySwitcher: mockComponent
		}
	});

	it('should render detail header', () => {
		expect(wrapper.find('.detail-header').exists()).toBe(true);
	});

	describe('computed: photoGallery', () => {
		it('should return empty array', () => {
			expect(wrapper.vm.photoGallery).toStrictEqual([]);
		});

		it('should return data detail card', () => {
			const store = new Vuex.Store(photoGallery);
			wrapper = shallowMount(DetailHeader, {
				localVue,
				propsData: {
					hasMedia: true
				},
				store
			});
			expect(wrapper.vm.photoGallery).toBe(store.state.detail.cards);
		});
	});

	describe('method: handleMoEngage', () => {
		beforeEach(() => {
			const authData = { all: mockUserData };
			const authCheck = { all: true, owner: true };

			global.authData = authData;
			global.authCheck = authCheck;

			wrapper = shallowMount(DetailHeader, {
				localVue,
				propsData: {
					hasMedia: true
				}
			});
		});

		it('should send correct tracking properti when photo is showed', () => {
			wrapper.vm.handleMoEngage('isPhoto');
			expect(global.tracker).toBeCalledWith('moe', [
				'Gallery Tab Clicked',
				mockMoEngageData
			]);
		});

		it('should send correct tracking properti when video is showed', () => {
			mockMoEngageData.tab_name = 'Video Tour';
			wrapper.vm.handleMoEngage('isVideo');
			expect(global.tracker).toBeCalledWith('moe', [
				'Gallery Tab Clicked',
				mockMoEngageData
			]);
		});

		it('should send correct tracking properti when tour is showed', () => {
			mockMoEngageData.tab_name = 'Video Tour';
			wrapper.vm.handleMoEngage('isTour');
			expect(global.tracker).toBeCalledWith('moe', [
				'Gallery Tab Clicked',
				mockMoEngageData
			]);
		});

		it('should send correct tracking properti in mobile', () => {
			global.innerWidth = 766;
			wrapper = shallowMount(DetailHeader, {
				localVue,
				propsData: {
					hasMedia: true
				}
			});

			mockMoEngageData.tab_name = 'Photo 360';
			mockMoEngageData.interface = 'mobile';

			wrapper.vm.handleMoEngage('is360');
			expect(wrapper.vm.navigator.isMobile).toBe(true);
			expect(global.tracker).toBeCalledWith('moe', [
				'Gallery Tab Clicked',
				mockMoEngageData
			]);
		});
	});

	describe('method: listenGallerySwitcher', () => {
		it('should call EventBus $on', async () => {
			global.authCheck = {
				all: false,
				tenant: false,
				owner: false
			};

			wrapper = shallowMount(DetailHeader, {
				localVue,
				propsData: {
					hasMedia: true
				}
			});

			const activeToggler = jest.fn();
			EventBus.$on('switchPhoto', activeToggler);
			EventBus.$on('switchMap', activeToggler);
			EventBus.$on('switch360', activeToggler);
			EventBus.$on('switchVideo', activeToggler);
			EventBus.$on('switchTour', activeToggler);

			await EventBus.$emit('switchPhoto');
			await EventBus.$emit('switchMap');
			await EventBus.$emit('switch360');
			await EventBus.$emit('switchVideo');
			await EventBus.$emit('switchTour');

			expect(activeToggler).toBeCalledTimes(5);
		});
	});

	describe('method: setActiveToggler', () => {
		global.window.scrollTo = jest.fn();
		it('should send tracking properti', () => {
			wrapper.vm.setActiveToggler();
			expect(global.window.scrollTo).toHaveBeenCalled();
		});
	});

	describe('watch: toggleStatus', () => {
		it('should call EventBus emit pause360 if is360 is true', async () => {
			EventBus.$emit = jest.fn();

			wrapper = shallowMount(DetailHeader, {
				localVue,
				propsData: {
					hasMedia: true
				}
			});

			await wrapper.setData({
				toggleStatus: {
					...wrapper.vm.toggleStatus,
					is360: true
				}
			});

			expect(EventBus.$emit).toBeCalledWith('pause360', true);
		});

		it('should call EventBus emit pause360 if isVideo is true', async () => {
			await wrapper.setData({
				toggleStatus: {
					...wrapper.vm.toggleStatus,
					is360: false,
					isVideo: true
				}
			});

			expect(EventBus.$emit).toBeCalledWith('pauseVideo', true);
		});
	});
});
