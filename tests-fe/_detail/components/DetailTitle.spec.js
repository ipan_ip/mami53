import 'tests-fe/utils/mock-vue';
import DetailTitle from 'Js/_detail/components/DetailTitle.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';
import detailApartmentStore from './__mocks__/detailApartmentStore';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailTitle.vue', () => {
	const wrapper = shallowMount(DetailTitle, { localVue, store });

	it('should render detail title', () => {
		expect(wrapper.find('#kostTitleSection').exists()).toBe(true);
	});

	describe('computed: gender', () => {
		it('should fill gender data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.gender).toBe(detailStore.state.detail.gender);
			});
		});
	});

	describe('computed: kostName', () => {
		it('should fill kostName data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.kostName).toBe(detailStore.state.detail.room_title);
			});
		});
	});

	describe('computed: lastUpdated', () => {
		it('should fill lastUpdated data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.lastUpdated).toBe(
					detailStore.state.detail.updated_at
				);
			});
		});
	});

	describe('computed: rating', () => {
		it('should fill rating data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.rating).toBe(detailStore.state.detail.rating);
			});
		});
	});

	describe('computed: kostClass', () => {
		it('should fill kostClass data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.kostClass).toBe(detailStore.state.detail.class);
			});
		});
	});

	describe('computed: kostkostClassBadgeClass', () => {
		it('should fill kostClassBadge data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.kostClassBadge).toBe(
					detailStore.state.detail.class_badge
				);
			});
		});
	});

	describe('computed: availableRoom', () => {
		it('should fill availableRoom data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.availableRoom).toBe(
					detailStore.state.detail.available_room
				);
			});
		});
	});

	describe('computed: unitCondition', () => {
		it('should be null if detail type is kost', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.unitCondition).toBe(null);
			});
		});

		it('should fill unitCondition data with data from store if detail type is apartment', () => {
			const store = new Vuex.Store(detailApartmentStore);
			const wrapper = shallowMount(DetailTitle, { localVue, store });

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.unitCondition).toBe(
					detailApartmentStore.state.detail.unit_properties.condition
				);
			});
		});
	});

	describe('computed: aptAddress', () => {
		it('should be empty if detail type is kost', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.aptAddress).toBe('');
			});
		});

		it('should fill aptAddress data with data from store if detail type is apartment', () => {
			const store = new Vuex.Store(detailApartmentStore);
			const wrapper = shallowMount(DetailTitle, { localVue, store });

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.aptAddress).toBe(
					detailApartmentStore.state.detail.project.address
				);
			});
		});
	});

	it('Computed isKosAndalan should return correctly', () => {
		expect(wrapper.vm.isKosAndalan).toBe(false);
	});

	it('Computed kosAndalanTitle should return correctly', () => {
		expect(wrapper.vm.kosAndalanTitle).toBe('Kos Andalan');
	});
});
