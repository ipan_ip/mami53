import 'tests-fe/utils/mock-vue';
import DetailKostReviewFacRating from 'Js/_detail/components/kost/kost-review/DetailKostReviewFacRating';
import { shallowMount, createLocalVue } from '@vue/test-utils';
const MixinKostReview = require('Js/_detail/mixins/MixinKostReview');

const localVue = createLocalVue();
localVue.mixin([MixinKostReview]);

const starRating = {
	template: `<span class="star-rating" />`
};

describe('DetailKostReviewFacRating.vue', () => {
	const wrapper = shallowMount(DetailKostReviewFacRating, {
		localVue,
		stubs: {
			starRating
		}
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
