import 'tests-fe/utils/mock-vue';
import DetailKostReviewModal from 'Js/_detail/components/kost/kost-review/DetailKostReviewModal';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { modal } from 'vue-strap';
const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigator]);

const handleCloseRulesModalSpy = jest.spyOn(
	DetailKostReviewModal.methods,
	'handleCloseRulesModal'
);
const handleSortingValueSpy = jest.spyOn(
	DetailKostReviewModal.methods,
	'handleSortingValue'
);
const handleSetSortingSpy = jest.spyOn(
	DetailKostReviewModal.methods,
	'handleSetSorting'
);
const handleToggleSortingModalSpy = jest.spyOn(
	DetailKostReviewModal.methods,
	'handleToggleSortingModal'
);
const handlePassReviewGalleryValueSpy = jest.spyOn(
	DetailKostReviewModal.methods,
	'handlePassReviewGalleryValue'
);

const DetailKostReviewSortingModal = {
	template: `<div><button class="set-sorting" @click="$emit('handle-set-sorting', 'last')"/><button class="toggle-sorting" @click="$emit('handle-toggle-sorting-modal', false)"/></div>`
};
const DetailKostReviewUsersFeedback = {
	template: `<button class="toggle-photo-gallery" @click="$emit('handle-toggle-review-gallery', true)"/>`
};
const BaseMainFilter = {
	template: `<button class="set-sorting-value" @click="$emit('set-value', 'last')"/>`
};

const stubs = {
	modal,
	DetailKostReviewSortingModal,
	DetailKostReviewUsersFeedback,
	BaseMainFilter
};

const propsData = {
	kostReviews: [
		{
			user_photo_profile_url: 'image.png',
			name: 'User 1',
			reviewDate: '1 hari yang lalu',
			rating: 4.0,
			reviewContent: 'long description',
			content: 'short description',
			hasPhoto: true,
			photo: [],
			hasOwnerReply: true,
			reply_owner: {}
		}
	],
	reviewSortedData: {
		data: []
	}
};

describe('DetailKostReviewModal.vue', () => {
	let wrapper = shallowMount(DetailKostReviewModal, {
		propsData,
		localVue,
		stubs
	});

	it('Watch isShow should be handled correctly to add event listener', async () => {
		expect(wrapper.vm.isScrollEventAssigned).toBe(false);

		await wrapper.setProps({ isShow: true });

		expect(wrapper.vm.isScrollEventAssigned).toBe(true);

		await wrapper.setProps({ isShow: false });

		expect(wrapper.vm.isScrollEventAssigned).toBe(true);
	});

	it('Should call method handleCloseRulesModal on closing the modal when the user hit the button', async () => {
		wrapper.setProps({ isShow: true });

		expect(wrapper.vm.isScrollEventAssigned).toBe(true);

		const closeElement = wrapper.find('span,kost-review-modal-header__close');

		expect(closeElement.exists()).toBe(true);

		closeElement.trigger('click');

		expect(handleCloseRulesModalSpy).toBeCalled();
		expect(wrapper.emitted('handle-toggle-all-review-modal')).toBeTruthy();
	});

	it('Should call handleSetSorting correctly when user pick the options', () => {
		wrapper.setData({ isSortingModalOpen: true });

		const setSortingElement = wrapper.find('button.set-sorting');

		expect(setSortingElement.exists()).toBe(true);

		setSortingElement.trigger('click');

		expect(handleSetSortingSpy).toBeCalledWith('last');
		expect(wrapper.emitted('handle-set-sorting')).toBeTruthy();
	});

	it('Should call handleToggleSortingModal correctly when user close the modal', () => {
		expect(wrapper.vm.isSortingModalOpen).toBe(true);

		const toggleSortingElement = wrapper.find('button.toggle-sorting');

		expect(toggleSortingElement.exists()).toBe(true);

		toggleSortingElement.trigger('click');

		expect(handleToggleSortingModalSpy).toBeCalledWith(false);
		expect(wrapper.vm.isSortingModalOpen).toBe(false);
	});

	it('Should call handlePassReviewGalleryValue correctly when user open the gallery', () => {
		const toggleGalleryElement = wrapper.find('button.toggle-photo-gallery');

		expect(toggleGalleryElement.exists()).toBe(true);

		toggleGalleryElement.trigger('click');

		expect(handlePassReviewGalleryValueSpy).toBeCalledWith(true);
		expect(wrapper.emitted('handle-toggle-review-gallery')).toBeTruthy();
	});

	it('Should call handleSortingValue with desktop user', () => {
		const sortingValueElement = wrapper.find('button.set-sorting-value');

		expect(sortingValueElement.exists()).toBe(true);

		sortingValueElement.trigger('click');

		expect(handleSortingValueSpy).toBeCalledWith('last');
		expect(handleSetSortingSpy).toBeCalled();

		wrapper.destroy();
	});

	it('Should call handleSortingValue with mobile user', () => {
		global.innerWidth = 400;

		wrapper = shallowMount(DetailKostReviewModal, {
			propsData,
			localVue,
			stubs
		});

		const sortingValueElement = wrapper.find('button.set-sorting-value');

		expect(sortingValueElement.exists()).toBe(true);

		sortingValueElement.trigger('click');

		expect(handleSortingValueSpy).toBeCalledWith('last');
		expect(wrapper.vm.isSortingModalOpen).toBe(true);
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const testCase = [
			{
				kostReviews: new Array(10),
				totalReviews: 10,
				isShow: true,
				scrollTop: 1,
				scrollHeight: 100,
				offsetHeight: 50,
				emitted: false
			},
			{
				kostReviews: new Array(10),
				totalReviews: 15,
				isShow: true,
				scrollTop: 500,
				scrollHeight: 200,
				offsetHeight: 50,
				emitted: true
			}
		];

		for (const test of testCase) {
			wrapper = shallowMount(DetailKostReviewModal, {
				localVue,
				propsData: {
					kostReviews: test.kostReviews,
					totalReviews: test.totalReviews,
					isShow: test.isShow,
					reviewSortedData: {
						data: []
					}
				}
			});

			wrapper.vm.$refs.allReview = {
				scrollTop: test.scrollTop,
				scrollHeight: test.scrollHeight,
				offsetHeight: test.offsetHeight
			};

			wrapper.vm.handleScroll();

			if (test.emitted) {
				expect(wrapper.emitted('handle-fetch-all-review')).toBeTruthy();
			}
		}
	});

	it('Should call handleSetScrollPosition when the modal is opened', async () => {
		const testCase = [
			{
				width: 1366,
				reviewModalScrollPosition: {
					type: 'top',
					indexUserFeedback: 0
				},
				call: false
			},
			{
				width: 400,
				reviewModalScrollPosition: {
					type: 'fail',
					indexUserFeedback: 0
				},
				refValue: [],
				call: false
			},
			{
				width: 400,
				reviewModalScrollPosition: {
					type: 'top',
					indexUserFeedback: 0
				},
				refValue: [],
				call: true,
				offset: 0
			},
			{
				width: 400,
				reviewModalScrollPosition: {
					type: 'section',
					indexUserFeedback: 0
				},
				refUserValue: [
					{
						offsetTop: 100
					}
				],
				refOwnerValue: [],
				call: true,
				offset: 40
			},
			{
				width: 400,
				reviewModalScrollPosition: {
					type: 'owner',
					indexUserFeedback: 0
				},
				refUserValue: [],
				refOwnerValue: [
					{
						$el: {
							offsetTop: 100
						}
					}
				],
				call: true,
				offset: 40
			}
		];

		for (const test of testCase) {
			global.innerWidth = test.width;

			wrapper = shallowMount(DetailKostReviewModal, {
				localVue,
				propsData: {
					...propsData,
					reviewModalScrollPosition: test.reviewModalScrollPosition
				}
			});

			const mockScrollTo = jest.fn();
			wrapper.vm.$refs.allReview.scrollTo = mockScrollTo;
			wrapper.vm.$refs.usersFeedbackComponent.$refs.userReviewModal0 =
				test.refUserValue;
			wrapper.vm.$refs.usersFeedbackComponent.$refs.ownerReplyModal0 =
				test.refOwnerValue;

			wrapper.vm.handleSetScrollPosition();

			if (test.call) {
				expect(mockScrollTo).toBeCalledWith(0, test.offset);
			}
		}
	});
});
