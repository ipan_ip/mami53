import 'tests-fe/utils/mock-vue';
import DetailKostReviewSortingModal from 'Js/_detail/components/kost/kost-review/DetailKostReviewSortingModal';
import { shallowMount } from '@vue/test-utils';
import ModalSortingReview from 'Js/@components/PickABooSelectModal';

const handleCloseSortingModalSpy = jest.spyOn(
	DetailKostReviewSortingModal.methods,
	'handleCloseSortingModal'
);
const handleSetSortingSpy = jest.spyOn(
	DetailKostReviewSortingModal.methods,
	'handleSetSorting'
);
const handlePassSortingValueSpy = jest.spyOn(
	DetailKostReviewSortingModal.methods,
	'handlePassSortingValue'
);

const baseFilterOption = {
	template: `<button class="choose-sorting" @click="$emit('set-value', 'last')"/>`
};
const BgButton = {
	template: `<button class="apply-sorting" @click="$emit('click')"/>`
};

const stubs = {
	baseFilterOption,
	ModalSortingReview,
	BgButton
};

describe('DetailKostReviewSortingModal.vue', () => {
	const wrapper = shallowMount(DetailKostReviewSortingModal, {
		propsData: {
			reviewSortedData: {
				data: []
			},
			isShow: true
		},
		stubs
	});

	it('Should call method handleSetSorting when user pick the sorting options', () => {
		expect(wrapper.vm.activeSorting).toBe('new');

		const setSortingElement = wrapper.find('button.choose-sorting');

		expect(setSortingElement.exists()).toBe(true);

		setSortingElement.trigger('click');

		expect(handleSetSortingSpy).toBeCalled();
		expect(wrapper.vm.activeSorting).toBe('last');
	});

	it('Should call handlePassSortingValue when apply the sorting value and close the modal', () => {
		const aplySortingElement = wrapper.find('button.apply-sorting');

		expect(aplySortingElement.exists()).toBe(true);

		aplySortingElement.trigger('click');

		expect(handlePassSortingValueSpy).toBeCalled();
		expect(wrapper.emitted('handle-set-sorting')).toBeTruthy();
		expect(handleCloseSortingModalSpy).toBeCalled();
		expect(wrapper.emitted('handle-toggle-sorting-modal')).toBeTruthy();
	});
});
