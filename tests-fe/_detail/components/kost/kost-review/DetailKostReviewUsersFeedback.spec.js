import 'tests-fe/utils/mock-vue';
import DetailKostReviewUsersFeedback from 'Js/_detail/components/kost/kost-review/DetailKostReviewUsersFeedback';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
const MixinKostReview = require('Js/_detail/mixins/MixinKostReview');
const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin([MixinKostReview, MixinNavigator]);

const handleOpenAllReviewModalSpy = jest.spyOn(
	DetailKostReviewUsersFeedback.methods,
	'handleOpenAllReviewModal'
);
const handlePassReviewGalleryValueSpy = jest.spyOn(
	DetailKostReviewUsersFeedback.methods,
	'handlePassReviewGalleryValue'
);
const handleOpenFromOwnerSpy = jest.spyOn(
	DetailKostReviewUsersFeedback.methods,
	'handleOpenFromOwner'
);
const handleOpenFromSectionSpy = jest.spyOn(
	DetailKostReviewUsersFeedback.methods,
	'handleOpenFromSection'
);

const DetailKostReviewPhotos = {
	template: `<button class="open-gallery" @click="$emit('handle-pass-review-gallery-value', {})"/>`
};

const stubs = {
	DetailKostReviewPhotos
};

const kostReviews = [
	{
		user_photo_profile_url: 'image.png',
		name: 'User 1',
		reviewDate: '1 hari yang lalu',
		rating: 4.0,
		reviewContent: 'long description',
		content: 'short description',
		hasPhoto: true,
		photo: [],
		hasOwnerReply: true,
		reply_owner: {}
	}
];

describe('DetailKostReviewUsersFeedback.vue', () => {
	let wrapper = shallowMount(DetailKostReviewUsersFeedback, {
		propsData: {
			kostReviews
		},
		stubs,
		localVue
	});

	it('Should call method handlePassReviewGalleryValue when user clicking the image', () => {
		wrapper.setProps({ identifier: 'Modal' });
		const openGalleryElement = wrapper.find('button.open-gallery');

		expect(openGalleryElement.exists()).toBe(true);

		openGalleryElement.trigger('click');

		expect(handlePassReviewGalleryValueSpy).toBeCalled();
		expect(wrapper.emitted('handle-toggle-review-gallery')[0]).toEqual([
			{
				entryPoint: 'Detail Review'
			}
		]);

		wrapper.setProps({ identifier: '' });
		openGalleryElement.trigger('click');

		expect(handlePassReviewGalleryValueSpy).toBeCalled();
		expect(wrapper.emitted('handle-toggle-review-gallery')[1]).toEqual([
			{
				entryPoint: 'Overview Review'
			}
		]);
	});

	it('Should call handleOpenFromOwner when the review have owner reply and the user click it', () => {
		global.innerWidth = 400;

		wrapper = shallowMount(DetailKostReviewUsersFeedback, {
			propsData: {
				kostReviews
			},
			stubs,
			localVue
		});

		wrapper.setProps({ useCardOnMobile: true });
		const openOwnerReplyElement = wrapper
			.findAll('span.user-feedback__owner-reply-text')
			.at(0);

		expect(openOwnerReplyElement.exists()).toBe(true);

		openOwnerReplyElement.trigger('click');

		expect(handleOpenFromOwnerSpy).toBeCalled();
		expect(handleOpenAllReviewModalSpy).toBeCalled();
	});

	it('Should call handleOpenFromSection method when user click the feedback section and pass the value to the parent component', () => {
		wrapper.vm.handleOpenFromSection();

		expect(handleOpenFromSectionSpy).toBeCalled();
		expect(handleOpenAllReviewModalSpy).toBeCalled();
	});
});
