export default {
	setReview1: {
		total: 3,
		rating: 4.5,
		total_page: 1,
		review: [
			{
				review_id: 15023,
				name: 'Anonim',
				is_anonim: true,
				rating: '1.0',
				content: 'Content',
				tanggal: '30 Apr 2019',
				status: 'live',
				is_me: false,
				photo: [
					{
						photo_url: {
							small: 'image1.png',
							large: 'image1.png'
						}
					}
				],
				reply_owner: {}
			}
		],
		all_rating: {
			clean: 2,
			happy: 1,
			safe: 3,
			pricing: 2,
			room_facilities: 2,
			public_facilities: 2
		}
	},
	setReview2: {
		total: 5,
		rating: 4.5,
		total_page: 1,
		review: [
			{
				review_id: 15023,
				name: 'Anonim',
				is_anonim: true,
				rating: '1.0',
				content: 'Content',
				tanggal: '30 Apr 2019',
				status: 'live',
				is_me: false,
				photo: [],
				reply_owner: {}
			},
			{
				review_id: 15024,
				name: 'Agus',
				is_anonim: true,
				rating: '3.0',
				content: 'Content',
				tanggal: '30 Apr 2019',
				status: 'live',
				is_me: false,
				photo: [],
				reply_owner: {}
			},
			{
				review_id: 15025,
				name: 'Anonim',
				is_anonim: true,
				rating: '5.0',
				content: 'Content',
				tanggal: '30 Apr 2019',
				status: 'live',
				is_me: false,
				photo: [],
				reply_owner: {}
			}
		],
		all_rating: {
			clean: 2,
			happy: 1,
			safe: 3,
			pricing: 2,
			room_facilities: 2,
			public_facilities: 2
		}
	}
};
