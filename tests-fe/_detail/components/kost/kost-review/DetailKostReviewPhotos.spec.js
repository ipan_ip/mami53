import 'tests-fe/utils/mock-vue';
import DetailKostReviewPhotos from 'Js/_detail/components/kost/kost-review/DetailKostReviewPhotos';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const handlePassReviewGalleryValueSpy = jest.spyOn(
	DetailKostReviewPhotos.methods,
	'handlePassReviewGalleryValue'
);

const localVue = createLocalVue();
mockVLazy(localVue);

describe('DetailKostReviewPhotos.vue', () => {
	const wrapper = shallowMount(DetailKostReviewPhotos, {
		propsData: {
			userPhoto: [
				{
					photo_url: {
						small: 'image.png'
					}
				}
			]
		},
		localVue
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Should call method handlePassReviewGalleryValue when user clicking the image', () => {
		const openImageGallery = wrapper
			.findAll('img.user-photo-review__image')
			.at(0);

		expect(openImageGallery.exists()).toBe(true);

		openImageGallery.trigger('click');

		expect(handlePassReviewGalleryValueSpy).toBeCalledWith(0);
	});
});
