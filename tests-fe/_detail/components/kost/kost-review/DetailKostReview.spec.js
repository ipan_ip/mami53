import 'tests-fe/utils/mock-vue';
import DetailKostReview from 'Js/_detail/components/kost/kost-review/DetailKostReview';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import detailStore from '../../__mocks__/detailStore';
import mockReviews from './__mocks__/kostReviewMock';

jest.mock('Js/@utils/makeAPICall.js');

const MixinKostReview = require('Js/_detail/mixins/MixinKostReview');
const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinKostReview, MixinNavigator]);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const handleFetchReviewSpy = jest.spyOn(
	DetailKostReview.methods,
	'handleFetchReview'
);
const handleToggleAllReviewModalSpy = jest.spyOn(
	DetailKostReview.methods,
	'handleToggleAllReviewModal'
);
const handleSetSortingSpy = jest.spyOn(
	DetailKostReview.methods,
	'handleSetSorting'
);
const handleToggleReviewGallerySpy = jest.spyOn(
	DetailKostReview.methods,
	'handleToggleReviewGallery'
);
const handleSeeAllReviewSpy = jest.spyOn(
	DetailKostReview.methods,
	'handleSeeAllReview'
);

const DetailKostReviewModal = {
	template: `<div><button class="toggle-all-review-false" @click="$emit('handle-toggle-all-review-modal', {isOpen: false})"/><button class="toggle-all-review-true" @click="$emit('handle-toggle-all-review-modal', {isOpen: true, indexUserFeedback: 0, type: 'top'})"/><button class="set-sorting" @click="$emit('handle-set-sorting', 'last')"/><button class="toggle-gallery-boolean" @click="$emit('handle-toggle-review-gallery', true)"/><button class="toggle-gallery" @click="$emit('handle-toggle-review-gallery', {entryPoint: 'Overview Review', isOpen: false, indexUserFeedback: 0, indexImage: 0})"/></div>`
};
const BgButton = {
	template: `<button class="see-all-review" @click="$emit('click')"/>`
};
const stubs = {
	DetailKostReviewModal,
	BgButton
};

makeAPICall.mockImplementation(() => {
	return mockReviews.setReview1;
});

const mocks = {
	$dayjs: jest.fn(() => ({
		diff: jest.fn().mockReturnValue('20')
	}))
};

describe('DetailKostReview.vue', () => {
	let wrapper = shallowMount(DetailKostReview, {
		localVue,
		store,
		mocks,
		stubs
	});

	it('Computed facilitiesRatingSet should return correctly', () => {
		expect(wrapper.vm.facilitiesRatingSet).toEqual([
			[
				{
					name: 'Kebersihan',
					rating: 2,
					tag: 'clean'
				},
				{
					name: 'Kenyamanan',
					rating: 1,
					tag: 'happy'
				},
				{
					name: 'Keamanan',
					rating: 3,
					tag: 'safe'
				}
			],
			[
				{
					name: 'Harga',
					rating: 2,
					tag: 'pricing'
				},
				{
					name: 'Fasilitas Kamar',
					rating: 2,
					tag: 'room_facilities'
				},
				{
					name: 'Fasilitas Umum',
					rating: 2,
					tag: 'public_facilities'
				}
			]
		]);
	});

	it('Computed reviewTitle should return correctly', () => {
		wrapper.setData({ kostRating: 4, totalReviews: 10 });

		expect(wrapper.vm.reviewTitle).toBe('4 (10 review)');
	});

	it('Computed kostId should return correctly', () => {
		expect(wrapper.vm.kostId).toBe(wrapper.vm.$store.state.detail._id);
	});

	it('Computed reviewButtonText should return correctly', () => {
		expect(wrapper.vm.reviewButtonText).toBe('Lihat semua review (10)');
	});

	it('Computed kostReviewsLastId should return correctly', () => {
		expect(wrapper.vm.kostReviewsLastId).toBe(null);
	});

	it('Computed modalEffect should return correctly', () => {
		expect(wrapper.vm.modalEffect).toBe('fade');
	});

	it('Computed reviewSortedData should return correctly', () => {
		expect(wrapper.vm.reviewSortedData).toEqual({
			active: false,
			data: [
				{ name: 'Review terbaru', value: 'new' },
				{ name: 'Review terlama', value: 'last' },
				{ name: 'Rating tertinggi', value: 'best' },
				{ name: 'Rating terendah', value: 'bad' }
			],
			getValue: 'new',
			icon: 'sorting-icon',
			title: 'Review terbaru'
		});
	});

	it('Method handleSetSorting should called correctly when user pick the options', async () => {
		await wrapper.setData({ isRendered: true });

		const setSortingElement = wrapper.find('button.set-sorting');

		expect(setSortingElement.exists()).toBe(true);

		setSortingElement.trigger('click');

		expect(handleSetSortingSpy).toBeCalled();
	});

	it('Method handleToggleReviewGallery should called correctly when user clicking the image on modal', () => {
		const toggleGalleryBoolean = wrapper.find('button.toggle-gallery-boolean');
		const toggleGalleryObject = wrapper.find('button.toggle-gallery');

		expect(toggleGalleryBoolean.exists()).toBe(true);
		expect(toggleGalleryObject.exists()).toBe(true);

		toggleGalleryBoolean.trigger('click');

		expect(handleToggleReviewGallerySpy).toBeCalled();

		toggleGalleryObject.trigger('click');

		expect(handleToggleReviewGallerySpy).toBeCalled();
	});

	it('Should call method handleToggleAllReviewModal correctly when user trigger the event from modal', () => {
		const toggleAllReviewTrue = wrapper.find('button.toggle-all-review-true');
		const toggleAllReviewFalse = wrapper.find('button.toggle-all-review-false');

		expect(toggleAllReviewTrue.exists()).toBe(true);
		expect(toggleAllReviewFalse.exists()).toBe(true);

		toggleAllReviewFalse.trigger('click');

		expect(handleToggleAllReviewModalSpy).toBeCalled();
		expect(wrapper.vm.isAllReviewModalShow).toBe(false);

		toggleAllReviewTrue.trigger('click');

		expect(handleToggleAllReviewModalSpy).toBeCalled();
		expect(wrapper.vm.isAllReviewModalShow).toBe(true);
	});

	it('Method handleFetchReviewSpy and other computed should return correctly when users is on mobile device', () => {
		global.innerWidth = 400;

		makeAPICall.mockImplementation(() => {
			return mockReviews.setReview2;
		});

		wrapper = shallowMount(DetailKostReview, {
			localVue,
			store,
			mocks,
			stubs
		});

		expect(handleFetchReviewSpy).toBeCalled();
		expect(wrapper.vm.modalEffect).toBe('');
	});

	it('Method handleSeeAllReview should return correctly when users click the button', async () => {
		await wrapper.setData({ isRendered: true, isAllReviewRendered: true });

		const seeAllElement = wrapper.find('button.see-all-review');

		expect(seeAllElement.exists()).toBe(true);

		seeAllElement.trigger('click');

		expect(handleSeeAllReviewSpy).toBeCalled();
	});
});
