import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPriceItem from 'Js/_detail/components/kost/kost-price/DetailKostPriceItem.vue';
import 'tests-fe/utils/mock-vue';

const localVue = createLocalVue();
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
localVue.mixin(mixinNavigator);

global.window.innerWidth = 700;

describe('DetailKostPriceItem.vue', () => {
	const wrapper = shallowMount(DetailKostPriceItem, {
		localVue,
		propsData: {
			price: {
				name: 'Price 1',
				price: 20000,
				priceLabel: 'Rp20.000'
			}
		}
	});

	it('should render price item', () => {
		expect(wrapper.find('.kost-price-item').exists()).toBe(true);
	});

	it('should render price data', () => {
		expect(wrapper.find('.kost-price-item__label').text()).toBe('Price 1');
		expect(wrapper.find('.kost-price-item__amount').text()).toBe('Rp20.000');
	});

	it('should emit price-label-clicked if label is clickable', async () => {
		await wrapper.setProps({ isClickablePriceLabel: true });
		wrapper.find('.kost-price-item__label').trigger('click');

		expect(wrapper.emitted('price-label-clicked')[0]).toBeTruthy();
	});

	it('should render price list item if price has items', async () => {
		await wrapper.setProps({
			price: {
				name: 'Price 2',
				items: [
					{ name: 'Price 2-1', price: 1000 },
					{ name: 'Price 2-2', price: 2000 }
				]
			},
			isMany: true
		});

		expect(wrapper.find('.kost-price-item__list').exists()).toBe(true);
		expect(wrapper.findAll('.kost-price-item__list-item').length).toBe(2);
	});

	it('should return emit when showInfoMobile triggered for mobile devices', async () => {
		await wrapper.setProps({
			price: {
				info: 'mock info'
			}
		});
		wrapper.vm.showInfoMobile(wrapper.vm.price.info);

		expect(wrapper.emitted('show-info')).toBeTruthy();
	});
});
