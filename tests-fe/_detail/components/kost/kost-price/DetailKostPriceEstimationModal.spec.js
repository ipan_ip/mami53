import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPriceEstimationModal from 'Js/_detail/components/kost/kost-price/DetailKostPriceEstimationModal.vue';

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();

describe('DetailKostPriceEstimationModal.vue', () => {
	const wrapper = shallowMount(DetailKostPriceEstimationModal, {
		localVue,
		propsData: { bookingButtonText: 'booking button text', value: false },
		stubs: {
			MamiButton: { template: '<button><slot/></button>' },
			DetailKostModal: {
				template: '<div><slot/><slot name="modal-footer"/></div>'
			}
		},
		mocks: {
			$store: {
				state: {
					authCheck: {
						all: true,
						user: true
					},
					detail: {
						is_booking: true
					}
				}
			}
		}
	});

	it('should render estimate price modal', () => {
		expect(wrapper.find('.estimate-price-modal').exists()).toBe(true);
	});

	it('should render booking button text props', () => {
		expect(wrapper.find('button').text()).toBe('booking button text');
	});

	it('should emit booking-clicked when handleActionButtonClicked triggered', () => {
		wrapper.vm.handleActionButtonClicked();

		expect(wrapper.emitted('input')).toBeTruthy();
		expect(wrapper.emitted('booking-clicked')).toBeTruthy();

		wrapper.vm.$store.state.detail.is_booking = false;
	});

	it('should emit chat-clicked when handleActionButtonClicked triggered', () => {
		wrapper.vm.$store.state.detail.is_booking = false;
		wrapper.vm.handleActionButtonClicked();

		expect(wrapper.emitted('chat-clicked')).toBeTruthy();
	});

	it('Watch value to call related method and update data correctly', async () => {
		wrapper.setProps({ value: true });
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.rentTypeParams).toEqual(wrapper.vm.defaultRentType);
	});
});
