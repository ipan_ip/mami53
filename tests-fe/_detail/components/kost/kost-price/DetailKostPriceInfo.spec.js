import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPriceInfo from 'Js/_detail/components/kost/kost-price/DetailKostPriceInfo.vue';

const localVue = createLocalVue();

describe('DetailKostPriceInfo.vue', () => {
	const createWrapper = isMobile =>
		shallowMount(DetailKostPriceInfo, {
			localVue,
			directives: {
				tooltip: {
					inserted: function(el) {
						el.setAttribute('tooltip-directive', true);
					}
				}
			},
			mocks: { navigator: { isMobile } }
		});

	it('should render tooltip if desktop', () => {
		const wrapper = createWrapper(false);

		expect(wrapper.find('.detail-kost-price-info').exists()).toBe(true);
		expect(
			wrapper.find('.detail-kost-price-info').attributes('tooltip-directive')
		).toBeTruthy();
	});

	it('should render info as normal span if is mobile', async () => {
		const wrapper = createWrapper(true);

		expect(
			wrapper.find('.detail-kost-price-info').attributes('tooltip-directive')
		).toBeFalsy();
	});

	it('should emit show info when info clicked', async () => {
		const wrapper = createWrapper(true);

		wrapper.find('.detail-kost-price-info').trigger('click');
		expect(wrapper.emitted('show-info')[0]).toBeTruthy();
	});
});
