import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPriceRentTypeSelect from 'Js/_detail/components/kost/kost-price/DetailKostPriceRentTypeSelect.vue';
import Vuex from 'vuex';
import store from '../../__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockStore = { ...store };
mockStore.getters = {
	availablePrices: () => {
		return {
			monthly: { type: 'monthly' },
			quarterly: { type: 'quarterly' }
		};
	}
};

describe('DetailKostPriceRentTypeSelect.vue', () => {
	const wrapper = shallowMount(DetailKostPriceRentTypeSelect, {
		localVue,
		store: new Vuex.Store(mockStore)
	});

	it('should render rent type select', () => {
		expect(wrapper.find('.rent-type-select').exists()).toBe(true);
	});

	it('should get and set rent type properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.$store.state.params.rent_count_type = 'monthly';
		expect(wrapper.vm.selectedRentType).toBe('monthly');

		wrapper.setData({ selectedRentType: 'quarterly ' });
		expect(spyCommit).toBeCalledWith('setIsOverrideDraftRentCountType', true);
		spyCommit.mockRestore();
	});

	it('should render options properly', async () => {
		wrapper.vm.$store.state.priceCardInfo.price.price_monthly = null;
		await wrapper.vm.$nextTick();

		['monthly', 'quarterly'].forEach(type => {
			expect(
				wrapper
					.find(`.rent-type-select__option[selectedvalue=${type}]`)
					.exists()
			).toBe(true);
		});

		['daily', 'weekly', 'semiannually', 'yearly'].forEach(type => {
			expect(
				wrapper
					.find(`.rent-type-select__option[selectedvalue=${type}]`)
					.exists()
			).not.toBe(true);
		});
	});
});
