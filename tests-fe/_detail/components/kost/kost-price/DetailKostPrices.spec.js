import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPrices from 'Js/_detail/components/kost/kost-price/DetailKostPrices.vue';
import kostPrices from '../../__mocks__/kostPrices.json';

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const monthlyPrice = kostPrices.monthly;

const localVue = createLocalVue();

const DetailKostPriceItem = {
	template: `<div class="detail-kost-price-item" @click='$emit("price-label-clicked")'/>`
};

describe('DetailKostPrices.vue', () => {
	const wrapper = shallowMount(DetailKostPrices, {
		localVue,
		stubs: {
			DetailKostPriceItem
		},
		mocks: {
			$store: {
				getters: {
					availablePrices: {
						monthly: monthlyPrice
					}
				}
			}
		}
	});

	it('should render detail prices', () => {
		expect(wrapper.find('.detail-kost-prices').exists()).toBe(true);
	});

	it('should merge additional price if it is collapsed', () => {
		wrapper.setProps({ isCollapsed: true });
		expect(wrapper.vm.priceOrder.includes('additionalsAndAdminFee')).toBe(true);
	});

	it('should set rendered next payment price list properly', () => {
		expect(
			wrapper.vm.renderedPriceListNext.every(
				price => !['flashSalePrice', 'adminFee', 'deposit'].includes(price.type)
			)
		).toBe(true);
	});

	it('should emit setPriceInfo when price item emit show info', () => {
		wrapper
			.findAllComponents(DetailKostPriceItem)
			.at(0)
			.vm.$emit('show-info', 'price info');

		expect(global.EventBus.$emit).toBeCalledWith('setPriceInfo', {
			show: true,
			info: 'price info'
		});
	});

	it('should handle price label properly', () => {
		wrapper
			.findAllComponents(DetailKostPriceItem)
			.at(0)
			.vm.$emit('price-label-clicked', { type: 'additionalsAndAdminFee' });

		expect(global.EventBus.$emit).toHaveBeenCalledWith('setPriceInfo', {
			show: true,
			items: wrapper.vm.additionalsAndAdminFee.items
		});

		jest.clearAllMocks();
		wrapper
			.findAllComponents(DetailKostPriceItem)
			.at(0)
			.vm.$emit('price-label-clicked', { type: 'additionals' });

		expect(global.EventBus.$emit).not.toHaveBeenCalled();
	});

	it('should handle show price estimation trigger properly', () => {
		jest.clearAllMocks();
		wrapper
			.find('.detail-kost-prices__total > .detail-kost-price-item')
			.trigger('click');

		expect(global.EventBus.$emit).toHaveBeenCalledWith(
			'showPriceEstimationModal',
			true
		);
	});

	it('should render next payment properly', async () => {
		await wrapper.setProps({ isShowNextPayment: false, isShowFullData: true });
		expect(wrapper.find('.detail-kost-prices__next').exists()).toBe(false);

		await wrapper.setProps({ isShowNextPayment: true, isShowFullData: true });
		expect(wrapper.find('.detail-kost-prices__next').exists()).toBe(true);
	});
});
