import 'tests-fe/utils/mock-vue';
import DetailKostOwnerInformation from 'Js/_detail/components/kost/kost-owner-information/DetailKostOwnerInformation.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from '../../__mocks__/detailStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const MixinBookingAcceptanceRate = require('Js/_detail/mixins/MixinBookingAcceptanceRate');
const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const localVue = createLocalVue();
localVue.mixin([MixinNavigator, MixinBookingAcceptanceRate]);
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const openChatModalSpy = jest.spyOn(
	DetailKostOwnerInformation.methods,
	'openChatModal'
);
const handleToggleRateModalSpy = jest.spyOn(
	DetailKostOwnerInformation.methods,
	'handleToggleRateModal'
);

const mocks = {
	$dayjs: jest.fn(() => ({
		locale: jest.fn(() => ({
			format: jest.fn().mockReturnValue('20 May 2020')
		}))
	}))
};

describe('DetailKostOwnerInformation.vue', () => {
	const wrapper = shallowMount(DetailKostOwnerInformation, {
		localVue,
		store,
		mocks
	});

	it('Computed msgTrackingClass should return correctly', () => {
		expect(wrapper.vm.msgTrackingClass).toBe(
			`track-message ${wrapper.vm.$store.state.detail.msg_tracking}`
		);
	});

	it('Computed isBooking should filled with data from store', () => {
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isBooking).toBe(detailStore.state.detail.is_booking);
		});
	});

	it('Computed triggerType should return correctly', () => {
		expect(wrapper.vm.triggerType).toBe('hover');
	});

	it('Computed ownerName should filled with data from store', () => {
		expect(wrapper.vm.ownerName).toBe(
			wrapper.vm.$store.state.detail.owner_name
		);
	});

	it('Computed isOwner should return correctly', () => {
		expect(wrapper.vm.isOwner).toEqual(wrapper.vm.$store.state.authCheck.owner);
	});

	it('Method openChatModal should open correctly', async () => {
		await wrapper.setData({ isRendered: true });

		const openElement = wrapper.find('button.owner-information__chat');

		expect(openElement.exists()).toBe(true);

		openElement.trigger('click');

		expect(openChatModalSpy).toBeCalled();
	});

	it('Method handleToggleRateModal should call correctly', async () => {
		const toggleElement = wrapper.find('button.owner-information__rates-link');

		expect(toggleElement.exists()).toBe(true);

		toggleElement.trigger('click');

		expect(handleToggleRateModalSpy).toBeCalled();
	});
});
