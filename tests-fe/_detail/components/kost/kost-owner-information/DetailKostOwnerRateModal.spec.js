import 'tests-fe/utils/mock-vue';
import DetailKostOwnerRateModal from 'Js/_detail/components/kost/kost-owner-information/DetailKostOwnerRateModal.vue';

import { shallowMount } from '@vue/test-utils';

const handleCloseRateModalSpy = jest.spyOn(
	DetailKostOwnerRateModal.methods,
	'handleCloseRateModal'
);

const modalStub = {
	template: `<button @click="$emit('closed')" class="close-modal" />`
};

describe('DetailKostOwnerRateModal.vue', () => {
	const wrapper = shallowMount(DetailKostOwnerRateModal, {
		stubs: {
			modal: modalStub
		},
		propsData: {
			ownerRates: []
		}
	});

	it('Method handleCloseRateModalSpy should call correctly', async () => {
		const closeElement = wrapper.find('button.close-modal');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(handleCloseRateModalSpy).toBeCalled();
	});
});
