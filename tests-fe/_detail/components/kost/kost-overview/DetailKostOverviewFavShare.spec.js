import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import MockComponent from 'tests-fe/utils/mock-component';
import DetailKostOverviewFavShare from 'Js/_detail/components/kost/kost-overview/DetailKostOverviewFavShare.vue';

describe('DetailKostOverviewFavShare.vue', () => {
	const BgButtonIcon = {
		template: `<button class="bg-button-icon" @click="$emit('click')"><slot/></button>`
	};

	const wrapper = shallowMount(DetailKostOverviewFavShare, {
		mocks: {
			'mami-share': MockComponent,
			BgButtonIcon
		}
	});

	it('Component should render correctly', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
