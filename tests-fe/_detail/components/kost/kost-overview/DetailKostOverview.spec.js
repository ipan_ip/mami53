import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import DetailKostOverview from 'Js/_detail/components/kost/kost-overview/DetailKostOverview.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import detailStore from '../../__mocks__/detailStore';

window.swal = require('sweetalert2/dist/sweetalert2.js');

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
const localVue = createLocalVue();
localVue.mixin([mixinNavigator, MixinSwalLoading, MixinSwalSuccessError]);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);
const mocks = {
	$dayjs: jest.fn(() => {
		return {
			locale: jest.fn(() => ({
				format: jest.fn().mockReturnValue('4 Jan 2020')
			}))
		};
	}),
	$toasted: { show: jest.fn() }
};

const sharePageSpy = jest.spyOn(DetailKostOverview.methods, 'sharePage');
const toggleFavoriteSpy = jest.spyOn(
	DetailKostOverview.methods,
	'toggleFavorite'
);
const submitFavoriteSpy = jest.spyOn(
	DetailKostOverview.methods,
	'submitFavorite'
);
const handleShareTrackingSpy = jest.spyOn(
	DetailKostOverview.methods,
	'handleShareTracking'
);
const handleScrollToComponentSpy = jest.spyOn(
	DetailKostOverview.methods,
	'handleScrollToComponent'
);

mockWindowProperty('navigator', {
	share: jest.fn()
});
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const DetailKostOverviewFavShareStub = {
	template: `<button class="toggle-favorite" @click="$emit('toggle-favorite')"/>`
};

describe('DetailKostOverview.vue', () => {
	let wrapper = shallowMount(DetailKostOverview, {
		localVue,
		store,
		mocks
	});

	it('computed gender should return correctly', () => {
		wrapper.vm.$store.commit('setDetail', { gender: 0 });

		expect(wrapper.vm.gender).toBe('Kos Campur');

		wrapper.vm.$store.commit('setDetail', { gender: 1 });

		expect(wrapper.vm.gender).toBe('Kos Putra');
	});

	it('computed area should return correctly', () => {
		expect(wrapper.vm.area).toBe('Pasar Minggu');
	});

	it('computed rating should return correctly', () => {
		expect(wrapper.vm.rating).toBe(4);
	});

	it('computed ratingValue should return correctly', () => {
		expect(wrapper.vm.ratingValue).toBe('4.0');
	});

	it('computed totalReview should return correctly', () => {
		expect(wrapper.vm.totalReview).toBe(4);
	});

	it('computed showGenderDivider should return correctly', () => {
		expect(wrapper.vm.showGenderDivider).toBe(true);

		wrapper.vm.$store.commit('setDetail', { gender: null });

		expect(wrapper.vm.showGenderDivider).toBe(false);
	});

	it('computed showAreaDivider should return correctly', () => {
		expect(wrapper.vm.showAreaDivider).toBe(true);

		wrapper.vm.$store.commit('setDetail', {
			gender: null,
			rating: 0,
			area_subdistrict: ''
		});

		expect(wrapper.vm.showAreaDivider).toBe(false);
	});

	it('computed showRatingDivider should return correctly', () => {
		expect(wrapper.vm.showRatingDivider).toBe(false);

		wrapper.vm.$store.commit('setDetail', {
			gender: 1,
			rating: 4,
			area_subdistrict: 'Kretek'
		});

		expect(wrapper.vm.showAreaDivider).toBe(true);
	});

	it('computed authCheck should return correctly', () => {
		expect(wrapper.vm.authCheck).toEqual({
			all: true,
			user: true,
			owner: false,
			admin: false
		});
	});

	it('computed detailTypeName should return correctly', () => {
		expect(wrapper.vm.detailTypeName).toBe('Kost');
	});

	it('computed isKostBenefitAvailable should return correctly', () => {
		expect(wrapper.vm.isKostBenefitAvailable).toBe(false);
	});

	it('computed seq should return correctly', () => {
		expect(wrapper.vm.seq).toBe(62041738);
	});

	it('computed token should return correctly', () => {
		expect(wrapper.vm.token).toBe('ABCD');
	});

	it('computed authData should return correctly', () => {
		expect(wrapper.vm.authData).toBe(detailStore.state.authData);
	});

	it('computed detail should return correctly', () => {
		expect(wrapper.vm.detail).toEqual(detailStore.state.detail);
	});

	it('computed roomAvailability should return correctly', () => {
		expect(wrapper.vm.roomAvailability).toEqual({
			icon: 'more-choices',
			title: 'Banyak pilihan kamar untukmu'
		});

		wrapper.vm.$store.commit('setDetail', {
			available_room: 0
		});

		expect(wrapper.vm.roomAvailability).toEqual({
			icon: 'fully-booked',
			style: 'recommendation',
			needRecommendation: false,
			title: 'Kamar penuh'
		});

		wrapper.vm.$store.commit('setDetail', {
			available_room: 2
		});

		expect(wrapper.vm.roomAvailability).toEqual({
			icon: 'room-available',
			style: 'availability',
			title: `2 kamar`
		});
	});

	it('method sharePage should called correctly', async () => {
		mockWindowProperty('navigator', {
			share: jest.fn()
		});

		const DetailKostOverviewFavShareStub = {
			template: `<button class="share-page" @click="$emit('share-page')"/>`
		};

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const sharePageElement = wrapper.find('button.share-page');

		expect(sharePageElement.exists()).toBe(true);

		await sharePageElement.trigger('click');

		expect(sharePageSpy).toBeCalled();
	});

	it('method handleShareTracking should called correctly', async () => {
		const DetailKostOverviewFavShareStub = {
			template: `<button class="share-tracking" @click="$emit('handle-share-tracking')"/>`
		};

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const shareTrackingElement = wrapper.find('button.share-tracking');

		expect(shareTrackingElement.exists()).toBe(true);

		await shareTrackingElement.trigger('click');

		expect(handleShareTrackingSpy).toBeCalled();
	});

	it('method toggleFavorite with login should called correctly', async () => {
		mockWindowProperty('axios', {
			post: jest.fn().mockResolvedValue({
				data: {
					status: true,
					success: 'love'
				}
			})
		});

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const toggleFavoriteElement = wrapper.find('button.toggle-favorite');

		expect(toggleFavoriteElement.exists()).toBe(true);

		await toggleFavoriteElement.trigger('click');

		expect(submitFavoriteSpy).toBeCalled();
		expect(global.tracker).toBeCalled();
		expect(wrapper.vm.$toasted.show).toBeCalledWith('Sukses tersimpan', {
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('method toggleFavorite with else if condition should called correctly', async () => {
		mockWindowProperty('axios', {
			post: jest.fn().mockResolvedValue({
				data: {
					status: true,
					success: 'unlove'
				}
			})
		});

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const toggleFavoriteElement = wrapper.find('button.toggle-favorite');

		expect(toggleFavoriteElement.exists()).toBe(true);

		await toggleFavoriteElement.trigger('click');

		expect(submitFavoriteSpy).toBeCalled();
		expect(wrapper.vm.isFavoritedByMe).toBe(false);
	});

	it('method toggleFavorite with else condition should called correctly', async () => {
		mockWindowProperty('axios', {
			post: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		});

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const toggleFavoriteElement = wrapper.find('button.toggle-favorite');

		expect(toggleFavoriteElement.exists()).toBe(true);

		await toggleFavoriteElement.trigger('click');

		expect(submitFavoriteSpy).toBeCalled();
		expect(wrapper.vm.$toasted.show).toBeCalledWith(
			'Gagal, silahkan coba lagi',
			{
				position: 'bottom-center',
				duration: 2000
			}
		);
	});

	it('method toggleFavorite with catch condition should called correctly', async () => {
		mockWindowProperty('axios', {
			post: jest.fn().mockRejectedValue(new Error('error'))
		});

		wrapper = shallowMount(DetailKostOverview, {
			localVue,
			store,
			mocks,
			stubs: {
				'detail-kost-overview-fav-share': DetailKostOverviewFavShareStub
			}
		});

		const toggleFavoriteElement = wrapper.find('button.toggle-favorite');

		expect(toggleFavoriteElement.exists()).toBe(true);

		await toggleFavoriteElement.trigger('click');

		expect(submitFavoriteSpy).toBeCalled();
		expect(global.bugsnagClient.notify).toBeCalled();
	});

	it('method toggleFavorite with not login or not user status should called correctly', async () => {
		wrapper.vm.$store.commit('setAuthCheck', {
			all: false,
			user: false,
			owner: false,
			admin: false
		});

		const toggleFavoriteElement = wrapper.find('button.toggle-favorite');

		expect(toggleFavoriteElement.exists()).toBe(true);

		await toggleFavoriteElement.trigger('click');

		expect(toggleFavoriteSpy).toBeCalled();
		expect(submitFavoriteSpy).toBeCalled();
		expect(wrapper.emitted('open-modal-login')).toBeTruthy();

		wrapper.vm.$store.commit('setAuthCheck', {
			all: true,
			user: false,
			owner: true,
			admin: false
		});

		await toggleFavoriteElement.trigger('click');

		expect(submitFavoriteSpy).toBeCalled();
	});

	it('method handleScrollToComponentSpy should be called and emitted event correctly', async () => {
		const scrollElement = wrapper.find('div.detail-kost-overview__rating');

		expect(scrollElement.exists()).toBe(true);

		await scrollElement.trigger('click');

		expect(handleScrollToComponentSpy).toBeCalled();
		expect(wrapper.emitted('scroll-to-component')).toBeTruthy();
	});
});
