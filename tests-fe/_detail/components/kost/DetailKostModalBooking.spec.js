import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailKostModalBooking from 'Js/_detail/components/kost/DetailKostModalBooking.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('DetailKostModalBooking.vue', () => {
	window.open = jest.fn().mockResolvedValue('/user/booking', '_self');
	const wrapper = shallowMount(DetailKostModalBooking, {
		localVue,
		propsData: {
			showModal: true
		}
	});

	it('should render modal kost booking', () => {
		expect(wrapper.find('#detailKostModalBooking').exists()).toBe(true);
	});

	it('should close modal when click Batal', () => {
		expect(wrapper.find('.btn-default').exists()).toBe(true);
		wrapper.find('.btn-default').trigger('click');
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should call openBookingList when click view history', () => {
		const openBookingListFn = jest.spyOn(wrapper.vm, 'openBookingList');
		wrapper.setMethods({ openBookingList: openBookingListFn });

		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.btn-primary').exists()).toBe(true);
			wrapper.find('.btn-primary').trigger('click');
			expect(openBookingListFn).toHaveBeenCalled();
			openBookingListFn.mockRestore();
		});
	});

	it('method: closeModal', () => {
		const { closeModal } = wrapper.vm;

		closeModal();
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('method: openBookingList', () => {
		const { openBookingList } = wrapper.vm;

		openBookingList();
		expect(window.open).toHaveBeenCalledWith('/user/booking', '_self');
	});
});
