import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import DetailKostRules from 'Js/_detail/components/kost/kost-rules/DetailKostRules.vue';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from '../../__mocks__/detailStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/@utils/makeAPICall.js');

const MixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigator]);
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const handleShowAllDataSpy = jest.spyOn(
	DetailKostRules.methods,
	'handleShowAllData'
);
const handleToggleRulesModalSpy = jest.spyOn(
	DetailKostRules.methods,
	'handleToggleRulesModal'
);

const detailKostRulesItem = {
	template: `<button class="show-all-data" @click="$emit('handle-show-all-data')"/>`
};
const stubs = {
	detailKostRulesItem: detailKostRulesItem,
	detailKostRulesGalleryModal: mockComponent
};

const mockAPIResponse = {
	rules: [
		{
			name: 'Maks. 2 orang/ kamar',
			small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
		}
	],
	photos: [
		{
			large: '/general/img/test1.jpg',
			medium: '/general/img/test1.jpg'
		}
	]
};

makeAPICall.mockImplementation(() => {
	return mockAPIResponse;
});

describe('DetailKostRules.vue', () => {
	let wrapper = shallowMount(DetailKostRules, {
		localVue,
		store,
		stubs
	});

	it('Computed rules should return correctly based on rules data', () => {
		expect(wrapper.vm.rules).toEqual([
			{
				name: 'Maks. 2 orang/ kamar',
				small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
			}
		]);

		wrapper.setData({
			availableRules: [
				{
					name: 'Maks. 2 orang/ kamar',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Akses 24 jam (bawa kunci)',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Bawa barang elektronik dikenakan biaya tambahan',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Kamar mandi dalam',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				}
			]
		});

		expect(wrapper.vm.rules).toEqual([
			{
				name: 'Maks. 2 orang/ kamar',
				small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
			},
			{
				name: 'Akses 24 jam (bawa kunci)',
				small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
			},
			{
				name: 'Bawa barang elektronik dikenakan biaya tambahan',
				small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
			}
		]);
	});

	it('Computed rulesGallery should return correctly based on rules gallery data', () => {
		expect(wrapper.vm.rulesGallery).toEqual({
			hasMore: false,
			label: '',
			photos: [
				{
					large: '/general/img/test1.jpg',
					medium: '/general/img/test1.jpg'
				}
			]
		});

		wrapper.setData({
			availableRulesGallery: [
				{
					large: '/general/img/test1.jpg',
					medium: '/general/img/test1.jpg'
				},
				{
					large: '/general/img/test2.jpg',
					medium: '/general/img/test2.jpg'
				},
				{
					large: '/general/img/test3.jpg',
					medium: '/general/img/test3.jpg'
				},
				{
					large: '/general/img/test4.jpg',
					medium: '/general/img/test4.jpg'
				}
			]
		});

		expect(wrapper.vm.rulesGallery).toEqual({
			hasMore: true,
			label: '+1 foto',
			photos: [
				{
					large: '/general/img/test1.jpg',
					medium: '/general/img/test1.jpg'
				},
				{
					large: '/general/img/test2.jpg',
					medium: '/general/img/test2.jpg'
				},
				{
					large: '/general/img/test3.jpg',
					medium: '/general/img/test3.jpg'
				}
			]
		});
	});

	it('Computed rulesGalleryCover should return correctly when rules gallery is available', () => {
		expect(wrapper.vm.rulesGalleryCover).toEqual({
			large: '/general/img/test1.jpg',
			medium: '/general/img/test1.jpg'
		});

		wrapper.setData({
			availableRulesGallery: []
		});

		expect(wrapper.vm.rulesGalleryCover).toBeNull();
	});

	it('Computed isMobileGallery should return correctly when the screen breakpoints is 800px and computed modalEffect should return based on user screen', () => {
		expect(wrapper.vm.isMobileGallery).toBe(false);
		expect(wrapper.vm.modalEffect).toBe('fade');

		global.innerWidth = 400;

		wrapper = shallowMount(DetailKostRules, {
			localVue,
			store,
			stubs
		});

		expect(wrapper.vm.isMobileGallery).toBe(true);
		expect(wrapper.vm.modalEffect).toBe('');
	});

	it('Should call method handleToggleRulesModal correctly and set swiper index when the modal is open', async () => {
		await wrapper.setData({ isRendered: true });

		const openModalElement = wrapper.find('div.kost-rules__gallery-cover');

		expect(openModalElement.exists()).toBe(true);

		openModalElement.trigger('click');

		expect(handleToggleRulesModalSpy).toBeCalledWith(true, -1);
	});

	it('Computed isThumbnailCentered should return correctly based on user screen width and the availability of rules gallery', () => {
		expect(wrapper.vm.isThumbnailCentered).toBe(true);

		wrapper.setData({
			availableRulesGallery: new Array(5)
		});

		expect(wrapper.vm.isThumbnailCentered).toBe(false);

		global.innerWidth = 1366;

		wrapper = shallowMount(DetailKostRules, {
			localVue,
			store,
			stubs
		});

		wrapper.setData({
			availableRulesGallery: new Array(4)
		});

		expect(wrapper.vm.isThumbnailCentered).toBe(true);

		wrapper.setData({
			availableRulesGallery: new Array(10)
		});

		expect(wrapper.vm.isThumbnailCentered).toBe(false);
	});

	it('Computed isDataAvailable should return true if rules or rules gallery is available', () => {
		expect(wrapper.vm.isDataAvailable).toBe(true);

		wrapper.setData({
			availableRulesGallery: [],
			availableRules: []
		});

		expect(wrapper.vm.isDataAvailable).toBe(false);
	});

	it('Should call method handleShowAllData when the availability of rules is more than 3 and the user click the toggler', async () => {
		expect(wrapper.vm.isLimited).toBe(true);

		await wrapper.setData({
			isRendered: true,
			availableRules: [
				{
					name: 'Maks. 2 orang/ kamar',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Akses 24 jam (bawa kunci)',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Bawa barang elektronik dikenakan biaya tambahan',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				},
				{
					name: 'Kamar mandi dalam',
					small_icon_url: 'https://sate1.kerupux.com/uploads/tags/fY7Fjerx.png'
				}
			]
		});

		const showAllElement = wrapper.find('button.show-all-data');

		expect(showAllElement.exists()).toBe(true);

		showAllElement.trigger('click');

		expect(handleShowAllDataSpy).toBeCalled();
		expect(wrapper.vm.isLimited).toBe(false);
	});

	it('Should call method handleFetchRules with 3 condition, set response to kost rules data when the response is true, not doing anything when the response is false, and return directly when kost id is have falsy value', async () => {
		wrapper.setData({
			isRendered: true,
			availableRules: [],
			availableRulesGallery: []
		});

		wrapper.vm.$store.commit('setIdKost', null);
		await wrapper.vm.handleFetchRules();

		expect(wrapper.vm.availableRules).toEqual([]);

		wrapper.vm.$store.commit('setIdKost', 123456);
		await wrapper.vm.handleFetchRules();

		expect(wrapper.vm.availableRules).toEqual(mockAPIResponse.rules);
	});
});
