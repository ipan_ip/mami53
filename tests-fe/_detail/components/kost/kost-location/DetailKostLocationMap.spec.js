import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailKostLocationMap from 'Js/_detail/components/kost/kost-location/DetailKostLocationMap.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

import detailStore from '../../__mocks__/detailStore';

window.Vue = require('vue');

const scrollWheelZoomFn = jest.fn();

global.osmHost = 'https://osm.mamikos.com';
global.tracker = jest.fn();
global.L = {
	tileLayer: jest.fn(() => ({
		addTo: jest.fn()
	})),
	control: {
		zoom: jest.fn().mockReturnValue({
			addTo: jest.fn()
		})
	},
	divIcon: jest.fn(),
	map: jest.fn().mockReturnValue({
		attributionControl: {
			addAttribution: jest.fn()
		},
		scrollWheelZoom: {
			disable: scrollWheelZoomFn
		}
	}),
	icon: jest.fn(),
	circleMarker: jest.fn(),
	marker: jest.fn().mockReturnValue({
		icon: jest.fn(),
		addTo: jest.fn()
	}),
	layerGroup: jest.fn().mockReturnValue({
		addTo: jest.fn()
	})
};

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);
const initMapObjectFn = jest.spyOn(
	DetailKostLocationMap.methods,
	'initMapObject'
);
const initMapLayerFn = jest.spyOn(
	DetailKostLocationMap.methods,
	'initMapLayer'
);
const initCustomMarkerFn = jest.spyOn(
	DetailKostLocationMap.methods,
	'initCustomMarker'
);
const initMapCustomControlSpy = jest.spyOn(
	DetailKostLocationMap.methods,
	'initMapCustomControl'
);

describe('DetailKostLocationMap.vue', () => {
	let wrapper = shallowMount(DetailKostLocationMap, {
		localVue,
		store,
		propsData: {
			mapId: 'DetailKostLocationMap'
		}
	});

	it('should render map', async () => {
		wrapper.vm.initMap();
		expect(wrapper.find('.map-container').exists()).toBeTruthy();
	});

	it('should fill latLng data with longitude and latitude', () => {
		expect(wrapper.vm.latLng).toMatchObject({
			lat: wrapper.vm.latitude || null,
			lng: wrapper.vm.longitude || null
		});
	});

	it('should call methoda for map initiation', () => {
		expect(initMapObjectFn).toBeCalled();
		expect(initMapLayerFn).toBeCalled();
		expect(initCustomMarkerFn).toBeCalled();
	});

	it('should call L map method and pass correct property', () => {
		expect(global.L.map).toBeCalledWith('DetailKostLocationMap', {
			attributionControl: false,
			center: wrapper.vm.latLng,
			zoom: 14,
			minZoom: 10,
			maxZoom: 18,
			zoomControl: false
		});
	});

	it('should call L tileLayer method and pass image link', () => {
		wrapper.vm.initMapLayer();

		wrapper.vm.$nextTick(() => {
			expect(global.L.tileLayer).toBeCalledWith(
				`${global.osmHost}/hot/{z}/{x}/{y}.png`
			);
		});
	});

	it('should call L layerGroup method and pass custom icon', async () => {
		expect(initCustomMarkerFn).toBeCalled();
		expect(global.L.divIcon).toBeCalled();

		wrapper.vm.$refs.marker = {
			$el: null
		};

		await wrapper.vm.$nextTick();

		wrapper.vm.initCustomMarker();

		expect(global.L.icon).toBeCalledWith({
			iconUrl: '/general/img/icons/ic_owner_placeholder.png',
			iconSize: [48, 48],
			className: 'marker-unclickable'
		});

		expect(global.L.circleMarker).toBeCalledWith(
			{ lat: -6.3042925, lng: 106.816102 },
			{
				stroke: false,
				fillOpacity: 0.2,
				fillColor: 'rgb(0, 135, 66)',
				radius: 36,
				className: 'marker-unclickable'
			}
		);
	});

	it('should fill longitude data with data from store', () => {
		expect(wrapper.vm.longitude).toBe(detailStore.state.detail.location[0]);
	});

	it('should fill latitude data with data from store', () => {
		expect(wrapper.vm.latitude).toBe(detailStore.state.detail.location[1]);
	});

	it('Computed latLng should return null if no data available', async () => {
		await wrapper.vm.$store.commit('setDetail', { location: [0, 0] });

		expect(wrapper.vm.latLng).toEqual({ lat: null, lng: null });
	});

	it('Computed mapContainerRefs should return correctly', async () => {
		wrapper.vm.$refs.DetailKostLocationMap = {
			style: {
				filter: 'none'
			}
		};

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.mapContainerRefs).toEqual({
			style: {
				filter: 'none'
			}
		});
	});

	it('Method initMapCustomControl should disable the scroll wheel on mobile', async () => {
		wrapper.setProps({ isMobile: true });
		wrapper.vm.initMapCustomControl();

		expect(initMapCustomControlSpy).toBeCalled();
		expect(scrollWheelZoomFn).toBeCalled();
	});

	it('should call L icon with mobile view', async () => {
		wrapper = shallowMount(DetailKostLocationMap, {
			localVue,
			store,
			propsData: {
				mapId: 'DetailKostLocationMap',
				isMobile: true,
				isOverlay: true
			}
		});

		wrapper.setProps({ isOverlay: false });
		wrapper.vm.$refs.marker = {
			$el: null
		};
		wrapper.vm.$store.commit('setExtras', true);

		wrapper.vm.initCustomMarker();
		await wrapper.vm.$nextTick();

		expect(global.L.icon).toBeCalledWith({
			iconUrl: '/general/img/icons/ic_owner_placeholder.png',
			iconSize: [32, 32],
			className: 'marker-unclickable'
		});

		expect(global.L.circleMarker).toBeCalledWith(
			{ lat: -6.3042925, lng: 106.816102 },
			{
				stroke: false,
				fillOpacity: 0.2,
				fillColor: 'rgb(0, 135, 66)',
				radius: 36,
				className: 'marker-unclickable'
			}
		);

		wrapper.vm.$refs.marker = {
			$el: '<div />'
		};
		wrapper.setProps({ isOverlay: true });
		wrapper.vm.initCustomMarker();

		expect(global.L.divIcon).toBeCalled();
	});
});
