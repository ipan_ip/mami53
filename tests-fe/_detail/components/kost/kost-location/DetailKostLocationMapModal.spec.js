import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostLocationMapModal from 'Js/_detail/components/kost/kost-location/DetailKostLocationMapModal.vue';
import Vuex from 'vuex';

import detailStore from '../../__mocks__/detailStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

describe('DetailKostLocationMapModal.vue', () => {
	const wrapper = shallowMount(DetailKostLocationMapModal, {
		localVue,
		store,
		propsData: {
			showModalMap: true
		},
		mocks: {
			$emit: jest.fn()
		}
	});

	it('Should emit close-modal when modal is closed', () => {
		wrapper.vm.closeModal();

		expect(wrapper.emitted('close-modal')).toBeTruthy();
	});

	it('Should emit ask-address when modal is askAddressButton clicked', () => {
		wrapper.vm.askAddress();

		expect(wrapper.emitted('ask-address')).toBeTruthy();
	});
});
