import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailKostLocation from 'Js/_detail/components/kost/kost-location/DetailKostLocation.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

import detailStore from '../../__mocks__/detailStore';

window.Vue = require('vue');
const mixinDetailType = require('Js/_detail/mixins/MixinDetailType');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

global.tracker = jest.fn();

mockWindowProperty('innerWidth', 700);

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin([mixinDetailType, mixinNavigator]);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const sendMapTrackerFn = jest.spyOn(
	DetailKostLocation.methods,
	'sendMapTracker'
);

describe('DetailKostLocation.vue', () => {
	const wrapper = shallowMount(DetailKostLocation, {
		localVue,
		store
	});

	it('should render map', () => {
		expect(wrapper.find('.kost-location__map').exists()).toBeTruthy();
	});

	it('should fill authCheck data with data from store', () => {
		expect(wrapper.vm.authCheck).toBe(detailStore.state.authCheck);
	});

	it('should fill isLimitedContent data with longitude and latitude', () => {
		expect(wrapper.vm.isLimitedContent).toBe(
			detailStore.state.extras.isLimitedContent
		);
	});

	it('should call EventBus emit sendPretextChat', () => {
		wrapper.vm.openModalChat();

		expect(global.EventBus.$emit).toBeCalledWith('sendPretextChat', {
			string: 'Tanya alamat ?',
			for: 'kos'
		});

		expect(wrapper.vm.isModalMapVisible).toBeFalsy();
	});

	it('should update isModalLoginVisible  to true', () => {
		wrapper.vm.openModalLogin();

		expect(wrapper.vm.isModalLoginVisible).toBeTruthy();
		expect(sendMapTrackerFn).toHaveBeenCalled();
	});

	it('should update isModalLoginVisible  to false', () => {
		wrapper.vm.closeModalLogin();

		expect(wrapper.vm.isModalLoginVisible).toBeFalsy();
	});

	it('should update isModalMapVisible  to true', () => {
		wrapper.vm.openModalMap();

		expect(wrapper.vm.isModalMapVisible).toBeTruthy();
	});

	it('should update isModalMapVisible  to false', () => {
		wrapper.vm.closeModalMap();

		expect(wrapper.vm.isModalMapVisible).toBeFalsy();
	});

	it('should send correct tracking property', () => {
		wrapper.vm.sendMapTracker();

		expect(global.tracker).toBeCalledWith('moe', [
			'[User] Detail Property - Click Lihat Peta',
			{
				is_limitedcontent: wrapper.vm.isLimitedContent,
				is_premium: detailStore.state.detail.is_premium_owner || false,
				is_promoted: detailStore.state.detail.is_promoted || false,
				is_booking: detailStore.state.detail.is_booking || false,
				is_login: wrapper.vm.authCheck.all || null,
				property_type: wrapper.vm.detailTypeName || null,
				property_id: detailStore.state.detail.seq || null,
				property_city: detailStore.state.detail.area_city,
				property_name: detailStore.state.detail.room_title || null
			}
		]);
	});
});
