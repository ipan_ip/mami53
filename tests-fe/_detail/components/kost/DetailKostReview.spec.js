import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import DetailKostReview from 'Js/_detail/components/kost/DetailKostReview.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import reviewData from '../__mocks__/reviewData';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import detailStore from '../__mocks__/detailStore';
window.Vue = require('vue');

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(MixinGetQueryString);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const initReviewSpy = jest.spyOn(DetailKostReview.methods, 'initReview');
const checkIfReviewSpy = jest.spyOn(DetailKostReview.methods, 'checkIfReview');
const isInViewportSpy = jest.spyOn(DetailKostReview.methods, 'isInViewport');
const getReviewsSpy = jest.spyOn(DetailKostReview.methods, 'getReviews');
const shareMyReviewSpy = jest.spyOn(DetailKostReview.methods, 'shareMyReview');
const openViewImageSpy = jest.spyOn(DetailKostReview.methods, 'openViewImage');
const closeViewImageSpy = jest.spyOn(
	DetailKostReview.methods,
	'closeViewImage'
);
const closeReplyReviewSpy = jest.spyOn(
	DetailKostReview.methods,
	'closeReplyReview'
);
const openReplyReviewSpy = jest.spyOn(
	DetailKostReview.methods,
	'openReplyReview'
);
const pageChangedSpy = jest.spyOn(DetailKostReview.methods, 'pageChanged');
const getMoreReviewsSpy = jest.spyOn(
	DetailKostReview.methods,
	'getMoreReviews'
);
const scrollIntoViewSpy = jest.fn();
const fbSpy = jest.fn();

jest.useFakeTimers();
Element.prototype.scrollIntoView = scrollIntoViewSpy;
const axios = {
	success: jest.fn(() => {
		return Promise.resolve({
			data: reviewData
		});
	}),
	// eslint-disable-next-line prefer-promise-reject-errors
	error: () => Promise.reject('error')
};
mockWindowProperty('fbAsyncInit ', fbSpy);
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('FB', {
	ui: jest.fn()
});

const detailKostReviewRepliesStub = {
	template: `<button class="close-review" @click="$emit('close')"/>`
};

const detailPhotoCarouselStub = {
	template: `<button class="close-gallery" @click="$emit('close-carousel')"/>`
};

const paginateStub = {
	template: `<button class="update-page" @click="clickHandler(2)"></button>`,
	props: ['clickHandler']
};

const transitionStub = () => ({
	render: function(h) {
		return this.$options._renderChildren;
	}
});

const stubs = {
	'detail-kost-review-all-replies': detailKostReviewRepliesStub,
	'detail-photo-carousel': detailPhotoCarouselStub,
	paginate: paginateStub,
	transition: transitionStub()
};

describe('DetailKostReview.vue', () => {
	let wrapper = shallowMount(DetailKostReview, {
		localVue,
		store,
		stubs
	});

	global.axios = { get: axios.success };
	it('Computed authCheck should return correctly', () => {
		expect(wrapper.vm.authCheck).toEqual(detailStore.state.authCheck);
	});

	it('Computed seq should return correctly', () => {
		expect(wrapper.vm.seq).toEqual(detailStore.state.detail.seq);
	});

	it('Computed rating should return correctly', () => {
		expect(wrapper.vm.rating).toEqual('4.0');
	});

	it('Computed totalPage should return correctly', async () => {
		await wrapper.setData({
			reviewData: {
				total: 0,
				limit: 5,
				review: []
			}
		});

		expect(wrapper.vm.totalPage).toEqual(0);
	});

	it('Should call all nessecary method on mounted', () => {
		expect(initReviewSpy).toBeCalled();
		expect(checkIfReviewSpy).toBeCalled();
	});

	it('Should go to kostReviewSection directly when have params', () => {
		wrapper = shallowMount(DetailKostReview, {
			localVue,
			store,
			methods: {
				getQueryString() {
					return 'true';
				}
			}
		});

		expect(checkIfReviewSpy).toBeCalled();
		jest.runAllTimers();
		expect(scrollIntoViewSpy).toBeCalled();
	});

	it('Should add event scroll and handle it correctly', () => {
		const testCase = [
			{
				reviewedCalled: true,
				expected: false,
				response: null
			},
			{
				reviewedCalled: false,
				expected: true,
				response: null
			},
			{
				reviewedCalled: false,
				expected: true,
				response: reviewData
			}
		];

		for (const test of testCase) {
			const windowEvent = {};
			window.addEventListener = jest.fn((event, cb) => {
				windowEvent[event] = cb;
			});

			makeAPICall.mockImplementation(() => test.response);

			wrapper = shallowMount(DetailKostReview, {
				localVue,
				store,
				data() {
					return {
						reviewedCalled: test.reviewedCalled
					};
				},
				stubs
			});

			expect(initReviewSpy).toBeCalled();

			windowEvent.scroll({ scrollTo: 1000 });

			!test.reviewedCalled && expect(isInViewportSpy).toBeCalled();
		}
	});

	it('Should call method shareMyReview correctly', async () => {
		await wrapper.setData({ reviewData, isLoading: false });
		const shareFBElement = wrapper.find('button.btn-fb-share');

		expect(shareFBElement.exists()).toBe(true);

		await shareFBElement.trigger('click');

		expect(shareMyReviewSpy).toBeCalled();
	});

	it('Should toggle method Reply Review correctly', async () => {
		await wrapper.setData({ reviewData, isLoading: false });
		const openElement = wrapper
			.findAll('span')
			.filter(node => node.text() === 'Lihat Semua Balasan');

		expect(openElement.exists()).toBe(true);

		await openElement.trigger('click');

		expect(openReplyReviewSpy).toBeCalled();
		expect(wrapper.vm.isReplyReview).toBe(true);

		const closeElement = wrapper.find('button.close-review');
		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeReplyReviewSpy).toBeCalled();
		expect(wrapper.vm.isReplyReview).toBe(false);
	});

	it('Should toggle method Open Image correctly', async () => {
		await wrapper.setData({ reviewData, isLoading: false });
		const openElement = wrapper.find('img.review-image');

		expect(openElement.exists()).toBe(true);

		await openElement.trigger('click');

		expect(openViewImageSpy).toBeCalled();
		expect(wrapper.vm.isViewImage).toBe(true);

		const closeElement = wrapper.find('button.close-gallery');
		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeViewImageSpy).toBeCalled();
		expect(wrapper.vm.isViewImage).toBe(false);
	});

	it('Should call method pageChanged correctly', async () => {
		await wrapper.setData({ reviewData, isLoading: false });
		const paginationElement = wrapper.find('button.update-page');

		expect(paginationElement.exists()).toBe(true);

		await paginationElement.trigger('click');

		expect(pageChangedSpy).toBeCalled();
	});

	it('Should call method getMoreReviews correctly', async () => {
		global.axios = { get: axios.error };
		wrapper = shallowMount(DetailKostReview, {
			localVue,
			store,
			stubs
		});

		await wrapper.setData({ reviewData, isLoading: false });
		const selectElement = wrapper.find('select.review-sort');

		expect(selectElement.exists()).toBe(true);

		const optionsElement = wrapper.findAll('select.review-sort > option');

		expect(optionsElement.length).toBe(4);

		optionsElement.at(2).element.selected = true;
		selectElement.trigger('change');

		expect(getMoreReviewsSpy).toBeCalled();
	});
});
