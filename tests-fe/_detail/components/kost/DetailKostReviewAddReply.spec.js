import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import '@babel/polyfill';
import DetailKostReviewAddReply from 'Js/_detail/components/kost/DetailKostReviewAddReply';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
describe('DetailKostReviewAddReply.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	const localVue = createLocalVue();
	localVue.mixin([MixinSwalLoading, MixinSwalSuccessError]);
	localVue.use(Vuex);
	const store = new Vuex.Store({
		state: {
			token: ''
		}
	});
	global.axios = mockAxios;
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(DetailKostReviewAddReply, {
			propsData: {
				replyInfo: {}
			},
			localVue,
			store
		});
	});

	it('Should update isReply when call toggleReply method on click span', async () => {
		expect(wrapper.find('span.--green').exists()).toBe(true);

		wrapper.find('span.--green').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isReply).toBe(true);
	});

	it('Should call sendReply method on click submit button and return error if blank', async () => {
		expect(wrapper.find('button.btn-success').exists()).toBe(true);

		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setMethods({ swalError: swalErrorSpy });

		wrapper.find('button.btn-success').trigger('click');
		await wrapper.vm.$nextTick();

		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Anda tidak bisa menulis balasan kosong'
		);
	});

	it('Should call sendReply method on click submit button and return error if value length is least than 10', async () => {
		expect(wrapper.find('button.btn-success').exists()).toBe(true);

		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setData({ replyReview: 'Bagus' });
		wrapper.setMethods({ swalError: swalErrorSpy });

		wrapper.find('button.btn-success').trigger('click');
		await wrapper.vm.$nextTick();

		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Anda tidak bisa menulis balasan kurang dari 10 karakter.'
		);
	});

	it('Should call sendReply method on click submit button and return success', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };
		axios.mockResolve({
			data: {
				status: 200
			}
		});
		expect(wrapper.find('button.btn-success').exists()).toBe(true);

		const sendReplySpy = jest.spyOn(wrapper.vm, 'sendReply');
		wrapper.setData({ replyReview: 'Kostnya Bagus' });
		wrapper.setMethods({ sendReply: sendReplySpy });
		await wrapper.vm.$nextTick();
		wrapper.find('button.btn-success').trigger('click');
		await wrapper.vm.$nextTick();

		expect(sendReplySpy).toBeCalled();

		assignMock.mockClear();
	});

	it('Should call sendReply method on click submit button and return success with additional error', async () => {
		axios.mockResolve({
			data: {
				status: false,
				meta: {
					message: 'error'
				}
			}
		});
		expect(wrapper.find('button.btn-success').exists()).toBe(true);

		const sendReplySpy = jest.spyOn(wrapper.vm, 'sendReply');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setData({ replyReview: 'Kostnya Bagus' });
		wrapper.setMethods({ sendReply: sendReplySpy, swalError: swalErrorSpy });
		await wrapper.vm.$nextTick();
		wrapper.find('button.btn-success').trigger('click');
		await wrapper.vm.$nextTick();

		expect(sendReplySpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(null, 'error');
	});

	it('Should call sendReply method on click submit button and return error', async () => {
		axios.mockReject('error');
		expect(wrapper.find('button.btn-success').exists()).toBe(true);

		const sendReplySpy = jest.spyOn(wrapper.vm, 'sendReply');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setData({ replyReview: 'Kostnya Bagus' });
		wrapper.setMethods({ sendReply: sendReplySpy, swalError: swalErrorSpy });
		await wrapper.vm.$nextTick();
		wrapper.find('button.btn-success').trigger('click');
		await wrapper.vm.$nextTick();

		expect(sendReplySpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Terjadi galat. Silakan coba lagi. (sendReply, DetailKostReviewAddReply)'
		);
	});
});
