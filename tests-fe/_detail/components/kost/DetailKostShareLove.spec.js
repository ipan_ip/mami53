import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import 'tests-fe/utils/mock-vue';
import DetailKostShareLove from 'Js/_detail/components/kost/DetailKostShareLove.vue';
import { dropdown } from 'vue-strap';
import detailStore from '../__mocks__/detailStore';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import EventBus from 'Js/_detail/event-bus';

global.axios = mockAxios;
window.swal = require('sweetalert2/dist/sweetalert2.js');

const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
const localVue = createLocalVue();
localVue.mixin([MixinSwalLoading, MixinSwalSuccessError]);
mockVLazy(localVue);
localVue.use(Vuex);
const store = new Vuex.Store(detailStore);
const notify = jest.fn();
mockWindowProperty('bugsnagClient', {
	notify
});
mockWindowProperty('tracker', jest.fn());

const assignLoveCountSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'assignLoveCount'
);
const checkLoveByMeSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'checkLoveByMe'
);
const assignUrlShareSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'assignUrlShare'
);
const checkNativeShareSupportSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'checkNativeShareSupport'
);
const toggleLoveSpy = jest.spyOn(DetailKostShareLove.methods, 'toggleLove');
const shareThisPageSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'shareThisPage'
);
const moengageEventTrackShareSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'moengageEventTrackShare'
);
const toggleLovePostSpy = jest.spyOn(
	DetailKostShareLove.methods,
	'toggleLovePost'
);

const mamiShareStub = {
	template: `<button class="event-tracker" @click="$emit('track')"></button>`
};
const componentToBeStub = {
	MamiShare: mamiShareStub,
	dropdown
};

describe('DetailKostShareLove.vue', () => {
	let wrapper = shallowMount(DetailKostShareLove, {
		localVue,
		store,
		stubs: componentToBeStub,
		mocks: {
			$toasted: { show: jest.fn() }
		}
	});

	it('computed token should return correctly', () => {
		expect(wrapper.vm.token).toBe(wrapper.vm.$store.state.token);
	});

	it('computed authData should return correctly', () => {
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('computed authCheck should return correctly', () => {
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('computed seq should return correctly', () => {
		expect(wrapper.vm.seq).toBe(wrapper.vm.$store.state.detail._id);
	});

	it('computed loveCount should return correctly', () => {
		expect(wrapper.vm.love_count).toBe(wrapper.vm.$store.state.love_count);
	});

	it('computed loveByMe should return correctly', () => {
		expect(wrapper.vm.loveByMe).toBe(wrapper.vm.$store.state.detail.love_by_me);
	});

	it('computed slug should return correctly', () => {
		expect(wrapper.vm.slug).toBe(wrapper.vm.$store.state.detail.slug);
	});

	it('computed detailTypeName should return correctly', () => {
		expect(wrapper.vm.detailTypeName).toBe(
			wrapper.vm.$store.state.detailTypeName
		);
	});

	it('computed detail should return correctly', () => {
		expect(wrapper.vm.detail).toEqual(wrapper.vm.$store.state.detail);
	});

	it('computed isOwner should return correctly', () => {
		expect(wrapper.vm.isOwner).toBe(detailStore.state.authCheck.owner);
	});

	it('computed isKost should return correctly', () => {
		expect(wrapper.vm.isKost).toBe(true);
	});

	it('should call method assignLoveCount on created', async () => {
		expect(assignLoveCountSpy).toHaveBeenCalled();
		expect(wrapper.vm.displayLoveCount).toBe(266);
	});

	it('should call method checkLoveByMe on created', () => {
		expect(checkLoveByMeSpy).toHaveBeenCalled();
		expect(wrapper.vm.isLove).toBe(false);
	});

	it('should call method assignUrlShare on created', () => {
		expect(assignUrlShareSpy).toHaveBeenCalled();
		expect(wrapper.vm.urlShare).toBe(
			'https://localhost/room/kost-jakarta-selatan-kost-campur-eksklusif-kost-rumah-kamang-residence-ragunan-pasar-minggu-jakarta-selatan-1'
		);
	});

	it('should call method checkNativeShareSupport on mounted with navigator exist', () => {
		mockWindowProperty('navigator', {
			share: jest.fn()
		});

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub
		});

		expect(checkNativeShareSupportSpy).toBeCalled();
		expect(wrapper.vm.isSupportNativeShare).toBe(true);
	});

	it('should call method shareThisPage on clicking share button', async () => {
		const shareElement = wrapper
			.findAll('button')
			.filter(node => node.text() === 'Bagikan');

		expect(shareElement.exists()).toBe(true);

		await shareElement.trigger('click');

		expect(shareThisPageSpy).toBeCalled();
	});

	it('should call method checkNativeShareSupport on mounted without navigator', () => {
		mockWindowProperty('navigator', {});

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub
		});

		expect(checkNativeShareSupportSpy).toBeCalled();
		expect(wrapper.vm.isSupportNativeShare).toBe(false);
	});

	it('should call method moengageEventTrackShare correctly', async () => {
		expect(wrapper.vm.isSupportNativeShare).toBe(false);

		const trackerElement = wrapper.find('button.event-tracker');

		expect(trackerElement.exists()).toBe(true);

		await trackerElement.trigger('click');

		expect(moengageEventTrackShareSpy).toBeCalled();
	});

	it('should call method toggleLovePost with success love status correctly', async () => {
		mockAxios.mockResolve({
			data: {
				status: true,
				success: 'love'
			}
		});

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub,
			mocks: {
				$toasted: { show: jest.fn() }
			}
		});

		const loveElement = wrapper
			.findAll('button')
			.filter(node => node.text() === '266 Disimpan');

		expect(loveElement.exists()).toBe(true);

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect(wrapper.vm.isLove).toBe(true);
		expect(tracker).toBeCalled();
		expect(wrapper.vm.$toasted.show).toBeCalledWith('Sukses tersimpan', {
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('should call method toggleLovePost with success unlove status correctly', async () => {
		mockAxios.mockResolve({
			data: {
				status: true,
				success: 'unlove'
			}
		});

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub,
			mocks: {
				$toasted: { show: jest.fn() }
			}
		});

		const loveElement = wrapper
			.findAll('button')
			.filter(node => node.text() === '266 Disimpan');

		expect(loveElement.exists()).toBe(true);

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect(wrapper.vm.isLove).toBe(false);
	});

	it('should call method toggleLovePost with fail status correctly', async () => {
		mockAxios.mockResolve({
			data: {
				status: false,
				success: 'unlove'
			}
		});

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub,
			mocks: {
				$toasted: { show: jest.fn() }
			}
		});

		const loveElement = wrapper
			.findAll('button')
			.filter(node => node.text() === '266 Disimpan');

		expect(loveElement.exists()).toBe(true);

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect(wrapper.vm.$toasted.show).toBeCalledWith(
			'Gagal, silahkan coba lagi',
			{
				position: 'bottom-center',
				duration: 2000
			}
		);
	});

	it('should call method toggleLovePost with fail api call correctly', async () => {
		mockAxios.mockReject('error');

		wrapper = shallowMount(DetailKostShareLove, {
			localVue,
			store,
			stubs: componentToBeStub,
			mocks: {
				$toasted: { show: jest.fn() }
			}
		});

		const loveElement = wrapper
			.findAll('button')
			.filter(node => node.text() === '266 Disimpan');

		expect(loveElement.exists()).toBe(true);

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();

		await wrapper.vm.$nextTick();

		expect(bugsnagClient.notify).toBeCalled();
		expect(wrapper.vm.$toasted.show).toBeCalledWith(
			'Gagal, silahkan coba lagi',
			{
				position: 'bottom-center',
				duration: 2000
			}
		);
	});

	it('should call method toggleLove condition correctly', async () => {
		EventBus.$emit = jest.fn();
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		const loveElement = wrapper
			.findAll('button')
			.filter(node => node.text() === '266 Disimpan');

		expect(loveElement.exists()).toBe(true);

		wrapper.vm.$store.commit('setAuthCheck', {
			all: true,
			user: true,
			owner: false,
			admin: false
		});

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();

		wrapper.vm.$store.commit('setAuthCheck', {
			all: false,
			user: true,
			owner: false,
			admin: false
		});

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('openModalLogin');

		wrapper.vm.$store.commit('setAuthCheck', {
			all: true,
			user: false,
			owner: true,
			admin: false
		});

		await loveElement.trigger('click');

		expect(toggleLoveSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
		);
	});
});
