import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import '@babel/polyfill';
import DetailKostReviewAllReplies from 'Js/_detail/components/kost/DetailKostReviewAllReplies';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
describe('DetailKostReviewAllReplies.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.mixin([MixinSwalLoading, MixinSwalSuccessError]);
	global.axios = mockAxios;
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(DetailKostReviewAllReplies, {
			localVue,
			propsData: {
				reviewerData: {
					name: 'Tester',
					rating: 5,
					is_me: true,
					status: 'live',
					content: 'kostnya bagus',
					photo: [],
					reply_owner: [],
					can_reply: false
				}
			}
		});
	});

	it('Should call getReplies method on mounted and return success', async () => {
		axios.mockResolve({
			data: {
				status: 200,
				data: {
					reply: [
						{
							is_show: true,
							name: 'Test',
							time: '16/04/2020',
							content: 'kostnya nyaman'
						}
					]
				}
			}
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.findAll('div.review-content__title').length).toEqual(1);
	});

	it('Should call getReplies method on mounted and return error', async () => {
		axios.mockReject('error');
		const swalError = jest.fn();
		wrapper = shallowMount(DetailKostReviewAllReplies, {
			localVue,
			propsData: {
				reviewerData: {
					name: 'Tester',
					rating: 5,
					is_me: true,
					status: 'live',
					content: 'kostnya bagus',
					photo: [],
					reply_owner: [],
					can_reply: false
				}
			},
			methods: {
				swalError
			}
		});
		await wrapper.vm.$nextTick();

		expect(swalError).toHaveBeenCalledTimes(1);
	});

	it('Should emmited close method on click close button', async () => {
		expect(wrapper.find('a.btn-close-filter').exists()).toBe(true);
		wrapper.find('a.btn-close-filter').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.emitted().close).toBeTruthy();
	});
});
