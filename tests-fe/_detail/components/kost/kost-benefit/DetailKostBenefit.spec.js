import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailKostBenefit from 'Js/_detail/components/kost/kost-benefit/DetailKostBenefit.vue';
import Vuex from 'vuex';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

import detailStore from '../../__mocks__/detailStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);

const mockAPIResponse = {
	benefits: [
		{
			description: 'Lorem Ipsum Dolor Sit Amet',
			icon_small_url:
				'https://static8.kerupux.com/uploads/benefits/hbW94kgG.png',
			icon_url: 'https://static8.kerupux.com/uploads/benefits/0rAzuYK3.png',
			id: 99,
			title: '35 Transaksi Booking Berhasil'
		},
		{
			description:
				'Uang dapat direfund jika kamu ingin membatalkan penyewaan kamar',
			icon_small_url:
				'https://static8.kerupux.com/uploads/benefits/hbW94kgG.png',
			icon_url: 'https://static8.kerupux.com/uploads/benefits/0rAzuYK3.png',
			id: 14,
			title: 'Free of Charge'
		}
	]
};

makeAPICall.mockImplementation(() => {
	return mockAPIResponse;
});

describe('DetailKostBenefit.vue', () => {
	const wrapper = shallowMount(DetailKostBenefit, { localVue, store });

	it('Should call handleFetchBenefit method correctly', async () => {
		wrapper.setData({
			isRendered: true,
			kosAndalanBenefit: []
		});

		wrapper.vm.$store.commit('setAbTestCheckedStatus', true);
		wrapper.vm.$store.commit('setIdKost', null);
		await wrapper.vm.handleFetchBenefit();

		expect(wrapper.vm.kosAndalanBenefit).toEqual([]);

		wrapper.vm.$store.commit('setIdKost', 123456);
		await wrapper.vm.handleFetchBenefit();

		expect(wrapper.vm.kosAndalanBenefit).toEqual(mockAPIResponse.benefits);
	});
});
