import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailKostBenefitSkeleton from 'Js/_detail/components/kost/kost-benefit/DetailKostBenefitSkeleton.vue';

const localVue = createLocalVue();

describe('DetailKostBenefitSkeleton', () => {
	const wrapper = shallowMount(DetailKostBenefitSkeleton, { localVue });

	it('should render detail kost benefit skeleton', () => {
		expect(wrapper.find('.kost-benefit-placeholder').exists()).toBe(true);
	});
});
