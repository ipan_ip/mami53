import 'tests-fe/utils/mock-vue';
import DetailKostSwiperGalleryModal from 'Js/_detail/components/kost/DetailKostSwiperGalleryModal.vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { modal } from 'vue-strap';
import { swiper, swiperSlide } from 'vue-awesome-swiper';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);

const handleCloseGalleryModalSpy = jest.spyOn(
	DetailKostSwiperGalleryModal.methods,
	'handleCloseGalleryModal'
);
const handleUpdateGallerySpy = jest.spyOn(
	DetailKostSwiperGalleryModal.methods,
	'handleUpdateGallery'
);
const handleUpdateSwiperComponentSpy = jest.spyOn(
	DetailKostSwiperGalleryModal.methods,
	'handleUpdateSwiperComponent'
);
const handleMoEngageTrackerSpy = jest.spyOn(
	DetailKostSwiperGalleryModal.methods,
	'handleMoEngageTracker'
);

const stubs = {
	modal,
	swiper,
	swiperSlide
};

global.tracker = jest.fn();
global.authCheck = {
	owner: false,
	user: true,
	all: true
};
global.authData = {
	all: {
		id: 1
	}
};

describe('DetailKostSwiperGalleryModal.vue', () => {
	let wrapper = shallowMount(DetailKostSwiperGalleryModal, {
		stubs,
		propsData: {
			images: [
				{
					large: '/general/img/test1.jpg',
					medium: '/general/img/test1.jpg'
				}
			],
			isSingle: true,
			isMobileGallery: true
		}
	});

	it('Should call method handleCloseGalleryModal on click close element', () => {
		const closeElement = wrapper.find('span.kost-gallery-modal-header__close');

		expect(closeElement.exists()).toBe(true);

		closeElement.trigger('click');

		expect(handleCloseGalleryModalSpy).toBeCalled();
	});

	it('Should call method handleUpdateGallery on clicking the image inside the slider', () => {
		const imageElement = wrapper.find(
			'img.kost-gallery-modal-content__thumbnails-img'
		);

		expect(imageElement.exists()).toBe(true);

		imageElement.trigger('click');

		expect(handleUpdateGallerySpy).toBeCalled();
	});

	it('Watch isShow should called correctly, when the initial slide is change', async () => {
		await wrapper.setProps({
			swiperInitialSlide: 1,
			isSingle: false,
			isMobileGallery: false,
			isThumbnailCentered: true,
			isShow: true
		});

		expect(handleMoEngageTrackerSpy).toBeCalled();
	});

	it('Should call method handleUpdateSwiperComponent on modal close', async () => {
		await wrapper.setProps({ swiperInitialSlide: 1, isShow: true });

		const modalStub = {
			template: `<button class="close-modal" @click="$emit('closed')" />`
		};

		wrapper = shallowMount(DetailKostSwiperGalleryModal, {
			stubs: {
				...stubs,
				modal: modalStub
			},
			propsData: {
				images: [
					{
						large: '/general/img/test1.jpg',
						medium: '/general/img/test1.jpg'
					}
				],
				isSingle: true,
				isMobileGallery: true
			}
		});

		const closeElement = wrapper.find('button.close-modal');

		expect(closeElement.exists()).toBe(true);

		closeElement.trigger('click');

		expect(handleUpdateSwiperComponentSpy).toBeCalled();

		await wrapper.setProps({ isShow: false });
	});
});
