import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostBookingRentType from 'Js/_detail/components/kost/booking-card/DetailKostBookingRentType.vue';
import kostPrice from '../../__mocks__/kostPrices.json';

const localVue = createLocalVue();

describe('DetailKostBookingRentType.vue', () => {
	const wrapper = shallowMount(DetailKostBookingRentType, {
		localVue,
		propsData: {
			options: kostPrice,
			value: 'monthly'
		}
	});

	it('should render component properly', () => {
		expect(wrapper.find('.booking-rent-type').exists()).toBe(true);
		expect(wrapper.findAll('.booking-rent-type__options-item').length).toBe(
			Object.keys(kostPrice).length
		);
	});

	it('should toggle open option when clicked', async () => {
		await wrapper.setData({ isShowRentTypeOptions: false });
		await wrapper.find('.booking-rent-type').trigger('click');

		expect(wrapper.vm.isShowRentTypeOptions).toBe(true);
		expect(wrapper.find('.booking-rent-type').classes('--open')).toBe(true);

		wrapper.vm.handleClickOutside();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowRentTypeOptions).toBe(false);
		expect(wrapper.find('.booking-rent-type').classes('--open')).toBe(false);
	});

	it('should emit input when rent type selected', () => {
		wrapper.vm.selectedRentType = 'quarterly';

		expect(wrapper.emitted('input')[0][0]).toBe('quarterly');
		expect(wrapper.vm.isShowRentTypeOptions).toBe(false);
	});

	it('should handle open from outside properly', async () => {
		wrapper.vm.showRentTypeOptionsFromOutside();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowRentTypeOptions).toBe(true);
		expect(wrapper.find('.booking-rent-type').classes('--open')).toBe(true);

		wrapper.vm.handleClickOutside();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isShowRentTypeOptions).toBe(true);
		expect(wrapper.find('.booking-rent-type').classes('--open')).toBe(true);
	});
});
