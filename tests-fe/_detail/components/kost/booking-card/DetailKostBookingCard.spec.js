import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostBookingCard from 'Js/_detail/components/kost/booking-card/DetailKostBookingCard.vue';
import Vuex from 'vuex';
import kostPrice from '../../__mocks__/kostPrices.json';

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const tracker = jest.fn();
global.tracker = tracker;

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailKostBookingCard.vue', () => {
	const wrapper = shallowMount(DetailKostBookingCard, {
		localVue,
		store: new Vuex.Store({
			state: {
				params: { rent_count_type: 'monthly', checkin: '' },
				authCheck: { all: true },
				detail: { is_booking: true, available_room: 2 },
				isCheckinDateFirst: true
			},
			getters: {
				availablePrices() {
					return kostPrice;
				}
			},
			mutations: {
				setRentCountType: jest.fn(),
				setIsOverrideDraftCheckin: jest.fn()
			}
		}),
		mocks: {
			$router: {
				currentRoute: {
					path: '/date'
				}
			}
		}
	});

	it('should render booking card', () => {
		expect(wrapper.find('.booking-card').exists()).toBe(true);
	});

	it('should show checkin datepicker if checkin is empty when booking button clicked', () => {
		wrapper.vm.$store.state.params.checkin = '';

		const openDatePickerFromOutside = jest.fn();
		const showRentTypeOptionsFromOutside = jest.fn();
		wrapper.vm.$refs = {
			bookingDatePicker: { openDatePickerFromOutside },
			bookingRentType: { showRentTypeOptionsFromOutside }
		};

		wrapper.vm.handleBookingButtonClicked();

		expect(openDatePickerFromOutside).toBeCalled();
	});

	it('should show rent type options if rent_count_type is empty when booking button clicked', () => {
		wrapper.vm.$store.state.params.checkin = '2020-02-02';
		wrapper.vm.$store.state.params.rent_count_type = '';

		const openDatePickerFromOutside = jest.fn();
		const showRentTypeOptionsFromOutside = jest.fn();
		wrapper.vm.$refs = {
			bookingDatePicker: { openDatePickerFromOutside },
			bookingRentType: { showRentTypeOptionsFromOutside }
		};

		wrapper.vm.handleBookingButtonClicked();

		expect(showRentTypeOptionsFromOutside).toBeCalled();
	});

	it('should emit openBookingKost when booking clicked and all is valid', () => {
		wrapper.vm.$store.state.params.checkin = '2020-02-02';
		wrapper.vm.$store.state.params.rent_count_type = 'monthly';

		wrapper.vm.handleBookingButtonClicked();
		expect(global.EventBus.$emit).toBeCalledWith(
			'openBookingKost',
			expect.any(Object)
		);
	});

	it('should handle date selected properly', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.handleOnDateSelected();

		expect(spyCommit).toBeCalledWith('setIsOverrideDraftCheckin', true);
		expect(tracker).toBeCalledWith('moe', [
			'[User] Booking - Click Booking on Datepicker',
			expect.any(Object)
		]);
		spyCommit.mockRestore();
	});

	it('should show after night validation when the booking time is unavailable', () => {
		wrapper.vm.nightValidation = true;

		expect(wrapper.vm.nightValidation).toBe(true);
		expect(tracker).toBeCalled();
	});

	it('should disable action button when after night validation is active', () => {
		wrapper.vm.nightValidation = true;
		expect(wrapper.vm.nightValidation).toBe(true);
		expect(wrapper.vm.isBookingDisabled).toBe(true);
	});
});
