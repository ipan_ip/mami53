import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostBookingDisclaimer from 'Js/_detail/components/kost/booking-card/DetailKostBookingDisclaimer.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DetailKostBookingDisclaimer.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetailKostBookingDisclaimer, {
			localVue,
			store: new Vuex.Store({
				state: {
					detail: { is_booking: true, min_month: 0 },
					params: { rent_count_type: 'monthly' }
				}
			})
		});
	});

	it('should render booking disclaimer properly when is not booking', async () => {
		wrapper.vm.$store.state.detail.is_booking = false;
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.detail-kost-booking-disclaimer').text()).toBe(
			'Kos ini belum bisa di-booking melalui Mamikos.'
		);
	});

	it('should render booking disclaimer properly when user select daily', async () => {
		wrapper.vm.$store.state.params.rent_count_type = 'daily';
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.detail-kost-booking-disclaimer').text()).toBe(
			'Saat ini, sewa harian belum bisa di-booking melalui Mamikos.'
		);
	});

	it('should render booking disclaimer properly when booking is true', async () => {
		wrapper.vm.$store.state.params.rent_count_type = 'monthly';
		wrapper.vm.$store.state.detail.min_month = 2;
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.detail-kost-booking-disclaimer').text()).toBe(
			'Peluang booking diterima lebih besar jika Anda menyewa minimal 2 bulan.'
		);
	});

	it('should not render booking disclaimer if nothing to inform', async () => {
		wrapper.vm.$store.state.params.rent_count_type = 'monthly';
		wrapper.vm.$store.state.detail.min_month = 0;
		wrapper.vm.$store.state.detail.is_booking = true;
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.detail-kost-booking-disclaimer').exists()).toBe(
			false
		);
	});
});
