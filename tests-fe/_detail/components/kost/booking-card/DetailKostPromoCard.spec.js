import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostPromoCard from 'Js/_detail/components/kost/booking-card/DetailKostPromoCard.vue';
import IconPromo from 'Js/@icons/Promo';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	IconPromo
};

describe('DetailKostPromoCard.vue', () => {
	let wrapper;

	const toggleSpy = jest.spyOn(DetailKostPromoCard.methods, 'toggle');

	const lineClampSpy = jest.spyOn(DetailKostPromoCard.methods, 'lineClamp');

	const truncateCharactersSpy = jest.spyOn(
		DetailKostPromoCard.methods,
		'truncateCharacters'
	);

	beforeEach(() => {
		wrapper = shallowMount(DetailKostPromoCard, {
			localVue,
			stubs
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.detail-kost-promo-card').exists()).toBe(true);
	});

	it('should call all method and update data properly when the component rendered', () => {
		expect(toggleSpy).toHaveBeenCalled();
	});

	it('should call toggle method properly', () => {
		const tempValue = wrapper.vm.isCollapse;
		wrapper.vm.toggle();

		expect(lineClampSpy).toHaveBeenCalled();
		expect(wrapper.vm.isCollapse).toBe(!tempValue);
	});

	it('should call lineClamp method properly when have no maxLines', async () => {
		const parentElement = document.createElement('div');
		const toggleElement = document.createElement('div');

		expect(
			await wrapper.vm.lineClamp(parentElement, 0, toggleElement)
		).toBeFalsy();
		expect(wrapper.vm.isCollapse).toBeFalsy();
	});

	it('should call lineClamp method properly when have maxLines AND scrollHeight more than maxHeight', async () => {
		const parentElement = document.createElement('div');
		const toggleElement = document.createElement('div');

		await wrapper.vm.lineClamp(parentElement, 3, toggleElement);
		expect(truncateCharactersSpy).toHaveBeenCalled();
	});

	it('should call lineClamp method properly when have maxLines AND scrollHeight less than maxHeight', async () => {
		mockWindowProperty(
			'getComputedStyle',
			jest.fn(param => {
				return {
					lineHeight: 10
				};
			})
		);

		const parentElement = document.createElement('div');
		const toggleElement = document.createElement('div');

		expect(
			await wrapper.vm.lineClamp(parentElement, 3, toggleElement)
		).toBeFalsy();
		expect(truncateCharactersSpy).toHaveBeenCalled();
	});

	it('should call truncateCharacters method properly when lenght content less than 1', () => {
		const parentElement = document.createElement('div');
		const toggleElement = document.createElement('div');

		expect(
			wrapper.vm.truncateCharacters(parentElement, 3, toggleElement)
		).toBeFalsy();
	});

	it('should call truncateCharacters method properly when lenght content more than 1', () => {
		const parentElement = document.createElement('div');
		const toggleElement = document.createElement('div');

		parentElement.textContent = 'mock content';

		expect(
			wrapper.vm.truncateCharacters(parentElement, 3, toggleElement)
		).toBeTruthy();
	});
});
