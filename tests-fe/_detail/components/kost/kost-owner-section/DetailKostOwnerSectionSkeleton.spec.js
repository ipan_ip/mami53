import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import DetailKostOwnerSectionSkeleton from 'Js/_detail/components/kost/kost-owner-section/DetailKostOwnerSectionSkeleton.vue';

const localVue = createLocalVue();

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
localVue.mixin(mixinNavigator);

describe('DetailKostOwnerSectionSkeleton', () => {
	const wrapper = shallowMount(DetailKostOwnerSectionSkeleton, { localVue });

	it('should render detail kost benefit skeleton', () => {
		expect(wrapper.find('.kost-owner-section-placeholder').exists()).toBe(true);
	});
});
