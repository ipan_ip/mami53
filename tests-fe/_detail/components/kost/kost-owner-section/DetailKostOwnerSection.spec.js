import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import DetailKostOwnerSection from 'Js/_detail/components/kost/kost-owner-section/DetailKostOwnerSection.vue';
import Vuex from 'vuex';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

import MixinContainerKost from 'Js/_detail/mixins/MixinContainerKost.js';
import MixinAbTestHelper from 'Js/_detail/mixins/MixinAbTestHelper.js';
import detailStore from '../../__mocks__/detailStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import dayjs from 'dayjs';

jest.mock('Js/@utils/makeAPICall.js');

const localVue = createLocalVue();
mockVLazy(localVue);

localVue.use(Vuex);
localVue.mixin([MixinContainerKost, MixinAbTestHelper]);
localVue.prototype.$dayjs = dayjs;

const store = new Vuex.Store(detailStore);

const ownerData = {
	_id: '32436',
	name: 'Daniel',
	photo_url: {
		real:
			'https://static8.kerupux.com/uploads/data/user/2020-04-06/V5iRikJ3.jpg',
		small:
			'https://static8.kerupux.com/uploads/cache/data/user/2020-04-06/V5iRikJ3-240x320.jpg',
		medium:
			'https://static8.kerupux.com/uploads/cache/data/user/2020-04-06/V5iRikJ3-360x480.jpg',
		large:
			'https://static8.kerupux.com/uploads/cache/data/user/2020-04-06/V5iRikJ3-540x720.jpg'
	},
	title: 'Pemilik Kos',
	statement: 'Lorem Ipsum Dolor Sit Amet',
	is_verified: true
};

const lastSeenData = {
	last_seen_at: '2021-04-06 15:44:15',
	server_time: '2021-04-06 20:44:15'
};

describe('DetailKostOwnerSection.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetailKostOwnerSection, {
			localVue,
			store
		});
	});

	it('should render detail kost benefit skeleton', () => {
		expect(wrapper.find('.detail-kost-owner-section').exists()).toBe(true);
	});

	it('Should call handleFetchOwnerData method correctly', async () => {
		wrapper.setData({
			isRendered: true,
			ownerData: {}
		});

		wrapper.vm.$store.commit('setAbTestCheckedStatus', true);
		wrapper.vm.$store.commit('setUserAbTest', true);
		wrapper.vm.$store.commit('setIdKost', null);
		await wrapper.vm.handleFetchOwnerData();

		expect(wrapper.vm.ownerData).toEqual({});

		wrapper.vm.$store.commit('setIdKost', 123456);

		makeAPICall.mockImplementation(payload => {
			if (payload.url === `/stories/${wrapper.vm.kostId}/owner`) {
				return {
					owner: ownerData
				};
			}
		});

		await wrapper.vm.handleFetchOwnerData();

		expect(wrapper.vm.ownerData).toEqual(ownerData);
	});

	it('Should call handleFetchOwnerLastOnline method correctly', async () => {
		wrapper.setData({
			isRendered: true,
			ownerData: {}
		});

		wrapper.vm.$store.commit('setAbTestCheckedStatus', true);
		wrapper.vm.$store.commit('setUserAbTest', true);
		wrapper.vm.$store.commit('setIdKost', null);
		await wrapper.vm.handleFetchOwnerLastOnline();

		expect(wrapper.vm.ownerData).toEqual(ownerData);
		expect(wrapper.vm.isOwnerLastOnlineRendered).toBeTruthy();

		wrapper.vm.$store.commit('setIdKost', 123456);

		makeAPICall.mockImplementation(payload => {
			if (payload.url === `stories/${wrapper.vm.kostId}/owner/last-seen`) {
				return lastSeenData;
			}
		});

		await wrapper.vm.handleFetchOwnerData();

		expect(wrapper.vm.ownerData).toEqual({
			...ownerData,
			...lastSeenData
		});
	});

	it('Computed ownerOnlineCondition should return correctly case-by-case', () => {
		const testCase = [
			// second < 60
			{
				last_seen_at: '2021-04-06 20:44:14',
				server_time: '2021-04-06 20:44:15',
				result: {
					needBullet: true,
					prefix: '',
					text: 'Sedang Online',
					style: 'primary'
				}
			},
			// second >= 60 && second < 3600
			{
				last_seen_at: '2021-04-06 20:44:15',
				server_time: '2021-04-06 20:45:15',
				result: {
					needBullet: false,
					prefix: '',
					text: 'Baru saja Online',
					style: 'primary'
				}
			},
			// hour < 24
			{
				last_seen_at: '2021-04-06 12:44:15',
				server_time: '2021-04-06 20:45:15',
				result: {
					needBullet: false,
					prefix: 'Online',
					text: '8 jam lalu',
					style: 'secondary'
				}
			},
			// day < 31
			{
				last_seen_at: '2021-04-04 12:44:15',
				server_time: '2021-04-06 20:45:15',
				result: {
					needBullet: false,
					prefix: 'Online',
					text: '3 hari yang lalu',
					style: 'tertiary'
				}
			},
			// else
			{
				last_seen_at: '2021-02-04 12:44:15',
				server_time: '2021-04-06 20:45:15',
				result: {
					needBullet: false,
					prefix: 'Online',
					text: 'lebih dari 1 bulan lalu',
					style: 'tertiary'
				}
			}
		];

		for (const test of testCase) {
			wrapper.setData({
				ownerData: {
					...ownerData,
					last_seen_at: test.last_seen_at,
					server_time: test.server_time
				}
			});

			expect(wrapper.vm.ownerOnlineCondition).toEqual(test.result);
		}
	});

	it('Computed maintenanceText should return correctly', () => {
		const testCase = [
			{
				isGP: 1,
				isMamirooms: true,
				text: 'disewakan'
			},
			{
				isGP: 0,
				isMamirooms: true,
				text: 'dikelola'
			},
			{
				isGP: 0,
				isMamirooms: false,
				text: 'disewakan'
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setDetailGoldplus', test.isGP);
			wrapper.vm.$store.commit('setDetailMamirooms', test.isMamirooms);

			expect(wrapper.vm.maintenanceText).toEqual(test.text);
		}
	});
});
