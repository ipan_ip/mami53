import { createLocalVue, shallowMount } from '@vue/test-utils';
import DetailKostModal from 'Js/_detail/components/kost/DetailKostModal.vue';

const localVue = createLocalVue();

describe('DetailKostModal.vue', () => {
	const wrapper = shallowMount(DetailKostModal, {
		localVue,
		propsData: {
			isShow: true,
			title: 'modal title',
			size: 'normal'
		},
		slots: {
			default: { template: '<div class="modal-body-slot"></div>' }
		}
	});

	it('should render detail kost modal properly', () => {
		expect(wrapper.find('.detail-kost-modal__header-title').text()).toBe(
			'modal title'
		);
		expect(
			wrapper.find('.detail-kost-modal__body > .modal-body-slot').exists()
		).toBe(true);
	});

	it('should emit close when button close clicked', () => {
		const closeButton = wrapper.find('.detail-kost-modal__nav-button-close');
		closeButton.trigger('click');

		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});
});
