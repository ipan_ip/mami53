import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailGallerySwitcher from 'Js/_detail/components/DetailGallerySwitcher.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});
window.open = jest.fn();

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.directive('tooltip', jest.fn());
localVue.mixin(mixinNavigator);
localVue.use(Vuex);

const mockDetailStore = {
	state: {
		detail: {
			photo_360: {
				real:
					'https://static.mamikos.com/uploads/data/style/2019-03-04/CJU7qaTU.jpg',
				small:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-360x480.jpg',
				medium:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-1080x1440.jpg',
				large:
					'https://static.mamikos.com/uploads/cache/data/style/2019-03-04/CJU7qaTU-2160x1080.jpg'
			},
			youtube_id: '5I0NsyxKJvk',
			checker: null,
			matterport_id: null
		}
	}
};

const store = new Vuex.Store(mockDetailStore);

describe('DetailGallerySwitcher.vue', () => {
	const wrapper = shallowMount(DetailGallerySwitcher, {
		localVue,
		store
	});

	describe('computed: photo360', () => {
		it('should fill photo360 data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.photo360).toBe(
					mockDetailStore.state.detail.photo_360
				);
				expect(
					wrapper
						.findAll('div.gallery-switcher__button')
						.at(0)
						.exists()
				).toBe(true);
			});
		});
	});

	describe('computed: youtubeId', () => {
		it('should fill youtubeId data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.youtubeId).toBe(
					mockDetailStore.state.detail.youtube_id
				);
				expect(
					wrapper
						.findAll('div.gallery-switcher__button')
						.at(1)
						.exists()
				).toBe(true);
			});
		});
	});

	describe('computed: matterportId', () => {
		it('should fill matterportId data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.matterportId).toBe(
					mockDetailStore.state.detail.matterport_id
				);
			});
		});
	});

	describe('computed: isChecker', () => {
		it('should fill isChecker data with data from store', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isChecker).toBe(mockDetailStore.state.detail.checker);
			});
		});
	});

	describe('computed: placeHolderVideo', () => {
		it('should fill placeHolderVideo with placeholder video', () => {
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.placeHolderVideo).toBe(wrapper.vm.placeHolder.video);
			});
		});

		it('should fill placeHolderVideo with placeholder video tour', () => {
			const customDetailStore = { ...mockDetailStore };
			customDetailStore.state.detail.checker = true;
			const store = new Vuex.Store(customDetailStore);

			const wrapper = shallowMount(DetailGallerySwitcher, {
				localVue,
				store
			});

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.placeHolderVideo).toBe(
					wrapper.vm.placeHolder.video_tour
				);
			});
		});
	});

	describe('method: switchGallery', () => {
		it('should emit EventBus switchTour', () => {
			wrapper.vm.switchGallery('tour');

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('switchTour');
			});
		});

		it('should go to virtual tour page if open in mobile', () => {
			global.innerWidth = 766;
			const customDetailStore = { ...mockDetailStore };
			customDetailStore.state.detail.matterport_id = 'test123';
			const store = new Vuex.Store(customDetailStore);

			const wrapper = shallowMount(DetailGallerySwitcher, {
				localVue,
				store
			});
			wrapper.vm.switchGallery('tour');

			wrapper.vm.$nextTick(() => {
				expect(window.open).toBeCalledWith(
					`/vrtour/matterport/${wrapper.vm.matterportId}`
				);
			});
		});

		it('should emit EventBus switchPhoto', () => {
			wrapper.vm.switchGallery('photo');

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('switchPhoto');
			});
		});

		it('should emit EventBus switch360', () => {
			wrapper.vm.switchGallery('360');

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('switch360');
			});
		});

		it('should emit EventBus switchVideo', () => {
			wrapper.vm.switchGallery('video');

			wrapper.vm.$nextTick(() => {
				expect(global.EventBus.$emit).toBeCalledWith('switchVideo');
			});
		});
	});
});
