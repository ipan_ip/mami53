import 'tests-fe/utils/mock-vue';
import DetailDesc from 'Js/_detail/components/DetailDesc.vue';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import detailStore from './__mocks__/detailStore';
import mockVLazy from '../../utils/mock-v-lazy';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store(detailStore);
const cutLongDescriptionSpy = jest.spyOn(
	DetailDesc.methods,
	'cutLongDescription'
);
const toggleLongDescSpy = jest.spyOn(DetailDesc.methods, 'toggleLongDesc');

describe('DetailDesc.vue', () => {
	const wrapper = shallowMount(DetailDesc, {
		localVue,
		store,
		data() {
			return {
				detailType: 'kost'
			};
		}
	});

	it('Computed description should filled with data from store', () => {
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.description).toBe(detailStore.state.detail.description);
		});
	});

	it('Computed isBooking should filled with data from store', () => {
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isBooking).toBe(detailStore.state.detail.is_booking);
		});
	});

	it('Computed checker should filled with data from store', () => {
		expect(wrapper.vm.checker).toBe(detailStore.state.detail.checker);
	});

	it('Should call method cutLongDescription method if description is too long', () => {
		expect(cutLongDescriptionSpy).toBeCalled();
		expect(wrapper.vm.shortDescription.length).toBe(200);
	});

	it('Should call method toggleLongDescSpy correctly', async () => {
		const toggleElement = wrapper.find('span.kost-desc__see-all');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleLongDescSpy).toBeCalled();
	});
});
