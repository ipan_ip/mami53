import { shallowMount, createLocalVue } from '@vue/test-utils';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import 'tests-fe/utils/mock-vue';
import DetailPhoto from 'Js/_detail/components/DetailPhoto';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import galleryData from './__mocks__/slideGallery.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';

window.swal = require('sweetalert2/dist/sweetalert2.js');

const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const querySelector = jest.fn(() => {
	return {
		content: 'ASD1720980ASKJDSADAD'
	};
});
const favComponentStub = {
	template: `<button class="fav-click" @click="$emit('toggleLove', {})"/>`
};
const detailCategoryGalleryStub = {
	template: `<button class="open-carousel" @click="$emit('toggle-category-carousel', 0, 0)"/>`
};
let stubs = {
	swiper,
	swiperSlide,
	'fav-share-widget': favComponentStub,
	'detail-category-gallery': detailCategoryGalleryStub
};
const mocks = {
	$toasted: {
		show: jest.fn()
	}
};

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('authCheck', {
	owner: true,
	all: true
});
mockWindowProperty('authData', {
	all: {
		id: 1
	}
});
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('document.head.querySelector', querySelector);

const fetchCategoryGallerySpy = jest.spyOn(
	DetailPhoto.methods,
	'fetchCategoryGallery'
);
const toggleCategoryCarouselSpy = jest.spyOn(
	DetailPhoto.methods,
	'toggleCategoryCarousel'
);
const toggleCategoryGallerySpy = jest.spyOn(
	DetailPhoto.methods,
	'toggleCategoryGallery'
);
const setLoveStatusSpy = jest.spyOn(DetailPhoto.methods, 'setLoveStatus');
const toggleImageCarouselSpy = jest.spyOn(
	DetailPhoto.methods,
	'toggleImageCarousel'
);
const shareThisPageSpy = jest.spyOn(DetailPhoto.methods, 'shareThisPage');
const handleShareMoEngageSpy = jest.spyOn(
	DetailPhoto.methods,
	'handleShareMoEngage'
);

describe('DetailPhoto.vue', () => {
	const localVue = createLocalVue();
	global.axios = mockAxios;
	global.navigator.share = jest.fn();
	axios.mockResolve({
		status: 200,
		data: {
			data: galleryData.categoryGalleryData
		}
	});
	mockVLazy(localVue);
	localVue.mixin([mixinNavigator, MixinSwalLoading, MixinSwalSuccessError]);
	localVue.use(Vuex);
	const store = new Vuex.Store({
		state: {
			detail: {
				_id: 123,
				type: 'Kost',
				room_title: 'Kost ABC',
				cards: galleryData.large,
				love_by_me: false
			},
			detailTypeName: 'Kost'
		},
		mutations: {
			setGallery(state, payload) {
				state.detail.cards = payload;
			}
		}
	});

	let wrapper = shallowMount(DetailPhoto, {
		localVue,
		store,
		stubs,
		mocks
	});

	it('Computed photoSourceData should return from store', () => {
		expect(wrapper.vm.photoSourceData).toEqual(
			wrapper.vm.$store.state.detail.cards
		);
	});

	it('Computed kostName should return from store', () => {
		expect(wrapper.vm.kostName).toBe(wrapper.vm.$store.state.detail.room_title);
	});

	it('Computed coverGallery should return correctly based on the availability of the cover image', () => {
		expect(wrapper.vm.coverGallery).toEqual({
			alt: 'Kost ABC',
			isHaveCover: true,
			photo_url: {
				large: 'https://i.ibb.co/6Hf9NNQ/image2.jpg',
				small: 'dummy-url'
			},
			title: 'Foto Kost ABC'
		});

		wrapper.vm.$store.commit('setGallery', []);

		expect(wrapper.vm.coverGallery).toEqual({
			alt: 'Kost ABC',
			isHaveCover: false,
			photo_url: {
				large: '/general/img/pictures/placeholder/placeholder_loading.svg',
				small: '/general/img/pictures/placeholder/placeholder_loading.svg'
			},
			title: 'Foto Kost ABC'
		});

		wrapper.vm.$store.commit('setGallery', galleryData.large);
	});

	it('Computed isSingle should return correctly if the gallery only have 1 photo', () => {
		expect(wrapper.vm.isSingle).toBe(false);

		wrapper.vm.$store.commit('setGallery', galleryData.solo);

		expect(wrapper.vm.isSingle).toBe(true);

		wrapper.vm.$store.commit('setGallery', galleryData.large);
	});

	it('Computed photoGalleryDesktop should filtered correctly wether the set of image have cover or not', async () => {
		expect(wrapper.vm.photoGalleryDesktop.length).toBe(2);

		wrapper.vm.$store.commit('setGallery', galleryData.small);

		expect(wrapper.vm.photoGalleryDesktop.length).toBe(1);
	});

	it('Computed isOwner should return correctly', () => {
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('Computed detail should return from store', () => {
		expect(wrapper.vm.detail).toEqual(wrapper.vm.$store.state.detail);
	});

	it('Computed detailTypeName should return lowercase from store', () => {
		expect(wrapper.vm.detailTypeName).toBe(
			wrapper.vm.$store.state.detailTypeName.toLowerCase()
		);
	});

	it('Should call fetchCategoryGallery method correctly', () => {
		expect(fetchCategoryGallerySpy).toBeCalled();
		expect(wrapper.vm.categoryGalleryPhoto).toEqual(
			galleryData.categoryGalleryData
		);

		wrapper.vm.$store.commit('setGallery', galleryData.solo);
	});

	it('Should call toggleCategoryGallery method correctly', async () => {
		const seeAllElement = wrapper.find('button.detail-photo__see-all');

		expect(seeAllElement.exists()).toBe(true);

		await seeAllElement.trigger('click');

		expect(toggleCategoryGallerySpy).toBeCalled();
		expect(wrapper.vm.showCategoryGallery).toBe(true);

		await wrapper.setData({ categoryGalleryPhoto: [] });

		const openCarouselElement = wrapper.find('button.open-carousel');

		expect(openCarouselElement.exists()).toBe(true);

		await openCarouselElement.trigger('click');

		expect(toggleCategoryCarouselSpy).toBeCalled();

		wrapper.setData({
			categoryGalleryPhoto: galleryData.categoryGalleryData
		});
		openCarouselElement.trigger('click');
		await wrapper.vm.$nextTick();

		expect(toggleCategoryCarouselSpy).toBeCalled();
	});

	it('Should call fetchCategoryGallery method even with 1 photo available', () => {
		expect(fetchCategoryGallerySpy).toBeCalled();

		wrapper.vm.$store.commit('setGallery', galleryData.large);
	});

	it('Should call fetchCategoryGallery method correctly with catch condition', () => {
		axios.mockReject('error');

		wrapper = shallowMount(DetailPhoto, {
			localVue,
			store,
			stubs,
			mocks
		});

		expect(fetchCategoryGallerySpy).toBeCalled();
		expect(wrapper.vm.categoryGalleryPhoto).toEqual([]);
	});

	it('Should call toggleImageCarousel when clicking image', async () => {
		const toggleElement = wrapper.find('img.desktop-only');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleImageCarouselSpy).toBeCalled();
	});

	it('Should call toggleImageCarousel on closing carousel without class modal open', async () => {
		jest.useFakeTimers();
		wrapper.setData({ showCarousel: true });
		await wrapper.vm.$nextTick();
		wrapper.vm.toggleImageCarousel();
		await wrapper.vm.$nextTick();

		expect(toggleImageCarouselSpy).toBeCalled();
		expect(wrapper.vm.showCarousel).toBe(false);

		jest.runAllTimers();
	});

	it('Should call method setLoveStatus correctly', () => {
		wrapper.vm.setLoveStatus(null, true);

		expect(setLoveStatusSpy).toBeCalledWith(null, true);
	});

	it('Should call toggleImageCarousel with 1 image only when clicking image', async () => {
		const store = new Vuex.Store({
			state: {
				detail: {
					type: 'Kost',
					room_title: 'Kost ABC',
					cards: galleryData.solo
				},
				detailTypeName: 'Kost'
			}
		});
		wrapper = shallowMount(DetailPhoto, {
			localVue,
			store,
			stubs,
			mocks
		});

		const toggleElement = wrapper.find('img.desktop-only');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleImageCarouselSpy).toBeCalled();
	});

	it('Should call method shareThisPage', async () => {
		mockWindowProperty('innerWidth', 400);
		mockWindowProperty('authCheck', {
			owner: false,
			all: true,
			tenant: true
		});

		const favComponentStub = {
			template: `<button class="share-click" @click="$emit('shareThisPage')"/>`
		};

		stubs = {
			...stubs,
			'fav-share-widget': favComponentStub
		};

		wrapper = shallowMount(DetailPhoto, {
			localVue,
			store,
			stubs,
			mocks
		});

		const shareElement = wrapper.find('button.share-click');

		expect(shareElement.exists()).toBe(true);

		await shareElement.trigger('click');

		expect(shareThisPageSpy).toBeCalled();
	});

	it('Should call setLoveStatus method and update component data correctly', () => {
		expect(wrapper.vm.isFavoritedByMe).toBe(false);

		wrapper.vm.setLoveStatus(1, true);

		expect(wrapper.vm.isFavoritedByMe).toBe(true);
	});

	it('Should call method shareThisPage', async () => {
		const favComponentStub = {
			template: `<button class="share-click" @click="$emit('handleShareMoEngage', 'whatsapp')"/>`
		};

		stubs = {
			...stubs,
			'fav-share-widget': favComponentStub
		};

		global.navigator.share = jest.fn();
		wrapper = shallowMount(DetailPhoto, {
			localVue,
			store,
			stubs,
			mocks
		});

		const shareElement = wrapper.find('button.share-click');

		expect(shareElement.exists()).toBe(true);

		await shareElement.trigger('click');

		expect(handleShareMoEngageSpy).toBeCalled();
	});
});
