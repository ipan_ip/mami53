import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ButtonSelectOption from 'Js/_area/components/mobile/ButtonSelectOption.vue';

describe('ButtonSelectOption.vue', () => {
	let wrapper;
	const PickABooSelectModal = {
		template: `
		<div>
			<button type="button" class="close-modal" @click="closeModal" />
		</div>`,
		props: ['closeModal']
	};

	const createComponent = options => {
		wrapper = shallowMount(ButtonSelectOption, {
			propsData: {
				modalTitle: 'Title',
				options: ['Jakarta', 'Semarang'],
				onClickOption: jest.fn()
			},
			stubs: {
				PickABooSelectModal
			}
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('.button-select');
			expect(findContainer().exists()).toBe(true);
		});

		it('should open modal select when button clicked', () => {
			createComponent();

			const buttonOpenModal = wrapper.find('button');
			buttonOpenModal.trigger('click');

			expect(wrapper.vm.isShowModalOption).toBe(true);
		});

		it('should close modal select when button close modal clicked', () => {
			createComponent();
			const buttonCloseModal = wrapper.find('button.close-modal');
			expect(buttonCloseModal.exists()).toBe(true);

			buttonCloseModal.trigger('click');

			expect(wrapper.vm.isShowModalOption).toBe(false);
		});
	});
});
