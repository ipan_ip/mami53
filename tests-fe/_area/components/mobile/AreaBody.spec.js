import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import AreaBody from 'Js/_area/components/mobile/AreaBody.vue';
import kostNearCampus from 'Js/_area/data/kostNearCampus.json';

describe('AreaBody.vue', () => {
	let wrapper;
	const ButtonSelectOption = {
		template: `
		<div>
			<a @click="onClickOption({name : 'Semarang'}, 0)" class="select-option">
				Semarang
			</a>
		</div>`,
		props: ['onClickOption']
	};

	const createComponent = options => {
		wrapper = shallowMount(AreaBody, {
			stubs: {
				ButtonSelectOption
			},
			...options
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('.area-body');
			expect(findContainer().exists()).toBe(true);
		});

		it('should concat props areas to data options', () => {
			createComponent({
				propsData: {
					areas: kostNearCampus.data
				}
			});

			expect(wrapper.vm.options.length).toBe(kostNearCampus.data.length + 1);
		});

		it('should update method on click option', () => {
			createComponent({
				propsData: {
					areas: kostNearCampus.data
				}
			});
			const buttonSelectOption = wrapper.find('a.select-option');
			expect(buttonSelectOption.exists()).toBe(true);

			buttonSelectOption.trigger('click');

			expect(wrapper.vm.selectedAreaTitle.name).toEqual('Semarang');
		});
	});
});
