import { createLocalVue, shallowMount } from '@vue/test-utils';
import AreaList from 'Js/_area/components/AreaList.vue';
import _flatMap from 'lodash/flatMap';
import areas from '../__mocks__/areas.json';
const localVue = createLocalVue();

describe('AreaList.vue', () => {
	const propsData = {
		areas
	};
	const wrapper = shallowMount(AreaList, { localVue, propsData });

	it('should render component correctly', () => {
		expect(wrapper.find('.area-group').exists()).toBe(true);
	});

	it('should render the area list correctly', () => {
		expect(wrapper.findAll('.area-group-list').length).toBe(areas.length);
		const firstAreaList = wrapper.findAll('.area-group-list').at(0);
		expect(firstAreaList.find('li h2').text()).toBe(areas[0].title);
	});

	it('should render the kost list correctly', () => {
		const kostList = _flatMap(areas, 'kostList');
		expect(wrapper.findAll('.area-group-list-item').length).toBe(
			kostList.length
		);
		const firstAreaListItem = wrapper.findAll('.area-group-list-item').at(0);
		expect(firstAreaListItem.find('a').text()).toBe(kostList[0].name);
		expect(firstAreaListItem.find('a').attributes('href')).toBe(
			kostList[0].url
		);
	});
});
