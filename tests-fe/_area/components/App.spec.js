import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import App from 'Js/_area/components/App.vue';

import kostNearCampus from 'Js/_area/data/kostNearCampus.json';
import popularArea from 'Js/_area/data/popularArea.json';

mockWindowProperty('location', { pathname: '/area' });
mockWindowProperty('authData', {
	all: {
		name: 'User',
		photo_user: ''
	}
});
mockWindowProperty('authCheck', {
	all: true,
	user: true
});

describe('App.vue', () => {
	let wrapper;
	const localVue = createLocalVue();

	const createComponent = options => {
		wrapper = shallowMount(App, {
			localVue,
			stubs: {
				SearchBoxModal: mockComponentWithAttrs({
					class: 'search-box-modal-stub',
					'@click': "$emit('hide-modal')"
				}),
				Navbar: mockComponentWithAttrs({
					class: 'navbar-stub',
					'@click': "$emit('button-click')"
				})
			},
			...options
		});
	};

	describe('basic', () => {
		it('should hide search box modal when hide-modal triggered', async () => {
			createComponent();
			await wrapper.setData({ isSearchModalShow: true });

			wrapper.find('.search-box-modal-stub').trigger('click');
			expect(wrapper.vm.isSearchModalShow).toBe(false);
		});

		it('should back to previous page when klik navbar', async () => {
			createComponent();
			mockWindowProperty('history.go', jest.fn());

			wrapper.find('.navbar-stub').trigger('click');
			await wrapper.vm.$nextTick();
			expect(history.go).toBeCalled();
		});

		describe('page: /area', () => {
			it('should render area list', async () => {
				createComponent();

				await wrapper.vm.$nextTick(); // wait component mounted
				await wrapper.vm.$nextTick(); // wait to get async data

				expect(wrapper.vm.areaList.length).toBe(popularArea.data.length);
			});
		});

		describe('page: /kampus', () => {
			it('should render campus list', async () => {
				mockWindowProperty('location', { pathname: '/kampus' });
				createComponent();

				await wrapper.vm.$nextTick(); // wait component mounted
				await wrapper.vm.$nextTick(); // wait to get async data

				expect(wrapper.vm.areaList.length).toBe(kostNearCampus.data.length);
			});
		});
	});
});
