import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import AreaBody from 'Js/_area/components/desktop/AreaBody.vue';
import kostNearCampus from 'Js/_area/data/kostNearCampus.json';

describe('AreaBody.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(AreaBody, {
			propsData: {
				areas: kostNearCampus.data
			}
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('.area-body');
			expect(findContainer().exists()).toBe(true);
		});
	});
});
