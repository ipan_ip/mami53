import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import TableOfContent from 'Js/_area/components/desktop/TableOfContent.vue';

describe('ButtonSelectOption.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(TableOfContent, {
			propsData: {
				contentLinks: [
					{
						url: '/',
						name: 'ToC 1'
					},
					{
						url: '/',
						name: 'ToC 1'
					}
				]
			}
		});
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('.area-toc');
			expect(findContainer().exists()).toBe(true);
		});
	});
});
