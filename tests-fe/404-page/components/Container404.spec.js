import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import Container404 from 'Js/404-page/components/Container404.vue';

describe('Container404.vue', () => {
	let wrapper;

	const createComponent = options => {
		wrapper = shallowMount(Container404);
	};

	describe('basic', () => {
		it('should render correctly', () => {
			createComponent();

			const findContainer = () => wrapper.find('.container-fluid');
			expect(findContainer().exists()).toBe(true);
		});
	});
});
