import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BookingHeader from 'Js/_landing/direct-booking/components/BookingHeader';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

const oxWebUrl = 'http://owner.mamikos.com';
global.oxWebUrl = oxWebUrl;

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingHeader.vue', () => {
	const wrapper = shallowMount(BookingHeader, { localVue });

	it('Should renders properly', () => {
		expect(wrapper.find('#bookingHeader').exists()).toBe(true);
	});
});
