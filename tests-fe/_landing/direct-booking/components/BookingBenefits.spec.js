import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BookingBenefits from 'Js/_landing/direct-booking/components/BookingBenefits';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingBenefits.vue', () => {
	const wrapper = shallowMount(BookingBenefits, { localVue });

	it('Should renders properly', () => {
		expect(wrapper.find('#bookingBenefits').exists()).toBe(true);
	});
});
