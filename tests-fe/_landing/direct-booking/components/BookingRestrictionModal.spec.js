import { createLocalVue, shallowMount } from '@vue/test-utils';
import { modal } from 'vue-strap';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import BookingRestrictionModal from 'Js/_landing/direct-booking/components/BookingRestrictionModal';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingRestrictionModal.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(BookingRestrictionModal, {
			localVue,
			stubs: {
				modal
			}
		});
	});

	it('Should renders properly', () => {
		expect(wrapper.find('.booking-restriction-modal').exists()).toBe(true);
	});

	it('Should emit closeRestrictionModal when modal is closed', () => {
		wrapper.vm.closeModal();

		expect(global.EventBus.$emit).toBeCalledWith('closeRestrictionModal');
	});
});
