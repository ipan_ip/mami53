import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BookingReason from 'Js/_landing/direct-booking/components/BookingReason';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingReason.vue', () => {
	const wrapper = shallowMount(BookingReason, { localVue });

	it('Should renders properly', () => {
		expect(wrapper.find('#bookingReason').exists()).toBe(true);
	});
});
