import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import BookingNavbar from 'Js/_landing/direct-booking/components/BookingNavbar';
import defaultStore from './__mocks__/bookingStore';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

global.tracker = jest.fn();

const oxWebUrl = 'http://owner.mamikos.com';
global.oxWebUrl = oxWebUrl;

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

const store = new Vuex.Store(defaultStore);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingNavbar.vue', () => {
	global.window.scrollTo = jest.fn();
	global.window.scrollY = 0;
	const wrapper = shallowMount(BookingNavbar, { localVue, store });

	it('Should scroll to selected section', () => {
		const getBoundingClientRectSpy = jest.fn(() => ({ height: 100 }));
		global.document.getElementById = jest.fn(() => ({
			getBoundingClientRect: getBoundingClientRectSpy
		}));

		const mockTargetId = 'benefits';
		wrapper.vm.goToSection(mockTargetId);
		expect(global.window.scrollTo).toHaveBeenCalled();
	});

	it('Should call toggleNavbarMenu function when navbar menu toggler button is clicked', async () => {
		const toggleNavbarMenuSpy = jest.spyOn(wrapper.vm, 'toggleNavbarMenu');
		const togglerButton = wrapper.find('[data-path="tgl_bookingNavbarMenu"]');

		wrapper.setMethods({ toggleNavbarMenu: toggleNavbarMenuSpy });

		expect(togglerButton.exists()).toBe(true);

		togglerButton.trigger('click');

		await wrapper.vm.$nextTick();
		expect(toggleNavbarMenuSpy).toHaveBeenCalled();
	});

	it('Should set isScrolling value to true when window.scrollY is more than 0', () => {
		global.window.scrollY = 1;
		wrapper.vm.handleScroll();
		expect(wrapper.vm.isScrolling).toBe(true);
	});

	it('Should track goToSection event properly', () => {
		const mockEventName = 'Mock Event Name';
		const mockMoEngageData = {
			interface: 'desktop',
			is_booking: false,
			is_premium: false
		};

		wrapper.vm.trackGoToSection(mockEventName);
		expect(global.tracker).toBeCalledWith('moe', [
			mockEventName,
			mockMoEngageData
		]);
	});

	it('Should close navbar properly', () => {
		wrapper.vm.navbarMenu = true;
		wrapper.vm.toggleNavbarMenu();
		expect(wrapper.vm.navbarMenu).toBe = false;
	});
});
