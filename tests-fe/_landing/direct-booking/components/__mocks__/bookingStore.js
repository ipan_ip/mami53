export default {
	state: {
		authCheck: {},
		authData: {},
		testimonial: [],
		owner: null
	},
	mutations: {
		setTestimonial(state, payload) {
			state.testimonial = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setOwner(state, payload) {
			state.owner = payload;
		}
	},
	actions: {
		fetchTestimonial() {
			jest.fn().mockName('fetchTestimonial');
		},
		fetchOwnerData() {
			jest.fn().mockName('fetchOwnerData');
		}
	}
};
