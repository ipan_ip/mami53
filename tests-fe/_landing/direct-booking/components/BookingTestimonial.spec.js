import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import SkeletonLoader from 'Js/@components/SkeletonLoader';
import BookingTestimonial from 'Js/_landing/direct-booking/components/BookingTestimonial';
import defaultStore from './__mocks__/bookingStore';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

const store = new Vuex.Store(defaultStore);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('BookingTestimonial.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(BookingTestimonial, {
			localVue,
			store,
			stubs: {
				swiper,
				swiperSlide,
				SkeletonLoader
			}
		});
	});

	it('Should renders properly', () => {
		expect(wrapper.find('#bookingTestimonial').exists()).toBe(true);
	});
});
