import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import BookingActivateButton from 'Js/_landing/direct-booking/components/BookingActivateButton';

const localVue = createLocalVue();

describe('BookingActivateButton.vue', () => {
	const wrapper = shallowMount(BookingActivateButton, { localVue });

	it('Should renders properly', () => {
		expect(wrapper.find('.booking-button').exists()).toBe(true);
	});

	it('Should emit activateBooking when clicked', async () => {
		const button = wrapper.find('[data-path="btn_activateBooking"]');
		expect(button.exists()).toBe(true);
		button.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('activateBooking')).toBeTruthy();
	});
});
