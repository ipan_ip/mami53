import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import App from 'Js/_landing/direct-booking/components/App';
import MixinActivateBooking from 'Js/_landing/direct-booking/mixins/MixinActivateBooking';
import defaultStore from './__mocks__/bookingStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockComponent from 'tests-fe/utils/mock-component';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinNavigatorIsMobile, MixinActivateBooking]);

const store = new Vuex.Store(defaultStore);

jest.mock('Js/_landing/direct-booking/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

describe('App.vue', () => {
	let wrapper;
	mockVLazy(localVue);

	const baseStubs = {
		BookingNavbar: mockComponent,
		BookingHeader: mockComponent,
		BookingReason: mockComponent,
		BookingBenefits: mockComponent,
		BookingTestimonial: mockComponent,
		BookingFAQ: mockComponent,
		BookingRestrictionModal: mockComponent,
		MamiFooter: mockComponent
	};
	const loginModalStub = `<div class="modal-stub"><button @click="$emit('closeModal')"></button></div>`;

	beforeEach(() => {
		wrapper = shallowMount(App, {
			localVue,
			store,
			stubs: {
				...baseStubs,
				OwnerLoginModal: loginModalStub
			}
		});
	});

	it('should close owner login modal when it emitted closeModal', async () => {
		expect(wrapper.find('.modal-stub button').exists()).toBe(true);
		wrapper.find('.modal-stub button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.ownerLoginModal).toBe(false);
	});

	it('Should trigger EventBus.$on properly when listenEventBus method is called', () => {
		wrapper.vm.listenEventBus();

		expect(global.EventBus.$on).toBeCalledWith(
			'showOwnerLoginModal',
			expect.any(Function)
		);

		expect(global.EventBus.$on).toBeCalledWith(
			'showRestrictionModal',
			expect.any(Function)
		);

		expect(global.EventBus.$on).toBeCalledWith(
			'closeRestrictionModal',
			expect.any(Function)
		);
	});

	// it('Should set active question to selected question index', () => {
	// 		const mockIndex = 1;
	// 		wrapper.vm.setActiveQuestion(mockIndex);
	// 		expect(wrapper.vm.activeQuestion).toBe(mockIndex);
	// 	});

	// 	it('Should track goToSection event properly', () => {
	// 		const mockMoEngageData = {
	// 			interface: 'desktop',
	// 			is_booking: false,
	// 			is_premium: false
	// 		};

	// 		wrapper.vm.trackHelpCenterIsClicked();
	// 		expect(global.tracker).toBeCalledWith('moe', [
	// 			'[Owner] Landing Page Booking Langsung - Pusat Bantuan',
	// 			mockMoEngageData
	// 		]);
	// 	});
});
