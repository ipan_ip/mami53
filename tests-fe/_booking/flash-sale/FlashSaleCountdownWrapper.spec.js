import { shallowMount, createLocalVue } from '@vue/test-utils';
import FlashSaleCountdownWrapper from 'Js/_booking/flash-sale/FlashSaleCountdownWrapper.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
const FlashSaleCountdown = { template: '<div />' };
const FlashSaleLabel = { template: '<div />' };
const $store = {
	state: {
		showBookingRequestProcess: true,
		submittingBooking: false,
		flashSale: { end: '2020-02-01', start: '2020-01-01' }
	},
	dispatch: jest.fn()
};
const $toasted = {
	show: jest.fn((eventName, { onComplete }) => {
		onComplete();
	})
};

mockWindowProperty('location.reload', jest.fn());

describe('FlashSaleCountdownWrapper.vue', () => {
	const wrapper = shallowMount(FlashSaleCountdownWrapper, {
		localVue,
		mocks: { $store, $toasted },
		stubs: { FlashSaleCountdown, FlashSaleLabel }
	});

	it('should render flash sale wrapper', () => {
		expect(wrapper.find('.flash-sale-countdown-wrapper').exists()).toBe(true);
	});

	it('should render flash sale label', () => {
		expect(wrapper.findComponent(FlashSaleLabel).exists()).toBe(true);
	});

	it('should render flash sale countdown', () => {
		expect(wrapper.findComponent(FlashSaleCountdown).exists()).toBe(true);
	});

	it('should reload page after countdown end and is on booking process', () => {
		const spyReload = jest.spyOn(global.location, 'reload');
		wrapper.findComponent(FlashSaleCountdown).vm.$emit('countdown-end');
		expect($store.dispatch).toBeCalled();
		expect($toasted.show).toBeCalled();
		expect(spyReload).toBeCalled();
		spyReload.mockRestore();
	});

	it('should not reload page after countdown end when is not on booking process', async () => {
		$store.state.submittingBooking = true;
		$store.state.showBookingSuccess = true;
		await wrapper.vm.$nextTick();
		const spyReload = jest.spyOn(global.location, 'reload');
		wrapper.findComponent(FlashSaleCountdown).vm.$emit('countdown-end');
		expect(spyReload).not.toBeCalled();
		spyReload.mockRestore();
	});
});
