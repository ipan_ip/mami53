import { shallowMount, createLocalVue } from '@vue/test-utils';
import FlashSaleCountdown from 'Js/_booking/flash-sale/FlashSaleCountdown.vue';

const localVue = createLocalVue();
jest.useFakeTimers();
describe('FlashSaleCountdown.vue', () => {
	const wrapper = shallowMount(FlashSaleCountdown, {
		localVue,
		propsData: {
			endTime: new Date(new Date().setDate(new Date().getDate() + 2)).getTime(),
			startTime: new Date().getTime(),
			isStart: false
		}
	});

	it('should render countdown container', () => {
		expect(wrapper.find('.countdown__container').exists()).toBe(true);
	});

	it('should start countdown when isStart is true', async () => {
		const spyStartCountdown = jest.spyOn(wrapper.vm, 'startCountdown');
		await wrapper.setProps({ isStart: true });
		expect(spyStartCountdown).toBeCalled();
		spyStartCountdown.mockRestore();
	});

	it('should reset countdown when isStart is false', async () => {
		await wrapper.setProps({ isStart: true });
		jest.clearAllMocks();
		await wrapper.setProps({ isStart: false });
		const spyStartCountdown = jest.spyOn(wrapper.vm, 'startCountdown');
		expect(spyStartCountdown).not.toBeCalled();
		expect(wrapper.vm.diff).toBe(0);
		spyStartCountdown.mockRestore();
	});

	it('should start countdown on created', async () => {
		const endTime = new Date(new Date().setDate(new Date().getDate() + 2));
		const spyCalculateCountdown = jest.spyOn(wrapper.vm, 'calculateCountdown');
		const spyClearInterval = jest.spyOn(global, 'clearInterval');
		await wrapper.setProps({ endTime });
		wrapper.vm.startCountdown();
		jest.advanceTimersByTime(1000);
		expect(spyCalculateCountdown).toBeCalledWith();
		expect(spyClearInterval).toBeCalled();
		spyCalculateCountdown.mockRestore();
		spyClearInterval.mockRestore();
	});

	it('should clear interval when countdown end', async () => {
		const endTime = new Date(
			new Date().setSeconds(new Date().getSeconds() - 20)
		).getTime();
		await wrapper.setProps({ endTime });
		wrapper.vm.calculateCountdown();
		expect(wrapper.vm.diff).toBe(0);
		expect(wrapper.emitted('countdown-end')[0]).toBeTruthy();
	});

	it('should get correct seconds', async () => {
		// set to 100 seconds
		await wrapper.setData({ diff: 100 });
		expect(wrapper.vm.seconds).toBe('40');
	});

	it('should get correct minutes', async () => {
		// set to 2 minutes + 20 seconds
		await wrapper.setData({ diff: 140 });
		expect(wrapper.vm.minutes).toBe('02');
	});

	it('should get correct hours', async () => {
		// set to 12 hours
		await wrapper.setData({ diff: 12 * 60 * 60 });
		expect(wrapper.vm.hours).toBe('12');
	});

	it('should get correct days', async () => {
		// set to 4 days
		await wrapper.setData({ diff: 4 * 24 * 60 * 60 });
		expect(wrapper.vm.days).toBe(4);
	});

	it('should clear countdown interval when component destroyed', () => {
		const spyClearInterval = jest.spyOn(global, 'clearInterval');
		wrapper.destroy();
		expect(spyClearInterval).toBeCalled();
		spyClearInterval.mockRestore();
	});
});
