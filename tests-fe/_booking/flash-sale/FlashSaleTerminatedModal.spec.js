import { shallowMount, createLocalVue } from '@vue/test-utils';
import FlashSaleTerminatedModal from 'Js/_booking/flash-sale/FlashSaleTerminatedModal.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('FlashSaleTerminatedModal.vue', () => {
	const wrapper = shallowMount(FlashSaleTerminatedModal, { localVue });

	it('should render booking flash sale terminated modal', () => {
		expect(wrapper.find('.flash-sale-terminated-modal').exists()).toBe(true);
	});

	it('should reload page with booking true if action button clicked', async () => {
		mockWindowProperty('window.location', {
			host: 'mamikos.com',
			pathname: '/pathname',
			origin: 'https://mamikos.com',
			protocol: 'https:',
			href: 'https://mamikos.com/pathname'
		});
		await wrapper
			.find('.flash-sale-terminated-modal__action-btn')
			.trigger('click');
		expect(window.location.href).toEqual(
			expect.stringContaining('https://mamikos.com/pathname?booking_form=true')
		);

		mockWindowProperty('window.location', {
			host: 'mamikos.com',
			pathname: '/pathname',
			href: 'https://mamikos.com/pathname'
		});
		await wrapper
			.find('.flash-sale-terminated-modal__action-btn')
			.trigger('click');
		expect(window.location.href).toEqual(
			expect.stringContaining('http://mamikos.com/pathname?booking_form=true')
		);
	});

	it('should reload page when action button clicked if no host and pathname specified', async () => {
		mockWindowProperty('window.location', {
			href: 'https://mamikos.com/pathname',
			reload: jest.fn()
		});
		const spyReload = jest.spyOn(window.location, 'reload');
		await wrapper
			.find('.flash-sale-terminated-modal__action-btn')
			.trigger('click');
		expect(spyReload).toBeCalled();
	});
});
