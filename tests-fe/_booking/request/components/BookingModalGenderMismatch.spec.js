import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingModalGenderMismatch from 'Js/_booking/request/components/BookingModalGenderMismatch.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

window.open = jest.fn();

describe('BookingModalGenderMismatch.vue', () => {
	const $store = {
		state: {
			detail: {
				gender: 1,
				fac_room_icon: [{ id: 70 }]
			}
		}
	};

	const wrapper = shallowMount(BookingModalGenderMismatch, {
		localVue,
		propsData: { showModal: true },
		mocks: { $store }
	});

	it('should render booking modal gender mismatch completely', () => {
		expect(wrapper.find('.modal-gender-img').exists()).toBe(true);
		expect(wrapper.find('.modal-gender-caption').exists()).toBe(true);
		expect(wrapper.find('.modal-gender-button').exists()).toBe(true);
	});

	it('should render correct gender', async () => {
		$store.state.detail.gender = 1;
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.modal-gender-caption__warning').exists()).toBe(true);
		expect(wrapper.find('.modal-gender-caption label').text()).toEqual(
			expect.stringContaining('pria')
		);

		$store.state.detail.gender = 2;
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.modal-gender-caption__warning').exists()).toBe(true);
		expect(wrapper.find('.modal-gender-caption label').text()).toEqual(
			expect.stringContaining('wanita')
		);
	});

	it('should render pasutri warning if kost is pasutri', async () => {
		// set kost gender to male
		$store.state.detail.gender = 1;
		// set kost to be pasutri
		$store.state.detail.fac_room_icon = [{ id: 60 }];

		await wrapper.vm.$nextTick();
		const pasutriWarning = wrapper.find(
			'.modal-gender-caption__warning-pasutri'
		);

		expect(pasutriWarning.exists()).toBe(true);
		const pasutriWarningText = pasutriWarning.text().toLowerCase();
		expect(pasutriWarningText).toEqual(expect.stringContaining('pria'));
		expect(pasutriWarningText).toEqual(expect.stringContaining('suami'));
	});

	it('should render pasutri warning depend on kost gender', async () => {
		// set kost gender to female
		$store.state.detail.gender = 2;
		// set kost to be pasutri
		$store.state.detail.fac_room_icon = [{ id: 60 }];

		await wrapper.vm.$nextTick();
		const pasutriWarning = wrapper.find(
			'.modal-gender-caption__warning-pasutri'
		);

		expect(pasutriWarning.exists()).toBe(true);
		const pasutriWarningText = pasutriWarning.text().toLowerCase();
		expect(pasutriWarningText).toEqual(expect.stringContaining('wanita'));
		expect(pasutriWarningText).toEqual(expect.stringContaining('istri'));
	});

	it('should emit close when cta clicked', async () => {
		jest.clearAllMocks();
		const buttonCTA = wrapper.find('.modal-gender-button .btn');
		await buttonCTA.trigger('click');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit close when modal closed', async () => {
		const wrapper = shallowMount(BookingModalGenderMismatch, {
			localVue,
			propsData: { showModal: true },
			stubs: {
				MamiModalWrap: `<div class="mami-modal-wrap"><button @click="$emit('close')" class="close-btn">close</button></div>`
			},
			mocks: { $store }
		});

		jest.clearAllMocks();
		wrapper.find('.mami-modal-wrap .close-btn').trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should open profile page to change gender', () => {
		const wrapper = shallowMount(BookingModalGenderMismatch, {
			localVue,
			propsData: { showModal: true, isModalGenderNull: true },
			stubs: {
				MamiModalWrap: `<div class="mami-modal-wrap"><button @click="$emit('close')" class="close-btn">close</button></div>`
			},
			mocks: { $store }
		});

		jest.clearAllMocks();
		wrapper.vm.submitAction();
		expect(window.open).toBeCalled();
	});
});
