import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingNavbar from 'Js/_booking/request/components/BookingNavbar.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import navbarSteps from './__mocks__/booking-navbar-steps.json';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingNavbar.vue', () => {
	const $route = { name: navbarSteps[0].routeName };

	const $router = { push: jest.fn() };

	const $store = {
		state: {
			params: {
				checkin: '2019-09-09',
				duration: 2,
				contact_name: 'Mamikos',
				contact_phone: '082000111222'
			}
		},
		commit: jest.fn()
	};

	const wrapper = shallowMount(BookingNavbar, {
		localVue,
		propsData: { steps: navbarSteps },
		mocks: { $route, $router, $store }
	});

	it('should render booking navbar', () => {
		expect(wrapper.find('#bookingNavbar').exists()).toBe(true);
	});

	it('should render correct navbar active label', done => {
		const { routeName } = navbarSteps[1];
		$route.name = routeName;
		wrapper.vm.$nextTick(() => {
			expect(
				wrapper
					.find('.header-title')
					.text()
					.toLowerCase()
			).toBe('langkah 2 dari 2');
			done();
		});
	});

	it('should set step active to 0 if route name is not registered', done => {
		$route.name = 'not-registered-route';
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.stepActive).toBe(0);
			done();
		});
	});

	it('should set step active to 1 if route name is snap or id-verification', done => {
		$route.name = 'snap';
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.stepActive).toBe(1);
			$route.name = 'id-verification';
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.stepActive).toBe(1);
				done();
			});
			done();
		});
	});

	it('should set route to previous step if active step index is above 0', done => {
		const navbarBack = wrapper.find('.navbar-back');
		const { routeName } = navbarSteps[2];
		$route.name = routeName;
		wrapper.vm.$nextTick(() => {
			navbarBack.trigger('click');
			expect($router.push).toBeCalledWith({
				name: navbarSteps[1].routeName
			});
			done();
		});
	});

	it('should open modal cancel booking when user close booking request process', done => {
		const navbarBack = wrapper.find('.navbar-back');
		const { routeName } = navbarSteps[0];
		$route.name = routeName;
		wrapper.vm.$nextTick(() => {
			jest.clearAllMocks();
			navbarBack.trigger('click');
			expect($store.commit).toBeCalledWith('setShowModalCancelBooking', true);
			expect(wrapper.emitted('back')[0]).toBeTruthy();
			done();
		});
	});
});
