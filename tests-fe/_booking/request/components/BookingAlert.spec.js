import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingAlert from 'Js/_booking/request/components/BookingAlert.vue';

const localVue = createLocalVue();

jest.useFakeTimers();
describe('BookingAlert.vue', () => {
	const propsData = {
		show: false,
		duration: 5000,
		message: 'instant booking alert'
	};
	const wrapper = shallowMount(BookingAlert, { localVue, propsData });

	it('should render alert according to props', () => {
		wrapper.setProps({ show: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.booking-alert').exists()).toBe(true);
			expect(wrapper.find('.booking-alert').text()).toBe(propsData.message);
			jest.advanceTimersByTime(propsData.duration);
			expect(wrapper.emitted('change')[0][0]).toBe(false);
		});
	});

	it('should emit false if show is false and no duration', () => {
		wrapper.setProps({
			show: true,
			duration: 1000
		});
		wrapper.vm.$nextTick(() => {
			wrapper.setProps({
				show: false,
				duration: 0
			});

			jest.clearAllMocks();
			wrapper.vm.setActiveAlertTimeout();
			expect(wrapper.emitted('change')[0][0]).toBe(false);
		});
	});
});
