import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BookingAfterNightValidation from 'Js/_booking/request/components/BookingAfterNightValidation.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingAfterNightValidation.vue', () => {
	const mockedStore = {
		state: {
			detail: {
				booking_time_restriction_active: true,
				available_booking_time_start: '06:00:00',
				available_booking_time_end: '18:00:00'
			},
			params: {
				checkin: '2020-02-02'
			}
		}
	};

	it('should show warning message when the booking time is unavailable', () => {
		const wrapper = shallowMount(BookingAfterNightValidation, {
			localVue,
			store: new Vuex.Store(mockedStore),
			propsData: {
				isAlert: false
			}
		});

		wrapper.vm.nightValidation = true;
		expect(wrapper.vm.nightValidation).toBe(true);
	});
});
