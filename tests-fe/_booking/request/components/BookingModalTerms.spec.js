import { shallowMount } from '@vue/test-utils';
import BookingModalTerms from 'Js/_booking/request/components/BookingModalTerms.vue';

describe('BookingModalTerms', () => {
	it('should mount vue component', () => {
		const wrapper = shallowMount(BookingModalTerms, {
			propsData: {
				showModal: true
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should close modal', () => {
		const wrapper = shallowMount(BookingModalTerms, {
			propsData: {
				showModal: true
			}
		});

		wrapper.find('.close-btn').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
