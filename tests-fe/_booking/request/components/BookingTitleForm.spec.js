import { createLocalVue, shallowMount } from '@vue/test-utils';
import BookingTitleForm from 'Js/_booking/request/components/BookingTitleForm.vue';
import navbarSteps from './__mocks__/booking-navbar-steps.json';

const localVue = createLocalVue();

describe('BookingTitleForm.vue', () => {
	const $route = { name: navbarSteps[0].routeName };

	it('should render the component', () => {
		const wrapper = shallowMount(BookingTitleForm, {
			localVue,
			propsData: { steps: navbarSteps },
			mocks: { $route }
		});

		expect(wrapper.find('.booking-title-form').exists()).toBe(true);
	});

	it('should return step 1 if route is not on id verification', done => {
		const wrapper = shallowMount(BookingTitleForm, {
			localVue,
			propsData: { steps: navbarSteps },
			mocks: { $route: { name: 'snap' } }
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.booking-title-form__step span').text()).toBe(
				'Langkah 2 dari 2'
			);
			done();
		});
	});
});
