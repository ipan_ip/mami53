export default {
	state: {
		detail: {
			_id: 1
		},
		params: {
			checkin: '2020-08-08',
			checkout: '2020-11-09',
			duration: 1,
			contact_name: 'Mamitest',
			contact_phone: '082143930215',
			contact_work_place: 'Lainnya'
		},
		checkinLabel: '1 Januari 2019',
		checkoutLabel: '1 Februari 2019',
		otherWorkPlace: 'Lainnya',
		bookingRoom: {
			booking_designer: [
				{
					id: 1
				}
			]
		},
		bookingRoomLoaded: true,
		durationLabel: {
			label: '1 Bulan'
		},
		priceKost: 30000,
		formError: {
			name: false,
			phone_number: false
		},
		formAgreement: false,
		submittingBooking: false,
		uncompleteBookingForm: {
			path: '/date',
			error: true,
			message: ''
		},
		showBookingSuccess: false,
		showBookingAlert: false,
		isShowModalTerms: false,
		forcedValidation: '',
		submitBookingData: {
			booking_code: 1234
		},
		chatData: {},
		extras: {
			emailVerifStatus: false
		},
		authData: {
			all: {
				birthday: '1998-10-10'
			}
		},
		flashSale: {
			isRunning: false,
			start: null,
			end: null
		}
	},
	getters: {
		uncompleteTenantInfo: state => {
			const {
				formError: { name, phone_number },
				params: { contact_name, contact_phone }
			} = state;
			return !contact_name || !contact_phone || name || phone_number;
		},
		uncompleteRentInfo: state => {
			const {
				params: { checkin, duration }
			} = state;
			return !duration || !checkin;
		},
		uncompleteBookingForm: (state, getters) => {
			let errorInfo = {
				error: false,
				message: ''
			};

			if (getters.uncompleteRentInfo) {
				return {
					error: true,
					message: 'Mohon lengkapi Data Sewa Anda',
					path: '/date'
				};
			} else if (getters.uncompleteTenantInfo) {
				return {
					error: true,
					message: 'Mohon lengkapi Data Penyewa dulu',
					path: '/tenant-info'
				};
			}

			return errorInfo;
		},
		disabledSubmitBooking: (state, getters) => {
			return !state.formAgreement || getters.uncompleteBookingForm.error;
		}
	},
	mutations: {
		setFormAgreement(state, value) {
			state.formAgreement = value;
		},
		setShowBookingSuccess(state, value) {
			state.showBookingSuccess = value;
		},
		setShowBookingAlert(state, value) {
			state.showBookingAlert = value;
		},
		setForcedValidation(state, value) {
			state.forcedValidation = value;
		},
		setShowModalTerms(state, payload) {
			state.isShowModalTerms = payload;
		},
		setSubmittingBooking(state, payload) {
			state.submittingBooking = payload;
		},
		setSubmitBookingData(state, payload) {
			state.submitBookingData = payload;
		},
		setChatData(state, payload) {
			state.chatData = payload;
		}
	},
	actions: {
		submitBooking() {
			return new Promise((resolve, reject) => {
				resolve(true);
			});
		}
	}
};
