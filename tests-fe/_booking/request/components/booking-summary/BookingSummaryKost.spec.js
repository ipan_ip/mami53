import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingSummaryKost from 'Js/_booking/request/components/booking-summary/BookingSummaryKost.vue';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

const state = {
	detail: {
		room_title: 'room title',
		gender: 1,
		photo_url: {
			small: 'url.com/image.jpg'
		}
	}
};

const createWrapper = () => {
	return shallowMount(BookingSummaryKost, {
		localVue,
		store: new Vuex.Store({
			state,
			getters: {
				priceKost: () => {
					return {
						label: '1 Bulan',
						price: 30000
					};
				}
			}
		})
	});
};

describe('BookingSummaryKost.vue', () => {
	const wrapper = createWrapper();

	it('should render kost title', () => {
		expect(wrapper.find('.info-kost__title-box--name').text()).toBe(
			state.detail.room_title
		);
	});

	describe('renders correct gender', () => {
		const genderText = wrapper => wrapper.find('.info-kost--gender').text();

		it('should render male gender when id is 1', () => {
			expect(genderText(wrapper)).toBe('putra');
		});

		it('should render female gender when id is 2', () => {
			state.detail.gender = 2;
			const wrapper = createWrapper();
			expect(genderText(wrapper)).toBe('putri');
		});

		it('should render campur gender when id is 0', () => {
			state.detail.gender = 0;
			const wrapper = createWrapper();
			expect(genderText(wrapper)).toBe('campur');
		});
	});
});
