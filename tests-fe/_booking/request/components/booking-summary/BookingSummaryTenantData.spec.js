import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingSummaryTenantData from 'Js/_booking/request/components/booking-summary/BookingSummaryTenantData.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = {
	state: {
		params: {
			contact_name: 'mamitest'
		}
	}
};

const stubs = {
	MamiInfo: {
		template: '<div class="mami-info"><slot></slot></div>'
	}
};

const $router = {
	push: jest.fn()
};

describe('BookingSummaryTenantData', () => {
	const wrapper = shallowMount(BookingSummaryTenantData, {
		localVue,
		store: new Vuex.Store(store),
		stubs,
		mocks: {
			$router
		}
	});

	it('should render info tenant', () => {
		expect(wrapper.find('.info-tenant').exists()).toBe(true);
	});

	it('should render tenant name', () => {
		expect(wrapper.find('.info-tenant--data-name').text()).toBe(
			store.state.params.contact_name
		);
	});

	it('should call router push if info-tenant clicked', () => {
		const infoTenant = wrapper.find('.info-tenant');
		infoTenant.trigger('click');
		expect($router.push).toBeCalled();
	});
});
