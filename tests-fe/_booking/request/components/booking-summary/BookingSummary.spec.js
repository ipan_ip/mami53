import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingSummary from 'Js/_booking/request/components/booking-summary/BookingSummary.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import store from '../__mocks__/bookingRequestStore';
import dayjs from 'dayjs';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin(mixinNavigator);

const $route = {
	path: '/date'
};

jest.mock('Js/_detail/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const push = jest.fn();
const $router = { push };
const tracker = jest.fn();
const swalError = jest.fn();
const bugsnagClient = {
	notify: jest.fn()
};
const dataLayer = {
	push: jest.fn()
};
global.tracker = tracker;
global.bugsnagClient = bugsnagClient;
global.dataLayer = dataLayer;

const mocks = {
	swalError
};

const generateWrapper = ({
	mockStore = null,
	mockRoute = null,
	methods = {}
} = {}) => {
	const mockedStore = mockStore || store;
	const shallowMountProp = {
		localVue,
		mocks: {
			$route: mockRoute || $route,
			$router,
			...mocks
		},
		methods: {
			...methods
		},
		store: new Vuex.Store(mockedStore)
	};

	return shallowMount(BookingSummary, shallowMountProp);
};

describe('BookingSummary.vue', () => {
	describe('init', () => {
		const wrapper = generateWrapper();

		it('should renders info kost', () => {
			expect(wrapper.find('.info-kost').exists()).toBe(true);
		});
	});

	describe('set state', () => {
		it('should show term agreement if link clicked', () => {
			const wrapper = generateWrapper();
			wrapper.find('.info-sk a').trigger('click');
			expect(wrapper.vm.$store.state.isShowModalTerms).toBe(true);
		});

		it('should set uncomplete booking form to true', () => {
			const wrapper = generateWrapper();
			wrapper.vm.$store.state.formError.name = true;
			expect(wrapper.vm.uncompleteBookingForm.error).toBe(true);
		});
	});

	describe('tracker', () => {
		const wrapper = generateWrapper();

		it('should call tracker moe with user booking submitted', () => {
			wrapper.vm.trackBookingSubmitted(123);
			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking Submitted',
				expect.objectContaining({
					booking_id: 123
				})
			]);
		});

		it('should call tracker moe with booking summary viewed', () => {
			wrapper.vm.formAgreement = true;
			wrapper.vm.$nextTick(() => {
				expect(tracker).toBeCalledWith('moe', [
					'[User] Booking Summary Viewed',
					expect.any(Object)
				]);
				jest.clearAllMocks();
				wrapper.vm.formAgreement = false;
				wrapper.vm.$nextTick(() => {
					expect(tracker).not.toBeCalled();
				});
			});
		});

		it('should call tracker moe with booking personal information submitted', () => {
			wrapper.vm.trackInputTenantInfo();
			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking Personal Information Submitted',
				expect.any(Object)
			]);
		});

		it('should cover all booking tracker conditions', () => {
			const mockStore = store;
			mockStore.state.params.checkin = '';
			mockStore.state.params.contact_gender = 'female';
			mockStore.state.authData = { all: { is_verify: 1 } };

			const wrapper = generateWrapper(mockStore);
			expect(wrapper.vm.dataTracked).toEqual(
				expect.objectContaining({
					booking_checkin_time: null,
					tenant_gender: 'Perempuan',
					is_verify_phone_number: 'true'
				})
			);
		});
	});

	describe('assign form agreement', () => {
		const wrapper = generateWrapper({
			mockRoute: {
				path: '/tenant-info'
			}
		});

		it('should set force validation if in current page', async () => {
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

			wrapper.vm.$store.state.bookingRoomLoaded = true;
			wrapper.vm.assignFormAgreementValue();

			expect(push).toBeCalled();

			wrapper.vm.$store.state.params.duration = 1;
			wrapper.vm.$store.state.params.checkin = '2020-02-02';
			wrapper.vm.$store.state.params.contact_phone = '';
			wrapper.vm.assignFormAgreementValue();

			await wrapper.vm.$nextTick();

			expect(spyCommit).toBeCalledWith(
				'setForcedValidation',
				expect.anything()
			);

			spyCommit.mockRestore();
		});

		it('should not force validation if all form completed', () => {
			jest.clearAllMocks();

			wrapper.vm.$store.state.bookingRoomLoaded = true;
			wrapper.vm.$store.state.params = {
				duration: 1,
				checkin: '2020-02-02',
				contact_phone: '082111222333',
				contact_name: 'Mamikos'
			};

			wrapper.vm.$store.state.formError = {
				name: false,
				phone_number: false
			};
			wrapper.vm.assignFormAgreementValue();

			expect(push).not.toBeCalled();
		});
	});

	describe('is mobile', () => {
		beforeEach(() => {
			mockWindowProperty('navigator.isMobile', true);
			mockWindowProperty('window.innerWidth', 600);
		});

		it('should give is mobile state', () => {
			const wrapper = generateWrapper();
			expect(wrapper.vm.isMobile).toBe(true);
		});

		it('should submit booking success', () => {
			const mockStore = store;
			mockStore.actions.submitBooking = jest.fn().mockResolvedValue(true);

			const wrapper = generateWrapper(mockStore);
			jest.clearAllMocks();
			wrapper.vm.submitBooking();
		});

		it('should assign price modal properties', () => {
			const wrapper = generateWrapper();
			const infoProperty = {
				show: true,
				info: 'discount price'
			};
			wrapper.vm.showInfoPrice(infoProperty);
			expect(wrapper.vm.priceInfoModal.show).toBe(true);
			expect(wrapper.vm.priceInfoModal.info).toBe('discount price');
		});

		it('should close price modal', () => {
			const wrapper = generateWrapper();
			const infoProperty = {
				show: true,
				info: 'discount price'
			};
			wrapper.vm.showInfoPrice(infoProperty);
			wrapper.vm.closePriceInfoModal();
			expect(wrapper.vm.priceInfoModal.show).toBe(false);
			expect(wrapper.vm.priceInfoModal.info).toBe('');
		});
	});

	describe('submit booking', () => {
		it('should render ajukan sewa', () => {
			let wrapper = generateWrapper();
			expect(wrapper.find('.booking-action--btn').text()).toBe('Ajukan Sewa');
			const mockStore = store;
			mockStore.state.submittingBooking = true;
			wrapper = generateWrapper({ mockStore });
			expect(wrapper.find('.booking-action--btn').text()).not.toBe(
				'Ajukan Sewa'
			);
		});

		it('should submit booking success', async () => {
			jest.clearAllMocks();

			const axios = {
				post: jest.fn().mockResolvedValue({
					data: {
						data: {},
						status: true
					}
				})
			};
			global.axios = axios;
			const wrapper = generateWrapper();

			wrapper.vm.$store.state.bookingRoom = { booking_designer: [{ id: 1 }] };
			wrapper.vm.$store.state.detail = { _id: 1 };
			wrapper.vm.$store.state.params = { contact_work_place: 'Mamikos' };
			await wrapper.vm.$nextTick;
			const onSubmitBookingSuccess = jest.spyOn(
				wrapper.vm,
				'onSubmitBookingSuccess'
			);
			await wrapper.vm.submitBooking();

			expect(onSubmitBookingSuccess).toBeCalled();

			onSubmitBookingSuccess.mockRestore();
			jest.clearAllMocks();

			await wrapper.vm.submitBooking();

			expect(axios.post).toBeCalledWith(
				expect.any(String),
				expect.objectContaining({ contact_work_place: 'Mamikos' })
			);
		});

		it('should call on booking error if booking has error', async () => {
			jest.clearAllMocks();

			const axios = {
				post: jest.fn().mockResolvedValue({
					data: {
						status: false,
						meta: {
							message: 'normal error'
						}
					}
				})
			};
			global.axios = axios;

			const wrapper = generateWrapper();
			const onSubmitBookingError = jest.spyOn(
				wrapper.vm,
				'onSubmitBookingError'
			);
			await wrapper.vm.submitBooking();

			expect(onSubmitBookingError).toBeCalled();

			onSubmitBookingError.mockRestore();
		});

		it('should commit show flash sale terminated modal if submit booking failed with flash sale terminated message', async () => {
			jest.clearAllMocks();

			const axios = {
				post: jest.fn().mockResolvedValue({
					data: {
						data: {
							prop: 'somedata'
						},
						status: false,
						meta: {
							message: 'flash_sale_terminated'
						}
					}
				})
			};
			global.axios = axios;

			const wrapper = generateWrapper();
			const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
			await wrapper.vm.submitBooking();

			expect(spyCommit).toBeCalledWith('setFlashSaleTerminatedModal', true);
			spyCommit.mockRestore();
		});
	});

	describe('after submit', () => {
		const afterSubmitWrapper = function(isMobile = false) {
			mockWindowProperty('navigator.isMobile', isMobile);
			mockWindowProperty('window.innerWidth', isMobile ? 900 : 1366);

			return generateWrapper();
		};

		it('should set chat data and tracker if success', () => {
			let wrapper = afterSubmitWrapper();
			const trackBookingSubmitted = jest.spyOn(
				wrapper.vm,
				'trackBookingSubmitted'
			);
			const trackInputTenantInfo = jest.spyOn(
				wrapper.vm,
				'trackInputTenantInfo'
			);

			wrapper.vm.$store.state.params.checkin = '2020-02-02';

			const responseData = {
				chat: {
					group_channel_url: 'groupchannelurl.com'
				}
			};
			wrapper.vm.onSubmitBookingSuccess(responseData);

			expect(wrapper.vm.$store.state.chatData).toEqual(
				JSON.stringify(responseData.chat)
			);

			responseData.chat = {};
			wrapper.vm.onSubmitBookingSuccess(responseData);

			expect(wrapper.vm.$store.state.chatData).toEqual('');

			wrapper = afterSubmitWrapper(true);
			responseData.chat = null;
			wrapper.vm.onSubmitBookingSuccess(responseData);

			expect(trackBookingSubmitted).toBeCalled();
			expect(trackInputTenantInfo).toBeCalled();
		});

		it('should log and show error if error', () => {
			const wrapper = afterSubmitWrapper();
			wrapper.vm.onSubmitBookingError('error', 'message');

			expect(mocks.swalError).toBeCalledWith(null, 'message');
			expect(bugsnagClient.notify).toBeCalledWith('error');
		});
	});

	describe('After night validation', () => {
		const wrapper = generateWrapper();

		it('should call tracker when alert showed', async () => {
			const setRestrictionTime = add => {
				return dayjs()
					.utcOffset(7, true)
					.add(add, 'hour')
					.format('HH:mm:ss');
			};

			wrapper.vm.$store.state.detail.booking_time_restriction_active = true;
			wrapper.vm.$store.state.detail.available_booking_time_start = setRestrictionTime(
				0.2
			);
			wrapper.vm.$store.state.detail.available_booking_time_end = setRestrictionTime(
				0.3
			);
			wrapper.vm.$store.state.params = {
				checkin: dayjs().format('YYYY-MM-DD')
			};
			wrapper.vm.handleNightValidation();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.nightValidation).toBe(true);
			expect(wrapper.find('.alert-wrapper').exists()).toBe(true);
			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking - Time Warning Executed',
				expect.any(Object)
			]);
		});
	});
});
