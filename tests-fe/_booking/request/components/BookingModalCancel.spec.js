import { shallowMount } from '@vue/test-utils';
import BookingModalCancel from 'Js/_booking/request/components/BookingModalCancel.vue';

describe('BookingModalCancel', () => {
	it('should mount vue component', () => {
		const wrapper = shallowMount(BookingModalCancel);

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should close modal', () => {
		const wrapper = shallowMount(BookingModalCancel);

		wrapper.find('.btn.btn-primary').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});

	it('should cancel booking', () => {
		const wrapper = shallowMount(BookingModalCancel);

		wrapper.find('.btn.btn-success').trigger('click');
		expect(wrapper.emitted().ok).toBeTruthy();
	});
});
