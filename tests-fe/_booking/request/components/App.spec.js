import 'tests-fe/utils/mock-vue';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockBookingRequestStore from './__mocks__/bookingRequestStore';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import App from 'Js/_booking/request/components/App.vue';
import dayjs from 'dayjs';
import 'dayjs/locale/id';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock(
	'Js/_booking/request/components/booking-summary/BookingSummary.vue',
	() => ({
		template: '<div />',
		render: jest.fn()
	})
);

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);

const mockStore = { ...mockBookingRequestStore };
mockStore.state.showBookingRequestProcess = false;
mockStore.state.authCheck = { all: true, tenant: true, owner: false };
mockStore.state.authData = { all: { name: 'Mamikos' } };
mockStore.state.detail = { _id: 123 };
mockStore.mutations.setBookingRequestProcess = jest.fn();

mockWindowProperty('navigator', { isMobile: false });
mockWindowProperty('window.innerWidth', 600);

const axios = {
	get: jest.fn().mockResolvedValue({
		data: { data: { data: { room: {} } }, status: true }
	}),
	post: jest.fn().mockResolvedValue({})
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const tracker = jest.fn();
global.tracker = tracker;

const swal = jest.fn().mockResolvedValue(true);
const openSwalLoading = jest.fn();
const closeSwalLoading = jest.fn();
const swalError = jest.fn();
swal.prototype.close = jest.fn();
swal.prototype.showLoading = jest.fn();
global.swal = swal;

const $toasted = { show: jest.fn() };
const $router = { push: jest.fn(), replace: jest.fn() };

const setTimeout = jest.fn((fn, _) => fn());
mockWindowProperty('setTimeout', setTimeout);

describe('App.vue', () => {
	const store = new Vuex.Store(mockStore);
	let wrapper;

	const generateWrapper = ({ mocksProp = {}, mockStubs = {} } = {}) => {
		const shallowMountProp = {
			localVue,
			store,
			stubs: {
				RouterView: { template: '<div></div>' }
			},
			mocks: {
				$toasted,
				$router,
				openSwalLoading,
				closeSwalLoading,
				swalError,
				...mocksProp
			}
		};
		return shallowMount(App, shallowMountProp);
	};

	it('should close modal terms', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		wrapper.vm.showModalTerms(true);
		expect(wrapper.vm.isShowModalTerms).toBe(true);
	});

	it('should show booking cancel modal', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		wrapper.vm.showModalTerms(true);
		expect(wrapper.vm.isShowModalTerms).toBe(true);
	});

	it('should replace route', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'snap' }
			}
		});

		expect(wrapper.vm.$router.replace).toBeCalled();
	});

	it('should set alert property properly', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		wrapper.vm.showAlert = { show: true, message: 'message' };
		expect(wrapper.vm.$store.state.showBookingAlert).toEqual({
			show: true,
			message: 'message'
		});
	});

	it('should get user profile', async () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				profile: { is_verify_identity_card: true, is_verify_email: true }
			}
		});

		await wrapper.vm.getUserProfile();
		expect(axios.get).toBeCalledWith('/user/profile');
		expect(store.state.extras.idVerifStatus).toBe(true);
		expect(store.state.extras.emailVerifStatus).toBe(true);
	});

	it('should show booking cancel modal', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		wrapper.vm.closeBooking(true);
		expect(wrapper.vm.showModalCancelBooking).toBe(true);
	});

	it('should terminate booking process', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		const closeBooking = jest.spyOn(wrapper.vm, 'closeBooking');
		const moengageEventTrack = jest.spyOn(wrapper.vm, 'moengageEventTrack');
		wrapper.vm.terminateBooking();

		expect(closeBooking).toBeCalled();
		expect(moengageEventTrack).toBeCalled();

		closeBooking.mockRestore();
		moengageEventTrack.mockRestore();
	});

	it('should send trcker', () => {
		wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});

		wrapper.vm.moengageEventTrack();
		expect(global.tracker).toBeCalled();
	});

	it('should fetch detail room when booking request modal shown', async () => {
		mockStore.state.showBookingRequestProcess = true;
		const axiosResolved = {
			get: jest.fn().mockResolvedValue({
				data: { data: { data: { room: {} } }, status: true }
			})
		};
		global.axios = axiosResolved;
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});
		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(axiosResolved.get).toBeCalledWith(
			'/user/booking/detail-room/' + wrapper.vm.roomSeq
		);
	});

	it('should set initial data with draft if draft is specified', async () => {
		mockStore.state.showBookingRequestProcess = true;

		const axiosResolved = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: {
						room: {},
						draft: {
							tenant_name: 'Mamikos Draft',
							tenant_job: 'MamiDraft',
							description: 'This is Draft'
						}
					},
					status: true
				}
			})
		};
		global.axios = axiosResolved;
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});
		wrapper.vm.$store.state.showBookingRequestProcess = false;
		await wrapper.vm.$nextTick();
		wrapper.vm.$store.state.showBookingRequestProcess = true;

		await flushPromises();

		expect(axiosResolved.get).toBeCalledWith(
			'/user/booking/detail-room/' + wrapper.vm.roomSeq
		);
		expect(wrapper.vm.$store.state.params.contact_name).toBe('Mamikos Draft');
	});

	it('should set initial data with default if draft is not specified', async () => {
		mockStore.state.showBookingRequestProcess = false;
		const axiosResolved = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: {
						room: {}
					},
					status: true
				}
			})
		};
		global.axios = axiosResolved;
		jest.clearAllMocks();

		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});
		wrapper.vm.$store.state.showBookingRequestProcess = true;
		await flushPromises();

		expect(axiosResolved.get).toBeCalledWith(
			'/user/booking/detail-room/' + wrapper.vm.roomSeq
		);
		expect(wrapper.vm.$store.state.params.contact_name).toBe(
			wrapper.vm.$store.state.authData.all.name
		);
	});

	it('should call swal error if room-detail request return status false', async () => {
		jest.clearAllMocks();
		mockStore.state.showBookingRequestProcess = false;
		const axiosResolvedError = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: { data: { room: {} } },
					status: false,
					meta: { message: 'error' }
				}
			})
		};
		global.axios = axiosResolvedError;
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			},
			methodsProp: { swal: jest.fn().mockResolvedValue(true) }
		});
		const spySwal = jest.spyOn(global, 'swal');
		wrapper.vm.$store.state.showBookingRequestProcess = true;
		await wrapper.vm.$nextTick();
		expect(axiosResolvedError.get).toBeCalledWith(
			'/user/booking/detail-room/' + wrapper.vm.roomSeq
		);
		expect(spySwal).toBeCalled();
		spySwal.mockRestore();
	});

	it('should set booking date params with draft if draft assigned as init data', async () => {
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});
		wrapper.vm.setBookingInfoDate({
			checkin: '2020-02-02',
			checkout: '2020-04-02',
			duration: 2,
			rent_count_type: 'monthly'
		});
		expect(wrapper.vm.$store.state.params.rent_count_type).toBe('monthly');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.$store.state.params.duration).toBe(2);
	});

	it('should call on error if detail room request failed', async () => {
		jest.clearAllMocks();
		mockStore.state.showBookingRequestProcess = false;
		const axiosRejectedValue = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};
		global.axios = axiosRejectedValue;
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { name: 'booking-date' }
			}
		});
		const spyOnErrorGetDetailRoom = jest.spyOn(
			wrapper.vm,
			'onErrorGetDetailRoom'
		);
		wrapper.vm.$store.state.showBookingRequestProcess = true;
		await wrapper.vm.$nextTick();

		expect(axiosRejectedValue.get).toBeCalledWith(
			'/user/booking/detail-room/' + wrapper.vm.roomSeq
		);

		await wrapper.vm.$nextTick();

		expect(spyOnErrorGetDetailRoom).toBeCalled();
		spyOnErrorGetDetailRoom.mockRestore();
	});

	it('should send booking draft if user choose to save as a draft and redirect to user draft list if first time', async () => {
		jest.clearAllMocks();
		const postResolved = {
			get: axios.get,
			post: jest.fn().mockResolvedValue({
				data: { data: { first_draft: true }, status: true }
			})
		};
		global.axios = postResolved;
		mockWindowProperty('window.location', { href: '/' });
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});

		wrapper.vm.$store.state.params.contact_name = 'Mamikos Draft';
		wrapper.vm.submitDraftBooking();
		await wrapper.vm.$nextTick();
		expect(postResolved.post).toBeCalledWith(
			'/user/draft-booking',
			expect.objectContaining({ contact_name: 'Mamikos Draft' })
		);
		expect(window.location.href).toBe('/user/booking/draft');
	});

	it('should send booking draft if user choose to save as a draft and show toast', async () => {
		jest.clearAllMocks();
		const postResolved = {
			get: jest.fn().mockResolvedValue(),
			post: jest.fn().mockResolvedValue({
				data: { data: { first_draft: false }, status: true }
			})
		};
		global.axios = postResolved;
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});

		wrapper.vm.$store.state.params.contact_name = 'Mamikos Draft';
		wrapper.vm.submitDraftBooking();
		await wrapper.vm.$nextTick();
		expect(postResolved.post).toBeCalledWith(
			'/user/draft-booking',
			expect.objectContaining({ contact_name: 'Mamikos Draft' })
		);
		expect($toasted.show).toBeCalled();
	});

	it('should show swal error if submit draft booking status is false', async () => {
		jest.clearAllMocks();
		const postResolved = {
			get: jest.fn().mockResolvedValue(),
			post: jest.fn().mockResolvedValue({
				data: { data: { first_draft: false }, status: false }
			})
		};
		global.axios = postResolved;
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});
		const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.vm.$store.state.params.contact_name = 'Mamikos Draft';
		wrapper.vm.submitDraftBooking();
		await wrapper.vm.$nextTick();
		expect(postResolved.post).toBeCalledWith(
			'/user/draft-booking',
			expect.objectContaining({ contact_name: 'Mamikos Draft' })
		);

		await wrapper.vm.$nextTick();
		expect(spySwalError).toBeCalled();
		spySwalError.mockRestore();
	});

	it('should show swal error if submit draft booking request is failed', async () => {
		jest.clearAllMocks();
		const postResolved = {
			get: jest.fn().mockResolvedValue(),
			post: jest.fn().mockRejectedValue(new Error('error'))
		};
		global.axios = postResolved;
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});
		const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.vm.$store.state.params.contact_name = 'Mamikos Draft';
		wrapper.vm.submitDraftBooking();
		await wrapper.vm.$nextTick();
		expect(postResolved.post).toBeCalledWith(
			'/user/draft-booking',
			expect.objectContaining({ contact_name: 'Mamikos Draft' })
		);

		await wrapper.vm.$nextTick();
		expect(spySwalError).toBeCalled();
		spySwalError.mockRestore();
	});

	it('should get flash sale info', async () => {
		jest.clearAllMocks();
		const getResolved = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { start: 'monday', end: 'tuesday' } }
			}),
			post: jest.fn().mockResolvedValue()
		};
		global.axios = getResolved;
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});
		wrapper.vm.getFlashSale();
		await wrapper.vm.$nextTick();
		expect(getResolved.get).toBeCalledWith('/flash-sale/running');
	});

	it('should show send error to bugsnag when get flash sale info failed', async () => {
		jest.clearAllMocks();
		const getResolved = {
			get: jest.fn().mockRejectedValue(new Error('error')),
			post: jest.fn().mockResolvedValue()
		};
		global.axios = getResolved;
		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});

		wrapper.vm.getFlashSale();
		await wrapper.vm.$nextTick();
		expect(getResolved.get).toBeCalledWith('/flash-sale/running');

		await wrapper.vm.$nextTick();
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should handle tracker conditionals', () => {
		mockWindowProperty('document.referrer', 'http://referrer.com');

		const wrapper = generateWrapper({
			mocksProp: { $route: { name: 'booking-date' } }
		});
		wrapper.vm.$store.state.detail = {
			seq: 1,
			area_city: 'city',
			area_subdistrict: 'subdistrict',
			room_title: 'property name',
			price_monthly: 20000
		};
		wrapper.vm.$store.state.params = {
			contact_name: 'name',
			contact_job: 'job',
			checkin: '2020-02-02',
			duration: 0
		};

		wrapper.vm.moengageEventTrack();
		expect(global.tracker).toBeCalledWith('moe', [
			'[User] Booking - Cancel Process Request',
			{
				last_page: 'http://referrer.com',
				property_type: 'kost',
				property_id: 1,
				property_city: 'city',
				property_subdistrict: 'subdistrict',
				property_name: 'property name',
				property_monthly_price: '20000',
				tenant_name: 'name',
				tenant_job: 'job',
				booking_start_time: '2020-02-02',
				booking_periode: null
			}
		]);
	});
});
