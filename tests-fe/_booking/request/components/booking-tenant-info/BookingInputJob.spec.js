import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingInputJob from 'Js/_booking/request/components/booking-tenant-info/BookingInputJob.vue';
import mockStore from '../__mocks__/bookingRequestStore';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(mockStore);

describe('BookingInputJob.vue', () => {
	const wrapper = shallowMount(BookingInputJob, {
		localVue,
		store
	});

	it('should render tenant job component', () => {
		expect(wrapper.find('#bookingTenantJob').exists()).toBe(true);
	});

	it('should set all work place to null and render correct label if tenant job changed', () => {
		const tenantJobList = [
			['kerja', 'Nama Perusahaan'],
			['kuliah', 'Nama Perguruan Tinggi'],
			['lainnya', 'Deskripsi']
		];
		tenantJobList.forEach(([job, label]) => {
			wrapper.setData({
				tenantJob: job
			});
			expect(wrapper.vm.jobLabel).toBe(label);
		});
	});

	it('should set on mounted other workplace to null if tenant job and workplace are listed', () => {
		wrapper.setData({
			tenantJob: 'kuliah',
			tenantWorkPlace: 'Institut Seni dan Budaya Indonesia Aceh'
		});
		wrapper.vm.checkWorkPlace();
		wrapper.vm.assignInitialWorkPlace();
		expect(wrapper.vm.userParamsOtherWorkPlace).toBe(null);
	});

	it('should set on mounted workplace to other workplace if workplace is not listed', () => {
		const otherWorkPlace = 'Institut Seni dan Budaya Indonesia Aceh Other';
		wrapper.setData({
			tenantJob: 'kuliah',
			tenantWorkPlace: otherWorkPlace
		});
		wrapper.vm.checkWorkPlace();
		wrapper.vm.assignInitialWorkPlace();
		expect(wrapper.vm.userParamsOtherWorkPlace).toBe(otherWorkPlace);
		expect(wrapper.vm.tenantWorkPlace).toBe('Lainnya');
	});

	it('should set correct tenant workplace from listed workplace', () => {
		wrapper.setData({
			tenantWorkPlace: 'Institut Seni dan Budaya Indonesia Aceh'
		});

		expect(wrapper.vm.isSelectedOtherWorkPlaceOption).toBe(false);
		wrapper.setData({
			tenantWorkPlace: 'Lainnya'
		});
		expect(wrapper.vm.isSelectedOtherWorkPlaceOption).toBe(true);
	});
});
