import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingTenantDataForm from 'Js/_booking/request/components/booking-tenant-info/BookingTenantDataForm.vue';
import mockStore from '../__mocks__/bookingRequestStore';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const storeData = { ...mockStore };
const mockStoreData = {
	params: {
		total_renter: 1,
		contact_gender: 1,
		contact_name: 'Mamikost test',
		contact_phone: '082111222111',
		is_married: true,
		contact_introduction: '',
		contact_work_place: 'Mamikos University',
		contact_job: 'kuliah'
	},
	detail: {
		fac_room_icon: [{ id: 60 }],
		gender: 2
	},
	bookingRoom: {
		max_renter: null
	},
	forcedValidation: '/date'
};

Object.keys(mockStoreData).forEach(key => {
	storeData.state[key] = mockStoreData[key];
});

const store = new Vuex.Store(storeData);

describe('BookingTenantDataForm.vue', () => {
	const $route = {
		params: {
			editTenant: true
		},
		query: {
			validate: false
		}
	};

	describe('initial render', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(BookingTenantDataForm, {
				localVue,
				stubs: {
					MamiCheckbox: {
						template: '<div></div>'
					}
				},
				mocks: { $route },
				store
			});
		});

		it('should render bookingTenantData', () => {
			expect(wrapper.find('#bookingTenantData').exists()).toBe(true);
		});

		it('should set max renter to 1 if it is null', () => {
			wrapper.vm.$store.state.bookingRoom.max_renter = null;
			expect(wrapper.vm.maxTotalGuest).toBe(1);
			expect(wrapper.vm.maxTotalGuestPlaceholder).toBe('Maksimal 1 Penyewa');
		});

		it('should set max renter value if it is not null', () => {
			wrapper.vm.$store.state.bookingRoom.max_renter = { max: 2 };
			expect(wrapper.vm.maxTotalGuest).toBe(2);
			expect(wrapper.vm.maxTotalGuestPlaceholder).toBe('Maksimal 2 Penyewa');
			expect(wrapper.vm.isSharedRoom).toBe(true);
		});

		it('should render correct gender', () => {
			const genderLabel = ['Campur', 'Laki-Laki', 'Perempuan'];
			genderLabel.forEach((label, index) => {
				wrapper.vm.$store.state.detail.gender = index;
				expect(wrapper.vm.roomGender).toBe(label);
			});
		});

		it('should set default gender selected to room gender if gender is null', () => {
			wrapper.vm.$store.state.detail.gender = 2;
			wrapper.setData({ contactGender: null });
			wrapper.vm.assignTenantData();
			expect(wrapper.vm.contactGender).toBe('female');

			wrapper.vm.$store.state.detail.gender = 1;
			wrapper.setData({ contactGender: ' ' });
			wrapper.vm.assignTenantData();
			expect(wrapper.vm.contactGender).toBe('male');
		});

		it('should render correct pasutri tag', () => {
			wrapper.vm.$store.state.detail.fac_room_icon = [{ id: 61 }, { id: 62 }];
			expect(wrapper.vm.isCouple).toBe(false);

			wrapper.setData({ totalRenter: 2 });
			wrapper.vm.$store.state.detail.fac_room_icon = [{ id: 60 }];
			expect(wrapper.vm.isCouple).toBe(true);
			expect(wrapper.vm.showMarriedCheckbox).toBe(true);
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('.married-couple-checkbox').exists()).toBe(true);
			});
		});

		it('should not disable all gender option if kost is for all gender', () => {
			wrapper.vm.$store.state.detail.gender = null;
			wrapper.setData({
				contactGender: 'male',
				genderOptions: [
					{
						label: 'Laki-laki',
						value: 'male',
						disabled: false
					},
					{
						label: 'Perempuan',
						value: 'female',
						disabled: false
					}
				]
			});
			wrapper.vm.assignTenantData();
			wrapper.vm.genderOptions.forEach(({ disabled }) => {
				expect(disabled).toBe(false);
			});
		});
	});

	describe('input validation', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(BookingTenantDataForm, {
				localVue,
				mocks: { $route },
				store
			});
		});

		it('should not validate input if it has not focused yet', () => {
			expect(wrapper.vm.tenantNameError).toEqual(
				expect.objectContaining({ status: false })
			);
		});

		it('should validate input if it has focused', () => {
			wrapper.setData({
				hasFocus: {
					name: true,
					phone: true
				}
			});

			wrapper.setData({
				contactName: '',
				contactPhone: ''
			});
			expect(wrapper.vm.tenantNameError).toEqual(
				expect.objectContaining({ status: true })
			);
			expect(wrapper.vm.tenantPhoneError).toEqual(
				expect.objectContaining({ status: true })
			);
		});

		it('should get error true if input is empty', () => {
			wrapper.setData({
				contactName: '',
				contactPhone: ''
			});
			expect(wrapper.vm.tenantNameError).toEqual(
				expect.objectContaining({ status: true })
			);
			expect(wrapper.vm.tenantPhoneError).toEqual(
				expect.objectContaining({ status: true })
			);
		});

		it('should get error true if input is not long enough', () => {
			wrapper.setData({
				contactName: 'M',
				contactPhone: '0821'
			});
			expect(wrapper.vm.tenantNameError).toEqual(
				expect.objectContaining({ status: true })
			);
			expect(wrapper.vm.tenantPhoneError).toEqual(
				expect.objectContaining({ status: true })
			);
		});

		it('should get error true if name has symbols', () => {
			wrapper.setData({
				contactName: 'Ma@$#!'
			});
			expect(wrapper.vm.tenantNameError).toEqual(
				expect.objectContaining({ status: true })
			);
		});

		it('should get error true if phone number is not following rule', () => {
			wrapper.setData({
				contactPhone: '09'
			});
			expect(wrapper.vm.tenantPhoneError).toEqual(
				expect.objectContaining({ status: true })
			);
		});
	});

	describe('set related data', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(BookingTenantDataForm, {
				localVue,
				mocks: { $route },
				store
			});
		});

		it('should set correct total renter and is married properly', () => {
			wrapper.setData({
				totalRenter: 2,
				isMarried: true
			});
			expect(wrapper.vm.totalRenter).toBe(2);
			wrapper.setData({
				totalRenter: 1
			});
			expect(wrapper.vm.totalRenter).toBe(1);
			expect(wrapper.vm.isMarried).toBe(false);
		});

		it('should set focused if route validate is true', () => {
			jest.clearAllMocks();
			const setFocuses = jest.spyOn(wrapper.vm, 'setFocuses');
			wrapper.vm.$store.state.forcedValidation = '';

			expect(setFocuses).not.toBeCalled();
			setFocuses.mockRestore();
		});
	});

	describe('immediate validation', () => {
		const wrapper = shallowMount(BookingTenantDataForm, {
			localVue,
			mocks: {
				$route: {
					query: {
						validate: true
					}
				}
			},
			store
		});

		it('should call set focus immediately', () => {
			const { vm } = wrapper;
			expect(vm.$route.query.validate).toBe(true);
			expect(vm.hasFocus.name).toBe(true);
			expect(vm.hasFocus.phone).toBe(true);
		});
	});
});
