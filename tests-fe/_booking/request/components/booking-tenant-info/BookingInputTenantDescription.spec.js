import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingInputTenantDescription from 'Js/_booking/request/components/booking-tenant-info/BookingInputTenantDescription.vue';

const localVue = createLocalVue();

describe('BookingInputTenantDescription.vue', () => {
	const $store = {
		state: {
			params: {
				contact_introduction: ''
			}
		},
		commit: jest.fn(function(_, value) {
			this.state.params.contact_introduction = value;
		})
	};
	const wrapper = shallowMount(BookingInputTenantDescription, {
		localVue,
		mocks: { $store }
	});

	it('should render tenant description', () => {
		expect(wrapper.find('.tenant-description').exists()).toBe(true);
	});

	it('should render correct counter', done => {
		wrapper.setData({
			tenantDescription: 'Mamikos'
		});
		expect($store.commit).toBeCalledWith('setTenantDesc', 'Mamikos');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.tenant-description__textarea-counter').text()).toBe(
				'7/200'
			);
			done();
		});
	});

	it('should filter input from symbols', done => {
		wrapper.setData({
			tenantDescription: 'Mamikos@!#'
		});
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.tenantDescription).toBe('Mamikos');
			done();
		});
	});
});
