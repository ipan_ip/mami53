import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingTenantData from 'Js/_booking/request/components/booking-tenant-info/BookingTenantData.vue';
import mockStore from '../__mocks__/bookingRequestStore';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

describe('BookingTenantData.vue', () => {
	const store = { ...mockStore };

	store.state.params = {
		checkin: '2020-02-02',
		contact_gender: 'female',
		total_renter: 2
	};
	store.state.detail = {
		room_title: 'Mamikos Kost',
		photo_url: {
			small: 'url.com/image.png'
		}
	};
	store.state.priceKost = {
		label: '1 Bulan',
		price: '20.000'
	};
	store.state.authData = {
		all: {
			birthday: '1998-04-06'
		}
	};
	store.state.extras = {
		idVerifStatus: 'waiting',
		emailVerifStatus: true
	};
	store.state.formError = {
		phone_number: false,
		name: false
	};

	const $router = {
		push: jest.fn()
	};

	const tracker = jest.fn();
	global.tracker = tracker;

	const $route = {
		params: {
			editTenant: false
		}
	};

	describe('initial render', () => {
		const wrapper = shallowMount(BookingTenantData, {
			localVue,
			mocks: { $route, $router },
			store: new Vuex.Store(store)
		});

		it('should render booking-input-tenant-data', () => {
			expect(wrapper.find('.booking-input-tenant-data').exists()).toBe(true);
		});
	});

	describe('is edit', () => {
		let wrapper;

		beforeEach(() => {
			$route.params.editTenant = true;
			wrapper = shallowMount(BookingTenantData, {
				localVue,
				mocks: { $route, $router },
				store: new Vuex.Store(store)
			});
		});

		it('should render save button', () => {
			expect(wrapper.find('.booking-save-edit').exists()).toBe(true);
		});

		it('should save old data before edit', () => {
			const oldData = {
				name: 'Edward',
				phone_number: '082111333222',
				gender: 'Laki-laki',
				description: '',
				job: 'kuliah',
				work_place: '',
				is_married: false,
				total_renter: 1
			};

			wrapper.vm.saveOldData(oldData);
			expect(wrapper.vm.oldDataTenant).toEqual(oldData);
		});

		it('should have correct id verification status', () => {
			wrapper.vm.$store.state.extras.idVerifStatus = '';
			expect(wrapper.vm.idVerifStatus).toBe(false);
		});

		it('should cancel edit and move router if it is not saved', () => {
			wrapper.vm.cancelEdit();

			expect($router.push).toBeCalled();
			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking Personal Information Updated',
				expect.any(Object)
			]);

			jest.clearAllMocks();
			wrapper.vm.$store.state.params.contact_gender = 'male';
			wrapper.vm.cancelEdit();

			expect($router.push).toBeCalled();
			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking Personal Information Updated',
				expect.objectContaining({ tenant_gender: 'Laki-Laki' })
			]);

			wrapper.vm.$store.state.params.contact_gender = 'female';
			wrapper.vm.trackUpdateTenantInfo();

			expect(tracker).toBeCalledWith('moe', [
				'[User] Booking Personal Information Updated',
				expect.objectContaining({ tenant_gender: 'Perempuan' })
			]);
		});
	});
});
