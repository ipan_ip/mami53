import { shallowMount } from '@vue/test-utils';
import BookingLoaderSubmitDraft from 'Js/_booking/request/components/BookingLoaderSubmitDraft.vue';

describe('BookingLoaderSubmitDraft.vue', () => {
	it('should show loader bullets', () => {
		const wrapper = shallowMount(BookingLoaderSubmitDraft, {
			propsData: { showModal: true },
			stubs: {
				MamiButtonLoading: '<div><ul><li></li><li></li><li></li></ul></div?>'
			}
		});

		expect(wrapper.findAll('li').length).toBe(3);
	});

	it('should show loader title', () => {
		const wrapper = shallowMount(BookingLoaderSubmitDraft, {
			propsData: { showModal: true }
		});

		const loaderTitle = wrapper.findAll('span').at(0);
		expect(loaderTitle.text()).toBe('Tunggu bentar ya!');
	});

	it('should show loader caption', () => {
		const wrapper = shallowMount(BookingLoaderSubmitDraft, {
			propsData: { showModal: true }
		});

		const loaderCaption = wrapper.findAll('span').at(1);
		expect(loaderCaption.text()).toBe(
			'Enggak bakal lama kok. Mami beresin halamannya dulu.'
		);
	});
});
