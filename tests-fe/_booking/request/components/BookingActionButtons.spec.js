import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingActionButtons from 'Js/_booking/request/components/BookingActionButtons.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import steps from './__mocks__/booking-navbar-steps.json';

const localVue = createLocalVue();

describe('BookingActionButtons.vue', () => {
	const $store = {
		state: {
			authData: { all: { birthday: '1990-09-09' } },
			detailKost: {},
			extras: {
				idVerifStatus: 'waiting',
				emailVerifStatus: true
			},
			params: {
				contact_name: 'Mamikos',
				contact_phone: '082222333111',
				checkin: '2020-02-02',
				duration: 1
			},
			formError: {
				name: false,
				phone_number: false
			}
		}
	};

	const $route = { name: 'booking-date' };
	const $router = { push: jest.fn() };
	const tracker = jest.fn();
	global.tracker = tracker;

	// set to desktop
	mockWindowProperty('navigator.isMobile', false);
	mockWindowProperty('window.innerWidth', 1200);

	const wrapper = shallowMount(BookingActionButtons, {
		localVue,
		mocks: { $route, $router, $store }
	});

	it('should render booking action button', () => {
		expect(wrapper.find('#bookingActionButtons').exists()).toBe(true);
	});

	it('should navigate to next step if button next clicked', () => {
		wrapper
			.find('.action-buttons-container .track_next_booking')
			.trigger('click');
		expect($router.push).toBeCalledWith(steps[1].path.replace('/', ''));
	});

	it('should navigate to booking summary when button next clicked if it is last step', done => {
		$route.name = steps[1].routeName;
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.disabledNext).toBe(false);
			wrapper
				.find('.action-buttons-container .track_next_booking')
				.trigger('click');
			expect($router.push).toBeCalledWith(steps[2].routeName);
			done();
		});
	});

	it('should set disabled next to false if active router is booking summary', () => {
		$route.name = steps[2].routeName;
		expect(wrapper.vm.disabledNext).toBe(false);
	});

	it('should track with correct interface data', () => {
		// set to mobile
		mockWindowProperty('navigator.isMobile', true);
		mockWindowProperty('window.innerWidth', 600);

		const wrapper = shallowMount(BookingActionButtons, {
			localVue,
			mocks: { $route, $router, $store }
		});

		wrapper.vm.trackInputDuration();
		expect(tracker).toBeCalledWith('moe', [
			expect.any(String),
			expect.objectContaining({ interface: 'mobile' })
		]);
		jest.clearAllMocks();
		wrapper.vm.trackInputTenantInfo();
		expect(tracker).toBeCalledWith('moe', [
			expect.any(String),
			expect.objectContaining({ interface: 'mobile' })
		]);
	});

	it('should track with correct gender data', () => {
		const genderData = {
			female: 'Perempuan',
			male: 'Laki-Laki'
		};

		Object.keys(genderData).forEach(genderKey => {
			$store.state.authData.all.gender = genderKey;
			$store.state.params.contact_gender = genderKey;
			jest.clearAllMocks();
			wrapper.vm.trackInputDuration();
			expect(tracker).toBeCalledWith('moe', [
				expect.any(String),
				expect.objectContaining({ tenant_gender: genderData[genderKey] })
			]);
			jest.clearAllMocks();
			wrapper.vm.trackInputTenantInfo();
			expect(tracker).toBeCalledWith('moe', [
				expect.any(String),
				expect.objectContaining({ tenant_gender: genderData[genderKey] })
			]);
		});
	});

	it('should track with correct phone number and id verification data', () => {
		const verificationState = ['false', 'true'];
		const idVerificationState = [
			{ value: false, label: '' },
			{ value: true, label: 'verified' }
		];
		verificationState.forEach((verificationStatus, index) => {
			$store.state.authData.all.is_verify = index;
			$store.state.extras.idVerifStatus = idVerificationState[index].label;
			jest.clearAllMocks();
			wrapper.vm.trackInputTenantInfo();
			expect(tracker).toBeCalledWith('moe', [
				expect.any(String),
				expect.objectContaining({
					is_verify_phone_number: verificationStatus,
					is_verify_identity_card: idVerificationState[index].value
				})
			]);
		});
	});
});
