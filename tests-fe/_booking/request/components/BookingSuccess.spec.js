import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingSuccess from 'Js/_booking/request/components/BookingSuccess.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingSuccess.vue', () => {
	const Cookies = {
		set: jest.fn()
	};
	const assign = jest.fn();
	const bookingContainer = {
		style: {
			overflow: 'inherit'
		}
	};
	const getElementById = jest.fn(() => {
		return bookingContainer;
	});
	Object.defineProperty(window.location, 'assign', {
		configurable: true
	});
	global.document.getElementById = getElementById;
	global.Cookies = Cookies;
	global.location.assign = assign;
	const $store = {
		state: {
			detail: {
				room_title: 'Mamirooms',
				_id: 123123
			},
			chatData: JSON.stringify({
				data: 'chat-data'
			}),
			submitBookingData: {
				chat: {
					owner_id: 4556
				}
			}
		}
	};

	const MamiButton = { template: `<button @click="$emit('click')"/>` };
	const wrapper = shallowMount(BookingSuccess, {
		localVue,
		mocks: { $store },
		stubs: { MamiButton }
	});

	it('should not render booking success modal if show is false', () => {
		expect(wrapper.find('.booking-success').exists()).toBe(false);
	});

	it('should render booking success modal if show is true', () => {
		wrapper.setProps({ show: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.booking-success').exists()).toBe(true);
			wrapper.vm.$nextTick(() => {
				expect(getElementById).toBeCalledWith('bookingContainer');
				expect(bookingContainer.style.overflowY).toBe('hidden');
			});
		});
	});

	it('should not set parent scroll behavior if modal close', () => {
		jest.clearAllMocks();
		wrapper.setProps({ show: false });
		expect(getElementById).not.toBeCalled();
	});

	it('should not set parent scroll behavior if element is null', () => {
		jest.clearAllMocks();
		bookingContainer.style.overflowY = 'inherit';
		global.document.getElementById = jest.fn(() => null);
		wrapper.setProps({ show: true });
		expect(bookingContainer.style.overflowY).toBe('inherit');
	});

	it('should set Cookies but redirect to user/booking if user choose to chat owner', () => {
		jest.clearAllMocks();
		wrapper.findComponent(MamiButton).trigger('click');
		expect(assign).toBeCalledWith(expect.stringMatching(/^\/user\/booking/));
		expect(Cookies.set).toBeCalledWith(
			'mami-tenant-booking-chat',
			expect.objectContaining(JSON.parse($store.state.chatData))
		);
	});

	it('should not set Cookies if chatData is empty', () => {
		jest.clearAllMocks();
		$store.state.chatData = '';
		wrapper.vm.setChatChannel();
		expect(Cookies.set).not.toBeCalled();
	});
});
