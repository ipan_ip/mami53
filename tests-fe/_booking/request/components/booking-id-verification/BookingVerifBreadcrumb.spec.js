import { createLocalVue, mount } from '@vue/test-utils';
import BookingVerifBreadcrumb from 'Js/_booking/request/components/booking-id-verification/BookingVerifBreadcrumb.vue';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingVerifBreadcrumb', () => {
	let wrapper;
	const store = new Vuex.Store(mockBookingRequestStore);

	test('should mount a vue component', () => {
		wrapper = mount(BookingVerifBreadcrumb, {
			localVue,
			mocks: {
				$route: { name: 'id-verification' }
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	test('should go to tenant info page', () => {
		wrapper = mount(BookingVerifBreadcrumb, {
			localVue,
			store,
			mocks: {
				$route: { name: 'id-verification' },
				$router: { push: jest.fn() }
			}
		});

		wrapper.vm.$store.state.saveIds = false;
		expect(wrapper.vm.isAlreadyInputIds).toBe(false);

		wrapper.find('#bookingVerifBreadcrumb').trigger('click');
		expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'tenant-info' });
	});

	test('should go to id id verification page', () => {
		wrapper = mount(BookingVerifBreadcrumb, {
			localVue,
			store,
			mocks: {
				$route: { name: 'tenant-info' },
				$router: { push: jest.fn() }
			}
		});

		wrapper.vm.$store.state.saveIds = false;
		expect(wrapper.vm.isAlreadyInputIds).toBe(false);

		wrapper.find('#bookingVerifBreadcrumb').trigger('click');
		expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'id-verification' });
	});
});
