import { mount, shallowMount } from '@vue/test-utils';
import BookingInputCamera from 'Js/_booking/request/components/booking-id-verification/BookingInputCamera.vue';

const IdVerifCameraStub = {
	template: '<div ref="refIdVerifCamera" class="id-verif-camera"></div>',
	data() {
		return {
			isPhotoTaken: true
		};
	},
	methods: {
		stopVideoStream: jest.fn()
	}
};

const generateWrapper = ({ mocksProp = {} } = {}) => {
	const mountProp = {
		mocks: {
			...mocksProp
		},
		stubs: {
			IdVerifCamera: IdVerifCameraStub
		}
	};

	return mount(BookingInputCamera, mountProp);
};

describe('BookingInputCamera', () => {
	it('should load the vue component', () => {
		const wrapper = generateWrapper({
			mocksProp: {
				$route: { params: { snapType: 'card' } }
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	describe('should push to id verification page', () => {
		it('should toggle the video stream', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: {
						params: { snapType: 'card' },
						name: 'id-verification'
					},
					$router: {
						push: jest.fn()
					}
				}
			});

			expect(wrapper.vm.snapType).toBe('card');
		});

		it('should cancel the video stream and return to id verif page', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: {
						params: { snapType: 'card' },
						name: 'snap'
					},
					$router: {
						push: jest.fn()
					}
				}
			});

			wrapper.vm.cancelCapture();
			expect(wrapper.vm.$router.push).toBeCalledWith({
				name: 'id-verification'
			});
		});

		it('should save the selfie image', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: {
						params: { snapType: 'selfie' }
					},
					$router: {
						push: jest.fn()
					},
					$store: {
						commit: jest.fn()
					}
				}
			});

			wrapper.vm.saveSelfie('selfie image');
			expect(wrapper.vm.$store.commit).toBeCalledWith(
				'setSelfieImg',
				'selfie image'
			);
			expect(wrapper.vm.$router.push).toBeCalledWith({
				name: 'id-verification'
			});
		});

		it('should save the card image', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: {
						params: { snapType: 'card' }
					},
					$router: {
						push: jest.fn()
					},
					$store: {
						commit: jest.fn()
					}
				}
			});

			wrapper.vm.saveCard('card image');
			expect(wrapper.vm.$store.commit).toBeCalledWith('setIdImg', 'card image');
			expect(wrapper.vm.$router.push).toBeCalledWith({
				name: 'id-verification'
			});
		});
	});
});
