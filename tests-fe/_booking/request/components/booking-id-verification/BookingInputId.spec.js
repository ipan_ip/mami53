import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import BookingInputId from 'Js/_booking/request/components/booking-id-verification/BookingInputId.vue';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
window.Vue = require('vue');
mockSwalLoading();
let store = new Vuex.Store(mockBookingRequestStore);

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

let axios = {
	post: jest.fn().mockResolvedValue(),
	all: jest.fn().mockResolvedValue(),
	spread: jest.fn().mockReturnValue()
};

const tracker = jest.fn();
global.tracker = tracker;
global.axios = axios;

const generateWrapper = ({ mocksProp, methodsProp = {} } = {}) => {
	const shallowMountProp = {
		localVue,
		store,
		mocks: {
			...mocksProp
		},
		methods: {
			...methodsProp
		}
	};

	return mount(BookingInputId, shallowMountProp);
};

describe('BookingInputId', () => {
	const uploadImage = jest.fn();
	const swalError = jest.fn();
	const closeModalChange = jest.fn();
	const selfieImgElm = new Image();
	const idImgElm = new Image();

	store.state.extras.selfieImg = selfieImgElm;
	store.state.extras.idImg = idImgElm;

	describe('test watchers', () => {
		it('should close modal change id type', () => {
			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage, closeModalChange }
			});

			expect(closeModalChange).toBeCalled();
		});

		it('should change current id state and open modal warning when change the id type', done => {
			store.state.extras.cardType = 'e_ktp';

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			store.state.extras.cardType = 'sim';

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.currentId).toBe('e_ktp');
				expect(wrapper.vm.changeId).toBe(true);
				expect(wrapper.vm.isModalCancelChange).toBe(true);
				done();
			});
		});

		it('should change the current id state when change the id type', done => {
			store.state.extras.cardType = 'e_ktp';

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			store.state.extras.cardType = 'sim';
			store.state.extras.selfieImg = null;
			store.state.extras.idImg = null;

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.currentId).toBe('sim');
				done();
			});
		});
	});

	describe('test regular functions', () => {
		it('should skip upload id verification', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: { params: { snapType: 'selfie' } },
					$router: { push: jest.fn() }
				},
				methodsProp: { uploadImage }
			});

			wrapper.vm.skipUploadId();
			expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'tenant-info' });
		});

		it('should delete current photo when change photo', () => {
			const deletePhoto = jest.fn();
			const nextPage = jest.fn();

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage, deletePhoto, nextPage }
			});

			wrapper.vm.changePhoto();
			expect(deletePhoto).toBeCalled();
			expect(nextPage).toBeCalled();
		});

		it('should close modal warning change and show photo viewer', () => {
			const closeModalChange = jest.fn();

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage, closeModalChange }
			});

			wrapper.vm.showPhoto();
			expect(closeModalChange).toBeCalled();
			expect(wrapper.vm.isModalShow).toBe(true);
		});

		it('should delete photo uploaded', () => {
			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			wrapper.vm.deletePhoto('selfie');
			expect(wrapper.vm.selfieImg).toBe(null);

			wrapper.vm.deletePhoto('card');
			expect(wrapper.vm.idImg).toBe(null);

			navigator.isMobile = true;

			wrapper.vm.snapCamType = 'selfie';
			wrapper.vm.deletePhoto();
			expect(wrapper.vm.selfieImg).toBe(null);

			wrapper.vm.snapCamType = 'card';
			wrapper.vm.deletePhoto();
			expect(wrapper.vm.idImg).toBe(null);
		});

		it('should delete all photo', () => {
			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			wrapper.vm.deleteAllPhoto();
			expect(wrapper.vm.selfieImg).toBe(null);
			expect(wrapper.vm.idImg).toBe(null);
			expect(wrapper.vm.selfieId).toBe(null);
			expect(wrapper.vm.cardId).toBe(null);
			expect(wrapper.vm.isModalShow).toBe(false);
		});

		it('should go to next step and next page', () => {
			const closeModalChange = jest.fn();
			const nextPage = jest.fn();

			const wrapper = generateWrapper({
				mocksProp: {
					$route: { params: { snapType: 'selfie' } }
				},
				methodsProp: { uploadImage, closeModalChange, nextPage }
			});

			wrapper.vm.nextStep();
			expect(closeModalChange).toBeCalled();
			expect(nextPage).toBeCalled();
		});

		it('should go to next page', () => {
			const wrapper = generateWrapper({
				mocksProp: {
					$route: { params: { snapType: 'selfie' } },
					$router: { push: jest.fn() }
				},
				methodsProp: { uploadImage }
			});

			wrapper.vm.nextPage();
			expect(wrapper.vm.$router.push).toBeCalledWith({
				name: 'snap',
				params: { snapType: wrapper.vm.snapCamType, camActive: true }
			});
		});

		it('should open modal to delete photo', () => {
			const closeModalChange = jest.fn();

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage, closeModalChange }
			});

			wrapper.vm.openModalDeletePhoto();
			expect(wrapper.vm.changeId).toBe(false);
			expect(wrapper.vm.isModalCancelChange).toBe(true);
			expect(closeModalChange).toBeCalled();
		});

		it('should close modal show and cancel', () => {
			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			wrapper.vm.closeModalShow();
			expect(wrapper.vm.isModalShow).toBe(false);
			wrapper.vm.closeModalCancel();
			expect(wrapper.vm.cardIdVal).toBe(wrapper.vm.currentId);
			expect(wrapper.vm.isModalCancelChange).toBe(false);
		});

		it('should call photo viewer function when tap on photo', () => {
			store.state.extras.selfieImg = selfieImgElm;
			store.state.extras.idImg = idImgElm;

			const showPhoto = jest.fn();

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage, showPhoto }
			});

			navigator.isMobile = false;
			wrapper.vm.openCamera('selfie');
			expect(showPhoto).toBeCalled();
			wrapper.vm.openCamera('card');
			expect(showPhoto).toBeCalled();

			navigator.isMobile = true;
			wrapper.vm.openCamera('selfie');
			expect(wrapper.vm.isModalChange).toBe(true);
			wrapper.vm.openCamera('card');
			expect(wrapper.vm.isModalChange).toBe(true);
		});

		it('should open camera', () => {
			store.state.extras.idImg = null;
			store.state.extras.selfieImg = null;

			const wrapper = generateWrapper({
				mocksProp: { $route: { params: { snapType: 'selfie' } } },
				methodsProp: { uploadImage }
			});

			wrapper.vm.openCamera('selfie');
			expect(wrapper.vm.isModalWebcam).toBe(true);
			wrapper.vm.openCamera('card');
			expect(wrapper.vm.isModalWebcam).toBe(true);
		});
	});

	describe('test save photos', () => {
		let wrapper, result, callback;
		let results = [{ data: { status: true } }, { data: { status: true } }];
		const mockAxiosSpreadResult = jest.fn();

		axios.all = jest.fn().mockResolvedValue(results);
		axios.spread = jest.fn().mockReturnValue(mockAxiosSpreadResult);

		global.axios = axios;

		store.state.extras.cardType = 'sim';
		store.state.extras.cardId = 123;
		store.state.extras.selfieId = 234;

		beforeEach(() => {
			wrapper = generateWrapper({
				mocksProp: {
					$route: { params: { snapType: 'selfie' } },
					$router: { push: jest.fn() }
				},
				methodsProp: { uploadImage, swalError }
			});
		});

		it('should save id verification images', async done => {
			await wrapper.vm.saveIdVerif();

			expect(axios.post).toHaveBeenCalledTimes(2);
			expect(axios.spread).toHaveBeenCalledWith(expect.any(Function));
			expect(mockAxiosSpreadResult).toHaveBeenCalledWith(results);

			callback = axios.spread.mock.calls[0][0];
			result = callback({ data: { status: true } }, { data: { status: true } });

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.savedIds).toBe(true);
				done();
			});
		});

		it('should throw error card id photo cannot be saved', async done => {
			await wrapper.vm.saveIdVerif();

			callback = axios.spread.mock.calls[0][0];
			result = callback(
				{ data: { status: false, meta: { message: 'upload id card error' } } },
				{ data: { status: true } }
			);

			wrapper.vm.$nextTick(() => {
				expect(swalError).toBeCalledWith(null, 'upload id card error');
				done();
			});
		});

		it('should throw error selfie and id photo cannot be saved', async done => {
			await wrapper.vm.saveIdVerif();

			callback = axios.spread.mock.calls[0][0];
			result = callback(
				{ data: { status: false } },
				{ data: { status: false } }
			);

			wrapper.vm.$nextTick(() => {
				expect(swalError).toBeCalledWith(
					null,
					'Maaf foto Anda gagal diverifikasi. Silahkan ulangi kembali atau cek konekasi internet Anda.'
				);
				done();
			});
		});

		it('should throw error and call send error to bugsnag', async () => {
			axios.all = jest.fn().mockRejectedValue();
			global.axios = axios;

			await wrapper.vm.saveIdVerif();

			expect(swalError).toBeCalled();
		});
	});
});
