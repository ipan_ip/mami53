import { createLocalVue, mount } from '@vue/test-utils';
import BookingPlaceholderIdVerif from 'Js/_booking/request/components/booking-id-verification/BookingPlaceholderIdVerif.vue';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingPlaceholderIdVerif', () => {
	let wrapper, store;

	beforeEach(() => {
		store = new Vuex.Store(mockBookingRequestStore);
		wrapper = mount(BookingPlaceholderIdVerif, {
			localVue,
			store,
			mocks: {
				$router: { push: jest.fn() }
			}
		});
	});

	test('should mount a vue component', () => {
		expect(wrapper.html).toMatchSnapshot();
	});

	test('should go to id verification upload page', () => {
		wrapper.find('.placeholder-container__box').trigger('click');
		expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'id-verification' });
	});
});
