import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingBreadcrumb from 'Js/_booking/request/components/BookingBreadcrumb.vue';
import navbarSteps from './__mocks__/booking-navbar-steps.json';

const localVue = createLocalVue();

describe('BookingBreadcrumb.vue', () => {
	// setup initial steps
	navbarSteps[0].disabled = false;
	navbarSteps[1].disabled = true;
	navbarSteps.pop();

	const propsData = { steps: navbarSteps };

	// initial path
	const { routeName, path } = navbarSteps[0];
	const $route = { name: routeName, path };
	const $router = { push: jest.fn() };

	const $store = {
		getters: {
			uncompleteBookingForm: {
				error: true,
				path,
				message: 'error message'
			}
		},
		commit: jest.fn()
	};

	const wrapper = shallowMount(BookingBreadcrumb, {
		localVue,
		propsData,
		mocks: { $route, $router, $store }
	});

	it('should render booking breadcrumb component', () => {
		expect(wrapper.find('.booking-breadcrumb__container').exists()).toBe(true);
	});

	it('should render all steps', () => {
		expect(
			wrapper.findAll('.booking-breadcrumb__crumbs-group > span').length
		).toBe(2);
	});

	it('should show error if step disabled clicked and not fully filled', done => {
		const disabledStep = wrapper
			.findAll('.booking-breadcrumb__crumbs-group > span')
			.at(1);

		disabledStep.trigger('click');
		expect($store.commit).toHaveBeenNthCalledWith(1, 'setShowBookingAlert', {
			show: true,
			message: $store.getters.uncompleteBookingForm.message
		});
		expect($store.commit).toHaveBeenNthCalledWith(2, 'setForcedValidation', '');
		wrapper.vm.$nextTick(() => {
			expect($store.commit).toHaveBeenNthCalledWith(
				3,
				'setForcedValidation',
				navbarSteps[0].path
			);
			done();
		});
	});

	it('should show error and move to error step when step clicked if error is not in current step', () => {
		$store.getters.uncompleteBookingForm.path = navbarSteps[1].path;
		const disabledStep = wrapper
			.findAll('.booking-breadcrumb__crumbs-group > span')
			.at(1);

		jest.clearAllMocks();
		disabledStep.trigger('click');
		expect($store.commit).toHaveBeenNthCalledWith(1, 'setShowBookingAlert', {
			show: true,
			message: $store.getters.uncompleteBookingForm.message
		});
		expect($router.push).toBeCalledWith({
			path: navbarSteps[1].path,
			query: { validate: true }
		});
	});

	it('should not show error and not move to error step when step clicked if there is no error', () => {
		$store.getters.uncompleteBookingForm.error = false;
		const disabledStep = wrapper
			.findAll('.booking-breadcrumb__crumbs-group > span')
			.at(1);

		jest.clearAllMocks();
		disabledStep.trigger('click');
		expect($store.commit).not.toBeCalled();
		expect($router.push).not.toBeCalled();
	});

	it('should move to selected step when step clicked if step is not disabled', done => {
		wrapper.setProps({
			steps: [navbarSteps[0], { ...navbarSteps[1], disabled: false }]
		});
		wrapper.vm.$nextTick(() => {
			const disabledStep = wrapper
				.findAll('.booking-breadcrumb__crumbs-group > span')
				.at(1);
			jest.clearAllMocks();
			disabledStep.trigger('click');
			expect($store.commit).not.toBeCalled();
			expect($router.push).toBeCalledWith({ name: navbarSteps[1].routeName });
			done();
		});
	});

	it('should go to previous page', () => {
		wrapper.vm.backRoute();
		expect(wrapper.emitted('back')).toBeTruthy();
	});
});
