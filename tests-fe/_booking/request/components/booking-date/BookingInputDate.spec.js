import '@babel/polyfill';
import { createLocalVue, shallow } from '@vue/test-utils';
import Vuex from 'vuex';
import BookingInputDate from 'Js/_booking/request/components/booking-date/BookingInputDate.vue';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

const $route = { path: '/date' };

const generateWrapper = () => {
	const mountProp = {
		localVue,
		stubs: {
			BookingInputCheckin: {
				template:
					'<div class="input-checkin"><div class="vdp-datepicker__calendar"></div></div>',
				methods: {
					validateDate: jest.fn(),
					openDatePickerFromOutside: jest.fn()
				}
			},
			BookingInputDurationCounter: '<div class="input-duration"></div>',
			BookingInputDurationRadio: '<div class="input-radio"></div>'
		},
		store: new Vuex.Store(mockBookingRequestStore),
		mocks: { $route }
	};

	return shallow(BookingInputDate, mountProp);
};

describe('BookingInputDate', () => {
	it('should render booking input date', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('#bookingInputPriceDate').exists()).toBe(true);
	});

	it.skip('should open date picker if force validate and checkin date is not valid', async () => {
		const wrapper = generateWrapper();
		const spyValidateDate = jest.spyOn(
			wrapper.vm.$refs.bookingInputCheckin,
			'validateDate'
		);
		const spyOpenDatePickerFromOutside = jest.spyOn(
			wrapper.vm.$refs.bookingInputCheckin,
			'openDatePickerFromOutside'
		);
		wrapper.vm.$store.state.forcedValidation = '/date';
		await wrapper.vm.$nextTick();
		expect(spyValidateDate).toBeCalled();
		expect(spyOpenDatePickerFromOutside).toBeCalled();
	});
});
