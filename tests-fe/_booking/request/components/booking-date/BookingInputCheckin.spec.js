import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import BookingInputCheckin from 'Js/_booking/request/components/booking-date/BookingInputCheckin.vue';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

const Cookies = {
	get: jest.fn(params => {
		return new Date();
	}),
	set: jest.fn(),
	remove: jest.fn()
};
const setTimeout = jest.fn((fn, _) => fn());

global.Cookies = Cookies;

const getBoundingClientRect = jest.fn(() => {
	return { top: 20 };
});

const scrollTo = jest.fn();
const mockQuerySelector = jest.fn(() => {
	return { getBoundingClientRect, scrollTo };
});

global.scrollTo = scrollTo;

const BookingInputCheckinContent = { template: '<div />' };

const tracker = jest.fn();
global.tracker = tracker;

describe('BookingInputCheckin.vue', () => {
	let wrapper, store;

	beforeEach(() => {
		mockWindowProperty('setTimeout', setTimeout);
		store = new Vuex.Store(mockBookingRequestStore);
		wrapper = shallowMount(BookingInputCheckin, {
			localVue,
			store,
			propsData: {
				scrollElement: '#inputCheckin'
			},
			mocks: {
				$router: {
					path: '/date',
					currentRoute: {
						path: '/date'
					}
				}
			},
			stubs: { BookingInputCheckinContent }
		});
	});

	it('should show input form to open date picker', () => {
		expect(wrapper.find('.booking-input-checkin').exists()).toBe(true);
	});

	it('should show date picker when input is clicked', async () => {
		wrapper.setData({ isShowDatePicker: false });
		jest.clearAllMocks();
		await wrapper.find('.booking-input-checkin__input').trigger('click');
		expect(wrapper.vm.isShowDatePicker).toBe(true);
		expect(wrapper.emitted('opened')[0]).toBeTruthy();
		expect(tracker).toBeCalled();
	});

	it('should close date picker when clicked from outside', async () => {
		wrapper.setData({ isShowDatePicker: true });
		wrapper.vm.handleClickOutside();
		expect(wrapper.vm.isShowDatePicker).toBe(false);
	});

	it('should open date picker when intended clicked from outside', async () => {
		wrapper.setData({ isShowDatePicker: false });
		wrapper.vm.openDatePickerFromOutside();
		expect(wrapper.vm.isShowDatePicker).toBe(true);
	});

	it('should scroll to date picker when date picker opened and isScrollOnOpen is true', async () => {
		wrapper.setData({ isShowDatePicker: false });
		wrapper.setProps({ isScrollOnOpen: true });
		mockWindowProperty('document.querySelector', mockQuerySelector);
		jest.clearAllMocks();
		wrapper.vm.toggleDatePicker();
		await wrapper.vm.$nextTick();
		expect(scrollTo).toBeCalled();
	});

	it('should set is valid to true if checkin params has value', () => {
		wrapper.vm.$store.state.params.checkin = '2020-02-02';
		expect(wrapper.vm.isError).toBe(false);
	});

	it('should validate when validateDate called', () => {
		wrapper.vm.$store.state.params.checkin = '';
		wrapper.vm.validateDate();
		expect(wrapper.vm.isError).toBe(true);
	});

	it('should emit close when isShowDatePicker change to false', async () => {
		jest.clearAllMocks();
		await wrapper.setData({ isShowDatePicker: true });
		await wrapper.setData({ isShowDatePicker: false });
		expect(wrapper.emitted('closed')[0]).toBeTruthy();
	});

	it('should emit date selected when booking input checkin content emit selected', () => {
		wrapper.findComponent(BookingInputCheckinContent).vm.$emit('date-selected');
		expect(wrapper.emitted('date-selected')[0]).toBeTruthy();
	});
});
