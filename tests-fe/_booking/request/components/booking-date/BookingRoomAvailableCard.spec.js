import { createLocalVue, shallowMount } from '@vue/test-utils';
import BookingRoomAvailableCard from 'Js/_booking/request/components/booking-date/BookingRoomAvailableCard.vue';

const localVue = createLocalVue();

describe('BookingRoomAvailableCard.vue', () => {
	const wrapper = shallowMount(BookingRoomAvailableCard, {
		localVue,
		mocks: {
			$store: {
				state: {
					detail: {
						available_room: 12
					}
				}
			}
		}
	});

	it('should render available room properly', () => {
		expect(wrapper.find('.room-available-card').exists()).toBe(true);
		expect(wrapper.find('.room-available-card__value').text()).toEqual(
			expect.stringContaining('12')
		);
	});
});
