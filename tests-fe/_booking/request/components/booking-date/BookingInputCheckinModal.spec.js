import { createLocalVue, shallowMount } from '@vue/test-utils';
import BookingInputCheckinModal from 'Js/_booking/request/components/booking-date/BookingInputCheckinModal.vue';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

global.Cookies = {
	get: jest.fn(),
	set: jest.fn(),
	remove: jest.fn()
};

describe('BookingInputCheckinModal.vue', () => {
	const wrapper = shallowMount(BookingInputCheckinModal, {
		localVue,
		store: new Vuex.Store(mockBookingRequestStore)
	});

	it('should render booking input checkin modal', () => {
		expect(wrapper.find('.booking-input-checkin-modal').exists()).toBe(true);
	});

	it('should disabled action button when checkin date is not valid', () => {
		expect(wrapper.vm.isDisabledActionButton).toBe(true);
	});

	it('should show error message if action button clicked and checkin date is not valid', async () => {
		wrapper.vm.$store.state.params.checkin = '';
		expect(wrapper.vm.isDisabledActionButton).toBe(true);
		wrapper.vm.handleActionButtonClick();
		expect(wrapper.vm.isShowRequiredMessage).toBe(true);
	});

	it('should emit action-button-clicked if action button clicked and checkin date is valid', async () => {
		jest.clearAllMocks();
		wrapper.vm.$store.state.params.checkin = '2020-04-02';
		wrapper.vm.nightValidation = false;
		await wrapper.vm.$nextTick();
		wrapper.vm.handleActionButtonClick();
		expect(wrapper.emitted('action-button-clicked')).toBeTruthy();
	});

	it('should disable action button when after night validation is active', () => {
		wrapper.vm.nightValidation = true;
		expect(wrapper.vm.nightValidation).toBe(true);
		expect(wrapper.vm.isDisabledActionButton).toBe(true);
	});
});
