import { createLocalVue, shallowMount } from '@vue/test-utils';
import BookingInputCheckinContent from 'Js/_booking/request/components/booking-date/BookingInputCheckinContent.vue';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import Vuex from 'vuex';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;
global.Cookies = { set: jest.fn(), remove: jest.fn(), get: jest.fn() };

const generateWrapper = () => {
	const shallowMountProps = {
		localVue,
		store: new Vuex.Store(mockBookingRequestStore),
		mocks: {
			navigator: {
				isMobile: true
			},
			$router: {
				currentRoute: {
					path: '/date'
				}
			}
		}
	};
	return shallowMount(BookingInputCheckinContent, shallowMountProps);
};

const tracker = jest.fn();
global.tracker = tracker;

describe('BookingInputCheckinContent.vue', () => {
	it('should render booking input checkin content', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.booking-input-checkin-content').exists()).toBe(true);
	});

	it('should assign from cookies if it is valid', () => {
		const tomorrow = dayjs()
			.add(1, 'day')
			.format('YYYY-MM-DD');
		global.Cookies.get = jest.fn(() => tomorrow);
		const wrapper = generateWrapper();
		expect(wrapper.vm.$store.state.params.checkin).toBe(tomorrow);
	});

	it('should computed value isMobile true when width of screen no more than 768', () => {
		const wrapper = generateWrapper();
		expect(wrapper.vm.isMobile).toBe(true);
	});

	it('should show after night validation when the booking time is unavailable', () => {
		const wrapper = generateWrapper();
		wrapper.vm.nightValidation = true;

		expect(wrapper.vm.nightValidation).toBe(true);
	});
});
