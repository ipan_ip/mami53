import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import BookingInputDurationRadio from 'Js/_booking/request/components/booking-date/BookingInputDurationRadio.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingInputDurationRadio', () => {
	let wrapper;

	mockBookingRequestStore.state.detail = {
		price_title_formats: {
			price_weekly: null,
			price_monthly: {
				currency_symbol: 'Rp',
				price: '1.580.000',
				rent_type_unit: 'bulan'
			},
			price_quarterly: null,
			price_semiannually: null,
			price_yearly: null
		}
	};

	const store = new Vuex.Store(mockBookingRequestStore);

	it('should load vue component', () => {
		wrapper = shallowMount(BookingInputDurationRadio, { localVue, store });

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should show the discount price', () => {
		store.state.detail.price_title_formats.price_monthly.discount_price =
			'500.000';

		wrapper = shallowMount(BookingInputDurationRadio, { localVue, store });

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should save the selected price', async () => {
		wrapper = shallowMount(BookingInputDurationRadio, {
			localVue,
			store
		});

		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.setData({ rentCountType: 'quarterly' });
		expect(spyCommit).toBeCalledWith('setRentCountType', 'quarterly');
		spyCommit.mockRestore();
	});

	it('should handle price description properly', async () => {
		wrapper = shallowMount(BookingInputDurationRadio, {
			localVue,
			store
		});

		await wrapper.setData({ rentCountType: 'monthly' });

		expect(wrapper.find('.rent-type-desc--selected').exists()).toBe(true);
		expect(wrapper.find('.rent-type-desc--selected').text()).toBe(
			'Dibayar sebulan sekali'
		);
	});
});
