import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockBookingRequestStore from '../__mocks__/bookingRequestStore';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import BookingInputDurationCounter from 'Js/_booking/request/components/booking-date/BookingInputDurationCounter.vue';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

const rentCountType = [
	{
		format: 'weekly',
		data: ['1 Minggu', '2 Minggu', '3 Minggu']
	},
	{
		format: 'monthly',
		data: ['1 Bulan', '2 Bulan']
	},
	{
		format: 'semiannually',
		data: ['6 Bulan', '12 Bulan']
	},
	{
		format: 'quarterly',
		data: ['3 Bulan', '6 Bulan', '9 Bulan']
	}
];

const mockStore = { ...mockBookingRequestStore };
mockStore.state.bookingRoom.rent_count_type = rentCountType;

window.Vue = localVue;
mockSwalLoading();

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

describe('BookingInputDurationCounter', () => {
	const store = new Vuex.Store(mockBookingRequestStore);
	const wrapper = shallowMount(BookingInputDurationCounter, {
		localVue,
		store
	});

	it('should count duration when rent type inputted', async () => {
		store.state.params.rent_count_type = 'monthly';
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.duration).toBe(1);
	});

	it('should set correct minimum duration for quarterly', () => {
		wrapper.vm.$store.state.params.rent_count_type = 'quarterly';
		expect(wrapper.vm.durationMin).toBe(3);
	});

	it('should set correct minimum duration for semiannually', () => {
		wrapper.vm.$store.state.params.rent_count_type = 'semiannually';
		expect(wrapper.vm.durationMin).toBe(6);
	});
});
