import '@babel/polyfill';

import { shallowMount } from '@vue/test-utils';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

import MixinRecommendation from 'Js/_home/components/mixins/MixinRecommendation';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import recommendationData from '../__mocks__/RecommendationData';
import recommendationEncryptedData from '../__mocks__/RecommendationEncryptionData.json';

jest.mock('Js/@utils/makeAPICall.js');

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
describe('MixinRecommendation.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	mockWindowProperty('cityOptions', recommendationData.cityOptions);
	mockWindowProperty('cityOption', recommendationData.cityOption);
	mockWindowProperty(
		'initialStaticRooms',
		recommendationData.initialStaticRooms
	);
	mockWindowProperty('authCheck', {
		owner: true
	});
	mockWindowProperty('tracker', jest.fn());
	mockWindowProperty('translateWords', translateWords);

	global.axios = mockAxios;
	let wrapper;
	const Component = {
		template: `<div><a class="promoted-room" @click="handleClickRoom" /><a class="promoted-more" @click="handleGoToMore" /></div>`,
		mixins: [MixinRecommendation, mixinNavigator]
	};
	beforeEach(() => {
		wrapper = shallowMount(Component);
	});

	it('Should render computed isOwner to true', () => {
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('Should call bugsnagClient notify when fetch data', async () => {
		axios.mockReject('error');
		mockWindowProperty('innerWidth', 400);
		const getRecommendationRoomsSpy = jest.spyOn(
			wrapper.vm,
			'getRecommendationRooms'
		);
		wrapper.setMethods({
			getRecommendationRooms: getRecommendationRoomsSpy
		});
		await wrapper.setData({
			selectedLocation: {
				name: 'Jakarta'
			}
		});
		wrapper.vm.selectedLocation = { id: '12313214' };
		await wrapper.vm.$nextTick();

		expect(getRecommendationRoomsSpy).toBeCalled();
	});

	it('Should call handleGoToMore correctly', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };
		const handleGoToMoreSpy = jest.spyOn(wrapper.vm, 'handleGoToMore');
		wrapper.setMethods({ handleGoToMore: handleGoToMoreSpy });

		expect(wrapper.find('a.promoted-more').exists()).toBe(true);

		wrapper.find('a.promoted-more').trigger('click');
		await wrapper.vm.$nextTick();

		expect(handleGoToMoreSpy).toBeCalled();
		assignMock.mockClear();
	});

	it('Should call handleClickRoom correctly', async () => {
		mockWindowProperty('window.open', jest.fn());
		const handleClickRoomSpy = jest.spyOn(wrapper.vm, 'handleClickRoom');
		wrapper.setMethods({ handleClickRoom: handleClickRoomSpy });

		expect(wrapper.find('a.promoted-room').exists()).toBe(true);

		wrapper.find('a.promoted-room').trigger('click');
		await wrapper.vm.$nextTick();

		expect(handleClickRoomSpy).toBeCalled();
	});

	it('Should call getCurrentPosition method when change selectedLocation', async () => {
		const getCurrentPositionSpy = jest.spyOn(wrapper.vm, 'getCurrentPosition');
		wrapper.setMethods({
			getCurrentPosition: getCurrentPositionSpy
		});
		await wrapper.setData({
			selectedLocation: {
				name: 'Jakarta'
			}
		});
		wrapper.vm.selectedLocation = { id: 'myLocation' };
		await wrapper.vm.$nextTick();

		expect(getCurrentPositionSpy).toBeCalled();
	});

	it('Should call getRecommendationRooms method when change selectedLocation', async () => {
		mockWindowProperty('innerWidth', 800);
		makeAPICall.mockImplementation(() => ({
			rooms: recommendationEncryptedData.encryptedData
		}));
		const getRecommendationRoomsSpy = jest.spyOn(
			wrapper.vm,
			'getRecommendationRooms'
		);
		wrapper.setMethods({
			getRecommendationRooms: getRecommendationRoomsSpy
		});
		await wrapper.setData({
			selectedLocation: {
				name: 'Jakarta'
			}
		});
		wrapper.vm.selectedLocation = { id: '12313214' };
		await wrapper.vm.$nextTick();

		expect(getRecommendationRoomsSpy).toBeCalled();
		expect(wrapper.vm.isLoading).toBe(false);
		expect(wrapper.vm.isEmptyList).toBe(false);
	});

	it('Should call getRecommendationRooms method when change selectedLocation and return blank array', async () => {
		mockWindowProperty('innerWidth', 800);
		makeAPICall.mockImplementation(() => ({
			rooms: 'lxIyWUCfxaof0hh116PWKQ=='
		}));
		const getRecommendationRoomsSpy = jest.spyOn(
			wrapper.vm,
			'getRecommendationRooms'
		);
		wrapper.setMethods({
			getRecommendationRooms: getRecommendationRoomsSpy
		});
		await wrapper.setData({
			selectedLocation: {
				name: 'Jakarta'
			}
		});
		wrapper.vm.selectedLocation = { id: '12313214' };
		await wrapper.vm.$nextTick();

		expect(getRecommendationRoomsSpy).toBeCalled();
		expect(wrapper.vm.isLoading).toBe(false);
		expect(wrapper.vm.isEmptyList).toBe(true);
	});

	it('Should call getCurrentPosition method with geolocation activated and success return', async () => {
		global.navigator.geolocation = {
			getCurrentPosition: jest.fn().mockImplementationOnce(success =>
				Promise.resolve(
					success({
						coords: {
							latitude: 11.1,
							longitude: 22.2
						}
					})
				)
			)
		};
		const getRoomsByCurrentLocationSpy = jest.spyOn(
			wrapper.vm,
			'getRoomsByCurrentLocation'
		);
		wrapper.setMethods({
			getRoomsByCurrentLocation: getRoomsByCurrentLocationSpy
		});
		await wrapper.setData({
			selectedLocation: {
				name: 'Jakarta'
			}
		});
		wrapper.vm.selectedLocation = { id: 'myLocation' };
		await wrapper.vm.$nextTick();

		expect(getRoomsByCurrentLocationSpy).toBeCalled();
	});
});
