import '@babel/polyfill';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

import MixinPromoted from 'Js/_home/components/mixins/MixinPromoted';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import promotedData from '../__mocks__/promotedEncryptionData.json';

window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const notify = jest.fn();

jest.mock('Js/@utils/makeAPICall.js');

describe('MixinPromoted.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify
	});
	mockWindowProperty('authCheck', {
		owner: true
	});
	mockWindowProperty('innerWidth', 800);
	mockWindowProperty('tracker', jest.fn());
	mockWindowProperty('translateWords', translateWords);

	global.axios = mockAxios;
	const localVue = createLocalVue();
	let wrapper;
	localVue.mixin([mixinNavigator, MixinPromoted]);
	const Component = {
		template: `<a class="promoted-room" @click="handleClickRoom" />`,
		mixins: [MixinPromoted]
	};
	beforeEach(() => {
		wrapper = shallowMount(Component, {
			localVue
		});
	});

	it('Should render computed isOwner to true', () => {
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('Should call bugsnagClient notify when fetch data', () => {
		axios.mockReject('error');
		mockWindowProperty('innerWidth', 400);
		const wrapper = shallowMount(Component, {
			localVue
		});

		expect(wrapper.vm.locationOptions).toEqual([]);
	});

	it('Should fill promoted rooms with correct data', async () => {
		makeAPICall.mockImplementation(() => ({
			rooms: promotedData.encryptedData
		}));

		const wrapper = shallowMount(Component, {
			localVue
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.promotedRooms.length).toEqual(0);
		expect(wrapper.vm.isLoading).toBe(false);
		expect(wrapper.vm.isEmptyList).toBe(false);
	});

	it('Should call handleClickRoom correctly', async () => {
		mockWindowProperty('window.open', jest.fn());
		const handleClickRoomSpy = jest.spyOn(wrapper.vm, 'handleClickRoom');
		wrapper.setMethods({ handleClickRoom: handleClickRoomSpy });

		expect(wrapper.find('a.promoted-room').exists()).toBe(true);

		wrapper.find('a.promoted-room').trigger('click');
		await wrapper.vm.$nextTick();

		expect(handleClickRoomSpy).toBeCalled();
	});

	it('Should call getPromotedRooms method when change selectedLocation', async () => {
		const getPromotedRoomsSpy = jest.spyOn(wrapper.vm, 'getPromotedRooms');
		wrapper.setMethods({ getPromotedRooms: getPromotedRoomsSpy });
		wrapper.setData({ selectedLocation: 'semua kota' });
		await wrapper.vm.$nextTick();

		expect(getPromotedRoomsSpy).toBeCalled();

		wrapper.setData({ selectedLocation: 'Makassar' });
		await wrapper.vm.$nextTick();

		expect(getPromotedRoomsSpy).toBeCalled();
	});
});
