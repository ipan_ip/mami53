import { shallowMount, createLocalVue } from '@vue/test-utils';
import KostNearCampus from 'Js/_home/components/KostNearCampus';
window.Vue = require('vue');
// eslint-disable-next-line
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

describe('KostNearCampus.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin(mixinNavigator);
	localVue.directive('lazy', (el, binding) => {
		return el.setAttribute('data-src', binding.value);
	});
	let wrapper;
	global.tracker = jest.fn();
	beforeEach(() => {
		wrapper = shallowMount(KostNearCampus, {
			localVue
		});
	});

	it('computed isOwner should return false if user not owner', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.isOwner).toBe(false);
	});

	it('computed isOwner should return true if user is owner', () => {
		const authCheck = {
			all: true,
			owner: true,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('computed isLogin should return false if user not login', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.isLogin).toBe(false);
	});

	it('computed isLogin should return true if user is login', () => {
		const authCheck = {
			all: true,
			owner: false,
			user: true,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.isLogin).toBe(true);
	});

	it('computed imgPrefixPath should return mobile asset on mobile screen', () => {
		global.innerWidth = 766;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.imgPrefixPath).toEqual('/assets/area/mobile/');
	});

	it('computed imgPrefixPath should return desktop asset on large screen', () => {
		global.innerWidth = 1366;
		const wrapper = shallowMount(KostNearCampus, {
			localVue
		});
		expect(wrapper.vm.imgPrefixPath).toEqual('/assets/area/desktop/');
	});

	it('should render kost near campus card section', () => {
		expect(wrapper.find('.popular-campus').exists()).toBe(true);
	});

	it('should render all kost near campus card according to kost data', () => {
		expect(wrapper.findAll('a.popular-campus-item').length).toEqual(
			wrapper.vm.popularCampus.length
		);
	});

	it('should call moEngage on click once of kost near campus', () => {
		const testCase = {
			name: 'UGM',
			location: 'Jogja',
			imgSrc: '/assets/logo-kampus/UGM.png',
			url: '/kost/kost-dekat-ugm-murah'
		};
		const spyUpdate = jest.spyOn(wrapper.vm, 'handleMoEngage');
		wrapper.setMethods({ handleMoEngage: spyUpdate });
		expect(wrapper.find('a.popular-campus-item').exists()).toBe(true);
		wrapper.find('a.popular-campus-item').trigger('click');
		expect(spyUpdate).toBeCalledWith(testCase);
	});
});
