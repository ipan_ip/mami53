import { shallowMount, createLocalVue } from '@vue/test-utils';
import Promoted from 'Js/_home/components/mobile/Promoted';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinPromoted from 'Js/_home/components/mixins/MixinPromoted';
jest.mock('Js/@utils/makeAPICall.js');
window.Vue = require('vue');

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const notify = jest.fn();

jest.mock('Js/@utils/makeAPICall.js');
mockWindowProperty('bugsnagClient', {
	notify
});

const openModalSelectLocationSpy = jest.spyOn(
	Promoted.methods,
	'openModalSelectLocation'
);
const closeModalSelectLocationSpy = jest.spyOn(
	Promoted.methods,
	'closeModalSelectLocation'
);
const onClickOptionSpy = jest.spyOn(Promoted.methods, 'onClickOption');

const PickABooSelectModal = {
	template: `<div><button class="close-select" @click="closeModal" /><button class="select-option" @click="onClickOption('Bandung')" /></div>`,
	props: ['closeModal', 'onClickOption']
};

describe('Promoted.vue', () => {
	global.axios = mockAxios;
	const localVue = createLocalVue();
	localVue.mixin([mixinNavigator, MixinPromoted]);

	const wrapper = shallowMount(Promoted, {
		localVue,
		stubs: {
			PickABooSelectModal
		}
	});

	it('Should render promoted location options correctly', () => {
		expect(wrapper.find('.promoted').exists()).toBe(true);
		expect(wrapper.findAll('div.promoted-item').length).toEqual(
			wrapper.vm.promotedRooms.length
		);
	});

	it('Should call method openModalSelectLocation when clicking the button', async () => {
		const openModalElement = wrapper.find('button.button-location');

		expect(openModalElement.exists()).toBe(true);

		await openModalElement.trigger('click');

		expect(openModalSelectLocationSpy).toBeCalled();
	});

	it('Should call method closeModalSelectLocation when toggle it on the child component', async () => {
		const closeModalElement = wrapper.find('button.close-select');

		expect(closeModalElement.exists()).toBe(true);

		await closeModalElement.trigger('click');

		expect(closeModalSelectLocationSpy).toBeCalled();
	});

	it('Should call method onClickOption when selecting the option', async () => {
		const selectElement = wrapper.find('button.select-option');

		expect(selectElement.exists()).toBe(true);

		await selectElement.trigger('click');

		expect(onClickOptionSpy).toBeCalledWith('Bandung');
	});
});
