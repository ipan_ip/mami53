import { shallowMount, createLocalVue } from '@vue/test-utils';
import ProductSearch from 'Js/_home/components/mobile/ProductSearch';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import productData from '../__mocks__/productData.json';
import { swiper, swiperSlide } from 'vue-awesome-swiper';

const handleClickProductImageSpy = jest.spyOn(
	ProductSearch.methods,
	'handleClickProductImage'
);
const handleScrollSpy = jest.spyOn(ProductSearch.methods, 'handleScroll');

const stubs = {
	swiper,
	swiperSlide
};

describe('ProductSearch.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const openModalSearchSpy = jest.fn();

	let wrapper = shallowMount(ProductSearch, {
		propsData: {
			openModalSearch: openModalSearchSpy
		},
		localVue,
		stubs
	});

	it('Should render product list the same as data provided', () => {
		expect(wrapper.find('.product-image-buttons').exists()).toBe(true);
		expect(wrapper.findAll('.product-image-button-item').length).toEqual(
			productData.length
		);
	});

	it('Should call handleClickProductImage function when click banner', () => {
		expect(wrapper.findAll('a').length).toEqual(productData.length);

		wrapper.find('a').trigger('click');

		expect(handleClickProductImageSpy).toBeCalled();
	});

	it('Should call handleClickProductImage function when click banner with assign url', () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };

		expect(wrapper.findAll('a').length).toEqual(productData.length);

		wrapper
			.findAll('a')
			.at(1)
			.trigger('click');

		expect(handleClickProductImageSpy).toBeCalled();
		expect(openModalSearchSpy).toBeCalled();

		assignMock.mockClear();
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});

		mockWindowProperty('pageYOffset', 112);

		wrapper = shallowMount(ProductSearch, {
			propsData: {
				openModalSearch: openModalSearchSpy
			},
			localVue,
			stubs
		});

		wrapper.vm.$refs.searchBarHomepage = {
			classList: {
				add: jest.fn()
			}
		};

		await mockMapEventListener.scroll({
			scrollTo: 112
		});

		expect(handleScrollSpy).toBeCalled();
	});

	it('Should call removeEventListener on destroy', async () => {
		mockWindowProperty('window', {
			addEventListener: jest.fn(),
			removeEventListener: jest.fn()
		});

		wrapper.destroy();

		expect(window.removeEventListener).toBeCalled();
	});
});
