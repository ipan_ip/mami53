import { shallowMount, createLocalVue } from '@vue/test-utils';
import Recommendation from 'Js/_home/components/mobile/Recommendation';
import '@babel/polyfill';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import recommendationData from '../__mocks__/RecommendationData';
import MixinRecommendation from 'Js/_home/components/mixins/MixinRecommendation';
window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

describe('Recommendation.vue', () => {
	global.axios = mockAxios;
	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', {
		notify
	});
	mockWindowProperty('cityOptions', recommendationData.cityOptions);
	mockWindowProperty('cityOption', recommendationData.cityOption);
	mockWindowProperty(
		'initialStaticRooms',
		recommendationData.initialStaticRooms
	);
	const localVue = createLocalVue();
	let wrapper;
	localVue.mixin([mixinNavigator, MixinRecommendation]);
	beforeEach(() => {
		wrapper = shallowMount(Recommendation, {
			localVue,
			data() {
				return {
					selectedLocation: {
						name: 'Jakarta'
					}
				};
			}
		});
	});

	it('Should render recommendation correctly', () => {
		expect(wrapper.find('div.recommendation-header').exists()).toBe(true);
		expect(wrapper.findAll('div.col-xs-6').length).toEqual(
			wrapper.vm.recommendedRooms.length
		);
	});

	it('Should call openModalSelectLocation method on click button', () => {
		const openModalSelectLocationSpy = jest.spyOn(
			wrapper.vm,
			'openModalSelectLocation'
		);
		wrapper.setMethods({
			openModalSelectLocation: openModalSelectLocationSpy
		});
		expect(
			wrapper
				.findAll('button')
				.filter(node => node.text() === wrapper.vm.selectedLocation.name)
				.at(0)
				.exists()
		).toBe(true);
		wrapper
			.findAll('button')
			.filter(node => node.text() === wrapper.vm.selectedLocation.name)
			.at(0)
			.trigger('click');

		expect(openModalSelectLocationSpy).toBeCalled();
		expect(wrapper.vm.isShowModalSelectLocation).toBe(true);
	});

	it('Should call closeModalSelectLocation method on click close button', () => {
		const customSelectModal = {
			template: `<div><button type="button" class="close-select-location" @click="closeModal" /><a @click="onClickOption({name : 'Bandung'})" class="select-location"></a></div>`,
			props: ['closeModal', 'onClickOption']
		};
		const wrapper = shallowMount(Recommendation, {
			localVue,
			stubs: {
				PickABooSelectModal: customSelectModal
			}
		});
		expect(wrapper.find('button.close-select-location').exists()).toBe(true);

		wrapper.find('button.close-select-location').trigger('click');

		expect(wrapper.vm.isShowModalSelectLocation).toBe(false);
	});

	it('Should call onClickOption method on click option', () => {
		const customSelectModal = {
			template: `<div><button type="button" class="close-select-location" @click="closeModal" /><a @click="onClickOption({name : 'Bandung'})" class="select-location"></a></div>`,
			props: ['closeModal', 'onClickOption']
		};
		const wrapper = shallowMount(Recommendation, {
			localVue,
			stubs: {
				PickABooSelectModal: customSelectModal
			}
		});
		expect(wrapper.find('a.select-location').exists()).toBe(true);

		wrapper.find('a.select-location').trigger('click');

		expect(wrapper.vm.selectedLocation.name).toEqual('Bandung');
	});
});
