import { shallowMount, createLocalVue } from '@vue/test-utils';
import FlashSale from 'Js/_home/components/mobile/FlashSale';
import mockAxios from 'tests-fe/utils/mock-axios';
import '@babel/polyfill';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinFlashSaleData from 'Js/_home/components/mixins/MixinFlashSaleData';
import MixinFlashSaleHelper from 'Js/_flash-sale/mixins/MixinFlashSale';
import FlashSaleData from '../__mocks__/flashSaleData.json';
window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const openModalSelectLocationSpy = jest.spyOn(
	FlashSale.methods,
	'openModalSelectLocation'
);

describe('FlashSale.vue', () => {
	mockWindowProperty('innerWidth', 400);
	mockWindowProperty('axios', mockAxios);
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	axios.mockResolve({
		data: {
			status: 200,
			data: FlashSaleData
		}
	});
	const localVue = createLocalVue();
	let wrapper;
	localVue.mixin([mixinNavigator, MixinFlashSaleData, MixinFlashSaleHelper]);
	beforeEach(() => {
		wrapper = shallowMount(FlashSale, {
			localVue,
			data() {
				return {
					flashSaleData: {
						areas: [
							{
								name: 'Jakarta',
								id: 1
							}
						]
					}
				};
			}
		});
	});

	it('Should call closeModalSelectLocation method on click close button', () => {
		const customSelectModal = {
			template: `<div><button type="button" class="close-select-location" @click="closeModal" /><a @click="onClickOption({name : 'Bandung'})" class="select-location"></a></div>`,
			props: ['closeModal', 'onClickOption']
		};
		wrapper = shallowMount(FlashSale, {
			localVue,
			stubs: {
				PickABooSelectModal: customSelectModal
			}
		});
		expect(wrapper.find('button.close-select-location').exists()).toBe(true);

		wrapper.find('button.close-select-location').trigger('click');

		expect(wrapper.vm.isShowModalSelectLocation).toBe(false);
	});

	it('Should call onClickOption method on click option', () => {
		const customSelectModal = {
			template: `<div><button type="button" class="close-select-location" @click="closeModal" /><a @click="onClickOption('Jakarta')" class="select-location"></a></div>`,
			props: ['closeModal', 'onClickOption']
		};
		wrapper = shallowMount(FlashSale, {
			localVue,
			stubs: {
				PickABooSelectModal: customSelectModal
			},
			data() {
				return {
					flashSaleData: {
						areas: [
							{
								name: 'Jakarta',
								id: 1
							}
						]
					}
				};
			}
		});
		expect(wrapper.find('a.select-location').exists()).toBe(true);

		wrapper.find('a.select-location').trigger('click');

		expect(wrapper.vm.selectedLocation).toEqual('Jakarta');
	});

	it('Should call openModalSelection method correctly', async () => {
		await wrapper.setData({ selectedLocation: 'Jakarta' });

		const selectElement = wrapper
			.findAll('button')
			.filter(node => node.text() === 'Jakarta');

		expect(selectElement.exists()).toBe(true);

		await expect(selectElement.trigger('click'));

		expect(openModalSelectLocationSpy).toBeCalled();
	});
});
