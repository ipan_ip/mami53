import { shallowMount, createLocalVue } from '@vue/test-utils';
import Navbar from 'Js/_home/components/Navbar';
import '@babel/polyfill';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockLocation from 'tests-fe/utils/mock-location';
import Toasted from 'vue-toasted';
window.Vue = require('vue');
// eslint-disable-next-line
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

window.oxWebUrl = 'local.mamikos.com';

const getChatUnreadCountSpy = jest.spyOn(Navbar.methods, 'getChatUnreadCount');
const Cookies = {
	remove: jest.fn()
};
mockWindowProperty('Cookies', Cookies);
mockWindowProperty('oxOauth2Domain', 'www.owner-mamikos.com');

describe('Navbar.vue', () => {
	const authData = {
		all: {
			id: 1
		}
	};
	const stubs = {
		'notif-center': mockComponent
	};
	const url = 'http://mamikos.com';
	const localVue = createLocalVue();
	let wrapper;
	global.authData = authData;
	localVue.mixin(mixinNavigator);
	localVue.use(Toasted);
	localVue.directive('lazy', (el, binding) => {
		return el.setAttribute('data-src', binding.value);
	});
	global.tracker = jest.fn();
	console.error = jest.fn();

	beforeEach(() => {
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				hasPromotionMenu: false,
				openModalSearch: jest.fn(),
				userName: '',
				profilPicture: '',
				isTenant: false,
				isLogin: false
			}
		});
	});

	it('computed unreadCountLabel to be 99+ if notif is more than 99', () => {
		wrapper.setData({ unreadChatCount: 120 });
		expect(wrapper.vm.unreadCountLabel).toBe('99+');
	});

	it('computed unreadCountLabel to be as it is if value is below 99', () => {
		wrapper.setData({ unreadChatCount: 50 });
		expect(wrapper.vm.unreadCountLabel).toBe(50);
	});

	it('computed helpCenterUrl to return help center owner if user is login and user is owner', () => {
		wrapper.setProps({ isLogin: true, isTenant: false });
		expect(wrapper.vm.helpCenterUrl).toEqual(
			'https://help.mamikos.com/booking/owner'
		);
	});

	it('computed helpCenterUrl to return help center tenant if user is login and user is tenant', () => {
		wrapper.setProps({ isLogin: true, isTenant: true });
		expect(wrapper.vm.helpCenterUrl).toEqual(
			'https://help.mamikos.com/booking/tenant'
		);
	});

	it('computed isLargeScreen should return true if window width is more than 991px', () => {
		global.innerWidth = 992;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		expect(wrapper.vm.isLargeScreen).toBe(true);
	});

	it('computed isLargeScreen should return false if window width is below or 991px', () => {
		global.innerWidth = 991;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		expect(wrapper.vm.isLargeScreen).toBe(false);
	});

	it('computed urlLoc should return window location if not include "ownerpage" or "user" in the url', () => {
		expect(wrapper.vm.urlLoc).toBe(window.location.href);
	});

	it('computed urlLoc should return null if url include "ownerpage" or "user" in the url', () => {
		const pathname = 'ownerpage/user';
		mockLocation(url, pathname);
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		expect(wrapper.vm.urlLoc).toBe(null);
		jest.clearAllMocks();
	});

	it('computed freeLabelPosition should return 12px if the page status is "isScrolled" and window width is more than 1091px', () => {
		global.innerWidth = 1366;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		wrapper.setData({ isScrolled: true });
		expect(wrapper.vm.freeLabelPosition).toEqual('12px');
	});

	it('computed freeLabelPosition should return 8px if the page status is "isScrolled" and window width is below 1091px', () => {
		global.innerWidth = 766;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		wrapper.setData({ isScrolled: true });
		expect(wrapper.vm.freeLabelPosition).toEqual('8px');
	});

	it('computed freeLabelPosition should return 12px if the page status is not "isScrolled"', () => {
		wrapper.setData({ isScrolled: false });
		expect(wrapper.vm.freeLabelPosition).toEqual('12px');
	});

	it('watch should add class navbar-fixed when status is "isScrolled" and not on largeScreen', () => {
		global.innerWidth = 767;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		wrapper.setData({ isScrolled: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.navbar-fixed').exists()).toBe(true);
		});
	});

	it('watch should remove class navbar-fixed when status is not "isScrolled" and not on largeScreen', async () => {
		global.innerWidth = 767;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		wrapper.setData({ isScrolled: true });
		wrapper.vm.$refs.homeNavbar.classList.add('navbar-fixed');
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.navbar-fixed').exists()).toBe(true);
		wrapper.setData({ isScrolled: false });
		wrapper.vm.$refs.homeNavbar.classList.remove('navbar-fixed');
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.navbar-fixed').exists()).toBe(false);
	});

	it('Should call toggleSidenav method on click button and set it to true', () => {
		global.innerWidth = 480;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const toggleSidenavSpy = jest.spyOn(wrapper.vm, 'toggleSidenav');
		const updateBodyOverflowSpy = jest.spyOn(wrapper.vm, 'updateBodyOverflow');
		wrapper.setMethods({ toggleSidenav: toggleSidenavSpy });
		expect(wrapper.find('[data-target="#toggleMenu"]').exists()).toBe(true);
		wrapper.find('[data-target="#toggleMenu"]').trigger('click');
		expect(toggleSidenavSpy).toBeCalled();
		wrapper.setData({ isSidenav: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isSidenav).toBe(true);
			expect(updateBodyOverflowSpy).toBeCalled();
			expect(document.body.style.overflow).toBe('hidden');
			toggleSidenavSpy.mockReset();
			updateBodyOverflowSpy.mockReset();
		});
	});

	it('Should call toggleSidenav method on click button and set it to false', () => {
		global.innerWidth = 480;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const toggleSidenavSpy = jest.spyOn(wrapper.vm, 'toggleSidenav');
		wrapper.setMethods({ toggleSidenav: toggleSidenavSpy });
		expect(wrapper.find('[data-target="#toggleMenu"]').exists()).toBe(true);
		wrapper.find('[data-target="#toggleMenu"]').trigger('click');
		expect(toggleSidenavSpy).toBeCalled();
		wrapper.setData({ isSidenav: false });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isSidenav).toBe(false);
			wrapper.vm.updateBodyOverflow();
			expect(document.body.style.overflow).toBe('inherit');
			toggleSidenavSpy.mockReset();
		});
	});

	it('Should open modal login with default when login button is clicked', () => {
		const openModalLoginSpy = jest.spyOn(wrapper.vm, 'openModalLogin');
		wrapper.setMethods({ openModalLogin: openModalLoginSpy });
		expect(wrapper.find('button.btn-login').exists()).toBe(true);
		wrapper.find('button.btn-login').trigger('click');
		expect(openModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLoginVisible).toBe(true);
		expect(wrapper.vm.modalLoginState).toBe('default');
	});

	it('Should open modal login with not default value when login button is clicked', () => {
		global.innerWidth = 480;
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const testCase = 'owner';
		const wrapperArray = wrapper.findAll('a');
		const lastLink = wrapperArray.at(wrapperArray.length - 1);
		const openModalLoginSpy = jest.spyOn(wrapper.vm, 'openModalLogin');
		wrapper.setMethods({ openModalLogin: openModalLoginSpy });
		expect(lastLink.exists()).toBe(true);
		wrapper
			.findAll('a')
			.filter(node => node.text() === 'Masuk Sebagai Pemilik')
			.at(0)
			.trigger('click');
		expect(openModalLoginSpy).toBeCalledWith(testCase);
		expect(wrapper.vm.isModalLoginVisible).toBe(true);
		expect(wrapper.vm.modalLoginState).toBe(testCase);
	});

	it('Should close modal login when click the close button', () => {
		const modalFormLoginStub = {
			name: 'child-component',
			template: `<a class="btn-close-overlay" @click="$emit('closeModal')"/>`
		};

		wrapper = shallowMount(Navbar, {
			localVue,
			stubs: {
				...stubs,
				'modal-form-login': modalFormLoginStub
			},
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const closeModalLoginSpy = jest.spyOn(wrapper.vm, 'closeModalLogin');
		wrapper.setMethods({ closeModalLogin: closeModalLoginSpy });
		expect(wrapper.find('a.btn-close-overlay').exists()).toBe(true);
		wrapper.find('a.btn-close-overlay').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(closeModalLoginSpy).toBeCalled();
			expect(wrapper.vm.isModalLoginVisible).toBe(false);
			expect(wrapper.vm.modalLoginState).toBe('default');
		});
	});

	it('Should remove listener on destroy', () => {
		const addEvent = jest
			.spyOn(global, 'addEventListener')
			.mockImplementation(() => {});
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs: {
				'notif-center': mockComponent
			},
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		expect(addEvent).toHaveBeenCalled();
		wrapper.destroy();
		expect(removeEvent).toHaveBeenCalled();
	});

	it('Should call handleScrollWindow and set scrolled to true on window scroll', () => {
		global.innerWidth = 488;
		global.pageYOffset = 1000;
		const map = {};
		window.addEventListener = jest.fn((event, cb) => {
			map[event] = cb;
		});
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		map.scroll({ scrollTo: 1000 });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isScrolled).toBe(true);
		});
	});

	it('Should call handleResizeWindow and updateBodyOverflow', () => {
		const map = {};
		window.addEventListener = jest.fn((event, cb) => {
			map[event] = cb;
		});
		global.innerWidth = 500;
		global.dispatchEvent(new Event('resize'));
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const updateBodyOverflowSpy = jest.spyOn(wrapper.vm, 'updateBodyOverflow');
		wrapper.setMethods({ updateBodyOverflow: updateBodyOverflowSpy });
		map.resize({ innerWidth: 500 });
		wrapper.vm.$nextTick(() => {
			expect(updateBodyOverflowSpy).toBeCalled();
		});
	});

	it('Should call handleResizeWindow and not updateBodyOverflow', () => {
		const map = {};
		window.addEventListener = jest.fn((event, cb) => {
			map[event] = cb;
		});
		global.innerWidth = 1200;
		global.dispatchEvent(new Event('resize'));
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const updateBodyOverflowSpy = jest.spyOn(wrapper.vm, 'updateBodyOverflow');
		wrapper.setMethods({ updateBodyOverflow: updateBodyOverflowSpy });
		map.resize({ innerWidth: 1200 });
		wrapper.vm.$nextTick(() => {
			expect(updateBodyOverflowSpy).not.toHaveBeenCalled();
		});
	});

	it('Should call shareThisPage method when click link on url location not null', () => {
		document.execCommand = jest.fn();
		window.getSelection = () => {
			return {
				removeAllRanges: () => {}
			};
		};
		mockLocation(url);
		const wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			},
			attachToDocument: true
		});
		const shareThisPageSpy = jest.spyOn(wrapper.vm, 'shareThisPage');
		wrapper.setMethods({ shareThisPage: shareThisPageSpy });
		wrapper
			.findAll('a')
			.filter(node => node.text() === 'Bagikan Halaman Ini')
			.at(0)
			.trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(shareThisPageSpy).toBeCalled();
		});
	});

	it('Should call shareThisPage method and open share navigator', () => {
		global.navigator.share = jest.fn();
		global.window.description = null;
		const pathname = '';
		mockLocation(url, pathname);
		const wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			},
			attachToDocument: true
		});
		const shareThisPageSpy = jest.spyOn(wrapper.vm, 'shareThisPage');
		wrapper.setMethods({ shareThisPage: shareThisPageSpy });
		wrapper
			.findAll('a')
			.filter(node => node.text() === 'Bagikan Halaman Ini')
			.at(0)
			.trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(shareThisPageSpy).toBeCalled();
			expect(wrapper.vm.navigator.share).toBeCalledWith({
				title: document.title,
				text: 'Cek halaman ini di Mamikos.com',
				url: wrapper.vm.urlLoc
			});
		});
	});

	it('Should call handleScrollWindow and set scrolled to false on window scroll', () => {
		global.innerWidth = 1366;
		global.pageYOffset = 0;
		const map = {};
		window.addEventListener = jest.fn((event, cb) => {
			map[event] = cb;
		});
		wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn()
			}
		});
		const handleScrollWindowSpy = jest.spyOn(wrapper.vm, 'handleScrollWindow');
		wrapper.setMethods({ handleScrollWindow: handleScrollWindowSpy });
		map.scroll({ scrollTo: 0 });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isScrolled).toBe(false);
		});
	});

	it('Should call openHandleChat method on tenant login when clicking chat', () => {
		const wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn(),
				isLogin: true,
				isTenant: true
			}
		});
		const handleOpenChatSpy = jest.spyOn(wrapper.vm, 'handleOpenChat');
		wrapper.setMethods({ handleOpenChat: handleOpenChatSpy });
		expect(wrapper.find('button[aria-label="chat"]').exists()).toBe(true);
		wrapper.find('button[aria-label="chat"]').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(handleOpenChatSpy).toBeCalled();
		});
	});

	it('Should cover getChatUnreadCount when mounting', () => {
		mockWindowProperty('sbWidget', {
			registerLocalHandlerCb: jest.fn((type, fn) => fn())
		});
		const testCase = [
			{
				error: false,
				expectCountChat: 10
			},
			{
				error: true,
				expectCountChat: 0
			}
		];
		for (const test of testCase) {
			mockWindowProperty('sb', {
				getTotalUnreadChannelCount: jest.fn(fn => fn(10, test.error))
			});
			wrapper = shallowMount(Navbar, {
				localVue,
				stubs,
				propsData: {
					openModalSearch: jest.fn(),
					isLogin: true,
					isTenant: true
				}
			});
			expect(getChatUnreadCountSpy).toBeCalled();
			expect(wrapper.vm.unreadChatCount).toEqual(test.expectCountChat);
		}
	});

	it('Should call handleOnLogout on click logout and disconnect sendbird', async () => {
		mockWindowProperty('sb', {
			disconnect: jest.fn(fn => fn())
		});
		shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn(),
				isLogin: false,
				isTenant: false
			}
		});
		const handleLogoutSpy = jest.spyOn(wrapper.vm, 'handleLogout');
		wrapper.setMethods({ handleLogout: handleLogoutSpy });
		wrapper.setProps({ isLogin: true, isTenant: true });
		await wrapper.vm.$nextTick();
		wrapper
			.findAll('a')
			.filter(node => node.text() === 'Keluar')
			.at(0)
			.trigger('click');

		expect(handleLogoutSpy).toBeCalled();
		expect(Cookies.remove).toBeCalled();
	});

	it('Should call handleOnLogout on click logout and disconnect sendbird but got catch error', async () => {
		mockWindowProperty('sb', {
			dummymethod: jest.fn()
		});
		const wrapper = shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn(),
				isLogin: false,
				isTenant: false
			}
		});
		const handleLogoutSpy = jest.spyOn(wrapper.vm, 'handleLogout');
		wrapper.setMethods({ handleLogout: handleLogoutSpy });
		wrapper.setProps({ isLogin: true, isTenant: true });
		await wrapper.vm.$nextTick();
		wrapper
			.findAll('a')
			.filter(node => node.text() === 'Keluar')
			.at(0)
			.trigger('click');

		expect(handleLogoutSpy).toBeCalled();
		expect(console.error).toBeCalled();
	});

	it('Should call handleOnLogout on click logout without disconnect sendbird', () => {
		global.sb = undefined;
		shallowMount(Navbar, {
			localVue,
			stubs,
			propsData: {
				openModalSearch: jest.fn(),
				isLogin: false,
				isTenant: false
			}
		});
		const handleLogoutSpy = jest.spyOn(wrapper.vm, 'handleLogout');
		wrapper.setMethods({ handleLogout: handleLogoutSpy });
		wrapper.setProps({ isLogin: true, isTenant: true });
		wrapper.vm.$nextTick(() => {
			wrapper
				.findAll('a')
				.filter(node => node.text() === 'Keluar')
				.at(0)
				.trigger('click');
			expect(handleLogoutSpy).toBeCalled();
		});
	});

	it('Should read getQueryString and set data correctly', () => {
		const testCase = [
			{
				all: true,
				loginQuery: null
			},
			{
				all: false,
				loginQuery: 'tenant'
			}
		];

		for (const test of testCase) {
			mockWindowProperty('authData', test);

			const wrapper = shallowMount(Navbar, {
				localVue,
				stubs,
				methods: {
					getQueryString: jest.fn().mockReturnValue(test.loginQuery)
				}
			});

			expect(wrapper.vm.modalLoginState).toBe(test.loginQuery || 'default');
			expect(wrapper.vm.isModalLoginVisible).toBe(!test.all);
		}
	});
});
