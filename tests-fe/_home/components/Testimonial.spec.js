import { shallowMount, createLocalVue } from '@vue/test-utils';
import Testimonial from 'Js/_home/components/Testimonial';
import LinkCTA from 'Js/@components/LinkCTA';
import Vuex from 'vuex';
import TestimonialData from './__mocks__/testimonialData.json';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
window.Vue = require('vue');
// eslint-disable-next-line
Vue.use(Vuex);
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

describe('Testimonial.vue', () => {
	const handleMoEngage = jest.fn();
	const localVue = createLocalVue();
	localVue.mixin(mixinNavigator);
	localVue.directive('lazy', (el, binding) => {
		return el.setAttribute('data-src', binding.value);
	});
	global.tracker = jest.fn();
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(Testimonial, {
			localVue,
			methods: {
				handleMoEngage
			},
			propsData: {
				testimonies: TestimonialData
			},
			stubs: {
				swiper,
				swiperSlide
			}
		});
	});

	it('should render testimonial section', () => {
		expect(wrapper.find('#testimonial').exists()).toBe(true);
		expect(wrapper.find('.testimonial-wrapper').exists()).toBe(true);
	});

	it('should render swiper section', () => {
		expect(wrapper.find('.testimonial-swiper-button-prev').exists()).toBe(true);
		expect(wrapper.find('.testimonial-swiper-button-next').exists()).toBe(true);
	});

	it('should render call to action section', () => {
		expect(wrapper.find('.testimonial-cta').exists()).toBe(true);
	});

	it('should render the same testimonial length as the passing props', () => {
		expect(wrapper.findAll('.testimonial-item').length).toEqual(
			wrapper.props('testimonies').length
		);
	});

	it('call register kos event on click link', () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				'link-c-t-a': LinkCTA,
				swiper,
				swiperSlide
			}
		});
		const spyUpdate = jest.spyOn(wrapper.vm, 'handleClickRegister');
		wrapper.setMethods({ handleClickRegister: spyUpdate });
		expect(wrapper.find('a.pasang-iklan-homepage').exists()).toBe(true);
		wrapper.find('a.pasang-iklan-homepage').trigger('click');
		expect(spyUpdate).toBeCalled();
		assignMock.mockClear();
		spyUpdate.mockReset();
	});

	it('should call moEngage on change slide', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		global.innerWidth = 1366;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.$refs.testimonial.swiper.realIndex).toBe(0);
		wrapper.vm.$refs.testimonial.swiper.slideNext();
		expect(wrapper.vm.$refs.testimonial.swiper.realIndex).toBe(1);
		wrapper.vm.handleMoEngage = jest.fn(null);
		wrapper.vm.handleMoEngage(null);
		expect(wrapper.vm.handleMoEngage).toBeCalled();
	});

	it('should set auto view slider on mobile screen and call moEngage method on mobile screen', () => {
		global.innerWidth = 766;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.swiperOptions.slidesPerView).toBe('auto');
		wrapper.vm.handleMoEngage = jest.fn();
		wrapper.vm.handleMoEngage(1);
		expect(wrapper.vm.handleMoEngage).toBeCalledWith(1);
	});

	it('computed isOwner should return false if user not owner', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.isOwner).toBe(false);
	});

	it('computed isOwner should return true if user is owner', () => {
		const authCheck = {
			all: true,
			owner: true,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('computed isLogin should return false if user not login', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.isLogin).toBe(false);
	});

	it('computed isLogin should return true if user is login', () => {
		const authCheck = {
			all: true,
			owner: false,
			user: true,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(Testimonial, {
			stubs: {
				swiper,
				swiperSlide
			}
		});
		expect(wrapper.vm.isLogin).toBe(true);
	});
});
