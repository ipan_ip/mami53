import { createLocalVue, shallowMount } from '@vue/test-utils';
import DraftBookingShortcut from 'Js/_home/components/draft-booking-shortcut/DraftBookingShortcut.vue';

const localVue = createLocalVue();

const returnedDraftShortcutData = {
	data: {
		data: {
			total: 1,
			room: {
				data: {
					name: 'Kos Sejahtera Sehat Sentosa Jaya Selalu Setia',
					type: 'campur',
					available_room: 5,
					price: {
						currency_symbol: 'Rp',
						price: '300.000',
						rent_type_unit: 'bulan'
					},
					image:
						'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-540x720.jpg',
					checkin: '2020-04-04',
					checkout: '2021-04-04',
					rent_duration: '12 Bulan',
					slug: 'kost-jayapura-kost-putri-murah-kost-garden-abepura-'
				}
			}
		},
		status: true
	}
};

const axios = {
	get: jest.fn().mockResolvedValue(returnedDraftShortcutData)
};

global.axios = axios;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

describe('DraftBookingShortcut.vue', () => {
	const stubs = {
		DraftBookingShortcutRoom: {
			template: `<div class="draft-booking-shortcut-room"></div>`
		},
		DraftBookingShortcutMany: {
			template: `<div class="draft-booking-shortcut-many"></div>`
		}
	};

	const mountComponent = () =>
		shallowMount(DraftBookingShortcut, { localVue, stubs });

	it('should render draft booking shortcut', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('#draftBookingShortcut').exists()).toBe(true);
	});

	it('should show section header properly if draft is only 1', async () => {
		const response = Object.assign({}, returnedDraftShortcutData);
		response.data.data.total = 1;
		global.axios = {
			get: jest.fn().mockResolvedValue(response)
		};
		const wrapper = mountComponent();
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.draft-booking-shortcut-room').exists()).toBe(true);
		expect(wrapper.find('h2').text()).toMatchSnapshot();
	});

	it('should show section header properly if draft is more than 1', async () => {
		const response = Object.assign({}, returnedDraftShortcutData);
		response.data.data.total = 2;
		global.axios = {
			get: jest.fn().mockResolvedValue(response)
		};
		const wrapper = mountComponent();
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.draft-booking-shortcut-many').exists()).toBe(true);
		expect(wrapper.find('h2').text()).toMatchSnapshot();
	});

	it('should not set draft if request status is false', async () => {
		const response = Object.assign({}, returnedDraftShortcutData);
		response.data.status = false;
		global.axios = {
			get: jest.fn().mockResolvedValue(response)
		};
		const wrapper = mountComponent();
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.draft-booking-shortcut-room').exists()).toBe(false);
		expect(wrapper.find('.draft-booking-shortcut-many').exists()).toBe(false);
		expect(wrapper.vm.totalDraft).toBe(0);
		expect(wrapper.vm.roomData).toBe(null);
	});

	it('should not set draft if request status is failed', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};
		const wrapper = mountComponent();
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.draft-booking-shortcut-room').exists()).toBe(false);
		expect(wrapper.find('.draft-booking-shortcut-many').exists()).toBe(false);
		expect(wrapper.vm.totalDraft).toBe(0);
		expect(wrapper.vm.roomData).toBe(null);
	});
});
