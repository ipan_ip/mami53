import { createLocalVue, shallowMount } from '@vue/test-utils';
import DraftBookingShortcutRoom from 'Js/_home/components/draft-booking-shortcut/DraftBookingShortcutRoom.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

const FlashSalePrice = { template: '<div/>' };
const flashSaleData = {
	is_flash_sale: true,
	price: {
		discount_price: 10000,
		currency_symbol: 'Rp',
		price: 20000,
		rent_type_unit: 'Bulan'
	}
};

describe('DraftBookingShortcutRoom.vue', () => {
	const room = {
		name: 'Kos Sejahtera Sehat Sentosa Jaya Selalu Setia',
		type: 'campur',
		available_room: 5,
		price: 'Rp300.000 Per Bulan',
		image:
			'https://static.mamikos.com/uploads/cache/data/style/2017-06-15/2zXplY9t-540x720.jpg',
		checkin: '2020-04-04',
		checkout: '2021-04-04',
		rent_duration: '12 Bulan',
		slug: 'kost-jayapura-kost-putri-murah-kost-garden-abepura-test'
	};

	const mountComponent = (customRoom = {}) => {
		return shallowMount(DraftBookingShortcutRoom, {
			localVue,
			propsData: {
				totalDraft: 1,
				room: { ...room, ...customRoom }
			},
			stubs: { FlashSalePrice }
		});
	};

	it('should render draft booking room', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('.draft-booking-card').exists()).toBe(true);
	});

	it('should render room available label properly', async () => {
		const wrapper = mountComponent();
		wrapper.setProps({ room: { ...wrapper.vm.room, available_room: 1 } });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.--room-available')).toMatchSnapshot();
		wrapper.setProps({ room: { ...wrapper.vm.room, available_room: 7 } });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.--room-available')).toMatchSnapshot();
	});

	it('should set date properly', async () => {
		const wrapper = mountComponent();
		expect(wrapper.vm.checkinDate).toMatchSnapshot();
		wrapper.setProps({ room: { ...wrapper.vm.room, checkin: '' } });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.checkinDate).toBe('');
	});

	it('should show normal price if as base if there is flash sale', () => {
		const wrapper = mountComponent(flashSaleData);
		expect(wrapper.findComponent(FlashSalePrice).attributes('price')).toBe(
			`${flashSaleData.price.currency_symbol}${flashSaleData.price.price}`
		);
	});

	it('should show kamar penuh if no room available', () => {
		const wrapper = mountComponent({ available_room: 0 });
		expect(wrapper.find('.--room-available').text()).toBe('Kamar Penuh');
	});

	it('should show subdistrict and city if given', () => {
		const wrapper = mountComponent({ subdistrict: 'Mlati', city: 'Sleman' });
		expect(
			wrapper
				.find('.draft-booking-card__kost-name')
				.text()
				.split(', ')
		).toEqual(
			expect.arrayContaining([
				expect.stringContaining('Mlati'),
				expect.stringContaining('Sleman')
			])
		);
	});
});
