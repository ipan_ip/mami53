import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetStatusAlert from 'Js/_home/components/DbetStatusAlert.vue';
import { flushPromises } from 'tests-fe/utils/promises';
import * as storage from 'Js/@utils/storage';

const localVue = createLocalVue();

const stubs = {
	BgAlert: { template: '<div><slot/><slot name="footer"/></div>' },
	BgLink: { template: `<button @click="$emit('click')"><slot/></button>` }
};

const axios = { get: jest.fn().mockRejectedValue(new Error('error')) };
global.axios = axios;

const sbWidget = { openChatRoom: jest.fn() };
global.sbWidget = sbWidget;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

describe('DbetStatusAlert.vue', () => {
	const createWrapper = () =>
		shallowMount(DbetStatusAlert, { localVue, stubs });

	const isExistAlert = wrapper => wrapper.find('.dbet-status-alert').exists();

	const getBgAlertAttr = (wrapper, attribute) =>
		wrapper.findComponent(stubs.BgAlert).attributes(attribute);

	it('should not show alert when status is empty or invalid', async () => {
		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(false);
	});

	it('should not show alert if status is invalid', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'invalid', id: 888 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(false);
	});

	it('should show alert when status is pending', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'pending', id: 123 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(true);
		expect(getBgAlertAttr(wrapper, 'variant')).toBe('info');
		expect(storage.local.getItem('home_dbet_alert_close')).toBeFalsy();
	});

	it('should show alert when status is rejected and not closed yet', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'rejected', id: 123 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(true);
		expect(getBgAlertAttr(wrapper, 'variant')).toBe('error');
		expect(storage.local.getItem('home_dbet_alert_close')).toBeFalsy();
	});

	it('should hide alert and set localStorage state if alert rejected closed', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'rejected', id: 999 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		wrapper.findComponent(stubs.BgAlert).vm.$emit('close');
		await wrapper.vm.$nextTick();

		expect(isExistAlert(wrapper)).toBe(false);
		expect(storage.local.getItem('home_dbet_alert_close')).toBe('999');
	});

	it('should remove existing home_dbet_alert_close and show alert if dbet id request changed', async () => {
		storage.local.setItem('home_dbet_alert_close', '123098');

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'rejected', id: 888 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(true);
		expect(storage.local.getItem('home_dbet_alert_close')).toBeFalsy();
	});

	it('should not show alert if home_dbet_alert_close is exist with current id', async () => {
		storage.local.setItem('home_dbet_alert_close', '123098');

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: true, data: { status: 'rejected', id: 123098 } }
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(isExistAlert(wrapper)).toBe(false);
	});

	it('should open group channel when button chat clicked', async () => {
		const groupChannelUrl = 'sendbird_group_channel_url_123';
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: 'pending',
						id: 888,
						group_channel_url: groupChannelUrl
					}
				}
			})
		};

		const wrapper = createWrapper();
		await flushPromises();
		wrapper.findComponent(stubs.BgLink).vm.$emit('click');

		expect(sbWidget.openChatRoom).toBeCalledWith(groupChannelUrl);
	});

	it('should not call open group channel when button chat clicked if is empty', async () => {
		jest.clearAllMocks();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: 'pending',
						id: 888,
						group_channel_url: ''
					}
				}
			})
		};

		const wrapper = createWrapper();
		await flushPromises();
		wrapper.findComponent(stubs.BgLink).vm.$emit('click');

		expect(sbWidget.openChatRoom).not.toBeCalled();
	});

	it('should not set anything and not show alert if get dbet request status return falsy status', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					data: {
						status: 'rejected',
						id: 999,
						group_channel_url: 'sendbird_group_channel_url_999'
					}
				}
			})
		};

		const wrapper = createWrapper();
		await flushPromises();

		expect(wrapper.vm.status).toBe('');
		expect(wrapper.vm.groupChannelUrl).toBe('');
		expect(wrapper.vm.dbetRequestId).toBe('');
		expect(isExistAlert(wrapper)).toBe(false);
	});
});
