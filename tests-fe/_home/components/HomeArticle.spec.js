import { createLocalVue, shallowMount } from '@vue/test-utils';
import HomeArticle from 'Js/_home/components/HomeArticle.vue';

const localVue = createLocalVue();

describe('HomeArticle.vue', () => {
	const wrapper = shallowMount(HomeArticle, { localVue });
	it('should render article section', () => {
		expect(wrapper.find('section.article').exists()).toBe(true);
	});
	it('should render properly', () => {
		const articleMobile = wrapper.find('.article-mobile');
		expect(articleMobile.exists() && articleMobile.text().length > 0).toBe(
			true
		);
		const articleNonMobile = wrapper.find('.article-mobile + div');
		expect(
			articleNonMobile.exists() && articleNonMobile.text().length > 0
		).toBe(true);
	});
	it('should show complain anchor tag', () => {
		expect(
			wrapper.find('.article > .container-fluid a').attributes().href
		).toBe('http://bit.ly/komplainform');
	});
});
