import { shallowMount, createLocalVue } from '@vue/test-utils';
import PopularArea from 'Js/_home/components/PopularArea';
window.Vue = require('vue');
// eslint-disable-next-line
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

describe('PopularArea.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin(mixinNavigator);
	localVue.directive('lazy', (el, binding) => {
		return el.setAttribute('data-src', binding.value);
	});
	let wrapper;
	global.tracker = jest.fn();
	beforeEach(() => {
		wrapper = shallowMount(PopularArea, {
			localVue
		});
	});

	it('computed isOwner should return false if user not owner', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.isOwner).toBe(false);
	});

	it('computed isOwner should return true if user is owner', () => {
		const authCheck = {
			all: true,
			owner: true,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('computed isLogin should return false if user not login', () => {
		const authCheck = {
			all: false,
			owner: false,
			user: false,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.isLogin).toBe(false);
	});

	it('computed isLogin should return true if user is login', () => {
		const authCheck = {
			all: true,
			owner: false,
			user: true,
			admin: false
		};
		global.authCheck = authCheck;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.isLogin).toBe(true);
	});

	it('computed imgPrefixPath should return mobile asset on mobile screen', () => {
		global.innerWidth = 766;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.imgPrefixPath).toEqual('/assets/area/mobile/');
	});

	it('computed imgPrefixPath should return desktop asset on large screen', () => {
		global.innerWidth = 1366;
		const wrapper = shallowMount(PopularArea, {
			localVue
		});
		expect(wrapper.vm.imgPrefixPath).toEqual('/assets/area/desktop/');
	});

	it('should render area card section', () => {
		expect(wrapper.find('.popular-area-cards').exists()).toBe(true);
	});

	it('should render all area card according to area data', () => {
		expect(wrapper.findAll('a.popular-area-card-item').length).toEqual(
			wrapper.vm.areas.length
		);
	});

	it('should call moEngage on click area', () => {
		const testCase = {
			name: 'Kos Yogyakarta',
			imgSrc: 'jogja.png',
			url: '/kost/kost-jogja-murah'
		};
		expect(wrapper.findAll('a.popular-area-card-item').length).toEqual(
			wrapper.vm.areas.length
		);
		const spyUpdate = jest.spyOn(wrapper.vm, 'handleMoEngage');
		wrapper.setMethods({ handleMoEngage: spyUpdate });
		expect(wrapper.find('a.popular-area-card-item').exists()).toBe(true);
		wrapper.find('a.popular-area-card-item').trigger('click');
		expect(spyUpdate).toBeCalledWith(testCase);
	});
});
