window.Moengage = {
  track_event: (eventName, eventData) => {
    return { [eventName]: eventData }
  }
}