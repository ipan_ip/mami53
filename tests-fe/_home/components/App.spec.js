import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import 'Moengage';
import '@babel/polyfill';
import '../../utils/mock-vue';
import App from 'Js/_home/components/App.vue';
import mockComponent from '../../utils/mock-component';
import bannerData from './__mocks__/bannerData.json';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
const notify = jest.fn();
mockWindowProperty('bugsnagClient', {
	notify
});
mockWindowProperty('ga', jest.fn());
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('innerWidth', 1366);
global.axios = mockAxios;

describe('App.vue', () => {
	mockWindowProperty('authCheck', {
		all: false,
		owner: true,
		user: false
	});
	mockWindowProperty('authData', {
		all: null
	});

	const setLeftPointSpy = jest.spyOn(App.methods, 'setLeftPoint');
	const handleClickBannerSpy = jest.spyOn(App.methods, 'handleClickBanner');
	const closeModalLoginSpy = jest.spyOn(App.methods, 'closeModalLogin');
	const openModalLoginSpy = jest.spyOn(App.methods, 'openModalLogin');
	const closeSearchSpy = jest.spyOn(App.methods, 'closeSearchModal');
	const openSearchModalSpy = jest.spyOn(App.methods, 'openSearchModal');

	const componentToBeStub = {
		MamiFooter: mockComponent,
		SearchBoxModal: mockComponent,
		GlobalNavbar: mockComponent,
		LoginUser: mockComponent,
		HomeArticle: mockComponent,
		KostNearCampus: mockComponent,
		LoginOwnerCTA: mockComponent,
		PopularArea: mockComponent,
		ProductSearch: mockComponent,
		Promoted: mockComponent,
		Recommendation: mockComponent,
		Testimonial: mockComponent,
		swiper,
		swiperSlide
	};
	const homeTopSectionStub = {
		template: `<div><a class="link-promo-banner" @click.prevent="onClickBanner({scheme: 'https://mamikos.com', title: 'title banner'}, 1)" /><button class="open-search-modal" @click="openModalSearch"/></div>`,
		props: ['onClickBanner', 'openModalSearch']
	};
	const searchBoxModalStub = {
		template: `<button type="button" class="close-search-input" @click="$emit('hide-modal')" />`
	};
	let wrapper;
	beforeEach(() => {
		axios.mockResolve({
			data: {
				status: 200,
				data: bannerData
			}
		});
		wrapper = shallowMount(App, {
			localVue,
			stubs: {
				...componentToBeStub,
				'promo-banner': homeTopSectionStub,
				'search-box-modal': searchBoxModalStub
			},
			methods: {
				MamiFooter: jest.fn()
			}
		});
	});

	it('should call method openSearchModal correctly', async () => {
		const openSearchElement = wrapper.find('button.open-search-modal');

		expect(openSearchElement.exists()).toBe(true);

		await openSearchElement.trigger('click');

		expect(openSearchModalSpy).toBeCalled();
	});

	it('computed isOwner should return false if not owner', () => {
		mockWindowProperty('authCheck', {
			all: false,
			owner: false,
			user: false
		});
		const wrapper = shallowMount(App, {
			stubs: componentToBeStub
		});

		expect(wrapper.vm.isOwner).toBe(false);
	});

	it('computed isOwner should return true if owner', () => {
		mockWindowProperty('authCheck', {
			all: false,
			owner: true,
			user: false
		});
		const wrapper = shallowMount(App, {
			localVue,
			stubs: componentToBeStub
		});

		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('should call bugsnagClient notify when fetch data', () => {
		axios.mockReject('error');
		const wrapper = shallowMount(App, {
			localVue,
			stubs: componentToBeStub
		});
		wrapper.vm.getPromoBanners();

		expect(wrapper.vm.promoBanners).toEqual([]);
	});

	it('should call handle banner on click with owner true tracker', async () => {
		mockWindowProperty('authCheck', {
			all: true,
			owner: true,
			user: false
		});
		const wrapper = shallowMount(App, {
			localVue,
			stubs: {
				...componentToBeStub,
				'promo-banner': homeTopSectionStub,
				'search-box-modal': searchBoxModalStub
			}
		});
		const assignMock = jest.fn();
		delete window.location;
		window.location = {
			assign: assignMock
		};

		expect(wrapper.find('a.link-promo-banner').exists()).toBe(true);

		await wrapper.find('a.link-promo-banner').trigger('click');

		expect(wrapper.vm.isLogin).toBe(true);
		expect(wrapper.vm.isOwner).toBe(true);
		expect(handleClickBannerSpy).toBeCalled();
		expect(tracker).toBeCalled();

		assignMock.mockClear();
	});

	it('should call handle banner on click', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };

		expect(wrapper.find('a.link-promo-banner').exists()).toBe(true);

		await wrapper.find('a.link-promo-banner').trigger('click');

		expect(handleClickBannerSpy).toBeCalled();
		expect(tracker).toBeCalledWith('moe', [
			'Homepage',
			{
				interface: 'desktop',
				is_login: false,
				is_owner: null,
				user_location_city: ''
			}
		]);

		assignMock.mockClear();
	});

	it('computed isLogin should return true if login', () => {
		mockWindowProperty('innerWidth', 500);
		mockWindowProperty('authCheck', {
			all: true,
			owner: true,
			user: false
		});
		const wrapper = shallowMount(App, {
			localVue,
			stubs: componentToBeStub
		});

		expect(wrapper.vm.isLogin).toBe(true);
	});

	it('should call setleftpoint method correctly', async () => {
		const globalNavbarStub = {
			template: `<button @click="$emit('setLeftPoint', 10)" class="set-left-point"></button>`
		};

		const wrapper = shallowMount(App, {
			localVue,
			stubs: {
				...componentToBeStub,
				'global-navbar': globalNavbarStub
			}
		});

		const setLeftPointElement = wrapper.find('button.set-left-point');

		expect(setLeftPointElement.exists()).toBe(true);

		await setLeftPointElement.trigger('click');

		expect(setLeftPointSpy).toBeCalledWith(10);
		expect(wrapper.vm.leftPoint).toBe(10);
	});

	it('should call toggle modal login method correctly', async () => {
		const loginUserStub = {
			template: `<button @click="$emit('close')" class="close-modal"></button>`
		};

		const flashSaleStub = {
			template: `<button @click="$emit('toggleLoginModal')" class="open-modal"></button></div>`
		};

		const wrapper = shallowMount(App, {
			localVue,
			stubs: {
				...componentToBeStub,
				'login-user': loginUserStub,
				'flash-sale': flashSaleStub
			}
		});

		const closeModalElement = wrapper.find('button.close-modal');
		const openModalElement = wrapper.find('button.open-modal');

		expect(closeModalElement.exists()).toBe(true);
		expect(openModalElement.exists()).toBe(true);

		await openModalElement.trigger('click');

		expect(openModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(true);

		await closeModalElement.trigger('click');

		expect(closeModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(false);
	});

	it('should call toggle search box method correctly', async () => {
		const searchBoxStub = {
			template: `<button @click="$emit('hide-modal')" class="hide-search-modal"></button>`
		};

		mockWindowProperty('innerWidth', 1366);

		const wrapper = shallowMount(App, {
			localVue,
			stubs: {
				...componentToBeStub,
				'search-box-modal': searchBoxStub
			}
		});

		const closeModalElement = wrapper.find('button.hide-search-modal');

		expect(closeModalElement.exists()).toBe(true);

		await closeModalElement.trigger('click');

		expect(closeSearchSpy).toBeCalled();
		expect(wrapper.vm.isSearchModalShow).toBe(false);
	});
});
