import { shallowMount, createLocalVue } from '@vue/test-utils';
import FlashSale from 'Js/_home/components/desktop/FlashSale';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinFlashSaleData from 'Js/_home/components/mixins/MixinFlashSaleData';
import MixinFlashSaleHelper from 'Js/_flash-sale/mixins/MixinFlashSale';
window.Vue = require('vue');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

describe('FlashSale.vue', () => {
	mockWindowProperty('axios', mockAxios);
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	const localVue = createLocalVue();
	localVue.mixin([mixinNavigator, MixinFlashSaleData, MixinFlashSaleHelper]);

	const wrapper = shallowMount(FlashSale, {
		localVue
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
