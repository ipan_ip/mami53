import { shallowMount } from '@vue/test-utils';
import PromoBanner from 'Js/_home/components/desktop/PromoBanner';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import BannerData from '../__mocks__/bannerData';
import '@babel/polyfill';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('PromoBanner.vue', () => {
	mockWindowProperty('authCheck', {
		owner: false
	});
	mockWindowProperty('tracker', jest.fn());
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(PromoBanner, {
			stubs: {
				swiper,
				swiperSlide
			},
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn()
			}
		});
	});

	it('Computed should return false on isOwner if not owner', () => {
		expect(wrapper.vm.isOwner).toBe(false);
	});

	it('Computed should return true on isOwner if user is owner', () => {
		mockWindowProperty('authCheck', {
			owner: true
		});
		const wrapper = shallowMount(PromoBanner, {
			stubs: {
				swiper,
				swiperSlide
			},
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn()
			}
		});
		expect(wrapper.vm.isOwner).toBe(true);
	});

	it('Should call handleClickSeeAll on click', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = {
			assign: assignMock
		};
		const wrapper = shallowMount(PromoBanner, {
			stubs: {
				swiper,
				swiperSlide
			},
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn()
			}
		});
		const handleClickSeeAllSpy = jest.spyOn(wrapper.vm, 'handleClickSeeAll');
		wrapper.setMethods({ handleClickSeeAll: handleClickSeeAllSpy });
		expect(wrapper.find('a.semua-promo-slide').exists()).toBe(true);
		wrapper.find('a.semua-promo-slide').trigger('click');
		await wrapper.vm.$nextTick();
		expect(handleClickSeeAllSpy).toBeCalled();
		assignMock.mockClear();
	});

	it('Should show the same lenght banenr with the props that passed', async () => {
		wrapper.vm.handleErrorImage({
			target: {
				src: '/general/img/pictures/placeholder/placeholder_loading.svg',
				style: {}
			}
		});
		expect(wrapper.findAll('a.banner-click-homepage').length).toEqual(
			wrapper.vm.banners.length
		);
	});
});
