import '@babel/polyfill';
import mockAxios from 'tests-fe/utils/mock-axios';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Recommendation from 'Js/_home/components/desktop/Recommendation';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import recommendationData from '../__mocks__/RecommendationData';
import MixinRecommendation from 'Js/_home/components/mixins/MixinRecommendation';
import Vuex from 'vuex';

window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
describe('Recommendation.vue', () => {
	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	mockWindowProperty('tracker', jest.fn());
	mockWindowProperty('cityOptions', recommendationData.cityOptions);
	mockWindowProperty('cityOption', recommendationData.cityOption);
	mockWindowProperty(
		'initialStaticRooms',
		recommendationData.initialStaticRooms
	);
	mockWindowProperty('authCheck', {
		owner: false,
		all: false
	});
	mockWindowProperty('authData', {
		all: null
	});
	const querySelector = jest.fn(() => {
		return {
			content: 'ASD1720980ASKJDSADAD'
		};
	});
	mockWindowProperty('document.head.querySelector', querySelector);
	const localVue = createLocalVue();
	localVue.mixin([
		mixinNavigator,
		MixinRecommendation,
		MixinSwalLoading,
		MixinSwalSuccessError
	]);
	const favComponentStub = {
		template: `<button class="fav-click" @click="$emit('toggleLove', {})"/>`
	};
	global.axios = mockAxios;
	let wrapper;
	const $toasted = {
		show: jest.fn()
	};
	const stubComponent = {
		swiper,
		swiperSlide,
		'room-card': favComponentStub
	};
	const mocks = {
		$toasted
	};
	beforeEach(() => {
		wrapper = shallowMount(Recommendation, {
			localVue,
			stubs: stubComponent,
			mocks,
			data() {
				return {
					selectedLocation: {
						name: 'Jakarta'
					},
					recommendationCities: [
						{
							name: 'Jakarta'
						},
						{
							name: 'Bandung'
						}
					]
				};
			}
		});
	});

	it('Should render option of city correctly', () => {
		expect(wrapper.find('#userLocation').exists()).toBe(true);
		expect(wrapper.findAll('option').length).toEqual(
			wrapper.vm.recommendationCities.length
		);
	});

	it('Computed token should return correctly', () => {
		expect(wrapper.vm.token).toEqual('ASD1720980ASKJDSADAD');
	});

	it('Computed authData should return global authData if store not exist', () => {
		expect(wrapper.vm.authData).toEqual(authData);
	});

	it('Computed authCheck should return global authCheck if store not exist', () => {
		expect(wrapper.vm.authCheck).toEqual(authCheck);
	});

	it('Computed authCheck should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(Recommendation, {
			localVue,
			store,
			stubs: stubComponent
		});
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('Computed authData should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(Recommendation, {
			localVue,
			store,
			stubs: stubComponent
		});
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('Should call method loveThisPage on click favorit', async () => {
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		wrapper.setMethods({ loveThisPage: loveThisPageSpy });
		await wrapper.vm.$nextTick();
		await wrapper.setData({
			isLoading: false,
			isEmptyList: false,
			recommendedRooms: recommendationData.roomData
		});
		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(wrapper.emitted('toggleLoginModal')).toBeTruthy();
	});

	it('Should call method loveThisPage and return error if not user', async () => {
		mockWindowProperty('authCheck', {
			owner: true,
			user: false,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});

		wrapper = shallowMount(Recommendation, {
			localVue,
			stubs: stubComponent
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			swalError: swalErrorSpy
		});
		await wrapper.vm.$nextTick();
		await wrapper.setData({
			isLoading: false,
			isEmptyList: false,
			recommendedRooms: recommendationData.roomData
		});
		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
		);
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into if statement', async () => {
		mockWindowProperty('authCheck', {
			owner: false,
			user: true,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});
		axios.mockResolve({
			data: {
				status: 200,
				success: 'love'
			}
		});
		wrapper = shallowMount(Recommendation, {
			localVue,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();
		await wrapper.setData({
			isLoading: false,
			isEmptyList: false,
			recommendedRooms: recommendationData.roomData
		});
		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
	});

	it('Should call method loveThisPage and return false', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();
		await wrapper.setData({
			isLoading: false,
			isEmptyList: false,
			recommendedRooms: recommendationData.roomData
		});
		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).toBeCalledWith('Gagal, silahkan coba lagi', {
			className: 'fav-share-widget-custom-toast-fail',
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('Should call method loveThisPage and call catch statement', async () => {
		axios.mockReject('error');
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();
		await wrapper.setData({
			isLoading: false,
			isEmptyList: false,
			recommendedRooms: recommendationData.roomData
		});
		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
	});
});
