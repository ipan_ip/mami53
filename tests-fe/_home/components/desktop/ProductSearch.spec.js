import { shallowMount } from '@vue/test-utils';
import ProductSearch from 'Js/_home/components/desktop/ProductSearch';
import '@babel/polyfill';

describe('ProductSearch.vue', () => {
	let wrapper;
	const openModalSearchSpy = jest.fn();
	beforeEach(() => {
		wrapper = shallowMount(ProductSearch, {
			propsData: {
				openModalSearch: openModalSearchSpy
			}
		});
	});
	it('Should render cari button', () => {
		expect(
			wrapper
				.findAll('button')
				.filter(node => node.text() === 'Ketik nama lokasi/ area/ alamat')
				.at(0)
				.exists()
		).toBe(true);
	});
	it('Should call openModalSearch function when click the button', () => {
		expect(
			wrapper
				.findAll('button')
				.filter(node => node.text() === 'Ketik nama lokasi/ area/ alamat')
				.at(0)
				.exists()
		).toBe(true);
		wrapper
			.findAll('button')
			.filter(node => node.text() === 'Ketik nama lokasi/ area/ alamat')
			.at(0)
			.trigger('click');
		expect(openModalSearchSpy).toBeCalled();
	});
});
