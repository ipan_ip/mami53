import { shallowMount } from '@vue/test-utils';
import HomeTopSection from 'Js/_home/components/desktop/HomeTopSection';
import ProductSearchInput from 'Js/@components/ProductSearchInput';
import BannerData from '../__mocks__/bannerData';
import '@babel/polyfill';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('HomeTopSection.vue', () => {
	it('Should remove listener on destroy', () => {
		const addEvent = jest
			.spyOn(global, 'addEventListener')
			.mockImplementation(() => {});
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});
		const wrapper = shallowMount(HomeTopSection, {
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn(),
				openModalSearch: jest.fn(),
				leftPoint: 0
			}
		});
		expect(addEvent).toHaveBeenCalled();
		wrapper.destroy();
		expect(removeEvent).toHaveBeenCalled();
	});

	it('Should call handleScroll on scroll and set custom class on different condition with platform win', async () => {
		mockWindowProperty('pageYOffset', 1000);
		mockWindowProperty('navigator.platform', 'win');
		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});
		const wrapper = shallowMount(HomeTopSection, {
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn(),
				openModalSearch: jest.fn(),
				leftPoint: 0
			},
			stubs: {
				'product-search-input': ProductSearchInput
			}
		});
		wrapper.vm.$refs.btnCTATag.getBoundingClientRect = jest.fn(() => ({
			top: -42
		}));
		mockMapEventListener.scroll({ scrollTo: 1000 });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.--custom-sticky').exists()).toBe(true);
		wrapper.vm.$refs.btnCTATag.getBoundingClientRect = jest.fn(() => ({
			top: 0
		}));

		mockMapEventListener.scroll({ scrollTo: 0 });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.--custom-sticky').exists()).toBe(false);
	});

	it('Should call handleScroll on scroll and set custom class on different condition with platform mac', async () => {
		mockWindowProperty('pageYOffset', 1000);
		mockWindowProperty('navigator.platform', 'mac');
		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});
		const wrapper = shallowMount(HomeTopSection, {
			propsData: {
				banners: BannerData.homes.posters,
				onClickBanner: jest.fn(),
				openModalSearch: jest.fn(),
				leftPoint: 0
			},
			stubs: {
				'product-search-input': ProductSearchInput
			}
		});
		wrapper.vm.$refs.btnCTATag.getBoundingClientRect = jest.fn(() => ({
			top: -42
		}));
		mockMapEventListener.scroll({ scrollTo: 1000 });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.--custom-sticky').exists()).toBe(true);
	});
});
