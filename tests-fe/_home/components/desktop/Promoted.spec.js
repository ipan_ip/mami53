import { shallowMount, createLocalVue } from '@vue/test-utils';
import Promoted from 'Js/_home/components/desktop/Promoted';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { translateWords } from 'Js/@utils/langTranslator.js';
import promotedData from '../__mocks__/promotedData';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinPromoted from 'Js/_home/components/mixins/MixinPromoted';
window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
jest.mock('Js/@utils/makeAPICall.js');
jest.mock('Js/@utils/langTranslator.js');
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('axios', {
	get: jest.fn(() => {
		return Promise.resolve({
			data: {
				cities: []
			}
		});
	}),
	post: jest.fn(() => {
		return Promise.resolve({
			data: {
				total_running: 0
			}
		});
	})
});

describe('Promoted.vue', () => {
	mockWindowProperty('authCheck', {
		owner: false,
		all: false
	});
	mockWindowProperty('authData', {
		all: null
	});
	const querySelector = jest.fn(() => {
		return {
			content: 'ASD1720980ASKJDSADAD'
		};
	});
	mockWindowProperty('document.head.querySelector', querySelector);
	const localVue = createLocalVue();
	localVue.mixin([
		mixinNavigator,
		MixinPromoted,
		MixinSwalLoading,
		MixinSwalSuccessError
	]);
	const $toasted = {
		show: jest.fn()
	};
	const favComponentStub = {
		template: `<button class="fav-click" @click="$emit('toggleLove', {})" />`
	};
	const stubComponent = {
		swiper,
		swiperSlide,
		'room-card': favComponentStub
	};
	const mocks = {
		$toasted
	};

	const store = new Vuex.Store({
		state: {
			authCheck: {
				owner: false,
				all: false
			},
			authData: {
				all: null
			}
		}
	});

	let wrapper = shallowMount(Promoted, {
		localVue,
		store,
		stubs: stubComponent,
		mocks,
		data() {
			return {
				promotedRooms: promotedData,
				isLoading: false,
				isEmptyList: false
			};
		}
	});

	it('Should render option of city correctly', () => {
		expect(wrapper.find('.promoted').exists()).toBe(true);
		expect(wrapper.findAll('option').length).toEqual(
			wrapper.vm.locationOptions.length
		);
	});

	it('Computed token should return correctly', () => {
		expect(wrapper.vm.token).toEqual('ASD1720980ASKJDSADAD');
	});

	it('Computed authData should return global authData if store not exist', () => {
		expect(wrapper.vm.authData).toEqual(authData);
	});

	it('Computed authCheck should return global authCheck if store not exist', () => {
		expect(wrapper.vm.authCheck).toEqual(authCheck);
	});

	it('Computed authCheck should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			store,
			stubs: stubComponent,
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('Computed authData should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			store,
			stubs: stubComponent,
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('Should call method loveThisPage on click favorit', async () => {
		const APIResponse = {
			rooms: [
				{
					id: 1,
					name: 'room1'
				},
				{
					id: 2,
					name: 'room2'
				}
			]
		};
		makeAPICall.mockImplementation(() => APIResponse);
		translateWords.mockImplementation(() => APIResponse);

		wrapper = shallowMount(Promoted, {
			localVue,
			store,
			stubs: stubComponent,
			data() {
				return {
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		wrapper.setMethods({ loveThisPage: loveThisPageSpy });
		await wrapper.vm.$nextTick();
		await wrapper.setData({ promotedRooms: promotedData });
		const favElement = wrapper.findAll('button.fav-click').at(0);

		expect(favElement.exists()).toBe(true);

		favElement.trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(wrapper.emitted('toggleLoginModal')).toBeTruthy();
	});

	it('Should call method loveThisPage and return error if not user', async () => {
		mockWindowProperty('authCheck', {
			owner: true,
			user: false,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});

		wrapper = shallowMount(Promoted, {
			localVue,
			stubs: stubComponent,
			mocks,
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			swalError: swalErrorSpy
		});

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
		);
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into if statement', async () => {
		mockWindowProperty('authCheck', {
			owner: false,
			user: true,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						cities: []
					}
				});
			}),
			post: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: 200,
						success: 'love'
					}
				});
			})
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			},
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		wrapper.setData({ isEmptyList: false });
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into else if statement', async () => {
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						cities: []
					}
				});
			}),
			post: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: 200,
						success: 'unlove'
					}
				});
			})
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			},
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});

		wrapper.setData({ isEmptyList: false });
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).not.toHaveBeenCalled();
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into else statement', async () => {
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						cities: []
					}
				});
			}),
			post: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: 200,
						success: 'typo'
					}
				});
			})
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			},
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});
		wrapper.setData({ isEmptyList: false });
		wrapper.setData({ promotedRooms: promotedData });
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).not.toHaveBeenCalled();
	});

	it('Should call method loveThisPage and return false', async () => {
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						cities: []
					}
				});
			}),
			post: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: false
					}
				});
			})
		});
		wrapper = shallowMount(Promoted, {
			localVue,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			},
			data() {
				return {
					promotedRooms: promotedData,
					isLoading: false,
					isEmptyList: false
				};
			}
		});

		wrapper.setData({ isEmptyList: false });
		wrapper.setData({ promotedRooms: promotedData });
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
	});
});
