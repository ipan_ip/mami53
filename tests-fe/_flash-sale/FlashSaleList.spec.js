import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockComponent from 'tests-fe/utils/mock-component';
import flashSaleRoomsData from './__mocks__/flashSaleEncryptionData.json';
import FlashSaleStore from './__mocks__/flashSaleStore';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import FlashSaleList from 'Js/_flash-sale/FlashSaleList';
import mixinFlashSale from 'Js/_flash-sale/mixins/MixinFlashSale';
/* eslint-disable-next-line */
window.Vue = require('vue');
// eslint-disable-next-line
Vue.use(Vuex);
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin([mixinNavigator, mixinFlashSale]);
localVue.use(Vuex);
const notify = jest.fn();
mockWindowProperty('window.scrollTo', jest.fn());
mockWindowProperty('bugsnagClient', { notify });
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('window.Android', {
	trackDiscountKosLPVisited: jest.fn()
});
const store = new Vuex.Store(FlashSaleStore);
describe('App.vue', () => {
	global.axios = mockAxios;
	axios.mockResolve({
		data: {
			status: 200,
			rooms: flashSaleRoomsData.encryptedData
		}
	});
	const loginStubs = {
		template: `<button class="close-login" @click="$emit('close')"></button>`
	};
	const roomListStubs = {
		template: `<button class="open-login" @click="$emit('toggleLoginModal')"></button>`
	};
	const paginationStubs = {
		template: `<button class="update-page" @click="clickHandler(2)"></button>`,
		props: ['clickHandler']
	};
	const filterDesktopStubs = {
		template: `<div><button class="update-location" @click="$emit('locationHandler', 1)"></button><button class="search-handler" @click="$emit('searchHandler',{
			selectedLocationPayload: 1,
			filterPayload: {
				gender: [0, 1, 2],
				rent_type: 2,
				property_type: 'kos',
				price_range: [0, 1500000]
			},
			sortingPayload: ['price', 'asc']
		})"></button></div>`
	};
	const stubs = {
		'login-user': loginStubs,
		'room-list': roomListStubs,
		paginate: paginationStubs,
		'filter-desktop': filterDesktopStubs,
		'filter-mobile': mockComponent
	};
	const mocks = {
		$route: {
			params: {},
			query: {
				redirection_source: 'top'
			}
		}
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(FlashSaleList, {
			localVue,
			store,
			stubs,
			mocks,
			computed: {
				getRooms() {
					return ['room1', 'room2'];
				}
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setListLoading', false);
		wrapper.vm.$store.commit('setFlashSaleStatus', 'running');
	});

	it('Should call method toggleLoginModal and closeLoginModal correctly', async () => {
		const toggleLoginModalSpy = jest.spyOn(wrapper.vm, 'toggleLoginModal');
		const closeModalLoginSpy = jest.spyOn(wrapper.vm, 'closeModalLogin');
		wrapper.setMethods({
			toggleLoginModal: toggleLoginModalSpy,
			closeModalLogin: closeModalLoginSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.open-login').exists()).toBe(true);

		wrapper.find('button.open-login').trigger('click');
		await wrapper.vm.$nextTick();

		expect(toggleLoginModalSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(true);
		expect(wrapper.find('button.close-login').exists()).toBe(true);

		wrapper.find('button.close-login').trigger('click');
		await wrapper.vm.$nextTick();

		expect(closeModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(false);
	});

	it('Should call method pageChangeHandler correctly', async () => {
		const pageChangeHandlerSpy = jest.spyOn(wrapper.vm, 'pageChangeHandler');
		wrapper.setMethods({ pageChangeHandler: pageChangeHandlerSpy });

		expect(wrapper.find('button.update-page').exists()).toBe(true);

		wrapper.find('button.update-page').trigger('click');
		await wrapper.vm.$nextTick();

		expect(pageChangeHandlerSpy).toBeCalledWith(2);
		wrapper.setData({ currentPage: 2 });
		await wrapper.vm.$nextTick();
	});

	it('Should call method setLocation correctly', async () => {
		const setLocationSpy = jest.spyOn(wrapper.vm, 'setLocation');
		wrapper.setMethods({ setLocation: setLocationSpy });

		expect(wrapper.find('button.update-location').exists()).toBe(true);

		wrapper.find('button.update-location').trigger('click');
		await wrapper.vm.$nextTick();

		expect(setLocationSpy).toBeCalledWith(1);
	});

	it('Should call method setFillterData correctly', async () => {
		const setFillterDataSpy = jest.spyOn(wrapper.vm, 'setFillterData');
		wrapper.setMethods({ setFillterData: setFillterDataSpy });

		expect(wrapper.find('button.search-handler').exists()).toBe(true);

		wrapper.find('button.search-handler').trigger('click');
		await wrapper.vm.$nextTick();

		expect(setFillterDataSpy).toBeCalled();
	});

	it('Should call bugsnagClient method on error', async () => {
		mockWindowProperty('innerWidth', 400);
		wrapper = shallowMount(FlashSaleList, {
			localVue,
			store,
			stubs,
			mocks,
			methods: {
				fetchFlashSaleWithUrl: jest.fn().mockRejectedValue(new Error('error')),
				settingDocumentMeta: jest.fn()
			}
		});
		await wrapper.vm.$nextTick();

		expect(notify).toBeCalled();
	});

	it('Should call handleMoEngage method on created', () => {
		mockWindowProperty('innerWidth', 400);
		wrapper = shallowMount(FlashSaleList, {
			localVue,
			store,
			stubs,
			mocks,
			computed: {
				getSourceWebview() {
					return 'android';
				},
				getIsWebview() {
					return true;
				},
				getAuthCheck() {
					return {
						all: true,
						owner: true
					};
				}
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		expect(tracker).toBeCalled();
	});

	it('Should call watcher with default state', async () => {
		const mocks = {
			$route: {
				params: {},
				query: {}
			}
		};
		wrapper = shallowMount(FlashSaleList, {
			localVue,
			store,
			stubs,
			mocks,
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		await wrapper.vm.$nextTick();
		wrapper.vm.$store.commit('setFlashSaleStatus', 'empty');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.getFlashSaleStatus).toEqual('empty');
	});
});
