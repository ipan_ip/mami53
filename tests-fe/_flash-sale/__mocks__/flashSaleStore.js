import { translateWords } from 'Js/@utils/langTranslator.js';
import { getParams } from 'Js/@utils/queryString';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

export default {
	state: {
		authCheck: {},
		authData: {},
		faqData: [
			{
				id: 1,
				title: 'Lorem Ipsum 1',
				answer:
					'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
			},
			{
				id: 2,
				title: 'Lorem Ipsum 2',
				answer:
					'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
			},
			{
				id: 3,
				title: 'Lorem Ipsum 3',
				answer:
					'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
			},
			{
				id: 4,
				title: 'Lorem Ipsum 4',
				answer:
					'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
			},
			{
				id: 5,
				title: 'Lorem Ipsum 5',
				answer:
					'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
			}
		],
		areas: [],
		searchModalStatus: false,
		selectedAreas: 0,
		flashSaleData: null,
		flashSaleStatus: '',
		sourceWebview: null,
		activeFaq: -1,
		flashRooms: [],
		isLoading: true,
		isListLoading: true,
		isEmptyList: false,
		params: {
			page: 1,
			filters: {
				gender: [0, 1, 2],
				rent_type: 2,
				property_type: 'kos',
				price_range: [0, 1500000],
				flash_sale: true
			},
			sorting: {
				field: 'price',
				direction: '-'
			},
			landings: ['kost-bebas-eksklusif-jogja', 'kost-pasutri-murah-jogja'],
			limit: 20,
			offset: 0,
			total: 0
		}
	},
	getters: {
		getBannerData: state => {
			return state.flashSaleData ? state.flashSaleData.banner : '';
		},
		getSourceWebview: state => {
			return state.sourceWebview;
		},
		getAuthCheck: state => {
			return state.authCheck;
		},
		getSearchStatus: state => {
			return state.searchModalStatus;
		},
		getEmptyList: state => {
			return state.isEmptyList;
		},
		getAllFaq: state => {
			return state.faqData;
		},
		getFlashSaleStatus: state => {
			return state.flashSaleStatus;
		},
		getFlashSaleTimeline: state => {
			return state.flashSaleData
				? [state.flashSaleData.start, state.flashSaleData.end]
				: '';
		},
		getFlashSaleData: state => {
			return state.flashSaleData;
		},
		getFlashSaleId: state => {
			return state.flashSaleData.id;
		},
		getIsWebview: state => {
			return state.sourceWebview !== null;
		},
		getActiveQuestion: state => {
			return state.activeFaq;
		},
		getRooms: state => {
			return state.flashRooms;
		},
		getIsLoading: state => {
			return state.isLoading;
		},
		getIsListLoading: state => {
			return state.isListLoading;
		},
		getFilter: state => {
			return state.params.filters;
		},
		getCurrentPage: state => {
			return state.params.page;
		},
		getTotalPage: state => {
			return Math.ceil(state.params.total / state.params.limit);
		},
		getSorting: state => {
			return [state.params.sorting.field, state.params.sorting.direction];
		},
		getDisplayNumber: state => {
			const start = state.params.offset + 1;
			const end = state.params.offset + state.flashRooms.length;
			return `${start} - ${end} Kos dari ${state.params.total}`;
		},
		getLocation: state => {
			return state.areas;
		},
		getSelectedLocation: state => {
			return state.selectedAreas;
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setEmptyList(state, payload) {
			state.isEmptyList = payload;
		},
		setFAQStatus(state, payload) {
			state.activeFaq = payload !== state.activeFaq ? payload : -1;
		},
		setFlashSaleData(state, payload) {
			state.flashSaleData = payload;
		},
		setFlashSaleStatus(state, payload) {
			state.flashSaleStatus = payload;
		},
		setSourceWebview(state, payload) {
			state.sourceWebview = payload;
		},
		setFilter(state, payload) {
			state.params.filters = { ...state.params.filters, ...payload };
		},
		setSort(state, payload) {
			state.params.sorting.field = payload[0];
			state.params.sorting.direction = payload[1];
		},
		setAllFilter(state, payload) {
			state.params.filters = payload;
		},
		setPage(state, payload) {
			state.params.page = payload;
		},
		setLanding(state, payload) {
			state.params.landings = payload.landingOptions;
			state.selectedAreas = payload.selected;
		},
		setRooms(state, payload) {
			state.flashRooms = payload.rooms;
			state.params.total = payload.total;
			state.isListLoading = false;
		},
		setOffset(state, payload) {
			state.params.offset = payload;
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setListLoading(state, payload) {
			state.isListLoading = payload;
		},
		setAreaOptions(state, payload) {
			state.areas = payload;
		},
		setSearchStatus(state, payload) {
			state.searchModalStatus = payload;
		},
		setFAQ(state, payload) {
			state.faqData = payload;
		},

		// mutation unit test helper
		setDummyDate(state, payload) {
			state.flashSaleData.end = payload;
		}
	},
	actions: {
		fetchFlashSaleWithUrl({ commit, state, dispatch }, searchParam) {
			return new Promise(resolve => {
				if (!state.flashSaleData) {
					commit('setLoading', true);
					commit('setListLoading', true);
					dispatch('fetchFAQ');
					// waiting API
					const payload = {
						status: true,
						meta: {
							response_code: 200,
							code: 200,
							severity: 'OK',
							message:
								'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
						},
						// This will be null if no data exists
						data: {
							start: '2020-05-01 16:07:11',
							end: '2020-05-30 16:07:11',
							remaining: {
								day: 0,
								hour: 4,
								minute: 45,
								second: 15
							},
							banner: {
								real:
									'https://static.mamikos.com/uploads/data/event/2020-05-10/6yGUhSck.jpg',
								small:
									'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-240x320.jpg',
								medium:
									'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-360x480.jpg',
								large:
									'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-540x720.jpg'
							},
							areas_count: 3,
							areas: [
								{
									id: 0,
									label: 'Semua Kota',
									landings: [
										'kost-bebas-murah-jakarta',
										'kost-pasutri-murah-jakarta',
										'info-kost-ac-murah-jakarta',
										'kost-bebas-murah-surabaya',
										'kost-pasutri-murah-surabaya',
										'info-kost-ac-murah-surabaya',
										'kost-bebas-eksklusif-jogja',
										'kost-pasutri-murah-jogja'
									]
								},
								{
									id: 1,
									label: 'Jakarta',
									landings: [
										'kost-bebas-murah-jakarta',
										'kost-pasutri-murah-jakarta',
										'info-kost-ac-murah-jakarta'
									]
								},
								{
									id: 2,
									label: 'Surabaya',
									landings: [
										'kost-bebas-murah-surabaya',
										'kost-pasutri-murah-surabaya',
										'info-kost-ac-murah-surabaya'
									]
								},
								{
									id: 3,
									label: 'Jogja',
									landings: [
										'kost-bebas-eksklusif-jogja',
										'kost-pasutri-murah-jogja'
									]
								}
							]
						}
					};
					const sourceData = payload.data ? payload.data : null;
					sourceData.id = searchParam;
					commit('setFlashSaleData', sourceData);
					commit('setFlashSaleStatus', sourceData ? 'running' : 'empty');
					dispatch('settingAreaLandings', sourceData ? sourceData.areas : []);
					commit('setLoading', false);
					if (sourceData) {
						const selectedPayload = {
							landingOptions: sourceData.areas[0].landings,
							selected: 0
						};
						commit('setLanding', selectedPayload);
						dispatch('fetchFlashSaleRoom');
					} else {
						commit('setListLoading', false);
						commit('setEmptyList', true);
					}
					resolve(true);
				} else {
					resolve(true);
				}
			});
		},
		fetchFlashSaleData({ commit, dispatch }, checkSource = false) {
			commit('setLoading', true);
			commit('setListLoading', true);
			dispatch('fetchFAQ');
			if (checkSource) {
				const params = getParams(null);
				const acquiredParams = params.source ? params.source[0] : null;
				commit('setSourceWebview', acquiredParams);
			}
			const payload = {
				total_running: 1,
				total_upcoming: 1,
				data: [
					{
						id: 1,
						name: 'ABC1 Running',
						is_running: true,
						is_finished: false,
						start: '2020-05-18 15:00:00',
						end: '2020-05-30 20:35:00',
						banner: {
							real:
								'https://static.mamikos.com/uploads/data/event/2020-05-10/6yGUhSck.jpg',
							small:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-240x320.jpg',
							medium:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-360x480.jpg',
							large:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-540x720.jpg'
						},
						areas_count: 3,
						areas: [
							{
								label: 'Jakarta',
								landings: [
									'kost-bebas-murah-jakarta',
									'kost-pasutri-murah-jakarta',
									'info-kost-ac-murah-jakarta'
								]
							},
							{
								label: 'Surabaya',
								landings: [
									'kost-bebas-murah-surabaya',
									'kost-pasutri-murah-surabaya',
									'info-kost-ac-murah-surabaya'
								]
							},
							{
								label: 'Jogja',
								landings: [
									'kost-bebas-eksklusif-jogja',
									'kost-pasutri-murah-jogja'
								]
							}
						]
					},
					{
						id: 2,
						name: 'ABC2 Upcoming',
						is_running: false,
						is_finished: false,
						start: '2020-05-30 03:15:00',
						end: '2020-05-30 18:00:00',
						banner: {
							real:
								'https://static.mamikos.com/uploads/data/event/2020-05-10/6yGUhSck.jpg',
							small:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-240x320.jpg',
							medium:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-360x480.jpg',
							large:
								'https://static.mamikos.com/uploads/cache/data/event/2020-05-10/6yGUhSck-540x720.jpg'
						},
						areas_count: 3,
						areas: [
							{
								label: 'Makassar',
								landings: [
									'kost-bebas-murah-jakarta',
									'kost-pasutri-murah-jakarta',
									'info-kost-ac-murah-jakarta'
								]
							},
							{
								label: 'Semarang',
								landings: [
									'kost-bebas-murah-surabaya',
									'kost-pasutri-murah-surabaya',
									'info-kost-ac-murah-surabaya'
								]
							},
							{
								label: 'Banjar Masin',
								landings: [
									'kost-bebas-eksklusif-jogja',
									'kost-pasutri-murah-jogja'
								]
							}
						]
					}
				]
			};
			let status = '';
			let sourceData = null;
			if (payload.total_running > 0) {
				sourceData = payload.data.find(
					flashsale =>
						flashsale.is_running === true && flashsale.is_finished === false
				);
				status = 'running';
			} else if (payload.total_upcoming > 0) {
				sourceData = payload.data.find(
					flashsale =>
						flashsale.is_running === false && flashsale.is_finished === false
				);
				status = 'upcoming';
			} else {
				status = 'empty';
			}
			commit('setFlashSaleData', sourceData);
			commit('setFlashSaleStatus', status);
			dispatch('settingAreaLandings', sourceData ? sourceData.areas : []);
			commit('setLoading', false);
			if (sourceData) {
				const selectedPayload = {
					landingOptions: sourceData.areas[0].landings,
					selected: 0
				};
				commit('setLanding', selectedPayload);
			}
			dispatch('fetchFlashSaleRoom');
		},
		fetchFAQ({ commit }) {
			const payload = [
				{
					id: 1,
					title: 'Lorem Ipsum 1',
					answer:
						'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
				},
				{
					id: 2,
					title: 'Lorem Ipsum 2',
					answer:
						'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
				},
				{
					id: 3,
					title: 'Lorem Ipsum 3',
					answer:
						'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
				},
				{
					id: 4,
					title: 'Lorem Ipsum 4',
					answer:
						'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
				},
				{
					id: 5,
					title: 'Lorem Ipsum 5',
					answer:
						'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
				}
			];
			commit('setFAQ', payload);
		},
		async fetchFlashSaleRoom({ commit, state }) {
			commit('setListLoading', true);
			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: state.params
			});
			if (response) {
				const rooms = translateWords(response.rooms) || [];
				if (rooms.length > 0) {
					const payload = {
						rooms,
						total: response.total
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
					commit('setListLoading', false);
				}
			} else {
				commit('setEmptyList', true);
				commit('setRooms', []);
			}
		},
		settingFilter({ commit, dispatch }, payload) {
			commit('setFilter', payload);
			dispatch('fetchFlashSaleRoom');
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchFlashSaleRoom');
		},
		settingPage({ commit, state, dispatch }, payload) {
			commit('setPage', payload);
			commit('setOffset', (payload - 1) * state.params.limit);
			dispatch('fetchFlashSaleRoom');
		},
		settingLandings({ commit, state, dispatch }, payload) {
			const selectedPayload = {
				landingOptions: state.flashSaleData.areas[payload].landings,
				selected: payload
			};
			commit('setLanding', selectedPayload);
			dispatch('fetchFlashSaleRoom');
		},
		settingAreaLandings({ commit }, payload) {
			const areaOptions = payload.map((value, index) => {
				return {
					label: value.label,
					value: index
				};
			});
			commit('setAreaOptions', areaOptions);
		},
		settingAllFilter({ commit, state, dispatch }, payload) {
			commit('setSort', payload.sortingPayload);
			const filters = {
				gender: payload.filterPayload.gender,
				rent_type: payload.filterPayload.rent_type,
				property_type: 'kos',
				price_range: payload.filterPayload.price_range,
				flash_sale: true
			};
			const selectedPayload = {
				landingOptions:
					state.flashSaleData.areas[payload.selectedLocationPayload].landings,
				selected: payload.selectedLocationPayload
			};
			commit('setAllFilter', filters);
			commit('setLanding', selectedPayload);
			dispatch('fetchFlashSaleRoom');
		}
	}
};
