import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockComponent from 'tests-fe/utils/mock-component';
import FlashSaleStore from './__mocks__/flashSaleStore';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import App from 'Js/_flash-sale/App';
/* eslint-disable-next-line */
window.Vue = require('vue');
// eslint-disable-next-line
Vue.use(Vuex);
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
const store = new Vuex.Store(FlashSaleStore);
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('innerWidth', 1366);

describe('App.vue', () => {
	const mocks = {
		$route: {
			params: {
				id: 1
			}
		}
	};
	const flashSaleFAQStub = {
		template: `<button class="open-faq" @click="$emit('activeToggle', 1)"></button>`
	};
	const stubs = {
		'router-view': mockComponent,
		'flash-sale-faq': flashSaleFAQStub
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(App, {
			localVue,
			store,
			stubs
		});
		wrapper.vm.$store.commit('setListLoading', false);
		wrapper.vm.$store.commit('setFlashSaleStatus', 'running');
	});

	it('Computed logo should return correctly', () => {
		const widthSelection = [
			{
				width: '400',
				expect: '/general/img/logo/logo_mamikos_green.svg'
			},
			{
				width: '1366',
				expect: '/general/img/logo/icon_mamikos.svg'
			}
		];

		widthSelection.forEach(selection => {
			mockWindowProperty('innerWidth', selection.width);
			wrapper = shallowMount(App, {
				localVue,
				store,
				stubs
			});

			expect(wrapper.vm.logo).toEqual(selection.expect);
		});
	});

	it('Should call openFaqHandler on click option FAQ with second branch options', async () => {
		mockWindowProperty('window.location', {
			pathname: '/kost-promo-ngebut'
		});

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs,
			computed: {
				getSourceWebview() {
					return '';
				},
				getIsWebview() {
					return false;
				},
				getAuthCheck() {
					return {
						all: false,
						owner: false
					};
				}
			}
		});

		await wrapper.vm.$nextTick();
		const openFaqHandlerSpy = jest.spyOn(wrapper.vm, 'openFaqHandler');
		wrapper.setMethods({ openFaqHandler: openFaqHandlerSpy });

		expect(wrapper.find('button.open-faq').exists()).toBe(true);

		wrapper.find('button.open-faq').trigger('click');
		await wrapper.vm.$nextTick();

		expect(openFaqHandlerSpy).toBeCalledWith(1);
		expect(tracker).toBeCalledWith('moe', [
			'FAQ Discount Clicked',
			{
				faq_content: 'Lorem Ipsum 2',
				interface: 'desktop',
				redirection_source: 'discount Kos LP'
			}
		]);
	});

	it('Should cal openFaqHandler on click option FAQ with 1 time call tracker', async () => {
		mockWindowProperty('innerWidth', 400);
		const mocks = {
			$route: {
				params: {}
			}
		};

		mockWindowProperty('window.location', {
			pathname: '/promo-ngebut'
		});

		const flashSaleFAQStub = {
			template: `<button class="open-faq" @click="$emit('activeToggle', 2)"></button>`
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs: {
				...stubs,
				'flash-sale-faq': flashSaleFAQStub
			},
			computed: {
				getSourceWebview() {
					return 'android';
				},
				getIsWebview() {
					return true;
				},
				getAuthCheck() {
					return {
						all: true,
						owner: true
					};
				}
			}
		});

		await wrapper.vm.$nextTick();
		const openFaqHandlerSpy = jest.spyOn(wrapper.vm, 'openFaqHandler');
		wrapper.setMethods({ openFaqHandler: openFaqHandlerSpy });

		expect(wrapper.find('button.open-faq').exists()).toBe(true);

		wrapper.find('button.open-faq').trigger('click');
		await wrapper.vm.$nextTick();

		expect(openFaqHandlerSpy).toBeCalledWith(2);
		expect(tracker).toBeCalledTimes(1);
	});

	it('Should call openFaqHandler on click option FAQ with the same id and not call handleMoEngage', async () => {
		const flashSaleFAQStub = {
			template: `<button class="open-faq" @click="$emit('activeToggle', 2)"></button>`
		};

		wrapper = shallowMount(App, {
			localVue,
			store,
			mocks,
			stubs: {
				...stubs,
				'flash-sale-faq': flashSaleFAQStub
			}
		});

		await wrapper.vm.$nextTick();
		wrapper.setData({
			posts: {
				edges: [{ node: { title: 'Question 1', content: 'Answer 1' } }]
			}
		});
		const openFaqHandlerSpy = jest.spyOn(wrapper.vm, 'openFaqHandler');
		const handleMoEngageSpy = jest.spyOn(wrapper.vm, 'handleMoEngage');
		wrapper.setMethods({ handleMoEngage: handleMoEngageSpy });

		expect(wrapper.find('button.open-faq').exists()).toBe(true);

		wrapper.find('button.open-faq').trigger('click');
		await wrapper.vm.$nextTick();

		expect(openFaqHandlerSpy).toBeCalledWith(2);
		expect(handleMoEngageSpy).not.toBeCalled();
	});
});
