import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import CountDownSection from 'Js/_flash-sale/components/CountDownSection.vue';
import MixinFlashSale from 'Js/_flash-sale/mixins/MixinFlashSale';
import flashSaleStore from '../__mocks__/flashSaleStore';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin(MixinFlashSale);

describe('CountDownSection.vue', () => {
	const store = new Vuex.Store(flashSaleStore);

	const wrapper = shallowMount(CountDownSection, {
		localVue,
		store,
		propsData: {
			title: 'Flash Sale'
		},
		mocks: {
			$dayjs: jest.fn(() => ({
				format: jest.fn().mockResolvedValue('4 Oktober 2020 10:00:00')
			}))
		}
	});

	const callCountdownTimerSpy = jest.spyOn(wrapper.vm, 'callCountdownTimer');
	const resetTimerSpy = jest.spyOn(wrapper.vm, 'resetTimer');

	it('Watcher getFlashSaleTimeline should call the callCountdownTimer when the condition is met', async () => {
		expect(callCountdownTimerSpy).not.toBeCalled();

		wrapper.vm.$store.commit('setFlashSaleData', {
			start: '2-10-2020 17:00:00',
			end: '4-10-2020 10:00:00'
		});

		await wrapper.setProps({ needCountDown: true });

		expect(callCountdownTimerSpy).toBeCalled();
	});

	it('Should call resetTimer on destroy', () => {
		wrapper.destroy();

		expect(resetTimerSpy).toBeCalled();
	});
});
