import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import 'tests-fe/utils/mock-vue';
import FilterDesktop from 'Js/_flash-sale/components/FilterDesktop.vue';
import flashSaleStore from '../__mocks__/flashSaleStore';
const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin(MixinNavigatorIsMobile);

const handleScrollSpy = jest.spyOn(FilterDesktop.methods, 'handleScroll');
const setFilterTypeSpy = jest.spyOn(FilterDesktop.methods, 'setFilterType');
const setFilterTimeSpy = jest.spyOn(FilterDesktop.methods, 'setFilterTime');
const setFilterPriceSpy = jest.spyOn(FilterDesktop.methods, 'setFilterPrice');
const setFilterSortSpy = jest.spyOn(FilterDesktop.methods, 'setFilterSort');
const setLocationSpy = jest.spyOn(FilterDesktop.methods, 'setLocation');
const locationChangeHandlerSpy = jest.spyOn(
	FilterDesktop.methods,
	'locationChangeHandler'
);
const changePriceMinSpy = jest.spyOn(FilterDesktop.methods, 'changePriceMin');
const changePriceMaxSpy = jest.spyOn(FilterDesktop.methods, 'changePriceMax');
const setAllFilterSpy = jest.spyOn(FilterDesktop.methods, 'setAllFilter');

const filterKostStub = {
	template: `<button class="set-filter" @click="$emit('setFilter')"/>`
};
const selectStub = {
	template: `<button class="set-location" @click="$emit('change')"/>`
};
const filterPriceStub = {
	template: `<div><button class="filter-price" @click="$emit('filterPrice')"/><button class="filter-price-min" @click="$emit('filterPriceMin')"/><button class="filter-price-max" @click="$emit('filterPriceMax')"/></div>`
};

const stubs = {
	'filter-kost-type': filterKostStub,
	'filter-time': filterKostStub,
	'filter-kost-sort': filterKostStub,
	'v-select': selectStub,
	'filter-price': filterPriceStub
};

describe('FilterDesktop.vue', () => {
	const store = new Vuex.Store(flashSaleStore);

	let wrapper = shallowMount(FilterDesktop, {
		localVue,
		store,
		stubs,
		propsData: {
			selectedLocation: 'Semua'
		}
	});

	it('Should call method setAllFilter on click search', async () => {
		await wrapper.setProps({ searchDirectly: false });

		const searchElement = wrapper.find('button.search-button');

		expect(searchElement.exists()).toBe(true);

		await searchElement.trigger('click');

		expect(setAllFilterSpy).toBeCalled();
	});

	it('Should call method setLocation on change select option', async () => {
		const selectElement = wrapper.find('button.set-location');

		expect(selectElement.exists()).toBe(true);

		await selectElement.trigger('click');

		expect(setLocationSpy).toBeCalled();

		await selectElement.trigger('click');

		expect(setLocationSpy).toBeCalled();
		expect(locationChangeHandlerSpy).toBeCalled();
	});

	it('Should call method setFilterType on change gender option', async () => {
		const setFilterType = wrapper.find('button.set-filter');

		expect(setFilterType.exists()).toBe(true);

		await setFilterType.trigger('click');

		expect(setFilterTypeSpy).toBeCalled();
	});

	it('Should call method setFilterTime on change time option', async () => {
		await wrapper.setProps({ needTime: true });

		const setFilterTime = wrapper.findAll('button.set-filter').at(1);

		expect(setFilterTime.exists()).toBe(true);

		await setFilterTime.trigger('click');

		expect(setFilterTimeSpy).toBeCalled();
	});

	it('Should call method setFilterSort on change sort option', async () => {
		const setFilterSort = wrapper.findAll('button.set-filter').at(2);

		expect(setFilterSort.exists()).toBe(true);

		await setFilterSort.trigger('click');

		expect(setFilterSortSpy).toBeCalled();
	});

	it('Should call method setFilterPrice on change price component', async () => {
		const setFilterPrice = wrapper.find('button.filter-price');
		const changePriceMin = wrapper.find('button.filter-price-min');
		const changePriceMax = wrapper.find('button.filter-price-max');
		await wrapper.setData({
			filterTemp: {
				price_range: [10000, 20000]
			}
		});

		expect(setFilterPrice.exists()).toBe(true);
		expect(changePriceMin.exists()).toBe(true);
		expect(changePriceMax.exists()).toBe(true);

		setFilterPrice.trigger('click');
		changePriceMin.trigger('click');
		changePriceMax.trigger('click');
		await wrapper.vm.$nextTick();

		expect(setFilterPriceSpy).toBeCalled();
		expect(changePriceMinSpy).toBeCalled();
		expect(changePriceMaxSpy).toBeCalled();
	});

	it('Method handleScroll should called correctly', () => {
		global.pageYOffset = 400;
		const eventList = {};
		window.addEventListener = jest.fn((event, cb) => {
			eventList[event] = cb;
		});

		wrapper = shallowMount(FilterDesktop, {
			localVue,
			store,
			stubs,
			propsData: {
				selectedLocation: 'Semua'
			}
		});

		eventList.scroll({ scrollTo: 400 });

		expect(handleScrollSpy).toBeCalled();
	});

	it('Should add and remove listener on destroy', () => {
		const addEvent = jest
			.spyOn(global, 'addEventListener')
			.mockImplementation(() => {});
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});

		wrapper = shallowMount(FilterDesktop, {
			localVue,
			store,
			stubs,
			propsData: {
				selectedLocation: 'Semua',
				searchDirectly: false
			}
		});

		expect(addEvent).toHaveBeenCalled();

		wrapper.destroy();

		expect(removeEvent).toHaveBeenCalled();
	});
});
