import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import flashSaleRoomsData from './__mocks__/flashSaleEncryptionData.json';
import FlashSaleStore from './__mocks__/flashSaleStore';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import FlashSaleLanding from 'Js/_flash-sale/FlashSaleLanding';
/* eslint-disable-next-line */
window.Vue = require('vue');
// eslint-disable-next-line
Vue.use(Vuex);
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
mockWindowProperty('tracker', jest.fn());
const store = new Vuex.Store(FlashSaleStore);
const mocks = {
	$dayjs: jest.fn(() => ({
		isValid: jest.fn().mockResolvedValue(true),
		locale: jest.fn(() => ({
			format: jest.fn()
		}))
	})),
	$router: {
		push: jest.fn()
	}
};

describe('App.vue', () => {
	global.axios = mockAxios;
	axios.mockResolve({
		data: {
			status: 200,
			rooms: flashSaleRoomsData.encryptedData
		}
	});
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks,
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
	});

	it('Computed flashSaleTitle should return correctly', () => {
		const titleSelection = [
			{
				title: 'running',
				expect: 'Mamikos Promo Ngebut akan berakhir dalam'
			},
			{
				title: 'upcoming',
				expect: 'Bersiap menuju Mamikos Promo Ngebut!'
			},
			{
				title: '',
				expect: ''
			}
		];
		titleSelection.forEach(selection => {
			wrapper.vm.$store.commit('setFlashSaleStatus', selection.title);

			expect(wrapper.vm.flashSaleTitle).toEqual(selection.expect);
		});
	});

	it('Computed flashSaleDuration should return correctly if date is not valid', () => {
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks: {
				$dayjs: jest.fn(() => ({
					isValid: jest.fn()
				}))
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setFlashSaleStatus', 'empty');
		wrapper.vm.$store.commit('setDummyDate', null);
		expect(wrapper.vm.flashSaleDuration).toEqual('');
	});

	it('Computed flashSaleDuration should return correctly if date is valid', () => {
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks: {
				$dayjs: jest.fn(() => ({
					isValid: jest.fn().mockResolvedValue(true),
					locale: jest.fn(() => ({
						format: jest.fn().mockReturnValue('20 May 2020')
					}))
				}))
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setFlashSaleStatus', 'upcoming');
		expect(wrapper.vm.flashSaleDuration).toEqual('20 May 2020 - 20 May 2020');
	});

	it('Should call goToList method on click button', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };

		const flashSaleInformationStub = {
			template: `<button class='button-cari-promo' @click="$emit('goToList')">Cari Kost Promo Ngebut</button>`
		};
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks,
			stubs: {
				'flash-sale-information': flashSaleInformationStub
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setFlashSaleStatus', 'running');
		const goToListSpy = jest.spyOn(wrapper.vm, 'goToList');
		wrapper.setMethods({ goToList: goToListSpy });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.button-cari-promo').exists()).toBe(true);

		wrapper.find('button.button-cari-promo').trigger('click');
		await wrapper.vm.$nextTick();

		expect(goToListSpy).toBeCalled();
		assignMock.mockClear();
	});

	it('Should call goToList method on click button with getIsWebview return true', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };
		mockWindowProperty('window.Android', {
			trackDiscountLPVisited: jest.fn()
		});

		const flashSaleInformationStub = {
			template: `<button class='button-cari-promo' @click="$emit('goToList')">Cari Kost Promo Ngebut</button>`
		};
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks,
			stubs: {
				'flash-sale-information': flashSaleInformationStub
			},
			computed: {
				getIsWebview() {
					return true;
				},
				getSourceWebview() {
					return 'android';
				}
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setFlashSaleStatus', 'running');
		const goToListSpy = jest.spyOn(wrapper.vm, 'goToList');
		wrapper.setMethods({ goToList: goToListSpy });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.button-cari-promo').exists()).toBe(true);

		wrapper.find('button.button-cari-promo').trigger('click');
		await wrapper.vm.$nextTick();

		expect(goToListSpy).toBeCalled();
		assignMock.mockClear();
	});

	it('Should call tracker method on mounted', async () => {
		mockWindowProperty('innerWidth', 400);
		mockWindowProperty('window.location', {
			search: 'https://mamikos.com/flash-sale?source=android&from=home_banner'
		});
		wrapper = shallowMount(FlashSaleLanding, {
			localVue,
			store,
			mocks,
			computed: {
				getSourceWebview() {
					return 'android';
				},
				getIsWebview() {
					return true;
				},
				getAuthCheck() {
					return {
						all: true,
						owner: true
					};
				}
			},
			methods: {
				settingDocumentMeta: jest.fn()
			}
		});
		wrapper.vm.$store.commit('setFlashSaleStatus', 'running');
		expect(tracker).toBeCalled();
	});
});
