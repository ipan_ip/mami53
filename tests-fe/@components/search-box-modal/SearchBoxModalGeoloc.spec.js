import { shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';
import SearchBoxModalGeoloc from 'Js/@components/search-box-modal/SearchBoxModalGeoloc.vue';

mockWindowProperty('location', { href: jest.fn() });
mockWindowProperty('navigator', {
	geolocation: {
		getCurrentPosition: jest.fn()
	}
});
mockWindowProperty('Cookies', {
	get: jest.fn(),
	remove: jest.fn()
});

describe('SearchBoxModalGeoloc.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(SearchBoxModalGeoloc);
	};

	describe('basic', () => {
		it('should render SearchBoxModalGeoloc', () => {
			createComponent();

			const mamiModalWrap = wrapper.find('.search-box-modal__geoloc');
			expect(mamiModalWrap.exists()).toBe(true);
		});
	});

	describe('get current location', () => {
		it('should call getCurrentPosition function', () => {
			createComponent();

			const locationLinkEl = wrapper.find('.geoloc-action');
			expect(locationLinkEl.exists()).toBe(true);

			locationLinkEl.trigger('click');
			expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalled();
		});

		it('should handle not supported geolocation', () => {
			mockWindowProperty('navigator', {});
			createComponent();

			const locationLinkEl = wrapper.find('.geoloc-action');
			expect(locationLinkEl.exists()).toBe(true);

			locationLinkEl.trigger('click');
			expect(wrapper.vm.geolocationAlert).toBe(true);
		});
	});
});
