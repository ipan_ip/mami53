import { shallowMount } from '@vue/test-utils';
import SearchBoxModalPopularPrimary from 'Js/@components/search-box-modal/SearchBoxModalPopularPrimary.vue';

describe('SearchBoxModalPopularPrimary.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(SearchBoxModalPopularPrimary, {
			propsData: {
				activePopulars: {
					campus: [
						{
							name: 'UGM'
						}
					]
				}
			}
		});
	};

	describe('basic', () => {
		it('should render SearchBoxModalPopularPrimary', () => {
			createComponent();

			const modalPupularWrapEl = wrapper.find('.popular-primary');
			expect(modalPupularWrapEl.exists()).toBe(true);
		});

		it('should update method on click option', () => {
			createComponent();
			const buttonPopularEl = wrapper.find('button');
			expect(buttonPopularEl.exists()).toBe(true);

			buttonPopularEl.trigger('click');
			expect(wrapper.emitted().select).toBeTruthy();
		});
	});
});
