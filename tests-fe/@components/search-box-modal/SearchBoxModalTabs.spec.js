import { shallowMount } from '@vue/test-utils';
import SearchBoxModalTabs from 'Js/@components/search-box-modal/SearchBoxModalTabs.vue';

describe('SearchBoxModalTabs.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(SearchBoxModalTabs, {
			propsData: {
				activeTab: 'area'
			}
		});
	};

	describe('basic', () => {
		it('should render SearchBoxModalTabs', () => {
			createComponent();

			const tabsEl = wrapper.find('.tabs');
			expect(tabsEl.exists()).toBe(true);
		});

		it('should emit changed when click on link tab', () => {
			createComponent();
			const tabLinkEl = wrapper.find('a');
			expect(tabLinkEl.exists()).toBe(true);

			tabLinkEl.trigger('click');
			expect(wrapper.emitted().changed).toBeTruthy();
		});
	});
});
