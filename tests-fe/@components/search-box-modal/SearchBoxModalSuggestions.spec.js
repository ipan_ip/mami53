import { shallowMount } from '@vue/test-utils';
import SearchBoxModalSuggestions from 'Js/@components/search-box-modal/SearchBoxModalSuggestions.vue';

describe('SearchBoxModalSuggestions.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(SearchBoxModalSuggestions, {
			propsData: {
				category: 'Area',
				suggestions: [
					{
						title: 'test',
						subtitle: 'test',
						tags: ['A', 'B']
					}
				]
			}
		});
	};

	describe('basic', () => {
		it('should render SearchBoxModalSuggestions', () => {
			createComponent();

			const modalPupularWrapEl = wrapper.find('.search-box-modal__results');
			expect(modalPupularWrapEl.exists()).toBe(true);
		});

		it('should update method on click option', () => {
			createComponent();
			const suggestionLinkEl = wrapper.find('.results-list');
			expect(suggestionLinkEl.exists()).toBe(true);

			suggestionLinkEl.trigger('click');
			expect(wrapper.emitted().select).toBeTruthy();
		});
	});

	describe('category title', () => {
		it('should render landing title properly', async () => {
			createComponent();
			await wrapper.setProps({
				category: 'Landing'
			});

			const categoryTitleEl = wrapper.find('.results-header');
			expect(categoryTitleEl.exists()).toBe(true);
			expect(categoryTitleEl.text()).toBe('Pencarian terkait');
		});

		it('should render other title properly', async () => {
			createComponent();
			await wrapper.setProps({
				category: 'Kost'
			});

			const categoryTitleEl = wrapper.find('.results-header');
			expect(categoryTitleEl.exists()).toBe(true);
			expect(categoryTitleEl.text()).toBe('Nama kost');
		});
	});
});
