import { shallowMount } from '@vue/test-utils';
import SearchBoxModalPopularSecondary from 'Js/@components/search-box-modal/SearchBoxModalPopularSecondary.vue';

describe('SearchBoxModalPopularSecondary.vue', () => {
	let wrapper;

	const createComponent = () => {
		wrapper = shallowMount(SearchBoxModalPopularSecondary, {
			propsData: {
				activePopulars: {
					cities: [
						{
							city: 'Jogja',
							campus: [
								{
									name: 'UGM'
								}
							],
							area: [
								{
									name: 'Malioboro'
								}
							]
						}
					]
				}
			}
		});
	};

	describe('basic', () => {
		it('should render SearchBoxModalPopularSecondary', () => {
			createComponent();

			const modalPupularWrapEl = wrapper.find('.popular-secondary');
			expect(modalPupularWrapEl.exists()).toBe(true);
		});

		it('should update method on click option', () => {
			createComponent();
			const buttonPopularEl = wrapper.find('.panel-child');
			expect(buttonPopularEl.exists()).toBe(true);

			buttonPopularEl.trigger('click');
			expect(wrapper.emitted().select).toBeTruthy();
		});
	});

	describe('panel', () => {
		it('should update method on click option', async () => {
			createComponent();
			await wrapper.setProps({
				activeTab: 'area'
			});

			const panelEl = wrapper.find('.panel-with-icon');
			expect(panelEl.exists()).toBe(true);

			panelEl.trigger('click');
			expect(wrapper.vm.selectedPanel.area).toBe(0); // First Index

			panelEl.trigger('click');
			expect(wrapper.vm.selectedPanel.area).toBe(null); // First Index
		});
	});
});
