import { shallowMount, createLocalVue } from '@vue/test-utils';
import NotifOwner from 'Js/@components/NotifOwner';

const localVue = createLocalVue();
Object.defineProperty(window, 'location', {
	value: {
		href: ''
	}
});

describe('NotifOwner.vue', () => {
	const propsData = { isOwnerData: false };
	const $router = { push: jest.fn() };
	const wrapper = shallowMount(NotifOwner, {
		localVue,
		propsData,
		mocks: { $router }
	});

	it('should load the component', () => {
		expect(wrapper.find('.notif-owner').exists()).toBe(true);
	});

	it('should direct the page to the notification center', () => {
		wrapper.vm.handleSeeMoreClicked();
		expect(window.location.href).toBe('/ownerpage/notification');
	});
});
