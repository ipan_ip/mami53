import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterCanBooking from 'Js/@components/FilterCanBooking.vue';
import PopperWidget from 'Js/@components/PopperWidget.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import * as storage from 'Js/@utils/storage';

describe('FilterCanBooking.vue', () => {
	mockWindowProperty('scrollTo', jest.fn());
	const localVue = createLocalVue();
	const checkShowConditionSpy = jest.spyOn(
		FilterCanBooking.methods,
		'checkShowCondition'
	);
	const showPopperSpy = jest.spyOn(FilterCanBooking.methods, 'showPopper');
	const hidePopperSpy = jest.spyOn(FilterCanBooking.methods, 'hidePopper');
	const toggleBookingSpy = jest.spyOn(
		FilterCanBooking.methods,
		'toggleBooking'
	);
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(FilterCanBooking, {
			localVue,
			stubs: {
				'popper-widget': PopperWidget
			}
		});
	});

	it('Should call checkShowCondition method on created and show the popper', () => {
		expect(checkShowConditionSpy).toBeCalled();
		expect(showPopperSpy).toBeCalled();
		expect(wrapper.vm.isShow).toBe(true);
	});

	it('Should call checkShowCondition method on created and not call the popper', () => {
		// Set can-booking-hl localStorage data to have value '1'
		storage.local.setItem('can-booking-hl', '1');
		wrapper = shallowMount(FilterCanBooking, {
			localVue,
			stubs: {
				'popper-widget': PopperWidget
			}
		});

		expect(checkShowConditionSpy).toBeCalled();
		expect(wrapper.vm.isShow).toBe(false);
		// Reset can-booking-hl localStorage data to have value null
		storage.local.setItem('can-booking-hl', null);
	});

	it('Should call checkShowCondition method with props', () => {
		wrapper.setProps({ showStatus: true, showFromProps: true });
		expect(showPopperSpy).toBeCalled();
		expect(wrapper.vm.isShow).toBe(true);
	});

	it('Should not call checkShowCondition method with props', async () => {
		wrapper = shallowMount(FilterCanBooking, {
			localVue,
			stubs: {
				'popper-widget': PopperWidget
			},
			propsData: {
				showFromProps: true,
				showStatus: false
			}
		});
		wrapper.setProps({ showStatus: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isShow).toBe(true);
	});

	it('Should call toggleBooking method on click button', async () => {
		wrapper.setData({ isShow: true });
		await wrapper.vm.$nextTick();

		const findingButton = wrapper.find('button.btn-can-booking');
		expect(findingButton.exists()).toBe(true);
		findingButton.trigger('click');

		await wrapper.vm.$nextTick();

		expect(toggleBookingSpy).toBeCalled();
		expect(hidePopperSpy).toBeCalledTimes(1);
		expect(wrapper.emitted('toggle-booking')).toBeTruthy();
		expect(wrapper.vm.isShow).toBe(false);

		wrapper.setProps({ needCallback: true });
		wrapper.setData({ isShow: true });
		await wrapper.vm.$nextTick();

		findingButton.trigger('click');
		expect(toggleBookingSpy).toBeCalled();
		expect(hidePopperSpy).toBeCalledTimes(2);
		expect(wrapper.emitted('callback')).toBeTruthy();
		expect(wrapper.emitted('toggle-booking')).toBeTruthy();

		wrapper.setData({ isShow: false });
		await wrapper.vm.$nextTick();

		findingButton.trigger('click');
		expect(hidePopperSpy).toBeCalledTimes(2);
	});
});
