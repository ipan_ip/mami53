import { createLocalVue, shallowMount } from '@vue/test-utils';
import StarRating from 'Js/@components/StarRating.vue';

describe('StarRating.vue', () => {
	const localVue = createLocalVue();

	let localStar = 0;
	let localRatingType = 0;
	const onStarClickFunction = function(star = 0, ratingType = 0) {
		localStar = star;
		localRatingType = ratingType;
	};
	const propsData = {
		rating: 3,
		maxRating: 5,
		customStyle: 'color: white;',
		ratingType: 1,
		starClick: onStarClickFunction
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(StarRating, {
			localVue,
			propsData
		});
		StarRating.computed.setRating.call(localVue);
	});

	it('should render correctly', () => {
		const stars = wrapper.findAll('.star');
		const firstStar = stars.at(0);

		expect(firstStar.exists()).toBe(true);
		expect(stars.length).toBe(propsData.maxRating);
	});

	it('should render the stars correctly', () => {
		const { setRating } = wrapper.vm;
		const stars = wrapper.findAll('.glyphicon-star');

		if (stars.length && stars.wrappers) {
			stars.wrappers.forEach((starWrapper, idx) => {
				const star = stars.at(idx);
				const starValue = idx + 1;

				if (setRating.full >= starValue) {
					expect(star.classes()).toContain('--green');
				}

				if (setRating.half === starValue) {
					expect(star.classes()).toContain('half');
				}

				if (starValue > setRating.full && starValue !== setRating.half) {
					expect(star.classes()).toContain('--grey');
				}
			});
		}
	});

	it('should render the star click function correctly', () => {
		const stars = wrapper.findAll('.star');
		const secondStar = stars.at(1);

		if (secondStar && secondStar.exists()) {
			secondStar.trigger('click');
			expect(localStar).toBe(2);
			expect(localRatingType).toBe(1);
		}
	});
});
