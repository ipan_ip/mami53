import { createLocalVue, shallowMount } from '@vue/test-utils';
import BreadcrumbTrails from 'Js/@components/BreadcrumbTrails.vue';

describe('BreadcrumbTrails.vue', () => {
	const localVue = createLocalVue();

	const propsData = {
		parent: 'parent route',
		active: 'active route',
		contents: [],
		overflow: true
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(BreadcrumbTrails, {
			localVue,
			propsData
		});
	});

	it('should render the section correctly', () => {
		const section = wrapper.find('section');
		expect(section.classes()).toContain('--display-block');
	});

	it('should render the trails wrapper correctly', () => {
		const breadcrumbTrails = wrapper.find('.breadcrumb-trails');
		expect(breadcrumbTrails.exists()).toBe(true);
	});

	it('should render the active trails for empty contents correctly', () => {
		const activeTrails = wrapper.findAll('.breadcrumb-trail.active');
		const parentTrail = activeTrails.at(0);
		const activeTrail = activeTrails.at(1);

		if (parentTrail && parentTrail.exists()) {
			expect(parentTrail.text()).toBe('parent route');
		}

		if (activeTrail && activeTrail.exists()) {
			expect(activeTrail.text()).toBe('active route');
		}
	});

	it('should render the classes for available contents correctly', () => {
		const propsData = {
			contents: [
				{
					name: 'routeName1',
					url: '/route/url/1'
				}
			],
			mobile: false,
			fixed: true
		};
		wrapper = shallowMount(BreadcrumbTrails, {
			localVue,
			propsData
		});

		const breadcrumbTrails = wrapper.find('.breadcrumb-trails');
		expect(breadcrumbTrails.classes()).toContain('is-fixed');
		expect(breadcrumbTrails.classes()).toContain('hidden-xs');
	});

	it('should render the trails for available contents correctly', () => {
		const propsData = {
			contents: [
				{
					name: 'routeName1',
					url: '/route/url/1'
				},
				{
					name: 'routeName2',
					url: '/route/url/2'
				}
			]
		};
		wrapper = shallowMount(BreadcrumbTrails, {
			localVue,
			propsData
		});

		const trails = wrapper.findAll('li');
		expect(trails.length).toBe(propsData.contents.length);

		const firstTrail = trails.at(0);
		expect(firstTrail.find('a').attributes('href')).toBe('/route/url/1');
		expect(firstTrail.find('a').text()).toBe('routeName1');

		const lastTrail = trails.at(trails.length - 1);
		expect(lastTrail.find('span').text()).toBe('routeName2');
	});

	it('set default contents to empty array if is not given', () => {
		wrapper = shallowMount(BreadcrumbTrails, {
			localVue,
			propsData: {
				parent: 'parent route',
				active: 'active route',
				overflow: true
			}
		});

		expect(wrapper.find('.breadcrumb-trail.inactive').text()).toBe('Home');
		expect(
			wrapper
				.findAll('.breadcrumb-trail.active')
				.at(0)
				.text()
		).toBe('parent route');
		expect(
			wrapper
				.findAll('.breadcrumb-trail.active')
				.at(1)
				.text()
		).toBe('active route');
	});
});
