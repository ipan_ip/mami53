import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterMobile from 'Js/@components/FilterMobile.vue';

const localVue = createLocalVue();
global.tracker = jest.fn();

describe('FilterMobile.vue', () => {
	const propsData = {
		filter: {
			gender: 'female',
			rent_type: 'monthly',
			price_range: ['1000000', '2000000']
		},
		sorting: ['asc']
	};
	const wrapper = shallowMount(FilterMobile, {
		localVue,
		propsData
	});

	it('should load the component', () => {
		expect(wrapper.find('.mobile-filter').exists()).toBe(true);
	});

	it('should toggle show modal', () => {
		wrapper.vm.showModal = false;
		wrapper.vm.toggleShow();
		expect(wrapper.vm.showModal).toBe(true);
	});

	it('should assign filter gender', () => {
		wrapper.vm.setFilterType('male');
		expect(wrapper.vm.filterTemp.gender).toBe('male');
	});

	it('should assign filter sort value', () => {
		wrapper.vm.setFilterSort(['dsc']);
		expect(wrapper.vm.sortingTemp[0]).toBe('dsc');
	});

	it('should assign filter rent type value', () => {
		wrapper.vm.setFilterTime('quarterly');
		expect(wrapper.vm.filterTemp.rent_type).toBe('quarterly');
	});

	it('should assign filter price min value', () => {
		wrapper.vm.changePriceMin('2000000');
		expect(wrapper.vm.filterTemp.price_range[0]).toBe(2000000);
	});

	it('should assign filter price max value', () => {
		wrapper.vm.changePriceMax('5000000');
		expect(wrapper.vm.filterTemp.price_range[1]).toBe(5000000);
	});

	it('should emit handle mobile filter', () => {
		wrapper.setProps({ selectedLocation: 'Yogyakarta' });
		wrapper.vm.setAllFilter();
		expect(wrapper.emitted('mobileFilterHandler')).toBeTruthy();
	});
});
