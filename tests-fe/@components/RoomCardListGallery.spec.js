import { createLocalVue, shallowMount } from '@vue/test-utils';
import RoomCardListGallery from 'Js/@components/RoomCardListGallery.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

const propsData = {
	isPromoted: true,
	isNativeShare: true,
	notOwner: true,
	isHorizontal: true,
	showFavoriteCondition: true,
	isLove: true
};

describe('RoomCardListGallery.vue', () => {
	const wrapper = shallowMount(RoomCardListGallery, {
		localVue,
		propsData
	});

	it('should render component', () => {
		expect(wrapper.find('#roomCardListGallery').exists()).toBe(true);
		expect(wrapper.find('.room-card-gallery-horizontal').exists()).toBe(true);
		expect(wrapper.find('fav-share-widget-stub').exists()).toBe(true);
	});

	it('should render promoted info', () => {
		expect(wrapper.find('.room-labels-sponsor').exists()).toBe(true);
		expect(wrapper.find('.room-general-img').exists()).toBe(true);
	});

	it('should render room cover', async () => {
		await wrapper.setProps({ photoList: { large: 'photo-large.png' } });

		expect(wrapper.find('.room-cover img').attributes('data-src')).toBe(
			'photo-large.png'
		);
	});
});
