import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserProfilDropdown from 'Js/@components/UserProfilDropdown.vue';

window.oxWebUrl = 'local.mamikos.com';

describe('UserProfilDropdown.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		userName: 'user name',
		profilPicture: 'profile/picture/image.jpg',
		isTenant: true,
		onLogout: jest.fn()
	};

	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(UserProfilDropdown, {
			localVue,
			propsData
		});
	});

	it('should render correctly', () => {
		expect(wrapper.find('.user-profil-dropdown').exists()).toBe(true);
	});

	it('should render the tenant links correctly', () => {
		const dropdownMenuList = wrapper.findAll('.dropdown-menu > li');
		expect(dropdownMenuList.length).toBe(2);
	});

	it('should render the owner links correctly', () => {
		wrapper = shallowMount(UserProfilDropdown, {
			localVue,
			propsData: {
				userName: 'user name',
				profilPicture: 'profile/picture/image.jpg',
				isTenant: false,
				onLogout: jest.fn()
			}
		});

		const dropdownMenuList = wrapper.findAll('.dropdown-menu > li');
		expect(dropdownMenuList.length).toBe(2);
	});

	it('should render the logout function correctly', async () => {
		const dropdownMenuList = wrapper.findAll('.dropdown-menu > li');
		const logoutList = dropdownMenuList.at(dropdownMenuList.length - 1);

		expect(logoutList.exists()).toBe(true);
		logoutList.find('a').trigger('click');

		await wrapper.vm.$nextTick();
		expect(wrapper.emitted().onLogout).toBeTruthy();
	});
});
