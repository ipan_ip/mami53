import { shallowMount } from '@vue/test-utils';
import MamiModalWrap from 'Js/@components/MamiModalWrap.vue';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

describe('MamiModalWrap.vue', () => {
	let wrapper;

	const createComponent = options => {
		wrapper = shallowMount(MamiModalWrap, {
			stubs: {
				modal: mockComponentWithAttrs({
					class: 'modal-wrap',
					'@close': "$emit('closed')"
				})
			},
			propsData: {
				showModal: true
			}
		});
	};

	it('should render MamiModalWrap', () => {
		createComponent();

		const mamiModalWrapContainer = wrapper.find('.modal-container');
		expect(mamiModalWrapContainer.exists()).toBe(true);
	});

	it('should update method on click option', () => {
		createComponent();
		const modalEl = wrapper.find('.modal-wrap');
		expect(modalEl.exists()).toBe(true);

		modalEl.trigger('close');
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
