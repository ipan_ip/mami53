import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterFacList from 'Js/@components/FilterFacList.vue';

const localVue = createLocalVue();

describe('FilterFacList.vue', () => {
	const propsData = { tagValue: ['a', 'b', 'c'] };
	const wrapper = shallowMount(FilterFacList, { localVue, propsData });

	it('should load the component', () => {
		expect(wrapper.find('.filter-checkbox-container').exists()).toBe(true);
	});

	it('should return correct tag ids', () => {
		expect(wrapper.vm.tagIds.length).toBe(3);
		expect(wrapper.vm.tagIds[1]).toBe('b');
	});

	it('should emit new tag ids value', () => {
		wrapper.vm.tagIds = ['d', 'e'];
		expect(wrapper.emitted('setTagIds')).toBeTruthy();
	});
});
