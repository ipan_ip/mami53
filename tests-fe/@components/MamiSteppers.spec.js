import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiSteppers from 'Js/@components/MamiSteppers.vue';

describe('MamiSteppers.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		steps: [
			'Pengajuan<br>Sewa',
			'Tunggu<br>Konfirmasi',
			'Pembayaran<br>Sewa',
			'Tempati<br>Kamar'
		],
		activeStep: 0,
		labelBold: true
	};
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(MamiSteppers, {
			localVue,
			propsData
		});
	});

	it('should render the steppers correctly', () => {
		const mamiSteppers = wrapper.find('.mami-steppers');
		expect(mamiSteppers.exists()).toBe(true);
	});

	it('should render the steps in steppers correctly', () => {
		const mamiSteppersSteps = wrapper.findAll('.mami-steppers-step');
		expect(mamiSteppersSteps.length).toBe(propsData.steps.length);

		const firstStepper = mamiSteppersSteps.at(0);
		expect(firstStepper.classes()).toContain('is-active');
		expect(firstStepper.classes()).toContain('is-bold');
	});

	it('should render the steps slot correctly', () => {
		wrapper = shallowMount(MamiSteppers, {
			localVue,
			slots: {
				steps: `<div class="mami-steppers-step steps-slot" >
					stepText
				</div>`
			}
		});

		const mamiSteppersSteps = wrapper.findAll('.mami-steppers-step.steps-slot');
		expect(mamiSteppersSteps.length).toBe(1);
	});

	it('should render the step slot correctly', () => {
		wrapper = shallowMount(MamiSteppers, {
			localVue,
			slots: {
				step: `<span class="step-content" >
					stepContent
				</span>`
			},
			propsData: {
				steps: ['Pengajuan<br>Sewa', 'Pengajuan<br>Dua']
			}
		});

		const mamiSteppersSteps = wrapper.findAll('.mami-steppers-step');
		expect(mamiSteppersSteps.length).toBe(2);
		const stepperSlots = wrapper.findAll('.step-content');
		expect(stepperSlots.length).toBe(2);
	});

	it('should toggle active class when it is changed', async () => {
		wrapper = shallowMount(MamiSteppers, {
			localVue,
			slots: {
				step: `<span class="step-content" >
					stepContent
				</span>`,
				steps: `<li class="step-1" /><li class="step-2" /><li class="step-3" />`
			},
			propsData: {
				steps: ['Pengajuan<br>Sewa', 'Pengajuan<br>Dua', 'Pebayaran']
			}
		});

		const spyToggleActiveClass = jest.spyOn(wrapper.vm, 'toggleActiveClass');
		await wrapper.setProps({ activeStep: 2 });
		expect(spyToggleActiveClass).toBeCalled();
		spyToggleActiveClass.mockRestore();
	});
});
