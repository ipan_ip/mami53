import { shallowMount, createLocalVue } from '@vue/test-utils';
import OverlayStepperModal from 'Js/@components/OverlayStepperModal';
import '@babel/polyfill';
import '../utils/mock-vue';
import _omit from 'lodash/omit';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import * as storage from 'Js/@utils/storage';

global.Vue = require('vue');

describe('OverlayStepperModal.vue', () => {
	const localVue = createLocalVue();

	const onCloseFunction = jest.fn();
	const propsData = {
		isAllowInteraction: true,
		isShow: true,
		isClosable: false,
		hightlightedTarget: '.onboarding-ftue-content',
		onClose: onCloseFunction,
		cover: {
			image: {
				large: '/cover/image.jpg'
			},
			title: 'imageTitle'
		},
		steps: [
			{
				image: {
					large: 'step/image.jpg'
				},
				title: 'stepTitle',
				content: 'stepContent'
			}
		]
	};

	const wrapper = shallowMount(OverlayStepperModal, {
		localVue,
		propsData,
		mocks: {
			navigator: {
				isMobile: true
			}
		},
		stubs: {
			swiper,
			swiperSlide
		}
	});

	global.tracker = jest.fn();

	const documentBodyClassList = {
		value: [],
		add(classToBeAdded = '') {
			if (this.value && classToBeAdded) {
				this.value.push(classToBeAdded);
			}
		},
		remove(classToBeRemoved = '') {
			if (this.value && this.value.includes(classToBeRemoved)) {
				_omit(this.value, classToBeRemoved);
			}
		}
	};
	const querySelector = jest.fn();

	const tempDocument = document;

	mockWindowProperty('document', {
		querySelector,
		body: {
			classList: documentBodyClassList
		},
		createComment: jest.fn()
	});
	mockWindowProperty('open', jest.fn());

	it('renders the component correctly', () => {
		const bookingBenefitModal = wrapper.find('.onboarding-ftue');
		expect(bookingBenefitModal.exists()).toBe(true);
		expect(document.body.classList.value).toContain('action-disable');
	});

	it('renders the isClosable prop correctly', () => {
		const { isClosable, hightlightedTarget } = propsData;
		if (isClosable) {
			const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');
			const closeButton = wrapper.find('.onboarding-close');
			expect(closeButton.exists()).toBe(true);

			closeButton.trigger('click');

			const highlightedWrapper = wrapper.find(hightlightedTarget);
			expect(highlightedWrapper).not.toContain('highlighted');
			expect(document.body.classList.remove).toBeCalled();
			expect(spyStorageLocalSetItem).toHaveBeenCalled();
			expect(onCloseFunction).toBeCalled();
		}
	});

	it('renders the onClick function of the booking modal correctly', () => {
		const bookingBenefitModal = wrapper.find('.onboarding-ftue.highlighted');
		let count = 0;
		let prevCount = count;
		bookingBenefitModal.trigger('click', {
			stopPropagation() {
				prevCount = count;
				count += 1;
			}
		});
		expect(count).toBe(prevCount + 1);
	});

	it('renders the device name correctly', () => {
		const isMobile = Boolean(wrapper.vm.navigator.isMobile);
		const { deviceName } = wrapper.vm;

		const expectedDeviceName = isMobile ? 'mobile' : 'desktop';
		expect(deviceName).toBe(expectedDeviceName);
	});

	it('renders cover and cover image correctly', () => {
		const { isShowCover, cover } = wrapper.vm;
		if (isShowCover && cover) {
			const stepCover = wrapper.find('.step-cover');
			expect(stepCover.exists()).toBe(true);

			const stepCoverImage = stepCover.find('img');
			if (stepCoverImage.exists()) {
				expect(stepCoverImage.attributes('src')).toBe(cover.image.large);
				expect(stepCoverImage.attributes('alt')).toBe(cover.title);
			}
		}
	});

	it("should call handleOnClose function on 'Saya Mengerti' button click", () => {
		const ctaButton = wrapper.find('.step-swiper .cta-wrapper .btn-primary');
		const spyHandleOnClose = jest.spyOn(wrapper.vm, 'handleOnClose');
		wrapper.setProps({
			steps: [
				{
					image: {
						large: 'step/image.jpg'
					},
					title: 'stepTitle',
					content: 'stepContent'
				}
			]
		});
		wrapper.setData({ activeSlideIndex: 1 });
		const isCtaExist = ctaButton.exists();
		const isNextButton = isCtaExist
			? ctaButton.classes().includes('btn-next')
			: false;

		if (isCtaExist) {
			if (isNextButton) {
				expect(ctaButton.text()).toBe('Lanjut');
			} else {
				ctaButton.trigger('click');
				expect(ctaButton.text()).toBe('Saya Mengerti');
				expect(spyHandleOnClose).toHaveBeenCalled();
			}
		}
	});

	it('should call handleClickRedirect function when cta link is clicked', () => {
		const handleClickRedirectLink = wrapper.find('.step-swiper .cta-wrapper a');
		if (handleClickRedirectLink.exists()) {
			const spyHandleClickRedirect = jest.spyOn(
				wrapper.vm,
				'handleClickRedirect'
			);
			const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');
			handleClickRedirectLink.trigger('click');
			expect(spyHandleClickRedirect).toHaveBeenCalled();
			expect(tracker).toBeCalled();
			expect(spyStorageLocalSetItem).toHaveBeenCalled();
			expect(window.open).toBeCalled();
		}
	});

	describe('actions', () => {
		global.document = tempDocument;

		const highlightedTarget = () =>
			document.querySelector('.highlighted-target');

		const spyAddEventListener = jest.spyOn(window, 'addEventListener');
		const highlightedElement = document.createElement('div');
		highlightedElement.classList.add('highlighted-target');

		document.body.appendChild(highlightedElement);

		const wrapper = shallowMount(OverlayStepperModal, {
			localVue,
			mocks: {
				navigator: {
					isMobile: false
				}
			},
			propsData: {
				...propsData,
				hightlightedTarget: '.highlighted-target',
				isClosable: true
			},
			stubs: {
				swiper,
				swiperSlide
			}
		});

		it('should set highlighted element properly', async () => {
			await wrapper.vm.$nextTick();

			expect(spyAddEventListener).toBeCalledWith('click', expect.any(Function));
			expect(document.body.classList.contains('action-disable')).toBe(true);
			expect(highlightedTarget().classList.contains('highlighted')).toBe(true);

			spyAddEventListener.mockRestore();
		});

		it('should handle close properly', async () => {
			wrapper.vm.handleOnClose();
			await wrapper.vm.$nextTick();

			expect(document.body.classList.contains('action-disable')).toBe(false);
			expect(highlightedTarget().classList.contains('highlighted')).toBe(false);

			await wrapper.setProps({ storageName: 'test' });
			const spyStorageSetItem = jest.spyOn(storage.local, 'setItem');
			highlightedTarget().classList.add('highlighted');
			await wrapper.vm.$nextTick();
			wrapper.vm.handleOnClose();

			expect(spyStorageSetItem).toBeCalledWith('test', true);
			spyStorageSetItem.mockRestore();

			const spyClassRemove = jest.spyOn(
				wrapper.vm.highlightedElement.classList,
				'remove'
			);
			wrapper.setData({ highlightedElement: null });
			wrapper.vm.handleOnClose();
			await wrapper.vm.$nextTick();

			expect(spyClassRemove).not.toBeCalled();
			spyClassRemove.mockRestore();
		});

		it('should handle on swipe properly', async () => {
			await wrapper.setProps({
				cover: { image: { large: 'image-large.png' }, title: 'image large' }
			});
			await wrapper.setData({ isShowCover: false });
			await wrapper.vm.$nextTick();

			wrapper.findComponent(swiper).vm.$emit('slideChange');

			expect(wrapper.emitted('swipeTracker')[0]).toBeTruthy();
		});

		it('should handle last slide click properly', async () => {
			const spyWindowOpen = jest.spyOn(window, 'open');
			const spyAddLocalStorageFlag = jest.spyOn(
				wrapper.vm,
				'addLocalStorageFlag'
			);

			await wrapper.setProps({
				cover: { image: { large: 'image-large.png' }, title: 'image large' },
				steps: [
					{
						image: { large: 'step-image.png' },
						title: 'step 1',
						content: 'step 1 content'
					}
				],
				redirect: {
					label: 'redirect label',
					url: 'https://redirect-url.com'
				},
				storageName: 'test'
			});
			await wrapper.setData({ isShowCover: false });
			wrapper.vm.navigator.isMobile = true;

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.isLastSlide).toBe(true);

			wrapper.find('.cta-wrapper .btn-secondary').trigger('click.prevent');

			expect(wrapper.emitted('redirectTracker')[0][0]).toBe('mobile');
			expect(spyWindowOpen).toBeCalledWith('https://redirect-url.com');
			expect(spyAddLocalStorageFlag).toBeCalledWith();

			jest.clearAllMocks();
			wrapper.vm.navigator.isMobile = false;
			await wrapper.setProps({
				storageName: '',
				redirect: { label: 'redirect label', url: '' }
			});
			await wrapper.vm.$nextTick();

			wrapper.find('.cta-wrapper .btn-secondary').trigger('click.prevent');
			await wrapper.vm.$nextTick();

			expect(wrapper.emitted('redirectTracker')[1][0]).toBe('desktop');
			expect(spyWindowOpen).not.toBeCalledWith();
			expect(spyAddLocalStorageFlag).not.toBeCalled();

			spyWindowOpen.mockRestore();
			spyAddLocalStorageFlag.mockRestore();
		});

		it('should handle show step properly', async () => {
			await wrapper.setData({ isShowCover: true });
			await wrapper.setProps({
				cover: { image: { large: 'image-large.png' }, title: 'image large' },
				steps: [
					{
						image: { large: 'step-image.png' },
						title: 'step 1',
						content: 'step 1 content'
					}
				]
			});

			wrapper.find('.cta-wrapper button').trigger('click');

			expect(wrapper.vm.isShowCover).toBe(false);
		});

		it('should remove click listener on window before component destroyed', () => {
			const spyRemoveEventListener = jest.spyOn(window, 'removeEventListener');

			wrapper.vm.$destroy();

			expect(spyRemoveEventListener).toBeCalledWith(
				'click',
				expect.any(Function)
			);

			spyRemoveEventListener.mockRestore();
		});
	});
});
