import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from '../utils/mock-v-lazy';
import '../utils/mock-vue';
import MamiFooter from 'Js/@components/MamiFooter.vue';

describe('MamiFooter.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const authCheck = { all: true, owner: true };
	global.authCheck = authCheck;
	global.tracker = jest.fn();
	window.open = jest.fn(() => ({ opener: '' }));
	Object.defineProperty(window, 'location', {
		value: {
			href: ''
		}
	});

	const wrapper = shallowMount(MamiFooter, {
		localVue,
		window: { authCheck }
	});

	it('should render footer content', () => {
		expect(wrapper.find('.product-identity').exists()).toBe(true);
		expect(wrapper.find('.site-links').exists()).toBe(true);
		expect(wrapper.find('.site-links-social').exists()).toBe(true);
		expect(wrapper.find('.site-links-contact').exists()).toBe(true);
		expect(wrapper.find('.footer-copyright').exists()).toBe(true);
	});

	it('should open play store url', () => {
		wrapper.vm.handleClickDownload({
			name: 'Google Play'
		});
		expect(window.open).toBeCalledWith();
	});

	it('should open whatsapp chat', () => {
		wrapper.vm.handleContactWhatsapp('08123444566');
		expect(window.location.href).toBe(
			'https://api.whatsapp.com/send?phone=08123444566&text=Hai%20Mamikos%2C'
		);
	});

	it('should open contact link', () => {
		const link = { action: jest.fn() };
		wrapper.vm.handleContactClick({ url: 'mamikos.com' });
		expect(window.location.href).toBe('mamikos.com');
		wrapper.vm.handleContactClick(link);
		expect(link.action).toBeCalled();
	});
});
