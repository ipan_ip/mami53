import { shallowMount } from '@vue/test-utils';
import ComplaintAttachmentUpload from 'Js/@components/complaint-form/ComplaintAttachmentUpload';

describe('ComplaintAttachmentUpload.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ComplaintAttachmentUpload, options);
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should trigger click at input file when picker was clicked', () => {
		const wrapper = mountComponent();
		const inputClick = jest.fn();
		wrapper.vm.$refs.inputFile.click = inputClick;

		const picker = wrapper.find('.complaint-attachment-upload__picker');
		picker.trigger('click');

		expect(inputClick).toHaveBeenCalled();
	});
});
