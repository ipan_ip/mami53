import { createLocalVue, shallowMount } from '@vue/test-utils';
import DeviceCameraDisplay from 'Js/@components/DeviceCameraDisplay.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

const bugsnagNotify = jest.fn();
global.bugsnagClient = { notify: bugsnagNotify };

describe('DeviceCameraDisplay.vue', () => {
	describe('no browser support', () => {
		mockWindowProperty('window.navigator', { mediaDevices: null });

		const wrapper = shallowMount(DeviceCameraDisplay, { localVue });

		it('should emit no browser support', () => {
			expect(wrapper.emitted('no-browser-support')[0]).toBeTruthy();
		});
	});

	describe('camera initiation', () => {
		const stop = jest.fn();
		const getTracks = jest.fn(() => [{ stop }]);
		const InputDeviceInfos = [{ kind: 'videoinput', groupId: 'asdGroupId123' }];
		const stream = { getTracks };
		const mediaDevices = {
			enumerateDevices: jest.fn().mockResolvedValue(InputDeviceInfos),
			getUserMedia: jest.fn().mockResolvedValue(stream)
		};

		mockWindowProperty('window.navigator', { mediaDevices });

		const wrapper = shallowMount(DeviceCameraDisplay, { localVue });

		it('should set available camera devices', async () => {
			await new Promise(resolve => setImmediate(resolve));
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.availableCameraDevices).toEqual(InputDeviceInfos);
			expect(wrapper.vm.$refs.video.srcObject.getTracks()[0]).toEqual(
				expect.objectContaining({
					stop: expect.any(Function)
				})
			);
		});

		it('should stop all streams first if there is', () => {
			mockWindowProperty('window.stream', stream);
			wrapper.vm.startVideoStream();

			expect(stop).toBeCalled();
		});

		it('should set ideal video dimension if provided', async () => {
			await wrapper.setProps({
				videoConstraintIdealWidth: 200,
				videoConstraintIdealHeight: 300
			});
			wrapper.vm.startVideoStream();

			expect(wrapper.vm.videoConstraints).toEqual({
				height: { ideal: 300 },
				width: { ideal: 200 }
			});
		});

		it('should set camera facing properly', async () => {
			mockWindowProperty('window.navigator.isMobile', true);
			await wrapper.setProps({ isFacingRear: true });
			wrapper.vm.startVideoStream();

			expect(wrapper.vm.videoConstraints.facingMode).toEqual({
				exact: 'environment'
			});

			await wrapper.setProps({ isFacingRear: false });
			wrapper.vm.startVideoStream();

			expect(wrapper.vm.videoConstraints.facingMode).toEqual({
				exact: 'user'
			});
		});

		it('should handle stream error properly', async () => {
			mockWindowProperty('window.navigator.mediaDevices', {
				getUserMedia: jest.fn().mockRejectedValue('error')
			});

			wrapper.vm.startVideoStream();
			await new Promise(resolve => setImmediate(resolve));

			expect(bugsnagNotify).toBeCalledWith('error');

			mockWindowProperty('window.navigator.mediaDevices', {
				getUserMedia: jest.fn().mockRejectedValue({ name: 'NotAllowedError' })
			});

			wrapper.vm.startVideoStream();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.emitted('permission-denied')[0]).toBeTruthy();
		});
	});

	describe('camera action', () => {
		const stop = jest.fn();
		const getTracks = jest.fn(() => [{ stop }]);
		const InputDeviceInfos = [{ kind: 'videoinput', groupId: 'asdGroupId123' }];
		const stream = { getTracks };
		const mediaDevices = {
			enumerateDevices: jest.fn().mockResolvedValue(InputDeviceInfos),
			getUserMedia: jest.fn().mockResolvedValue(stream)
		};

		mockWindowProperty('window.navigator', { mediaDevices });

		const wrapper = shallowMount(DeviceCameraDisplay, { localVue });

		it('should handle capture photo properly', async () => {
			expect(wrapper.find('.device-camera-display__video').exists()).toBe(true);

			const drawImage = jest.fn();
			jest
				.spyOn(wrapper.vm.$refs.canvas, 'getContext')
				.mockImplementation(() => {
					return {
						drawImage
					};
				});

			jest
				.spyOn(wrapper.vm.$refs.canvas, 'toDataURL')
				.mockImplementation(() => {
					return 'http://url.com/image-data-url';
				});

			wrapper.vm.capturePhoto();
			await wrapper.vm.$nextTick();

			expect(drawImage).toBeCalled();
			expect(wrapper.vm.isPhotoTaken).toBe(true);

			const emittedValue = wrapper.emitted('finish-capture-photo')[0][0];
			expect(emittedValue instanceof Image).toBe(true);
			expect(emittedValue.src).toEqual('http://url.com/image-data-url');
		});

		it('should handle isCapturePhoto props properly', async () => {
			const spyCapturePhoto = jest.spyOn(wrapper.vm, 'capturePhoto');
			await wrapper.setProps({ isCapturePhoto: true, isPhotoTaken: true });

			expect(spyCapturePhoto).toBeCalled();

			await wrapper.setProps({ isCapturePhoto: false });

			expect(wrapper.vm.isPhotoTaken).toBe(false);
		});

		it('should stop all stream before component destroyed', () => {
			mockWindowProperty('window.stream', stream);

			wrapper.vm.$destroy();

			expect(stop).toBeCalled();
		});
	});
});
