import { shallowMount, createLocalVue } from '@vue/test-utils';
import '../utils/mock-cookies';
import '../utils/mock-vue';
import Vuex from 'vuex';
import NavbarCustom from 'Js/@components/NavbarCustom';
import { mockComponent, mockComponentWithAttrs } from '../utils/mock-component';
import mockWindowProperty from '../utils/mock-window-property';

window.oxWebUrl = 'local.mamikos.com';
window.oxOauth2Domain = 'mamikos.com';

describe('NavbarCustom.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const authCheck = {
		all: false,
		owner: false,
		user: false
	};
	const authData = {
		all: {
			name: 'mamitest123'
		}
	};
	const propsData = {
		navigations: [
			{ name: 'Cari kost', url: '/cari' },
			{ name: 'Cari apartemen', url: '/apartemen' },
			{ name: 'Cari lowongan kerja', url: '/loker' }
		],
		showInputSearch: true,
		showPromotion: true,
		hasChatMenu: true
	};
	const store = {
		state: { authCheck, authData }
	};
	const parentComponent = {
		data() {
			return {
				state: {
					authCheck,
					authData
				}
			};
		}
	};
	const stubs = {
		ModalFormLogin: mockComponentWithAttrs({
			class: 'login-user-stub',
			'@click': "$emit('close')"
		}),
		SearchBoxModal: mockComponentWithAttrs({
			class: 'search-box-modal-stub',
			'@click': "$emit('hide-modal')"
		}),
		LoginUserJquery: mockComponent,
		NotifCenter: mockComponent
	};
	global.authCheck = authCheck;
	global.authData = authData;
	const sb = {
		getTotalUnreadChannelCount: jest.fn(fn => fn(3, null)),
		disconnect: jest.fn(fn => fn())
	};
	global.sb = sb;
	const sbWidget = {
		registerLocalHandlerCb: jest.fn((event, cb) => cb())
	};
	global.sbWidget = sbWidget;
	const $toasted = {
		show: jest.fn()
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(NavbarCustom, {
			parentComponent,
			localVue,
			stubs,
			propsData,
			mocks: { $toasted }
		});
	});

	it('should render navbarCustomize', () => {
		expect(wrapper.find('#navbarCustomize').exists()).toBe(true);
	});

	it('should toggle navbar when navbar-toggle button clicked', () => {
		const isSidenav = wrapper.vm.isSidenav;
		wrapper.find('.navbar-toggle').trigger('click');
		expect(wrapper.vm.isSidenav === !isSidenav).toBe(true);
	});

	it('should open modal login when sebagai pencari clicked', () => {
		wrapper
			.find('.navbar-menu-container .nav > li.user-login > a')
			.trigger('click');
		expect(wrapper.vm.isModalLoginVisible).toBe(true);
	});

	it('should close modal login when login user close triggered', () => {
		wrapper.find('.login-user-stub').trigger('click');
		expect(wrapper.vm.isModalLoginVisible).toBe(false);
	});

	it('should hide search box modal when search box modal hide-modal triggered', () => {
		wrapper.find('.search-box-modal-stub').trigger('click');
		expect(wrapper.vm.searchBoxModalShown).toBe(false);
	});

	it('should show search box modal when button input search clicked', () => {
		wrapper.find('.btn-input-search').trigger('click');
		expect(wrapper.vm.searchBoxModalShown).toBe(true);
	});

	it('should change isScrolled when window is scrolled', () => {
		mockWindowProperty('pageYOffset', 200);
		mockWindowProperty(
			'addEventListener',
			jest.fn((event, cb) => {
				if (event === 'scroll') return cb();
				else cb;
			})
		);
		wrapper.vm.scrollWindow();
		expect(wrapper.vm.isScrolled).toBe(true);
		mockWindowProperty('pageYOffset', 50);
		wrapper.vm.scrollWindow();
		expect(wrapper.vm.isScrolled).toBe(false);
	});

	describe('BackPrevPage', () => {
		const reload = jest.fn();
		const go = jest.fn();
		const close = jest.fn();
		mockWindowProperty('location.reload', reload);
		mockWindowProperty('history.go', go);
		mockWindowProperty('close', close);
		it('should go to correct page when back button clicked', () => {
			wrapper.find('.navbar-brand a:last-of-type').trigger('click');
			expect(close).toBeCalled();
		});
	});

	describe('Share This Page', () => {
		const mockedShareUrlDOM = {
			setAttribute: jest.fn(),
			select: jest.fn()
		};
		mockWindowProperty('document.getElementById', () => mockedShareUrlDOM);
		mockWindowProperty('document.execCommand', jest.fn());
		mockWindowProperty(
			'getSelection',
			jest.fn(() => {
				return {
					removeAllRanges: jest.fn()
				};
			})
		);

		it('should copy share link when nav-promotion link clicked and no share in navigatior', () => {
			wrapper
				.find('.navbar-nav .nav-promotion:nth-of-type(2) > a')
				.trigger('click');

			expect($toasted.show).toBeCalled();
		});

		it('should call navigator share when nav-promotion link clicked if navigator has share method', () => {
			mockWindowProperty('navigator.share', jest.fn());
			wrapper
				.find('.navbar-nav .nav-promotion:nth-of-type(2) > a')
				.trigger('click');

			expect(global.navigator.share).toBeCalled();
		});
	});

	describe('Logged in', () => {
		let wrapper;
		beforeEach(() => {
			authCheck.all = true;
			authCheck.user = true;
			global.authCheck = authCheck;
			wrapper = shallowMount(NavbarCustom, {
				localVue,
				stubs,
				store: new Vuex.Store(store),
				propsData
			});
		});

		it('should has longName true', () => {
			expect(wrapper.vm.isLongName).toBe(true);
		});

		it('should disconnect sb when logout is clicked', () => {
			wrapper
				.find('.user-nav .dropdown-menu li:last-of-type a')
				.trigger('click');
			expect(sb.disconnect).toBeCalled();
			expect(Cookies.remove).toBeCalled();
		});
	});

	describe('isHomePage', () => {
		const reload = jest.fn();
		mockWindowProperty('location.reload', reload);

		it('should call window location reload when backPrevPage called', () => {
			wrapper.setProps({ isHomePage: true });
			wrapper.vm.backPrevPage();
			expect(reload).toBeCalled();
		});
	});

	describe('OpenChat', () => {
		const click = jest.fn();
		const querySelector = jest.fn(() => {
			return {
				click
			};
		});
		mockWindowProperty('document.querySelector', querySelector);

		it('should click widget', () => {
			wrapper.vm.openChat();
			expect(click).toBeCalled();
		});

		it('should not click widget', () => {
			global.console = {
				error: jest.fn()
			};
			mockWindowProperty(
				'document.querySelector',
				jest.fn(() => {
					throw new Error('error');
				})
			);
			wrapper.vm.openChat();
			expect(click).toBeCalled();
		});
	});
});
