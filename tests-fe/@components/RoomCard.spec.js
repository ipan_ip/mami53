import { createLocalVue, shallowMount } from '@vue/test-utils';
import RoomCard from 'Js/@components/RoomCard.vue';

const localVue = createLocalVue();

const propsData = {
	availableRoom: 2,
	isGoldplus: true,
	goldplusName: 'Goldplus 1',
	isFlashSale: true,
	priceOtherDiscount: '20000',
	priceLabel: 'Rp200.000',
	isPromoted: false,
	isNativeShare: false,
	isLove: true,
	lastOnline: '2020/02/02',
	rating: '5.0',
	priceTitleTime: '',
	isPremiumOwner: true,
	unitType: 'kost',
	furnishedStatus: '',
	unitFloor: 1,
	roomTitle: 'Room Title',
	isVerified: true,
	showFavoriteCondition: false,
	showPromotionCondition: false,
	showFavorite: false,
	roomLevelId: 1,
	isHorizontal: true,
	promoName: 'Promo',
	isKost: true,
	areaName: 'Jakarta',
	rentTypeUnit: 'Per Bulan',
	topFacilities: 'Kamar Mandi Dalam'
};

const stubs = {
	TagText: { template: '<div />' }
};

describe('RoomCard.vue', () => {
	const wrapper = shallowMount(RoomCard, {
		localVue,
		propsData,
		stubs
	});

	it('should render component', () => {
		expect(wrapper.find('.room-card-horizontal').exists()).toBe(true);
		expect(wrapper.find('room-card-list-gallery-stub').exists()).toBe(true);
	});

	it('should render promo', () => {
		expect(wrapper.find('.room-promo').exists()).toBe(true);
		expect(wrapper.find('.room-promo > span').text()).toBe('Promo');
	});

	it('should render room info', () => {
		// title
		expect(wrapper.find('.room-title > h3').text()).toBe('Room Title');

		// verified
		expect(
			wrapper.find('.room-title [data-path=img_kostChecker]').exists()
		).toBe(true);

		// top facilities
		expect(wrapper.find('.room-facilities-text').text()).toBe(
			'Kamar Mandi Dalam'
		);
	});

	it('should render price properly', async () => {
		await wrapper.setProps({
			priceTitleFormat: {
				discount: '10%',
				discount_price: '900.000',
				price: '1.000.000',
				currency_symbol: 'Rp'
			}
		});

		expect(wrapper.find('.discount').text()).toBe('10%');
		expect(
			wrapper
				.find('.previous-prices')
				.text()
				.replace(/\s/g, '')
				.trim()
		).toBe('Rp1.000.000');

		await wrapper.setProps({
			priceTitleFormat: {
				discount: '10%',
				price: '1.000.000',
				currency_symbol: 'Rp'
			},
			priceOtherDiscount: 'Rp900.000'
		});

		expect(wrapper.find('.other-discount').text()).toBe('Rp900.000');
	});
});
