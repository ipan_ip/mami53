import { shallowMount, createLocalVue } from '@vue/test-utils';
import '../utils/mock-vue';
import NavbarOwner from 'Js/@components/NavbarOwner';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from '../utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);
mockWindowProperty('Cookies', {
	remove: jest.fn()
});
window.open = jest.fn();
window.oxOauth2Domain = 'mamikos.com';

global.axios = mockAxios;
global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();
global.sbWidget = { showChannel: jest.fn() };
global.channelListQuery = { next: jest.fn() };
global.Moengage = { track_event: jest.fn() };
const sb = {
	getTotalUnreadChannelCount: jest.fn(fn => fn(3, null)),
	disconnect: jest.fn(fn => fn())
};
global.sb = sb;
global.tracker = jest.fn();

const handleChatMoEngageSpy = jest.spyOn(
	NavbarOwner.methods,
	'handleChatMoEngage'
);
const handleToggleChatSpy = jest.spyOn(NavbarOwner.methods, 'handleToggleChat');
const handleSetUnreadMessagesSpy = jest.spyOn(
	NavbarOwner.methods,
	'handleSetUnreadMessages'
);

const OwnerChatWidget = {
	template: `<div><button class="toggle-chat" @click="$emit('handle-close-chat')"></button><button class="set-unread-chat" @click="$emit('handle-set-unread-messages', 20)"></button></div>`
};

describe('NavbarOwner.vue', () => {
	const authCheck = {
		all: false,
		owner: false,
		user: false
	};
	const authData = { all: { name: 'mamitest123' } };
	const $router = { push: jest.fn() };
	const $route = { fullPath: '/kos' };
	const $event = { stopPropagation: jest.fn() };
	const $root = {
		state: {
			authCheck,
			authData
		}
	};

	const mockStore = {
		state: {
			authCheck,
			authData,
			profile: {
				user: {
					is_verify: true,
					kost_total_active: 0
				},
				membership: {
					status: 'Premium',
					balance_status: 0
				}
			},
			isChatOpen: false
		},
		mutations: {
			setNotifCount: jest.fn(),
			setIsChatOpen(state, payload) {
				state.isChatOpen = payload;
			}
		}
	};

	const generateWrapper = () => {
		const shallowMountProp = {
			localVue,
			store: new Vuex.Store(mockStore),
			stubs: {
				OwnerChatWidget
			},
			mocks: { $route, $root, $router }
		};
		return shallowMount(NavbarOwner, shallowMountProp);
	};

	it('should load the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.navbar-owner-dashboard').exists()).toBe(true);
	});

	it('should close the user dropdown and show the notification dropdown', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleMenuClicked(
			$event,
			'isNotificationDropdownOpened',
			'notificationMenu'
		);
		expect(wrapper.vm.isUserDropdownOpened).toBe(false);
		expect(wrapper.vm.isNotificationDropdownOpened).toBe(true);
	});

	it('should close the notification dropdown and show the user dropdown', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleMenuClicked($event, 'isUserDropdownOpened', 'userMenu');
		expect(wrapper.vm.isNotificationDropdownOpened).toBe(false);
		expect(wrapper.vm.isUserDropdownOpened).toBe(true);
	});

	it('should set notification data when calling getNotifications method on succesful api request', async () => {
		axios.mockResolve({
			data: {
				data: ['a', 'b', 'c', 'd', 'e', 'f', 'g']
			}
		});
		const wrapper = generateWrapper();
		wrapper.vm.getNotifications();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(wrapper.vm.notifData).toEqual(['a', 'b', 'c', 'd', 'e']);
	});

	it('should set isLoading as false and calling bugsnagClient when calling getNotifications method on succesful api request', async () => {
		axios.all = jest.fn().mockRejectedValue();
		const wrapper = generateWrapper();
		wrapper.vm.getNotifications();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(global.bugsnagClient.notify).toBeCalled();
	});

	it('should set profile data when calling getProfile method on succesful api request', async () => {
		axios.mockResolve({
			data: {
				status: true,
				user: {
					profile: ''
				}
			}
		});
		const wrapper = generateWrapper();
		wrapper.vm.getProfile();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.getDataProfile).not.toEqual([]);
	});

	it('should call bugsnagClient when get profile returns false value', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});
		const wrapper = generateWrapper();
		wrapper.vm.getProfile();
		await wrapper.vm.$nextTick();
		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should call bugsnagClient when get profile returns error', async () => {
		axios.mockReject('error');
		const wrapper = generateWrapper();
		wrapper.vm.getProfile();
		await wrapper.vm.$nextTick();
		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should call window.open when notif.type is either survey or chat and  user membership has expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: true
		};
		wrapper.vm.openNotif({ type: 'survey' });
		expect(window.open).toBeCalled();
	});

	it('should call sbWidget.showChannel when notif.type is survey and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'survey',
			survey_data: { chat_group_id: '1' }
		});
		expect(global.sbWidget.showChannel).toBeCalled();
	});

	it('should call sbWidget.showChannel when notif.type is chat and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'chat',
			chat_data: { group_id: '1' }
		});
		expect(global.sbWidget.showChannel).toBeCalled();
	});

	it('should call window.open when notif.type is review and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'review'
		});
		expect(window.open).toBeCalled();
	});

	it('should call $router.push when notif.type is either "premium_verify, "premium_request_reminder", or  "premium_payment_reminder" and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'premium_request_reminder'
		});
		expect($router.push).toBeCalled();
	});

	it('should call $router.push when notif.type is either "balance_verify, "balance_request_reminder" and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'balance_verify'
		});
		expect($router.push).toBeCalled();
	});

	it('should call window.open when notif.type is not empty string,  none of the ones mentioned above and  user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		wrapper.vm.openNotif({
			type: 'test'
		});
		expect(window.open).toBeCalled();
	});

	it('should call channelListQuery.next when notif.type is  survey and user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		global.channelListQuery.next = jest.fn(callback => {
			const params = [
				{
					name: 'Survey - test : test',
					updateMetaData: jest.fn((data, existStatus, cb) => cb())
				},
				{
					name: 'b',
					updateMetaData: jest.fn((data, existStatus, cb) => cb())
				}
			];
			callback && callback(params);
		});
		wrapper.vm.openNotif({
			type: 'survey',
			survey_data: { chat_group_id: null },
			user_name: 'test',
			room_name: 'test'
		});
		expect(global.channelListQuery.next).toBeCalled();
	});

	it('should call channelListQuery.next and then do nothing when notif.type is  survey and user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		global.channelListQuery.next = jest.fn(callback => {
			callback && callback(null, 'error');
		});
		wrapper.vm.openNotif({
			type: 'survey',
			survey_data: { chat_group_id: null },
			user_name: 'test',
			room_name: 'test'
		});
		expect(global.channelListQuery.next).toBeCalled();
	});

	it('should call channelListQuery.next when notif.type is chat and user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		global.channelListQuery.next = jest.fn(callback => {
			const params = [
				{
					name: 'test : test',
					updateMetaData: jest.fn()
				},
				{
					name: 'b',
					updateMetaData: jest.fn()
				}
			];
			callback && callback(params);
		});
		wrapper.vm.openNotif({
			type: 'chat',
			chat_data: { group_id: null },
			user_name: 'test',
			room_name: 'test'
		});
		expect(global.channelListQuery.next).toBeCalled();
	});

	it('should call channelListQuery.next and then do nothing when notif.type is  chat and user membership has not expired', () => {
		const wrapper = generateWrapper();
		wrapper.vm.$store.state.profile.membership = {
			expired: false
		};
		global.channelListQuery.next = jest.fn(callback => {
			callback && callback(null, 'error');
		});
		wrapper.vm.openNotif({
			type: 'chat',
			chat_data: { group_id: null },
			user_name: 'test',
			room_name: 'test'
		});
		expect(global.channelListQuery.next).toBeCalled();
	});

	it('should push router to settings page', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleSettingClicked();
		expect($router.push).toBeCalledWith('/settings');
	});

	it('should disconnect sb when logout is clicked', () => {
		delete window.location;
		window.location = {
			href: {
				includes: jest.fn()
			}
		};

		authCheck.all = true;
		authCheck.user = true;
		const wrapper = generateWrapper();
		wrapper.vm.handleLogoutClicked();
		expect(sb.disconnect).toBeCalled();
	});

	it('should call Moengage when calling handleLogout method', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleLogoutClicked();
		expect(Moengage.track_event).toBeCalled();
	});

	it('should remove oauth token on cookies when calling handleLogout method', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleLogoutClicked();
		expect(Cookies.remove).toBeCalled();
	});

	it('Computed footerText should return correctly based on count of the unread messages', () => {
		const wrapper = generateWrapper();
		expect(wrapper.vm.footerText).toBe('Chat kosong');

		wrapper.setData({ unreadMessages: 10 });

		expect(wrapper.vm.footerText).toBe('10 belum terbaca');
	});

	it('Computed deviceType should return correctly based on the browser navigator', () => {
		global.navigator.isMobile = true;

		let wrapper = generateWrapper();
		expect(wrapper.vm.deviceType).toBe('mobile');

		global.navigator.isMobile = false;

		wrapper = generateWrapper();
		expect(wrapper.vm.deviceType).toBe('desktop');
	});

	it('Watch isChatOpen data should handled correctly and call the tracker if the chat section is open', async () => {
		const wrapper = generateWrapper();

		await wrapper.vm.$store.commit('setIsChatOpen', false);

		expect(handleChatMoEngageSpy).not.toBeCalled();

		await wrapper.vm.$store.commit('setIsChatOpen', true);

		expect(handleChatMoEngageSpy).toBeCalled();
	});

	it('Should call handleToggleChat when user clicking the chat icon', async () => {
		const wrapper = generateWrapper();

		const toggleChatElement = wrapper.find('button.toggle-chat');

		expect(toggleChatElement.exists()).toBe(true);

		toggleChatElement.trigger('click');

		expect(handleToggleChatSpy).toBeCalled();
	});

	it('Should call handleSetUnreadMessagesSpy when the chat count is updated', async () => {
		const wrapper = generateWrapper();

		const unreadChatElement = wrapper.find('button.set-unread-chat');

		expect(unreadChatElement.exists()).toBe(true);

		unreadChatElement.trigger('click');

		expect(handleSetUnreadMessagesSpy).toBeCalledWith(20);
	});
});
