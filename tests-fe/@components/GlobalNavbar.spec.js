import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import Vuex from 'vuex';
import GlobalNavbarStore from './__mocks__/globalNavbarStore';
import GlobalNavbar from 'Js/@components/GlobalNavbar.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
const store = new Vuex.Store(GlobalNavbarStore);
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('window.scrollTo', jest.fn());
mockWindowProperty('oxWebUrl', '/ownerpage');
mockWindowProperty('sessionStorage', {
	setItem: jest.fn(),
	getItem: jest.fn()
});
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const loginUser = {
	authCheck: {
		all: true,
		user: true,
		owner: false
	},
	authData: {
		all: true
	}
};
const loginOwner = {
	authCheck: {
		all: true,
		owner: true,
		user: false
	},
	authData: {
		all: true
	}
};
const notLogin = {
	authCheck: {
		all: false
	},
	authData: {
		all: false
	}
};

const searchBarStub = {
	template: `<div class="open-modal" @click="$emit('onClickEvent')"></div>`
};
const searchBoxModalStub = {
	template: `<div class="close-modal" @click="$emit('hide-modal')"></div>`
};
const modalFormLoginStub = {
	template: `<div class="close-modal-login" @click="$emit('closeModal')"></div>`
};
const userProfileDropdownStub = {
	template: `<div class="user-logout" @click="$emit('onLogout')"></div>`
};
const stubs = {
	'search-bar': searchBarStub,
	'search-box-modal': searchBoxModalStub,
	'modal-form-login': modalFormLoginStub,
	'user-profile-dropdown': userProfileDropdownStub
};

const getChatUnreadCountSpy = jest.spyOn(
	GlobalNavbar.methods,
	'getChatUnreadCount'
);
const handleNavigationBottomSpy = jest.spyOn(
	GlobalNavbar.methods,
	'handleNavigationBottom'
);
const handleOpenChatSpy = jest.spyOn(GlobalNavbar.methods, 'handleOpenChat');
const openModalLoginSpy = jest.spyOn(GlobalNavbar.methods, 'openModalLogin');
const closeModalLoginSpy = jest.spyOn(GlobalNavbar.methods, 'closeModalLogin');
const setLeftPointSpy = jest.spyOn(GlobalNavbar.methods, 'setLeftPoint');
const handleMoEngageSpy = jest.spyOn(GlobalNavbar.methods, 'handleMoEngage');
const setMaxWidthNavbarSpy = jest.spyOn(
	GlobalNavbar.methods,
	'setMaxWidthNavbar'
);
const toggleMobileSpy = jest.spyOn(GlobalNavbar.methods, 'toggleMobile');
const handleScrollSpy = jest.spyOn(GlobalNavbar.methods, 'handleScroll');
const handleLogoutSpy = jest.spyOn(GlobalNavbar.methods, 'handleLogout');
const hideSearchBoxModalSpy = jest.spyOn(
	GlobalNavbar.methods,
	'hideSearchBoxModal'
);
const showSearchBoxModalSpy = jest.spyOn(
	GlobalNavbar.methods,
	'showSearchBoxModal'
);

describe('GlobalNavbar.vue', () => {
	let wrapper = shallowMount(GlobalNavbar, {
		localVue,
		store,
		stubs
	});

	it('Computed bottomNavShowCondition should return correctly', async () => {
		const testCase = [
			{
				...notLogin,
				expected: true
			},
			{
				...loginUser,
				expected: true
			},
			{
				...loginOwner,
				expected: false
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setAuthCheck', test.authCheck);
			wrapper.vm.$store.commit('setAuthData', test.authData);
			wrapper.setData({ navbarMobile: true });
			wrapper.setProps({ needBottomNavigation: true });
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.bottomNavShowCondition).toBe(test.expected);
		}
	});

	it('Computed isShowSearchBar should return correctly', () => {
		wrapper.setProps({ needSearchBox: true });
		wrapper.setData({ navbarMobile: false });

		expect(wrapper.vm.isShowSearchBar).toBe(true);
	});

	it('Computed searchInputValue should return correctly', async () => {
		await wrapper.setProps({ criteria: { keywords: 'UGM' } });

		expect(wrapper.vm.searchInputValue).toEqual('UGM');
	});

	it('Computed unreadCountLabel should return correctly', async () => {
		await wrapper.setData({ unreadChatCount: 100 });

		expect(wrapper.vm.unreadCountLabel).toEqual('99+');

		await wrapper.setData({ unreadChatCount: 2 });

		expect(wrapper.vm.unreadCountLabel).toEqual(2);
	});

	it('Watch redirectionSourceNavbar handler should call correctly', async () => {
		await wrapper.setProps({ redirectionSourceNavbar: 'homepage' });
		expect(window.currentChatSource).toEqual('homepage');
	});

	it('Should call getChatUnreadCount method when user is login on initialize ', () => {
		const testCase = [
			{
				...notLogin,
				called: false
			},
			{
				...loginUser,
				called: true
			}
		];

		for (const test of testCase) {
			wrapper = shallowMount(GlobalNavbar, {
				localVue,
				store,
				stubs,
				computed: {
					authCheck() {
						return test.authCheck;
					},
					authData() {
						return test.authData;
					}
				}
			});

			if (test.called) expect(getChatUnreadCountSpy).toBeCalled();
		}
	});

	it('Should add and remove listener on destroy', () => {
		const addEvent = jest
			.spyOn(global, 'addEventListener')
			.mockImplementation(() => {});
		const removeEvent = jest
			.spyOn(global, 'removeEventListener')
			.mockImplementation(() => {});
		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs
		});
		expect(addEvent).toHaveBeenCalled();
		wrapper.destroy();
		expect(removeEvent).toHaveBeenCalled();
	});

	it('Should call sbWidget method correctly', () => {
		mockWindowProperty('sbWidget', {
			registerLocalHandlerCb: jest.fn((event, cb) => cb())
		});

		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs,
			computed: {
				authCheck() {
					return loginUser.authCheck;
				},
				authData() {
					return loginUser.authData;
				}
			}
		});
		expect(sbWidget.registerLocalHandlerCb).toBeCalled();
	});

	it('Should call handleNavigationBottom method correctly on login', async () => {
		const assignMock = jest.fn();
		delete window.location;
		window.location = { assign: assignMock };

		wrapper.setData({ navbarMobile: true });
		wrapper.setProps({ needBottomNavigation: true });
		await wrapper.vm.$nextTick();

		const bottomNav = wrapper.findAll('div.nav-footer-menu');

		expect(bottomNav.length).toBe(wrapper.vm.bottomNavigations.length);

		const testCase = ['Cari', 'Favorit', 'Chat'];

		for (const test in testCase) {
			await bottomNav.at(test).trigger('click');

			if (testCase[test] === 'Chat') expect(handleOpenChatSpy).toBeCalled();
			expect(handleNavigationBottomSpy).toBeCalledWith(testCase[test]);
		}

		await wrapper.vm.handleNavigationBottom('Default');

		expect(handleNavigationBottomSpy).toBeCalledWith('Default');

		assignMock.mockClear();
	});

	it('Should call handleNavigationBottom method correctly when user not login and handle it correctly', async () => {
		mockWindowProperty('window.location', {
			href: 'https://mamikos.com'
		});

		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs
		});
		wrapper.vm.$store.commit('setAuthCheck', notLogin.authCheck);
		wrapper.vm.$store.commit('setAuthData', notLogin.authCheck);

		await wrapper.vm.handleNavigationBottom('Default');

		expect(handleNavigationBottomSpy).toBeCalledWith('Default');
		expect(openModalLoginSpy).toBeCalled();

		const closeElement = wrapper.find('div.close-modal-login');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLoginVisible).toBe(false);
		expect(wrapper.vm.modalLoginState).toEqual('default');
	});

	it('Should toggle search modal correctly', async () => {
		wrapper.setData({ navbarMobile: true });
		wrapper.setProps({ needSearchBox: true });
		await wrapper.vm.$nextTick();
		const searchElement = wrapper.find('div.open-modal');
		const closeElement = wrapper.find('div.close-modal');

		expect(searchElement.exists()).toBe(true);
		expect(closeElement.exists()).toBe(true);

		await searchElement.trigger('click');

		expect(showSearchBoxModalSpy).toBeCalled();
		expect(wrapper.vm.searchBoxModalShown).toBe(true);

		await closeElement.trigger('click');

		expect(hideSearchBoxModalSpy).toBeCalled();
		expect(wrapper.vm.searchBoxModalShown).toBe(false);

		wrapper.setProps({ openModalWithProps: true });
		await searchElement.trigger('click');

		expect(showSearchBoxModalSpy).toBeCalled();
		expect(wrapper.vm.searchBoxModalShown).toBe(true);

		await closeElement.trigger('click');

		expect(hideSearchBoxModalSpy).toBeCalled();
		expect(wrapper.emitted('closeModalWithProps')).toBeTruthy();
	});

	it('Should call getChatUnreadCount method on initialize with success flow', () => {
		const sb = {
			getTotalUnreadChannelCount: jest.fn(fn => fn(10, null))
		};
		mockWindowProperty('sb', sb);

		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs,
			computed: {
				authData() {
					return loginUser.authData;
				},
				authCheck() {
					return loginUser.authCheck;
				}
			}
		});

		expect(sb.getTotalUnreadChannelCount).toBeCalled();
		expect(wrapper.vm.unreadChatCount).toEqual(10);
	});

	it('Should call getChatUnreadCount method on initialize with fail flow', () => {
		const sb = {
			getTotalUnreadChannelCount: jest.fn(fn => fn(null, true))
		};
		mockWindowProperty('sb', sb);

		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs,
			computed: {
				authData() {
					return loginUser.authData;
				},
				authCheck() {
					return loginUser.authCheck;
				}
			}
		});

		expect(sb.getTotalUnreadChannelCount).toBeCalled();
	});

	it('Should call getChatUnreadCount method on initialize with catch flow', () => {
		const sb = jest.fn();
		mockWindowProperty('sb', sb);

		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs,
			computed: {
				authData() {
					return loginUser.authData;
				},
				authCheck() {
					return loginUser.authCheck;
				}
			}
		});

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('Should call handleOpenChat method and call handleMoEngage method when chat room is not open', async () => {
		const click = jest.fn();
		const querySelectorActive = jest.fn(() => {
			return {
				click,
				classList: {
					contains: jest.fn(() => {
						return true;
					})
				}
			};
		});
		mockWindowProperty('document.querySelector', querySelectorActive);
		wrapper.vm.$store.commit('setAuthCheck', loginUser.authCheck);
		wrapper.vm.$store.commit('setAuthData', loginUser.authData);
		wrapper.setData({ navbarMobile: true });
		wrapper.setProps({ needChatFeature: true });
		await wrapper.vm.$nextTick();
		const chatTriggerElement = wrapper.find('a.nav-icon-group');
		expect(chatTriggerElement.exists()).toBe(true);
		await chatTriggerElement.trigger('click');

		expect(click).toBeCalled();

		const querySelector = jest.fn(() => {
			return {
				click,
				classList: {
					contains: jest.fn(() => {
						return false;
					})
				}
			};
		});
		mockWindowProperty('document.querySelector', querySelector);
		await chatTriggerElement.trigger('click');

		expect(click).toBeCalled();
		expect(handleMoEngageSpy).toBeCalled();
	});

	it('Should call handleMoEngage method correctly on mobile screen', async () => {
		wrapper.setData({ navbarMobile: true });
		wrapper.setProps({
			redirectionSourceNavbar: 'list kos result',
			needBottomNavigation: true
		});
		await wrapper.vm.$nextTick();
		const chatTriggerElement = wrapper.find('a.nav-icon-group');

		expect(chatTriggerElement.exists()).toBe(true);

		await chatTriggerElement.trigger('click');

		expect(handleMoEngageSpy).toBeCalled();
		expect(tracker).toBeCalledWith('moe', [
			'Chat Screen Viewed',
			{
				interface: 'mobile',
				is_owner: false,
				redirection_source: 'list kos result'
			}
		]);
	});

	it('Should call handleMoEngage method correctly on desktop screen', async () => {
		wrapper.setData({ navbarMobile: false });
		wrapper.setProps({
			redirectionSourceNavbar: '',
			redirectionSource: 'homepage',
			needBottomNavigation: true
		});

		await wrapper.vm.$nextTick();
		const chatTriggerElement = wrapper.findAll('li.nav-main-li').at(2);

		expect(chatTriggerElement.exists()).toBe(true);

		await chatTriggerElement.trigger('click');

		expect(handleOpenChatSpy).toBeCalled();
		expect(handleMoEngageSpy).toBeCalled();
	});

	it('Should call handleLogout method correctly', async () => {
		const testCase = [
			{
				sb: undefined,
				value: null
			},
			{
				sb: {
					disconnect: jest.fn(fn => fn())
				},
				value: 'disconnect'
			},
			{
				sb: jest.fn(),
				value: 'function'
			}
		];

		for (const test of testCase) {
			mockWindowProperty('sb', test.sb);
			mockWindowProperty('Cookies', {
				remove: jest.fn()
			});
			mockWindowProperty('console', {
				error: jest.fn()
			});
			await wrapper.setData({ navbarMobile: false });

			const logoutElement = wrapper.find('div.user-logout');

			expect(logoutElement.exists()).toBe(true);

			await logoutElement.trigger('click');

			expect(handleLogoutSpy).toBeCalled();
			expect(Cookies.remove).toBeCalled();

			switch (test.value) {
				case null:
					break;
				case 'disconnect':
					expect(sb.disconnect).toBeCalled();
					break;
				case 'function':
					expect(console.error).toBeCalled();
					break;

				default:
					break;
			}
		}
	});

	it('Should call method BackPrevPage correctly', async () => {
		wrapper.setData({ navbarMobile: true });
		wrapper.setProps({
			homepageStyle: false,
			needBackArrow: true
		});

		await wrapper.vm.$nextTick();
		mockWindowProperty('close', jest.fn());
		const backElement = wrapper.find('a.nav-back');

		expect(backElement.exists()).toBe(true);
		mockWindowProperty('history.go', jest.fn());
		backElement.trigger('click');

		expect(history.go).toBeCalled();
	});

	it('Should call method toggleMobile correctly with navbar exist', async () => {
		const toggleElement = wrapper.find('div.hamburger-container');
		const navElement = wrapper.find('nav.nav-main-navbar');

		expect(toggleElement.exists()).toBe(true);
		expect(navElement.exists()).toBe(true);

		toggleElement.trigger('click');

		expect(toggleMobileSpy).toBeCalled();
		expect(navElement.classes('nav-main-mobile')).toBe(true);
	});

	it('Should call method toggleMobile correctly with navbar not exist', async () => {
		wrapper = shallowMount(GlobalNavbar, {
			localVue,
			store,
			stubs,
			computed: {
				authData() {
					return loginUser.authData;
				},
				authCheck() {
					return loginUser.authCheck;
				}
			}
		});

		await wrapper.setData({ navbarMobile: true });
		const toggleElement = wrapper.find('div.hamburger-container');
		const navElement = wrapper.find('nav.nav-main-navbar');

		wrapper.vm.$refs.navMain = null;
		expect(toggleElement.exists()).toBe(true);
		expect(navElement.exists()).toBe(true);

		toggleElement.trigger('click');

		expect(toggleMobileSpy).toBeCalled();
		expect(navElement.classes('nav-main-mobile')).toBe(false);
	});

	it('Should call setMaxWidthNavbar and setLeftPoint method correctly', () => {
		const testCase = [
			{
				width: 400,
				maxWidth: 0,
				left: false,
				topBarCondition: true
			},
			{
				width: 1366,
				maxWidth: 1024,
				left: true,
				topBarCondition: true
			},
			{
				width: 1366,
				maxWidth: 1024,
				left: true,
				topBarCondition: false
			}
		];

		for (const test of testCase) {
			mockWindowProperty('innerWidth', test.width);
			wrapper = shallowMount(GlobalNavbar, {
				localVue,
				store,
				stubs,
				computed: {
					authData() {
						return loginUser.authData;
					},
					authCheck() {
						return loginUser.authCheck;
					},
					topBarShowCondition() {
						return test.topBarCondition;
					}
				},
				propsData: {
					homepageStyle: true,
					maxWidth: test.maxWidth
				}
			});

			expect(setMaxWidthNavbarSpy).toBeCalled();
			if (test.left) {
				expect(setLeftPointSpy).toBeCalled();
				expect(wrapper.emitted('setLeftPoint')).toBeTruthy();
			}
		}
	});

	it('Should call handleScroll method with various condition correctly', async () => {
		const testCase = [
			{
				width: 400,
				yOffset: 1000,
				mobile: true,
				homepage: false,
				bottomNav: true,
				transparentUntil: 0
			},
			{
				width: 1366,
				yOffset: 1000,
				mobile: false,
				homepage: true,
				bottomNav: false,
				transparentUntil: 1100
			},
			{
				width: 1366,
				yOffset: 200,
				mobile: false,
				homepage: true,
				bottomNav: false,
				transparentUntil: 100
			},
			{
				width: 500,
				yOffset: 35,
				mobile: true,
				homepage: true,
				bottomNav: true,
				transparentUntil: 0
			},
			{
				width: 500,
				yOffset: 50,
				mobile: true,
				homepage: true,
				bottomNav: true,
				transparentUntil: 0,
				destroyRef: true
			}
		];

		for (const test of testCase) {
			mockWindowProperty('innerWidth', test.width);
			mockWindowProperty('pageYOffset', test.yOffset);
			const eventList = {};
			window.addEventListener = jest.fn((event, cb) => {
				eventList[event] = cb;
			});
			wrapper = shallowMount(GlobalNavbar, {
				localVue,
				store,
				stubs,
				computed: {
					authData() {
						return loginUser.authData;
					},
					authCheck() {
						return loginUser.authCheck;
					}
				}
			});
			wrapper.setProps({
				homepageStyle: test.homepage,
				needBottomNavigation: test.bottomNav,
				transparentUntil: test.transparentUntil
			});
			wrapper.setData({ navbarMobile: test.mobile });
			if (test.destroyRef) wrapper.vm.$refs.navBottom = null;
			await wrapper.vm.$nextTick();

			eventList.scroll({ scrollTo: test.yOffset });

			await wrapper.vm.$nextTick();
			expect(handleScrollSpy).toBeCalled();
		}
	});
});
