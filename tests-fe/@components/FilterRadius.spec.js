import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterRadius from 'Js/@components/FilterRadius.vue';

describe('FilterRadius.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		radiusVal: 0
	};
	const wrapper = shallowMount(FilterRadius, { localVue, propsData });

	it('should render filter-radius class', () => {
		expect(wrapper.find('.filter-radius').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.vm.radius).toBe(propsData.radiusVal);
	});

	it('should emit changeRadius when select option changed', () => {
		let radius = 1;
		wrapper.setData({ radius });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().changeRadius[0]).toEqual([radius]);
			expect(wrapper.vm.showNoRadius).toBe(false);
			radius = 0;
			wrapper.setData({ radius });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted().changeRadius[1]).toEqual([radius]);
				expect(wrapper.vm.showNoRadius).toBe(false);
			});
		});
	});
});
