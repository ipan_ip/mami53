import { createLocalVue, shallowMount } from '@vue/test-utils';
import IdVerifForm from 'Js/@components/IdVerifForm.vue';
import mockVLazy from '../utils/mock-v-lazy';

describe('IdVerifForm.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		cardId: 'MAMITEST12ASKXXID',
		idImg: {
			src: 'image/idImg.png'
		},
		selfieImg: {
			src: 'image/selfieImg.png'
		}
	};
	mockVLazy(localVue);
	const wrapper = shallowMount(IdVerifForm, { localVue, propsData });
	const uploadBoxContainer = index =>
		'.upload-box-container > div:nth-of-type(' + index + ')';

	it('should render formIdVerification', () => {
		expect(wrapper.find('#formIdVerification').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.findAll('.delete-button').length).toBe(2);
		expect(wrapper.find('.input-id-warning').exists()).toBe(false);
	});

	it('should emit set-card-id when cardIdVal changed', () => {
		const cardId = 'MAMITESTEMITTEST';
		wrapper.setProps({ cardId });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.cardIdVal).toBe(cardId);
			const cardIdVal = 'NEWMAMITEST123';
			wrapper.setData({ cardIdVal });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('set-card-id')[1]).toEqual([cardIdVal]);
			});
		});
	});

	it('should emit delete-photo when delete button is clicked', () => {
		const deletePhotoCardButton = wrapper.find(
			uploadBoxContainer(1) + ' .delete-button'
		);
		const deletePhotoSelfieButton = wrapper.find(
			uploadBoxContainer(2) + ' .delete-button'
		);

		deletePhotoCardButton.trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('delete-photo')[0]).toEqual(['card']);
			deletePhotoSelfieButton.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('delete-photo')[1]).toEqual(['selfie']);
			});
		});
	});

	it('should emit open-camera when selfie button is clicked', () => {
		wrapper.setProps({ selfieImg: null });
		wrapper.vm.$nextTick(() => {
			const uploadSelfieButton = wrapper.find(uploadBoxContainer(2));
			uploadSelfieButton.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('open-camera')[0]).toEqual(['selfie']);
			});
		});
	});
});
