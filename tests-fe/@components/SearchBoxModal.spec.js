import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';

import SearchBoxModal from 'Js/@components/SearchBoxModal.vue';

mockWindowProperty('location', { pathname: '/area', href: '' });
mockWindowProperty('google', {
	maps: {
		places: {
			AutocompleteSessionToken: jest.fn(),
			AutocompleteService: jest.fn()
		},
		Geocoder: jest.fn().mockImplementation(() => {
			return { geocode: jest.fn() };
		})
	}
});
mockWindowProperty('authData', {
	all: {
		name: 'User',
		photo_user: ''
	}
});
mockWindowProperty('authCheck', {
	all: true,
	user: true
});
mockWindowProperty('navigator', {
	isMobile: false
});
mockWindowProperty('axios', {
	get: jest.fn().mockResolvedValue(),
	post: jest.fn().mockResolvedValue(),
	all: jest.fn().mockResolvedValue(),
	spread: jest.fn().mockReturnValue()
});
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('Cookies', {
	set: jest.fn()
});
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockSwalLoading();

describe('SearchBoxModal.vue', () => {
	let wrapper;
	const localVue = createLocalVue();

	const createComponent = options => {
		wrapper = shallowMount(SearchBoxModal, {
			localVue,
			stubs: {
				SearchBoxModalTabs: mockComponentWithAttrs({
					class: 'search-tabs',
					'@click': "$emit('changed', 'area')"
				}),
				SearchBoxModalPopularPrimary: mockComponentWithAttrs({
					class: 'popular-primary',
					'@click':
						"$emit('select', { name: 'keywords', coordinate: [[110.36178588867188, -7.8089631205593895], [110.43251037597658, -7.72528058053712]] })"
				}),
				SearchBoxModalSuggestions: mockComponentWithAttrs({
					class: 'search-suggestion',
					'@click': "$emit('select', { slug: 'test' })"
				})
			},
			propsData: {
				isShown: true,
				criteriaKeyword: 'Tebet'
			},
			...options
		});
	};

	describe('basic', () => {
		it('should render properly', () => {
			createComponent();

			expect(wrapper.find('.search-box-modal').exists()).toBe(true);
		});

		it('should close modal when click close button', () => {
			createComponent();

			wrapper.find('.form-group-img').trigger('click');
			expect(wrapper.emitted()['hide-modal']).toBeTruthy();
		});
	});

	describe('keyword', () => {
		it('should show reset button if keyword exist', async () => {
			createComponent();
			await wrapper.setData({
				keyword: 'Tebet'
			});

			const resetBtnEl = wrapper.find('.field-reset');

			expect(resetBtnEl.exists()).toBe(true);
		});

		it('should reset keyword when click button reset', async () => {
			createComponent();
			await wrapper.setData({
				keyword: 'asd'
			});
			const resetBtnEl = wrapper.find('.field-reset');

			resetBtnEl.trigger('click');
			expect(wrapper.vm.keyword).toBe('');
		});
	});

	describe('search box with criteria', () => {
		it('should show reset button if keyword exist', async () => {
			createComponent();

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.keyword).toBe('Tebet');
		});
	});

	describe('tabs controller', () => {
		it('should change activeTab when click on tab title', async () => {
			createComponent();
			await wrapper.setData({
				keywordActive: false,
				isLoadingPopular: false
			});

			const searchTabEl = wrapper.find('.search-tabs');
			expect(searchTabEl.exists()).toBe(true);

			searchTabEl.trigger('click');

			expect(wrapper.vm.activeTab).toBe('area');
		});
	});

	describe('suggestion controller', () => {
		it('should change activeTab when click on tab title', async () => {
			createComponent();
			await wrapper.setData({
				keywordActive: false,
				isLoadingPopular: false
			});

			const popularPrimaryEl = wrapper.find('.popular-primary');
			expect(popularPrimaryEl.exists()).toBe(true);

			popularPrimaryEl.trigger('click');

			expect(wrapper.vm.searchCriteria.keywords).toBe('keywords');
		});

		it('should navigate to suggested kost', async () => {
			createComponent();
			await wrapper.setData({
				keywordActive: true,
				keywordFound: true,
				isLoadingKeyword: false,
				isLoadingPopular: false,
				keywordSuggestion: {
					areas: [],
					landings: [],
					rooms: []
				}
			});
			await wrapper.vm.$nextTick();

			const searchSuggestionEl = wrapper.findAll('.search-suggestion');
			expect(searchSuggestionEl.exists()).toBe(true);

			searchSuggestionEl.trigger('click');
			await wrapper.vm.$nextTick();
			expect(window.location.href).toBe('/room/test');
		});
	});
});
