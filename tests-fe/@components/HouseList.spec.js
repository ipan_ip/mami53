import { createLocalVue, shallowMount } from '@vue/test-utils';
import HouseList from 'Js/@components/HouseList.vue';
import house from './__mocks__/house.json';
import mockVLazy from '../utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('HouseList.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const propsData = {
		houses: [house],
		grid: 'col-xs-12 col-sm-6 col-md-4',
		filterTime: 0
	};
	global.open = jest.fn();

	let wrapper;
	beforeEach(() => {
		mockWindowProperty(
			'matchMedia',
			jest.fn(() => {
				return { matches: true };
			})
		);
		wrapper = shallowMount(HouseList, { localVue, propsData });
	});

	it('should render all houses', () => {
		expect(wrapper.findAll('.row > div').length).toBe(1);
	});

	// it('should add tracking when houses change', done => {
	// 	const newHouse = { ...house, id: 971973 };
	// 	const moengageEventTrack = jest.fn();
	// 	wrapper.setProps({
	// 		houses: [house, newHouse]
	// 	});
	// 	wrapper.setMethods({
	// 		moengageEventTrack
	// 	});
	// 	wrapper.vm.$nextTick(() => {
	// 		wrapper.vm.$nextTick(() => {
	// 			expect(moengageEventTrack).toBeCalled();
	// 			done();
	// 		});
	// 	});
	// });

	it('should send correct available room property', () => {
		global.tracker = jest.fn();
		wrapper.vm.moengageEventTrack([house]);
		expect(global.tracker).toBeCalled();
		wrapper.vm.moengageEventTrack([{ ...house, is_available: false }]);
		expect(global.tracker).toBeCalled();
	});

	it('should open link when list-container clicked', () => {
		mockWindowProperty('open', open);
		mockWindowProperty('location', { href: house.share_url });
		wrapper.find('.list-container').trigger('click');
		expect(global.open).not.toBeCalled();
		mockWindowProperty('matchMedia', null);
		wrapper.find('.list-container').trigger('click');
		expect(global.open).toBeCalled();
	});
});
