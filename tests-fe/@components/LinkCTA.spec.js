import { createLocalVue, shallowMount } from '@vue/test-utils';
import LinkCTA from 'Js/@components/LinkCTA.vue';

describe('LinkCTA.vue', () => {
	const localVue = createLocalVue();
	let clickCount = 0;
	const onClickFunction = function() {
		clickCount += 1;
	};
	const wrapper = shallowMount(LinkCTA, {
		localVue,
		propsData: {
			goTo: 'http://mamikos.com',
			onClick: onClickFunction,
			gtmTrackingClass: 'link-cta-class',
			text: 'link cta text'
		}
	});
	Object.defineProperty(window, 'location', {
		value: {
			href: ''
		}
	});

	it('should render correctly', () => {
		const linkCta = wrapper.find('.link-cta');
		expect(linkCta.exists()).toBe(true);

		const linkCtaAnchor = linkCta.find('a');
		if (linkCtaAnchor.exists()) {
			expect(linkCta.find('a').text()).toBe('link cta text');
			expect(linkCta.find('a').classes()).toContain('link-cta-class');
		}
	});

	it('should trigger the click function correctly', () => {
		const linkCta = wrapper.find('.link-cta');
		const linkCtaAnchor = linkCta.find('a');
		if (linkCtaAnchor.exists()) {
			const previousClickCount = clickCount;
			linkCtaAnchor.trigger('click');
			expect(window.location.href).toBe('http://mamikos.com');
			expect(clickCount).toBe(previousClickCount + 1);
		}
	});
});
