import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterTime from 'Js/@components/FilterTime.vue';

describe('FilterTime.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		label: true,
		durationVal: 0
	};
	const wrapper = shallowMount(FilterTime, { localVue, propsData });

	it('should render filter-time class', () => {
		expect(wrapper.find('.filter-time').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('label').exists()).toBe(true);
	});

	it('should emit setFilter when select option changed', () => {
		const selectedRentType = 2;
		wrapper.setData({ selectedRentType });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().setFilter[0]).toEqual([selectedRentType]);
		});
	});
});
