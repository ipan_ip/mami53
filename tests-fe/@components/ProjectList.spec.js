import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ProjectList from 'Js/@components/ProjectList.vue';
import project from './__mocks__/project';

describe('ProjectList.vue', () => {
	const propsData = {
		projects: [project]
	};
	const localVue = createLocalVue();
	let wrapper;
	beforeEach(() => {
		mockWindowProperty(
			'matchMedia',
			jest.fn(() => {
				return { matches: true };
			})
		);
		wrapper = shallowMount(ProjectList, { localVue, propsData });
	});

	it('should render projects props', () => {
		expect(wrapper.findAll('.project-container').length).toBe(
			propsData.projects.length
		);
		expect(wrapper.find('.project-zero').exists()).toBe(false);
		wrapper.setProps({ projects: [] });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.project-container').exists()).toBe(false);
			expect(wrapper.find('.project-zero').exists()).toBe(true);
		});
	});

	it('should get unit type when project-type mouseovered', () => {
		wrapper.find('.project-type-custom-width').trigger('mouseover');
		const unitTypeList = project.unit_types.reduce(
			(list, unit_type) => (list += unit_type + '<br>'),
			''
		);
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.unitTypeList).toBe('<b>' + unitTypeList + '</b>');
		});
	});

	it('should open detail when project clicked', () => {
		const open = jest.fn();
		mockWindowProperty('open', open);
		mockWindowProperty('location', { href: '/someslug' });
		wrapper.find('.project-list-container').trigger('click');
		expect(global.location.href).toBe(project.share_url);
		mockWindowProperty('matchMedia', null);
		wrapper.find('.project-list-container').trigger('click');
		expect(open).toBeCalled();
	});
});
