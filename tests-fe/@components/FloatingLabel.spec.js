import { createLocalVue, shallowMount } from '@vue/test-utils';
import FloatingLabel from 'Js/@components/FloatingLabel.vue';

describe('FloatingLabel.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(FloatingLabel, {
		localVue,
		propsData: {
			text: 'floating label',
			top: '10px',
			right: '5px',
			left: '5px',
			bottom: '10px'
		}
	});

	it('should render correctly', () => {
		const floatingLabel = wrapper.find('.floating-label');

		expect(floatingLabel.exists()).toBe(true);
		expect(floatingLabel.text()).toBe('floating label');

		expect(floatingLabel.element.style.top).toBe('10px');
		expect(floatingLabel.element.style.right).toBe('5px');
		expect(floatingLabel.element.style.left).toBe('5px');
		expect(floatingLabel.element.style.bottom).toBe('10px');
	});
});
