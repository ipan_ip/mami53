import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterKostSort from 'Js/@components/FilterKostSort.vue';

describe('FilterKostSort.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		label: true,
		sortVal: ['price', 'asc']
	};
	const wrapper = shallowMount(FilterKostSort, { localVue, propsData });

	it('should render filter-sort class', () => {
		expect(wrapper.find('.filter-sort').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('label').exists()).toBe(propsData.label);
		expect(wrapper.vm.sorting).toBe(propsData.sortVal);
	});

	it('should emit setFilter when select option changed', () => {
		const sorting = ['price', '-'];
		wrapper.setData({ sorting });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().setFilter[0]).toEqual([sorting]);
		});
	});
});
