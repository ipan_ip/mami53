import { createLocalVue, shallowMount } from '@vue/test-utils';
import IdVerifModalCancelAction from 'Js/@components/IdVerifModalCancelAction.vue';

describe('IdVerifModalCancelAction.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		showModal: true,
		labelModal: 'confirmation'
	};
	const wrapper = shallowMount(IdVerifModalCancelAction, {
		localVue,
		propsData
	});

	it('should render idVerifModalCancelAction', () => {
		expect(wrapper.find('#idVerifModalCancelAction').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('.confirmation-text').text()).toBe(
			propsData.labelModal
		);
	});

	it("should emit close when 'no' button clicked", () => {
		wrapper.find('.confirmation-button-group .btn-default').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});

	it("should emit ok when 'yes' button clicked", () => {
		wrapper.find('.confirmation-button-group .btn-primary').trigger('click');
		expect(wrapper.emitted().ok).toBeTruthy();
	});
});
