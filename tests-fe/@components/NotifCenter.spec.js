import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import NotifCenter from 'Js/@components/NotifCenter.vue';
import notifCenterData from './__mocks__/notif-center.json';
import 'tests-fe/utils/mock-vue';
jest.mock('vue-popperjs/dist/vue-popper.css', () => {});
const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin([MixinNavigatorIsMobile]);

describe('NotifCenter.vue', () => {
	const localVue = createLocalVue();
	global.axios = mockAxios;
	global.authCheck = {};
	let wrapper;
	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	describe('Rejected Promise', () => {
		beforeEach(() => {
			axios.mockReject('error');
			wrapper = shallowMount(NotifCenter, { localVue });
			wrapper.vm.getNotificationData();
		});

		it('should call bugsnagClient notify when fetch data', () => {
			expect(notify).toBeCalled();
		});
	});

	describe('Promise return with no status data', () => {
		beforeEach(() => {
			axios.mockResolve({
				data: { data: notifCenterData.notifCategory.notifCategoryData }
			});
			wrapper = shallowMount(NotifCenter, { localVue });
			wrapper.vm.getNotificationData();
		});

		it('should set isEmpty to true', () => {
			expect(wrapper.vm.notifContentOptions.isEmpty).toBe(true);
		});
	});

	describe('Mobile or Small Screen', () => {
		beforeEach(() => {
			axios.mockResolve({
				data: {
					status: 200,
					data: notifCenterData.notifCategory.notifCategoryData
				}
			});
			wrapper = shallowMount(NotifCenter, { localVue });
			wrapper.setData({ ...notifCenterData });
		});

		mockWindowProperty('innerWidth', 700);
		mockWindowProperty('tracker', jest.fn());

		it('should render component depends on device', () => {
			expect(wrapper.find('.nav-notification').exists()).toBe(true);
		});

		it('should toggle mobile notif when bell button clicked', () => {
			const isShowNotifMobile = wrapper.vm.isShowNotifMobile;
			wrapper.vm.toggleNotifMobile();
			expect(wrapper.vm.isShowNotifMobile !== isShowNotifMobile).toBe(true);
		});
	});

	describe('Desktop', () => {
		mockWindowProperty('innerWidth', 1000);

		it('should render popper', () => {
			expect(wrapper.find('.popper').exists()).toBe(true);
		});

		it('should select menu when notif-center-menu selected', () => {
			const data = wrapper.vm.notifCategory.notifCategoryData[0];
			wrapper.vm.selectMenu(data);
			expect(wrapper.vm.notifContentOptions.categoryId).toBe(data.id);
			expect(wrapper.vm.notifContentOptions.categoryLabel).toBe(data.name);
		});

		it('should load more data when it is not loading and has next data', () => {
			const getNotificationData = jest.fn();
			wrapper.setData({
				notifContentOptions: notifCenterData.notifContentOptions,
				isLoading: false
			});
			wrapper.setMethods({ getNotificationData });
			jest.clearAllMocks();
			wrapper.vm.loadMore();
			expect(getNotificationData).toHaveBeenCalledTimes(1);
			wrapper.setData({ isLoading: true });
			jest.clearAllMocks();
			wrapper.vm.loadMore();
			expect(getNotificationData).not.toBeCalled();
		});
	});
});
