import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import TitleNavbar from 'Js/@components/TitleNavbar.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('TitleNavbar.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(TitleNavbar, {
		localVue,
		propsData: {
			title: 'title'
		}
	});
	let historyCount = 0;
	mockWindowProperty('history', {
		go: function(skipHistoryNumber) {
			historyCount = historyCount + parseInt(skipHistoryNumber);
		}
	});

	it('should render correctly', () => {
		const mobileNavbar = wrapper.find('.mobile-navbar');
		expect(mobileNavbar.exists()).toBe(true);

		const mobileNavbarTitle = mobileNavbar.find('h3');
		expect(mobileNavbarTitle.exists()).toBe(true);

		const mobileNavbarButton = mobileNavbar.find('button');
		expect(mobileNavbarButton.exists()).toBe(true);
	});

	it('should emit buttonClick event when icon is clicked', async () => {
		const mobileNavbar = wrapper.find('.mobile-navbar');
		const mobileNavbarButton = mobileNavbar.find('button');

		mobileNavbarButton.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('buttonClick')).toBeTruthy();
	});
});
