import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterMobileButton from 'Js/@components/FilterMobileButton.vue';

describe('FilterMobileButton.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(FilterMobileButton, { localVue });

	it('should render filter mobile button', () => {
		expect(wrapper.find('nav').exists()).toBe(true);
	});

	it('should emit open when button clicked', () => {
		wrapper.find('nav button').trigger('click');
		expect(wrapper.emitted().open).toBeTruthy();
	});
});
