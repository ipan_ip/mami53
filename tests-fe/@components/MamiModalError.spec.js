import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MamiModalError from 'Js/@components/MamiModalError.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('MamiModalError.vue', () => {
	const reload = jest.fn();
	mockWindowProperty('location.reload', reload);

	const wrapper = shallowMount(MamiModalError, {
		localVue,
		propsData: { showModal: true }
	});

	it('should render the component', () => {
		expect(wrapper.find('#mamiModalGeneralError').exists()).toBe(true);
	});

	it('should emit close event', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should reload the page', () => {
		wrapper.vm.reloadPage();
		expect(reload).toBeCalled();
	});
});
