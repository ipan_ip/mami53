import { createLocalVue, shallowMount } from '@vue/test-utils';
import IdVerifModalShowPhoto from 'Js/@components/IdVerifModalShowPhoto.vue';
import Vuex from 'vuex';

describe('IdVerifModalShowPhoto.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const propsData = {
		showModal: true,
		imageType: 'card'
	};
	const store = {
		state: {
			extras: {
				selfieImg: {
					src: 'img/selfieImg.jpg'
				},
				idImg: {
					src: 'img/idImg.jpg'
				}
			}
		}
	};
	const stubs = {
		FilterCloseButton: {
			template: `<div @click="$emit('press')"></div>`
		}
	};
	const wrapper = shallowMount(IdVerifModalShowPhoto, {
		localVue,
		propsData,
		stubs,
		store: new Vuex.Store(store)
	});

	const buttonActions = wrapper.findAll('.button-actions > button');

	it('should render idVerifModalShowPhoto', () => {
		expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(
			wrapper.find('.booking-show-photo-container img').attributes('src')
		).toBe(store.state.extras.idImg.src);

		wrapper.setProps({ imageType: 'selfie' });
		wrapper.vm.$nextTick(() => {
			expect(
				wrapper.find('.booking-show-photo-container img').attributes('src')
			).toBe(store.state.extras.selfieImg.src);
		});
	});

	it('should emit close when close button clicked', () => {
		wrapper.find('.photo-esc > div').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
