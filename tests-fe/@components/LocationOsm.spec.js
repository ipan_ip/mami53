import { createLocalVue, shallowMount } from '@vue/test-utils';
import LocationOsm from 'Js/@components/LocationOsm.vue';
jest.mock('leaflet/dist/leaflet.css', () => {});
jest.mock('leaflet', () => {
	return {
		map: jest.fn(() => {
			return {
				attributionControl: {
					addAttribution: jest.fn()
				}
			};
		}),
		tileLayer: jest.fn(() => {
			return {
				addTo: jest.fn()
			};
		}),
		circleMarker: jest.fn(() => {
			return {
				addTo: jest.fn()
			};
		})
	};
});

describe('LocationOsm.vue', () => {
	const localVue = createLocalVue();
	global.osmHost = {};
	const propsData = {
		grid: 'sm-12',
		center: {
			lat: '129837012',
			lng: '127280099'
		}
	};
	const wrapper = shallowMount(LocationOsm, { propsData, localVue });
	it('should render detailMap', () => {
		expect(wrapper.find('#detailMap').exists()).toBe(true);
	});
});
