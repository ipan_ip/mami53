import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterIsFlashSale from 'Js/@components/FilterIsFlashSale.vue';

const localVue = createLocalVue();

describe('FilterIsFlashSale.vue', () => {
	const propsData = {};

	const generateWrapper = ({ methodsProp = {}, mocksProp = {} } = {}) => {
		const shallowMountProp = {
			localVue,
			propsData,
			methods: { ...methodsProp },
			mocks: { ...mocksProp }
		};
		return shallowMount(FilterIsFlashSale, shallowMountProp);
	};

	it('should load the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.popper-content-container').exists()).toBe(true);
	});

	it('should emit the callback', () => {
		const wrapper = generateWrapper();
		wrapper.setProps({ needCallback: true });
		wrapper.vm.hidePopper();
		expect(wrapper.vm.isShow).toBe(false);
		expect(wrapper.emitted('callback')).toBeTruthy();
	});

	it('should emit track event', () => {
		const hidePopper = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { hidePopper } });
		wrapper.vm.hidePopperTrack();
		expect(wrapper.emitted('trackLearnMore')).toBeTruthy();
		expect(hidePopper).toBeCalled();
	});

	it('should emit toggle flash sale', () => {
		const hidePopper = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { hidePopper } });
		wrapper.vm.isShow = true;
		wrapper.vm.toggleFlashSale();
		expect(hidePopper).toBeCalled();
		expect(wrapper.emitted('toggleFlashsale')).toBeTruthy();
	});

	it('should show the popper', () => {
		const wrapper = generateWrapper();
		wrapper.vm.showPopper();
		expect(wrapper.vm.isShow).toBe(true);
	});
});
