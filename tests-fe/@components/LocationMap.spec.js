import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../utils/mock-vue';
import LocationMap from 'Js/@components/LocationMap.vue';
import { mockComponent, mockComponentWithAttrs } from '../utils/mock-component';
jest.mock('vue2-google-maps', () => {});

describe('LocationMap.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(LocationMap, {
		stubs: {
			VueGoogleMaps: mockComponent,
			GmapMap: mockComponentWithAttrs({ class: 'gmap-map' }),
			GmapMarker: mockComponent
		},
		localVue,
		propsData: {
			grid: 'sm-6',
			center: {}
		}
	});

	it('should render gmap', () => {
		expect(wrapper.find('.gmap-map').exists()).toBe(true);
	});
});
