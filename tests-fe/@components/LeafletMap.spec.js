import 'tests-fe/utils/mock-vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import LeafletMap from 'Js/@components/LeafletMap.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import * as storage from 'Js/@utils/storage';
import Vuex from 'vuex';

const MixinIsMobile = require('Js/_search/mixins/MixinIsMobile');

const mockMapEventListener = {};
let mockLeaflet = null;

jest.useFakeTimers();
jest.mock('lodash/debounce', () => jest.fn(fn => fn));
jest.mock('leaflet', () => {
	mockLeaflet = {
		map: jest.fn(() => ({
			attributionControl: {
				addAttribution: jest.fn()
			},
			scrollWheelZoom: {
				disable: jest.fn()
			},
			on: jest.fn((event, cb) => {
				mockMapEventListener[event] = cb;
				cb();
			}),
			dragging: {
				disable: jest.fn()
			},
			doubleClickZoom: {
				disable: jest.fn()
			},
			whenReady: jest.fn(),
			fitBounds: jest.fn(),
			getCenter: jest.fn(),
			getBounds: jest.fn(() => ({
				getNorthWest: jest.fn(() => {
					return { lng: 1, lat: 2 };
				}),
				getSouthEast: jest.fn(() => {
					return { lng: 3, lat: 4 };
				})
			})),
			eachLayer: jest.fn(layer => {
				layer({
					_url: undefined
				});
			}),
			removeLayer: jest.fn()
		})),
		Control: {
			Zoom: jest.fn(() => ({
				addTo: jest.fn()
			}))
		},
		Marker: jest.fn(() => {
			return {
				on: jest.fn((event, clickCallback) => {
					mockMapEventListener[event] = clickCallback;

					clickCallback({
						target: {
							options: {
								clusterData: {
									type: 'cluster',
									code: 'a23'
								}
							}
						}
					});
				}),
				openPopup: jest.fn(),
				closePopup: jest.fn()
			};
		}),
		DivIcon: jest.fn(),
		tileLayer: jest.fn(() => ({
			addTo: jest.fn()
		})),
		polygon: jest.fn(() => ({
			addTo: jest.fn().mockReturnValue({
				getBounds: jest.fn()
			})
		})),
		latLngBounds: jest.fn(() => ({
			getNorthWest: jest.fn(() => {
				return { lng: 5, lat: 6 };
			}),
			getSouthEast: jest.fn(() => {
				return { lng: 7, lat: 8 };
			})
		})),
		FeatureGroup: jest.fn(() => ({
			addTo: jest.fn(),
			addLayer: jest.fn()
		})),
		circleMarker: jest.fn().mockReturnValue({
			bindTooltip: jest.fn().mockReturnValue({
				openTooltip: jest.fn()
			}),
			bindPopup: jest.fn(),
			on: jest.fn()
		}),
		icon: jest.fn(),
		marker: jest.fn().mockReturnValue({
			setIcon: jest.fn(),
			icon: jest.fn()
		})
	};

	global.L = mockLeaflet;

	return mockLeaflet;
});

global.osmHost = 'https://osm.mamikos.com';
global.tracker = jest.fn();
const querySelector = jest.fn(() => {
	return {
		style: {
			width: 40,
			height: 40
		}
	};
});
const querySelectorAll = jest.fn(type => {
	if (type === '[cluster-code=cluster-a24]') {
		return [
			{
				style: {
					backgroundColor: '#000',
					color: '#000'
				}
			}
		];
	} else if (type === '[cluster-code=cluster-a23]') {
		return [
			{
				style: {
					backgroundColor: '#000',
					color: '#000',
					opacity: 1
				}
			}
		];
	}
});

global.document.querySelector = querySelector;
global.document.querySelectorAll = querySelectorAll;

mockWindowProperty('location', { href: '' });
mockWindowProperty('open', jest.fn());

const localVue = createLocalVue();
mockVLazy(localVue);

localVue.mixin(MixinIsMobile);
localVue.use(Vuex);

const stubs = {
	BgIcon: mockComponent,
	BgButton: mockComponent,
	BgAlert: mockComponent,
	BgButtonIcon: mockComponent,
	BgGrid: mockComponent,
	BgGridItem: mockComponent
};

const propsData = {
	propId: 'mamiMap',
	propClass: 'map-container',
	type: 'kos',
	center: {
		lat: -6.13694,
		lng: 106.862
	},
	minZoom: 14,
	maxZoom: 16,
	zoom: 14,
	scrollZoom: true,
	isShowNominatim: true
};

const store = new Vuex.Store({
	mutations: { setMapCenter: jest.fn() }
});

const setPolygonSpy = jest.spyOn(LeafletMap.methods, 'setPolygon');
const applyLocSpy = jest.spyOn(LeafletMap.methods, 'applyLoc');
const handleSetMarkerEventSpy = jest.spyOn(
	LeafletMap.methods,
	'handleSetMarkerEvent'
);

const mockRooms = [
	{
		count: 1,
		type: 'room',
		room: {
			gender: 0,
			price_marker: '000 rb',
			room_available: 2,
			share_url: 'mock share url 0',
			_id: 0
		}
	},
	{
		count: 2,
		type: 'room',
		room: {
			gender: 1,
			price_marker: '100 rb',
			room_available: 2,
			share_url: 'mock share url 1',
			_id: 1
		}
	},
	{
		count: 3,
		type: 'room',
		room: {
			gender: 2,
			price_marker: '200 rb',
			room_available: 2,
			share_url: 'mock share url 2',
			_id: 2
		}
	},
	{
		count: 4,
		type: 'room',
		room: {
			gender: 100,
			price_marker: '10 jt',
			room_available: 2,
			share_url: 'mock share url 100',
			_id: 100
		}
	}
];

const createWrapper = (isMobile = false, updatedProps = {}) => {
	if (isMobile) {
		mockWindowProperty('window.innerWidth', 600);
	} else {
		mockWindowProperty('window.innerWidth', 1380);
	}

	return shallowMount(LeafletMap, {
		localVue,
		stubs,
		propsData: {
			...propsData,
			...updatedProps
		},
		store
	});
};

describe('LeafletMap.vue Desktop', () => {
	const wrapper = createWrapper();
	wrapper.setData({ isWindowHideOnNominatim: true });

	it('should render mamiMap', () => {
		expect(wrapper.find('#mamiMap').exists()).toBe(true);
	});

	it('Should return emptyStateText properly when filter is off', () => {
		expect(wrapper.vm.emptyStateText).toEqual(
			'Coba geser peta atau ganti kata kunci pencarian.'
		);
	});

	it('Should return emptyStateText properly when filter is on', () => {
		wrapper.setProps({
			isFilterOn: true
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.emptyStateText).toEqual(
				'Coba ubah kata kunci, ubah filter, atau geser peta untuk memperbanyak hasil pencarian.'
			);
		});
	});

	it('Watch isMapLoading to update related data correctly', () => {
		wrapper.setProps({
			isMapLoading: true
		});

		expect(wrapper.vm.isShowSearchButton).toBeTruthy();
		expect(wrapper.vm.isMapLoading).toBeTruthy();
	});

	it('should close alert when handleCloseAlert called', () => {
		wrapper.vm.handleCloseAlert();

		expect(wrapper.vm.isAlertOpen).toBeFalsy();
	});

	it('should emit some value when handleSearchArea called', () => {
		wrapper.vm.handleSearchArea();

		expect(wrapper.emitted()['set-geocode'][0]).toEqual([null]);
		expect(wrapper.emitted()['set-criteria'][0]).toEqual([
			{
				tag: 'Area sesuai peta'
			}
		]);
		expect(wrapper.vm.isUnbindBoundaries).toBeTruthy();

		expect(wrapper.emitted()['handle-set-cluster'][0]).toEqual([false]);
		expect(applyLocSpy).toHaveBeenCalled();
	});

	it('should emit handle-set-cluster when handleZoomArea called', () => {
		wrapper.vm.handleZoomArea();

		expect(wrapper.emitted()['handle-set-cluster'][0]).toEqual([false]);
		expect(wrapper.vm.isShowSearchButton).toBeTruthy();
		expect(applyLocSpy).toHaveBeenCalled();
	});

	it('should show button toggle when handleShowButtonToggle called', () => {
		wrapper.vm.handleShowButtonToggle();

		expect(wrapper.vm.isShowSearchButton).toBeTruthy();
	});

	it('should show legend button properly when related method called', () => {
		expect(wrapper.vm.actionIcon).toBe('information-round');

		wrapper.vm.openLegend();

		expect(wrapper.vm.isLegendModalShown).toBeTruthy();
	});

	it('should clode modal legend when anywheren on map was clicked', () => {
		wrapper.vm.handleMapClicked();

		expect(wrapper.vm.isLegendModalShown).toBeFalsy();
		expect(wrapper.vm.actionIcon).toBe('information-round');
	});

	it('should handle zoom area when call setPolygon method', () => {
		wrapper.setData({
			isFirstTimeRendered: false
		});
		wrapper.vm.setPolygon();

		jest.runAllTimers();
		expect(wrapper.vm.isFirstTimeRendered).toBeTruthy();
	});

	it('should handle applyLoc properly ', () => {
		const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		wrapper.vm.applyLoc();

		expect(wrapper.vm.bounds).toHaveProperty('getNorthWest');
		expect(wrapper.vm.bounds).toHaveProperty('getSouthEast');
		expect(spyCommit).toBeCalledWith('setMapCenter', global.L.map.getCenter);
		expect(wrapper.vm.locations).toEqual([
			[1, 4],
			[3, 2]
		]);
		expect(wrapper.emitted('bounds-changed')).toBeTruthy();
	});

	it('should call onMapReady when not first time rendered and have no boundaries', () => {
		wrapper.vm.onMapReady();

		expect(wrapper.vm.bounds).toHaveProperty('getNorthWest');
		expect(wrapper.vm.bounds).toHaveProperty('getSouthEast');
		expect(applyLocSpy).toHaveBeenCalled();
	});

	it('should call onMapReady when first time rendered', () => {
		wrapper.setData({
			isFirstTimeRendered: false
		});
		wrapper.setProps({
			boundaries: [1, 2, 3]
		});
		wrapper.vm.onMapReady();

		expect(wrapper.vm.bounds).toHaveProperty('getNorthWest');
		expect(wrapper.vm.bounds).toHaveProperty('getSouthEast');
		expect(wrapper.vm.locations).toEqual([
			[5, 8],
			[7, 6]
		]);
	});

	it('should return price with fixed number less than 2', () => {
		const price = '1.12 jt';
		const resultOfPrice = wrapper.vm.priceFormatter(price);

		expect(resultOfPrice).toBe('Rp1,12jt');
	});

	it('should return price with fixed number more than 2', () => {
		const price = '1.12999999999 jt';
		const resultOfPrice = wrapper.vm.priceFormatter(price);

		expect(resultOfPrice).toBe('Rp1,13jt');
	});

	it('should call window open when the condition is false', () => {
		const slug = '/room/test';
		wrapper.vm.openDetail(slug);

		expect(window.open).toBeCalledWith('/room/test', '_blank');
	});

	it('should change the current url when the condition is true', () => {
		const slug = '/room/test';
		wrapper.vm.openDetail(slug);

		mockWindowProperty(
			'matchMedia',
			jest.fn(() => {
				return { matches: true };
			})
		);
		wrapper.vm.openDetail(slug);

		expect(window.location.href).toBe('/room/test');
	});

	it('should send cluster info when addTrackEventClickCluster method called', () => {
		const clusterInfo = { count: 1, price: 'Rp1,5jt', type: 'room' };

		wrapper.vm.addTrackEventClickCluster(clusterInfo);
		expect(global.tracker).toBeCalled();
	});

	it('Should call checkShowCondition method on created', async () => {
		storage.local.setItem('nominatim-visited', null);
		wrapper.setProps({
			isUsingLegend: true
		});

		const checkShowConditionSpy = jest.spyOn(wrapper.vm, 'checkShowCondition');
		const storageLocalGetItemSpy = jest.spyOn(storage.local, 'getItem');
		const storageLocalSetItemSpy = jest.spyOn(storage.local, 'setItem');

		await wrapper.vm.checkShowCondition();
		expect(checkShowConditionSpy).toHaveBeenCalled();
		expect(storageLocalGetItemSpy).toHaveBeenCalled();
		expect(storageLocalSetItemSpy).toBeCalled();
		checkShowConditionSpy.mockRestore();
	});

	it('set centre icon when showClusters method called', () => {
		wrapper.setProps({
			showCenterMarker: true,
			isShowNominatim: false,
			boundaries: [{ a: 1 }]
		});
		const clusters = [];
		wrapper.setData({ lastMarkerCode: 'a24' });

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.icon).toBeCalledWith({
				iconUrl: '/assets/icons/map/poin_pencarian.svg',
				iconSize: [40, 40],
				iconAnchor: [20, 40],
				className: 'marker-unclickable'
			});
			expect(global.L.marker().setIcon).toBeCalled();
			expect(global.L.FeatureGroup).toBeCalled();
		});
	});

	it('should not call bindpopup when clusters including kos only and call setPolygon when is not nominatim', () => {
		wrapper.setProps({
			showCenterMarker: true,
			boundaries: [{ a: 1 }]
		});
		let clusters = [
			{
				count: 2,
				latitude: '-1',
				longitude: '10',
				radius: 5,
				type: 'cluster'
			}
		];
		clusters = clusters.concat(mockRooms);

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.Marker).toBeCalled();
			expect(global.L.circleMarker().bindPopup).not.toBeCalled();
		});
	});

	it('should bindpopup and change window to hide when is nominatim and project type', () => {
		wrapper.setProps({
			type: 'project',
			boundaries: [{ a: 1 }]
		});
		const clusters = [
			{
				count: 20000,
				latitude: '-1',
				longitude: '10',
				radius: 5,
				type: 'cluster'
			}
		];

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.circleMarker().bindPopup).toBeCalled();
			expect(wrapper.vm.isWindowHideOnNominatim).toBeTruthy();
		});
	});

	it('should bindpopup and change window to hide when is nominatim and apartment/unit type', () => {
		wrapper.setProps({
			type: 'unit'
		});
		const clusters = mockRooms;

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.circleMarker().bindPopup).toBeCalled();
			expect(wrapper.vm.isWindowHideOnNominatim).toBeTruthy();
		});
	});

	it('should bindpopup and change window to hide when is nominatim and house type', () => {
		wrapper.setProps({
			type: 'house'
		});
		const clusters = [
			{
				count: 1,
				type: 'house',
				house: {
					price_marker: '100 jt',
					share_url: 'mock share url 1',
					_id: 1
				}
			},
			{
				count: 2,
				type: 'house',
				house: {
					price_marker: '200 jt',
					share_url: 'mock share url 2',
					_id: 2
				}
			}
		];

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.circleMarker().bindPopup).toBeCalled();
			expect(wrapper.vm.isWindowHideOnNominatim).toBeTruthy();
		});
	});

	it('should bindpopup and change window to hide when is nominatim and vacancy type', () => {
		wrapper.setProps({
			type: 'vacancy'
		});
		const clusters = [
			{
				count: 1,
				type: 'vacancy',
				vacancy: {
					type: 'FULL-TIME',
					_id: 1
				}
			},
			{
				count: 2,
				type: 'vacancy',
				vacancy: {
					type: 'PART-TIME',
					_id: 1
				}
			},
			{
				count: 3,
				type: 'vacancy',
				vacancy: {
					type: 'FREELANCE',
					_id: 1
				}
			},
			{
				count: 4,
				type: 'vacancy',
				vacancy: {
					type: 'MAGANG',
					_id: 1
				}
			}
		];

		wrapper.vm.showClusters(clusters);
		wrapper.vm.$nextTick(() => {
			expect(global.L.circleMarker().bindPopup).toBeCalled();
			expect(wrapper.vm.isWindowHideOnNominatim).toBeTruthy();
		});
	});

	it('should handle map event when check visibility is called', async () => {
		const eventSpy = jest.fn();

		wrapper.setData({
			map: {
				...wrapper.vm.map,
				dragging: {
					enable: eventSpy,
					disable: eventSpy
				},
				doubleClickZoom: {
					enable: eventSpy,
					disable: eventSpy
				}
			},
			isWindowHideOnNominatim: false
		});

		wrapper.vm.checkVisibility();

		expect(handleSetMarkerEventSpy).toBeCalled();
		expect(eventSpy).toBeCalledTimes(2);

		wrapper.setData({
			map: {
				...wrapper.vm.map,
				dragging: null,
				doubleClickZoom: null
			}
		});

		wrapper.vm.checkVisibility();

		expect(handleSetMarkerEventSpy).toBeCalled();
		expect(eventSpy).toBeCalledTimes(2);
	});

	it('should show legend button properly when related method called with window hide is false', () => {
		wrapper.setData({
			isWindowHideOnNominatim: true
		});

		wrapper.vm.checkVisibility();
		jest.advanceTimersByTime(1000);

		expect(setPolygonSpy).toBeCalled();
		expect(wrapper.vm.isWindowHideOnNominatim).toBeFalsy();
	});

	it('Should watch isMapDisabled correctly with dragging and doubleClickZoom function available to set the marker event', async () => {
		const eventSpy = jest.fn();

		wrapper.setProps({ isMapDisabled: true });
		wrapper.setData({
			map: {
				...wrapper.vm.map,
				dragging: {
					enable: eventSpy,
					disable: eventSpy
				},
				doubleClickZoom: {
					enable: eventSpy,
					disable: eventSpy
				}
			},
			isWindowHideOnNominatim: false
		});

		await wrapper.vm.$nextTick();

		expect(handleSetMarkerEventSpy).toBeCalled();
		expect(eventSpy).toBeCalledTimes(2);
	});

	it('Should watch isMapDisabled correctly without dragging and doubleClickZoom function to set the marker event', async () => {
		wrapper.setData({
			map: {
				...wrapper.vm.map,
				dragging: null,
				doubleClickZoom: null
			}
		});
		wrapper.setProps({ isMapDisabled: true });

		await wrapper.vm.$nextTick();

		expect(handleSetMarkerEventSpy).toBeCalled();
	});

	it('Should add and remove listener on destroy', () => {
		mockWindowProperty('document', {
			removeEventListener: jest.fn()
		});

		wrapper.destroy();

		expect(document.removeEventListener).toBeCalledWith(
			'visibilitychange',
			expect.any(Function)
		);
	});
});

describe('LeafletMap.vue Mobile', () => {
	const wrapper = createWrapper(true, { scrollZoom: false });

	it('should show legend button properly when related method called', () => {
		expect(wrapper.vm.actionIcon).toBe('information-round');

		wrapper.vm.openLegend();

		expect(wrapper.vm.isLegendModalMobileShown).toBeTruthy();
	});
});
