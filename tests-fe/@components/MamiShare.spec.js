import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiShare from 'Js/@components/MamiShare.vue';
import mockComponent from 'tests-fe/utils/mock-component';

describe('MamiShare.vue', () => {
	const localVue = createLocalVue();
	window.Vue = localVue;
	const share = jest.fn();
	let wrapper = shallowMount(MamiShare, {
		stubs: { network: mockComponent },
		localVue
	});

	it('should render mamiShareContainer', () => {
		expect(wrapper.find('#mamiShareContainer').exists()).toBe(true);
		expect(wrapper.find('#mamiShareContainer > button').exists()).toBe(false);
	});

	it('should set supportNativeShare to true', () => {
		window.navigator.share = share;
		wrapper = shallowMount(MamiShare, {
			stubs: { network: mockComponent },
			localVue
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isSupportNativeShare).toBe(true);
			expect(wrapper.find('#mamiShareContainer > button').exists()).toBe(true);
		});
	});

	it('should call shareThisPage when button share clicked', () => {
		wrapper.find('#mamiShareContainer > button').trigger('click');
		expect(window.navigator.share).toBeCalled();
		expect(wrapper.emitted().track).toBeTruthy();
	});
});
