import { shallowMount, createLocalVue } from '@vue/test-utils';
import NavbarOwnerBottom from 'Js/@components/NavbarOwnerBottom';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
window.oxWebUrl = 'owner.mamikos.com';

describe('NavbarOwnerBottom.vue', () => {
	const click = jest.fn();
	const $route = { name: 'settings' };
	const $router = { push: jest.fn() };
	const mockStore = {
		state: {
			authData: { all: true }
		}
	};
	const querySelectorActive = jest.fn(() => {
		return {
			click,
			classList: {
				contains: jest.fn(() => {
					return true;
				})
			}
		};
	});
	mockWindowProperty('document.querySelector', querySelectorActive);

	const generateWrapper = ({ methodsProp = {} } = {}) => {
		const shallowMountProp = {
			localVue,
			store: new Vuex.Store(mockStore),
			mocks: { $route, $router },
			methods: { ...methodsProp }
		};
		return shallowMount(NavbarOwnerBottom, shallowMountProp);
	};

	it('should load the component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.navbar-owner-bottom').exists()).toBe(true);
	});

	it('should change page to profile page', () => {
		const menuSelected = {
			label: 'Akun',
			icon: 'profile',
			showBadge: false,
			badgeNumber: 0,
			route: `/settings`
		};
		$route.name = 'default';
		const wrapper = generateWrapper();
		wrapper.vm.changeMenu(menuSelected);
		expect(wrapper.vm.activeMenu).toBe('profile');
		expect(wrapper.vm.$router.push).toBeCalledWith('/settings');
	});

	it('should open chat room', () => {
		const menuSelected = {
			label: 'Chat',
			icon: 'chat',
			showBadge: false,
			badgeNumber: 0,
			route: null
		};
		const handleOpenChat = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { handleOpenChat } });
		wrapper.vm.changeMenu(menuSelected);
		expect(handleOpenChat).toBeCalled();
	});

	it('should set default active menu to kos-management', () => {
		$route.name = 'kos';
		const wrapper = generateWrapper();
		expect(wrapper.vm.activeMenu).toBe('kos-management');
	});

	it('should update chat total message', () => {
		const wrapper = generateWrapper();
		wrapper.vm.updateTotalMessages(10);
		expect(wrapper.vm.menus[1].badgeNumber).toBe(10);
	});

	it('should update chat unread count', () => {
		const sb = {
			getTotalUnreadChannelCount: jest.fn(fn => fn(50, null))
		};
		mockWindowProperty('sb', sb);
		const wrapper = generateWrapper();
		wrapper.vm.getChatUnreadCount();
		expect(wrapper.vm.unreadChatCount).toBe(50);
	});

	it('should open chat room', () => {
		const wrapper = generateWrapper();
		wrapper.vm.handleOpenChat();
		expect(document.querySelector('#sb_widget .widget').click).toBeCalled();
	});
});
