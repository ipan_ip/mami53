import { createLocalVue, shallowMount } from '@vue/test-utils';
import ButtonCTA from 'Js/@components/ButtonCTA.vue';

describe('ButtonCTA.vue', () => {
	const localVue = createLocalVue();
	let clickCount = 0;
	const onClickFunction = function() {
		clickCount += 1;
	};
	const wrapper = shallowMount(ButtonCTA, {
		localVue,
		propsData: {
			goTo: 'http://mamikos.com',
			onClick: onClickFunction,
			gtmTrackingClass: 'link-cta-class',
			text: 'link cta text',
			width: '100px',
			height: '1.5'
		}
	});
	Object.defineProperty(window, 'location', {
		value: {
			href: ''
		}
	});

	it('should render correctly', () => {
		const ButtonCTA = wrapper.find('.button-cta');
		expect(ButtonCTA.exists()).toBe(true);
		expect(ButtonCTA.element.style.width).toBe('100px');
		expect(ButtonCTA.element.style.lineHeight).toBe('1.5');

		const ButtonCtaAnchor = ButtonCTA.find('a');
		if (ButtonCtaAnchor.exists()) {
			expect(ButtonCtaAnchor.text()).toBe('link cta text');
			expect(ButtonCtaAnchor.classes()).toContain('link-cta-class');
		}
	});

	it('should trigger the click function correctly', () => {
		const ButtonCTA = wrapper.find('.button-cta');
		const ButtonCtaAnchor = ButtonCTA.find('a');

		if (ButtonCtaAnchor.exists()) {
			const previousClickCount = clickCount;
			ButtonCtaAnchor.trigger('click');
			expect(window.location.href).toBe('http://mamikos.com');
			expect(clickCount).toBe(previousClickCount + 1);
		}
	});
});
