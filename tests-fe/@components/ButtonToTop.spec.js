import { createLocalVue, shallowMount } from '@vue/test-utils';
import ButtonToTop from 'Js/@components/ButtonToTop.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('ButtonToTop.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(ButtonToTop, {
		localVue
	});
	const scrollToTopBtn = wrapper.find('#scrollToTop');

	it('should render scrollToTop', () => {
		expect(scrollToTopBtn.exists()).toBe(true);
	});

	it('should emit to-top when clicked', () => {
		scrollToTopBtn.trigger('click');
		expect(wrapper.emitted()['to-top']).toBeTruthy();
	});

	it('should update isTopButton value when window scrolled', () => {
		mockWindowProperty('pageYOffset', 200);
		global.window.onscroll();
		expect(wrapper.vm.isTopButton).toBe(true);
		mockWindowProperty('pageYOffset', 0);
		global.window.onscroll();
		expect(wrapper.vm.isTopButton).toBe(false);
	});
});
