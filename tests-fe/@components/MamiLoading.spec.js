import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiLoading from 'Js/@components/MamiLoading.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

window.Vue = require('vue');

describe('MamiLoading.vue', () => {
	const jQuery = {
		on: jest.fn((_, cb) => cb()),
		modal: jest.fn()
	};
	mockWindowProperty(
		'$',
		jest.fn(() => {
			return jQuery;
		})
	);
	const localVue = createLocalVue();
	const data = () => {
		return { isClosed: false };
	};
	const wrapper = shallowMount(MamiLoading, { localVue, data });

	it('should render mamiLoading', () => {
		expect(wrapper.find('#mamiLoading').exists()).toBe(true);
	});

	it('should emit closed/opened when props isClosed is changed', () => {
		wrapper.setData({ isClosed: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('closed')[0][0]).toBe(true);
			wrapper.setData({ isClosed: false });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('opened')[0][0]).toBe(false);
			});
		});
	});
	it('should show modal with backdrop when openModal Called', () => {
		wrapper.vm.openModal();
		expect(jQuery.modal).toBeCalledWith({
			backdrop: 'static',
			keyboard: false
		});
	});
	it('should hide modal with backdrop when closeModal Called', () => {
		wrapper.vm.closeModal();
		expect(jQuery.modal).toBeCalledWith('hide');
	});
});
