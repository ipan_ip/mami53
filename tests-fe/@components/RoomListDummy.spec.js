import { createLocalVue, shallowMount } from '@vue/test-utils';
import RoomListDummy from 'Js/@components/RoomListDummy.vue';

describe('RoomListDummy.vue', () => {
	const localVue = createLocalVue();
	const open = jest.fn();
	global.open = open;
	const wrapper = shallowMount(RoomListDummy, { localVue, window: { open } });

	it('should render dummy room list', () => {
		expect(wrapper.findAll('.kost-list-container').length).toBe(
			wrapper.vm.roomCount
		);
		expect(wrapper.find('.kost-review').exists()).toBe(true);
		wrapper.setData({ isKost: false });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.kost-review').exists()).toBe(false);
			expect(wrapper.find('.unit-icons').exists()).toBe(true);
		});
	});

	it('should open link on new tab', () => {
		const link = 'path.com/kost';
		wrapper.vm.openRoom(link);
		expect(open).toBeCalledWith(link, '_blank');
	});
});
