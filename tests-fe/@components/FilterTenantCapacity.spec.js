import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterTenantCapacity from 'Js/@components/FilterTenantCapacity.vue';

describe('FilterTenantCapacity.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		tenantCapacity: 1,
		capacityTotal: 10
	};
	const wrapper = shallowMount(FilterTenantCapacity, { localVue, propsData });

	it('should render filter-tenant-capacity class', () => {
		expect(wrapper.find('.filter-tenant-capacity').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('label').exists()).toBe(true);
		expect(wrapper.findAll('#filterTenantCapacity > option').length).toBe(
			propsData.capacityTotal + 1
		);
	});

	it('should emit setFilter when select option changed', () => {
		const capacityVal = 2;
		wrapper.setData({ capacityVal });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().setFilter[0]).toEqual([capacityVal]);
		});
	});
});
