import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterRoomTotal from 'Js/@components/FilterRoomTotal.vue';

describe('FilterRoomTotal.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		roomTotal: 1,
		optionTotal: 10
	};
	const wrapper = shallowMount(FilterRoomTotal, { localVue, propsData });

	it('should render filter-room-total class', () => {
		expect(wrapper.find('.filter-room-total').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('label').exists()).toBe(true);
		expect(wrapper.findAll('#filterRoomTotal > option').length).toBe(
			propsData.optionTotal + 1
		);
	});

	it('should emit setFilter when select option changed', () => {
		const roomTotalVal = 2;
		wrapper.setData({ roomTotalVal });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().setFilter[0]).toEqual([roomTotalVal]);
		});
	});
});
