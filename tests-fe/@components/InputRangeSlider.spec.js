import { mount } from '@vue/test-utils';

import InputRangeSlider from 'Js/@components/InputRangeSlider';

const render = (options = {}) => {
	return mount(InputRangeSlider, {
		propsData: {
			value: '',
			min: 1,
			max: 15
		},
		...options
	});
};

test("should emit 'input' event and pass the value when users drag the range input", () => {
	const wrapper = render();

	wrapper.setProps({ value: 1 });

	const input = wrapper.find('input');

	input.element.value = '10';
	input.trigger('input');

	expect(wrapper.emitted().input[0]).toEqual(['10']);
});
