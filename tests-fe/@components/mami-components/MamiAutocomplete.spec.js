import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import MamiAutocomplete from 'Js/@components/mami-components/MamiAutocomplete.vue';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();

describe('MamiAutocomplete.vue', () => {
	const propsData = {
		items: [
			{
				id: 1,
				text: 'Item 1'
			},
			{
				id: 2,
				text: 'Item 2'
			}
		],
		itemValue: 'id',
		itemText: 'text',
		selectedItem: ''
	};

	let wrapper;

	beforeEach(() => {
		jest.useFakeTimers();

		wrapper = shallowMount(MamiAutocomplete, {
			localVue,
			propsData,
			stubs: {
				MamiTextField: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.mami-autocomplete').exists()).toBe(true);
	});

	it('Should emit click event and display item list wrapper when text field is clicked', async () => {
		wrapper.vm.onClickInput();

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('click')).toBeTruthy();
		expect(wrapper.find('[data-path="ddl_autocompleteItems"]').exists()).toBe(
			true
		);
	});

	it('Should emit input event everytime input value is changed', async () => {
		const testVal = 'item';
		wrapper.vm.inputValue = testVal;
		wrapper.vm.onChangeInput();

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('input')[0][0]).toBe(testVal);
	});

	it('Should emit blur evet and hide item list wrapper when input text is blurred', done => {
		wrapper.vm.onBlurInput();

		jest.advanceTimersByTime(500);
		jest.useRealTimers();

		setTimeout(() => {
			expect(wrapper.emitted('blur')).toBeTruthy();
			expect(wrapper.vm.itemListWrapper).toBe(false);
			done();
		}, 0);
	});

	it('Should emit input and select events with selected item value when an item is selected', () => {
		const selectedItem = wrapper.vm.items[0];

		wrapper.vm.onSelectItem(selectedItem);

		expect(wrapper.vm.inputValue).toEqual(selectedItem.text);
		expect(wrapper.emitted('input')[0][0]).toBe(selectedItem.value);
		expect(wrapper.emitted('select')[0][0]).toBe(selectedItem.value);
	});
});
