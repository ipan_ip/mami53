import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import MamiButton from 'Js/@components/mami-components/MamiButton.vue';

const localVue = createLocalVue();

describe('MamiButton.vue', () => {
	const propsData = {
		color: 'primary',
		block: false,
		disabled: false
	};

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(MamiButton, {
			localVue,
			propsData
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.mami-button').exists()).toBe(true);
	});

	it('Should return appropriate extra classes', () => {
		const prefix = 'mami-button--';
		const color = wrapper.vm.color;
		const block = (wrapper.vm.block && 'block') || null;
		const modifier = [color, 'medium', block, null, null, null].map(
			modifier => {
				if (modifier) return `${prefix}${modifier}`;
				return null;
			}
		);

		expect(wrapper.vm.extraClasses).toEqual(modifier);
	});

	it('Should return appropriate extra styles', async () => {
		await wrapper.setProps({
			width: '250px',
			minWidth: '200px'
		});
		const mamiButtonStyle = wrapper.find('.mami-button').element.style;

		expect(mamiButtonStyle.width).toBe('250px');
		expect(mamiButtonStyle.minWidth).toBe('200px');
	});

	it('Should render loading when loading props is true', async () => {
		await wrapper.setProps({ loading: true });
		expect(wrapper.find('.mami-button__loading').exists()).toBe(true);
	});

	it('Should emit click when clicked', async () => {
		await wrapper.setProps({ loading: false });
		const element = wrapper.find('.mami-button');
		element.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('click')).toBeTruthy();
	});
});
