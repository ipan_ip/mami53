import { shallowMount, createLocalVue } from '@vue/test-utils';
import MamiRadio from 'Js/@components/mami-components/MamiRadio.vue';

const localVue = createLocalVue();
describe('MamiRadio.vue', () => {
	const propsData = {
		id: 'mamitest',
		label: 'mamitest label',
		value: false,
		selectedValue: false,
		disabled: false
	};
	const wrapper = shallowMount(MamiRadio, { localVue, propsData });

	it('should render input radio', () => {
		expect(wrapper.find('input[type="radio"]').exists()).toBe(true);
	});

	it('should render label from props', () => {
		expect(wrapper.find('.mami-radio-label').text()).toBe(propsData.label);
	});

	it('should has input radio checked', () => {
		const radio = wrapper.find('input[type="radio"]');
		expect(radio.element.checked).toBe(true);
	});

	it('should not has input radio checked', () => {
		propsData.value = true;
		const wrapper = shallowMount(MamiRadio, { localVue, propsData });
		const radio = wrapper.find('input[type="radio"]');
		expect(radio.element.checked).toBe(false);
	});

	it('should emit change when radio clicked', done => {
		propsData.value = false;
		const wrapper = shallowMount(MamiRadio, { localVue, propsData });
		const radioWrapper = wrapper.find('.mami-radio');
		jest.clearAllMocks();
		radioWrapper.trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('change')[0]).toBeTruthy();
			done();
		});
	});

	it('should update current active value if props value is changed', done => {
		wrapper.setProps({ value: false });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isActive).toBe(false);
			done();
		});
	});

	it('should not update when value is false', done => {
		wrapper.setProps({
			value: 'mami',
			selectedValue: 'mami'
		});

		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isActive).toBe(true);
			jest.clearAllMocks();
			wrapper.setProps({ value: 'mamitest' });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isActive).toBe(false);
				done();
			});
			done();
		});
	});
});
