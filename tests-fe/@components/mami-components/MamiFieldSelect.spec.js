import { shallowMount, createLocalVue } from '@vue/test-utils';
import MamiFieldSelect from 'Js/@components/mami-components/MamiFieldSelect.vue';

const localVue = createLocalVue();
describe('MamiFieldSelect', () => {
	const propsData = {
		fieldLabel: 'Label for select options',
		optionValues: [
			{ val: 1, label: 'one' },
			{ val: 2, label: 'two' }
		],
		isRequired: true,
		defaultValue: { val: 1, label: 'one' }
	};

	const methods = {
		toggleOptions: jest.fn()
	};

	it('should emit selected value', async () => {
		const wrapper = shallowMount(MamiFieldSelect, {
			localVue,
			propsData,
			methods
		});

		wrapper.vm.selectedValue = { val: 2, label: 'two' };
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('select')).toBeTruthy();
		expect(methods.toggleOptions).toBeCalled();
	});

	it('should open the options modal', () => {
		const wrapper = shallowMount(MamiFieldSelect, {
			localVue,
			propsData
		});
		wrapper.vm.toggleOptions();
		expect(wrapper.vm.isShowOptions).toBe(true);
		expect(wrapper.emitted('on-open-options')).toBeTruthy();
	});

	it('should close the options modal', async () => {
		const wrapper = shallowMount(MamiFieldSelect, {
			localVue,
			propsData
		});
		wrapper.vm.isShowOptions = true;
		wrapper.vm.toggleOptions();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isShowOptions).toBe(false);
	});

	describe('isInfinite', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(MamiFieldSelect, {
				localVue,
				propsData: {
					...propsData,
					isInfinite: true,
					isHasMore: true,
					isLoading: false
				},
				methods
			});
		});

		const addEventListener = jest.fn();
		const removeEventListener = jest.fn();

		const createDropdownOptionsRefs = (
			clientHeight,
			scrollTop,
			scrollHeight
		) => {
			wrapper.vm.$refs = {
				dropdownOptions: {
					$el: {
						querySelector: () => {
							return {
								clientHeight,
								scrollTop,
								scrollHeight,
								addEventListener,
								removeEventListener
							};
						}
					}
				}
			};
		};

		it('should add event listener scroll when opened', async () => {
			createDropdownOptionsRefs(400, 0, 600);
			await wrapper.setData({ isShowOptions: false });
			await wrapper.setData({ isShowOptions: true });
			await wrapper.vm.$nextTick();
			expect(addEventListener).toBeCalledWith('scroll', expect.any(Function));
		});

		it('should remove event listener scroll when opened', async () => {
			createDropdownOptionsRefs(400, 0, 600);
			await wrapper.setData({ isShowOptions: true });
			await wrapper.setData({ isShowOptions: false });
			await wrapper.vm.$nextTick();
			expect(removeEventListener).toBeCalledWith(
				'scroll',
				expect.any(Function)
			);
		});

		it('should emit load more when scrolled to bottom', async () => {
			wrapper.setProps({ hasMore: true, isLoading: false });
			createDropdownOptionsRefs(400, 200, 600);
			await wrapper.vm.$nextTick();
			wrapper.vm.setEmitOnBottomScroll();

			expect(wrapper.emitted('load-more')[0]).toBeTruthy();
		});

		it('should remove event listener scroll before destroyed', async () => {
			wrapper.setProps({ isInfinite: true });
			const spy = jest.spyOn(wrapper.vm, 'setScrollEventListener');

			jest.clearAllMocks();

			await wrapper.vm.$destroy();
			expect(spy).toBeCalledWith('remove');
		});

		it('should show inline loading when it is loading', async () => {
			await wrapper.setProps({ isLoading: true });
			await wrapper.vm.$nextTick();

			expect(wrapper.find('.mami-loading-inline').exists()).toBe(true);
		});
	});
});
