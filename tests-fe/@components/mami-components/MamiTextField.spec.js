import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import MamiTextField from 'Js/@components/mami-components/MamiTextField.vue';

const localVue = createLocalVue();

describe('MamiTextField.vue', () => {
	const propsData = {
		inputLabel: 'Some Label',
		inputName: 'Some Name',
		inputType: 'text',
		inputValue: '',
		inputPlaceholder: 'Some Placeholder',
		isRequired: false,
		isAutocomplete: false
	};

	const wrapper = shallowMount(MamiTextField, {
		localVue,
		propsData
	});

	it('Should exists', () => {
		expect(wrapper.find('.mami-text-field').exists()).toBe(true);
	});

	it('Should not display error messages if there is none', () => {
		const errorLabel = wrapper.find('.mami-text-field__error-message');

		expect(wrapper.vm.formattedErrorMessages).toEqual('');
		expect(errorLabel.exists()).toBe(false);
	});

	it('Should display error messages correctly if any', async () => {
		const mockErrorMessage = 'There is an error';

		wrapper.setProps({
			errorMessages: [mockErrorMessage]
		});

		await wrapper.vm.$nextTick();

		const errorLabel = wrapper.find('.mami-text-field__error-message');

		expect(errorLabel.exists()).toBe(true);
		expect(errorLabel.text()).toEqual(mockErrorMessage);
	});

	it('Should emit input event everytime input value is changed', async () => {
		const testVal = 'Test Value';
		wrapper.findComponent({ ref: 'textField' }).setValue(testVal);
		wrapper.findComponent({ ref: 'textField' }).trigger('input');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('input')[0][0]).toBe(testVal);
	});

	it('Should emit click event everytime input text field is clicked', async () => {
		wrapper.findComponent({ ref: 'textField' }).trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('click')).toBeTruthy();
	});

	it('Should display dropdown icon when the component used as autocomplete', async () => {
		wrapper.setProps({
			isAutocomplete: true
		});

		await wrapper.vm.$nextTick();

		const dropdownIcon = wrapper.findComponent({ ref: 'dropdownIcon' });

		expect(dropdownIcon.exists()).toBe(true);
	});

	it('Should emit blur event everytime input text field is blurred', async () => {
		wrapper.findComponent({ ref: 'textField' }).trigger('blur');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('blur')).toBeTruthy();
	});

	it('Should handle show password method properly', async () => {
		expect(wrapper.vm.localType).toBe(null);

		wrapper.setProps({
			inputType: 'password'
		});

		await wrapper.vm.$nextTick();

		const showPasswordIcon = wrapper.find('.show-password');

		expect(wrapper.findComponent({ ref: 'textField' }).attributes('type')).toBe(
			'password'
		);
		expect(showPasswordIcon.exists()).toBe(true);

		showPasswordIcon.trigger('click');

		expect(showPasswordIcon.classes('--open')).toBe(true);
		expect(wrapper.vm.localType).toBe('text');
	});
});
