import { shallowMount, createLocalVue } from '@vue/test-utils';
import MamiTextCounter from 'Js/@components/mami-components/MamiTextCounter.vue';

const localVue = createLocalVue();
describe('MamiTextCounter.vue', () => {
	const propsData = {
		id: 'someId',
		min: 2,
		max: 4,
		step: 2,
		placeholder: 'placeholder',
		label: 'label',
		counter: 0,
		value: '',
		text: '',
		disabled: false,
		disabledCounter: false,
		disabledText: false
	};
	const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });

	it('should render mami-text-counter with correct id', () => {
		expect(wrapper.find('.mami-text-counter').exists()).toBe(true);
		expect(wrapper.find(`#${propsData.id}`).exists()).toBe(true);
	});

	describe('update counter immediately', () => {
		it('should return min value if counter is below min', () => {
			expect(wrapper.emitted('update:counter')[0][0]).toBe(propsData.min);
		});

		it('should return max value if counter is above max', () => {
			propsData.counter = 10;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			expect(wrapper.emitted('update:counter')[0][0]).toBe(propsData.max);
		});
	});

	describe('update counter when counter button clicked', () => {
		it('should add counter when inbound', done => {
			propsData.counter = 2;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const expectedCounter = propsData.counter + propsData.step;
			const counterPlus = wrapper.find('.mami-text-counter__counter-plus');
			counterPlus.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('update:counter')[0][0]).toBe(expectedCounter);
				done();
			});
		});

		it('should subtract counter when inbound', done => {
			propsData.counter = 4;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const expectedCounter = propsData.counter - propsData.step;
			const counterMinus = wrapper.find('.mami-text-counter__counter-minus');
			counterMinus.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('update:counter')[0][0]).toBe(expectedCounter);
				done();
			});
		});

		it('should not add counter when out of bound', done => {
			propsData.counter = 4;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const counterPlus = wrapper.find('.mami-text-counter__counter-plus');
			counterPlus.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('update:counter')).toBeFalsy();
				done();
			});
		});

		it('should not subtract counter when out of bound', done => {
			propsData.counter = 2;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const counterMinus = wrapper.find('.mami-text-counter__counter-minus');
			counterMinus.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('update:counter')).toBeFalsy();
				done();
			});
		});

		it('should not do anything if it is disabled', done => {
			propsData.disabled = true;
			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const counterMinus = wrapper.find('.mami-text-counter__counter-minus');
			counterMinus.trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('update:counter')).toBeFalsy();
				done();
			});
		});
	});

	describe('check UI from given props and actions', () => {
		it('should set style from props', () => {
			propsData.width = 100;
			propsData.color = 'blue';

			const wrapper = shallowMount(MamiTextCounter, { localVue, propsData });
			const inputStyle = wrapper.find(`#${propsData.id}`).element.style;
			expect(inputStyle.borderColor).toBe(propsData.color);
			expect(inputStyle.caretColor).toBe(propsData.color);
		});

		it('should set focus when input focused', () => {
			const input = wrapper.find(`input#${propsData.id}`);
			input.trigger('focus');
			expect(wrapper.vm.isFocused).toBe(true);
		});

		it('should update input value', () => {
			wrapper.setData({ inputValue: 'new value' });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.emitted('input')).toBeTruthy();
			});
		});
	});
});
