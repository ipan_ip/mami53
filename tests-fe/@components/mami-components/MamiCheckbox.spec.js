import { shallowMount, createLocalVue } from '@vue/test-utils';
import '@babel/polyfill';
import MamiCheckbox from 'Js/@components/mami-components/MamiCheckbox.vue';

const localVue = createLocalVue();
describe('MamiCheckbox.vue', () => {
	const propsData = {
		id: 'mamitest',
		value: false,
		disabled: false
	};
	const wrapper = shallowMount(MamiCheckbox, { localVue, propsData });

	it('should render input checkbox', () => {
		expect(wrapper.find('input[type="checkbox"]').exists()).toBe(true);
	});

	it('should emit active status when checkbox clicked', async () => {
		const checkbox = wrapper.find('input[type="checkbox"]');
		checkbox.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('change')).toBeTruthy();
		expect(wrapper.emitted('checkbox-clicked')).toBeTruthy();
	});
});
