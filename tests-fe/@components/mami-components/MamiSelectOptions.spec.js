import { shallowMount, createLocalVue } from '@vue/test-utils';
import MamiSelectOptions from 'Js/@components/mami-components/MamiSelectOptions.vue';

const localVue = createLocalVue();
const propsData = {
	options: [
		{
			value: 1,
			label: 'one'
		},
		{
			value: 2,
			label: 'two'
		},
		{
			value: 3,
			label: 'three'
		}
	],
	value: 1,
	optionsValue: 'value',
	optionsLabel: 'label',
	placeholder: 'placeholder'
};

describe('MamiSelectOptions.vue', () => {
	const wrapper = shallowMount(MamiSelectOptions, { localVue, propsData });

	it('should render component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit input if selected Value changed', async () => {
		await wrapper.setData({ selectedValue: 2 });
		expect(wrapper.emitted('input')[0][0]).toBe(2);
	});

	it('should set selectedValue if props value changed', async () => {
		await wrapper.setProps({ value: 3 });
		expect(wrapper.vm.selectedValue).toBe(3);
	});
});
