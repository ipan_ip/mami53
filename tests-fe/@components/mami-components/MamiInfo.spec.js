import { shallowMount, createLocalVue } from '@vue/test-utils';
import MamiInfo from 'Js/@components/mami-components/MamiInfo';

const localVue = createLocalVue();

describe('MamiInfo.vue', () => {
	const wrapper = shallowMount(MamiInfo, { localVue });

	it('should render MamiInfo component properly', () => {
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set icon as selected variant if variant has its own icon', () => {
		wrapper.setProps({ variant: 'success' });
		expect(wrapper.vm.infoIcon).toBe(wrapper.vm.iconComponents.success);
	});

	it('should set icon as info if no given icon value and selected variant has no icon', () => {
		wrapper.setProps({ variant: 'disabled' });
		expect(wrapper.vm.infoIcon).toBe(wrapper.vm.iconComponents.info);
	});

	it('should set icon as given icon props', () => {
		wrapper.setProps({ variant: 'success', icon: 'error' });
		expect(wrapper.vm.infoIcon).toBe(wrapper.vm.iconComponents.error);
	});
});
