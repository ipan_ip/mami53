import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline.vue';

describe('MamiLoadingInline.vue', () => {
	it('message props should show correct String value', () => {
		const localVue = createLocalVue();
		const loadingMessage = 'Memuat data';
		const wrapper = shallowMount(MamiLoadingInline, {
			localVue,
			propsData: {
				message: loadingMessage
			}
		});

		expect(typeof wrapper.vm.message !== 'string').toBe(false);
		expect(wrapper.vm.message).toBe('Memuat data');

		expect(wrapper.find('h3').exists()).toBe(true);
		expect(wrapper.find('h3').text()).toBe(loadingMessage);
	});
});
