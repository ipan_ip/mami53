import { createLocalVue, shallowMount } from '@vue/test-utils';
import relatedLinks from './__mocks__/related-links.json';
import relatedListKost from './__mocks__/related-list-kost.json';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import RelatedList from 'Js/@components/RelatedList.vue';

const localVue = createLocalVue();
global.relatedKostLink = relatedLinks.relatedKostLink;
const bugsnagNotify = jest.fn();
global.bugsnagClient = { notify: bugsnagNotify };
const tracker = jest.fn();
global.tracker = tracker;
window.open = jest.fn();

describe('RelatedList.vue', () => {
	const propsData = {
		propertyType: 'kost',
		params: { filters: { sorting: 'rand' } }
	};
	const wrapper = shallowMount(RelatedList, { localVue, propsData });

	it('should mount the component', () => {
		expect(wrapper.find('.suggestion-container').exists()).toBe(true);
	});

	it('should set empty related kost list flag to true', async () => {
		const axios = jest.fn().mockResolvedValue({});
		global.axios = axios;
		await wrapper.vm.getRelatedList();
		expect(axios).toBeCalled();
		expect(wrapper.vm.isEmptyRelatedListKost).toBe(true);
	});

	it('should assign new value to related list data', async () => {
		const axios = jest
			.fn()
			.mockResolvedValue({ status: 200, data: { kost: relatedListKost } });
		global.axios = axios;
		await wrapper.vm.getRelatedList();
		expect(axios).toBeCalled();
		expect(wrapper.vm.relatedListKost).toBe(relatedListKost);
	});

	describe('Open Link', () => {
		const trackerProps = (type, recomend, from) => {
			return [
				type,
				[
					'send',
					'event',
					{
						eventCategory: 'Related Landing',
						eventAction: 'click',
						eventLabel: 'rekomend_' + recomend + '_lp_' + from
					}
				]
			];
		};

		// it('should open related link', () => {
		// 	wrapper.vm.openLink('/slug');
		// 	expect(open).toBeCalledWith('/slug', '_blank');
		// });

		it('should track ga for kost-apt if it is kost and propertyType is apartment', () => {
			wrapper.setProps({ propertyType: 'apartment' });
			wrapper.vm.openLinkKost('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'kost', 'apt'));
		});

		it('should track ga for kost-loker if it is kost and propertyType is vacancy', () => {
			wrapper.setProps({ propertyType: 'vacancy' });
			wrapper.vm.openLinkKost('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'kost', 'loker'));
		});

		it('should track ga for apt-kost if it is apartment and propertyType is kost', () => {
			wrapper.setProps({ propertyType: 'kost' });
			wrapper.vm.openLinkApartment('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'apt', 'kost'));
		});

		it('should track ga for apt-loker if it is apartment and propertyType is vacancy', () => {
			wrapper.setProps({ propertyType: 'vacancy' });
			wrapper.vm.openLinkApartment('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'apt', 'loker'));
		});

		it('should track ga for loker-kost if it is vacancy and propertyType is kost', () => {
			wrapper.setProps({ propertyType: 'kost' });
			wrapper.vm.openLinkVacancy('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'loker', 'kost'));
		});

		it('should track ga for loker-apt if it is vacancy and propertyType is apartment', () => {
			wrapper.setProps({ propertyType: 'vacancy' });
			wrapper.vm.openLinkVacancy('');
			expect(tracker).toBeCalledWith(...trackerProps('ga', 'loker', 'apt'));
		});
	});

	describe('matchMedia and standalone', () => {
		it('should call window open with the link params', () => {
			wrapper.vm.openLink('mamikos.com');
			expect(window.open).toBeCalledWith('mamikos.com', '_blank');
		});

		it('should not call window open', () => {
			mockWindowProperty('location', { href: '/someslug' });
			mockWindowProperty(
				'matchMedia',
				jest.fn(() => {
					return { matches: true };
				})
			);

			jest.clearAllMocks();
			wrapper.vm.openLinkKost();
			wrapper.vm.openLinkApartment();
			wrapper.vm.openLinkVacancy();
			wrapper.vm.openLink();
			expect(open).not.toBeCalled();
		});
	});
});
