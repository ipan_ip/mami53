import { createLocalVue, shallowMount } from '@vue/test-utils';
import TagText from 'Js/@components/TagText.vue';

describe('TagText.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(TagText, {
		localVue,
		propsData: {
			ellipsis: true,
			text: 'tag text',
			variant: 'green'
		}
	});

	it('should render correctly', () => {
		expect(wrapper.find('.tag-text').exists()).toBe(true);
		expect(wrapper.find('.tag-text').text()).toBe('tag text');
		expect(wrapper.find('.tag-text').classes()).toContain('tag-ellipsis');
		expect(wrapper.find('.tag-text').classes()).toContain('--green');
	});
});
