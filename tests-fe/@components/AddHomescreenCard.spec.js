import { createLocalVue, shallowMount } from '@vue/test-utils';
import AddHomescreenCard from 'Js/@components/AddHomescreenCard.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('AddHomescreenCard.vue', () => {
	const localVue = createLocalVue();
	let wrapper;
	const deferredPrompt = {
		prompt: jest.fn(),
		userChoice: Promise.resolve({ outcome: 'accepted' })
	};

	beforeEach(() => {
		global.window.innerWidth = 700;
		wrapper = shallowMount(AddHomescreenCard, { localVue });
		wrapper.setData({ deferredPrompt });
		mockWindowProperty(
			'addEventListener',
			jest.fn((e, cb = jest.fn()) =>
				// eslint-disable-next-line standard/no-callback-literal
				cb({ preventDefault: jest.fn(), ...deferredPrompt })
			)
		);
		mockWindowProperty('document.querySelector', () => {
			return {
				getBoundingClientRect: jest.fn(() => {
					return { top: 200, left: 200, bottom: 0, right: 0 };
				})
			};
		});
		global.tracker = jest.fn();
		global.alert = jest.fn();
	});

	it('should not render pwa-install-card', () => {
		expect(wrapper.find('.pwa-install-card').exists()).toBe(true);
	});

	it('should show prompt when pwa button clicked', () => {
		expect(wrapper.find('.pwa-install-card').exists()).toBe(true);
		wrapper.find('.pwa-install-card button').trigger('click');
		expect(deferredPrompt.prompt).toBeCalled();
	});

	it('should send user dismiss info when user dismiss prompt', () => {
		deferredPrompt.userChoice = Promise.resolve({ outcome: 'rejected' });
		wrapper.setData({ deferredPrompt });
		mockWindowProperty(
			'addEventListener',
			jest.fn((e, cb = jest.fn()) =>
				// eslint-disable-next-line standard/no-callback-literal
				cb({ preventDefault: jest.fn(), ...deferredPrompt })
			)
		);
		wrapper.vm.$nextTick(() => {
			wrapper.find('.pwa-install-card button').trigger('click');
			expect(deferredPrompt.prompt).toBeCalled();
		});
	});

	it('should hide card when close button clicked', () => {
		wrapper.find('.close').trigger('click');
		expect(wrapper.vm.deferredPrompt).toBe(null);
	});
});
